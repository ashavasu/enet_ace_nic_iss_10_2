/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpntlnk.c,v 1.35 2017/12/20 11:18:45 siva Exp $
 *
 * Description: Functions handling the Linux IP NETLINK notification
 *              for ARP table.
 *
 *******************************************************************/
#ifndef _ARPNTLNK_C
#define _ARPNTLNK_C

#include "arpinc.h"
#include "lnxip.h"

INT4                gi4NetlinkSock = -1;
static INT4         ArpNetlinkSockInit (VOID);

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetLinkInit
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action             : This function initializes the LINUX NETLINK. 
 *         
+-------------------------------------------------------------------*/
INT4
ArpNetLinkInit (VOID)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];
#else

    INT4                ai4Appv4Solicit[] = { CTL_NET, NET_IPV4, NET_IPV4_NEIGH,
        NET_PROTO_CONF_DEFAULT,
        NET_NEIGH_APP_SOLICIT
    };
#ifdef IP6_WANTED
    INT4                ai4Appv6Solicit[] = { CTL_NET, NET_IPV6, NET_IPV6_NEIGH,
        NET_PROTO_CONF_DEFAULT,
        NET_NEIGH_APP_SOLICIT
    };
#endif
    INT4                i4OldVal = 0;
    UINT4               u4OldLen = sizeof (i4OldVal);
    INT4                i4NameLen = sizeof (ai4Appv4Solicit) / sizeof (INT4);
#endif

    INT4                i4NewVal = ARP_NETLINK_MAX_NOTIFY;

    /* To set the app_solicit - the max no. of probes to send to user space */
#ifdef LINUX_310_WANTED
    SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
              "echo %d >/proc/sys/net/ipv4/neigh/default/app_solicit",
              i4NewVal);
    system (ac1Command);
#ifdef IP6_WANTED
    SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
              "echo %d >/proc/sys/net/ipv6/neigh/default/app_solicit",
              i4NewVal);
    system (ac1Command);
#endif

#else
    if (sysctl ((int *) ai4Appv4Solicit, i4NameLen, &i4OldVal,
                (size_t *) & u4OldLen, &i4NewVal, sizeof (i4NewVal)) < 0)
    {
        system ("/sbin/sysctl -w net.ipv4.neigh.default.app_solicit=1");
        perror ("sysctl for APP_SOLICIT failed due to !!!\n");
    }
#ifdef IP6_WANTED
    i4NameLen = sizeof (ai4Appv6Solicit) / sizeof (INT4);
    if (sysctl ((int *) ai4Appv6Solicit, i4NameLen, &i4OldVal,
                (size_t *) & u4OldLen, &i4NewVal, sizeof (i4NewVal)) < 0)
    {
        perror ("sysctl for APP_SOLICIT failed due to !!!\n");
        return ARP_FAILURE;
    }
#endif
#endif
    /* To create a NETLINK socket to recv the notification from the kernel */
    if (ArpNetlinkSockInit () == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }
    SelAddFd (gi4NetlinkSock, ArpNotifyArpTask);

    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNotifyArpTask
 *
 * Input(s)           : i4SockFd - Sock desc   
 *
 * Output(s)          : None. 
 *
 * Returns            : None. 
 *
 * Action             : This function will notify the ARP task about the 
 *                      update in Linux kernel ARP table.
 *         
+-------------------------------------------------------------------*/
VOID
ArpNotifyArpTask (INT4 i4SockFd)
{
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (OsixQueSend (ARP_VRF_IP_PKT_Q_ID,
                     (UINT1 *) (&i4SockFd), OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        return;
    }
    OsixEvtSend (ARP_TASK_ID, ARP_KERNEL_UPD_EVENT);
#else
    UNUSED_PARAM (i4SockFd);
    OsixEvtSend (ARP_TASK_ID, ARP_KERNEL_UPD_EVENT);
#endif
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetlinkSockInit
 *
 * Input(s)           : None. 
 *
 * Output(s)          : pFd - ID for the initialized socket.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE. 
 *
 * Action             : This function will initialise the socket to the
 *                      specified address family.
 *         
+-------------------------------------------------------------------*/
static INT4
ArpNetlinkSockInit (VOID)
{
    UINT4               u4Groups;
    INT4                i4Ret;
    struct sockaddr_nl  snl;

    u4Groups =
        RTMGRP_NOTIFY | RTMGRP_NEIGH | RTMGRP_IPV6_IFADDR | RTMGRP_IPV6_PREFIX;

    gi4NetlinkSock = socket (AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (gi4NetlinkSock < 0)
    {
        perror ("ARP NETLINK socket creation failed.\n");
        return ARP_FAILURE;
    }

    i4Ret = fcntl (gi4NetlinkSock, F_SETFL, O_NONBLOCK);
    if (i4Ret < 0)
    {
        perror ("ARP NETLINK fcntl failed.\n");
        close (gi4NetlinkSock);
        return ARP_FAILURE;
    }

    MEMSET (&snl, 0, sizeof snl);
    snl.nl_family = AF_NETLINK;
    snl.nl_groups = u4Groups;

    /* Bind the socket to the netlink structure for anything. */
    i4Ret = bind (gi4NetlinkSock, (struct sockaddr *) &snl, sizeof snl);
    if (i4Ret < 0)
    {
        perror ("ARP NETLINK socket bind failed .\n");
        close (gi4NetlinkSock);
        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetlinkParseInfo
 *
 * Input(s)           : filter - Call back routine
 *
 * Output(s)          : None. 
 *
 * Returns            : ARP_FAILURE or ARP_SUCCESS
 *
 * Action             : Parse the response message received from Linux IP
 *                      for the  arp request message. 
 *         
+-------------------------------------------------------------------*/
INT4
ArpNetlinkParseInfo (INT4 (*filter) (struct nlmsghdr *), INT4 i4SockId)
{
    INT4                i4Status;
    INT4                i4Ret = 0;
    INT4                i4Error;

    while (1)
    {
        INT1                i1Buff[MAX_NETLINK_BUF_LEN];
        struct iovec        iov = { NULL, 0 };
        struct sockaddr_nl  snl;
        struct msghdr       msg =
            { (VOID *) NULL, sizeof snl, (VOID *) NULL, 1, NULL,
            0, 0
        };
        struct nlmsghdr    *h;

        iov.iov_base = (VOID *) i1Buff;
        iov.iov_len = sizeof (i1Buff);
        msg.msg_name = (VOID *) &snl;
        msg.msg_iov = (VOID *) &iov;

        i4Status = recvmsg (i4SockId, &msg, 0);

        if (i4Status < 0)
        {
            if (errno == EINTR)
                continue;
            if (errno == EWOULDBLOCK)
                break;
            continue;
        }

        if (i4Status == 0)
        {
            return ARP_FAILURE;
        }

        if (msg.msg_namelen != sizeof snl)
        {
            return ARP_FAILURE;
        }

        for (h = (struct nlmsghdr *) i1Buff; NLMSG_OK (h, (UINT4) i4Status);
             h = NLMSG_NEXT (h, i4Status))
        {
            /* Finish of reading. */
            if (h->nlmsg_type == NLMSG_DONE)
                return ARP_SUCCESS;

            /* Error handling. */
            if (h->nlmsg_type == NLMSG_ERROR)
            {
                struct nlmsgerr    *pErr = (struct nlmsgerr *) NLMSG_DATA (h);

                /* If the error field is zero, then this is an ACK */
                if (pErr->error == 0)
                {
                    /* return if not a multipart message, otherwise continue
                     *                      *                      */
                    if (!(h->nlmsg_flags & NLM_F_MULTI))
                    {
                        return ARP_SUCCESS;
                    }
                    continue;
                }

                if (h->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr)))
                {
                    return ARP_FAILURE;
                }
                return ARP_FAILURE;
            }
            if (filter)
            {
                i4Error = (*filter) (h);
                if (i4Error == ARP_FAILURE)
                {
                    i4Ret = i4Error;
                    UNUSED_PARAM (i4Ret);
                }
            }
        }

        /* After error care. */
        if (msg.msg_flags & MSG_TRUNC)
        {
            continue;
        }
        if (i4Status)
        {
            return ARP_FAILURE;
        }
        return ARP_SUCCESS;
    }
    return ARP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetlinkInformationFetch
 *
 * Input(s)           : h - netlink message header
 *
 * Output(s)          : None. 
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that handles the arp/link change indication
 *                      message from the Linux Kernal.
 *         
+-------------------------------------------------------------------*/
INT4
ArpNetlinkInformationFetch (struct nlmsghdr *h)
{
    switch (h->nlmsg_type)
    {
        case RTM_GETNEIGH:
        case RTM_NEWNEIGH:
        case RTM_DELNEIGH:
        case RTM_NEWADDR:
        case RTM_DELADDR:
        case RTM_GETADDR:
            return ArpNetlinkNeighChange (h);
            break;

        default:
            break;
    }
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetlinkNeighChange
 *
 * Input(s)           : h - netlink message header
 *
 * Output(s)          : None. 
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that handles the neigh change indication
 *                      message from the Linux Kernal.
 *         
+-------------------------------------------------------------------*/
INT4
ArpNetlinkNeighChange (struct nlmsghdr *h)
{
    INT4                i4Len;
    struct ndmsg       *pNdmsg;
    struct rtattr      *ptb[RTA_MAX + 1];
    struct ifaddrmsg   *paddrmsg;
    t_ARP_CACHE         tArpCache;
    INT4                i4ArpCheck;
    UINT1               au1Mac[CFA_ENET_ADDR_LEN];
    tARP_DATA           tArpData;
    UINT4               u4IfIndex;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpCxt            *pArpCxt = NULL;
    tUtlInAddr          InAddr;
    t_ARP_CACHE        *pArpEntry = NULL;
    UINT1               u1NextHopFlag;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
    MEMSET (&Ip6Addr, 0, sizeof (Ip6Addr));
#endif
    UINT4               u1Rtasize;
    struct rtattr      *pRta;
    struct ifaddrmsg   *pIfam;
    struct in6_addr    *pIp6Addr;

    MEMSET (&InAddr, 0, sizeof (InAddr));
    pNdmsg = NLMSG_DATA (h);
    paddrmsg = NLMSG_DATA (h);

    if (h->nlmsg_type == RTM_NEWADDR)
    {
        pIfam = (struct ifaddrmsg *) NLMSG_DATA (h);
        /* pRta = IFLA_RTA(pIfam); */
        pRta =
            ((struct rtattr *) ((void *) (((char *) (pIfam))) +
                                NLMSG_ALIGN (sizeof (struct ifaddrmsg))));
        u1Rtasize = IFLA_PAYLOAD (h);
        if (RTA_OK (pRta, u1Rtasize))
        {
            /*rta_payload = RTA_PAYLOAD (pRta); */
            switch (pRta->rta_type)
            {
                case IFA_ADDRESS:

                    switch (pIfam->ifa_family)
                    {
                        case AF_INET6:
                            pIp6Addr = (struct in6_addr *) RTA_DATA (pRta);
#ifdef IP6_WANTED
                            MEMCPY (&Ip6Addr, (pIp6Addr->s6_addr16),
                                    sizeof (tIp6Addr));
#endif
                            break;
                        default:
                            break;

                    }
                    break;
                default:
                    break;
            }
        }
    }

    if (!(h->nlmsg_type == RTM_NEWNEIGH || h->nlmsg_type == RTM_DELNEIGH
          || h->nlmsg_type == RTM_GETNEIGH || h->nlmsg_type == RTM_NEWADDR
          || h->nlmsg_type == RTM_DELADDR || h->nlmsg_type == RTM_GETADDR))
    {
        return ARP_FAILURE;
    }

    i4Len = h->nlmsg_len - NLMSG_LENGTH (sizeof (struct ndmsg));
    if (i4Len < 0)
    {
        return ARP_FAILURE;
    }
    MEMSET (ptb, 0, sizeof ptb);
    MEMSET (au1Mac, 0, CFA_ENET_ADDR_LEN);

    ArpNetlinkParseRtattr (ptb, RTA_MAX, RTM_RTA (pNdmsg), i4Len);

    if ((pNdmsg->ndm_state != NUD_REACHABLE) &&
        (pNdmsg->ndm_state != NUD_PERMANENT) &&
        (pNdmsg->ndm_state != NUD_STALE) &&
        (pNdmsg->ndm_state != NUD_FAILED) &&
        (pNdmsg->ndm_state != NUD_INCOMPLETE) &&
        (pNdmsg->ndm_state != NUD_DELAY) &&
        (h->nlmsg_type != RTM_NEWADDR) &&
        (h->nlmsg_type != RTM_DELADDR) && (h->nlmsg_type != RTM_GETADDR))
    {
        return ARP_FAILURE;
    }

    if (ptb[NDA_DST])
    {
        if (pNdmsg->ndm_family == AF_INET)
        {
            InAddr.u4Addr = *(UINT4 *) RTA_DATA (ptb[NDA_DST]);
        }
#ifdef IP6_WANTED
        else if (pNdmsg->ndm_family == AF_INET6)
        {
            MEMCPY (&Ip6Addr, RTA_DATA (ptb[NDA_DST]), sizeof (tIp6Addr));
        }
#endif
    }

    if (ptb[NDA_LLADDR])
    {
        MEMCPY (au1Mac, (UINT1 *) RTA_DATA (ptb[NDA_LLADDR]),
                CFA_ENET_ADDR_LEN);
    }

    if (NetIpv4GetIfInfo ((UINT4) pNdmsg->ndm_ifindex, &NetIpIfInfo)
        != NETIPV4_SUCCESS)
    {
        return ARP_FAILURE;
    }

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) pNdmsg->ndm_ifindex, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    if (pNdmsg->ndm_family == AF_INET)
    {
        pArpCxt = gArpGblInfo.apArpCxt[NetIpIfInfo.u4ContextId];
        if (pArpCxt == NULL)
        {
            return ARP_FAILURE;
        }
        tArpData.u4IpAddr = OSIX_NTOHL (InAddr.u4Addr);
        tArpData.u2Port = pNdmsg->ndm_ifindex;
        tArpData.i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);
        MEMCPY (tArpData.i1Hw_addr, (INT1 *) au1Mac, CFA_ENET_ADDR_LEN);
        tArpData.u1EncapType = NetIpIfInfo.u1EncapType;
        tArpData.i1Hwalen = CFA_ENET_ADDR_LEN;

        i4ArpCheck = ArpLookupWithIndex (u4IfIndex, tArpData.u4IpAddr,
                                         &tArpCache);

        switch (pNdmsg->ndm_state)
        {
            case NUD_REACHABLE:
            case NUD_DELAY:
                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                              CONTROL_PLANE_TRC, ARP_NAME,
                              "ARP notification received for %s as"
                              "NUD_REACHABLE state.\n", INET_NTOA (InAddr));
                tArpData.i1State = ARP_DYNAMIC;
                ArpAddArpEntry (tArpData, NULL, 0);
#ifdef ICCH_WANTED
                ArpIcchSyncArpMsg (&tArpData, u4IfIndex);
#endif /* ICCH_WANTED */
                break;

            case NUD_INCOMPLETE:
                /* should not allow the pending entry to override
                 * agedout entry.
                 */
                if (i4ArpCheck != ARP_SUCCESS)
                {
                    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  CONTROL_PLANE_TRC, ARP_NAME,
                                  "ARP notification received for %s as"
                                  "NUD_INCOMPLETE state.\n",
                                  INET_NTOA (InAddr));
                    tArpData.i1State = ARP_PENDING;
                    ArpAddArpEntry (tArpData, NULL, 0);
                }
                /* If the kernel tries to learn arp entry for an unrechable 
                 * address it gives NUD_INCOMPLETE followed by  NUD_FAILED 
                 * notification for each retry.
                 * To avoid adding the entry again, 
                 * the retry count is checked.
                 */
                else if (i4ArpCheck == ARP_SUCCESS &&
                         tArpCache.i1State == ARP_PENDING)
                {
                    if (tArpCache.i4Retries > pArpCxt->Arp_config.i4Max_retries)
                    {
                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      CONTROL_PLANE_TRC, ARP_NAME,
                                      "ARP notification received for %s as"
                                      "NUD_INCOMPLETE state.\n",
                                      INET_NTOA (InAddr));
                        tArpData.i1State = ARP_PENDING;
                        ArpAddArpEntry (tArpData, NULL, 0);
                    }
                }
                break;
            case NUD_STALE:
                /* This state is notified for two cases
                 * 1.Dyanamically deleted Entries.
                 * 2.Entries learned Automatically from the incoming packets
                 */
                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                              CONTROL_PLANE_TRC, ARP_NAME,
                              "ARP notification received for %s as"
                              "NUD_STALE state.\n", INET_NTOA (InAddr));
                /* Notification for aged out entry */
                if (i4ArpCheck == ARP_SUCCESS
                    && tArpCache.i1State != ARP_PENDING
                    && tArpCache.i1State != ARP_AGEOUT)
                {
                    ARP_PROT_UNLOCK ();
                    u1NextHopFlag =
                        RtmApiIsReachableNextHopInCxt (pArpCxt->u4ContextId,
                                                       tArpCache.u4Ip_addr);
                    ARP_PROT_LOCK ();

#ifdef NPAPI_WANTED
                    /* Check for HitBit of the ARP entry and accordingly
                       delete the ARP Entry */
                    if (ArpFsNpIpv4VrfCheckHitOnArpEntry (pArpCxt->u4ContextId,
                                                          tArpCache.u4Ip_addr,
                                                          u1NextHopFlag) ==
                        FNP_FALSE)
                    {
                        ARP_DS_LOCK ();
                        pArpEntry = RBTreeGet (gArpGblInfo.ArpTable,
                                               (tRBElem *) & tArpCache);
                        if (pArpEntry != NULL)
                        {
                            /* Drop the ARP entry as no traffic is going */
                            arp_drop (pArpEntry);
                        }
                        ARP_DS_UNLOCK ();
                    }
                    else
                    {

#if defined (LNXIP4_WANTED)
                        /* Delete the entry in the kernel arp cache 
                           This is work around patch for the kernel bug. kernel
                           does not give indication to ISS at the time of relearning                                        the arp entry */
                        ArpUpdateKernelEntry (ARP_INVALID,
                                              (UINT1 *) tArpData.i1Hw_addr,
                                              tArpData.i1Hwalen,
                                              tArpData.u4IpAddr,
                                              NetIpIfInfo.au1IfName);
#endif /* LNXIP4_WANTED */

                        /* Trigger ARP request to refresh the ARP cache */
                        tArpData.i1State = ARP_AGEOUT;
                        ArpAddArpEntry (tArpData, NULL, 0);
#ifdef ICCH_WANTED
                        ArpIcchSyncArpMsg (&tArpData, u4IfIndex);
#endif /* ICCH_WANTED */
                    }
#else
                    if (h->nlmsg_type == RTM_DELNEIGH)
                    {
                        ARP_DS_LOCK ();
                        pArpEntry = RBTreeGet (gArpGblInfo.ArpTable,
                                               (tRBElem *) & tArpCache);
                        if (pArpEntry != NULL)
                        {
                            arp_drop (pArpEntry);
                        }
                        ARP_DS_UNLOCK ();
                    }
#endif /* NPAPI_WANTED */
                }
                /* Notification for entry learned from the ARP request */
                else
                {
                    /* Stale indication is coming for an ARP entry that is not present 
                     * in the control plane and hardware.
                     * ARP entry is still not created in the control plane and 
                     * hence no action need to be taken in the control plane and hardware
                     */
#if defined (LNXIP4_WANTED)
                    /* Delete the entry in the kernel arp cache
                       This is work around patch for the kernel bug. kernel
                       does not give indication to ISS at the time of relearning
                       the arp entry */
#ifdef LINUX_310_WANTED
                    ArpUpdateKernelEntry (ARP_INVALID,
                                          (UINT1 *) tArpData.i1Hw_addr,
                                          tArpData.i1Hwalen,
                                          tArpData.u4IpAddr,
                                          NetIpIfInfo.au1IfName);
#else
                    tArpData.i1State = ARP_DYNAMIC;
                    ArpAddArpEntry (tArpData, NULL, 0);
#ifdef ICCH_WANTED
                    ArpIcchSyncArpMsg (&tArpData, u4IfIndex);
#endif /* ICCH_WANTED */
#endif
#endif /* LNXIP4_WANTED */
                }
                break;

            case NUD_FAILED:
                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                              CONTROL_PLANE_TRC, ARP_NAME,
                              "ARP notification received for %s as"
                              "NUD_FAILED state.\n", INET_NTOA (InAddr));
                if (i4ArpCheck == ARP_SUCCESS)
                {
                    /* If the kernel tries to learn arp entry for an unrechable 
                     * address it gives NUD_INCOMPLETE followed by  two NUD_FAILED 
                     * notification for each retry in case of BCM platform.
                     * So the retry check has been updated to retry only in case if 
                     * the count is one and in case of
                     * second FAILED state indication, the Arp entry is deleted from ISS.
                     */

                    if ((tArpCache.i1State == ARP_PENDING ||
                         tArpCache.i1State == ARP_AGEOUT) &&
                        (tArpCache.i4Retries == ARP_ONE))

                    {
                        /* For Ageout entries the retry count will be 
                         * incremented in the arp_timer_expiry_handler
                         * Since retry timer will be started. */
                        if (tArpCache.i1State == ARP_PENDING)
                        {
                            ArpIncArpCacheRetryWithIndex (u4IfIndex,
                                                          tArpCache.u4Ip_addr);
                        }
                    }
                    else if (tArpCache.i1State != ARP_STATIC)
                    {
                        ArpDeleteArpCacheWithIndex (u4IfIndex,
                                                    tArpCache.u4Ip_addr);
                    }
                }
                break;
            default:
                break;
        }
    }
#ifdef IP6_WANTED
    else if (pNdmsg->ndm_family == AF_INET6)
    {
        if ((h->nlmsg_type == RTM_NEWADDR) || (h->nlmsg_type == RTM_DELADDR))
        {
            NetIpv6UpdateIntfAddrStatus (u4IfIndex, Ip6Addr,
                                         paddrmsg->ifa_flags);
        }

        NetIpv6UpdateNDCache (u4IfIndex, &Ip6Addr, &au1Mac, pNdmsg->ndm_state,
                              h->nlmsg_type);
    }
#endif
    UNUSED_PARAM (u1NextHopFlag);
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpNetlinkParseRtattr
 *
 * Input(s)           : pRta - NdMsg info 
 *                      max - RTA max length 
 *                      i4Len - length of NdMsg      
 *
 * Output(s)          : ptb - Netlink buf rcvd. 
 *
 * Returns            : None 
 *
 * Action             : Function to parse the Nelink buf  
 *
+-------------------------------------------------------------------*/
VOID
ArpNetlinkParseRtattr (struct rtattr **ptb, INT4 max, struct rtattr *pRta,
                       INT4 i4Len)
{
    while (RTA_OK (pRta, i4Len))
    {
        if (pRta->rta_type <= max)
            ptb[pRta->rta_type] = pRta;
        pRta = RTA_NEXT (pRta, i4Len);
    }
}
#endif /* _ARPNTLNK_C */
