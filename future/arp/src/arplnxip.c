/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arplnxip.c,v 1.49 2017/12/20 11:18:45 siva Exp $
 *
 * Description: Functions handling the Linux IP arp table
 *              updation.
 *
 *******************************************************************/
#ifndef _ARPLNXIP_C
#define _ARPLNXIP_C

#include "arpinc.h"
#include "fssocket.h"
#include "lnxip.h"

#define ARP_CONNECT_PORT  1025

#ifdef LNXIP4_WANTED
/*-------------------------------------------------------------------+
 *
 * Function           : ArpUpdateKernelEntry
 *
 * Input(s)           : i1EntryType - ARP_STATIC/ARP_INVALID 
 *                      pu1MacAddr - physical address to be stored.
 *                      i4MacAddrLen - physical address length.
 *                      u4IpAddr - Ip address to be added in the kernel
 *                      pu1IfName - Interface name.
 * Output(s)          : None.
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that handles the static entry addition
 *                      in the Linux Kernal arp cache.
 *
+-------------------------------------------------------------------*/
INT4
ArpUpdateKernelEntry (INT1 i1EntryType, UINT1 *pu1MacAddr, INT4 i4MacAddrLen,
                      UINT4 u4IpAddr, UINT1 *pu1IfName)
{
    INT4                i4SockFd = -1;
    struct arpreq       ArpReq;
    struct sockaddr_in  IpAddr;
    UINT4               u4IoctlReq = 0;
    INT4                i4RetVal = ARP_SUCCESS;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    tLnxVrfEventInfo    LnxVrfEventInfo;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;

#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1IfName);
    if (pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    MEMSET (&LnxVrfEventInfo, 0, sizeof (tLnxVrfEventInfo));
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET;
    LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
    LnxVrfEventInfo.i4SockProto = 0;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock ();
    LnxVrfEventHandling (&LnxVrfEventInfo, &i4SockFd);
    LnxVrfSockUnLock ();

#else

    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
#endif
    if (i4SockFd < 0)
    {
        perror ("ArpUpdateKernelEntry - socket creation failed !!!\r\n");
        return ARP_FAILURE;
    }

    if (STRLEN (pu1IfName) >= 16)
    {
        printf ("Only device name size can be 16 \r\n");
        close (i4SockFd);
        return ARP_FAILURE;
    }

    MEMSET (&(IpAddr), 0, sizeof (struct sockaddr_in));
    MEMSET (&(ArpReq.arp_pa), 0, sizeof (ArpReq.arp_pa));
    MEMSET (&(ArpReq.arp_ha), 0, sizeof (ArpReq.arp_ha));

    IpAddr.sin_family = AF_INET;
    IpAddr.sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    MEMCPY (&(ArpReq.arp_pa), (struct sockaddr *) &IpAddr,
            sizeof (struct sockaddr));
    STRCPY (ArpReq.arp_dev, pu1IfName);
    ArpReq.arp_flags = ATF_PERM;

    if (i1EntryType == ARP_STATIC)
    {
        if (ioctl (i4SockFd, SIOCGARP, &ArpReq) < 0)
        {
            ArpReq.arp_ha.sa_family = AF_UNSPEC;
        }
        else
        {
            /* If the arp entry exists in dynamic state, the flag field will
             * have value other than ATF_PERM.So the flag value is again set 
             * to permanant.*/
            ArpReq.arp_flags = ATF_PERM;
        }
        MEMCPY (&(ArpReq.arp_ha.sa_data), (INT1 *) pu1MacAddr, i4MacAddrLen);
        u4IoctlReq = SIOCSARP;
    }
    else if (i1EntryType == ARP_INVALID)
    {
        u4IoctlReq = SIOCDARP;
    }

    if ((u4IoctlReq == SIOCSARP) || (u4IoctlReq == SIOCDARP))
    {
        if (ioctl (i4SockFd, u4IoctlReq, &ArpReq) < 0)
        {
            if (u4IoctlReq == SIOCDARP)
            {
                /* Failure in kernel ARP entry deletion is valid if 
                 * the entry doesn't exist. */
                i4RetVal = ARP_SUCCESS;
            }
            else
            {
                perror ("ArpUpdateKernelEntry - ioctl failed !!!\r\n");
                i4RetVal = ARP_FAILURE;
            }
        }
    }
    close (i4SockFd);
    return (i4RetVal);
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpUpdateKernelEntrywithCOM
 *
 * Input(s)           : i1EntryType - ARP_STATIC/ARP_INVALID
 *                      pu1MacAddr - physical address to be stored.
 *                      i4MacAddrLen - physical address length.
 *                      u4IpAddr - Ip address to be added in the kernel
 *                      pu1IfName - Interface name.
 * Output(s)          : None.
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that handles the static entry addition
 *                      in the Linux Kernal arp cache.
 *
-------------------------------------------------------------------*/
INT4
ArpUpdateKernelEntrywithCOM (INT1 i1EntryType, UINT1 *pu1MacAddr,
                             INT4 i4MacAddrLen, UINT4 u4IpAddr,
                             UINT1 *pu1IfName)
{
    INT4                i4SockFd = -1;
    struct arpreq       ArpReq;
    struct sockaddr_in  IpAddr;
    UINT4               u4IoctlReq = 0;
    INT4                i4RetVal = ARP_SUCCESS;

    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd < 0)
    {
        perror ("ArpUpdateKernelEntrywithCOM - socket creation failed !!!\r\n");
        return ARP_FAILURE;
    }

    if (STRLEN (pu1IfName) >= 16)
    {
        printf ("Only device name size can be 16 \r\n");
        return ARP_FAILURE;
    }

    MEMSET (&(IpAddr), 0, sizeof (struct sockaddr_in));
    MEMSET (&(ArpReq.arp_pa), 0, sizeof (ArpReq.arp_pa));
    MEMSET (&(ArpReq.arp_ha), 0, sizeof (ArpReq.arp_ha));

    IpAddr.sin_family = AF_INET;
    IpAddr.sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    MEMCPY (&(ArpReq.arp_pa), (struct sockaddr *) &IpAddr,
            sizeof (struct sockaddr));
    STRCPY (ArpReq.arp_dev, pu1IfName);
    ArpReq.arp_flags = ATF_COM;

    if ((i1EntryType == ARP_STATIC) || (i1EntryType == ARP_DYNAMIC))
    {
        if (ioctl (i4SockFd, SIOCGARP, &ArpReq) < 0)
        {
            ArpReq.arp_ha.sa_family = AF_UNSPEC;
        }
        else
        {
            ioctl (i4SockFd, SIOCDARP, &ArpReq);
            ArpReq.arp_flags = ATF_COM;
        }
        MEMCPY (&(ArpReq.arp_ha.sa_data), (INT1 *) pu1MacAddr, i4MacAddrLen);
        u4IoctlReq = SIOCSARP;
    }
    else if (i1EntryType == ARP_INVALID)
    {
        u4IoctlReq = SIOCDARP;
    }

    if ((u4IoctlReq == SIOCSARP) || (u4IoctlReq == SIOCDARP))
    {
        if (ioctl (i4SockFd, u4IoctlReq, &ArpReq) < 0)
        {
            if (u4IoctlReq == SIOCDARP)
            {
                /* Failure in kernel ARP entry deletion is valid if 
                 * the entry doesn't exist. */
                i4RetVal = ARP_SUCCESS;
            }
            else
            {
                i4RetVal = ARP_FAILURE;
            }
        }
    }
    close (i4SockFd);
    return (i4RetVal);
}

#endif /* LNXIP4_WANTED */

/*-------------------------------------------------------------------+
 *
 * Function           : ArpSendRequest.
 *
 * Input(s)           : u2Port - port number. 
 *                      u4Dest - Ip address to which req is to be sent.
 *                      i2Harware - unused.
 *                      u1EncapType - unused.
 * Output(s)          : None.
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that sends the arp req to the kernel
 *                      and learns the mac address.
 *
+-------------------------------------------------------------------*/

INT4
ArpSendRequest (UINT2 u2Port, UINT4 u4Dest, INT2 i2Hardware, UINT1 u1EncapType)
{
    INT1               *pi1Buff = NULL;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4IfIndex = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_PKT           ArpPkt;

    UNUSED_PARAM (i2Hardware);

    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    if ((NetIpIfInfo.u4Oper & IFF_UP) == 0)
    {
        return ARP_FAILURE;
    }

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        MEM_FREE (pi1Buff);
        return ARP_FAILURE;
    }

    if (ArpGetHwAddr ((UINT2) u4IfIndex, au1HwAddr) == ARP_FAILURE)
    {
        MEM_FREE (pi1Buff);
        return ARP_FAILURE;
    }

    /* filling source mac and ip addresses */
    MEMCPY (ArpPkt.i1Shwaddr, au1HwAddr, CFA_ENET_ADDR_LEN);
    if (CfaIpIfGetSrcAddressOnInterface (u4IfIndex, u4Dest,
                                         &ArpPkt.u4Sproto_addr) == CFA_FAILURE)
    {
        return ARP_FAILURE;
    }

    /* Filling the dest mac and ip address */
    MEMSET (ArpPkt.i1Thwaddr, 0xFF, CFA_ENET_ADDR_LEN);
    ArpPkt.u4Tproto_addr = u4Dest;

    ArpPkt.i2Opcode = ARP_REQUEST;

    if (ArpSendReqOrResp (u2Port, u1EncapType, &ArpPkt) == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;

}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpCheckAndStartTimer
 *
 * Input(s)           : pArpCache - pointer to the cache.
 *                      u4Duration - Duration of the timer.
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function starts the timer.this function is 
 *                      common to lnxip and fsip in fsip case no 
 *                      check is required.
 *
+-------------------------------------------------------------------*/
VOID
ArpCheckAndStartTimer (t_ARP_CACHE * pArpCache, UINT4 u4Duration)
{
#ifdef RM_WANTED
    tHwPortInfo         HwPortInfo;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    if (ARP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        /* When RM Stacking Interface is an Inband Ethernet port,ARP packets
           has to be transmitted over the Stacking Interface irrespective of
           RM Node State .Hence below check is Introduced */
        if ((IssGetRMTypeFromNvRam () != ISS_RM_STACK_INTERFACE_TYPE_INBAND) ||
            (pArpCache->u4IfIndex != HwPortInfo.au4ConnectingPortIfIndex[0]))
        {
            return;
        }
    }
#endif
    if (TmrStartTimer (ARP_TIMER_LIST_ID, &(pArpCache->Timer.Timer_node),
                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC * (u4Duration))
        == TMR_FAILURE)
    {
        ARP_TRC (pArpCache->pArpCxt->u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC,
                 ARP_NAME, "Timer failure in ArpCheckAndStartTimer fn.\n");
    }

}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpCheckAndSendReq
 *
 * Input(s)           : i1State - State of the entry.
 *                      u2Port  - Port in which req to be sent.
 *                      u4IpAddr - IpAddress of the entry.
 *                      i2Hardware - Hardware type.
 *                      u1EncapType - Encapsulation type 
                                      (used to fill ethernet header).
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function sends request.this function is 
 *                      common to lnxip and fsip in fsip case no 
 *                      check is required.
 *
+-------------------------------------------------------------------*/
VOID
ArpCheckAndSendReq (INT1 i1State, UINT2 u2Port, UINT4 u4IpAddr, INT2 i2Hardware,
                    UINT1 u1EncapType)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UNUSED_PARAM (i1State);

    NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo);
#ifdef RM_WANTED
    UINT4               u4IfIndex = 0;
    UINT4               u4ConnectingPortIfIndex = 0;

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port,
                                      &u4IfIndex) == NETIPV4_FAILURE)
    {
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                 ALL_FAILURE_TRC, ARP_NAME,
                 "ArpSendRequest failure in ArpCheckAndSendReq fn.\n");
        return;

    }
    CfaGetRmConnectingPortIfIndex (&u4ConnectingPortIfIndex);
    if (ARP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        /* When RM Stacking Interface is an Inband Ethernet port,ARP packets
           has to be transmitted over the Stacking Interface irrespective of
           RM Node State .Hence below check is Introduced */
        if ((IssGetRMTypeFromNvRam () != ISS_RM_STACK_INTERFACE_TYPE_INBAND) ||
            (u4IfIndex != u4ConnectingPortIfIndex))
        {
            return;
        }
    }
#endif

    if (ArpSendRequest (u2Port, u4IpAddr, i2Hardware, u1EncapType)
        == ARP_FAILURE)
    {
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                 ALL_FAILURE_TRC, ARP_NAME,
                 "ArpSendRequest failure in ArpCheckAndSendReq fn.\n");
    }
}

#if defined (LNXIP4_WANTED) && defined (LINUX_ARP_NETLINK)
/*-------------------------------------------------------------------+
 *
 * Function           : ArpSendReqAndStartTimer.
 *
 * Input(s)           : tArpData.
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function sends a request and starts a timer.
 *                      this function is required only for lnxip case
 *                      to send req and start timer when reuested from the other layers.
 *
+-------------------------------------------------------------------*/
VOID
ArpSendReqAndStartTimer (tARP_DATA tArpData)
{
    INT4                i4ArpCheck = ARP_SUCCESS;
    UINT4               u4IfIndex = 0;
    t_ARP_CACHE         tArpCache;
    t_ARP_CACHE        *pArpCache = NULL;

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) tArpData.u2Port, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpSendReqAndStartTimer: Getting the interface index "
                     " for the given port failed.\n");
        return;
    }

    i4ArpCheck = ArpLookupWithIndex (u4IfIndex, tArpData.u4IpAddr, &tArpCache);
    if (i4ArpCheck == ARP_SUCCESS)
    {
        if (ArpSendRequest (tArpCache.u2Port, tArpCache.u4Ip_addr,
                            tArpCache.i2Hardware, tArpCache.u1EncapType)
            == ARP_FAILURE)
        {
            ARP_TRC (tArpCache.pArpCxt->u4ContextId, ARP_MOD_TRC,
                     ALL_FAILURE_TRC, ARP_NAME,
                     "ArpSendRequest failure in ArpSendReqAndStartTimer fn.\n");
        }

        ARP_DS_LOCK ();
        pArpCache = tArpCache.Timer.pArg;
        if (pArpCache != NULL)
        {
            if (TmrStartTimer
                (ARP_TIMER_LIST_ID, &(pArpCache->Timer.Timer_node),
                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC * ARP_REQUEST_RETRY_TIMEOUT) ==
                TMR_FAILURE)
            {
                ARP_TRC (tArpCache.pArpCxt->u4ContextId, ARP_MOD_TRC,
                         ALL_FAILURE_TRC, ARP_NAME,
                         "Timer failure in ArpSendReqAndStartTimer fn.\n");
            }
        }
        ARP_DS_UNLOCK ();
    }
}
#endif
/*-------------------------------------------------------------------
 * Function           : ArpLnxInit 
 * Input(s)           : None.
 * Output(s)          : None
 * Returns            : ARP_SUCCESS/ARP_FAILURE
 * Action             : This function initialises linux ARP module with 
 *                      default values. 
 *-------------------------------------------------------------------*/
INT4
ArpLnxInit (VOID)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];
#else
    UINT4               u4OldVal = 0;
    UINT4               u4OidLen = 0;
    UINT4               au4GcThres3[] = {
        CTL_NET, NET_IPV4, NET_IPV4_NEIGH,
        NET_PROTO_CONF_DEFAULT,
        NET_NEIGH_GC_THRESH3
    };
    INT4                i4ObjectLen;
#endif
    INT4                i4SetVal = (INT4) ARP_MAX_ARP_ENTRIES;

    /* By default, the max. no. of ARP cache entries in
       ARP cache is 1024. 
       The sysctl call updates the proc file system 
       /proc/sys/net/ipv4/neigh/default/gc_thresh3.
       to set the maximum no. of ARP cache entries. */

    /* set sysctl object value */
#ifdef LINUX_310_WANTED
    SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
              "echo %d >/proc/sys/net/ipv4/neigh/default/gc_thresh3", i4SetVal);
    system (ac1Command);
#else
    i4ObjectLen = sizeof (INT4);
    u4OidLen = sizeof (au4GcThres3) / sizeof (UINT4);

    if (sysctl ((int *) au4GcThres3, u4OidLen, &u4OldVal,
                (size_t *) & i4ObjectLen, &i4SetVal, sizeof (i4SetVal)))
    {
        system ("/sbin/sysctl -w net.ipv4.neigh.default.gc_thresh3=512");
        perror ("sysctl failed in setting gc_thresh3\r\n");
    }
#endif

#ifdef LINUX_ARP_NETLINK
    if (ArpNetLinkInit () == ARP_FAILURE)
    {
        return (ARP_FAILURE);
    }
#endif /* LINUX_ARP_NETLINK */

#ifdef DUPLICATE_ARP_RESP_NOT_WANTED
    system ("echo 1 > /proc/sys/net/ipv4/conf/all/arp_filter");
#endif
    ArpSetLnxArpCacheTimeout (ARP_DEF_CACHE_TIMEOUT);

    return (ARP_SUCCESS);
}

/*-------------------------------------------------------------------
 * Function           : ArpLnxSetMaxRetries 
 * Input(s)           : i4ArpMaxRetries
 * Output(s)          : None
 * Returns            : ARP_SUCCESS/ARP_FAILURE
 * Action             : This function sets the ARP max retries value in 
 *                      LINUX. 
 *-------------------------------------------------------------------*/
INT4
ArpSetLnxMaxRetires (INT4 i4ArpMaxRetries)
{
    INT4                i4SetVal = i4ArpMaxRetries;
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];
    tNetIpv4IfInfo      NetIpIfInfo;

    if (NetIpv4GetFirstIfInfo (&NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }
    do
    {
        SNPRINTF (ac1Command, /*LNX_MAX_COMMAND_LEN */ 80,
                  "echo %d >/proc/sys/net/ipv4/neigh/%s/mcast_solicit",
                  i4SetVal, NetIpIfInfo.au1IfName);
        system (ac1Command);
    }
    while (NetIpv4GetNextIfInfo (NetIpIfInfo.u4IfIndex, &NetIpIfInfo)
           != NETIPV4_FAILURE);
    return (ARP_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : ArpSendReqOrResp
 *
 * Input(s)           : u2Port - Port number
 *                      u1EncapType - Encapsulation type  
 *                      pArpPkt - Pointer to ARP packet to be sent   
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action             : Constructs and sends an ARP request/response.
 *                      This function is also called by DHCP/VRRP.    
+-------------------------------------------------------------------*/
INT4
ArpSendReqOrResp (UINT2 u2Port, UINT1 u1EncapType, t_ARP_PKT * pArpPkt)
{
    INT1               *pi1Buff = NULL;
    INT1 
         
         
         
         
              ai1Buff[ARP_HDR_SIZE (CFA_ENET_ADDR_LEN, ARP_PROTOCOL_ADDR_LEN)];
    INT4                i4SendtoSock = -1;
    INT4                i4Sock = -1;
    struct sockaddr_in  SockAddr;
    struct sockaddr_ll  SAddr_ll;
    struct sockaddr_ll  DAddr_ll;
    struct arphdr      *pArpHdr;
    INT1               *pi1Val = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;
#endif

    UNUSED_PARAM (u1EncapType);
    MEMSET (&SAddr_ll, 0, sizeof (struct sockaddr_ll));
    MEMSET (&DAddr_ll, 0, sizeof (struct sockaddr_ll));
    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    /* To make the linux kernel learn the ARP entry, a UDP socket
     * is opened and a connect is done. So the connect is needed only
     * for ARP_REQUEST packets. 
     * When DHCP is enabled on an interface, DHCP b4 assigning the
     * IP address for the interface, checks whether the the IP address
     * is already being used by someother host. In this case, DHCP sends
     * an ARP_REQUEST for the IP addr that is going to be assigned. This
     * ARP_REQUEST has to be sent on the interface having 0.0.0.0 IP.
     * The connect() call fails in this case. The connect call need
     * not be done in this case, as the linux kernel need not learn this
     * arp entry.
     */
    if ((pArpPkt->i2Opcode == ARP_REQUEST) &&
        (NetIpIfInfo.u4Addr != 0) &&
        (NetIpv4IfIsOurAddressInCxt (NetIpIfInfo.u4ContextId,
                                     pArpPkt->u4Tproto_addr)
         != NETIPV4_SUCCESS))
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        MEMSET (&LnxVrfEventInfo, 0, sizeof (tLnxVrfEventInfo));
        LnxVrfEventInfo.u4ContextId = NetIpIfInfo.u4ContextId;
        LnxVrfEventInfo.i4Sockdomain = AF_INET;
        LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
        LnxVrfEventInfo.i4SockProto = 0;
        LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
#endif
        if (ArpVerifyIfAddrExists (u2Port) != ARP_SUCCESS)
        {
            return ARP_FAILURE;
        }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        LnxVrfSockLock ();
        /* Socket to send ARP packet */
        LnxVrfEventHandling (&LnxVrfEventInfo, &i4Sock);
        LnxVrfSockUnLock ();
#else
        /* Socket to send ARP packet */
        i4Sock = socket (AF_INET, SOCK_DGRAM, 0);
#endif
        if (i4Sock < 0)
        {
            perror ("ArpSendReqOrResp: Connect Socket creation failed\n");
            return ARP_FAILURE;
        }

        SockAddr.sin_family = AF_INET;
        SockAddr.sin_port = OSIX_HTONS (ARP_CONNECT_PORT);
        SockAddr.sin_addr.s_addr = OSIX_HTONL (pArpPkt->u4Tproto_addr);

        if (ArpVerifyIfAddrExists (u2Port) != ARP_SUCCESS)
        {
            close (i4SendtoSock);
            return ARP_FAILURE;
        }

        if (connect (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
            0)
        {
            perror ("ArpSendReqOrResp: connect failed\r\n");
            close (i4Sock);
            return ARP_FAILURE;
        }

        close (i4Sock);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    MEMSET (&LnxVrfEventInfo, 0, sizeof (tLnxVrfEventInfo));
    LnxVrfEventInfo.u4ContextId = NetIpIfInfo.u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = PF_PACKET;
    LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
    LnxVrfEventInfo.i4SockProto = 0;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
#endif
    if (ArpVerifyIfAddrExists (u2Port) != ARP_SUCCESS)
    {
        return ARP_FAILURE;
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    LnxVrfSockLock ();
    /* Socket to send ARP packet */
    LnxVrfEventHandling (&LnxVrfEventInfo, &i4SendtoSock);
    LnxVrfSockUnLock ();
#else
    /* Socket to send ARP packet */
    i4SendtoSock = socket (PF_PACKET, SOCK_DGRAM, 0);
#endif
    if (i4SendtoSock < 0)
    {
        perror ("ArpSendReqOrResp: Socket creation failed\r\n");
        return ARP_FAILURE;
    }

    if (fcntl (i4SendtoSock, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("ArpSendReqOrResp: fcntl failed");
        close (i4SendtoSock);
        return ARP_FAILURE;
    }

    SAddr_ll.sll_family = AF_PACKET;
    SAddr_ll.sll_ifindex = u2Port;
    SAddr_ll.sll_protocol = htons (ETH_P_ARP);

    if (ArpVerifyIfAddrExists (u2Port) != ARP_SUCCESS)
    {
        close (i4SendtoSock);
        return ARP_FAILURE;
    }

    if (bind (i4SendtoSock, (struct sockaddr *) &SAddr_ll,
              sizeof (struct sockaddr_ll)) < 0)
    {
        perror ("ArpSendReqOrResp: bind failed\r\n");
        close (i4SendtoSock);
        return ARP_FAILURE;
    }

    pi1Buff = ai1Buff;
    MEMSET (pi1Buff, 0,
            (ARP_HDR_SIZE (CFA_ENET_ADDR_LEN, ARP_PROTOCOL_ADDR_LEN)));
    pArpHdr = (struct arphdr *) (VOID *) pi1Buff;
    pi1Val = (INT1 *) (pArpHdr + 1);
    pArpHdr->ar_hrd = OSIX_HTONS (ARPHRD_ETHER);
    pArpHdr->ar_pro = OSIX_HTONS (ETH_P_IP);
    pArpHdr->ar_hln = CFA_ENET_ADDR_LEN;
    pArpHdr->ar_pln = ARP_MAX_PROTO_ADDR_LEN;
    pArpHdr->ar_op = OSIX_HTONS (pArpPkt->i2Opcode);

    /* filling source mac and ip addresses */
    MEMCPY (pi1Val, pArpPkt->i1Shwaddr, pArpHdr->ar_hln);
    pi1Val += pArpHdr->ar_hln;
    pArpPkt->u4Sproto_addr = OSIX_HTONL (pArpPkt->u4Sproto_addr);
    MEMCPY (pi1Val, (char *) &(pArpPkt->u4Sproto_addr), pArpHdr->ar_pln);
    pArpPkt->u4Sproto_addr = OSIX_HTONL (pArpPkt->u4Sproto_addr);
    pi1Val += pArpHdr->ar_pln;

    /* filling Destination mac and ip addresses */
    MEMCPY (pi1Val, pArpPkt->i1Thwaddr, pArpHdr->ar_hln);
    pi1Val += pArpHdr->ar_hln;
    pArpPkt->u4Tproto_addr = OSIX_HTONL (pArpPkt->u4Tproto_addr);
    MEMCPY (pi1Val, (char *) &(pArpPkt->u4Tproto_addr), pArpHdr->ar_pln);
    pArpPkt->u4Tproto_addr = OSIX_HTONL (pArpPkt->u4Tproto_addr);
    pi1Val += pArpHdr->ar_pln;

    DAddr_ll = SAddr_ll;
    MEMSET (DAddr_ll.sll_addr, 0xff, pArpHdr->ar_hln);
    DAddr_ll.sll_halen = pArpHdr->ar_hln;

    if (ArpVerifyIfAddrExists (u2Port) != ARP_SUCCESS)
    {
        close (i4SendtoSock);
        return ARP_FAILURE;
    }

    if (sendto (i4SendtoSock, pi1Buff, (pi1Val - pi1Buff), 0,
                (struct sockaddr *) &DAddr_ll, sizeof (struct sockaddr_ll)) < 0)
    {
        perror ("ArpSendReqOrResp: sendto failed\r\n");
        close (i4SendtoSock);
        return ARP_FAILURE;
    }
    close (i4SendtoSock);
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpVerifyIfAddrExists
 *
 * Input(s)           : u2Port - Port number
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action             : Using u2Port check ports availablity and ip address
 *                      configurred on it. 
+-------------------------------------------------------------------*/
INT4
ArpVerifyIfAddrExists (UINT2 u2Port)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    if ((NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_SUCCESS) &&
        (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE))
    {
        return ARP_SUCCESS;
    }
    else
    {
        return ARP_FAILURE;
    }
}

/*-------------------------------------------------------------------
 * Function           : ArpLnxSetArpCacheTimeout
 * Input(s)           : i4ArpCacheTimeout
 * Output(s)          : None
 * Returns            : ARP_SUCCESS/ARP_FAILURE
 * Action             : This function sets the ARP cache timeout value in
 *                      LINUX.
 *-------------------------------------------------------------------*/
INT4
ArpSetLnxArpCacheTimeout (INT4 i4ArpCacheTimeout)
{
    INT4                i4SetVal = i4ArpCacheTimeout;
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];

    SNPRINTF (ac1Command, /*LNX_MAX_COMMAND_LEN */ 80,
              "echo %d >/proc/sys/net/ipv4/neigh/default/base_reachable_time",
              i4SetVal);
    system (ac1Command);
    return (ARP_SUCCESS);
}

#endif /* _ARPLNXIP_C */
