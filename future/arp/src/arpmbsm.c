/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpmbsm.c,v 1.11 2013/07/02 13:27:46 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED
#include "arpinc.h"
#include "mbsm.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IpArpMbsmUpdateCardStatus                                  */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the IP ARP Task                           */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                i1Status  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
IpArpMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tArpQMsg            ArpQMsg;

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

    if ((ArpQMsg.pBuf =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg), 0)) == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "CRU Buf Allocation Failed\n");
        return MBSM_FAILURE;
    }

    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (ArpQMsg.pBuf, (UINT1 *) pProtoMsg, 0,
                               sizeof (tMbsmProtoMsg));

    ArpQMsg.u4MsgType = (UINT4) i4Event;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (ArpQMsg.pBuf, TRUE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "Send to IP Q failed\n");
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : ArpMbsmUpdateArpTable                                       */
/*                                                                           */
/* Description  : Updates the Arp table in the NP based on the               */
/*                Line card status received                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u1Status - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
ArpMbsmUpdateArpTable (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    UINT1               au1MacAddr[CFA_ENET_ADDR_LEN] = { 0 };
    INT4                i4RetStatus = MBSM_SUCCESS;
    INT4                i4CfaIfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4ContextId = 0;
    UINT1               bu1TblFull = FNP_FALSE;
    tVlanIfaceVlanId    u2VlanId = 0;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    t_ARP_CACHE        *pArpCache = NULL;
    INT1                i1State = 0;
    tCfaIfInfo          CfaIfInfo;

    /* Protocol Cookie should be for updating the ARP Table in the NP  
       If not then it should be an error */

    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                  "CRU Buf Copy From Buf Chain Failed\n");
        return;
    }

    pSlotInfo = &(ProtoMsg.MbsmSlotInfo);
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (u1Cmd == MBSM_MSG_CARD_INSERT)
        {
            pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

            while (pArpCache != NULL)
            {
                i4CfaIfIndex = pArpCache->u4IfIndex;
                u4IpAddress = pArpCache->u4Ip_addr;
                MEMCPY (au1MacAddr, pArpCache->i1Hw_addr, CFA_ENET_ADDR_LEN);
                u4ContextId = pArpCache->pArpCxt->u4ContextId;
                i1State = pArpCache->i1State;

                pArpCache =
                    RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);
                if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                {
                    /* If it is LinklocalIp, then there is no
                     * need to modify in Hw*/
                    if (IS_LINK_LOCAL_ADDR (u4IpAddress))
                    {
                        continue;
                    }
                }

                /* If the Port is OOB Port,Dont Program in HardWare */
                if (CfaIsMgmtPort ((UINT4) i4CfaIfIndex) == TRUE)
                {
                    continue;
                }

                if ((i1State != (INT1) ARP_STATIC) &&
                    (i1State != (INT1) ARP_DYNAMIC))
                {
                    continue;
                }

                MEMSET (&CfaIfInfo, ARP_ZERO, sizeof (tCfaIfInfo));

                if (CfaGetIfInfo (((UINT4) i4CfaIfIndex), &CfaIfInfo) !=
                    CFA_SUCCESS)
                {
                    continue;
                }

                if (CfaIfInfo.u1IfType == CFA_L3IPVLAN)
                {
                    /* (i) Identify the security VLAN associated with this L3 VLAN
                     *     interface index based on the MAC address learnt using
                     *     CfaGetSecVlanId (OR)
                     * (ii) Identify the direct L2 VLAN associated with the L3 VLAN
                     *      using CfaGetVlanId call
                     */
                    if (((CfaGetSecVlanId
                          (((UINT4) i4CfaIfIndex), au1MacAddr,
                           &u2VlanId)) == CFA_SUCCESS)
                        || (CfaGetVlanId (((UINT4) i4CfaIfIndex), &u2VlanId) ==
                            CFA_SUCCESS))
                    {
                        if (ArpFsNpMbsmIpv4VrfArpAddition
                            (u4ContextId, u2VlanId, u4IpAddress, au1MacAddr,
                             &bu1TblFull, ((UINT4) i4CfaIfIndex),
                             &CfaIfInfo.au1IfName[0], i1State,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            i4RetStatus = MBSM_FAILURE;
                        }

                        if (bu1TblFull == FNP_TRUE)
                        {
                            break;
                        }
                    }
                }
                else if (CfaIfInfo.u1IfType == CFA_ENET)
                {
                    if (ArpFsNpMbsmIpv4VrfArpAddition
                        (u4ContextId, u2VlanId, u4IpAddress, au1MacAddr,
                         &bu1TblFull, (UINT4) i4CfaIfIndex,
                         &CfaIfInfo.au1IfName[0], i1State,
                         pSlotInfo) == FNP_FAILURE)
                    {
                        i4RetStatus = MBSM_FAILURE;
                    }

                    if (bu1TblFull == FNP_TRUE)
                    {
                        break;
                    }
                }
            }
        }
    }

    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */

    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    MbsmSendAckFromProto (&protoAckMsg);

}
#endif
