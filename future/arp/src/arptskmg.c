/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arptskmg.c,v 1.58 2017/12/26 13:02:53 siva Exp $
 *
 * Description: This file contains TASK related functions.  
 *
 *******************************************************************/
#include "arpinc.h"
#include "arpred.h"

tArpGblInfo         gArpGblInfo;
tArpRegTbl          gaArpRegTbl[ARP_MAX_REG_MODULES];

tArpRedGlobalInfo   gArpRedGlobalInfo;
INT1                gi4ArpSysLogId;
UINT4               gu4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];
UINT4               gu4MaxArpEntries = 0;

t_ARP_CRU_TYPE      Arp_Cru_Type[MAX_ARP_TYPES] = {
    {IP_IF_TYPE_ETHERNET, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_OTHER, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_X25, ARP_X25_TYPE}
    ,
    {IP_IF_TYPE_FR, ARP_FRMRL_TYPE}
    ,
    {IP_IF_TYPE_PPP, ARP_PPP_TYPE}
    ,
    {IP_IF_TYPE_L2VLAN, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_L3IPVLAN, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_PSEUDO_WIRE, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_INVALID, ARP_UNDEFINED_TYPE}
    ,
    {IP_IF_TYPE_LAGG, ARP_ENET_TYPE}
    ,
    {IP_IF_TYPE_L3SUBIF, ARP_ENET_TYPE}
};

UINT4               gu4ArpDbg = ARP_ZERO;
/*-------------------------------------------------------------------+
 * Function           : ArpTaskMain
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 *
 * Main Function of the Arp Task.
 * Only the ARP module is present in the ARP task.
 *
+-------------------------------------------------------------------*/
VOID
ArpTaskMain (INT1 *pDummy)
{
#ifdef NPAPI_WANTED
    UINT4               u4IpAddr = ARP_ZERO;
    UINT4               u4IfIndex = ARP_ZERO;
    UINT4               u4ContextId = ARP_DEFAULT_CONTEXT;
#endif
    UINT4               u4EntryCount = ARP_ZERO;
#if defined (VRF_WANTED) && defined (LNXIP4_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4Retval = 0;
    INT4                i4SockId = -1;
#endif
    tArpQFlushMac      *pQMacAddr = NULL;
    UNUSED_PARAM (pDummy);

    if (ArpTaskInit () != ARP_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        ARP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /*Store the ARP Task Id globally */
    if (OsixTskIdSelf (&ARP_TASK_ID) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        ARP_INIT_COMPLETE (OSIX_FAILURE);
    }
    /* Indicate the status of initialization to the main routine */
    ARP_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    RegisterFSARP ();
    RegisterFSMPAR ();
#endif /* SNMP_2_WANTED */

    for (;;)
    {
        UINT4               u4Event = 0;

        OsixEvtRecv (ARP_TASK_ID,
                     (ARP_CACHE_TIMER_EVENT | ARP_PACKET_ARRIVAL_EVENT |
                      ARP_L2_MOVEMENT_EVENT | ARP_L3_DATA_PKT_EVENT |
                      ARP_RM_PKT_EVENT | ARP_RED_TIMER_EVENT |
                      ARP_HA_PEND_BLKUPD_EVENT | ARP_KERNEL_UPD_EVENT |
                      ARP_RED_START_TIMER_EVENT | ARP_ICCH_PKT_EVENT |
                      ARP_FLUSH_BY_PORT_EVENT), ARP_EVENT_WAIT_FLAGS, &u4Event);

        if (u4Event & ARP_CACHE_TIMER_EVENT)
        {
            arp_timer_expiry_handler ();
        }

        if (u4Event & ARP_PACKET_ARRIVAL_EVENT)
        {
            ARP_Process_Pkt_Arrival_Event ();
        }
#ifdef NPAPI_WANTED
        if (u4Event & ARP_L2_MOVEMENT_EVENT)
        {
            if (ARP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                while (ArpFsNpIpv4VrfGetSrcMovedIpAddr (&u4ContextId, &u4IpAddr)
                       == FNP_SUCCESS)
                {
                    CfaIpIfGetIfIndexFromHostIpAddressInCxt (u4ContextId,
                                                             u4IpAddr,
                                                             &u4IfIndex);

                    ArpDeleteArpCacheWithIndex (u4IfIndex, u4IpAddr);
                }
            }
        }
#endif
        if (u4Event & ARP_L3_DATA_PKT_EVENT)
        {
            ARPProcessDataPktEvent ();
        }
        if (u4Event & ARP_RM_PKT_EVENT)
        {
            ArpRedHandleRmEvents ();
        }

#ifdef  ICCH_WANTED
        if (u4Event & ARP_ICCH_PKT_EVENT)
        {
            ArpIcchProcessUpdateMsg ();
        }
#endif

        if (u4Event & ARP_FLUSH_BY_PORT_EVENT)
        {
            while (OsixQueRecv (ARP_NEW_PKT_Q_ID,
                                (UINT1 *) &pQMacAddr, sizeof (tArpQFlushMac),
                                OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                ArpFlushWithMac (pQMacAddr);

                MemReleaseMemBlock (ARP_VLAN_MAC_POOL_ID, (UINT1 *) pQMacAddr);
            }

        }

#if defined (LNXIP4_WANTED) && defined (LINUX_ARP_NETLINK)
        if (u4Event & ARP_KERNEL_UPD_EVENT)
        {
#if defined (VRF_WANTED) && defined (LNXIP4_WANTED) && defined (LINUX_310_WANTED)

            u4Retval = OsixQueRecv (ARP_VRF_IP_PKT_Q_ID,
                                    (UINT1 *) &i4SockId, OSIX_DEF_MSG_LEN,
                                    OSIX_NO_WAIT);
            UNUSED_PARAM (u4Retval);
            ArpNetlinkParseInfo (ArpNetlinkInformationFetch, i4SockId);
            SelAddFd (i4SockId, ArpNotifyArpTask);
#else
            ArpNetlinkParseInfo (ArpNetlinkInformationFetch, gi4NetlinkSock);
            SelAddFd (gi4NetlinkSock, ArpNotifyArpTask);
#endif

        }
#endif /* LNXIP4_WANTED && LINUX_ARP_NETLINK  */

        if (u4Event & ARP_HA_PEND_BLKUPD_EVENT)
        {
            ArpRedSendBulkUpdMsg ();
        }

        if (u4Event & ARP_RED_START_TIMER_EVENT)
        {
            RBTreeWalk (gArpGblInfo.ArpTable, (tRBWalkFn) ArpStartTimers, NULL,
                        NULL);
        }

        if (gArpRedGlobalInfo.u1RedStatus == ARP_SUCCESS)
        {
            /* A one second timer is used for invoking an event to send dynamic 
             * updates from a single point of the main loop. The function 
             * ArpRedSendDynamicCacheInfo is called whenever an event is received,
             * or when the one second timer expires. 
             * In the active node, when the bulk update is not started, the dynamic
             * update will not be sent. The update will be taken care in the 
             * bulk update message. And the timer is restarted after sending the
             * dynamic update. */

            if ((gArpRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
                (ARP_IS_STANDBY_UP () == OSIX_TRUE))
            {
                u4EntryCount = 0;
                RBTreeCount (gArpGblInfo.ArpRedTable, &u4EntryCount);
                if (u4EntryCount > 0)
                {
                    ArpRedSendDynamicCacheInfo ();
                }
            }
        }
    }
}

/*-------------------------------------------------------------------+
 * Function           : ArpTaskInit
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 * Calls initialization function.
 *
+-------------------------------------------------------------------*/
INT4
ArpTaskInit (VOID)
{
    tVcmRegInfo         VcmRegInfo;

#ifdef L2RED_WANTED
    gArpRedGlobalInfo.u1RedStatus = ARP_SUCCESS;
#else
    gArpRedGlobalInfo.u1RedStatus = ARP_FAILURE;
#endif
    if (OsixCreateSem (ARP_CACHE_SEMAPHORE, 1, ARP_ZERO, &ARP_CACHE_SEM_ID) !=
        OSIX_SUCCESS)
    {
        return ARP_FAILURE;
    }

    if (OsixCreateSem
        (ARP_PROTOCOL_SEMAPHORE, 1, ARP_ZERO,
         &ARP_PROTOCOL_SEM_ID) != OSIX_SUCCESS)
    {
        return ARP_FAILURE;
    }

    gArpGblInfo.ArpTable = RBTreeCreateEmbedded (FSAP_OFFSETOF (t_ARP_CACHE,
                                                                RbNode),
                                                 ArpRBTreeCacheEntryCmp);
    if (gArpGblInfo.ArpTable == NULL)
    {
        ARP_GBL_TRC (ARP_MOD_TRC,
                     INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     ARP_NAME, "ARP cache Creation Failure.\n");
        ArpDeInit ();
        return ARP_FAILURE;
    }

    if (gArpRedGlobalInfo.u1RedStatus == ARP_SUCCESS)
    {
        gArpGblInfo.ArpRedTable =
            RBTreeCreateEmbedded (FSAP_OFFSETOF (tArpRedTable, RbNode),
                                  ArpRBTreeRedEntryCmp);
        if (gArpGblInfo.ArpRedTable == NULL)
        {
            ARP_GBL_TRC (ARP_MOD_TRC,
                         INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                         ARP_NAME, "ARP Redundancy cache Creation Failure.\n");
            ArpDeInit ();
            return ARP_FAILURE;
        }
    }

    if (ARP_CREATE_ENET_TABLE () == ARP_FAILURE)
    {
        ArpDeInit ();
        return ARP_FAILURE;

    }

    ArpSizingMemCreateMemPools ();
    gu4MaxArpEntries =
        FsARPSizingParams[MAX_ARP_ENTRIES_SIZING_ID].u4PreAllocatedUnits;
    ARP_CREATE_ENET_ENTRIES ();

    arp_init_hw_table ();
    arp_add_hardware (ARP_ENET_PHYS_INTERFACE_TYPE, (INT4 (*)PROTO
                                                     ((UINT2, INT1 *, INT1 *,
                                                       INT1 *))) ArpGetHwAddr,
                      CRU_ENET_IP_TYPE,
                      (INT4 (*)PROTO ((VOID))) CfaHandlePktFromIp);

    /* Create the ARP Packet arrival Q */
    if (OsixQueCrt ((UINT1 *) ARP_PACKET_ARRIVAL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    ARP_PACKET_ARRIVAL_Q_DEPTH, &ARP_PKT_Q_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
    if (OsixQueCrt ((UINT1 *) ARP_NEW_PACKET_ARRIVAL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    ARP_NEW_PACKET_ARRIVAL_Q_DEPTH,
                    &ARP_NEW_PKT_Q_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }

    /* Create the IP Packet arrival Q */
    if (OsixQueCrt ((UINT1 *) ARP_IP_PKT_ARRIVAL_Q_NAME,
                    OSIX_MAX_Q_MSG_LEN,
                    ARP_IP_PKT_ARRIVAL_Q_DEPTH,
                    &ARP_IP_PKT_Q_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    /* Create the ARP VRF Packet arrival Q */
    if (OsixQueCrt ((UINT1 *) ARP_VRF_IP_PKT_ARRIVAL_Q_NAME,
                    OSIX_MAX_Q_MSG_LEN,
                    ARP_IP_PKT_ARRIVAL_Q_DEPTH,
                    &ARP_VRF_IP_PKT_Q_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
#endif

    if (ARP_CREATE_TMR_LIST ((const UINT1 *) ARP_TASK_NAME,
                             ARP_CACHE_TIMER_EVENT,
                             &ARP_TIMER_LIST_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }

    if (gArpRedGlobalInfo.u1RedStatus == ARP_SUCCESS)
    {
        /* Create the RM Packet arrival Q */
        if (OsixQueCrt ((UINT1 *) ARP_RM_PKT_ARRIVAL_Q_NAME,
                        OSIX_MAX_Q_MSG_LEN,
                        ARP_RM_PKT_ARRIVAL_Q_DEPTH,
                        &ARP_RM_PKT_Q_ID) != OSIX_SUCCESS)
        {
            ArpDeInit ();
            return ARP_FAILURE;
        }
        if (ArpRedInitGlobalInfo () == OSIX_FAILURE)
        {
            ArpDeInit ();
            return ARP_FAILURE;
        }
    }
    if (ArpHandleContextCreate (ARP_DEFAULT_CONTEXT) == ARP_FAILURE)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }

    gArpGblInfo.pArpCurrCxt = gArpGblInfo.apArpCxt[ARP_DEFAULT_CONTEXT];

#ifdef IP_WANTED
    RarpInit ();
#endif /* IP_WANTED */

#ifdef LNXIP4_WANTED
    if (ArpLnxInit () == ARP_FAILURE)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
#endif /* LNXIP4_WANTED */

    MEMSET (&VcmRegInfo, ARP_ZERO, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = ARP_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = ArpNotifyContextStatus;
    VcmRegInfo.u1InfoMask = VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }

#ifdef ICCH_WANTED

    /* Create the ICCH Packet arrival Q */
    if (OsixQueCrt ((UINT1 *) ARP_ICCH_PKT_ARRIVAL_Q_NAME,
                    OSIX_MAX_Q_MSG_LEN,
                    ARP_ICCH_PKT_ARRIVAL_Q_DEPTH,
                    &ARP_ICCH_PKT_Q_ID) != OSIX_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }

    /* Register ARP with ICCH module */
    if (ArpIcchRegisterWithICCH () != ARP_SUCCESS)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
#endif

    if (ArpEnable () == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpDeInit                                                  */
/*                                                                           */
/*  Description : DeInitializes the memory allocated for ARP module          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
ArpDeInit (VOID)
{
    ArpDisable ();
    VcmDeRegisterHLProtocol (ARP_PROTOCOL_ID);

    if (ARP_PROTOCOL_SEM_ID != ARP_ZERO)
    {
        OsixSemDel (ARP_PROTOCOL_SEM_ID);
    }
    if (ARP_CACHE_SEM_ID != ARP_ZERO)
    {
        OsixSemDel (ARP_CACHE_SEM_ID);
    }

    if (gArpGblInfo.ArpTable != NULL)
    {
        RBTreeDelete (gArpGblInfo.ArpTable);
    }
    ArpSizingMemDeleteMemPools ();

    if (ARP_TIMER_LIST_ID != ARP_ZERO)
    {
        TmrDeleteTimerList (ARP_TIMER_LIST_ID);

        /* Timer List Initialization */
        ARP_TIMER_LIST_ID = ARP_ZERO;
    }
    if (gArpRedGlobalInfo.u1RedStatus == ARP_SUCCESS)
    {
        if (ARP_RM_PKT_Q_ID != ARP_ZERO)
        {
            OsixQueDel (ARP_RM_PKT_Q_ID);
        }
        if (gArpGblInfo.ArpRedTable != NULL)
        {
            RBTreeDelete (gArpGblInfo.ArpRedTable);
        }
        ArpRedDeInitGlobalInfo ();
    }
#ifdef ICCH_WANTED
    ArpIcchDeRegisterWithICCH ();

    if (ARP_ICCH_PKT_Q_ID != ARP_ZERO)
    {
        OsixQueDel (ARP_ICCH_PKT_Q_ID);
    }
#endif

    if (ARP_IP_PKT_Q_ID != ARP_ZERO)
    {
        OsixQueDel (ARP_IP_PKT_Q_ID);
    }
    if (ARP_PKT_Q_ID != ARP_ZERO)
    {
        OsixQueDel (ARP_PKT_Q_ID);
    }
    if (ARP_NEW_PKT_Q_ID != ARP_ZERO)
    {
        OsixQueDel (ARP_NEW_PKT_Q_ID);
    }

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (ARP_VRF_IP_PKT_Q_ID != ARP_ZERO)
    {
        OsixQueDel (ARP_VRF_IP_PKT_Q_ID);
    }
#endif
    gArpGblInfo.pArpCurrCxt = NULL;

    ARP_DELETE_ENET_ENTRIES ();
    ARP_DELETE_ENET_TABLE ();
}

/*****************************************************************************/
/* Function     : ArpDisable                                                 */
/*                                                                           */
/*  Description : Deletes the dynamic entries and stops teh timers           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
ArpDisable (VOID)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE         ArpCacheNode;

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    while (pArpCache != NULL)
    {
        if ((pArpCache->i1State != ARP_STATIC) &&
            (pArpCache->i1State != ARP_STATIC_NOT_IN_SERVICE) &&
            (pArpCache->i1State != ARP_INVALID))
        {
            TmrStopTimer (ARP_TIMER_LIST_ID, &(pArpCache->Timer.Timer_node));
            arp_drop (pArpCache);
        }
        MEMSET (&ArpCacheNode, 0, sizeof (t_ARP_CACHE));
        ArpCacheNode.u4Ip_addr = pArpCache->u4Ip_addr;
        ArpCacheNode.u4IfIndex = pArpCache->u4IfIndex;
        pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                   (tRBElem *) & ArpCacheNode, NULL);
    }

    SYS_LOG_DEREGISTER ((UINT4) gi4ArpSysLogId);
    NetIpv4DeRegisterHigherLayerProtocol (IP_ARP_REG_ID);
}

/*****************************************************************************/
/* Function     : ArpEnable                                                  */
/*                                                                           */
/*  Description : Enables ARP, Registers with Higher layers and creates HA   */
/*                related RB trees and timers                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Success / Failure                                          */
/*****************************************************************************/
INT4
ArpEnable (VOID)
{
    tNetIpRegInfo       RegInfo;

    /* Register for receiving interface change updates */
    MEMSET (&RegInfo, ARP_ZERO, sizeof (tNetIpRegInfo));
    RegInfo.u1ProtoId = IP_ARP_REG_ID;
    RegInfo.u2InfoMask |= NETIPV4_ROUTECHG_REQ;
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.pIfStChng = NULL;
    RegInfo.pRtChng = ArpHandleUcastRtChg;

    if (NetIpv4RegisterHigherLayerProtocol (&RegInfo) == NETIPV4_FAILURE)
    {
        ArpDeInit ();
        return ARP_FAILURE;
    }
    /* Registering with SYSLOG */
    gi4ArpSysLogId = SYS_LOG_REGISTER ((UINT1 *) ARP_TASK_NAME,
                                       SYSLOG_CRITICAL_LEVEL);

    return ARP_SUCCESS;

}

/*-------------------------------------------------------------------+
 * Function           : arp_map_cru_hwtype
 *
 * Input(s)           : i2CruType - Interface type
 *
 * Output(s)          : None.
 *
 * Returns            : ARP type 
 *
 * Action             : Mapping the interface type to ARP type
 *
+-------------------------------------------------------------------*/
INT2
arp_map_cru_hwtype (INT2 i2CruType)
{
    INT2                i2Count = 0;
    for (i2Count = ARP_ZERO; i2Count < MAX_ARP_TYPES; i2Count++)
    {
        if (i2CruType == Arp_Cru_Type[i2Count].i2CruType)
        {
            return Arp_Cru_Type[i2Count].i2ArpType;
        }
    }
    return (ARP_UNDEFINED_TYPE);
}

/*-------------------------------------------------------------------+
 * Function           : arp_timer_expiry_handler
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Finds the expired timers and invokes the corresponding timer
 * routines.
 * Invoked when the event indicating a timer expiry occurs.
+-------------------------------------------------------------------*/
VOID
arp_timer_expiry_handler (VOID)
{
    tTMO_APP_TIMER     *pList_head = NULL;
    tTMO_APP_TIMER     *pTimer = NULL;
    t_ARP_CACHE        *pArp_cache = NULL;
    tArpCxt            *pArpCxt = NULL;
    UINT1               u1Id = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
#ifdef NPAPI_WANTED
    tARP_DATA           ArpData;
    UINT1               u1NextHopFlag;
#endif
    UINT4               u4MaxRetries = ARP_ZERO;
    UINT2               u2Port = ARP_ZERO;
    UINT4               u4Duration = ARP_ZERO;

    ARP_PROT_LOCK ();

    pList_head = ARP_NEXT_TIMER (ARP_TIMER_LIST_ID);

    while (pList_head != NULL)
    {
        pTimer = pList_head;
        pList_head = ARP_NEXT_TIMER (ARP_TIMER_LIST_ID);

        u1Id = ((t_IP_TIMER *) pTimer)->u1Id;
        switch (u1Id)
        {
            case ARP_CACHE_TIMER_ID:

                pArp_cache = ((t_IP_TIMER *) pTimer)->pArg;

#ifdef NPAPI_WANTED
                if ((pArp_cache->i1State == ARP_PENDING) ||
                    (pArp_cache->i1State == ARP_AGEOUT))
                {
                    ARP_DS_LOCK ();
                    arp_drop (pArp_cache);
                    ARP_DS_UNLOCK ();
                }
                else
                {
                    pArpCxt = pArp_cache->pArpCxt;
                    /* Check whether the  ARP entry for deletion is nexthop
                     * of any of the reachable route entry installed in H/W*/
                    ARP_PROT_UNLOCK ();
                    u1NextHopFlag =
                        RtmApiIsReachableNextHopInCxt (pArpCxt->u4ContextId,
                                                       pArp_cache->u4Ip_addr);
                    ARP_PROT_LOCK ();

                    /* If it is Link Local Ip, then no need to check Hit bit
                     * here STACKIP is used in Link Local address range */
                    if (CfaIsStackVlanIntf (pArp_cache->u4IfIndex) ==
                        CFA_FAILURE)
                    {
                        /* Check for HitBit of the ARP entry and accordingly 
                           delete the ARP Entry */
                        if (ArpFsNpIpv4VrfCheckHitOnArpEntry
                            (pArpCxt->u4ContextId, pArp_cache->u4Ip_addr,
                             u1NextHopFlag) == FNP_FALSE)
                        {
                            ARP_TRC_ARG1 (pArpCxt->u4ContextId, ARP_MOD_TRC,
                                          CONTROL_PLANE_TRC, ARP_NAME,
                                          "ARP Cache Timer Expired for %x.",
                                          ((t_IP_TIMER *) pTimer)->pArg);
                            ARP_DS_LOCK ();
                            arp_drop (pArp_cache);
                            ARP_DS_UNLOCK ();
                        }
                        else
                        {
                            /* Trigger ARP request to refresh the ARP cache */
                            MEMSET (&ArpData, ARP_ZERO, sizeof (tARP_DATA));
                            ArpData.i2Hardware = pArp_cache->i2Hardware;
                            ArpData.u4IpAddr = pArp_cache->u4Ip_addr;
                            ArpData.u2Port = pArp_cache->u2Port;
                            ArpData.i1Hwalen = pArp_cache->i1Hwalen;
                            MEMCPY (ArpData.i1Hw_addr, pArp_cache->i1Hw_addr,
                                    ArpData.i1Hwalen);
                            ArpData.u1EncapType = pArp_cache->u1EncapType;
                            ArpData.i1State = (INT1) ARP_AGEOUT;
                            ArpAddArpEntry (ArpData, NULL, ARP_ZERO);
                        }
                    }
                }
#else
                ARP_DS_LOCK ();
                arp_drop (pArp_cache);
                ARP_DS_UNLOCK ();
#endif
                break;

            case ARP_REQUEST_RETRY_TIMER_ID:
            {
                ARP_DS_LOCK ();
                if ((pArp_cache = ((t_IP_TIMER *) pTimer)->pArg) != NULL)
                {
                    u2Port = pArp_cache->u2Port;
                    pArpCxt = pArp_cache->pArpCxt;

                    ARP_TRC_ARG1 (pArp_cache->pArpCxt->u4ContextId, ARP_MOD_TRC,
                                  CONTROL_PLANE_TRC, ARP_NAME,
                                  "ARP Request retransmission Timer Expired"
                                  " for %x.", pArp_cache->u4Ip_addr);
                    if (NetIpv4GetIfInfo (u2Port, &NetIpIfInfo)
                        == NETIPV4_SUCCESS)
                    {
                        u4MaxRetries =
                            (UINT4) pArpCxt->Arp_config.i4Max_retries;

                        if (NetIpIfInfo.u1EncapType == ARP_ENET_AUTO_ENCAP)
                        {
                            u4MaxRetries =
                                ARP_TWO *
                                (UINT4) pArpCxt->Arp_config.i4Max_retries;
                        }

                        if ((UINT4) pArp_cache->i4Retries >= u4MaxRetries)
                        {
                            pArp_cache->Timer.u1Id = ARP_CACHE_TIMER_ID;

                            if (pArpCxt->Arp_config.i4Cache_pend_time -
                                pArpCxt->Arp_config.i4Max_retries < ARP_ZERO)
                            {
                                /* Misconfiguration Pending time is less than 
                                 * max retries so the pending timer is not 
                                 * started */
                                ARP_DS_UNLOCK ();
                                ARP_PROT_UNLOCK ();
                                return;
                            }
                            u4Duration =
                                ((UINT4) pArpCxt->Arp_config.i4Cache_pend_time -
                                 (UINT4) pArpCxt->Arp_config.i4Max_retries);
                            /* Since retries are over start a pending timer */
                            if (TmrStartTimer (ARP_TIMER_LIST_ID,
                                               &(pArp_cache->Timer.Timer_node),
                                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC *
                                               u4Duration) == TMR_FAILURE)
                            {
                                ARP_TRC (pArpCxt->u4ContextId, ARP_MOD_TRC,
                                         ALL_FAILURE_TRC, ARP_NAME,
                                         "Timer failure in arp_timer_expiry_handler fn.\n");
                            }
                        }
                        else
                        {
                            /* If still retries are left send a arp request 
                               and start the ARP request retry timer so that 
                               no request are send within a second to the 
                               same destination */
                            if (ArpSendRequest (pArp_cache->u2Port,
                                                pArp_cache->u4Ip_addr,
                                                pArp_cache->i2Hardware,
                                                pArp_cache->u1EncapType) ==
                                ARP_FAILURE)
                            {
                                ARP_TRC (pArpCxt->u4ContextId, ARP_MOD_TRC,
                                         ALL_FAILURE_TRC, ARP_NAME,
                                         "ArpSendRequest failure in arp_timer_expiry_handler fn.\n");
                            }
                            pArp_cache->i4Retries++;
                            if ((pArp_cache->i4Retries ==
                                 pArpCxt->Arp_config.i4Max_retries)
                                && (NetIpIfInfo.u1EncapType
                                    == ARP_ENET_AUTO_ENCAP))

                            {
                                /* next try in SNAP encap */
                                pArp_cache->u1EncapType = ARP_ENET_SNAP_ENCAP;
                            }
                            if (TmrStartTimer (ARP_TIMER_LIST_ID,
                                               &(pArp_cache->Timer.Timer_node),
                                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC *
                                               ARP_REQUEST_RETRY_TIMEOUT) ==
                                TMR_FAILURE)
                            {
                                ARP_TRC (pArpCxt->u4ContextId, ARP_MOD_TRC,
                                         ALL_FAILURE_TRC, ARP_NAME,
                                         "Timer failure in arp_timer_expiry_handler fn.\n");
                            }
                        }
                    }
                }
                /* If the ARP has already resolved then remove the timer node 
                   but this is a hypothetical case */
                else
                {
                    TmrStopTimer (ARP_TIMER_LIST_ID,
                                  &(pArp_cache->Timer.Timer_node));
                }
                ARP_DS_UNLOCK ();
            }
                break;

#ifdef IP_WANTED
            case RARP_CLIENT_TIMER_ID:
                ARP_GBL_TRC (ARP_MOD_TRC, CONTROL_PLANE_TRC,
                             ARP_NAME, "RARP Client Timer Expired .\n");
                RarpClientTimerExpiryHandler ();
                break;
#endif /* IP_WANTED */
            default:
                break;
        }                        /* end of switch */
    }                            /* end of while */

    ARP_PROT_UNLOCK ();
}

/*-------------------------------------------------------------------+
 * Function           : ArpNotifyIfStChg 
 *
 * Input(s)           : u2Port, 
 *                     u4BitMap OPER_STATE/IFACE_DELETED
 *                      u4Status = CFA_IF_UP / CFA_IF_DOWN
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS/ARP_FAILURE.
 *
 * Action             : Notify the interface oper status down
 *                      notification to ARP.               
+-------------------------------------------------------------------*/
INT4
ArpNotifyIfStChg (UINT2 u2Port, UINT4 u4BitMap, UINT4 u4Status)
{
    tArpQMsg            ArpQMsg;
    UINT4               u4CfaIfIndex = 0;

    MEMSET (&ArpQMsg, ARP_ZERO, sizeof (tArpQMsg));

    if (u4BitMap & IFACE_DELETED)
    {
        ArpQMsg.u4MsgType = ARP_IF_DELETED;
    }

    if (u4BitMap & OPER_STATE)
    {
        ArpQMsg.u4MsgType = ARP_IF_OPER_CHG;
    }

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4CfaIfIndex)
        == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    ArpQMsg.u4IfIndex = u4CfaIfIndex;
    ArpQMsg.u2Port = u2Port;
    ArpQMsg.u4Status = u4Status;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpHandleUcastRtChg                                        */
/*                                                                           */
/*  Description : Post the event to ARP task for handling directly connected */
/*                static route deletion                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
ArpHandleUcastRtChg (tNetIpv4RtInfo * pRtChg, tNetIpv4RtInfo * pRtChg1,
                     UINT1 u1BitMap)
{
    tArpQMsg            ArpQMsg;

    UNUSED_PARAM (u1BitMap);
    MEMSET (&ArpQMsg, ARP_ZERO, sizeof (tArpQMsg));
    if ((pRtChg->u2RtType != IPROUTE_LOCAL_TYPE) ||
        (pRtChg->u2RtProto != CIDR_STATIC_ID) ||
        (pRtChg->u4RowStatus != RTM_DESTROY))
    {
        return;
    }

    if (u1BitMap == NETIPV4_MODIFY_ROUTE)
    {
        /* MODIFY Case - Delete & Add call should be given */
        ArpQMsg.u4MsgType = ARP_STATIC_RT_CHANGE;
        ArpQMsg.u2Port = (UINT2) pRtChg1->u4RtIfIndx;
        ArpQMsg.u4IpAddr = pRtChg1->u4DestNet;
        ArpQMsg.u4NetMask = pRtChg1->u4DestMask;

        if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
        {
            ARP_TRC (pRtChg->u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC,
                     ARP_NAME, "Handling Unicast route failed \r\n");
        }
        MEMSET (&ArpQMsg, ARP_ZERO, sizeof (tArpQMsg));
    }

    ArpQMsg.u4MsgType = ARP_STATIC_RT_CHANGE;
    ArpQMsg.u2Port = (UINT2) pRtChg->u4RtIfIndx;
    ArpQMsg.u4IpAddr = pRtChg->u4DestNet;
    ArpQMsg.u4NetMask = pRtChg->u4DestMask;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        ARP_TRC (pRtChg->u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "Handling Unicast route failed \r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ArpUtilRBTreeCreateStub                          */
/*                                                                           */
/*    Description         : This stub function is called when ARP_ARRAY_TO_  */
/*                          RBTREE_WANTED is disabled. To avoid compilation  */
/*                          break , condition covers always true/false, this */
/*                          function is added. Check its usage at calling    */
/*                          function.                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : ARP_SUCCESS                                      */
/*****************************************************************************/
INT4
ArpUtilRBTreeCreateStub ()
{
    return ARP_SUCCESS;
}
