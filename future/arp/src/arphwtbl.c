/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arphwtbl.c,v 1.4 2009/09/17 11:04:01 prabuc Exp $
 *
 * Description: Contains functions to process ARP H/w Table. 
 *
 *******************************************************************/
#include "arpinc.h"

static              t_ARP_HW_TABLE
    * arp_direct_get_free_hw_entry PROTO ((INT2 i2Hardware)),
    *arp_scan_table_and_get_hw_entry PROTO ((INT2 i2Hardware)),
    *arp_scan_table_and_get_free_hw_entry PROTO ((INT2 i2Hardware));

/*-------------------------------------------------------------------+
 * Function           : arp_init_hw_table
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Procedure to initialize the hardware table.
 *
+-------------------------------------------------------------------*/
VOID
arp_init_hw_table (VOID)
{
    INT2                i2Count = 0;
    /* Assume shortcut way of finding the entry first */
    arp_get_hw_entry = arp_direct_get_hw_entry;
    arp_get_free_hw_entry = arp_direct_get_free_hw_entry;

    for (i2Count = 0; i2Count < ARP_MAX_HW_TYPES; i2Count++)
    {
        gArpGblInfo.Arp_hw_table[i2Count].i1State = ARP_HW_FREE;
    }
    ARP_GBL_TRC (ARP_MOD_TRC, INIT_SHUT_TRC, ARP_NAME,
                 "ARP Hardware Table initialised.\n");
}

/*-------------------------------------------------------------------+
 * Function           : arp_add_hardware
 *
 * Input(s)           : i2Hardware, addr_fn, i2Protocol
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action :
 * Add an ARP hardware entry.
 * Called from LL when initializing an interface type that supports ARP.
 * Used for all interface types
 *
 * Current implementation assumes 4 as protocol length and treats it as
 * an unsigned integer. Protocol and protocol address length are not used.
 *
+-------------------------------------------------------------------*/
INT4
arp_add_hardware (UINT1 u1IfaceType, INT4 (*addr_fn) PROTO
                  ((UINT2
                    u2Port, INT1 *i1pAddr, INT1 *pHwalen, INT1 *pPalen)),
                  INT2 i2Protocol, INT4 (*pWrite) PROTO ((VOID)))
{
    INT2                i2Hardware = 0;

    t_ARP_HW_TABLE     *pHw_entry = NULL;

    i2Hardware = ARP_IFACE_TYPE (u1IfaceType);

    if (i2Hardware > ARP_MAX_HW_TYPES)
    {
        /* I can no longer go in for shortcut */

        arp_get_hw_entry = arp_scan_table_and_get_hw_entry;
        arp_get_free_hw_entry = arp_scan_table_and_get_free_hw_entry;
    }

    if ((pHw_entry = arp_get_hw_entry (i2Hardware)) == NULL)
    {

        if ((pHw_entry = arp_get_free_hw_entry (i2Hardware)) == NULL)
        {
            return ARP_FAILURE;
        }
    }

    /* It can be a new addition or a modification */

    pHw_entry->i2Hardware = i2Hardware;
    pHw_entry->i2Protocol = i2Protocol;

    pHw_entry->get_hw_address = addr_fn;
    pHw_entry->i1State = ARP_HW_VALID;
    pHw_entry->pWrite = pWrite;
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : arp_direct_get_hw_entry
 *
 * Input(s)           : i2Hardware
 *
 * Output(s)          : None.
 *
 * Returns            : Pointer to an entry in hw_table if ARP_SUCCESS
 *                      NULL if ARP_FAILURE.
 * Action :
 *
 * Returns entry corresponding to a hardware type.
 * Avoids scanning through table - gets an entry directly.
+-------------------------------------------------------------------*/
t_ARP_HW_TABLE     *
arp_direct_get_hw_entry (INT2 i2Hardware)
{
    if (gArpGblInfo.Arp_hw_table[i2Hardware].i1State == ARP_HW_VALID)
    {
        return (&gArpGblInfo.Arp_hw_table[i2Hardware]);
    }
    return NULL;
}

/*-------------------------------------------------------------------+
 * Function           : arp_direct_get_free_hw_entry
 *
 * Input(s)           : i2Hardware
 *
 * Output(s)          : None.
 *
 * Returns            : pointer to an entry in hw_table if ARP_SUCCESS
 *                      NULL if failure
 *
 * Action :
 *
 * Returns slot corresponding to hardware type.
 *
+-------------------------------------------------------------------*/
static t_ARP_HW_TABLE *
arp_direct_get_free_hw_entry (INT2 i2Hardware)
{

    return (&gArpGblInfo.Arp_hw_table[i2Hardware]);
}

/*-------------------------------------------------------------------+
 * Function           : arp_scan_table_and_get_hw_entry
 *
 * Input(s)           : i2Hardware
 *
 * Output(s)          : None.
 *
 * Returns            : Pointer to an entry in hw_table if ARP_SUCCESS
 *                      NULL if failure
 * Action :
 *
 * Gets entry corresponding to a hardware type.
+-------------------------------------------------------------------*/
static t_ARP_HW_TABLE *
arp_scan_table_and_get_hw_entry (INT2 i2Hardware)
{
    INT2                i2Count = 0;

    for (i2Count = 0; i2Count < ARP_MAX_HW_TYPES; i2Count++)
    {
        if ((i2Hardware == gArpGblInfo.Arp_hw_table[i2Count].i2Hardware) &&
            (gArpGblInfo.Arp_hw_table[i2Count].i1State == ARP_HW_VALID))
        {
            return (&gArpGblInfo.Arp_hw_table[i2Count]);
        }
    }
    return NULL;
}

/*-------------------------------------------------------------------+
 * Function           : arp_scan_table_and_get_free_hw_entry
 *
 * Input(s)           : i2Hardware
 *
 * Output(s)          : None.
 *
 * Returns            : Pointer to an entry in hw_table if ARP_SUCCESS
 *                      NULL if failure
 *
 * Action :
 *
 * Returns first available entry in hw_table.
 *
+-------------------------------------------------------------------*/
static t_ARP_HW_TABLE *
arp_scan_table_and_get_free_hw_entry (INT2 i2Hardware)
{
    INT2                i2Count = 0;

    UNUSED_PARAM (i2Hardware);
    for (i2Count = 0; i2Count < ARP_MAX_HW_TYPES; i2Count++)
    {
        if (gArpGblInfo.Arp_hw_table[i2Count].i1State == ARP_HW_FREE)
        {
            return (&gArpGblInfo.Arp_hw_table[i2Count]);
        }
    }
    return NULL;
}
