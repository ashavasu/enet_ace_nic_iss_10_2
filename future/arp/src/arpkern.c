/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpkern.c,v 1.1 2017/04/03 15:16:23 siva Exp $
 *
 * Description: Functions for updating ARP cache infrmation in the kernel
 *
 *******************************************************************/
#ifndef _ARPKERN_C
#define _ARPKERN_C

#include "arpinc.h"
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<netdb.h>
#include<errno.h>
#include<string.h>
#include<netinet/if_ether.h>
#include<net/if.h>
#include<linux/sockios.h>

#define ARP_CONNECT_PORT  1025

#ifdef NTPS_WANTED

extern INT4  CfaGetIfIpPortVlanId(UINT4 u4IfIndex , INT4 *pi4PortVlanId);
extern INT1 NtpsGetSourceInterface(UINT4 * pu4NtpsSourceInterface);

/*-------------------------------------------------------------------+
 *
 * Function           : ArpUpdateKernelEntry
 *
 * Input(s)           : i1EntryType - ARP_STATIC/ARP_INVALID 
 *                      pu1MacAddr - physical address to be stored.
 *                      i4MacAddrLen - physical address length.
 *                      u4IpAddr - Ip address to be added in the kernel
 *                      pu1IfName - Interface name.
 * Output(s)          : None.
 *
 * Returns            : ARP_FAILURE on failure or ARP_SUCCESS on success
 *
 * Action             : Function that handles the static entry addition
 *                      in the Linux Kernal arp cache.
 *
+-------------------------------------------------------------------*/
INT4
ArpUpdateKernelEntry (INT1 i1EntryType, UINT1 *pu1MacAddr, INT4 i4MacAddrLen,
                      UINT4 u4IpAddr, UINT1 *pu1IfName)
{
    INT4                i4SockFd = -1;
    struct arpreq       ArpReq;
    struct sockaddr_in  IpAddr;
    UINT4               u4IoctlReq = 0;
    INT4                i4RetVal = ARP_SUCCESS;
    INT4                i4VlanId = 0;
    UINT4               u4NtpSourceInterface = 0;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1               u1IfaceType = 0;

    UNUSED_PARAM(*pu1IfName);
    MEMSET (&(IpAddr),0, sizeof (struct sockaddr_in));
    MEMSET (&(ArpReq.arp_pa),0, sizeof (ArpReq.arp_pa));
    MEMSET (&(ArpReq.arp_ha),0, sizeof (ArpReq.arp_ha));
    MEMSET(au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    NtpsGetSourceInterface(&u4NtpSourceInterface);
    if (u4NtpSourceInterface > 0)
    {
    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd < 0)
    {
        return ARP_FAILURE;
    }
    /* Get the NTPS Source Interface */
        if (CfaGetIfType(u4NtpSourceInterface, &u1IfaceType) == CFA_SUCCESS)
        {
            if (u1IfaceType == CFA_ENET)
            {
                CfaGetIfIpPortVlanId(u4NtpSourceInterface,&i4VlanId);
            }
            if (u1IfaceType == CFA_L3IPVLAN)
            {
                CfaGetVlanId(u4NtpSourceInterface, (UINT2 *)&i4VlanId);
            }
            SNPRINTF((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH,"vlan%d",i4VlanId);
        }

    IpAddr.sin_family = AF_INET;
    IpAddr.sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    MEMCPY (&(ArpReq.arp_pa), (struct sockaddr *) &IpAddr,
            sizeof (struct sockaddr));
    STRCPY (ArpReq.arp_dev, au1IfName);
    ArpReq.arp_flags = ATF_PERM;

    if ((i1EntryType == ARP_STATIC) || (i1EntryType == ARP_DYNAMIC))
    {
        if (ioctl (i4SockFd, SIOCGARP, &ArpReq) < 0)
        {
            ArpReq.arp_ha.sa_family = AF_UNSPEC;
        }
        else
        {
            /* If the arp entry exists in dynamic state, the flag field will
             * have value other than ATF_PERM.So the flag value is again set 
             * to permanant.*/
            ioctl (i4SockFd, SIOCDARP, &ArpReq);
            ArpReq.arp_flags = ATF_PERM;
        }
        MEMCPY (&(ArpReq.arp_ha.sa_data), (INT1 *) pu1MacAddr, i4MacAddrLen);
        u4IoctlReq = SIOCSARP;
    }
    else if (i1EntryType == ARP_INVALID)
    {
        u4IoctlReq = SIOCDARP;
    }

    if ((u4IoctlReq == SIOCSARP) || (u4IoctlReq == SIOCDARP))
    {
        if (ioctl (i4SockFd, u4IoctlReq, &ArpReq) < 0)
        {
            if (u4IoctlReq == SIOCDARP)
            {
                /* Failure in kernel ARP entry deletion is valid if 
                 * the entry doesn't exist. */
                i4RetVal = ARP_SUCCESS;
            }
            else
            {
                i4RetVal = ARP_FAILURE;

            }
        }
    }
    close (i4SockFd);
     }
    return (i4RetVal);
}
/*****************************************************************************/
/*  Function Name             : ArpAddAllArpEntriesInKernel                  */
/*  Description               : This function scans the compleate ARP table  */
/*                              to add all APR entries learnt in the default */
/*                              context to the Kernel                        */
/*  Input(s)                  : None.                                        */
/*  Output(s)                 : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : VOID                                         */
/*****************************************************************************/

VOID
ArpAddAllArpEntriesInKernel ()
{
    t_ARP_CACHE        *pArpCache, *pNextArpCache = NULL;
    tArpCxt            *pArpCxt = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4ContextId = ARP_DEFAULT_CONTEXT;

    ARP_PROT_LOCK ();
    ARP_DS_LOCK ();

    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];
    if (pArpCxt == NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                      "ArpAddAllArpEntriesInCxt: Invalid context id - %d\n",
                      u4ContextId);
        ARP_DS_UNLOCK ();
        ARP_PROT_UNLOCK ();
        return;
    }

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    while (pArpCache != NULL)
    {
        pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);
        if (pArpCache->pArpCxt->u4ContextId != u4ContextId)
        {
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE)
        {
            /* Statically configured and Rowstaus as NOT_IN_SERVICE 
             * or NOT_READY
             */
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC)
        {
            /* Dont add the Entries to NP if OperState of interface
             * is down. Entries will be added to NP when interface 
             * comes up */
            MEMSET (&NetIpIfInfo, ARP_ZERO, sizeof (tNetIpv4IfInfo));
            if (NetIpv4GetIfInfo ((UINT4) pArpCache->u2Port,
                                  &NetIpIfInfo) != NETIPV4_SUCCESS)
            {
                pArpCache = pNextArpCache;
                continue;
            }
            if (NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE)
            {
                pArpCache = pNextArpCache;
                continue;
            }
        }
         /*Add the ARP Entry in Kernel*/ 
         ArpUpdateKernelEntry (ARP_STATIC, (UINT1 *) pArpCache->i1Hw_addr,
			       pArpCache->i1Hwalen,pArpCache->u4Ip_addr,0);

        pArpCache = pNextArpCache;
    }

    ARP_DS_UNLOCK ();
    ARP_PROT_UNLOCK ();
    return;
}
#endif
#endif /* _ARPKERN_C */
