/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: arpstb.c,v 1.3 2016/05/26 10:25:34 siva Exp $ 
 *
 * Description: This file contains ARP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __ARPRED_C
#define __ARPRED_C

#include "arpinc.h"
#include "arpred.h"

/************************************************************************/
/*  Function Name   : ArpRedInitGlobalInfo                            */
/*                                                                      */
/*  Description     : This function is invoked by the ARP module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
ArpRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ArpRedRmRegisterProtocols                       */
/*                                                                      */
/*  Description     : This function is invoked by the ARP module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
ArpRedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ArpRmDeRegisterProtocols                        */
/*                                                                      */
/*  Description     : This function is invoked by the ARP module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
ArpRedRmDeRegisterProtocols (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedRmCallBack                              */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to ARP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedHandleRmEvents                          */
/*                                                                      */
/* Description        : This function is invoked by the ARP module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the ARP Q Msg        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleRmEvents ()
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleGoActive                          */
/*                                                                      */
/* Description        : This function is invoked by the ARP upon*/
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleGoStandby                         */
/*                                                                      */
/* Description        : This function is invoked by the ARP upon*/
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleGoStandby (tArpRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleIdleToActive                      */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleIdleToStandby                     */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpStartTimers                                  */
/*                                                                      */
/* Description        : This is a recursive function which is registered*/
/*                      to RBTreeWalk. The RBTree is walked and the     */
/*                      elements in the RBtree is checked for either    */
/*                      static or dynamic entry. If static entry, then  */
/*                      the timer is not started. If the entry is       */
/*                      dynamic, then the cache timer is started.       */
/*                                                                      */
/* Input(s)           : pRBElem - RBTree to walk.                       */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RB_WALK_CONT/RB_WALK_BREAK                      */
/************************************************************************/

INT4
ArpStartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                void *pArg, void *pOut)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (visit);
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);
    return RB_WALK_CONT;
}

/************************************************************************/
/* Function Name      : ArpRedHandleStandbyToActive                   */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleActiveToStandby                   */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessPeerMsgAtActive                  */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessPeerMsgAtStandby                 */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This function is invoked by the ARP module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkReqMsg                          */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedSendBulkReqMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedDeInitGlobalInfo                        */
/*                                                                      */
/* Description        : This function is invoked by the ARP module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register ARP with RM.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
ArpRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkUpdMsg                      */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedSendBulkUpdMsg (VOID)
{
    return;

}

/***********************************************************************
 * Function Name      : ArpRedSendBulkCacheInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby ARP.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : ARP_SUCCESS/ARP_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
ArpRedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);

    return ARP_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkUpdTailMsg                      */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the ARP offers an IP address */
/*                      to the ARP client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pArpBindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
ArpRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessBulkTailMsg                      */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    return;
}

/************************************************************************
 * Function Name      : ArpRedSendDynamicCacheInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the ARP offers an IP address 
 *                      to the ARP client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
ArpRedSendDynamicCacheInfo ()
{
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessBulkInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessDynamicInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pArpCache - ArpCacje message                    */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
ArpRedAddDynamicInfo (t_ARP_CACHE * pArpCache, UINT1 u1Operation)
{
    UNUSED_PARAM (pArpCache);
    UNUSED_PARAM (u1Operation);

    return;
}

/************************************************************************/
/* Function Name      : ArpRedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the ArpRedTable is walked, if      */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/*                      Second:                                         */
/*                      When there is a transaction between standby and */
/*                      active node, the entries in the hardware are    */
/*                      retrived and checked with the software. If      */
/*                      is a mismatch in the entry, the entry in the    */
/*                      hardware is updated to the software.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
ArpRedHwAudit ()
{

    return;
}

/*-------------------------------------------------------------------+
 * Function           : ArpRBTreeRedEntryCmp 
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      ARP_RB_GREATER if pRBElem >  pRBElemIn
 *                      ARP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
ArpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (pRBElemIn);
    return ARP_RB_EQUAL;
}


/************************************************************************
 * Function Name      : ArpRedSendDynamicCacheTimeInfo
 *
 * Description        : This function sends the binding table dynamic
 *                      sync up message to Standby node from the Active
 *                      node, when the ARP offers an IP address
 *                      to the ARP client and dynamically update the
 *                      binding table entries.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

PUBLIC VOID
ArpRedSendDynamicCacheTimeInfo (t_ARP_CACHE *pArpInfo)
{
    UNUSED_PARAM (pArpInfo);
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessDynamicCacheTimeInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessDynamicCacheTimeInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}
    
#endif
