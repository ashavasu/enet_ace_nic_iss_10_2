/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpmain.c,v 1.88 2017/12/26 13:02:53 siva Exp $
 *
 * Description:Contains functions to process arp packets,   
 *             send packets handle timer expiry and         
 *             initialize ARP.                              
 *                               
 *******************************************************************/
#include "arpinc.h"
#include "arpred.h"

t_ARP_HW_TABLE     *(*arp_get_hw_entry) PROTO ((INT2 i2Hardware)),
    *(*arp_get_free_hw_entry) PROTO ((INT2 i2Hardware));

/*****************************************************************************/
/* Function     : ArpProtocolLock                                            */
/*                                                                           */
/*  Description : Takes the ARP Protocol Semaphore                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
ArpProtocolLock (VOID)
{
    if (OsixSemTake (ARP_PROTOCOL_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpProtocolUnLock                                          */
/*                                                                           */
/*  Description : Releases the ARP Protcol Semaphore                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
ArpProtocolUnLock (VOID)
{
    OsixSemGive (ARP_PROTOCOL_SEM_ID);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpArpcacheLock                                            */
/*                                                                           */
/*  Description : Takes the Semaphore for Protecting ARPCACHE Table          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
ArpArpcacheLock (VOID)
{
    if (OsixSemTake (ARP_CACHE_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpArpcacheUnLock                                          */
/*                                                                           */
/*  Description : Releases the Semaphore for Protecting ARPCACHE Table       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
ArpArpcacheUnLock (VOID)
{
    OsixSemGive (ARP_CACHE_SEM_ID);
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ArpGetHwAddr                                      */
/*  Description     : Wrapper function for CfaGddGetHwAddr              */
/*  Input(s)        : u4IfIndex --> Interface Index                     */
/*                  : pHwAddr  --> pointer to Hw Address Array          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
INT4
ArpGetHwAddr (UINT2 u2IfIndex, UINT1 *pHwAddr)
{
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, ARP_ZERO, sizeof (tCfaIfInfo));
    if (CfaGetIfInfo (u2IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return ARP_FAILURE;
    }

    MEMCPY (pHwAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    return ARP_SUCCESS;
}

INT1
ArpGetCacheEntryCountInCxt (UINT4 u4ContextId, UINT4 *pu4ArpEntryCount)
{
    ARP_DS_LOCK ();
    *pu4ArpEntryCount = gArpGblInfo.apArpCxt[u4ContextId]->u4ArpEntryCount;
    ARP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/* This function returns Total Pending ArpEntry Count.This Function 
   will be replaced with nmhRoutine when a MIB object is added */

INT1
ArpGetPendingCacheEntryCountInCxt (UINT4 u4ContextId,
                                   UINT4 *pu4PendArpEntryCount)
{
    ARP_DS_LOCK ();
    *pu4PendArpEntryCount =
        gArpGblInfo.apArpCxt[u4ContextId]->u4PendingEntryCount;
    ARP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: ARP_Process_Pkt_Arrival_Event                           */
 /*                                                                         */
 /*  Description  : processes the pkt arrived from the ip.                  */
 /*                 should be called when pkt processing event is arrived.  */
 /*                                                                         */
 /*  Input(s)     : None.                                                   */
 /*                                                                         */
 /*  Output(s)    : None.                                                   */
 /*                                                                         */
 /*  Returns      : ARP_SUCCESS - if processing is successful.              */
 /*               : ARP_FAILURE - otherwise.                                */
 /*                                                                         */
 /***************************************************************************/

VOID
ARP_Process_Pkt_Arrival_Event (VOID)
{
    tArpQMsg           *pArpQMsg = NULL;
    UINT2               u2Len = ARP_ZERO;
    UINT4               u4ContextId = 0;
    /*
     * An Arp Packet is received.
     * The user data portion holds the port index information.
     */

    while (OsixQueRecv (ARP_PKT_Q_ID,
                        (UINT1 *) &pArpQMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pArpQMsg->pBuf != NULL)
        {
            u2Len = (UINT2) IP_GET_BUF_LENGTH (pArpQMsg->pBuf);
        }
        ARP_PROT_LOCK ();

        switch (pArpQMsg->u4MsgType)
        {
            case CFA_GRAT_ARP_REQ:
                ArpSendGratArpRequest (pArpQMsg);
                break;

            case FROM_CFA_TO_ARP:
                /* arp paket from lower layer */
                arp_input (pArpQMsg);
                break;
            case ARP_APP_IF_MSG:
            {
                /* IP could not resolve the packet so gives it to arp.
                 * arp will try to resolve by sending req. outside.
                 * till it successfully resolves or timeout, packet
                 * remains in pending state.  */

                VcmGetContextInfoFromIpIfIndex ((UINT4) pArpQMsg->u2Port,
                                                &u4ContextId);
                if (pArpQMsg->pBuf != NULL)
                {
                    ARP_PKT_DUMP (u4ContextId, ARP_MOD_TRC,
                                  DUMP_TRC, ARP_NAME, pArpQMsg->pBuf,
                                  u2Len, "IP pkt waiting in pending Q.\n");
                }
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                if (ArpProcessQMsg (pArpQMsg) != ARP_SUCCESS)
                {
                    /* Remove the Entry created in the DLF Hash Table */
                    ArpFsCfaHwRemoveIpNetRcvdDlfInHash (u4ContextId,
                                                        pArpQMsg->u4IpAddr,
                                                        0xffffffff);
                }
#else
                ArpProcessQMsg (pArpQMsg);
#endif
                break;
            }

            case ARP_IF_DELETED:
                ArpActOnIfDelete (pArpQMsg->u4IfIndex);
                break;

            case ARP_L2_IF_DELETED:
                ArpActOnL2IfDelete (pArpQMsg->u4IfIndex,
                                    pArpQMsg->u4PhyIfIndex);
                break;

            case ARP_IF_OPER_CHG:
                ArpActOnIfOperChg (pArpQMsg->u4IfIndex, pArpQMsg->u2Port,
                                   pArpQMsg->u4Status);
                break;

            case ARP_STATIC_RT_CHANGE:
                ArpActOnStaticRtChange (pArpQMsg->u2Port, pArpQMsg->u4IpAddr,
                                        pArpQMsg->u4NetMask);
                break;
#ifdef IP_WANTED
                /* For Enabling RARP Client on a Interface,this message is
                   posted from CFA .It is done to satify the constraints in
                   proposed for Mutual Exclusion at CFA. "From CFA->Other Task,
                   it should be message Posting" */
            case RARP_CLIENT_ENABLE:
                RarpClientSendRequest (pArpQMsg->u2Port);
                break;
#endif
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                ArpMbsmUpdateArpTable (pArpQMsg->pBuf, pArpQMsg->u4MsgType);
                IP_RELEASE_BUF (pArpQMsg->pBuf, FALSE);
                break;
#endif
            case ARP_CONTEXT_DELETE:
                ArpHandleContextDelete (pArpQMsg->u4ContextId);
                break;

            default:
            {

                ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC,
                             ARP_NAME,
                             "Alert. Rx buffer from unknown module.\n");
                IP_RELEASE_BUF (pArpQMsg->pBuf, FALSE);
                break;
            }
        }
        MemReleaseMemBlock (ARP_Q_MSG_POOL_ID, (UINT1 *) pArpQMsg);
        ARP_PROT_UNLOCK ();
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpEnqueuePkt.
 *
 * Input(s)           :pRecvdQMsg - Pointer to the tArpQMsg structure.
 *
 * Output(s)          : None
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 * Enqueues the packets to ARP for queueing it until address is resolved.
 *
+-------------------------------------------------------------------*/
INT4
ArpEnqueuePkt (tArpQMsg * pRecvdQMsg)
{
    tArpQMsg           *pArpQMsg = NULL;
    UINT4               u4ContextId = 0;

    if (ARP_Q_MSG_POOL_ID == 0)
    {
        /* In High availability case, this function can be called by IP task, eventhough
         * arp is not ready. In such cases, drop the event
         */
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ARP is not initialised. So dropping incoming event.\n");
        return ARP_FAILURE;
    }
    VcmGetContextIdFromCfaIfIndex ((UINT4) pRecvdQMsg->u4IfIndex, &u4ContextId);

    pArpQMsg = (tArpQMsg *) MemAllocMemBlk (ARP_Q_MSG_POOL_ID);
    if (NULL == pArpQMsg)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "Memory Allocation failure for ARP QMsg Entry.\n");
        return ARP_FAILURE;
    }
    MEMCPY (pArpQMsg, pRecvdQMsg, sizeof (tArpQMsg));

    if (OsixQueSend (ARP_PKT_Q_ID,
                     (UINT1 *) (&pArpQMsg), OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (ARP_Q_MSG_POOL_ID, (UINT1 *) pArpQMsg);
        return ARP_FAILURE;
    }

    OsixEvtSend (ARP_TASK_ID, ARP_PACKET_ARRIVAL_EVENT);

    return ARP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             : arp_new_call                                 */
/*  Description               : This function scans the compleate ARP table  */
/*                              to add all APR entries learnt in the default */
/*                              context to the L3 IP Table                   */
/*  Input(s)                  : pMacAddr - Mac Address                       */
/*                              VlanId  - VLan Id                            */
/*                              u2Port  - Port Number                        */
/*  Output(s)                 : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : ARP_FAILURE/ARP_SUCCESS                      */
/*****************************************************************************/
INT4
ArpDeleteByMac (UINT1 *pMacAddr, UINT2 VlanId, UINT2 u2Port)
{
    tArpQFlushMac      *pQMacAddr = NULL;

    if ((pQMacAddr =
         (tArpQFlushMac *) MemAllocMemBlk (ARP_VLAN_MAC_POOL_ID)) == NULL)
    {

        return ARP_FAILURE;
    }
    MEMCPY (pQMacAddr->pQMacAddr, pMacAddr, sizeof (tMacAddr));
    pQMacAddr->VlanId = VlanId;
    pQMacAddr->u2Port = u2Port;
    if (OsixQueSend (ARP_NEW_PKT_Q_ID,
                     (UINT1 *) (&pQMacAddr),
                     sizeof (tArpQFlushMac)) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (ARP_VLAN_MAC_POOL_ID, (UINT1 *) pQMacAddr);
        return ARP_FAILURE;
    }
    OsixEvtSend (ARP_TASK_ID, ARP_FLUSH_BY_PORT_EVENT);
    return ARP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             : ArpAddAllArpEntries                          */
/*  Description               : This function scans the compleate ARP table  */
/*                              to add all APR entries learnt in the default */
/*                              context to the L3 IP Table                   */
/*  Input(s)                  : None.                                        */
/*  Output(s)                 : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : VOID                                         */
/*****************************************************************************/

VOID
ArpAddAllArpEntries ()
{
    ArpAddAllArpEntriesInCxt (ARP_DEFAULT_CONTEXT);
}

/*****************************************************************************/
/*  Function Name             : ArpDelAllArpEntries                          */
/*  Description               : This function scans the complete ARP table   */
/*                              to delete all ARP entries learnt in the      */
/*                              default context from the H/W.                */
/*  Input(s)                  : None.                                        */
/*  Output(s)                 : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : None                                         */
/*****************************************************************************/
VOID
ArpDelAllArpEntries ()
{
    ArpDelAllArpEntriesInCxt (ARP_DEFAULT_CONTEXT);
}

/*****************************************************************************/
/*  Function Name             : ArpGetNextArpEntry                           */
/*  Description               : This function returns the ARP entry next to  */
/*                              the specified entry from the S/W table.      */
/*                              It returnas the First Entry if the specified */
/*                              entry is all zero.                           */
/*  Input(s)                  : pi4CfaIfIndex -CFA If Index.                 */
/*                              pu4IpAddr - IP address.                      */
/*                              pMacAddr - MAC address.                      */
/*  Output(s)                 : pi4CfaIfIndex - Next CFA If Index.           */
/*                              pu4IpAddr -Next IP address.                  */
/*                              pMacAddr -Next MAC address.                  */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : ARP_SUCCESS/ARP_FAILURE                        */
/******************************************************************************/

INT4
ArpGetNextArpEntry (INT4 *pi4CfaIfIndex, UINT4 *pu4IpAddr, UINT1 *pMacAddr)
{
    t_ARP_CACHE         tArp_cache;
    UINT4               u4NextIpAddress = ARP_ZERO;
    INT4                i4CfaNextIfIndex = ARP_ZERO;
    INT4                i4RetVal = ARP_ZERO;
    INT4                i4LookUp = ARP_ZERO;

    MEMSET (pMacAddr, ARP_ZERO, CFA_ENET_ADDR_LEN);
    i4RetVal = ArpGetNextFromArpcacheTable (*pi4CfaIfIndex,
                                            &i4CfaNextIfIndex, *pu4IpAddr,
                                            &u4NextIpAddress);
    if (i4RetVal != ARP_SUCCESS)
    {
        return ARP_FAILURE;
    }

    *pi4CfaIfIndex = i4CfaNextIfIndex;
    *pu4IpAddr = u4NextIpAddress;

    i4LookUp = ArpLookupWithIndex ((UINT4) i4CfaNextIfIndex, u4NextIpAddress,
                                   &tArp_cache);

    if (i4LookUp == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }

    if ((tArp_cache.i1State == ARP_DYNAMIC) ||
        (tArp_cache.i1State == ARP_STATIC))
    {
        MEMCPY (pMacAddr, tArp_cache.i1Hw_addr, tArp_cache.i1Hwalen);
        return ARP_SUCCESS;
    }
    return ARP_FAILURE;
}

/*****************************************************************************/
/*  Function Name             :  ArpNpAdd                                     */
/*  Description               :  Creates  an ARP entry in Fast Path          */
/*                               ARP Cache.                                   */
/*  Input(s)                  :  u4ContextId    -- Virtual Router ContextId   */
/*                               u4Tproto_addr  -- IpAddress                  */
/*                               i2Hardware     -- Hardware type              */
/*                               i1pHwAddr      -- Hardware Address           */
/*                               i1Hwalen       -- Hardware Address Length    */
/*                               u4IfIndex      -- Interface index through    */
/*                                                 which ARP entry is learnt  */
/*                               u1EncapType    -- Encapsulation Type         */
/*                               i1State        -- ARP_STATIC/ARP_DYNAMIC     */
/*  Output(s)                 :  None                                         */
/*  Global Variables Referred :  None                                         */
/*  Global variables Modified :  None                                         */
/*  Exceptions                :  None                                         */
/*  Use of Recursion          :  None                                         */
/*  Returns                   :  ARP_FAILURE(-1) on failure                   */
/*                               ARP_SUCCESS(0) on success                    */
/*****************************************************************************/
INT4
ArpNpAdd (UINT4 u4ContextId, UINT4 u4Tproto_addr,
          INT2 i2Hardware, INT1 *i1pHwAddr,
          INT1 i1Hwalen, UINT4 u4CfaIfIndex, UINT1 u1EncapType, INT1 i1State)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pTmpCache = NULL;
    tCfaIfInfo          CfaIfInfo;
#ifdef NPAPI_WANTED
    tVlanIfaceVlanId    u2VlanId = ARP_ZERO;
    UINT4               u4Port = ARP_ZERO;
    UINT4               u4TblFull = OSIX_FALSE;
    UINT1               bu1TblFull = OSIX_FALSE;
    UINT1               u1Status = VLAN_FDB_LEARNT;
    UINT2               u2Port = ARP_ZERO;
    UINT4               u4L2CxtId = ARP_ZERO;
    INT4                i4VlanAdminMacLearningStatus = ARP_ZERO;
#endif
    UINT1               u1IpForward;
    UNUSED_PARAM (i2Hardware);
    UNUSED_PARAM (i1pHwAddr);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (i1Hwalen);
    UNUSED_PARAM (u1EncapType);

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4CfaIfIndex;
    ArpCache.u4Ip_addr = u4Tproto_addr;
    pTmpCache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
    if (pTmpCache == NULL)
    {
        return ARP_FAILURE;
    }

    if (ARP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pTmpCache->u1HwStatus = NP_UPDATED;
        return ARP_SUCCESS;
    }

    MEMSET (&CfaIfInfo, ARP_ZERO, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return ARP_FAILURE;
    }

    if (NetIpv4GetIpForwardingInCxt (u4ContextId, &u1IpForward)
        == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }
    else
    {
        if (u1IpForward != IP_FORW_ENABLE)
        {
            return ARP_SUCCESS;
        }
    }

    if (CfaIfInfo.u1IfType == CFA_L2VLAN)
    {
        return ARP_SUCCESS;
    }

    /* If the Port is OOB Port,Dont Program in HardWare */
    if ((CfaIsMgmtPort (u4CfaIfIndex) == FALSE) &&
        (CfaIsLinuxVlanIntf (u4CfaIfIndex) == FALSE))
    {
#ifdef NPAPI_WANTED
        pTmpCache->u1HwStatus = NP_NOT_UPDATED;
        if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
            && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
        {
            ArpRedAddDynamicInfo (pTmpCache, ARP_RED_ADD_CACHE);
            ArpRedSendDynamicCacheInfo ();
        }

        if ((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
            (CfaIfInfo.u1IfType == CFA_L3SUB_INTF))
        {
            /* (i) Identify the security VLAN associated with this L3 VLAN 
             *     interface index based on the MAC address learnt using 
             *     CfaGetSecVlanId (OR)
             * (ii) Identify the direct L2 VLAN associated with the L3 VLAN
             *      using CfaGetVlanId call
             */
            if (((CfaGetSecVlanId
                  (u4CfaIfIndex, (UINT1 *) i1pHwAddr,
                   &u2VlanId)) == CFA_SUCCESS)
                || (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS))
            {
                /*Get the port number with the vlan and mac-address
                 *and pass it for ARP addition so that for hardware chipset
                 *the port info can be taken with this param, when the mac learning is enabled*/
                if (VcmGetL2CxtIdForIpIface (u4CfaIfIndex, &u4L2CxtId) ==
                    VCM_FAILURE)
                {
                    return ARP_FAILURE;
                }
                if (VlanGetAdminMacLearnStatus
                    (u4L2CxtId, (UINT4) u2VlanId,
                     &i4VlanAdminMacLearningStatus) == VLAN_FAILURE)
                {
                    return ARP_FAILURE;
                }
                if (i4VlanAdminMacLearningStatus == VLAN_ENABLED)
                {
                    if ((i1State != ARP_PENDING) && (i1State != ARP_STATIC))
                    {
                        if (ArpFsNpHwGetPortFromFdb
                            (u2VlanId, (UINT1 *) i1pHwAddr,
                             &u4Port) == FNP_FAILURE)
                        {
                            return ARP_FAILURE;
                        }
                    }
                    u4TblFull = u4Port;
                }

                if (ArpFsNpIpv4VrfArpAdd (u4ContextId, u2VlanId, u4CfaIfIndex,
                                          u4Tproto_addr, (UINT1 *) i1pHwAddr,
                                          CfaIfInfo.au1IfName, i1State,
                                          &u4TblFull) == OSIX_FALSE)
                {
                    return ARP_FAILURE;
                }
                /* Update the L2 physical index details in ARP cache entry. 
                 * This is required to refresh ARP cache entries upon the oper status
                 * change of a specific L2 physical port/LAG port.
                 */
                VlanGetFdbEntryDetails (u2VlanId, (UINT1 *) i1pHwAddr,
                                        &u2Port, &u1Status);
                if (u2Port != 0)
                {
                    pTmpCache->u4PhyIfIndex = u2Port;
                }
                ArpFsNpIpv4SyncVlanAndL3Info ();
            }
        }
        else if ((CfaIfInfo.u1IfType == CFA_ENET)
                 || (CfaIfInfo.u1IfType == CFA_PSEUDO_WIRE))
        {
            if (ArpFsNpL3Ipv4VrfArpAdd
                (u4ContextId, u4CfaIfIndex, u4Tproto_addr, (UINT1 *) i1pHwAddr,
                 CfaIfInfo.au1IfName, i1State, &bu1TblFull) == OSIX_FALSE)
            {
                return ARP_FAILURE;
            }
        }
        pTmpCache->u1HwStatus = NP_UPDATED;
        if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
            && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
        {
            ArpRedAddDynamicInfo (pTmpCache, ARP_RED_ADD_CACHE);
            ArpRedSendDynamicCacheInfo ();
        }
#else
        pTmpCache->u1HwStatus = NP_UPDATED;
#endif

    }
    return ARP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  ArpNpModify                                  */
/*  Description               :  Creates or updates an ARP entry in Fast Path */
/*                               ARP Cache.                                   */
/*  Input(s)                  :  u4ContextId    -- Virtual Router ContextId   */
/*                               u4Tproto_addr  -- IpAddress                  */
/*                               i2Hardware     -- Hardware type              */
/*                               i1pHwAddr      -- Hardware Address           */
/*                               i1Hwalen       -- Hardware Address Length    */
/*                               u4IfIndex      -- Interface index through    */
/*                                                 which ARP entry is learnt  */
/*                               u1EncapType    -- Encapsulation Type         */
/*                               i1State        -- ARP_STATIC/ARP_DYNAMIC     */
/*  Output(s)                 :  None                                         */
/*  Global Variables Referred :  None                                         */
/*  Global variables Modified :  None                                         */
/*  Exceptions                :  None                                         */
/*  Use of Recursion          :  None                                         */
/*  Returns                   :  ARP_FAILURE(-1) on failure                   */
/*                               ARP_SUCCESS(0) on success                    */
/*****************************************************************************/
INT4
ArpNpModify (UINT4 u4ContextId, UINT4 u4Tproto_addr,
             INT2 i2Hardware, INT1 *i1pHwAddr,
             INT1 i1Hwalen, UINT4 u4CfaIfIndex, UINT1 u1EncapType, INT1 i1State)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pTmpCache = NULL;
    tCfaIfInfo          CfaIfInfo;
#ifdef NPAPI_WANTED
    tVlanIfaceVlanId    u2VlanId = ARP_ZERO;
    UINT4               u4Port = ARP_ZERO;
    UINT4               u4PhyIfIndex = ARP_ZERO;
    UINT2               u2Port = ARP_ZERO;
    UINT4               u4L2CxtId = ARP_ZERO;
    UINT1               u1Status = VLAN_FDB_LEARNT;
    INT4                i4VlanAdminMacLearningStatus = ARP_ZERO;
#endif
    UINT1               u1IpForward;
    UNUSED_PARAM (i2Hardware);
    UNUSED_PARAM (i1Hwalen);
    UNUSED_PARAM (i1pHwAddr);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (u1EncapType);

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4CfaIfIndex;
    ArpCache.u4Ip_addr = u4Tproto_addr;
    pTmpCache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
    if (pTmpCache == NULL)
    {
        return ARP_FAILURE;
    }

    if (ARP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pTmpCache->u1HwStatus = NP_UPDATED;
        return ARP_SUCCESS;
    }

    MEMSET (&CfaIfInfo, ARP_ZERO, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return ARP_FAILURE;
    }

    if (NetIpv4GetIpForwardingInCxt (u4ContextId, &u1IpForward)
        == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }
    else
    {
        if (u1IpForward != IP_FORW_ENABLE)
        {
            return ARP_SUCCESS;
        }
    }

    if (CfaIfInfo.u1IfType == CFA_L2VLAN)
    {
        return ARP_SUCCESS;
    }

    /* If the Port is OOB Port,Dont Program in HardWare */
    if (CfaIsMgmtPort (u4CfaIfIndex) == FALSE)
    {
#ifdef NPAPI_WANTED
        pTmpCache->u1HwStatus = NP_NOT_UPDATED;
        if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
            && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
        {
            ArpRedAddDynamicInfo (pTmpCache, ARP_RED_ADD_CACHE);
            ArpRedSendDynamicCacheInfo ();
        }

        if ((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
            (CfaIfInfo.u1IfType == CFA_L3SUB_INTF))
        {
            if (((CfaGetSecVlanId
                  (u4CfaIfIndex, (UINT1 *) i1pHwAddr,
                   &u2VlanId)) == CFA_SUCCESS)
                || (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS))

            {
                /*Get the port number with the vlan and mac-address
                 *and pass it for ARP addition so that for BCM 
                 *the port info can be taken with this param, when the mac learning is enabled*/
                if (VcmGetL2CxtIdForIpIface (u4CfaIfIndex, &u4L2CxtId) ==
                    VCM_FAILURE)
                {
                    return ARP_FAILURE;
                }
                if (VlanGetAdminMacLearnStatus
                    (u4L2CxtId, (UINT4) u2VlanId,
                     &i4VlanAdminMacLearningStatus) == VLAN_FAILURE)
                {
                    return ARP_FAILURE;
                }
                if (i4VlanAdminMacLearningStatus == VLAN_ENABLED)
                {
                    if (ArpFsNpHwGetPortFromFdb
                        (u2VlanId, (UINT1 *) i1pHwAddr, &u4Port) == FNP_FAILURE)
                    {
                        return ARP_FAILURE;
                    }

                    u4PhyIfIndex = u4Port;
                }

                if (ArpFsNpIpv4VrfArpModify
                    (u4ContextId, u2VlanId, u4CfaIfIndex, u4PhyIfIndex,
                     u4Tproto_addr, (UINT1 *) i1pHwAddr, CfaIfInfo.au1IfName,
                     i1State) == OSIX_FALSE)
                {
                    return ARP_FAILURE;
                }
                /* Update the L2 physical index details in ARP cache entry. 
                 * This is required to refresh ARP cache entries, upon oper status
                 * change of a specific L2 physical port/LAG port.
                 */
                VlanGetFdbEntryDetails (u2VlanId, (UINT1 *) i1pHwAddr,
                                        &u2Port, &u1Status);
                if (u2Port != 0)
                {
                    pTmpCache->u4PhyIfIndex = u2Port;
                }

                ArpFsNpIpv4SyncVlanAndL3Info ();
            }
        }
        else if ((CfaIfInfo.u1IfType == CFA_ENET)
                 || (CfaIfInfo.u1IfType == CFA_PSEUDO_WIRE))
        {
            if (ArpFsNpL3Ipv4VrfArpModify
                (u4ContextId, u4CfaIfIndex, u4Tproto_addr,
                 (UINT1 *) i1pHwAddr, CfaIfInfo.au1IfName, i1State)
                == OSIX_FALSE)
            {
                return ARP_FAILURE;
            }
        }

        pTmpCache->u1HwStatus = NP_UPDATED;
        if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
            && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
        {
            ArpRedAddDynamicInfo (pTmpCache, ARP_RED_ADD_CACHE);
            ArpRedSendDynamicCacheInfo ();
        }
#else
        pTmpCache->u1HwStatus = NP_UPDATED;
#endif
    }

    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name             :  ArpNpDel                                         */
/* Description               :  Deletes ARP entry from the Fast Path Table       */
/*  Input(s)                 :  u4ContextId    -- Virtual Router ContextId       */
/*                              u4IpAddr   -- IpAddress                          */
/*                              i1pHwAddr  -- Hardware Address                   */
/*                              i1Hwalen   -- Hardware Address Length            */
/*                              u4IfIndex  -- Interface Index through which      */
/*                                             ARP entry is learnt               */
/*                              u1Flag  -- Flag Indicates whether need to    */
/*                                         inform RTM on successful deletion.*/
/*                              i1State -- ARP_STATIC / ARP_DYNAMIC.         */
/*  Output(s)                :  None                                             */
/*  Global Variables Referred :  None                                            */
/*  Global variables Modified :  None                                            */
/*  Exceptions                :  None                                            */
/*  Use of Recursion          :  None                                            */
/*  Returns                   :  ARP_FAILURE(-1) on failure                      */
/*                               ARP_SUCCESS(0) on success                       */
/*****************************************************************************/

INT4
ArpNpDel (UINT4 u4ContextId, UINT4 u4IpAddr,
          INT1 *i1pHwAddr, INT1 i1Hwalen, UINT4 u4CfaIfIndex, INT1 i1State)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pTmpCache = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IpForward;

    UNUSED_PARAM (i1pHwAddr);
    UNUSED_PARAM (i1Hwalen);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (i1State);

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4CfaIfIndex;
    ArpCache.u4Ip_addr = u4IpAddr;
    pTmpCache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
    if (pTmpCache == NULL)
    {
        return ARP_FAILURE;
    }

    if (ARP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        pTmpCache->u1HwStatus = NP_UPDATED;
        return ARP_SUCCESS;
    }

    MEMSET (&CfaIfInfo, ARP_ZERO, sizeof (tCfaIfInfo));
    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, CONTROL_PLANE_TRC,
                          ARP_NAME, "CfaGetIfInfo failed for ifIndex - 0x%x \n",
                          u4CfaIfIndex);
    }

    if (NetIpv4GetIpForwardingInCxt (u4ContextId, &u1IpForward)
        == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    if (u1IpForward == IP_FORW_DISABLE)
    {
        return ARP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    pTmpCache->u1HwStatus = NP_NOT_UPDATED;
    if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
        && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
    {
        ArpRedAddDynamicInfo (pTmpCache, ARP_RED_DEL_CACHE);
        ArpRedSendDynamicCacheInfo ();
    }

    if ((CfaIsMgmtPort (u4CfaIfIndex) == FALSE) &&
        (CfaIsLinuxVlanIntf (u4CfaIfIndex) == FALSE))
    {
        if ((CfaIfInfo.u1IfType == CFA_ENET) ||
            (CfaIfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
            (CfaIfInfo.u1IfType == CFA_L3IPVLAN))
        {
            ArpFsNpIpv4VrfArpDel (u4ContextId, u4IpAddr, CfaIfInfo.au1IfName,
                                  i1State);
        }
    }
    pTmpCache->u1HwStatus = NP_UPDATED;
    if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
        && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
    {
        ArpRedAddDynamicInfo (pTmpCache, ARP_RED_DEL_CACHE);
        ArpRedSendDynamicCacheInfo ();
    }
#else
    pTmpCache->u1HwStatus = NP_UPDATED;
#endif

    return ARP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  ArpNpGetNext                                */
/*  Description               :  Gets  the next ARP entry from the FPT       */
/*  Input(s)                  :  ArpInParam - The virtual router identifier, */
/*                                            IpAddress                      */
/*  Output(s)                 :  ArpOutParams - VRF Id                       */
/*                                            - Ip Address                   */
/*                                            - Cfa Index                    */
/*                                            - Hardware address             */
/*  Returns                   :  ARP_FAILURE on failure                      */
/*                               ARP_SUCCESS on success                      */
/*****************************************************************************/

INT4
ArpNpGetNext (tArpNpInParams ArpNpInParam, tArpNpOutParams * pArpNpOutParam)
{
    INT4                i4RetVal = ARP_FAILURE;

#ifdef NPAPI_WANTED
    tNpArpInput         ArpInput;
    tNpArpOutput        ArpOutput;

    MEMCPY (&ArpInput, &ArpNpInParam, sizeof (tArpNpInParams));
    MEMSET (&ArpOutput, 0, sizeof (tNpArpOutput));
    i4RetVal = ArpFsNpIpv4VrfArpGetNext (ArpInput, &ArpOutput);

    if (i4RetVal == OSIX_FALSE)
    {
        return ARP_FAILURE;
    }
    MEMCPY (pArpNpOutParam, &ArpOutput, sizeof (tNpArpOutput));
#endif
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);

    return i4RetVal;
}

/*****************************************************************************/
/*  Function Name             :  ArpNpGet                                    */
/*  Description               :  Gets ARP entry from the Fast Path Table     */
/*  Input(s)                  :  ArpInParam - The virtual router identifier, */
/*                                            IpAddress                      */
/*  Output(s)                 :  ArpOutParams - VRF Id                       */
/*                                            - Ip Address                   */
/*                                            - Cfa Index                    */
/*                                            - Hardware address             */
/*  Returns                   :  ARP_FAILURE on failure                      */
/*                               ARP_SUCCESS on success                      */
/*****************************************************************************/

INT4
ArpNpGet (tArpNpInParams ArpNpInParam, tArpNpOutParams * pArpNpOutParam)
{
#ifdef NPAPI_WANTED
    tNpArpInput         ArpInput;
    tNpArpOutput        ArpOutput;
    INT4                i4RetVal = ARP_SUCCESS;

    MEMCPY (&ArpInput, &ArpNpInParam, sizeof (tArpNpInParams));
    MEMSET (&ArpOutput, 0, sizeof (tNpArpOutput));
    i4RetVal = ArpFsNpIpv4VrfArpGet (ArpInput, &ArpOutput);

    if (i4RetVal == OSIX_FALSE)
    {
        return ARP_FAILURE;
    }
    MEMCPY (pArpNpOutParam, &ArpOutput, sizeof (tNpArpOutput));
#endif
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);

    return ARP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             : ArpProcessRcvdIpPkt                          */
/*  Description               : This function install the ARP/route to       */
/*                              hardware to drop the further data packets    */
/*                              at NP level/forward packets in Hardware      */
/*  Input(s)                  : pBuf - Received data packet                  */
/*  Output(s)                 : None                                         */
/*                                                                           */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : None                                         */
/*****************************************************************************/
VOID
ArpProcessRcvdIpPkt (UINT2 u2Port, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER         IpHdr;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4DestIp = ARP_ZERO;
#ifndef LNXIP4_WANTED
    t_ICMP              Icmp;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
#endif

    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, ARP_NAME,
                          "ArpProcessRcvdIpPkt: Obtaining interface information "
                          "for the port %d failed. n", u2Port);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    /* Extract the IP header information */
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpHdr, ARP_ZERO,
                                   CFA_IP_HDR_LEN) < CFA_IP_HDR_LEN)
    {
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                 DATA_PATH_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "Extracting IP Header failed \r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    u4DestIp = OSIX_NTOHL (IpHdr.u4Dest);

    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC,
                  ARP_NAME, "IP Packet received to destination for "
                  "resolution %x\r\n", u4DestIp);

    /* Trigger ARP resolution to reach the destination */
    ARP_PROT_UNLOCK ();
    if (RtmApiTriggerArpResolutionInCxt
        (NetIpIfInfo.u4ContextId, u4DestIp, pBuf) == IP_FAILURE)
    {
#ifndef LNXIP4_WANTED
        MEMSET (&Icmp, ARP_ZERO, sizeof (t_ICMP));
        Icmp.i1Type = ICMP_DEST_UNREACH;
        Icmp.i1Code = ICMP_NET_UNREACH;
        Icmp.args.u4Unused = ARP_ZERO;
        Icmp.u4ContextId = NetIpIfInfo.u4ContextId;
        /* The pTmpBuf Duplicated here will be released after processing
         * the Icmp Error message */
        pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);
        if (pTmpBuf != NULL)
        {
            IpGenerateIcmpErrorMesg (pTmpBuf, Icmp, FALSE);
        }
#endif
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        ArpFsCfaHwRemoveIpNetRcvdDlfInHash (NetIpIfInfo.u4ContextId, u4DestIp,
                                            0xffffffff);
#endif
    }
    ARP_PROT_LOCK ();
}

/*****************************************************************************/
/*  Function Name             : ArpInitiateAddressResolution                 */
/*  Description               : This function adds the ARP entry for the     */
/*                              IP address to be resolved                    */
/*  Input(s)                  : u4AddrtoResolve - Address to be resolved     */
/*                              u4IpIfIndex - IP Interface index             */
/*                              pBuf - Pending buffer to be sent after ARP   */
/*                                     is resolved                           */
/*  Output(s)                 : pi1ArpState -State of the ARP                */
/*                                                                           */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : None                                         */
/*****************************************************************************/
INT4
ArpInitiateAddressResolution (UINT4 u4AddrtoResolve,
                              UINT4 u4IpIfIndex, INT1 *pi1ArpState,
                              tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_ARP_CACHE         ArpCache;
    tARP_DATA           ArpData;
    UINT4               u4IfIndex = ARP_ZERO;
    UINT4               u4ContextId = ARP_ZERO;
    INT4                i4ArpLookUp = ARP_FAILURE;

    MEMSET (&ArpCache, ARP_ZERO, sizeof (t_ARP_CACHE));

    if (NetIpv4GetCfaIfIndexFromPort (u4IpIfIndex, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                          "ArpInitiateAddressResolution: Unable to obtain the interface "
                          "index for the port %d\n", u4IpIfIndex);
        return ARP_FAILURE;
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    i4ArpLookUp = ArpLookupWithIndex (u4IfIndex, u4AddrtoResolve, &ArpCache);

    if (i4ArpLookUp == ARP_FAILURE)
    {
        /* Add the ARP entry in Pending state .ARP Entry addition in 
         * pending state installs the drop route in Hardware */
        MEMSET (&ArpData, ARP_ZERO, sizeof (tARP_DATA));
        ArpData.i2Hardware = ARP_ENET_TYPE;
        ArpData.u4IpAddr = u4AddrtoResolve;
        ArpData.u2Port = (UINT2) u4IpIfIndex;
        ArpData.i1Hwalen = ARP_ZERO;
        MEMSET (ArpData.i1Hw_addr, ARP_ZERO, CFA_ENET_ADDR_LEN);
        ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
        ArpData.i1State = (INT1) ARP_PENDING;
        ArpData.u1RowStatus = (UINT1) ACTIVE;

        if (ArpAddArpEntry (ArpData, pBuf, ARP_CFA_MODULE_ID) != ARP_SUCCESS)
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                          "Arp Addition failed in pending state for %x.\n",
                          u4AddrtoResolve);
            return ARP_FAILURE;
        }
        ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                      "Arp Added in Pending State for %x.\t on interface %d\n",
                      u4AddrtoResolve, u4IpIfIndex);
        *pi1ArpState = (INT1) ARP_PENDING;
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        *pi1ArpState = ArpCache.i1State;
    }
    return ARP_SUCCESS;
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: ARPProcessDataPktEvent                                  */
 /*                                                                         */
 /*  Description  : Processes the data packet that are unable to be         */
 /*                 forwarded  in data plane.Based on detination of the     */
 /*                 packets, ARP is triggered to install ARP/Route entries  */
 /*                 in Hardware                                             */
 /*  Input(s)     : None.                                                   */
 /*                                                                         */
 /*  Output(s)    : None.                                                   */
 /*                                                                         */
 /*  Returns      : None                                                    */
 /***************************************************************************/
VOID
ARPProcessDataPktEvent (VOID)
{
    tArpQMsg           *pArpQMsg = NULL;

    while (OsixQueRecv (ARP_IP_PKT_Q_ID, (UINT1 *) &pArpQMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        ARP_PROT_LOCK ();
        switch (pArpQMsg->u4MsgType)
        {
            case IP_PKT_FOR_RESOLVE:
                ArpProcessRcvdIpPkt (pArpQMsg->u2Port, pArpQMsg->pBuf);
                break;

            default:
            {
                ARP_GBL_TRC (ARP_MOD_TRC,
                             ALL_FAILURE_TRC, ARP_NAME,
                             "Alert. Rx buffer from unknown module.\n");
                IP_RELEASE_BUF (pArpQMsg->pBuf, FALSE);
                break;
            }
        }
        MemReleaseMemBlock (ARP_Q_MSG_POOL_ID, (UINT1 *) pArpQMsg);
        ARP_PROT_UNLOCK ();
    }
}

 /***************************************************************************/
 /* Function           : ArpEnqueueIpPkt.                                   */
 /* Input(s)           :pRecvdQMsg - Pointer to the tArpQMsg structure.     */
 /* Output(s)          : None                                               */
 /* Returns            : ARP_SUCCESS, ARP_FAILURE                           */
 /* Action             :  Enqueues the packets to ARP for queueing it until */
/*                       address is resolved.                              */
 /***************************************************************************/
INT4
ArpEnqueueIpPkt (tArpQMsg * pRecvdQMsg)
{
    tArpQMsg           *pArpQMsg = NULL;
    UINT4               u4ContextId = 0;

    VcmGetContextInfoFromIpIfIndex ((UINT4) pRecvdQMsg->u2Port, &u4ContextId);
    if ((pArpQMsg = (tArpQMsg *) MemAllocMemBlk (ARP_Q_MSG_POOL_ID)) == NULL)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "Memory Allocation failure for ARP QMsg Entry.\n");
        return ARP_FAILURE;
    }
    MEMCPY (pArpQMsg, pRecvdQMsg, sizeof (tArpQMsg));

    if (OsixQueSend (ARP_IP_PKT_Q_ID,
                     (UINT1 *) (&pArpQMsg), OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (ARP_Q_MSG_POOL_ID, (UINT1 *) pArpQMsg);
        return ARP_FAILURE;
    }
    OsixEvtSend (ARP_TASK_ID, ARP_L3_DATA_PKT_EVENT);

    return ARP_SUCCESS;
}

#ifdef IP6_WANTED
/*****************************************************************************/
/*  Function Name             : IpGetIpv4AddrFromARPTable                    */
/*  Description               : This function returns the Ipv4 address       */
/*                              from ARP table for the given IfIndex         */
/*  Input(s)                  : pi4CfaIfIndex -CFA If Index.                 */
/*                              pu4IpAddr - IP address.                      */
/*  Output(s)                 : pu4IpAddr -Next IP address.                  */
/*                                                                           */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : IP_SUCCESS/IP_FAILURE                        */
/******************************************************************************/

INT4
IpGetIpv4AddrFromArpTable (INT4 i4CfaIfIndex, UINT4 *pu4IpAddr)
{
    INT4                i4CfaNxtIfIndex = 1;
    INT4                i4RetVal = ARP_FAILURE;
    UINT4               u4IpAddr = ARP_ZERO;
    UINT4               u4NxtIpAddr = ARP_ZERO;
    UINT4               u4ContextId = ARP_ZERO;

    VcmGetContextIdFromCfaIfIndex ((UINT4) i4CfaIfIndex, &u4ContextId);
    ARP_PROT_LOCK ();

    i4RetVal = ArpGetNextFromArpcacheTable (i4CfaIfIndex, &i4CfaNxtIfIndex,
                                            u4IpAddr, &u4NxtIpAddr);

    if (i4RetVal != ARP_SUCCESS)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "IpGetIpv4AddrFromArpTable: GetNext cache entry failed.\n");
        ARP_PROT_UNLOCK ();
        return IP_FAILURE;
    }

    if (i4CfaNxtIfIndex == i4CfaIfIndex)
    {
        *pu4IpAddr = u4NxtIpAddr;
        ARP_PROT_UNLOCK ();
        return IP_SUCCESS;
    }

    ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "IpGetIpv4AddrFromArpTable: No cache entry exists "
                  "for the index %d.\n", i4CfaIfIndex);

    ARP_PROT_UNLOCK ();
    return IP_FAILURE;
}
#endif
/***************************************************************************/
/* Function           : ArpEventSend                                       */
/* Input(s)           : u4Event- Event to be raised to ARP Task.           */
/* Output(s)          : None                                               */
/* Returns            : None                                               */
/* Action             : Gets the Event from NPAPI routine.                 */
/***************************************************************************/
VOID
ArpEventSend (UINT4 u4Event)
{
    OsixEvtSend (ARP_TASK_ID, u4Event);
}

/***************************************************************************/
/* Function           : ArpHandleContextCreate                             */
/* Description        : This function initializes the Arp context          */
/*                      information for the given context.                 */
/* Input(s)           : u4ContextId - Context to be created                */
/* Output(s)          : None                                               */
/* Returns            : ARP_SUCCESS / ARP_FAILURE                          */
/***************************************************************************/
INT4
ArpHandleContextCreate (UINT4 u4ContextId)
{
    tArpCxt            *pArpCxt = NULL;

    if (u4ContextId >= ARP_SIZING_CONTEXT_COUNT)
    {
        return ARP_FAILURE;
    }
    if (gArpGblInfo.apArpCxt[u4ContextId] != NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      ARP_NAME, "ArpHandleContextCreate: The given context %d"
                      " already exists in the arp task.\n", u4ContextId);
        return ARP_FAILURE;
    }
    /* Allocate memory for the context information entry */
    gArpGblInfo.apArpCxt[u4ContextId] = MemAllocMemBlk (ARP_CXT_POOL_ID);

    if (gArpGblInfo.apArpCxt[u4ContextId] == NULL)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "Memory Allocation failure for ARP Context Entry.\n");
        return ARP_FAILURE;
    }

    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];
    MEMSET (pArpCxt, ARP_ZERO, sizeof (tArpCxt));

    /* Initialize the default values for the
     * Arp configurations in the context */
    pArpCxt->Arp_config.i4Cache_timeout = ARP_DEF_CACHE_TIMEOUT;
    pArpCxt->Arp_config.i4Cache_pend_time = ARP_DEF_PEND_TIME;
    pArpCxt->Arp_config.i4Max_retries = ARP_DEF_REQ_RETRIES;
    pArpCxt->Arp_config.i4Flush_status = OSIX_FALSE;

    pArpCxt->u4ContextId = u4ContextId;

    return ARP_SUCCESS;
}

/***************************************************************************/
/* Function           : ArpHandleContextDelete                             */
/* Description        : This function deinitializes the Arp context        */
/*                      information for the given context.                 */
/* Input(s)           : u4ContextId - Context to be created                */
/* Output(s)          : None                                               */
/* Returns            : ARP_SUCCESS / ARP_FAILURE                          */
/***************************************************************************/
INT4
ArpHandleContextDelete (UINT4 u4ContextId)
{
    if (u4ContextId >= ARP_SIZING_CONTEXT_COUNT)
    {
        return ARP_FAILURE;
    }
    if (gArpGblInfo.apArpCxt[u4ContextId] == NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | OS_RESOURCE_TRC,
                      ARP_NAME, "Memory not allocated for the ARP Context \
                      Entry for the context - %d.\n", u4ContextId);
        return ARP_FAILURE;
    }

    if (gArpGblInfo.apArpCxt[u4ContextId] == NULL)
    {
        return ARP_FAILURE;
    }
    /* Setting ARP current context */
    gArpGblInfo.pArpCurrCxt = gArpGblInfo.apArpCxt[u4ContextId];
    nmhSetFsArpCacheTimeout (ARP_DEF_CACHE_TIMEOUT);
    nmhSetFsArpCachePendTime (ARP_DEF_PEND_TIME);
    nmhSetFsArpMaxRetries (ARP_DEF_REQ_RETRIES);
    /* Resetting ARP current context */
    ArpResetContext ();

    MEMSET (gArpGblInfo.apArpCxt[u4ContextId], ARP_ZERO, sizeof (tArpCxt));
    MemReleaseMemBlock (ARP_CXT_POOL_ID,
                        (UINT1 *) gArpGblInfo.apArpCxt[u4ContextId]);

    gArpGblInfo.apArpCxt[u4ContextId] = NULL;
    ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC, ARP_NAME,
                  "Memory allocated for the ARP Context \
                  Entry freed for the context - %d.\n", u4ContextId);

    return ARP_SUCCESS;
}

/****************************************************************************
* Function     : ArpVcmIsVRExist
*                                            
* Description  : This function verifies with the VCM task the existence of
*                 the given context Id.
*
* Input        : u4ContextId - Arp Context Id.            
*
* Output       : None.                        
*
* Returns      : SNMP_SUCCESS/SNMP_FAILURE   
*
***************************************************************************/
INT4
ArpVcmIsVRExist (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
    {
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;
}

/****************************************************************************
* Function     : ArpSetContext  
*                                            
* Description  : This function sets the current ARP context pointer for 
*                 the given context Id.
*
* Input        : u4ContextId - Arp Context Id.            
*
* Output       : None.                        
*
* Returns      : SNMP_SUCCESS/SNMP_FAILURE   
*
***************************************************************************/
INT4
ArpSetContext (UINT4 u4ContextId)
{
    if (ArpVcmIsVRExist (u4ContextId) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId >= ARP_SIZING_CONTEXT_COUNT)
    {
        return SNMP_FAILURE;
    }
    if (gArpGblInfo.apArpCxt[u4ContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    gArpGblInfo.pArpCurrCxt = gArpGblInfo.apArpCxt[u4ContextId];
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function     : ArpResetContext  
*                                            
* Description  : This function resets the current ARP context pointer  
*
* Input        : None.                                    
*
* Output       : None.                        
*
* Returns      : None.                           
*
***************************************************************************/
VOID
ArpResetContext (VOID)
{
    gArpGblInfo.pArpCurrCxt = NULL;
    return;
}

/****************************************************************************
* Function     : ArpGetDebugFlag  
*                                            
* Description  : This function returns the Arp Debug flag for the given
*                 context or returns the global arp debug flag if the 
*                 given context id is invalid context
*
* Input        : u4ContextId - context Id.
*
* Output       : None.                        
*
* Returns      : Debug Value.                    
*
***************************************************************************/
UINT4
ArpGetDebugFlag (UINT4 u4ContextId)
{
    if (u4ContextId == ARP_DEFAULT_CONTEXT)
    {
        return gArpGblInfo.u4ArpDbg;
    }

    if ((u4ContextId < ARP_SIZING_CONTEXT_COUNT) &&
        (gArpGblInfo.apArpCxt[u4ContextId] != NULL))
    {
        return gArpGblInfo.apArpCxt[u4ContextId]->u4ArpDbg;

    }
    return 0;
}

/****************************************************************************
* Function     : ArpSendGratArpRequest
*                                            
* Description  : This function sends out a gratuitous arp request message on 
*                 the given port, with the given sender IP and mac address.
*                 This function is invoked on receiving a Gratuitous request
*                 message from CFA when an l3 interface is made oper up.
*
* Input        : pArpQMsg - Arp message containing the port, ip and mac
*                            information.
*
* Output       : None.                        
*
* Returns      : ARP_SUCCESS/ARP_FAILURE         
*
***************************************************************************/
INT4
ArpSendGratArpRequest (tArpQMsg * pArpQMsg)
{
    t_ARP_PKT           ArpPkt;
    UINT4               u4IfIndex = 0;

    MEMSET (&ArpPkt, 0, sizeof (t_ARP_PKT));
    if (NetIpv4GetCfaIfIndexFromPort (pArpQMsg->u2Port, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }

    ArpPkt.u4Tproto_addr = pArpQMsg->u4IpAddr;
    ArpPkt.u4Sproto_addr = pArpQMsg->u4IpAddr;
    MEMSET (ArpPkt.i1Thwaddr, 0xff, CFA_ENET_ADDR_LEN);
    ArpPkt.i2Opcode = ARP_REQUEST;
    if (ArpGetHwAddr ((UINT2) u4IfIndex, (UINT1 *) ArpPkt.i1Shwaddr) !=
        ARP_SUCCESS)
    {
        return ARP_FAILURE;
    }

    if (ArpSendReqOrResp (pArpQMsg->u2Port, (UINT1) pArpQMsg->u4EncapType,
                          &ArpPkt) == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;
}
