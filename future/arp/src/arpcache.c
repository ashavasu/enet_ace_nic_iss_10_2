/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpcache.c,v 1.89 2017/12/20 11:18:45 siva Exp $
 *
 * Description:Contains functions to process ARP cache.
 *
 *******************************************************************/
#include "arpinc.h"
#include "arpred.h"
/*-------------------------------------------------------------------+
 * Function           : ArpAddArpEntry
 *
 * Input(s)           : Information needed to add ArpCache node
 *                      u4Tproto_addr, i2Hardware, i1pThwaddr, i1Hwalen,
 *                      u2Port, i1State
 *                      pDataBuffer - Pending data for the Destination
                                      u4Tproto_addr
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS/ARP_FAILURE.
 *
 * Action :
 *
 * Adds/modifies an entry in the ARP cache RBTree
 * If the entry is in the PENDING state and if there is any datagrams
 * waiting on queue, then it calls a procedure to send those datagrams.
+-------------------------------------------------------------------*/
INT4
ArpAddArpEntry (tARP_DATA tArpData, tCRU_BUF_CHAIN_HEADER * pDataBuff,
                UINT1 u1ModId)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pTmpCache = NULL;
    tArpCxt            *pArpCxt = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4ArpCheck = 0;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1EncapType = 0;
    INT1                i1CurState = 0;
    tCRU_BUF_CHAIN_HEADER *pPendBuff = NULL;    /* Pending data list of messages */
    INT4                i4Timeout = 0;
    UINT1               u1IfOperStatus = 0;

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) tArpData.u2Port, &u4IfIndex)
        != NETIPV4_SUCCESS)
    {
        if (pDataBuff != NULL)
        {
            ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                              "Releasing buffer %x.\n", pDataBuff);
            IP_RELEASE_BUF (pDataBuff, FALSE);
            pDataBuff = NULL;
        }
        return ARP_FAILURE;
    }
    i4RetVal = CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);
    if (i4RetVal == CFA_FAILURE)
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                          "Invalid Interface index %x.\n", pDataBuff);
        return ARP_FAILURE;

    }
    if ((u1IfOperStatus != CFA_IF_UP) && (tArpData.i1State == ARP_DYNAMIC))
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                          "Oper Status Down %x.\n", pDataBuff);
        return ARP_FAILURE;
    }

    if (NetIpv4GetIfInfo ((UINT4) tArpData.u2Port, &NetIpIfInfo) ==
        NETIPV4_FAILURE)
    {
        if (pDataBuff != NULL)
        {
            ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                              "Releasing buffer %x.\n", pDataBuff);
            IP_RELEASE_BUF (pDataBuff, FALSE);
            pDataBuff = NULL;
        }
        return ARP_FAILURE;
    }

    u4ContextId = NetIpIfInfo.u4ContextId;
    if (NetIpv4IfIsOurAddressInCxt (u4ContextId,
                                    tArpData.u4IpAddr) == NETIPV4_SUCCESS)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                      "Discarded ARP add request for our own address %x\r\n",
                      tArpData.u4IpAddr);
        if (pDataBuff != NULL)
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                          ARP_NAME, "Releasing buffer %x.\n", pDataBuff);
            IP_RELEASE_BUF (pDataBuff, FALSE);
            pDataBuff = NULL;
        }
        return ARP_FAILURE;
    }
    /* Since in linux IP environment, arp for loopback is learnt in control  *
     * plane and Arp Delete for the same is being triggered. This workaround *
     * is made to ignore the delete call since there will not be any entry in*
     * L3 host table, so it will override and delete the corresponding entry *
     * from route table */

#if ((!(defined (VRRP_WANTED))) && defined (LNXIP4_WANTED) && defined (NPAPI_WANTED))
    if (NetIpv4IpIsLocalNetInCxt (u4ContextId, tArpData.u4IpAddr) ==
        NETIPV4_FAILURE)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC, ARP_NAME,
                      "Discarded ARP add request for loopback address %x\r\n",
                      tArpData.u4IpAddr);
        return ARP_SUCCESS;
    }
#endif

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    i4ArpCheck = ArpLookupWithIndex (u4IfIndex, tArpData.u4IpAddr, &ArpCache);
    if (i4ArpCheck == ARP_SUCCESS)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC,
                      ARP_NAME, "ARP cache already has an entry for %x. \n",
                      tArpData.u4IpAddr);
        /* When interface is down,static ARP is in invalid state */
        if ((ArpCache.i1State == (INT1) ARP_STATIC) ||
            (ArpCache.i1State == (INT1) ARP_STATIC_NOT_IN_SERVICE) ||
            (ArpCache.i1State == (INT1) ARP_INVALID))
        {
            /* avoid dynamic entries overriding static entries. */
            if ((tArpData.i1State != (INT1) ARP_STATIC) &&
                (tArpData.i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE))
            {
                ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                              "Static Entry exist for %x. "
                              "Dynamic Can't Override.\n", tArpData.u4IpAddr);
                if (pDataBuff != NULL)
                {
                    ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                                  BUFFER_TRC, ARP_NAME,
                                  "Releasing buffer %x.\n", pDataBuff);
                    IP_RELEASE_BUF (pDataBuff, FALSE);
                    pDataBuff = NULL;
                }
                return ARP_FAILURE;
            }
        }
        else if ((ArpCache.i1State == (INT1) ARP_DYNAMIC) &&
                 (tArpData.i1State == (INT1) ARP_PENDING))
        {
            if (pDataBuff != NULL)
            {
                IP_RELEASE_BUF (pDataBuff, FALSE);
                pDataBuff = NULL;
            }
            return ARP_FAILURE;
        }
        i1CurState = ArpCache.i1State;
    }

    /*  Get the FreeCache Entry and Update it for Addition */
    if ((pTmpCache = (t_ARP_CACHE *) MemAllocMemBlk (ARP_POOL_ID)) == NULL)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "Memory Allocation failure for ARP cache Entry.\n");
        if (pDataBuff != NULL)
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                          ARP_NAME, "Releasing buffer %x.\n", pDataBuff);
            IP_RELEASE_BUF (pDataBuff, FALSE);
            pDataBuff = NULL;
        }
        return ARP_FAILURE;
    }

    MEMSET (pTmpCache, 0, sizeof (t_ARP_CACHE));

    /* the incoming interface id is mapped to the context. Hence, the 
     * ArpCxt is a valid node */
    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];

    if (pArpCxt == NULL)
    {
        if (pDataBuff != NULL)
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                          ARP_NAME, "Releasing buffer %x.\n", pDataBuff);
            IP_RELEASE_BUF (pDataBuff, FALSE);
            pDataBuff = NULL;
        }
        MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pTmpCache);
        return ARP_FAILURE;
    }

    /* Select the timer to use */
    if ((tArpData.i1State == ARP_PENDING))
    {
#ifdef LNXIP4_WANTED
        if (u1ModId == ARP_CFA_MODULE_ID)
        {
            /*We are triggering ARP w.r.t the IP data packet received.
             * In Linux IP,LINUX ARP is sending ARP for data packet received..
             * So just start the timer to delete the drop entry added in pending 
             * if the  destination is unreachable */
            i4Timeout = ARP_REQUEST_RETRY_TIMEOUT *
                pArpCxt->Arp_config.i4Max_retries;
        }
        else
        {
            i4Timeout = ARP_REQUEST_RETRY_TIMEOUT;
        }
#else
        i4Timeout = ARP_REQUEST_RETRY_TIMEOUT;
#endif
    }
    else if ((tArpData.i1State == ARP_AGEOUT))
    {
        i4Timeout = ARP_REQUEST_RETRY_TIMEOUT;
    }
    else
    {
        i4Timeout = pArpCxt->Arp_config.i4Cache_timeout;
    }

    pTmpCache->i2Hardware = tArpData.i2Hardware;
    pTmpCache->u4Ip_addr = tArpData.u4IpAddr;
    pTmpCache->u2Port = tArpData.u2Port;
    pTmpCache->i1Hwalen = tArpData.i1Hwalen;
    pTmpCache->i1Palen = ARP_PROTOCOL_ADDR_LEN;
    pTmpCache->u4IfIndex = u4IfIndex;

    u1EncapType = NetIpIfInfo.u1EncapType;

    if (u1EncapType == ARP_ENET_AUTO_ENCAP)
    {
        pTmpCache->u1EncapType = tArpData.u1EncapType;
    }
    else
    {
        pTmpCache->u1EncapType = u1EncapType;
    }

    if ((tArpData.i1State == ARP_PENDING) || (tArpData.i1State == ARP_AGEOUT))
    {
        pTmpCache->Timer.u1Id = ARP_REQUEST_RETRY_TIMER_ID;
    }
    else
    {
        pTmpCache->Timer.u1Id = ARP_CACHE_TIMER_ID;
    }

    pTmpCache->Timer.pArg = (VOID *) pTmpCache;
    OsixGetSysTime ((tOsixSysTime *) & pTmpCache->u4Time);
    pTmpCache->i4Retries = 0;
    pTmpCache->i1State = tArpData.i1State;
    pTmpCache->u1RowStatus = tArpData.u1RowStatus;
    pTmpCache->pArpCxt = pArpCxt;

    if (tArpData.i1State != ARP_PENDING)
    {
        MEMCPY (pTmpCache->i1Hw_addr, tArpData.i1Hw_addr, tArpData.i1Hwalen);
    }

    /* No timeout for static entries */
    /* For entries that have been already learnt the timer will be 
     * refreshed with ArpReplaceArpCacheWithIndex */
    if ((tArpData.i1State != (INT1) ARP_STATIC) &&
        (tArpData.i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE) &&
        (tArpData.i1State != (INT1) ARP_INVALID) && (i4ArpCheck != ARP_SUCCESS))
    {
        /* when ARP entires are learnt through the netlink notification,
         * the timer should be started only in the ageout state
         * thus this function is used to check
         * whether entry is in ageout state and start the timer.
         */
        ArpCheckAndStartTimer (pTmpCache, (UINT4) i4Timeout);
    }

    /* As the interface is down,add the arp entry as invalid */
    if ((NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE) &&
        ((tArpData.i1State == ARP_STATIC) || (tArpData.i1State == ARP_INVALID)))
    {
        pTmpCache->i1State = ARP_INVALID;
    }

    pTmpCache->pPending = pDataBuff;

    if (i4ArpCheck == ARP_FAILURE)
    {
        /* Add the Cache Entry in ARPCache Table */
        if (tArpData.i1State == ARP_PENDING)
        {
#ifdef LNXIP4_WANTED
            if (u1ModId == ARP_CFA_MODULE_ID)
            {
                /*We are triggering ARP w.r.t the IP data packet received.
                 * In Linux IP,LINUX ARP is sending ARP for data packet 
                 * received.. */
                pTmpCache->i4Retries = pArpCxt->Arp_config.i4Max_retries;
            }
            else
            {
                pTmpCache->i4Retries = 1;
            }
#else
            pTmpCache->i4Retries = 1;
#endif
        }

        if (ArpAddArpCache (pTmpCache) != ARP_SUCCESS)
        {
            if (pDataBuff != NULL)
            {
                ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                              ARP_NAME, "Releasing buffer %x.\n", pDataBuff);
                IP_RELEASE_BUF (pDataBuff, FALSE);
                pDataBuff = NULL;
            }
            return ARP_FAILURE;
        }

        if (tArpData.i1State == ARP_PENDING)
        {
            if (pTmpCache->i4Retries != pArpCxt->Arp_config.i4Max_retries)
            {
                ArpCheckAndSendReq (tArpData.i1State, tArpData.u2Port,
                                    tArpData.u4IpAddr, tArpData.i2Hardware,
                                    tArpData.u1EncapType);
            }
        }

        /* Add the ARP Entry to NP.If the operstate of the interface
         * over which static ARP Entry exists is down, dont add to H/W */
        if ((pTmpCache->i1State != ARP_INVALID) &&
            (pTmpCache->i1State != ARP_STATIC_NOT_IN_SERVICE))
        {
            if (CfaIsStackVlanIntf (u4IfIndex) == CFA_FAILURE)
            {
                if (ArpNpAdd (NetIpIfInfo.u4ContextId,
                              tArpData.u4IpAddr,
                              tArpData.i2Hardware, tArpData.i1Hw_addr,
                              tArpData.i1Hwalen, u4IfIndex,
                              tArpData.u1EncapType,
                              tArpData.i1State) != ARP_SUCCESS)
                {
                    ARP_TRC (u4ContextId, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                             "ARP entry addition in Fast Path fails\n");
                    arp_drop (pTmpCache);
                    return ARP_FAILURE;
                }
                /* The updated/added/deleted entry is added to the global 
                 * RedRBtree. This tree is scanned for sending dynamic updates. 
                 * In case of Async NP calls, the HwStatus is set as 
                 * NP_NOT_UPDATED before writing to the NP. And the change is 
                 * synced with the standby node. When the NP call is success, 
                 * the state is changed to NP_UPDATED and the new state is 
                 * synced as part of the NP calls return value. In case of 
                 * sequential NP calls, entry is synced only when the NP call
                 * returns success. So there is no need to sync the HwStatus
                 * before and after writing to NP. */

                if ((pTmpCache->i1State == ARP_STATIC) ||
                    (pTmpCache->i1State == ARP_DYNAMIC))
                {
                    ARP_PROT_UNLOCK ();
                    RtmApiHandlePRTRoutesForArpUpdationInCxt
                        (pArpCxt->u4ContextId, tArpData.u4IpAddr,
                         ARP_ADDITION, pTmpCache->i1State);
                    ARP_PROT_LOCK ();
                }
            }
        }

    }
    else
    {
        if (tArpData.i1State == ARP_AGEOUT)
        {
            pTmpCache->i4Retries = 1;
        }
        /* Replace the Existing ArpCache Entry and get the 
           Pending data for the Cache if any (pPendBuff) */

        /* ArpReplaceArpCacheWithIndex - u4IfIndex & Mac Addr from Arpdata will 
         * be compared with the ArpCache and New entry will be updated in RBTree
         * if change is observed.
         */
        i4RetVal = ArpReplaceArpCacheWithIndex (u4IfIndex, tArpData.u4IpAddr,
                                                pTmpCache, &pPendBuff,
                                                i4Timeout);
        if (i4RetVal != ARP_SUCCESS)
        {
            if (pDataBuff != NULL)
            {
                ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                              ARP_NAME, "Releasing buffer %x.\n", pDataBuff);
                IP_RELEASE_BUF (pDataBuff, FALSE);
                pDataBuff = NULL;
            }
            return i4RetVal;
        }
        /* In case of Linux IP, we should trigger ARP only when ageout occurs
         * to refresh the ARP entry. In pending state, Linux will be triggering
         * ARP */
        if (tArpData.i1State == ARP_AGEOUT)
        {
            /* Send ARP request if added entry is a Pending entry */
            ArpCheckAndSendReq (tArpData.i1State, tArpData.u2Port,
                                tArpData.u4IpAddr, tArpData.i2Hardware,
                                tArpData.u1EncapType);
        }
        /* ARP entry exists already in slow path ARP cache. Hence update \
         * ARP entry in Fast Path if entry exists in Fast Path also, \
         * otherwise create an entry. An ARP entry can exist in Slow path \
         * but not in fast path if the entry state is "ARP_PENDING".*/

        if ((tArpData.i1State == ARP_DYNAMIC) ||
            ((tArpData.i1State == ARP_STATIC) &&
             (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)) ||
            ((tArpData.i1State == ARP_AGEOUT) &&
             (MEMCMP (tArpData.i1Hw_addr, ArpCache.i1Hw_addr, tArpData.i1Hwalen)
              != 0)))

        {
            /* Program the Arp entry in the hardware only when the
             * contents of the Arp entry has changed
             * And the state of the ArpEntry is AGEOUT 
             * NP Program should be called during AGEOUT to have NP in sync with Control plane 
             * when there is a change in MAC Addr or IfIndex
             */
            if ((MEMCMP (tArpData.i1Hw_addr, ArpCache.i1Hw_addr,
                         tArpData.i1Hwalen) != 0)
                || (tArpData.u2Port != ArpCache.u2Port)
                || (tArpData.u1EncapType != ArpCache.u1EncapType))

            {
                if (CfaIsStackVlanIntf (u4IfIndex) == CFA_FAILURE)
                {
                    if ((ArpNpModify (NetIpIfInfo.u4ContextId,
                                      tArpData.u4IpAddr, tArpData.i2Hardware,
                                      tArpData.i1Hw_addr, tArpData.i1Hwalen,
                                      u4IfIndex,
                                      tArpData.u1EncapType,
                                      tArpData.i1State)) != ARP_SUCCESS)
                    {
                        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                                 "ARP entry updation fails\n");
                        if (ArpDeleteArpCacheWithIndex
                            (u4IfIndex, tArpData.u4IpAddr) != ARP_SUCCESS)
                        {
                            IP_RELEASE_BUF (pDataBuff, FALSE);
                        }
                        return ARP_FAILURE;
                    }
                }

                /* The updated/added/deleted entry is added to the global 
                 * RedRBtree. This tree is scanned for sending dynamic updates. 
                 * In case of Async NP calls, the HwStatus is set as 
                 * NP_NOT_UPDATED before writing to the NP. And the change is 
                 * synced with the standby node. When the NP call is success, 
                 * the state is changed to NP_UPDATED and the new state is 
                 * synced as part of the NP calls return value. In case of 
                 * sequential NP calls, entry is synced only when the NP call
                 * returns success. So there is no need to sync the HwStatus
                 * before and after writing to NP. */
            }
            /* ArpReplaceArpCacheWithIndex will reset the hardware status.
             * So the hardware status need to be updated in the case of
             * fast path unmodified hardware ARP entry so that fast path
             * deletion can be handled appropriately.*/
            pTmpCache->u1HwStatus = NP_UPDATED;
            if ((ARP_GET_NODE_STATUS () == RM_ACTIVE)
                && (ARP_IS_STANDBY_UP () == OSIX_TRUE))
            {
                ArpRedAddDynamicInfo (pTmpCache, ARP_RED_ADD_CACHE);
                ArpRedSendDynamicCacheInfo ();
            }
        }
        /* Updation for  the Existing ARP with PENDING State to the new 
         * state ARP_DYNAMIC/ARP_STATIC */
        if ((i1CurState == ARP_PENDING) &&
            ((tArpData.i1State == ARP_DYNAMIC) ||
             ((tArpData.i1State == ARP_STATIC)
              && (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE))))
        {
            ARP_PROT_UNLOCK ();
            RtmApiHandlePRTRoutesForArpUpdationInCxt (pArpCxt->u4ContextId,
                                                      tArpData.u4IpAddr,
                                                      ARP_ADDITION,
                                                      tArpData.i1State);
            ARP_PROT_LOCK ();
        }

        /* Send out any pending IP packet */
        if (pPendBuff != NULL)
        {
            switch (u1ModId)
            {
                case IP_MODULE_ID:
                default:
                    if (CfaHandlePktFromIp (pPendBuff, u4IfIndex,
                                            tArpData.u4IpAddr,
                                            (UINT1 *) (tArpData.i1Hw_addr),
                                            IP_PROTOCOL, IP_UCAST,
                                            tArpData.u1EncapType) !=
                        CFA_SUCCESS)
                    {

                        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                                 OS_RESOURCE_TRC, ARP_NAME,
                                 "Enqueue to Lower Layer Failed.\n");

                        IP_RELEASE_BUF (pPendBuff, FALSE);
                    }
            }                    /* End of Switch */
            pPendBuff = NULL;
        }
    }
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : arp_drop
 *
 * Input(s)           : pArg 
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Deletes a cache entry
 * May be called during timeout or in response to delete command.
 *
+-------------------------------------------------------------------*/
VOID
arp_drop (VOID *pArg)
{
    t_ARP_CACHE        *pArp_cache = (t_ARP_CACHE *) pArg;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpBasicInfo       ArpBasicInfo;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4ContextId = 0;
    INT1                i1Hwalen = 0;
    INT1                ai1HwAddr[CFA_ENET_ADDR_LEN];
    INT1                i1State = 0;
    UINT1               u1ProtoId = 0;

#ifdef IP_WANTED
    tCRU_BUF_CHAIN_HEADER *pTmp_Buf = NULL;
    t_ICMP              Icmp;
#endif

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&ArpBasicInfo, 0, sizeof (tArpBasicInfo));

    u4ContextId = pArp_cache->pArpCxt->u4ContextId;
    if ((pArp_cache->i1State != (INT1) ARP_STATIC) &&
        (pArp_cache->i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE))
    {
        TmrStopTimer (ARP_TIMER_LIST_ID, &(pArp_cache->Timer.Timer_node));
    }

#ifdef IP_WANTED
    if (pArp_cache->pPending != NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      BUFFER_TRC, ARP_NAME, "Releasing Pending ARP buffer.\n",
                      pArp_cache->pPending);
        pTmp_Buf = pArp_cache->pPending;

        MEMSET (&Icmp, 0, sizeof (t_ICMP));
        Icmp.i1Type = ICMP_DEST_UNREACH;
        Icmp.i1Code = ICMP_HOST_UNREACH;
        Icmp.u4ContextId = u4ContextId;

        IpGenerateIcmpErrorMesg (pTmp_Buf, Icmp, FALSE);
        pArp_cache->pPending = NULL;
    }
#endif /* IP_WANTED */

    ARP_TRC (u4ContextId, ARP_MOD_TRC, OS_RESOURCE_TRC,
             ARP_NAME, "ARP Cache Entry freed.\n");

    /* If the Entry is deleted in Pending State,Decrement
       the corresponding context pending counter */
    if ((pArp_cache->i1State == ARP_PENDING) ||
        (pArp_cache->i1State == ARP_AGEOUT))
    {
        pArp_cache->pArpCxt->u4PendingEntryCount--;
    }

    if (pArp_cache->pArpCxt->u4ArpEntryCount != 0)
    {
        /* Decrement the corresponding context ArpEntry Count */
        pArp_cache->pArpCxt->u4ArpEntryCount--;
    }
    i1State = pArp_cache->i1State;
    u4IpAddr = pArp_cache->u4Ip_addr;
    MEMCPY (ai1HwAddr, pArp_cache->i1Hw_addr, pArp_cache->i1Hwalen);
    i1Hwalen = pArp_cache->i1Hwalen;
    u4IfIndex = pArp_cache->u4IfIndex;

    /* The updated/added/deleted entry is added to the global RedRBtree.
     * This tree is scanned for sending dynamic updates.
     * In case of Async NP calls, the HwStatus is set as NP_NOT_UPDATED
     * before writing to the NP. And the change is synced with the standby
     * node. When the NP call is success, the state is changed to NP_UPDATED
     * and the new state is synced as part of the NP calls return value.
     * In case of sequential NP calls, entry is synced only when the NP
     * call returns success. So there is no need to sync the HwStatus
     * before and after writing to NP. */

    for (u1ProtoId = 0; u1ProtoId < ARP_MAX_REG_MODULES; u1ProtoId++)
    {
        if (gaArpRegTbl[u1ProtoId].pArpBasicInfo != NULL)
        {
            ArpBasicInfo.u4IpAddr = pArp_cache->u4Ip_addr;
            MEMCPY (ArpBasicInfo.MacAddr, (UINT1 *) pArp_cache->i1Hw_addr,
                    sizeof (tMacAddr));

            ArpBasicInfo.u1Action = ARP_CB_REMOVE_ENTRY;

            (gaArpRegTbl[u1ProtoId].pArpBasicInfo) (&ArpBasicInfo);

        }
    }
#if defined (LNXIP4_WANTED)
    /* Delete the entry in the kernel arp cache if the NETLINK
     * approach is not enabled. In NETLINK approach, kernel
     * entry is deleted by itself during age-out and then
     * sends the notification to user space.
     */
    if (NetIpv4GetIfInfo ((UINT4) pArp_cache->u2Port, &NetIpIfInfo)
        == NETIPV4_SUCCESS)
    {
        ArpUpdateKernelEntry (ARP_INVALID, (UINT1 *) pArp_cache->i1Hw_addr,
                              pArp_cache->i1Hwalen,
                              pArp_cache->u4Ip_addr, NetIpIfInfo.au1IfName);
    }
#endif /* LNXIP4_WANTED */

    /*Before Deleting the Arp in Hardware we need to handle the 
     *routes which is associated with the Arp which is going for
     *deletion*/

    ARP_DS_UNLOCK ();

    /* ARP entry is agedout/deleted in pending state. So delete ARP entry from
     * Fast Path ARP Cache also*/
    if ((i1State != ARP_INVALID) && (i1State != ARP_STATIC_NOT_IN_SERVICE))
    {
        /* Act on the routes for ARP deletion  */
        ARP_PROT_UNLOCK ();
        if (RtmApiHandlePRTRoutesForArpUpdationInCxt (u4ContextId, u4IpAddr,
                                                      ARP_DELETION,
                                                      i1State) == IP_FAILURE)
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     ARP_NAME, "Route updation failed during ARP deletion\n");
        }
        ARP_PROT_LOCK ();

    }

    ARP_DS_LOCK ();

    /*After handling the routes associated with the ARP
     *we are deletiing the Arp in the Hardware*/
    if ((i1State != ARP_INVALID) && (i1State != ARP_STATIC_NOT_IN_SERVICE))
    {
        if ((CfaIsStackVlanIntf (u4IfIndex) == CFA_FAILURE) &&
            (pArp_cache->u1HwStatus == NP_UPDATED))
        {
            if ((ArpNpDel
                 (u4ContextId, u4IpAddr, ai1HwAddr, i1Hwalen, u4IfIndex,
                  i1State)) != ARP_SUCCESS)
            {
                ARP_TRC (u4ContextId, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         ARP_NAME, "ARP entry deletion in Fast Path Fails\n");
                return;
            }

        }
    }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
    if ((pArp_cache->i1State == (INT1) ARP_PENDING) ||
        (pArp_cache->i1State == (INT1) ARP_AGEOUT))
    {
        ArpFsCfaHwRemoveIpNetRcvdDlfInHash (u4ContextId, u4IpAddr, 0xffffffff);
    }
#endif

    RBTreeRem (gArpGblInfo.ArpTable, pArp_cache);
    TmrStopTimer (ARP_TIMER_LIST_ID, &(pArp_cache->Timer.Timer_node));
    MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArp_cache);
}

/*-------------------------------------------------------------------+
 * Function           : ArpModifyWithIndex
 *
 * Input(s)           : u4IfIndex, u4Ip_Addr, i1State
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS / ARP_FAILURE.
 *
 * Action :
 *
 * This procedure modifies the state of the ARP cache entry or deletes
 * the ARP cache entry added for the given IP address learnt in the given 
 * interface. The procedure is called from set routines of
 * arp_mgmt.c.
+-------------------------------------------------------------------*/
INT4
ArpModifyWithIndex (UINT4 u4IfIndex, UINT4 u4Ip_Addr, INT1 i1State)
{
    UINT4               u4ContextId;
    if (i1State == ARP_INVALID)
    {
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
        ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC, MGMT_TRC, ARP_NAME,
                      "Deleting ARP entry for %x in the interface %d\n.",
                      u4Ip_Addr, u4IfIndex);
        if (ArpDeleteArpCacheWithIndex (u4IfIndex, u4Ip_Addr) == ARP_FAILURE)
        {
            return ARP_FAILURE;
        }

        return ARP_SUCCESS;
    }
    ArpUpdateArpCacheStateWithIndex (u4IfIndex, u4Ip_Addr, i1State);
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpRBTreeCacheEntryCmp 
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      ARP_RB_GREATER if pRBElem >  pRBElemIn
 *                      ARP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two cache entries in lexicographic
 * order of the indices of the cache entry.
+-------------------------------------------------------------------*/

INT4
ArpRBTreeCacheEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    t_ARP_CACHE        *pArpCache = pRBElem;
    t_ARP_CACHE        *pArpCacheIn = pRBElemIn;

    if (pArpCache->u4IfIndex < pArpCacheIn->u4IfIndex)
    {
        return ARP_RB_LESSER;
    }
    else if (pArpCache->u4IfIndex > pArpCacheIn->u4IfIndex)
    {
        return ARP_RB_GREATER;
    }

    if (pArpCache->u4Ip_addr < pArpCacheIn->u4Ip_addr)
    {
        return ARP_RB_LESSER;
    }
    else if (pArpCache->u4Ip_addr > pArpCacheIn->u4Ip_addr)
    {
        return ARP_RB_GREATER;
    }

    return ARP_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : ArpActOnL2IfDelete                                   */
/*                                                                           */
/* Description        : Function handler to refresh ARP entries while FDB    */
/*                      entries are deleted upon OPER status of L2 physical  */
/*                      port/LAG index.                                      */
/*                      (i) Scan the ARP table                               */
/*                      (ii) Flush the ARP entries corresponding to the      */
/*                           L2 physical index/LAG index specified as input. */
/*                                                                           */
/* Input(s)           : u4IfIndex - IVR interface index, corresponding to the*/
/*                      VLAN entry, specified in the recently deleted FDB    */
/*                      entry, due to OPER down.                             */
/*                      u4PhyIndex - Physical port index/LAG index,          */
/*                      corresponding to the destination port, as specified  */
/*                      in the recently deleted FDB entry, as a result of    */
/*                      OPER down.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ArpActOnL2IfDelete (UINT4 u4IfIndex, UINT4 u4PhyIfIndex)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pArpNextCache = NULL;
    t_ARP_CACHE         ArpCache;
#ifdef TRACE_WANTED
    UINT4               u4ContextId = 0;
#endif
    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));

    ARP_DS_LOCK ();
    ArpCache.u4IfIndex = u4IfIndex;

    pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                               (tRBElem *) & ArpCache, NULL);
    while (pArpCache != NULL)
    {
        if (pArpCache->u4IfIndex > u4IfIndex)
        {
            break;
        }
        if (pArpCache->u4PhyIfIndex == u4PhyIfIndex)
        {
            ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC, ARP_NAME,
                          "Deleting ARP entry corresponding to IVR interface %d and \
                       the port %d\r\n", u4IfIndex, u4PhyIfIndex);
            arp_drop (pArpCache);
        }

        /* Trigger ARP request to learn ARP with new L2 entry in case of MC-LAG interface */
        ArpSendRequest (pArpCache->u2Port,
                        pArpCache->u4Ip_addr,
                        pArpCache->i2Hardware, pArpCache->u1EncapType);

        pArpNextCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);
        pArpCache = pArpNextCache;
    }                            /* End of while */
    ARP_DS_UNLOCK ();
    return;
}

/***************************************************************************/
/*  Function Name: ArpActOnIfDelete                                        */
/*  Description  : Deletes all the entries from ARP cache with the port    */
/*                 matching with the port passed.                          */
/*  Input(s)     : u2Port - ip port number                                 */
/*  Output(s)    : None.                                                   */
/*  Returns      : None.                                                   */
/***************************************************************************/
VOID
ArpActOnIfDelete (UINT4 u4IfIndex)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pArpNextCache = NULL;
    t_ARP_CACHE         ArpCache;

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ARP_DS_LOCK ();
    ArpCache.u4IfIndex = u4IfIndex;

    pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                               (tRBElem *) & ArpCache, NULL);

    while (pArpCache != NULL)
    {
        if (pArpCache->u4IfIndex > u4IfIndex)
        {
            break;
        }
        pArpNextCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

        arp_drop (pArpCache);
        pArpCache = pArpNextCache;
    }
    ARP_DS_UNLOCK ();
    return;
}

/***************************************************************************/
/*  Function Name: ArpActOnIfOperChg                                       */
/*  Description  : Acts on the ARP Cache Entries for the Interface         */
/*                 OperState Change                                        */
/*  Input(s)     : u2Port - ip port number                                 */
/*  Output(s)    : None.                                                   */
/*  Returns      : None.                                                   */
/***************************************************************************/
VOID
ArpActOnIfOperChg (UINT4 u4IfIndex, UINT2 u2Port, UINT4 u4OperStatus)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pArpNextCache = NULL;
    t_ARP_CACHE         ArpCache;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return;
    }

    ARP_DS_LOCK ();

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4IfIndex;

    pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                               (tRBElem *) & ArpCache, NULL);

    while (pArpCache != NULL)
    {
        if (pArpCache->u4IfIndex > u4IfIndex)
        {
            break;
        }
        pArpNextCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

        /* When interface  goes down, state of STATIC ARP is made
         * INVALID.We should act on those arp entries as soon as
         * interface comes UP */
        if ((pArpCache->i1State == ARP_STATIC) ||
            (pArpCache->i1State == ARP_INVALID))
        {
            ArpHandleOperStateChgForStaticArp (pArpCache,
                                               u4OperStatus,
                                               NetIpIfInfo.au1IfName);
            if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
            {
                OsixGetSysTime ((tOsixSysTime *) &
                                (pArpCache->u4LastUpdatedTime));
                ArpRedSendDynamicCacheTimeInfo (pArpCache);
            }
        }
        else if (pArpCache->i1State != ARP_STATIC_NOT_IN_SERVICE)
        {
            if (u4OperStatus == IPIF_OPER_DISABLE)
            {
                arp_drop (pArpCache);
            }
        }

        if ((u4OperStatus == IPIF_OPER_ENABLE) &&
            (pArpCache->u4Ip_addr == NetIpIfInfo.u4Addr))
        {
            arp_drop (pArpCache);
        }

        pArpCache = pArpNextCache;
    }
    ARP_DS_UNLOCK ();

    return;
}

/***************************************************************************/
/*  Function Name: ArpHandleOperStateChgForStaticArp                       */
/*  Description  : Acts on the STATIC ARP  Entries for the Interface       */
/*                 OperState Change                                        */
/*  Input(s)     : pArpCache -ARP Cache                                    */
/*                 u4OperState - Interface OperStatus                      */
/*  Output(s)    : None.                                                   */
/*  Returns      : None.                                                   */
/***************************************************************************/
VOID
ArpHandleOperStateChgForStaticArp (t_ARP_CACHE * pArpCache,
                                   UINT4 u4OperState, UINT1 *pu1IfName)
{
    /* Add/Delete the static entries only from Linux  Kernel & NP based
     * on oper status */
    if (u4OperState == IPIF_OPER_ENABLE)
    {
        if (pArpCache->i1State == ARP_INVALID)
        {
            pArpCache->i1State = ARP_STATIC;
            ARP_DS_UNLOCK ();
            RtmApiHandlePRTRoutesForArpUpdationInCxt (pArpCache->pArpCxt->
                                                      u4ContextId,
                                                      pArpCache->u4Ip_addr,
                                                      ARP_ADDITION,
                                                      pArpCache->i1State);
            ARP_DS_LOCK ();
            if (CfaIsStackVlanIntf (pArpCache->u4IfIndex) == CFA_FAILURE)
            {
                if (ArpNpAdd (pArpCache->pArpCxt->u4ContextId,
                              pArpCache->u4Ip_addr, pArpCache->i2Hardware,
                              pArpCache->i1Hw_addr, pArpCache->i1Hwalen,
                              pArpCache->u4IfIndex, pArpCache->u1EncapType,
                              ARP_STATIC) == ARP_FAILURE)
                {
                    ARP_TRC (pArpCache->pArpCxt->u4ContextId, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                             "ARP entry deletion in Fast Path Fails\n");
                }
            }
        }
    }
    else
    {
        if (pArpCache->i1State == ARP_STATIC)
        {
            if (CfaIsStackVlanIntf (pArpCache->u4IfIndex) == CFA_FAILURE)

            {
                /* Delete the static entries only from Linux  Kernel & NP. */
                if ((ArpNpDel (pArpCache->pArpCxt->u4ContextId,
                               pArpCache->u4Ip_addr, pArpCache->i1Hw_addr,
                               pArpCache->i1Hwalen, pArpCache->u4IfIndex,
                               ARP_STATIC)) != ARP_SUCCESS)
                {

                    ARP_TRC (pArpCache->pArpCxt->u4ContextId, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                             "ARP entry deletion in Fast Path Fails\n");
                }
            }
            pArpCache->i1State = ARP_INVALID;
        }
    }

#if defined (LNXIP4_WANTED)
    /* If the OPER is down,ARP entries on that interface are
     * deleted by KERNEL(even static ARP entries). So no need to
     * update the KERNEL */
    if (u4OperState == IPIF_OPER_ENABLE)
    {
        ArpUpdateKernelEntry (ARP_STATIC, (UINT1 *) pArpCache->i1Hw_addr,
                              pArpCache->i1Hwalen, pArpCache->u4Ip_addr,
                              pu1IfName);
    }
#else /* LNXIP4_WANTED */
    UNUSED_PARAM (pu1IfName);
#endif
}

/***************************************************************************/
/*  Function Name: ArpActOnStaticRtChange                                  */
/*  Description  : Flush the ARP Cache Entries learnt because of directly  */
/*                 connected static routes                                 */
/*                 OperState Change                                        */
/*  Input(s)     : u2Port - ip port number                                 */
/*                 u4RtNet - Route Network                                 */
/*                 u4RtMask - Mask of the route                            */
/*  Output(s)    : None.                                                   */
/*  Returns      : None.                                                   */
/***************************************************************************/
VOID
ArpActOnStaticRtChange (UINT2 u2Port, UINT4 u4RtNet, UINT4 u4RtMask)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pArpNextCache = NULL;
    t_ARP_CACHE         ArpCache;
    UINT4               u4IfIndex = 0;

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        return;
    }

    ARP_DS_LOCK ();
    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = (u4RtNet & u4RtMask);

    pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                               (tRBElem *) & ArpCache, NULL);

    while (pArpCache != NULL)
    {
        if ((pArpCache->u4IfIndex > u4IfIndex) ||
            ((pArpCache->u4Ip_addr & u4RtMask) > (u4RtNet & u4RtMask)))
        {
            break;
        }
        pArpNextCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);
        if (pArpCache->i1State == ARP_DYNAMIC)
        {
            arp_drop (pArpCache);
        }
        pArpCache = pArpNextCache;
    }
    ARP_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function     : ArpAddArpCache                                             */
/*                                                                           */
/*  Description  : This function adds the given cache entry to ARP global    */
/*                  RBTree                                                   */
/*                                                                           */
/* Input        : pArpCacheNode -   ArpCacheEntry                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ARP_SUCCESS/IP_FFAILURE                                     */
/*****************************************************************************/
INT4
ArpAddArpCache (t_ARP_CACHE * pArpCacheNode)
{
    tArpBasicInfo       ArpBasicInfo;
    UINT4               u4EntryCount = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1ProtoId = 0;
#ifdef LNXIP4_WANTED
    INT4                i4RetVal = ARP_SUCCESS;
    UINT1               au1IfName[MAX_IFNAME_SIZE];
    MEMSET (&au1IfName, 0, sizeof (MAX_IFNAME_SIZE));
#endif
    MEMSET (&ArpBasicInfo, 0, sizeof (tArpBasicInfo));

    if ((pArpCacheNode == NULL) || (pArpCacheNode->pArpCxt == NULL))
    {
        return ARP_FAILURE;
    }
    u4ContextId = pArpCacheNode->pArpCxt->u4ContextId;

    ARP_DS_LOCK ();

    RBTreeCount (gArpGblInfo.ArpTable, &u4EntryCount);

    if (u4EntryCount >= ARP_MAX_ARP_ENTRIES)
    {
        TmrStopTimer (ARP_TIMER_LIST_ID, &(pArpCacheNode->Timer.Timer_node));
        MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArpCacheNode);
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                      "ARP cache Overflow. Maximum Num of Entries is %d.\n",
                      ARP_MAX_ARP_ENTRIES);
        ARP_DS_UNLOCK ();
        return ARP_FAILURE;
    }

    if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        OsixGetSysTime ((tOsixSysTime *) & (pArpCacheNode->u4LastUpdatedTime));
    }

    if (RBTreeAdd (gArpGblInfo.ArpTable, pArpCacheNode) == RB_FAILURE)
    {
        TmrStopTimer (ARP_TIMER_LIST_ID, &(pArpCacheNode->Timer.Timer_node));
        MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArpCacheNode);
        ARP_TRC (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 ARP_NAME, "Adding the cache node RBTree failed.\n");
        ARP_DS_UNLOCK ();
        return ARP_FAILURE;

    }
#ifdef LNXIP4_WANTED
    if (pArpCacheNode->i1State == ARP_DYNAMIC)
    {
        if (NetIpv4GetLinuxIfName ((UINT4) pArpCacheNode->u2Port, au1IfName) ==
            NETIPV4_SUCCESS)
        {
            i4RetVal =
                ArpUpdateKernelEntry (ARP_STATIC,
                                      (UINT1 *) pArpCacheNode->i1Hw_addr,
                                      pArpCacheNode->i1Hwalen,
                                      pArpCacheNode->u4Ip_addr, au1IfName);
            if (i4RetVal == ARP_FAILURE)
            {
                RBTreeRem (gArpGblInfo.ArpTable, pArpCacheNode);
                ARP_TRC (pArpCacheNode->pArpCxt->u4ContextId, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "Adding the cache node in kernel failed\n");
                ARP_DS_UNLOCK ();
                return ARP_FAILURE;
            }
        }
        else
        {
            ARP_TRC (pArpCacheNode->pArpCxt->u4ContextId, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                     "Getting Linux Port Name failed\n");
        }
    }
#endif
    for (u1ProtoId = 0; u1ProtoId < ARP_MAX_REG_MODULES; u1ProtoId++)
    {
        if (gaArpRegTbl[u1ProtoId].pArpBasicInfo != NULL)
        {
            ArpBasicInfo.u4IpAddr = pArpCacheNode->u4Ip_addr;
            MEMCPY (ArpBasicInfo.MacAddr,
                    (UINT1 *) pArpCacheNode->i1Hw_addr, sizeof (tMacAddr));
            ArpBasicInfo.u1Action = ARP_CB_ADD_ENTRY;

            (gaArpRegTbl[u1ProtoId].pArpBasicInfo) (&ArpBasicInfo);

        }
    }
    pArpCacheNode->u1HwStatus = NP_NOT_UPDATED;

    /* If the Entry is added in Pending State,Increment 
       the corresponding counter */
    if (pArpCacheNode->i1State == ARP_PENDING)
    {
        pArpCacheNode->pArpCxt->u4PendingEntryCount++;
    }

    /* Increment the Total Entry Count */
    pArpCacheNode->pArpCxt->u4ArpEntryCount++;
    ARP_DS_UNLOCK ();
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpReplaceArpCacheWithIndex                                */
/*                                                                           */
/*  Description : This function replaces an existing ARP cache entry in the  */
/*                ARP RBTree with the given cache entry and return the       */
/*                Pending data if any                                        */
/*                                                                           */
/* Input        : u4IfIndex     - Interface Index                            */
/*                u4Proto_addr  - IpAddress                                  */
/*                                                                           */
/* Output       : pArpCacheNode - ArpCacheEntry                              */
/*                                                                           */
/* Returns      : ARP_SUCCESS/ARP_FFAILURE                                   */
/*****************************************************************************/
INT4
ArpReplaceArpCacheWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr,
                             t_ARP_CACHE * pArpCacheNode,
                             tCRU_BUF_CHAIN_HEADER ** pDataBuff, INT4 i4Timeout)
{
    t_ARP_CACHE        *pArp_cache = NULL;
    tArpCxt            *pArpCxt = NULL;
    tCRU_BUF_CHAIN_HEADER *pPendBuff = NULL;
    t_ARP_CACHE         ArpCache;
    tArpBasicInfo       ArpBasicInfo;
    UINT1               u1ProtoId = 0;

#ifdef LNXIP4_WANTED
    INT4                i4RetVal = ARP_SUCCESS;
    UINT1               au1IfName[MAX_IFNAME_SIZE];
    MEMSET (&au1IfName, 0, sizeof (MAX_IFNAME_SIZE));
#endif

    MEMSET (&ArpBasicInfo, 0, sizeof (tArpBasicInfo));

    ARP_DS_LOCK ();

    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    pArp_cache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);

    if (pArp_cache != NULL)
    {
        /* If the Added ARP Entry is in Dynamic State and
           Previous state is Pending...
           Get the Pending Data ..
           In other Circumstance there is no Pending Data to transmit */
        if ((pArp_cache->i1State == ARP_PENDING) &&
            (pArpCacheNode->i1State == ARP_DYNAMIC))
        {
            pPendBuff = pArp_cache->pPending;
            pArpCacheNode->i4Retries = ARP_ZERO;
        }

        pArpCxt = pArp_cache->pArpCxt;
        /* Hold Only the Latest Packet..
           If any old data,Release it */
        if ((pArp_cache->i1State == ARP_PENDING) &&
            (pArpCacheNode->i1State == ARP_PENDING))
        {
            if (pArp_cache->i4Retries < pArpCxt->Arp_config.i4Max_retries)
            {
                pArpCacheNode->i4Retries = pArp_cache->i4Retries;
            }
            if (pArp_cache->pPending != NULL)
            {
                IP_RELEASE_BUF (pArp_cache->pPending, FALSE);
                pArp_cache->pPending = NULL;
            }
        }

        TmrStopTimer (ARP_TIMER_LIST_ID, &(pArp_cache->Timer.Timer_node));

        /* Replace the Arp entry in the RBTree only when the
         * contents of the Arp entry has changed.*/
        if ((MEMCMP (pArp_cache->i1Hw_addr, pArpCacheNode->i1Hw_addr,
                     pArpCacheNode->i1Hwalen) == 0)
            && (pArp_cache->u1EncapType == pArpCacheNode->u1EncapType)
            && (pArp_cache->i1State == pArpCacheNode->i1State))
        {
            /* The encapsulation type and hwaddress are not modified. Hence
             * only refresh the arp cache timers */
            if ((pArp_cache->i1State != (INT1) ARP_STATIC) &&
                (pArp_cache->i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE) &&
                (pArp_cache->i1State != (INT1) ARP_INVALID))
            {
                ArpCheckAndStartTimer (pArp_cache, (UINT4) i4Timeout);
            }

            if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
            {
                OsixGetSysTime ((tOsixSysTime *) &
                                (pArp_cache->u4LastUpdatedTime));
                ArpRedSendDynamicCacheTimeInfo (pArp_cache);
            }
            /* The old  data queued with the cache entry is released
             * and the current data is queued so that it will be
             * forwarded after the mac address is resolved. */
            if (pArpCacheNode->pPending != NULL)
            {
                /* Hold only the last data */
                if (pArp_cache->pPending != NULL)
                {
                    IP_RELEASE_BUF (pArp_cache->pPending, FALSE);
                }
                pArp_cache->pPending = pArpCacheNode->pPending;
            }
            /* Release the memory allocated for the new node */
            TmrStopTimer (ARP_TIMER_LIST_ID,
                          &(pArpCacheNode->Timer.Timer_node));
            MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArpCacheNode);
        }
        else
        {
            /*If there is a change from Pending State,
               Decrement the Pending Entry Count */
            if (((pArp_cache->i1State == ARP_PENDING) &&
                 (pArpCacheNode->i1State != ARP_PENDING)) ||
                ((pArp_cache->i1State == ARP_AGEOUT) &&
                 pArpCacheNode->i1State != ARP_AGEOUT))
            {
                pArpCxt->u4PendingEntryCount--;
            }
            if ((pArp_cache->i1State == ARP_DYNAMIC) &&
                (pArpCacheNode->i1State == ARP_AGEOUT))
            {
                pArpCxt->u4PendingEntryCount++;
            }

            if ((pArpCacheNode->i1State != (INT1) ARP_STATIC) &&
                (pArpCacheNode->i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE) &&
                (pArpCacheNode->i1State != (INT1) ARP_INVALID))
            {
                ArpCheckAndStartTimer (pArpCacheNode, (UINT4) i4Timeout);
            }

            if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
            {
                OsixGetSysTime ((tOsixSysTime *)
                                & (pArp_cache->u4LastUpdatedTime));
                ArpRedSendDynamicCacheTimeInfo (pArp_cache);
            }

            RBTreeRem (gArpGblInfo.ArpTable, pArp_cache);
            RBTreeAdd (gArpGblInfo.ArpTable, pArpCacheNode);
#ifdef LNXIP4_WANTED
            if (pArpCacheNode->i1State == ARP_DYNAMIC)
            {
                if (NetIpv4GetLinuxIfName
                    ((UINT4) pArpCacheNode->u2Port,
                     au1IfName) == NETIPV4_SUCCESS)
                {
                    i4RetVal =
                        ArpUpdateKernelEntry (ARP_STATIC,
                                              (UINT1 *) pArpCacheNode->
                                              i1Hw_addr,
                                              pArpCacheNode->i1Hwalen,
                                              pArpCacheNode->u4Ip_addr,
                                              au1IfName);
                    if (i4RetVal == ARP_FAILURE)
                    {
                        RBTreeRem (gArpGblInfo.ArpTable, pArpCacheNode);
                        ARP_TRC (pArpCacheNode->pArpCxt->u4ContextId,
                                 ARP_MOD_TRC,
                                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                                 "Adding the cache node in kernel failed\n");
                        ARP_DS_UNLOCK ();
                        return ARP_FAILURE;
                    }
                }
                else
                {
                    ARP_TRC (pArpCacheNode->pArpCxt->u4ContextId, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                             "Getting Linux Port Name failed\n");
                }
            }
#endif
            for (u1ProtoId = 0; u1ProtoId < ARP_MAX_REG_MODULES; u1ProtoId++)
            {
                if (gaArpRegTbl[u1ProtoId].pArpBasicInfo != NULL)
                {
                    ArpBasicInfo.u4IpAddr = pArp_cache->u4Ip_addr;
                    MEMCPY (ArpBasicInfo.MacAddr,
                            (UINT1 *) pArp_cache->i1Hw_addr, sizeof (tMacAddr));

                    ArpBasicInfo.u1Action = ARP_CB_REMOVE_ENTRY;

                    (gaArpRegTbl[u1ProtoId].pArpBasicInfo) (&ArpBasicInfo);

                    ArpBasicInfo.u4IpAddr = pArpCacheNode->u4Ip_addr;
                    MEMCPY (ArpBasicInfo.MacAddr,
                            (UINT1 *) pArpCacheNode->i1Hw_addr,
                            sizeof (tMacAddr));
                    ArpBasicInfo.u1Action = ARP_CB_ADD_ENTRY;

                    (gaArpRegTbl[u1ProtoId].pArpBasicInfo) (&ArpBasicInfo);

                }
            }
            pArpCacheNode->u1HwStatus = NP_NOT_UPDATED;

            /*Stop the Timer running for the detached ArpCache Node */
            TmrStopTimer (ARP_TIMER_LIST_ID, &(pArp_cache->Timer.Timer_node));
            MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArp_cache);

            if ((pArpCacheNode->i1State == ARP_PENDING) ||
                (pArpCacheNode->i1State == ARP_AGEOUT))
            {
                if (pArpCacheNode->i4Retries >=
                    pArpCxt->Arp_config.i4Max_retries)
                {
                    ARP_DS_UNLOCK ();
                    return ARP_RETRY_LIMIT;
                }
            }
        }
        *pDataBuff = pPendBuff;
        ARP_DS_UNLOCK ();
        return ARP_SUCCESS;
    }
    ARP_DS_UNLOCK ();
    TmrStopTimer (ARP_TIMER_LIST_ID, &(pArpCacheNode->Timer.Timer_node));
    MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArpCacheNode);
    return ARP_FAILURE;
}

/*****************************************************************************/
/* Function     : ArpDeleteArpCacheWithIndex                                */
/*                                                                           */
/*  Description : Deletes a ArpCache Entry from ARPCACHE Table RBTree        */
/*                                                                           */
/* Input        : u4IfIndex     - Interface Index                            */
/*                u4Proto_addr  - IpAddress                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ARP_SUCCESS/IP_FFAILURE                                     */
/*****************************************************************************/
INT4
ArpDeleteArpCacheWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr)
{
    t_ARP_CACHE        *pArp_cache = NULL;
    t_ARP_CACHE        *pArpNextCache = NULL;
    t_ARP_CACHE         ArpCache;

    ARP_DS_LOCK ();

    MEMSET (&ArpCache, ARP_ZERO, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    pArp_cache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
    if (pArp_cache == NULL)
    {
        MEMSET (&ArpCache, ARP_ZERO, sizeof (t_ARP_CACHE));
        ArpCache.u4IfIndex = u4IfIndex;
        pArp_cache =
            RBTreeGetNext (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache, NULL);
        while (pArp_cache != NULL)
        {
            pArpNextCache =
                RBTreeGetNext (gArpGblInfo.ArpTable, pArp_cache, NULL);

            if (u4Proto_addr == pArp_cache->u4Ip_addr)
            {
                arp_drop (pArp_cache);
                ARP_DS_UNLOCK ();
                return ARP_SUCCESS;

            }
            pArp_cache = pArpNextCache;

        }
    }

    if (pArp_cache != NULL)
    {
        arp_drop (pArp_cache);
        ARP_DS_UNLOCK ();
        return ARP_SUCCESS;
    }

    ARP_DS_UNLOCK ();
    return ARP_FAILURE;
}

/*****************************************************************************/
/* Function     : ArpUpdateArpCacheStateWithIndex                            */
/*                                                                           */
/*  Description : Updates State of the ArpCache Entry in the ARP RBTree      */
/*                                                                           */
/* Input        : u4IfIndex     - Interface Index                            */
/*                u4Proto_addr  - IpAddress                                  */
/*                i1State       - ArpCache State                             */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ARP_SUCCESS/IP_FFAILURE                                     */
/*****************************************************************************/
INT4
ArpUpdateArpCacheStateWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr,
                                 INT1 i1State)
{
    t_ARP_CACHE        *pArp_cache = NULL;
    t_ARP_CACHE         ArpCache;

    ARP_DS_LOCK ();

    MEMSET (&ArpCache, ARP_ZERO, sizeof (t_ARP_CACHE));
    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    pArp_cache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);

    if (pArp_cache != NULL)
    {
        if (((pArp_cache->i1State != ARP_STATIC) && (i1State == ARP_STATIC)) ||
            ((pArp_cache->i1State != ARP_STATIC_NOT_IN_SERVICE) &&
             (i1State == ARP_STATIC)))
        {
            TmrStopTimer (ARP_TIMER_LIST_ID, &(pArp_cache->Timer.Timer_node));
        }

        if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pArp_cache->u4LastUpdatedTime));
            ArpRedSendDynamicCacheTimeInfo (pArp_cache);
        }

        pArp_cache->i1State = i1State;
        ARP_DS_UNLOCK ();
        return ARP_SUCCESS;
    }

    ARP_DS_UNLOCK ();
    return ARP_FAILURE;
}

/*****************************************************************************/
/* Function     : ArpIncArpCacheRetryWithIndex                               */
/*                                                                           */
/*  Description : Increments the retry count of the ArpCache Entry           */
/*                                                                           */
/* Input        : u4IfIndex     - Interface Index                            */
/*                u4Proto_addr  - IpAddress                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*****************************************************************************/
VOID
ArpIncArpCacheRetryWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr)
{
    t_ARP_CACHE        *pArp_cache = NULL;
    t_ARP_CACHE         ArpCache;

    ARP_DS_LOCK ();

    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    pArp_cache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);

    if (pArp_cache != NULL)
    {
        pArp_cache->i4Retries++;
        if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pArp_cache->u4LastUpdatedTime));
            ArpRedSendDynamicCacheTimeInfo (pArp_cache);
        }
    }
    ARP_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function     : ArpGetNextFromArpcacheTable                                */
/*                                                                           */
/*  Description : Returns Lexographically higher Entry from ARPCACHE Table   */
/*                                                                           */
/* Input        : i4IfIndex     - Interface Index                            */
/*                u4IpAddress   - IpAddress                                  */
/*                                                                           */
/* Output       :  pi4NextIfIndex   - Next Interface Index                   */
/*                 pu4NextIpAddress - Next IpAddress                         */
/*                                                                           */
/* Returns      : ARP_SUCCESS/IP_FFAILURE                                     */
/*****************************************************************************/
INT4
ArpGetNextFromArpcacheTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                             UINT4 u4IpAddress, UINT4 *pu4NextIpAddress)
{
    t_ARP_CACHE        *pArp_cache = NULL;
    t_ARP_CACHE         ArpCache;

    ARP_DS_LOCK ();

    ArpCache.u4IfIndex = (UINT4) i4IfIndex;
    ArpCache.u4Ip_addr = u4IpAddress;

    pArp_cache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                (tRBElem *) & ArpCache, NULL);

    if (pArp_cache != NULL)
    {
        *pi4NextIfIndex = (INT4) pArp_cache->u4IfIndex;
        *pu4NextIpAddress = pArp_cache->u4Ip_addr;
        ARP_DS_UNLOCK ();
        return ARP_SUCCESS;
    }

    ARP_DS_UNLOCK ();
    return ARP_FAILURE;

}

/****************************************************************************
* Function     : ArpGetEntryCount     
*                                            
* Description  : API to get the number of ARP entries across 
*                 all virtual routers
*
* Input        : None
*
* Output       : Total number of cache entries saved  
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/
UINT4
ArpGetEntryCount (VOID)
{
    UINT4               u4EntryCount = ARP_ZERO;

    ARP_DS_LOCK ();

    RBTreeCount (gArpGblInfo.ArpTable, &u4EntryCount);

    ARP_DS_UNLOCK ();

    return u4EntryCount;
}

/*-------------------------------------------------------------------+
 * Function           : ArpGetValidEntryWithIndex
 *
 * Input(s)           : u4IfIndex, u4Dest
 *
 * Output(s)          : pi1Hw_addr, pu1EncapType
 *
 * Returns            : ARP_SUCCESS   -- if address can be resolved.
 *                      ARP_FAILURE   -- if failed to resolve address.
 * Action :
 *
 * This procedure is called from interface send functions.
 * When an IP address is to be resolved to get hardware address
 * this function is called.  The caller passes the pointer to hardware
 * address to be returned.
+-------------------------------------------------------------------*/
INT1
ArpGetValidEntryWithIndex (UINT4 u4IfIndex, UINT4 u4Dest,
                           INT1 *pi1Hw_addr, UINT1 *pu1EncapType)
{
    t_ARP_CACHE         ArpCache;
    UINT4               u4ContextId;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC, ARP_NAME,
                  "Resolving Protocol Address %x in the interface %d.\n",
                  u4Dest, u4IfIndex);

    if (ArpLookupWithIndex (u4IfIndex, u4Dest, &ArpCache) == ARP_SUCCESS)
    {
        if ((ArpCache.i1State == (INT1) ARP_STATIC) ||
            (ArpCache.i1State == (INT1) ARP_DYNAMIC))
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                          CONTROL_PLANE_TRC, ARP_NAME,
                          "Valid Address Mapping available for %x.\n", u4Dest);
            MEMCPY (pi1Hw_addr, ArpCache.i1Hw_addr, ArpCache.i1Hwalen);
            *pu1EncapType = ArpCache.u1EncapType;
            return ARP_SUCCESS;
        }
        else
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                          CONTROL_PLANE_TRC, ARP_NAME,
                          "Resolve Failed for  %x. Cache state is not valid\n",
                          u4Dest);
            return ARP_FAILURE;
        }
    }
    else
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC, ARP_NAME,
                      "Resolve Failed for  %x.\n", u4Dest);
        return ARP_FAILURE;
    }
}

/*-------------------------------------------------------------------+
 * Function           : ArpLookupWithIndex
 *
 * Input(s)           : u4IfIndex, u4Proto_addr
 *
 * Output(s)          : Arp cache entry for the given address / NULL
 *
 * Returns            : ARP_SUCCESS /ARP_FAILURE.
 *
 * Action :
 * Scans the Arp cache RBTree to find the entry corresponding to
 * this type and this destination learnt on the given interface.
 *
+-------------------------------------------------------------------*/

INT4
ArpLookupWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr,
                    t_ARP_CACHE * pArp_cacheInfo)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pArpCache = NULL;
    UINT4               u4ContextId;

    ARP_DS_LOCK ();

    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    pArpCache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);

    if (pArpCache == NULL)
    {
        ARP_DS_UNLOCK ();
        ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      ARP_NAME, "ARP Lookup Failure for %x in index %d.\n",
                      u4Proto_addr, u4IfIndex);
        return ARP_FAILURE;
    }
    MEMCPY (pArp_cacheInfo, pArpCache, sizeof (t_ARP_CACHE));
    ARP_DS_UNLOCK ();
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpEvpnLookupWithIpAddr
 *
 * Input(s)           : u4Proto_addr
 *
 * Output(s)          : Arp cache entry for the given address / NULL
 *
 * Returns            : ARP_SUCCESS /ARP_FAILURE.
 *
 * Action :
 * Scans the Arp cache RBTree to find the entry corresponding to
 * this type and this destination learnt on the given interface.
 *
+-------------------------------------------------------------------*/

INT4
ArpEvpnLookupWithIpAddr (UINT4 u4Proto_addr, t_ARP_CACHE * pArp_cacheInfo)
{
    t_ARP_CACHE        *pArpCache, *pNextArpCache = NULL;

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    while (pArpCache != NULL)
    {
        pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

        if (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE)
        {
            /* Statically configured and Rowstaus as NOT_IN_SERVICE
             * or NOT_READY
             */
            pArpCache = pNextArpCache;
            continue;
        }
        if ((u4Proto_addr == pArpCache->u4Ip_addr)
            && (pArpCache->i1State == ARP_EVPN))
        {
            break;
        }
        pArpCache = pNextArpCache;
    }
    if (pArpCache == NULL)
    {
        return ARP_FAILURE;
    }
    MEMCPY (pArp_cacheInfo, pArpCache, sizeof (t_ARP_CACHE));
    return ARP_SUCCESS;
}

/***************************************************************************/
/*  Function Name: ArpFlushWithMac                                         */
/*  Description  : Deletes all the entries from ARP cache with the port    */
/*                 matching with the port passed and MAC                   */
/*  Input(s)     : tArpQFlushMac - Contain Mac,Vlan,Port                   */
/*  Output(s)    : None.                                                   */
/*  Returns      : ARP_FAILURE/ARP_SUCCESS                                 */
/***************************************************************************/
INT4
ArpFlushWithMac (tArpQFlushMac * pMacAddr)
{
    t_ARP_CACHE        *pArpCache = NULL, *pNextArpCache = NULL;
    INT1                i1ArpMatchFound = ARP_ZERO;

    ARP_DS_LOCK ();
    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);
    while (pArpCache != NULL)
    {
        if ((MEMCMP (pArpCache->i1Hw_addr, pMacAddr->pQMacAddr,
                     CFA_ENET_ADDR_LEN) == 0))
        {
            arp_drop (pArpCache);
            i1ArpMatchFound = ARP_ONE;    /* Match Found */
            break;
        }
        pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

        pArpCache = pNextArpCache;
    }
    if (i1ArpMatchFound == ARP_ZERO)
    {
        ARP_DS_UNLOCK ();        /* Match NOT Found */
        return ARP_FAILURE;
    }

    ARP_DS_UNLOCK ();
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpGetCacheWithIndex
 *
 * Input(s)           : u4IfIndex, u4Proto_addr
 *
 * Output(s)          : Arp cache entry for the given address / NULL
 *
 * Returns            : ARP_SUCCESS /ARP_FAILURE.
 *
 * Action :
 * Scans the Arp cache RBTree to find the entry corresponding to
 * this type and this destination learnt on the given interface.
 *
-------------------------------------------------------------------*/

INT4
ArpGetCacheWithIndex (UINT4 u4IfIndex, UINT4 u4Proto_addr,
                      t_ARP_CACHE ** pArp_cacheInfo)
{
    t_ARP_CACHE         ArpCache;
    UINT4               u4ContextId;
    UNUSED_PARAM (pArp_cacheInfo);
    ARP_DS_LOCK ();

    ArpCache.u4IfIndex = u4IfIndex;
    ArpCache.u4Ip_addr = u4Proto_addr;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    *pArp_cacheInfo = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);

    if (*pArp_cacheInfo == NULL)
    {
        ARP_DS_UNLOCK ();
        ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      ARP_NAME, "ARP Lookup Failure for %x in index %d.\n",
                      u4Proto_addr, u4IfIndex);
        return ARP_FAILURE;
    }
    ARP_DS_UNLOCK ();
    return ARP_SUCCESS;
}
