/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsarplw.c,v 1.11 2015/09/15 06:46:24 siva Exp $
 *
 * Description:These routines provide hooks for  network    
 *             management protocols to access arp config    
 *             variables and data structures.            
 *
 *******************************************************************/
#include "arpinc.h"
#include "fsarplw.h"
#include "fsmparcli.h"

 
extern UINT4 FsArpGlobalDebug [10]; 
 
/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsArpCacheTimeout
 Input       :  The Indices

                The Object 
                retValFsArpCacheTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsArpCacheTimeout (INT4 *pi4RetValFsArpCacheTimeout)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpCacheTimeout =
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Cache_timeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpCachePendTime
 Input       :  The Indices

                The Object 
                retValFsArpCachePendTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsArpCachePendTime (INT4 *pi4RetValFsArpCachePendTime)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpCachePendTime =
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Cache_pend_time;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpMaxRetries
 Input       :  The Indices

                The Object 
                retValFsArpMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsArpMaxRetries (INT4 *pi4RetValFsArpMaxRetries)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpMaxRetries =
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Max_retries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpPendingEntryCount
 Input       :  The Indices

                The Object 
                retValFsArpPendingEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsArpPendingEntryCount(INT4 *pi4RetValFsArpPendingEntryCount)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpPendingEntryCount =
        (INT4 )gArpGblInfo.pArpCurrCxt->u4PendingEntryCount;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsArpCacheEntryCount
 Input       :  The Indices

                The Object 
                retValFsArpCacheEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsArpCacheEntryCount(INT4 *pi4RetValFsArpCacheEntryCount)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpCacheEntryCount =
        (INT4 )gArpGblInfo.pArpCurrCxt->u4ArpEntryCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpRedEntryTime
 Input       :  The Indices
 
                The Object
                retValFsArpRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
                Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhGetFsArpRedEntryTime(INT4 *pi4RetValFsArpRedEntryTime)
{
     *pi4RetValFsArpRedEntryTime = gArpRedGlobalInfo.i4ArpRedEntryTime;
     return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpRedExitTime
 Input       :  The Indices

                The Object
                retValFsArpRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/ 
INT1 nmhGetFsArpRedExitTime(INT4 *pi4RetValFsArpRedExitTime)
{
     *pi4RetValFsArpRedExitTime = gArpRedGlobalInfo.i4ArpRedExitTime;
     return SNMP_SUCCESS;
}



/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsArpCacheTimeout
 Input       :  The Indices

                The Object 
                setValFsArpCacheTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsArpCacheTimeout (INT4 i4SetValFsArpCacheTimeout)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    gArpGblInfo.pArpCurrCxt->Arp_config.i4Cache_timeout =
        i4SetValFsArpCacheTimeout;
#ifdef LNXIP4_WANTED
    ArpSetLnxArpCacheTimeout (i4SetValFsArpCacheTimeout);
#endif /* LNXIP4_WANTED */
    ArpIncMsrNotifyCfg (gArpGblInfo.pArpCurrCxt->u4ContextId,
                        FsMIArpCacheTimeout,
                        (sizeof (FsMIArpCacheTimeout) / sizeof (UINT4)),
                        i4SetValFsArpCacheTimeout);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsArpCachePendTime
 Input       :  The Indices

                The Object 
                setValFsArpCachePendTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsArpCachePendTime (INT4 i4SetValFsArpCachePendTime)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    gArpGblInfo.pArpCurrCxt->Arp_config.i4Cache_pend_time =
        i4SetValFsArpCachePendTime;
    ArpIncMsrNotifyCfg (gArpGblInfo.pArpCurrCxt->u4ContextId,
                        FsMIArpCachePendTime,
                        (sizeof (FsMIArpCachePendTime) / sizeof (UINT4)),
                        i4SetValFsArpCachePendTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsArpMaxRetries
 Input       :  The Indices

                The Object 
                setValFsArpMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsArpMaxRetries (INT4 i4SetValFsArpMaxRetries)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    gArpGblInfo.pArpCurrCxt->Arp_config.i4Max_retries = i4SetValFsArpMaxRetries;

#ifdef LNXIP4_WANTED
    ArpSetLnxMaxRetires (i4SetValFsArpMaxRetries);
#endif /* LNXIP4_WANTED */
    ArpIncMsrNotifyCfg (gArpGblInfo.pArpCurrCxt->u4ContextId,
                        FsMIArpMaxRetries,
                        (sizeof (FsMIArpMaxRetries) / sizeof (UINT4)),
                        i4SetValFsArpMaxRetries);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsArpCacheTimeout
 Input       :  The Indices

                The Object 
                testValFsArpCacheTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsArpCacheTimeout (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsArpCacheTimeout)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsArpCacheTimeout < ARP_MIN_CACHE_TIMEOUT)
        || (i4TestValFsArpCacheTimeout > ARP_MAX_CACHE_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP_INV_ARPCACHE_TIMEOUT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsArpCachePendTime
 Input       :  The Indices

                The Object 
                testValFsArpCachePendTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsArpCachePendTime (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsArpCachePendTime)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsArpCachePendTime < ARP_MIN_PEND_TIME) ||
        (i4TestValFsArpCachePendTime > ARP_MAX_PEND_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsArpMaxRetries
 Input       :  The Indices

                The Object 
                testValFsArpMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsArpMaxRetries (UINT4 *pu4ErrorCode, INT4 i4TestValFsArpMaxRetries)
{
    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsArpMaxRetries < ARP_MIN_REQ_RETRIES) ||
        (i4TestValFsArpMaxRetries > ARP_MAX_REQ_RETRIES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsArpCacheTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsArpCacheTimeout (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsArpCachePendTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsArpCachePendTime (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsArpMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsArpMaxRetries (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ArpIncMsrNotifyCfg                                         */
/*                                                                           */
/* Description  : This function sends a notification to MSR for the ARP      */
/*                 context specific configurations                           */
/*                                                                           */
/* Input        : i4ContextId   - The context id in which configuration is   */
/*                                 is done                                   */
/*                pu4ObjectId   - The object for which notification is to be */
/*                               sent                                        */
/*                u4OidLen      - Object Oid length                          */
/*                u1IsRowStatus - TRUE/FALSE                                 */
/*                i4SetVal      - Value which should be stored.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
ArpIncMsrNotifyCfg (INT4 i4ContextId, UINT4 *pu4ObjectId,
                    UINT4 u4OidLen, INT4 i4SetVal)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = ArpProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = ArpProtocolUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4ContextId, i4SetVal));
    return;
}

/****************************************************************************
 Function    :  nmhGetFsArpCacheFlushStatus
 Input       :  The Indices

                The Object 
                retValFsArpCacheFlushStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsArpCacheFlushStatus(INT4 *pi4RetValFsArpCacheFlushStatus)
{

    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsArpCacheFlushStatus =
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Flush_status;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsArpGlobalDebug
 Input       :  The Indices

                The Object
                retValFsArpGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsArpGlobalDebug(INT4 *pi4RetValFsArpGlobalDebug)
{
   INT4 u4DebugLevel = ARP_FAILURE;

   u4DebugLevel = ArpGetDebugFlag(ARP_DEFAULT_CONTEXT);
    if (u4DebugLevel == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsArpGlobalDebug = u4DebugLevel;
         return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhSetFsArpCacheFlushStatus
 Input       :  The Indices

                The Object 
                setValFsArpCacheFlushStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsArpCacheFlushStatus(INT4 i4SetValFsArpCacheFlushStatus,
                                 UINT4 u4ContextId)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pNextArpCache = NULL;
   
    /* Check whether ARP is initialised */

    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
     /* If the Flush status flag is set as TRUE, clear the dynamically learnt arp entry*/
    if (i4SetValFsArpCacheFlushStatus == OSIX_TRUE)
    {
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Flush_status =
            i4SetValFsArpCacheFlushStatus;

        if (u4ContextId != VCM_DEFAULT_CONTEXT)
        {
            if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
            {
                return SNMP_FAILURE;
            }
        }
        ARP_DS_LOCK ();
        pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

        while (pArpCache != NULL)
        {
            pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

            if (pArpCache->pArpCxt->u4ContextId != u4ContextId)
            {
		/* Skip the current context */
                pArpCache = pNextArpCache;
                continue;
            }
            if (pArpCache->i1State == ARP_DYNAMIC)
            {
		/* Flush the dynamic arp entry */    
                arp_drop(pArpCache);
            }
            pArpCache = pNextArpCache; 
        }
        ARP_DS_UNLOCK (); 
        gArpGblInfo.pArpCurrCxt->Arp_config.i4Flush_status = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsArpGlobalDebug
 Input       :  The Indices

                The Object
                setValFsArpGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsArpGlobalDebug(INT4 i4SetValFsArpGlobalDebug)
{
  ARP_PROT_UNLOCK (); /* Lock taken in cli and snmp thread 
           so releasing the lock */
  ArpSetArpDebug(ARP_DEFAULT_CONTEXT, (UINT4)i4SetValFsArpGlobalDebug);
  ARP_PROT_LOCK ();

  ArpIncMsrNotifyCfg (ARP_DEFAULT_CONTEXT,
                        FsArpGlobalDebug,
                        (sizeof (FsArpGlobalDebug) / sizeof (UINT4)),
                        i4SetValFsArpGlobalDebug);

  return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsArpCacheFlushStatus
 Input       :  The Indices

                The Object 
                testValFsArpCacheFlushStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsArpCacheFlushStatus(UINT4 *pu4ErrorCode , 
		                    INT4 i4TestValFsArpCacheFlushStatus)
{

    if (gArpGblInfo.pArpCurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsArpCacheFlushStatus != OSIX_TRUE )
        && (i4TestValFsArpCacheFlushStatus != OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP_INVALID_FLUSH_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsArpGlobalDebug
 Input       :  The Indices

                The Object
                testValFsArpGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsArpGlobalDebug(UINT4 *pu4ErrorCode , INT4 i4TestValFsArpGlobalDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsArpGlobalDebug);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsArpCacheFlushStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsArpCacheFlushStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsArpGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsArpGlobalDebug(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

