/***********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: arpicch.c,v 1.13 2016/02/23 12:20:03 siva Exp $
 *
 * Description     : This file contains ARP ICCH related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : arpicch.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : ARP Module                                       */
/*  MODULE NAME           : ARP                                              */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Aug 2015                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
#ifndef _ARPICCH_C_
#define _ARPICCH_C_
#include "arpinc.h"

/*****************************************************************************/
/* Function Name      : ArpIcchRegisterWithICCH                              */
/*                                                                           */
/* Description        : Registers ARP module with ICCH to receive update     */
/*                      messages.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then ARP_SUCCESS          */
/*                      Otherwise ARP_FAILURE                               */
/*****************************************************************************/
INT4
ArpIcchRegisterWithICCH (VOID)
{
    tIcchRegParams        IcchRegParams;

    MEMSET (&IcchRegParams, 0, sizeof (tIcchRegParams));

    IcchRegParams.u4EntId = ICCH_ARP_APP_ID;
    IcchRegParams.pFnRcvPkt = ArpIcchHandleUpdateEvents;

    if (IcchRegisterProtocols (&IcchRegParams) == ICCH_FAILURE)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpIcchRegisterWithICCH: Registration with ICCH failed");

        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : ArpIcchDeRegisterWithICCH                            */
/*                                                                           */
/* Description        : De-registers ARP module with ICCH to receive update  */
/*                      messages.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if de-registration is success then ARP_SUCCESS       */
/*                      Otherwise ARP_FAILURE                               */
/*****************************************************************************/
INT4
ArpIcchDeRegisterWithICCH (VOID)
{
    if (IcchDeRegisterProtocols (ICCH_ARP_APP_ID) == ICCH_FAILURE)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "De-Registration with ICCH failed for ARP\r\n");

        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpIcchHandleUpdateEvents                            */
/*                                                                           */
/* Description        : This function will be invoked by the ICCH module to  */
/*                      pass events  to ARP module                           */
/*                                                                           */
/* Input(s)           : u1Event - Event given by ICCH module.                */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS - If the message is enq'ed successfully */
/*                      ICCH_FAILURE otherwise                               */
/*****************************************************************************/
INT4
ArpIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg *pData, UINT2 u2DataLen)
{
    tArpIcchData        *pMsg = NULL;

    if ((u1Event != ICCH_MESSAGE) &&
        (u1Event != L2_INIT_BULK_UPDATES)) 
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "ICCH event is invalid \r\n");

        return ICCH_FAILURE;
    }

    if ((pData == NULL) && (u1Event == ICCH_MESSAGE))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to ARP task. */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "Message from ICCH ignored - Buffer missing !!! \r\n");

        return ICCH_FAILURE;
    }
    pMsg = (tArpIcchData *) MemAllocMemBlk ((tMemPoolId) ARP_ICCH_MSG_POOL_ID);
    if (NULL == pMsg)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpIcchHandleUpdateEvents: Queue message allocation "
                 "failure\r\n");
        return ICCH_FAILURE;
    } 

    MEMSET (pMsg, 0, sizeof (tArpIcchData));

    pMsg->u1Event =  u1Event;
    pMsg->pIcchMsg = pData;
    pMsg->u2DataLen = u2DataLen;

    /* Send the message associated with the event to ARP module in a queue */
    if (OsixQueSend (ARP_ICCH_PKT_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) ARP_ICCH_MSG_POOL_ID, (UINT1 *) pMsg);

        
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpIcchHandleUpdateEvents: Failed to send the message to "
                 "ARP queue\r\n");
        return ICCH_FAILURE;
    }

    /* Post a event to ARP to process ICCH events */
    if (OsixEvtSend (ARP_TASK_ID, ARP_ICCH_PKT_EVENT) != OSIX_SUCCESS)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpIcchHandleUpdateEvents: Failed to send Event to ARP\r\n");
        return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpIcchProcessUpdateMsg                              */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the ICCH module. This function invokes          */
/*                      appropriate functions to handle the update message.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ArpIcchProcessUpdateMsg ()
{
    tArpIcchData         *pMsg = NULL;
    tIcchProtoAck         ProtoAck;
    UINT4                 u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tIcchProtoAck));

    while (OsixQueRecv (ARP_ICCH_PKT_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {

        switch (pMsg->u1Event)
        {
            case ICCH_MESSAGE:
                /* Read the sequence number from the ICCH Header */
                ICCH_PKT_GET_SEQNUM (pMsg->pIcchMsg, &u4SeqNum);
                /* Remove the ICCH Header */
                ICCH_STRIP_OFF_ICCH_HDR (pMsg->pIcchMsg,
                        pMsg->u2DataLen);

                ProtoAck.u4AppId = ICCH_ARP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                ArpIcchHandleUpdates (pMsg->pIcchMsg,
                        pMsg->u2DataLen);

                ICCH_FREE (pMsg->pIcchMsg);
                MemReleaseMemBlock ((tMemPoolId) ARP_ICCH_MSG_POOL_ID, (UINT1 *) pMsg);

                /* Sending ACK to ICCH */
                IcchApiSendProtoAckToICCH (&ProtoAck);
                break;

            case L2_INIT_BULK_UPDATES:
                ArpIcchInitBulkReq();
                break;

            default:
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                        CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                        "Received unknown event in ARP from ICCH\r\n");
                break;
        } /* End of switch */
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ArpIcchHandleUpdates                                 */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      ICCH module. This function invokes appropriate       */
/*                      function to process the received update messsage.    */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to tIcchMsg.                          */
/*                      u2DataLen - Data length.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ArpIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen)
{
    tIcchProtoEvt       ProtoEvt;
    tArpQMsg            ArpQMsg;
    t_ARP_PKT           Arp_pkt;
    UINT4               u4OffSet = 0;
    UINT2               u2MinLen = 0;
    UINT2               u2Len = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&Arp_pkt, 0, sizeof (t_ARP_PKT));
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    ProtoEvt.u4AppId = ICCH_ARP_APP_ID;

    u2MinLen = ARP_ICCH_TYPE_FIELD_SIZE + ARP_ICCH_LEN_FIELD_SIZE;

    if (u2DataLen < u2MinLen)
    {
        /* No room for type and length field itself */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "No space to accommodate type and length field\r\n");

        return;
    }

    ARP_ICCH_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    ARP_ICCH_GET_2_BYTE (pMsg, &u4OffSet, u2Len);

    switch (u1MsgType)
    {
        case ARP_ICCH_SYNC_MSG:
             if(ARP_FAILURE ==  ArpIcchProcessArpSyncMsg (pMsg, &u4OffSet))
             {
               ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                   "Processing ARP sync up message failed\r\n");
             }
             break;

        case ARP_ICCH_BULK_REQUEST:
            ArpIcchSendBulkUpdates ();
            break;

        case ARP_ICCH_BULK_UPDATE:

            ArpIcchProcessBulkInfo (pMsg, &u4OffSet, u2Len);
            break;

        case ARP_ICCH_TAIL_MSG:
            ProtoEvt.u4Error = ICCH_NONE;
            ProtoEvt.u4Event = ICCH_PROTOCOL_BULK_UPDT_COMPLETION;
            IcchApiHandleProtocolEvent (&ProtoEvt);

            break;

        default:
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ArpIcchSyncArpMsg                                    */
/*                                                                           */
/* Description        : This function forms ARP sync-up message and send it  */
/*                      to ICCH for trasmitting to remote MC-LAG node        */ 
/*                                                                           */
/* Input(s)           : pArpQMsg  - ARP message                              */
/*                      u4IfIndex - L3 inteface index                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE.                           */
/*****************************************************************************/
INT4
ArpIcchSyncArpMsg (tARP_DATA *pArpData, UINT4 u4IfIndex)
{
    tIcchMsg           *pMsg = NULL;
    UINT2               u2Len = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Port = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1           u1Status = VLAN_FDB_LEARNT;

    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                "ARP Sync-up is not enabled, So, ARP message is not send "
                "to remote MC-LAG node\r\n");
        return ARP_FAILURE;
    }

    u2Len =    ARP_ICCH_IP_ADDR_SIZE +
               ARP_ICCH_ARP_STATE_SIZE + ARP_ICCH_HW_ADDR_SIZE +
               ARP_ICCH_HW_TYPE_SIZE + ARP_ICCH_ROW_STATUS_SIZE +
               ARP_ICCH_HW_STATUS_SIZE + ARP_ICCH_VLAN_ID_SIZE;
    
    pMsg = ICCH_ALLOC_TX_BUF (u2Len);
    if ( pMsg == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ICCH alloc failed. Unable to send ARP sync request\r\n");
        return ARP_FAILURE;
    }

    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, ARP_ICCH_SYNC_MSG);
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2Len);
    ARP_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, pArpData->u4IpAddr);
    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, pArpData->i1State);
    ARP_ICCH_PUT_N_BYTE (pMsg, &u2OffSet, pArpData->i1Hw_addr,
                             CFA_ENET_ADDR_LEN);
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, pArpData->i2Hardware);
    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, pArpData->u1RowStatus);

    if (CfaGetVlanId (u4IfIndex, &u2VlanId) != CFA_SUCCESS)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                "Failed to get Vlan ID while sending ARP sync-up message to "
                "remote MC-LAG node");
       ICCH_FREE (pMsg); 
       return ARP_FAILURE;
    }

    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2VlanId);
    VlanGetFdbEntryDetails (u2VlanId, (UINT1 *) pArpData->i1Hw_addr,
                        &u2Port, &u1Status);
    CfaGetInterfaceNameFromIndex ((UINT4) u2Port,  au1IfName);
    u2Port = ATOI (au1IfName + 2);
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2Port);

    if (ARP_FAILURE == ArpIcchSendMsgToIcch (pMsg, u2OffSet))
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                "Failed to send sync-up message to remote MC-LAG node\r\n");
    }
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpIcchProcessArpSyncMsg                             */
/*                                                                           */
/* Description        : This function processes the ARP sync-up message      */
/*                                                                           */
/* Input(s)           : pMsg      - ARP TLV message                          */
/*                      pu2Offset - offset of the ARP message                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE.                           */
/*****************************************************************************/
INT4
ArpIcchProcessArpSyncMsg (tIcchMsg *pMsg, UINT4 *pu4Offset)
{
    tARP_DATA       ArpInfo;
    tNetIpv4IfInfo  NetIpIfInfo;
    tUtlInAddr      IpAddr;
    UINT4           u4IfIndex = 0;
    UINT4           u4Port = 0;
    tVlanId         u2VlanId = 0;
    UINT2           u2Key = 0;
    UINT2           u2Index = 0;
    UINT1           u1Status = VLAN_FDB_LEARNT;

    MEMSET (&ArpInfo, 0, sizeof (tARP_DATA));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                "ARP Sync-up is not enabled \r\n");
        return ARP_FAILURE;
    }

    ARP_ICCH_GET_4_BYTE (pMsg, pu4Offset, ArpInfo.u4IpAddr);
    ARP_ICCH_GET_1_BYTE (pMsg, pu4Offset, ArpInfo.i1State);
    ARP_ICCH_GET_N_BYTE (pMsg, pu4Offset, ArpInfo.i1Hw_addr,
            CFA_ENET_ADDR_LEN);
    ARP_ICCH_GET_2_BYTE (pMsg, pu4Offset, ArpInfo.i2Hardware);
    ARP_ICCH_GET_1_BYTE (pMsg, pu4Offset, ArpInfo.u1RowStatus);
    ARP_ICCH_GET_2_BYTE (pMsg, pu4Offset, u2VlanId);
    ARP_ICCH_GET_2_BYTE (pMsg, pu4Offset, u2Key);

    u4IfIndex = CfaGetVlanInterfaceIndex (u2VlanId);
    if ( u4IfIndex == CFA_INVALID_INDEX)
    {
       ARP_TRC_ARG1 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                     "Failed to get Vlan Interface index for Vlan ID %d\r\n",
                      u2VlanId);

       return ARP_FAILURE;
    }

    if (NETIPV4_FAILURE == NetIpv4GetPortFromIfIndex (u4IfIndex , &u4Port))
    {
        ARP_TRC_ARG1 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                "Failed to get Port for Interface Index %d\r\n",u4IfIndex);
        return ARP_FAILURE;
    }
    ArpInfo.u2Port = (UINT2) u4Port;

    if (NETIPV4_FAILURE == NetIpv4GetIfInfo ( u4Port, &NetIpIfInfo))
    {
       ARP_TRC_ARG1 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to get Interface Information for the port"
                    "%d\r\n",u4Port);
       return ARP_FAILURE;
    }
    ArpInfo.u1EncapType = NetIpIfInfo.u1EncapType;
    ArpInfo.i1Hwalen = CFA_ENET_ADDR_LEN;
    
    if (VLAN_FAILURE == VlanGetFdbEntryDetails (u2VlanId, (UINT1 *) ArpInfo.i1Hw_addr, 
                                                 &u2Index, &u1Status))
    {
       if (LA_TRUE ==
           LaGetAggIndexFromAdminKey (u2Key, &u4IfIndex))
       {
            VlanHwAddFdbEntry (u4IfIndex, (UINT1 *) ArpInfo.i1Hw_addr, 
                               u2VlanId, VLAN_DELETE_ON_TIMEOUT);
       }
    }
#if !defined (LNXIP4_WANTED)
    if (ARP_SUCCESS != arp_add (ArpInfo))
    {
        IpAddr.u4Addr = OSIX_NTOHL (ArpInfo.u4IpAddr);
        ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                      "Failed to add ARP entry for IP address %s and VLAN %u "
                      "in MC-LAG sync-up processing\r\n", 
                      INET_NTOA (IpAddr), u2VlanId);

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                     "Failed to add ARP entry for IP address %s and VLAN %u "
                     "in MC-LAG sync-up processing\r\n", 
                     INET_NTOA (IpAddr), u2VlanId));
        return ARP_FAILURE;
    }
#else
    /* While Synching Entries incase of Linux IP, Indicate kernel alone
     * kernel will indicate control plane thorugh netlink socket. At that
     * time entry will be programmed in control plane and NP */
    if (ARP_SUCCESS != ArpUpdateKernelEntrywithCOM (ARP_DYNAMIC,
                       		         (UINT1 *)ArpInfo.i1Hw_addr,
                                	 CFA_ENET_ADDR_LEN,
                                	 ArpInfo.u4IpAddr,
                                	 NetIpIfInfo.au1IfName))
    {
        IpAddr.u4Addr = OSIX_NTOHL (ArpInfo.u4IpAddr);
        ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                      "Failed to add ARP entry for IP address %s and VLAN %u "
                      "in MC-LAG sync-up processing\r\n",
                      INET_NTOA (IpAddr), u2VlanId);

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                     "Failed to add ARP entry for IP address %s and VLAN %u "
                     "in MC-LAG sync-up processing\r\n",
                     INET_NTOA (IpAddr), u2VlanId));
        return ARP_FAILURE;
    }
#endif
    return ARP_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : ArpIcchInitBulkReq                                      */
/*                                                                            */
/*  Description     : This function forms and sends bulk request to remote    */
/*                    MC-LAG node.                                            */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : ARP_SUCCESS/ARP_FAILURE                                 */
/******************************************************************************/
VOID
ArpIcchInitBulkReq (VOID)
{
    tIcchMsg             *pMsg = NULL;
    UINT2                 u2OffSet = 0;
   
    /*
     *    ARP Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     2     |    3       |
     *    |-------------------------
     *
     */
    
    pMsg = ICCH_ALLOC_TX_BUF (ARP_ICCH_BULK_REQ_MSG_LEN);
    if (pMsg == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "ICCH alloc failed. Unable to send ARP bulk request\r\n");
        return;
    }

    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, ARP_ICCH_BULK_REQUEST);
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, ARP_ICCH_BULK_REQ_MSG_LEN);
    
    if (ARP_FAILURE == ArpIcchSendMsgToIcch (pMsg, u2OffSet))
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
               "Failed to send bulk request to remote MC-LAG node\r\n");
    }

    return;
}

/******************************************************************************/
/*  Function Name   : ArpIcchSendBulkUpdates                                  */
/*                                                                            */
/*  Description     : This function sends bulk updates to remote MC-LAG node. */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : ARP_SUCCESS/ARP_FAILURE                                 */
/******************************************************************************/
VOID
ArpIcchSendBulkUpdates (VOID)
{
    tIcchMsg             *pMsg = NULL;
    t_ARP_CACHE          *pArpCache = NULL;
    t_ARP_CACHE           ArpCache;
    UINT2                 u2MsgLen = 0;
    UINT2                 u2OffSet = 0;
    UINT2                 u2VlanId = 0;
    UINT2                 u2TotalLenOffSet = 0;
    UINT2                 u2LenOffSet = 0;
    UINT2                 u2TotalLen = 0;

    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    
    pMsg = ICCH_ALLOC_TX_BUF (ARP_ICCH_MAX_MSG_SIZE);
    if (pMsg  == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "ICCH alloc failed. Unable to send ARP bulk request\r\n");
        return;
    }

    if (OSIX_FAILURE == IcchApiCheckProtoSyncEnabled ())
    {
        IcchSetBulkUpdatesStatus (ICCH_ARP_APP_ID);
        if (ARP_SUCCESS != ArpIcchSendBulkUpdTailMsg ())
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to send tail message to remote MC-LAG node\r\n");
        }
        ICCH_FREE (pMsg);
        return;
    }

    u2MsgLen = ARP_ICCH_IP_ADDR_SIZE + 
               ARP_ICCH_ARP_STATE_SIZE + ARP_ICCH_HW_ADDR_SIZE +
               ARP_ICCH_HW_TYPE_SIZE + ARP_ICCH_ROW_STATUS_SIZE +
               ARP_ICCH_VLAN_ID_SIZE;
               
    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, ARP_ICCH_BULK_UPDATE);
    u2TotalLenOffSet = u2OffSet;
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);

    while (pArpCache != NULL)
    {
        if ((pArpCache->i1State != ARP_DYNAMIC) ||
            (ArpIsMclagEnabled(pArpCache->u4IfIndex) == ARP_FAILURE) ||
            (CfaGetVlanId (pArpCache->u4IfIndex, &u2VlanId) != CFA_SUCCESS))
        {
            /* No need to sync entries which are learnt on non MC-LAG 
             * interface or non dynamic entries */ 

            ArpCache.u4Ip_addr = pArpCache->u4Ip_addr;
            ArpCache.u4IfIndex = pArpCache->u4IfIndex;
            pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                      (tRBElem *) & ArpCache, NULL);
            continue;
        } 
       
        ARP_ICCH_PUT_4_BYTE (pMsg, &u2OffSet, pArpCache->u4Ip_addr);
        ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->i1State);
        ARP_ICCH_PUT_N_BYTE (pMsg, &u2OffSet, pArpCache->i1Hw_addr,
                             CFA_ENET_ADDR_LEN);
        ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, pArpCache->i2Hardware);
        ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->u1RowStatus);
        ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2VlanId);

        ArpCache.u4Ip_addr = pArpCache->u4Ip_addr;
        ArpCache.u4IfIndex = pArpCache->u4IfIndex;

        u2TotalLen += u2MsgLen;
        
        u2LenOffSet = u2TotalLenOffSet;

        ARP_ICCH_PUT_2_BYTE (pMsg, &u2LenOffSet, u2TotalLen);

        pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                (tRBElem *) & ArpCache, NULL);

        if (ARP_FAILURE == ArpIcchCheckAndSendMsg (&pMsg, u2MsgLen, &u2OffSet))
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to send bulk request to remote MC-LAG node\r\n");
            return;
        }

        /* Once the buffer is full, sync-up messages are sent to remote MC-LAG
         * node. New buffer is allocated and offset is reset to zero. In such
         * case, add type and length field before values.
         */
        if (u2OffSet == 0)
        {
            ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, ARP_ICCH_BULK_UPDATE);
            u2TotalLenOffSet = u2OffSet;
            ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
            u2TotalLen = 0;
        }
    }

    if (u2OffSet != 0)
    {
        if (ARP_FAILURE == ArpIcchSendMsgToIcch (pMsg, u2OffSet))
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to send bulk request to remote MC-LAG node\r\n");
        }
    }

    IcchSetBulkUpdatesStatus (ICCH_ARP_APP_ID); 
    if (ARP_SUCCESS != ArpIcchSendBulkUpdTailMsg ())
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to send tail message to remote MC-LAG node\r\n");
    }
    return;
}

/******************************************************************************/
/*  Function Name   : ArpIcchProcessBulkInfo                                  */
/*                                                                            */
/*  Description     : This function processes bulk updates and updates        */
/*                    local ARP cache.                                        */
/*                                                                            */
/*  Input(s)        : pMsg          - pointer to ARP bulk sync-up message     */
/*                    pu2Offset     - pointerf to the offset                  */
/*                    u2TotalMsgLen - Total number of bytes in the bulk       */
/*                                    message                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
ArpIcchProcessBulkInfo (tIcchMsg *pMsg, UINT4 *pu4Offset, UINT2 u2TotalMsgLen)
{
    tARP_DATA           ArpInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tUtlInAddr          IpAddr; 
    UINT4               u4IfIndex = 0;
    UINT4               u4Port = 0;
    UINT2               u2VlanId = 0;

    MEMSET (&ArpInfo, 0, sizeof (tARP_DATA));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    while (*pu4Offset < u2TotalMsgLen)
    {
        ARP_ICCH_GET_4_BYTE (pMsg, pu4Offset, ArpInfo.u4IpAddr);
        ARP_ICCH_GET_1_BYTE (pMsg, pu4Offset, ArpInfo.i1State);
        ARP_ICCH_GET_N_BYTE (pMsg, pu4Offset, ArpInfo.i1Hw_addr,
                CFA_ENET_ADDR_LEN);
        ARP_ICCH_GET_2_BYTE (pMsg, pu4Offset, ArpInfo.i2Hardware);
        ARP_ICCH_GET_1_BYTE (pMsg, pu4Offset, ArpInfo.u1RowStatus);
        ARP_ICCH_GET_2_BYTE (pMsg, pu4Offset, u2VlanId);

        u4IfIndex = CfaGetVlanInterfaceIndex (u2VlanId);
        
        if(u4IfIndex == CFA_INVALID_INDEX)
        {
           ARP_TRC_ARG1 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                      "Failed to get Vlan Interface index for Vlan ID %d\r\n",
                       u2VlanId);

           return;
        }

        if (NETIPV4_FAILURE == NetIpv4GetPortFromIfIndex (u4IfIndex , &u4Port))
        {
           continue;
        }
        ArpInfo.u2Port =(UINT2) u4Port;

        if (NETIPV4_FAILURE == NetIpv4GetIfInfo (u4Port, &NetIpIfInfo))
        {
           continue;
        }             
        ArpInfo.u1EncapType = NetIpIfInfo.u1EncapType;
        ArpInfo.i1Hwalen = CFA_ENET_ADDR_LEN;
#if !defined (LNXIP4_WANTED)
        if (ARP_SUCCESS != arp_add (ArpInfo))
        {
            IpAddr.u4Addr = OSIX_NTOHL (ArpInfo.u4IpAddr);
            ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                          "Failed to add ARP entry for IP address %s and "
                          "VLAN %u in MC-LAG bulk processing\r\n", 
                          INET_NTOA (IpAddr), u2VlanId);

            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                         "Failed to add ARP entry for IP address %s and "
                         "VLAN %u in MC-LAG bulk processing\r\n", 
                         INET_NTOA (IpAddr), u2VlanId));
        }
#else
	/* While Synching Entries incase of Linux IP, Indicate kernel alone
	 * kernel will indicate control plane thorugh netlink socket. At that
	 * time entry will be programmed in control plane and NP */
	if (ARP_SUCCESS != ArpUpdateKernelEntrywithCOM (ARP_DYNAMIC,
				(UINT1 *)ArpInfo.i1Hw_addr,
				CFA_ENET_ADDR_LEN,
				ArpInfo.u4IpAddr,
				NetIpIfInfo.au1IfName))
	{
	    IpAddr.u4Addr = OSIX_NTOHL (ArpInfo.u4IpAddr);
	    ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
			   CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
			   "Failed to add ARP entry for IP address %s and VLAN %u "
			   "in MC-LAG sync-up processing\r\n",
			   INET_NTOA (IpAddr), u2VlanId);

	    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
			"Failed to add ARP entry for IP address %s and VLAN %u "
			"in MC-LAG sync-up processing\r\n",
			INET_NTOA (IpAddr), u2VlanId));
	}
#endif
    }

    return;
}

/*****************************************************************************/
/* Function Name      : ArpIcchCheckAndSendMsg                               */
/*                                                                           */
/* Description        : This function checks if enough room is exists in     */
/*                      the input buffer to accommodate the ARP sync-up      */
/*                      message. If no, the buffer is sent to ICCH.          */
/*                      else, returns from this function to append further   */
/*                      messages until the buffer is full.                   */
/*                                                                           */
/* Input(s)           : ppIcchMsg     - Message to be sent to ICCH           */
/*                      u2MsgLen      - Length of the message                */
/*                      pu2OffSet     - Current offset of the message        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE.                           */
/*****************************************************************************/
INT4
ArpIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, UINT2 *pu2OffSet)
{
    tIcchProtoEvt ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    if ((ARP_ICCH_MAX_MSG_SIZE - *pu2OffSet) < u2MsgLen)
    {
        /* No room for the current message. Send the messages in the buffer*/
        if (ARP_FAILURE == ArpIcchSendMsgToIcch (*ppIcchMsg, *pu2OffSet))
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                    CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                    "Failed to send bulk request to remote MC-LAG node\r\n");
        }
        
        *ppIcchMsg = ICCH_ALLOC_TX_BUF (ARP_ICCH_MAX_MSG_SIZE);

        if (*ppIcchMsg == NULL)
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "ICCH alloc failed. Unable to allocate \r\n");
            return ARP_FAILURE;
        }
        *pu2OffSet = 0;
    }
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpIcchSendBulkUpdTailMsg                            */
/*                                                                           */
/* Description        : This function sends tail message to remote MC-LAG    */
/*                      once bulk update is over. It is sent to notify that  */
/*                      ARP bulk message is sent to remote node. So, ICCH    */
/*                      can start bulk update for other modlues              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE.                           */
/*****************************************************************************/
INT4
ArpIcchSendBulkUpdTailMsg (VOID)
{
    tIcchMsg             *pMsg = NULL;
    UINT2                 u2OffSet = 0;

    /*
     *    ARP Tail message to indicate that bulk sync is over.
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     3     |    3       |
     *    |-------------------------
     *
     */
    
    pMsg = ICCH_ALLOC_TX_BUF (ARP_ICCH_TAIL_MSG_LEN);
    if (pMsg == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                  "ICCH alloc failed. Unable to send ARP tail message\r\n");
        return ARP_FAILURE;
    }

    ARP_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, ARP_ICCH_TAIL_MSG);
    ARP_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, ARP_ICCH_TAIL_MSG_LEN);

    if (ARP_FAILURE == ArpIcchSendMsgToIcch (pMsg, u2OffSet))
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
               "Failed to send ARP tail message\r\n");
    }

    return ARP_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : ArpIcchSendMsgToIcch                                    */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the ICCH  */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to RM module                 */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : ARP_SUCCESS/ARP_FAILURE                                 */
/******************************************************************************/
INT4
ArpIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal = 0;

    u4RetVal = IcchEnqMsgToIcchFromAppl (pMsg, u2Len, ICCH_ARP_APP_ID,
                                         ICCH_ARP_APP_ID);

    if (u4RetVal != ICCH_SUCCESS)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "Enqueuing of Messages from Appl to Icch failed \n");

        ICCH_FREE (pMsg);
        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

#endif  /*_ARPICCH_C_*/
