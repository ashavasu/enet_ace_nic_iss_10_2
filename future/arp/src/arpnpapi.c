
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: arpnpapi.c,v 1.7 2018/02/02 09:47:31 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of ARP module.
 ******************************************************************************/

#ifndef __ARP_NPAPI_C__
#define __ARP_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfArpDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfArpDel (UINT4 u4VrId, UINT4 u4IpAddr, UINT1 *pu1IfName,
                      INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_DEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpDel;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                      UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                      INT1 i1State, UINT4 *pu4TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pu4TblFull = pu4TblFull;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4SyncVlanAndL3Info                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SyncVlanAndL3Info
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SyncVlanAndL3Info
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4SyncVlanAndL3Info (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpL3Ipv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpL3Ipv4VrfArpAdd (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                        UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                        UINT1 *pbu1TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpAdd;

    pEntry->u4VrfId = u4VrfId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pbu1TblFull = pbu1TblFull;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfArpModify (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                         UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr,
                         UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpModify;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4PhyIfIndex = u4PhyIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpL3Ipv4VrfArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4VrfArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4VrfArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpL3Ipv4VrfArpModify (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                           UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4VrfArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_VRF_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpModify;

    pEntry->u4VrfId = u4VrfId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfArpGetNext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpGetNext
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpGetNext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfArpGetNext (tNpArpInput ArpNpInParam,
                          tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpGetNext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_GET_NEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGetNext;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfArpGet                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpGet
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpGet
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfArpGet (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpGet *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_GET,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGet;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfCheckHitOnArpEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfCheckHitOnArpEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfCheckHitOnArpEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfCheckHitOnArpEntry (UINT4 u4VrId, UINT4 u4IpAddress,
                                  UINT1 u1NextHopFlag)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfCheckHitOnArpEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpAddress = u4IpAddress;
    pEntry->u1NextHopFlag = u1NextHopFlag;
    pEntry->u1HitBitStatus = 0;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FALSE);
    }
    return (pEntry->u1HitBitStatus);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpIpv4VrfGetSrcMovedIpAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfGetSrcMovedIpAddr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfGetSrcMovedIpAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpIpv4VrfGetSrcMovedIpAddr (UINT4 *pu4VrId, UINT4 *pu4IpAddress)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfGetSrcMovedIpAddr;

    pEntry->pu4VrId = pu4VrId;
    pEntry->pu4IpAddress = pu4IpAddress;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpMbsmIpv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpMbsmIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                          UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsNpMbsmIpv4VrfArpAddition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrfArpAddition
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrfArpAddition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ArpFsNpMbsmIpv4VrfArpAddition (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                               UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                               UINT4 u4IfIndex, UINT1 *pu1IfName, INT1 i1State,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrfArpAddition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRF_ARP_ADDITION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAddition;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */
/***************************************************************************
 *
 *    Function Name       : ArpFsNpHwGetPortFromFdb
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpHwGetPortFromFdb
 *
 *    Input(s)            : Arguments of FsNpHwGetPortFromFdb
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
ArpFsNpHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *i1pHwAddr, UINT4 *pu4Port)
{
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsNpHwGetPortFromFdb *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VLAN_MOD,    /* Module ID */
                         FS_NP_HW_GET_PORT_FROM_FDB,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
    pEntry = &pVlanNpModInfo->VlanNpFsNpHwGetPortFromFdb;

    pEntry->u2VlanId = u2VlanId;
    pEntry->i1pHwAddr = i1pHwAddr;
    pEntry->pu4Port = pu4Port;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef CFA_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : ArpFsCfaHwRemoveIpNetRcvdDlfInHash                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Input(s)            : Arguments of IpFsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
ArpFsCfaHwRemoveIpNetRcvdDlfInHash (UINT4 u4ContextId, UINT4 u4IpNet,
                                    UINT4 u4IpMask)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IpNet = u4IpNet;
    pEntry->u4IpMask = u4IpMask;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
#if defined (MBSM_WANTED) && defined (CFA_WANTED)
/***************************************************************************
 *
 *    Function Name       : ArpFsNpCfaModifyFilter
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpCfaModifyFilter
 *
 *    Input(s)            : GO_ACTIVE/GO_STANDBY
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
ArpFsNpCfaModifyFilter (UINT1 u1Action)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsNpCfaModifyFilter *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_NP_CFA_MBSM_ARP_MODIFY_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsNpCfaModifyFilter;

    pEntry->u1Action = u1Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM WANTED */

#endif /* __ARP_NPAPI_C__ */
