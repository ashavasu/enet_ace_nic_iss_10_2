/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsarpwr.c,v 1.8 2015/09/15 06:46:24 siva Exp $
 *
 * Description: Wrapper routines for ARP objects
 *
 *******************************************************************/
#include "arpinc.h"
#include "fsarplw.h"
#include "fsarpwr.h"
#include "fsarpdb.h"

VOID
RegisterFSARP ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsarpOID, &fsarpEntry,
                                         ArpProtocolLock, ArpProtocolUnLock,
                                         ArpSetContext, ArpResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsarpOID, (const UINT1 *) "fsarp");
}

INT4
FsArpCacheTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsArpCacheTimeout (&(pMultiData->i4_SLongValue)));
}

INT4
FsArpCachePendTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsArpCachePendTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsArpMaxRetriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsArpMaxRetries (&(pMultiData->i4_SLongValue)));
}

INT4 FsArpPendingEntryCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsArpPendingEntryCount(&(pMultiData->i4_SLongValue)));
}
INT4 FsArpCacheEntryCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsArpCacheEntryCount(&(pMultiData->i4_SLongValue)));
}
INT4 FsArpRedEntryTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
            return(nmhGetFsArpRedEntryTime(&(pMultiData->i4_SLongValue)));
}
INT4 FsArpRedExitTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
            return(nmhGetFsArpRedExitTime(&(pMultiData->i4_SLongValue)));
}

INT4
FsArpCacheTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsArpCacheTimeout (pMultiData->i4_SLongValue));
}

INT4
FsArpCachePendTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsArpCachePendTime (pMultiData->i4_SLongValue));
}

INT4
FsArpMaxRetriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsArpMaxRetries (pMultiData->i4_SLongValue));
}

INT4
FsArpCacheTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsArpCacheTimeout (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsArpCachePendTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsArpCachePendTime (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsArpMaxRetriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsArpMaxRetries (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsArpCacheTimeoutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsArpCacheTimeout
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsArpCachePendTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsArpCachePendTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsArpMaxRetriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsArpMaxRetries (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsArpCacheFlushStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsArpCacheFlushStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsArpCacheFlushStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return(nmhSetFsArpCacheFlushStatus(pMultiData->i4_SLongValue,
                                       pMultiIndex->pIndex[0].i4_SLongValue));
}


INT4 FsArpCacheFlushStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsArpCacheFlushStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsArpCacheFlushStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsArpCacheFlushStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsArpGlobalDebugGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsArpGlobalDebug(&(pMultiData->i4_SLongValue)));
}

INT4 FsArpGlobalDebugSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsArpGlobalDebug(pMultiData->i4_SLongValue));
}

INT4 FsArpGlobalDebugTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsArpGlobalDebug(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FsArpGlobalDebugDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsArpGlobalDebug(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

