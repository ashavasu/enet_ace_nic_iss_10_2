/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564util.c,v 1.20 2017/02/27 13:52:09 siva Exp $
 *
 * Description: This file contains utility function for Y1564 module
 *
 *******************************************************************/

#ifndef __Y1564UTIL_C__
#define __Y1564UTIL_C__

#include "y1564inc.h"
#include <time.h>
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateSlaTable                          *
*                                                                           *
*    Description         : This function will create RBTree for SlaTable.   *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created SlaTable.                 *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create SlaTable.                  *
****************************************************************************/
INT4
Y1564UtilCreateSlaTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tSlaInfo, SlaNode);

    if ((Y1564_SLA_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilSlaTableCmpFn)) == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilCreateSlaTable : SlaTable creation "
                          "failed \r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilSlaCreateSlaNode                        *
*                                                                           *
*    Description         : This function will allocate memory for SLA node  *
*                          and initialize the default values to SLA         *
*                          strcuture                                        *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pSlaInfo - pointer to newly created SLA node.    *
*                                                                           *
****************************************************************************/
PUBLIC tSlaInfo *
Y1564UtilSlaCreateSlaNode (VOID)
{
   tSlaInfo         *pSlaEntry = NULL;

   if ((pSlaEntry = (tSlaInfo *)
        MemAllocMemBlk (gY1564GlobalInfo.SlaPoolId)) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilSlaCreateSlaNode : Memory Allocation "
                         "for Sla Entry failed\r\n");
       return NULL;
   }

   MEMSET (pSlaEntry, 0, sizeof (tSlaInfo));
   
   /*Assigning default values to SLA structure */
   pSlaEntry->u2SlaRateStep          = (INT4) (Y1564_MAX_RATE_STEP /
                                               Y1564_SLA_DEF_RATE_STEP);
   pSlaEntry->u4SlaConfTestDuration  = Y1564_SLA_DEF_DURATION;
   pSlaEntry->u1SlaConfTestStatus    = Y1564_SLA_DEF_STATUS;
   pSlaEntry->u1ColorMode            = Y1564_SLA_DEF_COLOR_MODE;
   pSlaEntry->u1SlaTrafPolicing      = Y1564_SLA_DEF_TRAFFIC_POLICING_STATUS;
   pSlaEntry->u1SlaConfTestSelector  = Y1564_ALL_TEST;
   pSlaEntry->u1SlaCurrentTestState  = Y1564_SLA_TEST_NOT_INITIATED;

   return pSlaEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilSlaAddNodeToSlaRBTree                       *
*                                                                           *
* Description        : This routine add a SLA Entry to a SLA Table          *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : pSlaEntry - New node added to the SLA Table          *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
*****************************************************************************/
PUBLIC INT4
Y1564UtilSlaAddNodeToSlaRBTree (tSlaInfo * pSlaEntry)
{
    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilSlaAddNodeToSlaRBTree : "
                          "SLA Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_SLA_TABLE (), (tRBElem *) (pSlaEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC 
                           | Y1564_RESOURCE_TRC," %s : Node Addition to "
                           "SlaTable failed for SlaId %d \r\n", 
                           __FUNCTION__,pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilSlaGetNodeFromSlaTable                      *
*                                                                           *
* Description        : This routine gets a SlaEntry from the SLA Table      *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSlaEntry - Pointer to the SLA node of SLA table.    *
*                                  returns NULL if node is not present in   *
*                                  SlaTable                                 *
****************************************************************************/
PUBLIC tSlaInfo  *
Y1564UtilSlaGetNodeFromSlaTable (tSlaInfo * pSlaEntry)
{
    return  (tSlaInfo *) RBTreeGet
        (Y1564_SLA_TABLE (), (tRBElem *) pSlaEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilSlaGetFirstNodeFromSlaTable                 *
*                                                                           *
* Description        : This routine will return the first node of the SLA   *
*                      Table                                                *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSlaEntry - Pointer to the first SLA node of SLA     *
*                                  Table.                                   *
*                                  returns NULL if node is not present in   *
*                                  SlaTable                                 *
****************************************************************************/
PUBLIC tSlaInfo  *
Y1564UtilSlaGetFirstNodeFromSlaTable (VOID)
{
    return  (tSlaInfo *) RBTreeGetFirst (Y1564_SLA_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilSlaGetNextNodeFromSlaTable                  *
*                                                                           *
* Description        : This routine will return next node of the SLA Table  *
*                                                                           *
* Input(s)           : pCurrentSlaEntry - Pointer to current SLA node       *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSlaNextEntry - Pointer to the next SLA node of SLA  *
*                                  table.                                   *
*                                  returns NULL if node is not present in   *
*                                  SlaTable                                 *
****************************************************************************/
PUBLIC tSlaInfo  *
Y1564UtilSlaGetNextNodeFromSlaTable (tSlaInfo * pCurrentSlaEntry)
{
    return  (tSlaInfo *) 
        RBTreeGetNext (Y1564_SLA_TABLE (), pCurrentSlaEntry, NULL);

}
/****************************************************************************
* Function Name      : Y1564UtilSlaDelNodeFromRBTree                        *
*                                                                           *
* Description        : This routine deletes SLA node from RBTree            *
*                      and releases the memory allocated for SLA Table.     *
*                                                                           *
* Input(s)           : pSlaEntry - Pointer to SLA node                      *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilSlaDelNodeFromRBTree (tSlaInfo * pSlaEntry)
{
    if (RBTreeRemove (Y1564_SLA_TABLE (), (tRBElem *) pSlaEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC 
                           | Y1564_MGMT_TRC," %s: Node deletion from" 
                           " SlaTable failed for SlaId %d \r\n", 
                           __FUNCTION__,pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_SLA_POOL(), (UINT1 *) pSlaEntry);
    pSlaEntry = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilSlaTableCmpFn                               *
*                                                                           *
* Description        : This routine is used in SLA Table for comparing two  *
*                      keys used in RBTree functionality.                   *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilSlaTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tSlaInfo          *pNode = NULL;
    tSlaInfo          *pNodeIn = NULL;

    pNode = (tSlaInfo *) Node;
    pNodeIn = (tSlaInfo *) NodeIn;

    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }
    if (pNode->u4SlaId < pNodeIn->u4SlaId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4SlaId > pNodeIn->u4SlaId)
    {
        return (Y1564_RB_GREATER);
    }

    return (Y1564_RB_EQUAL);
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilSlaGetSlaEntry                          *
*                                                                           *
*    Description         : This function will return the SLA entry based    *
*                          on the Context ID and Sla ID                     *
*                                                                           *
*    Input(s)            : u4ContextId  - Context Identifier                *
*                          u4SlaId     - SLA Identifier                     *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pSlaInfo - pointer to the SLA node of            *
*                                       SLA table.                          *
*                                       returns NULL, if node is not present*
*                                       in the SlaTable.                    *
*****************************************************************************/
PUBLIC tSlaInfo *
Y1564UtilSlaGetSlaEntry (UINT4 u4ContextId, UINT4 u4SlaId)
{
    tSlaInfo      *pSlaEntry = NULL;
    tSlaInfo  SlaEntry;

    MEMSET (&SlaEntry, 0, sizeof (tSlaInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);

        return NULL;
    }

    SlaEntry.u4ContextId = u4ContextId;
    SlaEntry.u4SlaId = u4SlaId;

    pSlaEntry = (Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry));

    return pSlaEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDeleteSlaTableInContext                     *
 *                                                                           *
 * Description        : This routine deletes SLA Table                       *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
VOID
Y1564UtilDeleteSlaTableInContext (UINT4 u4ContextId)
{
    tSlaInfo          *pSlaEntry = NULL;
    tSlaInfo          *pNextSlaEntry = NULL;

    pSlaEntry = Y1564UtilSlaGetFirstNodeFromSlaTable (); 

    while (pSlaEntry != NULL)
    {
        pNextSlaEntry = Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry);

        if (pSlaEntry->u4ContextId == u4ContextId)
        {
            pSlaEntry->u4SlaSacId = Y1564_ZERO;
            pSlaEntry->u4SlaTrafProfId = Y1564_ZERO;
            pSlaEntry->u2EvcId = Y1564_ZERO;
#ifndef MEF_WANTED
            pSlaEntry->u2SlaServiceConfId = Y1564_ZERO;
#endif
            /* RBTree deletion for SLA Table. */
            Y1564UtilDelSlaEntry (pSlaEntry);
        }

        pSlaEntry = pNextSlaEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelSlaEntry                                 *
 *                                                                           *
 * Description        : This routine delete a SLA Entry from SLA Table       *
 *                                                                           *
 * Input(s)           : pSlaEntry - node to be deleted                       *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
INT4
Y1564UtilDelSlaEntry (tSlaInfo * pSlaEntry)
{
    tConfTestReportInfo *pConfTestReportEntry = NULL;
    tConfTestReportInfo *pNextConfTestReportEntry = NULL;
    tPerfTestReportInfo *pPerfTestReportEntry = NULL;
    tPerfTestReportInfo *pNextPerfTestReportEntry = NULL;
    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilDelSlaEntry : SLA Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    /* Before deleting a SLA check if test is in progress 
     * Stop the test */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    { 
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Test is In-Progress for SLA Id %d "
                           "Either stop the test or wait for the "
                           "test to be completed \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaId);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return OSIX_FAILURE;
    }

    /* Check Statistics information is present for this SLA 
     * Delete all the available statistics before deleting the SLA */
    
    /* 1. Check Configuration test report is present for SLA
     *    If present then delete the statistics for this SLA
     *
     * 2. Check for Performance test report for the same SLA
     *    If present then delete the statistice for the same */

    pConfTestReportEntry = Y1564UtilGetFirstNodeFromConfTestReportTable();

    while (pConfTestReportEntry != NULL)
    {
        if ((pConfTestReportEntry->u4ContextId == pSlaEntry->u4ContextId) &&
            (pConfTestReportEntry->u4SlaId == pSlaEntry->u4SlaId))
        {
            if (Y1564UtilConfTestReportDelNodeFromRBTree (pConfTestReportEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId,
                                   Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC 
                                   | Y1564_RESOURCE_TRC,
                                   " %s : RBTree remove failed for SLA Id "
                                   "%d \r\n", __FUNCTION__,
                                   pConfTestReportEntry->u4SlaId);
                return OSIX_FAILURE;
            }
        }
        pNextConfTestReportEntry = Y1564UtilGetNextNodeFromConfTestReportTable
            (pConfTestReportEntry);
        pConfTestReportEntry = pNextConfTestReportEntry;
    }

    pPerfTestReportEntry = Y1564UtilGetFirstNodeFromPerfTestReportTable();

    while (pPerfTestReportEntry != NULL)
    {
        if ((pPerfTestReportEntry->u4ContextId == pSlaEntry->u4ContextId) &&
            (pPerfTestReportEntry->u4SlaId == pSlaEntry->u4SlaId))
        {
            if (Y1564UtilPerfTestReportDelNodeFromRBTree (pPerfTestReportEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId,
                                   Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC 
                                   | Y1564_RESOURCE_TRC,
                                   " %s : RBTree remove failed for SLA Id "
                                   "%d \r\n", __FUNCTION__,
                                   pPerfTestReportEntry->u4SlaId);
                return OSIX_FAILURE;
            }
        }
        pNextPerfTestReportEntry = Y1564UtilGetNextNodeFromPerfTestReportTable
            (pPerfTestReportEntry);
        pPerfTestReportEntry = pNextPerfTestReportEntry;
    }

    if (Y1564UtilSlaDelNodeFromRBTree (pSlaEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC 
                           | Y1564_RESOURCE_TRC,
                           " %s : RBTree remove failed for SLA Id "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u4SlaId);
        MemReleaseMemBlock (Y1564_SLA_POOL (), (UINT1 *) (pSlaEntry));
        pSlaEntry = NULL;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*--------  RBTREE UTILITIES FOR Traffic Profile Table -----------*/
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateTrafProfTable                     *
*                                                                           *
*    Description         : This function will create RBTree for             *
*                          Traffic profile Table.                           *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created Traffic profile Table     *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create Traffic profile Table      *
****************************************************************************/
INT4
Y1564UtilCreateTrafProfTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tTrafficProfileInfo, TrafProfNode);

    if ((Y1564_TRAF_PROF_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilTrafProfTableCmpFn)) == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilCreateTrafProfTable : " 
                          "Traffic profile Table creation failed \r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateTrafProfNode                      *
*                                                                           *
*    Description         : This function will allocate memory for Traffic   *
*                          Profile Table and initialize the default values  *
*                          to Traffic Profile Structure                     *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : tTrafficProfileInfo - pointer to newly created   *
*                                               Traf Prof Table             *
****************************************************************************/
PUBLIC tTrafficProfileInfo *
Y1564UtilCreateTrafProfNode (VOID)
{
   tTrafficProfileInfo         *pTrafProfEntry = NULL;

   if ((pTrafProfEntry = (tTrafficProfileInfo *)
        MemAllocMemBlk (Y1564_TRAF_PROF_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreateTrafProfNode : Memory Allocation "
                         "for Traffic Profile Entry failed \r\n");
       return NULL;
   }

   MEMSET (pTrafProfEntry, 0, sizeof (tTrafficProfileInfo));

   /* Assigning default values */
   pTrafProfEntry->TrafProfDirection = Y1564_TRAF_PROF_DEFAULT_DIRECTION;
   pTrafProfEntry->u2TrafProfPktSize = Y1564_TRAF_PROF_DEFAULT_PACKET_SIZE;
   MEMCPY(pTrafProfEntry->au1TrafProfOptEmixPktSize, gau1EmixDefaultPktSize,
          STRLEN(gau1EmixDefaultPktSize));
   return pTrafProfEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilAddNodeToTrafProfRBTree                     *
*                                                                           *
* Description        : This routine add a Traf Prof Entry to a Traffic      *
*                      Profile Table                                        *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : pTrafProfEntry - New node added to the Traf Prof     *
*                                       Table                               *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
*****************************************************************************/
PUBLIC INT4
Y1564UtilAddNodeToTrafProfRBTree (tTrafficProfileInfo * pTrafProfEntry)
{
    if (pTrafProfEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilAddNodeToTrafProfRBTree : "
                          "Traffic Profile Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_TRAF_PROF_TABLE (), (tRBElem *) (pTrafProfEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pTrafProfEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                           Y1564_RESOURCE_TRC," %s : Node Addition to "
                           "Traffic Profile Table failed for Id %d \r\n", 
                           __FUNCTION__,pTrafProfEntry->u4TrafProfId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilGetNodeFromTrafProfTable                    *
*                                                                           *
* Description        : This routine gets a Traffic Profile Entry from the   *
*                      Traffic Profile Table                                *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pTrafProfEntry - Pointer to the Traffic Profile node *
*                                       of Traffic Profile table.           *
*                                       returns NULL if node is not present *
*                                       in Traffic Profile Table            *
****************************************************************************/
PUBLIC tTrafficProfileInfo  *
Y1564UtilGetNodeFromTrafProfTable (tTrafficProfileInfo * pTrafProfEntry)
{
    return  (tTrafficProfileInfo *) RBTreeGet
        (Y1564_TRAF_PROF_TABLE (), (tRBElem *) pTrafProfEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilTrafProfDelNodeFromRBTree                   *
*                                                                           *
* Description        : This routine deletes Traffic Profile node from       *
*                      RBTree and releases the memory allocated for         *
*                      Traffic Profile Table                                *
*                                                                           *
* Input(s)           : pTrafProfEntry - Pointer to TrafProf node            *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilTrafProfDelNodeFromRBTree (tTrafficProfileInfo * pTrafProfEntry)
{
    if (RBTreeRemove (Y1564_TRAF_PROF_TABLE (), (tRBElem *) pTrafProfEntry) 
        == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pTrafProfEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion to "
                           "TrafProf Table failed for Id %d \r\n", 
                           __FUNCTION__, pTrafProfEntry->u4TrafProfId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_TRAF_PROF_POOL(), (UINT1 *) pTrafProfEntry);
    pTrafProfEntry = NULL;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : Y1564UtilTrafProfTableCmpFn                          *
 *                                                                           *
 * Description        : This routine is used in Traffic profile Table for    *
 *                      comparing two keys used in RBTree functionality.     *
 *                                                                           *
 * Input(s)           : pNode     - Key 1                                    *
 *                      pNodeIn   - Key 2                                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
 *                      0    -> when all key elements are Equal              *
 *****************************************************************************/
INT4
Y1564UtilTrafProfTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tTrafficProfileInfo    *pNode = NULL;
    tTrafficProfileInfo    *pNodeIn = NULL;

    pNode = (tTrafficProfileInfo *) Node;
    pNodeIn = (tTrafficProfileInfo *) NodeIn;

    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }
    if (pNode->u4TrafProfId < pNodeIn->u4TrafProfId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4TrafProfId > pNodeIn->u4TrafProfId)
    {
        return (Y1564_RB_GREATER);
  }
    return (Y1564_RB_EQUAL);
}


/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilGetTrafProfEntry                        *
*                                                                           *
*    Description         : This function will return the Traffic Profile    *
*                          entry based on the Context ID and Sla ID         *
*                                                                           *
*    Input(s)            : u4ContextId  - Context Identifier                *
*                          u4TrafProfId - Traffic Profile Identifier        *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : tTrafficProfileInfo - pointer to the Traffic     *
*                                                Profile node of Traffic    *
*                                                Profile table.             *
*                                                returns NULL, if node is   *
*                                                not present in the Traffic *
*                                                Profile Table              *
*****************************************************************************/
PUBLIC tTrafficProfileInfo *
Y1564UtilGetTrafProfEntry (UINT4 u4ContextId, UINT4 u4TrafProfId)
{
    tTrafficProfileInfo      *pTrafProfEntry = NULL;
    tTrafficProfileInfo      TrafProfEntry;

    MEMSET (&TrafProfEntry, 0, sizeof (tTrafficProfileInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);
        return NULL;
    }

    TrafProfEntry.u4ContextId = u4ContextId;
    TrafProfEntry.u4TrafProfId = u4TrafProfId;

    pTrafProfEntry = (Y1564UtilGetNodeFromTrafProfTable 
                                                 (&TrafProfEntry));

    return pTrafProfEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilGetFirstNodeFromTrafProfTable               *
*                                                                           *
* Description        : This routine will return the first node of the       *
*                      Traffic Profile Table                                *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pTrafProfEntry - Pointer to the first node of Traf   *
     *                                  Prof Table.                         *
*                                       returns NULL if node is not present *
*                                       in TrafProf Table                   *
****************************************************************************/
PUBLIC tTrafficProfileInfo  *
Y1564UtilGetFirstNodeFromTrafProfTable (VOID )
{
    return  (tTrafficProfileInfo *) RBTreeGetFirst (Y1564_TRAF_PROF_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilGetNextNodeFromTrafProfTable                * 
*                                                                           *
* Description        : This routine will return next node of the Traf Prof  *
*                      Table                                                *
*                                                                           *
* Input(s)           : pCurrentSlaEntry - Pointer to current SLA node       *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pTrafProfNextEntry - Pointer to the next node of     * 
*                                           TrafProf table.                 *
*                                           returns NULL if node is not     *
*                                           present in TrafProfTable        *
****************************************************************************/
PUBLIC tTrafficProfileInfo  *
Y1564UtilGetNextNodeFromTrafProfTable (tTrafficProfileInfo * 
                                               pCurrentTrafProfEntry)
{
    return  (tTrafficProfileInfo *) 
        RBTreeGetNext (Y1564_TRAF_PROF_TABLE (), pCurrentTrafProfEntry, NULL);

}

/*****************************************************************************
 * Function Name      : Y1564UtilDeleteTrafProfTableInContext                *
 *                                                                           *
 * Description        : This routine deletes Traffic Table                   *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
VOID
Y1564UtilDeleteTrafProfTableInContext (UINT4 u4ContextId)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;
    tTrafficProfileInfo          *pNextTrafProfEntry = NULL;

    pTrafProfEntry = Y1564UtilGetFirstNodeFromTrafProfTable ();

    while (pTrafProfEntry != NULL)
    {
        pNextTrafProfEntry = Y1564UtilGetNextNodeFromTrafProfTable (pTrafProfEntry);

        if (pTrafProfEntry->u4ContextId == u4ContextId)
        {
            /* RBTree deletion for SAC Table. */
            Y1564UtilDelTrafProfEntry (pTrafProfEntry);
        }

        pTrafProfEntry = pNextTrafProfEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelTrafProfEntry                            *
 *                                                                           *
 * Description        : This routine delete a Traf profile Entry from        *
 *                      traffic profile Table                                *
 *                                                                           *
 * Input(s)           : pTrafProfEntry - node to be deleted                  *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
INT4
Y1564UtilDelTrafProfEntry (tTrafficProfileInfo * pTrafProfEntry)
{
    UINT4 u4SlaId = Y1564_ZERO;

    if (pTrafProfEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564DelTrafProfEntry : Traffic Profile Entry "
                          "is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilValidateTrafProfAndSla (pTrafProfEntry->u4ContextId, u4SlaId,
                                         pTrafProfEntry->u4TrafProfId) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pTrafProfEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Deletion not allowed when "
                           "Traf prof entry %d mapped to SLA "
                           "and test is in-progress for that SLA \r\n", 
                           __FUNCTION__, pTrafProfEntry->u4TrafProfId);
        return OSIX_FAILURE;
    }

    if (Y1564UtilTrafProfDelNodeFromRBTree (pTrafProfEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pTrafProfEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC
                           | Y1564_RESOURCE_TRC,
                           " %s : RBTree remove failed for Traffic Profile Id "
                           "%d \r\n", __FUNCTION__,pTrafProfEntry->u4TrafProfId);
        MemReleaseMemBlock (Y1564_TRAF_PROF_POOL (), (UINT1 *) (pTrafProfEntry));
        pTrafProfEntry = NULL;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
/********************************************************************************
 * FUNCTION NAME    : Y1564UtilValidateTrafProfAndSla                           *
 *                                                                              *
 * DESCRIPTION      : This function will check whether the given Traf prof is   *
 *                    associated with SLA and check for the SLA test current    *
 *                    state.                                                    *
 *                                                                              *
 * INPUT            : u4ContextId - Context Identifier                          *
 *                    u4SlaId - SLA Identifier                                  *
 *                    u4TrafProfId - Traffic Profile Identifier                 *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PUBLIC INT4
Y1564UtilValidateTrafProfAndSla (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4TrafProfId)
{
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo SlaEntry;

    MEMSET( &SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    SlaEntry.u4SlaId = u4SlaId;
    SlaEntry.u4ContextId = u4ContextId;

    pSlaEntry = &SlaEntry;

    /* Traffic Profile configuration/deletion will be allowed when any one of the below
       conditions are met.

       1. SAC has associated with SLA but test is either in not-initiate or aborted
       or complete state
       2. SAC is not associated to any of the SLA.

       To achieve this, SLA entry is fetched from SLA table
       and SAC Id values are validated.

       If the SAC is associated with SLA, then current test
       state of the SLA is validated . If the SLA state is In-Progress
       then SAC configuration/deletion will not be allowed. */

    while ((pSlaEntry =
            Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry)) != NULL)
    {
        /* If Sla Entry is found for the SAC entry context, proceed further
           otherwise exit from the while loop */
        if (pSlaEntry->u4ContextId != u4ContextId)
        {
            break;
        }

        if (pSlaEntry->u4SlaTrafProfId == u4TrafProfId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Test is "
                                   "in-progress for Taffic profile Id %d "
                                   "Either test has to be stopped or wait for "
                                   "the test to be completed \r\n",
                                   __FUNCTION__,u4TrafProfId);
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return OSIX_FAILURE;
            }

            else
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC,
                                   " %s : De-Associate Traffic Profile Id "
                                   "from SLA \r\n",__FUNCTION__,
                                   pSlaEntry->u4SlaId);
                CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_TRAF_PROF_ENTRY);
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/*--------  RBTREE UTILITIES FOR SAC Table -----------*/

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateSacTable                          *
*                                                                           *
*    Description         : This function will create RBTree for SacTable.   *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created SacTable.                 *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create SacTable.                  *
***************************************************************************/
INT4
Y1564UtilCreateSacTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tSacInfo, SacNode);

    if ((Y1564_SAC_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilSacTableCmpFn)) == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CreateSacTable : SacTable creation failed\r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilSacCreateSacNode                        *
*                                                                           *
*    Description         : This function will allocate memory for SAC node  *
*                          and initialize the default values to SAC         *
*                          strcuture                                        *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pSacInfo - pointer to newly created SAC node.    *
*                                                                           *
****************************************************************************/
PUBLIC tSacInfo *
Y1564UtilSacCreateSacNode (VOID)
{
   tSacInfo         *pSacEntry = NULL;

   if ((pSacEntry = (tSacInfo *)
        MemAllocMemBlk (Y1564_SAC_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilSacCreateSacNode : Memory Allocation "
                         "for Sac Entry failed\r\n");
       return NULL;
   }

   MEMSET (pSacEntry, 0, sizeof (tSacInfo));
   
   return pSacEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilSacAddNodeToSacRBTree                       *
*                                                                           *
* Description        : This routine add a SAC Entry to a SAC Table          *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : pSacEntry - New node added to the SAC Table          *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
*****************************************************************************/
PUBLIC INT4
Y1564UtilSacAddNodeToSacRBTree (tSacInfo * pSacEntry)
{
    if (pSacEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilSacAddNodeToSacRBTree : " 
                          "Sac Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_SAC_TABLE (), (tRBElem *) (pSacEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSacEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                           Y1564_RESOURCE_TRC, " %s : Node Addition to "
                           "SacTable failed for SacId %d \r\n", 
                           __FUNCTION__,pSacEntry->u4SacId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilSacGetNodeFromSacTable                      *
*                                                                           *
* Description        : This routine gets a SacEntry from the SAC Table      *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSacEntry - Pointer to the SAC node of SAC table.    *
*                                  returns NULL if node is not present in   *
*                                  SacTable                                 *
****************************************************************************/
PUBLIC tSacInfo  *
Y1564UtilSacGetNodeFromSacTable (tSacInfo * pSacEntry)
{
    return  (tSacInfo *) RBTreeGet
        (Y1564_SAC_TABLE (), (tRBElem *) pSacEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilSacGetFirstNodeFromSacTable                 *
*                                                                           *
* Description        : This routine will return the first node of the SAC   *
*                      Table                                                *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSacEntry - Pointer to the first SAC node of SAC     *
*                                  Table.                                   *
*                                  returns NULL if node is not present in   *
*                                  SacTable                                 *
****************************************************************************/
PUBLIC tSacInfo  *
Y1564UtilSacGetFirstNodeFromSacTable (VOID)
{
    return  (tSacInfo *) RBTreeGetFirst (Y1564_SAC_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilSacGetNextNodeFromSacTable                  *
*                                                                           *
* Description        : This routine will return next node of the SAC Table  *
*                                                                           *
* Input(s)           : pCurrentSacEntry - Pointer to current SAC node       *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pSacNextEntry - Pointer to the next SAC node of SAC  *
*                                  table.                                   *
*                                  returns NULL if node is not present in   *
*                                  SacTable                                 *
****************************************************************************/
PUBLIC tSacInfo  *
Y1564UtilSacGetNextNodeFromSacTable (tSacInfo * pCurrentSacEntry)
{
    return  (tSacInfo *) 
        RBTreeGetNext (Y1564_SAC_TABLE (), pCurrentSacEntry, NULL);

}

/****************************************************************************
* Function Name      : Y1564UtilSacDelNodeFromRBTree                        *
*                                                                           *
* Description        : This routine deletes SAC node from RBTree            *
*                      and releases the memory allocated for SAC Table.     *
*                                                                           *
* Input(s)           : pSacEntry - Pointer to SAC node                      *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilSacDelNodeFromRBTree (tSacInfo * pSacEntry)
{
    if (RBTreeRemove (Y1564_SAC_TABLE (), (tRBElem *) pSacEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSacEntry->u4ContextId, 
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion to SacTable "
                           "failed for SacId %d \r\n", __FUNCTION__,
                           pSacEntry->u4SacId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_SAC_POOL(), (UINT1 *) pSacEntry);
    pSacEntry = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilSacTableCmpFn                               *
*                                                                           *
* Description        : This routine is used in SAC Table for comparing two  *
*                      keys used in RBTree functionality.                   *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilSacTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tSacInfo          *pNode = NULL;
    tSacInfo          *pNodeIn = NULL;

    pNode = (tSacInfo *) Node; 
    pNodeIn = (tSacInfo *) NodeIn; 

    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }
    if (pNode->u4SacId < pNodeIn->u4SacId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4SacId > pNodeIn->u4SacId)
    {
        return (Y1564_RB_GREATER);
    }
    return (Y1564_RB_EQUAL);
}

/****************************************************************************
* Function Name      : Y1564UtilSacGetSacEntry                              *
*                                                                           *
* Description        : This routine gets a SacEntry for the corresponding   *
*                      the Context ID and Sla ID                            *
*                                                                           *
* Input(s)           : u4ContextId - Context Identifier                     *
*                      u4SacId   - SAC Identifier                           *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Returns            : pSacInfo - pointer to the SAC node of                *
*                                 SAC table.                                *
*                                 returns NULL, if node is not present      *
*                                 in the SacTable.                          *
                                                                            *
*****************************************************************************/
tSacInfo          *
Y1564UtilSacGetSacEntry (UINT4 u4ContextId, UINT4 u4SacId)
{
    tSacInfo          *pSacEntry = NULL;
    tSacInfo           SacEntry;

    MEMSET (&SacEntry, Y1564_ZERO, sizeof (tSacInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);
        return NULL;
    }

    SacEntry.u4ContextId = u4ContextId;
    SacEntry.u4SacId = u4SacId;

    pSacEntry = Y1564UtilSacGetNodeFromSacTable (&SacEntry);

    return pSacEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDeleteSacTableInContext                     *
 *                                                                           *
 * Description        : This routine deletes SAC Table                       *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
VOID
Y1564UtilDeleteSacTableInContext (UINT4 u4ContextId)
{
    tSacInfo          *pSacEntry = NULL;
    tSacInfo          *pNextSacEntry = NULL;

    pSacEntry = Y1564UtilSacGetFirstNodeFromSacTable ();

    while (pSacEntry != NULL)
    {
        pNextSacEntry = Y1564UtilSacGetNextNodeFromSacTable (pSacEntry);

        if (pSacEntry->u4ContextId == u4ContextId)
        {
            /* RBTree deletion for SAC Table. */
            Y1564UtilDelSacEntry (pSacEntry);
        }

        pSacEntry = pNextSacEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelSacEntry                                 *
 *                                                                           *
 * Description        : This routine delete a SAC Entry from SAC Table       *
 *                                                                           *
 * Input(s)           : pSacEntry - node to be deleted                       *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                           *
 *****************************************************************************/
INT4
Y1564UtilDelSacEntry (tSacInfo * pSacEntry)
{

    UINT4 u4SlaId = Y1564_ZERO;

    if (pSacEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564DelSacEntry : SAC Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilValidateSacAndSla (pSacEntry->u4ContextId,
                                    u4SlaId,
                                    pSacEntry->u4SacId) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSacEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_RESOURCE_TRC,
                           " %s : Deletion not allowed when "
                           "SAC Profile Id %d mapped with "
                           "SLA and test is in-progress\r\n",
                           __FUNCTION__,pSacEntry->u4SacId);
                           
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_SAC_EXISTS);
        return OSIX_FAILURE;
    }

    if (Y1564UtilSacDelNodeFromRBTree (pSacEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSacEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_RESOURCE_TRC,
                           " %s : RBTree remove failed for SAC %d \r\n",
                           __FUNCTION__,pSacEntry->u4SacId);
        MemReleaseMemBlock (Y1564_SAC_POOL (), (UINT1 *) (pSacEntry));
        pSacEntry = NULL;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/********************************************************************************
 * FUNCTION NAME    : Y1564UtilValidateSacAndSla                              *
 *                                                                              *
 * DESCRIPTION      : This function will check whether the given SAC is         *
 *                    associated with SLA and check for the SLA test current    *
 *                    state.                                                    *
 *                                                                              *
 * INPUT            : u4ContextId - Context Identifier                          *
 *                    u4SlaId - SLA Identifier                                  *
 *                    u4SacId - SAC Identifier                                  *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PUBLIC INT4
Y1564UtilValidateSacAndSla (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4SacId)
{
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo SlaEntry;

    MEMSET( &SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    SlaEntry.u4SlaId = u4SlaId;
    SlaEntry.u4ContextId = u4ContextId;

    pSlaEntry = &SlaEntry;

    /* SAC configuration/deletion will be allowed when any one of the below
       conditions are met.

       1. SAC is associated to SLA but test is either in not-initiate, aborted
       and complete state
       2. SAC is not associated to any of the SLA.

       To achieve this, SLA entry is fetched from SLA table
       and SAC Id values are validated.

       If the SAC is associated with SLA, then current test
       state of the SLA is validated . If the SLA state is In-Progress
       then SAC configuration/deletion will not be allowed. */

    while ((pSlaEntry =
            Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry)) != NULL)
    {
        /* If Sla Entry is found for the SAC entry context, proceed further
           otherwise exit from the while loop */
        if (pSlaEntry->u4ContextId != u4ContextId)
        {
            break;
        }

        if (pSlaEntry->u4SlaSacId == u4SacId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : SAC entry associated with SLA "
                                   "test is in-progress.either test has to be "
                                   "stopped or wait for the test to be "
                                   "completed \r\n",__FUNCTION__);
               CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return OSIX_FAILURE;
            }
            else
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : De-Associate SAC Id from SLA "
                                   "and then Modify/Update SAC entry \r\n",
                                   __FUNCTION__,pSlaEntry->u4SlaId);
                CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_SAC_ENTRY);
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

#ifdef MEF_WANTED
/****************************************************************************
* Function Name      : Y1564UtilGetEvcEntry                                 *
*                                                                           *
* Description        : This routine validates whether the given EVC Index   *
*                      is present in MEF or not.                            *
*                                                                           *
* Input(s)           : u4ContextId - context Identifier                     *
*                      u4EvcIndex  - Evc Identifier                         *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
INT4 Y1564UtilGetEvcEntry (UINT4 u4ContextId, UINT4 u4EvcIndex)
{
   INT4 i4RetVal = OSIX_FAILURE;

   i4RetVal = MefUtilCheckEvcEntryPresence (u4ContextId, u4EvcIndex);

   return i4RetVal;
}

/****************************************************************************
* Function Name      : Y1564UtilGetBandwidthProfile                         *
*                                                                           *
* Description        : This routine fetches the bandwidth profile           *
*                      parameters from MEF for the given EVC Index          *
*                                                                           *
* Input(s)           : pSlaEntry - Pointer to SLA structure                 *
*                      u4ContextId - context Identifier                     *
*                      u4EvcIndex  - Evc Identifier                         *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
INT4 Y1564UtilGetBandwidthProfile (tSlaInfo *pSlaEntry, 
                                   UINT4 u4ContextId, UINT4 u4EvcIndex)
{
   INT4 i4RetVal = OSIX_FAILURE;

   i4RetVal = MefUtilGetBanwidthProfile (u4ContextId, u4EvcIndex,
                                         &pSlaEntry->u4CIR, &pSlaEntry->u4CBS,
                                         &pSlaEntry->u4EIR, &pSlaEntry->u4EBS,
                                         &pSlaEntry->u1ColorMode,
                                         &pSlaEntry->u1CoupFlag);
                                       
   return i4RetVal;
}
#endif

#ifndef MEF_WANTED
/*--------  RBTREE UTILITIES FOR Service Conf Table -----------*/

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateServConfTable                     *
*                                                                           *
*    Description         : This function will create RBTree for             *
*                          ServiceConfTable                                 *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created ServiceConfTable.         *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create ServiceConfTable.          *
****************************************************************************/
INT4
Y1564UtilCreateServConfTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tServiceConfInfo, ServiceConfNode);

    if ((Y1564_SERV_CONF_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilServConfTableCmpFn)) == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CreateServConfTable "
                          "ServiceConfTable creation failed\r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateServConfNode                      *
*                                                                           *
*    Description         : This function will allocate memory for ServConf  *
*                          node and initialize the default values to        *
*                          service Config strcuture                         *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pServiceConfInfo - pointer to newly created      *
*                                             ServiceConfnode.              *
*                                                                           *
****************************************************************************/
PUBLIC tServiceConfInfo *
Y1564UtilCreateServConfNode (VOID)
{
   tServiceConfInfo         *pServiceConfEntry = NULL;

   if ((pServiceConfEntry = (tServiceConfInfo *)
        MemAllocMemBlk (Y1564_SERV_CONF_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreateServConfNode: "
                         "Memory Allocation for Service Conf Entry "
                         "failed\r\n");
       return NULL;
   }

   MEMSET (pServiceConfEntry, 0, sizeof (tServiceConfInfo));

   /*Assigning default values to SERVICE CONF structure */
   pServiceConfEntry->u1ServiceConfColorMode = Y1564_SERV_CONF_COLOR_BLIND;
   pServiceConfEntry->u1ServiceConfCoupFlag = Y1564_COUPLING_TYPE_DECOUPLED;
 
   return pServiceConfEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilAddNodeToServConfRBTree                     *
*                                                                           *
* Description        : This routine add a ServiceConf Entry to a Service    *
*                      Conf Table                                           *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : pServiceConfEntry - New node added to the            *
*                                          service Conf Table               *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
*****************************************************************************/
PUBLIC INT4
Y1564UtilAddNodeToServConfRBTree (tServiceConfInfo * pServiceConfEntry)
{
    if (pServiceConfEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilAddNodeToServConfRBTree : "
                          "Service Conf Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_SERV_CONF_TABLE (), 
                   (tRBElem *) (pServiceConfEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pServiceConfEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                           Y1564_RESOURCE_TRC, " %s: Node Addition to "
                           "ServConfTable failed for Id %d \r\n", 
                           __FUNCTION__, pServiceConfEntry->u4ServiceConfId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilGetNodeFromServConfTable                    *
*                                                                           *
* Description        : This routine gets a ServiceConfEntry from the        *
*                      service configuration Table                          *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pServiceConfEntry - Pointer to the service conf      *
*                                          node of ServiceConf table.       *
*                                          returns NULL if node is not      * 
*                                          present in ServiceConf Table     *
****************************************************************************/
PUBLIC tServiceConfInfo  *
Y1564UtilGetNodeFromServConfTable (tServiceConfInfo * pServiceConfEntry)
{
    return  (tServiceConfInfo *) RBTreeGet
        (Y1564_SERV_CONF_TABLE (), (tRBElem *) pServiceConfEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilGetFirstNodeFromServConfTable               *
*                                                                           *
* Description        : This routine will return the first node of the       *
*                      ServiceConf Table                                    *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pServiceConfEntry - Pointer to the first node of     *
*                                          Service Conf Table.              *
*                                          returns NULL if node is not      *
*                                          present in ServiceConf           *
*                                          Table                            *
****************************************************************************/
PUBLIC tServiceConfInfo  *
Y1564UtilGetFirstNodeFromServConfTable (VOID)
{
    return  (tServiceConfInfo *) RBTreeGetFirst (Y1564_SERV_CONF_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilGetNextNodeFromServConfTable                *
*                                                                           *
* Description        : This routine will return next node of the Service    *
*                      conf Table                                           *
*                                                                           *
* Input(s)           : pCurrentServiceConfEntry - Pointer to current        *
*                                                 ServiceConf  node         *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pServiceConfNextEntry - Pointer to the next service  *
*                                              conf node of ServiceConf     *
*                                              table.                       *
*                                              returns NULL if node is not  * 
*                                              present in ServiceConf       *
*                                              Table                        *
****************************************************************************/
PUBLIC tServiceConfInfo  *
Y1564UtilGetNextNodeFromServConfTable (tServiceConfInfo * pCurrentServiceConfEntry)
{
    return  (tServiceConfInfo *) 
        RBTreeGetNext (Y1564_SERV_CONF_TABLE (), pCurrentServiceConfEntry, NULL);

}
/****************************************************************************
* Function Name      : Y1564UtilServConfDelNodeFromRBTree                   *
*                                                                           *
* Description        : This routine deletes Service Conf node from RBTree   *
*                      and releases the memory allocated for                *
*                      Service Conf Table.                                  *
*                                                                           *
* Input(s)           : pServiceConfEntry - Pointer to Service Conf node     *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilServConfDelNodeFromRBTree (tServiceConfInfo * pServiceConfEntry)
{
    if (RBTreeRemove (Y1564_SERV_CONF_TABLE (), 
                      (tRBElem *) pServiceConfEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pServiceConfEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion from " 
                           "ServiceConfTable failed for Id %d \r\n", 
                           __FUNCTION__,pServiceConfEntry->u4ServiceConfId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_SERV_CONF_POOL(), (UINT1 *) pServiceConfEntry);
    pServiceConfEntry = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilServConfTableCmpFn                          *
*                                                                           *
* Description        : This routine is used in ServiceConf Table for        *
*                      comparing two keys used in RBTree functionality.     *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilServConfTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tServiceConfInfo          *pNode = NULL;
    tServiceConfInfo          *pNodeIn = NULL;

    pNode = (tServiceConfInfo *) Node;
    pNodeIn = (tServiceConfInfo *) NodeIn;

    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }
    if (pNode->u4ServiceConfId < pNodeIn->u4ServiceConfId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ServiceConfId > pNodeIn->u4ServiceConfId)
    {
        return (Y1564_RB_GREATER);
    }

    return (Y1564_RB_EQUAL);
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564GetServConfEntry                            *
*                                                                           *
*    Description         : This function will return the Service            *
*                          Configuration entry based                        *
*                          on the Context ID and ServiceConfId              *
*                                                                           *
*    Input(s)            : u4ContextId     - Context Identifier             *
*                          u4ServiceConfId - SLA Identifier                 *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pServiceConfInfo - pointer to the Service Conf   *
*                                             node of Service Conf Table    *
*                                             returns NULL, if node is not  *
*                                             present in the ServiceConf    *
*                                             Table.                        *
*****************************************************************************/
PUBLIC tServiceConfInfo *
Y1564UtilGetServConfEntry (UINT4 u4ContextId, UINT4 u4ServiceConfId)
{
    tServiceConfInfo      *pServiceConfEntry = NULL;
    tServiceConfInfo  ServiceConfEntry;

    MEMSET (&ServiceConfEntry, 0, sizeof (tServiceConfInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);
        return NULL;
    }

    ServiceConfEntry.u4ContextId = u4ContextId;
    ServiceConfEntry.u4ServiceConfId = u4ServiceConfId;

    pServiceConfEntry = (Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry));

    return pServiceConfEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDeleteServConfTableInContext                *
 *                                                                           *
 * Description        : This routine deletes SAC Table                       *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
VOID
Y1564UtilDeleteServConfTableInContext (UINT4 u4ContextId)
{
    tServiceConfInfo          *pServConfEntry = NULL;
    tServiceConfInfo          *pNextServConfEntry = NULL;

    pServConfEntry = Y1564UtilGetFirstNodeFromServConfTable ();

    while (pServConfEntry != NULL)
    {
        pNextServConfEntry = Y1564UtilGetNextNodeFromServConfTable (pServConfEntry);

        if (pServConfEntry->u4ContextId == u4ContextId)
        {
            /* RBTree deletion for Service Configuration Table. */
            Y1564UtilDelServConfEntry (pServConfEntry);
        }

        pServConfEntry = pNextServConfEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelServConfEntry                            *
 *                                                                           *
 * Description        : This routine delete a Service Configuration          *
 *                      Entry from Service Configuration Table               *
 *                                                                           *
 * Input(s)           : pServConfEntry - node to be deleted                  *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                           *
 *****************************************************************************/
INT4
Y1564UtilDelServConfEntry (tServiceConfInfo * pServConfEntry)
{
    UINT4 u4SlaId = Y1564_ZERO;

    if (pServConfEntry == NULL)
    {
        Y1564_GLOBAL_TRC  ("Y1564DelServConfEntry : Service Configuration "
                           "Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilValidateServConfAndSla (pServConfEntry->u4ContextId, u4SlaId, 
                                         pServConfEntry->u4ServiceConfId) != 
        OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pServConfEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_RESOURCE_TRC,
                           " %s : Deletion not allowed when Service "
                           "Configuration Id %d mapped to "
                           "SLA %d test is in-progress\r\n",
                           __FUNCTION__, pServConfEntry->u4ServiceConfId, u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_SERVICE_CONFIG_EXISTS);
        return OSIX_FAILURE;

    }
    if (Y1564UtilServConfDelNodeFromRBTree (pServConfEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pServConfEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_RESOURCE_TRC,
                           " %s : RBTree remove failed for SAC %d \r\n",
                           __FUNCTION__,pServConfEntry->u4ServiceConfId);
        MemReleaseMemBlock (Y1564_SERV_CONF_POOL (), (UINT1 *) (pServConfEntry));
        pServConfEntry = NULL;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}


/********************************************************************************
 * FUNCTION NAME    : Y1564UtilValidateServConfAndSla                         *
 *                                                                              *
 * DESCRIPTION      : This function will check whether the given Service Config *
 *                    associated with SLA and check for the SLA test current    *
 *                    state.                                                    *
 *                                                                              *
 * INPUT            : u4ContextId - Context Identifier                          *
 *                    u4SlaId - SLA Identifier                                  *
 *                    u4ServConfId - Traffic Profile Identifier                 *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PUBLIC INT4
Y1564UtilValidateServConfAndSla (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4ServConfId)
{
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo  SlaEntry;

    MEMSET( &SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    SlaEntry.u4SlaId = u4SlaId;
    SlaEntry.u4ContextId = u4ContextId;

    pSlaEntry = &SlaEntry;
    /* Service configuration/deletion will be allowed when any one of the below
       conditions are met.

       1. Service Config has associated with SLA but test is either in 
          not-initiate or aborted or complete state
       2. Service Config is not associated to any of the SLA.

       To achieve this, SLA entry is fetched from SLA table
       and ServiceConfig Id values are validated.

       If the Service Config is associated with SLA, then current test
       state of the SLA is validated . If the SLA state is In-Progress
       then Service configuration/deletion will not be allowed. */

    while ((pSlaEntry =
            Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry)) != NULL)
    {
        /* If Sla Entry is found for the SAC entry context, proceed further
           otherwise exit from the while loop */
        if (pSlaEntry->u4ContextId != u4ContextId)
        {
            break;
        }

        if (pSlaEntry->u2SlaServiceConfId == u4ServConfId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {

                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : Service Conf Id %d associated to SLA "
                                   "%d and SLA test is in-progress.either test "
                                   "has to be stopped or wait for the test "
                                   "completion \r\n", __FUNCTION__, u4ServConfId,
                                   pSlaEntry->u4SlaId);
               CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return OSIX_FAILURE;
            }
            else
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : Service Conf Id %d is associated to "
                                   "Sla Id %d ,de-associate it from SLA \r\n",
                                   __FUNCTION__,u4ServConfId);
                CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_SERVICE_CONF_ENTRY);
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

#endif
/*--------  RBTREE UTILITIES FOR Performance Test Table -----------*/

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreatePerfTestTable                     *
*                                                                           *
*    Description         : This function will create RBTree for             *
*                          PerfTestTable.                                   *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created PerfTestTable.            *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create PerfTestTable.             *
****************************************************************************/
INT4
Y1564UtilCreatePerfTestTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tPerfTestInfo, Y1564PerfTestNode);

    if ((Y1564_PERF_TEST_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilPerfTestTableCmpFn)) 
        == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilCreatePerfTestTable : "
                          "PerfTestTable creation failed\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreatePerfTestNode                      *
*                                                                           *
*    Description         : This function will allocate memory for Perf Test *
*                          node and initialize the default values to        *
*                          PerfTest strcuture                               *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pPerfTestInfo - pointer to newly created         *
*                                          PerfTest node.                   *
*                                                                           *
****************************************************************************/
PUBLIC tPerfTestInfo *
Y1564UtilCreatePerfTestNode (VOID)
{
   tPerfTestInfo         *pPerfTestEntry = NULL;

   if ((pPerfTestEntry = (tPerfTestInfo *)
        MemAllocMemBlk (Y1564_PERF_TEST_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreatePerfTestNode : "
                         "Memory Allocation for PerfTest Entry failed\r\n");
       return NULL;
   }

   MEMSET (pPerfTestEntry, 0, sizeof (tPerfTestInfo));

   if ((pPerfTestEntry->pau1SlaList =
       MemAllocMemBlk (gY1564GlobalInfo.SlaListPoolId)) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreatePerfTestNode : Memory Allocation "
                         "for Sla List Entry failed\r\n");
       MemReleaseMemBlock (Y1564_SLA_POOL (), (UINT1 *)pPerfTestEntry);
       return NULL;
   }

   MEMSET (pPerfTestEntry->pau1SlaList, 0, sizeof (tY1564SlaList));

   /*Assigning default values to Perf Test structure */
   pPerfTestEntry->u1PerfTestDuration = Y1564_TEST_DURATION_2HOUR;
   pPerfTestEntry->u1PerfTestStatus  = Y1564_PERF_TEST_STOP; 

   return pPerfTestEntry;
}

/****************************************************************************
* Function Name      : Y1564UtilAddNodeToPerfTestRBTree                     *
*                                                                           *
* Description        : This routine add a Perf Test Entry to a PerfTest     *
*                      Table                                                *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : pPerfTestEntry - New node added to the PerfTest      *
*                                       Table                               *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
*****************************************************************************/
PUBLIC INT4
Y1564UtilAddNodeToPerfTestRBTree (tPerfTestInfo * pPerfTestEntry)
{
    if (pPerfTestEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilAddNodeToPerfTestRBTree : "
                          "PerfTest Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_PERF_TEST_TABLE (), (tRBElem *) (pPerfTestEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_RESOURCE_TRC,
                           " %s : Node Addition to PerfTestTable "
                           "failed for PerfTestId %d \r\n", 
                            __FUNCTION__,pPerfTestEntry->u4PerfTestId);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilGetNodeFromPerfTestTable                    *
*                                                                           *
* Description        : This routine gets a PerfTestEntry from               *
*                      PerfTestTable                                        *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestEntry - Pointer to the PerfTest node of     *
*                                       PerfTest table.                     *
*                                       returns NULL if node is not         *
*                                       present in PerfTestTable            *
****************************************************************************/
PUBLIC tPerfTestInfo  *
Y1564UtilGetNodeFromPerfTestTable (tPerfTestInfo * pPerfTestEntry)
{
    return  (tPerfTestInfo *) RBTreeGet
        (Y1564_PERF_TEST_TABLE (), (tRBElem *) pPerfTestEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilGetFirstNodeFromPerfTestTable               *
*                                                                           *
* Description        : This routine will return the first node of the       *
*                      PerfTest Table                                       *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestEntry - Pointer to the first PerfTest node  *
*                                       of PerfTestTable.                   *
*                                       returns NULL if node is not present *
*                                       in PerfTestTable                    *
****************************************************************************/
PUBLIC tPerfTestInfo  *
Y1564UtilGetFirstNodeFromPerfTestTable (VOID)
{
    return  (tPerfTestInfo *) RBTreeGetFirst (Y1564_PERF_TEST_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilGetNextNodeFromPerfTestTable                *
*                                                                           *
* Description        : This routine will return next node of the            *
*                      PerfTestTable                                        *
*                                                                           *
* Input(s)           : pCurrentPerfTestEntry - Pointer to current           *
*                                              PerfTest node                *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestNextEntry - Pointer to the next             *
*                                           PerfTest node of PerfTest       *
*                                           table.                          *
*                                           returns NULL if node is not     *
*                                           present in PerfTestTable        *
****************************************************************************/
PUBLIC tPerfTestInfo  *
Y1564UtilGetNextNodeFromPerfTestTable (tPerfTestInfo * 
                                               pCurrentPerfTestEntry)
{
    return  (tPerfTestInfo *) 
        RBTreeGetNext (Y1564_PERF_TEST_TABLE (), pCurrentPerfTestEntry, NULL);

}
/****************************************************************************
* Function Name      : Y1564UtilPerfTestDelNodeFromRBTree                   *
*                                                                           *
* Description        : This routine deletes PerfTest node from RBTree       *
*                      and releases the memory allocated for PerfTest Table.*
*                                                                           *
* Input(s)           : pPerfTestEntry - Pointer to PerfTest node            *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilPerfTestDelNodeFromRBTree (tPerfTestInfo * pPerfTestEntry)
{
    if (RBTreeRemove (Y1564_PERF_TEST_TABLE (), 
                      (tRBElem *) pPerfTestEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion to PerfTestTable "
                           " failed for PerfTestId %d \r\n", __FUNCTION__,
                           pPerfTestEntry->u4PerfTestId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_SLA_LIST_POOL(), (UINT1 *) pPerfTestEntry->pau1SlaList);
    pPerfTestEntry->pau1SlaList = NULL;
    MemReleaseMemBlock (Y1564_PERF_TEST_POOL(), (UINT1 *) pPerfTestEntry);
    pPerfTestEntry = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilPerfTestTableCmpFn                          *
*                                                                           *
* Description        : This routine is used in PerfTest Table for comparing *
*                      two keys used in RBTree functionality.               *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilPerfTestTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tPerfTestInfo          *pNode = NULL;
    tPerfTestInfo          *pNodeIn = NULL;

    pNode = (tPerfTestInfo *) Node;
    pNodeIn = (tPerfTestInfo *) NodeIn;

    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }
    if (pNode->u4PerfTestId < pNodeIn->u4PerfTestId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4PerfTestId > pNodeIn->u4PerfTestId)
    {
        return (Y1564_RB_GREATER);
    }

    return (Y1564_RB_EQUAL);
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilGetPerfTestEntry                        *
*                                                                           *
*    Description         : This function will return the PerfTest           *
*                          entry based on the Context ID and PerfTest ID    *
*                                                                           *
*    Input(s)            : u4ContextId  - Context Identifier                *
*                          u4PerfTestId - PerfTest Identifier               *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pPerfTestInfo - pointer to the PerfTest node of  *
*                                          PerfTest table.                  *
*                                          returns NULL, if node is not     *
*                                          present in the PerfTestTable.     *
*****************************************************************************/
PUBLIC tPerfTestInfo *
Y1564UtilGetPerfTestEntry (UINT4 u4ContextId, UINT4 u4PerfTestId)
{
    tPerfTestInfo      *pPerfTestEntry = NULL;
    tPerfTestInfo       PerfTestEntry;

    MEMSET (&PerfTestEntry, 0, sizeof (tPerfTestInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);
        return NULL;
    }

    PerfTestEntry.u4ContextId = u4ContextId;
    PerfTestEntry.u4PerfTestId = u4PerfTestId;

    pPerfTestEntry = (Y1564UtilGetNodeFromPerfTestTable (&PerfTestEntry));

    return pPerfTestEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDeletePerfTestTableInContext                *
 *                                                                           *
 * Description        : This routine deletes SAC Table                       *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
VOID
Y1564UtilDeletePerfTestTableInContext (UINT4 u4ContextId)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;
    tPerfTestInfo          *pNextPerfTestEntry = NULL;

    pPerfTestEntry = Y1564UtilGetFirstNodeFromPerfTestTable ();

    while (pPerfTestEntry != NULL)
    {
        pNextPerfTestEntry = Y1564UtilGetNextNodeFromPerfTestTable (pPerfTestEntry);

        if (pPerfTestEntry->u4ContextId == u4ContextId)
        {
            /* RBTree deletion for Service Configuration Table. */
            Y1564UtilDelPerfTestEntry (pPerfTestEntry);
        }

        pPerfTestEntry = pNextPerfTestEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelPerfTestEntry                            *
 *                                                                           *
 * Description        : This routine delete the Performance Test             *
 *                      Entry from the performance test Table                *
 *                                                                           *
 * Input(s)           : pPerfTestEntry - node to be deleted                  *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                           *
 *****************************************************************************/
INT4
Y1564UtilDelPerfTestEntry (tPerfTestInfo * pPerfTestEntry)
{
    if (pPerfTestEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564DelPerfTestEntry : Performance Test "
                          "table Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilValidateSlaTestState (pPerfTestEntry) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC ," %s : Deletion not "
                           "allowed when test is in-progress for "
                           "one of the SLA Id in Performance SLA "
                           "list Id %d \r\n",__FUNCTION__,
                           pPerfTestEntry->u4PerfTestId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return OSIX_FAILURE;
    }

    if (Y1564UtilPerfTestDelNodeFromRBTree (pPerfTestEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : RBTree remove failed for PerfId %d \r\n",
                           __FUNCTION__,pPerfTestEntry->u4PerfTestId);
        MemReleaseMemBlock (Y1564_PERF_TEST_POOL (), (UINT1 *) (pPerfTestEntry));
        pPerfTestEntry = NULL;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
/********************************************************************************
 * FUNCTION NAME    : Y1564UtilValidateSlaTestState                             *
 *                                                                              *
 * DESCRIPTION      : This function will check whether test is in-progress      *
 *                    for any one of the SLA present in SLA List                *
 *                                                                              *
 * INPUT            : pPerfTestEntry  -  Pointer to perf entry                  *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PUBLIC INT4
Y1564UtilValidateSlaTestState (tPerfTestInfo * pPerfTestEntry)
{
    tSlaInfo     *pSlaEntry = NULL;
    tSlaInfo     SlaEntry;
    UINT2        u2Index = Y1564_ONE;
    BOOL1        bResult = Y1564_ZERO;

    MEMSET(&SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    if (pPerfTestEntry->u2SlaListCount != Y1564_ZERO)
    {
        for (; u2Index <= pPerfTestEntry->u2SlaListCount; u2Index++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)),
                                     u2Index, sizeof (tY1564SlaList),
                                     bResult);
           if (bResult == OSIX_FALSE)
           {
              continue;
           }

           else
           {
               SlaEntry.u4SlaId = u2Index;
               SlaEntry.u4ContextId = pPerfTestEntry->u4ContextId;

               pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

               if (pSlaEntry != NULL)
               {
                    /* when service performance test in in-progress
                     * deletion of performance test table is not
                     * allowed. either test has to be stopped in middle
                     * of the execution or have to wait for the test
                     * completion.

                     * When none of the service configuration
                     * test is initiated for this SLA then Test Mode will be
                     * Perf test and state will be in progress, it means service
                     * performance test is initiated for this SLA */

                     if ((pSlaEntry->u1SlaCurrentTestState ==
                          Y1564_SLA_TEST_IN_PROGRESS) &&
                         (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
                     {
                          Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                             Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                             " %s : Perf Test is in-progress "
                                             "for SLA  Id  %d\r\n",
                                             __FUNCTION__,pSlaEntry->u4SlaId);
                          CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                          return OSIX_FAILURE;
                     }
               }
           }  
        }
    }
    return OSIX_SUCCESS;
}

/*--------  RBTREE UTILITIES For Configuration Test Report Table -----------*/

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateConfTestReportTable               *
*                                                                           *
*    Description         : This function will create RBTree for             *
*                          ConfTestReportTable.                             *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created ConfTestReportTable.      *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create ConfTestReportTable.       *
****************************************************************************/
INT4
Y1564UtilCreateConfTestReportTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tConfTestReportInfo, Y1564ConfTestReportNode);

    if ((Y1564_CONF_REPORT_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilConfTestReportTableCmpFn)) 
        == NULL)
    {
        Y1564_GLOBAL_TRC (" Y1564UtilCreateConfTestReportTable : "
                          "Configuration Test Report Table "
                          "creation failed \r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreateConfTestReportNode                *
*                                                                           *
*    Description         : This function will allocate memory for           *
*                          Configuration Test report table to store the     *
*                          test result                                      *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pConfTestReportInfo - pointer to newly created   *
*                                          ConfTestReport node.             *
*                                                                           *
****************************************************************************/
PUBLIC tConfTestReportInfo *
Y1564UtilCreateConfTestReportNode (VOID)
{
   tConfTestReportInfo         *pConfTestReportEntry = NULL;

   if ((pConfTestReportEntry = (tConfTestReportInfo *)
        MemAllocMemBlk (Y1564_CONF_REPORT_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreateConfTestReportNode : "
                         "Memory Allocation for ConfTestReport Entry "
                         "failed \r\n");
       return NULL;
   }

   MEMSET (pConfTestReportEntry, 0, sizeof (tConfTestReportInfo));

   return pConfTestReportEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilAddNodeToConfTestReportRBTree               *
 *                                                                           *
 * Description        : This routine will add Conf Test Report Entry         * 
 *                      to ConfTestReport Table                              *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pConfTestReportEntry - New node to be added          *
 *                                             to Table                      *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
PUBLIC INT4
Y1564UtilAddNodeToConfTestReportRBTree (tConfTestReportInfo * pConfTestReportEntry)
{
    if (pConfTestReportEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilAddNodeToConfTestReportRBTree : "
                          "Configuration Test Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_CONF_REPORT_TABLE (), 
                   (tRBElem *) (pConfTestReportEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId, Y1564_ALL_FAILURE_TRC 
                           | Y1564_RESOURCE_TRC," %s: Node Addition to "
                           "Conf Test Report failed for SlaId %d \r\n",
                           __FUNCTION__, pConfTestReportEntry->u4SlaId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilConfTestReportDelNodeFromRBTree             *
*                                                                           *
* Description        : This routine deletes node from RBTree                *
*                      and releases the memory allocated for Table.         *
*                                                                           *
* Input(s)           : pConfTestReportEntry - Pointer to configuration      *
*                                             test report node              *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilConfTestReportDelNodeFromRBTree (tConfTestReportInfo * pConfTestReportEntry)
{
    if (RBTreeRemove (Y1564_CONF_REPORT_TABLE (), 
                      (tRBElem *) pConfTestReportEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion to Configuration test report "
                           "Table failed for Index SLA Id %d Mode  %d Step Id %d \r\n", 
                           __FUNCTION__, pConfTestReportEntry->u4SlaId,
                           pConfTestReportEntry->u4CurrentTestMode,
                           pConfTestReportEntry->u4StatsStepId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_CONF_REPORT_POOL(), (UINT1 *) pConfTestReportEntry);
    pConfTestReportEntry = NULL;

    return OSIX_SUCCESS;
}
/****************************************************************************
* Function Name      : Y1564UtilConfTestReportTableCmpFn                    *
*                                                                           *
* Description        : This routine is used in Conf Test Report Table       *
*                      for comparing two keys used in RBTree                *
*                      functionality.                                       *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilConfTestReportTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tConfTestReportInfo          *pNode = NULL;
    tConfTestReportInfo          *pNodeIn = NULL;

    pNode = (tConfTestReportInfo *) Node;
    pNodeIn = (tConfTestReportInfo *) NodeIn;

    /* Context Id */
    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }

    /* SLA Id */
    if (pNode->u4SlaId < pNodeIn->u4SlaId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4SlaId > pNodeIn->u4SlaId)
    {
        return (Y1564_RB_GREATER);
    }
    /* Frame Size*/
    if (pNode->i4FrameSize < pNodeIn->i4FrameSize)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->i4FrameSize > pNodeIn->i4FrameSize)
    {
        return (Y1564_RB_GREATER);
    }

    /* Test Mode */
    if (pNode->u4CurrentTestMode < pNodeIn->u4CurrentTestMode)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4CurrentTestMode > pNodeIn->u4CurrentTestMode)
    {
        return (Y1564_RB_GREATER);
    }

    /* Step Id */
    if (pNode->u4StatsStepId < pNodeIn->u4StatsStepId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4StatsStepId > pNodeIn->u4StatsStepId)
    {
        return (Y1564_RB_GREATER);
    }


    return (Y1564_RB_EQUAL);
}
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilGetConfTestReportEntry                  *
*                                                                           *
*    Description         : This function will return the Configuration      *
*                          TestReport entry based on the ContextId,         *
*                          SlaId, TestMode, RateStep Id                     *
*                                                                           *
*    Input(s)            : u4ContextId       - Context Identifier           *
*                          u4SlaId           - Sla Identifier               *
*                          u4StatsStepId     - Rate Step Identifier         *
*                          u4CurrentTestMode - Test Mode                    *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pConfTestReportEntry - pointer to the ConfTest   *
*                                                 Reporttable.              *
*                                                 returns NULL, if node is  *
*                                                 not present in the        *
*                                                 ConfTestReportTable.      *
*****************************************************************************/
PUBLIC tConfTestReportInfo *
Y1564UtilGetConfTestReportEntry (UINT4 u4ContextId, UINT4 u4SlaId,
                                 INT4 i4FrameSize,UINT4 u4CurrentTestMode, 
                                 UINT4 u4StatsStepId)
{
    tConfTestReportInfo      *pConfTestReportEntry = NULL;
    tConfTestReportInfo       ConfTestReportEntry;
    tSlaInfo                  *pSlaEntry = NULL;

    MEMSET (&ConfTestReportEntry, 0, sizeof (tConfTestReportInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " does not exist \r\n",__FUNCTION__);
        return NULL;
    }

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);
    
    if (pSlaEntry == NULL)
    {
       Y1564_GLOBAL_TRC ("Y1564UtilGetConfTestReportEntry : SlaEntry "
                         "is NULL \r\n");
       return SNMP_FAILURE;
    }

    ConfTestReportEntry.u4ContextId = u4ContextId;
    ConfTestReportEntry.u4SlaId = u4SlaId;
    ConfTestReportEntry.u4CurrentTestMode = u4CurrentTestMode;
    ConfTestReportEntry.u4StatsStepId = u4StatsStepId;
    ConfTestReportEntry.i4FrameSize = i4FrameSize;

    pConfTestReportEntry = (Y1564UtilGetNodeFromConfTestReportTable (&ConfTestReportEntry));

    return pConfTestReportEntry;
}


/****************************************************************************
* Function Name      : Y1564UtilGetNodeFromConfTestReportTable              *
*                                                                           *
* Description        : This routine gets a ConfTestReportEntry from the     *
*                      configuration test report Table                      *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pConfTestReportEntry - Pointer to the configuration  *
*                                          test report of ConfTestReport    *
*                                          returns NULL if node is not      *
*                                          present in ConfTestReport Table  *
****************************************************************************/
PUBLIC tConfTestReportInfo  *
Y1564UtilGetNodeFromConfTestReportTable (tConfTestReportInfo *
                                         pConfTestReportEntry)
{
    return  (tConfTestReportInfo *) RBTreeGet
        (Y1564_CONF_REPORT_TABLE (), (tRBElem *) pConfTestReportEntry);

}

/****************************************************************************
* Function Name      : Y1564UtilGetFirstNodeFromConfTestReportTable         *
*                                                                           *
* Description        : This routine will return the first node of the       *
*                      ConfTestReportTable                                  *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pConfTestReportEntry - Pointer to the first node     *
*                                             of ConfTestReportTable.       *
*                                             returns NULL if node is not   *
*                                             present in                    *
*                                             ConfTestReportTable           *
****************************************************************************/
PUBLIC tConfTestReportInfo  *
Y1564UtilGetFirstNodeFromConfTestReportTable (VOID)
{
    return  (tConfTestReportInfo *) RBTreeGetFirst (Y1564_CONF_REPORT_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilGetNextNodeFromConfTestReportTable          *
*                                                                           *
* Description        : This routine will return next node of the            *
*                      conftestreport Table                                 *
*                                                                           *
* Input(s)           : pCurrentConfTestReportEntry - Pointer to current     *
*                                                    node of table          *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pConfTestReportNextEntry - Pointer to the next node  *
*                                                 of ConfTesReportTable.    *
*                      returns NULL if node is not present in               *
*                      ConfTestReportTable                                  *
****************************************************************************/
PUBLIC tConfTestReportInfo  *
Y1564UtilGetNextNodeFromConfTestReportTable (tConfTestReportInfo * 
                                             pCurrentConfTestReportEntry)
{
    return  (tConfTestReportInfo *)
        RBTreeGetNext (Y1564_CONF_REPORT_TABLE (), pCurrentConfTestReportEntry, NULL);

}

/*--------  RBTREE UTILITIES For Performance Test Report Table -----------*/
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreatePerfTestReportTable               *
*                                                                           *
*    Description         : This function will create RBTree for             *
*                          PerfTestReportTable.                             *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created PerfTestReportTable.      *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create PerfTestReportTable.       *
****************************************************************************/
INT4
Y1564UtilCreatePerfTestReportTable (VOID)
{
    UINT4               u4RBNodeOffset = Y1564_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tPerfTestReportInfo, Y1564PerfTestReportNode);

    if ((Y1564_PERF_REPORT_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, Y1564UtilPerfTestReportTableCmpFn)) 
        == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilCreatePerfTestReportTable : "
                          "Performance Test Report Table "
                          "creation failed \r\n");

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilCreatePerfTestReportNode                *
*                                                                           *
*    Description         : This function will allocate memory for           *
*                          performance Test report table to store the     *
*                          test result                                      *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pPerfTestReportInfo - pointer to newly created   *
*                                          PerfTestReport node.             *
*                                                                           *
****************************************************************************/
PUBLIC tPerfTestReportInfo *
Y1564UtilCreatePerfTestReportNode (VOID)
{
   tPerfTestReportInfo         *pPerfTestReportEntry = NULL;

   if ((pPerfTestReportEntry = (tPerfTestReportInfo *)
        MemAllocMemBlk (Y1564_PERF_REPORT_POOL ())) == NULL)
   {
       gY1564GlobalInfo.u4MemFailCount++;
       Y1564_GLOBAL_TRC ("Y1564UtilCreatePerfTestReportNode : "
                         "Memory Allocation for PerfTestReport Entry failed \r\n");
       return NULL;
   }

   MEMSET (pPerfTestReportEntry, 0, sizeof (tPerfTestReportInfo));

   return pPerfTestReportEntry;
}

/*****************************************************************************
 * Function Name      : Y1564UtilAddNodeToPerfTestReportRBTree               *
 *                                                                           *
 * Description        : This routine will add Perf Test Report Entry         * 
 *                      to PerfTestReport Table                              *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pPerfTestReportEntry - New node to be added          *
 *                                             to Table                      *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
PUBLIC INT4
Y1564UtilAddNodeToPerfTestReportRBTree (tPerfTestReportInfo * pPerfTestReportEntry)
{
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564UtilAddNodeToPerfTestReportRBTree : "
                          "Performance Test Entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (Y1564_PERF_REPORT_TABLE (), 
                   (tRBElem *) (pPerfTestReportEntry)) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId, Y1564_ALL_FAILURE_TRC 
                           | Y1564_RESOURCE_TRC," %s: Node Addition to "
                           "Perf Test Report failed for SlaId %d \r\n",
                           __FUNCTION__, pPerfTestReportEntry->u4SlaId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilPerfTestReportDelNodeFromRBTree             *
*                                                                           *
* Description        : This routine deletes node from RBTree                *
*                      and releases the memory allocated for Table.         *
*                                                                           *
* Input(s)           : pPerffTestReportEntry - Pointer to configuration      *
*                                             test report node              *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
****************************************************************************/
PUBLIC INT4
Y1564UtilPerfTestReportDelNodeFromRBTree (tPerfTestReportInfo * pPerfTestReportEntry)
{
    if (RBTreeRemove (Y1564_PERF_REPORT_TABLE (), 
                      (tRBElem *) pPerfTestReportEntry) == RB_FAILURE)
    {
        Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Node deletion to performance test report "
                           "Table failed for Index SLA Id %d Perf Id %d \r\n", 
                           __FUNCTION__, pPerfTestReportEntry->u4SlaId,
                           pPerfTestReportEntry->u4StatsPerfId);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_PERF_REPORT_POOL(), (UINT1 *) pPerfTestReportEntry);
    pPerfTestReportEntry = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function Name      : Y1564UtilPerfTestReportTableCmpFn                    *
*                                                                           *
* Description        : This routine is used in Perf Test Report Table       *
*                      for comparing two keys used in RBTree                *
*                      functionality.                                       *
*                                                                           *
* Input(s)           : pNode     - Key 1                                    *
*                      pNodeIn   - Key 2                                    *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : -1/1 -> when any key element is Less/Greater         *
*                      0    -> when all key elements are Equal              *
****************************************************************************/
INT4
Y1564UtilPerfTestReportTableCmpFn (tRBElem * Node, tRBElem * NodeIn)
{
    tPerfTestReportInfo          *pNode = NULL;
    tPerfTestReportInfo          *pNodeIn = NULL;

    pNode = (tPerfTestReportInfo *) Node;
    pNodeIn = (tPerfTestReportInfo *) NodeIn;

    /* Context Id */
    if (pNode->u4ContextId < pNodeIn->u4ContextId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4ContextId > pNodeIn->u4ContextId)
    {
        return (Y1564_RB_GREATER);
    }

    /* SLA Id */
    if (pNode->u4SlaId < pNodeIn->u4SlaId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4SlaId > pNodeIn->u4SlaId)
    {
        return (Y1564_RB_GREATER);
    }

    /* Performance Test Index */
    if (pNode->u4StatsPerfId < pNodeIn->u4StatsPerfId)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->u4StatsPerfId > pNodeIn->u4StatsPerfId)
    {
        return (Y1564_RB_GREATER);
    }

    /* Frame Size*/
    if (pNode->i4FrameSize < pNodeIn->i4FrameSize)
    {
        return (Y1564_RB_LESSER);
    }
    else if (pNode->i4FrameSize > pNodeIn->i4FrameSize)
    {
        return (Y1564_RB_GREATER);
    }

    return (Y1564_RB_EQUAL);
}
/****************************************************************************
*                                                                           *
*    Function Name       : Y1564UtilGetPerfTestReportEntry                  *
*                                                                           *
*    Description         : This function will return the performance        *
*                          TestReport entry based on the ContextId,         *
*                          SlaId,PerformanceTestIndex                       *
*                                                                           *
*    Input(s)            : u4ContextId       - Context Identifier           *
*                          u4SlaId           - Sla Identifier               *
*                          u4StatsPerfId     - Performance Test Id          *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : pPerfTestReportEntry - pointer to the PerfTest   *
*                                                 Reporttable.              *
*                                                 returns NULL, if node is  *
*                                                 not present in the        *
*                                                 PerfTestReportTable.      *
*****************************************************************************/
PUBLIC tPerfTestReportInfo *
Y1564UtilGetPerfTestReportEntry (UINT4 u4ContextId, UINT4 u4SlaId,
                                 UINT4 u4StatsPerfId,
                                 INT4 i4FrameSize)
{
    tPerfTestReportInfo      *pPerfTestReportEntry = NULL;
    tPerfTestReportInfo       PerfTestReportEntry;
    tSlaInfo                  *pSlaEntry = NULL;

    MEMSET (&PerfTestReportEntry, 0, sizeof (tPerfTestReportInfo));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId ,Y1564_ALL_FAILURE_TRC, " %s : "
                           "does not exist\r\n",__FUNCTION__);
        return NULL;
    }

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);
    
    if (pSlaEntry == NULL)
    {
       Y1564_GLOBAL_TRC ("Y1564UtilGetPerfTestReportEntry : "
                         "SlaEntry is NULL \r\n");
       return SNMP_FAILURE;
    }

    PerfTestReportEntry.u4ContextId = u4ContextId;
    PerfTestReportEntry.u4SlaId = u4SlaId;
    PerfTestReportEntry.u4StatsPerfId = u4StatsPerfId;
    PerfTestReportEntry.i4FrameSize = i4FrameSize;

    pPerfTestReportEntry = (Y1564UtilGetNodeFromPerfTestReportTable 
                                                   (&PerfTestReportEntry));

    return pPerfTestReportEntry;
}       
/****************************************************************************
* Function Name      : Y1564UGetNodeFromPerfTestReportTable                 *
*                                                                           *
* Description        : This routine gets a PerfTestReportEntry from the     *
*                      configuration test report Table                      *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestReportEntry - Pointer to the configuration  *
*                                          test report of PerfTestReport    *
*                                          returns NULL if node is not      *
*                                          present in PerfTestReport Table  *
****************************************************************************/
PUBLIC tPerfTestReportInfo  *
Y1564UtilGetNodeFromPerfTestReportTable (tPerfTestReportInfo *
                                         pPerfTestReportEntry)
{
    return  (tPerfTestReportInfo *) RBTreeGet
        (Y1564_PERF_REPORT_TABLE (), (tRBElem *) pPerfTestReportEntry);

}
/****************************************************************************
* Function Name      : Y1564UtilGetFirstNodeFromPerfTestReportTable         *
*                                                                           *
* Description        : This routine will return the first node of the       *
*                      PerfTestReportTable                                  *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestReportEntry - Pointer to the first node     *
*                                             of PerfTestReportTable.       *
*                                             returns NULL if node is not   *
*                                             present in                    *
*                                             PerfTestReportTable           *
****************************************************************************/
PUBLIC tPerfTestReportInfo  *
Y1564UtilGetFirstNodeFromPerfTestReportTable (VOID)
{
    return  (tPerfTestReportInfo *) RBTreeGetFirst (Y1564_PERF_REPORT_TABLE ());

}

/****************************************************************************
* Function Name      : Y1564UtilGetNextNodeFromPerfTestReportTable          *
*                                                                           *
* Description        : This routine will return next node of the            *
*                      conftestreport Table                                 *
*                                                                           *
* Input(s)           : pCurrentPerfTestReportEntry - Pointer to current     *
*                                                    node of table          *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pPerfTestReportNextEntry - Pointer to the next node  *
*                                                 of ConfTesReportTable.    *
*                      returns NULL if node is not present in               *
*                      PerfTestReportTable                                  *
****************************************************************************/
PUBLIC tPerfTestReportInfo  *
Y1564UtilGetNextNodeFromPerfTestReportTable (tPerfTestReportInfo * 
                                             pCurrentPerfTestReportEntry)
{
    return  (tPerfTestReportInfo *)
        RBTreeGetNext (Y1564_PERF_REPORT_TABLE (), pCurrentPerfTestReportEntry, NULL);

}

#ifdef TRACE_WANTED
/*****************************************************************************
 *    Function Name       : Y1564UtilContextTrc                              *
 *                                                                           *
 *    Description         : This function prints the context traces          *
 *                                                                           *
 *    Input(s)            : Context Identifier,Trace Mask                    *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None                                             *
 ****************************************************************************/
VOID
Y1564UtilContextTrc (UINT4 u4ContextId, UINT4 u4Mask, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         ac1Buf[Y1564_TRC_MAX_SIZE];
    UINT4               u4OffSet = 0;

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));

    if (Y1564CxtGetContextEntry (u4ContextId) == NULL)
    {
        return;
    }

    SPRINTF (ac1Buf, "[Switch:%s]",
             Y1564_CONTEXT_NAME (u4ContextId));
    u4OffSet = STRLEN (ac1Buf);

    va_start (ap, fmt);
    vsprintf (&ac1Buf[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcLog (Y1564_MODULE_TRACE(u4ContextId), u4Mask,
               Y1564_MODULE_NAME, ac1Buf);
    return;
}
#endif
/*****************************************************************************
 *    Function Name       : Y1564UtilFetchReport                             *
 *                                                                           *
 *    Description         : This function posts an event to ECFM/Y1731       *
 *                          to fetch the performance monitoring results      *
 *                                                                           *
 *    Input(s)            : pSlaEntry    - Pointer to SLA Entry              *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None                                             *
 ****************************************************************************/
PUBLIC INT4
Y1564UtilFetchReport (tSlaInfo * pSlaEntry)
{  
       INT4    i4RetVal = OSIX_FAILURE;

       i4RetVal = Y1564PortMepGetPerformanceResult (pSlaEntry);

       return i4RetVal;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564UtilIsY1564Started                                  *
 *                                                                             *
 * DESCRIPTION      : This function will check whether Y1564 is started in the *
 *                    context. If started, this function will return OSIX_TRUE,*
 *                    OSIX_FALSE otherwise.                                    *
 *                                                                             *
 * INPUT            : u4ContextId - Context Identifier                         *
 *                                                                             *
 * OUTPUT           : NONE                                                     *
 *                                                                             *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE                                     *
 *                                                                             *
 *******************************************************************************/
PUBLIC INT4
Y1564UtilIsY1564Started (UINT4 u4ContextId)
{
    if (u4ContextId >= Y1564_SIZING_CONTEXT_COUNT)
    {
        return OSIX_FALSE;
    }

    if (gY1564GlobalInfo.apY1564ContextInfo[u4ContextId] == NULL)
    {
        /* Y1564 is not initialized in this context */
        return OSIX_FALSE;
    }

    return ((gau1Y1564SystemStatus[u4ContextId]
             == Y1564_START) ? OSIX_TRUE : OSIX_FALSE);
}


/****************************************************************************
 * FUNCTION NAME    : Y1564UtilGetSlaTestParams                             *
 *                                                                          *
 * DESCRIPTION      : This function will fetch the test params              *
 *                                                                          *
 * INPUT            : pSlaEntry - Sla Entry                                 *
 *                                                                          *
 * OUTPUT           : NONE                                                  *
 *                                                                          *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT1
Y1564UtilGetSlaTestParams (tSlaInfo * pSlaEntry)
{
    tEcfmReqParams *pEcfmReqParams = NULL;
    tTrafficProfileInfo *pTrafProfEntry = NULL;

    if (pSlaEntry == NULL)
    {
        return OSIX_FAILURE;
    }


    pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                pSlaEntry->u4SlaTrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
                           " %s : Traffic Profile Entry is not present "
                           "for the given SLA Id %d \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_MAPPED);
        return OSIX_FAILURE;
    }
    pEcfmReqParams  = EcfmSlaGetPerfTestParams (pSlaEntry->u4ContextId,
                      pSlaEntry->u4SlaId,
                      pSlaEntry->u4SlaMegId,
                      pSlaEntry->u4SlaMeId,
                      pSlaEntry->u4SlaMepId);

    if (pEcfmReqParams == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : Failed to "
                           "update Sla test parameters for SLA %d  \r\n", 
                           __FUNCTION__, pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_REMOTE_NODE_NOT_REACHABLE);
        return OSIX_FAILURE;

    }

    pTrafProfEntry->u4IfIndex = pEcfmReqParams->u4IfIndex;
    pTrafProfEntry->u4TagType = pEcfmReqParams->u4TagType;
    pTrafProfEntry->u4PortSpeed = pEcfmReqParams->u4PortSpeed;
    pTrafProfEntry->u2OutVlanId = pEcfmReqParams->OutVlanId;

    MEMCPY (pTrafProfEntry->SrcMacAddr, pEcfmReqParams->TxSrcMacAddr,
                     sizeof(tMacAddr));

    MEMCPY (pTrafProfEntry->DstMacAddr, pEcfmReqParams->TxDstMacAddr,
                sizeof(tMacAddr));

    pTrafProfEntry->u4IfMtu = pEcfmReqParams->u4IfMtu;

    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                       Y1564_ALL_FAILURE_TRC," %s : Test Parameters "
                       "updation is successful for SLA %d  \r\n", 
                       __FUNCTION__, pSlaEntry->u4SlaId);

    return OSIX_SUCCESS;

}

/*****************************************************************************
 * Function Name      : Y1564UtilValidateSlaCFMParams                        *
 *                                                                           *
 * Description        : This routine checks whether MEP/ME/MEG is already    *
 *                      mapped to any other SLA and Validate CFM Entries     *
 *                      and Register with CFM entries to get Signal Failure  * 
 *                      and clearance indication                             *
 *                                                                           *
 * Input(s)           : pSlaEntry - Sla node pointer                         *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
PUBLIC
INT4 Y1564UtilValidateSlaCFMParams (tSlaInfo * pSlaEntry)
{
    tSlaInfo *pSlaInfo = NULL;
    tSlaInfo *pNextSlaInfo = NULL;

    if ((pSlaEntry->u4SlaMegId == Y1564_ZERO) ||
        (pSlaEntry->u4SlaMeId == Y1564_ZERO) ||
        (pSlaEntry->u4SlaMepId == Y1564_ZERO))
    {
        return OSIX_SUCCESS;
    }
    
    else
    {
        pSlaInfo = (tSlaInfo *) Y1564UtilSlaGetFirstNodeFromSlaTable ();

        while (pSlaInfo != NULL)
        {
            /* check is added to validate CFM entries in same context
             * with different SLA */
            if ((pSlaInfo->u4ContextId == pSlaEntry->u4ContextId) &&
                (pSlaEntry->u4SlaId != pSlaInfo->u4SlaId))
            {
                if ((pSlaInfo->u4SlaMegId == pSlaEntry->u4SlaMegId) &&
                    (pSlaInfo->u4SlaMeId == pSlaEntry->u4SlaMeId) &&
                    (pSlaInfo->u4SlaMepId == pSlaEntry->u4SlaMepId))
                {
                    /*Reset MEG, ME and MEP values to ZERO, Since the configuration
                     *is not success, it should be reset in SLA entry */
                    nmhSetFsY1564SlaMEG (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                         Y1564_ZERO);
                    nmhSetFsY1564SlaME (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                        Y1564_ZERO);
                    nmhSetFsY1564SlaMEP (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                         Y1564_ZERO);
                    CLI_SET_ERR (CLI_Y1564_ERR_MEG_ME_MEP_EXIST);
                    return OSIX_FAILURE;
                }
            }
            pNextSlaInfo = Y1564UtilSlaGetNextNodeFromSlaTable (pSlaInfo); 
            pSlaInfo = pNextSlaInfo;
       }
    }

    if (Y1564PortValMepIndex (pSlaEntry) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : "
                           "MEP %d ME %d MEG %d associated with SLA is "
                           "not present in ECFM/Y.1731\r\n", 
                           __FUNCTION__,pSlaEntry->u4SlaMepId,
                           pSlaEntry->u4SlaMeId,pSlaEntry->u4SlaMegId);
        /*Reset MEG, ME and MEP values to ZERO, Since the configuration
         *got failed, it should not be stored in SLA entry */
        nmhSetFsY1564SlaMEG (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                             Y1564_ZERO);
        nmhSetFsY1564SlaME (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                            Y1564_ZERO);
        nmhSetFsY1564SlaMEP (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                             Y1564_ZERO);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_CFM_NOT_EXISTS);
        return OSIX_FAILURE;
    }

    if (Y1564PortMepRegAndGetFltState (pSlaEntry) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : Failed to register "
                           "with ECFM/Y.1731 for MEP %d ME %d MEG %d"
                           "and get existing fault\r\n", __FUNCTION__,
                           pSlaEntry->u4SlaMepId, pSlaEntry->u4SlaMeId,
                           pSlaEntry->u4SlaMegId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}


/*****************************************************************************
 * Function Name      : Y1564UtilValidateSlaTest                             *
 *                                                                           *
 * Description        : This routine validates configuration test initiation *
 *                      for EIR and Traffic Policing.                        *
 *                                                                           *
 * Input(s)           : pSlaEntry - Sla node pointer                         *
 *                      pTrafProfEntry - Traffic Profile node pointer        *                                                         *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/

INT4
Y1564UtilValidateSlaTest (tSlaInfo *pSlaEntry, tTrafficProfileInfo *pTrafProfEntry,
		UINT4 * u4Duration)
{
    tConfTestReportInfo      *pConfigTestReportEntry = NULL;
    tConfTestReportInfo      *pNextConfTestReportEntry = NULL;
    UINT4                     u4MilliSec = Y1564_ZERO;
    UINT1                    u1TstSelector = Y1564_ZERO;
    UINT4                    u4EIRvalue = Y1564_ZERO;
    UINT1                    u1Count = Y1564_ZERO;
    UINT1                    u1EmixPatternLength = Y1564_ZERO;
    UINT1                    u1Flag = OSIX_FALSE;
    UINT1                    u1CIRFlag = OSIX_FALSE;
    UINT1                    u1Index = Y1564_ZERO;
    UINT1                    u1TstFlag = OSIX_FALSE;
    UINT1                    u1StartFlag = OSIX_TRUE;
    UINT1                    u1ColorMode;
    UINT1                    u1TestMode = Y1564_ZERO;
    UINT1                    u1CurrentTestSelector = Y1564_ZERO;

#ifndef MEF_WANTED
    tServiceConfInfo         *pServiceConfInfo = NULL;
#endif

    if (Y1564PortGetTestSelector (pSlaEntry, &u1TstSelector) ==
        OSIX_SUCCESS)
    {
        pSlaEntry->u1SlaCurrentTestMode  = u1TstSelector;
    }

#ifndef MEF_WANTED

    pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                  (UINT4) pSlaEntry->u2SlaServiceConfId);

    if (pServiceConfInfo == NULL)
    {
        return OSIX_FAILURE;

    }

    u4EIRvalue = pServiceConfInfo->u4ServiceConfEIR;
    u1ColorMode = pServiceConfInfo->u1ServiceConfColorMode;
#endif
    if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
    {
        /* Traffic Profile Information */
        pSlaEntry->i4CurrentFrSize  =  pTrafProfEntry->u2TrafProfPktSize;
    }
    else
    {
        pSlaEntry->i4CurrentFrSize = pTrafProfEntry->u2CurrentEmixPktSize;
    }

#ifdef MEF_WANTED
    u4EIRvalue = pSlaEntry->u4EIR;
    u1ColorMode = pSlaEntry->u1ColorMode;
#endif

    Y1564_CONVERT_SEC_TO_MILLISECONDS (pSlaEntry->u4SlaConfTestDuration,
                                       u4MilliSec);

    /*Proceed if the Test Selector is not Simple CIR or Step Load and
     * all tests are being selected otherwise no validation is required
     * for SLA before starting EIR or Traffic Policing Test*/
    if ((pSlaEntry->u1SlaCurrentTestMode != Y1564_CIR_TEST) &&
         (pSlaEntry->u1SlaCurrentTestMode != Y1564_STEP_LOAD_CIR_TEST) &&
         ((pSlaEntry->u1SlaConfTestSelector & Y1564_ALL_TEST) == Y1564_ALL_TEST))
        
    {
        do
        {
            if (u1StartFlag == OSIX_TRUE)
            {
                pConfigTestReportEntry = 
                    Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                     pSlaEntry->u4SlaId,
                                                     pSlaEntry->i4CurrentFrSize,
                                                     Y1564_ONE, Y1564_ONE);

                if (pConfigTestReportEntry ==  NULL)
                {
                    /* Simple CIR is not initiated for the current frame size
                     * so checking for Step Load */
                    pConfigTestReportEntry = 
                        Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                         pSlaEntry->u4SlaId,
                                                         pSlaEntry->i4CurrentFrSize,
                                                         Y1564_TWO, Y1564_ONE);
                    if (pConfigTestReportEntry ==  NULL)
                    {
                        if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                        {
                            /*Returning failure if the ConfigTestReportEntry is NULL*/
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                               Y1564_CRITICAL_TRC, " %s : "
                                               "simple cir or step load cir test is"
                                               "not successful for Sla: %d Fr Size: %d "
                                               ".\r\n",__FUNCTION__, 
                                               pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            u1StartFlag = OSIX_FALSE; /*Flag reset to false
                                                        because the check to be
                                                        executed only once */
                            return OSIX_FAILURE;
                        }

                        /* EMix pattern is selected and simple cir or step load cir
                         * is not present for the current frame size, so moving
                         * to next frame size */
                        else
                        {
                            u1StartFlag = OSIX_FALSE; /*Flag reset to false
                                                        because the check to be
                                                        executed only once */
                            if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE) ||
                                (pSlaEntry->u1SlaCurrentTestMode ==  Y1564_EIR_TEST_COLOR_BLIND))
                            {

                                u1CIRFlag = OSIX_TRUE;

                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                   Y1564_CTRL_TRC, " %s : Simple Cir "
                                                   "or Step Load Cir test is not "
                                                   "success for Sla :%d Fr Size: %d "
                                                   "so Eir cannot be initiated "
                                                   "Continuing with next frame size "
                                                   "in Emix Pattern.\r\n",__FUNCTION__, 
                                                   pSlaEntry->u4SlaId, 
                                                   pSlaEntry->i4CurrentFrSize);
                            }
                            else
                            {
                                u1Flag = OSIX_TRUE;

                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                   Y1564_CTRL_TRC, " %s : Simple Cir "
                                                   "or Step Load Cir test is not "
                                                   "success for Sla :%d Fr Size: %d "
                                                   "so Traffic Policing cannot be initiated "
                                                   "Continuing with next frame size "
                                                   "in Emix Pattern.\r\n",__FUNCTION__, 
                                                   pSlaEntry->u4SlaId, 
                                                   pSlaEntry->i4CurrentFrSize);

                            }
                        }
                    }
                }
                u1StartFlag = OSIX_FALSE; /*Flag reset to false
                                            because it shoule be executed only once */
            }

            if (pConfigTestReportEntry != NULL)
            {
                /* If EIR  test is selected for SLA then the below
                 * checks has to be validated before starting the test.
                 * Rec.ITU-T Y.1564(03/2011) section 8.1.2 Service Configuration
                 * test procedure.
                 * A.1 and A.2
                 *
                 * CIR test result has to be validated and test will be 
                 * initiated only if the CIR test result is PASS.
                 *
                 * First, simple cir test result will be validated if the
                 * entry is present otherwise step load test will be validated
                 * If both entries are not present and EMIX pattern is not
                 * selected then test will not be started for the frame size.
                 *
                 * Suppose, EMIX pattern is selected then the above validation
                 * will be checked for each frame sizes in EMIX pattern.
                 *
                 * If EIR value == ZERO and Emix Pattern is not
                 * selected then Traffic Policing test
                 * will be initiated if the test selector has
                 * Traffic Policing test.
                 *
                 * If EIR Value == ZERO and EMIX pattern is 
                 * selected then Traffic policing test
                 * will be initiated after traversing all the frame 
                 * sizes in that EMIX pattern */


                if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE) ||
                    (pSlaEntry->u1SlaCurrentTestMode ==  Y1564_EIR_TEST_COLOR_BLIND))
                {
                    if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) ||
                        (pConfigTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST))
                    {
                        if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) &&
                            (pConfigTestReportEntry->u1StatsResult == Y1564_TEST_PASS))
                        {
                            /*Simple CIR Test Result is Success ,so proceeding to EIR Test*/
                            if (u4EIRvalue != Y1564_ZERO)
                            {
                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                   Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                   " %s : Simple Cir test is success "
                                                   "for Sla :%d Fr Size: %d "
                                                   "so initiating EIR test.\r\n",__FUNCTION__, 
                                                   pSlaEntry->u4SlaId, 
                                                   pSlaEntry->i4CurrentFrSize);
                                break;
                            }
                            else
                            {
                                if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                {
                                    /* EIR value is zero, so proceeding with Traffic
                                     * policing test if that is also selected as one
                                     * the test in test selector */
                                    if (Y1564PortGetTestSelector (pSlaEntry, &u1TstSelector) ==
                                        OSIX_SUCCESS)
                                    {
                                        pSlaEntry->u1SlaCurrentTestMode  = u1TstSelector;
                                    }

                                    if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
                                        (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
                                    {
                                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                           Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                           " %s : Simple Cir test is success "
                                                           "for Sla :%d Fr Size: %d "
                                                           "but EIR value is Zero. So "
                                                           "initiating Traffic Policing "
                                                           "test.\r\n",__FUNCTION__, 
                                                           pSlaEntry->u4SlaId, 
                                                           pSlaEntry->i4CurrentFrSize);
                                        break;
                                    }
                                    else
                                    {
                                        /* none of the test is selected */
                                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                                           Y1564_ALL_FAILURE_TRC | 
                                                           Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                           " %s : Simple Cir test is success "
                                                           "for Sla :%d Fr Size: %d "
                                                           "but EIR value is Zero and traffic "
                                                           "policing test is not selected in"
                                                           "test selector. so none of the test "
                                                           "is initiated.\r\n",__FUNCTION__, 
                                                           pSlaEntry->u4SlaId, 
                                                           pSlaEntry->i4CurrentFrSize);
                                        return OSIX_FAILURE;
                                    }
                                }
                                else
                                {
                                    /* If EMIX pattern is selected and EIR value is ZERO, 
                                     * other frame sizes has to be validated before proceeding to
                                     * Traffic policing test. once all the frame sizes are traversed
                                     * then traffic policing test can be initiated
                                     * for all the frame sizes in that EMIX*/
                                    u1CIRFlag = OSIX_TRUE;
                                    /* Flag set to true to select the next traffic policing 
                                     * test mode for emix pattern */
                                    u1TstFlag = OSIX_TRUE;

                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Simple Cir test is success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "but EIR value is Zero and Emix "
                                                       "pattern is selected. So proceeding "
                                                       "to next frame size.\r\n",__FUNCTION__, 
                                                       pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                }
                            }
                        }
                        else
                        {
                            if (((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) &&
                                 (pConfigTestReportEntry->u1StatsResult != Y1564_TEST_PASS)) ||
                                ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST) &&
                                 (pConfigTestReportEntry->u1StatsResult == Y1564_TEST_PASS)))
                            {
                                pNextConfTestReportEntry = 
                                    Y1564UtilGetNextNodeFromConfTestReportTable (pConfigTestReportEntry);

                                pConfigTestReportEntry = pNextConfTestReportEntry;

                                if ((pConfigTestReportEntry == NULL) || 
                                    (pConfigTestReportEntry->u4CurrentTestMode != Y1564_STEPLOAD_CIR_TEST))
                                {
                                    /*Stepload CIR Test Result is Success ,so proceeding to EIR Test*/
                                    if (u4EIRvalue != Y1564_ZERO)
                                    {
                                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                           Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                           " %s : Step load Cir test is success "
                                                           "for Sla :%d Fr Size: %d "
                                                           "so initiating EIR test.\r\n",__FUNCTION__, 
                                                           pSlaEntry->u4SlaId, 
                                                           pSlaEntry->i4CurrentFrSize);
                                        break;
                                    }
                                    else
                                    {
                                        if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                        {
                                            /* EIR value is zero, so proceeding with Traffic
                                             * policing test if that is also selected as one
                                             * the test in test selector */
                                            if (Y1564PortGetTestSelector (pSlaEntry, &u1TstSelector) ==
                                                OSIX_SUCCESS)
                                            {
                                                pSlaEntry->u1SlaCurrentTestMode  = u1TstSelector;
                                            }

                                            if ((pSlaEntry->u1SlaCurrentTestMode == 
                                                 Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
                                                (pSlaEntry->u1SlaCurrentTestMode == 
                                                 Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
                                            {
                                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                                   Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                                   " %s : Step load Cir test is success "
                                                                   "for Sla :%d Fr Size: %d "
                                                                   "but EIR value is Zero. So "
                                                                   "initiating Traffic Policing "
                                                                   "test.\r\n",__FUNCTION__, 
                                                                   pSlaEntry->u4SlaId, 
                                                                   pSlaEntry->i4CurrentFrSize);
                                                break;
                                            }
                                            else
                                            {
                                                /* Traffic Policing test is not selected 
                                                 * and EIR value is ZERO. so returning failure*/
                                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                                                   Y1564_ALL_FAILURE_TRC |
                                                                   Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                                   " %s : Step load Cir test is success "
                                                                   "for Sla :%d Fr Size: %d "
                                                                   "but EIR value is Zero and traffic "
                                                                   "policing test is not selected in"
                                                                   "test selector. so none of the test "
                                                                   "is initiated.\r\n",__FUNCTION__, 
                                                                   pSlaEntry->u4SlaId, 
                                                                   pSlaEntry->i4CurrentFrSize);
                                                return OSIX_FAILURE;
                                            }
                                        }
                                        else
                                        {
                                            /* If EMIX pattern is selected and EIR value is ZERO, 
                                             * other frame sizes has to be validated before proceeding to
                                             * Traffic policing test. once all the frame sizes are traversed
                                             * then traffic policing test can be initiated
                                             * for all the frame sizes in that EMIX*/
                                            u1CIRFlag = OSIX_TRUE;
                                            /* Flag set to true to select the next traffic policing 
                                             * test mode for emix pattern */
                                            u1TstFlag = OSIX_TRUE;

                                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                               " %s : Step load Cir test is success "
                                                               "for Sla :%d Fr Size: %d "
                                                               "but EIR value is Zero and Emix "
                                                               "pattern is selected. So proceeding "
                                                               "to next frame size.\r\n",__FUNCTION__, 
                                                               pSlaEntry->u4SlaId, 
                                                               pSlaEntry->i4CurrentFrSize);
                                        }

                                    }

                                }
                                else
                                {
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Step load Cir test is success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "for the current step. so proceeding "
                                                       "to the next step before initiating "
                                                       "EIR test.\r\n",__FUNCTION__, 
                                                       pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    continue;
                                }
                            } 
                            else
                            {
                                if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                {
                                    /* both simple cir and step load cir is not success
                                     * so cannot start eir test and returning failure */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Both Simple Cir and Step load Cir "
                                                       "test is not success for Sla :%d "
                                                       "Fr Size: %d. so not initiating "
                                                       "EIR test.\r\n",__FUNCTION__, 
                                                       pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    return OSIX_FAILURE;
                                }
                                else
                                {
                                    /*In Emix pattern , if either simple cir
                                     * or step load cir is not success
                                     * move to next frame size */
                                    u1CIRFlag = OSIX_TRUE;
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Configuration test is not "
                                                       "success for Sla :%d Fr Size: %d "
                                                       "and Emix pattern is selected. "
                                                       "so continuing with next "
                                                       "frame size.\r\n",__FUNCTION__, 
                                                       pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                }
                            }
                        }
                    }
                }

                /* If Traffic policing test is selected for SLA then the below
                 * checks has to be validated before starting the test.
                 * Rec.ITU-T Y.1564(03/2011) section 8.1.2 Service Configuration
                 * test procedure.
                 * B.1 and B.2
                 *
                 * If EIR value == ZERO , then CIR test result has to be
                 * validated and test will be initiated only
                 * if the CIR test result is PASS.
                 *
                 * First, simple cir test result will be validated if the
                 * entry is present otherwise step load test will be validated
                 * If both entries are not present and EMIX pattern is not
                 * selected then test will not be started for the frame size.
                 *
                 * Suppose, EMIX pattern is selected then the above validation
                 * will be checked for all frame sizes in EMIX pattern.
                 *
                 * If EIR value != ZERO, then EIR test result  will be 
                 * validated, if the result is fail and EMIX pattern is not
                 * selected then test will not be initiated otherwise
                 * validation will be achieved for EMIX pattern.*/

                if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
                    (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
                {
                    if (u4EIRvalue == Y1564_ZERO)
                    {
                        /* If EIR value is zero then we should check for
                         * CIR test result before proceeding to Traffic policing
                         * test. If the test PASS then start Traffic Policing test
                         * else return failure */

                        if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) &&
                            (pConfigTestReportEntry->u1StatsResult == Y1564_TEST_PASS))
                        {
                            /*Simple CIR Test Result is Success ,so proceeding to EIR Test*/
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                               " %s : Simple Cir test is success "
                                               "for Sla :%d Fr Size: %d "
                                               "so initiating Traffic Policing "
                                               "test.\r\n",__FUNCTION__, pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            break;
                        }     
                        else
                        {
                            if (((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) &&
                                 (pConfigTestReportEntry->u1StatsResult != Y1564_TEST_PASS)) ||
                                ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST) &&
                                 (pConfigTestReportEntry->u1StatsResult == Y1564_TEST_PASS)))
                            {
                                pNextConfTestReportEntry = 
                                    Y1564UtilGetNextNodeFromConfTestReportTable (pConfigTestReportEntry);

                                pConfigTestReportEntry = pNextConfTestReportEntry;

                                if ((pConfigTestReportEntry == NULL) || 
                                    (pConfigTestReportEntry->u4CurrentTestMode != Y1564_STEPLOAD_CIR_TEST))
                                {
                                    /* Stepload CIR Test Result is Success ,so proceeding to traffic 
                                     * policing Test */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Step load Cir test is success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "so initiating Traffic Policing "
                                                       "test.\r\n",__FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    break;
                                }
                                else
                                {
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Step load Cir test is success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "so continuing with next step id.\r\n",
                                                       __FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    continue;
                                }
                            }
                            else
                            {

                                if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                {
                                    /* both simple cir or step load cir is not success
                                     * so cannot start traffic Policing test and returning failure */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Both Simple Cir and Step load Cir "
                                                       "test is not success for Sla :%d Fr Size: %d "
                                                       "so cannot start traffic policing "
                                                       "test.\r\n",__FUNCTION__,pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);

                                    return OSIX_FAILURE;
                                }
                                else
                                {
                                    /*In Emix pattern , if both simple cir
                                     * or step load cir is not success
                                     * move to next frame size */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Both Simple Cir and Step load Cir "
                                                       "test is not success for Sla :%d Fr Size: %d "
                                                       "and Emix pattern is selected. so continuing "
                                                       "with next frame size.\r\n",__FUNCTION__,
                                                       pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    u1CIRFlag = OSIX_TRUE;
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE) ||
                            (pConfigTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_BLIND))
                        {
                            if (pConfigTestReportEntry->u1StatsResult == Y1564_TEST_PASS)
                            {
                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                   Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                   " %s : Eir test is success "
                                                   "for Sla :%d Fr Size: %d "
                                                   "so initiating Traffic Policing "
                                                   "test.\r\n",__FUNCTION__, pSlaEntry->u4SlaId, 
                                                   pSlaEntry->i4CurrentFrSize);
                                break;
                            }
                            else
                            {
                                if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                {
                                    /*EIR is initiated but result is Fail. so returning
                                     * failure */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Eir test is not success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "so not initiating Traffic Policing "
                                                       "test.\r\n",__FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    return OSIX_FAILURE;
                                }
                                else
                                {
                                    /*For Emix pattern, if EIR test is not successs for the
                                     * current frame size then next frame size has to
                                     * be checked */
                                    u1Flag = OSIX_TRUE;

                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Eir test is not success "
                                                       "for Sla :%d Fr Size: %d "
                                                       "and Emix pattern is selected "
                                                       "so continuing with next frame size.\r\n",
                                                       __FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                }

                            }
                        }

                        if (u1Flag != OSIX_TRUE)
                        {
                            pNextConfTestReportEntry = 
                                Y1564UtilGetNextNodeFromConfTestReportTable (pConfigTestReportEntry);

                            pConfigTestReportEntry = pNextConfTestReportEntry;

                            if (pConfigTestReportEntry == NULL)
                            {
                                if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                                {
                                    /*EIR is not initiated. so returning
                                     * failure */
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Eir test report is not "
                                                       "present for Sla :%d Fr Size: %d "
                                                       "so not initiating Traffic Policing "
                                                       "test.\r\n",__FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                    return OSIX_FAILURE;
                                }
                                else
                                {
                                    /*For Emix pattern, if EIR test is not initiated for the
                                     * current frame size then next frame size has to
                                     * be checked */
                                    u1Flag = OSIX_TRUE;

                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                                       Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                                       " %s : Eir test report is not present "
                                                       "for Sla :%d Fr Size: %d "
                                                       "and Emix pattern is selected "
                                                       "so continuing with next frame size.\r\n",
                                                       __FUNCTION__, pSlaEntry->u4SlaId, 
                                                       pSlaEntry->i4CurrentFrSize);
                                }
                            }

                            /* continue the loop till the EIR report.
                             * before getting EIR report, Conf Report entry can have either
                             * simple cir/step load cir */
                            if (pConfigTestReportEntry != NULL) 
                            {
                                if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) ||
                                    (pConfigTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST))
                                {
                                    continue;
                                }
                            }
                        }
                    }       
                }
            }
            if (u1CIRFlag == OSIX_TRUE)
            {
                /* checking CIR test results for the next frame sizes*/
                u1EmixPatternLength = (UINT1) 
                    STRLEN (pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                if (pTrafProfEntry->u1LastFrSize == OSIX_TRUE)
                {
                    if (((u1TstFlag == OSIX_TRUE) &&
                         ((pSlaEntry->u1LastSlaConfTestSelector & Y1564_TRAF_POLICING_TEST)
                          != Y1564_TRAF_POLICING_TEST)) || (u1TstFlag != OSIX_TRUE))
                    {
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC | 
                                           Y1564_CRITICAL_TRC | Y1564_SESSION_TRC,
                                           " %s : Reached end of the EMIX "
                                           "pattern for Sla :%d,So test will "
                                           "not be initiated \r\n",
                                           __FUNCTION__, pSlaEntry->u4SlaId);
                        return OSIX_FAILURE;
                    }
                }

                for (u1Index = Y1564_ZERO; u1Index < u1EmixPatternLength; u1Index++)
                {
                    Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

                    u4MilliSec = (UINT4) ((u4MilliSec /
                                           STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))                                                                                   * u1Count);

                    pSlaEntry->u4SlaStepLoadDuration = u4MilliSec;
                    *u4Duration = u4MilliSec;
                    pSlaEntry->i4CurrentFrSize = pTrafProfEntry->u2CurrentEmixPktSize;

                    pConfigTestReportEntry =
                        Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                         pSlaEntry->u4SlaId,
                                                         pSlaEntry->i4CurrentFrSize,
                                                         Y1564_ONE, Y1564_ONE);
                    if (pConfigTestReportEntry == NULL)
                    {
                        /* Simple CIR is not initiated for this frame size, 
                         * so checking for step load test */ 
                        pConfigTestReportEntry =
                            Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                             pSlaEntry->u4SlaId,
                                                             pSlaEntry->i4CurrentFrSize,
                                                             Y1564_TWO, Y1564_ONE);
                        if (pConfigTestReportEntry == NULL)
                        {
                            /* If entry is not present for step load cir
                             * also then continue for loop to check for 
                             * the next frame size */
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, " %s : "
                                               "Simple Cir or Step Load Cir "
                                               "test report is not present "
                                               "for Sla: %d Fr Size: %d. Continuing "
                                               "with next frame size.\r\n",__FUNCTION__, 
                                               pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            continue;
                        }
                        else
                        { 
                            /* step load cir is present for this frame size, 
                             * so break for loop and continue while loop
                             * to check the step load result*/
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, " %s : "
                                               "step load cir test report is present "
                                               "for Sla: %d Fr Size: %d. continue "
                                               "with other step id to validate the "
                                               "test results.\r\n",__FUNCTION__, 
                                               pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            break;
                        }
                    }
                    else
                    {
                        /* simple cir is present for this frame size, 
                         * so break for loop and continue while loop
                         * to check simple cir result*/
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                           Y1564_CTRL_TRC | Y1564_SESSION_TRC, " %s : "
                                           "simple cir test report is present "
                                           "for Sla: %d Fr Size: %d and continue "
                                           "to validate the test result \r\n",__FUNCTION__, 
                                           pSlaEntry->u4SlaId, 
                                           pSlaEntry->i4CurrentFrSize);
                        break;
                    }

                }
                if (u1TstFlag == OSIX_TRUE)
                {
                    if (Y1564PortGetTestSelector (pSlaEntry, &u1TstSelector) ==
                        OSIX_SUCCESS)
                    {
                        pSlaEntry->u1SlaCurrentTestMode  = u1TstSelector;
                    }

                    if ((pSlaEntry->u1SlaCurrentTestMode ==
                         Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
                        (pSlaEntry->u1SlaCurrentTestMode ==
                         Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
                    {
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                           Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                           " %s : CIR test is success "
                                           "for Sla :%d Fr Size: %d "
                                           "in Emix Pattern. So "
                                           "initiating Traffic Policing "
                                           "test.\r\n",__FUNCTION__, 
                                           pSlaEntry->u4SlaId, 
                                           pSlaEntry->i4CurrentFrSize);
                        break;                                                                                                        }
                    else
                    {
                        /* Traffic Policing test will be selected
                         * after traversing all the packet sizes 
                         * in Emix pattern. so continuing to
                         * next frame size*/
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                           Y1564_ALL_FAILURE_TRC | 
                                           Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                           " %s : CIR test is success "
                                           "for Sla :%d Fr Size: %d "
                                           "Continuing Emix pattern "
                                           "with next frame size before "
                                           "starting Traffic policing test.\r\n",
                                           __FUNCTION__, pSlaEntry->u4SlaId, 
                                           pSlaEntry->i4CurrentFrSize);
                        continue;
                    }
                }

                if ((u1EmixPatternLength != Y1564_ZERO) && 
                    (u1Index == u1EmixPatternLength))
                {
                    /* simple cir or step load cir test is not 
                     * success for any of the frame
                     * Size in emix pattern, return failure*/
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,  Y1564_ALL_FAILURE_TRC |
                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                       " %s : Simple cir and Step load cir test "
                                       "is not success for Sla Id: %d. so not "
                                       "initiating eir test.\r\n",
                                       __FUNCTION__, pSlaEntry->u4SlaId); 
                    return OSIX_FAILURE;
                }
            }

            if (u1Flag == OSIX_TRUE)
            {
                /* Checking EIR test results for next frame size*/
                u1EmixPatternLength = (UINT1)  
                    STRLEN (pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                if (pTrafProfEntry->u1LastFrSize == OSIX_TRUE)
                {
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC | 
                                       Y1564_ALL_FAILURE_TRC | Y1564_SESSION_TRC, 
                                       " %s : Reached end of the EMIX "
                                       "pattern for Sla :%d and test will "
                                       "not be initiated \r\n",
                                       __FUNCTION__, pSlaEntry->u4SlaId);
                    return OSIX_FAILURE;
                }

                if (u1EmixPatternLength != Y1564_ZERO)
                {
                    for (u1Index = Y1564_ZERO; u1Index < u1EmixPatternLength; u1Index++)
                    {
                        Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

                        u4MilliSec = (UINT4) ((u4MilliSec /
                                               STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))                                                                                   * u1Count);

                        pSlaEntry->u4SlaStepLoadDuration = u4MilliSec;
                        *u4Duration = u4MilliSec;
                        pSlaEntry->i4CurrentFrSize = pTrafProfEntry->u2CurrentEmixPktSize;

                        if (u1ColorMode == Y1564_SLA_DEF_COLOR_MODE)
                        {
                            u1TestMode = Y1564_FOUR; /*EIR color blind */
                        }
                        else
                        {
                            u1TestMode = Y1564_THREE; /* EIR color aware */
                        }

                        pConfigTestReportEntry =
                            Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                             pSlaEntry->u4SlaId,
                                                             pSlaEntry->i4CurrentFrSize,
                                                             u1TestMode, 
                                                             Y1564_ONE);
                        if (pConfigTestReportEntry == NULL)
                        {
                            /* If entry is not present for EIR
                             * then continue for loop to check for 
                             * the next frame size */
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                               " %s : Eir test report is not present "
                                               "for Sla :%d Fr Size: %d "
                                               "and Emix pattern is selected "
                                               "so continuing with next frame size.\r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            continue;
                        }
                        else
                        { 
                            /* EIR is  present for this frame size, 
                             * so break for loop and continue while loop
                             * to check the EIR test result */
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                               Y1564_CTRL_TRC | Y1564_SESSION_TRC, 
                                               " %s : Eir test report is present "
                                               "for Sla :%d Fr Size: %d "
                                               ",Emix pattern is selected "
                                               "and continue to validate the "
                                               "test result.\r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId, 
                                               pSlaEntry->i4CurrentFrSize);
                            break;
                        }
                    }
                }
                /*EMIX pattern length = ZERO or EIR test is not success for
                 * any of the frame size in EMIX pattern, return failure */
                if (u1Index == u1EmixPatternLength)
                {
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,  Y1564_ALL_FAILURE_TRC |
                                       Y1564_CRITICAL_TRC | Y1564_SESSION_TRC, 
                                       " %s : Eir test is not success "
                                       "for Sla Id: %d. so not initiating traffic "
                                       "policing test.\r\n",
                                       __FUNCTION__, pSlaEntry->u4SlaId); 
                    return OSIX_FAILURE;
                }
            }

        }while (pConfigTestReportEntry != NULL);
    }

    if  ((pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
         (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND)) 
    {
	    u1CurrentTestSelector = Y1564_TRAF_POLICING_TEST;
	    /* Traffic policing test can be started as part of
	     * configuration test only when Traffic policing
	     * status is on */
	    if (((pSlaEntry->u1LastSlaConfTestSelector | u1CurrentTestSelector)
				    == Y1564_TRAF_POLICING_TEST) &&
			    (pSlaEntry->u1SlaTrafPolicing != Y1564_TRAF_POLICY_STATUS_ON))
	    {
		    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
				    " %s : Traffic Policing status is off "
				    "for the given Sla Id %d.So Traffic Policing "
				    "test cannot be started \r\n", __FUNCTION__,
				    pSlaEntry->u4SlaId);
		    return OSIX_FAILURE;
	    }
    }


    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelConfTestResultEntry                      *
 *                                                                           *
 * Description        : This routine delete a Configuration Test Result      *
 *                      Entry from Configuration Result Table                *
 *                                                                           *
 * Input(s)           : u4ContextId  - Context Identifier                    *
 *                      u4SlaId      - SLA Identifier                        *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
VOID
Y1564UtilDelConfTestResultEntry (UINT4 u4ContextId, UINT4 u4SlaId)
{
    tConfTestReportInfo     *pConfTestReportEntry = NULL;
    tConfTestReportInfo     *pNextConfTestReportEntry = NULL;

    pConfTestReportEntry = Y1564UtilGetFirstNodeFromConfTestReportTable();

    while (pConfTestReportEntry != NULL)
    {
        if (((pConfTestReportEntry->u4ContextId == u4ContextId) &&
             (pConfTestReportEntry->u4SlaId == u4SlaId)) ||
            ((pConfTestReportEntry->u4ContextId == u4ContextId) &&
             (u4SlaId == Y1564_ZERO)))
        {
            if (Y1564UtilConfTestReportDelNodeFromRBTree (pConfTestReportEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId,
                                   Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC
                                   | Y1564_RESOURCE_TRC,
                                   " %s : RBTree remove failed for SLA Id "
                                   "%d \r\n", __FUNCTION__,
                                   pConfTestReportEntry->u4SlaId);
                return;
            }
        }
        pNextConfTestReportEntry = Y1564UtilGetNextNodeFromConfTestReportTable
            (pConfTestReportEntry);
        pConfTestReportEntry = pNextConfTestReportEntry;
    }

    Y1564UtilDelPerfTestResultEntry (u4ContextId, Y1564_ZERO, u4SlaId);

    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilDelPerfTestResultEntry                      *
 *                                                                           *
 * Description        : This routine delete a Perf Test Result Entry from    *
 *                      Performance Result  Table                            *
 *                                                                           *
 * Input(s)           : u4ContextId  - Context Identifier                    *
 *                      u4PerfTestId - Performance Test Identifier           *
 *                      u4SlaId      - SLA Identifier                        *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
VOID
Y1564UtilDelPerfTestResultEntry (UINT4 u4ContextId, UINT4 u4PerfTestId,
                                 UINT4 u4SlaId)
{
    tPerfTestReportInfo *pPerfTestReportEntry = NULL;
    tPerfTestReportInfo *pNextPerfTestReportEntry = NULL;

    pPerfTestReportEntry = Y1564UtilGetFirstNodeFromPerfTestReportTable();

    while (pPerfTestReportEntry != NULL)
    {
        if (((pPerfTestReportEntry->u4ContextId == u4ContextId) &&
             (pPerfTestReportEntry->u4StatsPerfId == u4PerfTestId) &&
             (pPerfTestReportEntry->u4SlaId == u4SlaId)) ||
            ((pPerfTestReportEntry->u4ContextId == u4ContextId) &&
             (pPerfTestReportEntry->u4StatsPerfId == u4PerfTestId) &&
             (u4SlaId == Y1564_ZERO)) ||
            ((pPerfTestReportEntry->u4ContextId == u4ContextId) &&
             (pPerfTestReportEntry->u4SlaId == u4SlaId) &&
             (u4PerfTestId == Y1564_ZERO)) ||
            ((pPerfTestReportEntry->u4ContextId == u4ContextId) &&
             (u4PerfTestId == Y1564_ZERO) &&
             (u4SlaId == Y1564_ZERO)))
        {
            if (Y1564UtilPerfTestReportDelNodeFromRBTree (pPerfTestReportEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId,
                                   Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC
                                   | Y1564_RESOURCE_TRC,
                                   " %s : RBTree remove failed for Performance Id "
                                   "%d SLA Id %d \r\n", __FUNCTION__,
                                   pPerfTestReportEntry->u4StatsPerfId,
                                   pPerfTestReportEntry->u4SlaId);
                return;
            }
        }
        pNextPerfTestReportEntry = Y1564UtilGetNextNodeFromPerfTestReportTable
            (pPerfTestReportEntry);
        pPerfTestReportEntry = pNextPerfTestReportEntry;
    }

    return;
}

/*****************************************************************************
 * Function Name      : Y1564UtilPortType                                    *
 *                                                                           *
 * Description        : This routine populate EXTIN params for Port type     *
 *                                                                           *
 * Input(s)           : pTrafProfEntry - Traffic Profile node pointer        *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : i4RetVal                                             *
 *****************************************************************************/

PUBLIC INT4
Y1564UtilPortType(tTrafficProfileInfo *pTrafProfEntry)
{
    tY1564ExtInParams   *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    INT4                 i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_PORT_TYPE_MSG;

    /* Interface Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex = pTrafProfEntry->u4IfIndex;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    pTrafProfEntry->u4PortType = pY1564ExtInParams->Y1564EcfmReqParams.u4PortType;

    return i4RetVal;

}

/*****************************************************************************
 * Function Name      : Y1564UtilGetEmixStatus                               *
 *                                                                           *
 * Description        : This routine is used to get the Emix Selected status *
 *                      to store the fsY1564TrafProfOptEmixPktSize. If the   *
 *                      Emix Selected Value is Zero then MSR will skip the   *                                    
 *                      fsY1564TrafProfOptEmixPktSize value. If the Emix     *                                    
 *                      Selected Value is One then MSR will save the         *
 *                      fsY1564TrafProfOptEmixPktSize value                  *
 *                                                                           *
 * Input(s)           : pTrafProfEntry - Traffic Profile node pointer        *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : i4RetVal                                             *
 *****************************************************************************/
PUBLIC UINT1 
Y1564UtilGetEmixStatus (UINT4 u4ContextId, UINT4 u4TrafProfId,
                                     UINT1 *u1EmixSelected)
{
    tTrafficProfileInfo    *pTrafProfEntry = NULL; 

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4ContextId,
                                                u4TrafProfId);
    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Traffic Profile Entry %d is not present"
                           "\r\n", __FUNCTION__,u4TrafProfId);
        return OSIX_FAILURE;
    }

    *u1EmixSelected = pTrafProfEntry->u1EmixSelected;

    return OSIX_SUCCESS;
}
#endif
