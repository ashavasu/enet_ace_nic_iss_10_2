/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: y1564main.c,v 1.4 2016/02/16 10:25:40 siva Exp $                     *
 *                                                                           *
 * Description: This file contains the init and de-init routines for Y1564   *
 *              Task,MemPool and Timers.                                     *
 * .                                                                         *
 ****************************************************************************/
                                                                           
#ifndef __Y1564MAIN_C__
#define __Y1564MAIN_C__

#include "y1564inc.h"

/****** Prototypes ******/

PRIVATE INT4 Y1564TaskInit (VOID);
PRIVATE VOID Y1564TaskDeInit (VOID);
PRIVATE VOID Y1564AssignMemPools (VOID);

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564Task                                        *
 *                                                                           *
 *    Description         : This function will perform following task in     *
 *                          Y1564 Module:                                    *
 *                          o  Initialized the Y1564 task (Sem, queue,       *
 *                             mempool creation).                            *
 *                          o  Register Y1564 Mib with SNMP module           *
 *                          o  Wait for external event and call the          *
 *                             corresponding even handler routines           *
 *                             on receiving the events.                      *
 *                                                                           *
 *    Input(s)            : pi1Arg -  Pointer to input arguments             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None                                             *
 *                                                                           *
 *****************************************************************************/
VOID
Y1564Task (INT1 *pi1Arg)
{
    UINT4               u4Events = Y1564_ZERO;

#ifdef SYSLOG_WANTED
    INT4               i4SysLogId= Y1564_ZERO;
#endif

    UNUSED_PARAM (pi1Arg);

    if (Y1564TaskInit () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564Task : Task Initialization Failure \r\n");

        /* Deinitialize Y1564 task */
        Y1564TaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to log messages. */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) Y1564_TASK_NAME,
                                   SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        Y1564_GLOBAL_TRC ("Y1564Task : Y1564 registration with Syslog server "
                          "failed \r\n");

        /* Deinitialize Y1564 task */
        Y1564TaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }
    Y1564_SYSLOG_ID = (UINT4) i4SysLogId;

#endif

#ifdef SNMP_2_WANTED
    RegisterFSY156 ();
#endif


    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (Y1564_TASK_ID (), Y1564_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            Y1564_LOCK ();

            if (u4Events & Y1564_QMSG_EVENT)
            {
                Y1564MsgQueueHandler ();

            }

            if (u4Events & Y1564_TMR_EXPIRY_EVENT)
            {
                Y1564TmrExpHandler ();
            }

            Y1564_UNLOCK ();
        }
    }

    return;
}
/*******************************************************************************
 *                                                                             *
 *    Function Name       : Y1564TaskInit                                      *
 *                                                                             *
 *    Description         : This function will perform following task in Y1564 *
 *                          Module:                                            *
 *                          o  Initializes global variables                    *
 *                          o  Create queues, semaphore, mempools for Y1564    *
 *                                                                             *
 *    Input(s)            : None                                               *
 *                                                                             *
 *    Output(s)           : None                                               *
 *                                                                             *
 *    Returns             : OSIX_SUCCESS - When this function successfully     *
 *                                         allocated Y1564 Module resource.    *
 *                          OSIX_FAILURE - When this function failed to        *
 *                                         allocate Y1564 Module resources.    * 
 *******************************************************************************/
INT4
Y1564TaskInit (VOID)
{

    MEMSET (&gY1564GlobalInfo, Y1564_ZERO, sizeof (tY1564GlobalInfo));
    MEMSET (&gaNullPayload, Y1564_ZERO, sizeof (gaNullPayload));

    if (OsixGetTaskId (SELF, Y1564_TASK_NAME, &(Y1564_TASK_ID ()))
        == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TaskInit : Get Task Id FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixSemCrt (Y1564_SEM_NAME, &(Y1564_SEM_ID ())) == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TaskInit: Sem Creation failed \r\n");
        return OSIX_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (Y1564_SEM_ID ());

    if (OsixQueCrt ((UINT1 *) Y1564_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    Y1564_SIZING_MAX_QUEUE, &(Y1564_QUEUE_ID ())) == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TaskInit : Queue Creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (Y1564ApiModuleStart () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TaskInit : Module Start failed\r\n");
        return OSIX_FAILURE;
    }
    /* set Y1564 Initialization as True */
    Y1564_INITIALISED() = Y1564_TRUE;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TaskDeInit                                  *
 *                                                                           *
 *    Description         : This function will perform deinitialization of   *
 *                          Y1564 Task                                       *
 *                                                                           *
 *    Input(s)            : None                                             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None                                             *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
Y1564TaskDeInit (VOID)
{
    Y1564ApiModuleShutDown ();

    if (Y1564_QUEUE_ID () != Y1564_ZERO)
    {
        OsixQueDel (Y1564_QUEUE_ID ());
    }
    if (Y1564_SEM_ID () != Y1564_ZERO)
    {
        OsixSemDel (Y1564_SEM_ID ());
    }

    MEMSET (&gY1564GlobalInfo, Y1564_ZERO, sizeof (tY1564GlobalInfo));
    MEMSET (&gaNullPayload, Y1564_ZERO, sizeof (gaNullPayload));
    
    /* set Y1564 Initialization as False */
    Y1564_INITIALISED () = Y1564_FALSE;

    return;
}
/*****************************************************************************
 *    Function Name       : Y1564AssignMemPools                              *
 *                                                                           *
 *    Description         : This function will Assign the mempool Id's       *
 *                          created for Y1564 Module                         *
 *                                                                           *
 *    Input(s)            : None                                             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
Y1564AssignMemPools (VOID)
{
    /* Assigning the pool information */
    Y1564_QMSG_POOL() = 
        Y1564MemPoolIds[MAX_Y1564_Q_MESG_SIZING_ID];
    Y1564_SLA_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_SLA_ENTRIES_SIZING_ID];
    Y1564_SLA_LIST_POOL () =
        Y1564MemPoolIds[MAX_Y1564_SLA_LIST_INFO_SIZING_ID];
    Y1564_TRAF_PROF_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_TRAFFIC_PROFILES_SIZING_ID];
    Y1564_SAC_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_SAC_ENTRIES_SIZING_ID];
#ifndef MEF_WANTED
    Y1564_SERV_CONF_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_SERV_CONF_ENTRIES_SIZING_ID];
#endif
    Y1564_CONF_REPORT_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_CONF_TEST_REPORT_ENTRIES_SIZING_ID];
    Y1564_PERF_REPORT_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_PERF_TEST_REPORT_ENTRIES_SIZING_ID];
    Y1564_PERF_TEST_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_PERF_TEST_ENTRIES_SIZING_ID];
    Y1564_CTXT_INFO_POOL () = 
        Y1564MemPoolIds[MAX_Y1564_CONTEXT_INFO_SIZING_ID];
   Y1564_EXTINPARAMS_POOL() = 
       Y1564MemPoolIds[MAX_EXT_IN_PARAMS_SIZING_ID];
   Y1564_EXTOUTPARAMS_POOL() = 
       Y1564MemPoolIds[MAX_EXT_OUT_PARAMS_SIZING_ID];
   Y1564_REQPARAMS_POOL() = 
       Y1564MemPoolIds[MAX_EXT_REQ_PARAMS_SIZING_ID];

        return;
}

/******************************************************************************
 * FUNCTION NAME    : Y1564ApiModuleShutDown                                  *
 *                                                                            *
 * DESCRIPTION      : This function will be called when Y1564 module is shut  *
 *                    down                                                    *
 *                                                                            *
 * INPUT            : None                                                    *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564ApiModuleShutDown (VOID)
{
    UINT4          u4ContextId = Y1564_ZERO;

    if (Y1564_INITIALISED() == Y1564_FALSE)
    {
        return OSIX_FAILURE;
    }    

    for (; u4ContextId < Y1564_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (Y1564CxtGetContextEntry (u4ContextId) != NULL)
        {    
            Y1564CxtHandleDeleteContext (u4ContextId);
        }
    }

    if (Y1564_SAC_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_SAC_TABLE (), NULL, Y1564_ZERO);
        Y1564_SAC_TABLE () = NULL;
    }

    if (Y1564_TRAF_PROF_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_TRAF_PROF_TABLE (), NULL, Y1564_ZERO);
        Y1564_TRAF_PROF_TABLE () = NULL;
    }

#ifndef MEF_WANTED
    if (Y1564_SERV_CONF_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_SERV_CONF_TABLE (), NULL, Y1564_ZERO);
        Y1564_SERV_CONF_TABLE () = NULL;
    }
#endif

    if (Y1564_PERF_TEST_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_PERF_TEST_TABLE (), NULL, Y1564_ZERO);
        Y1564_PERF_TEST_TABLE () = NULL;
    }

    if (Y1564_SLA_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_SLA_TABLE (), NULL, Y1564_ZERO);
        Y1564_SLA_TABLE () = NULL;
    }

    if (Y1564_CONF_REPORT_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_CONF_REPORT_TABLE (), NULL, Y1564_ZERO);
        Y1564_CONF_REPORT_TABLE () = NULL;
    }

    if (Y1564_PERF_REPORT_TABLE () != NULL)
    {
        RBTreeDestroy (Y1564_PERF_REPORT_TABLE (), NULL, Y1564_ZERO);
        Y1564_PERF_REPORT_TABLE () = NULL;
    }    

    /* Deinit the Timers */
    Y1564TmrDeInit ();

    /* Delete the MemPools */
    Y1564SizingMemDeleteMemPools ();

    Y1564_INITIALISED () = Y1564_FALSE;

    Y1564_GLOBAL_TRC ("Y1564ModuleShutDown : Y1564 Module is shutdown \r\n");

    return OSIX_SUCCESS;
}
/******************************************************************************
 * FUNCTION NAME    : Y1564ApiModuleStart                                     *
 *                                                                            *
 * DESCRIPTION      : This function will be called by RM module to start      *
 *                    Y1564 module.                                           *
 *                                                                            *
 * INPUT            : None                                                    *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564ApiModuleStart (VOID)
{
    Y1564_GLOBAL_TRC ("Y1564ModuleStart :Y1564 Module is getting started \r\n");

    UINT4        u4ContextId = Y1564_ZERO;

    for (; u4ContextId < Y1564_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        gY1564GlobalInfo.au1SystemCtrl[u4ContextId] = Y1564_SHUTDOWN;
    }

    if (Y1564SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : MemPool init failed \r\n");
        return OSIX_FAILURE;
    }

    Y1564AssignMemPools ();

    if (Y1564TmrInit () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Timer init failed \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilCreateSlaTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : SlaTable creation failed \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilCreateTrafProfTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Traffic Profile creation failed \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilCreateSacTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : SAC Table creation failed \r\n");
        return OSIX_FAILURE;
    }

#ifndef MEF_WANTED
    if (Y1564UtilCreateServConfTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Service Configuration Table "
                          "creation failed \r\n");
        return OSIX_FAILURE;
    }
#endif
    if (Y1564UtilCreateConfTestReportTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Configuration Test report Table "
                          "creation failed \r\n");
        return OSIX_FAILURE;
    }

    if (Y1564UtilCreatePerfTestReportTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Performance Test report Table "
                          "creation failed \r\n");
        return OSIX_FAILURE;
    }
    if (Y1564UtilCreatePerfTestTable () == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564ModuleStart : Performance Test Table "
                          "creation failed \r\n");
        return OSIX_FAILURE;
    }

    Y1564CxtHandleCreateContext (Y1564_DEFAULT_CONTEXT_ID);

    Y1564_GLOBAL_TRC ("Y1564ModuleStart :Y1564 Module is started \r\n");

    return OSIX_SUCCESS;
}

#endif /*__Y1564MAIN_C__*/
