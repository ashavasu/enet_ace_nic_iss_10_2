/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: y1564port.c,v 1.17 2016/03/06 10:05:32 siva Exp $                     *
 *                                                                           *
 * Description: This file contains the Y1564  API related functions          *
 *                                                                           *
 *****************************************************************************/
#include "y1564inc.h"
#include "snmputil.h"
/*****************************************************************************
 *                                                                           *
 * Function     : Y1564PortHandleExtInteraction                              *
 *                                                                           *
 * Description  : This function is the common exit  point for interacting    *
 *                with all external module.                                  *
 *                                                                           *
 * Input        :                                                            *
 *                pY1564ExtReqInParams - Pointer to the Request params.      *
 *                                                                           *
 * Output       : pY1564ExtReqOutParams - Pointer to the response params     *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
Y1564PortHandleExtInteraction (tY1564ExtInParams * pY1564ExtInParams, 
                               tY1564ExtOutParams * pY1564ExtOutParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tEcfmRegParams      Y1564RegParams;
    tEcfmMepInfoParams  MepInfo;
    tEcfmEventNotification Y1564MepFltStatus;

    MEMSET (&MepInfo, Y1564_ZERO, sizeof (MepInfo));

    /* External requests handling */
    switch (pY1564ExtInParams->eExtReqType)
    {
        case Y1564_REQ_VCM_GET_CONTEXT_NAME:
            /* This function is called by Y1564 module to fetch the context
             * name for the given context id.
             * Input:
             *      u4ContextId- Context Id
             * Output:
             *      pu1Alias - Alias Name
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = VcmGetAliasName (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                                        pY1564ExtOutParams->au1ContextName);
            if (i4RetVal == VCM_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                i4RetVal = OSIX_SUCCESS;
            }
            break;

        case Y1564_REQ_VCM_IS_CONTEXT_VALID:
            /* This function is called by Y1564 module to check whether the
             * given Context name is present or not.
             *
             * Input:
             *      pu1Alias - Alias name
             * Output:
             *      u4ContextId - Context Identifier.
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = VcmIsSwitchExist (pY1564ExtInParams->Y1564EcfmReqParams.au1Alias,
                                         &pY1564ExtOutParams->u4ContextId);
            if (i4RetVal == VCM_TRUE)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;
        
         case Y1564_REQ_VCM_GET_SYSTEM_MODE:

            /* This function is called by the Y1564 module
             * to get the System mode from VCM
             *
             * Input:
             *         u2ProtocolId - VCM protocol ID.
             * output
             *         None
             * Returns:
             *        VCM_SI_MODE or VCM_MI_MODE
             **/
            if (VcmGetSystemMode (Y1564_PROTOCOL_ID) == VCM_MI_MODE)
            {
                return VCM_MI_MODE;
            }
            else
            {
                return VCM_SI_MODE;
            }
            break;

         case Y1564_REQ_L2IWF_GET_BRG_MODE:

            /* This function is called by Y1564 Module to get the bridge mode
             * of the context 
             *
             * Input : Context Id
             *
             * Output : Bridge Mode
             *
             * Returns : L2IWF_SUCCESS or L2IWF_FAILURE */

            if (L2IwfGetBridgeMode (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                                    &pY1564ExtOutParams->u4BrgMode) 
                != L2IWF_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }
            break;

         case Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY:
            /* This function is called to register
             * MEP information with ECFM/Y1731 to get
             * Fault notification 
             * Input : MEPInfo structure
             *         SlaId
             * Output: None
             * Returns : OSIX_SUCCES / OSIX_FAILURE*/

            MEMSET (&Y1564RegParams, 0, sizeof (tEcfmRegParams));

            Y1564RegParams.u4ModId = ECFM_Y1564_APP_ID;

            Y1564RegParams.u4EventsId = (ECFM_DEFECT_CONDITION_CLEARED |
                                         ECFM_DEFECT_CONDITION_ENCOUNTERED |
                                         ECFM_RDI_CONDITION_ENCOUNTERED |
                                         ECFM_RDI_CONDITION_CLEARED);

            Y1564RegParams.u4VlanInd = ECFM_INDICATION_FOR_ALL_VLANS;

            Y1564RegParams.pFnRcvPkt = Y1564PortExternalEventNotify;

            Y1564MepFltStatus.u4ContextId = pY1564ExtInParams->Y1564EcfmReqParams.
                u4ContextId;
            Y1564MepFltStatus.u4MdIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            Y1564MepFltStatus.u4MaIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEId;
            Y1564MepFltStatus.u2MepId = (UINT2) pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEPId;
            Y1564MepFltStatus.u4SlaId = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4SlaId;

            i4RetVal = EcfmMepRegisterAndGetFltStatus (&Y1564RegParams, 
                                                       &Y1564MepFltStatus);

            i4RetVal = (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;

         case Y1564_REQ_CFM_DEREGISTER_MEP:
            /* This function is called to De-register
             * MEP information with ECFM/Y1731.:w
             * Input : MEPInfo structure
             *         SlaId
             * Output: None
             * Returns : OSIX_SUCCES / OSIX_FAILURE*/

            MEMSET (&Y1564RegParams, 0, sizeof (tEcfmRegParams));

            Y1564RegParams.u4ModId = ECFM_Y1564_APP_ID;

            Y1564RegParams.pAppInfo = &pY1564ExtInParams->Y1564EcfmReqParams.
                u4ContextId;

            Y1564MepFltStatus.u4ContextId = pY1564ExtInParams->Y1564EcfmReqParams.
                u4ContextId;
            Y1564MepFltStatus.u4MdIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            Y1564MepFltStatus.u4MaIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEId;
            Y1564MepFltStatus.u2MepId = (UINT2) pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEPId;
            Y1564MepFltStatus.u4SlaId = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4SlaId;

            i4RetVal = EcfmMepDeRegister (&Y1564RegParams, &Y1564MepFltStatus);

            i4RetVal = (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;

         case Y1564_REQ_CFM_MEP_INDEX_VALIDATION:
            MepInfo.u4ContextId = pY1564ExtInParams->Y1564EcfmReqParams.
                u4ContextId;
            MepInfo.u4MdIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            MepInfo.u4MaIndex = pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEId;
            MepInfo.u2MepId = (UINT2) pY1564ExtInParams->Y1564EcfmReqParams.
                EcfmMepInfo.u4MEPId;

            i4RetVal = EcfmApiValMepIndex(&MepInfo);

            i4RetVal = (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;

         case Y1564_REQ_SLA_CONF_TEST_START:
         case Y1564_REQ_SLA_PERF_TEST_START:

            if (pY1564ExtInParams->eExtReqType == Y1564_REQ_SLA_CONF_TEST_START)
            {
                pY1564ExtInParams->Y1564EcfmReqParams.u4MsgType = 
                    ECFM_Y1564_REQ_SLA_CONF_TEST_START;
            }
            else if (pY1564ExtInParams->eExtReqType == Y1564_REQ_SLA_PERF_TEST_START)
            {
                pY1564ExtInParams->Y1564EcfmReqParams.u4MsgType = 
                    ECFM_Y1564_REQ_SLA_PERF_TEST_START;
            }

            Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                               Y1564_CTRL_TRC,
                               " %s : TEST_START : u4ContextId : %d  u4SlaId : %d  u2FrameSize : %d \r\n", __FUNCTION__,
                               pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                               pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId,
                               pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize);

            i4RetVal = Y1731ApiHandleExtRequest (&pY1564ExtInParams->Y1564EcfmReqParams, 
                                                 &pY1564ExtInParams->Y1564EcfmRespParams);
            break;
         case Y1564_REQ_CFM_GET_TEST_REPORT:

            pY1564ExtInParams->Y1564EcfmReqParams.u4MsgType = 
                ECFM_Y1564_REQ_CFM_GET_TEST_REPORT;

            i4RetVal = Y1731ApiHandleExtRequest (&pY1564ExtInParams->
                                                                   Y1564EcfmReqParams, 
                                                                   &pY1564ExtInParams->
                                                                   Y1564EcfmRespParams);
            Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                               Y1564_CTRL_TRC,
                               " %s : TEST_REPORT : u4ContextId : %d  u4SlaId : %d  u2FrameSize : %d \r\n", __FUNCTION__,
                               pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                               pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId,
                               pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize); 
            break;

         case Y1564_REQ_CFM_CONF_TEST_STOP_TEST:
         case Y1564_REQ_CFM_PERF_TEST_STOP_TEST:
           
            if (pY1564ExtInParams->eExtReqType == Y1564_REQ_CFM_CONF_TEST_STOP_TEST)
            {
                pY1564ExtInParams->Y1564EcfmReqParams.u4MsgType = 
                    ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST;
            
            }
            else
            {
                pY1564ExtInParams->Y1564EcfmReqParams.u4MsgType = 
                    ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST;
            
            }

            i4RetVal = Y1731ApiHandleExtRequest (&pY1564ExtInParams->Y1564EcfmReqParams, 
                                                 &pY1564ExtInParams->Y1564EcfmRespParams);
            break;

         case Y1564_PORT_TYPE_MSG:

            i4RetVal = L2IwfApiGetPortType (pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex,
                                            &pY1564ExtInParams->Y1564EcfmReqParams.u4PortType);

            i4RetVal = (((i4RetVal == L2IWF_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));

            break;
         
         default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* End of switch */
    return i4RetVal;

}
#ifdef SNMP_3_WANTED
/******************************************************************************
 * FUNCTION NAME    : Y1564PortFmNotifyFaults                                 *
 *                                                                            *
 * DESCRIPTION      : This function Sends the trap message to the Fault       *
 *                    Manager.                                                *
 *                                                                            *
 * INPUT            : pEnterpriseOid - OID                                    *
 *                    u4GenTrapType - Trap Type                               *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : None                                                    *
 *                                                                            * 
 ******************************************************************************/
PUBLIC VOID
Y1564PortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                        UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                        UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName)
{
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (pContextName);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_Y1564;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC
            ("Y1564PortFmNotifyFaults: Sending Notification to FM failed.\r\n");
        if (pTrapMsg != NULL)
        {
            SNMP_AGT_FreeVarBindList (pTrapMsg);
        }

    }
#else
    if (pTrapMsg != NULL)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
    }
    UNUSED_PARAM (pu1Msg);
#endif
    return;
}
#endif
/******************************************************************************
 * Function Name      : Y1564PortMepRegAndGetFltState                         *
 *                                                                            *
 * Description        : This function calls the CFM module to register MEP    *
 *                      information                                           *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortMepRegAndGetFltState (tSlaInfo *pSlaEntry)
{
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    INT4                 i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY;

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;
   
    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : Y1564PortMepDeRegister                                *
 *                                                                            *
 * Description        : This function calls the CFM module to De-register MEP *
 *                      information                                           *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortMepDeRegister (UINT4 u4ContextId, UINT4 u4SlaMegId, UINT4 u4SlaMeId,
                        UINT4 u4SlaMepId)
{
    tY1564ExtInParams   *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    INT4                 i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_DEREGISTER_MEP;

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = u4SlaMepId;
   
    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_DEREGISTER_MEP \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
    return i4RetVal;
}

/******************************************************************************
* Function Name      : Y1564PortValidateMepIndex                             *
*                                                                            *
* Description        : This function calls the CFM module to register MEP    *
*                      information                                           *
* Input(s)           : tSlaInfo - Sla Information structure                  *
*                                                                            *
* Output(s)          : None                                                  *
*                                                                            *
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
*                                                                            *
******************************************************************************/

PUBLIC INT4
Y1564PortValMepIndex (tSlaInfo *pSlaEntry)
{
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    INT4                 i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

 if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_MEP_INDEX_VALIDATION;

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_ALL_FAILURE_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : Y1564PortGetTestSelector                              *
 *                                                                            *
 * Description        : This routine get the test selector value from SLA     *
 *                      Table                                                 *
 *                                                                            *
 * Input(s)           : pSlaEntry - SLA node                                  *
 *                                                                            *
 * Output(s)          : pu1TstSelector - Test Selector value                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 ******************************************************************************/
PUBLIC INT4
Y1564PortGetTestSelector (tSlaInfo * pSlaEntry, UINT1 *pu1TstSelector)
{
  tTrafficProfileInfo  *pTrafProfEntry = NULL;
#ifndef MEF_WANTED
  tServiceConfInfo     *pServiceConfInfo = NULL;
#endif
  UINT1                 u1TestSelector = Y1564_ONE;

  *pu1TstSelector = Y1564_ZERO;

  pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                              pSlaEntry->u4SlaTrafProfId);
  if (pTrafProfEntry == NULL)
  {
      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                         " %s : Traffic Profile Entry is not present"
                         "for the given SLA Id %d \r\n", __FUNCTION__,
                         pSlaEntry->u4SlaId);
      return OSIX_FAILURE;
  }

#ifndef MEF_WANTED
    pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                  (UINT4) pSlaEntry->u2SlaServiceConfId);

    if (pServiceConfInfo == NULL)
    {

        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Service Configuration Entry is not present"
                           "for the given SLA Id %d \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }
#endif

  if ((STRLEN (pTrafProfEntry->au1TempTrafProfOptEmixPktSize) == Y1564_ZERO) &&
      (pTrafProfEntry->u1EmixSelected == Y1564_TRUE))
  {
      pTrafProfEntry->u1LastFrSize = OSIX_TRUE;
      
      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                         " %s : EMIX NULL \r\n", __FUNCTION__);
  }
  else
  {
      pTrafProfEntry->u1LastFrSize = OSIX_FALSE;
  }

  for(; u1TestSelector <= pSlaEntry->u1LastSlaConfTestSelector; u1TestSelector++)
  {
      if (*pu1TstSelector == Y1564_ZERO)
      {
          if ((pSlaEntry->u1LastSlaConfTestSelector & Y1564_SIMPLE_CIR_TEST) 
              == Y1564_SIMPLE_CIR_TEST)
          {
              *pu1TstSelector = Y1564_CIR_TEST;

              if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                  ( pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
              {
                  pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_SIMPLE_CIR_TEST;

                  MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                          (Y1564_EMIX_PKT_SIZE + 1));

                  MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                         pTrafProfEntry->au1TrafProfOptEmixPktSize,
                         STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));                 

                  Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                     Y1564_CTRL_TRC," %s : Simple Cir: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

              }
              else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
              {
                  pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_SIMPLE_CIR_TEST;
              }
              break;
          }
          else if ((pSlaEntry->u1LastSlaConfTestSelector & Y1564_STEPLOAD_CIR_TEST) 
                   == Y1564_STEPLOAD_CIR_TEST)
          {
              *pu1TstSelector = Y1564_STEP_LOAD_CIR_TEST;

              if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                  (pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
              {
                  if ((pSlaEntry->u2SlaRateStep == Y1564_ONE) ||
                      (pSlaEntry->u2SlaRateStep <= pSlaEntry->u2SlaCurrentStep))
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_STEP_LOAD_CIR_TEST;

                      MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                              (Y1564_EMIX_PKT_SIZE + 1));

                      MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                             pTrafProfEntry->au1TrafProfOptEmixPktSize,
                             STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                     Y1564_CTRL_TRC," %s : Step Load Cir: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                  }
              }
              else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
              {
                  if ((pSlaEntry->u2SlaRateStep == Y1564_ONE) ||
                      (pSlaEntry->u2SlaRateStep <= pSlaEntry->u2SlaCurrentStep))
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_STEP_LOAD_CIR_TEST;
                  }
              }
              break;
          }
#ifdef MEF_WANTED
          else if (((pSlaEntry->u1LastSlaConfTestSelector & Y1564_EIR_TEST)
                    == Y1564_EIR_TEST))
#else
          else if (((pSlaEntry->u1LastSlaConfTestSelector & Y1564_EIR_TEST)
                    == Y1564_EIR_TEST) && (pServiceConfInfo->u4ServiceConfEIR != Y1564_ZERO))
#endif
          {
#ifdef MEF_WANTED          
              if (pSlaEntry->u1ColorMode == Y1564_SERV_CONF_COLOR_AWARE)
#else
              if (pServiceConfInfo->u1ServiceConfColorMode == Y1564_SERV_CONF_COLOR_AWARE)
#endif    
              {
                  *pu1TstSelector = Y1564_EIR_TEST_COLOR_AWARE;

                  if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                      (pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_EIR_TEST;

                      MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                              (Y1564_EMIX_PKT_SIZE + 1));

                      MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                             pTrafProfEntry->au1TrafProfOptEmixPktSize,
                             STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                     Y1564_CTRL_TRC," %s : Eir: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                  }
                  else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_EIR_TEST;
                  }
                  break;
              }
              else
              {
                  *pu1TstSelector = Y1564_EIR_TEST_COLOR_BLIND;

                  if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                      (pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_EIR_TEST;

                      MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                              (Y1564_EMIX_PKT_SIZE + 1));

                      MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                             pTrafProfEntry->au1TrafProfOptEmixPktSize,
                             STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                     Y1564_CTRL_TRC," %s : Eir: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                  }
                  else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                  {
                      pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_EIR_TEST;
                  }
                  break;
              }
          }
          else
          {
              if ((pSlaEntry->u1LastSlaConfTestSelector & Y1564_TRAF_POLICING_TEST) 
                  == Y1564_TRAF_POLICING_TEST)
              {
#ifdef MEF_WANTED          
                  if (pSlaEntry->u1ColorMode == Y1564_SERV_CONF_COLOR_AWARE)
#else
                  if (pServiceConfInfo->u1ServiceConfColorMode == Y1564_SERV_CONF_COLOR_AWARE)
#endif    
                  {
                      *pu1TstSelector = Y1564_TRAF_POLICING_TEST_COLOR_AWARE;

                      if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                          (pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
                      {
                          pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_TRAF_POLICING_TEST;

                          MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                                  (Y1564_EMIX_PKT_SIZE + 1));

                          MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                                 pTrafProfEntry->au1TrafProfOptEmixPktSize,
                                 STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                          Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                     Y1564_CTRL_TRC," %s : Traffic Policing: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                      }
                      else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                      {
                          pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_TRAF_POLICING_TEST;
                      }
                      break;
                  }
                  else
                  {
                      *pu1TstSelector = Y1564_TRAF_POLICING_TEST_COLOR_BLIND;

                      if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                          (pTrafProfEntry->u1LastFrSize == Y1564_TRUE))
                      {
                          pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_TRAF_POLICING_TEST;

                          MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                                  (Y1564_EMIX_PKT_SIZE + 1));

                          MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                                 pTrafProfEntry->au1TrafProfOptEmixPktSize,
                                 STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                          Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC | 
                                             Y1564_CTRL_TRC," %s : Traffic Policing: Sla Id: %d "
                                     "u1EmixSelected: %d TempEmixPktSize: %s\r\n",
                                     __FUNCTION__, pSlaEntry->u4SlaId,
                                     pTrafProfEntry->u1EmixSelected,
                                     pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

                      }
                      else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                      {
                          pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_TRAF_POLICING_TEST;
                      }
                      break;
                  }
              }
          }
          if (*pu1TstSelector != Y1564_ZERO)
          {
              break;
          }
      }
  }

  return  OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : Y1564PortGetTestRate                                  *
 *                                                                            *
 * Description        : This routine get the test rate value from SLA         *
 *                      Table                                                 *
 *                                                                            *
 * Input(s)           : pSlaEntry - SLA node                                  *
 *                                                                            *
 * Output(s)          : pu1TstSelector - Test Selector value                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 ******************************************************************************/
PUBLIC INT4
Y1564PortGetTestRate(tSlaInfo * pSlaEntry, tY1564ExtInParams * pY1564ExtInParams)
{
#ifndef MEF_WANTED
    tServiceConfInfo   *pServiceConfInfo = NULL;
#endif
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4         u4Rate = Y1564_ZERO;
    UINT4         u4YellowFrRate = Y1564_ZERO;
    UINT4         u4Divider = Y1564_ZERO;
    UINT4         u4PortSpeed = Y1564_ZERO;
   

    /* To calculate packets per second use the below formula.
     * Rate(CIR or EIR) = PktSize * Bits per Byte * Packets per second
     * Packets per second = Rate / (PktSize * Bits per Byte)
     * 1 sec = (Rate / (PktSize * Bits per Byte)
     * Calculating Divider to get the number of packets to be
     * send per second*/

#ifndef MEF_WANTED
    pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                  (UINT4) pSlaEntry->u2SlaServiceConfId);

    if (pServiceConfInfo == NULL)
    {

        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Service Configuration Entry is not present"
                           "for the given SLA Id %d \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }
#endif

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                pSlaEntry->u4SlaTrafProfId);
    if (pTrafProfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    u4PortSpeed = pTrafProfEntry->u4PortSpeed;

    /* 20 bytes is the interframe gap (12 bytes) and preamble (8 bytes)*/
    u4Divider =  (UINT4) (pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize +
                Y1564_TWENTY) * BITS_PER_BYTE;

    if (u4Divider == Y1564_ZERO)
    {
        return OSIX_FAILURE;
    }

#ifdef MEF_WANTED
    if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_CIR_TEST) || 
        (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
    {
	    u4Rate = pSlaEntry->u4CIR * Y1564_BPS_COVERTER;
    }    

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_STEP_LOAD_CIR_TEST)
    {
        u4Rate = (UINT4) (pSlaEntry->u4CIR * (Y1564_BPS_COVERTER / Y1564_MAX_RATE_STEP)
                  * pSlaEntry->u2SlaCurrentRateStep);
    }    

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE)
    {
        u4Rate = pSlaEntry->u4CIR * Y1564_BPS_COVERTER;
        u4YellowFrRate = pSlaEntry->u4EIR * Y1564_BPS_COVERTER;
   }    

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_BLIND)
    {
        u4Rate = pSlaEntry->u4CIR * Y1564_BPS_COVERTER + 
                 pSlaEntry->u4EIR * Y1564_BPS_COVERTER;
       
    }

    if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
       (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
    {
        /* EIR rate for yellow colored frames */
        /* 125 % percent of EIR should be transmitted as part of
         * traffic Policing test */

        if (((pSlaEntry->u4EIR * Y1564_BPS_COVERTER) + (pSlaEntry->u4CIR * Y1564_BPS_COVERTER))
            < u4PortSpeed)
        {
                if (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE)
                {
                    u4Rate = pSlaEntry->u4CIR * Y1564_BPS_COVERTER;

                    u4YellowFrRate = (Y1564_HUNDRED_TWENTY_FIVE * pSlaEntry->u4CIR *
                                      (Y1564_BPS_COVERTER / Y1564_HUNDRED));
                }
                else
                {
                    u4Rate = ((Y1564_HUNDRED_TWENTY_FIVE * pSlaEntry->u4CIR * 
                               (Y1564_BPS_COVERTER / Y1564_HUNDRED)) +
                              (pSlaEntry->u4EIR * Y1564_BPS_COVERTER));
                }
        }
        else
        {
            if (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE)
            {
                u4Rate = pSlaEntry->u4CIR * Y1564_BPS_COVERTER;

                u4YellowFrRate = ((Y1564_HUNDRED_TWENTY_FIVE * pSlaEntry->u4EIR * 
                                   Y1564_BPS_COVERTER) / Y1564_HUNDRED);
            }
            else
            {
                u4Rate = ((Y1564_HUNDRED_TWENTY_FIVE * pSlaEntry->u4EIR * 
                           (Y1564_BPS_COVERTER / Y1564_HUNDRED)) +
                          (pSlaEntry->u4CIR * Y1564_BPS_COVERTER));
            }
        }
    }
#else
    if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_CIR_TEST) ||
        (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
    {
        u4Rate = pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER;
    }

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_STEP_LOAD_CIR_TEST)
    {
        u4Rate = (UINT4) (pServiceConfInfo->u4ServiceConfCIR * (Y1564_BPS_COVERTER / Y1564_MAX_RATE_STEP) 
                           * pSlaEntry->u2SlaCurrentRateStep);
    }

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE)
    {
        u4Rate = pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER;
        u4YellowFrRate = pServiceConfInfo->u4ServiceConfEIR * Y1564_BPS_COVERTER;
   }

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_EIR_TEST_COLOR_BLIND)
    {
        u4Rate = pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER +
                 pServiceConfInfo->u4ServiceConfEIR * Y1564_BPS_COVERTER;
    }
    if ((pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
       (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
    {
        /* EIR rate for yellow colored frames */
        /* 125 % percent of EIR should be transmitted as part of
         * traffic Policing test */

        if (((pServiceConfInfo->u4ServiceConfEIR * Y1564_BPS_COVERTER) +
             (pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER))
            < u4PortSpeed)
        {
                if (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE)
                {
                    u4Rate = pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER;

                    u4YellowFrRate = (Y1564_HUNDRED_TWENTY_FIVE * pServiceConfInfo->u4ServiceConfEIR *
                                      (Y1564_BPS_COVERTER / Y1564_HUNDRED));
                }
                else
                {
                    u4Rate = ((Y1564_HUNDRED_TWENTY_FIVE * pServiceConfInfo->u4ServiceConfCIR *
                               (Y1564_BPS_COVERTER / Y1564_HUNDRED)) +
                              (pServiceConfInfo->u4ServiceConfEIR * Y1564_BPS_COVERTER));
                }
        }
        else
        {
            if (pSlaEntry->u1SlaCurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE)
            {
                u4Rate = pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER;

                u4YellowFrRate = (Y1564_HUNDRED_TWENTY_FIVE * pServiceConfInfo->u4ServiceConfEIR *
                                  (Y1564_BPS_COVERTER / Y1564_HUNDRED));
            }
            else
            {
                u4Rate = ((Y1564_HUNDRED_TWENTY_FIVE * pServiceConfInfo->u4ServiceConfEIR *
                           (Y1564_BPS_COVERTER / Y1564_HUNDRED)) +
                          (pServiceConfInfo->u4ServiceConfCIR * Y1564_BPS_COVERTER));
            }
        }
    }
#endif
    if (u4Rate != Y1564_ZERO)
    {
        pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u4Rate = u4Rate;
        pY1564ExtInParams->Y1564EcfmReqParams.u4CIRPps =  u4Rate / u4Divider;
        
    }
    if (u4YellowFrRate != Y1564_ZERO)
    {
        pY1564ExtInParams->Y1564EcfmReqParams.u4YellowFrRate = u4YellowFrRate;
        pY1564ExtInParams->Y1564EcfmReqParams.u4YellowFrPps =  
                                                   u4YellowFrRate / u4Divider;
    }

    if ((u4Rate == Y1564_ZERO) && (u4YellowFrRate == Y1564_ZERO))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : Y1564PortSlaStartConfTest                             *
 *                                                                            *
 * Description        : This function calls the CFM module to start the       *
 *                      below tests.                                          *
 *                      1. Frame Loss                                         *
 *                      2. Frame Delay                                        *
 *                      3. Frame Delay Variation                              *
 *                      4. Test Frame Generation                              *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortSlaStartConfTest (tSlaInfo *pSlaEntry, 
                           tTrafficProfileInfo *pTrafProfEntry,
                           UINT4 u4Duration)
{
    tUtlSysPreciseTime        SysPreciseTime;
    tY1564ExtInParams        *pY1564ExtInParams = NULL;
    tY1564ExtOutParams       *pY1564ExtOutParams = NULL;
#ifndef MEF_WANTED
    tServiceConfInfo         *pServiceConfInfo = NULL;
#endif
    tConfTestReportInfo      *pConfTestReportEntry = NULL;
    tConfTestReportInfo      *pNextConfTestReportEntry = NULL;
    UINT4                    u4EntryCount = Y1564_ZERO;
    INT4                     i4RetVal = OSIX_SUCCESS;
    UINT1                    u1SlaCurrentTestMode = Y1564_ZERO;
    UINT1                    u1CurrentTestSelector = Y1564_ZERO;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_SLA_CONF_TEST_START;

    if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
    {
        /* Traffic Profile Information */
        pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize =
            pTrafProfEntry->u2TrafProfPktSize;
    }
    else
    {
        pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize =
            pTrafProfEntry->u2CurrentEmixPktSize;
    }

    pSlaEntry->i4CurrentFrSize =
        (INT4) pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize;

    if ((UINT4) pSlaEntry->i4CurrentFrSize > pTrafProfEntry->u4IfMtu)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,Y1564_ALL_FAILURE_TRC |
                           Y1564_CRITICAL_TRC," %s : Frame Size : %d is "
                           "greater than MTU size : %d of the port. So test "
                           "will not be initiated \r\n",
                           __FUNCTION__,pSlaEntry->i4CurrentFrSize,
                           pTrafProfEntry->u4IfMtu);
        CLI_SET_ERR(CLI_Y1564_FR_SIZE_GREATER_THAN_MTU);
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }

#ifdef MEF_WANTED
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4CIR = pSlaEntry->u4CIR;
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4EIR = pSlaEntry->u4EIR;
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1ColorMode = pSlaEntry->u1ColorMode;
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1CoupFlag = pSlaEntry->u1CoupFlag;
    pY1564ExtInParams->Y1564EcfmReqParams.u2EvcId = pSlaEntry->u2EvcId;
#else

    pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                  (UINT4) pSlaEntry->u2SlaServiceConfId);
    
    if (pServiceConfInfo != NULL)
    {
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4CIR = pServiceConfInfo->
            u4ServiceConfCIR;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4EIR = pServiceConfInfo->
            u4ServiceConfEIR;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1ColorMode = pServiceConfInfo->
            u1ServiceConfColorMode;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1CoupFlag = pServiceConfInfo->
            u1ServiceConfCoupFlag;
        pY1564ExtInParams->Y1564EcfmReqParams.u2EvcId = (UINT2) pServiceConfInfo->u4ServiceConfId;
    }
    else
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }
#endif

    pY1564ExtInParams->EcfmReqParams.TestParam.u1TestMode = pSlaEntry->u1SlaCurrentTestMode;

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;

    pY1564ExtInParams->Y1564EcfmReqParams.OutVlanId = pTrafProfEntry->u2OutVlanId;
    pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex = pTrafProfEntry->u4IfIndex;
    pY1564ExtInParams->Y1564EcfmReqParams.u4TagType = pTrafProfEntry->u4TagType;
    pY1564ExtInParams->Y1564EcfmReqParams.u4PortSpeed = pTrafProfEntry->u4PortSpeed;

    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TxSrcMacAddr , pTrafProfEntry->SrcMacAddr ,
            sizeof(tMacAddr));
    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TxDstMacAddr , pTrafProfEntry->DstMacAddr ,
            sizeof(tMacAddr));

    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.au1TrafProfPayload,
            pTrafProfEntry->au1TrafProfPayload, STRLEN
            (pTrafProfEntry->au1TrafProfPayload));
    pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u1TrafProfPCP = 
        pTrafProfEntry->u1TrafProfPCP;

    /* Test Param Information */
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4TestDuration =  u4Duration / 1000;

    /* If Simple CIR test is selected then update the
     * current step as ONE */
    if (pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
        Y1564_SIMPLE_CIR_TEST)
    {
        pSlaEntry->u2SlaCurrentStep = Y1564_ONE;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep =
            pSlaEntry->u2SlaCurrentStep;

        u1CurrentTestSelector = Y1564_CIR_TEST;
    }
    /* If Step Load CIR is selected then update the
     * current step and Rate Step */
    if (pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
        Y1564_STEPLOAD_CIR_TEST)
    {
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaRateStep = (UINT2) 
            ((Y1564_MAX_RATE_STEP) / pSlaEntry->u2SlaRateStep);

        if (pSlaEntry->u2SlaCurrentStep == Y1564_ZERO)
        {
            pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep = Y1564_ONE;
        }
        else
        {
            pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaRateStep = (UINT2)
                (pSlaEntry->u2SlaCurrentRateStep + 
                pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaRateStep);

            pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep = 
                pSlaEntry->u2SlaCurrentStep;
        }

        pSlaEntry->u2SlaCurrentRateStep = 
            pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaRateStep;
        pSlaEntry->u2SlaCurrentStep = 
            pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep;

        u1CurrentTestSelector = Y1564_STEP_LOAD_CIR_TEST;
    }

    /* If EIR/Traffic Policing test is selected then update the
     * current step according to color mode */

    if ((pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
         Y1564_TRAF_POLICING_TEST_COLOR_BLIND) ||
        (pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
         Y1564_TRAF_POLICING_TEST_COLOR_AWARE))
    {
        pSlaEntry->u2SlaCurrentStep = Y1564_ONE;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep =
            pSlaEntry->u2SlaCurrentStep;

        u1CurrentTestSelector = Y1564_TRAF_POLICING_TEST;
    }

    if ((pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
         Y1564_EIR_TEST_COLOR_BLIND) ||
        (pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode ==
         Y1564_EIR_TEST_COLOR_AWARE))
    {
        pSlaEntry->u2SlaCurrentStep = Y1564_ONE;
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep =
            pSlaEntry->u2SlaCurrentStep;

        u1CurrentTestSelector = Y1564_EIR_TEST;
    }

    pSlaEntry->u1SlaCurrentTestMode = 
        pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode;

    if (Y1564PortGetTestRate (pSlaEntry, pY1564ExtInParams) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_ALL_FAILURE_TRC," %s : Failed in "
                           "Y1564PortGetTestRate \r\n", __FUNCTION__);
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }

    pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

   /* Delete the configuration report entry if its already present */

    if (((pSlaEntry->u2SlaCurrentStep == Y1564_ONE) &&
         ((pSlaEntry->u1SlaConfTestSelector & Y1564_ALL_TEST) == Y1564_ALL_TEST) &&
         (pSlaEntry->u1SlaCurrentTestMode == Y1564_SIMPLE_CIR_TEST)) ||
        (((pSlaEntry->u1SlaConfTestSelector & Y1564_ALL_TEST) != Y1564_ALL_TEST) &&
         (pSlaEntry->u1SlaConfTestSelector == 
          (pSlaEntry->u1LastSlaConfTestSelector | u1CurrentTestSelector)) 
         && (pSlaEntry->u2SlaCurrentStep == Y1564_ONE)))

    {
        for (u1SlaCurrentTestMode = Y1564_ONE; u1SlaCurrentTestMode <= Y1564_SIX;
             u1SlaCurrentTestMode++)
        {
            pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                                    pSlaEntry->u4SlaId,
                                                                    pSlaEntry->i4CurrentFrSize,
                                                                    u1SlaCurrentTestMode,
                                                                    pSlaEntry->u2SlaCurrentStep); 
            while (pConfTestReportEntry != NULL)
            {
                pNextConfTestReportEntry =
                    Y1564UtilGetNextNodeFromConfTestReportTable (pConfTestReportEntry);

                if ((pSlaEntry->u4ContextId == pConfTestReportEntry->u4ContextId) &&
                    (pSlaEntry->u4SlaId == pConfTestReportEntry->u4SlaId))
                {
                    if (Y1564UtilConfTestReportDelNodeFromRBTree (pConfTestReportEntry)
                        != OSIX_SUCCESS)
                    {
                        Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId,
                                           Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC
                                           | Y1564_RESOURCE_TRC,
                                           " %s : RBTree remove failed for SLA Id "
                                           "%d \r\n", __FUNCTION__,
                                           pConfTestReportEntry->u4SlaId);
                        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
                        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
                        return OSIX_FAILURE;
                    }
                }
                pConfTestReportEntry = pNextConfTestReportEntry;
            }
        }
    }

    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                            pSlaEntry->u4SlaId,
                                                            pSlaEntry->i4CurrentFrSize,
                                                            pSlaEntry->u1SlaCurrentTestMode,
                                                            pSlaEntry->u2SlaCurrentStep);

    if (pConfTestReportEntry == NULL)
    {
        /* Count is fetched from RBTreeCount datastructure and it is
         * compared with maximum Number of Configuration Test Report
         * entries that are allowed to create */
        RBTreeCount (Y1564_CONF_REPORT_TABLE(), &u4EntryCount);

        if (u4EntryCount == MAX_Y1564_CONF_TEST_REPORT_ENTRIES)
        {
            Y1564_CONTEXT_TRC (pConfTestReportEntry->u4ContextId, Y1564_CTRL_TRC |
                               Y1564_ALL_FAILURE_TRC |  Y1564_RESOURCE_TRC,
                               " %s : Maximum number of allowed Configuration Test "
                               "Report Entries reached \r\n",
                               __FUNCTION__);
            CLI_SET_ERR (CLI_Y1564_ERR_MAX_CONF_TEST_REPORT_REACHED);
            return OSIX_FAILURE;
        }

        pConfTestReportEntry = Y1564UtilCreateConfTestReportNode ();

        if (pConfTestReportEntry == NULL)
        {
            Y1564_GLOBAL_TRC ("Y1564UtilProcessConfTestResult : Memory "
                              "Allocation failed for ConfTestReport Entry \r\n");
            MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
            MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
            return OSIX_FAILURE;
        }

        MEMSET (pConfTestReportEntry, Y1564_ZERO, sizeof (tConfTestReportInfo));

        pConfTestReportEntry->u4ContextId = pSlaEntry->u4ContextId;
        pConfTestReportEntry->u4SlaId = pSlaEntry->u4SlaId;
        pConfTestReportEntry->u4CurrentTestMode =  pSlaEntry->u1SlaCurrentTestMode;
        pConfTestReportEntry->u4StatsStepId = (UINT4) pSlaEntry->u2SlaCurrentStep;
        pConfTestReportEntry->i4FrameSize = pSlaEntry->i4CurrentFrSize;
        pConfTestReportEntry->u1StatsResult = pSlaEntry->u1SlaCurrentTestState;
        pConfTestReportEntry->u4StatsDuration = pSlaEntry->u4SlaConfTestDuration;

        if (pTrafProfEntry->u1EmixSelected  != Y1564_TRUE)
        {
            pConfTestReportEntry->TestStartTimeStamp =  pSlaEntry->TestStartTimeStamp;
        }

        if ((pSlaEntry->u2SlaCurrentStep == Y1564_ONE) &&
            (pTrafProfEntry->u1EmixSelected == Y1564_TRUE))
        {
            UtlGetPreciseSysTime (&SysPreciseTime);
            pConfTestReportEntry->TestStartTimeStamp = SysPreciseTime.u4Sec;
        }


        if (Y1564UtilAddNodeToConfTestReportRBTree (pConfTestReportEntry)
            != OSIX_SUCCESS)
        {
            /* Release memory allocated for Conf Test Report Entry */
            MemReleaseMemBlock (Y1564_CONF_REPORT_POOL (), (UINT1 *) pConfTestReportEntry);
            pConfTestReportEntry = NULL;

            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                               Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                               "%s : Conf Test Entry Add Failed for Sla Id %d\r\n",
                               __FUNCTION__, pSlaEntry->u4SlaId);
           MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
           MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

            return OSIX_FAILURE;
        }

    }

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_SLA_CONF_TEST_START \r\n", __FUNCTION__); 
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : Y1564PortSlaStopConfTest                              *
 *                                                                            *
 * Description        : This function calls the CFM module to stop the        *
 *                      below tests.                                          *
 *                      1. Frame Loss                                         *
 *                      2. Frame Delay                                        *
 *                      3. Frame Delay Variation                              *
 *                      4. Test Frame Generation                              *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortSlaStopConfTest (tSlaInfo *pSlaEntry) 
{
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

   pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                pSlaEntry->u4SlaTrafProfId);
    if (pTrafProfEntry == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    if (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST)
    {
        pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_PERF_TEST_STOP_TEST;
    }
    else
    {
        pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_CONF_TEST_STOP_TEST;
    }

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;
    pY1564ExtInParams->Y1564EcfmReqParams.u2EvcId = pTrafProfEntry->u2OutVlanId;
    pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex = pTrafProfEntry->u4IfIndex;
    pY1564ExtInParams->Y1564EcfmReqParams.OutVlanId = pTrafProfEntry->u2OutVlanId;

    pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_ABORTED;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_CONF_TEST_STOP_TEST \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }
    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    return i4RetVal;
}
/******************************************************************************
 * Function Name      : Y1564PortMepGetPerformanceResult                      *
 *                                                                            *
 * Description        : This function calls the CFM module to get the         *
 *                      performance test result for the below tests.          *
 *                      1. Frame Loss                                         *
 *                      2. Frame Delay                                        *
 *                      3. Frame Delay Variation                              *
 *                      4. Test Frame Generation                              *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortMepGetPerformanceResult (tSlaInfo * pSlaEntry)
{
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    tTrafficProfileInfo *pTrafProfEntry = NULL;

    INT4                i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams)); 

    pY1564ExtInParams->eExtReqType = Y1564_REQ_CFM_GET_TEST_REPORT;

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
		    pSlaEntry->u4SlaTrafProfId);

    if (pTrafProfEntry == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
	    return OSIX_FAILURE;
    }
    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4TransId = pSlaEntry->u4TransId;

    pY1564ExtInParams->Y1564EcfmReqParams.OutVlanId = pTrafProfEntry->u2OutVlanId;
    pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex = pTrafProfEntry->u4IfIndex;

    /* Test Param Information */
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaRateStep =
        pSlaEntry->u2SlaCurrentRateStep;

    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u2SlaCurrentStep =
        pSlaEntry->u2SlaCurrentStep;

    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode =
        pSlaEntry->u1SlaCurrentTestMode ;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_GET_TEST_REPORT \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    return i4RetVal;
}
/******************************************************************************
 * Function Name      : Y1564PortSlaStartPerfTest                             *
 *                                                                            *
 * Description        : This function calls the CFM module to start the       *
 *                      below tests.                                          *
 *                      1. Frame Loss                                         *
 *                      2. Frame Delay                                        *
 *                      3. Frame Delay Variation                              *
 *                      4. Test Frame Generation                              *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564PortSlaStartPerfTest (tSlaInfo *pSlaEntry, tTrafficProfileInfo *pTrafProfEntry,
                           UINT4 u4Duration, UINT4 u4PerfTestId)
{
    tY1564ExtInParams       *pY1564ExtInParams = NULL;
    tY1564ExtOutParams      *pY1564ExtOutParams = NULL;
    tPerfTestReportInfo     *pPerfTestReportEntry = NULL;
    tPerfTestReportInfo     *pNextPerfTestReportEntry = NULL;
    UINT4                    u4EntryCount = Y1564_ZERO;
    INT4                     i4RetVal = OSIX_SUCCESS;
    tMacAddr                 DstMacAddr;

    MEMSET (DstMacAddr, Y1564_ZERO, sizeof (tMacAddr));

    if (Y1564UtilGetSlaTestParams (pSlaEntry) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : Failed to get "
                           "MEP related information for MEP %d ME %d MEG "
                           " %d in ECFM/Y.1731 \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaMepId,
                           pSlaEntry->u4SlaMeId,pSlaEntry->u4SlaMegId);
        return OSIX_FAILURE;
    }

    if (MEMCMP (pTrafProfEntry->DstMacAddr, DstMacAddr, sizeof (tMacAddr)) == Y1564_ZERO)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,  Y1564_ALL_FAILURE_TRC,
                           " %s  Failed because either ECFM is not settled "
                           "or Remote node is not reachable \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_REMOTE_NODE_NOT_REACHABLE);
        return OSIX_FAILURE;
    } 
    
    pSlaEntry->u1LastSlaConfTestSelector &= (UINT1) ~Y1564_EIR_TEST;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_SLA_PERF_TEST_START;
    
    if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
    {
        /* Traffic Profile Information */
        pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize = 
            pTrafProfEntry->u2TrafProfPktSize;
    }
    else
    {
        pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize =
            pTrafProfEntry->u2CurrentEmixPktSize;
    }

    pSlaEntry->i4CurrentFrSize = 
        (INT4) pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u2FrameSize;

    if ((UINT4) pSlaEntry->i4CurrentFrSize > pTrafProfEntry->u4IfMtu)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,Y1564_ALL_FAILURE_TRC ,
                           " %s : Frame Size : %d is greater than "
                           "MTU size : %d of the port. So test will not be initiated \r\n",
                           __FUNCTION__,pSlaEntry->i4CurrentFrSize,
                           pTrafProfEntry->u4IfMtu);
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        CLI_SET_ERR(CLI_Y1564_FR_SIZE_GREATER_THAN_MTU);
        return OSIX_FAILURE;
    }

    /* MEP Information */
    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMegId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMeId;
    pY1564ExtInParams->Y1564EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMepId;

    pY1564ExtInParams->Y1564EcfmReqParams.OutVlanId = pTrafProfEntry->u2OutVlanId;
    pY1564ExtInParams->Y1564EcfmReqParams.u4IfIndex = pTrafProfEntry->u4IfIndex;
    pY1564ExtInParams->Y1564EcfmReqParams.u4TagType = pTrafProfEntry->u4TagType;
    pY1564ExtInParams->Y1564EcfmReqParams.u4PortSpeed = pTrafProfEntry->u4PortSpeed;

    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TxSrcMacAddr , pTrafProfEntry->SrcMacAddr ,
            sizeof(tMacAddr));
    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TxDstMacAddr , pTrafProfEntry->DstMacAddr ,
            sizeof(tMacAddr));

    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.au1TrafProfPayload,
            pTrafProfEntry->au1TrafProfPayload, STRLEN
            (pTrafProfEntry->au1TrafProfPayload));
    pY1564ExtInParams->Y1564EcfmReqParams.TrafficProfileParam.u1TrafProfPCP =
        pTrafProfEntry->u1TrafProfPCP;

    /* Test Param Information */

    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4TestDuration =  u4Duration / 1000;

    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u4CIR = pSlaEntry->u4CIR;

    pSlaEntry->u1SlaCurrentTestMode = Y1564_PERF_TEST; 
    pY1564ExtInParams->Y1564EcfmReqParams.TestParam.u1TestMode = 
        pSlaEntry->u1SlaCurrentTestMode;
 
    if (Y1564PortGetTestRate (pSlaEntry, pY1564ExtInParams) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId,
                           Y1564_CRITICAL_TRC," %s : Failed in "
                           "Y1564PortGetTestRate \r\n", __FUNCTION__);
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }

    pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (pSlaEntry->u4ContextId,
                                                            pSlaEntry->u4SlaId,u4PerfTestId,
                                                            pSlaEntry->i4CurrentFrSize);         
    while (pPerfTestReportEntry != NULL) 
    {
        pNextPerfTestReportEntry = Y1564UtilGetNextNodeFromPerfTestReportTable
            (pPerfTestReportEntry);

        if ((pPerfTestReportEntry->u4StatsPerfId == u4PerfTestId) &&
            (pPerfTestReportEntry->u4ContextId == pSlaEntry->u4ContextId))
        {
            if (Y1564UtilPerfTestReportDelNodeFromRBTree (pPerfTestReportEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId,
                                   Y1564_CTRL_TRC | Y1564_ALL_FAILURE_TRC
                                   | Y1564_RESOURCE_TRC,
                                   " %s : RBTree remove failed for Sla Id "
                                   "%d in Perf Test Id %d \r\n", __FUNCTION__,
                                   pPerfTestReportEntry->u4SlaId,
                                   u4PerfTestId);
                MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
                MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
                return OSIX_FAILURE;
            }
        }
         /* Update current entry as NULL and assign the next entry */
         pPerfTestReportEntry = NULL;
         pPerfTestReportEntry = pNextPerfTestReportEntry;
    }
    
    if (pPerfTestReportEntry == NULL)
    {
        /* Count is fetched from RBTreeCount datastructure and it is
         * compared with maximum Number of Performance Test Report
         * entries that are allowed to create */
        RBTreeCount (Y1564_PERF_REPORT_TABLE(), &u4EntryCount);

        if (u4EntryCount == MAX_Y1564_PERF_TEST_REPORT_ENTRIES)
        {
            Y1564_CONTEXT_TRC (pPerfTestReportEntry->u4ContextId, Y1564_CTRL_TRC |
                               Y1564_ALL_FAILURE_TRC |  Y1564_RESOURCE_TRC,
                               " %s : Maximum number of allowed Performance Test "
                               "Report Entries reached \r\n",
                               __FUNCTION__);
            CLI_SET_ERR (CLI_Y1564_ERR_MAX_PERF_TEST_REPORT_REACHED);
            return OSIX_FAILURE;
        }

        pPerfTestReportEntry = Y1564UtilCreatePerfTestReportNode ();

        if (pPerfTestReportEntry == NULL)
        {
            Y1564_GLOBAL_TRC ("Y1564PortSlaStartPerfTest : Memory "
                              "Allocation failed for PerfTestReport Entry \r\n");
            MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
            MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
            return OSIX_FAILURE;
        }

        pPerfTestReportEntry->u4ContextId = pSlaEntry->u4ContextId;
        pPerfTestReportEntry->u4SlaId = pSlaEntry->u4SlaId;
        pPerfTestReportEntry->u4StatsPerfId = u4PerfTestId;
        pPerfTestReportEntry->i4FrameSize = pSlaEntry->i4CurrentFrSize; 

        pPerfTestReportEntry->PerfTestStartTimeStamp =  pSlaEntry->TestStartTimeStamp;
        pPerfTestReportEntry->u1StatsResult = pSlaEntry->u1SlaCurrentTestState;

        if (Y1564UtilAddNodeToPerfTestReportRBTree (pPerfTestReportEntry)
            != OSIX_SUCCESS)
        {
			  /* Release memory allocated for Perf Test Report Entry */
			  MemReleaseMemBlock (Y1564_PERF_REPORT_POOL (), (UINT1 *) pPerfTestReportEntry);
			  pPerfTestReportEntry = NULL;

			  Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
					  Y1564_MGMT_TRC | Y1564_CRITICAL_TRC,
					  "%s : Perf Test Report Entry Add Failed for Sla Id %d\r\n",
					  __FUNCTION__, pSlaEntry->u4SlaId);
                          MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
                          MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);
			  return OSIX_FAILURE;
		  }
	  }

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId, Y1564_CRITICAL_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_CFM_START_PERF_TEST \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    return i4RetVal;
}

/*****************************************************************************
 * Function Name      : Y1564PortExternalEventNotify                         *
 *                                                                           *
 * Description        : This routine populates the tY1564ReqParams           *
 *                      structure from the tEcfmEventNotification structure. *
 *                                                                           *
 * Input(s)           : pEvent - pointer to tEcfmEventNotification structure *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
Y1564PortExternalEventNotify (tEcfmEventNotification * pEvent)
{
    tY1564ReqParams *pY1564ReqParams = NULL;
    tY1564RespParams *pY1564RespParams = NULL;

    pY1564ReqParams = (tY1564ReqParams *)MemAllocMemBlk (Y1564_REQPARAMS_POOL ());

    if (pY1564ReqParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ReqParams failed\r\n");
        return;
    }
    MEMSET (pY1564ReqParams, Y1564_ZERO, sizeof (tY1564ReqParams));

    pY1564ReqParams->u4ReqType = pEvent->u4Event;
    pY1564ReqParams->u4ContextId = pEvent->u4ContextId;
    pY1564ReqParams->u4VlanIdIsid = pEvent->u4VlanIdIsid;
    pY1564ReqParams->u4SlaId = pEvent->u4SlaId;
    pY1564ReqParams->u4MdIndex = pEvent->u4MdIndex;
    pY1564ReqParams->u4MaIndex = pEvent->u4MaIndex;
    pY1564ReqParams->u2MepId = pEvent->u2MepId;
    pY1564ReqParams->u1Direction = pEvent->u1Direction;

    /* Fill the result info structure here */
    
   if (Y1564ApiHandleExtRequest (pY1564ReqParams, pY1564RespParams) != OSIX_SUCCESS)
   {
       MemReleaseMemBlock (Y1564_REQPARAMS_POOL (), (UINT1 *) pY1564ReqParams);
       pY1564ReqParams = NULL;
       return;
   }

    MemReleaseMemBlock (Y1564_REQPARAMS_POOL (), (UINT1 *) pY1564ReqParams);
    return;
}

/****************************************************************************
 * FUNCTION NAME    : Y1564L2IwfGetBridgeMode                               *
 *                                                                          *
 * DESCRIPTION      : Routine used to get the bridge mode for the context   *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           : pu4BridgeMode - Bridge Mode                           *
 *                                                                          *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
Y1564L2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL ());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = u4ContextId;
    pY1564ExtInParams->eExtReqType = Y1564_REQ_L2IWF_GET_BRG_MODE;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId, 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : Failed while processing the request "
                           " Y1564_REQ_L2IWF_GET_BRG_MODE \r\n", __FUNCTION__);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        *pu4BridgeMode = pY1564ExtOutParams->u4BrgMode;
    }

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL (), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL (), (UINT1 *) pY1564ExtOutParams);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : Y1564PortRmGetNodeState                             */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Y1564PortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
        return (RmGetNodeState ());
#else
            return RM_ACTIVE;
#endif
}

