/*****************************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564cxt.c,v 1.10 2016/03/06 10:05:32 siva Exp $
 *
 * Description: This file contains the Y1564 Context related functions
 *
 *****************************************************************************/
#include "y1564inc.h"


/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564CxtGetContextEntry                          *
 *                                                                           *
 *    Description         : This function used to get the pointer to the     *
 *                          context info entry.                              *
 *                                                                           *
 *    Input(s)            : u4ContextId - Context index                      *
 *                                                                           *
 *    Output(s)           : None.                                            *
 *                                                                           *
 *    Returns             : pointer to tY1564ContextInfo                     *
 *                                                                           *
 *****************************************************************************/
PUBLIC tY1564ContextInfo *
Y1564CxtGetContextEntry (UINT4 u4ContextId)
{
    if (u4ContextId >= Y1564_SIZING_CONTEXT_COUNT)
    {
        return NULL;
    }

    return gY1564GlobalInfo.apY1564ContextInfo[u4ContextId];
}

/*****************************************************************************
 * Function Name      : Y1564VcmGetSystemMode                                *
 *                                                                           *
 * Description        : This function calls the VCM Module to get the        *
 *                      mode of the system (SI / MI).                        *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 ****************************************************************************/
PUBLIC INT4
Y1564VcmGetSystemMode ()
{
   
    INT4                i4Return = Y1564_ZERO;
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;

    pY1564ExtInParams = (tY1564ExtInParams *)MemAllocMemBlk (Y1564_EXTINPARAMS_POOL ());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_VCM_GET_SYSTEM_MODE;

    i4Return = Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams);

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL(), (UINT1 *) pY1564ExtOutParams);

    return i4Return;
}
/******************************************************************************
 * Function Name      : Y1564VcmGetContextInfoFromIfIndex                     *
 *                                                                            *
 * Description        : This function calls the VCM Module to get the         *
 *                      Context-Id and the Localport number.                  *
 *                                                                            *
 * Input(s)           : u4IfIndex - Interface Identifier.                     *
 *                                                                            *
 * Output(s)          : pu4ContextId - Context Identifier.                    *
 *                      pu2LocalPortId - Local port number.                   *
 *                                                                            *
 * Return Value(s)    : None                                                  *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
Y1564VcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}
/******************************************************************************
 * FUNCTION NAME    : Y1564VcmIsSwitchExist                                   *
 *                                                                            *
 * DESCRIPTION      : Routine used to get if the context exists for the input *
 *                    Switch Alias name Information                           *
 *                                                                            *
 * INPUT            : pu1Alias - Context Name                                 *
 *                                                                            *
 * OUTPUT           : pu4ContextId - Context Identifier                       *
 *                                                                            *
 * RETURNS          : OSIX_TRUE / OSIX_FALSE                                  *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
Y1564VcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams)); 

    MEMSET (pY1564ExtInParams->Y1564EcfmReqParams.au1Alias, Y1564_ZERO,
            sizeof (pY1564ExtInParams->Y1564EcfmReqParams.au1Alias));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams)); 

    pY1564ExtInParams->eExtReqType = Y1564_REQ_VCM_IS_CONTEXT_VALID;
    MEMCPY (pY1564ExtInParams->Y1564EcfmReqParams.au1Alias, pu1Alias, STRLEN(pu1Alias));

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL(), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4ContextId = pY1564ExtOutParams->u4ContextId;

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL(), (UINT1 *) pY1564ExtOutParams);
    return OSIX_SUCCESS;

}
/*****************************************************************************
 *    Function Name       : Y1564CxtMemInit                                  *
 *                                                                           *
 *    Description         : This function used to create and do the proper   *
 *                          initialization for the context entry.            *
 *                                                                           *
 *    Input(s)            : u4ContextId - Context index                      *
 *                                                                           *
 *    Output(s)           : None.                                            *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      *
 *                                                                           *
 *****************************************************************************/
PUBLIC tY1564ContextInfo *
Y1564CxtMemInit (UINT4 u4ContextId)
{

    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = (tY1564ContextInfo *) MemAllocMemBlk (Y1564_CTXT_INFO_POOL());

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_RESOURCE_TRC,
                           " %s : Memory Allocation for context info "
                           " failed \r\n", __FUNCTION__);
        gY1564GlobalInfo.u4MemFailCount++;
        return NULL;
    }

    MEMSET (pContextInfo, Y1564_ZERO, sizeof (tY1564ContextInfo));

    gY1564GlobalInfo.apY1564ContextInfo[u4ContextId] = pContextInfo;

    pContextInfo->u4ContextId = u4ContextId;
    pContextInfo->u1ModuleStatus = Y1564_DISABLED;
    pContextInfo->u1TrapStatus = Y1564_SNMP_TRUE;
    pContextInfo->u4TraceOption = Y1564_CRITICAL_TRC;
  
    gau1Y1564SystemStatus[u4ContextId] =  Y1564_SHUTDOWN;

    if (Y1564VcmGetAliasName (u4ContextId, pContextInfo->au1ContextName) 
        == OSIX_FAILURE)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_RESOURCE_TRC,
                           " %s : Alais name get failure \r\n", __FUNCTION__);

        MemReleaseMemBlock (Y1564_CTXT_INFO_POOL (), (UINT1 *)pContextInfo);
        gY1564GlobalInfo.apY1564ContextInfo[u4ContextId] = NULL;
        return NULL;
    }
    return pContextInfo;
}

/*****************************************************************************
 *    Function Name       : Y1564CxtHandleCreateContext                      *
 *                                                                           *
 *    Description         : This function creates the context in the Y1564   *
 *                          Module                                           *
 *                                                                           *
 *    Input(s)            : None.                                            *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564CxtHandleCreateContext (UINT4 u4ContextId)
{
    tY1564ContextInfo *pContextInfo = NULL;

    pContextInfo = Y1564CxtMemInit (u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CxtHandleCreateContext : Context "
                          "creation failed in Y1564 \r\n");
        return ;

    }
    Y1564_GLOBAL_TRC ("Y1564CxtHandleCreateContext : Context "
                      "creation success in Y1564 \r\n");
    return ;
}
/*****************************************************************************
 *    Function Name       : Y1564CxtHandleDeleteContext                      *
 *                                                                           *
 *    Description         : This function will check whether any current     *
 *                          running test has this context ID and if present  *
 *                          it will increment the Port Status change counter *
 *                                                                           *
 *    Input(s)            : None.                                            *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564CxtHandleDeleteContext (UINT4 u4ContextId)
{
    tY1564ContextInfo *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CxtHandleDeleteContext : Context "
                          "information is not present \r\n");
        return;
    }

    if ((pContextInfo != NULL) &&
        (gY1564GlobalInfo.au1SystemCtrl[u4ContextId] == Y1564_START))
    {
        Y1564CxtModuleShutDown (u4ContextId);
    }
    gY1564GlobalInfo.au1SystemCtrl[u4ContextId] = Y1564_SHUTDOWN;
    gY1564GlobalInfo.apY1564ContextInfo[u4ContextId] = NULL;

    MemReleaseMemBlock (Y1564_CTXT_INFO_POOL (), (UINT1 *)pContextInfo);

    return;
}


/*****************************************************************************
 *    Function Name       : Y1564CxtHandleUpdateCxtName                      *
 *                                                                           *
 *    Description         : This function will update the context name in    *
 *                          Y1564 module whenever context name changes in    *
 *                          VCM                                              *
 *                                                                           *
 *    Input(s)            : u4ContextId - Context Identifier                 *
 *                                                                           *
 *    Output(s)           : None.                                            *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS                                     *
 *                          OSIX_FAILURE                                     *
 *****************************************************************************/
PUBLIC INT4
Y1564CxtHandleUpdateCxtName (UINT4 u4ContextId)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    if ((pContextInfo = Y1564CxtGetContextEntry (u4ContextId)) == NULL)
    {
        return OSIX_FAILURE;
    }

    if (Y1564VcmGetAliasName (u4ContextId, pContextInfo->au1ContextName)
        == OSIX_FAILURE)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_ALL_FAILURE_TRC | Y1564_MGMT_TRC,
                           " %s : Alais name get failure \r\n", __FUNCTION__);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
/*****************************************************************************
 *    Function Name       : Y1564CxtModuleShutDown                           *
 *                                                                           *
 *    Description         : This function shutdown the Y1564 module in the   *
 *                          given context                                    *
 *                                                                           *
 *    Input(s)            : None.                                            *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564CxtModuleShutDown (UINT4 u4ContextId)
{
    tY1564ContextInfo *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_CRITICAL_TRC," %s :"
                           "does not exist \r\n", __FUNCTION__);
        return;
    }

    if (Y1564CxtModuleDisable (pContextInfo) != OSIX_TRUE)
    {
        return;
    }

    Y1564UtilDelConfTestResultEntry (u4ContextId, Y1564_ZERO);
    
    /* Delete SLA RB Tree */
    Y1564UtilDeleteSlaTableInContext (u4ContextId);

    /* Delete SAC RB Tree */
    Y1564UtilDeleteSacTableInContext (u4ContextId);

    /* Delete traffic profile RB Tree */
    Y1564UtilDeleteTrafProfTableInContext (u4ContextId);
#ifndef MEF_WANTED
    /* Delete Service Configuration RB Tree */
    Y1564UtilDeleteServConfTableInContext (u4ContextId);
#endif
    /* Delete Performance Test RB Tree */
    Y1564UtilDeletePerfTestTableInContext (u4ContextId);

    /* Reset Context Info structure to default values
     * Since Context structure  memory is not
     * released and it will hold the configured 
     * values when shutdown and no shutdown 
     * is given */

    pContextInfo->u1TrapStatus = Y1564_SNMP_TRUE;
    pContextInfo->u4TraceOption = Y1564_CRITICAL_TRC;

    gY1564GlobalInfo.au1SystemCtrl[u4ContextId] = Y1564_SHUTDOWN;

    return;
}
/******************************************************************************
 *    Function Name       : Y1564CxtStopCurrentlyRunningTest                  *
 *                                                                            *
 *    Description         : This function stops the currently running test in *
 *                          the Y1564 module for the given context            *
 *                                                                            *
 *    Input(s)            : None.                                             *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : None.                                             *
 *****************************************************************************/
PUBLIC INT4
Y1564CxtStopCurrentlyRunningTest (UINT4 u4ContextId)
{
    tSlaInfo          * pSlaEntry = NULL;
    tY1564ContextInfo * pContextInfo = NULL;
    tPerfTestInfo     * pPerfTestEntry = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CxtStopCurrentlyRunningTest : Context "
                          "information is not present \r\n");
        return OSIX_FAILURE;
    }

    pPerfTestEntry = Y1564UtilGetFirstNodeFromPerfTestTable ();

    while (pPerfTestEntry != NULL)
    {
        if (pPerfTestEntry->u4ContextId == u4ContextId)
        {
            if (Y1564CoreStopPerfTest (pPerfTestEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, 
                                   Y1564_CRITICAL_TRC, " %s : "
                                   "Performance test stop failed "
                                   "\r\n", __FUNCTION__); 
                return OSIX_FAILURE;
            }
        }
        pPerfTestEntry = Y1564UtilGetNextNodeFromPerfTestTable (pPerfTestEntry);
    }

    pSlaEntry = Y1564UtilSlaGetFirstNodeFromSlaTable ();

    while (pSlaEntry != NULL)
    {
        if (pSlaEntry->u4ContextId == u4ContextId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                if (Y1564CoreStopConfTest (pSlaEntry) != OSIX_SUCCESS)
                { 
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC, " %s : "
                                       "Configuration test stop failed for SLA Id %d \r\n",
                                       __FUNCTION__,pSlaEntry->u4SlaId);
                    return OSIX_FAILURE;
                }
            }

            if ((pSlaEntry->u4SlaMegId != Y1564_ZERO) &&
                (pSlaEntry->u4SlaMeId != Y1564_ZERO) &&
                (pSlaEntry->u4SlaMepId != Y1564_ZERO))
            {
                if (Y1564PortMepDeRegister (u4ContextId, pSlaEntry->u4SlaMegId,
                                            pSlaEntry->u4SlaMeId, pSlaEntry->u4SlaMepId)
                    != OSIX_SUCCESS)
                {
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                                       Y1564_ALL_FAILURE_TRC," %s : Failed to De-register"
                                       "with ECFM/Y.1731 for MEP %d ME %d MEG %d"
                                       "\r\n", __FUNCTION__, pSlaEntry->u4SlaMepId,
                                       pSlaEntry->u4SlaMeId, pSlaEntry->u4SlaMegId);
                    return OSIX_FAILURE;
                }
            }
        }
        pSlaEntry = Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry);
    }

    return OSIX_SUCCESS;
}
/****************************************************************************
 * FUNCTION NAME    : Y1564CxtIsY1564Started                                *
 *                                                                          *
 * DESCRIPTION      : This function will check whether Y15644 is started in *
 *                    context. If started, this function will return        *
 *                    OSIX_TRUE, OSIX_FALSE otherwise.                      *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           : NONE                                                  *
 *                                                                          *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE                                  *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
Y1564CxtIsY1564Started (UINT4 u4ContextId)
{
    if (u4ContextId >= Y1564_SIZING_CONTEXT_COUNT)
    {
        return OSIX_FALSE;
    }
    if (gY1564GlobalInfo.apY1564ContextInfo[u4ContextId] == NULL)
    {
        /* Y1564 is not initialized in this context */
        return OSIX_FALSE;
    }
    return ((gau1Y1564SystemStatus[u4ContextId]
             == Y1564_START) ? OSIX_TRUE : OSIX_FALSE);
}
/****************************************************************************
 * FUNCTION NAME    : Y1564CxtModuleEnable                                  *
 *                                                                          *
 * DESCRIPTION      : This function will check whether the context is       *
 *                    created and also the system control of the context.   *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           :                                                       *
 *                                                                          *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT4
Y1564CxtModuleEnable (tY1564ContextInfo * pContextInfo)
{
    tSlaInfo *pSlaEntry = NULL;

    if (pContextInfo == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CxtModuleEnable : Context information "
                          "is NULL \r\n");
        return OSIX_FAILURE;
    }
    if (Y1564CxtIsY1564Started (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        Y1564_CONTEXT_TRC (pContextInfo->u4ContextId, Y1564_CRITICAL_TRC,
                           " %s : Y1564 Module is shutdown \r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        /* *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;*/
        return OSIX_FAILURE;
    }

    pSlaEntry = Y1564UtilSlaGetFirstNodeFromSlaTable ();

    while (pSlaEntry != NULL)
    {
        if ((pSlaEntry->u4ContextId == pContextInfo->u4ContextId) &&
            (pSlaEntry->u4SlaMepId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaMegId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaMeId != Y1564_ZERO))
        {
            if (Y1564UtilValidateSlaCFMParams (pSlaEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pContextInfo->u4ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC,"%s : CFM Entry "
                                   "MEP Id %d validation failed for SLA %d\r\n",
                                   __FUNCTION__,pSlaEntry->u4SlaMepId,
                                   pSlaEntry->u4SlaId);
            }
        }
        pSlaEntry = Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : Y1564CxtModuleDisable                                 *
 *                                                                          *
 * DESCRIPTION      : This function will check whether the context is       *
 *                    created and also the system control of the context.   *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           :                                                       *
 *                                                                          *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE                                  *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT4
Y1564CxtModuleDisable (tY1564ContextInfo * pContextInfo)
{
    if (pContextInfo == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564CxtModuleDisable : Context information "
                          "is NULL \r\n");
        return OSIX_FALSE;
    }
    if (Y1564CxtIsY1564Started (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        Y1564_CONTEXT_TRC (pContextInfo->u4ContextId, 
                           Y1564_CRITICAL_TRC,
                           " %s : Y1564 Module is shutdown \r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        /* *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;*/
        return OSIX_FALSE;
    }

    if (Y1564CxtStopCurrentlyRunningTest (pContextInfo->u4ContextId)
        == OSIX_FAILURE)
    {
        Y1564_CONTEXT_TRC (pContextInfo->u4ContextId, Y1564_CRITICAL_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Unable to stop "
                           "the currently running test \r\n",
                           __FUNCTION__);
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

/****************************************************************************
 * FUNCTION NAME    : Y1564VcmGetAliasName                                  *
 *                                                                          *
 * DESCRIPTION      : Routine used to get the Alias Name for the context    *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           : pu1Alias - Context Name                               *
 *                                                                          *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
Y1564VcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    
    tY1564ExtInParams  *pY1564ExtInParams = NULL;
    tY1564ExtOutParams  *pY1564ExtOutParams = NULL;

    pY1564ExtInParams = (tY1564ExtInParams *)
        MemAllocMemBlk (Y1564_EXTINPARAMS_POOL());

    if (pY1564ExtInParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtInParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ExtInParams, Y1564_ZERO, sizeof (tY1564ExtInParams));

    pY1564ExtOutParams = (tY1564ExtOutParams *)
        MemAllocMemBlk (Y1564_EXTOUTPARAMS_POOL());

    if (pY1564ExtOutParams == NULL)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ExtOutParam failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pY1564ExtOutParams, Y1564_ZERO, sizeof (tY1564ExtOutParams));

    pY1564ExtInParams->eExtReqType = Y1564_REQ_VCM_GET_CONTEXT_NAME;

    pY1564ExtInParams->Y1564EcfmReqParams.u4ContextId = u4ContextId;

    if (Y1564PortHandleExtInteraction (pY1564ExtInParams, pY1564ExtOutParams)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
        MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL(), (UINT1 *) pY1564ExtOutParams);
        return OSIX_FAILURE;
    }
    MEMCPY (pu1Alias, pY1564ExtOutParams->au1ContextName, STRLEN(pY1564ExtOutParams->au1ContextName));

    MemReleaseMemBlock (Y1564_EXTINPARAMS_POOL(), (UINT1 *) pY1564ExtInParams);
    MemReleaseMemBlock (Y1564_EXTOUTPARAMS_POOL(), (UINT1 *) pY1564ExtOutParams);
    return OSIX_SUCCESS;
}
