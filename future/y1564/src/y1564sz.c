/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564sz.c,v 1.2 2016/05/19 10:48:28 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _Y1564SZ_C
#include "y1564inc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  Y1564SizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < Y1564_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal =(INT4)MemCreateMemPool( 
                          FsY1564SizingParams[i4SizingId].u4StructSize,
                          FsY1564SizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(Y1564MemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            Y1564SizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   Y1564SzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsY1564SizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, Y1564MemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  Y1564SizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < Y1564_MAX_SIZING_ID; i4SizingId++) {
        if(Y1564MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( Y1564MemPoolIds[ i4SizingId] );
            Y1564MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
