/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564msg.c,v 1.8 2016/02/18 10:44:35 siva Exp $
 *
 * Description: This file contains the Y1564 Msg related functions        
 *                                                        
 *****************************************************************************/
#ifndef _Y1564MSG_C
#define _Y1564MSG_C

#include "y1564inc.h"

/****************************************************************************/
PRIVATE VOID Y1564MsgHandleCfmSignalMsg (tY1564QMsg *pMsg);

PRIVATE VOID
Y1564MsgProcessTestResult (tY1564QMsg * pMsg);

/***************************************************************************
 * FUNCTION NAME    : Y1564MsgEnque                                        *
 *                                                                         *
 * DESCRIPTION      : Function is used to post queue message to Y1564 task.*
 *                                                                         *
 * INPUT            : pMsg - Pointer to the message to be posted.          *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          *
 *                                                                         *
 ***************************************************************************/
INT4
Y1564MsgEnque (tY1564QMsg * pMsg)
{
    if (OsixQueSend (Y1564_QUEUE_ID (), (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (Y1564_QMSG_POOL(), (UINT1 *) pMsg);
        Y1564_GLOBAL_TRC ("Y1564MsgEnque: Osix Queue Send Failed!!!\r\n");
        return OSIX_FAILURE;
    }
    if (OsixEvtSend (Y1564_TASK_ID (), Y1564_QMSG_EVENT) == OSIX_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564MsgEnque: Osix Event Send Failed!!!\r\n");
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : Y1564MsgQueueHandler                             *
 *                                                                           *
 *    Description         : This function will receive messages posted to    *
 *                          Y1564 task to indicate occurance of event        *
 *                                                                           *
 *    Input(s)            : None.                                            *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564MsgQueueHandler (VOID)
{
    tY1564QMsg   *pMsg = NULL;
    while (OsixQueRecv (Y1564_QUEUE_ID (), (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->Y1564ReqParam.u4ReqType)
        {
            case Y1564_CREATE_CONTEXT_MSG:
                Y1564CxtHandleCreateContext (pMsg->Y1564ReqParam.u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case Y1564_DELETE_CONTEXT_MSG:
                Y1564CxtHandleDeleteContext (pMsg->Y1564ReqParam.u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case ECFM_DEFECT_CONDITION_ENCOUNTERED:
            case ECFM_DEFECT_CONDITION_CLEARED:
            case ECFM_RDI_CONDITION_ENCOUNTERED:
            case ECFM_RDI_CONDITION_CLEARED:

                if ((pMsg->Y1564ReqParam.u4ReqType == ECFM_DEFECT_CONDITION_ENCOUNTERED)
                    || (pMsg->Y1564ReqParam.u4ReqType == ECFM_RDI_CONDITION_ENCOUNTERED))
                {
                    pMsg->Y1564ReqParam.u4Status = Y1564_CFM_SIGNAL_FAIL;

                }
                else if ((pMsg->Y1564ReqParam.u4ReqType == ECFM_DEFECT_CONDITION_CLEARED)
                         || (pMsg->Y1564ReqParam.u4ReqType == ECFM_RDI_CONDITION_CLEARED))
                {
                    pMsg->Y1564ReqParam.u4Status = Y1564_CFM_SIGNAL_OK;
                }
                Y1564MsgHandleCfmSignalMsg (pMsg);
                break;

            case ECFM_Y1564_RESULT_RECEIVED:                  
                Y1564MsgProcessTestResult (pMsg);
                break;
            case Y1564_UPDATE_CONTEXT_NAME:
                Y1564CxtHandleUpdateCxtName (pMsg->Y1564ReqParam.u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            default:
                break;
        }

        /* Release the buffer to pool */
        if (MemReleaseMemBlock (Y1564_QMSG_POOL (), (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            Y1564_GLOBAL_TRC ("ErpsMsgQueueHandler:Free MemBlock QMsg FAILED\n");
        }

    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564MsgProcessTestResult                            *
 *                                                                         *
 * DESCRIPTION      : Function is used to process the test result received *
 *                    ECFM/Y1731                                           *
 *                                                                         *
 * INPUT            : pMsg - Pointer to the message to be posted.          *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : NONE                                                 *
 *                                                                         *
 ***************************************************************************/
PRIVATE VOID
Y1564MsgProcessTestResult (tY1564QMsg * pMsg)
{
    tUtlSysPreciseTime SysPreciseTime;
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo  SlaEntry;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    UtlGetPreciseSysTime (&SysPreciseTime);

    SlaEntry.u4ContextId = pMsg->Y1564ReqParam.u4ContextId;
    SlaEntry.u4SlaId = pMsg->Y1564ReqParam.u4SlaId;

    pSlaEntry =  Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

    if (pSlaEntry != NULL)
    {
        pSlaEntry->u4TransId = pMsg->Y1564ReqParam.u4TransId;

        if (pSlaEntry->u1SlaCurrentTestMode != Y1564_PERF_TEST)
        {
            pSlaEntry->TestEndTimeStamp =  SysPreciseTime.u4Sec;

            if (Y1564CoreProcessConfTestResult (pSlaEntry, pMsg) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
                                   " %s : Failed to process test results "
                                   "received for Configuration "
                                   "test with SLA Id %d \r\n",__FUNCTION__,
                                   pSlaEntry->u4SlaId);
                return;
            }
        }
     
      else if (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST)
      {
          if (Y1564CoreProcessPerfTestResult (pSlaEntry, pMsg) != OSIX_SUCCESS)
          {
              Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
                                 " %s : Failed to process test results "
                                 "received for Performance "
                                 "test with SLA Id %d \r\n",__FUNCTION__,
                                 pSlaEntry->u4SlaId);
              return;
          }
      }
  }
  else
  {
      Y1564_GLOBAL_TRC ("Y1564MsgProcessTestResult : SLA information is not"
                        " present \r\n");
  }
  return;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564MsgHandleCfmSignalMsg                           *
 *                                                                         *
 * DESCRIPTION      : Function is used to handle the CFM signal status for *
 *                    a MEP.                                               *
 *                                                                         *
 * INPUT            : pMsg - Pointer to the message to be posted.          *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : NONE                                                 *
 *                                                                         *
 ***************************************************************************/
PRIVATE VOID
Y1564MsgHandleCfmSignalMsg (tY1564QMsg * pMsg)
{
  tSlaInfo       *pSlaEntry = NULL;
  tPerfTestInfo  *pPerfTestEntry = NULL;
  tSlaInfo       SlaEntry;
  UINT4          u4IfIndex = 0;
  BOOL1          bResult = Y1564_ZERO;

  SlaEntry.u4ContextId = pMsg->Y1564ReqParam.u4ContextId;
  SlaEntry.u4SlaId = pMsg->Y1564ReqParam.u4SlaId;

  pSlaEntry =  Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

  switch (pMsg->Y1564ReqParam.u4Status)
  {
      case Y1564_CFM_SIGNAL_FAIL:

          if ((pSlaEntry != NULL) &&
              (pSlaEntry->u4SlaMepId == (UINT2) pMsg->Y1564ReqParam.u2MepId) &&
              (pSlaEntry->u4SlaMegId == pMsg->Y1564ReqParam.u4MdIndex) &&    
              (pSlaEntry->u4SlaMeId == pMsg->Y1564ReqParam.u4MaIndex))
          {
              if (pSlaEntry->u1SlaCurrentTestMode != Y1564_PERF_TEST)
              {
                  if (Y1564CoreStopConfTest (pSlaEntry) != OSIX_SUCCESS)
                  {
                      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC, " %s : "
                                         "Configuration test stop failed for SLA Id %d \r\n",
                                         __FUNCTION__,pSlaEntry->u4SlaId);
                      return;
                  }
              }
              if (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST)
              {
                  pPerfTestEntry = Y1564UtilGetFirstNodeFromPerfTestTable();

                  if (pPerfTestEntry == NULL)
                  {
                      Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
                                         " %s : Perf TestEntry is NULL %d \r\n",
                                         __FUNCTION__,pSlaEntry->u4SlaId);
                          return;
                  }
                  while (pPerfTestEntry != NULL)
                  {
                      for (; u4IfIndex <= pPerfTestEntry->u2SlaListCount ; u4IfIndex++)
                      {
                          /* Scanning the SLA list bitmap to identify the SLA id's mapped to
                           * Perf Table*/
                          OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)), u4IfIndex,
                                                   sizeof (tY1564SlaList), bResult);

                          /* If the bit is not set or bit is set but SLA Id present in "
                           * SlaEntry is not matching with SLA Id present in perf entry */
                          if ((bResult == OSIX_FALSE)|| (u4IfIndex != pSlaEntry->u4SlaId))
                          {
                              continue;
                          }
                          else
                          {
                              if (Y1564CoreStopPerfTest (pPerfTestEntry) != OSIX_SUCCESS)
                              {
                                  Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC, " %s : "
                                                     "Performance test stop failed for SLA Id %d \r\n",
                                                     __FUNCTION__,pSlaEntry->u4SlaId);
                                  return;
                              }
                              return;
                          }

                      }
                      pPerfTestEntry = 
                          Y1564UtilGetNextNodeFromPerfTestTable (pPerfTestEntry);
                  }
              }
          }
          else
	  {
		  Y1564_GLOBAL_TRC ("Y1564MsgHandleCfmSignalMsg : SLA is not "
				  "present for the given CFM Entries \r\n");
	  }
      default:
	  break;
  }
  return;
}
#endif /*_Y1564MSG_C */
