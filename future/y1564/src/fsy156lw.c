/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsy156lw.c,v 1.30 2016/06/02 12:49:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "y1564inc.h"

PRIVATE tSlaInfo *
Y1564LwUtilValidateCxtAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                              UINT4 *pu4ErrorCode);

PRIVATE tSacInfo * 
Y1564LwUtilValidateCxtAndSac (UINT4 u4ContextId, UINT4 u4RingId,
                              UINT4 *pu4ErrorCode);

PRIVATE tPerfTestInfo * 
Y1564LwUtilValidateCxtAndPerfTest (UINT4 u4ContextId, UINT4 u4PerfId,
                              UINT4 *pu4ErrorCode);

PRIVATE tTrafficProfileInfo *
Y1564LwUtilValidateCxtAndTrafProf (UINT4 u4ContextId, UINT4 u4TrafProfId,
                                   UINT4 *pu4ErrorCode);

PRIVATE VOID
Y1564LwUtilSlaRowStatusNotInServ (tSlaInfo * pSlaEntry);

PRIVATE VOID
Y1564LwUtilSacRowStatusNotInServ (tSacInfo * pSacEntry);

PRIVATE VOID
Y1564LwUtilTrafProfRowStatusNotInServ (tTrafficProfileInfo * pTrafProfEntry);

#ifndef MEF_WANTED
PRIVATE VOID
Y1564LwUtilServiceConfInfoRowStatusNotInServ (tServiceConfInfo * pServiceConfEntry);

PRIVATE INT4
Y1564LwUtilValidateServConfAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                                   UINT4 u4ServConfId);

PRIVATE tServiceConfInfo *
Y1564LwUtilValidateCxtAndServConf (UINT4 u4ContextId, UINT4 u4ServiceConfId,
                                   UINT4 *pu4ErrorCode);

#endif

PRIVATE VOID
Y1564LwUtilPerfTestRowStatusNotInServ (tPerfTestInfo * pPerfTestEntry);


PRIVATE tY1564ContextInfo * 
Y1564LwUtilValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode);

PRIVATE INT4
Y1564LwUtilValidateSlaList (tPerfTestInfo *pPerfTestEntry, UINT4 *pu4ErrorCode);
/* LOW LEVEL Routines for Table : FsY1564ContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564ContextTable
 Input       :  The Indices
                FsY1564ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564ContextTable (UINT4 u4FsY1564ContextId)
{
    if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_ALL_FAILURE_TRC, " %s: does not exist\r\n",
                           __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564ContextTable
 Input       :  The Indices
                FsY1564ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsY1564ContextTable(UINT4 *pu4FsY1564ContextId)
{

    UINT4               u4ContextId = Y1564_ZERO;

    *pu4FsY1564ContextId = Y1564_ZERO;

    for (; u4ContextId < Y1564_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (Y1564CxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4FsY1564ContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    Y1564_GLOBAL_TRC ("nmhGetFirstIndexFsY1564ContextTable : "
                      "Context does not Present\r\n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564ContextTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564ContextTable (UINT4 u4FsY1564ContextId,
                                         UINT4 *pu4NextFsY1564ContextId)
{
    UINT4               u4ContextId = u4FsY1564ContextId + 1;

    *pu4NextFsY1564ContextId = Y1564_ZERO;

    for (; u4ContextId < Y1564_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (Y1564CxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4NextFsY1564ContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsY1564ContextName
Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextName (UINT4 u4FsY1564ContextId, 
                               tSNMP_OCTET_STRING_TYPE * pRetValFsY1564ContextName)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the context name */
    pRetValFsY1564ContextName->i4_Length =
        (INT4) STRLEN (pContextInfo->au1ContextName);

    MEMCPY (pRetValFsY1564ContextName->pu1_OctetList,
            pContextInfo->au1ContextName, pRetValFsY1564ContextName->i4_Length);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextSystemControl
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextSystemControl (UINT4 u4FsY1564ContextId , 
                                        INT4 *pi4RetValFsY1564ContextSystemControl)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the system control status */
    *pi4RetValFsY1564ContextSystemControl = 
        (INT4) gau1Y1564SystemStatus[u4FsY1564ContextId]; 
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextModuleStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextModuleStatus (UINT4 u4FsY1564ContextId , 
                                       INT4 *pi4RetValFsY1564ContextModuleStatus)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the module status */
    *pi4RetValFsY1564ContextModuleStatus = (INT4) pContextInfo->u1ModuleStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextTraceOption
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextTraceOption (UINT4 u4FsY1564ContextId, 
                                      UINT4 *pu4RetValFsY1564ContextTraceOption)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* Returns the current Trace option */
    *pu4RetValFsY1564ContextTraceOption = Y1564_MODULE_TRACE (u4FsY1564ContextId);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextTrapStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextTrapStatus (UINT4 u4FsY1564ContextId, 
                                     INT4 *pi4RetValFsY1564ContextTrapStatus)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the Trace Status */
    *pi4RetValFsY1564ContextTrapStatus = (INT4) pContextInfo->u1TrapStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextNumOfConfTestRunning
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextNumOfConfTestRunning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextNumOfConfTestRunning (UINT4 u4FsY1564ContextId, 
                                               UINT4 *pu4RetValFsY1564ContextNumOfConfTestRunning)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the number of configuration tests are currently running 
     * in the virtual context */
    *pu4RetValFsY1564ContextNumOfConfTestRunning =  pContextInfo->u1ConfTestCount;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ContextNumOfPerfTestRunning
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                retValFsY1564ContextNumOfPerfTestRunning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ContextNumOfPerfTestRunning (UINT4 u4FsY1564ContextId, 
                                               UINT4 *pu4RetValFsY1564ContextNumOfPerfTestRunning)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* returns the number of performance tests are currently running 
     * in the virtual context */
    *pu4RetValFsY1564ContextNumOfPerfTestRunning =  pContextInfo->u1PerfTestCount;
    return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564ContextSystemControl
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                setValFsY1564ContextSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564ContextSystemControl (UINT4 u4FsY1564ContextId, 
                                        INT4 i4SetValFsY1564ContextSystemControl)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    if (gau1Y1564SystemStatus[u4FsY1564ContextId] == 
        (UINT1) i4SetValFsY1564ContextSystemControl) 
    {
        /* If the system control Status present in the context is
         * same as new system control Status then return success */
        return SNMP_SUCCESS;
    }
    if (i4SetValFsY1564ContextSystemControl == Y1564_SHUTDOWN)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_INIT_SHUT_TRC,
                           " %s : Y1564 Module is shutdown\r\n", __FUNCTION__);
        /* shutdown the Y1564 */  
        Y1564CxtModuleShutDown (pContextInfo->u4ContextId);
    }
    gau1Y1564SystemStatus[u4FsY1564ContextId] = (UINT1) i4SetValFsY1564ContextSystemControl;
    gY1564GlobalInfo.au1SystemCtrl[u4FsY1564ContextId] = (UINT1) i4SetValFsY1564ContextSystemControl;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564ContextModuleStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                setValFsY1564ContextModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564ContextModuleStatus (UINT4 u4FsY1564ContextId, 
                                       INT4 i4SetValFsY1564ContextModuleStatus)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    if (pContextInfo->u1ModuleStatus == (UINT1)i4SetValFsY1564ContextModuleStatus) 
    {
        /* If the module status present in the context is
         * same as new module status then return success */
        return SNMP_SUCCESS;
    }
    if (i4SetValFsY1564ContextModuleStatus == Y1564_ENABLED)
    {
       /* enable Y1564 in the context*/
       if (Y1564CxtModuleEnable (pContextInfo) != OSIX_SUCCESS)
       {
           Y1564_CONTEXT_TRC (u4FsY1564ContextId,Y1564_INIT_SHUT_TRC 
                              | Y1564_ALL_FAILURE_TRC, " %s : Failed to enable "
                              "Y1564 Module \r\n",__FUNCTION__);
           return SNMP_FAILURE;
       }
       Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_INIT_SHUT_TRC, " %s : "
                          "Y1564 Module is enabled \r\n", __FUNCTION__);
    }
    if (i4SetValFsY1564ContextModuleStatus == Y1564_DISABLED)
    {  
       /* Disable the Y1564 */  
       Y1564CxtModuleDisable (pContextInfo);

       Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_INIT_SHUT_TRC, " %s : "
                          "Y1564 Module is disabled \r\n", __FUNCTION__);
    }
    pContextInfo->u1ModuleStatus = (UINT1)i4SetValFsY1564ContextModuleStatus;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsY1564ContextTraceOption
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                setValFsY1564ContextTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564ContextTraceOption (UINT4 u4FsY1564ContextId, 
                                      UINT4 u4SetValFsY1564ContextTraceOption)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    /* Set the trace option,
     *   1 bit - Init and Shutdown Traces
     *   2 bit - Management Traces
     *   3 bit - Control Plane Traces
     *   4 bit - Resources related Traces such as memory, data structure etc
     *   5 bit - Timer related Traces
     *   6 bit - Y1731 interface related traces
     *   7 bit - Configuration/Performance test start/stop/calculation traces
     *   8 bit - Configuration/Performance test session record traces
     *   9 bit - Critical Traces
     *  Multiple options can be selected by setting multiple bits */

   pContextInfo->u4TraceOption = u4SetValFsY1564ContextTraceOption; 
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsY1564ContextTrapStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                setValFsY1564ContextTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564ContextTrapStatus (UINT4 u4FsY1564ContextId, 
                                     INT4 i4SetValFsY1564ContextTrapStatus)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : does not exist\r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    pContextInfo->u1TrapStatus = (UINT1) i4SetValFsY1564ContextTrapStatus;

    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564ContextSystemControl
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                testValFsY1564ContextSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564ContextSystemControl (UINT4 *pu4ErrorCode, 
                                           UINT4 u4FsY1564ContextId, 
                                           INT4 i4TestValFsY1564ContextSystemControl)
{ 
    tY1564ContextInfo   *pContextInfo = NULL;
    UINT4               u4BridgeMode = Y1564_ZERO;

    /* Check added to validate the maximum allowed Contexts */
    if (u4FsY1564ContextId >= Y1564_SIZING_CONTEXT_COUNT)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : "
                           "Invalid Context Identifier \r\n",__FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* check bridge mode is set for the given context */
    if (i4TestValFsY1564ContextSystemControl == Y1564_START)
    {
        if (Y1564L2IwfGetBridgeMode
            (u4FsY1564ContextId, &u4BridgeMode) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : "
                               "Unable to get bridge mode \r\n", __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (u4BridgeMode == Y1564_INVALID_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_Y1564_ERR_BRG_MODE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    /* Check whether the System control value is in allowed limit */
    if ((i4TestValFsY1564ContextSystemControl != Y1564_START) &&
        (i4TestValFsY1564ContextSystemControl != Y1564_SHUTDOWN))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : "
                          "Invalid System Control \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* check whether context is present to configure Y1564 system control*/
    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564ContextModuleStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                testValFsY1564ContextModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564ContextModuleStatus (UINT4 *pu4ErrorCode, 
                                           UINT4 u4FsY1564ContextId, 
                                           INT4 i4TestValFsY1564ContextModuleStatus)
{

    /* Check the system status. Module status configuration will be allowed
     * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Check whether the Module status values are in allowed range*/
    if ((i4TestValFsY1564ContextModuleStatus != Y1564_ENABLED) &&
        (i4TestValFsY1564ContextModuleStatus != Y1564_DISABLED))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : "
                           "Invalid Module Status Value \r\n",__FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564ContextTraceOption
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                testValFsY1564ContextTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564ContextTraceOption (UINT4 *pu4ErrorCode, 
                                         UINT4 u4FsY1564ContextId, 
                                         UINT4 u4TestValFsY1564ContextTraceOption)
{   
   /* Check the system status. Trace configuration will be allowed
    * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC, " %s: "
                           "System Status shutdown \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Check whether the trace values are in allowed limit */
    if ((u4TestValFsY1564ContextTraceOption < Y1564_MIN_TRC_VAL) ||
        (u4TestValFsY1564ContextTraceOption > Y1564_ALL_TRC)) 
    {
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_TRACE_OPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564ContextTrapStatus
 Input       :  The Indices
                FsY1564ContextId

                The Object 
                testValFsY1564ContextTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564ContextTrapStatus (UINT4 *pu4ErrorCode, 
                                        UINT4 u4FsY1564ContextId, 
                                        INT4 i4TestValFsY1564ContextTrapStatus)
{
   /* Check the system status. Trap configuration will be allowed
    * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check whether the trap values are in allowed limit */
    if ((i4TestValFsY1564ContextTrapStatus != Y1564_SNMP_TRUE) &&
        (i4TestValFsY1564ContextTrapStatus != Y1564_SNMP_FALSE))
    {
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_TRAP_OPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564ContextTable
 Input       :  The Indices
                FsY1564ContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsY1564ContextTable (UINT4 *pu4ErrorCode, 
                                  tSnmpIndexList *pSnmpIndexList, 
                                  tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564SlaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564SlaTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564SlaTable (UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564SlaId)
{
    tSlaInfo *pSlaEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * SlaTable */

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry %d is "
                           "NULL \r\n", __FUNCTION__,u4FsY1564SlaId);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564SlaTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsY1564SlaTable (UINT4 *pu4FsY1564ContextId, 
                                      UINT4 *pu4FsY1564SlaId)
{
  tSlaInfo *pSlaEntry = NULL;

  pSlaEntry = Y1564UtilSlaGetFirstNodeFromSlaTable();

  if (pSlaEntry != NULL)
  {
      *pu4FsY1564ContextId = pSlaEntry->u4ContextId;
      *pu4FsY1564SlaId = pSlaEntry->u4SlaId;
      return SNMP_SUCCESS;
  }

  return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564SlaTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564SlaId
                nextFsY1564SlaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564SlaTable (UINT4 u4FsY1564ContextId,
                                     UINT4 *pu4NextFsY1564ContextId, 
                                     UINT4 u4FsY1564SlaId,
                                     UINT4 *pu4NextFsY1564SlaId )
{
    tSlaInfo      *pNextSlaEntry = NULL;
    tSlaInfo       SlaEntry;

    MEMSET (&SlaEntry, 0, sizeof (tSlaInfo));

    SlaEntry.u4ContextId = u4FsY1564ContextId;
    SlaEntry.u4SlaId = u4FsY1564SlaId;

    pNextSlaEntry = Y1564UtilSlaGetNextNodeFromSlaTable (&SlaEntry);

    if (pNextSlaEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextSlaEntry->u4ContextId;
        *pu4NextFsY1564SlaId = pNextSlaEntry->u4SlaId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsY1564SlaEvcIndex
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaEvcIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaEvcIndex (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564SlaId, 
                               INT4 *pi4RetValFsY1564SlaEvcIndex)
{

#ifdef MEF_WANTED
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d \r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaEvcIndex = (INT4) pSlaEntry->u2EvcId;
#else
    *pi4RetValFsY1564SlaEvcIndex = Y1564_ZERO;
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (*pi4RetValFsY1564SlaEvcIndex);;
#endif
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaMEG
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaMEG (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 *pu4RetValFsY1564SlaMEG)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaMEG  = pSlaEntry->u4SlaMegId;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaME
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaME (UINT4 u4FsY1564ContextId, 
                         UINT4 u4FsY1564SlaId, 
                         UINT4 *pu4RetValFsY1564SlaME)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaME  = pSlaEntry->u4SlaMeId;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaMEP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaMEP (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 *pu4RetValFsY1564SlaMEP)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    { 
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaMEP  = pSlaEntry->u4SlaMepId;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaSacId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaSacId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaSacId (UINT4 u4FsY1564ContextId, 
                            UINT4 u4FsY1564SlaId, 
                            UINT4 *pu4RetValFsY1564SlaSacId)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaSacId  = pSlaEntry->u4SlaSacId;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaTrafProfileId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaTrafProfileId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaTrafProfileId (UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SlaId, 
                                    UINT4 *pu4RetValFsY1564SlaTrafProfileId)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaTrafProfileId  = pSlaEntry->u4SlaTrafProfId;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaStepLoadRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaStepLoadRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaStepLoadRate (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 *pi4RetValFsY1564SlaStepLoadRate)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    /* Step Load Rate specifies the number of steps to be 
     * followed to complete the step load CIR.
     
     * This Object configures only the rate to be
     * followed as part of StepLoadCIR and based on this
     * rate steps are calculated internally and stored in 
     * u2SlaRateStep.
      
     * For Example, If the rate is configured as 20, then the total 
     * step to complete the StepLoadCIR will be 5.
     * So converting rate step into rate.
   
     * Y1564_MAX_RATE_STEP = 100 and u2SlaRateStep is 5 means 
     * then the configured rate will be 20 */

    *pi4RetValFsY1564SlaStepLoadRate  = (INT4) (Y1564_MAX_RATE_STEP /
                                            pSlaEntry->u2SlaRateStep);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaConfTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaConfTestDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaConfTestDuration (UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       INT4 *pi4RetValFsY1564SlaConfTestDuration)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaConfTestDuration = (INT4) pSlaEntry->u4SlaConfTestDuration;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaTestStatus (UINT4 u4FsY1564ContextId, 
                                 UINT4 u4FsY1564SlaId, 
                                 INT4 *pi4RetValFsY1564SlaTestStatus)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaTestStatus = (INT4) pSlaEntry->u1SlaConfTestStatus;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaServiceConfId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaServiceConfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaServiceConfId (UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SlaId, 
                                    UINT4 *pu4RetValFsY1564SlaServiceConfId)
{
#ifndef MEF_WANTED
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaServiceConfId = (UINT4) pSlaEntry->u2SlaServiceConfId;
#else
    *pu4RetValFsY1564SlaServiceConfId = Y1564_ZERO;
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (*pu4RetValFsY1564SlaServiceConfId);
#endif
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaColorMode
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaColorMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaColorMode (UINT4 u4FsY1564ContextId, 
                                UINT4 u4FsY1564SlaId, 
                                INT4 *pi4RetValFsY1564SlaColorMode)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaColorMode = (INT4) pSlaEntry->u1ColorMode;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaCoupFlag
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaCoupFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaCoupFlag (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564SlaId, 
                               INT4 *pi4RetValFsY1564SlaCoupFlag)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaCoupFlag = (INT4) pSlaEntry->u1CoupFlag;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaCIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaCIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaCIR (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 *pu4RetValFsY1564SlaCIR)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaCIR = pSlaEntry->u4CIR;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaCBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaCBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaCBS (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 *pu4RetValFsY1564SlaCBS)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaCBS = pSlaEntry->u4CBS;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaEIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaEIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaEIR (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 *pu4RetValFsY1564SlaEIR)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaEIR = pSlaEntry->u4EIR;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaEBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaEBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaEBS (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId,
                          UINT4 *pu4RetValFsY1564SlaEBS)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564SlaEBS = pSlaEntry->u4EBS;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaTrafPolicing
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaTrafPolicing
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaTrafPolicing (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 *pi4RetValFsY1564SlaTrafPolicing)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaTrafPolicing = (INT4) pSlaEntry->u1SlaTrafPolicing;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaTestSelector
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaTestSelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaTestSelector (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 *pi4RetValFsY1564SlaTestSelector)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFsY1564SlaTestSelector = (INT4) pSlaEntry->u1SlaConfTestSelector;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaCurrentTestMode
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaCurrentTestMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaCurrentTestMode (UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SlaId, 
                                      INT4 *pi4RetValFsY1564SlaCurrentTestMode)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaCurrentTestMode = (INT4) 
                                           pSlaEntry->u1SlaCurrentTestMode;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaCurrentTestState
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaCurrentTestState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaCurrentTestState (UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       INT4 *pi4RetValFsY1564SlaCurrentTestState)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaCurrentTestState = (INT4) 
        pSlaEntry->u1SlaCurrentTestState;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SlaRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                retValFsY1564SlaRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SlaRowStatus (UINT4 u4FsY1564ContextId, 
                                UINT4 u4FsY1564SlaId, 
                                INT4 *pi4RetValFsY1564SlaRowStatus)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SlaRowStatus = (INT4) pSlaEntry->u1RowStatus;

    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564SlaEvcIndex
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaEvcIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaEvcIndex (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564SlaId, 
                               INT4 i4SetValFsY1564SlaEvcIndex)
{
#ifdef MEF_WANTED
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for Id %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the EVC Id to the SLA */
    pSlaEntry->u2EvcId = (UINT2) i4SetValFsY1564SlaEvcIndex;

    if (i4SetValFsY1564SlaEvcIndex != Y1564_ZERO)
    {
        if ((Y1564UtilGetBandwidthProfile (pSlaEntry, u4FsY1564ContextId, 
                                           i4SetValFsY1564SlaEvcIndex)) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : Unable to fetch "
                               "bandwidth profile parameters for EVC %d \r\n",
                               __FUNCTION__, i4SetValFsY1564SlaEvcIndex);
            return SNMP_FAILURE;
        }
    }
    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */

    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);
#else
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (i4SetValFsY1564SlaEvcIndex);
#endif
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaMEG
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaMEG (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 u4SetValFsY1564SlaMEG)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the MEG to the SLA */
    pSlaEntry->u4SlaMegId = u4SetValFsY1564SlaMEG;
 
    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */

    if (u4SetValFsY1564SlaMEG != Y1564_ZERO)
    {
        if (Y1564UtilValidateSlaCFMParams (pSlaEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC," %s : CFM Entry "
                               " MEG Id %d validation failed for SLA %d \r\n",
                               __FUNCTION__,u4SetValFsY1564SlaMEG, u4FsY1564SlaId);
            return SNMP_FAILURE;
        }
    }

    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaME
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaME (UINT4 u4FsY1564ContextId, 
                         UINT4 u4FsY1564SlaId, 
                         UINT4 u4SetValFsY1564SlaME)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the ME to the SLA */
    pSlaEntry->u4SlaMeId = u4SetValFsY1564SlaME;

    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */

    if (u4SetValFsY1564SlaME != Y1564_ZERO)
    {
        if (Y1564UtilValidateSlaCFMParams (pSlaEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC," %s : CFM Entry "
                               "ME Id %d validation failed for SLA %d\r\n", 
                               __FUNCTION__,u4SetValFsY1564SlaME,u4FsY1564SlaId);
            return SNMP_FAILURE;
        }
    }

    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaMEP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaMEP (UINT4 u4FsY1564ContextId, 
                          UINT4 u4FsY1564SlaId, 
                          UINT4 u4SetValFsY1564SlaMEP)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__,u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the ME to the SLA */
    pSlaEntry->u4SlaMepId = u4SetValFsY1564SlaMEP;

    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */

    if (u4SetValFsY1564SlaMEP != Y1564_ZERO)
    {
        if (Y1564UtilValidateSlaCFMParams (pSlaEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC," %s : CFM Entry "
                               "MEP Id %d validation failed for SLA %d\r\n",
                               __FUNCTION__,u4SetValFsY1564SlaMEP,u4FsY1564SlaId);
            return SNMP_FAILURE;
        }
    }

    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaSacId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaSacId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaSacId (UINT4 u4FsY1564ContextId, 
                            UINT4 u4FsY1564SlaId, 
                            UINT4 u4SetValFsY1564SlaSacId)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__,u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the SAC Id to the SLA */
    pSlaEntry->u4SlaSacId = u4SetValFsY1564SlaSacId;

    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */
    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaTrafProfileId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaTrafProfileId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaTrafProfileId (UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SlaId, 
                                    UINT4 u4SetValFsY1564SlaTrafProfileId)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Traffic Profile Id to the SLA */
    pSlaEntry->u4SlaTrafProfId= u4SetValFsY1564SlaTrafProfileId;

    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */     
    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaStepLoadRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaStepLoadRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaStepLoadRate (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 i4SetValFsY1564SlaStepLoadRate)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : SLA Entry not present "
                           "for %d\r\n",__FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Step Load Rate to the SLA */
    pSlaEntry->u2SlaRateStep = (UINT2) (Y1564_MAX_RATE_STEP /
                                        i4SetValFsY1564SlaStepLoadRate);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaConfTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaConfTestDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaConfTestDuration (UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       INT4 i4SetValFsY1564SlaConfTestDuration)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__,u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Configuration Test Duration to the SLA */
    pSlaEntry->u4SlaConfTestDuration = (UINT4) i4SetValFsY1564SlaConfTestDuration;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaTestStatus (UINT4 u4FsY1564ContextId, 
                                 UINT4 u4FsY1564SlaId,
                                 INT4 i4SetValFsY1564SlaTestStatus)
{
    tSlaInfo            *pSlaEntry = NULL;
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Test Selector values are assigned to LastSlaSelector variable
     * to perform the configuration test */
    pSlaEntry->u1LastSlaConfTestSelector = pSlaEntry->u1SlaConfTestSelector;

    if (i4SetValFsY1564SlaTestStatus == Y1564_SLA_START) 
    {
        if (pContextInfo->u1ModuleStatus == Y1564_ENABLED)
        {           
            if (Y1564CoreStartConfTest (pSlaEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Failed to "
                                   "start the test for Sla Id %d\r\n", __FUNCTION__, 
                                   u4FsY1564SlaId);
                return SNMP_FAILURE;
            }
            pContextInfo->u1ConfTestCount ++;
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_CTRL_TRC, 
                               " %s : Currently running Configuration test "
                               "count for Sla Id %d : %d \r\n", __FUNCTION__, 
                               u4FsY1564SlaId, pContextInfo->u1ConfTestCount);
        }
        else
        {
            /* Test will not be started when Y1564 module is in disabled "
           * state */
            CLI_SET_ERR (CLI_Y1564_ERR_MODULE_NOT_ENABLED);
            return SNMP_FAILURE;
        }
    }
    if (i4SetValFsY1564SlaTestStatus == Y1564_SLA_STOP)
    {
        if (Y1564CoreStopConfTest (pSlaEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : Failed to stop the "
                               "test for Sla Id %d\r\n", __FUNCTION__, 
                               u4FsY1564SlaId);
            CLI_SET_ERR (CLI_Y1564_ERR_TEST_STOP_FAIL);
            return SNMP_FAILURE;
        }
        /* Set the Test Status to the SLA */
        pSlaEntry->u1SlaConfTestStatus = (UINT1) i4SetValFsY1564SlaTestStatus;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaServiceConfId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaServiceConfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaServiceConfId (UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SlaId, 
                                    UINT4 u4SetValFsY1564SlaServiceConfId)
{
#ifndef MEF_WANTED
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Service Configuration Id to the SLA */
    pSlaEntry->u2SlaServiceConfId = (UINT2) u4SetValFsY1564SlaServiceConfId;

    /* To make the Sla Row Status as Not-In-Service
     * all the mandatory parameters should be
     * configured */
    Y1564LwUtilSlaRowStatusNotInServ (pSlaEntry);

    if (u4SetValFsY1564SlaServiceConfId == Y1564_ZERO)
    {
        /* Below function used to delete the Configuration and Performance 
         * report entries related to given SLA Id, when unmap the 
         * service Id from the SLA */

        Y1564UtilDelConfTestResultEntry (u4FsY1564ContextId, u4FsY1564SlaId);
    }

#else
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (u4SetValFsY1564SlaServiceConfId);

#endif

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaTrafPolicing
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaTrafPolicing
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaTrafPolicing (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 i4SetValFsY1564SlaTrafPolicing)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Traffic Policing status to the SLA */
    pSlaEntry->u1SlaTrafPolicing = (UINT1) i4SetValFsY1564SlaTrafPolicing;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaTestSelector
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaTestSelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaTestSelector (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 i4SetValFsY1564SlaTestSelector)
{
    tSlaInfo          *pSlaEntry = NULL;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry not present "
                           "for %d\r\n", __FUNCTION__, u4FsY1564SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Service configuration tests and their corresponding bit postions:

       0 - Simple CIR Validation Test

       1 - Step Load CIR Test

       2 - EIR Configuration Test, Colour-Aware/Non-Colour-Aware based on the
       value set for the object fsY1564SlaColormode or
       fsY1564ServiceConfColorMode.

       3 - Traffic Policing Test, Colour-Aware/Non-Colour-Aware based on the
       value present in the object fsY1564SlaColormode or
       fsY1564ServiceConfColorMode. */

    /* Set the Test selector to the SLA */
    pSlaEntry->u1SlaConfTestSelector = (UINT1) i4SetValFsY1564SlaTestSelector;
 
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SlaRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                setValFsY1564SlaRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SlaRowStatus (UINT4 u4FsY1564ContextId, 
                                UINT4 u4FsY1564SlaId, 
                                INT4 i4SetValFsY1564SlaRowStatus)
{
    tSlaInfo  *pSlaEntry = NULL;
    tSlaInfo   SlaEntry;

    if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           "%s : does not exist \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
   
    switch (i4SetValFsY1564SlaRowStatus)
    {
        case CREATE_AND_WAIT:

            pSlaEntry = Y1564UtilSlaCreateSlaNode();

            if (pSlaEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC," %s : Failed to create "
                                   "Sla Entry for ID %d\r\n", __FUNCTION__, 
                                   u4FsY1564SlaId);
                return SNMP_FAILURE;
            }
            /* Update SLA and Context Id to Sla 
             * Structure */
            pSlaEntry->u4SlaId = u4FsY1564SlaId;
            pSlaEntry->u4ContextId = u4FsY1564ContextId;
            
            if (Y1564UtilSlaAddNodeToSlaRBTree (pSlaEntry) != OSIX_SUCCESS)
            {
                /* Release memory allocated for SLA Entry */
                MemReleaseMemBlock (Y1564_SLA_POOL (), (UINT1 *) pSlaEntry);
                pSlaEntry = NULL;
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : SLA Entry Add "
                                   "Failed for SlaID %d\r\n", __FUNCTION__, 
                                   u4FsY1564SlaId);
                return SNMP_FAILURE;
            }
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_CTRL_TRC, " %s : "
                               "SLA Entry Added to RBTree with SlaId %d\r\n",
                               __FUNCTION__, pSlaEntry->u4SlaId);

            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set */
            pSlaEntry->u1RowStatus = NOT_READY;
            break;
         
        case NOT_IN_SERVICE:
            SlaEntry.u4SlaId = u4FsY1564SlaId;
            SlaEntry.u4ContextId = u4FsY1564ContextId;

            pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

            if (pSlaEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified Sla Id "
                                   "is not presen \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pSlaEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            SlaEntry.u4SlaId = u4FsY1564SlaId;
            SlaEntry.u4ContextId = u4FsY1564ContextId;

            pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

            if (pSlaEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified Sla Id "
                                   "is not present \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            
            if (Y1564UtilSlaDelNodeFromRBTree (pSlaEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC," %s : SLA Entry delete "
                                   "failed for Sla Id  %d\r\n", __FUNCTION__,
                                   pSlaEntry->u4SlaId);
                return SNMP_FAILURE;
            }
            break;

         case ACTIVE:
            
            SlaEntry.u4SlaId = u4FsY1564SlaId;
            SlaEntry.u4ContextId = u4FsY1564ContextId;

            pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

            if (pSlaEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC," %s : Specified Sla Id "
                                   "is not present  \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
           
            if (pSlaEntry->u1RowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pSlaEntry->u1RowStatus = ACTIVE;
            break;

         default:
           return SNMP_FAILURE;
    }

   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaEvcIndex
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaEvcIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaEvcIndex (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsY1564ContextId, 
                                  UINT4 u4FsY1564SlaId, 
                                  INT4  i4TestValFsY1564SlaEvcIndex)
{
#ifdef MEF_WANTED
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo *pNextSlaEntry = NULL;

    if (i4TestValFsY1564SlaEvcIndex != Y1564_ZERO)
    {
        if ((i4TestValFsY1564SlaEvcIndex < Y1564_MIN_EVC_INDEX) ||
            (i4TestValFsY1564SlaEvcIndex > Y1564_MAX_EVC_INDEX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_Y1564_ERR_INVALID_EVC);
            return SNMP_FAILURE;
        }   
    }

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (i4TestValFsY1564SlaEvcIndex != Y1564_ZERO)
    {
        if ((pSlaEntry->u2EvcId != Y1564_ZERO) &&
            (pSlaEntry->u2EvcId != i4TestValFsY1564SlaEvcIndex))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC,  " %s : "
                               "EVC Id cannot be changed "
                               "without de-associating the existing EVC"
                               "Id from SLA  \r\n",
                               __FUNCTION__, i4TestValFsY1564SlaEvcIndex);
            CLI_SET_ERR (CLI_Y1564_DEASSOCIATE_EVC_ENTRY);
            return SNMP_FAILURE;
        }

        if ((Y1564UtilGetEvcEntry (u4FsY1564ContextId, i4TestValFsY1564SlaEvcIndex))
            != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : "
                               "Given EVC Index %d is not present\r\n",__FUNCTION__,
                               i4TestValFsY1564SlaEvcIndex);
            CLI_SET_ERR (CLI_Y1564_ERR_SLA_EVC_NOT_EXISTS);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsY1564SlaEvcIndex == Y1564_ZERO) &&
        (pSlaEntry->u2EvcId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Evc "
                           "Id is not present to de-associate it "
                           "from SLA \r\n", __FUNCTION__,
                           i4TestValFsY1564SlaEvcIndex);
        CLI_SET_ERR (CLI_Y1564_ERR_EVC_NOT_MAPPED);
        return SNMP_FAILURE;
    }

    /* EVC Id cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : EVC Id %d cannot be "
                           "de-associated from SLA "
                           "when SLA test is in-progress \r\n",
                           __FUNCTION__,i4TestValFsY1564SlaEvcIndex);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }
   
    pSlaEntry = (tSlaInfo *) Y1564UtilSlaGetFirstNodeFromSlaTable ();

    while (pSlaEntry != NULL)
    {
        /* check is added to validate EVC entry in same context
         * with different SLA Id */

        if ((pSlaEntry->u4ContextId == u4FsY1564ContextId) &&
            (pSlaEntry->u4SlaId != u4FsY1564SlaId))
        {
            if ((pSlaEntry->u2EvcId == i4TestValFsY1564SlaEvcIndex) &&
                (pSlaEntry->u2EvcId != Y1564_ZERO))
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, "%s : EVC Id %d already "
                                   "mapped with SLA %d \r\n", __FUNCTION__,
                                   i4TestValFsY1564SlaEvcIndex, pSlaEntry->u4SlaId);
                CLI_SET_ERR (CLI_Y1564_ERR_EVC_EXIST);
                return SNMP_FAILURE;
            }
        }
        pNextSlaEntry = Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry); 
        pSlaEntry = pNextSlaEntry;
    }
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (i4TestValFsY1564SlaEvcIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaMEG
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaMEG (UINT4 *pu4ErrorCode, 
                             UINT4 u4FsY1564ContextId, 
                             UINT4 u4FsY1564SlaId,
                             UINT4 u4TestValFsY1564SlaMEG)
{
    tSlaInfo *pSlaEntry = NULL;


    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    /*
     * Test validation is not done for the below two reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false
     * Maximum Value  -> decimal constant is unsigned only in ISO C90
     * */
    if (u4TestValFsY1564SlaMEG != Y1564_ZERO)
    {
        if ((pSlaEntry->u4SlaMegId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaMegId != u4TestValFsY1564SlaMEG))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : "
                               "MEG Id cannot be changed "
                               "without de-associating the existing MEG"
                               "Id from SLA  \r\n",
                               __FUNCTION__,u4TestValFsY1564SlaMEG);
            CLI_SET_ERR (CLI_Y1564_DEASSOCIATE_CFM_ENTRY);
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaMEG == Y1564_ZERO) &&
        (pSlaEntry->u4SlaMegId == Y1564_ZERO)) 
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : MEG Id is not"
                               "present to de-associate it from SLA \r\n",
                               __FUNCTION__, u4TestValFsY1564SlaMEG);
        CLI_SET_ERR (CLI_Y1564_ERR_MEG_ME_MEP_NOT_MAPPED);
        return SNMP_FAILURE;
    }

    /* MEG cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s :MEG "
                           "Id %d cannot be de-associated from SLA when SLA "
                           "test is in-progress \r\n", __FUNCTION__,
                           u4TestValFsY1564SlaMEG);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaME
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaME (UINT4 *pu4ErrorCode,
                            UINT4 u4FsY1564ContextId,
                            UINT4 u4FsY1564SlaId,
                            UINT4 u4TestValFsY1564SlaME)
{
    tSlaInfo *pSlaEntry = NULL;

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    /*
     * Test validation is not done for the below two reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false
     * Maximum Value  -> decimal constant is unsigned only in ISO C90
     * */

    if (u4TestValFsY1564SlaME != Y1564_ZERO)
    {
        if ((pSlaEntry->u4SlaMeId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaMeId != u4TestValFsY1564SlaME))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : ME Id cannot be "
                               "changed without de-associating the existing ME"
                               "Id  from SLA  \r\n",__FUNCTION__,
                               u4TestValFsY1564SlaME);
            CLI_SET_ERR (CLI_Y1564_DEASSOCIATE_CFM_ENTRY);
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaME == Y1564_ZERO) &&
        (pSlaEntry->u4SlaMeId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : ME Id is not"
                               "present to de-associate it from SLA \r\n",
                               __FUNCTION__, u4TestValFsY1564SlaME);
        CLI_SET_ERR (CLI_Y1564_ERR_MEG_ME_MEP_NOT_MAPPED);
        return SNMP_FAILURE;
    }

    /* ME cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, "%s : ME Id "
                           "%d cannot be de-associated from SLA when SLA test "
                           "is in-progress \r\n", __FUNCTION__,
                           u4TestValFsY1564SlaME);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaMEP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaMEP (UINT4 *pu4ErrorCode,
                             UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564SlaId, 
                             UINT4 u4TestValFsY1564SlaMEP)
{
    tSlaInfo *pSlaEntry = NULL;

    /*
     * Test validation is not done for the below reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false
     */
    if (u4TestValFsY1564SlaMEP > Y1564_MAX_MEP_VALUE)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Invalid value for MEP "
                           "Index \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_MEP_INVALID_RANGE);
        return SNMP_FAILURE;
    }
   
    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (u4TestValFsY1564SlaMEP != Y1564_ZERO)
    {
        if ((pSlaEntry->u4SlaMepId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaMepId != u4TestValFsY1564SlaMEP))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : MEP Id cannot be "
                               "changed without de-associating the existing MEP"
                               "Id  from SLA  \r\n",
                               __FUNCTION__,u4TestValFsY1564SlaMEP);
            CLI_SET_ERR (CLI_Y1564_DEASSOCIATE_CFM_ENTRY);
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaMEP == Y1564_ZERO) &&
        (pSlaEntry->u4SlaMepId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : MEP Id is not"
                               "present to de-associate it from SLA \r\n",
                               __FUNCTION__, u4TestValFsY1564SlaMEP);
        CLI_SET_ERR (CLI_Y1564_ERR_MEG_ME_MEP_NOT_MAPPED);
        return SNMP_FAILURE;
    }
    /* MEP cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : MEP "
                           "Id %d cannot be de-associated from SLA when SLA "
                           "test is in-progress \r\n", __FUNCTION__,
                           u4TestValFsY1564SlaMEP);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaSacId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaSacId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaSacId (UINT4 *pu4ErrorCode, 
                               UINT4 u4FsY1564ContextId,
                               UINT4 u4FsY1564SlaId, 
                               UINT4 u4TestValFsY1564SlaSacId)
{
    tSlaInfo *pSlaEntry = NULL;
    tSacInfo *pSacEntry = NULL;

    /*
     * Test validation is not done for the below reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false
     */
    if (u4TestValFsY1564SlaSacId != Y1564_ZERO)
    {
        if (u4TestValFsY1564SlaSacId > Y1564_MAX_SAC_ID)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC," %s : Invalid value for SAC "
                               "Id Range should be between 1 to 10 \r\n",
                               __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_Y1564_ERR_INVALID_SAC_ID);
            return SNMP_FAILURE;
        } 

        pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                                  u4TestValFsY1564SlaSacId,
                                                  pu4ErrorCode);
        if (pSacEntry == NULL)
        {
            /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
             * of filling the error code, CLI set error and trace messages.
             * So simply return failure. */
            return SNMP_FAILURE;
        }
    }
    if ((pSacEntry != NULL) &&  (pSacEntry->u1RowStatus == NOT_READY))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : SAC "
                           "Id %d cannot be associated to SLA when "
                           "any of the IR/FLR/FTD/FDV/Availability information"
                           " is not set to SAC Entry \r\n", __FUNCTION__,
                           u4TestValFsY1564SlaSacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_READY);
        return SNMP_FAILURE;
    }

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (u4TestValFsY1564SlaSacId != Y1564_ZERO)
    {
        if ((pSlaEntry->u4SlaSacId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaSacId != u4TestValFsY1564SlaSacId))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : SAC Id cannot be "
                               "Modified/Deleted without de-associating the existing SAC"
                               "Id from SLA \r\n",__FUNCTION__);
            CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_SAC_ENTRY);
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaSacId == Y1564_ZERO) &&
        (pSlaEntry->u4SlaSacId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : SAC Id is not"
                           "present to de-associate it from SLA \r\n",
                           __FUNCTION__, u4TestValFsY1564SlaSacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_MAPPED);
        return SNMP_FAILURE;
    }
    /* SAC cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : SAC "
                           "Id %d cannot be de-associated from SLA when SLA "
                           "test is in-progress \r\n", __FUNCTION__,
                           u4TestValFsY1564SlaSacId);
       CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }

    /* Check whether all the mandatory parameters are mapped to SAC
     * table before associating it to SLA Table.
     * Configuration should not be allowed when mandatory parameters
     * are not configured in SAC*/
    if (nmhTestv2FsY1564SacRowStatus (pu4ErrorCode, 
                                      pSlaEntry->u4ContextId,
                                      u4TestValFsY1564SlaSacId, 
                                      ACTIVE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaTrafProfileId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaTrafProfileId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaTrafProfileId (UINT4 *pu4ErrorCode, 
                                       UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       UINT4 u4TestValFsY1564SlaTrafProfileId)
{
    tSlaInfo *pSlaEntry = NULL;
    tTrafficProfileInfo *pTrafProfEntry = NULL;

    /*
     * Test validation is not done for the below reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false
     */

    if (u4TestValFsY1564SlaTrafProfileId  != Y1564_ZERO)
    {
        if (u4TestValFsY1564SlaTrafProfileId > Y1564_MAX_TRAF_PROF_ID)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : Invalid value "
                               "for Traffic profile Id. Range should be "
                               "between 1 to 10 \r\n",
                               __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_Y1564_ERR_INVALID_TRAF_PROF_ID);
            return SNMP_FAILURE;
        } 

        pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                            u4TestValFsY1564SlaTrafProfileId,
                                                            pu4ErrorCode);
        if (pTrafProfEntry == NULL)
        {
            /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
             * of filling the error code, CLI set error and trace messages.
             * So simply return failure. */
            return SNMP_FAILURE;
        }
   }
    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (u4TestValFsY1564SlaTrafProfileId != Y1564_ZERO)
    {
        if ((pSlaEntry->u4SlaTrafProfId != Y1564_ZERO) &&
            (pSlaEntry->u4SlaTrafProfId != u4TestValFsY1564SlaTrafProfileId))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                               "Id cannot be changed without de-associating "
                               "the existing Traffic Profile Id from SLA \r\n",
                               __FUNCTION__,u4TestValFsY1564SlaTrafProfileId);
            CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_TRAF_PROF_ENTRY);
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaTrafProfileId == Y1564_ZERO) &&
        (pSlaEntry->u4SlaTrafProfId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : TrafProf "
                               "Id is not present to de-associate it "
                               "from SLA \r\n", __FUNCTION__, 
                               u4TestValFsY1564SlaTrafProfileId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_MAPPED);
        return SNMP_FAILURE;
    }
    /* Traffic Profile Id cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : "
                           "Traffic Profile Id  Id %d cannot be de-associated "
                           "from SLA when SLA test is in-progress \r\n",
                           __FUNCTION__, u4TestValFsY1564SlaTrafProfileId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }

    /* Make the Traffic Profile as ACTIVE, check whether all 
     * all parameters are present */
    if (nmhTestv2FsY1564TrafProfRowStatus (pu4ErrorCode,
                                           pSlaEntry->u4ContextId,
                                           u4TestValFsY1564SlaTrafProfileId,
                                           ACTIVE) == SNMP_FAILURE)
    {
        /* TestRoutine itself takes care of filling Traces */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaStepLoadRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaStepLoadRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaStepLoadRate (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SlaId, 
                                      INT4 i4TestValFsY1564SlaStepLoadRate)
{
    tSlaInfo *pSlaEntry = NULL;

    if ((i4TestValFsY1564SlaStepLoadRate < Y1564_MIN_RATE_STEP) ||
        (i4TestValFsY1564SlaStepLoadRate > Y1564_MAX_RATE_STEP))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Invalid value for Step "
                           "Load Rate.Range should be between 1 to 10 \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_RATE);
        return SNMP_FAILURE;
    }   

    /* Rate step should be divisor of 100 */
    if ((Y1564_MAX_RATE_STEP % i4TestValFsY1564SlaStepLoadRate) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_RATE);
        return SNMP_FAILURE;
    }

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Step Load Rate cannot be modified when Step Load CIR is selected 
     * and test is in-progress */

    if ((pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS) &&
        ((pSlaEntry->u1SlaConfTestSelector & Y1564_TEST_STEP_LOAD_CIR) ==
         Y1564_TEST_STEP_LOAD_CIR))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | Y1564_CTRL_TRC 
                           | Y1564_ALL_FAILURE_TRC, " %s :step "
                           "load rate %d cannot be modified when SLA test is "
                           "in-progress and step load CIR is also selected for "
                           "configuration test \r\n", __FUNCTION__,
                           i4TestValFsY1564SlaStepLoadRate);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaConfTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaConfTestDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaConfTestDuration (UINT4 *pu4ErrorCode, 
                                          UINT4 u4FsY1564ContextId, 
                                          UINT4 u4FsY1564SlaId, 
                                          INT4 i4TestValFsY1564SlaConfTestDuration)
{
    tSlaInfo *pSlaEntry = NULL;

    if ((i4TestValFsY1564SlaConfTestDuration < Y1564_CONF_TEST_MIN_DURATION) ||
        (i4TestValFsY1564SlaConfTestDuration > Y1564_CONF_TEST_MAX_DURATION))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid value for "
                           "configuration test duration. Range should be "
                           "between 10 to 60 \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_CONF_TEST_DURATION);
        return SNMP_FAILURE;
    }   

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Test Duration cannot be modified for a SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | Y1564_CTRL_TRC 
                           | Y1564_ALL_FAILURE_TRC, " %s : "
                           "Configuration test duration %d cannot  be modified "
                           "from SLA when test is in-progress \r\n", 
                           __FUNCTION__,i4TestValFsY1564SlaConfTestDuration);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaTestStatus (UINT4 *pu4ErrorCode, 
                                    UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SlaId, 
                                    INT4 i4TestValFsY1564SlaTestStatus)
{
    tSlaInfo *pSlaEntry = NULL;
    UINT1     u1CurrentTestSelector = Y1564_ZERO;

    if ((i4TestValFsY1564SlaTestStatus != Y1564_SLA_START) &&
        (i4TestValFsY1564SlaTestStatus != Y1564_SLA_STOP))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid value for "
                           "test status. The value can be either start(1) or "
                           "stop (2) \r\n ", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR(CLI_Y1564_ERR_INVALID_TEST_STATUS);
        return SNMP_FAILURE;
    }   

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

     u1CurrentTestSelector = pSlaEntry->u1SlaConfTestSelector;

    /* Traffic policing test can be started as part of
     * configuration test only when Traffic policing 
     * status is on */
     if ((u1CurrentTestSelector & Y1564_TRAF_POLICING_TEST) 
		     == Y1564_TRAF_POLICING_TEST)
     {
	     u1CurrentTestSelector &= (UINT1) ~ Y1564_TRAF_POLICING_TEST;

	     if (pSlaEntry->u1SlaTrafPolicing != Y1564_TRAF_POLICY_STATUS_ON)
	     {
		     if (u1CurrentTestSelector != Y1564_ZERO) 
		     {
			     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
					     " %s : Traffic Policing status is off "
					     "for the given Sla Id %d.So Traffic Policing "
					     "test alone cannot be started \r\n", __FUNCTION__,
					     pSlaEntry->u4SlaId);
		     }
		     else
		     {
			     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
					     " %s : Traffic Policing status is off "
					     "for the given Sla Id %d.So Traffic Policing "
					     "test cannot be started \r\n", __FUNCTION__,
					     pSlaEntry->u4SlaId);
			     CLI_SET_ERR(CLI_Y1564_ERR_TRAF_POLICY_TEST_START);
			     return SNMP_FAILURE;

		     }
	     }
     }

     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaServiceConfId
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaServiceConfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaServiceConfId (UINT4 *pu4ErrorCode, 
                                       UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       UINT4 u4TestValFsY1564SlaServiceConfId)
{
#ifndef MEF_WANTED
    tSlaInfo *pSlaEntry = NULL;
    tServiceConfInfo *pServiceConfEntry = NULL;

    if (u4TestValFsY1564SlaServiceConfId != Y1564_ZERO)
    {
        if (u4TestValFsY1564SlaServiceConfId > Y1564_MAX_SERVICE_CONF_ID)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : Invalid range for "
                               "ServiceConfig Id. The value should be "
                               "between 1 to 10 \r\n ", __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR(CLI_Y1564_ERR_INVALID_SERVICE_CONF_ID);
            return SNMP_FAILURE;
        }  

        pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                               u4TestValFsY1564SlaServiceConfId,
                                                               pu4ErrorCode);
        if (pServiceConfEntry == NULL)
        {
            /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
             * of filling the error code, CLI set error and trace messages.
             * So simply return failure. */
            return SNMP_FAILURE;
        }
    }

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (u4TestValFsY1564SlaServiceConfId != Y1564_ZERO)
    {
        if ((pSlaEntry->u2SlaServiceConfId != Y1564_ZERO) &&
            (pSlaEntry->u2SlaServiceConfId != u4TestValFsY1564SlaServiceConfId))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : "
                               "Service Configuration Id cannot be changed "
                               "without de-associating the existing Service "
                               "Configuration Id  from SLA  \r\n",
                               __FUNCTION__,u4TestValFsY1564SlaServiceConfId);
            CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_SERVICE_CONF_ENTRY);        
            return SNMP_FAILURE;
        }
    }

    if ((u4TestValFsY1564SlaServiceConfId == Y1564_ZERO) &&
        (pSlaEntry->u2SlaServiceConfId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                               Y1564_ALL_FAILURE_TRC, " %s : ServiceConfId"
                               "is not present to de-associate it "
                               "from SLA \r\n", __FUNCTION__, 
                               u4TestValFsY1564SlaServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_MAPPED);
        return SNMP_FAILURE;
    }

    /* Service Configuration Id cannot be de-associated from SLA when
     * test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : "
                           "Service Configuration Id cannot be de-associated "
                           "from SLA when test is in-progress \r\n",
                           __FUNCTION__,u4TestValFsY1564SlaServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }

    /* Check whether all the mandatory parameters are
     * configured while mapping the entry to SLA
     * MApping should not be allowed when mandatroy parameters
     * for service configuration is not done */
    if (nmhTestv2FsY1564ServiceConfRowStatus (pu4ErrorCode,
                                              pSlaEntry->u4ContextId,
                                              u4TestValFsY1564SlaServiceConfId, 
                                              ACTIVE) == SNMP_FAILURE)
    {
        /* SetRoutine itself takes care of filling Traces */
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (u4FsY1564ContextId);
    UNUSED_PARAM (u4FsY1564SlaId);
    UNUSED_PARAM (u4TestValFsY1564SlaServiceConfId);
#endif

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaTrafPolicing
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaTrafPolicing
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaTrafPolicing (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SlaId, 
                                      INT4 i4TestValFsY1564SlaTrafPolicing)
{
    tSlaInfo *pSlaEntry = NULL;

    if ((i4TestValFsY1564SlaTrafPolicing != Y1564_TRAF_POLICY_STATUS_ON) &&
        (i4TestValFsY1564SlaTrafPolicing != Y1564_TRAF_POLICY_STATUS_OFF))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Invalid value for "
                           "Traffic Policing status. The value should be "
                           "on(1) or off(2) \r\n ", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR(CLI_Y1564_ERR_INVALID_TRAF_POLICING);
        return SNMP_FAILURE;
    }   

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Traffic Policing Status cannot be modified when 
     * test is in-progress */

    if ((pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS) &&
        (pSlaEntry->u1SlaTrafPolicing == Y1564_TRAF_POLICY_STATUS_ON))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Traf "
                           "Policing cannot be modified when SLA "
                           "test is in-progress \r\n",
                           __FUNCTION__,i4TestValFsY1564SlaTrafPolicing);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaTestSelector
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaTestSelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaTestSelector (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SlaId, 
                                      INT4 i4TestValFsY1564SlaTestSelector)
{

    tSlaInfo *pSlaEntry = NULL;

    if ((i4TestValFsY1564SlaTestSelector < Y1564_MIN_TEST_VAL) ||
        (i4TestValFsY1564SlaTestSelector > Y1564_MAX_TEST_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR(CLI_Y1564_ERR_INVALID_TEST_SELECTOR);
        return SNMP_FAILURE;
    }

    pSlaEntry = Y1564LwUtilValidateCxtAndSla (u4FsY1564ContextId,
                                              u4FsY1564SlaId,
                                              pu4ErrorCode);
    if (pSlaEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSla itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* test selector cannot be modified when test is in-progress */
    if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : "
                           "Test Selector cannot be modified for SLA "
                           "when test is in-progress \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         CLI_SET_ERR (CLI_Y1564_ERR_SAME_SLA_IN_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SlaRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId

                The Object 
                testValFsY1564SlaRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SlaRowStatus (UINT4 *pu4ErrorCode, 
                                   UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SlaId, 
                                   INT4 i4TestValFsY1564SlaRowStatus)
{
    tSlaInfo          *pSlaEntry = NULL;
    UINT4             u4EntryCount = Y1564_ZERO;

  
   /* Check the system status. Trap configuration will be allowed
    * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the range of SLA ID */
    if ((u4FsY1564SlaId < Y1564_MIN_SLA_ID) || 
        (u4FsY1564SlaId > Y1564_MAX_SLA_ID))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s :Invalid Service Level Agreement Id  \r\n ",
                           __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check the Row Status value */
    if ((i4TestValFsY1564SlaRowStatus < ACTIVE) ||
        (i4TestValFsY1564SlaRowStatus > DESTROY))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Invalid Row Status "
                           "for SLA id %d \r\n ",__FUNCTION__, u4FsY1564SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4FsY1564ContextId, u4FsY1564SlaId);
   
    if ((pSlaEntry == NULL) &&
        ((i4TestValFsY1564SlaRowStatus == NOT_IN_SERVICE) ||
        (i4TestValFsY1564SlaRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s :SLA entry is not present to make the "
                           "Row status as Not-In-Service or Active \r\n ",
                           __FUNCTION__);

        return SNMP_FAILURE;
    }

    switch (i4TestValFsY1564SlaRowStatus)
    {
	    case CREATE_AND_WAIT:
		    if (pSlaEntry != NULL)
		    {
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC, 
                                " %s : Sla Id %d is already created \r\n",
					    __FUNCTION__,u4FsY1564SlaId);
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    CLI_SET_ERR (CLI_Y1564_ERR_SLA_ALREADY_CREATED);
			    return SNMP_FAILURE;
		    }
		    /* Count is fetched from RBTreeCount datastructure and it is
		     * compared with maximum Number of SAC entries that are allowed to create */
		    RBTreeCount (Y1564_SLA_TABLE(), &u4EntryCount);
		    if (u4EntryCount == MAX_Y1564_SLA_ENTRIES)
		    {
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC
					    | Y1564_MGMT_TRC, " %s : Maximum number of allowed SLAs "
                   "reached \r\n", __FUNCTION__);
			    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			    CLI_SET_ERR (CLI_Y1564_ERR_MAX_SLA_REACHED);
			    return SNMP_FAILURE;
		    }

            break;
        case DESTROY:

            if (pSlaEntry == NULL)
            {

                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : Specified Sla Id is not present \r\n",
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : SLA Entry deletion is not allowed "
                                   "as the test is in-progress for Sla Id %d, "
                                   "either stop the test or wait for the test completion \r\n",
                                   __FUNCTION__,u4FsY1564SlaId);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return SNMP_FAILURE;
            }

            if (pSlaEntry->u4SlaSacId != Y1564_ZERO)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : SLA Entry "
                                   "deletion is not allowed as the SAC "
                                   "Id %d is associated with this SLA\r\n",__FUNCTION__,
                                   pSlaEntry->u4SlaSacId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_SAC_EXISTS);
                return SNMP_FAILURE;

            }
            if (pSlaEntry->u4SlaTrafProfId != Y1564_ZERO)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : SLA Entry "
                                   "deletion is not allowed as the Traffic Profile is "
                                   "%d is associated with this SLA\r\n",__FUNCTION__,
                                   pSlaEntry->u4SlaTrafProfId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TRAF_PROF_EXISTS);
                return SNMP_FAILURE;

            }
#ifdef MEF_WANTED
            if (pSlaEntry->u2EvcId != Y1564_ZERO)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : SLA Entry "
                                   "deletion is not allowed as the EVC "
                                   "Id %d is associated with this SLA\r\n",__FUNCTION__,
                                   pSlaEntry->u2EvcId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_EVC_EXISTS);
                return SNMP_FAILURE;
            }
#else
            if (pSlaEntry->u2SlaServiceConfId != Y1564_ZERO)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, 
                                   " %s : SLA deletion is not allowed when "
                                   "ServiceConfId is associated with SLA "
                                   "Id %d\r\n",__FUNCTION__,pSlaEntry->u2SlaServiceConfId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_SERVICE_CONFIG_EXISTS);
                return SNMP_FAILURE;
            }

#endif
           /* Note: Add check for performance table before deleting the SLA table
              or check to be done in creation of performance table */            
            break;
        case NOT_IN_SERVICE:

            if (pSlaEntry->u1RowStatus == NOT_READY)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : SLA entry  %d cannot move from "
                                   "Not-Ready to Not-In-Service \r\n",
                                   __FUNCTION__,u4FsY1564SlaId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                 CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_READY);
                return SNMP_FAILURE;
            }

            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : SLA Entry cannot be set as "
                                   "not-in-service as the test is in-progress for "
                                   "Sla Id %d either stop the test or wait for the "
                                   "test completion \r\n",
                                   __FUNCTION__,u4FsY1564SlaId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return SNMP_FAILURE;
            }
           break;
        case ACTIVE:
            /* Row status can be Active only if all the below mandatory parameters are
             * associated with SLA 
             * SAC should be mapped to a SLA
             * Traffic Profile should be mapped to a SLA
             * EVC Id/Service Config Id (if MEF is not defined) should be mapped to 
               a SLA 
             * CFM entries (MEG, ME, MEP)should be mapped to a SLA
             * If all the parameters are mapped to SLA then row status can be set as
             * Not-In-Service */
 
            if (pSlaEntry->u1RowStatus == NOT_READY)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : SLA entry  %d cannot move from "
                                   "Not-Ready to Active \r\n",
                                   __FUNCTION__,u4FsY1564SlaId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_READY);
                return SNMP_FAILURE;
            }
            break;

        default:
          *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
           return SNMP_FAILURE;
     }
 
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564SlaTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsY1564SlaTable (UINT4 *pu4ErrorCode, 
                              tSnmpIndexList *pSnmpIndexList, 
                              tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564TrafProfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564TrafProfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564TrafProfTable (UINT4 u4FsY1564ContextId, 
                                                   UINT4 u4FsY1564TrafProfId)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;

    /* Inside this function the following are validated *
     * Traffic Profile Identifier
     * Context Identifier
     * TrafProfTable */

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : Traffic Profile Entry is NULL  \r\n",
                           __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564TrafProfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsY1564TrafProfTable (UINT4 *pu4FsY1564ContextId, 
                                           UINT4 *pu4FsY1564TrafProfId)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;

    pTrafProfEntry = Y1564UtilGetFirstNodeFromTrafProfTable ();

    if (pTrafProfEntry != NULL)
    {
        *pu4FsY1564ContextId  = pTrafProfEntry->u4ContextId;
        *pu4FsY1564TrafProfId = pTrafProfEntry->u4TrafProfId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564TrafProfTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564TrafProfId
                nextFsY1564TrafProfId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564TrafProfTable (UINT4 u4FsY1564ContextId,
                                          UINT4 *pu4NextFsY1564ContextId, 
                                          UINT4 u4FsY1564TrafProfId,
                                          UINT4 *pu4NextFsY1564TrafProfId)
{
    tTrafficProfileInfo      *pNextTrafProfEntry = NULL;
    tTrafficProfileInfo       TrafProfEntry;

    MEMSET (&TrafProfEntry, 0, sizeof (tTrafficProfileInfo));

    TrafProfEntry.u4ContextId = u4FsY1564ContextId;
    TrafProfEntry.u4TrafProfId = u4FsY1564TrafProfId;

    pNextTrafProfEntry = Y1564UtilGetNextNodeFromTrafProfTable (&TrafProfEntry);

    if (pNextTrafProfEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextTrafProfEntry->u4ContextId;
        *pu4NextFsY1564TrafProfId = pNextTrafProfEntry->u4TrafProfId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfDir
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfDir
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfDir (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564TrafProfId,
                               INT4 *pi4RetValFsY1564TrafProfDir)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProf Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);
    if (pTrafProfEntry== NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564TrafProfDir = (INT4) pTrafProfEntry->TrafProfDirection;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfPktSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfPktSize (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564TrafProfId, 
                                   INT4 *pi4RetValFsY1564TrafProfPktSize)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and Traf Prof Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);

    if (pTrafProfEntry== NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564TrafProfPktSize = (INT4) pTrafProfEntry->u2TrafProfPktSize;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfPayload
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfPayload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfPayload (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564TrafProfId, 
                                   tSNMP_OCTET_STRING_TYPE * pRetValFsY1564TrafProfPayload)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and Traf Prof Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);
    if (pTrafProfEntry== NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        return SNMP_FAILURE;
    }

    /* Copy the Payload information of the traffic profile */
    MEMSET (pRetValFsY1564TrafProfPayload->pu1_OctetList, 0, 
            sizeof (tSNMP_OCTET_STRING_TYPE));

    pRetValFsY1564TrafProfPayload->i4_Length =
        (INT4) (STRLEN (pTrafProfEntry->au1TrafProfPayload));

    MEMCPY (pRetValFsY1564TrafProfPayload->pu1_OctetList,
            pTrafProfEntry->au1TrafProfPayload,
            pRetValFsY1564TrafProfPayload->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfOptEmixPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfOptEmixPktSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfOptEmixPktSize (UINT4 u4FsY1564ContextId, 
                                          UINT4 u4FsY1564TrafProfId, 
                                          tSNMP_OCTET_STRING_TYPE * 
                                          pRetValFsY1564TrafProfOptEmixPktSize)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and Traf Prof Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);
    if (pTrafProfEntry== NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        return SNMP_FAILURE;
    }


   /* Copy the Emix Packet Size information of the traffic profile */
    pRetValFsY1564TrafProfOptEmixPktSize->i4_Length =
                (INT4) (STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize));

    MEMSET (pRetValFsY1564TrafProfOptEmixPktSize->pu1_OctetList, 0,
                            pRetValFsY1564TrafProfOptEmixPktSize->i4_Length);

    MEMCPY (pRetValFsY1564TrafProfOptEmixPktSize->pu1_OctetList,
            pTrafProfEntry->au1TrafProfOptEmixPktSize,
             pRetValFsY1564TrafProfOptEmixPktSize->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfPCP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfPCP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfPCP (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564TrafProfId, 
                               INT4 *pi4RetValFsY1564TrafProfPCP)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and Traf Prof Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);
    if (pTrafProfEntry== NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        return SNMP_FAILURE;
    }

   *pi4RetValFsY1564TrafProfPCP  = (INT4) pTrafProfEntry->u1TrafProfPCP;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564TrafProfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                retValFsY1564TrafProfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564TrafProfRowStatus (UINT4 u4FsY1564ContextId, 
                                     UINT4 u4FsY1564TrafProfId, 
                                     INT4 *pi4RetValFsY1564TrafProfRowStatus)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and Traf Prof Entry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, 
                                                    u4FsY1564TrafProfId);
    if (pTrafProfEntry== NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564TrafProfRowStatus  = (INT4) pTrafProfEntry->u1RowStatus;

    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfDir
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfDir
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfDir (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564TrafProfId, 
                               INT4 i4SetValFsY1564TrafProfDir)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProfEntry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Traffic Profile "
                           "Entry not present for %d\r\n",__FUNCTION__,
                           u4FsY1564TrafProfId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return SNMP_FAILURE;
    }

   /* Set the direction for the traffic profile */
    pTrafProfEntry->TrafProfDirection = (UINT4) i4SetValFsY1564TrafProfDir;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfPktSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfPktSize (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564TrafProfId, 
                                   INT4 i4SetValFsY1564TrafProfPktSize)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProfEntry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return SNMP_FAILURE;
    }

     /* Set the Packet Size for the traffic profile */
    pTrafProfEntry->u2TrafProfPktSize = (UINT2) i4SetValFsY1564TrafProfPktSize;

    /* Reset Emix selected to False when Individual packet size is selected */

    pTrafProfEntry->u1EmixSelected = Y1564_SNMP_FALSE;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfPayload
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfPayload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfPayload (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564TrafProfId, 
                                   tSNMP_OCTET_STRING_TYPE *pSetValFsY1564TrafProfPayload)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProfEntry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n", __FUNCTION__,
                           u4FsY1564TrafProfId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Payload information for the traffic profile */
    MEMSET (pTrafProfEntry->au1TrafProfPayload,
            Y1564_ZERO, sizeof (pTrafProfEntry->au1TrafProfPayload));

    MEMCPY (pTrafProfEntry->au1TrafProfPayload,
            pSetValFsY1564TrafProfPayload->pu1_OctetList,
            pSetValFsY1564TrafProfPayload->i4_Length);

    Y1564LwUtilTrafProfRowStatusNotInServ (pTrafProfEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfOptEmixPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfOptEmixPktSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfOptEmixPktSize (UINT4 u4FsY1564ContextId,
                                          UINT4 u4FsY1564TrafProfId, 
                                          tSNMP_OCTET_STRING_TYPE *pSetValFsY1564TrafProfOptEmixPktSize)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProfEntry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    pTrafProfEntry->u1EmixSelected = Y1564_SNMP_TRUE;

    /* Set the Emix Packet Size for the traffic profile */
    MEMSET (pTrafProfEntry->au1TrafProfOptEmixPktSize,
            Y1564_ZERO, sizeof (pTrafProfEntry->au1TrafProfOptEmixPktSize));

    MEMCPY(pTrafProfEntry->au1TrafProfOptEmixPktSize,
           pSetValFsY1564TrafProfOptEmixPktSize->pu1_OctetList,
           pSetValFsY1564TrafProfOptEmixPktSize->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfPCP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfPCP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfPCP (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564TrafProfId, 
                               INT4 i4SetValFsY1564TrafProfPCP)
{
    tTrafficProfileInfo          *pTrafProfEntry = NULL;

    /* Inside this function Context Id and TrafProfEntry are validated */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Traffic Profile "
                           "Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564TrafProfId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the PCP Value for the traffic profile */
    pTrafProfEntry->u1TrafProfPCP = (UINT1) i4SetValFsY1564TrafProfPCP;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564TrafProfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                setValFsY1564TrafProfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564TrafProfRowStatus (UINT4 u4FsY1564ContextId, 
                                     UINT4 u4FsY1564TrafProfId, 
                                     INT4 i4SetValFsY1564TrafProfRowStatus)
{
    tTrafficProfileInfo  *pTrafProfEntry = NULL;
    tTrafficProfileInfo  TrafProfEntry;

    if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s: does not exist\r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

   switch (i4SetValFsY1564TrafProfRowStatus)
   {
       case CREATE_AND_WAIT:

           pTrafProfEntry = Y1564UtilCreateTrafProfNode();

           if (pTrafProfEntry == NULL)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                  Y1564_ALL_FAILURE_TRC, " %s : Failed to "
                                  "create Traffic profile entry for Id %d\r\n",
                                  __FUNCTION__,u4FsY1564TrafProfId);
               return SNMP_FAILURE;
           }

           /* Update Traffic Profile  and Context Id to Sla
            * Structure */
           pTrafProfEntry->u4TrafProfId = u4FsY1564TrafProfId;
           pTrafProfEntry->u4ContextId = u4FsY1564ContextId;

           if (Y1564UtilAddNodeToTrafProfRBTree (pTrafProfEntry) != OSIX_SUCCESS)
           {
               /* Release memory allocated for Traffic Profile Entry */
               MemReleaseMemBlock (Y1564_TRAF_PROF_POOL (), (UINT1 *)  pTrafProfEntry);
               pTrafProfEntry = NULL;

               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                  Y1564_ALL_FAILURE_TRC, " %s : Traffic "
                                  "Profile Entry Add Failed for Traffic "
                                  "Profile %d\r\n",
                                  __FUNCTION__,u4FsY1564TrafProfId);
               return SNMP_FAILURE;
           }
           Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                              Y1564_CTRL_TRC, " %s : Traffic Profile Entry "
                              "Added to RBTree with Id  %d\r\n",__FUNCTION__,
                              u4FsY1564TrafProfId);

           /* Set the RowStatus as Not Ready, to be moved to Not In Service
            * only when the mandatory objects for the Table are set */

           pTrafProfEntry->u1RowStatus = NOT_READY;
           break;

       case NOT_IN_SERVICE:

           TrafProfEntry.u4TrafProfId = u4FsY1564TrafProfId;
           TrafProfEntry.u4ContextId = u4FsY1564ContextId;

           pTrafProfEntry = Y1564UtilGetNodeFromTrafProfTable (&TrafProfEntry);

           if (pTrafProfEntry == NULL)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                  Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                  " %s : Specific Traffic Profile Id "
                                  "is not present \r\n",
                                  __FUNCTION__);

               return SNMP_FAILURE;
           }

           pTrafProfEntry->u1RowStatus = (UINT1) i4SetValFsY1564TrafProfRowStatus;
           break;

       case DESTROY:

           TrafProfEntry.u4TrafProfId = u4FsY1564TrafProfId;
           TrafProfEntry.u4ContextId = u4FsY1564ContextId;

           pTrafProfEntry = Y1564UtilGetNodeFromTrafProfTable (&TrafProfEntry);

           if (pTrafProfEntry == NULL)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                  Y1564_ALL_FAILURE_TRC, " %s : Specified Traffic "
                                  "Profile Id is not  present \r\n", __FUNCTION__);
               CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
               return SNMP_FAILURE;
           }

           if (Y1564UtilTrafProfDelNodeFromRBTree (pTrafProfEntry) != OSIX_SUCCESS)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                  Y1564_ALL_FAILURE_TRC," %s : Traffilc "
                                  "Profile Entry deletion failed \r\n", 
                                  __FUNCTION__);
               return SNMP_FAILURE;
           }
           break;

       case ACTIVE:

           TrafProfEntry.u4TrafProfId = u4FsY1564TrafProfId;
           TrafProfEntry.u4ContextId = u4FsY1564ContextId;

           pTrafProfEntry = Y1564UtilGetNodeFromTrafProfTable (&TrafProfEntry);

           if (pTrafProfEntry == NULL)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                  Y1564_ALL_FAILURE_TRC," %s : Specified "
                                  "Traffic Profile Id is not present \r\n", 
                                  __FUNCTION__);
               CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
               return SNMP_FAILURE;
           }

           if (pTrafProfEntry->u1RowStatus == ACTIVE)
           {
               return SNMP_SUCCESS;
           }

           pTrafProfEntry->u1RowStatus = (UINT1) i4SetValFsY1564TrafProfRowStatus;
           break;

       default:
           return SNMP_FAILURE;
   }
   return SNMP_SUCCESS; 
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfDir
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfDir
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfDir (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsY1564ContextId, 
                                  UINT4 u4FsY1564TrafProfId, 
                                  INT4 i4TestValFsY1564TrafProfDir)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;
 
    if ((i4TestValFsY1564TrafProfDir != Y1564_DIRECTION_INTERNAL) &&
        (i4TestValFsY1564TrafProfDir != Y1564_DIRECTION_EXTERNAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_DIRECTION);
        return SNMP_FAILURE;
    }   

    pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                        u4FsY1564TrafProfId,
                                                        pu4ErrorCode);
    if (pTrafProfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Traf Prof to SLA
     * and returns failure if the SLA test is in-progress.

     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                           u4FsY1564TrafProfId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfPktSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfPktSize (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564TrafProfId, 
                                      INT4 i4TestValFsY1564TrafProfPktSize)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;

    if ((i4TestValFsY1564TrafProfPktSize < Y1564_MIN_PACKET_SIZE) ||
        (i4TestValFsY1564TrafProfPktSize > Y1564_MAX_PACKET_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PKT_SIZE);
        return SNMP_FAILURE;
    }   

    if ((i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_64) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_128) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_256) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_512) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_1024) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_1280) &&
        (i4TestValFsY1564TrafProfPktSize != Y1564_FRAME_1518))
     {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid Packet Size "
                           "for Traffic Profile \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PKT_SIZE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return SNMP_FAILURE;
     }

    pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                        u4FsY1564TrafProfId,
                                                        pu4ErrorCode);
    if (pTrafProfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Traf Prof to SLA
     * and returns failure if the SLA test is in-progress.

     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                           u4FsY1564TrafProfId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfPayload
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfPayload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfPayload (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564TrafProfId, 
                                      tSNMP_OCTET_STRING_TYPE *pTestValFsY1564TrafProfPayload)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4                u4SlaId = Y1564_ZERO;
    INT4                 i4Index = Y1564_ZERO;
    INT4                 i4StrLen = Y1564_ZERO;

    i4StrLen = (INT4) STRLEN (pTestValFsY1564TrafProfPayload->pu1_OctetList);

    if (i4StrLen > Y1564_PAYLOAD_SIZE)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Payload size exceeds allowed range "  
                           "for Traffic Profile Payload \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PAYLOAD_LENGTH);
        return SNMP_FAILURE;
    }   

    for (; i4Index <= i4StrLen - 1; i4Index++)
    {
        if (!(((pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] >=
               Y1564_UPPER_CHAR_START) &&
              (pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] <=
               Y1564_UPPER_CHAR_END)) || 
              ((pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] >=
               Y1564_LOWER_CHAR_START) &&
              (pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] <=
               Y1564_LOWER_CHAR_END)) ||
              ((pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] >=
               Y1564_NUM_CHAR_START) &&
              (pTestValFsY1564TrafProfPayload->pu1_OctetList[i4Index] <=
               Y1564_NUM_CHAR_END))))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                               Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                               " %s : characters (A-Z, a-z) and numeric "
                               "values (0-9) only allowed \r\n",
                               __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PAYLOAD);
            return SNMP_FAILURE;
        }
    }

    pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                        u4FsY1564TrafProfId,
                                                        pu4ErrorCode);
    if (pTrafProfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Traf Prof to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                           u4FsY1564TrafProfId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfOptEmixPktSize
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfOptEmixPktSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfOptEmixPktSize (UINT4 *pu4ErrorCode, 
                                             UINT4 u4FsY1564ContextId, 
                                             UINT4 u4FsY1564TrafProfId, 
                                             tSNMP_OCTET_STRING_TYPE *pTestValFsY1564TrafProfOptEmixPktSize)
{
    
    tTrafficProfileInfo        *pTrafProfEntry = NULL;
    UINT4                      u4SlaId = Y1564_ZERO;
    UINT1                      au1SlaList[Y1564_EMIX_PKT_SIZE] = { 0 };
    INT4                       i4Index = Y1564_ZERO;
    INT4                       i4StrLen = Y1564_ZERO;

    i4StrLen = (INT4) STRLEN (pTestValFsY1564TrafProfOptEmixPktSize->pu1_OctetList);

    if ((i4StrLen > Y1564_EMIX_PKT_SIZE) || (i4StrLen <= Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC," %s : Emix Size not in "
                           "allowed range for Emix pattern size \r\n", 
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_EMIX_PATTERN);
        return SNMP_FAILURE;
    }   

    MEMCPY(au1SlaList, pTestValFsY1564TrafProfOptEmixPktSize->pu1_OctetList,
           pTestValFsY1564TrafProfOptEmixPktSize->i4_Length);

    for (; i4Index <= i4StrLen - 1; i4Index++)
    {
        /* if the frame size given is not meeting the
         * allowable frame size then return failure */
        if (!((au1SlaList[i4Index] == 'a')    ||
              (au1SlaList[i4Index] == 'b')   ||
              (au1SlaList[i4Index] == 'c')   ||
              (au1SlaList[i4Index] == 'd')   ||
              (au1SlaList[i4Index] == 'e')  ||
              (au1SlaList[i4Index] == 'f')  ||
              (au1SlaList[i4Index] == 'g')))
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC," %s : Packet Size "
                               "configured in Emix Pattern is not in "
                               "the allowed range \r\n",
                               __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
             CLI_SET_ERR (CLI_Y1564_ERR_INVALID_EMIX_PATTERN);
            return SNMP_FAILURE;

        }
    }
    pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                        u4FsY1564TrafProfId,
                                                        pu4ErrorCode);
    if (pTrafProfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }


    /* This function check for association of Traf Prof to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                           u4FsY1564TrafProfId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfPCP
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfPCP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfPCP (UINT4 *pu4ErrorCode, 
                                  UINT4 u4FsY1564ContextId, 
                                  UINT4 u4FsY1564TrafProfId, 
                                  INT4 i4TestValFsY1564TrafProfPCP)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;

    if ((i4TestValFsY1564TrafProfPCP < Y1564_VLAN_8P0D_SEL_ROW) ||
        (i4TestValFsY1564TrafProfPCP > Y1564_VLAN_5P3D_SEL_ROW))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid PCP value. "
                           "Value can be Y1564_VLAN_8P0D_SEL_ROW(1), "
                           "Y1564_VLAN_7P1D_SEL_ROW(2), " 
                           "Y1564_VLAN_6P2D_SEL_ROW(3) or "
                           "Y1564_VLAN_5P3D_SEL_ROW(4) \r\n", __FUNCTION__,
                           u4FsY1564TrafProfId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    pTrafProfEntry = Y1564LwUtilValidateCxtAndTrafProf (u4FsY1564ContextId,
                                                        u4FsY1564TrafProfId,
                                                        pu4ErrorCode);
    if (pTrafProfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndTrafProf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Traf Prof to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                           u4FsY1564TrafProfId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564TrafProfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId

                The Object 
                testValFsY1564TrafProfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564TrafProfRowStatus (UINT4 *pu4ErrorCode, 
                                        UINT4 u4FsY1564ContextId, 
                                        UINT4 u4FsY1564TrafProfId, 
                                        INT4 i4TestValFsY1564TrafProfRowStatus)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    UINT4  u4SlaId = Y1564_ZERO;
    UINT4  u4EntryCount = Y1564_ZERO;


    /* Check the system status. Traf Prof configuration will be allowed
     * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

   /* Check the range of Taffic Profile ID */
    if ((u4FsY1564TrafProfId < Y1564_MIN_TRAF_PROF_ID) ||
        (u4FsY1564TrafProfId > Y1564_MAX_TRAF_PROF_ID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check the Row Status value */
    if ((i4TestValFsY1564TrafProfRowStatus < ACTIVE) ||
        (i4TestValFsY1564TrafProfRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4FsY1564ContextId, u4FsY1564TrafProfId);

    /* Modifying or Activating the non-existing entry 
     * is not allowed when the entry is not present */
    if ((pTrafProfEntry == NULL) &&
        ((i4TestValFsY1564TrafProfRowStatus == NOT_IN_SERVICE) ||
        (i4TestValFsY1564TrafProfRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        Y1564_GLOBAL_TRC ( "Traffic Profile entry is not present to make the "
                           "Row status as Not-In-Service or Active \r\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValFsY1564TrafProfRowStatus)
    {
	    case CREATE_AND_WAIT:
		    if (pTrafProfEntry != NULL)
		    {
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId,
					    Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
					    " %s : Traffic Profile Entry for Id %d\r\n",
					    __FUNCTION__,u4FsY1564TrafProfId);
			    CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_ALREADY_CREATED);
			    return SNMP_FAILURE;
		    }
		    /* Count is fetched from RBTreeCount datastructure and it is
		     * compared with maximum Number of  Trafic Profile entries that are allowed to create */
		    RBTreeCount (Y1564_TRAF_PROF_TABLE(), &u4EntryCount);
		    if (u4EntryCount == MAX_Y1564_TRAFFIC_PROFILES)
		    {
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC
					    | Y1564_MGMT_TRC, " %s : Maximum number of allowed Traffic "
                   "Profile Entries reached \r\n",
					    __FUNCTION__);
			    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			    CLI_SET_ERR (CLI_Y1564_ERR_MAX_TRAF_PROF_REACHED);
			    return SNMP_FAILURE;
		    }

            break;
        case DESTROY:
            if (pTrafProfEntry == NULL)
            {
                Y1564_GLOBAL_TRC ( "Entry for the given Traffic Profile Id " 
                           "is not present \r\n");
                CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
                return SNMP_FAILURE;
            }

            /* This function check for association of Traf Prof to SLA
             * and returns failure if the SLA test is in-progress.

             * configuration will not be allowed if the test is
             * in-progress */

            if (Y1564UtilValidateTrafProfAndSla (u4FsY1564ContextId, u4SlaId,
                                                   u4FsY1564TrafProfId) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;

            }
            break;
        case ACTIVE:
           /* Row status can be Active only if all the below mandatory parameters are
            * associated with traffic profile.
            * Payload Information */

           /* If the parameters are not mapped to Traffic profile then row status will
            * be set as Not-Ready.*/

           if (pTrafProfEntry->u1RowStatus == NOT_READY)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                  Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                  " %s : Traffic Profile entry  %d cannot move from "
                                  "Not-Ready to Not-In-Service \r\n",
                                  __FUNCTION__,u4FsY1564TrafProfId);

               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_READY);
               return SNMP_FAILURE;

           }
           break;           
        default:
          *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
           return SNMP_FAILURE;
     }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564TrafProfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564TrafProfId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsY1564TrafProfTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList *pSnmpIndexList, 
                                   tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564SacTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564SacTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564SacTable (UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564SacId)
{
    tSacInfo *pSacEntry = NULL;

    /* Inside this function the following are validated *
     * Sac Identifier
     * Context Identifier
     * SacTable */

    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
           Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                              Y1564_ALL_FAILURE_TRC," %s : SAC Entry is NULL \r\n",
                              __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564SacTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsY1564SacTable (UINT4 *pu4FsY1564ContextId, 
                                      UINT4 *pu4FsY1564SacId)
{
    tSacInfo *pSacEntry = NULL;

    pSacEntry = Y1564UtilSacGetFirstNodeFromSacTable();

    if (pSacEntry != NULL)
    {
        *pu4FsY1564ContextId = pSacEntry->u4ContextId;
        *pu4FsY1564SacId = pSacEntry->u4SacId;
        return SNMP_SUCCESS;
    }

    Y1564_GLOBAL_TRC ("nmhGetFirstIndexFsY1564SacTable : SAC Entry is NULL \r\n");
   
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564SacTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564SacId
                nextFsY1564SacId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564SacTable (UINT4 u4FsY1564ContextId,
                                     UINT4 *pu4NextFsY1564ContextId, 
                                     UINT4 u4FsY1564SacId,
                                     UINT4 *pu4NextFsY1564SacId )
{
    tSacInfo      *pNextSacEntry = NULL;
    tSacInfo       SacEntry;

    MEMSET (&SacEntry, 0, sizeof (tSacInfo));

    SacEntry.u4ContextId = u4FsY1564ContextId;
    SacEntry.u4SacId = u4FsY1564SacId;

    pNextSacEntry = Y1564UtilSacGetNextNodeFromSacTable (&SacEntry);

    if (pNextSacEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextSacEntry->u4ContextId;
        *pu4NextFsY1564SacId = pNextSacEntry->u4SacId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564SacInfoRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacInfoRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacInfoRate (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564SacId, 
                               INT4 *pi4RetValFsY1564SacInfoRate)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Sac Entry not present"
                           " for %d\r\n", __FUNCTION__,u4FsY1564SacId);
        return SNMP_FAILURE;
    }
   
    *pi4RetValFsY1564SacInfoRate = (INT4) pSacEntry->u4SacInfoRate;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564SacFrLossRatio
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacFrLossRatio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacFrLossRatio (UINT4 u4FsY1564ContextId,
                                  UINT4 u4FsY1564SacId, 
                                  INT4 *pi4RetValFsY1564SacFrLossRatio)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Sac Entry not present"
                           " for %d\r\n",__FUNCTION__, u4FsY1564SacId);

        return SNMP_FAILURE;
    }
 
    *pi4RetValFsY1564SacFrLossRatio = (INT4) pSacEntry->u4SacFrLossRatio;
   
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SacFrTransDelay
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacFrTransDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacFrTransDelay (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SacId, 
                                   INT4 *pi4RetValFsY1564SacFrTransDelay)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Sac Entry not"
                           " present for %d\r\n", __FUNCTION__, u4FsY1564SacId);

        return SNMP_FAILURE;
    }
   
    *pi4RetValFsY1564SacFrTransDelay = (INT4) pSacEntry->u4SacFrTxDelay;
    
     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SacFrDelayVar
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacFrDelayVar
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacFrDelayVar (UINT4 u4FsY1564ContextId, 
                                 UINT4 u4FsY1564SacId, 
                                 INT4 *pi4RetValFsY1564SacFrDelayVar)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Sac Entry not "
                           "present for %d\r\n",__FUNCTION__,u4FsY1564SacId);

        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SacFrDelayVar = (INT4) pSacEntry->u4SacFrDelayVar;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SacAvailability
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacAvailability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacAvailability (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SacId, 
                                   tSNMP_OCTET_STRING_TYPE *pRetValFsY1564SacAvailability)
{
    tSacInfo          *pSacEntry = NULL;
    
    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);

        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsY1564SacAvailability->pu1_OctetList, Y1564_ZERO, 
            sizeof (tSNMP_OCTET_STRING_TYPE));

    if (Y1564_FLOAT_TO_OCTETSTRING ((DBL8) pSacEntry->f4SacAvailability,
                                    pRetValFsY1564SacAvailability) == EOF)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Availability conversion "
                           "error for Sac Id  %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        return SNMP_FAILURE;
    }

    pRetValFsY1564SacAvailability->i4_Length = (INT4) STRLEN (pRetValFsY1564SacAvailability->pu1_OctetList);
        
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564SacRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                retValFsY1564SacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564SacRowStatus (UINT4 u4FsY1564ContextId, 
                                UINT4 u4FsY1564SacId, 
                                INT4 *pi4RetValFsY1564SacRowStatus)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564SacRowStatus = (INT4) pSacEntry->u1RowStatus;

    return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564SacInfoRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacInfoRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacInfoRate (UINT4 u4FsY1564ContextId, 
                               UINT4 u4FsY1564SacId, 
                               INT4 i4SetValFsY1564SacInfoRate)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Information rate to the SAC */
    pSacEntry->u4SacInfoRate = (UINT4) i4SetValFsY1564SacInfoRate;

    Y1564LwUtilSacRowStatusNotInServ (pSacEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SacFrLossRatio
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacFrLossRatio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacFrLossRatio (UINT4 u4FsY1564ContextId, 
                                  UINT4 u4FsY1564SacId, 
                                  INT4 i4SetValFsY1564SacFrLossRatio)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Frame Loss Ratio to the SAC */
    pSacEntry->u4SacFrLossRatio = (UINT4) i4SetValFsY1564SacFrLossRatio;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SacFrTransDelay
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacFrTransDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacFrTransDelay (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SacId, 
                                   INT4 i4SetValFsY1564SacFrTransDelay)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Frame Transfer Delay to the SAC */
    pSacEntry->u4SacFrTxDelay = (UINT4) i4SetValFsY1564SacFrTransDelay;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SacFrDelayVar
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacFrDelayVar
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacFrDelayVar (UINT4 u4FsY1564ContextId, 
                                 UINT4 u4FsY1564SacId, 
                                 INT4 i4SetValFsY1564SacFrDelayVar)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Frame Delay Variation to the SAC */
    pSacEntry->u4SacFrDelayVar = (UINT4) i4SetValFsY1564SacFrDelayVar;

    return SNMP_SUCCESS; 
}
/****************************************************************************
 Function    :  nmhSetFsY1564SacAvailability
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacAvailability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacAvailability (UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SacId, 
                                   tSNMP_OCTET_STRING_TYPE *pSetValFsY1564SacAvailability)
{
    tSacInfo          *pSacEntry = NULL;

    /* Inside this function Context Id and SacEntry are validated */
    pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Sac Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564SacId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Availability to the SAC */
    if (Y1564_OCTETSTRING_TO_FLOAT (pSetValFsY1564SacAvailability,
                                    pSacEntry->f4SacAvailability) == EOF)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Availability conversionerror for Sac Id %d"
                           "\r\n",__FUNCTION__,u4FsY1564SacId);
        return SNMP_FAILURE;
    }    

    Y1564LwUtilSacRowStatusNotInServ (pSacEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564SacRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                setValFsY1564SacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564SacRowStatus (UINT4 u4FsY1564ContextId, 
                                UINT4 u4FsY1564SacId, 
                                INT4 i4SetValFsY1564SacRowStatus)
{
  tSacInfo          *pSacEntry = NULL;
  tSacInfo           SacEntry;

  MEMSET(&SacEntry ,Y1564_ZERO, sizeof (tSacInfo));

  if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
  {
      Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                         Y1564_ALL_FAILURE_TRC, " %s : does not exist\r\n",
                         __FUNCTION__);
      return SNMP_FAILURE;
  }

  switch (i4SetValFsY1564SacRowStatus)
  {
      case CREATE_AND_WAIT:

          pSacEntry = Y1564UtilSacCreateSacNode();

          if (pSacEntry == NULL)
          {
              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s : Failed to create Sac entry for Id %d\r\n",
                                 __FUNCTION__,u4FsY1564SacId);
              return SNMP_FAILURE;
          }
          /* Update SLA and Context Id to Sla 
           * Structure */
          pSacEntry->u4SacId = u4FsY1564SacId;
          pSacEntry->u4ContextId = u4FsY1564ContextId;

          if (Y1564UtilSacAddNodeToSacRBTree (pSacEntry) != OSIX_SUCCESS)
          {
              /* Release memory allocated for SLA Entry */
              MemReleaseMemBlock (Y1564_SAC_POOL (), (UINT1 *) pSacEntry);
              pSacEntry = NULL;

              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s : SAC entry add failed for Id %d\r\n",
                                 __FUNCTION__,u4FsY1564SacId);
              return SNMP_FAILURE;
          }
          Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                             Y1564_MGMT_TRC | Y1564_CTRL_TRC,
                             " %s : SAC entry added to RBTree for Id %d\r\n",
                             __FUNCTION__,u4FsY1564SacId);

          /* Set the RowStatus as Not Ready, to be moved to Not In Service
           * only when the mandatory objects for the Table are set */
          pSacEntry->u1RowStatus = NOT_READY;
          break;

      case NOT_IN_SERVICE:
          SacEntry.u4SacId = u4FsY1564SacId;
          SacEntry.u4ContextId = u4FsY1564ContextId;

          pSacEntry = Y1564UtilSacGetNodeFromSacTable (&SacEntry);

          if (pSacEntry == NULL)
          {
              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s : Specified SAC entry is not present \r\n",
                                 __FUNCTION__);
              CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
              return SNMP_FAILURE;
          }
          pSacEntry->u1RowStatus = NOT_IN_SERVICE;
          break;

      case DESTROY:

          SacEntry.u4SacId = u4FsY1564SacId;
          SacEntry.u4ContextId = u4FsY1564ContextId;

          pSacEntry = Y1564UtilSacGetNodeFromSacTable (&SacEntry);

          if (pSacEntry == NULL)
          {
              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s : Specified SAC entry is not present \r\n",
                                 __FUNCTION__);
              CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
              return SNMP_FAILURE;
          }

          if (Y1564UtilSacDelNodeFromRBTree (pSacEntry) != OSIX_SUCCESS)
          {
              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s :SAC entry delete failed for Sac Id %d\r\n",
                                 __FUNCTION__,u4FsY1564SacId);

              return SNMP_FAILURE;
          }
          break;

      case ACTIVE:

          SacEntry.u4SacId = u4FsY1564SacId;
          SacEntry.u4ContextId = u4FsY1564ContextId;

          pSacEntry = Y1564UtilSacGetNodeFromSacTable (&SacEntry);

          if (pSacEntry == NULL)
          {
              Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                 Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                 " %s : Specified SAC entry is not present\r\n",
                                 __FUNCTION__);
              CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
              return SNMP_FAILURE;
          }

          if (pSacEntry->u1RowStatus == ACTIVE)
          {
              return SNMP_SUCCESS;
          }
          pSacEntry->u1RowStatus = ACTIVE;
          break;

      default:
          return SNMP_FAILURE;
  }

   return SNMP_SUCCESS;

  
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564SacInfoRate
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacInfoRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacInfoRate (UINT4 *pu4ErrorCode, 
                                  UINT4 u4FsY1564ContextId, 
                                  UINT4 u4FsY1564SacId, 
                                  INT4 i4TestValFsY1564SacInfoRate)
{
    tSacInfo *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;
 
    if ((i4TestValFsY1564SacInfoRate <= Y1564_ZERO) ||
        (i4TestValFsY1564SacInfoRate > Y1564_HUNDRED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_INVALID_IR);
        return SNMP_FAILURE;
    }   

    pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                              u4FsY1564SacId,
                                              pu4ErrorCode);
    if (pSacEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
 
    /* This function check for association of SAC to SLA 
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is 
     * in-progress */

    if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
        OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SacFrLossRatio
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacFrLossRatio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacFrLossRatio (UINT4 *pu4ErrorCode, 
                                     UINT4 u4FsY1564ContextId, 
                                     UINT4 u4FsY1564SacId, 
                                     INT4 i4TestValFsY1564SacFrLossRatio)
{
    tSacInfo *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;

    if ((i4TestValFsY1564SacFrLossRatio < Y1564_ZERO) ||
        (i4TestValFsY1564SacFrLossRatio > Y1564_MAX_LOSS_RATIO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         CLI_SET_ERR (CLI_Y1564_ERR_SAC_INVALID_FLR);
        return SNMP_FAILURE;
    }   

    pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                              u4FsY1564SacId,
                                              pu4ErrorCode);
    if (pSacEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of SAC to SLA 
     * and returns failure if the SLA test is in-progress.

     * configuration will not be allowed if the test is 
     * in-progress */

    if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
        OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SacFrTransDelay
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacFrTransDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacFrTransDelay (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SacId, 
                                      INT4 i4TestValFsY1564SacFrTransDelay)
{
    tSacInfo *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;

    if (i4TestValFsY1564SacFrTransDelay < Y1564_ZERO) 
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         CLI_SET_ERR (CLI_Y1564_ERR_SAC_INVALID_FTD);
        return SNMP_FAILURE;
    }   

    pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                              u4FsY1564SacId,
                                              pu4ErrorCode);
    if (pSacEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of SAC to SLA 
     * and returns failure if the SLA test is in-progress.

     * configuration will not be allowed if the test is 
     * in-progress */

    if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
        OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SacFrDelayVar
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacFrDelayVar
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacFrDelayVar (UINT4 *pu4ErrorCode, 
                                    UINT4 u4FsY1564ContextId, 
                                    UINT4 u4FsY1564SacId, 
                                    INT4 i4TestValFsY1564SacFrDelayVar)
{
    tSacInfo *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;

    if (i4TestValFsY1564SacFrDelayVar < Y1564_ZERO) 
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         CLI_SET_ERR (CLI_Y1564_ERR_SAC_INVALID_FDV);
        return SNMP_FAILURE;
    }   

    pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                              u4FsY1564SacId,
                                              pu4ErrorCode);
    if (pSacEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of SAC to SLA 
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is 
     * in-progress */

    if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
        OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SacAvailability
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacAvailability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacAvailability (UINT4 *pu4ErrorCode, 
                                      UINT4 u4FsY1564ContextId, 
                                      UINT4 u4FsY1564SacId, 
                                      tSNMP_OCTET_STRING_TYPE *pTestValFsY1564SacAvailability)
{
    tSacInfo *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;
    UINT4     u4digitCnt = Y1564_ZERO;
    UINT4     u4Flag = OSIX_FALSE;
    FLT4      f4AvailValue = Y1564_ZERO;
    UINT1     u1Index = Y1564_ZERO;

   if (Y1564_OCTETSTRING_TO_FLOAT (pTestValFsY1564SacAvailability,
                                   f4AvailValue) == EOF)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Availability "
                           "conversion error \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((f4AvailValue <= (FLT4)Y1564_ZERO) ||
       ((f4AvailValue > (FLT4) Y1564_MAX_AVAIL_PERCENTAGE)))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s :Invalid Value for Availability \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_AVAILABILITYVALUE);
        return SNMP_FAILURE;
    }



    for (u1Index = Y1564_ZERO; u1Index < pTestValFsY1564SacAvailability->i4_Length; u1Index++)
    {
        if (!isdigit (pTestValFsY1564SacAvailability->pu1_OctetList[u1Index]))
        {
            if (pTestValFsY1564SacAvailability->pu1_OctetList[u1Index] == '.')
            {
                u4Flag = OSIX_TRUE;
                u4digitCnt = Y1564_ZERO;
                continue; 

            }
            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : Availability "
                               "value is not Float or Integer Values \r\n", 
                               __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_Y1564_ERR_INVALID_AVAILABILITYVALUE);
            return SNMP_FAILURE;
        }
        else 
        {
            u4digitCnt++;
            if ((u4digitCnt > Y1564_THREE) || 
                ((u4Flag == OSIX_TRUE) && (u4digitCnt > Y1564_TWO)))
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Availability "
                                   "value is not in acceptable format\r\n", 
                                   __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_INVALID_AVAILABILITYVALUE);
                return SNMP_FAILURE;
            }
        }
    }
  
    pSacEntry = Y1564LwUtilValidateCxtAndSac (u4FsY1564ContextId,
                                              u4FsY1564SacId,
                                              pu4ErrorCode);
    if (pSacEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndSac itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of SAC to SLA 
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is 
     * in-progress */

    if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
        OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }
    /* If the value is same as already configured value
       simply return */   
    if (pSacEntry->f4SacAvailability == f4AvailValue)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsY1564SacRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId

                The Object 
                testValFsY1564SacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564SacRowStatus (UINT4 *pu4ErrorCode, 
                                   UINT4 u4FsY1564ContextId, 
                                   UINT4 u4FsY1564SacId, 
                                   INT4 i4TestValFsY1564SacRowStatus)
{

    tSacInfo  *pSacEntry = NULL;
    UINT4     u4SlaId = Y1564_ZERO;
    UINT4     u4EntryCount = Y1564_ZERO;


    /* Check the system status. Traf Prof configuration will be allowed
     * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        return SNMP_FAILURE;
    }

    /* Check the range of SAC ID */
    if ((u4FsY1564SacId < Y1564_MIN_SAC_ID) || 
        (u4FsY1564SacId > Y1564_MAX_SAC_ID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check the Row Status value */
    if ((i4TestValFsY1564SacRowStatus < ACTIVE) ||
        (i4TestValFsY1564SacRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

     pSacEntry = Y1564UtilSacGetSacEntry (u4FsY1564ContextId, u4FsY1564SacId);

    /* Modifying or Activating the non-existing entry 
     * is not allowed when the entry is not present */
    if ((pSacEntry == NULL) &&
        ((i4TestValFsY1564SacRowStatus == NOT_IN_SERVICE) ||
        (i4TestValFsY1564SacRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s :Sac entry is not "
                           "present to make the Row status as "
                           "Not-In-Service or Active \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsY1564SacRowStatus)
    {
	    case CREATE_AND_WAIT:
		    if (pSacEntry != NULL)
		    {
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
					    Y1564_ALL_FAILURE_TRC," %s : SAC entry for Id "
					    "%d is already created \r\n", __FUNCTION__,
					    u4FsY1564SacId);
			    CLI_SET_ERR (CLI_Y1564_ERR_SAC_ID_ALREADY_CREATED);
			    return SNMP_FAILURE;
		    }

		    /* Count is fetched from RBTreeCount datastructure and it is
		     * compared with maximum Number of SAC entries that are allowed to create */
		    RBTreeCount (Y1564_SAC_TABLE(), &u4EntryCount);
		    if (u4EntryCount == MAX_Y1564_SAC_ENTRIES)
		    {
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC
					    | Y1564_MGMT_TRC, " %s : "
					    "Maximum number of allowed SACs reached \r\n",
					    __FUNCTION__);
			    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			    CLI_SET_ERR (CLI_Y1564_ERR_MAX_SAC_REACHED);
			    return SNMP_FAILURE;
		    }



            break;
        case DESTROY:

            if (pSacEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : SAC entry is "
                                   "not present for the given Sac Id \r\n",
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
                return SNMP_FAILURE;
            }
            /* This function check for association of SAC to SLA 
             * and returns failure if the SLA test is in-progress.
             * Deletion will not be allowed if the test is 
             * in-progress */

            if (Y1564UtilValidateSacAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564SacId) !=
                OSIX_SUCCESS) 
            {
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
           /* Row status can be Active only if all the below mandatory parameters are
            * associated with SAC.
            * Information Rate 
            * Frame-Loss-Ratio  - may be not required
            * Frame-Time-Delay  -  may be not required 
            * Frame-Delay-Variation -  may be not required 
            * Availability */

           /* If the parameters are not mapped to SAC then row status will
            * be maintained as Not-Ready.*/

           if (pSacEntry->u1RowStatus == NOT_READY)
           {
               Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                  Y1564_ALL_FAILURE_TRC, " %s : SAC entry for "
                                  "Id %d cannot move from Not-Ready to "
                                  "Not-In-Service \r\n", __FUNCTION__,
                                  u4FsY1564SacId);
               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_READY);
               return SNMP_FAILURE;
           }
           break;           
        default:
          *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
           return SNMP_FAILURE;
     }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564SacTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SacId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsY1564SacTable (UINT4 *pu4ErrorCode, 
                              tSnmpIndexList *pSnmpIndexList, 
                              tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564ServiceConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564ServiceConfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564ServiceConfTable (UINT4 u4FsY1564ContextId,
                                                      UINT4 u4FsY1564ServiceConfId)
{

#ifndef MEF_WANTED
    tServiceConfInfo *pServiceConfEntry = NULL;

    /* Inside this function the following are validated *
     * Service Config Identifier
     * Context Identifier
     * Service Config Table */

    pServiceConfEntry = Y1564UtilGetServConfEntry (u4FsY1564ContextId,
                                             u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Service Config Entry "
                           "is NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564ServiceConfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
nmhGetFirstIndexFsY1564ServiceConfTable (UINT4 *pu4FsY1564ContextId,
                                         UINT4 *pu4FsY1564ServiceConfId)
{

#ifndef MEF_WANTED

    tServiceConfInfo *pServiceConfEntry = NULL;

    pServiceConfEntry = Y1564UtilGetFirstNodeFromServConfTable();

    if (pServiceConfEntry != NULL)
    {
        *pu4FsY1564ContextId = pServiceConfEntry->u4ContextId;
        *pu4FsY1564ServiceConfId = pServiceConfEntry->u4ServiceConfId;
        return SNMP_SUCCESS;
    }

    Y1564_CONTEXT_TRC (*pu4FsY1564ContextId, Y1564_MGMT_TRC | Y1564_CTRL_TRC,
                       " %s : Service Config Entry is NULL \r\n",
                       __FUNCTION__);
#else

    UNUSED_PARAM(pu4FsY1564ContextId);
    UNUSED_PARAM(pu4FsY1564ServiceConfId);

#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564ServiceConfTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564ServiceConfId
                nextFsY1564ServiceConfId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1 
nmhGetNextIndexFsY1564ServiceConfTable (UINT4 u4FsY1564ContextId,
                                        UINT4 *pu4NextFsY1564ContextId,
                                        UINT4 u4FsY1564ServiceConfId,
                                        UINT4 *pu4NextFsY1564ServiceConfId )
{

#ifndef MEF_WANTED

    tServiceConfInfo      *pNextServiceConfEntry = NULL;
    tServiceConfInfo       ServiceConfEntry;

    MEMSET (&ServiceConfEntry, 0, sizeof (tServiceConfInfo));

    ServiceConfEntry.u4ContextId = u4FsY1564ContextId;
    ServiceConfEntry.u4ServiceConfId = u4FsY1564ServiceConfId;

    pNextServiceConfEntry = Y1564UtilGetNextNodeFromServConfTable (&ServiceConfEntry);

    if (pNextServiceConfEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextServiceConfEntry->u4ContextId;
        *pu4NextFsY1564ServiceConfId = pNextServiceConfEntry->u4ServiceConfId;
        return SNMP_SUCCESS;
    }

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(pu4NextFsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pu4NextFsY1564ServiceConfId);

#endif

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfColorMode
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfColorMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsY1564ServiceConfColorMode (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564ServiceConfId,
                                   INT4 *pi4RetValFsY1564ServiceConfColorMode)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564ServiceConfColorMode = (INT4)
        pServiceConfEntry->u1ServiceConfColorMode;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pi4RetValFsY1564ServiceConfColorMode);

#endif

     return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfCoupFlag
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfCoupFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsY1564ServiceConfCoupFlag (UINT4 u4FsY1564ContextId,
                                  UINT4 u4FsY1564ServiceConfId,
                                  INT4 *pi4RetValFsY1564ServiceConfCoupFlag)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry (u4FsY1564ContextId,
                                                   u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564ServiceConfCoupFlag = (INT4)
        pServiceConfEntry->u1ServiceConfCoupFlag;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pi4RetValFsY1564ServiceConfCoupFlag);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfCIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfCIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsY1564ServiceConfCIR (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 *pu4RetValFsY1564ServiceConfCIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry(u4FsY1564ContextId,
                                                  u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ServiceConfCIR = pServiceConfEntry->u4ServiceConfCIR;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pu4RetValFsY1564ServiceConfCIR);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfCBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfCBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhGetFsY1564ServiceConfCBS (UINT4 u4FsY1564ContextId,
                                  UINT4 u4FsY1564ServiceConfId,
                                  UINT4 *pu4RetValFsY1564ServiceConfCBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry(u4FsY1564ContextId,
                                                  u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ServiceConfCBS = pServiceConfEntry->u4ServiceConfCBS;


#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pu4RetValFsY1564ServiceConfCBS);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfEIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfEIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsY1564ServiceConfEIR (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 *pu4RetValFsY1564ServiceConfEIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry(u4FsY1564ContextId,
                                                  u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ServiceConfEIR = pServiceConfEntry->u4ServiceConfEIR;
#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pu4RetValFsY1564ServiceConfEIR);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfEBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfEBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsY1564ServiceConfEBS (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 *pu4RetValFsY1564ServiceConfEBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry(u4FsY1564ContextId,
                                                  u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ServiceConfEBS = pServiceConfEntry->u4ServiceConfEBS;
#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pu4RetValFsY1564ServiceConfEBS);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsY1564ServiceConfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                retValFsY1564ServiceConfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhGetFsY1564ServiceConfRowStatus (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564ServiceConfId,
                                   INT4 *pi4RetValFsY1564ServiceConfRowStatus)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry(u4FsY1564ContextId,
                                                  u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564ServiceConfRowStatus = (UINT4) pServiceConfEntry->
        u1RowStatus;
#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(pi4RetValFsY1564ServiceConfRowStatus);

#endif

return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfColorMode
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfColorMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfColorMode (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564ServiceConfId,
                                   INT4 i4SetValFsY1564ServiceConfColorMode)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Information rate to the SAC */
    pServiceConfEntry->u1ServiceConfColorMode = (UINT1)
        i4SetValFsY1564ServiceConfColorMode;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4SetValFsY1564ServiceConfColorMode);

#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfCoupFlag
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfCoupFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfCoupFlag (UINT4 u4FsY1564ContextId,
                                  UINT4 u4FsY1564ServiceConfId,
                                  INT4 i4SetValFsY1564ServiceConfCoupFlag)
{
#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)                                                                                {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Information rate to the SAC */
    pServiceConfEntry->u1ServiceConfCoupFlag = (UINT1)
        i4SetValFsY1564ServiceConfCoupFlag;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4SetValFsY1564ServiceConfCoupFlag);

#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfCIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfCIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfCIR (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 u4SetValFsY1564ServiceConfCIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)                                                                               
    {

        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Set the Information rate to the SAC */
    pServiceConfEntry->u4ServiceConfCIR =  u4SetValFsY1564ServiceConfCIR;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4SetValFsY1564ServiceConfCIR);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfCBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfCBS (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 u4SetValFsY1564ServiceConfCBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)                                                                                   
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    pServiceConfEntry->u4ServiceConfCBS = u4SetValFsY1564ServiceConfCBS;


#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4SetValFsY1564ServiceConfCBS);

#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfEIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfEIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfEIR (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 u4SetValFsY1564ServiceConfEIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)  
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }
    pServiceConfEntry->u4ServiceConfEIR =  u4SetValFsY1564ServiceConfEIR;

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4SetValFsY1564ServiceConfEIR);

#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfEBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfEBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsY1564ServiceConfEBS (UINT4 u4FsY1564ContextId,
                             UINT4 u4FsY1564ServiceConfId,
                             UINT4 u4SetValFsY1564ServiceConfEBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;

    /* Inside this function Context Id and ServiceConfInfoEntry are validated */
    pServiceConfEntry = Y1564UtilGetServConfEntry
        (u4FsY1564ContextId, u4FsY1564ServiceConfId);

    if (pServiceConfEntry == NULL)                                                                                    
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |                                                   
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConfInfo Entry not present for %d\r\n",
                           __FUNCTION__,u4FsY1564ServiceConfId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        return SNMP_FAILURE;
    }

    pServiceConfEntry->u4ServiceConfEBS = u4SetValFsY1564ServiceConfEBS;

    Y1564LwUtilServiceConfInfoRowStatusNotInServ (pServiceConfEntry);

#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4SetValFsY1564ServiceConfEBS);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsY1564ServiceConfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                setValFsY1564ServiceConfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhSetFsY1564ServiceConfRowStatus (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564ServiceConfId,
                                   INT4 i4SetValFsY1564ServiceConfRowStatus)
{

#ifndef MEF_WANTED

    tServiceConfInfo          *pServiceConfEntry = NULL;
    tServiceConfInfo           ServiceConfEntry;

    MEMSET(&ServiceConfEntry ,Y1564_ZERO, sizeof (tServiceConfInfo));

    if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsY1564ServiceConfRowStatus)
    {
        case CREATE_AND_WAIT:

            pServiceConfEntry = Y1564UtilCreateServConfNode();

            if (pServiceConfEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Failed to create "
                                   "ServiceConf Entry for ID  %d\r\n",
                                   __FUNCTION__,u4FsY1564ServiceConfId);
                return SNMP_FAILURE;
            }
            /* Update ServiceConfId and Context Id to Service Conf
             * Structure */
            pServiceConfEntry->u4ServiceConfId = u4FsY1564ServiceConfId;
            pServiceConfEntry->u4ContextId = u4FsY1564ContextId;

            if (Y1564UtilAddNodeToServConfRBTree (pServiceConfEntry)
                != OSIX_SUCCESS)
            {
                /* Release memory allocated for ServiceConf Entry */
                MemReleaseMemBlock (Y1564_SERV_CONF_POOL (), (UINT1 *) pServiceConfEntry);
                pServiceConfEntry = NULL;
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC," %s : ServiceConf Entry "
                                   "add failed for Id %d\r\n",
                                   __FUNCTION__,u4FsY1564ServiceConfId);
                return SNMP_FAILURE;
            }
            Y1564_CONTEXT_TRC (u4FsY1564ContextId,Y1564_CTRL_TRC,
                        "nmhSetFsY1564ServiceConfRowStatus : ServiceConf "
                        "Entry Added to RBTree with Id %d \r\n",
                        u4FsY1564ServiceConfId);
            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set */

            pServiceConfEntry->u1RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:
            ServiceConfEntry.u4ServiceConfId = u4FsY1564ServiceConfId;
            ServiceConfEntry.u4ContextId = u4FsY1564ContextId;

            pServiceConfEntry = Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry);

            if (pServiceConfEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified "
                                   "ServiceConf Entry is not present \r\n",
                                   __FUNCTION__);
                return SNMP_FAILURE;
            }
            pServiceConfEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            ServiceConfEntry.u4ServiceConfId = u4FsY1564ServiceConfId;
            ServiceConfEntry.u4ContextId = u4FsY1564ContextId;

            pServiceConfEntry = Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry);

            if (pServiceConfEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified "
                                   "ServiceConf Entry is not present \r\n",
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (Y1564UtilServConfDelNodeFromRBTree (pServiceConfEntry)
                != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC,
                                   " %s :ServiceConf entry delete failed for"
                                   "ServiceConf Id %d \r\n",
                                   __FUNCTION__,u4FsY1564ServiceConfId);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:

            ServiceConfEntry.u4ServiceConfId = u4FsY1564ServiceConfId;
            ServiceConfEntry.u4ContextId = u4FsY1564ContextId;

            pServiceConfEntry = Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry);

            if (pServiceConfEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified "
                                   "ServiceConf Entry is not present \r\n",
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (pServiceConfEntry->u1RowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pServiceConfEntry->u1RowStatus = ACTIVE;
            break;

        default:
            return SNMP_FAILURE;
    }
#else

    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4SetValFsY1564ServiceConfRowStatus);

#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfColorMode
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfColorMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfColorMode (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsY1564ContextId,
                                      UINT4 u4FsY1564ServiceConfId,
                                      INT4  i4TestValFsY1564ServiceConfColorMode)
{
#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((i4TestValFsY1564ServiceConfColorMode != Y1564_SERV_CONF_COLOR_AWARE) &&
        (i4TestValFsY1564ServiceConfColorMode != Y1564_SERV_CONF_COLOR_BLIND))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_COLOR_MODE);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf
        (u4FsY1564ContextId,
         u4FsY1564ServiceConfId,
         pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4TestValFsY1564ServiceConfColorMode);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfCoupFlag
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfCoupFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfCoupFlag (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsY1564ContextId,
                                     UINT4 u4FsY1564ServiceConfId,
                                     INT4  i4TestValFsY1564ServiceConfCoupFlag)
{
#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((i4TestValFsY1564ServiceConfCoupFlag != Y1564_COUPLING_TYPE_DECOUPLED) &&
        (i4TestValFsY1564ServiceConfCoupFlag != Y1564_COUPLING_TYPE_COUPLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_COUP_FLAG);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                           u4FsY1564ServiceConfId,
                                                           pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4TestValFsY1564ServiceConfCoupFlag);

#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfCIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfCIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfCIR (UINT4 *pu4ErrorCode,
                                UINT4 u4FsY1564ContextId,
                                UINT4 u4FsY1564ServiceConfId,
                                UINT4 u4TestValFsY1564ServiceConfCIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((u4TestValFsY1564ServiceConfCIR <= Y1564_CIR_MIN_VAL) ||
        (u4TestValFsY1564ServiceConfCIR > Y1564_CIR_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_CIR_RATE);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                           u4FsY1564ServiceConfId,
                                                           pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4TestValFsY1564ServiceConfCIR);

#endif

    return SNMP_SUCCESS;
}

/***************************************************************************v
 Function    :  nmhTestv2FsY1564ServiceConfCBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfCBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfCBS (UINT4 *pu4ErrorCode,
                                UINT4 u4FsY1564ContextId,
                                UINT4 u4FsY1564ServiceConfId,
                                UINT4 u4TestValFsY1564ServiceConfCBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((u4TestValFsY1564ServiceConfCBS <= Y1564_CBS_MIN_VAL) ||
        (u4TestValFsY1564ServiceConfCBS > Y1564_CBS_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_CBS_RATE);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                           u4FsY1564ServiceConfId,
                                                           pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4TestValFsY1564ServiceConfCBS);

#endif


    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfEIR
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfEIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfEIR (UINT4 *pu4ErrorCode,
                                UINT4 u4FsY1564ContextId,
                                UINT4 u4FsY1564ServiceConfId,
                                UINT4 u4TestValFsY1564ServiceConfEIR)
{

#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((u4TestValFsY1564ServiceConfEIR <= Y1564_EIR_MIN_VAL) ||
        (u4TestValFsY1564ServiceConfEIR > Y1564_EIR_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_EIR_RATE);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                           u4FsY1564ServiceConfId,
                                                           pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pServiceConfEntry->u4ServiceConfCIR > u4TestValFsY1564ServiceConfEIR)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_CIR_GREATER_THAN_EIR_VALUE);
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4TestValFsY1564ServiceConfEIR);

#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfEBS
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfEBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 
nmhTestv2FsY1564ServiceConfEBS (UINT4 *pu4ErrorCode,
                                UINT4 u4FsY1564ContextId,
                                UINT4 u4FsY1564ServiceConfId,
                                UINT4 u4TestValFsY1564ServiceConfEBS)
{

#ifndef MEF_WANTED

    tServiceConfInfo    *pServiceConfEntry = NULL;
    UINT4               u4SlaId = Y1564_ZERO;

    if ((u4TestValFsY1564ServiceConfEBS <= Y1564_EBS_MIN_VAL) ||
        (u4TestValFsY1564ServiceConfEBS > Y1564_EBS_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_EBS_RATE);
        return SNMP_FAILURE;
    }

    pServiceConfEntry = Y1564LwUtilValidateCxtAndServConf (u4FsY1564ContextId,
                                                           u4FsY1564ServiceConfId,
                                                           pu4ErrorCode);
    if (pServiceConfEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndServConf itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* This function check for association of Service Conf to SLA
     * and returns failure if the SLA test is in-progress.
     * configuration will not be allowed if the test is
     * in-progress */

    if (Y1564UtilValidateServConfAndSla (u4FsY1564ContextId, u4SlaId, u4FsY1564ServiceConfId) !=
        OSIX_SUCCESS)
    { 
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(u4TestValFsY1564ServiceConfEBS);

#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsY1564ServiceConfRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId

                The Object 
                testValFsY1564ServiceConfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsY1564ServiceConfRowStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsY1564ContextId,
                                           UINT4 u4FsY1564ServiceConfId,
                                           INT4 i4TestValFsY1564ServiceConfRowStatus)
{

#ifndef MEF_WANTED

    tServiceConfInfo  *pServiceConfEntry = NULL;
    tServiceConfInfo   ServiceConfEntry;
    UINT4              u4SlaId = Y1564_ZERO;
    UINT4             u4EntryCount = Y1564_ZERO;


    /* Check the system status. Traf Prof configuration will be allowed
     * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check the range of SAC ID */
    if ((u4FsY1564ServiceConfId < Y1564_MIN_SERVICE_CONF_ID) ||
        (u4FsY1564ServiceConfId > Y1564_MAX_SERVICE_CONF_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR(CLI_Y1564_ERR_INVALID_SERVICE_CONF_ID);
        return SNMP_FAILURE;
    }

    /* Check the Row Status value */
    if ((i4TestValFsY1564ServiceConfRowStatus < ACTIVE) ||
        (i4TestValFsY1564ServiceConfRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ServiceConfEntry.u4ContextId = u4FsY1564ContextId;
    ServiceConfEntry.u4ServiceConfId = u4FsY1564ServiceConfId;

    pServiceConfEntry = Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry);

    /* Modifying or Activating the non-existing entry
     * is not allowed when the entry is not present */
    if ((pServiceConfEntry == NULL) &&
        ((i4TestValFsY1564ServiceConfRowStatus == NOT_IN_SERVICE) ||
        (i4TestValFsY1564ServiceConfRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ServiceConf entry is not present to make the "
                           "Row status as Not-In-Service or Active \r\n",
                           __FUNCTION__);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsY1564ServiceConfRowStatus)
    {
	    case CREATE_AND_WAIT:
		    if (pServiceConfEntry != NULL)
		    {
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
					    Y1564_ALL_FAILURE_TRC,
					    " %s : Service Config entry for Id %d  "
					    "is already create  \r\n",
					    __FUNCTION__,u4FsY1564ServiceConfId);
			    CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_ALREADY_CREATED);
			    return SNMP_FAILURE;
		    }
		    /* Count is fetched from RBTreeCount datastructure and it is
		     * compared with maximum Number of Service Config entries that are allowed to create */
		    RBTreeCount (Y1564_SERV_CONF_TABLE(), &u4EntryCount);
		    if (u4EntryCount == MAX_Y1564_SERV_CONF_ENTRIES)
		    {
			    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC
					    | Y1564_MGMT_TRC, " %s : Maximum number of allowed "
                   "Service Config Entries reached \r\n",
					    __FUNCTION__);
			    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			    CLI_SET_ERR (CLI_Y1564_ERR_MAX_SERV_CONF_REACHED);
			    return SNMP_FAILURE;
		    }


            break;
        case DESTROY:

            if (pServiceConfEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Service Config "
                                   "entry is not present for the given "
                                   "ServiceConfig Id \r\n",__FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
                return SNMP_FAILURE;
            }
            /* This function check for association of SAC to SLA
             * and returns failure if the SLA test is in-progress.

             * Deletion will not be allowed if the test is
             * in-progress */

            if (Y1564LwUtilValidateServConfAndSla (u4SlaId, u4FsY1564ContextId,
                                                      u4FsY1564ServiceConfId) !=
                SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            /* Row status can be Active only if all the below mandatory
             * parameters are associated with SAC.
             * CIR Rate
             * CBS
             * EIR Rate
             * EBS*/

            /* If the parameters are not mapped to Service Configuration
             * then row status will be set as Not-Ready.*/

            if (pServiceConfEntry->u1RowStatus == NOT_READY)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC,
                                   " %s : Service Config entry for Id %d "
                                   "cannot move from Not-Ready to Not-In-Service \r\n",
                                   __FUNCTION__,u4FsY1564ServiceConfId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONFIG_NOT_READY);
                return SNMP_FAILURE;

            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

#else

    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(u4FsY1564ContextId);
    UNUSED_PARAM(u4FsY1564ServiceConfId);
    UNUSED_PARAM(i4TestValFsY1564ServiceConfRowStatus);

#endif

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564ServiceConfTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564ServiceConfId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsY1564ServiceConfTable (UINT4 *pu4ErrorCode, 
                                 tSnmpIndexList *pSnmpIndexList, 
                                 tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564ConfigTestReportTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564ConfigTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564ConfigTestReportTable (UINT4 u4FsY1564ContextId, 
                                                           UINT4 u4FsY1564SlaId, 
                                                           INT4 i4FsY1564ConfigTestReportFrSize,
                                                           INT4 i4FsY1564SlaCurrentTestMode, 
                                                           INT4 i4FsY1564ConfigTestReportStepId)
{
    tConfTestReportInfo  *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */

    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId, 
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode, 
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_GLOBAL_TRC ( "ConfTestReport entry is NULL \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564ConfigTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564ConfigTestReportFrSize
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsY1564ConfigTestReportTable (UINT4 *pu4FsY1564ContextId,
                                              UINT4
                                              *pu4FsY1564SlaId,
                                              INT4
                                              *pi4FsY1564ConfigTestReportFrSize,
                                              INT4
                                              *pi4FsY1564SlaCurrentTestMode,
                                              INT4
                                              *pi4FsY1564ConfigTestReportStepId)
{
    tConfTestReportInfo *pConfTestReportEntry = NULL;

    pConfTestReportEntry = Y1564UtilGetFirstNodeFromConfTestReportTable();

    if (pConfTestReportEntry != NULL)
    {
        *pu4FsY1564ContextId = pConfTestReportEntry->u4ContextId;
        *pu4FsY1564SlaId = pConfTestReportEntry->u4SlaId;
        *pi4FsY1564ConfigTestReportFrSize = pConfTestReportEntry->i4FrameSize;
        *pi4FsY1564SlaCurrentTestMode = (INT4) pConfTestReportEntry->u4CurrentTestMode;
        *pi4FsY1564ConfigTestReportStepId =  (INT4) pConfTestReportEntry->u4StatsStepId;
        return SNMP_SUCCESS;
    }

    Y1564_GLOBAL_TRC ( "Conf test report Entry is NULL \r\n");

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564ConfigTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564SlaId
                nextFsY1564SlaId
                FsY1564SlaCurrentTestMode
                nextFsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                nextFsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize
                nextFsY1564ConfigTestReportFrSize
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564ConfigTestReportTable (UINT4 u4FsY1564ContextId,
                                                  UINT4 *pu4NextFsY1564ContextId, 
                                                  UINT4 u4FsY1564SlaId,
                                                  UINT4 *pu4NextFsY1564SlaId, 
                                                  INT4 i4FsY1564ConfigTestReportFrSize,
                                                  INT4 *pi4NextFsY1564ConfigTestReportFrSize,
                                                  INT4 i4FsY1564SlaCurrentTestMode,
                                                  INT4 *pi4NextFsY1564SlaCurrentTestMode, 
                                                  INT4 i4FsY1564ConfigTestReportStepId,
                                                  INT4 *pi4NextFsY1564ConfigTestReportStepId)
{
    tConfTestReportInfo      *pNextConfTestReportEntry = NULL;
    tConfTestReportInfo       ConfTestReportEntry;

    MEMSET (&ConfTestReportEntry, 0, sizeof (tConfTestReportInfo));

    ConfTestReportEntry.u4ContextId = u4FsY1564ContextId;
    ConfTestReportEntry.u4SlaId = u4FsY1564SlaId;
    ConfTestReportEntry.i4FrameSize = i4FsY1564ConfigTestReportFrSize;
    ConfTestReportEntry.u4CurrentTestMode = (UINT4) i4FsY1564SlaCurrentTestMode;
    ConfTestReportEntry.u4StatsStepId = (UINT4) i4FsY1564ConfigTestReportStepId;

    pNextConfTestReportEntry = Y1564UtilGetNextNodeFromConfTestReportTable 
                                              (&ConfTestReportEntry);

    if (pNextConfTestReportEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextConfTestReportEntry->u4ContextId;
        *pu4NextFsY1564SlaId = pNextConfTestReportEntry->u4SlaId;
        *pi4NextFsY1564ConfigTestReportFrSize = pNextConfTestReportEntry->i4FrameSize;
        *pi4NextFsY1564SlaCurrentTestMode = (INT4) pNextConfTestReportEntry->u4CurrentTestMode;
        *pi4NextFsY1564ConfigTestReportStepId =  (INT4) pNextConfTestReportEntry->u4StatsStepId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportResult
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsY1564ConfigTestReportResult (UINT4 u4FsY1564ContextId,
                                     UINT4 u4FsY1564SlaId,
                                     INT4 i4FsY1564ConfigTestReportFrSize,
                                     INT4 i4FsY1564SlaCurrentTestMode,
                                     INT4 i4FsY1564ConfigTestReportStepId,
                                     INT4
                                     *pi4RetValFsY1564ConfigTestReportResult)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for "
                           "Sla %d Step Id %d \r\n", __FUNCTION__,
                           u4FsY1564SlaId, i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564ConfigTestReportResult = (INT4) 
                                      pConfTestReportEntry->u1StatsResult;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportIrMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportIrMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportIrMin (UINT4 u4FsY1564ContextId, 
                                         UINT4 u4FsY1564SlaId, 
                                         INT4 i4FsY1564ConfigTestReportFrSize,
                                         INT4 i4FsY1564SlaCurrentTestMode, 
                                         INT4 i4FsY1564ConfigTestReportStepId, 
                                         UINT4 *pu4RetValFsY1564ConfigTestReportIrMin)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportIrMin = pConfTestReportEntry->u4StatsIrMin;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportIrMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportIrMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportIrMean (UINT4 u4FsY1564ContextId, 
                                          UINT4 u4FsY1564SlaId, 
                                          INT4 i4FsY1564ConfigTestReportFrSize,
                                          INT4 i4FsY1564SlaCurrentTestMode, 
                                          INT4 i4FsY1564ConfigTestReportStepId, 
                                          UINT4 *pu4RetValFsY1564ConfigTestReportIrMean)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportIrMean = pConfTestReportEntry->u4StatsIrMean;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportIrMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportIrMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportIrMax (UINT4 u4FsY1564ContextId, 
                                         UINT4 u4FsY1564SlaId, 
                                         INT4 i4FsY1564ConfigTestReportFrSize,
                                         INT4 i4FsY1564SlaCurrentTestMode, 
                                         INT4 i4FsY1564ConfigTestReportStepId,
                                         UINT4 *pu4RetValFsY1564ConfigTestReportIrMax)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportIrMax = pConfTestReportEntry->u4StatsIrMax;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrLossCnt
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrLossCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrLossCnt (UINT4 u4FsY1564ContextId, 
                                             UINT4 u4FsY1564SlaId, 
                                             INT4 i4FsY1564ConfigTestReportFrSize,
                                             INT4 i4FsY1564SlaCurrentTestMode, 
                                             INT4 i4FsY1564ConfigTestReportStepId, 
                                             UINT4 *pu4RetValFsY1564ConfigTestReportFrLossCnt)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrLossCnt = 
                      pConfTestReportEntry->u4StatsFrLossCnt;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrLossRatio
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrLossRatio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrLossRatio (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               INT4 i4FsY1564ConfigTestReportFrSize,
                                               INT4 i4FsY1564SlaCurrentTestMode, 
                                               INT4 i4FsY1564ConfigTestReportStepId, 
                                               UINT4 *pu4RetValFsY1564ConfigTestReportFrLossRatio)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrLossRatio = 
                        pConfTestReportEntry->u4StatsFrLossRatio;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrTxDelayMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrTxDelayMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrTxDelayMin (UINT4 u4FsY1564ContextId, 
                                                UINT4 u4FsY1564SlaId, 
                                                INT4 i4FsY1564ConfigTestReportFrSize,
                                                INT4 i4FsY1564SlaCurrentTestMode, 
                                                INT4 i4FsY1564ConfigTestReportStepId, 
                                                UINT4 *pu4RetValFsY1564ConfigTestReportFrTxDelayMin)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrTxDelayMin = 
                             pConfTestReportEntry->u4StatsFrTxDelayMin;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrTxDelayMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrTxDelayMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrTxDelayMean (UINT4 u4FsY1564ContextId, 
                                                 UINT4 u4FsY1564SlaId, 
                                                 INT4 i4FsY1564ConfigTestReportFrSize,
                                                 INT4 i4FsY1564SlaCurrentTestMode, 
                                                 INT4 i4FsY1564ConfigTestReportStepId, 
                                                 UINT4 *pu4RetValFsY1564ConfigTestReportFrTxDelayMean)
{

    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrTxDelayMean = 
                            pConfTestReportEntry->u4StatsFrTxDelayMean;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrTxDelayMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrTxDelayMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrTxDelayMax (UINT4 u4FsY1564ContextId, 
                                                UINT4 u4FsY1564SlaId, 
                                                INT4 i4FsY1564ConfigTestReportFrSize,
                                                INT4 i4FsY1564SlaCurrentTestMode, 
                                                INT4 i4FsY1564ConfigTestReportStepId, 
                                                UINT4 *pu4RetValFsY1564ConfigTestReportFrTxDelayMax)
{

    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrTxDelayMax = 
                             pConfTestReportEntry->u4StatsFrTxDelayMax;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrDelayVarMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrDelayVarMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrDelayVarMin (UINT4 u4FsY1564ContextId, 
                                                 UINT4 u4FsY1564SlaId, 
                                                 INT4 i4FsY1564ConfigTestReportFrSize,
                                                 INT4 i4FsY1564SlaCurrentTestMode, 
                                                 INT4 i4FsY1564ConfigTestReportStepId, 
                                                 UINT4 *pu4RetValFsY1564ConfigTestReportFrDelayVarMin)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrDelayVarMin = 
                            pConfTestReportEntry->u4StatsFrDelayVarMin;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrDelayVarMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrDelayVarMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrDelayVarMean (UINT4 u4FsY1564ContextId, 
                                                  UINT4 u4FsY1564SlaId, 
                                                  INT4 i4FsY1564ConfigTestReportFrSize,
                                                  INT4 i4FsY1564SlaCurrentTestMode, 
                                                  INT4 i4FsY1564ConfigTestReportStepId, 
                                                  UINT4 *pu4RetValFsY1564ConfigTestReportFrDelayVarMean)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrDelayVarMean = 
                                pConfTestReportEntry->u4StatsFrDelayVarMean;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportFrDelayVarMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportFrDelayVarMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportFrDelayVarMax (UINT4 u4FsY1564ContextId, 
                                                 UINT4 u4FsY1564SlaId, 
                                                 INT4 i4FsY1564ConfigTestReportFrSize,
                                                 INT4 i4FsY1564SlaCurrentTestMode, 
                                                 INT4 i4FsY1564ConfigTestReportStepId, 
                                                 UINT4 *pu4RetValFsY1564ConfigTestReportFrDelayVarMax)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportFrDelayVarMax =
                                 pConfTestReportEntry->u4StatsFrDelayVarMax;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportTestStartTime
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportTestStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportTestStartTime (UINT4 u4FsY1564ContextId, 
                                                 UINT4 u4FsY1564SlaId, 
                                                 INT4 i4FsY1564ConfigTestReportFrSize,
                                                 INT4 i4FsY1564SlaCurrentTestMode, 
                                                 INT4 i4FsY1564ConfigTestReportStepId, 
                                                 UINT4 * 
                                                 pu4RetValFsY1564ConfigTestReportTestStartTime)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportTestStartTime =
        pConfTestReportEntry->TestStartTimeStamp;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564ConfigTestReportTestEndTime
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564SlaCurrentTestMode
                FsY1564ConfigTestReportStepId
                FsY1564ConfigTestReportFrSize

                The Object 
                retValFsY1564ConfigTestReportTestEndTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564ConfigTestReportTestEndTime (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               INT4 i4FsY1564ConfigTestReportFrSize,
                                               INT4 i4FsY1564SlaCurrentTestMode, 
                                               INT4 i4FsY1564ConfigTestReportStepId, 
                                               UINT4 * 
                                               pu4RetValFsY1564ConfigTestReportTestEndTime)
{
    tConfTestReportInfo          *pConfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Current Test Mode
     * Rate Step
     * SLA Table
     * ConfigTestReportTable */
    pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            i4FsY1564ConfigTestReportFrSize,
                                                            (UINT4) i4FsY1564SlaCurrentTestMode,
                                                            (UINT4) i4FsY1564ConfigTestReportStepId);
    if (pConfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,
                           " %s : ConfTestReport entry not present for Sla %d "
                           " Step Id %d \r\n",__FUNCTION__, u4FsY1564SlaId,
                           i4FsY1564ConfigTestReportStepId);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564ConfigTestReportTestEndTime =
        pConfTestReportEntry->TestEndTimeStamp;

    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564PerformanceTestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564PerformanceTestTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564PerformanceTestTable (UINT4 u4FsY1564ContextId,
                                                          UINT4 u4FsY1564PerformanceTestIndex)
{

    tPerfTestInfo *pPerfTestEntry = NULL;

    /* Inside this function the following are validated *
     * PerfTest Identifier
     * Context Identifier
     * PerfTestTable */

    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId, 
                                                        u4FsY1564PerformanceTestIndex);

    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry is "
                           "NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564PerformanceTestTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsY1564PerformanceTestTable (UINT4 *pu4FsY1564ContextId, 
                                                  UINT4 *pu4FsY1564PerformanceTestIndex)
{
    tPerfTestInfo *pPerfTestEntry = NULL;

    pPerfTestEntry = Y1564UtilGetFirstNodeFromPerfTestTable();

    if (pPerfTestEntry != NULL)
    {
        *pu4FsY1564ContextId = pPerfTestEntry->u4ContextId;
        *pu4FsY1564PerformanceTestIndex = pPerfTestEntry->u4PerfTestId;
        return SNMP_SUCCESS;
    }

    Y1564_GLOBAL_TRC ("nmhGetFirstIndexFsY1564PerformanceTestTable : "
                      "Perf Test Entry is NULL  \r\n");
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564PerformanceTestTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564PerformanceTestIndex
                nextFsY1564PerformanceTestIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564PerformanceTestTable (UINT4 u4FsY1564ContextId,
                                                 UINT4 *pu4NextFsY1564ContextId, 
                                                 UINT4 u4FsY1564PerformanceTestIndex,
                                                 UINT4 *pu4NextFsY1564PerformanceTestIndex )
{
    tPerfTestInfo      *pNextPerfTestEntry = NULL;
    tPerfTestInfo       PerfTestEntry;

    MEMSET (&PerfTestEntry, 0, sizeof (tPerfTestInfo));

    PerfTestEntry.u4ContextId = u4FsY1564ContextId;
    PerfTestEntry.u4PerfTestId = u4FsY1564PerformanceTestIndex;

    pNextPerfTestEntry = Y1564UtilGetNextNodeFromPerfTestTable (&PerfTestEntry);

    if (pNextPerfTestEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextPerfTestEntry->u4ContextId;
        *pu4NextFsY1564PerformanceTestIndex = pNextPerfTestEntry->u4PerfTestId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564PerformanceTestSlaList
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                retValFsY1564PerformanceTestSlaList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerformanceTestSlaList (UINT4 u4FsY1564ContextId,
                                          UINT4 u4FsY1564PerformanceTestIndex, 
                                          tSNMP_OCTET_STRING_TYPE * 
                                          pRetValFsY1564PerformanceTestSlaList)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId, 
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry not "
                           "present for %d \r\n",
                            __FUNCTION__,u4FsY1564PerformanceTestIndex);
        return SNMP_FAILURE;
    }

    if ((pPerfTestEntry->u2SlaListCount) != Y1564_ZERO)
    {
        MEMCPY (pRetValFsY1564PerformanceTestSlaList->pu1_OctetList, 
                pPerfTestEntry->pau1SlaList,Y1564_MAX_SLA_LIST);
        pRetValFsY1564PerformanceTestSlaList->i4_Length = Y1564_MAX_SLA_LIST;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerformanceTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                retValFsY1564PerformanceTestDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerformanceTestDuration (UINT4 u4FsY1564ContextId, 
                                           UINT4 u4FsY1564PerformanceTestIndex, 
                                           INT4 *pi4RetValFsY1564PerformanceTestDuration)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId, 
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry not "
                           "present for %d \r\n", __FUNCTION__,
                           u4FsY1564PerformanceTestIndex);
        return SNMP_FAILURE;
    }

    if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
    {
        *pi4RetValFsY1564PerformanceTestDuration = (INT4) Y1564_PERF_TEST_MIN_DURATION;
    }
    if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_2HOUR)
    {
        *pi4RetValFsY1564PerformanceTestDuration = (INT4) Y1564_PERF_TEST_DEF_DURATION;
    }
    if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_24HOUR)
    {
        *pi4RetValFsY1564PerformanceTestDuration = (INT4) Y1564_PERF_TEST_MAX_DURATION;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerformanceTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                retValFsY1564PerformanceTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerformanceTestStatus (UINT4 u4FsY1564ContextId, 
                                         UINT4 u4FsY1564PerformanceTestIndex, 
                                         INT4 *pi4RetValFsY1564PerformanceTestStatus)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry not "
                           "present for %d \r\n", __FUNCTION__,
                           u4FsY1564PerformanceTestIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsY1564PerformanceTestStatus = (INT4) 
                                    pPerfTestEntry->u1PerfTestStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerformanceTestRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                retValFsY1564PerformanceTestRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerformanceTestRowStatus (UINT4 u4FsY1564ContextId, 
                                            UINT4 u4FsY1564PerformanceTestIndex, 
                                            INT4 *pi4RetValFsY1564PerformanceTestRowStatus)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsY1564PerformanceTestRowStatus = (INT4)
                                                  pPerfTestEntry->u1RowStatus;
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsY1564PerformanceTestSlaList
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                setValFsY1564PerformanceTestSlaList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564PerformanceTestSlaList (UINT4 u4FsY1564ContextId, 
                                          UINT4 u4FsY1564PerformanceTestIndex, 
                                          tSNMP_OCTET_STRING_TYPE * 
                                          pSetValFsY1564PerformanceTestSlaList)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;
    UINT2                  u2Index = Y1564_ONE;
    BOOL1                  bResult = Y1564_ZERO;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf Test Entry not "
                           "present for %d \r\n",__FUNCTION__,
                           u4FsY1564PerformanceTestIndex);
        CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
        return SNMP_FAILURE;
    }
    
    MEMCPY (pPerfTestEntry->pau1SlaList,
            pSetValFsY1564PerformanceTestSlaList->pu1_OctetList,
            pSetValFsY1564PerformanceTestSlaList->i4_Length);

   /* To traverse the whole 16 bits the value of 
    * Y1564_MAX_SLA_LIST is multiplied with 8*/

   for (; u2Index <= Y1564_MAX_SLA_LIST * Y1564_BITS_PER_BYTE; u2Index++)
   {
        /* Scanning the SLA list bitmap to identify the SLA id's mapped to
         * Perf Table */
        OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)), u2Index,
                                 sizeof (tY1564SlaList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue */
            continue;
        }
        /* If the bit is set, set the u2Index value to
         * u2SlaListCount. */
        pPerfTestEntry->u2SlaListCount = u2Index;
   }

   /* To make the PerfTest Row Status as Not-In-Service
    * all the mandatory parameters should be
    * configured */

    Y1564LwUtilPerfTestRowStatusNotInServ (pPerfTestEntry);

   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564PerformanceTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                setValFsY1564PerformanceTestDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564PerformanceTestDuration (UINT4 u4FsY1564ContextId, 
                                           UINT4 u4FsY1564PerformanceTestIndex, 
                                           INT4 i4SetValFsY1564PerformanceTestDuration)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;
    INT4                    i4Duration = Y1564_ZERO;

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId, 
                                                        u4FsY1564PerformanceTestIndex);

    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry not "
                           "present for %d \r\n",__FUNCTION__,
                           u4FsY1564PerformanceTestIndex);
        CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
        return SNMP_FAILURE;
    }

    if (i4SetValFsY1564PerformanceTestDuration == Y1564_PERF_TEST_MIN_DURATION) 
    {
        i4Duration = Y1564_TEST_DURATION_15MIN;
    }
    if (i4SetValFsY1564PerformanceTestDuration == Y1564_PERF_TEST_DEF_DURATION)
    {
        i4Duration = Y1564_TEST_DURATION_2HOUR;
    }
    if (i4SetValFsY1564PerformanceTestDuration == Y1564_PERF_TEST_MAX_DURATION)
    {
        i4Duration = Y1564_TEST_DURATION_24HOUR;
    }

    /* Set the performance duration */
    pPerfTestEntry->u1PerfTestDuration = (UINT1) i4Duration;
 
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564PerformanceTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                setValFsY1564PerformanceTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564PerformanceTestStatus (UINT4 u4FsY1564ContextId, 
                                         UINT4 u4FsY1564PerformanceTestIndex, 
                                         INT4 i4SetValFsY1564PerformanceTestStatus)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;
    tY1564ContextInfo      *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4FsY1564ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Inside this function Context Id and PerfTestEntry are validated */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex);
    if (pPerfTestEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf test entry not "
                           "present for %d \r\n",__FUNCTION__,
                           u4FsY1564PerformanceTestIndex);
        CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
        return SNMP_FAILURE;
    }


    if (i4SetValFsY1564PerformanceTestStatus == Y1564_SLA_START)
    {
        if (pContextInfo->u1ModuleStatus == Y1564_ENABLED)
        {

            if (Y1564CoreStartPerfTest (pPerfTestEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                                   Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : Failed to start the test for Perf Id %d \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* Test will not be started when Y1564 module is in disabled "
             * state */
            CLI_SET_ERR (CLI_Y1564_ERR_MODULE_NOT_ENABLED);
            return SNMP_FAILURE;
        }
    }
    if (i4SetValFsY1564PerformanceTestStatus == Y1564_SLA_STOP)
    {
        if (Y1564CoreStopPerfTest (pPerfTestEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                               Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                               " %s : Failed to start the test for Perf Id %d \r\n",
                               __FUNCTION__,u4FsY1564PerformanceTestIndex);
            CLI_SET_ERR (CLI_Y1564_ERR_TEST_STOP_FAIL);
            return SNMP_FAILURE;
        }
    }

    /* Set the performance test status */
    pPerfTestEntry->u1PerfTestStatus = (UINT1) 
                               i4SetValFsY1564PerformanceTestStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsY1564PerformanceTestRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                setValFsY1564PerformanceTestRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsY1564PerformanceTestRowStatus (UINT4 u4FsY1564ContextId, 
                                            UINT4 u4FsY1564PerformanceTestIndex, 
                                            INT4 i4SetValFsY1564PerformanceTestRowStatus)
{
    tPerfTestInfo  *pPerfTestEntry = NULL;
    tPerfTestInfo   PerfTestEntry;
    
    MEMSET (&PerfTestEntry, Y1564_ZERO, sizeof (tPerfTestInfo));
    if (Y1564CxtGetContextEntry (u4FsY1564ContextId) == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC, 
                           " %s : does not exist\r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsY1564PerformanceTestRowStatus)
    {
        case CREATE_AND_WAIT:

            pPerfTestEntry = Y1564UtilCreatePerfTestNode();

            if (pPerfTestEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Failed to create "
                                   "PerfTest Entry for ID %d \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                return SNMP_FAILURE;
            }
            /* Update PerfTest Id and Context Id to PerfTest
             * Structure */
            pPerfTestEntry->u4PerfTestId = u4FsY1564PerformanceTestIndex;
            pPerfTestEntry->u4ContextId = u4FsY1564ContextId;

            if (Y1564UtilAddNodeToPerfTestRBTree (pPerfTestEntry) != OSIX_SUCCESS)
            {
                /* Release memory allocated for Perf Test Entry */
                MemReleaseMemBlock (Y1564_PERF_TEST_POOL (), (UINT1 *) pPerfTestEntry);
                pPerfTestEntry = NULL;
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Perf test entry "
                                   "Add Failed for PerfTestId  %d\r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                return SNMP_FAILURE;
            }

            Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC,
                               " %s : Perf test entry added "
                               "to RBTree with PerfTestId %d\r\n",
                               __FUNCTION__,u4FsY1564PerformanceTestIndex);

            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set */
            pPerfTestEntry->u1RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:
            PerfTestEntry.u4PerfTestId = u4FsY1564PerformanceTestIndex;
            PerfTestEntry.u4ContextId = u4FsY1564ContextId;

            pPerfTestEntry = Y1564UtilGetNodeFromPerfTestTable (&PerfTestEntry);

            if (pPerfTestEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified Perf "
                                   "test entry is not present \r\n", 
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
                return SNMP_FAILURE;
            }
            pPerfTestEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            PerfTestEntry.u4PerfTestId = u4FsY1564PerformanceTestIndex;
            PerfTestEntry.u4ContextId = u4FsY1564ContextId;

            pPerfTestEntry = Y1564UtilGetNodeFromPerfTestTable (&PerfTestEntry);

            if (pPerfTestEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified Perf "
                                   "test entry is not present \r\n", 
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (Y1564UtilPerfTestDelNodeFromRBTree (pPerfTestEntry) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Perf test rntry "
                                   "delete failed for Id %d \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:

            PerfTestEntry.u4PerfTestId = u4FsY1564PerformanceTestIndex;
            PerfTestEntry.u4ContextId = u4FsY1564ContextId;

            pPerfTestEntry = Y1564UtilGetNodeFromPerfTestTable (&PerfTestEntry);

            if (pPerfTestEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified perf "
                                   "test entry is not present \r\n", 
                                   __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (pPerfTestEntry->u1RowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pPerfTestEntry->u1RowStatus = ACTIVE;
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsY1564PerformanceTestSlaList
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                testValFsY1564PerformanceTestSlaList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564PerformanceTestSlaList (UINT4 *pu4ErrorCode, 
                                             UINT4 u4FsY1564ContextId, 
                                             UINT4 u4FsY1564PerformanceTestIndex, 
                                             tSNMP_OCTET_STRING_TYPE * 
                                             pTestValFsY1564PerformanceTestSlaList)
{
    tPerfTestInfo     *pPerfTestEntry = NULL;
        
    pPerfTestEntry = Y1564LwUtilValidateCxtAndPerfTest (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex,
                                                        pu4ErrorCode);
    if (pPerfTestEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndPerfTest itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    MEMSET (pPerfTestEntry->au1TempSlaList, Y1564_ZERO, sizeof (pPerfTestEntry->au1TempSlaList));

    MEMCPY (pPerfTestEntry->au1TempSlaList, pTestValFsY1564PerformanceTestSlaList->pu1_OctetList,
            pTestValFsY1564PerformanceTestSlaList->i4_Length);

    if ((pTestValFsY1564PerformanceTestSlaList->i4_Length < Y1564_MIN_SLA_LIST) 
        || (pTestValFsY1564PerformanceTestSlaList->i4_Length > Y1564_MAX_SLA_LIST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_SLA_LIST);
        return SNMP_FAILURE;
    }
 
    /* Function itself takes care of filling error code and trace */
    if (Y1564LwUtilValidateSlaList (pPerfTestEntry, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* SLA List specified for performance test cannot be changed
     * if the performance test in in-progress */
    if (Y1564UtilValidateSlaTestState (pPerfTestEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (pPerfTestEntry->u2SlaListCount != Y1564_ZERO)
    {    
        /* Delete performance test report all the SLA's 
         * if the report present of perf entry */

        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_CRITICAL_TRC, " %s : change in "
                           "SLA list for perf test table, "
                           "deletes the performance test report "
                           "for that perf Id %d\r\n", __FUNCTION__,
                           pPerfTestEntry->u4PerfTestId);

        Y1564UtilDelPerfTestResultEntry (u4FsY1564ContextId, 
                                         pPerfTestEntry->u4PerfTestId,
                                         Y1564_ZERO);
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564PerformanceTestDuration
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                testValFsY1564PerformanceTestDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564PerformanceTestDuration (UINT4 *pu4ErrorCode, 
                                              UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564PerformanceTestIndex, 
                                              INT4 i4TestValFsY1564PerformanceTestDuration)
{

    tPerfTestInfo *pPerfTestEntry = NULL;

    if ((i4TestValFsY1564PerformanceTestDuration != 
                             Y1564_PERF_TEST_MIN_DURATION) &&
        (i4TestValFsY1564PerformanceTestDuration != 
                             Y1564_PERF_TEST_DEF_DURATION) && 
        (i4TestValFsY1564PerformanceTestDuration != 
                             Y1564_PERF_TEST_MAX_DURATION))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid performance test "
                           "duration for performance test. The value can be "
                           "(1) 15 min, (2) 2 hours and (3) 24 hours \r\n",
                            __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PERF_DURATION);
        return SNMP_FAILURE;
    }

    pPerfTestEntry = Y1564LwUtilValidateCxtAndPerfTest (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex,
                                                        pu4ErrorCode);
    if (pPerfTestEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndPerfTest itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* SLA Duration specified for performance test cannot be changed
     * if the performance test in in-progress */
    if (Y1564UtilValidateSlaTestState (pPerfTestEntry ) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, "%s : SLA Duration in Id  %d cannot"
                           " be modified when perf test is in-progress \r\n",
                           __FUNCTION__,u4FsY1564PerformanceTestIndex);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564PerformanceTestStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                testValFsY1564PerformanceTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564PerformanceTestStatus (UINT4 *pu4ErrorCode, 
                                            UINT4 u4FsY1564ContextId, 
                                            UINT4 u4FsY1564PerformanceTestIndex, 
                                            INT4 i4TestValFsY1564PerformanceTestStatus)
{
    tPerfTestInfo *pPerfTestEntry = NULL;

    if ((i4TestValFsY1564PerformanceTestStatus != Y1564_PERF_TEST_START) &&
        (i4TestValFsY1564PerformanceTestStatus != Y1564_PERF_TEST_STOP))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : Invalid test status for performance test. "
                           " The value can be start (1) and stop (2) \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPerfTestEntry = Y1564LwUtilValidateCxtAndPerfTest (u4FsY1564ContextId,
                                                        u4FsY1564PerformanceTestIndex,
                                                        pu4ErrorCode);
    if (pPerfTestEntry == NULL)
    {
        /* As the function Y1564LwUtilValidateCxtAndPerfTest itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsY1564PerformanceTestRowStatus
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex

                The Object 
                testValFsY1564PerformanceTestRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsY1564PerformanceTestRowStatus (UINT4 *pu4ErrorCode, 
                                               UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564PerformanceTestIndex, 
                                               INT4 i4TestValFsY1564PerformanceTestRowStatus)
{
    tPerfTestInfo          *pPerfTestEntry = NULL;
    UINT4                   u4EntryCount = Y1564_ZERO;


    /* Check the system status. Configuration will be allowed
     * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4FsY1564ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the range of Performance ID */
    if ((u4FsY1564PerformanceTestIndex < Y1564_MIN_PERF_TEST_ID) ||
        (u4FsY1564PerformanceTestIndex > Y1564_MAX_PERF_TEST_ID))
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : Invalid Service "
                           "perforamance Id.The range can be from 1 to 10 \r\n",
                           __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_INVALID_PERF_ID_RANGE);
        return SNMP_FAILURE;
    }

    /* Check the Row Status value */
    if ((i4TestValFsY1564PerformanceTestRowStatus < ACTIVE) ||
        (i4TestValFsY1564PerformanceTestRowStatus > DESTROY))
    {
        Y1564_GLOBAL_TRC ( "Invalid Row Status \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
  
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4FsY1564ContextId, 
                                                        u4FsY1564PerformanceTestIndex);

    if ((pPerfTestEntry == NULL) &&
        ((i4TestValFsY1564PerformanceTestRowStatus == NOT_IN_SERVICE) ||
        (i4TestValFsY1564PerformanceTestRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf entry is not "
                           "present to make the Row status as Not-In-Service "
                           "or Active \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsY1564PerformanceTestRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pPerfTestEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : PerfTest Id %d is "
                                   "already created  \r\n", __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_ALREADY_CREATED);
                return SNMP_FAILURE;
            }
          /* Count is fetched from RBTreeCount datastructure and it is
           * compared with maximum Number of Performance Table  entries that are allowed to create */
            RBTreeCount (Y1564_PERF_TEST_TABLE(), &u4EntryCount);
            if (u4EntryCount == MAX_Y1564_PERF_TEST_ENTRIES)
            {
                    Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_ALL_FAILURE_TRC
                                    | Y1564_MGMT_TRC, " %s : Maximum number of "
                                    "allowed Performance Table Entries "
                                    "reached \r\n", __FUNCTION__);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (CLI_Y1564_ERR_MAX_PERF_TABLE_ID_REACHED);
                    return SNMP_FAILURE;
            }

            break;

        case DESTROY:
            if (pPerfTestEntry == NULL)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : Specified perf"
                                   "entry is not present \r\n", __FUNCTION__);
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
                return SNMP_FAILURE;
            }

            if (Y1564UtilValidateSlaTestState (pPerfTestEntry ) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC,
                                   " %s : PerfTest entry  %d cannot "
                                   "be destroyed when perf test is in-progress \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            if (pPerfTestEntry->u1RowStatus == NOT_READY)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : PerfTest entry "
                                   "%d cannot move from Not-Ready to "
                                   "Not-In-Service \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_READY);
                return SNMP_FAILURE;
            }
            if (Y1564UtilValidateSlaTestState (pPerfTestEntry ) != OSIX_SUCCESS)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC, " %s : PerfTest entry "
                                   "%d cannot be set as Not-In-Service when perf "
                                   "test is in-progress \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            /* Row status can be Active only if the SLA list is associated with 
             * PerformanceTest table
             * If the parameters are mapped to PerfTable then row status can be set as
             * Not-In-Service */
            if (pPerfTestEntry->u1RowStatus == NOT_READY)
            {
                Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                                   Y1564_ALL_FAILURE_TRC," %s : PerfTest entry %d "
                                   "cannot move from Not-Ready to Active \r\n",
                                   __FUNCTION__,u4FsY1564PerformanceTestIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_READY);
                return SNMP_FAILURE;
            }
     
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
 }
    /* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsY1564PerformanceTestTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564PerformanceTestIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsY1564PerformanceTestTable (UINT4 *pu4ErrorCode, 
                                          tSnmpIndexList *pSnmpIndexList, 
                                          tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsY1564PerfTestReportTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsY1564PerfTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsY1564PerfTestReportTable (UINT4 u4FsY1564ContextId, 
                                                         UINT4 u4FsY1564SlaId, 
                                                         UINT4 u4FsY1564PerformanceTestIndex,
                                                         INT4 i4FsY1564PerfTestReportFrSize )
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsY1564PerfTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsY1564PerfTestReportTable (UINT4 *pu4FsY1564ContextId,
                                            UINT4 *pu4FsY1564SlaId,
                                            UINT4
                                            *pu4FsY1564PerformanceTestIndex,
                                            INT4
                                            *pi4FsY1564PerfTestReportFrSize)
{
    tPerfTestReportInfo *pPerfTestReportEntry = NULL;

    pPerfTestReportEntry = Y1564UtilGetFirstNodeFromPerfTestReportTable();

    if (pPerfTestReportEntry != NULL)
    {
        *pu4FsY1564ContextId = pPerfTestReportEntry->u4ContextId;
        *pu4FsY1564SlaId = pPerfTestReportEntry->u4SlaId;
        *pu4FsY1564PerformanceTestIndex = pPerfTestReportEntry->u4StatsPerfId;
        *pi4FsY1564PerfTestReportFrSize = pPerfTestReportEntry->i4FrameSize;
         return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsY1564PerfTestReportTable
 Input       :  The Indices
                FsY1564ContextId
                nextFsY1564ContextId
                FsY1564SlaId
                nextFsY1564SlaId
                FsY1564PerformanceTestIndex
                nextFsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize
                nextFsY1564PerfTestReportFrSize
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsY1564PerfTestReportTable (UINT4 u4FsY1564ContextId,
                                                UINT4 *pu4NextFsY1564ContextId, 
                                                UINT4 u4FsY1564SlaId,
                                                UINT4 *pu4NextFsY1564SlaId, 
                                                UINT4 u4FsY1564PerformanceTestIndex,
                                                UINT4 *pu4NextFsY1564PerformanceTestIndex,
                                                INT4   i4FsY1564PerfTestReportFrSize,
                                                INT4 *pi4NextFsY1564PerfTestReportFrSize)
{
    tPerfTestReportInfo      *pNextPerfTestReportEntry = NULL;
    tPerfTestReportInfo       PerfTestReportEntry;

    MEMSET (&PerfTestReportEntry, 0, sizeof (tPerfTestReportInfo));

    PerfTestReportEntry.u4ContextId = u4FsY1564ContextId;
    PerfTestReportEntry.u4SlaId = u4FsY1564SlaId;
    PerfTestReportEntry.u4StatsPerfId = 
        u4FsY1564PerformanceTestIndex;
    PerfTestReportEntry.i4FrameSize = i4FsY1564PerfTestReportFrSize;

    pNextPerfTestReportEntry = Y1564UtilGetNextNodeFromPerfTestReportTable
                                  (&PerfTestReportEntry);

    if (pNextPerfTestReportEntry != NULL)
    {
        *pu4NextFsY1564ContextId = pNextPerfTestReportEntry->u4ContextId;
        *pu4NextFsY1564SlaId = pNextPerfTestReportEntry->u4SlaId;
        *pu4NextFsY1564PerformanceTestIndex = pNextPerfTestReportEntry->u4StatsPerfId;
        *pi4NextFsY1564PerfTestReportFrSize = pNextPerfTestReportEntry->i4FrameSize;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportResult
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsY1564PerfTestReportResult (UINT4 u4FsY1564ContextId,
                                   UINT4 u4FsY1564SlaId,
                                   UINT4 u4FsY1564PerformanceTestIndex,
                                   INT4 i4FsY1564PerfTestReportFrSize,
                                   INT4 *pi4RetValFsY1564PerfTestReportResult)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsY1564PerfTestReportResult = pPerfTestReportEntry->u1StatsResult;
    
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportIrMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportIrMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportIrMin (UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId, 
                                       UINT4 u4FsY1564PerformanceTestIndex, 
                                       INT4 i4FsY1564PerfTestReportFrSize,
                                       UINT4 *pu4RetValFsY1564PerfTestReportIrMin)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportIrMin = pPerfTestReportEntry->u4StatsIrMin;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportIrMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportIrMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportIrMean (UINT4 u4FsY1564ContextId, 
                                        UINT4 u4FsY1564SlaId, 
                                        UINT4 u4FsY1564PerformanceTestIndex, 
                                        INT4 i4FsY1564PerfTestReportFrSize,
                                        UINT4 *pu4RetValFsY1564PerfTestReportIrMean)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportIrMean = pPerfTestReportEntry->u4StatsIrMean;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportIrMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportIrMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportIrMax (UINT4 u4FsY1564ContextId, 
                                       UINT4 u4FsY1564SlaId,
                                       UINT4 u4FsY1564PerformanceTestIndex,
                                       INT4 i4FsY1564PerfTestReportFrSize,
                                       UINT4 *pu4RetValFsY1564PerfTestReportIrMax)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportIrMax = pPerfTestReportEntry->u4StatsIrMax;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrLossCnt
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrLossCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrLossCnt (UINT4 u4FsY1564ContextId, 
                                           UINT4 u4FsY1564SlaId, 
                                           UINT4 u4FsY1564PerformanceTestIndex, 
                                           INT4 i4FsY1564PerfTestReportFrSize,
                                           UINT4 *pu4RetValFsY1564PerfTestReportFrLossCnt)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrLossCnt = pPerfTestReportEntry->u4StatsFrLossCnt;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrLossRatio
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrLossRatio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrLossRatio (UINT4 u4FsY1564ContextId, 
                                             UINT4 u4FsY1564SlaId, 
                                             UINT4 u4FsY1564PerformanceTestIndex,
                                             INT4 i4FsY1564PerfTestReportFrSize,
                                             UINT4 *pu4RetValFsY1564PerfTestReportFrLossRatio)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry " 
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrLossRatio = pPerfTestReportEntry->u4StatsFrLossRatio;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrTxDelayMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrTxDelayMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrTxDelayMin (UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564SlaId, 
                                              UINT4 u4FsY1564PerformanceTestIndex, 
                                              INT4 i4FsY1564PerfTestReportFrSize,
                                              UINT4 *pu4RetValFsY1564PerfTestReportFrTxDelayMin)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrTxDelayMin = pPerfTestReportEntry->u4StatsFrTxDelayMin;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrTxDelayMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrTxDelayMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrTxDelayMean (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               UINT4 u4FsY1564PerformanceTestIndex,
                                               INT4 i4FsY1564PerfTestReportFrSize,
                                               UINT4 *pu4RetValFsY1564PerfTestReportFrTxDelayMean)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrTxDelayMean = pPerfTestReportEntry->u4StatsFrTxDelayMean;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrTxDelayMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrTxDelayMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrTxDelayMax (UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564SlaId, 
                                              UINT4 u4FsY1564PerformanceTestIndex, 
                                              INT4 i4FsY1564PerfTestReportFrSize,
                                              UINT4 *pu4RetValFsY1564PerfTestReportFrTxDelayMax)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);

        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrTxDelayMax = pPerfTestReportEntry->u4StatsFrTxDelayMax;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrDelayVarMin
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrDelayVarMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrDelayVarMin (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               UINT4 u4FsY1564PerformanceTestIndex,
                                               INT4 i4FsY1564PerfTestReportFrSize,
                                               UINT4 *pu4RetValFsY1564PerfTestReportFrDelayVarMin)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrDelayVarMin = pPerfTestReportEntry->u4StatsFrDelayVarMin;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrDelayVarMean
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrDelayVarMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrDelayVarMean (UINT4 u4FsY1564ContextId, 
                                                UINT4 u4FsY1564SlaId, 
                                                UINT4 u4FsY1564PerformanceTestIndex, 
                                                INT4 i4FsY1564PerfTestReportFrSize,
                                                UINT4 *pu4RetValFsY1564PerfTestReportFrDelayVarMean)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId,
                           Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                           " %s : PerfTestReport Entry is NULL \r\n",
                           __FUNCTION__);

        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrDelayVarMean = pPerfTestReportEntry->u4StatsFrDelayVarMean;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportFrDelayVarMax
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportFrDelayVarMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportFrDelayVarMax (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               UINT4 u4FsY1564PerformanceTestIndex, 
                                               INT4 i4FsY1564PerfTestReportFrSize,
                                               UINT4 *pu4RetValFsY1564PerfTestReportFrDelayVarMax)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportFrDelayVarMax = pPerfTestReportEntry->u4StatsFrDelayVarMax;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportAvailability
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportAvailability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportAvailability (UINT4 u4FsY1564ContextId, 
                                              UINT4 u4FsY1564SlaId, 
                                              UINT4 u4FsY1564PerformanceTestIndex, 
                                              INT4 i4FsY1564PerfTestReportFrSize,
                                              tSNMP_OCTET_STRING_TYPE * 
                                              pRetValFsY1564PerfTestReportAvailability)
{

    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }


    if (Y1564_FLOAT_TO_OCTETSTRING ( (DBL8) pPerfTestReportEntry->f4StatsAvailability,
                                     pRetValFsY1564PerfTestReportAvailability) == EOF)

    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Availability "
                           "conversion error for Perf Id %d\r\n",
                           __FUNCTION__,u4FsY1564PerformanceTestIndex);
        return SNMP_FAILURE;
    }

    pRetValFsY1564PerfTestReportAvailability->i4_Length = (INT4)
        STRLEN (pRetValFsY1564PerfTestReportAvailability);

    pRetValFsY1564PerfTestReportAvailability->pu1_OctetList
    [pRetValFsY1564PerfTestReportAvailability->i4_Length] ='\0';

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportUnavailableCount
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportUnavailableCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportUnavailableCount (UINT4 u4FsY1564ContextId, 
                                                  UINT4 u4FsY1564SlaId, 
                                                  UINT4 u4FsY1564PerformanceTestIndex,
                                                  INT4 i4FsY1564PerfTestReportFrSize,
                                                  UINT4 *pu4RetValFsY1564PerfTestReportUnavailableCount)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportUnavailableCount = pPerfTestReportEntry->u4StatsUnAvailCount;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportTestStartTime
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportTestStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportTestStartTime (UINT4 u4FsY1564ContextId, 
                                               UINT4 u4FsY1564SlaId, 
                                               UINT4 u4FsY1564PerformanceTestIndex, 
                                               INT4 i4FsY1564PerfTestReportFrSize,
                                               UINT4 * pu4RetValFsY1564PerfTestReportTestStartTime)
{
    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportTestStartTime =
        pPerfTestReportEntry->PerfTestStartTimeStamp;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsY1564PerfTestReportTestEndTime
 Input       :  The Indices
                FsY1564ContextId
                FsY1564SlaId
                FsY1564PerformanceTestIndex
                FsY1564PerfTestReportFrSize

                The Object 
                retValFsY1564PerfTestReportTestEndTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsY1564PerfTestReportTestEndTime (UINT4 u4FsY1564ContextId, 
                                             UINT4 u4FsY1564SlaId, 
                                             UINT4 u4FsY1564PerformanceTestIndex,
                                             INT4 i4FsY1564PerfTestReportFrSize,
                                             UINT4 *pu4RetValFsY1564PerfTestReportTestEndTime)
{

    tPerfTestReportInfo  *pPerfTestReportEntry = NULL;

    /* Inside this function the following are validated *
     * Sla Identifier
     * Context Identifier
     * Performance Test Identifier
     * SLA Table
     * PerfTestReportTable */

    pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4FsY1564ContextId,
                                                            u4FsY1564SlaId,
                                                            u4FsY1564PerformanceTestIndex,
                                                            i4FsY1564PerfTestReportFrSize);
    if (pPerfTestReportEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4FsY1564ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : PerfTestReport Entry "
                           "is NULL \r\n",__FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsY1564PerfTestReportTestEndTime = 
                                 pPerfTestReportEntry->PerfTestEndTimeStamp;

    return SNMP_SUCCESS;

}

/*******************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateCxtAndSla                             *
 *                                                                             * 
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context. After that   *
 *                    this function will check for the existance of SLA entry  *
 *                    to the given SlaId in that context.                      *
 *                                                                             *
 *                    This function will be called by the low level            *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId - Context Identifier                         *
 *                    u4SlaId      - Sla Identifier                            *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tY1564SlaInfo structure.                  *
 *                                                                             *
 ******************************************************************************/
PRIVATE tSlaInfo *
Y1564LwUtilValidateCxtAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                              UINT4 *pu4ErrorCode)
{
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo  SlaEntry;

    MEMSET (&SlaEntry, Y1564_ZERO, sizeof (tSlaInfo));

    if (Y1564LwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    }

    SlaEntry.u4ContextId = u4ContextId;
    SlaEntry.u4SlaId = u4SlaId;

    pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

    if (pSlaEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
    }  
    return pSlaEntry;
}
/*******************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateCxtAndSac                             *
 *                                                                             * 
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context. After that   *
 *                    this function will check for the existance of SAC entry  *
 *                    to the given SacId in that context.                      *
 *                                                                             *
 *                    This function will be called by the low level            *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId - Context Identifier                         *
 *                    u4SacId      - SAC Identifier                            *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tY1564SacInfo structure.                  *
 *                                                                             *
 ******************************************************************************/
PRIVATE tSacInfo *
Y1564LwUtilValidateCxtAndSac (UINT4 u4ContextId, UINT4 u4SacId,
                              UINT4 *pu4ErrorCode)
{
    tSacInfo *pSacEntry = NULL;
    tSacInfo  SacEntry;

    MEMSET (&SacEntry, Y1564_ZERO, sizeof (tSacInfo));

    if (Y1564LwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    } 

    SacEntry.u4ContextId = u4ContextId;
    SacEntry.u4SacId = u4SacId;

    pSacEntry = Y1564UtilSacGetNodeFromSacTable (&SacEntry);

    if (pSacEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_CREATED);
        return NULL;    
    }

    return pSacEntry;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateCxtAndPerfTest                        *
 *                                                                             * 
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context. After that   *
 *                    this function will check for the existance of Traffic    *
 *                    Profile entry to the given Traf Prof Id in that context  *
 *                                                                             *
 *                    This function will be called by the low level            *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId   - Context Identifier                       *
 *                    u4PerfId      - Performance Identifier                   *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tY1564PerfTestInfo structure.             *
 *                                                                             *
 ******************************************************************************/
PRIVATE tPerfTestInfo *
Y1564LwUtilValidateCxtAndPerfTest (UINT4 u4ContextId, UINT4 u4PerfId, 
                                   UINT4 *pu4ErrorCode)
{
    tPerfTestInfo *pPerfTestEntry = NULL;
    tPerfTestInfo  PerfTestEntry;

    MEMSET (&PerfTestEntry, Y1564_ZERO, sizeof (tPerfTestInfo));

    if (Y1564LwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    } 

    PerfTestEntry.u4ContextId = u4ContextId;
    PerfTestEntry.u4PerfTestId = u4PerfId;

    pPerfTestEntry = Y1564UtilGetNodeFromPerfTestTable (&PerfTestEntry);

    if (pPerfTestEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_NOT_CREATED);
    }  
    return pPerfTestEntry;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateCxtAndTrafProf                        *
 *                                                                             * 
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context. After that   *
 *                    this function will check for the existance of Traffic    *
 *                    Profile entry to the given Traf Prof Id in that context  *
 *                                                                             *
 *                    This function will be called by the low level            *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId   - Context Identifier                       *
 *                    u4TrafProfId  - Traffic Profile Identifier               *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tTrafficProfileInfo  structure.           *
 *                                                                             *
 ******************************************************************************/
PRIVATE tTrafficProfileInfo *
Y1564LwUtilValidateCxtAndTrafProf (UINT4 u4ContextId, UINT4 u4TrafProfId,
                                   UINT4 *pu4ErrorCode)
{
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    tTrafficProfileInfo  TrafProfEntry;

    MEMSET (&TrafProfEntry, Y1564_ZERO, sizeof (tTrafficProfileInfo));

    if (Y1564LwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    } 

    TrafProfEntry.u4ContextId = u4ContextId;
    TrafProfEntry.u4TrafProfId = u4TrafProfId;

    pTrafProfEntry = Y1564UtilGetNodeFromTrafProfTable (&TrafProfEntry);

    if (pTrafProfEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED);
        return NULL;
    }  
    return pTrafProfEntry;
}

#ifndef MEF_WANTED
/*******************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateCxtAndServConf                        *
 *                                                                             * 
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context. After that   *
 *                    this function will check for the existance of Service    *
 *                    Configuration entry to the given Service Config Id       *
 *                    in that context                                          *
 *                                                                             *
 *                    This function will be called by the low level            *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId   - Context Identifier                       *
 *                    u4ServiceConfId  - Service Conf Identifier               *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tServiceConfInfo structure.               *
 *                                                                             *
 ******************************************************************************/
PRIVATE tServiceConfInfo *
Y1564LwUtilValidateCxtAndServConf (UINT4 u4ContextId, UINT4 u4ServiceConfId,
                                   UINT4 *pu4ErrorCode)
{
    tServiceConfInfo *pServiceConfEntry = NULL;
    tServiceConfInfo  ServiceConfEntry;

    MEMSET (&ServiceConfEntry, Y1564_ZERO, sizeof (tServiceConfInfo));

    if (Y1564LwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    } 

    ServiceConfEntry.u4ContextId = u4ContextId;
    ServiceConfEntry.u4ServiceConfId = u4ServiceConfId;

    pServiceConfEntry = Y1564UtilGetNodeFromServConfTable (&ServiceConfEntry);

    if (pServiceConfEntry == NULL)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }  
    return pServiceConfEntry;
}
#endif
/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateContextInfo                            *
 *                                                                              *
 * DESCRIPTION      : This function will check whether the context is created   *
 *                    and also the system control of the context.               *
 *                                                                              *
 *                    This function will be called by the nmhGet/nmhSet         *
 *                    routines and this function will take care of filling the  *
 *                    the SNMP error codes, CLI set error and trace messages.   *
 *                                                                              *
 * INPUT            : u4ContextId - Context Identifier                          *
 *                                                                              *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                 *
 *                                                                              *
 * RETURNS          : Pointer to the tY1564ContextInfo structure.               *
 *                                                                              *
 *******************************************************************************/
PRIVATE tY1564ContextInfo *
Y1564LwUtilValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tY1564ContextInfo   *pContextInfo = NULL;

    pContextInfo = Y1564CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        return NULL;
    }

   /* Check the system status. Configuration will be allowed
    * only when Y1564 is started in the context*/
    if ((Y1564CxtIsY1564Started (u4ContextId)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_Y1564_ERR_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return NULL;
    } 
    return pContextInfo;
}

/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilSlaRowStatusNotInServ                          *
 *                                                                              *
 * DESCRIPTION      : This function will configure the Row Status of SLA into   *
 *                    Not-In-Service once all the mandatory parameters are      *
 *                    configured                                                *
 *                                                                              *
 * INPUT            : tSlaInfo - Pointer to Sla Entry                           *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE VOID
Y1564LwUtilSlaRowStatusNotInServ (tSlaInfo * pSlaEntry)
{

    if ((pSlaEntry->u4SlaSacId != Y1564_ZERO) &&
        (pSlaEntry->u4SlaTrafProfId != Y1564_ZERO) &&
#ifdef MEF_WANTED
        (pSlaEntry->u2EvcId != Y1564_ZERO) &&
#else
        (pSlaEntry->u2SlaServiceConfId != Y1564_ZERO) &&
#endif
        (pSlaEntry->u4SlaMegId != Y1564_ZERO) &&
        (pSlaEntry->u4SlaMeId != Y1564_ZERO) &&
        (pSlaEntry->u4SlaMepId != Y1564_ZERO))
    {
        pSlaEntry->u1RowStatus = NOT_IN_SERVICE;
    }
    else
    {
        pSlaEntry->u1RowStatus = NOT_READY;
    }
}
/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilSacRowStatusNotInServ                          *
 *                                                                              *
 * DESCRIPTION      : This function will configure the Row Status of SAC into   *
 *                    Not-In-Service once all the mandatory parameters are      *
 *                    configured                                                *
 *                                                                              *
 * INPUT            : tSacInfo - Pointer to Sac Entry                           *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE VOID
Y1564LwUtilSacRowStatusNotInServ (tSacInfo * pSacEntry)
{
    if ((pSacEntry->u4SacInfoRate != Y1564_ZERO) &&
        (pSacEntry->f4SacAvailability != Y1564_ZERO))
    {
        pSacEntry->u1RowStatus = NOT_IN_SERVICE;
    }

}

/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilTrafProfRowStatusNotInServ                     *
 *                                                                              *
 * DESCRIPTION      : This function will configure the Row Status of Traf Prof  *
 *                    into Not-In-Service once all the mandatory parameters are *
 *                    configured                                                *
 *                                                                              *
 * INPUT            : tTrafficProfileInfo - Pointer to  Entry                   *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE VOID
Y1564LwUtilTrafProfRowStatusNotInServ (tTrafficProfileInfo * pTrafProfEntry)
{

   if (MEMCMP (&gaNullPayload, pTrafProfEntry->au1TrafProfPayload,
                Y1564_PAYLOAD_SIZE) != Y1564_ZERO)
    {
        pTrafProfEntry->u1RowStatus = NOT_IN_SERVICE;
    }
}
/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilPerfTestRowStatusNotInServ                     *
 *                                                                              *
 * DESCRIPTION      : This function will configure the Row Status of Perf       *
 *                    Test into  Not-In-Service once all the mandatory          *
 *                    parameters are configured                                 *
 *                                                                              *
 * INPUT            : tPerfTestInfo - Pointer to PerfTest Entry                 *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE VOID
Y1564LwUtilPerfTestRowStatusNotInServ (tPerfTestInfo * pPerfTestEntry)
{

    if (pPerfTestEntry->u2SlaListCount != Y1564_ZERO)
    {
        pPerfTestEntry->u1RowStatus = NOT_IN_SERVICE;
    }
    else
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC, " %s : Perf Test Entry "
                           "shouldn't be activate when SLA List is not "
                           "configured for Id  %d\r\n",
                           __FUNCTION__,pPerfTestEntry->u4PerfTestId);
    }
}

#ifndef MEF_WANTED

/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateServConfAndSla                         *
 *                                                                              *
 * DESCRIPTION      : This function will check whether the given Service Config *
 *                    associated with SLA and check for the SLA test current    *
 *                    state.                                                    *
 *                                                                              *
 * INPUT            : u4ContextId - Context Identifier                          *
 *                    u4SlaId - Sla Identifier                                  *
 *                    u4ServConfId - Traffic Profile Identifier                 *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE INT4
Y1564LwUtilValidateServConfAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                                   UINT4 u4ServConfId)
{
    tSlaInfo *pSlaEntry = NULL;
    tSlaInfo SlaEntry;

    MEMSET( &SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    SlaEntry.u4SlaId = u4SlaId;
    SlaEntry.u4ContextId = u4ContextId;

    pSlaEntry = &SlaEntry;

    /* Service configuration/deletion will be allowed when any one of the below
       conditions are met.

       1. Service Config is associated to SLA but test is either in not-initiate, aborted
       and complete state
       2. Service Config is not associated to any of the SLA.

       To achieve this, SLA entry is fetched from SLA table
       and ServiceConfig Id values are validated.

       If the Service Config is associated with SLA, then current test
       state of the SLA is validated . If the SLA state is In-Progress
       then Service configuration/deletion will not be allowed. */

    while ((pSlaEntry =
            Y1564UtilSlaGetNextNodeFromSlaTable (pSlaEntry)) != NULL)
    {
        /* If Sla Entry is found for the SAC entry context, proceed further
           otherwise exit from the while loop */
        if (pSlaEntry->u4ContextId != u4ContextId)
        {
            break;
        }

        if (pSlaEntry->u2SlaServiceConfId == (UINT2) u4ServConfId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)
            {
                Y1564_CONTEXT_TRC (u4ContextId,Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Service Config "
                                   "Id %d is associated to SLA %d and SLA test "
                                   "is in-progress.either test has to be "
                                   "stopped or wait for the test completion \r\n",
                                   __FUNCTION__,u4ServConfId, pSlaEntry->u4SlaId);
                CLI_SET_ERR (CLI_Y1564_ERR_SLA_TEST_PROGRESS);
                return SNMP_FAILURE;
            }
            else
            {
                Y1564_CONTEXT_TRC (u4ContextId, Y1564_MGMT_TRC |
                                   Y1564_ALL_FAILURE_TRC, " %s : Service Config"
                                   " Id %d is associated to Sla Id %d. "
                                   "Deletion/ Modification is not allowed\r\n",
                                   __FUNCTION__,u4ServConfId, pSlaEntry->u4SlaId);
                CLI_SET_ERR (CLI_Y1564_ERR_DEASSOCIATE_SERVICE_CONF_ENTRY);
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilServiceConfInfoRowStatusNotInServ              *
 *                                                                              *
 * DESCRIPTION      : This function will configure the Row Status of Serv Conf  *
 *                    into Not-In-Service once all the mandatory parameters are *
 *                    configured                                                *
 *                                                                              *
 * INPUT            : tServiceConfInfo - Pointer to Service Config Entry        *
 *                                                                              *
 * OUTPUT           : None                                                      *
 *                                                                              *
 * RETURNS          : None                                                      *
 *                                                                              *
 *******************************************************************************/
PRIVATE VOID
Y1564LwUtilServiceConfInfoRowStatusNotInServ (tServiceConfInfo * pServiceConfEntry)
{
    if ((pServiceConfEntry->u4ServiceConfCIR != Y1564_ZERO) &&
        (pServiceConfEntry->u4ServiceConfCBS != Y1564_ZERO) &&
        (pServiceConfEntry->u4ServiceConfEIR != Y1564_ZERO) &&
        (pServiceConfEntry->u4ServiceConfEBS != Y1564_ZERO))
    {
        pServiceConfEntry->u1RowStatus = NOT_IN_SERVICE;
    }

}

#endif
/********************************************************************************
 * FUNCTION NAME    : Y1564LwUtilValidateSlaList                                *
 *                                                                              *
 * DESCRIPTION      : This function will validate the sla list for the perf     *
 *                    entry with other perf test table entries before mapping   *
 *                    it to given perf test entry and also checks for the       *
 *                    SLA existence                                             *  
 *                                                                              *
 * INPUT            : pPerfTestEntry - Pointer to Perf test Entry               *
 *                                                                              *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code                  *
 *                                                                              *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                 *
 *                                                                              *
 *******************************************************************************/
PRIVATE INT4
Y1564LwUtilValidateSlaList (tPerfTestInfo *pPerfTestEntry, UINT4 *pu4ErrorCode)
{
    tSlaInfo          *pSlaEntry = NULL;
    tSlaInfo          *pSlaInfo = NULL;
    tPerfTestInfo *pPerfTstEntry = NULL;
    tPerfTestInfo *pNxtPerfTstEntry = NULL;
    tSlaInfo          SlaEntry;
    tSlaInfo          SlaInfo;
    UINT4             u4SlaId = Y1564_ZERO;
    UINT2             u2ByteIndex = 0;
    UINT2             u2BitIndex = 0;
    UINT2             u2SlaIndex = Y1564_ZERO;
    UINT2             u2Index = Y1564_ONE;
    BOOL1             bResult = Y1564_ZERO;

    for (u2ByteIndex = Y1564_ZERO; u2ByteIndex < Y1564_MAX_SLA_LIST;
         u2ByteIndex++)
    {
        if (pPerfTestEntry->au1TempSlaList[u2ByteIndex] != Y1564_ZERO)
        {
            u2SlaIndex = pPerfTestEntry->au1TempSlaList[u2ByteIndex];

            for (u2BitIndex = Y1564_ZERO;
                 (u2BitIndex < Y1564_BITS_PER_BYTE && u2SlaIndex != Y1564_ZERO);
                 u2BitIndex++)
            {
                if ((u2SlaIndex & VLAN_BIT8) != Y1564_ZERO)
                {
                    u4SlaId = (UINT4) ((u2ByteIndex * Y1564_BITS_PER_BYTE) +
                               u2BitIndex + 1);

                    SlaEntry.u4SlaId = u4SlaId;
                    SlaEntry.u4ContextId = pPerfTestEntry->u4ContextId;      

                    pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

                    if (pSlaEntry == NULL)
                    {
                        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, Y1564_MGMT_TRC | 
                                           Y1564_ALL_FAILURE_TRC, " %s : "
                                           "SLA %d configured in PerfTest Entry %d "
                                           "is not present\r\n", __FUNCTION__,u4SlaId,
                                           pPerfTestEntry->u4PerfTestId);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (CLI_Y1564_ERR_PERF_TEST_SLA_NOT_PRESENT);
                        return OSIX_FAILURE;
                    }
                    /* Check is added here to validate the Sla list configured for perf test 
                     * entry with other perf test entry in perf test table
                     * Because same SLA cannot be mapped with multiple perf test entry */

                    pPerfTstEntry = Y1564UtilGetFirstNodeFromPerfTestTable ();

                    while (pPerfTstEntry != NULL)
                    {
                        if (pPerfTstEntry->u2SlaListCount == Y1564_ZERO)
                        {
                            /* Update Perf Test Id in SLA strcuture
                             * here itself When Sla list count is ZERO, 
                             * while creating SLA list for the first time, 
                             * perf entry will have SlaListCount value as ZERO.
                             * */
                            pSlaEntry->u4PerfId = pPerfTestEntry->u4PerfTestId;
                        }

                        for (; u2Index <= pPerfTstEntry->u2SlaListCount; u2Index++)
                        {
                            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTstEntry->pau1SlaList)),
                                                     u2Index, sizeof (tY1564SlaList),
                                                     bResult);
                            if (bResult == OSIX_FALSE)
                            {
                                continue;
                            }
                            else
                            {
                                if ((u2Index == pSlaEntry->u4SlaId) &&
                                    (pPerfTstEntry->u4PerfTestId != pPerfTestEntry->u4PerfTestId))
                                {
                                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                                       Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                                       " %s : Sla Id %d is already mapped "
                                                       "with other Perf Entry %d.De-associate "
                                                       "it from existing entry and associate it "
                                                       "to current perf test entry\r\n",
                                                       __FUNCTION__,pSlaEntry->u4SlaId,
                                                       pPerfTstEntry->u4PerfTestId);
                                    CLI_SET_ERR (CLI_Y1564_ERR_SLA_MAP_ERR);
                                    return OSIX_FAILURE;
                                }
                                else
                                {
                                    /* while changing the SLA list for perf entry
                                     * Perf test id association to existing SLA 
                                     * needs to be reset to ZERO */
                                    if ((u2Index != pSlaEntry->u4SlaId) &&
                                        (pPerfTstEntry->u4PerfTestId == pPerfTestEntry->u4PerfTestId))
                                    {
                                        SlaInfo.u4SlaId = (UINT4) u2Index;
                                        SlaInfo.u4ContextId = pPerfTestEntry->u4ContextId;      

                                        pSlaInfo = Y1564UtilSlaGetNodeFromSlaTable (&SlaInfo);

                                        if (pSlaInfo != NULL)
                                        {
                                            pSlaInfo->u4PerfId = Y1564_ZERO;
                                        }
                                    }
                                    /* SLA is not part of other perf test entry
                                     * so update the perf test id in SLA structure */
                                    pSlaEntry->u4PerfId = pPerfTestEntry->u4PerfTestId;
                                }
                            }
                        }
                        pNxtPerfTstEntry = Y1564UtilGetNextNodeFromPerfTestTable (pPerfTstEntry);
                        pPerfTstEntry = pNxtPerfTstEntry;
                        u2Index = Y1564_ONE;
                    }
                }
                u2SlaIndex = (UINT2) (u2SlaIndex  << 1);
            }
        }
    }
    return OSIX_SUCCESS;
}
