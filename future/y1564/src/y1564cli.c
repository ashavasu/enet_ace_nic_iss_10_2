/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: y1564cli.c,v 1.34 2017/02/27 13:52:09 siva Exp $
*
* Description: This file contains the Y1564 CLI related routines 
*********************************************************************/

#ifndef _Y1564CLI_C_
#define _Y1564CLI_C_

#include "y1564inc.h"
#include "fsy156cli.h"
#include <time.h>

PRIVATE VOID
Y1564CliWrapperValueHandling (UINT4 u4WrapValue, UINT4 * pu4TempValue, 
                              UINT1 *pu1Count);
PRIVATE VOID
Y1564CliGetTimeStamp(tConfTestReportInfo *pConfigTestReportEntry,
                     UINT4 *u4TempStartTime, UINT4 *u4TempEndTime);

UINT4               gu4Y1564TrcLvl;
/****************************************************************************
 * Function    :  cli_process_y1564_cmd                                     *
 * Description :  This function is exported to CLI module to handle the     *
 *                Y1564 cli commands to take the corresponding action.      *
 *                                                                          *
 * Input       :  Variable arguments                                        *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
cli_process_y1564_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[Y1564_CLI_MAX_ARGS];
    UINT4               u4ErrCode = Y1564_ZERO;
    UINT4               u4IfIndex = Y1564_ZERO;
    UINT4               u4ContextId = Y1564_ZERO;
    UINT4               u4TrafProfId = Y1564_ZERO;
    UINT4               u4SacId = Y1564_ZERO;
    UINT4               u4SlaMeg = Y1564_ZERO;
    UINT4               u4SlaMe = Y1564_ZERO;
    UINT4               u4SlaMep = Y1564_ZERO;
    UINT4               u4EvcIndex = Y1564_ZERO;
#ifndef MEF_WANTED    
    UINT4               u4ServConfigId = Y1564_ZERO;
#endif    
    UINT4               u4TrafficPolicing = Y1564_ZERO;
    INT4                i4ModeId = Y1564_ZERO;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst = Y1564_ZERO;
    INT1                argno = Y1564_ZERO;
    UINT2               u2LocalPortId = Y1564_ZERO;
    INT4                i4Args = Y1564_ZERO;

    if (Y1564_IS_SYSTEM_INITIALISED () != Y1564_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Y1564 module is shutdown\r\n");
        return CLI_FAILURE;
    }

    CliRegisterLock (CliHandle, Y1564ApiLock, Y1564ApiUnLock); 

    Y1564_LOCK ();

    if (Y1564CliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId)
        == Y1564_FAILURE)
    {
        Y1564_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    if (i4Inst != Y1564_ZERO)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    UNUSED_PARAM (u4IfIndex);

    /* Walk through the rest of the arguements and store in args array.
     * Store CLI_Y1564_MAX_ arguements at the max */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == Y1564_CLI_MAX_ARGS)
        {
            break;
        }
    }

    i4ModeId = CLI_GET_MODE_ID ();

    va_end (ap);

    switch (u4Command)
    {
        case CLI_Y1564_SHUTDOWN:

            i4RetStatus = Y1564CliSetSystemControl (CliHandle, u4ContextId, 
                                                    Y1564_SHUTDOWN);
            break;

        case CLI_Y1564_START:

            i4RetStatus = Y1564CliSetSystemControl (CliHandle, u4ContextId, 
                                                    Y1564_START);
            break;

        case CLI_Y1564_ENABLE:

            i4RetStatus = Y1564CliSetModuleStatus (CliHandle, u4ContextId,  
                                                   Y1564_ENABLED);
            break;

        case CLI_Y1564_DISABLE:

            i4RetStatus = Y1564CliSetModuleStatus (CliHandle, u4ContextId,  
                                                   Y1564_DISABLED);
            break;

        case CLI_Y1564_SLA_CREATE:

            i4RetStatus = Y1564CliSetSlaCreate (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_SLA_DELETE:

            i4RetStatus = Y1564CliSetSlaDelete (CliHandle, u4ContextId,
                                               CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_TRAF_PROF_CREATE:

            i4RetStatus = Y1564CliSetTrafProfCreate (CliHandle, u4ContextId,
                                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_TRAF_PROF_DELETE:

            i4RetStatus = Y1564CliSetTrafProfDelete (CliHandle, u4ContextId, 
                                                     CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_Y1564_SERVICE_CREATE:

            i4RetStatus = Y1564CliSetServiceCreate (CliHandle, u4ContextId,
                                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_SERVICE_DELETE:

            i4RetStatus = Y1564CliSetServiceDelete (CliHandle, u4ContextId, 
                                                     CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_Y1564_SAC_CREATE:

            /* args[0] - SAC Id
             * args[1] - Inforamtion Rate
             * args[2] - Frame-loss-ratio
             * args[3] - Frame-time-delay
             * args[4] - Frame-delay-variation
             * args[5] - Availability */

            i4RetStatus = Y1564CliSetSacCreate (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]),
                                                CLI_PTR_TO_U4 (args[1]),
                                                CLI_PTR_TO_U4 (args[2]),
                                                CLI_PTR_TO_U4 (args[3]),
                                                CLI_PTR_TO_U4 (args[4]),
                                                (UINT1 *) (args[5]));
            break;

        case CLI_Y1564_SAC_DELETE:

             /*args[0] - SAC Id */

            i4RetStatus = Y1564CliSetSacDelete (CliHandle, u4ContextId, 
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_NOTIFY_ENABLE:

            i4RetStatus = Y1564CliSetNotification (CliHandle, u4ContextId,
                                                   Y1564_SNMP_TRUE);              
            break;
        case  CLI_Y1564_NOTIFY_DISABLE:

            i4RetStatus = Y1564CliSetNotification (CliHandle, u4ContextId,
                                                   Y1564_SNMP_FALSE);   
            break;

        case CLI_Y1564_SLA_PERFORMANCE_DURATION:
            /* args[1] - Performance test Identifier
             * args[2] - Performance test duration */

            if (args[0] != NULL)
            {
                    if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                    {
                            CliPrintf (CliHandle, "\r%% Switch %s Does not "
                                            "exist.\r\n", args[0]);
                            i4RetStatus = CLI_FAILURE;
                            break;
                    }
            }
            else
            {
                    u4ContextId = Y1564_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = Y1564CliSetPerfDuration (CliHandle, u4ContextId,
                                                   CLI_PTR_TO_U4 (args[1]),
                                                   CLI_PTR_TO_U4 (args[2]));
            break;


        case CLI_Y1564_CONFIGURATION_TEST:

            /*  args [0] - Context Name
             *  args [1] - SLA Id
             *  args [2] - SLA Test Status (start/stop) */

            if (args[0] != NULL)
            { 
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID; 
            }
            i4RetStatus = Y1564CliSetConfigurationTest (CliHandle,  u4ContextId,
                                                        CLI_PTR_TO_U4 (args[1]),   
                                                        CLI_PTR_TO_U4 (args[2])); 
            break;

        case CLI_Y1564_PERFORMANCE_SLA_LIST:

            /*  args [0] - ContextId
             *  args [1] - Performance Id
             *  args [2] - SLA List */

            if (args[0] != NULL)
            {
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID;
            }

            if (CLI_STRLEN (args[2]) > Y1564_TWENTY)
            {
                CliPrintf (CliHandle, "\r%% SLA list exceeds the "
                           "Max Length limit\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            CLI_MEMSET (&gau1Y1564SlaList, 0, MAX_Y1564_SLA_LIST_SIZE);
            /*   If SLA list is given convert them to bitmap format
             *  and store them in gau1Y1564SlaList */
            if (CliStrToPortList ((UINT1 *) args[2], &gau1Y1564SlaList[0],
                                  Y1564_TWENTY,
                                  CFA_L2VLAN) == CLI_FAILURE)
            {

                CliPrintf (CliHandle, "\r%% Invalid SLA List\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = Y1564CliSetPerformanceSlaList (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_U4 (args[1]),
                                                     &gau1Y1564SlaList[0]);
            break;

        case CLI_Y1564_PERFORMANCE_SLA_LIST_DELETE:

            /*  args [0] - ContextId
             *  args [1] - Performance Id */

            if (args[0] != NULL)
            {
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = Y1564CliSetPerformanceSlaListDelete (CliHandle, u4ContextId,
                                                               CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_Y1564_PERFORMANCE_TEST:
             /*  args [0] - ContextId
             *  args [1] - Performance Id
             *  args [2] - Start/Stop */

            if (args[0] != NULL)
            {
               if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
               {
                       CliPrintf (CliHandle, "\r%% Switch %s Does not "
                                       "exist.\r\n", args[0]);
                       i4RetStatus = CLI_FAILURE;
                       break;
               }
            }

            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = Y1564CliSetPerformanceTest (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_U4 (args[1]),
                                                      CLI_PTR_TO_U4 (args[2]));
               
            break;
             
        case CLI_Y1564_REPORT_SLA:
        case CLI_Y1564_REPORT_SLA_TFTP:

            /*  args [0] - ContextId
             *  args [1] - Report Name or TFTP Path
             *  args [2] - SLA Id */

            if (args[0] != NULL)
            { 
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID; 
            }

            i4RetStatus = Y1564CliSaveConfReport (CliHandle, u4ContextId, u4Command,
                                                  (CHR1 *) args[1],
                                                  CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_Y1564_PERF_REPORT_SLA:
        case CLI_Y1564_PERF_REPORT_SLA_TFTP:

            /*args [0] - ContextId
             *args [1] - Report Name or TFTP Path
             *args [2] - PerfTestId */

            if (args[0] != NULL)
            {
                    if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                    {
                            CliPrintf (CliHandle, "\r%% Switch %s Does not "
                                            "exist.\r\n", args[0]);
                            i4RetStatus = CLI_FAILURE;
                            break;
                    }
            }

            else
            {
                    u4ContextId = Y1564_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = Y1564CliSavePerfReport (CliHandle, u4ContextId, u4Command,
                            (CHR1 *) args[1],
                            CLI_PTR_TO_U4 (args[2]));
            break;


        case CLI_Y1564_DELETE_REPORT_SLA:

             /* args [0] - Report Name */

            i4RetStatus = Y1564CliDeleteReport (CliHandle, (CHR1 *) args[0]);

            break;

        case CLI_Y1564_DEBUG:

            /* args [0] - ContextId 
             * args [1] - Debug Type
             * args [2] - Trc levels */

            if (args[2] != NULL)
            {
              i4Args = CLI_PTR_TO_I4 (args[2]);
              Y1564CliSetDebugLevel (CliHandle, i4Args);
            }
            if (args[0] != NULL)
            { 
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID; 
            }

            i4RetStatus =
                Y1564CliSetTraceOption (CliHandle, u4ContextId,
                                        CLI_PTR_TO_I4 (args[1]),
                                        Y1564_ENABLED);
            break;

        case CLI_Y1564_NO_DEBUG:

            /* args [0] - ContextId 
             * args [1] - Debug Type */

            if (args[0] != NULL)
            {
                if (Y1564VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = Y1564_DEFAULT_CONTEXT_ID; 
            }

            i4RetStatus =
                Y1564CliSetTraceOption (CliHandle, u4ContextId,
                                        CLI_PTR_TO_I4 (args[1]),
                                        Y1564_DISABLED);
            
           break;

        case CLI_Y1564_MAP_EVC:

           /* args[0] - EVC Id */
            i4RetStatus =
                Y1564CliSetSlaMapEvc (CliHandle, u4ContextId,
                                      (UINT4) i4ModeId,
                                       CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_NO_MAP_EVC:

            i4RetStatus =
                Y1564CliSetSlaUnMapEvc (CliHandle, u4ContextId,
                                        (UINT4) i4ModeId,
                                        u4EvcIndex);
            break;

#ifndef MEF_WANTED
        case CLI_Y1564_MAP_SERVICECONF:

           /* args[0] - Service Configuration Id */
            i4RetStatus =
                Y1564CliSetSlaMapServiceConfig (CliHandle, u4ContextId,
                                                (UINT4) i4ModeId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_NO_MAP_SERVICECONF:

           /* args[0] - Service Configuration Id */
            i4RetStatus =
                Y1564CliSetSlaUnMapServiceConfig (CliHandle, u4ContextId,
                                                  (UINT4) i4ModeId,
                                                   u4ServConfigId);
            break;
#endif
        case CLI_Y1564_MAP_TRAF_PROFILE:

           /* args[0] - Traffic Profile Id */

            i4RetStatus =
                Y1564CliSetSlaMapTrafficProfile (CliHandle, u4ContextId, 
                                                 (UINT4) i4ModeId,
                                                 CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_NO_MAP_TRAF_PROFILE:

            i4RetStatus =
                Y1564CliSetSlaUnMapTrafficProfile (CliHandle, u4ContextId, 
                                                  (UINT4) i4ModeId,
                                                  u4TrafProfId);
            break;

        case CLI_Y1564_MAP_SAC:

           /* args[0] - SAC Id */
            i4RetStatus = Y1564CliSetSlaMapSac (CliHandle, u4ContextId, 
                                                (UINT4) i4ModeId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_NO_MAP_SAC:

            i4RetStatus = Y1564CliSetSlaUnMapSac (CliHandle, u4ContextId, 
                                                (UINT4) i4ModeId,
                                                u4SacId);
            break;

        case CLI_Y1564_CFM_CFG:

           /* args[0] - MEG Id
            * args[1] - ME Id
            * args[2] - MEP Id */
            i4RetStatus = Y1564CliSetSlaMapCfmEntries (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]),
                                                       CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_Y1564_NO_CFM_CFG:

            i4RetStatus = Y1564CliSetSlaUnMapCfmEntries (CliHandle, u4ContextId,
                                                         (UINT4) i4ModeId,
                                                         u4SlaMeg,u4SlaMe, 
                                                         u4SlaMep);
            break;

        case CLI_Y1564_SLA_STEP:

           /* args[0] - Step Load Rate */
            i4RetStatus = Y1564CliSetSlaStepLoadRate (CliHandle, u4ContextId,
                                                      (UINT4) i4ModeId,
                                                      CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_SLA_DURATION:

           /* args[0] - SLA Duration */
            i4RetStatus = Y1564CliSetSlaDuration (CliHandle, u4ContextId,
                                                  (UINT4) i4ModeId,
                                                   CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_SLA_TRAFFIC_POLICING:
            
           /* args[0] - Traffic Policing Status */
            u4TrafficPolicing = CLI_PTR_TO_U4 (args[0]);
            
            if (u4TrafficPolicing == OSIX_ENABLED)
            { 
               i4RetStatus = Y1564CliSetTrafPolicingStatus 
                             (CliHandle, u4ContextId,
                              (UINT4) i4ModeId, Y1564_SNMP_TRUE);
            }
            else 
            {
                i4RetStatus = Y1564CliSetTrafPolicingStatus 
                    (CliHandle, u4ContextId, 
                     (UINT4) i4ModeId, Y1564_SNMP_FALSE);              
            }
            break;

        case CLI_Y1564_SLA_METER:

           /* args[0] - CIR Value
            * args[1] - CBS Value
            * args[2] - EIR Value
            * args[3] - EBS Value 
            * args[4] - Color aware
            * args[5] - Coupling Flag */

            i4RetStatus = Y1564CliSetMeterInfo (CliHandle, u4ContextId,
                                                     (UINT4) i4ModeId,
                                                      CLI_PTR_TO_U4 (args[0]),
                                                      CLI_PTR_TO_U4 (args[1]),
                                                      CLI_PTR_TO_U4 (args[2]),
                                                      CLI_PTR_TO_U4 (args[3]),
                                                      CLI_PTR_TO_U4 (args[4]),
                                                      CLI_PTR_TO_U4 (args[5]));
             break;

       case CLI_Y1564_TRAF_PROF_DIRECTION:

           /* args[0] - Traffic Profile Direction */

            i4RetStatus = Y1564CliSetTrafProfDirection (CliHandle ,u4ContextId, 
                                                        (UINT4) i4ModeId,
                                                        CLI_PTR_TO_U4 (args[0]));
            break;

       case CLI_Y1564_TRAF_PROF_PKT_SIZE:

           /* args[0] - packet Size */
            i4RetStatus = Y1564CliSetPacketSize (CliHandle ,u4ContextId, 
                                                 (UINT4) i4ModeId,
                                                 CLI_PTR_TO_U4 (args[0]));
            break;

       case CLI_Y1564_TRAF_PROF_PKT_SIZE_EMIX:

           /* args[0] - Emix packet Size */
            i4RetStatus = Y1564CliSetEmixPacketSize (CliHandle, u4ContextId, 
                                                     (UINT4) i4ModeId,
                                                     (UINT1 *) (args[0]));
            break;


        case CLI_Y1564_TRAF_PROF_PAYLOAD:

           /* args[0] - Traffic Profile Payload */
            i4RetStatus = Y1564CliSetPayload (CliHandle, u4ContextId, 
                                              (UINT4) i4ModeId,
                                              (UINT1 *) args[0]);
            break;

        case CLI_Y1564_TRAF_PROF_PCP:

           /* args[0] - Priority Code Point */
            i4RetStatus = Y1564CliSetPCP (CliHandle, u4ContextId, 
                                          (UINT4) i4ModeId,
                                          CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_Y1564_TEST_SELECTOR:

           /* args[0] - Test Selector Value */
            i4RetStatus = Y1564CliSetTestSelector (CliHandle, u4ContextId, 
                                                   (UINT4) i4ModeId, 
                                                   CLI_PTR_TO_U4 (args[0]));
            break;

        default:
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > Y1564_ZERO) && (u4ErrCode < CLI_Y1564_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaY1564CliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    Y1564_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;

}

/****************************************************************************
 * Function    :  cli_process_y1564_show_cmd
 * Description :  This function is exported to CLI module to handle the
 *                       Y1564 show commands to take the corresponding action.
 *
 * Input       :  Variable arguments
 *
 * Output      :  None 
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_y1564_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_Y1564_MAX_COMMANDS];
    UINT1              *pu1ContextName = NULL;   
    UINT4               u4ContextId = Y1564_ZERO;
    UINT4               u4CurrContextId = 0xffffffff;
    INT4                i4RetStatus = Y1564_ZERO;
    INT4                i4Inst = 0;
    INT1                i1Argno = Y1564_ZERO;


    CliRegisterLock (CliHandle, Y1564ApiLock, Y1564ApiUnLock); 

    Y1564_LOCK ();

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    UNUSED_PARAM (i4Inst);

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     *       After the u4IfIndex. (ie) In all the cli commands we
     *       are passing IfIndex as the first argument in
     *       variable argument list. Like that as the second argument
     *       we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    MEMSET (args, 0, sizeof (args));

   /* Walk through the rest of arguments and store in args array */
    while (1)
    {
        args[i1Argno++] = va_arg (ap, UINT1 *);
        if (i1Argno == Y1564_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);


    /* Display required info for the appropriate context id (If it is
     * given pu1ContextName by user) or display info for all contexts
     * available (if context id is not given) */

    while (Y1564CliGetContextInfoForShowCmd (CliHandle, pu1ContextName,
                                             u4CurrContextId, &u4ContextId) == OSIX_SUCCESS)
    {
        switch (u4Command)
        {
            case CLI_Y1564_SHOW_SLA:

                i4RetStatus = Y1564CliShowSlaTable (CliHandle, u4ContextId, 
                                                    CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_TRAF_PROF:

                i4RetStatus = Y1564CliShowTrafProfTable (CliHandle, u4ContextId, 
                                                         CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_SAC:

                i4RetStatus = Y1564CliShowSacTable (CliHandle, u4ContextId, 
                                                    CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_SERVICE_CONF:
                i4RetStatus = Y1564CliShowServiceConfTable (CliHandle, u4ContextId, 
                                                            CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_PERF_TEST_CONF:

                i4RetStatus = Y1564CliShowPerfTestTable (CliHandle, u4ContextId, 
                                                         CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_SLA_CT_REPORT:
                i4RetStatus = Y1564CliShowConfTestReport (CliHandle, u4ContextId, 
                                                          CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_SLA_PT_REPORT:
                i4RetStatus = Y1564CliShowPerfTestReport (CliHandle, u4ContextId, 
                                                          CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_Y1564_SHOW_GLOBAL:

                i4RetStatus = Y1564CliShowGlobalInfo (CliHandle, u4ContextId);
                break;

            default:
                i4RetStatus = CLI_FAILURE;
                break;
        }

        /* If context name is given by user , break from loop as display
         *          * is over */
        if (pu1ContextName != NULL)
        {
            break;
        }

        u4CurrContextId = u4ContextId;
    }

    CLI_SET_ERR (0);

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    Y1564_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}


/*******************************************************************************
 * FUNCTION NAME    : Y1564CliGetContextInfoForShowCmd                         *
 *                                                                             *
 * DESCRIPTION      : This Routine validates the string input for the switch   *
 *                    name or loop through for all the context. It also checks *
 *                    if Y1564 is Started or not.                               *
 *                                                                             *
 * INPUT            : u4CurrContextId - Context Identifier                     *
 *                    pu1ContextName - Entered Context Name                    *
 *                    u4Command      - Command Identifier                      *
 *                                                                             *
 * OUTPUT           : pu4ContextId - Next Context Id or                        *
 *                                   Context Id of the entered Context name    *
 *                                                                             *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE                                  *
 *                                                                             *
 *******************************************************************************/   
INT4
Y1564CliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                 UINT4 u4CurrContextId, UINT4 *pu4ContextId)
{

 /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (Y1564VcmIsSwitchExist (pu1ContextName, pu4ContextId)
            != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsY1564ContextTable (u4CurrContextId,
                                               pu4ContextId) != SNMP_SUCCESS)
        {
            if (Y1564UtilIsY1564Started (*pu4ContextId) != OSIX_TRUE)
            {
               return OSIX_FAILURE;
            }
            return OSIX_FAILURE; 
        }
    }

    return OSIX_SUCCESS;
}
/********************************************************************************   
 * Function    :  Y1564CliSetSystemControl                                      *
 *                                                                              *
 * Description :  This function sets the system control status of               *
 *                Y1564 module depending on the input parameter                 *
 *                                                                              *
 * Input       :  CliHandle             - Cli context                           *
 *                u4ContextId           - Context Identifier                    *
 *                i4SystemControl       - System Control Status                 *
 *                (Y1564_START/Y1564_SHUTDOWN)                                  *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/
INT4
Y1564CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, 
                          INT4 i4SystemControl)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564ContextSystemControl (&u4ErrorCode, u4ContextId,
                                              i4SystemControl) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ContextSystemControl (u4ContextId, i4SystemControl) 
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/********************************************************************************   
 * Function    : Y1564CliSetModuleStatus                                        *
 *                                                                              *
 * Description :  This function sets the system control status of               *
 *                Y1564 module depending on the input parameter                 *
 *                                                                              *
 * Input       :  CliHandle              - Cli context                          *
 *                u4ContextId            - Context Identifier                   *
 *                i4ModuleStatus         - Module Status                        *
 *                (Y1564_ENABLED/Y1564_DISABLED)                                *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/
INT4
Y1564CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4ModuleStatus)    
{
   UINT4               u4ErrorCode = 0;

   if (nmhTestv2FsY1564ContextModuleStatus (&u4ErrorCode, u4ContextId,
                                            i4ModuleStatus) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }

   if (nmhSetFsY1564ContextModuleStatus (u4ContextId, i4ModuleStatus) !=
       SNMP_SUCCESS)
   {
      CLI_FATAL_ERROR (CliHandle);
      return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}
/********************************************************************************
 * Function    :  Y1564CliSetSlaCreate                                          *
 *                                                                              *
 * Description :  This function creates a sla if the sla is not already present *
 *                                                                              *
 * Input       :  CliHandle     - Cli context                                   *
 *                u4ContextId   - Context Identifier                            *
 *                u4SlaId       - SLA Identifier                                *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/
INT4
Y1564CliSetSlaCreate (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{
    INT4                i4RowStatus = -1;
    UINT4               u4ErrorCode = Y1564_ZERO;

    /* If Entry is already present then only change the mode 
     * else create the new entry */
    if (nmhGetFsY1564SlaRowStatus (u4ContextId, u4SlaId,
                                   &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FsY1564SlaRowStatus (&u4ErrorCode, u4ContextId,
                                          u4SlaId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsY1564SlaRowStatus (u4ContextId , u4SlaId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (CliChangePath ("sla-1564") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into sla "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }
    /* If the mode is changed successfully then set the sla id */
    CLI_SET_SLA_ID ((INT4) u4SlaId);

    return CLI_SUCCESS;
}
/********************************************************************************
 * Function    :  Y1564CliSetSlaDelete                                          *
 *                                                                              *
 * Description :  This function deletes a sla if the sla entry  is present in   *
 *                SlaTable                                                      *
 *                                                                              *
 * Input       :  CliHandle     - Cli context                                   *
 *                u4ContextId   - Context Identifier                            *
 *                u4SlaId       - SLA Identifier                                *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/

INT4
Y1564CliSetSlaDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SlaRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SlaId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaRowStatus (u4ContextId, u4SlaId, DESTROY) 
                                   != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/********************************************************************************
 * Function    : Y1564CliSetTrafProfCreate                                      *
 *                                                                              *
 * Description :  This function creates a Traffic Profile entry if the          *
 *                traffic profile is not already present                        *
 *                                                                              *
 * Input       :  CliHandle     - Cli context                                   *
 *                u4ContextId   - Context Identifier                            *
 *                u4TrafProfId  - Traffic Profile Identifier                    *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/
INT4
Y1564CliSetTrafProfCreate (tCliHandle CliHandle, UINT4 u4ContextId, 
                           UINT4 u4TrafProfId)
{
    INT4                i4RowStatus = -1;
    UINT4               u4ErrorCode = Y1564_ZERO;

    /* If Entry is already present then only change the mode 
     * else create the new entry */
    if (nmhGetFsY1564TrafProfRowStatus (u4ContextId, u4TrafProfId,
                                        &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FsY1564TrafProfRowStatus (&u4ErrorCode, u4ContextId, 
                                               u4TrafProfId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsY1564TrafProfRowStatus (u4ContextId, u4TrafProfId, CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (CliChangePath ("traffic-profile-1564") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into traffic profile"
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    /* If the mode is changed successfully then set the sla id */
    CLI_SET_TRAF_PROF_ID ((INT4) u4TrafProfId);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetTrafProfDelete                                 *
 *                                                                          *
 * Description :  This function Deletes a traffic profile entry if present  *
 *                                                                          *                                     
 * Input       :  CliHandle              - Cli context                      *
 *                u4ContextId - Context Identifier                          *
 *                u4TrafProfId - Traffic profile identifier                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetTrafProfDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafProfId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564TrafProfRowStatus (&u4ErrorCode, u4ContextId,
                                           u4TrafProfId,
                                           DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564TrafProfRowStatus (u4ContextId, 
                                        u4TrafProfId, DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/********************************************************************************
 * Function    :  Y1564CliSetServiceCreate                                      *
 *                                                                              *
 * Description :  This function creates a service configuration entry if the    *
 *                service config id is not already present                      *
 *                                                                              *
 * Input       :  CliHandle     - Cli context                                   *
 *                u4ContextId   - Context Identifier                            *
 *                u4ServConfId  - Service configuration identifier              *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/
INT4
Y1564CliSetServiceCreate (tCliHandle CliHandle, UINT4 u4ContextId, 
                          UINT4 u4ServConfId)
{

    UINT4               u4ErrorCode = Y1564_ZERO;
    INT4                i4RowStatus = -1;

    /* If Entry is already present then only change the mode 
     * else create the new entry */
    if (nmhGetFsY1564ServiceConfRowStatus (u4ContextId, u4ServConfId,
                                           &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FsY1564ServiceConfRowStatus (&u4ErrorCode, u4ContextId, 
                                                  u4ServConfId, CREATE_AND_WAIT) 
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsY1564ServiceConfRowStatus (u4ContextId, u4ServConfId, 
                                               CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (CliChangePath ("service-config-1564") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into service config"
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    CLI_SET_SERV_CONFIG_ID ((INT4) u4ServConfId);

    return CLI_SUCCESS;
}

/********************************************************************************
 * Function    :  Y1564CliSetServiceDelete                                      *
 *                                                                              *
 * Description :  This function deletes a service configuration entry if the    *
 *                service config id is present                                  *
 *                                                                              *
 * Input       :  CliHandle     - Cli context                                   *
 *                u4ContextId   - Context Identifier                            *
 *                u4ServConfId  - Service configuration identifier              *
 *                                                                              *
 * Output      :  None                                                          *
 *                                                                              *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                       *
 *******************************************************************************/

INT4
Y1564CliSetServiceDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4ServConfId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564ServiceConfRowStatus(&u4ErrorCode, u4ContextId,
                                             u4ServConfId,
                                             DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ServiceConfRowStatus(u4ContextId, u4ServConfId, 
                                          DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSacCreate                                      *
 * Description :  This function creates a SAC if the Entry is not present   *
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId - Context Identifier                          *        
 *                u4SacId - SAC Identifier                                  *
 *                u4SacIr - Information rate in Kbps                        *
 *                u4SacFlr - Frame loss ratio in percentage                 *
 *                u4SacFtd - Frame transfer delay in Milliseconds           *
 *                u4SacFdv - Frame delay variation in Milliseconds          *
 *                pu1Availablity - Avaiability value in percentage          *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSacCreate (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId,
                      UINT4 u4SacIr, UINT4 u4SacFlr, UINT4 u4SacFtd,
                      UINT4 u4SacFdv, UINT1 *pu1Availablity)
{
    UINT4                      u4ErrorCode = Y1564_ZERO;
    INT4                       i4RowStatus = -1;
    tSNMP_OCTET_STRING_TYPE    Availability;

    MEMSET (&Availability, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    Availability.pu1_OctetList = pu1Availablity;
    Availability.i4_Length = (INT4) STRLEN (pu1Availablity);

    if (nmhGetFsY1564SacRowStatus (u4ContextId, u4SacId,
                                   &i4RowStatus) != SNMP_SUCCESS)
    {
        /* Create SAC Entry */
        if (nmhTestv2FsY1564SacRowStatus (&u4ErrorCode, u4ContextId,
                                          u4SacId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsY1564SacRowStatus (u4ContextId, u4SacId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Update information rate */
    if (nmhTestv2FsY1564SacInfoRate (&u4ErrorCode, u4ContextId,
                                     u4SacId, (INT4) u4SacIr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacInfoRate (u4ContextId, u4SacId, 
                                  (INT4) u4SacIr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Update Frame Loss ratio */
    if (nmhTestv2FsY1564SacFrLossRatio (&u4ErrorCode, u4ContextId,
                                        u4SacId, (INT4) u4SacFlr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacFrLossRatio (u4ContextId, u4SacId, (INT4) u4SacFlr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }


   /* Update Frame Tx delay */
    if (nmhTestv2FsY1564SacFrTransDelay (&u4ErrorCode, u4ContextId,
                                      u4SacId, (INT4) u4SacFtd) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacFrTransDelay (u4ContextId, u4SacId, (INT4) u4SacFtd) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Update Frame delay variation */
    if (nmhTestv2FsY1564SacFrDelayVar (&u4ErrorCode, u4ContextId,
                                       u4SacId, (INT4) u4SacFdv) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacFrDelayVar (u4ContextId, u4SacId, (INT4) u4SacFdv) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SacAvailability (&u4ErrorCode, u4ContextId,
                                         u4SacId, &Availability) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacAvailability (u4ContextId, u4SacId, &Availability) 
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Make the SAC Entry Active */
    if (nmhTestv2FsY1564SacRowStatus (&u4ErrorCode, u4ContextId,
                                      u4SacId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacRowStatus (u4ContextId, u4SacId, ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSacDelete                                      *
 *                                                                          *
 * Description :  This function deletes SAC Entry                           *             
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId - Context Identifier                          *
 *                u4SacId - SAC Identifier                                  *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSacDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SacRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SacId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SacRowStatus (u4ContextId, u4SacId, DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
/****************************************************************************
 * Function    :  Y1564CliSetNotification                                   *
 *                                                                          *
 * Description :  This function sets the TRAP status                        *             
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId  - Context Identifier                         *
 *                i4TrapStatus - Trap Status                                *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetNotification (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4TrapStatus)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
   
    if (nmhTestv2FsY1564ContextTrapStatus (&u4ErrorCode, u4ContextId,
                                           i4TrapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE ;
    }

    if (nmhSetFsY1564ContextTrapStatus (u4ContextId, i4TrapStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetPerfDuration                                   *
 *                                                                          *
 * Description :  This function sets the performance duration for           * 
 *                performance test.                                         *
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId  - Context Identifier                         *
 *                u4PerfTestDuration  - Performance Duration                *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPerfDuration (tCliHandle CliHandle, UINT4 u4ContextId, 
                         UINT4 u4PerfTestId, UINT4 u4PerfTestDuration)
{
    UINT4           u4ErrorCode = Y1564_ZERO;
    INT4            i4RowStatus = -1;

    if (nmhGetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                               &i4RowStatus) != SNMP_FAILURE)
    {

            if (nmhTestv2FsY1564PerformanceTestDuration (&u4ErrorCode, u4ContextId,
                                                         u4PerfTestId,
                                                         (INT4) u4PerfTestDuration)
                                                         != SNMP_SUCCESS)
            {
                    return CLI_FAILURE ;
            }

            if (nmhSetFsY1564PerformanceTestDuration (u4ContextId, u4PerfTestId,
                                                      (INT4) u4PerfTestDuration)
                                                      != SNMP_SUCCESS)
            {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
            }
    }
    else
    {
	    if ((Y1564CxtIsY1564Started (u4ContextId)) != OSIX_TRUE)
	    {
		    CliPrintf (CliHandle, "\r%% Y1564 module not started\r\n");
		    return CLI_FAILURE;
	    }
	    else
	    {	
		    CliPrintf (CliHandle, "\r%% Unable to configure performance test "
				    "duration for Perf Id %d \r\n", u4PerfTestId);
		    return CLI_FAILURE;
	    }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetTestSelector                                   *
 *                                                                          *
 * Description :  This function sets the Test Selector value                *
 *                value for a SLA                                           *
 *                                                                          *
 * Input       :  CliHandle      - Cli context                              *
 *                u4ContextId    - Context Identifier                       *
 *                u4SlaId        - SLA Identifier                           *
 *                u4TestSelector - Test selector value                      *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetTestSelector (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4SlaId, UINT4 u4TestSelector)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    /* Set the test selector value */
    if (nmhTestv2FsY1564SlaTestSelector (&u4ErrorCode, u4ContextId, 
                                         u4SlaId, (INT4) u4TestSelector) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsY1564SlaTestSelector (u4ContextId, u4SlaId, 
                                      (INT4) u4TestSelector) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to configure test selector values \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetConfigurationTest                              *
 *                                                                          *
 * Description :  This function sets the configuration test status, step    * 
 *                load duration, configuration test selector for a          *
 *                SLA                                                       *
 *                                                                          *
 * Input       :  CliHandle      - Cli context                              *
 *                u4ContextId    - Context Identifier                       *
 *                u4SlaId        - SLA Identifier                           *
 *                u4SlaStatus    - Configuration test SLA status            *
 *                                 Start/Stop                               *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetConfigurationTest (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4SlaId, UINT4 u4SlaStatus)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    /* Set the test status */
    if (nmhTestv2FsY1564SlaTestStatus (&u4ErrorCode, u4ContextId,
                                       u4SlaId, (INT4) u4SlaStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsY1564SlaTestStatus (u4ContextId, u4SlaId, 
                                    (INT4) u4SlaStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetPerformanceSlaList                             *
 *                                                                          *
 * Description :  This function sets the performance test status, SLA       * 
 *                Lists selected for performance test.                      *
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId    - Context Identifier                       *
 *                u4PerfTestId   - Performance Test Identifier              *
 *                pu1SlaList     - SLA Lists                                *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPerformanceSlaList (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4PerfTestId, UINT1 * pu1SlaList)
{
    tSNMP_OCTET_STRING_TYPE SlaList;
    UINT4               u4ErrorCode = Y1564_ZERO;
    INT4                i4RowStatus = -1;

    MEMSET (&SlaList, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Convert the string to OCTET_STRING */
    SlaList.pu1_OctetList = &pu1SlaList[Y1564_ZERO];
    SlaList.i4_Length = MAX_Y1564_SLA_LIST_SIZE;

    MEMCPY (SlaList.pu1_OctetList, pu1SlaList, MAX_Y1564_SLA_LIST_SIZE);

    if (nmhGetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                               &i4RowStatus) != SNMP_SUCCESS)
    {

        if (nmhTestv2FsY1564PerformanceTestRowStatus (&u4ErrorCode, u4ContextId,
                                                      u4PerfTestId, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                                   CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsY1564PerformanceTestSlaList (&u4ErrorCode, u4ContextId,
                                                u4PerfTestId, &SlaList) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564PerformanceTestSlaList (u4ContextId, u4PerfTestId,
                                             &SlaList) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564PerformanceTestRowStatus (&u4ErrorCode, u4ContextId,
                                                  u4PerfTestId, ACTIVE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                               ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetPerformanceSlaListDelete                       *
 *                                                                          *
 * Description :  This function Delete the performance test table, for      * 
 *                Performance Id.                                           *
 *                                                                          *
 * Input       :  CliHandle      - Cli context                              *
 *                u4ContextId    - Context Identifier                       *
 *                u4PerfTestId   - Performance Test Identifier              *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPerformanceSlaListDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                                     UINT4 u4PerfTestId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564PerformanceTestRowStatus (&u4ErrorCode, u4ContextId,
                                                  u4PerfTestId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                               DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Y1564UtilDelPerfTestResultEntry (u4ContextId, u4PerfTestId, Y1564_ZERO);
    return CLI_SUCCESS;
}


/****************************************************************************
 * Function    :  Y1564CliSetPerformanceTest                                *
 *                                                                          *
 * Description :  This function sets the performance test status, SLA       *
 *                Lists selected for performance test.                      *
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId    - Context Identifier                       *
 *                u4PerfTestId   - Performance Test Identifier              *
 *                u4PerfStatus   - Performance test status                  *
 *                                 Start/Stop                               *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPerformanceTest (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4PerfTestId, UINT4 u4PerfStatus)
{
     INT4                i4RowStatus = -1;
     UINT4               u4ErrorCode = Y1564_ZERO;

     if (nmhGetFsY1564PerformanceTestRowStatus (u4ContextId, u4PerfTestId,
                                                &i4RowStatus) != SNMP_FAILURE)
     {
         if (nmhTestv2FsY1564PerformanceTestStatus (&u4ErrorCode, u4ContextId,
                                                    u4PerfTestId, (INT4) u4PerfStatus)
             != SNMP_SUCCESS)
         {
             return CLI_FAILURE;
         }
         if (nmhSetFsY1564PerformanceTestStatus (u4ContextId, u4PerfTestId,
                                                 (INT4) u4PerfStatus)
             != SNMP_SUCCESS)
         {
             return CLI_FAILURE;
         }
         return CLI_SUCCESS;
     }
     else
     {
	     /* Check the system status. Configuration will be allowed
	      * only when Y1564 is started in the context*/
	     if ((Y1564CxtIsY1564Started (u4ContextId)) != OSIX_TRUE)
	     {
		     CliPrintf (CliHandle, "\r%% Y1564 module not started\r\n");
		     return CLI_FAILURE;
	     }

	     CliPrintf (CliHandle, "\r%% Unable to start performance test \r\n");
	     return CLI_FAILURE;
     }
}

/****************************************************************
 * Function    :  Y1564CliSetDebugLevel                         *
 * Description :                                                *
 * Input       :  CliHandle, i4CliDebugLevel                    *
 * Output      :  None                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                       *
 ***************************************************************/
INT4
Y1564CliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    gu4Y1564TrcLvl = 0;

    UNUSED_PARAM (CliHandle);

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        gu4Y1564TrcLvl =
            Y1564_INIT_SHUT_TRC | 
            Y1564_MGMT_TRC      | 
            Y1564_CTRL_TRC      |
            Y1564_RESOURCE_TRC  | 
            Y1564_TIMER_TRC     | 
            Y1564_CRITICAL_TRC  |
            Y1564_Y1731_TRC     | 
            Y1564_TEST_TRC      |
            Y1564_SESSION_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        gu4Y1564TrcLvl = 
             Y1564_INIT_SHUT_TRC |
             Y1564_CTRL_TRC      |
             Y1564_RESOURCE_TRC  |
             Y1564_TIMER_TRC     |
             Y1564_CRITICAL_TRC  |
             Y1564_Y1731_TRC     |
             Y1564_TEST_TRC      |
             Y1564_SESSION_TRC;
                     
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        gu4Y1564TrcLvl = Y1564_INIT_SHUT_TRC | 
                         Y1564_CRITICAL_TRC  | 
                         Y1564_Y1731_TRC     |
                         Y1564_TEST_TRC      |
                         Y1564_SESSION_TRC   |
                         Y1564_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        gu4Y1564TrcLvl = Y1564_INIT_SHUT_TRC |
                         Y1564_Y1731_TRC     |
                         Y1564_TEST_TRC      |
                         Y1564_SESSION_TRC   |
                         Y1564_CRITICAL_TRC  |
                         Y1564_RESOURCE_TRC; 
                   
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        gu4Y1564TrcLvl = Y1564_INIT_SHUT_TRC |
                         Y1564_CRITICAL_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4Y1564TrcLvl =  Y1564_INIT_SHUT_TRC | 
                          Y1564_TEST_TRC      |
                          Y1564_SESSION_TRC   |
                          Y1564_CRITICAL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
/*****************************************************************************
 * Function    :  Y1564CliSetTraceOption                                     *
 *                                                                           *
 * Description :  This function configues the trace option for Y1564 module  *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
INT4
Y1564CliSetTraceOption (tCliHandle CliHandle, UINT4 u4ContextId, 
                        INT4 i4DebugType, INT4 i4Action)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
    UINT4               u4Level = Y1564_ZERO;
    INT4                i4Level = Y1564_ZERO;

    if (nmhGetFsY1564ContextTraceOption (u4ContextId, &u4Level) == 
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4Action ==  Y1564_ENABLED)
    {
        i4DebugType = i4DebugType | (INT4) u4Level;
    } 
    else
    {
        i4Level = (INT4) u4Level;
        i4Level &= (~(i4DebugType));

        if (i4Level == Y1564_ZERO)
        {
            i4Level = Y1564_CRITICAL_TRC;
        }

        i4DebugType = i4Level;
    }

    if (nmhTestv2FsY1564ContextTraceOption (&u4ErrorCode, u4ContextId, 
                                            (UINT4) i4DebugType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsY1564ContextTraceOption (u4ContextId, (UINT4) i4DebugType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaMapEvc                                      *
 *                                                                          *
 * Description :  This function maps a EVC index to a SLA                   *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4EvcIndex  - EVC Index                                   *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaMapEvc (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4EvcIndex)
{
    UINT4               u4ErrorCode = Y1564_ZERO;


    if (nmhTestv2FsY1564SlaEvcIndex (&u4ErrorCode, u4ContextId,
                                    u4SlaId, (INT4) u4EvcIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaEvcIndex (u4ContextId, u4SlaId, (INT4) u4EvcIndex) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaUnMapEvc                                    *
 *                                                                          *
 * Description :  This function de-associate EVC index from SLA             *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4EvcIndex  - EVC Index                                   *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaUnMapEvc (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4EvcIndex)
{
    UINT4               u4ErrorCode = Y1564_ZERO;


    if (nmhTestv2FsY1564SlaEvcIndex (&u4ErrorCode, u4ContextId,
                                    u4SlaId, (INT4) u4EvcIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaEvcIndex (u4ContextId, u4SlaId, (INT4) u4EvcIndex) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifndef MEF_WANTED
/****************************************************************************
 * Function    :  Y1564CliSetSlaMapServiceConfig                            *
 *                                                                          *
 * Description :  This function maps a ServiceConfig index to a SLA         *
 *                                                                          *
 * Input       :  CliHandle      - Cli context                              *
 *                u4ContextId    - Context Identifier                       *
 *                u4SlaId        - SLA Identifier                           *
 *                u4ServConfigId - Service Config Index                     *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaMapServiceConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4SlaId, UINT4 u4ServConfigId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;


    if (nmhTestv2FsY1564SlaServiceConfId (&u4ErrorCode, u4ContextId,
                                          u4SlaId, u4ServConfigId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaServiceConfId (u4ContextId, u4SlaId, u4ServConfigId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaUnMapServiceConfig                          *
 *                                                                          *
 * Description :  This function de-associate ServiceConfig Id from SLA      *
 *                                                                          *
 * Input       :  CliHandle      - Cli context                              *
 *                u4ContextId    - Context Identifier                       *
 *                u4SlaId        - SLA Identifier                           *
 *                u4ServConfigId - Service Config Index                     *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaUnMapServiceConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4SlaId, UINT4 u4ServConfigId)
{
    tSlaInfo        *pSlaEntry = NULL;
    UINT4            u4ErrorCode = Y1564_ZERO;

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry %d is "
                           "NULL \r\n", __FUNCTION__, u4SlaId);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaServiceConfId (&u4ErrorCode, u4ContextId,
                                          u4SlaId, pSlaEntry->u2SlaServiceConfId) 
                                         != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap Service Configuration Id from SLA , the value can be assigned to Zero */
    u4ServConfigId = Y1564_ZERO;

    if (nmhSetFsY1564SlaServiceConfId (u4ContextId, u4SlaId, u4ServConfigId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif

/****************************************************************************
 * Function    :  Y1564CliSetSlaMapTrafficProfile                           * 
 * Description :  This function maps a traffic profile to a SLA             *
 *                                                                          *
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4SlaId      - SLA Identifier                             *
 *                u4TrafProfId - Traffic profile Identifier                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaMapTrafficProfile (tCliHandle CliHandle,UINT4 u4ContextId,
                                UINT4 u4SlaId, UINT4 u4TrafProfId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SlaTrafProfileId (&u4ErrorCode,
                                          u4ContextId, u4SlaId,
                                          u4TrafProfId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaTrafProfileId (u4ContextId, u4SlaId, u4TrafProfId)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaUnMapTrafficProfile                         * 
 * Description :  This function de-associate traffic profile from  SLA      *
 *                                                                          *
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4SlaId      - SLA Identifier                             *
 *                u4TrafProfId - Traffic profile Identifier                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaUnMapTrafficProfile (tCliHandle CliHandle,UINT4 u4ContextId,
                                UINT4 u4SlaId, UINT4 u4TrafProfId)
{
    tSlaInfo        *pSlaEntry = NULL;    
    UINT4            u4ErrorCode = Y1564_ZERO;

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry %d is "
                           "NULL \r\n", __FUNCTION__, u4SlaId);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaTrafProfileId (&u4ErrorCode,
                                          u4ContextId, u4SlaId,
                                          pSlaEntry->u4SlaTrafProfId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap Traffic Profile Id from SLA , the value can be assigned to Zero */
    u4TrafProfId = Y1564_ZERO;

    if (nmhSetFsY1564SlaTrafProfileId (u4ContextId, u4SlaId, u4TrafProfId)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Y1564UtilDelConfTestResultEntry (u4ContextId, u4SlaId);

    return CLI_SUCCESS;
}
/****************************************************************************
 * Function    :  Y1564CliSetSlaMapSac                                      * 
 * Description :  This function maps a traffic profile to a SLA             *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4SacId     - SAC  Identifier                             *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaMapSac (tCliHandle CliHandle, UINT4 u4ContextId, 
                      UINT4 u4SlaId, UINT4 u4SacId)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SlaSacId (&u4ErrorCode, u4ContextId, u4SlaId,
                                  u4SacId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaSacId (u4ContextId, u4SlaId, u4SacId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaUnMapSac                                    * 
 * Description :  This function de-associate a traffic profile from SLA     *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4SacId     - SAC  Identifier                             *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaUnMapSac (tCliHandle CliHandle, UINT4 u4ContextId, 
                      UINT4 u4SlaId, UINT4 u4SacId)
{
    tSlaInfo        *pSlaEntry = NULL;
    UINT4            u4ErrorCode = Y1564_ZERO;

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_MGMT_TRC |
                           Y1564_ALL_FAILURE_TRC, " %s : SLA Entry %d is "
                           "NULL \r\n", __FUNCTION__, u4SlaId);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaSacId (&u4ErrorCode, u4ContextId, u4SlaId,
                                  pSlaEntry->u4SlaSacId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap SAC Id from SLA , the value can be assigned to Zero */

    u4SacId = Y1564_ZERO;

    if (nmhSetFsY1564SlaSacId (u4ContextId, u4SlaId, u4SacId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Y1564UtilDelConfTestResultEntry (u4ContextId, u4SlaId);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaMapCfmEntries                               *
 * Description :  This function associates CFM entries to a SLA             *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          * 
 *                u4SacId - service acceptance criteria Identifier          *
 *                u4SlaMeg - Meg Identifier                                 *
 *                u4SlaMe  - Me Identifier                                  *
 *                u4SlaMep - Mep Identifier                                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId, 
                             UINT4 u4SlaId, UINT4 u4SlaMeg, UINT4 u4SlaMe, 
                             UINT4 u4SlaMep)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsY1564SlaMEG (&u4ErrorCode, u4ContextId,
                                u4SlaId, u4SlaMeg) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaMEG (u4ContextId, u4SlaId, u4SlaMeg) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaME (&u4ErrorCode, u4ContextId, 
                               u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaME (u4ContextId, u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaMEP (&u4ErrorCode, u4ContextId, 
                                u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaMEP (u4ContextId, u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
/****************************************************************************
 * Function    :  Y1564CliSetSlaUnMapCfmEntries                             *
 * Description :  This function de-associate the CFM entries from  SLA      *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          * 
 *                u4SacId - service acceptance criteria Identifier          *
 *                u4SlaMeg - Meg Identifier                                 *
 *                u4SlaMe  - Me Identifier                                  *
 *                u4SlaMep - Mep Identifier                                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaUnMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId, 
                               UINT4 u4SlaId, UINT4 u4SlaMeg, UINT4 u4SlaMe, 
                               UINT4 u4SlaMep)
{
    tSlaInfo           *pSlaEntry = NULL;
    UINT4               u4ErrorCode = Y1564_ZERO;
    UINT4               u4TmpSlaMeg = Y1564_ZERO;
    UINT4               u4TmpSlaMe = Y1564_ZERO;
    UINT4               u4TmpSlaMep = Y1564_ZERO;

    /* Inside this function Context Id and SlaEntry are validated */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);

    if (pSlaEntry == NULL)
    {
        Y1564_CONTEXT_TRC (u4ContextId, Y1564_MGMT_TRC | 
                           Y1564_ALL_FAILURE_TRC,"%s : SLA Entry not present"
                           "for %d\r\n", __FUNCTION__, u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return CLI_FAILURE;
    }

    u4TmpSlaMeg = pSlaEntry->u4SlaMegId;
    u4TmpSlaMe = pSlaEntry->u4SlaMeId;
    u4TmpSlaMep = pSlaEntry->u4SlaMepId;

    if (nmhTestv2FsY1564SlaMEG (&u4ErrorCode, u4ContextId,
                                u4SlaId, u4SlaMeg) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap MEG from SLA , the value can be assigned to Zero */
    u4SlaMeg = Y1564_ZERO;

    if (nmhSetFsY1564SlaMEG (u4ContextId, u4SlaId, u4SlaMeg) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaME (&u4ErrorCode, u4ContextId, 
                               u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap ME from SLA , the value can be assigned to Zero */
    u4SlaMe = Y1564_ZERO;

    if (nmhSetFsY1564SlaME (u4ContextId, u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564SlaMEP (&u4ErrorCode, u4ContextId, 
                                u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

   /* To unmap ME from SLA , the value can be assigned to Zero */
    u4SlaMep = Y1564_ZERO;

    if (nmhSetFsY1564SlaMEP (u4ContextId, u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (Y1564PortMepDeRegister (u4ContextId, u4TmpSlaMeg, u4TmpSlaMe, u4TmpSlaMep) 
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : Failed to De-register"
                           "with ECFM/Y.1731 for MEP %d ME %d MEG %d"
                           "\r\n", __FUNCTION__, u4TmpSlaMep, u4TmpSlaMe, 
                           u4TmpSlaMeg);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaStepLoadRate                                *
 *                                                                          *
 * Description :  This function configues the step load rate to a SLA       *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4RateStep  - Rate Step value                             *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ***************************************************************************/
INT4
Y1564CliSetSlaStepLoadRate (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4RateStep)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SlaStepLoadRate (&u4ErrorCode, u4ContextId,
                                         u4SlaId,(INT4) u4RateStep) 
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaStepLoadRate (u4ContextId, u4SlaId, 
                                      (INT4) u4RateStep) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetSlaDuration                                    *
 *                                                                          *
 * Description :  This function configues the configuration test            *
 *                duration for a SLA                                        *   
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId     - SLA Identifier                              *
 *                u4Duration  - Duration for each test specified            *
 *                              in Service Configuration Test               *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetSlaDuration (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4SlaId, UINT4 u4Duration)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564SlaConfTestDuration (&u4ErrorCode, u4ContextId,
                                             u4SlaId,
                                             (INT4) u4Duration) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaConfTestDuration (u4ContextId, u4SlaId, 
                                          (INT4) u4Duration) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetTrafPolicingStatus                             *
 *                                                                          *
 * Description :  This function sets the Traffic Policing                   *
 *                status to a SLA                                           *             
 *                                                                          *
 * Input       :  CliHandle            - Cli context                        *
 *                u4ContextId          - Context Identifier                 *
 *                u4SlaId              - Sla Identifier                     *
 *                i4TrafPolicingStatus - Traf Policing Status               *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetTrafPolicingStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SlaId, INT4 i4TrafPolicingStatus)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
   
    if (nmhTestv2FsY1564SlaTrafPolicing (&u4ErrorCode, u4ContextId,
                                         u4SlaId, i4TrafPolicingStatus) 
                                         != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564SlaTrafPolicing (u4ContextId, u4SlaId, 
                                      i4TrafPolicingStatus) !=
                                      SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetMeterInfo                                      *
 *                                                                          *
 * Description :  This function sets the service configuration table        *
 *                (bandwidth profile) information                           *
 *                                                                          *
 * Input       :  CliHandle   - Cli context                                 *
 *                u4ContextId - Context Identifier                          *
 *                u4ServiceConfId - Service Configuration Id                *
 *                u4ServiceConfCIR - CIR value                              *
 *                u4ServiceConfCBS - CBS value                              *
 *                u4ServiceConfEIR - EIR value                              *
 *                u4ServiceConfEBS - EBS value                              *
 *                u4ServiceConfColorMode - Color Mode                       *
 *                u4ServiceConfCoupFlag - Couplng Flag                      *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetMeterInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4ServiceConfId, UINT4 u4ServiceConfCIR, 
                      UINT4 u4ServiceConfCBS, UINT4 u4ServiceConfEIR, 
                      UINT4 u4ServiceConfEBS, UINT4 u4ServiceConfColorMode,
                      UINT4 u4ServiceConfCoupFlag)
{

    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564ServiceConfCIR 
        (&u4ErrorCode, u4ContextId, 
         u4ServiceConfId, u4ServiceConfCIR) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
  
    if (nmhSetFsY1564ServiceConfCIR (u4ContextId, u4ServiceConfId,
                                     u4ServiceConfCIR) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564ServiceConfCBS
        (&u4ErrorCode, u4ContextId, 
         u4ServiceConfId, u4ServiceConfCBS) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ServiceConfCBS (u4ContextId, u4ServiceConfId,
                                     u4ServiceConfCBS) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564ServiceConfEIR 
        (&u4ErrorCode, u4ContextId, 
         u4ServiceConfId, u4ServiceConfEIR) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ServiceConfEIR (u4ContextId, u4ServiceConfId,
                                     u4ServiceConfEIR) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsY1564ServiceConfEBS 
        (&u4ErrorCode, u4ContextId, 
         u4ServiceConfId, u4ServiceConfCBS) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ServiceConfEBS (u4ContextId, u4ServiceConfId,
                                     u4ServiceConfEBS) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsY1564ServiceConfColorMode 
        (&u4ErrorCode, u4ContextId,
         u4ServiceConfId, (INT4) u4ServiceConfColorMode) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564ServiceConfColorMode (u4ContextId, u4ServiceConfId, 
                                           (INT4) u4ServiceConfColorMode) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }


    if(nmhTestv2FsY1564ServiceConfCoupFlag 
       (&u4ErrorCode, u4ContextId,
        u4ServiceConfId, (INT4) u4ServiceConfCoupFlag) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if(nmhSetFsY1564ServiceConfCoupFlag (u4ContextId, u4ServiceConfId,
                                         u4ServiceConfCoupFlag) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetTrafProfDirection                              *
 *                                                                          *
 * Description :  This function sets the direction for a traffic profile    *
 *                                                                          *
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4TrafProfId - Traffic profile identifier                 *
 *                u4Direction  - Direction in which traffic to be generated *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetTrafProfDirection (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4TrafProfId, UINT4 u4Direction)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsY1564TrafProfDir (&u4ErrorCode, u4ContextId, u4TrafProfId,
                                    (INT4) u4Direction) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564TrafProfDir (u4ContextId, u4TrafProfId, (INT4) u4Direction)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : Y1564CliSetPacketSize                                      *
 *                                                                          *
 * Description :  This function sets the packet size for a traffic profile  *
 *                                                                          *
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4TrafProfId - Traffic profile identifier                 *
 *                u4PacketSize - Packet Size                                *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPacketSize (tCliHandle CliHandle, UINT4 u4ContextId, 
                       UINT4 u4TrafProfId, UINT4 u4PacketSize)
{
    UINT4               u4ErrorCode = Y1564_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsY1564TrafProfPktSize (&u4ErrorCode, u4ContextId, u4TrafProfId,
                                        (INT4) u4PacketSize) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564TrafProfPktSize (u4ContextId, u4TrafProfId, (INT4) u4PacketSize)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetEmixPacketSize                                 *
 *                                                                          *
 * Description :  This function sets the packet size for a traffic profile  *
 *                                                                          *              
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4TrafProfId - Traffic Profile Identifier                 *
 *                EmixPktSize  -  Emix packet size                          *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetEmixPacketSize (tCliHandle CliHandle, UINT4 u4ContextId, 
                           UINT4 u4TrafProfId, UINT1 *pu1EmixPktSize)
{
    tSNMP_OCTET_STRING_TYPE EmixPktSize;
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (pu1EmixPktSize != NULL)
    {    
        MEMSET (&EmixPktSize, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

        /* Convert the string to OCTET_STRING */
        EmixPktSize.pu1_OctetList = pu1EmixPktSize;
        EmixPktSize.i4_Length = (INT4) STRLEN (pu1EmixPktSize);

        if (nmhTestv2FsY1564TrafProfOptEmixPktSize (&u4ErrorCode, u4ContextId, u4TrafProfId,
                                                    &EmixPktSize) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsY1564TrafProfOptEmixPktSize (u4ContextId, u4TrafProfId, &EmixPktSize) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}
/****************************************************************************
 * Function    :  Y1564CliSetPayload                                        *
 *                                                                          *
 * Description :  This function sets the payload for a traffic profile      *
 *                                                                          *              
 * Input       :  CliHandle    - Cli context                                *
 *                u4ContextId  - Context Identifier                         *
 *                u4TrafProfId - Traffic Profile Identifier                 *
 *                pu1Payload   - payload of 16 bytes                        *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPayload (tCliHandle CliHandle, UINT4 u4ContextId, 
                    UINT4 u4TrafProfId, UINT1 *pu1Payload)
{
    tSNMP_OCTET_STRING_TYPE Payload;
    UINT4               u4ErrorCode = Y1564_ZERO;

    MEMSET (&Payload, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Convert the string to OCTET_STRING */
    Payload.pu1_OctetList = pu1Payload;
    Payload.i4_Length = (INT4) STRLEN (pu1Payload);

    if (nmhTestv2FsY1564TrafProfPayload (&u4ErrorCode, u4ContextId, u4TrafProfId,
                                        &Payload) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564TrafProfPayload (u4ContextId, u4TrafProfId, &Payload) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Y1564CliSetPCP                                            *
 *                                                                          *
 * Description :  This function sets the Priotity Code Point                *
 *                value for a traffic profile                               *             
 *                                                                          *
 * Input       :  CliHandle     - Cli context                               *
 *                u4ContextId   - Context Identifier                        *
 *                u4TrafProfId  - Traffic Profile Identifier                *
 *                u1TrafProfPCP - PCP Value                                 *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
Y1564CliSetPCP (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4 u4TrafProfId, UINT4 u4TrafProfPCP)
{
    UINT4               u4ErrorCode = Y1564_ZERO;

    if (nmhTestv2FsY1564TrafProfPCP(&u4ErrorCode, u4ContextId,
                                    u4TrafProfId, (INT4) u4TrafProfPCP) 
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsY1564TrafProfPCP (u4ContextId, u4TrafProfId, 
                                  (INT4) u4TrafProfPCP) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
     UNUSED_PARAM (u4TrafProfId); 
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetSlaCfgPrompt                                 *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for SLA. It is exported to CLI module.               *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            * 
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sla-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sla-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-sla-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetTrafficProfileCfgPrompt                      *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Traffic Profile. It is exported to CLI module.   *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           * 
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("traffic-profile-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "traffic-profile-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-traffic-profile-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetVcmSlaCfgPrompt                              *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for SLA. It is exported to CLI module.               *
 *                    This function will be invoked when the System is     *
 *                    running in MI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           * 
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetVcmSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sla-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sla-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-sla-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetVcmTrafficProfileCfgPrompt                   * 
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Traffic Profile. It is exported to CLI module.   *
 *                    This function will be invoked when the System is     *
 *                    running in MI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetVcmTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("traffic-profile-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "traffic-profile-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-traffic-profile-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetServiceConfCfgPrompt                         *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Service configuration. It is exported to         *
 *                    CLI module.                                          *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            * 
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("service-config-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "service-config-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-service-config-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564GetVcmServiceConfCfgPrompt                      *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Service configuration. It is exported to         *
 *                    CLI module.                                          *
 *                    This function will be invoked when the System is     *
 *                    running in MI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            * 
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
Y1564GetVcmServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("service-config-1564");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "service-config-1564", u4Len) != Y1564_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_y1564_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-service-config-1564)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfig                               *
 *                                                                         *
 * DESCRIPTION      : This function used in show running config of Y1564   *
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4
Y1564ShowRunningConfig (tCliHandle CliHandle)
{

    if (CLI_SUCCESS != Y1564ShowRunningConfigCxtCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != Y1564ShowRunningConfigSacCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != Y1564ShowRunningConfigTrafProfCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != Y1564ShowRunningConfigServConfCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != Y1564ShowRunningConfigSlaCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != Y1564ShowRunningConfigPerfTestCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
     return CLI_SUCCESS;
}
/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigCxtCmds                        *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    context table                                        *
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigCxtCmds (tCliHandle CliHandle)
{
  
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4FsY1564ContextId;
    UINT4               u4FsY1564ContextIdNext;
    INT4                i4FsY1564CtxtSystemControl; 
    INT4                i4FsY1564CtxtModuleStatus;
    INT4                i4FsY1564CxtTrapStatus;

    /* Code to get Y1564 context entries */
    if (nmhGetFirstIndexFsY1564ContextTable (&u4FsY1564ContextId) != SNMP_FAILURE)
    {
        u4FsY1564ContextIdNext = u4FsY1564ContextId;

        do 
        {
            u4FsY1564ContextId = u4FsY1564ContextIdNext;

            MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

            if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
                != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                           u4FsY1564ContextId);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "switch %s", au1VcAlias);

            /* code to verify the execution of command :no shutdown y1564 */
            nmhGetFsY1564ContextSystemControl (u4FsY1564ContextId ,
                                               &i4FsY1564CtxtSystemControl);

            if (i4FsY1564CtxtSystemControl == Y1564_START)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "no shutdown y1564");
            } 

            /* code to verify the execution of command : y1564 enable*/
            nmhGetFsY1564ContextModuleStatus (u4FsY1564ContextId ,
                                              &i4FsY1564CtxtModuleStatus);

            if (i4FsY1564CtxtModuleStatus == Y1564_ENABLED)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "y1564 enable");
            } 

            /* code to verify the execution of command : y1564 notification enable*/
            nmhGetFsY1564ContextTrapStatus (u4FsY1564ContextId ,
                                            &i4FsY1564CxtTrapStatus);

            if (i4FsY1564CxtTrapStatus == Y1564_SNMP_FALSE)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "no y1564 notification enable");
            } 

            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFsY1564ContextTable 
              (u4FsY1564ContextId, &u4FsY1564ContextIdNext) != SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigPerfTestCmds                   *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    PerformanceTest table                                *
 *                                                                         *
 * INPUT            : CliHandle    - Handle to the CLI                     *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigPerfTestCmds (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE  SlaList;
    UINT1            au1SlaList[MAX_Y1564_SLA_LIST_SIZE];
    UINT4            u4FsY1564ContextId;
    UINT4            u4FsY1564ContextIdNext;
    UINT4            u4FsY1564PerformanceTestIndexNext;
    UINT4            u4FsY1564PerformanceTestIndex;
    INT4             i4FsY1564PerformanceTestDuration;
    UINT2            u2Index = 1;
    BOOL1            bResult = Y1564_ZERO;
    UINT1            u1Flag = Y1564_SNMP_TRUE;
    UINT1            au1VcAlias[VCM_ALIAS_MAX_LEN];

    MEMSET (au1SlaList, Y1564_ZERO, MAX_Y1564_SLA_LIST_SIZE);

    /* code to get SAC Table Entries */
    if (nmhGetFirstIndexFsY1564PerformanceTestTable (&u4FsY1564ContextId,
                                                     &u4FsY1564PerformanceTestIndex)
        != SNMP_FAILURE)
    {
        u4FsY1564ContextIdNext = u4FsY1564ContextId;
        u4FsY1564PerformanceTestIndexNext = u4FsY1564PerformanceTestIndex;

        do
        {
            u4FsY1564ContextId = u4FsY1564ContextIdNext;
            u4FsY1564PerformanceTestIndex = u4FsY1564PerformanceTestIndexNext;

            MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

            if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
                != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                           u4FsY1564ContextId);
                return CLI_FAILURE;
            }

            /* code to verify the execution of command : 
             * y1564 service-performance-test <> sla <>*/
            SlaList.pu1_OctetList = au1SlaList;

            nmhGetFsY1564PerformanceTestSlaList (u4FsY1564ContextId,
                                                 u4FsY1564PerformanceTestIndex,
                                                 &SlaList);
            if (SlaList.i4_Length != Y1564_ZERO)
            {
                for (; u2Index <= (MAX_Y1564_SLA_LIST_SIZE * Y1564_BITS_PER_BYTE);
                     u2Index++)
                {
                    OSIX_BITLIST_IS_BIT_SET (SlaList.pu1_OctetList,
                                             u2Index, MAX_Y1564_SLA_LIST_SIZE,
                                             bResult);
                    if (bResult == OSIX_FALSE)
                    {
                        /* If the bit is not set, continue */
                        continue;
                    }
                    else
                    {
                        if (u1Flag == Y1564_SNMP_TRUE)
                        {
                            CliPrintf (CliHandle, "\r\n");
                            CliPrintf (CliHandle, "y1564 service-performance-test ");
                            CliPrintf (CliHandle, " %d ", u4FsY1564PerformanceTestIndex);
                            CliPrintf (CliHandle, "sla ");
                            CliPrintf (CliHandle, "%d", u2Index);
                            u1Flag = Y1564_SNMP_FALSE;
                        }
                        else
                        {
                           CliPrintf (CliHandle, ",%d", u2Index);
                        }
                    }
                 }
                if (u1Flag == Y1564_SNMP_FALSE)
                {
                    CliPrintf (CliHandle, " switch %s", au1VcAlias);
                }

                u1Flag = Y1564_SNMP_TRUE;
             }

            /* code to verify the execution of command :
             * duration-performance-test */
            nmhGetFsY1564PerformanceTestDuration (u4FsY1564ContextId,
                                                  u4FsY1564PerformanceTestIndex,
                                                  &i4FsY1564PerformanceTestDuration);
            /* Checking for default value */
            if (i4FsY1564PerformanceTestDuration != Y1564_PERF_TEST_DEF_DURATION) 
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "y1564 service-performance-test ");
                CliPrintf (CliHandle, " %d ", u4FsY1564PerformanceTestIndex);

                if (i4FsY1564PerformanceTestDuration == Y1564_PERF_TEST_MIN_DURATION)
                {
                    CliPrintf (CliHandle, "duration fifteen-min");
                }
                if (i4FsY1564PerformanceTestDuration == Y1564_PERF_TEST_MAX_DURATION)
                {
                    CliPrintf (CliHandle, "duration twenty-four-hour");
                }
                CliPrintf (CliHandle, " switch %s", au1VcAlias);
            }

            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
            u2Index = Y1564_ZERO;
        }
        while (nmhGetNextIndexFsY1564PerformanceTestTable 
               (u4FsY1564ContextId, &u4FsY1564ContextIdNext, 
                u4FsY1564PerformanceTestIndex, 
                &u4FsY1564PerformanceTestIndexNext) != SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigSacCmds                        *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    SAC table                                            * 
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigSacCmds (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE    Availability;
    tSNMP_OCTET_STRING_TYPE    *pAvailability;
    FLT4                       f4Availability = Y1564_ZERO;
    UINT1                      au1Availability[Y1564_AVLBLTY_LENGTH] = { 0 };
    UINT1                      au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4                      u4FsY1564ContextId;
    UINT4                      u4FsY1564ContextIdNext;
    UINT4                      u4FsY1564SacId;
    UINT4                      u4FsY1564SacIdNext;
    INT4                       i4FsY1564SacFrLossRatio = Y1564_ZERO;
    INT4                       i4FsY1564SacInfoRate = Y1564_ZERO;
    INT4                       i4FsY1564SacFrTransDelay = Y1564_ZERO;
    INT4                       i4FsY1564SacFrDelayVar = Y1564_ZERO;

    /* code to get SAC Table Entries */
   if (nmhGetFirstIndexFsY1564SacTable (&u4FsY1564ContextId, 
                                        &u4FsY1564SacId) != SNMP_FAILURE)
   {
       u4FsY1564ContextIdNext = u4FsY1564ContextId;
       u4FsY1564SacIdNext = u4FsY1564SacId;

       do
       {
           u4FsY1564ContextId = u4FsY1564ContextIdNext;
           u4FsY1564SacId = u4FsY1564SacIdNext;
    
           MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

           if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
               != OSIX_SUCCESS)
           {
               CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                          u4FsY1564ContextId);
               return CLI_FAILURE;
           }
           CliPrintf (CliHandle, "switch %s", au1VcAlias);

           /* code to verify execution of command : 
            * y1564 sac <> information-rate <> frame-loss-ratio <>
              frame-time-delay <> frame-delay-variation <> 
              availability */
	   nmhGetFsY1564SacInfoRate (u4FsY1564ContextId, u4FsY1564SacId,
			   &i4FsY1564SacInfoRate);

	   MEMSET (&Availability, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
	   Availability.pu1_OctetList = au1Availability;


	   nmhGetFsY1564SacAvailability (u4FsY1564ContextId, u4FsY1564SacId,
			   &Availability);
	   pAvailability = &Availability;

	   if (Y1564_OCTETSTRING_TO_FLOAT (pAvailability, f4Availability) == EOF)
	   {
		   return CLI_FAILURE;
	   }



	   if ((i4FsY1564SacInfoRate != Y1564_ZERO) && (f4Availability != Y1564_ZERO))
	   {
		   CliPrintf (CliHandle, "\r\n");
		   CliPrintf (CliHandle, "y1564 sac ");
		   CliPrintf (CliHandle, "%u ", u4FsY1564SacId);

		   CliPrintf (CliHandle, "information-rate ");
		   CliPrintf (CliHandle, "%d ", i4FsY1564SacInfoRate);


		   nmhGetFsY1564SacFrLossRatio (u4FsY1564ContextId, u4FsY1564SacId,
				   &i4FsY1564SacFrLossRatio);

		   CliPrintf (CliHandle, "frame-loss-ratio ");
		   CliPrintf (CliHandle, "%u ", i4FsY1564SacFrLossRatio);

		   nmhGetFsY1564SacFrTransDelay (u4FsY1564ContextId, u4FsY1564SacId,
				   &i4FsY1564SacFrTransDelay);

		   CliPrintf (CliHandle, "frame-time-delay ");
		   CliPrintf (CliHandle, "%d ", i4FsY1564SacFrTransDelay);

		   nmhGetFsY1564SacFrDelayVar (u4FsY1564ContextId, u4FsY1564SacId,
				   &i4FsY1564SacFrDelayVar);

		   CliPrintf (CliHandle, "frame-delay-variation ");
		   CliPrintf (CliHandle, "%d ", i4FsY1564SacFrDelayVar);

		   CliPrintf (CliHandle, "availability ");
		   CliPrintf (CliHandle, "%.2f ", (DBL8) f4Availability);

		   CliPrintf (CliHandle, "\r\n!");
		   CliPrintf (CliHandle, "\r\n");
	   }
       }
       while (nmhGetNextIndexFsY1564SacTable 
              (u4FsY1564ContextId, &u4FsY1564ContextIdNext,
               u4FsY1564SacId, &u4FsY1564SacIdNext) != SNMP_FAILURE);
   }
   return CLI_SUCCESS;
}
/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigTrafProfCmds                   *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    Traffic Profile Table                                *
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigTrafProfCmds (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE    EmixPktSize;
    tSNMP_OCTET_STRING_TYPE    Payload;
    UINT1                      au1EmixPktSize[Y1564_EMIX_PKT_SIZE + 1];
    UINT1                      au1Payload[Y1564_PAYLOAD_SIZE + 1];
    UINT1                      au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4                      u4FsY1564ContextId;
    UINT4                      u4FsY1564ContextIdNext;
    UINT4                      u4FsY1564TrafProfId;
    UINT4                      u4FsY1564TrafProfNext;
    INT4                       i4FsY1564TrafProfDir;
    INT4                       i4FsY1564TrafProfPktSize;
    INT4                       i4TrafProfPCP;

    MEMSET (au1Payload, Y1564_ZERO, sizeof (au1Payload));
    MEMSET (au1EmixPktSize, Y1564_ZERO, sizeof (au1EmixPktSize)); 

    if (nmhGetFirstIndexFsY1564TrafProfTable (&u4FsY1564ContextId, 
                                              &u4FsY1564TrafProfId ) != SNMP_FAILURE)
    {
        u4FsY1564ContextIdNext = u4FsY1564ContextId;
        u4FsY1564TrafProfNext = u4FsY1564TrafProfId;

        do
        {
            u4FsY1564ContextId = u4FsY1564ContextIdNext;
            u4FsY1564TrafProfId = u4FsY1564TrafProfNext;

 
            MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

            if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
               != OSIX_SUCCESS)
           {
               CliPrintf (CliHandle, "unable to get alias name for context id  %d", 
                          u4FsY1564ContextId);
               return CLI_FAILURE;
           }
           CliPrintf (CliHandle, "switch %s", au1VcAlias);

            /* code to verify execution of command: y1564 traffic-profile <> */
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "y1564 traffic-profile ");
               CliPrintf (CliHandle, "%d ", u4FsY1564TrafProfId);
           /* code to verify execution of command: direction <> */
           nmhGetFsY1564TrafProfDir (u4FsY1564ContextId, 
                                     u4FsY1564TrafProfId,
                                     &i4FsY1564TrafProfDir);

           if (i4FsY1564TrafProfDir != Y1564_DIRECTION_EXTERNAL)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "direction internal ");
           }

           /* code to verify execution of command: packet-size <> */
           nmhGetFsY1564TrafProfPktSize (u4FsY1564ContextId, u4FsY1564TrafProfId,
                                         &i4FsY1564TrafProfPktSize);

           if (i4FsY1564TrafProfPktSize != Y1564_TRAF_PROF_DEFAULT_PACKET_SIZE)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "packet-size ");
               CliPrintf (CliHandle, "%d ", i4FsY1564TrafProfPktSize);
           }

           MEMSET (&EmixPktSize, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
           EmixPktSize.pu1_OctetList = au1EmixPktSize;
           EmixPktSize.i4_Length = Y1564_ZERO;

           /* code to verify exeuction of command: packet-size emix  <> */
           nmhGetFsY1564TrafProfOptEmixPktSize (u4FsY1564ContextId, u4FsY1564TrafProfId,
                                                &EmixPktSize);

           if (MEMCMP (&gau1EmixDefaultPktSize, EmixPktSize.pu1_OctetList,
                       Y1564_EMIX_PKT_SIZE) != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "packet-size emix ");
               CliPrintf (CliHandle, "%s ", EmixPktSize.pu1_OctetList);
           }
           MEMSET (au1EmixPktSize, Y1564_ZERO, sizeof (au1EmixPktSize)); 

           MEMSET (&Payload, Y1564_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
           Payload.pu1_OctetList = au1Payload;
           Payload.i4_Length = Y1564_ZERO;

           /* code to verify execution of command: signature <> */
           nmhGetFsY1564TrafProfPayload (u4FsY1564ContextId, u4FsY1564TrafProfId,
                                         &Payload);

           /* Signature, payload format, in the Packet */
           if (MEMCMP (&gaNullPayload, Payload.pu1_OctetList,
                       Y1564_PAYLOAD_SIZE) != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "signature ");
               CliPrintf (CliHandle, "%s ", Payload.pu1_OctetList);
           }

           /* code to verify execution of command: pcp <> */
           nmhGetFsY1564TrafProfPCP (u4FsY1564ContextId, u4FsY1564TrafProfId,
                                         &i4TrafProfPCP);

           if (i4TrafProfPCP != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "pcp ");
               CliPrintf (CliHandle, "%d ", i4TrafProfPCP);
           }

           CliPrintf (CliHandle, "\r\n!");
           CliPrintf (CliHandle, "\r\n!\r\n");

        }
        while (nmhGetNextIndexFsY1564TrafProfTable 
               (u4FsY1564ContextId, &u4FsY1564ContextIdNext,
                u4FsY1564TrafProfId,&u4FsY1564TrafProfNext) != SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigServConfCmds                   *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    Service Configuration Table                          *
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigServConfCmds (tCliHandle CliHandle)
{

#ifndef MEF_WANTED

    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4FsY1564ContextId = Y1564_ZERO;
    UINT4               u4FsY1564ContextIdNext;
    UINT4               u4FsY1564ServiceConfId;
    UINT4               u4FsY1564ServiceConfIdNext;
    UINT4               u4FsY1564ServiceConfCIR;
    UINT4               u4FsY1564ServiceConfCBS;
    UINT4               u4FsY1564ServiceConfEIR;
    UINT4               u4FsY1564ServiceConfEBS;
    INT4                i4FsY1564ServiceConfCoupFlag;
    INT4                i4FsY1564ServiceConfColorMode;

    if (nmhGetFirstIndexFsY1564ServiceConfTable (&u4FsY1564ContextId, 
                                                 &u4FsY1564ServiceConfId) != SNMP_FAILURE)
    {

        u4FsY1564ContextIdNext = u4FsY1564ContextId;
        u4FsY1564ServiceConfIdNext = u4FsY1564ServiceConfId;

        do
        {
            u4FsY1564ContextId = u4FsY1564ContextIdNext;
            u4FsY1564ServiceConfId = u4FsY1564ServiceConfIdNext;

            MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

            if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
                != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                           u4FsY1564ContextId);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "switch %s", au1VcAlias);

            /* code to verify execution of command : 
             * y1564 service-configuration <> */

            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "y1564 service-configuration ");
            CliPrintf (CliHandle, "%u ", u4FsY1564ServiceConfId);

            nmhGetFsY1564ServiceConfCIR (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                         &u4FsY1564ServiceConfCIR);

            nmhGetFsY1564ServiceConfCBS (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                         &u4FsY1564ServiceConfCBS);

            nmhGetFsY1564ServiceConfEIR (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                         &u4FsY1564ServiceConfEIR);

            nmhGetFsY1564ServiceConfEBS (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                         &u4FsY1564ServiceConfEBS);

            nmhGetFsY1564ServiceConfCoupFlag (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                              &i4FsY1564ServiceConfCoupFlag);

            nmhGetFsY1564ServiceConfColorMode (u4FsY1564ContextId, u4FsY1564ServiceConfId,
                                               &i4FsY1564ServiceConfColorMode);

            CliPrintf (CliHandle, "\r\n");

            if ((u4FsY1564ServiceConfCIR != Y1564_ZERO) &&
                (u4FsY1564ServiceConfCBS != Y1564_ZERO) &&
                (u4FsY1564ServiceConfEIR != Y1564_ZERO) &&
                (u4FsY1564ServiceConfEBS != Y1564_ZERO))

            {
                CliPrintf (CliHandle, "service-config cir ");
                CliPrintf (CliHandle, "%u ", u4FsY1564ServiceConfCIR);

                CliPrintf (CliHandle, "cbs ");
                CliPrintf (CliHandle, "%u ", u4FsY1564ServiceConfCBS);

                CliPrintf (CliHandle, "eir ");
                CliPrintf (CliHandle, "%u ", u4FsY1564ServiceConfEIR);

                CliPrintf (CliHandle, "ebs ");
                CliPrintf (CliHandle, "%u ", u4FsY1564ServiceConfEBS);

                if (i4FsY1564ServiceConfColorMode != Y1564_SERV_CONF_COLOR_BLIND)
                {
                    CliPrintf (CliHandle, "color-aware ");
                }

                if (i4FsY1564ServiceConfCoupFlag == Y1564_COUPLING_TYPE_COUPLED)
                {
                    CliPrintf (CliHandle, "coupling-flag ");
                }
            }

            CliPrintf (CliHandle, "\r\n!\r\n");

        }
        while (nmhGetNextIndexFsY1564ServiceConfTable 
               (u4FsY1564ContextId, &u4FsY1564ContextIdNext,
                u4FsY1564ServiceConfId, &u4FsY1564ServiceConfIdNext) != SNMP_FAILURE);
    }
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : Y1564ShowRunningConfigSlaCmds                        *
 *                                                                         *
 * DESCRIPTION      : This function used in show runnig config of the      *
 *                    SLA     table                                        *
 *                                                                         *
 * INPUT            : CliHandle   - Handle to the CLI                      *
 *                                                                         *
 * OUTPUT           : None.                                                *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
INT4 Y1564ShowRunningConfigSlaCmds (tCliHandle CliHandle)
{

    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4FsY1564ContextId;
    UINT4               u4FsY1564ContextIdNext;
    UINT4               u4FsY1564SlaId;
    UINT4               u4FsY1564SlaIdNext;
#ifndef MEF_WANTED
    UINT4               u4FsY1564SlaServiceConfId;
#endif
    UINT4               u4FsY1564SlaTrafProfileId;
    UINT4               u4FsY1564SlaSacId;
    UINT4               u4FsY1564SlaMEG;
    UINT4               u4FsY1564SlaME;
    UINT4               u4FsY1564SlaMEP;
#ifdef MEF_WANTED
    INT4                i4FsY1564SlaEvcIndex;
#endif
    INT4                i4FsY1564SlaStepLoadRate;
    INT4                i4FsY1564SlaTrafPolicing;
    INT4                i4FsY1564SlaConfTestDuration;
    INT4                i4FsY1564SlaTestSelector;

    /* code to get SLA Table Entries */
   if (nmhGetFirstIndexFsY1564SlaTable (&u4FsY1564ContextId, 
                                        &u4FsY1564SlaId) != SNMP_FAILURE)
   {
       u4FsY1564ContextIdNext = u4FsY1564ContextId;
       u4FsY1564SlaIdNext = u4FsY1564SlaId;

       do
       {
           u4FsY1564ContextId = u4FsY1564ContextIdNext;
           u4FsY1564SlaId = u4FsY1564SlaIdNext;
    
           MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);
           
           if (Y1564VcmGetAliasName (u4FsY1564ContextId, au1VcAlias)
               != OSIX_SUCCESS)
           {
               CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                          u4FsY1564ContextId);
               return CLI_FAILURE;
           }
           CliPrintf (CliHandle, "switch %s", au1VcAlias);

           /* code to verify execution of command : y1564 sla */
           CliPrintf (CliHandle, "\r\n");
           CliPrintf (CliHandle, "y1564 sla ");
           CliPrintf (CliHandle, "%u ", u4FsY1564SlaId);
#ifdef MEF_WANTED
           /* code to verify exeucution of command : map evc */
           nmhGetFsY1564SlaEvcIndex (u4FsY1564ContextId, u4FsY1564SlaId, 
                                     &i4FsY1564SlaEvcIndex);

           if (i4FsY1564SlaEvcIndex != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "map evc ");
               CliPrintf (CliHandle, "%d ", i4FsY1564SlaEvcIndex);
           }
#else
           /* code to verify execution of command : map service configuration */
           nmhGetFsY1564SlaServiceConfId (u4FsY1564ContextId, u4FsY1564SlaId, 
                                          &u4FsY1564SlaServiceConfId);

           if (u4FsY1564SlaServiceConfId != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "map service-configuration ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaServiceConfId);
           }
#endif
           /* code to verify execution of command : map traffic-profile */
           nmhGetFsY1564SlaTrafProfileId (u4FsY1564ContextId, u4FsY1564SlaId, 
                                          &u4FsY1564SlaTrafProfileId);

           if (u4FsY1564SlaTrafProfileId != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "map traffic-profile ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaTrafProfileId);
           }

           /* code to verify execution of command : map sac */
           nmhGetFsY1564SlaSacId (u4FsY1564ContextId, u4FsY1564SlaId, 
                                  &u4FsY1564SlaSacId);

           if (u4FsY1564SlaSacId != Y1564_ZERO)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "map sac ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaSacId);
           }

           /* code to verify execution of command : map meg x me x mep x*/
           nmhGetFsY1564SlaMEG (u4FsY1564ContextId, u4FsY1564SlaId,
                                &u4FsY1564SlaMEG);
           nmhGetFsY1564SlaME (u4FsY1564ContextId, u4FsY1564SlaId,
                               &u4FsY1564SlaME); 
           nmhGetFsY1564SlaMEP (u4FsY1564ContextId, u4FsY1564SlaId,
                                &u4FsY1564SlaMEP);

           CliPrintf (CliHandle, "\r\n");

           if ((u4FsY1564SlaMEG != Y1564_ZERO) &&
               (u4FsY1564SlaME  != Y1564_ZERO) &&
               (u4FsY1564SlaMEP != Y1564_ZERO))
           {

               CliPrintf (CliHandle, "map meg ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaMEG);

               CliPrintf (CliHandle, "me ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaME);

               CliPrintf (CliHandle, "mep ");
               CliPrintf (CliHandle, "%u ", u4FsY1564SlaMEP);
           }
           /* code to verify execution of command: rate step */
           nmhGetFsY1564SlaStepLoadRate (u4FsY1564ContextId, u4FsY1564SlaId,
                                         &i4FsY1564SlaStepLoadRate);

           if (i4FsY1564SlaStepLoadRate != Y1564_SLA_DEF_RATE_STEP)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "rate-step ");
               CliPrintf (CliHandle, "%d ", i4FsY1564SlaStepLoadRate);
           }

           /* code to verify execution of command: duration-configuration-test */
           nmhGetFsY1564SlaConfTestDuration (u4FsY1564ContextId, u4FsY1564SlaId, 
                                             &i4FsY1564SlaConfTestDuration);

           if (i4FsY1564SlaConfTestDuration != Y1564_SLA_DEF_DURATION)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "configuration-test-duration ");
               CliPrintf (CliHandle, "%d ", i4FsY1564SlaConfTestDuration);
           }
 
           /* code to verify execution of command: traffic-policing */
 
           nmhGetFsY1564SlaTrafPolicing (u4FsY1564ContextId, u4FsY1564SlaId, 
                                         &i4FsY1564SlaTrafPolicing);

           if (i4FsY1564SlaTrafPolicing != Y1564_SLA_DEF_TRAFFIC_POLICING_STATUS)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "traffic-policing disable");
           }

           /* code to verify execution of command: test selector */

           nmhGetFsY1564SlaTestSelector (u4FsY1564ContextId, u4FsY1564SlaId, 
                                         &i4FsY1564SlaTestSelector);

           if (i4FsY1564SlaTestSelector != Y1564_ALL_TEST)
           {
               CliPrintf (CliHandle, "\r\n");
               CliPrintf (CliHandle, "test selector ");

               if ((i4FsY1564SlaTestSelector & Y1564_SIMPLE_CIR_TEST) == 
                   Y1564_SIMPLE_CIR_TEST)
               {
                   CliPrintf (CliHandle, "simple-cir-test ");
               }

               if ((i4FsY1564SlaTestSelector & Y1564_STEPLOAD_CIR_TEST) == 
                   Y1564_STEPLOAD_CIR_TEST)
               {
                   CliPrintf (CliHandle, "stepload-cir-test ");
               }

               if ((i4FsY1564SlaTestSelector & Y1564_EIR_TEST) == 
                   Y1564_EIR_TEST)
               {
                   CliPrintf (CliHandle, "eir-test ");
               }

               if ((i4FsY1564SlaTestSelector & Y1564_TRAF_POLICING_TEST) == 
                   Y1564_TRAF_POLICING_TEST)
               {
                   CliPrintf (CliHandle, "traffic-policing-test");
               }
           }

            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n!\r\n");
             
     }
       while (nmhGetNextIndexFsY1564SlaTable 
              (u4FsY1564ContextId, &u4FsY1564ContextIdNext,
               u4FsY1564SlaId, &u4FsY1564SlaIdNext) != SNMP_FAILURE);
  }
       return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowSacTable
 * Description   : This routine shows the contents present in the
 *                 SAC Table.
 * Input         : CliHandle - Handle to CLI
 *                 pu4SacId  - Pointer to SAC Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowSacTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacIndex)
{
    tSacInfo           *pSacEntry = NULL;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4NxtSacId = Y1564_ZERO;
    UINT4               u4NxtSacContextId = Y1564_ZERO;
    UINT1               u1IsShowAll = OSIX_TRUE;

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    if (u4SacIndex != Y1564_ZERO)
    {
        /* show information required for specific SAC */
        u4NxtSacId = u4SacIndex;
        u4NxtSacContextId = u4ContextId;
        u1IsShowAll = OSIX_FALSE;
    }
    else
    {
        /* show information for all the SAC Entries */
        /* Get the first SAC Index */

        if (nmhGetNextIndexFsY1564SacTable (u4ContextId,&u4NxtSacContextId,
                                            u4SacIndex, &u4NxtSacId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    /* Get the SAC Entry */
    pSacEntry = Y1564UtilSacGetSacEntry (u4NxtSacContextId, u4NxtSacId);
    if (pSacEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {       
        if (u4NxtSacContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4SacIndex = u4NxtSacId;

         /*
         * There are no default values for the SAC Table,
         * so printing all the values for each SAC Entry.
         */
        CliPrintf (CliHandle, "\r\nSac Number : %u\r\n", u4NxtSacId);
        CliPrintf (CliHandle, "------------------------------------------\r\n");
        /* Information Rate */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Information Rate",
                   pSacEntry->u4SacInfoRate);
        /* Frame Loss Ratio */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Frame Loss Ratio",
                   pSacEntry->u4SacFrLossRatio);
        /* Frame Transfer Delay */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Frame Transfer Delay",
                   pSacEntry->u4SacFrTxDelay);
        /* Frame Delat Variation */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Frame Delay Variation",
                   pSacEntry->u4SacFrDelayVar);
        /* Availability of the Service */
        CliPrintf (CliHandle, "%-33s : %.2f\r\n", "Availability",
                   (DBL8) pSacEntry->f4SacAvailability);

        CliPrintf (CliHandle, "\r\n");

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        /* Get the next SAC Index */
        if (nmhGetNextIndexFsY1564SacTable (u4ContextId,
                                            &u4NxtSacContextId,
                                            u4SacIndex, &u4NxtSacId) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Get the next SAC Entry */
        pSacEntry = Y1564UtilSacGetSacEntry (u4NxtSacContextId, u4NxtSacId);
    }
    while (pSacEntry != NULL);
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowTrafProfTable
 * Description   : This routine shows the contents present in the
 *                 Traffic Profile Table.
 * Input         : CliHandle    - Handle to CLI
 *                 pu4ProfileId - Pointer to Traffic Profile Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowTrafProfTable (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4ProfileId)
{
    tTrafficProfileInfo     *pTrafProfEntry = NULL;
    UINT1                   au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4                   u4NxtTrafProfId = Y1564_ZERO;
    UINT1                   au1ShowString[CLI_Y1564_MAX_SHOW_STRING_SIZE];
    UINT4                   u4NxtTrafProfContextId = Y1564_ZERO;
    UINT1                   u1IsShowAll = OSIX_TRUE;


    MEMSET (au1ShowString, Y1564_ZERO, CLI_Y1564_MAX_SHOW_STRING_SIZE);

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    if (u4ProfileId != Y1564_ZERO)
    {
        /* show information required for specific Traffic Profile */
        u4NxtTrafProfId = u4ProfileId;
        u4NxtTrafProfContextId = u4ContextId;
        u1IsShowAll = OSIX_FALSE;
    }
    else
    {
        /* show information for all the Traffic Profiles */
        /* Get the first Traffic Profile Index */
        if (nmhGetNextIndexFsY1564TrafProfTable (u4ContextId,&u4NxtTrafProfContextId,
                                                 u4ProfileId,&u4NxtTrafProfId) 
                                                 != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    /* Get the Traffic Profile Entry */
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4NxtTrafProfContextId, u4NxtTrafProfId);
    if (pTrafProfEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {

        if(u4NxtTrafProfContextId != u4ContextId)
        {
             return CLI_SUCCESS;
        }    
            u4ProfileId = u4NxtTrafProfId;

            /* Traffic Profile Index */
            CliPrintf (CliHandle, "\r\nTraffic Profile Number : %u\r\n", u4NxtTrafProfId);
            CliPrintf (CliHandle, "------------------------------------------\r\n");

            /* Since Traffic Profile Direction internal is not support
             * Display is masked 
             * Traffic Profile Direction 
            CliPrintf (CliHandle, "%-33s : %s\r\n", "Traffic Profile Direction",
                       ((pTrafProfEntry->TrafProfDirection ==
                         CLI_Y1564_DIRECTION_EXTERNAL) ?
                        "External" : "Internal"));
            */
            /* Packet Size */
            CliPrintf (CliHandle, "%-33s : %d\r\n", "Packet Size",
                       pTrafProfEntry->u2TrafProfPktSize);

            /* EMIX Pattern */
            CliPrintf (CliHandle, "%-33s : %s\r\n", "EMIX Pattern",
                       pTrafProfEntry->au1TrafProfOptEmixPktSize);

            /* EMIX Pattern Status*/
            CliPrintf (CliHandle, "%-33s : %s\r\n", "EMIX Pattern Status",
                       ((pTrafProfEntry->u1EmixSelected ==
                         CLI_Y1564_EMIX_ENABLE) ?
                        "Enable" : "Disable"));

            CliPrintf (CliHandle, "%-33s : %s\r\n", "Signature",
                       pTrafProfEntry->au1TrafProfPayload);
            /*PCP */
            CliPrintf (CliHandle, "%-33s : %d\r\n", "pcp",
                       pTrafProfEntry->u1TrafProfPCP);

            CliPrintf (CliHandle, "\r\n");

            if (u1IsShowAll == OSIX_FALSE)
            {
                break;
            }

            /* Get the next Traffic Profile Index */
            if (nmhGetNextIndexFsY1564TrafProfTable (u4ContextId,
                                                 &u4NxtTrafProfContextId,
                                                 u4ProfileId,
                                                 &u4NxtTrafProfId)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Get the next Traffic Profile Entry */
        pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4NxtTrafProfContextId, u4NxtTrafProfId);
    }
    while (pTrafProfEntry != NULL);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowSlaTable
 * Description   : This routine shows the contents present in the
 *                 SLA Table.
 * Input         : CliHandle - Handle to CLI
 *                 u4ContextId - Context Identifier
 *                 pu4SlaId  - Pointer to SLA Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowSlaTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaIndex)
{

    tSlaInfo            *pSlaEntry = NULL;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4NxtSlaId = Y1564_ZERO;
    UINT4               u4NxtSlaContextId = Y1564_ZERO;
    UINT4               u4Idx = Y1564_ZERO;
    UINT4               u4NumLevels = Y1564_ZERO;
    UINT1               u1IsShowAll = OSIX_TRUE;
    UINT1               u1Flag = OSIX_FALSE;
    CONST CHR1         *pc1Separator = ", ";
    CONST CHR1         *apY1564ConfigurationTestLevels[] = {
        "Simple CIR",
        "Step Load CIR",
        "EIR",
        "Traffic Policing",
        NULL
    };

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    if (u4SlaIndex != Y1564_ZERO)
    {
        /* show information required for specific SLA */
        u4NxtSlaId = u4SlaIndex;
        u4NxtSlaContextId = u4ContextId;
        u1IsShowAll = OSIX_FALSE;
    }
    else
    {
        /* show information for all the SLAs */
        /* Get the first SLA Index */
        if (nmhGetNextIndexFsY1564SlaTable (u4ContextId, &u4NxtSlaContextId,
                                            u4SlaIndex, &u4NxtSlaId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    /* Get the SLA Entry */
    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4NxtSlaContextId, u4NxtSlaId);

    if (pSlaEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (u4NxtSlaContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4SlaIndex = u4NxtSlaId;

        /* SLA Index */
        CliPrintf (CliHandle, "\r\nSLA Number : %u\r\n", u4NxtSlaId);
        CliPrintf (CliHandle, "----------------------------------------\r\n");

#ifdef MEF_WANTED
        /* EVC Index */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped EVC",
                   pSlaEntry->u2EvcId);
#else
        /* Service Configuration Index */
        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped Service Configuration",
                   pSlaEntry->u2SlaServiceConfId);
#endif

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped MEG",
                   pSlaEntry->u4SlaMegId);

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped ME",
                   pSlaEntry->u4SlaMeId);

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped MEP",
                   pSlaEntry->u4SlaMepId);

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped SAC",
                   pSlaEntry->u4SlaSacId);

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Mapped Traffic Profile",
                   pSlaEntry->u4SlaTrafProfId);

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Rate Step",
                   (Y1564_MAX_RATE_STEP / pSlaEntry->u2SlaRateStep));

        CliPrintf (CliHandle, "%-33s : %u\r\n", "Configuration Test Duration",
                   pSlaEntry->u4SlaConfTestDuration);

        CliPrintf (CliHandle, "%-33s : %s\r\n","Traffic Policing Status",
                   ((pSlaEntry->u1SlaTrafPolicing ==
                     Y1564_ENABLED) ?
                    "Enable" : "Disable"));

        /* Selected Test Parameters*/
        if (pSlaEntry->u1SlaConfTestSelector != Y1564_ZERO)
        {
            /* sizeof gets munged when apY1564ConfigurationTestLevels[] is
             * passed to a function Hence Iterate through
             * apY1564ConfigurationTestLevels and find the size of
             * apY1564ConfigurationTestLevels. A NULL marks the end of
             * apY1564ConfigurationTestLevels */
            for (u4Idx = 0; apY1564ConfigurationTestLevels[u4Idx]; u4Idx++);

            u4NumLevels = u4Idx;

            CliPrintf (CliHandle, "%-33s : ", "Selected Test");

            for (u4Idx = 0; u4Idx < u4NumLevels; u4Idx++)
            {
                if (pSlaEntry->u1SlaConfTestSelector & (1 << u4Idx))
                {
                    if(u1Flag != OSIX_FALSE)
                    {
                        CliPrintf (CliHandle, "%s",pc1Separator);
                    }
                    CliPrintf (CliHandle, "%s", apY1564ConfigurationTestLevels[u4Idx]);
                    u1Flag = OSIX_TRUE;
                }
            }
            u1Flag = OSIX_FALSE;

            CliPrintf (CliHandle, "\r\n");
        }
#ifdef MEF_WANTED
        /* Bandwith Profile Parametrs */
        CliPrintf (CliHandle, "%-33s : %s\r\n", "Color Mode",
                   ((pSlaEntry->u1ColorMode == Y1564_SERV_CONF_COLOR_AWARE) ?
                    "Color Aware" : "Non-Color Aware"));

        CliPrintf (CliHandle, "%-33s : %s\r\n", "Coupling Flag",
                   ((pSlaEntry->u1CoupFlag == Y1564_COUPLING_TYPE_COUPLED) ?
                    "Enable" : "Disable"));

        CliPrintf (CliHandle, "%-33s : %d\r\n", "CIR", pSlaEntry->u4CIR);

        CliPrintf (CliHandle, "%-33s : %d\r\n", "CBS", pSlaEntry->u4CBS);

        CliPrintf (CliHandle, "%-33s : %d\r\n", "EIR", pSlaEntry->u4EIR);

        CliPrintf (CliHandle, "%-33s : %d\r\n", "EBS", pSlaEntry->u4EBS);
#endif
        /* Status of the SLA */
        CliPrintf (CliHandle, "%-33s : %s\r\n", "Status",
                   ((pSlaEntry->u1SlaConfTestStatus == Y1564_SLA_START) ?
                    "Started" : "Not Started"));

        CliPrintf (CliHandle, "\r\n");

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }
        /* Get the next SLA Index */
        if (nmhGetNextIndexFsY1564SlaTable (u4ContextId, &u4NxtSlaContextId,
                                            u4SlaIndex, &u4NxtSlaId) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Get the next SLA Entry */
        pSlaEntry = Y1564UtilSlaGetSlaEntry (u4NxtSlaContextId, u4NxtSlaId);

    }
    while (pSlaEntry != NULL);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowServiceConfTable
 * Description   : This routine shows the contents present in the
 *                 Service Configuration Table.
 * Input         : CliHandle - Handle to CLI
 *                 u4ContextId - Context Identifier
 *                 pu4ServiceConfId  - Pointer to Service Configuration Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowServiceConfTable (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4ServiceConfIndex)
{

#ifndef MEF_WANTED
    tServiceConfInfo   *pServiceConfEntry = NULL;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4NxtServiceConfId = Y1564_ZERO;
    UINT4               u4NxtServiceConfContextId = Y1564_ZERO;
    UINT1               u1IsShowAll = OSIX_TRUE;

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    if (u4ServiceConfIndex != Y1564_ZERO)
    {
        /* show information required for specific Service Configuration
         * or Context */
        u4NxtServiceConfId = u4ServiceConfIndex;
        u4NxtServiceConfContextId = u4ContextId;
        u1IsShowAll = OSIX_FALSE;
    }
    else
    {
        /* show information for all the Service Configurations */
        /* Get the first Service Configuration Index */
        if (nmhGetNextIndexFsY1564ServiceConfTable (u4ContextId,
                                                    &u4NxtServiceConfContextId,
                                                    u4ServiceConfIndex,&u4NxtServiceConfId)
                                                    != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    /* Get the Service Configuration Entry */
    pServiceConfEntry = Y1564UtilGetServConfEntry (u4NxtServiceConfContextId,
                                                   u4NxtServiceConfId);
    if (pServiceConfEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {        
        if (u4NxtServiceConfContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4ServiceConfIndex = u4NxtServiceConfId;

        /* Service Configuration Index */
        CliPrintf (CliHandle, "\r\nService Configuration Number : %u\r\n", u4NxtServiceConfId);
        CliPrintf (CliHandle, "----------------------------------------\r\n");

        /* Bandwith Profile Parametrs */
        CliPrintf (CliHandle, "%-33s : %s\r\n", "Color Mode",
                   ((pServiceConfEntry->u1ServiceConfColorMode ==
                     Y1564_SERV_CONF_COLOR_AWARE) ?
                    "Color Aware" : "Non-color Aware"));

        CliPrintf (CliHandle, "%-33s : %s\r\n","Coupling Flag",
                   ((pServiceConfEntry->u1ServiceConfCoupFlag ==
                     Y1564_COUPLING_TYPE_COUPLED) ?
                    "Coupling Enable" : "Coupling Disable"));

        CliPrintf (CliHandle, "%-33s : %d\r\n", "CIR",
                   pServiceConfEntry->u4ServiceConfCIR);

        CliPrintf (CliHandle, "%-33s : %d\r\n", "CBS",
                   pServiceConfEntry->u4ServiceConfCBS);
        CliPrintf (CliHandle, "%-33s : %d\r\n", "EIR",
                   pServiceConfEntry->u4ServiceConfEIR);

        CliPrintf (CliHandle, "%-33s : %d\r\n", "EBS",
                   pServiceConfEntry->u4ServiceConfEBS);

        CliPrintf (CliHandle, "\r\n");

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        /* Get the next Service Configuration Index */
        if (nmhGetNextIndexFsY1564ServiceConfTable (u4ContextId,&u4NxtServiceConfContextId,
                                                    u4ServiceConfIndex,&u4NxtServiceConfId) 
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Get the next Service Configuration Entry */
        pServiceConfEntry = Y1564UtilGetServConfEntry (u4NxtServiceConfContextId,
                                                       u4NxtServiceConfId);

    }
    while (pServiceConfEntry != NULL);
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4ServiceConfIndex);
#endif
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowPerfTestTable
 * Description   : This routine shows the contents present in the
 *                 Service Configuration Table.
 * Input         : CliHandle - Handle to CLI
 *                 u4ContextId - Context Identifier
 *                 pu4PerfTestId  - Pointer to Performance Test Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowPerfTestTable (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4PerfTestIndex)
{

    tPerfTestInfo      *pPerfTestEntry = NULL;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4NxtPerfTestId = Y1564_ZERO;
    UINT4               u4NxtPerfTestContextId = Y1564_ZERO;
    UINT2               u2Index;
    UINT1               u1Flag = Y1564_SNMP_TRUE;
    BOOL1               bResult = Y1564_ZERO;
    UINT1               u1IsShowAll = OSIX_TRUE;

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d", 
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }


    if (u4PerfTestIndex != Y1564_ZERO)
    {
        /* show information required for specific Performance Test
         * or Context */
        u4NxtPerfTestId = u4PerfTestIndex;
        u4NxtPerfTestContextId = u4ContextId;
        u1IsShowAll = OSIX_FALSE;
    }
    else
    {
        /* show information for all the Performance Test */
        /* Get the first Performance Test Index */
        if (nmhGetNextIndexFsY1564PerformanceTestTable (u4ContextId,&u4NxtPerfTestContextId,
                                                        u4PerfTestIndex,&u4NxtPerfTestId)
                                                        !=SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    /* Get the Performance Test Entry */
    pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4NxtPerfTestContextId,u4NxtPerfTestId);
    if (pPerfTestEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {     
        if (u4NxtPerfTestContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4PerfTestIndex = u4NxtPerfTestId;

        /* Performance Test Index */
        CliPrintf (CliHandle, "\r\nPerformance Test Number : %u\r\n", u4NxtPerfTestId);
        CliPrintf (CliHandle, "----------------------------------------\r\n");

        for (u2Index = 0; u2Index <= (MAX_Y1564_SLA_LIST_SIZE * Y1564_BITS_PER_BYTE);
             u2Index++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)),
                                     u2Index, MAX_Y1564_SLA_LIST_SIZE,
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue */
                continue;
            }
            else
            {
                if (u1Flag == Y1564_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, "%-33s : ", "Performance Test Sla List");
                    CliPrintf (CliHandle, "%d ", u2Index);
                    u1Flag = Y1564_SNMP_FALSE;
                }
                else
                {
                    CliPrintf (CliHandle, ",%d ", u2Index);
                }
            }
        }

        /* when none of the SLA's are configured for performance
         * test table atleast performance test sla list should be 
         * displayed without SLA values */

        if (u1Flag == Y1564_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "%-33s : ", "Performance Test Sla List");
        }

        u1Flag = Y1564_SNMP_TRUE;

        /* Performance Test Duration */
        CliPrintf (CliHandle, "\r\n");

        if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
        { 
            CliPrintf (CliHandle, "%-33s : %u minutes\r\n", "Performance Test Duration",
                       pPerfTestEntry->u1PerfTestDuration);
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %u hours\r\n", "Performance Test Duration",
                       pPerfTestEntry->u1PerfTestDuration);
        }

        /* Performance Test Status */
        CliPrintf (CliHandle, "%-33s : %s\r\n", "Status",
                   ((pPerfTestEntry->u1PerfTestStatus == Y1564_SLA_START) ?
                    "Started" : "Not Started"));
        
        CliPrintf (CliHandle, "\r\n");

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        /* Get the next Performance Test Index */
        if (nmhGetNextIndexFsY1564PerformanceTestTable (u4ContextId,
                                                        &u4NxtPerfTestContextId,
                                                        u4PerfTestIndex,
                                                        &u4NxtPerfTestId) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        /* Get the next Performance Test Entry */
        pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4NxtPerfTestContextId,
                                                    u4NxtPerfTestId);

    }
    while (pPerfTestEntry != NULL);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowConfTestReport
 * Description   : This routine shows the test report for the SLA
 * Input         : CliHandle   - Handle to CLI
 *                 pu4SlaId    - Pointer to SLA Identifier
 *                 u4ContextId - Context Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowConfTestReport (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaIndex)
{
    time_t                   lStartTime = Y1564_ZERO;
    time_t                   lEndTime = Y1564_ZERO;
    tConfTestReportInfo     *pConfigTestReportEntry = NULL;
    tConfTestReportInfo      ConfigTestReportEntry;
    tConfTestReportInfo     *pConfigValTestReportEntry  = NULL;
    tSlaInfo                *pSlaEntry = NULL;
    tSacInfo                *pSacEntry = NULL;
#ifndef MEF_WANTED
    tServiceConfInfo        *pServiceConfEntry = NULL;
#endif
    tTrafficProfileInfo     *pTrafProfEntry = NULL;
    tPerfTestInfo           *pPerfTestEntry = NULL;
    FLT4                     fInfoRateMin = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fInfoRateMean = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fInfoRateMax = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fCBS = Y1564_ZERO;
    FLT4                     fEBS = Y1564_ZERO;
    UINT4                    u4SlaId = Y1564_ZERO; 
    UINT4                    u4NxtSlaId = Y1564_ZERO;
    UINT4                    u4NxtSlaContextId = Y1564_ZERO;
    UINT4                    u4SlaConfigTestReportStepId = Y1564_ZERO;
    FLT4                     f4ServiceVlanSpeed = Y1564_ZERO;
    UINT4                    u4NxtValSlaCurrentTestMode = Y1564_ZERO;
    UINT4                    u4SlaCurrentTestMode = Y1564_ZERO;
    UINT4                    u4NxtSlaCurrentTestMode = Y1564_ZERO;
    UINT4                    u4NxtValSlaConfigTestReportStepId = Y1564_ZERO;
    UINT4                    u4NxtSlaConfigTestReportStepId = Y1564_ZERO;
    UINT4                    u4TempVal = Y1564_ZERO;
    UINT4                    u4TempStartTime = Y1564_ZERO;
    UINT4                    u4TempEndTime = Y1564_ZERO;
    UINT4                    u4TotalElapsedTime = Y1564_ZERO;
    INT4                     i4TempFrSize = Y1564_ZERO;
    INT4                     i4FrameSize = Y1564_ZERO;
    INT4                     i4NextFrameSize = Y1564_ZERO;
    UINT1                    au1TempString[Y1564_FIVE];
    UINT1                    u1Count = Y1564_ZERO;
    UINT1                    u1IsShowAll = OSIX_TRUE;
    UINT1                    u1TestRecurrence = OSIX_FALSE;
    UINT1                    au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT1                    au1String[CLI_Y1564_MAX_MAC_STRING_SIZE];
    UINT1                    u1TmpStatus = Y1564_ZERO;
    CONST CHR1              *apY1564PerfTestiString[] = {"", "K", "L", NULL};
    CONST CHR1              *apY1564ConfigTestStatus[] = {
        "    NE     ",
        "   Pass    ",
        "   Fail    ",
        "In-Progress",
        "  Aborted  ",
        "     -     ",
        NULL
    };

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }


    /*
       Below is the standard format to diplay the report generated for CIR test,

  |----------|-------|--------------------|--------------|------------------|------------------|
  |          | Pass/ |       IR           |      FL      |      FTD         |      FDV         |
  |          | Fail  |     (Mbit/s)       |              |      (ms)        |      (ms)        |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  | Overall  | xxxx  |  Min | Mean |  Max | Count |  FLR | Min | Mean | Max | Min | Mean | Max |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  | CIR test | xxxx  |                           Duration: tt seconds                          |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Step 1| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Step 2| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Step 3| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Step 4| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  | EIR test | xxxx  |                           Duration: tt seconds                          |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |     Green| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Yellow| ----  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |     Total| ----  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  | Traffic  |       |                    |                                                    |
  | Policing |       |Duration: tt seconds|           Transmitted rate : Mbit/sec              |
  | test     | xxxx  |                    |                                                    |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |     Green| xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |    Yellow| ----  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
  |     Total| ----  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |
  |----------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|
*/
    if (u4SlaIndex != Y1564_ZERO)
    {
        /*show information required fo r specific SLA*/
        u1IsShowAll = OSIX_FALSE;
        u4NxtSlaId = u4SlaIndex;
        u4NxtSlaContextId = u4ContextId;
    }
    /* Get the SLA Entry */
    if (nmhGetNextIndexFsY1564ConfigTestReportTable (u4ContextId, &u4NxtSlaContextId,
                                                     u4SlaIndex, &u4NxtSlaId,
                                                     i4FrameSize, &i4NextFrameSize,
                                                     (INT4) u4SlaCurrentTestMode,
                                                     (INT4 *)&u4NxtValSlaCurrentTestMode,
                                                     (INT4) u4SlaConfigTestReportStepId,
                                                     (INT4 *)&u4NxtValSlaConfigTestReportStepId)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u1IsShowAll == OSIX_TRUE)
    {
        if (u4ContextId != u4NxtSlaContextId)
        {
            return CLI_SUCCESS;
        }
        u4ContextId = u4NxtSlaContextId;
    }
    else
    {
        if (u4SlaIndex != u4NxtSlaId)
        {
            return CLI_SUCCESS;
        }
    }

    i4TempFrSize = i4NextFrameSize;

    pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4NxtSlaId);

    if (pSlaEntry == NULL)
    {
        return CLI_FAILURE;
    }

    pConfigTestReportEntry =
        Y1564UtilGetConfTestReportEntry (u4ContextId,
                                         u4NxtSlaId,
                                         i4NextFrameSize,
                                         u4NxtValSlaCurrentTestMode,
                                         u4NxtValSlaConfigTestReportStepId);

    if (pConfigTestReportEntry == NULL)
    {
        return CLI_FAILURE;
    }
 
    do
    {   
        if (u4NxtSlaContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }
               /* Get the SLA Entry */
        pSlaEntry = Y1564UtilSlaGetSlaEntry (pConfigTestReportEntry->u4ContextId,
                                             pConfigTestReportEntry->u4SlaId);

        if (pSlaEntry == NULL)
        {
            return CLI_FAILURE;
        }

       /* Get the Traffic Profile Entry */
        pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                    pSlaEntry->u4SlaTrafProfId);
        if (pTrafProfEntry == NULL)
        {
            return CLI_FAILURE;
        }
 
        if ((u4NxtSlaId != u4SlaId) || 
                (i4TempFrSize != pConfigTestReportEntry->i4FrameSize))
        {
            i4TempFrSize = pConfigTestReportEntry->i4FrameSize;

            u4TempStartTime = Y1564_ZERO;
            u4TempEndTime = Y1564_ZERO;
            Y1564CliGetTimeStamp (pConfigTestReportEntry, &u4TempStartTime,
                                  &u4TempEndTime);

            lStartTime = (INT4) u4TempStartTime;

            lEndTime = (INT4) u4TempEndTime;

            /* show the basic configuration */

            CliPrintf (CliHandle, "\r\n%-37s : \r\n", "UNI Attributes"); 

            f4ServiceVlanSpeed = (FLT4) pTrafProfEntry->u4PortSpeed;

            CliPrintf (CliHandle, "%-37s : %.2f Gbit/s\r\n", "Port Speed", 
                       (DBL8) (f4ServiceVlanSpeed / (FLT4) Y1564_BPS_TO_GBPS_CONV_FACTOR));

            Y1564CliShowPortType(CliHandle, pSlaEntry->u4ContextId, 
                                 pSlaEntry->u4SlaTrafProfId);

            CliPrintf (CliHandle, "\r\n%-37s : 1 \r\n", "Number of Services Tested");

            CliPrintf (CliHandle, "\r\n%-37s : %u\r\n", "SLA Number", 
                       pConfigTestReportEntry->u4SlaId);

#ifdef MEF_WANTED
            /* Bandwith Profile Parametrs */
            CliPrintf (CliHandle, "\r\n%-37s : ", "Service attributes");
            
            CliPrintf (CliHandle, "\r\n%-37s : ", "Bandwidth profile");

            CliPrintf (CliHandle, "\r\n%-37s : %d Mbit/s\r\n", "CIR",
                       (pSlaEntry->u4CIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);
    
            fCBS = (FLT4) pSlaEntry->u4CBS;

            CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "CBS",
                       (fCBS) / Y1564_BYTES_TO_KBYTES_CONV_FACTOR);

            CliPrintf (CliHandle, "%-37s : %d Mbit/s\r\n", "EIR",
                       (pSlaEntry->u4EIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);
            
            fEBS = (FLT4) pSlaEntry->u4EBS;

            CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "EBS",
                       (fEBS) / Y1564_BYTES_TO_KBYTES_CONV_FACTOR);

            CliPrintf (CliHandle, "%-37s : %s\r\n", "Colour aware",
                       ((pSlaEntry->u1ColorMode == Y1564_SERV_CONF_COLOR_AWARE) ?
                        "on" : "off"));
            if(pTrafProfEntry->u1TrafProfPCP != Y1564_ZERO)
            {
                CliPrintf (CliHandle, "%-37s : PCP\r\n", "Colour method " );
            }

#endif
#ifndef MEF_WANTED
            /* Get the Service-Configuration Entry */
            pServiceConfEntry = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                           pSlaEntry->u2SlaServiceConfId);
            if (pServiceConfEntry == NULL)
            {
                return CLI_FAILURE;
            }
            /* Bandwith Profile Parametrs */
            CliPrintf (CliHandle, "\r\n%-37s : ", "Service attributes");
            
            CliPrintf (CliHandle, "\r\n%-37s : ", "Bandwidth profile");
            
            CliPrintf (CliHandle, "\r\n%-37s : %d Mbit/s\r\n", "CIR",
                       (pServiceConfEntry->u4ServiceConfCIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);
      
            fCBS = (FLT4) pServiceConfEntry->u4ServiceConfCBS;

            CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "CBS",
                       (DBL8) ((fCBS) / (FLT4) Y1564_BYTES_TO_KBYTES_CONV_FACTOR));

            CliPrintf (CliHandle, "%-37s : %d Mbit/s\r\n", "EIR",
                       (pServiceConfEntry->u4ServiceConfEIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);
 
            fEBS = (FLT4) pServiceConfEntry->u4ServiceConfEBS;


            CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "EBS",
                       (DBL8) ((fEBS) / (FLT4) Y1564_BYTES_TO_KBYTES_CONV_FACTOR));

            CliPrintf (CliHandle, "\r\n%-37s : %s\r\n", "Colour aware",
                       ((pServiceConfEntry->u1ServiceConfColorMode ==
                         Y1564_SERV_CONF_COLOR_AWARE) ?
                        "on" : "off"));
            if(pTrafProfEntry->u1TrafProfPCP != Y1564_ZERO)
            {
                CliPrintf (CliHandle, "%-37s : PCP\r\n", "Colour method " );
            }
             
#endif
            /* Get the SAC Entry */
            pSacEntry = Y1564UtilSacGetSacEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaSacId);
            if (pSacEntry == NULL)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\n%-37s : ", "Service acceptance criteria");
            /* Frame Loss Ratio */
            CliPrintf (CliHandle, "\r\n%-37s : %u\r\n", "FLR",
                       pSacEntry->u4SacFrLossRatio);
            /* Frame Transfer Delay */
            CliPrintf (CliHandle, "%-37s : %u ms\r\n", "FTD",
                       pSacEntry->u4SacFrTxDelay);
            /* Frame Delat Variation */
            CliPrintf (CliHandle, "%-37s : %u ms\r\n", "FDV",
                       pSacEntry->u4SacFrDelayVar);
            /* Availability of the Service */
            CliPrintf (CliHandle, "%-37s : %.2f\r\n", "AVAIL",
                       (DBL8) pSacEntry->f4SacAvailability);

            /* MTU Value */
            CliPrintf (CliHandle, "%-37s : %d bytes\r\n", "MTU",
                       pTrafProfEntry->u4IfMtu );

            CliPrintf (CliHandle, "%-37s : %s\r\n", "Traffic policing",
                       ((pSlaEntry->u1SlaTrafPolicing ==
                         Y1564_SLA_DEF_TRAFFIC_POLICING_STATUS) ?
                        "on" : "off"));


            CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Per flow test frame definitions");

            MEMSET (au1String, Y1564_ZERO, sizeof (au1String));
            PrintMacAddress (pTrafProfEntry->DstMacAddr, au1String);

            CliPrintf (CliHandle, "%-37s : %s\r\n", "Destination MAC Address",
                       au1String);
            
            MEMSET (au1String, Y1564_ZERO, sizeof (au1String));
            PrintMacAddress (pTrafProfEntry->SrcMacAddr, au1String);

            CliPrintf (CliHandle, "%-37s : %s\r\n", "Source MAC Address",
                       au1String);

            CliPrintf (CliHandle, "%-37s : %d\r\n", "VLAN ID",
                       pTrafProfEntry->u2OutVlanId);

            CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Test Times");
            
            CliPrintf (CliHandle, "%-37s : %d seconds\r\n", "Service Configuration test step time",
                       pConfigTestReportEntry->u4StatsDuration);

            CliPrintf (CliHandle, "%-37s :", "Service Performance test time");

            if (pSlaEntry->u4PerfId != Y1564_ZERO) 
            {
                /* Get the Preformance Test Entry */
                pPerfTestEntry = Y1564UtilGetPerfTestEntry (pSlaEntry->u4ContextId,
                                                            pSlaEntry->u4PerfId);

		if (pPerfTestEntry == NULL)
		{
		    return CLI_FAILURE;
		}

                if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
                { 
                    CliPrintf (CliHandle, " %d minutes\r\n", pPerfTestEntry->u1PerfTestDuration);
                }
                else
                {
                    CliPrintf (CliHandle, " %d hours\r\n", pPerfTestEntry->u1PerfTestDuration);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            CliPrintf (CliHandle, "\r\n%-37s : %d bytes fixed\r\n", "Test Patterns used",
                       pConfigTestReportEntry->i4FrameSize);

            MEMSET (&ConfigTestReportEntry, Y1564_ZERO, sizeof (tConfTestReportInfo));

            ConfigTestReportEntry.u4ContextId = pConfigTestReportEntry->u4ContextId;
            ConfigTestReportEntry.u4SlaId = pConfigTestReportEntry->u4SlaId;
            ConfigTestReportEntry.i4FrameSize = pConfigTestReportEntry->i4FrameSize;
            ConfigTestReportEntry.u4CurrentTestMode = pConfigTestReportEntry->u4CurrentTestMode;
            ConfigTestReportEntry.u4StatsStepId = pConfigTestReportEntry->u4StatsStepId;
            u1TmpStatus = pConfigTestReportEntry->u1StatsResult;
            pConfigValTestReportEntry = pConfigTestReportEntry;

            while (pConfigValTestReportEntry != NULL)
            {
                if ((ConfigTestReportEntry.u4ContextId != pConfigValTestReportEntry->u4ContextId) ||
                    (ConfigTestReportEntry.u4SlaId != pConfigValTestReportEntry->u4SlaId) ||
                    (ConfigTestReportEntry.i4FrameSize != pConfigValTestReportEntry->i4FrameSize))
                {
                    break;
                }

                if (((u1TmpStatus != Y1564_FIVE) ? (u1TmpStatus < pConfigValTestReportEntry->u1StatsResult) : Y1564_ONE))
                {
                    if (!((pConfigTestReportEntry->u4CurrentTestMode == Y1564_TWO) &&
                          ((pConfigTestReportEntry->u4CurrentTestMode & Y1564_STEP_LOAD_CIR_TEST) == Y1564_STEP_LOAD_CIR_TEST)))
                    {
                        if (pConfigValTestReportEntry->u1StatsResult != Y1564_SLA_TEST_IN_PROGRESS)
                        {
                            if (pConfigValTestReportEntry->u1StatsResult != Y1564_FIVE)
                            {
                                u1TmpStatus = pConfigValTestReportEntry->u1StatsResult;
                            }
                        }
                        else if (pConfigValTestReportEntry->u1StatsResult == 
                                 Y1564_SLA_TEST_IN_PROGRESS)
                        {
                            u1TmpStatus = Y1564_FIVE;
                            break;
                        }
                    }
                }

                MEMSET (&ConfigTestReportEntry, Y1564_ZERO, sizeof (tConfTestReportInfo));

                ConfigTestReportEntry.u4ContextId = pConfigValTestReportEntry->u4ContextId;
                ConfigTestReportEntry.u4SlaId = pConfigValTestReportEntry->u4SlaId;
                ConfigTestReportEntry.i4FrameSize = pConfigValTestReportEntry->i4FrameSize;
                ConfigTestReportEntry.u4CurrentTestMode = pConfigValTestReportEntry->u4CurrentTestMode;
                ConfigTestReportEntry.u4StatsStepId = pConfigValTestReportEntry->u4StatsStepId;

                pConfigValTestReportEntry = Y1564UtilGetNextNodeFromConfTestReportTable
                    (&ConfigTestReportEntry);
            }

            CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Date and Time of Test");

            CliPrintf (CliHandle, "%-37s : %s", "SLA Test Start Time", ctime (&lStartTime));

            if ((lEndTime != Y1564_ZERO) && (u1TmpStatus != Y1564_FIVE))
            {
                CliPrintf (CliHandle, "%-37s : %s", "SLA Test End Time", ctime (&lEndTime));
               
            }
            if (lEndTime != Y1564_ZERO)
            {
                u4TotalElapsedTime = (UINT4) (lEndTime - lStartTime);

                CliPrintf (CliHandle, "\r\n%-37s : %d seconds \r\n", "Total Elapsed Time of Test",
                           u4TotalElapsedTime);
            }

            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "(K : Thousand, L : Lakhs)\r\n");

            /* start displaying the stats for each step */
            CliPrintf (CliHandle, "|---------|-----------|----------------|"
                       "---------|--------------|------------|\r\n");
            CliPrintf (CliHandle, "|         |   Pass/   |       IR       |"
                       "    FL   |     FTD      |    FDV     |\r\n");
            CliPrintf (CliHandle, "|         |   Fail/   |    (Mbit/s)    |"
                       "         |     (ms)     |    (ms)    |\r\n");
            CliPrintf (CliHandle, "|         |   Abort/  |                |"
                       "         |              |            |\r\n");
            CliPrintf (CliHandle, "|         |   NE/-/   |                |"
                       "         |              |            |\r\n");
            CliPrintf (CliHandle, "|         |In-Progress|                |"
                       "         |              |            |\r\n");
            CliPrintf (CliHandle, "|---------|-----------|---|--------|---|"
                       "-----|---|----|----|----|---|----|---|\r\n");
 
            /* for now displaying the single step results */
            CliPrintf (CliHandle, "| Overall |%.11s|Min|  Mean  |Max|Count|"
                       "FLR| Min|Mean| Max|Min|Mean|Max|\r\n",
                       (apY1564ConfigTestStatus[u1TmpStatus]));
            CliPrintf (CliHandle, "|---------|-----------|---|--------|---|"
                       "-----|---|----|----|----|---|----|---|\r\n");

        }
            if ((pConfigTestReportEntry->u4CurrentTestMode == 
                 Y1564_CIR_TEST))
            {
                /* for now displaying the Simple CIR Test Results */
                CliPrintf (CliHandle, "| CIR test|%.11s|                 "
                           "Duration: %2d seconds                 |\r\n",
                           apY1564ConfigTestStatus[pConfigTestReportEntry->
                           u1StatsResult], pConfigTestReportEntry->u4StatsDuration);
                CliPrintf (CliHandle,
                           "|---------|-----------|---|--------|---|"
                           "-----|---|----|----|----|---|----|---|\r\n");
            }
            else if ((pConfigTestReportEntry->u4CurrentTestMode 
                      == Y1564_STEP_LOAD_CIR_TEST))
            {
                if (u1TestRecurrence == OSIX_FALSE)
                {
                    /* for now displaying the Step LOad CIR Test Results */
                    CliPrintf (CliHandle, "|Step Load|%.11s|                 "
                               "Duration: %3d seconds                |\r\n",
                               apY1564ConfigTestStatus[pConfigTestReportEntry->
                               u1StatsResult], (pSlaEntry->u2SlaRateStep * 
                               pConfigTestReportEntry->u4StatsDuration));
                    CliPrintf (CliHandle,
                               "|---------|-----------|---|--------|---|"
                               "-----|---|----|----|----|---|----|---|\r\n");
                    u1TestRecurrence = OSIX_TRUE;
                }

            }
            else if ((pConfigTestReportEntry->u4CurrentTestMode
                      == Y1564_EIR_TEST_COLOR_AWARE) ||
                     (pConfigTestReportEntry->u4CurrentTestMode ==
                      Y1564_EIR_TEST_COLOR_BLIND))
            {
                if (u1TestRecurrence == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "| EIR test|%.11s|                 "
                               "Duration: %2d seconds                 |\r\n",
                               apY1564ConfigTestStatus[pConfigTestReportEntry->
                               u1StatsResult], pConfigTestReportEntry->u4StatsDuration);
                    CliPrintf (CliHandle,
                               "|---------|-----------|---|--------|---|"
                               "-----|---|----|----|----|---|----|---|\r\n");
                    u1TestRecurrence = OSIX_TRUE;
                }
            }
            else if ((pConfigTestReportEntry->u4CurrentTestMode
                      == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
                     (pConfigTestReportEntry->u4CurrentTestMode == 
                      Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
            {
                if (u1TestRecurrence == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "| Traffic |%.11s|                 "
                               "Duration: %2d seconds                 |\r\n",
                               apY1564ConfigTestStatus[pConfigTestReportEntry->
                               u1StatsResult], pConfigTestReportEntry->u4StatsDuration);
                    CliPrintf (CliHandle, "| policing|           |                "
                               "                                      |\r\n");

                    CliPrintf (CliHandle,
                               "|---------|-----------|---|--------|---|"
                               "-----|---|----|----|----|---|----|---|\r\n");
                    u1TestRecurrence = OSIX_TRUE;
                }
            }

            if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_CIR_TEST) ||
                (pConfigTestReportEntry->u4CurrentTestMode == Y1564_STEP_LOAD_CIR_TEST))
            {
                CliPrintf (CliHandle, "| Step %2d |%.11s|", 
                           pConfigTestReportEntry->u4StatsStepId,
                           apY1564ConfigTestStatus[pConfigTestReportEntry->
                           u1StatsResult]);
            }
            if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE) ||
                (pConfigTestReportEntry->u4CurrentTestMode == 
                 Y1564_TRAF_POLICING_TEST_COLOR_AWARE))
            {
                CliPrintf (CliHandle, "| %-7.6s |%.11s|",
                           ((pConfigTestReportEntry->u4StatsStepId == Y1564_ONE) ?
                            " Green" : "Yellow"),
                           apY1564ConfigTestStatus[pConfigTestReportEntry->
                           u1StatsResult]);
            }
            if ((pConfigTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_BLIND) ||
                (pConfigTestReportEntry->u4CurrentTestMode ==
                 Y1564_TRAF_POLICING_TEST_COLOR_BLIND))
            {
                CliPrintf (CliHandle, "| Step %2d |%.11s|", 
                           pConfigTestReportEntry->u4StatsStepId,
                           apY1564ConfigTestStatus[pConfigTestReportEntry->
                           u1StatsResult]);
            }

            fInfoRateMin = (FLT4) pConfigTestReportEntry->u4StatsIrMin;
            fInfoRateMean = (FLT4) pConfigTestReportEntry->u4StatsIrMean;
            fInfoRateMax = (FLT4) pConfigTestReportEntry->u4StatsIrMax;

            if (fInfoRateMax > Y1564_ZERO)
            {
                CliPrintf (CliHandle, "%.2f | %7.2f|%.2f |",
                          (DBL8) ((fInfoRateMin / (FLT4) Y1564_MBPS_CONV_FACTOR)),
                          (DBL8) ((fInfoRateMean / (FLT4) Y1564_MBPS_CONV_FACTOR)),
                          (DBL8) ((fInfoRateMax / (FLT4) Y1564_MBPS_CONV_FACTOR)));
            }
            else
            {
                CliPrintf (CliHandle, " %c | %7.2f| %c |", '-', 
                          (DBL8) ((fInfoRateMean / (FLT4) Y1564_MBPS_CONV_FACTOR)), '-');

            }

            Y1564CliWrapperValueHandling (pConfigTestReportEntry->u4StatsFrLossCnt,
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, "%5s|%3d|", au1TempString,
                           pConfigTestReportEntry->u4StatsFrLossRatio);
            }
            else
            {
                CliPrintf (CliHandle, "%5d|%3d|", 
                           pConfigTestReportEntry->u4StatsFrLossCnt,
                           pConfigTestReportEntry->u4StatsFrLossRatio);
            }

            Y1564CliWrapperValueHandling (pConfigTestReportEntry->u4StatsFrTxDelayMin,
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, "%4s|", au1TempString);
            }
            else
            {
                CliPrintf (CliHandle, "%4d|", 
                           pConfigTestReportEntry->u4StatsFrTxDelayMin);
            }

            Y1564CliWrapperValueHandling (pConfigTestReportEntry->u4StatsFrTxDelayMean,
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, "%4s|", au1TempString);
            }
            else
            {
                CliPrintf (CliHandle, "%4d|", 
                           pConfigTestReportEntry->u4StatsFrTxDelayMean);
            }

            Y1564CliWrapperValueHandling (pConfigTestReportEntry->u4StatsFrTxDelayMax,
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, "%4s|", au1TempString);
            }
            else
            {
                CliPrintf (CliHandle, "%4d|", 
                           pConfigTestReportEntry->u4StatsFrTxDelayMax);
            }

            if (pConfigTestReportEntry->u4StatsFrDelayVarMax > Y1564_ZERO)
            {
                CliPrintf (CliHandle, "%3d|%4d|%4c |\r\n",
                           pConfigTestReportEntry->u4StatsFrDelayVarMin,
                           pConfigTestReportEntry->u4StatsFrDelayVarMean,
                           pConfigTestReportEntry->u4StatsFrDelayVarMax);
            }
            else
            {
                Y1564CliWrapperValueHandling (pConfigTestReportEntry->
                                              u4StatsFrDelayVarMean,
                                              &u4TempVal, &u1Count);

                if (u1Count != Y1564_ZERO)
                {
                    MEMSET (au1TempString, 0, sizeof (au1TempString));
                    SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                            apY1564PerfTestiString[u1Count]);
                    CliPrintf (CliHandle, " %c |%4s| %c |\r\n", '-', au1TempString, '-');
                }
                else
                {
                    CliPrintf (CliHandle, " %c |%4d| %c |\r\n", '-', 
                               pConfigTestReportEntry->u4StatsFrDelayVarMean, '-');
                }
            }

            CliPrintf (CliHandle,
                       "|---------|-----------|---|--------|---|"
                       "-----|---|----|----|----|---|----|---|\r\n");

            u4SlaCurrentTestMode = pConfigTestReportEntry->u4CurrentTestMode;

            u4SlaId = u4NxtSlaId;

            if (nmhGetNextIndexFsY1564ConfigTestReportTable (pConfigTestReportEntry->u4ContextId,
                                                             &u4NxtSlaContextId,
                                                             pConfigTestReportEntry->u4SlaId, 
                                                             &u4NxtSlaId,
                                                             pConfigTestReportEntry->i4FrameSize,
                                                             &i4NextFrameSize,
                                                             (INT4) pConfigTestReportEntry->u4CurrentTestMode,
                                                             (INT4 *)&u4NxtSlaCurrentTestMode,
                                                             (INT4) pConfigTestReportEntry->u4StatsStepId,
                                                             (INT4 *)&u4NxtSlaConfigTestReportStepId)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if ((u1IsShowAll == OSIX_FALSE) && (u4NxtSlaId != u4SlaId))
            {
                return CLI_SUCCESS;
            }

            pConfigTestReportEntry =
                Y1564UtilGetConfTestReportEntry (u4NxtSlaContextId,
                                                 u4NxtSlaId,
                                                 i4NextFrameSize,
                                                 u4NxtSlaCurrentTestMode,
                                                 u4NxtSlaConfigTestReportStepId);

            if (((u4NxtSlaCurrentTestMode == u4SlaCurrentTestMode) &&
                (pConfigTestReportEntry->i4FrameSize != i4TempFrSize)) ||
                (u4NxtSlaCurrentTestMode != u4SlaCurrentTestMode))
            {
                u1TestRecurrence = OSIX_FALSE;
            }
    }
    while (pConfigTestReportEntry != NULL);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowPerfTestReport
 * Description   : This routine shows the test report for the SLA
 * Input         : CliHandle   - Handle to CLI
 *                 pu4SlaId    - Pointer to SLA Identifier
 *                 u4ContextId - Context Identifier
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowPerfTestReport (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4PerfTestIndex)
{
    time_t                   ltime;    
    tPerfTestReportInfo     *pPerfTestReportEntry = NULL;
    tPerfTestInfo           *pPerfTestEntry = NULL;
    tSlaInfo                *pSlaEntry = NULL;
#ifndef MEF_WANTED
    tServiceConfInfo        *pServiceConfEntry = NULL;
#endif
    tSacInfo                *pSacEntry = NULL;
    tTrafficProfileInfo     *pTrafProfEntry = NULL;
    FLT4                     fInfoRateMin = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fInfoRateMean = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fInfoRateMax = Y1564_ZERO;    /* for Mbits/s */
    FLT4                     fCBS = Y1564_ZERO;
    FLT4                     fEBS = Y1564_ZERO;
    UINT4                    u4TempVal = Y1564_ZERO;
    UINT4                    u4PerfTestId = Y1564_ZERO;
    UINT4                    u4NxtPerfTestId = Y1564_ZERO;
    UINT4                    u4PerfTestSlaId = Y1564_ZERO;
    UINT4                    u4NxtPerfTestSlaId = Y1564_ZERO;
    UINT4                    u4NxtPerfTestContextId = Y1564_ZERO;
    UINT4                    u4TotalElapsedTime = Y1564_ZERO;
    INT4                     i4FrameSize = Y1564_ZERO;
    INT4                     i4NxtFrameSize = Y1564_ZERO;
    FLT4                     f4ServiceVlanSpeed = 0;
    UINT1                    u1Count = Y1564_ZERO;
    UINT1                    u1IsShowAll = OSIX_TRUE;
    UINT1                    au1String[CLI_Y1564_MAX_MAC_STRING_SIZE];
    UINT1                    au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT1                    au1TempString[Y1564_FIVE];
    BOOL1                    bResult = Y1564_ZERO;
    CONST CHR1              *apY1564PerfTestiString[] = {"", "K", "L", NULL};
    CONST CHR1              *apY1564PerfTestStatus[] = {
        "  NE ",
        " Pass",
        " Fail",
        " In-P",
        "Abort",
        NULL
    };

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }


    /*
  Below is the standard format to diplay the report generated for CIR test,

  |-------------|-------|--------------------|--------------|------------------|------------------|---------|-----------|
  |             | Pass/ |       IR           |      FL      |      FTD         |      FDV         |  AVAIL  |  Unavail  |
  |             | Fail  |     (Mbit/s)       |              |      (ms)        |      (ms)        |   (%)   |           |
  |-------------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|---------|-----------|
  |   Overall   | xxxx  |  Min | Mean |  Max | Count |  FLR | Min | Mean | Max | Min | Mean | Max |         |   Count   |
  |-------------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|---------|-----------|
  |   Service   |       |                                                                                               |
  | Performance | xxxx  |                                   Duration: tt seconds                                        |
  |-------------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|---------|-----------|
  |   Service 1 | xxxx  | m.ir | m.ir | m.ir |  cnt  | f.lr | ms  |  ms  | ms  | ms  |  ms  | ms  |   (%)   |    cnt    |
  |-------------|-------|------|------|------|-------|------|-----|------|-----|-----|------|-----|---------|-----------|
*/
    if (u4PerfTestIndex != Y1564_ZERO)
    {
        /* show report for specific Performance Id
         * or Context */
        u4NxtPerfTestContextId = u4ContextId;
        u4NxtPerfTestId = u4PerfTestIndex;
        u1IsShowAll = OSIX_FALSE;

        /* Get the Preformance Test Entry */
        pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4NxtPerfTestContextId, 
                                                    u4NxtPerfTestId);

        if (pPerfTestEntry == NULL)
        {
            return CLI_FAILURE;
        }

        for (; u4PerfTestSlaId <= Y1564_MAX_SLA_LIST_ID; u4PerfTestSlaId++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)),
                                     u4PerfTestSlaId, sizeof (tY1564SlaList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                continue;
            }

            u4NxtPerfTestSlaId = u4PerfTestSlaId;
            break;
        }
    }

    /* Get the SLA Entry */
    if (nmhGetNextIndexFsY1564PerfTestReportTable (u4ContextId, 
                                                   &u4NxtPerfTestContextId,
                                                   u4PerfTestSlaId,
                                                   &u4NxtPerfTestSlaId,
                                                   u4PerfTestIndex, &u4NxtPerfTestId,
                                                   i4FrameSize, &i4NxtFrameSize)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u1IsShowAll == OSIX_TRUE)
    {
        if (u4ContextId != u4NxtPerfTestContextId)
        {
            return CLI_SUCCESS;
        }
        u4ContextId = u4NxtPerfTestContextId;
    }
    else
    {
        if (u4PerfTestIndex != u4NxtPerfTestId)
        {
            return CLI_SUCCESS;
        }
    }

    pPerfTestReportEntry =
                Y1564UtilGetPerfTestReportEntry (u4ContextId,
                                                 u4NxtPerfTestSlaId,
                                                 u4NxtPerfTestId,
                                                 i4NxtFrameSize);
    if (pPerfTestReportEntry == NULL)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (u4NxtPerfTestContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4PerfTestId = u4NxtPerfTestId;

        /* Get the Preformance Test Entry */
        pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4NxtPerfTestContextId, 
                                                    u4NxtPerfTestId);

        if (pPerfTestEntry == NULL)
        {
            return CLI_FAILURE;
        }

        pSlaEntry = Y1564UtilSlaGetSlaEntry (u4NxtPerfTestContextId, 
                                             pPerfTestReportEntry->u4SlaId);

        if (pSlaEntry == NULL)
        {
            return CLI_FAILURE;
        }

        /* Get the Traffic Profile Entry */
        pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                    pSlaEntry->u4SlaTrafProfId);
        if (pTrafProfEntry == NULL)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\n%-37s : \r\n", "UNI Attributes"); 

        f4ServiceVlanSpeed = (FLT4) pTrafProfEntry->u4PortSpeed;

        CliPrintf (CliHandle, "%-37s : %.2f Gbit/s\r\n", "Port Speed", 
                   (DBL8) (f4ServiceVlanSpeed/ (FLT4) Y1564_BPS_TO_GBPS_CONV_FACTOR));

        Y1564CliShowPortType(CliHandle, pSlaEntry->u4ContextId, 
                             pSlaEntry->u4SlaTrafProfId);

        CliPrintf (CliHandle, "\r\n%-37s : 1 \r\n", "Number of Services Tested");

        CliPrintf (CliHandle, "\r\n%-37s : %u\r\n", "Performance Number", 
                   u4NxtPerfTestId);

        CliPrintf (CliHandle, "\r\n%-37s : %u\r\n", "SLA Number", 
                   pPerfTestReportEntry->u4SlaId);

#ifdef MEF_WANTED
        /* Bandwith Profile Parametrs */
        CliPrintf (CliHandle, "\r\n%-37s : ", "Service attributes");

        CliPrintf (CliHandle, "\r\n%-37s : ", "Bandwidth profile");

        CliPrintf (CliHandle, "\r\n%-37s : %d Mbit/s\r\n", "CIR",
                   (pSlaEntry->u4CIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);

        fCBS = (FLT4) pSlaEntry->u4CBS;

        CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "CBS",
                   (fCBS) / Y1564_BYTES_TO_KBYTES_CONV_FACTOR);

        CliPrintf (CliHandle, "%-37s : %d Mbit/s\r\n", "EIR",
                   (pSlaEntry->u4EIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);

        fEBS = (FLT4) pSlaEntry->u4EBS;

        CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "EBS",
                   (fEBS) / Y1564_BYTES_TO_KBYTES_CONV_FACTOR);

        CliPrintf (CliHandle, "%-37s : %s\r\n", "Colour aware",
                   ((pSlaEntry->u1ColorMode == Y1564_SERV_CONF_COLOR_AWARE) ?
                    "on" : "off"));
        if(pTrafProfEntry->u1TrafProfPCP != Y1564_ZERO)
        {
            CliPrintf (CliHandle, "%-37s : PCP\r\n", "Colour method " );
        }

#endif
#ifndef MEF_WANTED
        /* Get the Service-Configuration Entry */
        pServiceConfEntry = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                       pSlaEntry->u2SlaServiceConfId);
        if (pServiceConfEntry == NULL)
        {
            return CLI_FAILURE;
        }
        /* Bandwith Profile Parametrs */
        CliPrintf (CliHandle, "\r\n%-37s : ", "Service attributes");

        CliPrintf (CliHandle, "\r\n%-37s : ", "Bandwidth profile");

        CliPrintf (CliHandle, "\r\n%-37s : %d Mbit/s\r\n", "CIR",
                   (pServiceConfEntry->u4ServiceConfCIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);

        fCBS = (FLT4) pServiceConfEntry->u4ServiceConfCBS;

        CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "CBS",
                   (DBL8) ((fCBS) / (FLT4) Y1564_BYTES_TO_KBYTES_CONV_FACTOR));

        CliPrintf (CliHandle, "%-37s : %d Mbit/s\r\n", "EIR",
                   (pServiceConfEntry->u4ServiceConfEIR) / Y1564_KBPS_TO_MBPS_CONV_FACTOR);

        fEBS = (FLT4) pServiceConfEntry->u4ServiceConfEBS;


        CliPrintf (CliHandle, "%-37s : %.2f KB\r\n", "EBS",
                   (DBL8) ((fEBS) / (FLT4) Y1564_BYTES_TO_KBYTES_CONV_FACTOR));

        CliPrintf (CliHandle, "\r\n%-37s : %s\r\n", "Colour aware",
                   ((pServiceConfEntry->u1ServiceConfColorMode ==
                     Y1564_SERV_CONF_COLOR_AWARE) ?
                    "on" : "off"));
        if(pTrafProfEntry->u1TrafProfPCP != Y1564_ZERO)
        {
            CliPrintf (CliHandle, "%-37s : PCP\r\n", "Colour method " );
        }

#endif
        /* Get the SAC Entry */
        pSacEntry = Y1564UtilSacGetSacEntry (pSlaEntry->u4ContextId,
                                             pSlaEntry->u4SlaSacId);
        if (pSacEntry == NULL)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n%-37s : ", "Service acceptance criteria");
        /* Frame Loss Ratio */
        CliPrintf (CliHandle, "\r\n%-37s : %u\r\n", "FLR",
                   pSacEntry->u4SacFrLossRatio);
        /* Frame Transfer Delay */
        CliPrintf (CliHandle, "%-37s : %u ms\r\n", "FTD",
                   pSacEntry->u4SacFrTxDelay);
        /* Frame Delat Variation */
        CliPrintf (CliHandle, "%-37s : %u ms\r\n", "FDV",
                   pSacEntry->u4SacFrDelayVar);
        /* Availability of the Service */
        CliPrintf (CliHandle, "%-37s : %.2f\r\n", "AVAIL",
                   (DBL8) pSacEntry->f4SacAvailability);

        /* MTU Value */
        CliPrintf (CliHandle, "%-37s : %d bytes\r\n", "MTU",
                   pTrafProfEntry->u4IfMtu );

        CliPrintf (CliHandle, "%-37s : %s\r\n", "Traffic policing",
                   ((pSlaEntry->u1SlaTrafPolicing ==
                     Y1564_SLA_DEF_TRAFFIC_POLICING_STATUS) ?
                    "on" : "off"));


        CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Per flow test frame definitions");

        MEMSET (au1String, Y1564_ZERO, sizeof (au1String));
        PrintMacAddress (pTrafProfEntry->DstMacAddr, au1String);

        CliPrintf (CliHandle, "%-37s : %s\r\n", "Destination MAC Address",
                   au1String);

        MEMSET (au1String, Y1564_ZERO, sizeof (au1String));
        PrintMacAddress (pTrafProfEntry->SrcMacAddr, au1String);

        CliPrintf (CliHandle, "%-37s : %s\r\n", "Source MAC Address",
                   au1String);

        CliPrintf (CliHandle, "%-37s : %d\r\n", "VLAN ID",
                   pTrafProfEntry->u2OutVlanId);

        CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Test Times");

        CliPrintf (CliHandle, "%-37s : %d seconds", "Service Configuration test step time",
                   pSlaEntry->u4SlaConfTestDuration);

        if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
        { 
            CliPrintf (CliHandle, "\r\n%-37s : %d minutes\r\n", "Service performance test time",
                       pPerfTestEntry->u1PerfTestDuration);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%-37s : %d hours\r\n", "Service performance test time",
                       pPerfTestEntry->u1PerfTestDuration);
        }


        CliPrintf (CliHandle, "\r\n%-37s : %d bytes fixed\r\n", "Test Patterns used",
                   i4NxtFrameSize);

        CliPrintf (CliHandle, "\r\n%-37s : \r\n", "Date and Time of Test");

        ltime = (INT4) pPerfTestReportEntry->PerfTestStartTimeStamp;
        CliPrintf (CliHandle, "%-37s : %s", "SLA Test Start Time", ctime (&ltime));

        if (pPerfTestReportEntry->PerfTestEndTimeStamp != Y1564_ZERO)
        {
            ltime = (INT4) pPerfTestReportEntry->PerfTestEndTimeStamp;
            CliPrintf (CliHandle, "%-37s : %s", "SLA Test End Time", ctime (&ltime));
        }

        if (pPerfTestReportEntry->PerfTestEndTimeStamp != Y1564_ZERO)
        {
            u4TotalElapsedTime = pPerfTestReportEntry->PerfTestEndTimeStamp - 
                pPerfTestReportEntry->PerfTestEndTimeStamp;

            CliPrintf (CliHandle, "\r\n%-37s : %d seconds \r\n", "Total Elapsed Time of Test",
                       u4TotalElapsedTime);
        }

        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "(In-P : In-Progress, K : Thousand, L : Lakhs, "
                              "Serv = Service)\r\n");

        /* start displaying the stats for each services */
        CliPrintf (CliHandle, "|-------|-----|------------|"
                   "---------|--------------|------------|-----|-----|\r\n");
        CliPrintf (CliHandle, "|       |Pass/|     IR     |"
                   "    FL   |     FTD      |    FDV     |AVAIL| Una-|\r\n");
        CliPrintf (CliHandle, "|       |Fail/|  (Mbit/s)  |"
                   "         |     (ms)     |    (ms)    |     | vail|\r\n");
        CliPrintf (CliHandle, "|       |In-P/|            |"
                   "         |              |            |     |     |\r\n");
        CliPrintf (CliHandle, "|       | NE/ |            |"
                   "         |              |            |     |     |\r\n");
        CliPrintf (CliHandle, "|       |Abort|            |"
                   "         |              |            |     |     |\r\n");
        CliPrintf (CliHandle, "|-------|-----|---|----|---|"
                   "-----|---|----|----|----|---|----|---|-----|-----|\r\n");

        CliPrintf (CliHandle, "|Overall|%.5s|Min|Mean|Max|Count|"
                   "FLR| Min|Mean| Max|Min|Mean|Max|     |Count|\r\n",
                   (apY1564PerfTestStatus[pPerfTestReportEntry->
                    u1StatsResult]));
        CliPrintf (CliHandle, "|-------|-----|------------|"
                   "---------|--------------|------------|-----|-----|\r\n");

        if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
        { 


            CliPrintf (CliHandle, "|Service|%.5s|         "
                       "              Duration: %2d minutes                   |\r\n",
                       apY1564PerfTestStatus[pPerfTestReportEntry->
                       u1StatsResult], pPerfTestEntry->u1PerfTestDuration);
        }
        else
        {
            CliPrintf (CliHandle, "|Service|%.5s|         "
                       "              Duration: %2d hours                     |\r\n",
                       apY1564PerfTestStatus[pPerfTestReportEntry->
                       u1StatsResult], pPerfTestEntry->u1PerfTestDuration);

        }

        CliPrintf (CliHandle, "|  Perf |     |         "
                   "                                                     |\r\n");
        CliPrintf (CliHandle, "|-------|-----|------------|"
                   "---------|--------------|------------|-----|-----|\r\n");

        CliPrintf (CliHandle, "|Serv %2d|%.5s|", u4PerfTestId,
                   apY1564PerfTestStatus[pPerfTestReportEntry->
                   u1StatsResult]);

        fInfoRateMin = (FLT4) pPerfTestReportEntry->u4StatsIrMin;
        fInfoRateMean = (FLT4) pPerfTestReportEntry->u4StatsIrMean;
        fInfoRateMax = (FLT4) pPerfTestReportEntry->u4StatsIrMax;

        if (fInfoRateMax > Y1564_ZERO)
        {
            CliPrintf (CliHandle, "%.2f|%4d|%.2f|",
                       (DBL8) ((fInfoRateMin / (FLT4) Y1564_MBPS_CONV_FACTOR)),
                       (DBL8) ((fInfoRateMean / (FLT4) Y1564_MBPS_CONV_FACTOR)),
                       (DBL8) ((fInfoRateMax / (FLT4) Y1564_MBPS_CONV_FACTOR)));
        }
        else
        {
            Y1564CliWrapperValueHandling ((pPerfTestReportEntry->u4StatsIrMean / 
                                           Y1564_KBPS_TO_MBPS_CONV_FACTOR),
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, " %c |%4s| %c |", '-', au1TempString, '-');
            }
            else
            {
                CliPrintf (CliHandle, " %c |%4d| %c |", '-', 
                           (pPerfTestReportEntry->u4StatsIrMean / 
                            Y1564_KBPS_TO_MBPS_CONV_FACTOR), '-');
            }
        }

        Y1564CliWrapperValueHandling (pPerfTestReportEntry->u4StatsFrLossCnt,
                                      &u4TempVal, &u1Count);

        if (u1Count != Y1564_ZERO)
        {
            MEMSET (au1TempString, 0, sizeof (au1TempString));
            SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                    apY1564PerfTestiString[u1Count]);
            CliPrintf (CliHandle, "%5s|%3d|", au1TempString,
                       pPerfTestReportEntry->u4StatsFrLossRatio);
        }
        else
        {
            CliPrintf (CliHandle, "%5d|%3d|", 
                       pPerfTestReportEntry->u4StatsFrLossCnt,
                       pPerfTestReportEntry->u4StatsFrLossRatio);
        }

        Y1564CliWrapperValueHandling (pPerfTestReportEntry->u4StatsFrTxDelayMin,
                                      &u4TempVal, &u1Count);

        if (u1Count != Y1564_ZERO)
        {
            MEMSET (au1TempString, 0, sizeof (au1TempString));
            SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                    apY1564PerfTestiString[u1Count]);
            CliPrintf (CliHandle, "%4s|", au1TempString);
        }
        else
        {
            CliPrintf (CliHandle, "%4d|", 
                       pPerfTestReportEntry->u4StatsFrTxDelayMin);
        }

        Y1564CliWrapperValueHandling (pPerfTestReportEntry->u4StatsFrTxDelayMean,
                                      &u4TempVal, &u1Count);

        if (u1Count != Y1564_ZERO)
        {
            MEMSET (au1TempString, 0, sizeof (au1TempString));
            SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                    apY1564PerfTestiString[u1Count]);
            CliPrintf (CliHandle, "%4s|", au1TempString);
        }
        else
        {
            CliPrintf (CliHandle, "%4d|", 
                       pPerfTestReportEntry->u4StatsFrTxDelayMean);
        }

        Y1564CliWrapperValueHandling (pPerfTestReportEntry->u4StatsFrTxDelayMax,
                                      &u4TempVal, &u1Count);

        if (u1Count != Y1564_ZERO)
        {
            MEMSET (au1TempString, 0, sizeof (au1TempString));
            SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                    apY1564PerfTestiString[u1Count]);
            CliPrintf (CliHandle, "%4s|", au1TempString);
        }
        else
        {
            CliPrintf (CliHandle, "%4d|", 
                       pPerfTestReportEntry->u4StatsFrTxDelayMax);
        }

        if (pPerfTestReportEntry->u4StatsFrDelayVarMax > Y1564_ZERO)
        {
            CliPrintf (CliHandle, "%3d|%4d|%3d|",
                       pPerfTestReportEntry->u4StatsFrDelayVarMin,
                       pPerfTestReportEntry->u4StatsFrDelayVarMean,
                       pPerfTestReportEntry->u4StatsFrDelayVarMax);
        }
        else
        {
            Y1564CliWrapperValueHandling (pPerfTestReportEntry->
                                          u4StatsFrDelayVarMean,
                                          &u4TempVal, &u1Count);

            if (u1Count != Y1564_ZERO)
            {
                MEMSET (au1TempString, 0, sizeof (au1TempString));
                SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                        apY1564PerfTestiString[u1Count]);
                CliPrintf (CliHandle, " %c |%4s| %c |", '-', au1TempString, '-');
            }
            else
            {
                CliPrintf (CliHandle, " %c |%4d| %c |", '-', 
                           pPerfTestReportEntry->u4StatsFrDelayVarMean, '-');
            }
        }

        Y1564CliWrapperValueHandling (pPerfTestReportEntry->u4StatsUnAvailCount,
                                      &u4TempVal, &u1Count);

        if (u1Count != Y1564_ZERO)
        {
            MEMSET (au1TempString, 0, sizeof (au1TempString));
            SPRINTF((CHR1 *)au1TempString, "%d%s", u4TempVal, 
                    apY1564PerfTestiString[u1Count]);
            CliPrintf (CliHandle, "%5.1f| %4s|\r\n",
                       (DBL8) pPerfTestReportEntry->f4StatsAvailability, au1TempString);
        }
        else
        {
            CliPrintf (CliHandle, "%5.1f| %4d|\r\n",
                       (DBL8) pPerfTestReportEntry->f4StatsAvailability,
                       pPerfTestReportEntry->u4StatsUnAvailCount);
        }

        CliPrintf (CliHandle, "|-------|-----|------------|"
                   "---------|--------------|------------|-----|-----|\r\n");

        CliPrintf (CliHandle, "\r\n");

        u4PerfTestSlaId = pPerfTestReportEntry->u4StatsPerfId;

        if (nmhGetNextIndexFsY1564PerfTestReportTable (pPerfTestReportEntry->u4ContextId,
                                                       &u4NxtPerfTestContextId,
                                                       pPerfTestReportEntry->u4SlaId,
                                                       &u4NxtPerfTestSlaId,
                                                       pPerfTestReportEntry->u4StatsPerfId,
                                                       &u4NxtPerfTestId,
                                                       pPerfTestReportEntry->i4FrameSize,
                                                       &i4NxtFrameSize)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if ((u1IsShowAll == OSIX_FALSE) && (u4NxtPerfTestId != u4PerfTestSlaId))
        {
            return CLI_SUCCESS;
        }

        pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry (u4NxtPerfTestContextId,
                                                                u4NxtPerfTestSlaId,
                                                                u4NxtPerfTestId,
                                                                i4NxtFrameSize);
    }
    while (pPerfTestReportEntry != NULL);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : Y1564CliShowGlobalInfo
 * Description   : This routine shows the contents present in the
 *                 Global Information.
 * Input         : CliHandle - Handle to CLI
 * Output        : NONE
 * Return        : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
Y1564CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tY1564ContextInfo      *pContextEntry = NULL;
    UINT1                  au1VcAlias[VCM_ALIAS_MAX_LEN];
    INT4                   i4SystemControl = Y1564_ZERO;


    MEMSET (au1VcAlias, 0, sizeof (au1VcAlias));

  if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                   u4ContextId);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    pContextEntry = Y1564CxtGetContextEntry (u4ContextId);

    if (pContextEntry == NULL)
    {
        return CLI_FAILURE;
    }


    /* Switch Index */
    CliPrintf (CliHandle, "------------------------------------------\r\n");

    /* Checking SystemControl value other than default value */
    nmhGetFsY1564ContextSystemControl (u4ContextId, &i4SystemControl);

    /* System Control Status */
    CliPrintf (CliHandle, "%-36s : %s\r\n", "Y1564 System Status",
               ((i4SystemControl == Y1564_START) ? "Start" : "Shutdown"));
    /* Module Status */
    CliPrintf (CliHandle, "%-36s : %s\r\n", "Y1564 Module Status",
               ((pContextEntry->u1ModuleStatus == Y1564_ENABLED) ? "Enable" : "Disable"));
    /* Trap Status */
    CliPrintf (CliHandle, "%-36s : %s\r\n", "Y1564 Trap Status",
               ((pContextEntry->u1TrapStatus == Y1564_SNMP_TRUE) ? "Enable" : "Disable"));
    /* No of configuration test running in the context */
    CliPrintf (CliHandle, "%-36s : %u\r\n", "Number Of Configuration Test Running",
               pContextEntry->u1ConfTestCount);
    /* No of performance test running in the context */
    CliPrintf (CliHandle, "%-36s : %u\r\n", "Number Of Performance Test Running",
               pContextEntry->u1PerfTestCount);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564CliSelectContextOnMode                      *
 *                                                                           *
 *    Description         : This function is used to check the Mode of       *
 *                          the command and also if it a Config Mode         *   
 *                          command it will do SelectContext for the         *
 *                          Context.                                         *
 *                                                                           *
 *    Input(s)            : u4Cmd - CLI Command.                             *
 *                                                                           *
 *    Output(s)           : CliHandle - Contains error messages.             *
 *                          pu4ContextId - Context Id.                       *
 *                          pu2LocalPort - Local Port Number.                *
 *                                                                           *
 *    Returns            : Y1564_SUCCESS/Y1564_FAILURE                       *
 *                                                                           * 
 *****************************************************************************/
INT4
Y1564CliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                             UINT4 *pu4Context, UINT2 *pu2LocalPort)
                          
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    BOOL1               b1IntfCmd = Y1564_FALSE;
    UNUSED_PARAM(u4Cmd);
    *pu4Context = Y1564_DEFAULT_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != Y1564_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (b1IntfCmd) is used only in case of MI
         * by refering this flag we have to get the context-id and
         * local port number from the IfIndex (CLI_GET_IFINDEX)*/
        b1IntfCmd = Y1564_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * Y1564GetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (Y1564VcmGetSystemMode () == VCM_MI_MODE)
    {
        if (b1IntfCmd == Y1564_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (Y1564VcmGetContextInfoFromIfIndex
                    ((UINT2) i4PortId, pu4Context, pu2LocalPort) != VCM_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return Y1564_FAILURE;
                }
            }
        }
    }
    return Y1564_SUCCESS;
 }
/***************************************************************************
 *                                                                         *
 *     Function Name : Y1564CliShowDebug                                   *
 *                                                                         *
 *     Description   : This function prints the debug level                *
 *                                                                         *
 *     Input(s)      :                                                     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ****************************************************************************/
VOID
Y1564CliShowDebug (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4DbgLevel = 0;

    if ((Y1564CxtIsY1564Started (u4ContextId)) != OSIX_TRUE)
    {
        return;
    }

    nmhGetFsY1564ContextTraceOption (u4ContextId, &u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rY1564:\n");

    if ((u4DbgLevel & Y1564_ALL_TRC) == Y1564_ALL_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 All debugging is on\r\n");
        return;
    }
    if ((u4DbgLevel & Y1564_INIT_SHUT_TRC) == Y1564_INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 start and shutdown debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_MGMT_TRC) == Y1564_MGMT_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 management debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_CTRL_TRC) == Y1564_CTRL_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 control debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_RESOURCE_TRC) == Y1564_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 resources debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_TIMER_TRC) == Y1564_TIMER_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 timer debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_CRITICAL_TRC) == Y1564_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 critical debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_Y1731_TRC) == Y1564_Y1731_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 y1731 debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_TEST_TRC) == Y1564_TEST_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 test debugging is on\r\n");
    }
    if ((u4DbgLevel & Y1564_SESSION_TRC) == Y1564_SESSION_TRC)
    {
        CliPrintf (CliHandle, "  Y1564 session debugging is on\r\n");
    }
    return;
}

/*****************************************************************************
 * Function    :  Y1564CliSaveConfReport                                     *
 *                                                                           *
 * Description :  This function save the Confguration test Report            *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
INT4
Y1564CliSaveConfReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                        CHR1 * pu1FileName, UINT4 u4SlaId)
{
    CHR1                    *pu1TempFileName = "report.txt";
    UINT1                    au1UserCmd[204];
    UINT1                    au1TempCmd[204];
    UINT1                    au1FileName[MAX_FLASH_FILENAME_LEN];
    tSlaInfo                *pSlaEntry = NULL;
    UINT1                    au1VcAlias[VCM_ALIAS_MAX_LEN];
    tIPvXAddr                DstIpAddress;
    tIp6Addr                 Ip6Addr;
    tIPvXAddr                IpAddress;
    UINT4                    u4DstIpAddress = 0;
    UINT4                    u4Val = 0;
    UINT1                    au1TempFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    UINT1                    au1DstHostName[DNS_MAX_QUERY_LEN];

    MEMSET (au1TempFileName, 0, sizeof (au1TempFileName));
    MEMSET (&au1DstHostName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&DstIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&IpAddress, 0, sizeof (tIPvXAddr));

    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }


    if (u4SlaId != 0)
    {
        /* Get the SLA Entry */
        pSlaEntry = Y1564UtilSlaGetSlaEntry (u4ContextId, u4SlaId);

        if (pSlaEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% Invalid Sla Entry \r\n ");
            return CLI_FAILURE;
        }
    }

    MEMSET (au1UserCmd, Y1564_ZERO, sizeof (au1UserCmd));

    Y1564_FILE_SAVE (u4SlaId, au1VcAlias, au1UserCmd);
    Y1564_UNLOCK ();


    if (u4Command == CLI_Y1564_REPORT_SLA)
    {
        MEMSET (au1FileName, Y1564_ZERO, sizeof (au1FileName));
        MEMCPY (au1FileName, pu1FileName, STRLEN (pu1FileName));

        MEMSET (au1TempCmd, Y1564_ZERO, sizeof (au1TempCmd));
        Y1564_FILE_ERASE (pu1FileName, au1TempCmd);
        cli_file_operation ((CONST CHR1 *) au1TempCmd, (CONST CHR1 *) au1FileName);

        cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) au1FileName);
    }
    else if (u4Command == CLI_Y1564_REPORT_SLA_TFTP)
    {
        MEMSET (au1TempCmd, Y1564_ZERO, sizeof (au1TempCmd));
        MEMCPY (au1TempCmd, pu1FileName, STRLEN (pu1FileName));

        cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) pu1TempFileName);

        MEMSET (au1UserCmd, Y1564_ZERO, sizeof (au1UserCmd));

        if (u4Command == CLI_Y1564_REPORT_SLA_TFTP)
        {
            if (CliGetTftpParams ((INT1 *) au1TempCmd, (INT1 *) au1TempFileName, 
                                  &DstIpAddress, (UINT1 *) au1DstHostName) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");
                Y1564_LOCK ();
                return CLI_FAILURE;
            }
            if (DstIpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4DstIpAddress, DstIpAddress.au1Addr);
                /* Check for self IP address */
                if (CfaIpIfIsOurAddress (u4DstIpAddress) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% This TFTP operation is not permitted for self IP  \r\n");
                    Y1564_LOCK ();
                    return CLI_FAILURE;
                }
            }
#ifdef IP6_WANTED
            else
            {
                MEMCPY (&Ip6Addr, DstIpAddress.au1Addr, sizeof (tIp6Addr));
                if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) == NETIPV6_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% This SFTP operation is not permitted for self IP  \r\n");
                    Y1564_LOCK ();
                    return CLI_FAILURE;
                }
            }
#else
            UNUSED_PARAM(u4Val);
#endif
        }

        IssCopyFileGeneric (CliHandle, (UINT1 *) pu1TempFileName,
                                          Y1564_FOUR,
                                          NULL, NULL,
                                          IpAddress,
                                          NULL,
                                          au1TempFileName,
                                          Y1564_ONE,
                                          NULL, NULL,
                                          DstIpAddress, au1DstHostName);

        CliSetIssErase (CliHandle, Y1564_FOUR, (UINT1 *) pu1TempFileName);
    }

    Y1564_LOCK ();

    return CLI_SUCCESS;
}


/*****************************************************************************
 * Function    :  Y1564CliSavePerfReport                                     *
 *                                                                           *
 * Description :  This function save the Performance test Report             *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
Y1564CliSavePerfReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                        CHR1 * pu1FileName, UINT4 u4PerfTestId)
{
    CHR1                    *pu1TempFileName = "report.txt";
    UINT1                    au1UserCmd[204];
    UINT1                    au1TempCmd[204];
    UINT1                    au1FileName[MAX_FLASH_FILENAME_LEN];
    tPerfTestInfo           *pPerfTestEntry = NULL;
    UINT1                    au1VcAlias[VCM_ALIAS_MAX_LEN];
    tIPvXAddr                DstIpAddress;
    tIp6Addr                 Ip6Addr;
    tIPvXAddr                IpAddress;
    UINT4                    u4DstIpAddress = 0;
    UINT4                    u4Val = 0;
    UINT1                    au1TempFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    UINT1                    au1DstHostName[DNS_MAX_QUERY_LEN];

    MEMSET (au1TempFileName, 0, sizeof (au1TempFileName));
    MEMSET (&au1DstHostName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&DstIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&IpAddress, 0, sizeof (tIPvXAddr));



    MEMSET (au1VcAlias, Y1564_ZERO, VCM_ALIAS_MAX_LEN);

    if (Y1564VcmGetAliasName (u4ContextId, au1VcAlias)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "unable to get Alias Name for context Id  %d",
                   u4ContextId);
        return CLI_FAILURE;
    }

    /* Switch Index */
    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n", au1VcAlias);

    if (Y1564UtilIsY1564Started (u4ContextId) != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r\n %%Y1564 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    if (u4PerfTestId != 0)
    {
        /* Get the  PerfTest Entry */
        pPerfTestEntry = Y1564UtilGetPerfTestEntry (u4ContextId,u4PerfTestId);

        if (pPerfTestEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% Invalid PerfTest Entry \r\n ");
            return CLI_FAILURE;
        }
    }

    MEMSET (au1UserCmd, Y1564_ZERO, sizeof (au1UserCmd));

    Y1564_PERF_FILE_SAVE (u4PerfTestId, au1VcAlias, au1UserCmd);

    Y1564_UNLOCK ();
    if (u4Command == CLI_Y1564_PERF_REPORT_SLA)
    {
        MEMSET (au1FileName, Y1564_ZERO, sizeof (au1FileName));
        MEMCPY (au1FileName, pu1FileName, STRLEN (pu1FileName));

        MEMSET (au1TempCmd, Y1564_ZERO, sizeof (au1TempCmd));
        Y1564_FILE_ERASE (pu1FileName, au1TempCmd);
        cli_file_operation ((CONST CHR1 *) au1TempCmd, (CONST CHR1 *) au1FileName);

        cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) au1FileName);
    }
    else if (u4Command == CLI_Y1564_PERF_REPORT_SLA_TFTP)
    {
        MEMSET (au1TempCmd, Y1564_ZERO, sizeof (au1TempCmd));
        MEMCPY (au1TempCmd, pu1FileName, STRLEN (pu1FileName));

        cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) pu1TempFileName);

        if (CliGetTftpParams ((INT1 *) au1TempCmd, (INT1 *) au1TempFileName, 
                              &DstIpAddress, (UINT1 *) au1DstHostName) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");
            Y1564_LOCK ();
            return CLI_FAILURE;
        }
        if (DstIpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4DstIpAddress, DstIpAddress.au1Addr);
            /* Check for self IP address */
            if (CfaIpIfIsOurAddress (u4DstIpAddress) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% This TFTP operation is not permitted for self IP  \r\n");
                Y1564_LOCK ();
                return CLI_FAILURE;
            }
        }
#ifdef IP6_WANTED
        else
        {
            MEMCPY (&Ip6Addr, DstIpAddress.au1Addr, sizeof (tIp6Addr));
            if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) == NETIPV6_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% This SFTP operation is not permitted for self IP  \r\n");
                Y1564_LOCK ();
                return CLI_FAILURE;
            }
        }
#else
        UNUSED_PARAM(u4Val);
#endif

       IssCopyFileGeneric (CliHandle, (UINT1 *) pu1TempFileName,
                                          Y1564_FOUR,
                                          NULL, NULL,
                                          IpAddress,
                                          NULL,
                                          au1TempFileName,
                                          Y1564_ONE,
                                          NULL, NULL,
                                          DstIpAddress, au1DstHostName);

        CliSetIssErase (CliHandle, Y1564_FOUR, (UINT1 *) pu1TempFileName);
    }

    Y1564_LOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  Y1564CliDeleteReport                                       *
 *                                                                           *
 * Description :  This function delete test Report                           *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
INT4
Y1564CliDeleteReport (tCliHandle CliHandle, CHR1 * pu1FileName)
{
    UINT1                    au1UserCmd[204];

    MEMSET (au1UserCmd, Y1564_ZERO, sizeof (au1UserCmd));

    Y1564_UNLOCK ();

    MEMSET (au1UserCmd, Y1564_ZERO, sizeof (au1UserCmd));
    Y1564_FILE_ERASE (pu1FileName, au1UserCmd);
    cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) pu1FileName);

    Y1564_LOCK ();

    UNUSED_PARAM(CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  Y1564CliWrapperValueHanding                                *
 *                                                                           *
 * Description :  This function delete test Report                           *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
PRIVATE VOID
Y1564CliWrapperValueHandling (UINT4 u4WrapValue, UINT4 * pu4TempValue, 
                              UINT1 *pu1Count)
{
    UINT1       u1Count = Y1564_ZERO;
    UINT4       u4TmpValue = Y1564_ZERO;

    u4TmpValue = u4WrapValue;

    while (u4TmpValue / Y1564_KBPS_TO_MBPS_CONV_FACTOR)
    {
        u1Count++;
        u4TmpValue /= Y1564_KBPS_TO_MBPS_CONV_FACTOR;
    }

    *pu1Count = u1Count;
    *pu4TempValue = u4TmpValue;
    return;
}

/*****************************************************************************
 * Function    :  Y1564CliShowPortType                                       *
 *                                                                           *
 * Description :  This function prints Port Type                             *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                u4ContextId  - Context ID                                  *
 *                u4TrafProfId - Traffic profile ID                          *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  None                                                       *
 ****************************************************************************/ 
VOID
Y1564CliShowPortType(tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafProfId)
{

    tTrafficProfileInfo *pTrafProfEntry;
    pTrafProfEntry = Y1564UtilGetTrafProfEntry (u4ContextId,
                                                u4TrafProfId);
    if (pTrafProfEntry == NULL)
    {
            return ;
    }
 
    if(OSIX_FAILURE ==  Y1564UtilPortType(pTrafProfEntry))
    {
        return;
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_LXFD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base LXFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_THD)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base THD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_TFD)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base TFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_TXHD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base TXHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_TXFD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base TXFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_TFD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base TFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_X)
    {
        CliPrintf (CliHandle, "%-37s : 10000 Base X\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_2500_BASE_TFD)
    {
        CliPrintf (CliHandle, "%-37s : 2500 Base TFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_OTHER)
    {
        CliPrintf (CliHandle, "%-37s : Other\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType ==Y1564_TYPE_AUI )
    {
        CliPrintf (CliHandle, "%-37s : AUI\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_5)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base 5\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_FOIRL)
    {
        CliPrintf (CliHandle, "%-37s : Foirl\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_2)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base 2\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_T)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base T\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_FP)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base FP\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_FB)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base FB\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_FL)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base FL\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BROAD_36)
    {
        CliPrintf (CliHandle, "%-37s : 10 Broad 36\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_FLHD)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base FLHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_BASE_FLFD)
    {
        CliPrintf (CliHandle, "%-37s : 10 Base FLFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_T4)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base T4\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_FXHD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base FXHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_FXFD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base FXFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_T2HD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base T2HD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_T2FD)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base T2FD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_XHD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base XHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_XFD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base XFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_LXHD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base LXHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_SXHD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base SXHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_SXFD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base SXFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_CXHD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base CXHD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_CXFD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base CXFD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_THD)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base THD\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_LX4)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base LX4\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_R)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base R\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_ER)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base ER\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_LR)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base LR\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_SR)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base SR\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_W)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base W\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_EW)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base EW\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_LW)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base LW\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_SW)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base SW\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10GIG_BASE_CX4)
    {
        CliPrintf (CliHandle, "%-37s : 10GIG Base CX4\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_2_BASE_TL)
    {
        CliPrintf (CliHandle, "%-37s : 2 Base TL\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_10_PASS_TS)
    {
        CliPrintf (CliHandle, "%-37s : 10 Pass TS\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_BX10D)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base BX10D\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_BX10U)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base BX10U\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_100_BASE_LX10)
    {
        CliPrintf (CliHandle, "%-37s : 100 Base LX10\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_BX10D)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base BX10D\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_BX10U)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base BX10U\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_LX10)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base LX10\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_PX10D)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base PX10D\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_PX10U)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base PX10U\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_PX20D)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base PX20D\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_1000_BASE_PX20U)
    {
        CliPrintf (CliHandle, "%-37s : 1000 Base PX20U\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_40GIG_BASE_X)
    {
        CliPrintf (CliHandle, "%-37s : 40GIG Base X\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_56GIG_BASE_X)
    {
        CliPrintf (CliHandle, "%-37s : 56GIG Base X\r\n", "Port Type");
    }
    if (pTrafProfEntry->u4PortType == Y1564_TYPE_2500_BASE_THD)
    {
        CliPrintf (CliHandle, "%-37s : 2500 Base THD\r\n", "Port Type");
    }
                                  
    return;
}

/*****************************************************************************
 * Function    :  Y1564CliGetTimeStamp                                       *
 *                                                                           *
 * Description :  This function get the start time and end time stamp        *
 *                                                                           *
 * Input       :  pConfigTestReportEntry - Test report entry                 *
 *                                                                           *
 * Output      :  u4TempStartTime                                            *
 *                u4TempEndTime                                              *
 *                                                                           *
 * Returns     :  None                                                       *
 ****************************************************************************/ 

PRIVATE VOID
Y1564CliGetTimeStamp(tConfTestReportInfo *pConfigTestReportEntry,
                     UINT4 *u4TempStartTime, UINT4 *u4TempEndTime)
{
    tConfTestReportInfo *pConfigTestReportInfo = NULL;
    INT4                 i4TestMode = Y1564_ZERO;
    INT4                 i4TestStep = Y1564_ZERO;
    INT4                 i4MaxStep = Y1564_ZERO;
    UINT4                u4StartTime = Y1564_ZERO;
    UINT4                u4EndTime = Y1564_ZERO;
    UINT1                u1Flag = Y1564_TRUE;


    for (i4TestMode = Y1564_ONE; i4TestMode <= Y1564_SIX; i4TestMode++)
    {
        if (i4TestMode == Y1564_ONE)
        {
            i4MaxStep = Y1564_ONE;
        }
        if (i4TestMode == Y1564_TWO)
        {
            i4MaxStep = Y1564_TEN;
        }
        else
        {
            i4MaxStep = Y1564_TWO;
        }
        for (i4TestStep = Y1564_ONE; i4TestStep <= i4MaxStep; i4TestStep++)
        {
            pConfigTestReportInfo =
                Y1564UtilGetConfTestReportEntry (pConfigTestReportEntry->u4ContextId,
                                                 pConfigTestReportEntry->u4SlaId,
                                                 pConfigTestReportEntry->i4FrameSize,
                                                 (UINT4) i4TestMode, (UINT4) i4TestStep);
            if (pConfigTestReportInfo != NULL)
            {
                if (u1Flag == Y1564_TRUE)
                {
                    nmhGetFsY1564ConfigTestReportTestStartTime (pConfigTestReportEntry->u4ContextId,
                                                                pConfigTestReportEntry->u4SlaId,
                                                                pConfigTestReportEntry->i4FrameSize,
                                                                i4TestMode, i4TestStep,
                                                                &u4StartTime);

                    *u4TempStartTime = u4StartTime;
                    if (u4StartTime != Y1564_ZERO)
                    {
                        u1Flag = Y1564_FALSE;
                    }
                }
                nmhGetFsY1564ConfigTestReportTestEndTime (pConfigTestReportEntry->u4ContextId,
                                                          pConfigTestReportEntry->u4SlaId,
                                                          pConfigTestReportEntry->i4FrameSize,
                                                          i4TestMode, i4TestStep,
                                                          &u4EndTime);
                /* For yellow frames, start time and end time is not
                 * updated. So before updating end time in local varaible
                 * validate the end time value got it from 
                 * configuration report entry */
                if (u4EndTime != Y1564_ZERO)
                {
                    *u4TempEndTime = u4EndTime;
                }
            }
        }
    }  
}


#endif
