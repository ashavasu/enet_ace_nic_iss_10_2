/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564trap.c,v 1.5 2016/02/18 10:44:35 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "y1564inc.h"
#include "fsy1564.h"

CHR1               *gac1Y1564TrapMsg[] = {
    NULL,
    "Transaction failure.",
    "Buffer Alloc Failure.",
    "Timer Failure."
};
/****************************************************************************
 *                           y1564trap.c prototypes                          *
 ****************************************************************************/
#ifdef SNMP_3_WANTED
PRIVATE tSNMP_OID_TYPE *Y1564TrapMakeObjIdFrmString (INT1 *pi1TextStr, 
                                                     UINT1 *pu1TableName);


PRIVATE tSNMP_VAR_BIND *Y1564TrapConstructString (tSlaInfo  *pSlaEntry, 
                                                  UINT1 *pu1ObjName,
                                                  UINT1 *pu1ObjValue, 
                                                  UINT2 u2ObjLen);

PRIVATE INT4  Y1564TrapParseSubIdNew (UINT1 **ppu1TempPtr, 
                                      UINT4 *pu4Value);

#endif
/*****************************************************************************
* Function Name      : Y1564SendTrapNotifications                            *
*                                                                            *
* Description        : This function will send an SNMP trap to the           *
*                      administrator for various Y1564 conditions.           *
*                                                                            *
* Input(s)           : pNotifyInfo - Information to be sent in the Trap      *
*                                   message.                                 *
*                      u1TrapEvent- Specific Type for Trap Message           *
*                                                                            *
* Output(s)          : None                                                  *
*                                                                            *
* Return Value(s)    : VOID                                                  *
*****************************************************************************/
PUBLIC VOID
Y1564SendTrapNotifications (tSlaInfo  *pSlaEntry, UINT1 u1TrapEvent)
{
#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               Y1564_TRAP_OID[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 88, 9 };
    UINT4               u4SpecTrapType = 0;
    UINT4               u4CtxtNameLen = 0;
    UINT1               au1Buf[Y1564_OBJECT_NAME_LEN];
    UINT1               au1Val[Y1564_OBJECT_VALUE_LEN];
    UINT1               au1SyslogStr[Y1564_MAX_TRAP_STR_LEN];
    UINT2               u2Len = 0;
    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    if (pSlaEntry == NULL)
    {
        return;
    }
    if (Y1564_TRAP_STATUS (pSlaEntry->u4ContextId) == Y1564_SNMP_FALSE)
    {
        /* Trap is not enabled for this context */
        return;
    }
    pEnterpriseOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, Y1564_TRAP_OID, sizeof (Y1564_TRAP_OID));
    pEnterpriseOid->u4_Length = sizeof (Y1564_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4SpecTrapType;

    /* Allocate the Memory for SNMP Trap OID to form the VbList */
    pSnmpTrapOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    MEMSET (au1SyslogStr, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1SyslogStr, "[Ctx : %s][SLAID : %d]-",
             Y1564_CONTEXT_NAME (pSlaEntry->u4ContextId), pSlaEntry->u4SlaId);

    switch (u1TrapEvent)
    {
        case Y1564_TRANSACTION_FAIL:
        case Y1564_TRAP_BUF_ALLOC_FAIL:  
        case Y1564_TRAP_TIMER_FAIL:
            u4SpecTrapType = Y1564_TRAP_FAILURE;
            break;
	default:
	    break;
    }
    pStartVb = pVbList;

    /* Filling the Context-Name */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (au1Val, 0, sizeof (au1Val));
    SPRINTF ((char *) au1Buf, "fsY1564ContextName");
    u4CtxtNameLen = sizeof (Y1564_CONTEXT_NAME (pSlaEntry->u4ContextId));
    STRCPY (au1Val, "Context:");
    STRNCAT (au1Val, Y1564_CONTEXT_NAME (pSlaEntry->u4ContextId), u4CtxtNameLen);
    pVbList->pNextVarBind
        =
        Y1564TrapConstructString (pSlaEntry, au1Buf, au1Val,
                                 (UINT2) STRLEN (au1Val));

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }
    pVbList = pVbList->pNextVarBind;
    /* Filling the SLA ID  */
    MEMSET (au1Val, 0, sizeof (au1Val));
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsY1564TrapSlaId");
    SPRINTF ((char *) au1Val, "SlaId : %d",
             pSlaEntry->u4SlaId);
    
    pVbList->pNextVarBind
        =
        Y1564TrapConstructString (pSlaEntry, au1Buf, au1Val,
                                  (UINT2) STRLEN (au1Val));

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    /* Filling the failure type */
    MEMSET (au1Val, 0, sizeof (au1Val));
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsY1564TypeOfFailure");
    u2Len = (UINT2) STRLEN (gac1Y1564TrapMsg[u1TrapEvent]);
	STRNCPY (au1Val,gac1Y1564TrapMsg[u1TrapEvent],
	STRLEN (gac1Y1564TrapMsg[u1TrapEvent]));
	au1Val[u2Len] = '\0';
	STRCAT (au1SyslogStr, au1Val);
    pVbList->pNextVarBind
        = Y1564TrapConstructString (pSlaEntry, au1Buf, au1Val,
                                   (UINT2) STRLEN (au1Val));

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }
    pContextName =
        SNMP_AGT_FormOctetString (Y1564_CONTEXT_NAME (pSlaEntry->u4ContextId),
                                  (INT4) u4CtxtNameLen);

    if (pContextName == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        Y1564_GLOBAL_TRC("Y1564SendTrapNotifications: Failed to form"
                         "Octet string for ContextName.\r\n");
        return;
    }
    /*Increment trap sent count */
    Y1564PortFmNotifyFaults (pEnterpriseOid, ENTERPRISE_SPECIFIC, u4SpecTrapType,
                            pStartVb, (UINT1 *) au1SyslogStr, pContextName);

    SNMP_AGT_FreeOctetString (pContextName);
#else
    UNUSED_PARAM (pSlaEntry);
    UNUSED_PARAM (u1TrapEvent);
#endif
    return;
}

#ifdef SNMP_3_WANTED
/******************************************************************************
 * Function :   Y1564TrapConstructString                                      *
 *                                                                            *
 * Description: This function used to construct the string based              *
 *              on the object name and value.                                 *
 *                                                                            *  
 * Input    :   pSlaEntry  - Pointer to the ring entry.                       *  
 *              pu1ObjName  - Pointer to the array of Object name.            * 
 *              pu1ObjValue - Pointer to the array of object value.           *
 *              u2ObjLen    - Length of the object value.                     *
 *                                                                            * 
 * Output   :   None.                                                         *
 *                                                                            *
 * Returns  :   pVbList or NULL                                               * 
 ******************************************************************************/
PRIVATE tSNMP_VAR_BIND *
Y1564TrapConstructString (tSlaInfo  *pSlaEntry, UINT1 *pu1ObjName,
                         UINT1 *pu1ObjValue, UINT2 u2ObjLen)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;

    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    pOid = Y1564TrapMakeObjIdFrmString ((INT1 *) pu1ObjName,
                                       (UINT1 *) fs_y1564_orig_mib_oid_table);
    if (pOid == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CRITICAL_TRC, "Y1564TrapMakeObjIdFrmString: OID "
                          "for %s Not Found. Failed!!!\r\n", pu1ObjName);
        return NULL;
    }
    pOstring = SNMP_AGT_FormOctetString (pu1ObjValue, (INT4) u2ObjLen);

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CRITICAL_TRC, "Y1564TrapMakeObjIdFrmString: Failed "
                          "to form Octet string for fsY1564CtxtName.\r\n");
        return NULL;
    }

    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                    pOstring, NULL, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CRITICAL_TRC, "Y1564TrapMakeObjIdFrmString: Failed "
                          "to form VarBind for fsY1564CtxtName.\r\n");
        return NULL;
    }

    return pVbList;
}
/******************************************************************************
 * Function :   Y1564TrapMakeObjIdFrmString                                   *
 *                                                                            *
 * Description: This Function retuns the OID  of the given string for the     *
 *              proprietary MIB.                                              *
 *                                                                            *
 * Input    :   pi1TextStr - pointer to the string.                           *
 *              pTableName - TableName has to be fetched.                     *
 *                                                                            * 
 * Output   :   None.                                                         * 
 *                                                                            *
 * Returns  :   pOidPtr or NULL                                               *
 * ****************************************************************************/
PRIVATE tSNMP_OID_TYPE *
Y1564TrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[Y1564_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;
    UINT2               u2Len = 0;

    MEMSET (ai1TempBuffer, 0, Y1564_OBJECT_NAME_LEN);
    /* see if there is an alpha descriptor at begining */
    if ((ISALPHA (*pi1TextStr)) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) && (u2Index < 256)); u2Index++)
        {
            ai1TempBuffer[u2Index] = (INT1) *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         Y1564_OBJECT_NAME_LEN);
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u2Len =
            (UINT2) ((Y1564_OBJECT_NAME_LEN-STRLEN(ai1TempBuffer)-1) <
                     STRLEN (pi1DotPtr) ?
                     (Y1564_OBJECT_NAME_LEN-STRLEN(ai1TempBuffer)-1) :  STRLEN (pi1DotPtr));

        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 u2Len);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;

    for (u2Index = 0; ((u2Index <= Y1564_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) (u2DotCount + 1);

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (Y1564TrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}
/******************************************************************************
 * Function :   Y1564TrapParseSubIdNew                                        *
 *                                                                            *
 * Description : Parse the string format in number.number..format.            *
 *                                                                            *
 * Input       : ppu1TempPtr - pointer to the string.                         *
 *               pu4Value    - Pointer the OID List value.                    *
 *                                                                            *
 * Output      : value of ppu1TempPtr                                         *
 *                                                                            *
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE                                 *
 ******************************************************************************/
PRIVATE INT4
Y1564TrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}
#endif
