/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564api.c,v 1.5 2016/02/16 10:25:40 siva Exp $
 *
 * Description: This file contains the Y1564 API related functions        
 *                                                        
 *****************************************************************************/
#ifndef _Y1564API_C
#define _Y1564API_C

#include "y1564inc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : Y1564ApiHandleExtRequest                                   *
 *                                                                           *
 * Description  : This function is the common entry point for all the        *
 *                external module.                                           *
 *                                                                           *
 * Input        : pY1564ReqParams - Pointer to the Request params.           *
 *                                                                           *
 * Output       : pY1564RespParams - Pointer to the response params          *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
Y1564ApiHandleExtRequest (tY1564ReqParams * pY1564ReqParams,
                          tY1564RespParams * pY1564RespParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    /*tY1564ReqParams     *pMsg = NULL;*/
    tY1564QMsg          *pMsg = NULL; 
    UNUSED_PARAM (pY1564RespParams);
    /* External Requests Handling */

    if (pY1564ReqParams == NULL)
    {
        return OSIX_FAILURE;
    }
  /* External Requests Handling */
    switch (pY1564ReqParams->u4ReqType)
    {
        /* Intentional fall through */
        case Y1564_CREATE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the creation of context
             *
             * Input : u4ContextId-Context id
             *
             **/
            Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Received Context "
                              "create Notification \r\n");
        case Y1564_DELETE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the deletion of context
             *
             * Input : u4ContextId-Context id
             **/
            Y1564_LOCK ();

            /* Check the system control Status */
            if (Y1564CxtIsY1564Started (pY1564ReqParams->u4ContextId)
                == Y1564_SHUTDOWN)
            {
                Y1564_UNLOCK ();
                return OSIX_SUCCESS;
            }
            Y1564_UNLOCK ();
            Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Received Context "
                              "Delete Notification\r\n");

        case Y1564_UPDATE_CONTEXT_NAME:
            /* This API is called by the VCM module.
             * This indicates the updation of context
             * Alias Name
             * Input : u4ContextId-Context id
             **/
            Y1564_LOCK ();

            /* Check the system control Status */
            if (Y1564CxtIsY1564Started (pY1564ReqParams->u4ContextId)
                == Y1564_SHUTDOWN)
            {
                Y1564_UNLOCK ();
                return OSIX_SUCCESS;
            }
            Y1564_UNLOCK ();
            Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Received Context "
                              "Alias Name update Notification\r\n");

        case ECFM_DEFECT_CONDITION_ENCOUNTERED:
        case ECFM_RDI_CONDITION_ENCOUNTERED:
        case ECFM_DEFECT_CONDITION_CLEARED:
        case ECFM_RDI_CONDITION_CLEARED:
            /* This is called by ECFM/Y1731 module to indicate the 
             * defect condition and defect clearance status 
             * Input : CFM entries, 
             *         Context Id,
             *         SLA Id */            
            Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Received either "
                              "defect or defect clearance Notification\r\n");
        case ECFM_Y1564_RESULT_RECEIVED:
            /* This is called by ECFM/Y1731 module to give the test 
             * result information.
             * Input : CFM entries, 
             *         Context Id,
             *         SLA Id 
             *         Performance Monitoring Results*/
        
            Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Received Performance "
                              "test result Notification\r\n");

            if ((pMsg = (tY1564QMsg *) MemAllocMemBlk (Y1564_QMSG_POOL ())) == NULL) 
            {
                Y1564_GLOBAL_TRC ("Y1564ApiHandleExtRequest: Allocation of "
                                  "memory for Queue Message FAILED !!!\r\n");
                return OSIX_FAILURE;
            }

            /* Form the message */
                    /* MEMCPY (pMsg, pY1564ReqParams, sizeof (tY1564ReqParams));*/
            MEMCPY (pMsg, pY1564ReqParams, sizeof (tY1564QMsg)); 
             pMsg->Y1564ReqParam.u4ContextId = pY1564ReqParams->u4ContextId;

            i4RetVal = Y1564MsgEnque (pMsg);
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* end of switch */
    return i4RetVal;
}
/****************************************************************************
 * FUNCTION NAME    : Y1564ApiLock                                          *
 *                                                                          *
 * DESCRIPTION      : Function to take the mutual exclusion protocol        *
 *                    semaphore                                             *
 *                                                                          *
 * INPUT            : NONE                                                  *
 *                                                                          *
 * OUTPUT           : NONE                                                  *
 *                                                                          *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                          *
 ****************************************************************************/
INT4
Y1564ApiLock (VOID)
{
    if (OsixSemTake (Y1564_SEM_ID ()) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : Y1564ApiUnLock                                        *
 *                                                                          *
 * DESCRIPTION      : Function to release the mutual exclusion protocol     *
 *                    semaphore                                             *
 *                                                                          *
 * INPUT            : NONE                                                  *
 *                                                                          *
 * OUTPUT           : NONE                                                  *
 *                                                                          *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                          *
 ****************************************************************************/
INT4
Y1564ApiUnLock (VOID)
{
    if (OsixSemGive (Y1564_SEM_ID ()) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function     : Y1564ApiCreateContext                                      *
 *                                                                           *
 * Description  : This function is called to by VCM Module to indicate the   *
 *                context creation.                                          *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
Y1564ApiCreateContext (UINT4 u4ContextId)
{
    tY1564ReqParams *pY1564ReqParams = NULL;
    tY1564RespParams *pY1564RespParams = NULL;
    INT4            i4RetVal = OSIX_SUCCESS;

    if (Y1564_INITIALISED () != Y1564_TRUE)
    {
        Y1564_GLOBAL_TRC ("Y1564 Module - not initialized\r\n");
        return OSIX_FAILURE;
    }
    pY1564ReqParams = (tY1564ReqParams *)MemAllocMemBlk (Y1564_REQPARAMS_POOL ());

    if (pY1564ReqParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ReqParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ReqParams, Y1564_ZERO, sizeof (tY1564ReqParams));

    pY1564ReqParams->u4ReqType = Y1564_CREATE_CONTEXT_MSG;
    pY1564ReqParams->u4ContextId = u4ContextId;

    i4RetVal = Y1564ApiHandleExtRequest (pY1564ReqParams, pY1564RespParams);

    MemReleaseMemBlock (Y1564_REQPARAMS_POOL(), (UINT1 *) pY1564ReqParams);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************
 * Function     : Y1564ApiDeleteContext                                      *
 *                                                                           *
 * Description  : This function is called to by VCM Module to indicate the   *
 *                context deletion.                                          *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
Y1564ApiDeleteContext (UINT4 u4ContextId)
{
    tY1564ReqParams *pY1564ReqParams = NULL;
    tY1564RespParams *pY1564RespParams = NULL;
    INT4            i4RetVal = OSIX_SUCCESS;

    pY1564ReqParams = (tY1564ReqParams *)MemAllocMemBlk (Y1564_REQPARAMS_POOL ());

    if (pY1564ReqParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ReqParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ReqParams, Y1564_ZERO, sizeof (tY1564ReqParams));

    pY1564ReqParams->u4ReqType = Y1564_DELETE_CONTEXT_MSG;
    pY1564ReqParams->u4ContextId = u4ContextId;

    i4RetVal = Y1564ApiHandleExtRequest (pY1564ReqParams, pY1564RespParams);

    MemReleaseMemBlock (Y1564_REQPARAMS_POOL(), (UINT1 *) pY1564ReqParams);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}


/******************************************************************************
 * FUNCTION NAME    : Y1564ApiUpdateCxtNameFromVcm                            *
 *                                                                            *
 * DESCRIPTION      : This function will be called by VCM module to indicate  *
 *                    context name change in VCM.                             *
 *                                                                            *
 * INPUT            : u4ContextId - Context Identifier                        *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
Y1564ApiUpdateCxtNameFromVcm (UINT4 u4ContextId)
{
    tY1564ReqParams *pY1564ReqParams = NULL;
    tY1564RespParams *pY1564RespParams = NULL;
    INT4            i4RetVal = OSIX_SUCCESS;

    if (Y1564_INITIALISED () != Y1564_TRUE)
    {
        Y1564_GLOBAL_TRC ("Y1564 Module - not initialized\r\n");
        return OSIX_FAILURE;
    }
    pY1564ReqParams = (tY1564ReqParams *)MemAllocMemBlk (Y1564_REQPARAMS_POOL ());

    if (pY1564ReqParams == NULL)
    {
        gY1564GlobalInfo.u4MemFailCount++;
        Y1564_GLOBAL_TRC ("Memory Allocation for ReqParams failed\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pY1564ReqParams, Y1564_ZERO, sizeof (tY1564ReqParams));

    pY1564ReqParams->u4ReqType = Y1564_UPDATE_CONTEXT_NAME;
    pY1564ReqParams->u4ContextId = u4ContextId;

    i4RetVal = Y1564ApiHandleExtRequest (pY1564ReqParams, pY1564RespParams);

    MemReleaseMemBlock (Y1564_REQPARAMS_POOL(), (UINT1 *) pY1564ReqParams);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}
#endif
