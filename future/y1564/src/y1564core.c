/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564core.c,v 1.21 2016/03/06 10:05:32 siva Exp $
 *
 * Description: This file contains utility function for Y1564 module
 *
 *******************************************************************/

#ifndef __Y1564UTIL_C__
#define __Y1564UTIL_C__

#include "y1564inc.h"
#include <time.h>

/*****************************************************************************
 *    Function Name       : Y1564CoreProcessConfTestResult                   *
 *                                                                           *
 *    Description         : This function will process the event received    *
 *                          ECFM/Y1731 and validate the SAC and add results  *
 *                          to RB Tree                                       *
 *                                                                           *
 *    Input(s)            : pSlaEntry    - Entry to SLA Node                 *
 *                          pResultEntry - Performance Monitoring            *
 *                                         Test Result entry                 *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
 PUBLIC INT4
 Y1564CoreProcessConfTestResult (tSlaInfo *pSlaEntry, tY1564QMsg * pMsg)
 {
     tY1564ContextInfo           *pContextInfo = NULL;
     tConfTestReportInfo         *pConfTestReportEntry = NULL;
     tConfTestReportInfo         *pConfTestYellowPacketReportEntry = NULL;
     tSacInfo                    *pSacEntry = NULL;
     tTrafficProfileInfo         *pTrafProfEntry = NULL;
#ifndef MEF_WANTED
     tServiceConfInfo            *pServiceConfInfo = NULL;
#endif
     UINT4                       u4MilliSec = Y1564_ZERO;
     UINT4                       u4InfoRate = Y1564_ZERO;
     UINT4                       u4Rate = Y1564_ZERO;
     UINT4                       u4Duration = Y1564_ZERO;
     UINT4                       u4EntryCount = Y1564_ZERO;
     UINT1                       u1Count = Y1564_ZERO;
     UINT1                       u1Flag = OSIX_FALSE;
    
     /* Added to update the conftestcount*/
     pContextInfo = Y1564CxtGetContextEntry (pSlaEntry->u4ContextId);

     if (pContextInfo == NULL)
     {
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                            Y1564_MGMT_TRC, " %s : does not exist\r\n",
                            __FUNCTION__);
         return OSIX_FAILURE;
     }

     pConfTestReportEntry = Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                             pSlaEntry->u4SlaId,
                                                             pSlaEntry->i4CurrentFrSize,
                                                             pSlaEntry->u1SlaCurrentTestMode,
                                                             pSlaEntry->u2SlaCurrentStep);
     if (pConfTestReportEntry == NULL)
     {
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                            " %s : Configuration test report entry"
                            "is present in Sla Id %d \r\n", __FUNCTION__,
                            pSlaEntry->u4SlaId);
         return OSIX_FAILURE;
     }

#ifndef MEF_WANTED
     pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId,
                                                   (UINT4) pSlaEntry->u2SlaServiceConfId);

     if (pServiceConfInfo == NULL)
     {

         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CRITICAL_TRC,
                            " %s : Service Configuration Entry is not present"
                            "for the given Sla Id %d \r\n", __FUNCTION__,
                            pSlaEntry->u4SlaId);
         return OSIX_FAILURE;
     }
#endif

     pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);
     if (pTrafProfEntry == NULL)
     {
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                            Y1564_CRITICAL_TRC," %s : Traffic Profile Entry "
                            "is not present for the given Sla Id %d \r\n",
                            __FUNCTION__, pSlaEntry->u4SlaId);
         return OSIX_FAILURE;
     }

     /* All the necessary information is retrieved
      * Check whether the test is pass/Fail */
     pSacEntry = Y1564UtilSacGetSacEntry (pSlaEntry->u4ContextId,
                                          pSlaEntry->u4SlaSacId);
     if (pSacEntry == NULL)
     {
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,Y1564_CTRL_TRC |
                            Y1564_ALL_FAILURE_TRC, " %s : SAC Entry is not present "
                            "for ID %d\r\n",__FUNCTION__, pSlaEntry->u4SlaSacId);
         return OSIX_FAILURE;
     }


     /* Default Values */
     pConfTestReportEntry->u4StatsFrLossRatio = Y1564_MAX_LOSS_RATIO;

     pConfTestReportEntry->u1StatsResult = Y1564_TEST_FAIL;

     pSlaEntry->u1SlaCurrentTestState =  pMsg->Y1564ReqParam.u1SlaCurrentTestState;

     pConfTestReportEntry->u1StatsResult = pSlaEntry->u1SlaCurrentTestState;

     /* Copy the port state counter and Duration from the SLA entry */
     pConfTestReportEntry->u4StatsPortStateCounter = pSlaEntry->u4SlaPortStateCounter;
     pConfTestReportEntry->u4StatsDuration = pSlaEntry->u4SlaConfTestDuration;

     /* Reseting the Port state counter in the SLA Entry */
     pSlaEntry->u4SlaPortStateCounter = Y1564_ZERO;

     /* Copy the Performance monitoring test results
      * into Conf Test Report Entry */
     pConfTestReportEntry->u4StatsFrTxDelayMin =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMin;
     pConfTestReportEntry->u4StatsFrTxDelayMean =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMean;
     pConfTestReportEntry->u4StatsFrTxDelayMax =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMax;
     pConfTestReportEntry->u4StatsFrDelayVarMin =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMin;
     pConfTestReportEntry->u4StatsFrDelayVarMean =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMean;
     pConfTestReportEntry->u4StatsFrDelayVarMax =
         pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMax;


     u4Rate = (UINT4) (((pTrafProfEntry->u4PortSpeed / Y1564_HUNDRED) * 
                pSacEntry->u4SacInfoRate) / Y1564_BPS_TO_KBPS_CONV_FACTOR);

     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,Y1564_CTRL_TRC ,
                        " %s : Green Frame : Tx Count : %d Rx Count : %d "
                        "Frame size : %d Pps : %d for Sla Id : %d\r\n",
                        __FUNCTION__, pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts,
                        pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts,
                        pMsg->Y1564ReqParam.ResultInfo.u2FrameSize,
                        pMsg->Y1564ReqParam.ResultInfo.u4CIRPps,
                        pSlaEntry->u4SlaId);

     if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts != Y1564_ZERO)
     {
         /*Calculate Frame Loss Ratio */
         if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts >=  pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts)
         {
             /* Frame loss count is TX-RX
              * Frmae loss ratio is (frame loss count * 100)/ 
              * TX count */
             pConfTestReportEntry->u4StatsFrLossCnt = 
                 (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts - pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts);

             pConfTestReportEntry->u4StatsFrLossRatio =
                 ((pConfTestReportEntry->u4StatsFrLossCnt  /
                   pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts) * 100);

         }
         else
         {
             /* No Frame loss has occured*/
             pConfTestReportEntry->u4StatsFrLossCnt = Y1564_ZERO;
             pConfTestReportEntry->u4StatsFrLossRatio =  Y1564_ZERO;

         }

         /* RX count will have the accumulated value for the whole
          * duration. So it is divided by the total duration to get 
          * the throughput.*/
         if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts == 
             pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts)
         {
              /* Tx Count can never be zero
               *  Throughput can be calculated using TxCount and RxCount */
             pConfTestReportEntry->u4StatsIrMean = (UINT4)
                 ((pMsg->Y1564ReqParam.ResultInfo.u4CIRPps * pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * 
                   Y1564_BITS_PER_BYTE) / Y1564_BPS_TO_KBPS_CONV_FACTOR);
         }
         else
         {
              /* if RX count is less than the TX count then throughput
               * calculation can be done based on RX count
               * RX count will have the accumulated value for the whole
               * duration. So it is divided by the total duration to get
               * the throughput.*/
             pConfTestReportEntry->u4StatsIrMean = (UINT4) 
                 (((pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts / pSlaEntry->u4SlaConfTestDuration) 
                   * pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * Y1564_BITS_PER_BYTE) / 
                  Y1564_BPS_TO_KBPS_CONV_FACTOR);
         }
     }

     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                        " %s : Sac Table : Frame Delay : %d "
                        "Frame Delay Var : %d Frame Loss (%) : %d"
                        " Report Entry : Frame Delay : %d "
                        "Frame Delay Var : %d Frame Loss (%) : %d "
                        "Sla Id : %d Test Mode : %d \r\n",__FUNCTION__,
                        pSacEntry->u4SacFrTxDelay,
                        pSacEntry->u4SacFrDelayVar,
                        pSacEntry->u4SacFrLossRatio, 
                        pConfTestReportEntry->u4StatsFrTxDelayMean,
                        pConfTestReportEntry->u4StatsFrDelayVarMean,
                        pConfTestReportEntry->u4StatsFrLossRatio,
                        pSlaEntry->u4SlaId,
                        pConfTestReportEntry->u4CurrentTestMode);

     if ((pConfTestReportEntry->u4CurrentTestMode == Y1564_SIMPLE_CIR_TEST) ||
         (pConfTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST))
     {
         /* Check if all the calculated parameters matches the service
          * acceptance criteria and make the result as PASS */

         if (pConfTestReportEntry->u4CurrentTestMode == Y1564_STEPLOAD_CIR_TEST)
         {
             /*Calculating Rate from port speed based on the percentage value present
              * in InfoRate */
             u4InfoRate = ((u4Rate / Y1564_MAX_RATE_STEP) * pSlaEntry->u2SlaCurrentRateStep);
         }
         else
         {
             u4InfoRate = u4Rate;
         }

         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC ,
                            " %s : Sac Table : Rate in percentage : %d "
                            "Rate in Kbps  : %d Report Entry : Rate in Kbps "
                            ": %d for Sla Id %d Test Mode %d \r\n",
                            __FUNCTION__,pSacEntry->u4SacInfoRate, u4InfoRate, 
                            pConfTestReportEntry->u4StatsIrMean,
                            pSlaEntry->u4SlaId,
                            pConfTestReportEntry->u4CurrentTestMode);

         if (pSlaEntry->u1SlaCurrentTestState != Y1564_SLA_TEST_ABORTED)
         {
             if ((u4InfoRate <= pConfTestReportEntry->u4StatsIrMean) &&
                 (pSacEntry->u4SacFrLossRatio >= pConfTestReportEntry->u4StatsFrLossRatio) &&
                 (pSacEntry->u4SacFrTxDelay >= pConfTestReportEntry->u4StatsFrTxDelayMean) &&
                 (pSacEntry->u4SacFrDelayVar >= pConfTestReportEntry->u4StatsFrDelayVarMean))
             {
                 pConfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
             }
         }
     }

     if(pSlaEntry->u1SlaCurrentTestState != Y1564_SLA_TEST_ABORTED)
     {
         if (pConfTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE)
         {
             /* Check if all the calculated parameters matches the service
              * acceptance criteria and make the result as PASS */
             if ((pSacEntry->u4SacFrLossRatio >= pConfTestReportEntry->u4StatsFrLossRatio) &&
                 (pSacEntry->u4SacFrTxDelay >= pConfTestReportEntry->u4StatsFrTxDelayMean) &&
                 (pSacEntry->u4SacFrDelayVar >= pConfTestReportEntry->u4StatsFrDelayVarMean))
             {
                 pConfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
             }
         }

         if (pConfTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_BLIND)
         {
#ifndef MEF_WANTED
             if ((((pServiceConfInfo->u4ServiceConfCIR / Y1564_MAX_LOSS_RATIO) * 
                   (Y1564_MAX_LOSS_RATIO - pSacEntry->u4SacFrLossRatio))
                  <= (pConfTestReportEntry->u4StatsIrMean)) &&
                 (pConfTestReportEntry->u4StatsIrMean <= (pServiceConfInfo->u4ServiceConfCIR +
                                                          pServiceConfInfo->u4ServiceConfEIR)))
#else
                 if ((((pSlaEntry->u4CIR / Y1564_MAX_LOSS_RATIO) * (Y1564_MAX_LOSS_RATIO - pSacEntry->u4SacFrLossRatio))
                      <= (pConfTestReportEntry->u4StatsIrMean)) &&
                     ((pConfTestReportEntry->u4StatsIrMean) <= (pSlaEntry->u4CIR + pSlaEntry->u4EIR)))
#endif
                 {
                     pConfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
                 }
         }

         if (pConfTestReportEntry->u4CurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE)
         {
             /* Check if all the calculated parameters matches the service
              * acceptance criteria and make the result as PASS */
             if ((pSacEntry->u4SacFrLossRatio >= pConfTestReportEntry->u4StatsFrLossRatio) &&
                 (pSacEntry->u4SacFrTxDelay >= pConfTestReportEntry->u4StatsFrTxDelayMean) &&
                 (pSacEntry->u4SacFrDelayVar >= pConfTestReportEntry->u4StatsFrDelayVarMean))
             {
#ifndef MEF_WANTED
                 if ((pConfTestReportEntry->u4StatsIrMean <= (pServiceConfInfo->u4ServiceConfCIR +
                                                              pServiceConfInfo->u4ServiceConfEIR +
                                                              Y1564_ZERO)))
#else
                     if ((pConfTestReportEntry->u4StatsIrMean <=
                          (pSlaEntry->u4CIR + pSlaEntry->u4EIR + Y1564_ZERO)))
#endif
                     {
                         pConfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
                     }
             }
         }

         if (pConfTestReportEntry->u4CurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_BLIND)
         {
#ifndef MEF_WANTED
             if ((((pServiceConfInfo->u4ServiceConfCIR / Y1564_MAX_LOSS_RATIO) *
                   (Y1564_MAX_LOSS_RATIO - pSacEntry->u4SacFrLossRatio))
                  <= (pConfTestReportEntry->u4StatsIrMean)) &&
                 ((pConfTestReportEntry->u4StatsIrMean) <=
                  (pServiceConfInfo->u4ServiceConfCIR + 
                   ((pServiceConfInfo->u4ServiceConfEIR / Y1564_HUNDRED) * Y1564_HUNDRED_TWENTY_FIVE) +
                   Y1564_ZERO)))
#else
                 if ((((pSlaEntry->u4CIR / Y1564_MAX_LOSS_RATIO) * (Y1564_MAX_LOSS_RATIO - pSacEntry->u4SacFrLossRatio))
                      <= (pConfTestReportEntry->u4StatsIrMean)) &&
                     ((pConfTestReportEntry->u4StatsIrMean) <=
                      (pSlaEntry->u4CIR + ((pSlaEntry->u4EIR / Y1564_HUNDRED) * Y1564_HUNDRED_TWENTY_FIVE) + 
                       Y1564_ZERO)))
#endif
                 {
                     pConfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
                 }
         }
     }

     pConfTestReportEntry->TestEndTimeStamp =  pSlaEntry->TestEndTimeStamp;

     if ((pConfTestReportEntry->u4CurrentTestMode == Y1564_TRAF_POLICING_TEST_COLOR_AWARE) ||
         (pConfTestReportEntry->u4CurrentTestMode == Y1564_EIR_TEST_COLOR_AWARE))
     {
         /* Count is fetched from RBTreeCount datastructure and it is
          * compared with maximum Number of Configuration Test Report
          * entries that are allowed to create */
         RBTreeCount (Y1564_CONF_REPORT_TABLE(), &u4EntryCount);

         if (u4EntryCount == MAX_Y1564_CONF_TEST_REPORT_ENTRIES)
         {
             Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                                " %s : Maximum number of allowed Configuration Test "
                                "Report Entries reached \r\n", __FUNCTION__);
             CLI_SET_ERR (CLI_Y1564_ERR_MAX_CONF_TEST_REPORT_REACHED);
             return OSIX_FAILURE;
         }

         pConfTestYellowPacketReportEntry = Y1564UtilCreateConfTestReportNode ();

         if (pConfTestYellowPacketReportEntry == NULL)
         {
             Y1564_GLOBAL_TRC ("Y1564UtilProcessConfTestResult : Memory "
                               "Allocation failed for ConfTestReport Entry \r\n");
             return OSIX_FAILURE;
         }

         MEMSET (pConfTestYellowPacketReportEntry, Y1564_ZERO, sizeof (tConfTestReportInfo));

         /* Copy the Performance monitoring test results
          * into Conf Test Report Entry for Yellow Packets */
         pConfTestYellowPacketReportEntry->u4ContextId = pSlaEntry->u4ContextId;
         pConfTestYellowPacketReportEntry->u4SlaId = pSlaEntry->u4SlaId;
         pConfTestYellowPacketReportEntry->u4CurrentTestMode =  pSlaEntry->u1SlaCurrentTestMode;
         pConfTestYellowPacketReportEntry->u4StatsStepId = (UINT4) Y1564_TWO;
         pConfTestYellowPacketReportEntry->i4FrameSize = pSlaEntry->i4CurrentFrSize;
         pConfTestYellowPacketReportEntry->u1StatsResult = Y1564_FIVE;

         pConfTestYellowPacketReportEntry->u4StatsFrTxDelayMin =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMin;
         pConfTestYellowPacketReportEntry->u4StatsFrTxDelayMean =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMean;
         pConfTestYellowPacketReportEntry->u4StatsFrTxDelayMax =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMax;
         pConfTestYellowPacketReportEntry->u4StatsFrDelayVarMin =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMin;
         pConfTestYellowPacketReportEntry->u4StatsFrDelayVarMean =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMean;
         pConfTestYellowPacketReportEntry->u4StatsFrDelayVarMax =
             pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMax;

         if (pMsg->Y1564ReqParam.ResultInfo.u4YellowFrTxPkts != Y1564_ZERO)
         {
             /*Calculate Frame Loss Ratio */
             if (pMsg->Y1564ReqParam.ResultInfo.u4YellowFrTxPkts > pMsg->Y1564ReqParam.ResultInfo.u4YellowFrRxPkts)
             {
                 /* Frame loss count is TX-RX
                  * Frmae loss ratio is (frame loss count * 100)/
                  * TX count */
                 pConfTestYellowPacketReportEntry->u4StatsFrLossCnt =
                     (pMsg->Y1564ReqParam.ResultInfo.u4YellowFrTxPkts - pMsg->Y1564ReqParam.ResultInfo.u4YellowFrRxPkts);

                 pConfTestYellowPacketReportEntry->u4StatsFrLossRatio =
                     ((pConfTestYellowPacketReportEntry->u4StatsFrLossCnt  /
                       pMsg->Y1564ReqParam.ResultInfo.u4YellowFrTxPkts) * 100);

                 /* if RX count is less than the TX count then throughput
                  * calculation can be done based on RX count
                  * RX count will have the accumulated value for the whole
                  * duration. So it is divided by the total duration to get
                  * calculate the throughput.*/

                 pConfTestYellowPacketReportEntry->u4StatsIrMean = (UINT4)
                     (((pMsg->Y1564ReqParam.ResultInfo.u4YellowFrRxPkts / pSlaEntry->u4SlaConfTestDuration)
                       * pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * Y1564_BITS_PER_BYTE) /
                      Y1564_BPS_TO_KBPS_CONV_FACTOR);
             }
             else if (pMsg->Y1564ReqParam.ResultInfo.u4YellowFrTxPkts == pMsg->Y1564ReqParam.ResultInfo.u4YellowFrRxPkts)
             {
                 /* No Frame loss has occured*/
                 pConfTestYellowPacketReportEntry->u4StatsFrLossCnt = Y1564_ZERO;
                 pConfTestYellowPacketReportEntry->u4StatsFrLossRatio =  Y1564_ZERO;

                 /* Tx Count can never be zero
                  * Throughput can be calculated using TxCount and RxCount */
                 pConfTestYellowPacketReportEntry->u4StatsIrMean = (UINT4) ((pMsg->Y1564ReqParam.ResultInfo.u4YellowFrPps *
                                                                     pMsg->Y1564ReqParam.ResultInfo.u2FrameSize *
                                                                     Y1564_BITS_PER_BYTE) /
                                                                    Y1564_BPS_TO_KBPS_CONV_FACTOR);
             }
         }

         if (Y1564UtilAddNodeToConfTestReportRBTree (pConfTestYellowPacketReportEntry)
             != OSIX_SUCCESS)
         {
             /* Release memory allocated for Conf Test Report Entry */
             MemReleaseMemBlock (Y1564_CONF_REPORT_POOL (), (UINT1 *) pConfTestYellowPacketReportEntry);
             pConfTestYellowPacketReportEntry = NULL;

             Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                Y1564_MGMT_TRC | Y1564_CRITICAL_TRC,
                                "%s : Conf Test Yellow Packet Entry Add Failed for Sla Id %d\r\n",
                                __FUNCTION__, pSlaEntry->u4SlaId);

             return OSIX_FAILURE;
         }
     }

     /* Reset Current Step to Zero before initiating next test */
     if (pSlaEntry->u1SlaCurrentTestMode != Y1564_STEP_LOAD_CIR_TEST)
     {
         pSlaEntry->u2SlaCurrentStep = Y1564_ZERO;
     }

     if ((pSlaEntry->u1SlaConfTestStatus == Y1564_SLA_START) &&
         (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_COMPLETED))
     {

         pContextInfo->u1ConfTestCount --;  /*Decrement the 
                                              currently running count */
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                            Y1564_CTRL_TRC | Y1564_CRITICAL_TRC,
                            " %s : Sla Id : %d Packet Size : %d u1EmixSelected : %d "
                            "TempEmixPktSize : %s Test Count : %d \r\n", __FUNCTION__,
                            pSlaEntry->u4SlaId, pSlaEntry->i4CurrentFrSize,
                            pTrafProfEntry->u1EmixSelected,
                            pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                            pContextInfo->u1ConfTestCount);

         if ((pSlaEntry->u1LastSlaConfTestSelector & Y1564_SIMPLE_CIR_TEST) ||
             (pSlaEntry->u1LastSlaConfTestSelector & Y1564_STEPLOAD_CIR_TEST)||
             (pSlaEntry->u1LastSlaConfTestSelector & Y1564_EIR_TEST) ||
             (pSlaEntry->u1LastSlaConfTestSelector & Y1564_TRAF_POLICING_TEST))
         {
             Y1564_CONVERT_SEC_TO_MILLISECONDS (pSlaEntry->u4SlaConfTestDuration,
                                                u4MilliSec);

             if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                 (pSlaEntry->u1SlaCurrentTestMode == Y1564_STEPLOAD_CIR_TEST) &&
                 ((STRCMP (pTrafProfEntry->au1TrafProfOptEmixPktSize,
                           pTrafProfEntry->au1TempTrafProfOptEmixPktSize)) != Y1564_ZERO))
             {
                 /* Increment the current step and fetch the timer value 
                  *  for next step in step load cir */
                 if (pSlaEntry->u2SlaRateStep > pSlaEntry->u2SlaCurrentStep)
                 {
                     u4MilliSec = pSlaEntry->u4SlaStepLoadDuration;
                     u1Count = Y1564_ONE;
                     pSlaEntry->u2SlaCurrentStep = (UINT2) (pSlaEntry->u2SlaCurrentStep + (UINT2) Y1564_ONE);
                 }
                 else
                 {
                     u1Flag = OSIX_TRUE;
                 }
                 /* test result has to be validated only for step load
                  *  before starting to next step in step load cir */
                 if ((u1Flag == OSIX_TRUE) ||
                     ((pConfTestReportEntry->u4CurrentTestMode == Y1564_STEP_LOAD_CIR_TEST) &&
                     (pConfTestReportEntry->u1StatsResult != Y1564_TEST_PASS)))
                 {
                     Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

                     u4MilliSec = (UINT4) ((u4MilliSec /
                                            STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))
                                           * u1Count);
                     pSlaEntry->u2SlaCurrentStep = Y1564_ZERO;
                     pSlaEntry->u4SlaStepLoadDuration = u4MilliSec;
                 }
             }
             else if (pTrafProfEntry->u1EmixSelected == Y1564_TRUE)
             {
                 Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

                 u4MilliSec = (UINT4) ((u4MilliSec /
                                        STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))
                                       * u1Count);
             }
             else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
             {
                 if (pSlaEntry->u1LastSlaConfTestSelector & Y1564_STEPLOAD_CIR_TEST)
                 {
                     pSlaEntry->u2SlaCurrentStep = (UINT2) (pSlaEntry->u2SlaCurrentStep + (UINT2) Y1564_ONE);
                 }
             }

             if ((pTrafProfEntry->u1EmixSelected == Y1564_TRUE) &&
                 (u1Count != Y1564_ZERO))
             {  
                 if (Y1564UtilValidateSlaTest (pSlaEntry, pTrafProfEntry, &u4Duration)
                     != OSIX_SUCCESS)
                 {
                     return  OSIX_FAILURE;
                 }

                 if (u4Duration != Y1564_ZERO)
                 {
                     u4MilliSec = u4Duration;
                 }

                 if (Y1564PortSlaStartConfTest (pSlaEntry, pTrafProfEntry, u4MilliSec) 
                     != OSIX_FAILURE)
                 { 
                     /* Start the SLA Configuration Test Timer */
                     if (Y1564TmrStartTimer (pSlaEntry, Y1564_SLA_CONF_TEST_TMR,
                                             u4MilliSec) 
                         != OSIX_SUCCESS)
                     {
                         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                                            " %s : Failed to start Configuration test timer "
                                            "for Sla Id %d \r\n" ,__FUNCTION__,
                                            pSlaEntry->u4SlaId);
                         return OSIX_FAILURE;
                     }

                     pContextInfo->u1ConfTestCount ++;  /* Increment the Configuration 
                                                           test running count */
                     pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                                        " %s : Sla : %d, Emix : Current test state : "
                                        "In-Progress and configuration test count : %d "
                                        "\r\n", __FUNCTION__,
                                        pSlaEntry->u4SlaId, pContextInfo->u1ConfTestCount);
                     return OSIX_SUCCESS;
                 }
                 else
                 {
                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                        Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                        "%s : Configuration Test start failed for SLA "
                                        "%d : Frame Size %d \r\n", __FUNCTION__,
                                        pSlaEntry->u4SlaId, 
                                        pTrafProfEntry->u2CurrentEmixPktSize);
                     return OSIX_FAILURE;
                 }
             }
             else if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
             {

                 if (Y1564UtilValidateSlaTest (pSlaEntry, pTrafProfEntry, &u4Duration)
                     != OSIX_SUCCESS)
                 {
                     return OSIX_FAILURE;
                 }

                 if (u4Duration != Y1564_ZERO)
                 {
                     u4MilliSec = u4Duration;
                 }

                 /* test result has to be validated only for step load
                  * before starting to next step in step load cir */
                 if ((pConfTestReportEntry->u4CurrentTestMode != Y1564_STEP_LOAD_CIR_TEST) ||
                     ((pConfTestReportEntry->u4CurrentTestMode == Y1564_STEP_LOAD_CIR_TEST) &&
                      (pConfTestReportEntry->u1StatsResult == Y1564_TEST_PASS) &&
                      (pSlaEntry->u1SlaCurrentTestMode == Y1564_STEP_LOAD_CIR_TEST)) ||
                     (pSlaEntry->u1SlaCurrentTestMode != Y1564_STEP_LOAD_CIR_TEST))
                 {

                     if (Y1564PortSlaStartConfTest (pSlaEntry, pTrafProfEntry, u4MilliSec) 
                         != OSIX_FAILURE)
                     { 
                         /* Start the SLA Configuration Test Timer */
                         if (Y1564TmrStartTimer (pSlaEntry, Y1564_SLA_CONF_TEST_TMR,
                                                 u4MilliSec) 
                             != OSIX_SUCCESS)
                         {
                             Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                                                " %s : Failed to start Configuration test timer "
                                                "for Sla Id %d \r\n" ,__FUNCTION__,
                                                pSlaEntry->u4SlaId);
                             return OSIX_FAILURE;
                         }

                         pContextInfo->u1ConfTestCount ++;  /* Increment the Configuration 
                                                               test running count */
                         pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

                         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                                            " %s : Sla : %d , Current test state : "
                                            "In-Progress and configuration test count : %d "
                                            "\r\n", __FUNCTION__,
                                            pSlaEntry->u4SlaId, pContextInfo->u1ConfTestCount);
                         return OSIX_SUCCESS;
                     }
                 }
                 else
                 {
                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                        Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                        "%s : Configuration Test start failed for SLA "
                                        "%d : Frame Size : %d and currently "
                                        "running test count : %d \r\n", __FUNCTION__,
                                        pSlaEntry->u4SlaId, pTrafProfEntry->u2TrafProfPktSize,
                                        pContextInfo->u1ConfTestCount);
                     return OSIX_FAILURE;
                 }
             }
         }
     }

     if (pConfTestReportEntry->u1StatsResult == Y1564_TEST_FAIL)
     {
         if (pContextInfo->u1ConfTestCount != Y1564_ZERO)
         {
             pContextInfo->u1ConfTestCount --;  /*Decrement the 
                                                  currently running count */
         }
         Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                            Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                            "%s : Configuration Test result failed for SLA "
                            "%d for test mode %d and currently running "
                            "test count %d \r\n", __FUNCTION__,
                            pSlaEntry->u4SlaId, 
                            pConfTestReportEntry->u4CurrentTestMode,
                            pContextInfo->u1ConfTestCount);
     }
     /* Test is completed and mark the test mode is
      * no test is in progress and mark the test status as STOP */ 
     pSlaEntry->u2SlaCurrentStep = Y1564_ZERO;
     pSlaEntry->u2SlaCurrentRateStep = Y1564_ZERO;
     pConfTestReportEntry->TestEndTimeStamp =  pSlaEntry->TestEndTimeStamp;
     pSlaEntry->u1SlaConfTestStatus = Y1564_SLA_STOP;
     pSlaEntry->u1SlaCurrentTestMode = Y1564_NO_TEST_IN_PROGRESS;
     return OSIX_SUCCESS;
 }
/*****************************************************************************
 *    Function Name       : Y1564CoreProcessPerfTestResult                   *
 *                                                                           *
 *    Description         : This function will process the event received    *
 *                          ECFM/Y1731 and validate the SAC and add results  *
 *                          to RB Tree                                       *
 *                                                                           *
 *    Input(s)            : pSlaEntry    - Entry to SLA Node                 *
 *                          pResultEntry - Performance Monitoring            *
 *                                         Test Result entry                 *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
 PUBLIC INT4
 Y1564CoreProcessPerfTestResult (tSlaInfo *pSlaEntry,  tY1564QMsg * pMsg)
 {
     tUtlSysPreciseTime          SysPreciseTime;
     tPerfTestReportInfo         *pPerfTestReportEntry = NULL;
     tSacInfo                    *pSacEntry = NULL;
     tTrafficProfileInfo         *pTrafProfEntry = NULL;
     tPerfTestInfo               *pPerfTestEntry = NULL;
     tPerfTestInfo               *pNextPerfTestEntry = NULL;
     tY1564ContextInfo           *pContextInfo = NULL;
     UINT4                       u4PerfTestEndTime = Y1564_ZERO;
     UINT4                       u4MilliSec = Y1564_ZERO;
     UINT4                       u4Duration = Y1564_ZERO;
     UINT4                       u4InfoRate = Y1564_ZERO;
     UINT2                       u2Index = Y1564_ZERO;
     BOOL1                       bResult = Y1564_ZERO;
   /* Get the entry from performance Table */

     /* Completed :Either PerfId has to be passed as one of the input 
      * or for loop has to be implemented to get the
      * perf Id */


     /* Added to update the Perftestcount*/
    pContextInfo = Y1564CxtGetContextEntry (pSlaEntry->u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        return OSIX_FAILURE;
    }

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    UtlGetPreciseSysTime (&SysPreciseTime);

    u4PerfTestEndTime = SysPreciseTime.u4Sec;
    pPerfTestEntry = Y1564UtilGetFirstNodeFromPerfTestTable();

    if (pPerfTestEntry == NULL)
    {
         Y1564_GLOBAL_TRC ("Y1564CoreProcessPerfTestResult : Perf Test"
                           " Entry is NULL \r\n");
        return OSIX_FAILURE;
    }
    while (pPerfTestEntry != NULL)
    {
        for (; u2Index <= pPerfTestEntry->u2SlaListCount ; u2Index++)
        {
            /* Scanning the SLA list bitmap to identify the SLA id's mapped to
             * Perf Table*/
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)), u2Index,
                                     sizeof (tY1564SlaList), bResult);

            /* If the bit is not set or bit is set but Sla Id present in "
             * SlaEntry is not matching with Sla Id present in perf entry */
            if ((bResult == OSIX_FALSE)|| (u2Index != pSlaEntry->u4SlaId))
            { 
                continue;
            }
            else
            {
                pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry 
                                          (pPerfTestEntry->u4ContextId, 
                                           u2Index, 
                                           pPerfTestEntry->u4PerfTestId,
                                           pSlaEntry->i4CurrentFrSize);
             
                if (pPerfTestReportEntry == NULL)
                {
                    Y1564_GLOBAL_TRC ("Y1564CoreProcessPerfTestResult : Perf Test"
                                      " Report Entry is NULL \r\n");
                    return OSIX_FAILURE;
                }
                pPerfTestReportEntry->u4StatsFrLossRatio = Y1564_MAX_LOSS_RATIO;
                pPerfTestReportEntry->u1StatsResult = Y1564_TEST_FAIL;
                pPerfTestReportEntry->f4StatsAvailability = 
                    pMsg->Y1564ReqParam.ResultInfo.f4StatsAvailability;
                pPerfTestReportEntry->u4StatsUnAvailCount =
                    pMsg->Y1564ReqParam.ResultInfo.u4UnAvailCnt;

                pPerfTestReportEntry->u4SlaId = pSlaEntry->u4SlaId;
           
                pPerfTestReportEntry->u4StatsPerfId = pPerfTestEntry->u4PerfTestId;
                /* Test is either completed ot aborted and update the SlaCurrentTest
                 * state */
                pSlaEntry->u1SlaCurrentTestState = 
                    pMsg->Y1564ReqParam.u1SlaCurrentTestState;

                pPerfTestReportEntry->u1StatsResult = 
                    pMsg->Y1564ReqParam.u1SlaCurrentTestState;

                /* Copy the port state counter and Duration from the SLA entry */
                pPerfTestReportEntry->u4StatsPortStateCounter = 
                    pSlaEntry->u4SlaPortStateCounter;

                pPerfTestReportEntry->u4StatsDuration = (UINT4) 
                    pPerfTestEntry->u1PerfTestDuration;

                /* Reseting the Port state counter in the SLA Entry */
                pSlaEntry->u4SlaPortStateCounter = Y1564_ZERO;

                /* Copy the Performance monitoring test results
                 * into Perf Test Report Entry */
                pPerfTestReportEntry->u4StatsFrTxDelayMin =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMin;
                pPerfTestReportEntry->u4StatsFrTxDelayMean =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMean;
                pPerfTestReportEntry->u4StatsFrTxDelayMax =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrTxDelayMax;

                pPerfTestReportEntry->u4StatsFrDelayVarMin =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMin;
                pPerfTestReportEntry->u4StatsFrDelayVarMean =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMean;
                pPerfTestReportEntry->u4StatsFrDelayVarMax =
                    pMsg->Y1564ReqParam.ResultInfo.u4StatsFrDelayVarMax;

                if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts != Y1564_ZERO)
                {
                    /*Calculate Frame Loss Ratio */
                    if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts >= 
                        pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts)
                    {
                        /* Frame loss count is TX-RX
                         * Frmae loss ratio is (frame loss count * 100)/ 
                         * TX count */
                        pPerfTestReportEntry->u4StatsFrLossCnt = 
                            (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts - 
                             pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts);

                        pPerfTestReportEntry->u4StatsFrLossRatio =
                            ((pPerfTestReportEntry->u4StatsFrLossCnt * Y1564_HUNDRED) /
                             pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts);
                    }
                    else
                    {
                        /* No Frame loss has occured*/
                        pPerfTestReportEntry->u4StatsFrLossCnt = Y1564_ZERO;
                        pPerfTestReportEntry->u4StatsFrLossRatio =  Y1564_ZERO;


                    }
                    
                    if (pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts == 
                         pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts)
                     {
                         /* Tx Count can never be zero
                          * Throughput can be calculated using TxCount and RxCount */
                         pPerfTestReportEntry->u4StatsIrMean = (UINT4) ((pMsg->Y1564ReqParam.ResultInfo.u4CIRPps * 
                                                                 pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * 
                                                                 Y1564_BITS_PER_BYTE) / 
                                                                Y1564_BPS_TO_KBPS_CONV_FACTOR);
                     }
                     else
                     {
                         if (pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts < 
                             pMsg->Y1564ReqParam.ResultInfo.u4StatsTxPkts)
                         {
                             if ((pPerfTestEntry->u1PerfTestDuration) ==  Y1564_TEST_DURATION_15MIN)
                             {
                                 pPerfTestReportEntry->u4StatsIrMean = (UINT4) 
                                     (((pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts / 
                                        (Y1564_TEST_DURATION_15MIN * Y1564_SEC_IN_MIN)) 
                                       *pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * Y1564_BITS_PER_BYTE) / 
                                      Y1564_BPS_TO_KBPS_CONV_FACTOR);
                             }
                             if ((pPerfTestEntry->u1PerfTestDuration) == Y1564_TEST_DURATION_2HOUR) 
                             {
                                 pPerfTestReportEntry->u4StatsIrMean = (UINT4)
                                     (((pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts / 
                                        (Y1564_TEST_DURATION_2HOUR * Y1564_SEC_IN_HOURS)) 
                                       *pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * Y1564_BITS_PER_BYTE) / 
                                      Y1564_BPS_TO_KBPS_CONV_FACTOR);
                             }
                             if((pPerfTestEntry->u1PerfTestDuration) == Y1564_TEST_DURATION_24HOUR)
                             {
                                 pPerfTestReportEntry->u4StatsIrMean = (UINT4)
                                     (((pMsg->Y1564ReqParam.ResultInfo.u4StatsRxPkts / 
                                        (Y1564_TEST_DURATION_24HOUR * Y1564_SEC_IN_HOURS)) 
                                       *pMsg->Y1564ReqParam.ResultInfo.u2FrameSize * Y1564_BITS_PER_BYTE)/
                                      Y1564_BPS_TO_KBPS_CONV_FACTOR);
                             }

                         }
                     }
                 }

                 /* All the necessary information is retrieved
                  * Check whether the test is pass/Fail */
                 pSacEntry = Y1564UtilSacGetSacEntry (pSlaEntry->u4ContextId,
                                                      pSlaEntry->u4SlaSacId);
                 if (pSacEntry == NULL)
                 {
                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,Y1564_CTRL_TRC |
                                        Y1564_ALL_FAILURE_TRC, " %s : SAC Entry "
                                        "is not present for ID %d\r\n", 
                                        __FUNCTION__,pSlaEntry->u4SlaSacId);
                     return OSIX_FAILURE;
                 }

                 pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                             pSlaEntry->u4SlaTrafProfId);
                 if (pTrafProfEntry == NULL)
                 {
                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                                        " %s : Traffic Profile Entry is not present "
                                        "for the given Sla Id %d \r\n", __FUNCTION__,
                                        pSlaEntry->u4SlaId);
                     return OSIX_FAILURE;
                 }

                 if(pSlaEntry->u1SlaCurrentTestState != Y1564_SLA_TEST_ABORTED)
                 {
                     u4InfoRate = (UINT4) (((pTrafProfEntry->u4PortSpeed / Y1564_HUNDRED) * 
                                    pSacEntry->u4SacInfoRate) / Y1564_BPS_TO_KBPS_CONV_FACTOR);

                     /* Check if all the calculated parameters matches the service
                      * acceptance criteria and make the result as PASS */
                     if ((u4InfoRate <= pPerfTestReportEntry->u4StatsIrMean) &&
                         (pSacEntry->u4SacFrLossRatio >= pPerfTestReportEntry->u4StatsFrLossRatio) &&
                         (pSacEntry->u4SacFrTxDelay >= pPerfTestReportEntry->u4StatsFrTxDelayMean) &&
                         (pSacEntry->u4SacFrDelayVar >= pPerfTestReportEntry->u4StatsFrDelayVarMean)&&
                         (pSacEntry->f4SacAvailability <= pPerfTestReportEntry->f4StatsAvailability))
                     {
                         pPerfTestReportEntry->u1StatsResult = Y1564_TEST_PASS;
                     }
                 }

                 Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                                    " %s : Sac Table : Rate in Kbps : %d "
                                    "Frame Delay : %d Frame Delay Var : %d "
                                    "Frame Loss (%) : %d  Availability in Percentage : %d "
                                    "Report Entry : Rate in Kbps : %d Frame Delay : %d "
                                    " \r\n",__FUNCTION__,u4InfoRate,
                                    pSacEntry->u4SacFrTxDelay,pSacEntry->u4SacFrDelayVar,
                                    pSacEntry->u4SacFrLossRatio, (DBL8) pSacEntry->f4SacAvailability,
                                    pPerfTestReportEntry->u4StatsIrMean,
                                    pPerfTestReportEntry->u4StatsFrTxDelayMean);

		
                 Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                                    "Frame Delay Var : %d Frame Loss (%) : %d "
                                    "Availability in Percentage : %d for "
                                    "Sla Id : %d in Perf test Id : %d Test Mode : "
                                    "%d \r\n",pPerfTestReportEntry->u4StatsFrDelayVarMean,
                                    pPerfTestReportEntry->u4StatsFrLossRatio,
                                    (DBL8) pPerfTestReportEntry->f4StatsAvailability,
                                    pSlaEntry->u4SlaId,pPerfTestReportEntry->u4StatsPerfId,
                                    pSlaEntry->u1SlaCurrentTestState);


                 /* Update TimeStamp Details */
                 pPerfTestReportEntry->PerfTestEndTimeStamp =  u4PerfTestEndTime;

                 Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                    Y1564_MGMT_TRC | Y1564_CTRL_TRC,
                                    " %s : PerfTest Id : %d Sla Id : %d "
                                    "Packet Size : %d u1EmixSelected : %d "
                                    "TempEmixPktSize : %s Perf Test Count : %d \r\n",
                                    __FUNCTION__, pPerfTestEntry->u4PerfTestId,
                                    pSlaEntry->u4SlaId, pPerfTestReportEntry->i4FrameSize,
                                    pTrafProfEntry->u1EmixSelected,
                                    pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                                    pContextInfo->u1PerfTestCount);

                 if ((pPerfTestEntry->u1PerfTestStatus == Y1564_SLA_START) &&
                     (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_COMPLETED) &&
                     (pTrafProfEntry->u1EmixSelected == Y1564_TRUE))
                 {
                        /* Decrement the currently running performance
                          * test count value */
                         pContextInfo->u1PerfTestCount --;

                         if (Y1564CoreValidateSlaPerfTest (pSlaEntry,
                                                           pTrafProfEntry,
                                                           pPerfTestEntry,
                                                           &u4Duration) != OSIX_SUCCESS)
                         {
                             return OSIX_FAILURE;
                         }

                         /* Test Should not be initiated for frame size 
                          * Zero */
                         if (pTrafProfEntry->u2CurrentEmixPktSize == Y1564_ZERO)
                         {
                             return OSIX_SUCCESS;
                         }

                         u4MilliSec = u4Duration;

                         UtlGetPreciseSysTime (&SysPreciseTime);
                         pSlaEntry->TestStartTimeStamp = SysPreciseTime.u4Sec;

                         if (Y1564PortSlaStartPerfTest (pSlaEntry, pTrafProfEntry, u4MilliSec, 
                                                        pPerfTestEntry->u4PerfTestId) != 
                             OSIX_FAILURE)
                         { 
                             /* Start the SLA Configuration Test Timer */
                             if (Y1564TmrStartTimer (pSlaEntry, Y1564_SLA_PERF_TEST_TMR,
                                                     u4MilliSec) 
                                 != OSIX_SUCCESS)
                             {
                                 Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                                                    " %s : Failed to start Performance test timer "
                                                    "for Sla Id %d Duration : %d \r\n" ,__FUNCTION__,
                                                    pSlaEntry->u4SlaId, u4MilliSec);
                                 return OSIX_FAILURE;
                             }

                             /* Increment the currently running performance
                              * test count */
                             pContextInfo->u1PerfTestCount ++;
                             pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

                             Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                                Y1564_CTRL_TRC," %s : "
                                                "Performance Test for PerfTest Id : %d "
                                                "Sla Id %d Packet Size : %d "
                                                "started successfully and "
                                                "perf test count %d \r\n", __FUNCTION__,
                                                pPerfTestEntry->u4PerfTestId,
                                                pSlaEntry->u4SlaId,
                                                pTrafProfEntry->u2CurrentEmixPktSize,
                                                pContextInfo->u1PerfTestCount);

                             return OSIX_SUCCESS;
                         }
                         else
                         {
                             Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                                Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                                " %s : Performance Test start failed for Sla "
                                                "%d and perf test count %d \r\n",
                                                __FUNCTION__, pSlaEntry->u4SlaId,
                                                pContextInfo->u1PerfTestCount);
                             return OSIX_FAILURE;
                         }
                     }

                 if (pPerfTestReportEntry->u1StatsResult == Y1564_TEST_FAIL)
                 {
                     if (pContextInfo->u1PerfTestCount != Y1564_ZERO)
                     {
                         /* Decrement the currently running performance
                          * test count value */
                         pContextInfo->u1PerfTestCount --;
                     }

                     Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                        Y1564_MGMT_TRC | Y1564_ALL_FAILURE_TRC,
                                        " %s : Performance Test failed for PerfTest Id : %d "
                                        "%d Sla Id %d Packet Size : %d perf test count : %d \r\n",
                                        __FUNCTION__, pPerfTestEntry->u4PerfTestId,
                                        pSlaEntry->u4SlaId,
                                        pPerfTestReportEntry->i4FrameSize,
                                        pContextInfo->u1PerfTestCount);
                 }

                 /* Test is completed and mark the test mode is
                  * no test is in progress */
                 /* Test duration got over change the SLA status */
                 pPerfTestEntry->u1PerfTestStatus = Y1564_SLA_STOP;
                 pSlaEntry->u1SlaCurrentTestMode = Y1564_NO_TEST_IN_PROGRESS;
                 return OSIX_SUCCESS;
             }
         }
         pNextPerfTestEntry = Y1564UtilGetNextNodeFromPerfTestTable (pPerfTestEntry);
         pPerfTestEntry = pNextPerfTestEntry;
     }

     return OSIX_SUCCESS;
 }

/******************************************************************************
 * Function Name      : Y1564CoreStartConfTest                                *
 *                                                                            *
 * Description        : This routine starts a Configuration test              *
 *                                                                            *
 * Input(s)           : pSlaEntry - SLA node for which test to be started     *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 ******************************************************************************/
INT4
Y1564CoreStartConfTest (tSlaInfo * pSlaEntry)
{
    tUtlSysPreciseTime      SysPreciseTime;
    tTrafficProfileInfo    *pTrafProfEntry = NULL;
    tSacInfo               *pSacEntry = NULL;
#ifndef MEF_WANTED
    tServiceConfInfo       *pServiceConfInfo = NULL;
#endif
    UINT4                   u4MilliSec = Y1564_ZERO;
    UINT4                   u4CIR = Y1564_ZERO;
    UINT4                   u4ErrorCode = Y1564_ZERO;
    UINT4                   u4Duration = Y1564_ZERO;
    UINT4                   u4SacRate = Y1564_ZERO;
    UINT1                   u1Count = Y1564_ZERO;
    tMacAddr                DstMacAddr;

    MEMSET (DstMacAddr, Y1564_ZERO, sizeof (tMacAddr));
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564StartConffTest : SLA entry is"
                          " NULL \r\n");
        CLI_SET_ERR (CLI_Y1564_ERR_SLA_NOT_CREATED);
        return OSIX_FAILURE;
    }

    pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                pSlaEntry->u4SlaTrafProfId);
    if (pTrafProfEntry == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Traffic Profile Entry %d is not present in SLA"
                           "%d \r\n", __FUNCTION__,pSlaEntry->u4SlaTrafProfId, 
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_TRAF_PROF_NOT_MAPPED);
        return OSIX_FAILURE;
    }

    pSacEntry = Y1564UtilSacGetSacEntry (pSlaEntry->u4ContextId,
                                         pSlaEntry->u4SlaSacId);
    if (pSacEntry == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : SAC entry  %d is not present in SLA"
                           "%d \r\n", __FUNCTION__, pSlaEntry->u4SlaSacId, 
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SAC_NOT_MAPPED);
        return OSIX_FAILURE;
    }
    else
    {
        u4SacRate = (UINT4) (((pTrafProfEntry->u4PortSpeed / Y1564_HUNDRED) *
                      pSacEntry->u4SacInfoRate) / Y1564_BPS_TO_KBPS_CONV_FACTOR);
    }

#ifndef MEF_WANTED
    pServiceConfInfo = Y1564UtilGetServConfEntry (pSlaEntry->u4ContextId, 
                                                  pSlaEntry->u2SlaServiceConfId);

    if (pServiceConfInfo == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Service Configuration Entry %d is not present in SLA "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u2SlaServiceConfId, 
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_SERVICE_CONF_NOT_MAPPED);
        return OSIX_FAILURE;
    }

    u4CIR = pServiceConfInfo->u4ServiceConfCIR;
#else
    u4CIR = pSlaEntry->u4CIR;

    if (pSlaEntry->u2EvcId == Y1564_ZERO)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : EVC %d is not present in SLA "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u2EvcId, 
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_EVC_NOT_MAPPED);
        return OSIX_FAILURE;
    }
#endif

    if ((pSlaEntry->u4SlaMegId == Y1564_ZERO) &&
        (pSlaEntry->u4SlaMeId == Y1564_ZERO) &&
        (pSlaEntry->u4SlaMepId == Y1564_ZERO))
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : MEG : %d, ME : %d, MEP : %d is not present in SLA "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u4SlaMegId, 
                           pSlaEntry->u4SlaMeId, pSlaEntry->u4SlaMepId, 
                           pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_ERR_MEG_ME_MEP_NOT_MAPPED);
        return OSIX_FAILURE;
    }
  
    if (nmhTestv2FsY1564SlaRowStatus (&u4ErrorCode,
                                      pSlaEntry->u4ContextId,
                                      pSlaEntry->u4SlaId, 
                                      ACTIVE) == SNMP_FAILURE)
    {
        /* TestRoutine itself takes care of filling Traces */
        return OSIX_FAILURE;
    }

    if (nmhSetFsY1564TrafProfRowStatus (pSlaEntry->u4ContextId,
                                        pSlaEntry->u4SlaTrafProfId,
                                        ACTIVE) == SNMP_FAILURE)
    {
        /* SetRoutine itself takes care of filling Traces */
        return OSIX_FAILURE;
    }

#ifndef MEF_WANTED
    if (nmhSetFsY1564ServiceConfRowStatus (pSlaEntry->u4ContextId, 
                                           pSlaEntry->u2SlaServiceConfId,
                                           ACTIVE) == SNMP_FAILURE)
    {
        /* SetRoutine itself takes care of filling Traces */
        return OSIX_FAILURE;
    }
#endif
    /* Make the SLA ACTIVE , it will check whether
     * all SLA parameters are mapped  */
    if (nmhSetFsY1564SlaRowStatus (pSlaEntry->u4ContextId,
                                   pSlaEntry->u4SlaId, 
                                   ACTIVE) == SNMP_FAILURE)
    {
        /* TestRoutine itself takes care of filling Traces */
        return OSIX_FAILURE;
    }

    if (Y1564UtilGetSlaTestParams (pSlaEntry) != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC |
                           Y1564_ALL_FAILURE_TRC," %s : Failed to get "
                           "MEP related information for MEP %d ME %d MEG "
                           " %d in ECFM/Y.1731 \r\n", __FUNCTION__,
                           pSlaEntry->u4SlaMepId,
                           pSlaEntry->u4SlaMeId,pSlaEntry->u4SlaMegId);
        return OSIX_FAILURE;
    }

    if (u4CIR > (pTrafProfEntry->u4PortSpeed / 1000))
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : CIR value is greater than the port speed "
                           "of the interface. So, Test will not be initiated "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_CIR_GREATER_THAN_PORT_SPEED);
        return OSIX_FAILURE;
    }
    else if (u4SacRate > u4CIR)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : SAC value is greater than the CIR Value. "
                           "So, Test will not be initiated "
                           "%d \r\n", __FUNCTION__,pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_Y1564_SAC_GREATER_THAN_CIR_VALUE);
        return OSIX_FAILURE;

    }

    if (MEMCMP (pTrafProfEntry->DstMacAddr, DstMacAddr, sizeof (tMacAddr)) == Y1564_ZERO)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,  Y1564_ALL_FAILURE_TRC,
                           " %s  Failed because either ECFM is not settled "
                           "or Remote node is not reachable \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_REMOTE_NODE_NOT_REACHABLE);
        return OSIX_FAILURE;
    }

    pSlaEntry->u1SlaConfTestStatus = Y1564_SLA_START;
    Y1564_CONVERT_SEC_TO_MILLISECONDS (pSlaEntry->u4SlaConfTestDuration,
                                       u4MilliSec);

    if (pTrafProfEntry->u1EmixSelected == Y1564_TRUE)
    {
        MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, 0,
                (Y1564_EMIX_PKT_SIZE + 1));

        MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
               pTrafProfEntry->au1TrafProfOptEmixPktSize,
               STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));
        Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

        u4MilliSec = (UINT4) ((u4MilliSec / 
                               STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize)) 
                              * u1Count);
        pSlaEntry->u4SlaStepLoadDuration = u4MilliSec;
    }

    if (Y1564UtilValidateSlaTest (pSlaEntry, pTrafProfEntry, &u4Duration) 
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (u4Duration != Y1564_ZERO)
    {
        u4MilliSec = u4Duration;
    }

    UtlGetPreciseSysTime (&SysPreciseTime);
    pSlaEntry->TestStartTimeStamp =  SysPreciseTime.u4Sec;

    if (Y1564PortSlaStartConfTest (pSlaEntry, pTrafProfEntry, u4MilliSec) !=
        OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Configuration Test start failed for : Sla Id %d "
                           "SAC Id %d Traffic Profile Id %d \r\n",__FUNCTION__,
                           pSlaEntry->u4SlaId, pSlaEntry->u4SlaSacId,
                           pSlaEntry->u4SlaTrafProfId);
        return OSIX_FAILURE;

    }

    pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_IN_PROGRESS;

    /* Start the SLA Configuration Test Timer */
    if (Y1564TmrStartTimer (pSlaEntry, Y1564_SLA_CONF_TEST_TMR,
                            u4MilliSec) 
        != OSIX_SUCCESS)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                           " %s : Failed to start Configuration test timer "
                           "for Sla Id %d \r\n" ,__FUNCTION__,
                           pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : Y1564CoreStartPerfTest                                *
 *                                                                            *
 * Description        : This routine starts a Configuration test              *
 *                                                                            *
 * Input(s)           : pSlaEntry - SLA node for which test to be started     *
 *                                                                            *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                             *
 ******************************************************************************/
INT4
Y1564CoreStartPerfTest (tPerfTestInfo * pPerfTestEntry)
{
    tUtlSysPreciseTime   SysPreciseTime;
    tSlaInfo            *pSlaEntry = NULL;
    tTrafficProfileInfo *pTrafProfEntry = NULL;
    tConfTestReportInfo *pConfTestReportEntry = NULL;
    tConfTestReportInfo *pNextConfTestReportEntry = NULL;
    tY1564ContextInfo   *pContextInfo = NULL;
    tSlaInfo            SlaEntry;
    UINT4               u4MilliSec  = Y1564_ZERO;
    UINT4               u4Duration = Y1564_ZERO;
    UINT2               u2SlaListCount = Y1564_ZERO;
    UINT2               u2Index = Y1564_ONE;
    BOOL1               bResult = Y1564_ZERO;
    UINT1               u1TstStartFlag = OSIX_FALSE;
    UINT1               u1TstStartCount = Y1564_ZERO;
    UINT1               u1ErrFlag = OSIX_FALSE;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    /* Check Configuration test for the SLA is pass before starting the
     * performance test for that SLA */

    if (pPerfTestEntry == NULL)
    {
	    Y1564_GLOBAL_TRC ("Y1564StartPerfTest : Performance test entry is"
			    " NULL \r\n");
	    return OSIX_FAILURE;
    }

    /* Added to update the Perftestcount*/
    pContextInfo = Y1564CxtGetContextEntry (pPerfTestEntry->u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        return OSIX_FAILURE;
    }
 
    if (pPerfTestEntry->u2SlaListCount == Y1564_ZERO)
    {
        Y1564_GLOBAL_TRC ("Y1564StartPerfTest : SLA Count in Performance "
                          "test entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    else if (pPerfTestEntry->u2SlaListCount != Y1564_ZERO)
    {
        /* Ex : If SLA 3 is configured for Perf Id 1
         * then SlaListCount has value 3 because 3rd bit is 
         * set in BitMask. The variable pPerfTestEntry->u2SlaListCount
         * will be updated when ever a bit is set,to 
         * avoid unnecessary loop */
        u2SlaListCount  = pPerfTestEntry->u2SlaListCount;

        for (; u2Index <= pPerfTestEntry->u2SlaListCount; u2Index++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)),
                                     u2Index, sizeof (tY1564SlaList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                u2SlaListCount--;
                continue;
            }
            else
            {
                SlaEntry.u4SlaId = u2Index;
                SlaEntry.u4ContextId = pPerfTestEntry->u4ContextId;

                pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

                if (pSlaEntry == NULL)
                {
                    if (u2SlaListCount != Y1564_ZERO)
                    {
                        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, 
                                           Y1564_SESSION_TRC | Y1564_CRITICAL_TRC
                                           , " %s : SLA entry is NULL for Sla "
                                           "Id %d Continue with Next Sla \r\n",
                                           __FUNCTION__,u2Index);

                        u2SlaListCount--;
                        continue;
                    }

                    Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, 
                                       Y1564_SESSION_TRC | Y1564_CRITICAL_TRC |
                                       Y1564_ALL_FAILURE_TRC, " %s : SLA "
                                       "entry is not present to initiate "
                                       "performance test \r\n", __FUNCTION__);
                    return OSIX_FAILURE;                                 
                }

                pTrafProfEntry = Y1564UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                            pSlaEntry->u4SlaTrafProfId);
                if (pTrafProfEntry == NULL)
                {
                    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                       Y1564_ALL_FAILURE_TRC |
                                       Y1564_CRITICAL_TRC," %s : "
                                       "Traffic Profile Entry is not "
                                       "present for Sla Id %d. Proceed "
                                       "to next Sla \r\n", 
                                       __FUNCTION__,
                                       pSlaEntry->u4SlaId);
                    continue;
                }

                if (pSlaEntry != NULL)
                {
                    u2SlaListCount--;

                    UtlGetPreciseSysTime (&SysPreciseTime);
                    pSlaEntry->TestStartTimeStamp = SysPreciseTime.u4Sec;

                    /* simple cir or step load cir will be the first entry for each
                     * sla's in the configuration report entry . So the below
                     * actions are done before starting performance test.
                     * 1. Simple CIR test report is validated first, if 
                     * entry is present proceed to next test.
                     * 2. Step Load CIR test is validated if entry is not present
                     * for Simple CIR test */

                    if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
                    {

                        if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
                        {
                            Y1564_CONVERT_MIN_TO_MILLISECONDS ((UINT4) pPerfTestEntry->u1PerfTestDuration,
                                                               u4MilliSec);
                        }
                        if ((pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_2HOUR) ||
                            (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_24HOUR))
                        {
                            Y1564_CONVERT_HOURS_TO_MILLISECONDS ((UINT4) pPerfTestEntry->u1PerfTestDuration,
                                                                 u4MilliSec);
                        }

                        pConfTestReportEntry  = Y1564CoreGetConfTestReport (pSlaEntry, pTrafProfEntry);

                        if (pConfTestReportEntry == NULL)
                        {
                            Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                               Y1564_SESSION_TRC | Y1564_MGMT_TRC |
                                               Y1564_ALL_FAILURE_TRC, " %s : "
                                               "Configuration test report is not "
                                               "present for Sla Id %d Proceed "
                                               "to next SLA \r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId);
                            u1ErrFlag = OSIX_TRUE;
                            continue;
                        }

                        /* Simple CIR report is NULL but step load CIR report is 
                         * present, we cannot use else case, So using if case to 
                         * validate the configuration report entry */
                        else if (pConfTestReportEntry != NULL)
                        {
                            do
                            {
                                if ((pConfTestReportEntry->u4ContextId == pSlaEntry->u4ContextId) &&
                                    (pConfTestReportEntry->u4SlaId == pSlaEntry->u4SlaId) &&
                                    (pConfTestReportEntry->i4FrameSize == pTrafProfEntry->u2TrafProfPktSize))
                                {
                                    if (((pConfTestReportEntry->u1StatsResult == Y1564_TEST_PASS) ||
                                         (pConfTestReportEntry->u1StatsResult == Y1564_FIVE)) && 
                                         (pSlaEntry->u1SlaCurrentTestState != Y1564_SLA_TEST_IN_PROGRESS))
                                    {
                                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC |
                                                           Y1564_SESSION_TRC," %s : Configuration "
                                                           "test report entry found for Sla Id %d "
                                                           "Fr Size : %d Test Mode : %d Step Id : %d"
                                                           " Validating next configuration entry "
                                                           "for same SLA\r\n",
                                                           __FUNCTION__, pSlaEntry->u4SlaId,
                                                           pConfTestReportEntry->i4FrameSize,
                                                           pConfTestReportEntry->u4CurrentTestMode,
                                                           pConfTestReportEntry->u4StatsStepId);
                                        u1TstStartFlag = OSIX_TRUE;
                                    }
                                    else
                                    {
                                        if ((pSlaEntry->u1SlaCurrentTestState ==
                                             Y1564_SLA_TEST_IN_PROGRESS) &&
                                            (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
                                        {
                                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                                               Y1564_SESSION_TRC | Y1564_CRITICAL_TRC,
                                                               "%s : Perf Test is in-progress "
                                                               "for Sla Id %d\r\n",
                                                               __FUNCTION__,pSlaEntry->u4SlaId);
                                        }
                                        else
                                        {

                                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC |
                                                               Y1564_SESSION_TRC," %s : Configuration "
                                                               "test report entry found for Sla Id %d "
                                                               "Fr Size : %d Test Mode : %d Step Id : %d"
                                                               " but either test is not success or "
                                                               "test is in-progress for same SLA \r\n",
                                                               __FUNCTION__, pSlaEntry->u4SlaId,
                                                               pConfTestReportEntry->i4FrameSize,
                                                               pConfTestReportEntry->u4CurrentTestMode,
                                                               pConfTestReportEntry->u4StatsStepId);
                                        }
                                        u1ErrFlag = OSIX_TRUE;
                                        u1TstStartFlag = OSIX_FALSE;
                                        break;
                                    }
                                }

                                pNextConfTestReportEntry = 
                                    Y1564UtilGetNextNodeFromConfTestReportTable (pConfTestReportEntry);

                                pConfTestReportEntry = pNextConfTestReportEntry;

                            }while (pConfTestReportEntry != NULL);
                        }

                    }

                    else
                    {
                        MEMSET (pTrafProfEntry->au1TempTrafProfOptEmixPktSize, Y1564_ZERO,
                                (Y1564_EMIX_PKT_SIZE + Y1564_ONE));

                        MEMCPY(pTrafProfEntry->au1TempTrafProfOptEmixPktSize,
                               pTrafProfEntry->au1TrafProfOptEmixPktSize,
                               STRLEN(pTrafProfEntry->au1TrafProfOptEmixPktSize));

                        if (Y1564CoreValidateSlaPerfTest (pSlaEntry,
                                                          pTrafProfEntry,
                                                          pPerfTestEntry,
                                                          &u4Duration) != OSIX_SUCCESS)
                        {
                            u1TstStartFlag = OSIX_FALSE;
                            Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                               Y1564_SESSION_TRC | Y1564_MGMT_TRC |
                                               Y1564_ALL_FAILURE_TRC, " %s : "
                                               "Either configuration test "
                                               "report is not present or test "
                                               "is in-progress for Sla Id %d Proceed "
                                               "to next SLA \r\n",
                                               __FUNCTION__,pSlaEntry->u4SlaId);
                            u1ErrFlag = OSIX_TRUE;
                            continue;
                        }

                        /* Test should not be initiated for frame size
                         * Zero */
                        if (pTrafProfEntry->u2CurrentEmixPktSize == Y1564_ZERO)
                        {
                            return OSIX_SUCCESS;
                        }

                        u4MilliSec = u4Duration;
                        u1TstStartFlag = OSIX_TRUE;
                    }


                    if (u1TstStartFlag == OSIX_TRUE)
                    {
                        pPerfTestEntry->u1PerfTestStatus = Y1564_SLA_START;

                        if (Y1564PortSlaStartPerfTest (pSlaEntry, pTrafProfEntry,
                                                       u4MilliSec, pPerfTestEntry->u4PerfTestId) 
                            != OSIX_SUCCESS)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                               Y1564_ALL_FAILURE_TRC," %s : "
                                               "Test start failed for Sla Id %d "
                                               "Sac Id %d Traffic Profile Id %d "
                                               "\r\n",__FUNCTION__,
                                               pSlaEntry->u4SlaId, 
                                               pSlaEntry->u4SlaSacId,
                                               pSlaEntry->u4SlaTrafProfId);
                            return OSIX_FAILURE;

                        }

                        /* Start the SLA Performance Test Timer */
                        if (Y1564TmrStartTimer (pSlaEntry, Y1564_SLA_PERF_TEST_TMR,
                                                u4MilliSec) 
                            != OSIX_SUCCESS)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                               Y1564_ALL_FAILURE_TRC," %s : "
                                               "Failed to start "
                                               "Performance test timer "
                                               "for Sla Id %d \r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId);
                            return OSIX_FAILURE;
                        }

                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                           Y1564_CRITICAL_TRC |
                                           Y1564_SESSION_TRC | Y1564_CTRL_TRC,
                                           " %s : Performance test has been "
                                           "been started successfully for Sla "
                                           "Id %d in Perf Id %d \r\n",__FUNCTION__,
                                           pSlaEntry->u4SlaId, 
                                           pPerfTestEntry->u4PerfTestId);

                        pContextInfo->u1PerfTestCount ++ ;

                        Y1564_CONTEXT_TRC (pContextInfo->u4ContextId, Y1564_CTRL_TRC, 
                                           " %s : Currently running performance test "
                                           "count for Perf Id %d : %d \r\n", __FUNCTION__, 
                                           pPerfTestEntry->u4PerfTestId, 
                                           pContextInfo->u1PerfTestCount);

                        /* Start count is used to check whether performance
                         * test is started successfully for all SLA's or not */

                        u1TstStartCount ++; 

                        continue;
                    }
                    else
                    {
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                           Y1564_CTRL_TRC |
                                           Y1564_SESSION_TRC | Y1564_MGMT_TRC,
                                           " %s : Performance test start "
                                           "fail for Sla Id %d in Perf "
                                           "Id %d. Continue with next SLA \r\n",
                                           __FUNCTION__,pSlaEntry->u4SlaId,
                                           pPerfTestEntry->u4PerfTestId);
                        /* Perf test start fail for few SLAs after the successful
                         * start of SLA's */
                        if (u1TstStartCount != Y1564_ZERO)
                        {
                            CLI_SET_ERR (CLI_Y1564_ERR_FEW_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE);
                            u1ErrFlag = OSIX_TRUE;
                        }
                        continue;
                    }
                }
            }
        }
        /* case where the configuration test report for first SLA is not success
         * but report for other SLA is success then perf test will be started
         * for other SLAs but fail to start for the entry, then error should be
         * thrown */

        if ((u1TstStartCount != Y1564_ZERO) &&
            (u1ErrFlag == OSIX_TRUE))
        {
            CLI_SET_ERR (CLI_Y1564_ERR_FEW_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE);
            return OSIX_FAILURE;
        }

        if ((u2Index > pPerfTestEntry->u2SlaListCount) &&
            (u1TstStartFlag == OSIX_FALSE))
        {
            if ((u1TstStartCount != Y1564_ZERO) &&
                (u1TstStartCount < pPerfTestEntry->u2SlaListCount))

            {
                Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                   Y1564_SESSION_TRC | Y1564_CTRL_TRC
                                   | Y1564_MGMT_TRC," %s : "
                                   "Reached end of Sla list for Perf Id %d "
                                   "Performance test start fail for "
                                   "few SLA's\r\n",
                                   __FUNCTION__, pPerfTestEntry->u4PerfTestId);
                CLI_SET_ERR (CLI_Y1564_ERR_FEW_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE);
            }
            else if (u1TstStartCount == Y1564_ZERO)
            {
                Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                   Y1564_SESSION_TRC | Y1564_CTRL_TRC
                                   | Y1564_MGMT_TRC," %s : "
                                   "Reached end of Sla list for Perf Id %d "
                                   "Performance test start failed for "
                                   "all SLA's\r\n",
                                   __FUNCTION__, pPerfTestEntry->u4PerfTestId);
                CLI_SET_ERR (CLI_Y1564_ERR_ALL_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE);
            }
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}
/*****************************************************************************
 * Function Name      : Y1564CoreStopConfTest                                *
 *                                                                           *
 * Description        : This routine stops the SLA test forcefully           *
 *                                                                           *
 * Input(s)           : pSlaEntry - SLA node for which test to be stopped    *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
INT4
Y1564CoreStopConfTest (tSlaInfo * pSlaEntry)
{
    /* The below function send event to stop the currently running performance
     * test in ECFM/Y1731 */

    tY1564ContextInfo       *pContextInfo = NULL;
    tConfTestReportInfo     *pConfigTestReportEntry = NULL;


    pContextInfo = Y1564CxtGetContextEntry (pSlaEntry->u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        CLI_SET_ERR (CLI_Y1564_ERR_CONTEXT_NOT_PRESENT);
        return OSIX_FAILURE;
    }

    if ((pSlaEntry->u1SlaConfTestStatus == (UINT1) Y1564_START) &&
        (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS))
    {
        pContextInfo->u1ConfTestCount --;

        Y1564SendTrapNotifications (pSlaEntry, Y1564_TRANSACTION_FAIL);
        if (Y1564PortSlaStopConfTest (pSlaEntry) != OSIX_SUCCESS)
        {
            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC, " %s : "
                               "Configuration test stop failed for Sla Id %d \r\n",
                               __FUNCTION__,pSlaEntry->u4SlaId);
            return OSIX_FAILURE;
        }
        /* Stop the SLA Timer */
        if (Y1564TmrStopTimer (pSlaEntry, Y1564_SLA_CONF_TEST_TMR) == OSIX_FAILURE)
        {
            Y1564_GLOBAL_TRC ("Y1564StopConfTest : Failed to Stop the timer\r\n");
            return OSIX_FAILURE;
        }

        pConfigTestReportEntry = Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                                  pSlaEntry->u4SlaId,
                                                                  pSlaEntry->i4CurrentFrSize,
                                                                  pSlaEntry->u1SlaCurrentTestMode,
                                                                  pSlaEntry->u2SlaCurrentStep);
        if (pConfigTestReportEntry == NULL)
        {
            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC,
                               " %s : Configuration test report entry "
                               "is not present for Sla Id %d \r\n", __FUNCTION__,
                               pSlaEntry->u4SlaId);
            return OSIX_FAILURE;
        }

        pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_ABORTED;
        pConfigTestReportEntry->u1StatsResult = pSlaEntry->u1SlaCurrentTestState;

        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_CTRL_TRC,
                           " %s : Sla : %d , Current test state : "
                           "Aborted and configuration test count : %d "
                           "\r\n", __FUNCTION__,
                           pSlaEntry->u4SlaId, pContextInfo->u1ConfTestCount);

        return OSIX_SUCCESS;
    }

    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_ALL_FAILURE_TRC, " %s : "
                       "Configuration test status is already in STOP state "
                       "for Sla Id %d \r\n",__FUNCTION__,pSlaEntry->u4SlaId);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : Y1564CoreStopPerfTest                                *
 *                                                                           *
 * Description        : This routine stops a SLA Perf test forcefully        *
 *                                                                           *
 * Input(s)           : pPerfTestEntry - Perf node for which test to be      *
 *                                       stopped                             *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 *****************************************************************************/
INT4
Y1564CoreStopPerfTest (tPerfTestInfo * pPerfTestEntry)
{
    tY1564ContextInfo   *pContextInfo = NULL;
    tSlaInfo            *pSlaEntry = NULL;
    tPerfTestReportInfo *pPerfTestReportEntry = NULL;
    tSlaInfo            SlaEntry;
    UINT2               u2Index = Y1564_ONE;
    BOOL1               bResult = Y1564_ZERO;

    MEMSET(&SlaEntry, Y1564_ZERO, sizeof(tSlaInfo));

    /* Added to update the Perftestcount*/
    pContextInfo = Y1564CxtGetContextEntry (pPerfTestEntry->u4ContextId);

    if (pContextInfo == NULL)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId, Y1564_ALL_FAILURE_TRC |
                           Y1564_MGMT_TRC, " %s : does not exist\r\n",
                           __FUNCTION__);
        return OSIX_FAILURE;
    }

    if (pPerfTestEntry->u2SlaListCount != Y1564_ZERO)
    {
        for (; u2Index <= pPerfTestEntry->u2SlaListCount; u2Index++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pPerfTestEntry->pau1SlaList)),
                                     u2Index, sizeof (tY1564SlaList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                continue;
            }
            else
            {
                SlaEntry.u4SlaId = u2Index;
                SlaEntry.u4ContextId = pPerfTestEntry->u4ContextId;

                pSlaEntry = Y1564UtilSlaGetNodeFromSlaTable (&SlaEntry);

                if (pSlaEntry != NULL)
                {
                    /* The below function send an event to stop the currently 
                     * running performance test in ECFM/Y1731 */

                    if ((pPerfTestEntry->u1PerfTestStatus == Y1564_START) &&
                        (pSlaEntry->u1SlaCurrentTestState == Y1564_SLA_TEST_IN_PROGRESS)&&
                        (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
                    {
                        /* Decrement the currently running test count */
                        pContextInfo->u1PerfTestCount --;

                        Y1564SendTrapNotifications (pSlaEntry, Y1564_TRANSACTION_FAIL);
                        if (Y1564PortSlaStopConfTest (pSlaEntry) != OSIX_SUCCESS)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                               Y1564_ALL_FAILURE_TRC, " %s : "
                                               "Performance test stop failed "
                                               "for SlaId %d in Perf Test Id %d \r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId,
                                               pPerfTestEntry->u4PerfTestId);
                            return OSIX_FAILURE;
                        }
                        /* Stop the SLA Timer */
                        if (Y1564TmrStopTimer (pSlaEntry, Y1564_SLA_PERF_TEST_TMR) 
                            == OSIX_FAILURE)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                               Y1564_ALL_FAILURE_TRC, " %s : "
                                               "Failed to performance test "
                                               "timer for Sla Id %d in Perf Test "
                                               "Id %d \r\n",__FUNCTION__,
                                               pSlaEntry->u4SlaId,
                                               pPerfTestEntry->u4PerfTestId);
                            return OSIX_FAILURE;
                        }

                        pPerfTestReportEntry = Y1564UtilGetPerfTestReportEntry
                            (pPerfTestEntry->u4ContextId,
                             pSlaEntry->u4SlaId,
                             pPerfTestEntry->u4PerfTestId,
                             pSlaEntry->i4CurrentFrSize);

                        if (pPerfTestReportEntry == NULL)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                               Y1564_CTRL_TRC, " %s : "
                                               "Performance test entry"
                                               "for Sla Id %d in Perf Test Id %d "
                                               "is not present and perf test count %d \r\n",
                                               __FUNCTION__, pSlaEntry->u4SlaId,
                                               pPerfTestEntry->u4PerfTestId,
                                               pContextInfo->u1PerfTestCount);
                            return OSIX_FAILURE;
                        }

                        pSlaEntry->u1SlaCurrentTestState = Y1564_SLA_TEST_ABORTED;
                        pPerfTestReportEntry->u1StatsResult = pSlaEntry->u1SlaCurrentTestState;
 
                        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, 
                                           Y1564_CTRL_TRC, " %s : "
                                           "Performance test stopped successfully "
                                           "for Sla Id %d in Perf Test Id %d "
                                           "and Perf Test count %d \r\n",
                                           __FUNCTION__, pSlaEntry->u4SlaId,
                                           pPerfTestEntry->u4PerfTestId,
                                           pContextInfo->u1PerfTestCount);

                    }
                }
            }
        }
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564PortGetEmixPktSizeAndTimerCount                     *
 *                                                                             *
 * DESCRIPTION      : This function Get the EMIX Pattern size for the current  *
 *                    context and the current test run.                        *
 *                                                                             *
 * INPUT            : pTrafficProfileInfo - Pointer to current Traffic Profile *
 *                                          node.                              *
 *                    pu1Count - No of times current pattern occurrence at the *
 *                               EMIX Pattern.                                 *
 *                                                                             *
 * OUTPUT           : NONE                                                     *
 *                                                                             *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE                                     *
 *                                                                             *
 *******************************************************************************/
PUBLIC VOID
Y1564PortGetEmixPktSizeAndTimerCount (tTrafficProfileInfo *pTrafficProfileInfo,
                                      UINT1 *pu1Count)
{
    UINT1               u1Index = Y1564_ZERO;
    UINT1               u1Count = Y1564_ZERO;
    UINT1               au1MatchEmixPattern[Y1564_TWO];
    UINT1              *pau1MatchEmixPattern;
    UINT1               au1EmixPattern[Y1564_EMIX_PKT_SIZE + 1];
    UINT1              *pau1EmixPattern;
    UINT1               au1TmpEmixPattern[Y1564_EMIX_PKT_SIZE + 1];
    UINT1              *pau1TmpEmixPattern;
    UINT1               u1EmixPatternLength = Y1564_ZERO;
    UINT1               au1EmixParams[Y1564_EMIX_PKT_SIZE + 3] = "abcdefg";
    UINT1              *pau1EmixParams;

    MEMSET (au1MatchEmixPattern, 0, Y1564_TWO);
    MEMSET (au1EmixPattern, 0, (Y1564_EMIX_PKT_SIZE + 1));
    MEMSET (au1TmpEmixPattern, 0, (Y1564_EMIX_PKT_SIZE + 1));

    pau1MatchEmixPattern = au1MatchEmixPattern;
    MEMCPY (pau1MatchEmixPattern, pTrafficProfileInfo->au1TempTrafProfOptEmixPktSize,
            Y1564_ONE);

    u1EmixPatternLength = (UINT1) STRLEN (pTrafficProfileInfo->au1TempTrafProfOptEmixPktSize);

    pau1EmixPattern = au1EmixPattern;
    MEMCPY (pau1EmixPattern, pTrafficProfileInfo->au1TempTrafProfOptEmixPktSize,
            u1EmixPatternLength);

    pau1TmpEmixPattern = au1TmpEmixPattern;


    if (u1EmixPatternLength != Y1564_ZERO)
    {
        for (u1Index = Y1564_ZERO; u1Index < u1EmixPatternLength; u1Index++)
        {
            if (STRNCMP (pau1MatchEmixPattern, pau1EmixPattern, Y1564_ONE) == 0)
            {
                u1Count++;
                pau1EmixPattern++;
                continue;
            }

            MEMCPY (pau1TmpEmixPattern, pau1EmixPattern, Y1564_ONE);
            pau1TmpEmixPattern++;
            pau1EmixPattern++;
        }

        *pu1Count = u1Count;

        MEMSET (pTrafficProfileInfo->au1TempTrafProfOptEmixPktSize, 0,
                (Y1564_EMIX_PKT_SIZE + 1));

        MEMCPY (pTrafficProfileInfo->au1TempTrafProfOptEmixPktSize,
                au1TmpEmixPattern, STRLEN (au1TmpEmixPattern));

        pau1EmixParams = au1EmixParams;

        /* If EMIX pattern is selected then use the EMIX pattern size */
        if (STRNCMP (pau1MatchEmixPattern, pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_64;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_128;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_256;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_512;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_1024;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_1280;
        }
        else if (STRNCMP (pau1MatchEmixPattern, ++pau1EmixParams, Y1564_ONE) == 0)
        {
            pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_FRAME_1518;
        }
    }
    else
    {
        /*Resetting to zero becuase this variable will have
         *previous value*/
         pTrafficProfileInfo->u2CurrentEmixPktSize = Y1564_ZERO;
    }

    return;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564CoreValidateSlaPerfTest                             *
 *                                                                             *
 * DESCRIPTION      : This function validate the configuration test result     *
 *                    to start the performance test.                           *
 *                                                                             *
 * INPUT            : pSlaEntry - Pointer to SLA structure                     *
 *                    pTrafficProfileInfo - Pointer to current Traffic Profile *
 *                                          node.                              *
 *                    pPerfTestEntry - Perf node for which test to be started  *
 *                                                                             *
 * OUTPUT           : u4Duration                                               *
 *                                                                             *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                *
 *                                                                             *
 *******************************************************************************/

INT4
Y1564CoreValidateSlaPerfTest (tSlaInfo *pSlaEntry,
                              tTrafficProfileInfo *pTrafProfEntry,
                              tPerfTestInfo  *pPerfTestEntry,
                              UINT4  *u4Duration)
{
    tConfTestReportInfo *pConfTestReportEntry = NULL;
    tConfTestReportInfo *pNextConfTestReportEntry = NULL;
    UINT4                u4MilliSec = Y1564_ZERO;
    UINT1                u1Len = Y1564_ONE;
    UINT1                u1PktLen = Y1564_ZERO;
    UINT1                u1Flag = OSIX_FALSE;
    UINT1                u1PerfTstStartFlag = OSIX_FALSE;
    UINT1                u1Count = Y1564_ZERO;

    if (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_15MIN)
    {
        Y1564_CONVERT_MIN_TO_MILLISECONDS ((UINT4) pPerfTestEntry->u1PerfTestDuration,
                                           u4MilliSec);
    }
    if ((pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_2HOUR) ||
        (pPerfTestEntry->u1PerfTestDuration == Y1564_TEST_DURATION_24HOUR))
    {
        Y1564_CONVERT_HOURS_TO_MILLISECONDS ((UINT4) pPerfTestEntry->u1PerfTestDuration,
                                             u4MilliSec);
    }

    Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

    u4MilliSec = (UINT4) ((u4MilliSec /
                           STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))
                          * u1Count);

    u1PktLen = (UINT1) STRLEN (pTrafProfEntry->au1TempTrafProfOptEmixPktSize);

    /* Validations are not required for the frame value 
     * Zero */
    if (pTrafProfEntry->u2CurrentEmixPktSize == Y1564_ZERO)
    {
        return OSIX_SUCCESS;
    }

    for (u1Len = Y1564_ZERO; u1Len <= u1PktLen; u1Len++)
    {
        pConfTestReportEntry  = Y1564CoreGetConfTestReport (pSlaEntry, pTrafProfEntry);
        
        if (pConfTestReportEntry == NULL)
        {
            Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                               Y1564_SESSION_TRC | 
                               Y1564_ALL_FAILURE_TRC, " %s : "
                               "Configuration test report  "
                               "entry for Sla Id %d for frame size %d "
                               "is not present. Proceed to next "
                               "frame size \r\n",
                               __FUNCTION__,pSlaEntry->u4SlaId,
                               pTrafProfEntry->u2CurrentEmixPktSize);                  

            Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);

            u4MilliSec = (UINT4) ((u4MilliSec /
                                   STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))
                                  * u1Count);
            continue;
        }

        if (pConfTestReportEntry != NULL)
        {
            do
            {
                if ((pConfTestReportEntry->i4FrameSize == pTrafProfEntry->u2CurrentEmixPktSize)
                    && (pConfTestReportEntry->u4ContextId == pSlaEntry->u4ContextId) &&
                        (pConfTestReportEntry->u4SlaId == pSlaEntry->u4SlaId))

                {
                    if ((pConfTestReportEntry->u1StatsResult == Y1564_TEST_PASS) ||
                        (pConfTestReportEntry->u1StatsResult == Y1564_FIVE))
                    {
                        if (pSlaEntry->u1SlaCurrentTestState != Y1564_SLA_TEST_IN_PROGRESS)
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC |
                                               Y1564_SESSION_TRC," %s : Configuration "
                                               "test report entry found for Sla Id : %d "
                                               "Pkt Size : %d Test Mode : %d Step Id : %d "
                                               "Validating next configuration entry "
                                               "for same SLA\r\n",__FUNCTION__,
                                               pSlaEntry->u4SlaId,
                                               pTrafProfEntry->u2CurrentEmixPktSize,
                                               pConfTestReportEntry->u4CurrentTestMode,
                                               pConfTestReportEntry->u4StatsStepId);

                            u1PerfTstStartFlag = OSIX_TRUE;
                        }
                        else
                        {
                            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC |
                                               Y1564_SESSION_TRC," %s : Configuration "
                                               "test report entry found for Sla Id : %d "
                                               "Pkt Size : %d Test Mode : %d Step Id : %d "
                                               "but test is in-progress. Proceed to "
                                               "next SLA \r\n",__FUNCTION__,
                                               pSlaEntry->u4SlaId,
                                               pTrafProfEntry->u2CurrentEmixPktSize,
                                               pConfTestReportEntry->u4CurrentTestMode,
                                               pConfTestReportEntry->u4StatsStepId);
                            u1PerfTstStartFlag = OSIX_FALSE;
                            break;
                        }

                    }
                    else
                    {

                        if (pConfTestReportEntry->u1StatsResult != Y1564_TEST_PASS)
                        {
                            if ((pSlaEntry->u1SlaCurrentTestState ==
                                 Y1564_SLA_TEST_IN_PROGRESS) &&
                                (pSlaEntry->u1SlaCurrentTestMode == Y1564_PERF_TEST))
                            {
                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                                   Y1564_SESSION_TRC | Y1564_CRITICAL_TRC,
                                                   "%s : Perf Test is in-progress "
                                                   "for Sla Id %d\r\n",
                                                   __FUNCTION__,pSlaEntry->u4SlaId);
                            }
                            else
                            {

                                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId, Y1564_MGMT_TRC |
                                                   Y1564_SESSION_TRC," %s : Configuration "
                                                   "test report entry found for Sla Id %d "
                                                   "Pkt Size : %d Test Mode : %d "
                                                   "Step Id : %d but either test "
                                                   "is not success or test is "
                                                   "in-progress for same SLA. "
                                                   "Continue with next frame size \r\n",
                                                   __FUNCTION__, pSlaEntry->u4SlaId,
                                                   pTrafProfEntry->u2CurrentEmixPktSize,
                                                   pConfTestReportEntry->u4CurrentTestMode,
                                                   pConfTestReportEntry->u4StatsStepId);
                                u1Flag = OSIX_TRUE;
                            }
                            u1PerfTstStartFlag = OSIX_FALSE;
                            break;
                        }
                    }
                }
                else
                {
                    /* Frame size in configuration test report 
                     * differs from  current frame size in traffic profile
                     * so return success or failure based on the
                     * u1PerfTstStartFlag value */
                    if (u1PerfTstStartFlag == OSIX_TRUE) 
                    {
                        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                           Y1564_SESSION_TRC | Y1564_MGMT_TRC ,
                                            " %s : Configuration test report available "
                                           "to start the performance test for "
                                           "Sla Id : %d Perf Id : %d \r\n",__FUNCTION__,
                                           pSlaEntry->u4SlaId,
                                           pPerfTestEntry->u4PerfTestId);
                        * u4Duration = u4MilliSec;
                        return OSIX_SUCCESS;
                    }
                    else
                    {
                        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                                           Y1564_SESSION_TRC | Y1564_MGMT_TRC ,
                                           " %s : Configuration test report "
                                           " available but result is not "
                                           "success to start the performance "
                                           "test for Sla Id : %d Perf Id : %d "
                                           "Fr Size : %d or test is "
                                           "in-progress \r\n", __FUNCTION__,
                                           pSlaEntry->u4SlaId,
                                           pPerfTestEntry->u4PerfTestId,
                                           pConfTestReportEntry->i4FrameSize);                  
                        return OSIX_FAILURE;
                    }
                }

                pNextConfTestReportEntry =
                    Y1564UtilGetNextNodeFromConfTestReportTable (pConfTestReportEntry);

                pConfTestReportEntry = pNextConfTestReportEntry;

            }while (pConfTestReportEntry != NULL);
        }
        /* Report is NULL return failure */
        else
        {
            Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                               Y1564_SESSION_TRC | Y1564_MGMT_TRC ,
                               " %s : Configuration test report "
                               "entry is not available for Sla Id : %d "
                               "Perf Id : %d \r\n",__FUNCTION__,
                               pSlaEntry->u4SlaId,
                               pPerfTestEntry->u4PerfTestId);                  
            return OSIX_FAILURE;
        }

        if (u1Flag == OSIX_TRUE)
        {
           /* check other frame sizes in EMIX Pattern*/

            Y1564PortGetEmixPktSizeAndTimerCount (pTrafProfEntry, &u1Count);
            u4MilliSec = (UINT4) ((u4MilliSec /
                                   STRLEN (pTrafProfEntry->au1TrafProfOptEmixPktSize))
                                  * u1Count);
            continue;
        }
      
    }
    /* Return failure when none of the test is pass for the
     * given emix patern for the SLA */
    if (u1PerfTstStartFlag == OSIX_FALSE)
    {
        Y1564_CONTEXT_TRC (pPerfTestEntry->u4ContextId,
                           Y1564_SESSION_TRC | Y1564_MGMT_TRC ,
                           " %s : Either emix pattern does not "
                           "have packet size to proceed "
                           "or report is not present for Sla Id : %d "
                           "Perf Id : %d \r\n",__FUNCTION__,
                           pSlaEntry->u4SlaId,
                           pPerfTestEntry->u4PerfTestId);                  
        return OSIX_FAILURE;
    }

    * u4Duration = u4MilliSec;

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : Y1564CoreGetConfTestReport                               *
 *                                                                             *
 * DESCRIPTION      : This function fetch the configuration test report        *
 *                    to start the performance test.                           *
 *                                                                             *
 * INPUT            : pSlaEntry - Pointer to SLA structure                     *
 *                    pTrafficProfileInfo - Pointer to current Traffic Profile *
 *                                          node.                              *
 *                                                                             *
 * OUTPUT           : None                                                     *
 *                                                                             *
 * RETURNS          : Pointer to configuration test report structure           *
 *                                                                             *
 *******************************************************************************/
PUBLIC tConfTestReportInfo *
Y1564CoreGetConfTestReport (tSlaInfo *pSlaEntry, tTrafficProfileInfo *pTrafProfEntry)
{
    tConfTestReportInfo *pConfTestReportEntry = NULL;
    UINT4               u4TestMode = Y1564_ZERO;
    UINT4               u4TestStep = Y1564_ZERO;
    UINT4               u4MaxStep = Y1564_ZERO;
    UINT4               u2PktSize = Y1564_ZERO;

    if (pTrafProfEntry->u1EmixSelected != Y1564_TRUE)
    {
        u2PktSize = pTrafProfEntry->u2TrafProfPktSize;
    }
    else
    {
        u2PktSize = (UINT4) pTrafProfEntry->u2CurrentEmixPktSize;
    }

    for (u4TestMode = Y1564_ONE; u4TestMode <= Y1564_SIX; u4TestMode++)
    {
        if (u4TestMode == Y1564_ONE)
        {
            u4MaxStep = Y1564_ONE;
        }
        else if (u4TestMode == Y1564_TWO)
        {
            u4MaxStep = Y1564_TEN;
        }
        else
        {
            u4MaxStep = Y1564_TWO;
        }
        for (u4TestStep = Y1564_ONE; u4TestStep <= u4MaxStep; u4TestStep++)
        {
            pConfTestReportEntry =

                Y1564UtilGetConfTestReportEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaId,
                                                 (INT4) u2PktSize,
                                                 (UINT1) u4TestMode,
                                                 (UINT2) u4TestStep);
            if (pConfTestReportEntry != NULL)
            {
                return pConfTestReportEntry;
            }
            else
            {
                Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                                   Y1564_SESSION_TRC | Y1564_ALL_FAILURE_TRC,
                                   " %s : Configuration test eport entry "
                                   "for Sla Id %d is not present "
                                   "for frame size %d Test Mode %d"
                                   "Proceed to next test \r\n",
                                   __FUNCTION__, pSlaEntry->u4SlaId,
                                   u2PktSize, u4TestMode);
                continue;
            }
        }
    }
    return pConfTestReportEntry;
}
#endif
