/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: y1564tmr.c,v 1.6 2016/05/06 10:11:56 siva Exp $                 *
 *                                                                         *
 * Description: This file contains the Y1564 timer utility and expiry       *
 *              functions.                                                 *
 ***************************************************************************/

#ifndef __Y1564TMR_C__
#define __Y1564TMR_C__

#include "y1564inc.h"

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TmrExpHandler                               *
 *                                                                           *
 *    Description         : This function will invoke timer expiry function  *
 *                          based on expiried timers.                        *
 *                                                                           *
 *    Input(s)            : None.                                            *
 *                                                                           *
 *    Output(s)           : None.                                            *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
PUBLIC VOID
Y1564TmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimer = NULL;
    INT2                i2Offset = 0;
    UINT1               u1TimerType = 0;
    
    while ((pExpiredTimer = TmrGetNextExpiredTimer (Y1564_TIMER_LIST())) != NULL)
    {
        /* Expiry of timers will be ignored in StandBy Node.
         * Checking the RM API instead of Y1564 status, ensures that
         * even if Y1564is not ready to take the expiry, but Y1564 will
         * not loose the timer expiry.*/

        u1TimerType = ((tTmrBlk *) pExpiredTimer)->u1TimerId;

        if ((Y1564PortRmGetNodeState () != RM_STANDBY) &&
            (u1TimerType < Y1564_MAX_TMR_TYPES))
        {
            i2Offset = gY1564GlobalInfo.aTmrDesc[u1TimerType].i2Offset;

            (*(gY1564GlobalInfo.aTmrDesc[u1TimerType].TmrExpFn))
                ((UINT1 *) pExpiredTimer - i2Offset);
        }
    }
    return;
}
/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TmrInit                                     *
 *                                                                           *
 *    Description         : This function will perform following task in     *
 *                          Y1564 Module:                                    *
 *                          o  Creates timer list for Y1564 timers.          *
 *                                                                           *
 *    Input(s)            : None                                             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS - When this function successfully   *
 *                                         created timer list.               *
 *                          OSIX_FAILURE - When this function failed to      *
 *                                         create timer list.                *
 *****************************************************************************/
INT4
Y1564TmrInit (VOID)
{
    if (TmrCreateTimerList (Y1564_TASK_NAME, Y1564_TMR_EXPIRY_EVENT,
                            NULL, &(Y1564_TIMER_LIST ())) == TMR_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TmrInit : Timer list creation failed \r\n");
        return OSIX_FAILURE;
    }

    Y1564TmrInitTimerDesc ();

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TmrDeInit                                   *
 *                                                                           *
 *    Description         : If timer list was created, this function will    *
 *                          delete the timer List.                           *
 *                                                                           *
 *    Input(s)            : None                                             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *                                                                           *
 *****************************************************************************/
VOID
Y1564TmrDeInit (VOID)
{
    if (TmrDeleteTimerList (Y1564_TIMER_LIST ()) == TMR_FAILURE)
    {
        Y1564_GLOBAL_TRC ("Y1564TmrDeInit : Timer list deletion failed \r\n");
        return;
    }

    MEMSET (&(Y1564_TIMER_DESC ()), Y1564_ZERO, 
            sizeof (tTmrDesc) * Y1564_MAX_TMR_TYPES);

    return;
}

/*****************************************************************************
 *    Function Name       : Y1564TmrInitTimerDesc                            *
 *                                                                           *
 *    Description         : This function intializes the timer               *
 *                          descriptor for all the timers in Y1564 module    *
 *                                                                           *
 *    Input(s)            : None                                             *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
Y1564TmrInitTimerDesc (VOID)
{
    gY1564GlobalInfo.aTmrDesc[Y1564_SLA_CONF_TEST_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tSlaInfo, ConfTestTimer);

    gY1564GlobalInfo.aTmrDesc[Y1564_SLA_CONF_TEST_TMR].TmrExpFn =
        Y1564TmrConfTestTimerExp;

    gY1564GlobalInfo.aTmrDesc[Y1564_SLA_PERF_TEST_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tSlaInfo, PerfTestTimer);

    gY1564GlobalInfo.aTmrDesc[Y1564_SLA_PERF_TEST_TMR].TmrExpFn =
        Y1564TmrPerfTestTimerExp;

    return;
}

/*****************************************************************************
 *    Function Name       : Y1564TmrStartTimer                               *
 *                                                                           *
 *    Description         : This function will start the timer               *
 *                          for given SLA and duration.                      *
 *                                                                           *
 *    Input(s)            : pSlaInfo    - Pointer to SLA node.               *
 *                          u1TimerType - Timer type that needs              *
 *                                        to be started                      *
 *                          u4Duration - Timer duration.                     *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS - When this function successfully   *
 *                                         started timer of given            *
 *                                         timer type                        *
 *                          OSIX_FAILURE - When this function failed to      *
 *                                         start the timer.                  *
 *****************************************************************************/
PUBLIC INT4
Y1564TmrStartTimer (tSlaInfo *pSlaEntry, UINT1 u1TimerType,
                    UINT4 u4Duration)
{

    tTmrBlk            *pTimer = NULL;
    UINT4               u4Sec = Y1564_ZERO;
    UINT4               u4MilliSec = Y1564_ZERO;

    switch (u1TimerType)
    {
        case Y1564_SLA_CONF_TEST_TMR:

            pTimer = &(pSlaEntry->ConfTestTimer);
            break;

        case Y1564_SLA_PERF_TEST_TMR:

            pTimer = &(pSlaEntry->PerfTestTimer);
            break;

        default:

            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                               Y1564_CTRL_TRC | Y1564_CRITICAL_TRC,
                               " %s : Invalid Timer Type received \r\n",
                               __FUNCTION__);
            return OSIX_FAILURE;

    }

    Y1564_CONVERT_TMR_DURATION_TO_SEC (u4Duration, u4Sec, u4MilliSec);


    if (TmrStart
        (gY1564GlobalInfo.Y1564TmrListId, pTimer, u1TimerType, 
         u4Sec, u4MilliSec) == TMR_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_CRITICAL_TRC,
                           " %s : Timer start failed for SLA Id %d \r\n",
                           __FUNCTION__,pSlaEntry->u4SlaId);
        gY1564GlobalInfo.u4TimerFailCount++;
        Y1564SendTrapNotifications (pSlaEntry, Y1564_TRAP_TIMER_FAIL);

        return OSIX_FAILURE;
    }

    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                       Y1564_CTRL_TRC,
                       " %s : %s has been started with duration %d "
                       "for SLA Id %d \r\n",
                       __FUNCTION__,gaai1Y1564TimerState[u1TimerType],
                       u4Duration, pSlaEntry->u4SlaId);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : Y1564TmrStopTimer                                *
 *                                                                           *
 *    Description         : This function will stop the timer                *
 *                          for given SLA and duration.                      *
 *                                                                           *
 *    Input(s)            : pSlaInfo    - Pointer to SLA node.               *
 *                          u1TimerType - Timer type that needs              *
 *                                        to be started                      *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : OSIX_SUCCESS - When this function successfully   *
 *                                         stopped timer of given            *
 *                                         timer type                        *
 *                          OSIX_FAILURE - When this function failed to      *
 *                                         stop the timer.                  *
 *****************************************************************************/
PUBLIC INT4
Y1564TmrStopTimer (tSlaInfo *pSlaEntry, UINT1 u1TimerType)
{

    tTmrBlk            *pTimer = NULL;

    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564TmrStopTimer : SLA entry is NULL \r\n");
        return OSIX_FAILURE;
    }

    switch (u1TimerType)
    {
        case Y1564_SLA_CONF_TEST_TMR:

            pTimer = &(pSlaEntry->ConfTestTimer);
            break;

        case Y1564_SLA_PERF_TEST_TMR:

            pTimer = &(pSlaEntry->PerfTestTimer);
            break;

        default:

            Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                               Y1564_CTRL_TRC | Y1564_CRITICAL_TRC,
                               " %s : Invalid Timer Type received \r\n",
                               __FUNCTION__);
            return OSIX_FAILURE;

    }

    if (TmrStop (gY1564GlobalInfo.Y1564TmrListId, pTimer) == TMR_FAILURE)
    {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                           Y1564_CTRL_TRC | Y1564_CRITICAL_TRC,
                           " %s : Timer stop failed for SLA Id %d \r\n",
                           __FUNCTION__,pSlaEntry->u4SlaId);
        gY1564GlobalInfo.u4TimerFailCount++;
        return OSIX_FAILURE;
    }
    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                       Y1564_CTRL_TRC,
                       " %s : %s is stopped for SLA Id %d \r\n",
                       __FUNCTION__,gaai1Y1564TimerState[u1TimerType],
                       pSlaEntry->u4SlaId);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TmrConfTestTimerExp                         *
 *                                                                           *
 *    Description         : This function will handle holdoff timer expiry   *
 *                          event.                                           *
 *                                                                           *
 *    Input(s)            : pArg - Pointer to ring node.                     *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564TmrConfTestTimerExp (VOID *pArg)
{
    tSlaInfo                   *pSlaEntry = NULL;

    pSlaEntry = (tSlaInfo *) pArg;

    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564TmrConfTestTimerExp: SLA entry is NULL \r\n");
        return ;
    }

    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                       Y1564_CTRL_TRC, " %s : Timer Expired for SLA"
                       " %d \r\n", __FUNCTION__, pSlaEntry->u4SlaId);

    /* Post an event to ECFM/Y1731 to fetch the
     * performance monitoring results */
    OsixTskDelay (Y1564_FOUR * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

   if (Y1564UtilFetchReport (pSlaEntry) != OSIX_SUCCESS)
   {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                          Y1564_CRITICAL_TRC | Y1564_CTRL_TRC,
                          " %s : Event Posting failed "
                          "for SLA Id %d \r\n", __FUNCTION__,
                          pSlaEntry->u4SlaId);
        return;
   }
   return;
 }

/*****************************************************************************
 *                                                                           *
 *    Function Name       : Y1564TmrPerfTestTimerExp                         *
 *                                                                           *
 *    Description         : This function will handle holdoff timer expiry   *
 *                          event.                                           *
 *                                                                           *
 *    Input(s)            : pArg - Pointer to ring node.                     *
 *                                                                           *
 *    Output(s)           : None                                             *
 *                                                                           *
 *    Returns             : None.                                            *
 *****************************************************************************/
VOID
Y1564TmrPerfTestTimerExp (VOID *pArg)
{
    tSlaInfo                   *pSlaEntry = NULL;

    pSlaEntry = (tSlaInfo *) pArg;

    if (pSlaEntry == NULL)
    {
        Y1564_GLOBAL_TRC ("Y1564TmrPerfTestTimerExp : SLA entry is NULL \r\n");
        return ;
    }
 
    Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                       Y1564_CTRL_TRC, " %s : Timer Expired for SLA"
                       " %d \r\n", __FUNCTION__, pSlaEntry->u4SlaId);


    /* Post an event to ECFM/Y1731 to fetch the
     * performance monitoring results */
    
   OsixTskDelay (Y1564_FOUR * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

   if (Y1564UtilFetchReport (pSlaEntry) != OSIX_SUCCESS)
   {
        Y1564_CONTEXT_TRC (pSlaEntry->u4ContextId,
                          Y1564_CRITICAL_TRC | Y1564_CTRL_TRC,
                          " %s : Event Posting failed"
                          " for SLA Id %d \r\n", __FUNCTION__,
                          pSlaEntry->u4SlaId);
        return;
   }
   return;
 }

#endif
