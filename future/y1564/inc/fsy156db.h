/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsy156db.h,v 1.2 2015/11/18 11:57:08 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSY156DB_H
#define _FSY156DB_H

UINT1 FsY1564ContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564SlaTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564TrafProfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564SacTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564ServiceConfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564ConfigTestReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsY1564PerformanceTestTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsY1564PerfTestReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsy156 [] ={1,3,6,1,4,1,29601,2,88};
tSNMP_OID_TYPE fsy156OID = {9, fsy156};


UINT4 FsY1564ContextId [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,1};
UINT4 FsY1564ContextName [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,2};
UINT4 FsY1564ContextSystemControl [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,3};
UINT4 FsY1564ContextModuleStatus [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,4};
UINT4 FsY1564ContextTraceOption [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,5};
UINT4 FsY1564ContextTrapStatus [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,6};
UINT4 FsY1564ContextNumOfConfTestRunning [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,7};
UINT4 FsY1564ContextNumOfPerfTestRunning [ ] ={1,3,6,1,4,1,29601,2,88,1,1,1,8};
UINT4 FsY1564SlaId [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,1};
UINT4 FsY1564SlaEvcIndex [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,2};
UINT4 FsY1564SlaMEG [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,3};
UINT4 FsY1564SlaME [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,4};
UINT4 FsY1564SlaMEP [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,5};
UINT4 FsY1564SlaSacId [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,6};
UINT4 FsY1564SlaTrafProfileId [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,7};
UINT4 FsY1564SlaStepLoadRate [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,8};
UINT4 FsY1564SlaConfTestDuration [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,9};
UINT4 FsY1564SlaTestStatus [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,10};
UINT4 FsY1564SlaServiceConfId [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,11};
UINT4 FsY1564SlaColorMode [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,12};
UINT4 FsY1564SlaCoupFlag [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,13};
UINT4 FsY1564SlaCIR [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,14};
UINT4 FsY1564SlaCBS [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,15};
UINT4 FsY1564SlaEIR [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,16};
UINT4 FsY1564SlaEBS [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,17};
UINT4 FsY1564SlaTrafPolicing [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,18};
UINT4 FsY1564SlaTestSelector [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,19};
UINT4 FsY1564SlaCurrentTestMode [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,20};
UINT4 FsY1564SlaCurrentTestState [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,21};
UINT4 FsY1564SlaRowStatus [ ] ={1,3,6,1,4,1,29601,2,88,2,1,1,22};
UINT4 FsY1564TrafProfId [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,1};
UINT4 FsY1564TrafProfDir [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,2};
UINT4 FsY1564TrafProfPktSize [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,3};
UINT4 FsY1564TrafProfPayload [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,4};
UINT4 FsY1564TrafProfOptEmixPktSize [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,5};
UINT4 FsY1564TrafProfPCP [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,6};
UINT4 FsY1564TrafProfRowStatus [ ] ={1,3,6,1,4,1,29601,2,88,3,1,1,7};
UINT4 FsY1564SacId [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,1};
UINT4 FsY1564SacInfoRate [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,2};
UINT4 FsY1564SacFrLossRatio [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,3};
UINT4 FsY1564SacFrTransDelay [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,4};
UINT4 FsY1564SacFrDelayVar [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,5};
UINT4 FsY1564SacAvailability [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,6};
UINT4 FsY1564SacRowStatus [ ] ={1,3,6,1,4,1,29601,2,88,4,1,1,7};
UINT4 FsY1564ServiceConfId [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,1};
UINT4 FsY1564ServiceConfColorMode [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,2};
UINT4 FsY1564ServiceConfCoupFlag [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,3};
UINT4 FsY1564ServiceConfCIR [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,4};
UINT4 FsY1564ServiceConfCBS [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,5};
UINT4 FsY1564ServiceConfEIR [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,6};
UINT4 FsY1564ServiceConfEBS [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,7};
UINT4 FsY1564ServiceConfRowStatus [ ] ={1,3,6,1,4,1,29601,2,88,5,1,1,8};
UINT4 FsY1564ConfigTestReportSlaId [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,1};
UINT4 FsY1564ConfigTestReportFrSize [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,2};
UINT4 FsY1564ConfigTestCurrentTestMode [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,3};
UINT4 FsY1564ConfigTestReportStepId [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,4};
UINT4 FsY1564ConfigTestReportResult [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,5};
UINT4 FsY1564ConfigTestReportIrMin [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,6};
UINT4 FsY1564ConfigTestReportIrMean [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,7};
UINT4 FsY1564ConfigTestReportIrMax [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,8};
UINT4 FsY1564ConfigTestReportFrLossCnt [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,9};
UINT4 FsY1564ConfigTestReportFrLossRatio [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,10};
UINT4 FsY1564ConfigTestReportFrTxDelayMin [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,11};
UINT4 FsY1564ConfigTestReportFrTxDelayMean [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,12};
UINT4 FsY1564ConfigTestReportFrTxDelayMax [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,13};
UINT4 FsY1564ConfigTestReportFrDelayVarMin [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,14};
UINT4 FsY1564ConfigTestReportFrDelayVarMean [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,15};
UINT4 FsY1564ConfigTestReportFrDelayVarMax [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,16};
UINT4 FsY1564ConfigTestReportTestStartTime [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,17};
UINT4 FsY1564ConfigTestReportTestEndTime [ ] ={1,3,6,1,4,1,29601,2,88,6,1,1,18};
UINT4 FsY1564PerformanceTestIndex [ ] ={1,3,6,1,4,1,29601,2,88,7,1,1,1};
UINT4 FsY1564PerformanceTestSlaList [ ] ={1,3,6,1,4,1,29601,2,88,7,1,1,2};
UINT4 FsY1564PerformanceTestDuration [ ] ={1,3,6,1,4,1,29601,2,88,7,1,1,3};
UINT4 FsY1564PerformanceTestStatus [ ] ={1,3,6,1,4,1,29601,2,88,7,1,1,4};
UINT4 FsY1564PerformanceTestRowStatus [ ] ={1,3,6,1,4,1,29601,2,88,7,1,1,5};
UINT4 FsY1564PerfTestReportFrSize [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,1};
UINT4 FsY1564PerfTestReportResult [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,2};
UINT4 FsY1564PerfTestReportIrMin [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,3};
UINT4 FsY1564PerfTestReportIrMean [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,4};
UINT4 FsY1564PerfTestReportIrMax [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,5};
UINT4 FsY1564PerfTestReportFrLossCnt [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,6};
UINT4 FsY1564PerfTestReportFrLossRatio [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,7};
UINT4 FsY1564PerfTestReportFrTxDelayMin [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,8};
UINT4 FsY1564PerfTestReportFrTxDelayMean [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,9};
UINT4 FsY1564PerfTestReportFrTxDelayMax [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,10};
UINT4 FsY1564PerfTestReportFrDelayVarMin [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,11};
UINT4 FsY1564PerfTestReportFrDelayVarMean [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,12};
UINT4 FsY1564PerfTestReportFrDelayVarMax [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,13};
UINT4 FsY1564PerfTestReportAvailability [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,14};
UINT4 FsY1564PerfTestReportUnavailableCount [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,15};
UINT4 FsY1564PerfTestReportTestStartTime [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,16};
UINT4 FsY1564PerfTestReportTestEndTime [ ] ={1,3,6,1,4,1,29601,2,88,8,1,1,17};
UINT4 FsY1564TrapSlaId [ ] ={1,3,6,1,4,1,29601,2,88,9,1,1};
UINT4 FsY1564TypeOfFailure [ ] ={1,3,6,1,4,1,29601,2,88,9,1,2};




tMbDbEntry fsy156MibEntry[]= {

{{13,FsY1564ContextId}, GetNextIndexFsY1564ContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsY1564ContextName}, GetNextIndexFsY1564ContextTable, FsY1564ContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsY1564ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsY1564ContextSystemControl}, GetNextIndexFsY1564ContextTable, FsY1564ContextSystemControlGet, FsY1564ContextSystemControlSet, FsY1564ContextSystemControlTest, FsY1564ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ContextTableINDEX, 1, 0, 0, "2"},

{{13,FsY1564ContextModuleStatus}, GetNextIndexFsY1564ContextTable, FsY1564ContextModuleStatusGet, FsY1564ContextModuleStatusSet, FsY1564ContextModuleStatusTest, FsY1564ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ContextTableINDEX, 1, 0, 0, "2"},

{{13,FsY1564ContextTraceOption}, GetNextIndexFsY1564ContextTable, FsY1564ContextTraceOptionGet, FsY1564ContextTraceOptionSet, FsY1564ContextTraceOptionTest, FsY1564ContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564ContextTableINDEX, 1, 0, 0, "32"},

{{13,FsY1564ContextTrapStatus}, GetNextIndexFsY1564ContextTable, FsY1564ContextTrapStatusGet, FsY1564ContextTrapStatusSet, FsY1564ContextTrapStatusTest, FsY1564ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ContextTableINDEX, 1, 0, 0, "1"},

{{13,FsY1564ContextNumOfConfTestRunning}, GetNextIndexFsY1564ContextTable, FsY1564ContextNumOfConfTestRunningGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsY1564ContextNumOfPerfTestRunning}, GetNextIndexFsY1564ContextTable, FsY1564ContextNumOfPerfTestRunningGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsY1564SlaId}, GetNextIndexFsY1564SlaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaEvcIndex}, GetNextIndexFsY1564SlaTable, FsY1564SlaEvcIndexGet, FsY1564SlaEvcIndexSet, FsY1564SlaEvcIndexTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaMEG}, GetNextIndexFsY1564SlaTable, FsY1564SlaMEGGet, FsY1564SlaMEGSet, FsY1564SlaMEGTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaME}, GetNextIndexFsY1564SlaTable, FsY1564SlaMEGet, FsY1564SlaMESet, FsY1564SlaMETest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaMEP}, GetNextIndexFsY1564SlaTable, FsY1564SlaMEPGet, FsY1564SlaMEPSet, FsY1564SlaMEPTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaSacId}, GetNextIndexFsY1564SlaTable, FsY1564SlaSacIdGet, FsY1564SlaSacIdSet, FsY1564SlaSacIdTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaTrafProfileId}, GetNextIndexFsY1564SlaTable, FsY1564SlaTrafProfileIdGet, FsY1564SlaTrafProfileIdSet, FsY1564SlaTrafProfileIdTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaStepLoadRate}, GetNextIndexFsY1564SlaTable, FsY1564SlaStepLoadRateGet, FsY1564SlaStepLoadRateSet, FsY1564SlaStepLoadRateTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, "25"},

{{13,FsY1564SlaConfTestDuration}, GetNextIndexFsY1564SlaTable, FsY1564SlaConfTestDurationGet, FsY1564SlaConfTestDurationSet, FsY1564SlaConfTestDurationTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, "30"},

{{13,FsY1564SlaTestStatus}, GetNextIndexFsY1564SlaTable, FsY1564SlaTestStatusGet, FsY1564SlaTestStatusSet, FsY1564SlaTestStatusTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, "2"},

{{13,FsY1564SlaServiceConfId}, GetNextIndexFsY1564SlaTable, FsY1564SlaServiceConfIdGet, FsY1564SlaServiceConfIdSet, FsY1564SlaServiceConfIdTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaColorMode}, GetNextIndexFsY1564SlaTable, FsY1564SlaColorModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, "2"},

{{13,FsY1564SlaCoupFlag}, GetNextIndexFsY1564SlaTable, FsY1564SlaCoupFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaCIR}, GetNextIndexFsY1564SlaTable, FsY1564SlaCIRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaCBS}, GetNextIndexFsY1564SlaTable, FsY1564SlaCBSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaEIR}, GetNextIndexFsY1564SlaTable, FsY1564SlaEIRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaEBS}, GetNextIndexFsY1564SlaTable, FsY1564SlaEBSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaTrafPolicing}, GetNextIndexFsY1564SlaTable, FsY1564SlaTrafPolicingGet, FsY1564SlaTrafPolicingSet, FsY1564SlaTrafPolicingTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, "1"},

{{13,FsY1564SlaTestSelector}, GetNextIndexFsY1564SlaTable, FsY1564SlaTestSelectorGet, FsY1564SlaTestSelectorSet, FsY1564SlaTestSelectorTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 0, "15"},

{{13,FsY1564SlaCurrentTestMode}, GetNextIndexFsY1564SlaTable, FsY1564SlaCurrentTestModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SlaCurrentTestState}, GetNextIndexFsY1564SlaTable, FsY1564SlaCurrentTestStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsY1564SlaTableINDEX, 2, 0, 0, "1"},

{{13,FsY1564SlaRowStatus}, GetNextIndexFsY1564SlaTable, FsY1564SlaRowStatusGet, FsY1564SlaRowStatusSet, FsY1564SlaRowStatusTest, FsY1564SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564SlaTableINDEX, 2, 0, 1, NULL},

{{13,FsY1564TrafProfId}, GetNextIndexFsY1564TrafProfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564TrafProfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564TrafProfDir}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfDirGet, FsY1564TrafProfDirSet, FsY1564TrafProfDirTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 0, "1"},

{{13,FsY1564TrafProfPktSize}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfPktSizeGet, FsY1564TrafProfPktSizeSet, FsY1564TrafProfPktSizeTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 0, "512"},

{{13,FsY1564TrafProfPayload}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfPayloadGet, FsY1564TrafProfPayloadSet, FsY1564TrafProfPayloadTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564TrafProfOptEmixPktSize}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfOptEmixPktSizeGet, FsY1564TrafProfOptEmixPktSizeSet, FsY1564TrafProfOptEmixPktSizeTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564TrafProfPCP}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfPCPGet, FsY1564TrafProfPCPSet, FsY1564TrafProfPCPTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564TrafProfRowStatus}, GetNextIndexFsY1564TrafProfTable, FsY1564TrafProfRowStatusGet, FsY1564TrafProfRowStatusSet, FsY1564TrafProfRowStatusTest, FsY1564TrafProfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564TrafProfTableINDEX, 2, 0, 1, NULL},

{{13,FsY1564SacId}, GetNextIndexFsY1564SacTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacInfoRate}, GetNextIndexFsY1564SacTable, FsY1564SacInfoRateGet, FsY1564SacInfoRateSet, FsY1564SacInfoRateTest, FsY1564SacTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacFrLossRatio}, GetNextIndexFsY1564SacTable, FsY1564SacFrLossRatioGet, FsY1564SacFrLossRatioSet, FsY1564SacFrLossRatioTest, FsY1564SacTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacFrTransDelay}, GetNextIndexFsY1564SacTable, FsY1564SacFrTransDelayGet, FsY1564SacFrTransDelaySet, FsY1564SacFrTransDelayTest, FsY1564SacTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacFrDelayVar}, GetNextIndexFsY1564SacTable, FsY1564SacFrDelayVarGet, FsY1564SacFrDelayVarSet, FsY1564SacFrDelayVarTest, FsY1564SacTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacAvailability}, GetNextIndexFsY1564SacTable, FsY1564SacAvailabilityGet, FsY1564SacAvailabilitySet, FsY1564SacAvailabilityTest, FsY1564SacTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564SacRowStatus}, GetNextIndexFsY1564SacTable, FsY1564SacRowStatusGet, FsY1564SacRowStatusSet, FsY1564SacRowStatusTest, FsY1564SacTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564SacTableINDEX, 2, 0, 1, NULL},

{{13,FsY1564ServiceConfId}, GetNextIndexFsY1564ServiceConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfColorMode}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfColorModeGet, FsY1564ServiceConfColorModeSet, FsY1564ServiceConfColorModeTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, "2"},

{{13,FsY1564ServiceConfCoupFlag}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfCoupFlagGet, FsY1564ServiceConfCoupFlagSet, FsY1564ServiceConfCoupFlagTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfCIR}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfCIRGet, FsY1564ServiceConfCIRSet, FsY1564ServiceConfCIRTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfCBS}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfCBSGet, FsY1564ServiceConfCBSSet, FsY1564ServiceConfCBSTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfEIR}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfEIRGet, FsY1564ServiceConfEIRSet, FsY1564ServiceConfEIRTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfEBS}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfEBSGet, FsY1564ServiceConfEBSSet, FsY1564ServiceConfEBSTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564ServiceConfRowStatus}, GetNextIndexFsY1564ServiceConfTable, FsY1564ServiceConfRowStatusGet, FsY1564ServiceConfRowStatusSet, FsY1564ServiceConfRowStatusTest, FsY1564ServiceConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564ServiceConfTableINDEX, 2, 0, 1, NULL},

{{13,FsY1564ConfigTestReportSlaId}, GetNextIndexFsY1564ConfigTestReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrSize}, GetNextIndexFsY1564ConfigTestReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestCurrentTestMode}, GetNextIndexFsY1564ConfigTestReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportStepId}, GetNextIndexFsY1564ConfigTestReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportResult}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportIrMin}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportIrMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportIrMean}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportIrMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportIrMax}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportIrMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrLossCnt}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrLossCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrLossRatio}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrLossRatioGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrTxDelayMin}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrTxDelayMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrTxDelayMean}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrTxDelayMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrTxDelayMax}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrTxDelayMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrDelayVarMin}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrDelayVarMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrDelayVarMean}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrDelayVarMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportFrDelayVarMax}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportFrDelayVarMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportTestStartTime}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportTestStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564ConfigTestReportTestEndTime}, GetNextIndexFsY1564ConfigTestReportTable, FsY1564ConfigTestReportTestEndTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsY1564ConfigTestReportTableINDEX, 5, 0, 0, NULL},

{{13,FsY1564PerformanceTestIndex}, GetNextIndexFsY1564PerformanceTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsY1564PerformanceTestTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564PerformanceTestSlaList}, GetNextIndexFsY1564PerformanceTestTable, FsY1564PerformanceTestSlaListGet, FsY1564PerformanceTestSlaListSet, FsY1564PerformanceTestSlaListTest, FsY1564PerformanceTestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsY1564PerformanceTestTableINDEX, 2, 0, 0, NULL},

{{13,FsY1564PerformanceTestDuration}, GetNextIndexFsY1564PerformanceTestTable, FsY1564PerformanceTestDurationGet, FsY1564PerformanceTestDurationSet, FsY1564PerformanceTestDurationTest, FsY1564PerformanceTestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564PerformanceTestTableINDEX, 2, 0, 0, "2"},

{{13,FsY1564PerformanceTestStatus}, GetNextIndexFsY1564PerformanceTestTable, FsY1564PerformanceTestStatusGet, FsY1564PerformanceTestStatusSet, FsY1564PerformanceTestStatusTest, FsY1564PerformanceTestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564PerformanceTestTableINDEX, 2, 0, 0, "2"},

{{13,FsY1564PerformanceTestRowStatus}, GetNextIndexFsY1564PerformanceTestTable, FsY1564PerformanceTestRowStatusGet, FsY1564PerformanceTestRowStatusSet, FsY1564PerformanceTestRowStatusTest, FsY1564PerformanceTestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsY1564PerformanceTestTableINDEX, 2, 0, 1, NULL},

{{13,FsY1564PerfTestReportFrSize}, GetNextIndexFsY1564PerfTestReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportResult}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportIrMin}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportIrMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportIrMean}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportIrMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportIrMax}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportIrMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrLossCnt}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrLossCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrLossRatio}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrLossRatioGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrTxDelayMin}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrTxDelayMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrTxDelayMean}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrTxDelayMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrTxDelayMax}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrTxDelayMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrDelayVarMin}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrDelayVarMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrDelayVarMean}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrDelayVarMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportFrDelayVarMax}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportFrDelayVarMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportAvailability}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportAvailabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportUnavailableCount}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportUnavailableCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportTestStartTime}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportTestStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{13,FsY1564PerfTestReportTestEndTime}, GetNextIndexFsY1564PerfTestReportTable, FsY1564PerfTestReportTestEndTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsY1564PerfTestReportTableINDEX, 4, 0, 0, NULL},

{{12,FsY1564TrapSlaId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsY1564TypeOfFailure}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsy156Entry = { 94, fsy156MibEntry };

#endif /* _FSY156DB_H */

