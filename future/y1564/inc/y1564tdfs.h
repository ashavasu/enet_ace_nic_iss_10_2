/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: y1564tdfs.h,v 1.12 2016/03/01 10:04:54 siva Exp $
*
* Description: This file contains data structures defined for Y1564 module.
*********************************************************************/

#ifndef __Y1564TDFS_H__
#define __Y1564TDFS_H__

/* Request type for Y1564 exit Functions */
typedef enum
{
   Y1564_REQ_VCM_GET_CONTEXT_NAME,
   Y1564_REQ_VCM_IS_CONTEXT_VALID,
   Y1564_REQ_VCM_GET_SYSTEM_MODE,
   Y1564_REQ_L2IWF_GET_BRG_MODE,
   Y1564_REQ_CFM_REG_MEP_AND_FLT_NOTIFY,
   Y1564_REQ_CFM_DEREGISTER_MEP,
   Y1564_REQ_CFM_MEP_INDEX_VALIDATION,
   Y1564_REQ_SLA_CONF_TEST_START,
   Y1564_REQ_SLA_PERF_TEST_START,
   Y1564_REQ_CFM_GET_TEST_REPORT,
   Y1564_REQ_CFM_CONF_TEST_STOP_TEST,
   Y1564_REQ_CFM_PERF_TEST_STOP_TEST,
   Y1564_PORT_TYPE_MSG
}tExtReqType;

/* Structure used by Y1564 protocol for FsY1564ContextTable */
typedef struct  __sY1564ContextInfo
{

    UINT4               u4ContextId;
                            /* Virtual Context ID maintained by the VCM module */
    UINT4               u4TraceOption;
                            /* This variable will maintain a bitmap for trace
                             * options. If bit is set, this will represent
                             * option is enabled and if bit is not set then
                             * that particular trace option is disabled */
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
                            /* Context alias name */
    UINT1               u1ModuleStatus;
                            /* This variable maintains Y1564 module status of
                             * the context */
    UINT1               u1TrapStatus;
                            /* This variable maintains Trap enable/disable
                             * status of the Y1564 in the context */
    UINT1               u1ConfTestCount;
                            /* This variable maintains the number of 
                             * configuration tests are currently running
                             * in the context */
    UINT1               u1PerfTestCount;
                            /* This variable maintains the number of 
                             * performance tests are currently running
                             * in the context */
}tY1564ContextInfo;

/* Global information */
typedef struct _Y1564GlobalInfo {
    tOsixTaskId         TaskId; 
                            /* Y1564 Main Task Identifier */
    tOsixQId            MsgQId; 
                            /* Y1564 Main Task Queue Identifier for handling 
                             * the event messages */
    tOsixSemId          TaskSemId; 
                            /* Y1564 Main Task Semaphore Identifier */
    tTimerListId        Y1564TmrListId; 
                            /* Timer List to maintain timers */
    tTmrDesc            aTmrDesc[Y1564_MAX_TMR_TYPES]; 
                            /* Timer descriptor to hold  information about 
                             * the timer */ 
    tMemPoolId          TaskQMsgPoolId; 
                            /* MemPool Id to hold memory for Y1564 Task messages */
    tMemPoolId          SlaPoolId; 
                            /* MemPool Id to hold memory for SLA */
    tMemPoolId          TrafProfPoolId; 
                            /* MemPool Id to hold memory for Traffic profile */
    tMemPoolId          SacPoolId; 
                            /* MemPool Id to hold memory for SAC */
#ifndef MEF_WANTED
    tMemPoolId          ServiceConfPoolId;
                            /* MemPool Id to hold memory for Service Configuration
                             * (Bandwidth profile configuration)*/
#endif
    tMemPoolId          SlaConfTestReportPoolId;
                            /* MemPool Id to hold memory for Service Configuration 
                             * Test Report */
    tMemPoolId          SlaPerfTestReportPoolId;
                            /* MemPool Id to hold memory for Service Performance 
                             * Test Report */
    tMemPoolId          SlaPerfTestPoolId;
                            /* MemPool Id to hold memory for Service Performance Test
                             * parameter Table */
    tMemPoolId          SlaContextInfoPoolId;
                            /* MemPool Id to hold memory for ty1564ContextInfo*/
    tMemPoolId          Y1564ResultPoolId;
                            /* Mempool Id to fetch performance monitoring
                             * results from ECFM/Y1731 */
    tMemPoolId          Y1564ExtInParamsPoolId;
                           /* Mempool Id to hold External In params. This is
                            * used to interact with external modules*/
    tMemPoolId          Y1564ExtOutParamsPoolId;
                           /* Mempool Id to hold External Out params. This is
                            * used to interact with external modules*/
    tMemPoolId          Y1564ReqParamsPoolId;
                           /* Mempool Id to hold Request params. This is
                            * used when Y1564 module process the external 
                            * request received from external modules*/
    tMemPoolId           SlaListPoolId;
                           /* Mempool Id for SLA list */
    tRBTree             SlaTable; 
                            /* RBTree for maintaining SLA information */
    tRBTree             TrafProfTable; 
                            /* RBTree for maintaining Traffic Profile
                             * information (Information required to 
                             * test frames */
    tRBTree             SacTable; 
                            /* RBTree for maintaining SAC information */
#ifndef MEF_WANTED
    tRBTree             ServiceConfTable;
                            /* RBTree for maintaining Service Configuration
                             * related information */
#endif
    tRBTree             SlaConfTestReportTable;
                            /* RBTree for maintaining Service Configuration 
                             * Test Report */
    tRBTree             SlaPerfTestReportTable;
                            /* RBTree for maintaining Service Performance
                             * Test Report */
    tRBTree             SlaPerfTestTable;
                            /* RBTree for maintaining Service Performance
                             * Test information */
    tY1564ContextInfo   *apY1564ContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
                            /* This is an array of pointers of context
                             * information of Y1564. When context 
                             * create indication is received by Y1564 
                             * module memory will be allocated for the 
                             * context and context information will get 
                             * initialized with valid info.
                             * When the context delete is received by the
                             * Y1564, this context memory will be
                             * Released */
    UINT4               u4MemFailCount;
                            /* This variable will be incremented, when the
                             * memory allocation failed */
    UINT4               u4TimerFailCount;
                            /* This variable will be incremented, when timer
                             * related failure happens */
    UINT4               u4SysLogId;
                            /* This variable will be used to log the notification 
                             * in Syslog server */
    UINT1              au1SystemCtrl [SYS_DEF_MAX_NUM_CONTEXTS];
                            /* start or shutdown status of Y1564 
                             * per context */
}tY1564GlobalInfo;

#if 0
/*Y1564 Emix Packet size */
typedef struct _Y1564EmixString
{
    UINT1   *pu1_OctetList;
    INT4    i4_Length  ;
}tY1564EmixString;
#endif
/* Structure used by Y1564 protocol for FsY1564TrafProfTable */
typedef struct  __sTrafficProfileInfo
{
    tRBNodeEmbd         TrafProfNode; 
                            /* This field will contain RBTree node pointer
                             * for Traffic Profile indexed by Traffic Profile
                             * Identifier and Context Id */
    UINT4               u4TrafProfId; 
                            /* Traffic profile Id will be used by management
                             * entity to identify particular Traffic profile */
    UINT4               u4ContextId;
                            /* Thid field will have Context Id Configured for
                             * this Traffic Profile */
    UINT4               u4PortType;
    UINT4               u4IfIndex;
                            /* the value specified in ECFM configurations*/
    UINT4               u4TagType;
                             /* tag value specified in ECFM configurations*/
    UINT4               u4PortSpeed;
                            /* This field will have Port speed value*/
    UINT4               u4IfMtu;
                             /* This field will have MTU value of the port*/
    tMacAddr            SrcMacAddr;
                             /* Soucre Mac-Addr specified in ECFM configrations*/
    UINT2               u2OutVlanId;
                             /* Vlan ID specified in ECFM configurations*/
    tMacAddr            DstMacAddr;
                             /* Destination Mac-Addr specified in ECFM configrations*/
    UINT2               u2InVlanId;
                             /* Vlan ID specified in ECFM configurations*/
    eY1564Direction     TrafProfDirection; 
                            /* Direction (UNI or NNI side) in which test frames 
                             * to be sent */
    UINT1               au1TrafProfOptEmixPktSize[Y1564_EMIX_PKT_SIZE + 1]; 
                            /* specifies the optional variable-size pattern 
                             * (EMIX)in bytes for Y1564 Traffic Profile.
                             * Frame Sizes and corresponding size desiginations 
                             * are a - 64, b - 128, c - 256, d - 512,e - 1024, 
                             * f - 1280 ,g - 1518. Either of u2TrafProfPktSize or 
                             * TrafProfOptEmixPktSize can be used.*/
    UINT2               u2TrafProfPktSize; 
                            /* Frame Size of test frame to be sent while 
                             * for the outgoing packet */
    UINT1               au1TempTrafProfOptEmixPktSize[Y1564_EMIX_PKT_SIZE + 1];
                            /* This varaible holds the other then current EMIX
                             * pattern in bytes for Y1564 Traffic Profile.
                             * Frame Sizes and corresponding size desiginations
                             * are a - 64, b - 128, c - 256, d - 512,e - 1024,
                             * f - 1280 ,g - 1518. Either of u2TrafProfPktSize or
                             * TrafProfOptEmixPktSize can be used.*/
    UINT2               u2CurrentEmixPktSize;
                            /* Current Emix Frame Size of test frame to be sent,
                             * while for the outgoing packet */

    UINT1               au1TrafProfPayload[Y1564_PAYLOAD_SIZE + 1]; 
                            /* Payload information to be sent in the packet*/
    UINT1               u1EmixSelected;
                            /* This field will represent whether EMIX pattern
                             * is selected to perform the Configuration/
                             * Performance Test */
    UINT1               u1TrafProfPCP;
                            /* Specifies the Priority Code Point to be
                             * included in packet */
    UINT1               u1RowStatus; 
                            /* This field will represent the Row status of
                             * Traffic Profile Table */
    UINT1               u1LastFrSize;
                            /* This field will represent whether EMIX pattern
                             * reached end of the frame size or not*/
    UINT1               u1Reserved[3];
                            /* Padding */
}tTrafficProfileInfo;

/* Structure used by Y1564 protocol for FsY1564SacTable */
typedef struct  __sSacInfo
{
    tRBNodeEmbd         SacNode; 
                            /* This field will contain RBTree node pointer
                             * for SAC indexed by SAC Identifier
                             * and Context Id */
    UINT4               u4SacId; 
                            /* SAC Id will be used by management
                             * entity to identify particular SAC */
    UINT4               u4ContextId;
                                /* Thid field will have Context Id Configured for
                                 *  this SAC */
    UINT4               u4SacInfoRate; 
                            /* This field will store the minimum information rate 
                             * specified in Kbps that each services are expected 
                               to be met */
    UINT4               u4SacFrLossRatio; 
                            /* This field will store the minimum Frame Loss ratio in  
                             * percentage that each services are expected 
                               to be met */
    UINT4               u4SacFrTxDelay; 
                            /* This field will store the minimum Frame transfer 
                             * delay in milliseconds that each services are expected
                             * to be met */ 
    UINT4               u4SacFrDelayVar; 
                            /* This field will store the minimum  Frame delay 
                             * variation in milliseconds that each services are
                             * expected to be met */
    FLT4                f4SacAvailability;
                           /* This field will store the maximum Avialability
                            * in percentage that each services are
                            * expected to be met */
    UINT1               u1RowStatus; 
                            /* This field will represent the Row status of
                                SAC Table*/
    UINT1               au1Pad[3];
                            /* Padding */
}tSacInfo;


/* Maximum 10 SLA lists can be specified as part of 
 * service performance test. So MAX_Y1564_SLA_LIST_SIZE ((10 + 31)/32 * 2) 
 * is defined to have 16 bits */

#define   MAX_Y1564_SLA_LIST_SIZE  (((MAX_Y1564_SLA_LIST_INFO + 31)/32) * 2) 

/*Y1564 SLA List */
typedef UINT1 tY1564SlaList [MAX_Y1564_SLA_LIST_SIZE];


/* Structure used by Y1564 protocol for FsY1564PerformanceTestTable */
typedef struct  __sPerfTestInfo
{
    tRBNodeEmbd         Y1564PerfTestNode; 
                            /* This field will contain RBTree node pointer
                             * for Service performance test table indexed by
                             * perf test Identifier and Context Id */
    tY1564SlaList       *pau1SlaList;  
                            /* This field will store the list of SLA's
                             * participate in service performance test */
    UINT4               u4PerfTestId; 
                            /* Service performance test Id will be used by management
                             * entity to identify particular Service Conf
                             * Identifier */
    UINT4               u4ContextId;
                            /* Thid field will have Context Id Configured for
                             * the Service Performance Test Specific
                             * Configuration */
    tY1564SysTime       TestStartTimeStamp;
                            /* Indicates the performance
                             * test start time for the SLA */
    UINT1               au1TempSlaList[Y1564_MAX_SLA_LIST];
                            /* This variable used to validate the 
                             * SLA list for the given perf entry with other 
                             * perf entry before mapping to perf test
                             * entry*/
    UINT2               u2SlaListCount;
                            /* This field used to represent the bit postion set
                             * by SLAList */
    UINT1               u1PerfTestDuration; 
                            /* This field will store the duration to 
                             * complete the service performance test */  
    UINT1               u1PerfTestStatus; 
                            /* This field will indicate the service
                             * performance test initiation status
                             * (start / stop )*/ 
    UINT1               u1RowStatus;
                            /* This field will represent the Row status of
                             * Performance Test Table */
    UINT1               au1Pad[1];
                            /* Padding */
}tPerfTestInfo;

#ifndef MEF_WANTED
/* Structure used by Y1564 protocol for FsY1564ServiceConfTable */
typedef struct  __sServiceConfInfo
{
    tRBNodeEmbd         ServiceConfNode; 
                            /* This field will contain RBTree node pointer
                             * for Service Conf table indexed by Service
                             * Conf Identifier and Context Id */
    UINT4               u4ServiceConfId; 
                            /* Service Conf Id will be used by management
                             * entity to identify particular Service Conf
                             * Identifier */
    UINT4               u4ContextId;
                            /* Thid field will have Context Id Configured for
                             *  this Service Configuration */
    UINT4               u4ServiceConfCIR; 
                            /* This field will store the Committed Information
                             * Rate */
    UINT4               u4ServiceConfCBS; 
                            /* This field will store the Committed Burst 
                             * Size */  
    UINT4               u4ServiceConfEIR; 
                            /* This field will store the Excess Information 
                             * Rate that service */ 
    UINT4               u4ServiceConfEBS; 
                            /* This field will store the Excess Burst Size */
    UINT1               u1ServiceConfColorMode; 
                            /* This field will store the color Mode property */
    UINT1               u1ServiceConfCoupFlag;
                            /* This field will store the Coulpling Flag */
    UINT1               u1RowStatus;
                            /* This field will represent the Row status of
                             * ServiceConf Table */
    UINT1               au1Pad[1];
                            /* Padding */
}tServiceConfInfo;
#endif



/* Structure used by Y1564 protocol for FsY1564SlaTable */
typedef struct  __sSlaInfo
{
    tRBNodeEmbd         SlaNode; 
                            /* This field will contain RBTree node pointer
                             * for SLA table indexed by SLA Id and Context Id*/
    tTmrBlk             ConfTestTimer; 
                            /* Timer Block */
    tTmrBlk             PerfTestTimer; 
                            /* Timer Block */
    UINT4               u4SlaId;
                            /* SLA ID will be used by management entity
                             * to identify particular SLA */
    UINT4               u4ContextId;
                            /* Thid field will have Context Id Configured for
                             * this SLA */
    UINT4               u4SlaMegId; 
                            /* MEG information */
    UINT4               u4SlaMeId; 
                            /* ME information */
    UINT4               u4SlaMepId; 
                            /* MEP information */
    UINT4               u4SlaConfTestDuration; 
                            /* Duration of each indivial test selected in 
                             * configuration test */
    UINT4               u4SlaTrafProfId; 
                            /* Traffic Profile Id to which SLA is mapped */
    UINT4               u4SlaSacId; 
                            /* SAC Id to which SLA is mapped */
    UINT4               u4CIR; 
                            /* Update CIR using EVC Index */
    UINT4               u4CBS; 
                            /* Update CBS using EVC Index */
    UINT4               u4EIR; 
                            /* Update EIR using EVC Index */
    UINT4               u4EBS; 
                            /* Update EBS using EVC Index */
    UINT4               u4SlaPortStateCounter; 
                            /* Temp variable to take the number of
                             * port state toggle during a test, will be 
                             * copied to the SLA stats information once 
                             * the test is completed */

    UINT4               u4TransId;

    UINT4               u4SlaStepLoadDuration;
    
    UINT4               u4PerfId;
                             /* This variable will be updated when
                              * SLA is associated to Performance test 
                              * structure to perform the performance test*/

    tY1564SysTime       TestStartTimeStamp;
                            /* Indicates the configuration 
                             * test start time for the SLA */
    tY1564SysTime       TestEndTimeStamp;
                            /* Indicates the configuration 
                             * test completed time for the SLA */
    INT4                i4CurrentFrSize;

    UINT2               u2SlaRateStep; 
                            /* Indicates the number of steps to be done in
                             * step load CIR */
    UINT2               u2SlaCurrentRateStep; 
                            /* Indicates the current rate step selected in the
                             * * step load CIR */
    UINT2               u2EvcId; 
                            /* EVC index */
    UINT2               u2SlaCurrentStep; 
                            /* Indicates the number of rate steps 
                             * completed in Configuration test */
#ifndef MEF_WANTED
    UINT2               u2SlaServiceConfId;
                            /* Service Config Id to which SLA is mapped */
#endif
    UINT1               u1ColorMode;
                            /* Update ColorMode using EVC Index.
                             * Variable to identify the configuration
                             * test mode property (either color
                             * aware or color-blind property is employed
                             * with bandwidth profile) */
    UINT1               u1CoupFlag;
                            /* Update Coupling Flag using EVC index. 
                             * Varaiable to specify the modes of
                             * operation of the rate enforcement 
                             * algorithm. If the value is '0' then
                             * Yellow colored frames are bounded by EIR.
                             * If the value is '1' then Yellow 
                             * colored frames are bounded by EIR+CIR */
    UINT1               u1SlaConfTestStatus;
                            /* This field will indicate the service
                             * configuration test initiation status
                             * (start /stop) */
    UINT1               u1SlaTrafPolicing;
                            /* SLA Traffic Policing Status */
    UINT1               u1SlaConfTestSelector;
                            /* Indicates the test inputs selected to start 
                             * the Service Configuration test */
    UINT1               u1LastSlaConfTestSelector;
                            /* This varaible holds the value present in 
                             * u1SlaConfTestSelector and reset the individual
                             * test after selecting the test to be started 
                             * successfully. This variable will be useful to 
                             * select the next test, after the timer expiry of
                             * the first test only if the previously selected
                             * test result is PASS. */
    UINT1               u1SlaCurrentTestMode;
                            /* Indicates the currently running
                             * tests in service configuration test */
    UINT1               u1SlaCurrentTestState;
                            /* Indicates the Configuration test 
                             * status either completed/aborted/
                             * in-progress/not-initiated */
    UINT1               u1RowStatus; 
                            /* This field will represent the Row status of
                             * SLA Table*/
#ifndef MEF_WANTED
    UINT1               au1Pad [1];
#else
    UINT1               au1Pad [3];
#endif
}tSlaInfo;

/* Structure used by Y1564 protocol for FsY1564ConfTestReportTable */
typedef struct  __sConfTestReportInfo
{
    tRBNodeEmbd         Y1564ConfTestReportNode; 
                            /* This field will contain RBTree node pointer
                             * for configuration test report table indexed by
                             * Context Id, Sla Id, Current Test mode and 
                             * Step Id */
    UINT4               u4ContextId;
                            /* Context Id */
    UINT4               u4SlaId;
                            /* SLA Id */
    UINT4               u4CurrentTestMode;
                            /* Current Test Mode (CIR, StepLoad CIR, 
                             * EIR, Traffic Policing) */
    UINT4               u4StatsStepId; 
                            /* Step number */
    INT4                i4FrameSize;
                                /* Frame size */
    UINT4               u4StatsDuration; 
                            /* Duration of each step */
    UINT4               u4StatsTxPkts; 
                            /* Total Packets transmitted */
    FS_UINT8            u8StatsTxBytes; 
                            /* Total transmitted bytes */
    UINT4               u4StatsRxPkts; 
                            /* Total Packets received */
    FS_UINT8            u8StatsRxBytes; 
                            /* Total Bytes received */
    UINT4               u4StatsIrMin; 
                            /* Information rate min */
    UINT4               u4StatsIrMean; 
                            /* Information rate mean */
    UINT4               u4StatsIrMax; 
                            /* Informtion rate Max */
    UINT4               u4StatsFrLossCnt; 
                            /* Total Frame loss */
    UINT4               u4StatsFrLossRatio; 
                            /* Frame Loss ratio */
    UINT4               u4StatsFrTxDelayMin; 
                            /* Frame Tx Delay min */
    UINT4               u4StatsFrTxDelayMean; 
                            /* Frame Tx Delay mean */
    UINT4               u4StatsFrTxDelayMax; 
                            /* Frame Tx Delay max */
    UINT4               u4StatsFrDelayVarMin; 
                            /* Frame Delay  variation min */
    UINT4               u4StatsFrDelayVarMean; 
                            /* Frame Delay variation mean */
    UINT4               u4StatsFrDelayVarMax;  
                            /* Frame Delay variation max */
    UINT4               u4StatsPortStateCounter;
    UINT4               u4StatsYellowFrLossCnt;
                            /* Total Frame loss */
    UINT4               u4StatsYellowFrLossRatio;
                            /* Frame Loss ratio */
    UINT4               u4StatsYellowIrMin; 
                            /* Information rate min */
    UINT4               u4StatsYellowIrMean;
                            /* Information rate mean */
    UINT4               u4StatsYellowIrMax; 
                            /* Informtion rate Max */
    tY1564SysTime       TestStartTimeStamp;
                            /* Indicates the configuration 
                             * test start time for the SLA */
    tY1564SysTime       TestEndTimeStamp;
                            /* Indicates the configuration 
                             * test completed time for the SLA */
    UINT1               u1StatsResult; 
                            /* Test execution result */
    UINT1               u1StatsYellowResult;
                            /* Test execution result */
    UINT1               au1pad[2];
                            /* Padding */
}tConfTestReportInfo;

/* Structure used by Y1564 protocol for FsY1564PerfTestReportTable */
typedef struct  __sConfPerfReportInfo
{
    tRBNodeEmbd         Y1564PerfTestReportNode; 
                            /* This field will contain RBTree node pointer
                             * for configuration test report table indexed by
                             * Context Id, Sla Id,Performance Identifier */
    UINT4               u4ContextId;
                            /* Context Id */
    UINT4               u4SlaId;
                            /* SLA Id */
    UINT4               u4StatsPerfId; 
                            /* Perf number */
    INT4                i4FrameSize;
                                /* Frame size */
    UINT4               u4StatsDuration; 
                            /* Duration of each step */
    UINT4               u4StatsTxPkts; 
                            /* Total Packets transmitted */
    FS_UINT8            u8StatsTxBytes; 
                            /* Total transmitted bytes */
    UINT4               u4StatsRxPkts; 
                            /* Total Packets received */
    FS_UINT8            u8StatsRxBytes; 
                            /* Total Bytes received */
    UINT4               u4StatsIrMin; 
                            /* Information rate min */
    UINT4               u4StatsIrMean; 
                            /* Information rate mean */
    UINT4               u4StatsIrMax; 
                            /* Informtion rate Max */
    UINT4               u4StatsFrLossCnt; 
                            /* Total Frame loss */
    UINT4               u4StatsFrLossRatio; 
                            /* Frame Loss ratio */
    UINT4               u4StatsFrTxDelayMin; 
                            /* Frame Tx Delay min */
    UINT4               u4StatsFrTxDelayMean; 
                            /* Frame Tx Delay mean */
    UINT4               u4StatsFrTxDelayMax; 
                            /* Frame Tx Delay max */
    UINT4               u4StatsFrDelayVarMin; 
                            /* Frame Delay  variation min */
    UINT4               u4StatsFrDelayVarMean; 
                            /* Frame Delay variation mean */
    UINT4               u4StatsFrDelayVarMax;  
                            /* Frame Delay variation max */
    UINT4               u4StatsPortStateCounter;
    tY1564SysTime       PerfTestStartTimeStamp;
                            /* Indicates the performance
                             * test start time for the SLA */
    tY1564SysTime       PerfTestEndTimeStamp;
                            /* Indicates the performance 
                             * test completed time for the SLA */
    FLT4                f4StatsAvailability;
                            /* Indicates the service Availability percentage
                             * during the service performance test */
    UINT4               u4StatsUnAvailCount;
                            /* Indicates the unavailability count of 
                             * the service during the service performance 
                             * test */
    UINT1               u1StatsResult; 
                            /* Test execution result */
    UINT1               au1pad[3];
                            /* Padding */
}tPerfTestReportInfo;

typedef struct __Y1564RmMsg {
    tRmMsg               *pData;
                            /* RM message pointer */
    UINT2                 u2DataLen;
                            /* Length of RM message */
    UINT1                 u1Event;
                            /* RM event */
    UINT1                 au1Reserved[1];
}tY1564RmMsg;

/* Strcuture used by Y1564 protocol for Queues */
typedef struct  __sQueueTable
{
     tY1564ReqParams        Y1564ReqParam;
}tY1564QMsg;

typedef union __Y1564ExtOutParams
{
    UINT4              u4ContextId;
    UINT4              u4BrgMode;
    UINT1              au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1              u1TestStatus;
}tY1564ExtOutParams;

/* This Structure is the input for the Exit Function
 * (Function to interact with the external modules) */
typedef struct __Y1564ExtInParams
{
    tExtReqType      eExtReqType;
                            /* This variable denotes the
                             * type of the external request */
    tVcmRegInfo         VcmRegInfo; /* For VCM registration */
    tEcfmReqParams   Y1564EcfmReqParams;
    tEcfmRespParams  Y1564EcfmRespParams;

#define EcfmReqParams Y1564EcfmReqParams
#define EcfmRespParams Y1564EcfmRespParams
}tY1564ExtInParams;

#endif
