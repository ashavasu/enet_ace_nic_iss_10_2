/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564extn.h,v 1.2 2015/11/23 12:08:17 siva Exp $
 *
 * Description: This file contains the PUBLICs.
 *            
 *******************************************************************/
#ifndef __Y1564EXTN_H__
#define __Y1564EXTN_H__

#ifdef MEF_WANTED

INT4 MefUtilCheckEvcEntryPresence (UINT4 u4ContextId, UINT4 u4EvcId);

INT4 MefUtilGetBanwidthProfile (UINT4 u4ContextId, UINT4 u4EvcId,
                                UINT4 *pu4MefMeterCIR, UINT4 *pu4MefMeterCBS,
                                UINT4 *pu4MefMeterEIR, UINT4 *pu4MefMeterEBS,
                                UINT1 *pu1MefMeterColorMode,
                                UINT1 *pu1MefMeterType);
#endif

INT4 CliGetTftpParams (INT1 *pi1TftpStr, INT1 *pi1TftpFileName, 
                       tIPvXAddr *pIpAddress, UINT1 *pu1HostName);

INT4 IssCopyFileGeneric (tCliHandle, UINT1 *,
                         UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *,
                         UINT1 *, UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *);

INT4 CliSetIssErase (tCliHandle ,UINT4, UINT1 *);

INT2 cli_file_operation ( CONST CHR1 *, CONST CHR1 * );
#endif
