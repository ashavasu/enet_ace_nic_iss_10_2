/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564trc.h,v 1.2 2017/02/27 13:52:09 siva Exp $
 *
 * Description:This file contains procedures and definitions
 * used for debugging.
 ********************************************************************/

#ifndef __Y1564TRC_H__
#define __Y1564TRC_H__


#define  Y1564_SET_MODULE_TRACE(u4ContextId, u4TraceOption)\
    (gY1564GlobalInfo.apY1564ContextInfo[u4ContextId]->u4TraceOption = u4TraceOption)

#define Y1564_MODULE_TRACE(u4ContextId) \
     (gY1564GlobalInfo.apY1564ContextInfo[u4ContextId]->u4TraceOption)

#ifdef TRACE_WANTED

#define Y1564_MODULE_NAME        ((const char *)"[Y1564]")

#define   Y1564_CONTEXT_TRC Y1564UtilContextTrc

#define   Y1564_GLOBAL_TRC(args) \
          UtlTrcLog(Y1564_CRITICAL_TRC, Y1564_RESOURCE_TRC, Y1564_MODULE_NAME, args)
#else
#define   Y1564_CONTEXT_TRC(CtxId, Mask, ...) \
          {\
               UNUSED_PARAM(CtxId); \
          }
#define Y1564_GLOBAL_TRC(args) do {}while(0);
#endif /*TRACE_WANTED*/

#endif /*__Y1564TRC_H__*/
