/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: y1564prot.h,v 1.12 2016/03/01 10:04:54 siva Exp $
*
* Description: This file contains the function prototypes for
*              Y1564 module
*******************************************************************/

#ifndef __Y1564PROT_H__
#define __Y1564PROT_H__



/*****************************Y1564UtilCxt.c************************************/
PUBLIC tY1564ContextInfo * Y1564UtilCxtGetContextEntry (UINT4 u4ContextId);

PUBLIC INT4
Y1564VcmGetSystemMode (VOID);

PUBLIC INT4
Y1564VcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId, 
                                  UINT2 *pu2LocalPortId);
PUBLIC INT4
Y1564VcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId);

VOID
Y1564CxtHandleDeleteContext (UINT4 u4ContextId);

VOID
Y1564CxtHandleCreateContext (UINT4 u4ContextId);

PUBLIC INT4
Y1564CxtHandleUpdateCxtName (UINT4 u4ContextId);

PUBLIC tY1564ContextInfo * Y1564CxtMemInit(UINT4 u4ContextId);

PUBLIC tY1564ContextInfo *
Y1564CxtGetContextEntry (UINT4 u4ContextId);

PUBLIC INT4
Y1564VcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);
PUBLIC INT4
Y1564CxtStopCurrentlyRunningTest (UINT4 u4ContextId);

PUBLIC INT4
Y1564CxtIsY1564Started (UINT4 u4ContextId);

PUBLIC INT4
Y1564CxtModuleEnable (tY1564ContextInfo * pContextInfo);

PUBLIC INT4
Y1564CxtModuleDisable (tY1564ContextInfo * pContextInfo);

VOID
Y1564CxtModuleShutDown (UINT4 u4ContextId);

/*************************** y1564tmr.c ************************************/
INT4
Y1564TmrInit (VOID);
VOID
Y1564TmrDeInit (VOID);
PUBLIC VOID
Y1564TmrInitTimerDesc(VOID);
PUBLIC INT4
Y1564TmrStartTimer (tSlaInfo *pSlaEntry, UINT1 u1TimerType, UINT4 u4Duration);
PUBLIC INT4
Y1564TmrStopTimer (tSlaInfo *pSlaEntry, UINT1 u1TimerType);
VOID
Y1564TmrExpHandler (VOID);
INT4
Y1564TmrStartSlaTimer (tSlaInfo  * pSlaEntry);
INT4
Y1564TmrStopSlaTimer (tSlaInfo  * pSlaEntry);
VOID
Y1564TmrSlaTimerExp (VOID *pArg);
VOID
Y1564TmrPerfTestTimerExp (VOID *pArg);
VOID
Y1564TmrConfTestTimerExp (VOID *pArg);

/*************************** y1564npwr.c ****************************************/
INT4
Y1564UtilNpWrStartSlaTest (tSlaInfo *pSlaEntry,
                           tTrafficProfileInfo *pTrafProfEntry);

INT4
Y1564UtilNpWrStopSlaTest (tSlaInfo *pSlaEntry);

INT4
Y1564UtilNpWrFetchSlaReport (tSlaInfo *pSlaEntry);

/**************************** y1564core.c **************************************/

INT4
Y1564CoreStartPerfTest (tPerfTestInfo * pPerfTestEntry);
INT4
Y1564CoreStopPerfTest (tPerfTestInfo * pPerfEntry);

INT4
Y1564CoreStartConfTest (tSlaInfo * pSlaEntry);
INT4
Y1564CoreStopConfTest (tSlaInfo * pSlaEntry);

PUBLIC INT4
Y1564CoreProcessConfTestResult (tSlaInfo *pSlaEntry, tY1564QMsg * pMsg);

PUBLIC INT4
Y1564CoreProcessPerfTestResult (tSlaInfo *pSlaEntry, tY1564QMsg * pMsg);

INT4
Y1564CoreValidateSlaPerfTest (tSlaInfo *pSlaEntry, 
                              tTrafficProfileInfo *pTrafProfEntry,
                              tPerfTestInfo *pPerfTestEntry,
                              UINT4  * u4Duration);

PUBLIC tConfTestReportInfo *
Y1564CoreGetConfTestReport (tSlaInfo *pSlaEntry, tTrafficProfileInfo *pTrafProfEntry);

/*************************** y1564msg.c ****************************************/
INT4
Y1564MsgEnque (tY1564QMsg * pMsg);
VOID
Y1564MsgQueueHandler (VOID);
VOID
Y1564HandlePortDownEvent (UINT4 u4IfIndex);

/*************************** y1564util.c ***************************************/

PUBLIC INT4
Y1564UtilIsY1564Started (UINT4 u4ContextId);

INT4 Y1564UtilGetEvcEntry (UINT4 u4ContextId, UINT4 u4EvcIndex);

INT4 Y1564UtilGetBandwidthProfile (tSlaInfo *pSlaEntry, UINT4 u4ContextId, 
                                   UINT4 u4EvcIndex);

PUBLIC VOID
Y1564PortGetEmixPktSizeAndTimerCount (tTrafficProfileInfo *pTrafficProfileInfo,
                                      UINT1 *pu1Count);
PUBLIC INT4
Y1564PortGetTestSelector (tSlaInfo * pSlaEntry, UINT1 *pu1TstSelector);

PUBLIC INT1
Y1564UtilGetSlaTestParams (tSlaInfo * pSlaEntry);

PUBLIC
INT4 Y1564UtilValidateSlaCFMParams (tSlaInfo * pSlaEntry);

/******** SLA TABLE **********/

INT4 Y1564UtilSlaTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreateSlaTable (VOID);

PUBLIC tSlaInfo *
Y1564UtilSlaCreateSlaNode (VOID);

PUBLIC INT4
Y1564UtilSlaAddNodeToSlaRBTree (tSlaInfo * pSlaEntry);

PUBLIC tSlaInfo  *
Y1564UtilSlaGetNodeFromSlaTable (tSlaInfo * pSlaEntry);

PUBLIC INT4
Y1564UtilSlaDelNodeFromRBTree (tSlaInfo * pSlaEntry);

PUBLIC tSlaInfo *
Y1564UtilSlaGetSlaEntry (UINT4 u4ContextId, UINT4 u4SlaId);

PUBLIC tSlaInfo  *
Y1564UtilSlaGetFirstNodeFromSlaTable (VOID);

PUBLIC tSlaInfo  *
Y1564UtilSlaGetNextNodeFromSlaTable (tSlaInfo * pCurrentSlaEntry);

VOID
Y1564UtilDeleteSlaTableInContext (UINT4 u4ContextId);

INT4
Y1564UtilDelSlaEntry (tSlaInfo * pSlaEntry);

VOID
Y1564UtilContextTrc (UINT4 u4ContextId,UINT4 u4Mask,const char *fmt, ...);

/******** Traffic Profile TABLE **********/

INT4 Y1564UtilTrafProfTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreateTrafProfTable (VOID);

PUBLIC tTrafficProfileInfo * Y1564UtilCreateTrafProfNode (VOID);

PUBLIC INT4
Y1564UtilAddNodeToTrafProfRBTree (tTrafficProfileInfo *pTrafProfEntry);

PUBLIC  tTrafficProfileInfo *
Y1564UtilGetNodeFromTrafProfTable (tTrafficProfileInfo * pTrafProfEntry);

PUBLIC INT4
Y1564UtilTrafProfDelNodeFromRBTree (tTrafficProfileInfo * pTrafProfEntry);

PUBLIC tTrafficProfileInfo *
Y1564UtilGetTrafProfEntry (UINT4 u4ContextId, UINT4 u4TrafProfId);

PUBLIC tTrafficProfileInfo  *
Y1564UtilGetFirstNodeFromTrafProfTable (VOID);

PUBLIC tTrafficProfileInfo  *
Y1564UtilGetNextNodeFromTrafProfTable (tTrafficProfileInfo * pCurrentTrafProfEntry);

VOID
Y1564UtilDeleteTrafProfTableInContext (UINT4 u4ContextId);

INT4
Y1564UtilDelTrafProfEntry (tTrafficProfileInfo * pTrafProfEntry);

INT4
Y1564UtilValidateSlaTest (tSlaInfo *pSlaEntry, tTrafficProfileInfo *pTrafProfEntry,
                          UINT4  * u4Duration);

/******** SAC TABLE **********/

INT4 Y1564UtilSacTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreateSacTable (VOID);

PUBLIC tSacInfo * Y1564UtilSacCreateSacNode (VOID);

PUBLIC INT4
Y1564UtilSacAddNodeToSacRBTree (tSacInfo * pSacEntry);

PUBLIC tSacInfo  *
Y1564UtilSacGetNodeFromSacTable (tSacInfo * pSacEntry);

PUBLIC INT4
Y1564UtilSacDelNodeFromRBTree (tSacInfo * pSacEntry);

tSacInfo *
Y1564UtilSacGetSacEntry (UINT4 u4ContextId, UINT4 u4SacId);

PUBLIC tSacInfo  *
Y1564UtilSacGetFirstNodeFromSacTable (VOID);


PUBLIC tSacInfo  *
Y1564UtilSacGetNextNodeFromSacTable (tSacInfo * pCurrentSacEntry);

VOID
Y1564UtilDeleteSacTableInContext (UINT4 u4ContextId);

INT4
Y1564UtilDelSacEntry (tSacInfo * pSacEntry);


/******** SERVICE CONF TABLE **********/
#ifndef MEF_WANTED
INT4
Y1564UtilServConfTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreateServConfTable (VOID);

PUBLIC tServiceConfInfo * Y1564UtilCreateServConfNode (VOID);

PUBLIC INT4
Y1564UtilAddNodeToServConfRBTree (tServiceConfInfo * pServiceConfEntry);

PUBLIC tServiceConfInfo  *
Y1564UtilGetNodeFromServConfTable (tServiceConfInfo * pServiceConfEntry);

PUBLIC INT4
Y1564UtilServConfDelNodeFromRBTree (tServiceConfInfo * pServiceConfEntry);

PUBLIC tServiceConfInfo *
Y1564UtilGetServConfEntry (UINT4 u4ContextId, UINT4 u4ServiceConfId);

PUBLIC tServiceConfInfo  *
Y1564UtilGetFirstNodeFromServConfTable (VOID);

PUBLIC tServiceConfInfo  *
Y1564UtilGetNextNodeFromServConfTable (tServiceConfInfo * 
                                       pCurrentServiceConfEntry);

VOID
Y1564UtilDeleteServConfTableInContext (UINT4 u4ContextId);

INT4
Y1564UtilDelServConfEntry (tServiceConfInfo * pServConfEntry);

PUBLIC INT4
Y1564UtilValidateServConfAndSla (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4ServConfId);

#endif
/******** PERFORMANCE TEST TABLE **********/

INT4 Y1564UtilPerfTestTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreatePerfTestTable (VOID);

PUBLIC tPerfTestInfo * Y1564UtilCreatePerfTestNode (VOID);

PUBLIC INT4
Y1564UtilAddNodeToPerfTestRBTree (tPerfTestInfo * pPerfTestEntry);

PUBLIC tPerfTestInfo  *
Y1564UtilGetNodeFromPerfTestTable (tPerfTestInfo * pPerfTestEntry);

PUBLIC INT4
Y1564UtilPerfTestDelNodeFromRBTree (tPerfTestInfo * pPerfTestEntry);

tPerfTestInfo *
Y1564UtilGetPerfTestEntry (UINT4 u4ContextId, UINT4 u4PerfTestId);

PUBLIC tPerfTestInfo  *
Y1564UtilGetFirstNodeFromPerfTestTable (VOID);

PUBLIC tPerfTestInfo  *
Y1564UtilGetNextNodeFromPerfTestTable (tPerfTestInfo * pCurrentPerfTestEntry);

PUBLIC INT4
Y1564UtilValidateSlaTestState (tPerfTestInfo * pPerfTestEntry);

VOID
Y1564UtilDeletePerfTestTableInContext (UINT4 u4ContextId);

INT4
Y1564UtilDelPerfTestEntry (tPerfTestInfo * pPerfTestEntry);

/******** CONFIGURATION TEST REPORT TABLE **********/

INT4 Y1564UtilConfTestReportTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreateConfTestReportTable (VOID);

PUBLIC tConfTestReportInfo * Y1564UtilCreateConfTestReportNode (VOID);

PUBLIC INT4
Y1564UtilAddNodeToConfTestReportRBTree (tConfTestReportInfo * pConfTestReportEntry);

PUBLIC tConfTestReportInfo *
Y1564UtilGetConfTestReportEntry (UINT4 u4ContextId, UINT4 u4SlaId,
                                 INT4 i4FrameSize,UINT4 u4CurrentTestMode,
                                 UINT4 u4StatsStepId);

PUBLIC tConfTestReportInfo  *
Y1564UtilGetNodeFromConfTestReportTable (tConfTestReportInfo *
                                         pConfTestReportEntry);

PUBLIC tConfTestReportInfo  *
Y1564UtilGetFirstNodeFromConfTestReportTable (VOID);

PUBLIC tConfTestReportInfo  *
Y1564UtilGetNextNodeFromConfTestReportTable (tConfTestReportInfo *
                                             pCurrentConfTestReportEntry);

PUBLIC INT4
Y1564UtilConfTestReportDelNodeFromRBTree (tConfTestReportInfo * pConfTestReportEntry);


VOID
Y1564UtilDelConfTestResultEntry (UINT4 u4ContextId, UINT4 u4SlaId);


/******** PERFORMANCE TEST REPORT TABLE **********/

INT4 Y1564UtilPerfTestReportTableCmpFn (tRBElem * Node, tRBElem * NodeIn);

INT4 Y1564UtilCreatePerfTestReportTable (VOID);

PUBLIC tPerfTestReportInfo * Y1564UtilCreatePerfTestReportNode (VOID);

PUBLIC INT4
Y1564UtilAddNodeToPerfTestReportRBTree (tPerfTestReportInfo * pPerfTestReportEntry);

PUBLIC tPerfTestReportInfo *
Y1564UtilGetPerfTestReportEntry (UINT4 u4ContextId, UINT4 u4SlaId,
                                 UINT4 u4StatsPerfId,
                                 INT4 i4FrameSize);

PUBLIC tPerfTestReportInfo  *
Y1564UtilGetNodeFromPerfTestReportTable (tPerfTestReportInfo *
                                         pPerfTestReportEntry);

PUBLIC tPerfTestReportInfo  *
Y1564UtilGetFirstNodeFromPerfTestReportTable (VOID);

PUBLIC tPerfTestReportInfo  *
Y1564UtilGetNextNodeFromPerfTestReportTable (tPerfTestReportInfo *
                                             pCurrentPerfTestReportEntry);

PUBLIC INT4
Y1564UtilPerfTestReportDelNodeFromRBTree (tPerfTestReportInfo * pPerfTestReportEntry);

PUBLIC INT4
Y1564UtilFetchReport (tSlaInfo * pSlaEntry);

PUBLIC INT4
Y1564UtilValidateSacAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                              UINT4 u4SacId);

PUBLIC INT4
Y1564UtilValidateTrafProfAndSla (UINT4 u4ContextId, UINT4 u4SlaId,
                                   UINT4 u4TrafProfId);

PUBLIC INT4
Y1564UtilPortType(tTrafficProfileInfo *pTrafProfEntry);

VOID
Y1564UtilDelPerfTestResultEntry (UINT4 u4ContextId, UINT4 u4PerfTestId,
                                 UINT4 u4SlaId);

/*************************** Y1564trap.c ***************************************/
VOID
Y1564SendTrapNotifications (tSlaInfo  *pSlaEntry, UINT1 u1TrapEvent);
/*************************** y1564port.c ***************************************/

PUBLIC INT4 
Y1564PortRmRegisterProtocols (tRmRegParams * pRmReg);
PUBLIC INT4
Y1564PortRmDeRegisterProtocols (VOID);

PUBLIC INT4
Y1564PortMepGetPerformanceResult(tSlaInfo * pSlaEntry);

PUBLIC INT4
Y1564PortSlaStartConfTest (tSlaInfo *pSlaEntry,
                           tTrafficProfileInfo *pTrafProfEntry,
                           UINT4 u4Duration);
PUBLIC INT4
Y1564PortSlaStartPerfTest (tSlaInfo *pSlaEntry,
                           tTrafficProfileInfo *pTrafProfEntry,
                           UINT4 u4Duration, UINT4 u4PerfTestId);
PUBLIC INT4
Y1564PortSlaStopConfTest (tSlaInfo *pSlaEntry);

PUBLIC INT4
Y1564L2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);

PUBLIC VOID
Y1564PortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                         UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                         UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName);
PUBLIC INT4
Y1564PortMepRegAndGetFltState (tSlaInfo *pSlaEntry);

PUBLIC INT4
Y1564PortMepDeRegister (UINT4 u4ContextId, UINT4 u4SlaMegId, UINT4 u4SlaMeId,
                        UINT4 u4SlaMepId);

PUBLIC INT4
Y1564PortEcfmMepDeRegister (tEcfmRegParams * pEcfmReg, 
                            tEcfmEventNotification *pMepInfo);

PUBLIC INT4
Y1564PortValMepIndex (tSlaInfo *pSlaEntry);

PUBLIC INT4
Y1564PortMepRegAndGet(tSlaInfo *pSlaEntry);

INT4
Y1564PortHandleExtInteraction (tY1564ExtInParams * pY1564ExtInParams,
                               tY1564ExtOutParams * pY1564ExtOutParams);

PUBLIC VOID 
Y1564PortExternalEventNotify (tEcfmEventNotification * pEvent);

PUBLIC UINT4
Y1564PortRmGetNodeState (VOID);

PUBLIC INT4
Y1564PortGetTestRate(tSlaInfo * pSlaEntry, tY1564ExtInParams * pY1564ExtInParams);

/********************* Y1564api.c ******************/

/*************** END **************/
#endif
