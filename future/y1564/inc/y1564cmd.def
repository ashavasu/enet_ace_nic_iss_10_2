/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564cmd.def,v 1.14 2016/05/06 10:11:56 siva Exp $
 *
 * Description: This file contains Y1564 CLI configuration commands.
 *
 *******************************************************************/

DEFINE GROUP : Y1564_CXT_GBL_CMDS

   COMMAND  : shutdown y1564
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SHUTDOWN, NULL);
   SYNTAX   : shutdown y1564 
   PRVID    : 15
   HELP     : Shuts down Y1564 module in the context.
   CXT_HELP : shutdown Shuts down the feature | 
              y1564 Y1564 related configuration | 
              <CR> Shuts down Y1564 module in the context.

   COMMAND  : no shutdown y1564
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_START, NULL);
   SYNTAX   : no shutdown y1564
   PRVID    : 15
   HELP     : Starts Y1564 module in the context.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              shutdown Shuts down the feature | 
              y1564 Y1564 related configuration | 
              <CR> Starts Y1564 module in the context.

   COMMAND  : y1564 enable
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_ENABLE, NULL);
   SYNTAX   : y1564 enable
   PRVID    : 15
   HELP     : Enable the Operation State of Y1564 module.
   CXT_HELP : y1564 Y1564 related configuration |
              enable Enable the Operation State of the feature | 
              <CR> Enable the Operation State of Y1564 module.

   COMMAND  : no y1564 enable
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_DISABLE, NULL);
   SYNTAX   : no y1564 enable
   PRVID    : 15
   HELP     : Disable the Operation State of Y1564 module.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
              y1564 Y1564 related configuration |
              enable Enable the Operation State of the feature | 
              <CR> Disable the Operation State of Y1564 module.

   COMMAND  : y1564 sla <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_CREATE, NULL, *(UINT4*)$2);
   SYNTAX   : y1564 sla <sla-id (1-10)>
   PRVID    : 15
   HELP     : Creates a Service level Agreement in Y1564 with default values and enters into SLA mode.
              
   CXT_HELP : y1564 Configures Y1564 related information | 
              sla SLA related configuration | 
              (1-10) SLA Identifier | 
              <CR> Creates a Service level Agreement in Y1564 with default values and enters into SLA mode.
              
   COMMAND  : no y1564 sla <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_DELETE, NULL, *(UINT4*)$3);
   SYNTAX   : no y1564 sla <sla-id (1-10)>
   PRVID    : 15
   HELP     : Deletes a Service level Agreement in active state from Y1564.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
              y1564 Y1564 related configuration | 
              sla SLA related configuration | 
              (1-10) SLA Identifier | 
              <CR> Deletes a Service level Agreement in active state from Y1564.

   COMMAND  : y1564 traffic-profile <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_CREATE, NULL, *(UINT4*)$2);
   SYNTAX   : y1564 traffic-profile <traffic-profile-id (1-10)>
   PRVID    : 15
   HELP     : Creates a traffic profile.
   CXT_HELP : y1564 Configures Y1564 related information | 
              traffic-profile Traffic profile related configuration | 
              (1-10) Traffic profile identifier | 
              <CR> Creates a traffic profile.

   COMMAND  : no y1564 traffic-profile <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_DELETE, NULL, *(UINT4*)$3);
   SYNTAX   : no y1564 traffic-profile <traffic-profile-id (1-10)>
   PRVID    : 15
   HELP     : Deletes the traffic profile. 
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
              y1564 Y1564 related configuration | 
              traffic-profile Traffic profile related configuration | 
              (1-10) Traffic profile identifier |
              <CR> Deletes the traffic profile. 

#ifndef MEF_WANTED
   COMMAND  : y1564 service-configuration <integer (1-10)> 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SERVICE_CREATE, NULL, *(UINT4*)$2);
   SYNTAX   : y1564 service-configuration <service-id (1-10)>
   PRVID    : 15
   HELP     : Creates a Service Configuration, to configure the Bandwidth Profile Information.
   CXT_HELP : y1564 Configures Y1564 related information | 
              service-configuration Service Configuration related configuration | 
              (1-10) Service Configuration Identifier | 
              <CR> Creates  Bandwidth Profile Information.

   COMMAND  : no y1564 service-configuration <integer (1-10)> 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SERVICE_DELETE, NULL, *(UINT4*)$3);
   SYNTAX   : no y1564 service-configuration <service-id (1-10)>
   PRVID    : 15
   HELP     : Deletes a Service Configuration, which configured the Bandwidth Profile Information.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
              y1564 Y1564 related configuration | 
              service-configuration Service Configuration related configuration | 
              <service-id(1-10)> Service Configuration Identifier | 
              <CR> Deletes a Service  the Bandwidth Profile Information.
#endif

   COMMAND  : y1564 sac <integer (1-10)> information-rate <integer (1-100)> frame-loss-ratio <integer (0-100)>
              frame-time-delay <integer (0-2147483647)> frame-delay-variation <integer (0-2147483647)> availability <string(6)>
   ACTION   : {
                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_SAC_CREATE, NULL, 
                                         *(UINT4*)$2, *(UINT4*)$4, *(UINT4*)$6, *(UINT4*)$8, *(UINT4*)$10, $12);
              }
   SYNTAX   : y1564 sac <sac-id (1-10)> information-rate <1-100> frame-loss-ratio <loss-ratio (0-100)>
              frame-time-delay <frame-delay-millisec (0-2147483647)> frame-delay-variation <frame-delay-var-milliseconds (0-2147483647)> availability <availability-string(6)>
   PRVID    : 15
   HELP     : Create and configure Service Acceptance Criteria.
   CXT_HELP : y1564 Configures Y1564 related information |
              sac Service Acceptance Criteria related configuration |
              (1-10) SAC identifier | 
              information-rate Information rate related configuration |
              (1-100) Information rate value in percentage | 
              frame-loss-ratio Frame loss ratio related configuration |
              (0-100) Frame loss Ratio value in percentage |
              frame-time-delay Frame transfer delay related configuration |
              (0-2147483647) Frame transfer delay value in milli seconds |
              frame-delay-variation Frame delay variation related configuration |
              (0-2147483647) Frame delay variation value in milli seconds |
              availability Availability related configuration |
              <string(6)> Availability Ratio Value in percentage |
              <CR> Create and configure Service Acceptance Criteria.

   COMMAND  : no y1564 sac <integer (1-10)> 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SAC_DELETE, NULL, *(UINT4*)$3);
   SYNTAX   : no y1564 sac <sac-id (1-10)>
   PRVID    : 15
   HELP     : Deletes the Service Acceptance Criteria.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              y1564 Y1564 related configuration |
              sac Service Acceptance Criteria related configuration |
              (1-10) SAC identifier | 
              <CR> Deletes the Service Acceptance Criteria.

   COMMAND  : y1564 notification enable
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NOTIFY_ENABLE, NULL);
   SYNTAX   : y1564 notification enable
   PRVID    : 15
   HELP     : Enables the Y1564 trap notification.
   CXT_HELP : y1564 Y1564 related configuration |
	           notification Notification message related information |
              enable Enables the Y1564 trap notification messages |
              <CR> Enables the Y1564 trap notification.

   COMMAND  : no y1564 notification enable
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NOTIFY_DISABLE, NULL);
   SYNTAX   : no y1564 notification enable
   PRVID    : 15
   HELP     : Disables the sending notification message from Y1564 to a remote management entity upon specific events.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default values |
	           y1564 Y1564 related configuration |
	           notification Notification message related information |
              enable Enables the Y1564 trap notification messages|
              <CR> Disables the notification message from Y1564 to a remote management entity upon specific events.

END GROUP


DEFINE GROUP : Y1564_GBLCFG_CMDS
  
   COMMAND  :  y1564 service-configuration-test sla <integer (1-10)> {start | stop} [switch <string>]
   ACTION   : {
                  UINT4 u4Type = Y1564_ZERO;

                  if ($4 != NULL)
                  {
                      u4Type = Y1564_CLI_START;
                  }
                  else if ($5 != NULL)
                  {
                      u4Type = Y1564_CLI_STOP;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_CONFIGURATION_TEST, NULL, 
                                         $7, *(UINT4*)$3, u4Type);

              }
   SYNTAX   : y1564 service-configuration-test sla <sla-id (1-10)> {start | stop} [switch <context_name>]
   PRVID    : 15
   HELP     : Starts/Stops the service configuration test of a SLA. If switch is not defined it will take default switch.
   CXT_HELP : y1564 Configures Y1564 related information | 
              service-configuration-test Service configuration test related configuration |
              sla SLA related configuration |
              (1-10) SLA Identifier | 
              start Start the test |
              stop Forcefully stop the test |
              switch Virtual context or a component|
              <string(32)> Context name|              
              <CR> Starts/Stops the service configuration test of a SLA. If switch is not defined it will take default switch.

   COMMAND  : no y1564 service-performance-test <integer> [switch <string>]
   ACTION   : {
                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_PERFORMANCE_SLA_LIST_DELETE, NULL,
                                         $5, *(UINT4*)$3);
              }

   SYNTAX   : no y1564 service-performance-test <performance-id (1-10)> [switch <context_name>]
   PRVID    : 15
   HELP     : Deletes a Service Performance Table in active state from Y1564. If switch is not defined it will take default switch.
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
              y1564 Configures Y1564 related information |
              service-performance-test Service performance test related configuration |
              (1-10) Performance Identifier |
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Deletes a Service Performance Table in active state from Y1564. If switch is not defined it will take default switch.

   COMMAND  : y1564 service-performance-test <integer> sla <string> [switch <string>]
   ACTION   : {
                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_PERFORMANCE_SLA_LIST, NULL,
                                         $6, *(UINT4*)$2, $4);
              }

   SYNTAX   : y1564 service-performance-test <performance-id (1-10)> sla <sla-list> [switch <context_name>]
   PRVID    : 15
   HELP     : Starts/Stops the service performance test of a SLA. If switch is not defined it will take default switch.
   CXT_HELP : y1564 Configures Y1564 related information |
              service-performance-test Service performance test related configuration |
              (1-10) Performance Identifier |
              sla SLA related configuration |
              <sla-list> List of SLAs seperated by a comma(Example : 1,2,4,7) |
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Starts/Stops the service performance test of a SLA. If switch is not defined it will take default switch.

   COMMAND  : y1564 service-performance-test <integer> {start | stop} [switch <string>]
   ACTION   : {
                  UINT4 u4Type = 0;

                  if ($3 != NULL)
                  {
                      u4Type = Y1564_CLI_START;
                  }
                  else if ($4 != NULL)
                  {
                      u4Type = Y1564_CLI_STOP;
                  }
                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_PERFORMANCE_TEST, NULL,
                                         $6, *(UINT4*)$2, u4Type);
              }

   SYNTAX   : y1564 service-performance-test <performance-id (1-10)> {start | stop} [switch <context_name>]
   PRVID    : 15
   HELP     : Starts/Stops the service performance test of a SLA. If switch is not defined it will take default switch.
   CXT_HELP : y1564 Configures Y1564 related information |
              service-performance-test Service performance test related configuration |
              (1-10) Performance Identifier |
              start Start the test |
              stop Forcefully stop the test |
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Starts/Stops the service performance test of a SLA. If switch is not defined it will take default switch.
   COMMAND  : y1564 service-performance-test <integer> duration [{fifteen-min | two-hour | twenty-four-hour}] [switch <string>]
   ACTION   : {
                  UINT4 u4PerfDuration = Y1564_PERF_TEST_DEF_DURATION;

                  if ($4 != NULL)                                                                                                                             {
                      u4PerfDuration = Y1564_PERF_TEST_MIN_DURATION;
                  }                                                                                                                            
                  if ($5 != NULL)
                  {
                      u4PerfDuration = Y1564_PERF_TEST_DEF_DURATION;
                  }

                  if ($6 != NULL)
                  {
                      u4PerfDuration = Y1564_PERF_TEST_MAX_DURATION;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_PERFORMANCE_DURATION, NULL, $8,*(UINT4*)$2, u4PerfDuration);

              }
   SYNTAX   : y1564 service-performance-test <performance-id (1-10)> duration [{fifteen-min | two-hour | twenty-four-hour}] [switch <context_name>]
   PRVID    : 15
   HELP     : Configures the service performance test duration.
   CXT_HELP : y1564 Configures Y1564 related information |
              service-performance-test Service performance test related configuration |
              (1-10) Performance Identifier |
              duration service performance test duration related configuration |
              fifteen-min 15 MIN Duration for performance test |
              two-hour 2 HOUR Duration for performance test |
              twenty-four-hour 24 HOUR Duration for performance test |
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Configures the service Performance test duration.

END GROUP

DEFINE GROUP : Y1564_DEBUG_CMDS
#ifdef TRACE_WANTED
   COMMAND  : debug y1564 {[init-shut] [mgmt] [ctrl] [resource] [timer] [critical] 
              [y1731] [test] [session-record] [fail] | all} [{ <short (0-7)> | alerts | critical | 
              debugging | emergencies | errors | informational | notification | warnings }]
              [switch <string>]
   ACTION   : {
                  UINT4 u4TrcValue  = DEBUG_DEF_LVL_FLAG; 
                  INT4  i4DebugType = Y1564_CRITICAL_TRC;
 
                  if ($2 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_INIT_SHUT_TRC;
                  }
                  if ($3 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_MGMT_TRC;
                  }
                  if ($4 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_CTRL_TRC;
                  }
                  if ($5 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_RESOURCE_TRC;
                  }
                  if ($6 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_TIMER_TRC;
                  }
                  if ($7 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_CRITICAL_TRC;
                  }
                  if ($8 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_Y1731_TRC;
                  }
                  if ($9 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_TEST_TRC;
                  }
                  if ($10 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_SESSION_TRC;
                  }
                  if ($11 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_ALL_FAILURE_TRC;
                  }
                  if ($12 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_ALL_TRC;
                  }
                  if ($13 != NULL)
                  {
                      u4TrcValue = *(UINT4 *) $12;
                  }
                  else if ($14 != NULL)
                  {
                      u4TrcValue = DEBUG_ALERT_LEVEL;
                  }
                  else if ($15 != NULL)
                  {
                      u4TrcValue = DEBUG_CRITICAL_LEVEL;
                  }
                  else if ($16 != NULL)
                  {
                      u4TrcValue = DEBUG_DEBUG_LEVEL;
                  }
                  else if ($17 != NULL)
                  {
                      u4TrcValue = DEBUG_EMERG_LEVEL;
                  }
                  else if ($18 != NULL)
                  {
                      u4TrcValue = DEBUG_ERROR_LEVEL;
                  }
                  else if ($19 != NULL)
                  {
                      u4TrcValue = DEBUG_INFO_LEVEL;
                  }
                  else if ($20 != NULL)
                  {
                      u4TrcValue = DEBUG_NOTICE_LEVEL;
                  }
                  else if ($21 != NULL)
                  {
                      u4TrcValue = DEBUG_WARN_LEVEL;
                  }
 
                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_DEBUG,
                                         NULL, $23, i4DebugType, u4TrcValue);

             }
   SYNTAX   : debug y1564 {[init-shut] [mgmt] [ctrl] [resource] [timer] [critical]
              [y1731] [test] [session-record] [fail] | all} [{ <severity-level (0-7)> | alerts | critical | 
              debugging | emergencies | errors | informational | notification | warnings }]
              [switch <context_name>] 
   PRVID    : 15
   HELP     : Enables the tracing of Y1564 module as per the configured debug levels. 
              If switch is not defined it will take default switch.  
   CXT_HELP : debug Configures trace for the protocol|
              y1564 Y1564 related configuration|
              init-shut Start and shut down traces|
              mgmt Management traces|
              ctrl Control Plane traces|
              resource Traces related to all resources such as memory, data structure etc|
              timer Timer related traces|
              critical Critical traces|
              y1731 Y1731 interface related traces|
              test Configuration/Performance test start/stop/calculation traces |
              session-record Configuration/Performance test session record traces |
              fail Failure traces |
              all All trace messages|
              <severity-level(0-7)> Severity level value |
              alerts Immediate action needed |
              critical Critical conditions |
              debugging Debugging messages |
              emergencies System is unusable |
              errors Error conditions |
              informational Information messages |
              notification Normal but significant messages |
              warnings Warning conditions |
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Enables the tracing of Y1564 module as per the configured debug levels. 

   COMMAND  : no debug y1564 {[init-shut] [mgmt] [ctrl] [resource] [timer] [critical]
              [y1731] [test] [session-record] [fail] | all} [switch <string>] 
   ACTION   : { 
                  INT4 i4DebugType = Y1564_ZERO;

                  if ($3 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_INIT_SHUT_TRC;
                  }
                  if ($4 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_MGMT_TRC;
                  }
                  if ($5 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_CTRL_TRC;
                  }
                  if ($6 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_RESOURCE_TRC;
                  }
                  if ($7 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_TIMER_TRC;
                  }
                  if ($8 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_CRITICAL_TRC;
                  }
                  if ($9 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_Y1731_TRC;
                  }
                  if ($10 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_TEST_TRC;
                  }
                  if ($11 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_SESSION_TRC;
                  }
                  if ($12 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_ALL_FAILURE_TRC;
                  }
                  if ($13 != NULL)
                  {
                      i4DebugType = i4DebugType | Y1564_ALL_TRC;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_DEBUG,
                                         NULL, $15, i4DebugType);

              }
   SYNTAX   : no debug y1564 {[init-shut] [mgmt] [ctrl] [resource] [timer] [critical]
              [y1731] [test] [session-record] [fail] | all} [switch <context_name>]
   PRVID    : 15
   HELP     : Disables tracing of Y1564 module for the specified debug level. 
              If switch is not defined it will take default switch.  
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              debug Debug Traces|
              y1564 Y1564 related configuration|
              init-shut Start and shut down traces|
              mgmt Management traces|
              ctrl Control Plane traces|
              resource Traces related to all resources such as memory, data structure etc|
              timer Timer releated traces|
              critical Critical traces|
              y1731 Y1731 interface related traces|
              test Configuration/Performance test start/stop/calculation traces |
              session-record Configuration/Performance test session record traces |
              fail Failure traces |
              all All trace messages|
              switch Virtual context or a component|
              <string(32)> Context name|
              <CR> Disables tracing of Y1564 module for the specified debug level. 
#endif
END GROUP

DEFINE GROUP : Y1564_SLA_CMDS

   COMMAND  : map traffic-profile <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_MAP_TRAF_PROFILE, NULL, *(UINT4*)$2);
   SYNTAX   : map traffic-profile <Traf-Prof-id (1-10)>
   PRVID    : 15
   HELP     : Associates the Traffic Profile Identifier to the SLA.
   CXT_HELP : map Maps a traffic-profile to the SLA |
	      traffic-profile Traffic Profile related configuration |
              (1-10) Traffic Profile Identifier | 
              <CR> Associates the Traffic Profile Identifier to the SLA.

   COMMAND  : no map traffic-profile
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_MAP_TRAF_PROFILE, NULL);
   SYNTAX   : no map traffic-profile
   PRVID    : 15
   HELP     : De-Associates the Traffic Profile Identifier from the SLA.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default value |
              map Maps a traffic-profile to the SLA |
              traffic-profile Traffic Profile related configuration |
              <CR> De-Associates the Traffic Profile Identifier from the SLA.

   COMMAND  : map sac <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_MAP_SAC, NULL, *(UINT4*)$2);
   SYNTAX   : map sac <sac-id (1-10)>
   PRVID    : 15
   HELP     : Associates the Service Acceptance Criteria Identifier to the SLA.
   CXT_HELP : map Maps a Service Acceptance Criteria to the SLA |
	           sac Service Acceptance criteria related configuration |
              (1-10) SAC Identifier | 
              <CR> Associates the Service Acceptance Criteria Identifier to the SLA.

   COMMAND  : no map sac
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_MAP_SAC, NULL);
   SYNTAX   : no map sac
   PRVID    : 15
   HELP     : De-Associates the Service Acceptance Criteria Identifier from the SLA.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default value |
              map Maps a Service Acceptance Criteria related configuration |
              sac Service Acceptance criteria |
              <CR> De-Associates the Service Acceptance Criteria Identifier from the SLA.

   COMMAND  : map meg <integer> me <integer> mep <integer (1-8191)> 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_CFM_CFG, NULL, *(UINT4*)$2, *(UINT4*)$4, *(UINT4*)$6);
   SYNTAX   : map meg <meg-id (1-4294967295)> me <me-id (1-4294967295)> mep <mep-id (1-8191)>
   PRVID    : 15
   HELP     : Associates the MEP,ME and MEG (Y.1731 specific) for the SLA.
   CXT_HELP : map Maps a CFM entries to a SLA |
              meg Maintenance entity group related configuration |
              (1-4294967295) Maintenance entity group ID |
              me Maintenance entity related configuration |
              (1-4294967295) Maintenance entity ID |
              mep Maintenance entity group end point related configuration |
              (1-8191) Maintenance entity group end point ID |
              <CR> Associates the MEP,ME and MEG (Y.1731 specific) for the SLA.

   COMMAND  : no map meg me mep
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_CFM_CFG, NULL);
   SYNTAX   : no map meg me mep
   PRVID    : 15
   HELP     : De-associates the MEP,ME and MEG (Y.1731 specific) from the SLA.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default value |
              map Maps a CFM entries to a SLA |
              meg Maintenance entity group related configuration |
              me Maintenance entity related configuration |
              mep Maintenance entity group end point related configuration |
              <CR> De-associates the MEP,ME and MEG (Y.1731 specific) from the SLA.

   COMMAND  : rate-step <integer (10-100)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_STEP, NULL, *(UINT4*)$1);
   SYNTAX   : rate-step <sla-step (10-100)>
   PRVID    : 15
   HELP     : Configures the rate step i.e the step load rate for step load CIR. 
   CXT_HELP : rate-step rate step related configuration |
              (10-100) Number of steps in which the CIR is generated. The value should be perfect divisor of 100 |
              <CR> Configures the rate step i.e the step load rate for step load CIR.

   COMMAND  : configuration-test-duration <integer (10-60)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_DURATION, NULL, *(UINT4*)$1);
   SYNTAX   : configuration-test-duration <configuration-test-duration (10-60)>
   PRVID    : 15
   HELP     : Configures the service configuration test duration.
   CXT_HELP : configuration-test-duration service configuration test duration related configuration |
              <configuration-test-duration(10-60)> Duration in seconds |
              <CR> Configures the service configuration test duration.

   COMMAND  : traffic-policing {enable | disable}
   ACTION   : {
                  UINT4 u4TrafficPolicing = Y1564_ZERO;

                  if ($1 != NULL)
                  {
                      u4TrafficPolicing = OSIX_ENABLED;
                  }
                  else if ($2 != NULL)
                  {
                      u4TrafficPolicing = OSIX_DISABLED;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_TRAFFIC_POLICING, NULL, u4TrafficPolicing);
              }
   SYNTAX   : traffic-policing {enable | disable}
   PRVID    : 15
   HELP     : Enable/Disable traffic policing status for this SLA.
   CXT_HELP : traffic-policing Traffic Policing related information |
              enable Enables traffic policing status |
              disable Disable traffic policing status |
              <CR> Enable/Disable traffic policing status for the SLA.

#ifdef MEF_WANTED
   COMMAND  : map evc <vlan_vfi_id> 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_MAP_EVC, NULL, *(UINT4*)$2);
   SYNTAX   : map evc <vlan_vfi_id>
   PRVID    : 15
   HELP     : Associates the EVC Identifier to SLA.
   CXT_HELP : map Maps an EVC to the SLA |
	      evc EVC related configuration |
              <vlan_vfi_id> The range (1-4094) is for VLAN ID |
              <CR> Associates the EVC Identifier to SLA.

   COMMAND  : no map evc 
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_MAP_EVC, NULL);
   SYNTAX   : no map evc
   PRVID    : 15
   HELP     : De-Associates the EVC Identifier from SLA.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default value |
              map Maps an EVC to the SLA |
              evc EVC related configuration |
              <CR> De-Associates the EVC Identifier from SLA.

#else
   COMMAND  : map service-configuration <integer (1-10)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_MAP_SERVICECONF, NULL, *(UINT4*)$2);
   SYNTAX   : map service-configuration <service-configuration-id (1-10)>
   PRVID    : 15
   HELP     : Associates the Service Configuration Identifier to SLA.
   CXT_HELP : map Maps a service configuration Identifier to the SLA |
              service-configuration Service Configuration Identifier related configuration |
              <service-configuration-id(1-10)> Service Configuration Identifier |
              <CR> Associates the Service Configuration Identifier to the SLA.

   COMMAND  : no map service-configuration
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_NO_MAP_SERVICECONF, NULL);
   SYNTAX   : no map service-configuration
   PRVID    : 15
   HELP     : De-Associates the Service Configuration Identifier from the SLA.
   CXT_HELP : no Disables the configuration / deletes the entry / reset to default value |
              map Maps a Service Configuration to the SLA |
              service-configuration Service Configuration Identifier related configuration |
              <CR> De-Associates the Service Configuration Identifier from the SLA.
#endif              

   COMMAND  : test selector [[simple-cir-test] [stepload-cir-test] [eir-test] [traffic-policing-test]]
   ACTION   : {
                  UINT4 u4TestValue = Y1564_ZERO;

                  if ($2 != NULL)
                  {
                      u4TestValue |= Y1564_SIMPLE_CIR_TEST;
                  }

                  if ($3 != NULL)
                  {
                      u4TestValue |= Y1564_STEPLOAD_CIR_TEST;
                  }

                  if ($4 != NULL)
                  {
                      u4TestValue |= Y1564_EIR_TEST;
                  }

                  if ($5 != NULL)
                  {
                      u4TestValue |= Y1564_TRAF_POLICING_TEST;
                  }

                  if (($2 == NULL) && ($3 == NULL) && ($4 == NULL) && ($5 == NULL))
                  {
                      u4TestValue = Y1564_ALL_TEST;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_TEST_SELECTOR, NULL, u4TestValue);
              }
   SYNTAX   : test selector [[simple-cir-test] [stepload-cir-test] [eir-test] [traffic-policing-test]]
   PRVID    : 15
   HELP     : Test selector for current SLA, If the test is not selected explicitly then all the tests will be selected.
   CXT_HELP : test test mode related configuration for this SLA|
              selector test selector related configuration|
              simple-cir-test Start the test for Simple CIR|
              stepload-cir-test Start the test for Stepload CIR|
              eir-test Start the test for EIR|
              traffic-policing-test Start the test for Traffic Policing|
              <CR> Test selector for current SLA, If the test is not selected explicitly then all the tests will be selected.

   COMMAND  : exit
   ACTION   :
             {
                 CLI_SET_SLA_ID (-1);
                 CliChangePath ("..");
             }
   SYNTAX   : exit
   PRVID    : 15
   HELP     : Exit to the Global Configure Mode.
   CXT_HELP : exit Exit to the Global Configuration Mode|
             <CR> Exit to the Global Configuration Mode.
END GROUP

#ifndef MEF_WANTED
DEFINE GROUP : Y1564_SER_CONF_CMDS
   
   COMMAND  : service-config cir <integer(1-10485760)> cbs <integer(1-10485760)>
              eir <integer(1-10485760)> ebs <integer(1-10485760)> [color-aware] [coupling-flag]
   ACTION   : {
                  UINT4 u4ColorMode = Y1564_SERV_CONF_COLOR_BLIND;;
                  UINT4 u4CouplingFlag = Y1564_COUPLING_TYPE_DECOUPLED;

                  if ($9 != NULL)
                  {
                      u4ColorMode = Y1564_SERV_CONF_COLOR_AWARE;
                  }
                  if ($10 != NULL)
                  {
                      u4CouplingFlag = Y1564_COUPLING_TYPE_COUPLED;
                  }

                  cli_process_y1564_cmd (CliHandle, CLI_Y1564_SLA_METER, NULL, *(UINT4*)$2, *(UINT4*)$4,
                                         *(UINT4*)$6, *(UINT4*)$8, u4ColorMode, u4CouplingFlag);
              }
   SYNTAX   : service-config cir <cir-value-kbps (1-10485760)> cbs <cbs-value-bytes (1-10485760)>
              eir <eir-value-kbps (1-10485760)> ebs <ebs-value-bytes (1-10485760)> [color-aware] [coupling-flag]
   PRVID    : 15
   HELP     : Create and configures service configuration (bandwidth profile) parameters.
   CXT_HELP : service-config Service Configuration related configuration|
              cir Committed information rate configuration|
              (1-10485760) Committed information rate value in Kbps|
              cbs Committed burst size configuration |
              (1-10485760) Committed burst size value in bytes|
              eir Excessive information rate configuration |
              (1-10485760) Excessive information rate value in Kbps|
              ebs Excess burst size configuration |
              (1-10485760) Excess burst size value in bytes|
              color-aware Color aware related configuration|
              coupling-flag Coupling flag related configuration|
              <CR> Create and Configures service configuration (bandwidth profile) parameters.

   COMMAND  : exit
   ACTION   :
              {
                  CLI_SET_SLA_ID (-1);
                  CliChangePath ("..");
              }
   SYNTAX   : exit
   PRVID    : 15
   HELP     : Exit to the Global Configure Mode.
   CXT_HELP : exit Exit to the Global Configuration Mode|
              <CR> Exit to the Global Configuration Mode.

END GROUP
#endif

DEFINE GROUP : Y1564_TPROF_CMDS

   COMMAND  : packet-size <integer (64-1518)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_PKT_SIZE, NULL, *(UINT4*)$1);
   SYNTAX   : packet-size <packet-size (64-1518)>
   PRVID    : 15
   HELP     : Configures the packet size for the traffic profile.
   CXT_HELP : packet-size Configures packet size related information |
              (64-1518) Packet size in bytes. Allowable frame sizes 64,128,256,512,1024,1280 and 1518 | 
              <CR> Configures the packet size for the traffic profile.

   COMMAND  : packet-size emix-pattern <string(5)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_PKT_SIZE_EMIX, NULL, $2);
   SYNTAX   : packet-size emix-pattern <emix-string(5)>]
   PRVID    : 15
   HELP     : Configures the packet size as EMIX pattern for the traffic profile.
   CXT_HELP : packet-size Configures packet size related information |
              emix-pattern EMIX pattern related configuration |
              <emix-string(5)> EMIX pattern, a-64, b-128, c-256, d-512, e-1024, f-1280 , g-1518 (Example : abceg) |
              <CR> Configures the packet size as EMIX pattern for the traffic profile.

   COMMAND  : signature <string(16)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_PAYLOAD, NULL, $1);
   SYNTAX   : signature <payload(16)>
   PRVID    : 15
   HELP     : Configures the payload for the traffic profile.
   CXT_HELP : signature Configures the packet payload |
              <string(16)>  Payload string | 
              <CR> Configures the payload for the traffic profile.

   COMMAND  : pcp <integer (1-4)>
   ACTION   : cli_process_y1564_cmd (CliHandle, CLI_Y1564_TRAF_PROF_PCP, NULL, *(UINT4*)$1);
   SYNTAX   : pcp <pcp-value (1-4)>
   PRVID    : 15
   HELP     : Configures the pcp for the SLA.
   CXT_HELP : pcp Configures PCP for the SLA |
              (1-4) priority-code-point value|
              <CR> Configures the PCP for the SLA.

   COMMAND  : exit
   ACTION   :
              {
                  CLI_SET_TRAF_PROF_ID (-1);
                  CliChangePath ("..");
              }
   SYNTAX   : exit
   PRVID    : 15
   HELP     : Exit to Global Configuration Mode.
   CXT_HELP : exit Exit to Global Configuration Mode |
             <CR> Exit to Global Configuration Mode.
END GROUP
