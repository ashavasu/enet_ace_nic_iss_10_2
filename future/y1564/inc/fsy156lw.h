/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsy156lw.h,v 1.2 2015/11/05 13:05:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/


/* Proto Validate Index Instance for FsY1564ContextTable. */
INT1
nmhValidateIndexInstanceFsY1564ContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564ContextTable  */

INT1
nmhGetFirstIndexFsY1564ContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564ContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564ContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564ContextSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsY1564ContextModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsY1564ContextTraceOption ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ContextTrapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsY1564ContextNumOfConfTestRunning ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ContextNumOfPerfTestRunning ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564ContextSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsY1564ContextModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsY1564ContextTraceOption ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsY1564ContextTrapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564ContextSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564ContextModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564ContextTraceOption ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564ContextTrapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564ContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564SlaTable. */
INT1
nmhValidateIndexInstanceFsY1564SlaTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564SlaTable  */

INT1
nmhGetFirstIndexFsY1564SlaTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564SlaTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564SlaEvcIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaSacId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaTrafProfileId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaStepLoadRate ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaConfTestDuration ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaServiceConfId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaColorMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaCoupFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaCIR ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaCBS ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaEIR ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaEBS ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564SlaTrafPolicing ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaTestSelector ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaCurrentTestMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaCurrentTestState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SlaRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564SlaEvcIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaSacId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaTrafProfileId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaStepLoadRate ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaConfTestDuration ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaServiceConfId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564SlaTrafPolicing ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaTestSelector ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SlaRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564SlaEvcIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaSacId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaTrafProfileId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaStepLoadRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaConfTestDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaServiceConfId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564SlaTrafPolicing ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaTestSelector ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SlaRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564SlaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564TrafProfTable. */
INT1
nmhValidateIndexInstanceFsY1564TrafProfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564TrafProfTable  */

INT1
nmhGetFirstIndexFsY1564TrafProfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564TrafProfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564TrafProfDir ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564TrafProfPktSize ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564TrafProfPayload ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564TrafProfOptEmixPktSize ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564TrafProfPCP ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564TrafProfRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564TrafProfDir ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564TrafProfPktSize ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564TrafProfPayload ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsY1564TrafProfOptEmixPktSize ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsY1564TrafProfPCP ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564TrafProfRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564TrafProfDir ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564TrafProfPktSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564TrafProfPayload ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsY1564TrafProfOptEmixPktSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsY1564TrafProfPCP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564TrafProfRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564TrafProfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564SacTable. */
INT1
nmhValidateIndexInstanceFsY1564SacTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564SacTable  */

INT1
nmhGetFirstIndexFsY1564SacTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564SacTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564SacInfoRate ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SacFrLossRatio ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SacFrTransDelay ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SacFrDelayVar ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564SacAvailability ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564SacRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564SacInfoRate ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SacFrLossRatio ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SacFrTransDelay ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SacFrDelayVar ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564SacAvailability ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsY1564SacRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564SacInfoRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SacFrLossRatio ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SacFrTransDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SacFrDelayVar ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564SacAvailability ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsY1564SacRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564SacTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564ServiceConfTable. */
INT1
nmhValidateIndexInstanceFsY1564ServiceConfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564ServiceConfTable  */

INT1
nmhGetFirstIndexFsY1564ServiceConfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564ServiceConfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564ServiceConfColorMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564ServiceConfCoupFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564ServiceConfCIR ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ServiceConfCBS ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ServiceConfEIR ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ServiceConfEBS ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsY1564ServiceConfRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564ServiceConfColorMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564ServiceConfCoupFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564ServiceConfCIR ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564ServiceConfCBS ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564ServiceConfEIR ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564ServiceConfEBS ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsY1564ServiceConfRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564ServiceConfColorMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564ServiceConfCoupFlag ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564ServiceConfCIR ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564ServiceConfCBS ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564ServiceConfEIR ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564ServiceConfEBS ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsY1564ServiceConfRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564ServiceConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564ConfigTestReportTable. */
INT1
nmhValidateIndexInstanceFsY1564ConfigTestReportTable ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564ConfigTestReportTable  */

INT1
nmhGetFirstIndexFsY1564ConfigTestReportTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564ConfigTestReportTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564ConfigTestReportResult ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsY1564ConfigTestReportIrMin ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportIrMean ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportIrMax ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrLossCnt ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrLossRatio ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrTxDelayMin ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrTxDelayMean ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrTxDelayMax ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrDelayVarMin ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrDelayVarMean ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportFrDelayVarMax ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportTestStartTime ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564ConfigTestReportTestEndTime ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsY1564PerformanceTestTable. */
INT1
nmhValidateIndexInstanceFsY1564PerformanceTestTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564PerformanceTestTable  */

INT1
nmhGetFirstIndexFsY1564PerformanceTestTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564PerformanceTestTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564PerformanceTestSlaList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564PerformanceTestDuration ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564PerformanceTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsY1564PerformanceTestRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsY1564PerformanceTestSlaList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsY1564PerformanceTestDuration ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564PerformanceTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsY1564PerformanceTestRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsY1564PerformanceTestSlaList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsY1564PerformanceTestDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564PerformanceTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsY1564PerformanceTestRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsY1564PerformanceTestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsY1564PerfTestReportTable. */
INT1
nmhValidateIndexInstanceFsY1564PerfTestReportTable ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsY1564PerfTestReportTable  */

INT1
nmhGetFirstIndexFsY1564PerfTestReportTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsY1564PerfTestReportTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsY1564PerfTestReportResult ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsY1564PerfTestReportIrMin ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportIrMean ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportIrMax ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrLossCnt ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrLossRatio ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrTxDelayMin ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrTxDelayMean ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrTxDelayMax ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrDelayVarMin ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrDelayVarMean ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportFrDelayVarMax ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportAvailability ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsY1564PerfTestReportUnavailableCount ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportTestStartTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsY1564PerfTestReportTestEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));
