/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564cons.h,v 1.7 2016/01/25 12:04:37 siva Exp $
 *
 * Description: This file contains the constant macros of Y1564 module
 *
 * *******************************************************************/
#ifndef __Y1564CONS_H__
#define __Y1564CONS_H__


#define Y1564_START                           1
#define Y1564_SHUTDOWN                        2

#define Y1564_SUCCESS                         SNMP_SUCCESS
#define Y1564_FAILURE                         SNMP_FAILURE

#define Y1564_SNMP_TRUE                       1
#define Y1564_SNMP_FALSE                      2

#define Y1564_TRUE                            TRUE
#define Y1564_FALSE                           FALSE

#define Y1564_ENABLED                         1 
#define Y1564_DISABLED                        2

#define Y1564_TRC_MAX_SIZE                    255

#define Y1564_TRAP_FAILURE                    1

#define Y1564_INVALID_BRIDGE_MODE             7

#define Y1564_DEFAULT_CONTEXT_ID              L2IWF_DEFAULT_CONTEXT

#define Y1564_MIN_SLA_ID                      1
#define Y1564_MAX_SLA_ID                      10
#define Y1564_MIN_TRAF_PROF_ID                1
#define Y1564_MAX_TRAF_PROF_ID                10
#define Y1564_MIN_SAC_ID                      1
#define Y1564_MAX_SAC_ID                      10
#define Y1564_MIN_SERVICE_CONF_ID             1
#define Y1564_MAX_SERVICE_CONF_ID             10
#define Y1564_MIN_PERF_TEST_ID                1
#define Y1564_MAX_PERF_TEST_ID                10

#define Y1564_SLA_START                       1
#define Y1564_SLA_STOP                        2

/* Default Values */
#define Y1564_SLA_DEF_RATE_STEP               25
#define Y1564_SLA_DEF_DURATION                30
#define Y1564_SLA_DEF_STATUS                  Y1564_SLA_STOP
#define Y1564_SLA_DEF_COLOR_MODE              QOS_METER_COLOR_BLIND
/* #define Y1564_SLA_DEF_COUPLING_FLAG        QOS_METER_TYPE_MEF_DECOUPLED  */
#define Y1564_SLA_DEF_STEP_LOAD_DURATION      Y1564_SLA_DEF_DURATION
#define Y1564_SLA_DEF_TRAFFIC_POLICING_STATUS OSIX_ENABLED

/* SLA Configuration tests */
#define Y1564_SIMPLE_CIR_TEST                 Y1564_TEST_SIMPLE_CIR
#define Y1564_STEPLOAD_CIR_TEST               Y1564_TEST_STEP_LOAD_CIR
#define Y1564_EIR_TEST                        Y1564_TEST_EIR
#define Y1564_TRAF_POLICING_TEST              Y1564_TEST_TRAF_POLICY

#define Y1564_ALL_TEST                        (Y1564_SIMPLE_CIR_TEST    |\
                                               Y1564_STEPLOAD_CIR_TEST  |\
                                               Y1564_EIR_TEST           |\
                                               Y1564_TRAF_POLICING_TEST)

#define Y1564_CFM_SIGNAL_OK                 1
#define Y1564_CFM_SIGNAL_FAIL               2

/* Trap Events */
enum 
{
    Y1564_TRANSACTION_FAIL = 1,
    Y1564_TRAP_BUF_ALLOC_FAIL,
    Y1564_TRAP_TIMER_FAIL
};

#if 0
/* SLA current test state */
#define Y1564_SLA_TEST_NOT_INITIATED          1
#define Y1564_SLA_TEST_IN_PROGRESS            2
#define Y1564_SLA_TEST_COMPLETED              3
#define Y1564_SLA_TEST_ABORTED                4
#endif

#define Y1564_ZERO                            0 
#define Y1564_ONE                             1
#define Y1564_TWO                             2
#define Y1564_THREE                           3
#define Y1564_FOUR                            4
#define Y1564_FIVE                            5
#define Y1564_SIX                             6
#define Y1564_SEVEN                           7
#define Y1564_EIGHT                           8
#define Y1564_NINE                            9
#define Y1564_TEN                             10

#define Y1564_TRAF_PROF_DEFAULT_DIRECTION     1
#define Y1564_TRAF_PROF_DEFAULT_PACKET_SIZE   512

#define Y1564_MAX_LOSS_RATIO                  100
#define Y1564_MAX_AVAIL_PERCENTAGE            100.0

#define Y1564_MIN_PACKET_SIZE                 64
#define Y1564_MAX_PACKET_SIZE                 1518
#define Y1564_FRAME_64                        Y1564_MIN_PACKET_SIZE
#define Y1564_FRAME_128                       128
#define Y1564_FRAME_256                       256
#define Y1564_FRAME_512                       512
#define Y1564_FRAME_1024                      1024
#define Y1564_FRAME_1280                      1280
#define Y1564_FRAME_1518                      Y1564_MAX_PACKET_SIZE 
#define Y1564_BITS_PER_BYTE                   8       /* Bits per Byte  */
#define Y1564_SEC_IN_MIN                      60      /* seconds in a minutes */
#define Y1564_SEC_IN_HOURS                    60 * 60 /* seconds in a minutes */

#define Y1564_PAYLOAD_SIZE                    16

#define Y1564_EMIX_PKT_SIZE                   5
#define Y1564_MIN_RATE_STEP                   10
#define Y1564_MAX_RATE_STEP                   100

#define Y1564_MIN_EVC_INDEX                   1
#define Y1564_MAX_EVC_INDEX                   4094

#define Y1564_MIN_MEG_VALUE                   1
#define Y1564_MAX_MEG_VALUE                   4294967295

#define Y1564_MIN_ME_VALUE                    1
#define Y1564_MAX_ME_VALUE                    4294967295
 
#define Y1564_MIN_MEP_VALUE                   1
#define Y1564_MAX_MEP_VALUE                   8191

#define Y1564_CONF_TEST_MIN_DURATION           10
#define Y1564_CONF_TEST_MAX_DURATION           60

#define Y1564_STEP_LOAD_MIN_DURATION           10
#define Y1564_STEP_LOAD_MAX_DURATION           60

#define Y1564_TRAF_POLICY_STATUS_ON            1
#define Y1564_TRAF_POLICY_STATUS_OFF           2


#define Y1564_CIR_MIN_VAL                      0
#define Y1564_CIR_MAX_VAL                      10485760
#define Y1564_CBS_MIN_VAL                      0
#define Y1564_CBS_MAX_VAL                      10485760
#define Y1564_EIR_MIN_VAL                      0
#define Y1564_EIR_MAX_VAL                      10485760
#define Y1564_EBS_MIN_VAL                      0
#define Y1564_EBS_MAX_VAL                      10485760

#define Y1564_PERF_TEST_START                 Y1564_SLA_START
#define Y1564_PERF_TEST_STOP                  Y1564_SLA_STOP

#define Y1564_MAX_SLA_LIST_ID                 10
#define Y1564_MIN_SLA_LIST_ID                 1

#define Y1564_VLAN_8P0D_SEL_ROW               1
#define Y1564_VLAN_7P1D_SEL_ROW               2
#define Y1564_VLAN_6P2D_SEL_ROW               3
#define Y1564_VLAN_5P3D_SEL_ROW               4

/* Timer Types */
enum {
    Y1564_SLA_CONF_TEST_TMR,
    Y1564_SLA_PERF_TEST_TMR,
    Y1564_MAX_TMR_TYPES
};

#define Y1564_AVLBLTY_LENGTH                  10

/* RBTree related macros */
#define  Y1564_RB_LESSER                    -1
#define  Y1564_RB_GREATER                    1
#define  Y1564_RB_EQUAL                      0

#define Y1564_TEST_PASS                      1
#define Y1564_TEST_FAIL                      2

#define Y1564_TASK_NAME                      ((UINT1 *)"Y1564")
#define Y1564_SEM_NAME                       ((UINT1 *)"Y1564S")
#define Y1564_QUEUE_NAME                     ((UINT1 *)"Y1564Q")

#define Y1564_MAX_SLA_STEPS                  10


/* Events of Y1564 Main task */
#define Y1564_TMR_EXPIRY_EVENT               0x01 /* Bit 1 */
#define Y1564_QMSG_EVENT                     0x02 /* Bit 2 */
#define Y1564_ALL_EVENTS                    (Y1564_TMR_EXPIRY_EVENT | \
                                             Y1564_QMSG_EVENT)

#define Y1564_SIZING_CONTEXT_COUNT   FsY1564SizingParams[MAX_Y1564_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits



#define CLI_Y1564_MAX_SHOW_STRING_SIZE        24
#define CLI_Y1564_MAX_MAC_STRING_SIZE      21

#define Y1564_BPS_COVERTER                   1000
#define Y1564_BPS_TO_GBPS_CONV_FACTOR         1000000000.0
#define Y1564_HUNDRED                        100
#define Y1564_HUNDRED_TWENTY_FIVE            125
#define Y1564_TWENTY                         20
#define Y1564_TWENTY_FIVE                    25
#define BITS_PER_BYTE                        8

#define Y1564_MBPS_CONV_FACTOR               1000.0
#define Y1564_KBPS_TO_MBPS_CONV_FACTOR       1000
#define Y1564_BYTES_TO_KBYTES_CONV_FACTOR    1000.0
#define Y1564_BPS_TO_KBPS_CONV_FACTOR        1000.0
#define Y1564_OBJECT_NAME_LEN                256
#define Y1564_OBJECT_VALUE_LEN               128
#define Y1564_MAX_TRAP_STR_LEN               256

/* To check the payload has numeral character within it - 0(ascii 48) to 9(ascii 57) */
#define Y1564_NUM_CHAR_START 48
#define Y1564_NUM_CHAR_END   57
/* To check the password has upper case alphabet within it - A(ascii 65) to Z(ascii 90) */
#define Y1564_UPPER_CHAR_START  65
#define Y1564_UPPER_CHAR_END    90
/* To check the password has lower case alphabet within it - a(ascii 97) to z(ascii 122) */
#define Y1564_LOWER_CHAR_START  97
#define Y1564_LOWER_CHAR_END    122

#endif
