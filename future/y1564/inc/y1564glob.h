/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *  $Id: y1564glob.h,v 1.2 2015/12/21 11:20:08 siva Exp $
 * 
 *  Description: This file contains the global variables for Y1564
 *  module
*******************************************************************/

#ifndef __Y1564GLOB_H__
#define __Y1564GLOB_H__

#ifdef __Y1564MAIN_C__

tY1564GlobalInfo  gY1564GlobalInfo;
UINT1             gau1Y1564SystemStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1             gaNullPayload[Y1564_PAYLOAD_SIZE + 1];
UINT1             gu1Y1564Initialised = Y1564_FALSE;
UINT1             gau1EmixDefaultPktSize[Y1564_EMIX_PKT_SIZE] = {"abceg"};
INT1              gaai1Y1564TimerState[Y1564_MAX_TMR_TYPES][30] =  {"Configuration Test Timer",
    "Performance Test Timer"};
UINT1               gau1Y1564SlaList[MAX_Y1564_SLA_LIST_SIZE];
UINT1               gau1Y1564SlaDelList[MAX_Y1564_SLA_LIST_SIZE];

#else
extern tY1564GlobalInfo  gY1564GlobalInfo;
extern UINT1             gau1Y1564SystemStatus[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1             gaNullPayload[Y1564_PAYLOAD_SIZE + 1];
extern UINT1             gu1Y1564Initialised;
extern UINT1             gau1EmixDefaultPktSize[Y1564_EMIX_PKT_SIZE];
extern INT1 gaai1Y1564TimerState[Y1564_MAX_TMR_TYPES][30];
extern UINT1               gau1Y1564SlaList[MAX_Y1564_SLA_LIST_SIZE];
extern UINT1               gau1Y1564SlaDelList[MAX_Y1564_SLA_LIST_SIZE];

#endif

#endif /*end of __Y1564GLOB_H__*/
