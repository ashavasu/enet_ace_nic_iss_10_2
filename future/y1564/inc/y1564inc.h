/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564inc.h,v 1.1.1.1 2015/10/14 13:01:31 siva Exp $
 *
 * Description: This file contains header files included in Y1564 module.
 *****************************************************************************/
#ifndef __Y1564INC_H__
#define __Y1564INC_H__

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "iss.h"

#include "fm.h"
#include "fsvlan.h"
#include "vcm.h"
#include "npapi.h"

#include "dbutil.h"
#include "y1564.h"
#include "y1564cons.h"
#include "fssyslog.h"

#include "y1564macs.h"
#include "ecfm.h"
#include "y1564tdfs.h"

#include "y1564cli.h"
#include "y1564sz.h"
#include "y1564glob.h"
#include "fsy156wr.h"
#include "fsy156lw.h"
#include "y1564trc.h"
#include "y1564prot.h"
#include "rstp.h"
#include "fsmef.h"
#include "y1564extn.h"
#endif /*__Y1564INC_H__*/
