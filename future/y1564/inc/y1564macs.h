/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: y1564macs.h,v 1.3 2016/01/22 10:27:02 siva Exp $
*
* Description: Proto types & DataStructure definition 
*********************************************************************/
#ifndef __Y1564MACS_H__
#define __Y1564MACS_H__


/* Global Information */
#define Y1564_TASK_ID()              gY1564GlobalInfo.TaskId
#define Y1564_GLB_TRC()              gY1564GlobalInfo.i4GblTrace
#define Y1564_SEM_ID()               gY1564GlobalInfo.TaskSemId
#define Y1564_QUEUE_ID()             gY1564GlobalInfo.MsgQId
#define Y1564_SLA_POOL()             gY1564GlobalInfo.SlaPoolId
#define Y1564_SLA_LIST_POOL()        gY1564GlobalInfo.SlaListPoolId
#define Y1564_SLA_TABLE()            gY1564GlobalInfo.SlaTable
#define Y1564_TRAF_PROF_TABLE()      gY1564GlobalInfo.TrafProfTable
#define Y1564_TRAF_PROF_POOL()       gY1564GlobalInfo.TrafProfPoolId
#define Y1564_SAC_TABLE()            gY1564GlobalInfo.SacTable
#define Y1564_SAC_POOL()             gY1564GlobalInfo.SacPoolId
#ifndef MEF_WANTED
#define Y1564_SERV_CONF_TABLE()      gY1564GlobalInfo.ServiceConfTable
#define Y1564_SERV_CONF_POOL()       gY1564GlobalInfo.ServiceConfPoolId
#endif
#define Y1564_PERF_TEST_TABLE()      gY1564GlobalInfo.SlaPerfTestTable
#define Y1564_PERF_TEST_POOL()       gY1564GlobalInfo.SlaPerfTestPoolId
#define Y1564_CONF_REPORT_TABLE()    gY1564GlobalInfo.SlaConfTestReportTable
#define Y1564_CONF_REPORT_POOL()     gY1564GlobalInfo.SlaConfTestReportPoolId
#define Y1564_PERF_REPORT_TABLE()    gY1564GlobalInfo.SlaPerfTestReportTable
#define Y1564_PERF_REPORT_POOL()     gY1564GlobalInfo.SlaPerfTestReportPoolId
#define Y1564_CTXT_INFO_POOL()       gY1564GlobalInfo.SlaContextInfoPoolId
#define Y1564_EXTINPARAMS_POOL()     gY1564GlobalInfo.Y1564ExtInParamsPoolId
#define Y1564_EXTOUTPARAMS_POOL()    gY1564GlobalInfo.Y1564ExtOutParamsPoolId
#define Y1564_REQPARAMS_POOL()       gY1564GlobalInfo.Y1564ReqParamsPoolId 
#define Y1564_TIMER_LIST()           gY1564GlobalInfo.Y1564TmrListId
#define Y1564_TIMER_DESC()           gY1564GlobalInfo.aTmrDesc
/*#define Y1564_TIMER_DESC()           gY1564GlobalInfo.TmrDesc */

#define Y1564_QMSG_POOL()            gY1564GlobalInfo.TaskQMsgPoolId

#define Y1564_INITIALISED() gu1Y1564Initialised
 
/************* OCTET_STRING conversion Macros ********************/

#define Y1564_OCTETSTRING_TO_FLOAT(pOctetString, f4Val) \
                SSCANF((CHR1 *)pOctetString->pu1_OctetList, "%f", &f4Val)
#define Y1564_FLOAT_TO_OCTETSTRING(f4Value, pOctetString) \
                SPRINTF((CHR1 *)pOctetString->pu1_OctetList, "%.2f", f4Value)

/************* FILE Operations Macros ********************/

#define Y1564_FILE_SAVE(u4SlaId, au1VcAlias, pu1UserCmd) \
        if (u4SlaId != 0)                                                                                                    \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "show y1564 service-configuration-report sla-id %d switch %s", u4SlaId, au1VcAlias); \
        }                                                                                                                    \
        else                                                                                                                 \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "show y1564 service-configuration-report sla-id switch %s", au1VcAlias);             \
        }

#define Y1564_PERF_FILE_SAVE(u4PerfTestId, au1VcAlias, pu1UserCmd)                                                           \
        if (u4PerfTestId != 0)                                                                                                    \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "show y1564 service-performance-report performance-id %d switch %s", u4PerfTestId, au1VcAlias);\
        }                                                                                                                    \
        else                                                                                                                 \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "show y1564  service-performance-report performance-id switch %s", au1VcAlias);      \
        }

#define Y1564_FILE_TFTP(pu1OutputFile, pu1TftpPath, pu1UserCmd) \
        SPRINTF( (CHR1 *)pu1UserCmd, "        copy flash:%s %s ", pu1OutputFile, pu1TftpPath)

#define Y1564_FILE_ERASE(pu1OutputFile, pu1UserCmd) \
        SPRINTF( (CHR1 *)pu1UserCmd, "        erase flash:%s ", pu1OutputFile)

/* To convert timer duration to seconds and milliseconds*/

#define Y1564_CONTEXT_NAME(u4ContextId) \
             gY1564GlobalInfo.apY1564ContextInfo[u4ContextId]->au1ContextName

#define Y1564_IS_SYSTEM_INITIALISED()\
    ((Y1564_INITIALISED()== Y1564_TRUE))

#define Y1564_TRAP_STATUS(u4ContextId) \
            gY1564GlobalInfo.apY1564ContextInfo[u4ContextId]->u1TrapStatus


#define Y1564_SYSTEM_STATUS (u4ContextId)\
        (gau1Y1564SystemStatus[u4ContextId])

#define Y1564_SET_SYSTEM_STATUS (u4ContextId, u1Status)\
        (gau1Y1564SystemStatus[u4ContextId] = u1Status;)          

#define Y1564_IS_SYSTEM_STARTED (u4FsY1564ContextId)\
            (gau1Y1564SystemStatus[u4FsY1564ContextId] == Y1564_START)

#define Y1564_IS_SYSTEM_SHUTDOWN (u4FsY1564ContextId)\
            (!Y1564_IS_SYSTEM_STARTED (u4FsY1564ContextId))

/* Macro for storing Syslog ID for Y1564 */
#define Y1564_SYSLOG_ID          gY1564GlobalInfo.u4SysLogId
#define Y1564_HR_STDY_ST_REQ_RCVD()     \
            gY1564GlobalInfo.Y1564RedGlobalInfo.u1StdyStReqRcvd

/* Sizing related Macros */
#define Y1564_SIZING_MAX_QUEUE       (FsY1564SizingParams[MAX_Y1564_Q_MESG_SIZING_ID].u4PreAllocatedUnits)


#define SLA_STAT_INCREMENT_PORT_STATE_COUNTER(pSlaEntry) \
{\
    if (pSlaEntry->u2SlaCurrentStep < Y1564_MAX_SLA_STEPS)\
    {\
        pSlaEntry->u4SlaPortStateCounter++;\
    }\
}

#endif

#define Y1564_GET_4BYTE(u4Val, pu1Buf) \
{\
    MEMCPY (&u4Val, pu1Buf, 4);\
    u4Val = (OSIX_NTOHL(u4Val));\
}


/************* OCTET_STRING conversion Macros ********************/

/*#define Y1564_OCTETSTRING_TO_FLOAT (pOctetString, f4Val) \
            SSCANF((CHR1 *)pOctetString->pu1_OctetList, "%f", &f4Val)
#define Y1564_FLOAT_TO_OCTETSTRING (f4Value, pOctetString) \
            SPRINTF((CHR1 *)pOctetString->pu1_OctetList, "%f", f4Value)*/

/* To convert timer duration to seconds and milliseconds*/
#define Y1564_CONVERT_TMR_DURATION_TO_SEC(u4Duration,u4Sec,u4MilliSec) \
    u4Sec = u4Duration / 1000; \
    u4MilliSec = u4Duration % 1000;

/* To convert minutes to milliseonds from minutes */
#define Y1564_CONVERT_MIN_TO_MILLISECONDS(u4Duration, u4MilliSec)\
    u4MilliSec = u4Duration * 60 * 1000;

#define Y1564_CONVERT_HOURS_TO_MILLISECONDS(u4Duration, u4MilliSec)\
    u4MilliSec = u4Duration * 60 * 60 * 1000;

#define Y1564_CONVERT_SEC_TO_MILLISECONDS(u4Duration, u4MilliSec)\
    u4MilliSec = u4Duration * 1000;

#define Y1564_TRC_OPTION 
