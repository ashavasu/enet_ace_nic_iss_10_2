/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdefg.h,v 1.8 2018/01/05 09:57:09 siva Exp $
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define VXLAN_FSVXLANVTEPTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANVTEPTABLE_SIZING_ID]


#define VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANVTEPTABLE_ISSET_SIZING_ID]


#define VXLAN_FSVXLANNVETABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANNVETABLE_SIZING_ID]


#define VXLAN_FSVXLANNVETABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANNVETABLE_ISSET_SIZING_ID]


#define VXLAN_FSVXLANMCASTTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANMCASTTABLE_SIZING_ID]


#define VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANMCASTTABLE_ISSET_SIZING_ID]


#define VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_SIZING_ID]


#define VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_SIZING_ID]

#define VXLAN_FSVXLANINREPLICATABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANINREPLICATABLE_SIZING_ID]

#define VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANINREPLICATABLE_ISSET_SIZING_ID]

#ifdef EVPN_VXLAN_WANTED
#define VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_SIZING_ID]

#define VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_SIZING_ID]

#define VXLAN_FSEVPNVXLANVRFTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANVRFTABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANVRFTABLE_ISSET_SIZING_ID]

#define VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_SIZING_ID]

#define VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_SIZING_ID]

#define VXLAN_FSVXLANECMPNVETABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_FSVXLANECMPNVETABLE_SIZING_ID]

#define VXLAN_FSEVPNVXLANARPTABLE_POOLID  VXLANMemPoolIds[MAX_VXLAN_FSEVPNVXLANARPTABLE_SIZING_ID]

#define VXLAN_EVPNARPSUPMACTABLE_POOLID  VXLANMemPoolIds[MAX_VXLAN_EVPNARPSUPMACTABLE_SIZING_ID]

#endif /* EVPN_VXLAN_WANTED */

#define VXLAN_VNIVLANPORTTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_VNIVLANPORTTABLE_SIZING_ID]
#define VXLAN_VNIVLANPORTMACTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_VNIVLANPORTMACTABLE_SIZING_ID]
#define VXLAN_NVEPORTTABLE_POOLID   VXLANMemPoolIds[MAX_VXLAN_NVEPORTTABLE_SIZING_ID]

/* Macro used to fill the CLI structure for FsVxlanVtepEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANVTEPTABLE_ARGS(pVxlanFsVxlanVtepEntry,\
   pVxlanIsSetFsVxlanVtepEntry,\
   pau1FsVxlanVtepNveIfIndex,\
   pau1FsVxlanVtepAddressType,\
   pau1FsVxlanVtepAddress,\
   pau1FsVxlanVtepAddressLen,\
   pau1FsVxlanVtepRowStatus)\
  {\
  if (pau1FsVxlanVtepNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex = *(INT4 *) (pau1FsVxlanVtepNveIfIndex);\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVtepAddressType != NULL)\
  {\
   pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType = *(INT4 *) (pau1FsVxlanVtepAddressType);\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVtepAddress != NULL)\
  {\
   MEMCPY (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress, pau1FsVxlanVtepAddress, *(INT4 *)pau1FsVxlanVtepAddressLen);\
   pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen = *(INT4 *)pau1FsVxlanVtepAddressLen;\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVtepRowStatus != NULL)\
  {\
   pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus = *(INT4 *) (pau1FsVxlanVtepRowStatus);\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsVxlanNveEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANNVETABLE_ARGS(pVxlanFsVxlanNveEntry,\
   pVxlanIsSetFsVxlanNveEntry,\
   pau1FsVxlanNveIfIndex,\
   pau1FsVxlanNveVniNumber,\
   pau1FsVxlanNveDestVmMac,\
   pau1FsVxlanNveRemoteVtepAddressType,\
   pau1FsVxlanNveRemoteVtepAddress,\
   pau1FsVxlanNveRemoteVtepAddressLen,\
   pau1FsVxlanSuppressArp,\
   pau1FsVxlanNveRowStatus)\
  {\
  if (pau1FsVxlanNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = *(INT4 *) (pau1FsVxlanNveIfIndex);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsVxlanNveVniNumber != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber = *(UINT4 *) (pau1FsVxlanNveVniNumber);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsVxlanNveDestVmMac != NULL)\
  {\
   StrToMac ((UINT1 *)pau1FsVxlanNveDestVmMac, pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_FALSE;\
  }\
  if (pau1FsVxlanNveRemoteVtepAddressType != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType = *(INT4 *) (pau1FsVxlanNveRemoteVtepAddressType);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType = OSIX_FALSE;\
  }\
  if (pau1FsVxlanNveRemoteVtepAddress != NULL)\
  {\
   MEMCPY (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress, pau1FsVxlanNveRemoteVtepAddress, *(INT4 *)pau1FsVxlanNveRemoteVtepAddressLen);\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen = *(INT4 *)pau1FsVxlanNveRemoteVtepAddressLen;\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress = OSIX_FALSE;\
  }\
  if (pau1FsVxlanSuppressArp != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp = *(INT4 *) (pau1FsVxlanSuppressArp);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp = OSIX_FALSE;\
  }\
  if (pau1FsVxlanNveRowStatus != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus = *(INT4 *) (pau1FsVxlanNveRowStatus);\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsVxlanInReplicaEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANINREPLICATABLE_ARGS(pVxlanFsVxlanInReplicaEntry,\
   pVxlanIsSetFsVxlanInReplicaEntry,\
   pau1FsVxlanInReplicaNveIfIndex,\
   pau1FsVxlanInReplicaVniNumber,\
   pau1FsVxlanInReplicaIndex, \
   pau1FsVxlanInReplicaRemoteVtepAddressType,\
   pau1FsVxlanInReplicaRemoteVtepAddress,\
   pau1FsVxlanInReplicaRemoteVtepAddressLen,\
   pau1FsVxlanInReplicaRowStatus)\
  {\
  if (pau1FsVxlanInReplicaNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex = *(INT4 *) (pau1FsVxlanInReplicaNveIfIndex);\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsVxlanInReplicaVniNumber != NULL)\
  {\
   pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber = *(UINT4 *) (pau1FsVxlanInReplicaVniNumber);\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsVxlanInReplicaIndex != NULL)\
                {\
   pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex = *(INT4 *) (pau1FsVxlanInReplicaIndex);\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;\
                }\
                else\
                {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_FALSE;\
                }\
  if (pau1FsVxlanInReplicaRemoteVtepAddressType != NULL)\
  {\
   pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressType = *(INT4 *) (pau1FsVxlanInReplicaRemoteVtepAddressType);\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddressType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddressType = OSIX_FALSE;\
  }\
  if (pau1FsVxlanInReplicaRemoteVtepAddress != NULL)\
  {\
 MEMCPY (pVxlanFsVxlanInReplicaEntry->MibObject.au1FsVxlanInReplicaRemoteVtepAddress, pau1FsVxlanInReplicaRemoteVtepAddress, \
(*(INT4 *)pau1FsVxlanInReplicaRemoteVtepAddressLen));\
   pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen = *(INT4 *)pau1FsVxlanInReplicaRemoteVtepAddressLen;\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress = OSIX_FALSE;\
  }\
  if (pau1FsVxlanInReplicaRowStatus != NULL)\
  {\
   pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus = *(INT4 *) (pau1FsVxlanInReplicaRowStatus);\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus = OSIX_FALSE;\
  pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsVxlanMCastEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANMCASTTABLE_ARGS(pVxlanFsVxlanMCastEntry,\
   pVxlanIsSetFsVxlanMCastEntry,\
   pau1FsVxlanMCastNveIfIndex,\
   pau1FsVxlanMCastVniNumber,\
   pau1FsVxlanMCastGroupAddressType,\
   pau1FsVxlanMCastGroupAddress,\
   pau1FsVxlanMCastGroupAddressLen,\
   pau1FsVxlanMCastRowStatus)\
  {\
  if (pau1FsVxlanMCastNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex = *(INT4 *) (pau1FsVxlanMCastNveIfIndex);\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsVxlanMCastVniNumber != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber = *(UINT4 *) (pau1FsVxlanMCastVniNumber);\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsVxlanMCastGroupAddressType != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType = *(INT4 *) (pau1FsVxlanMCastGroupAddressType);\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType = OSIX_FALSE;\
  }\
  if (pau1FsVxlanMCastGroupAddress != NULL)\
  {\
   MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress, pau1FsVxlanMCastGroupAddress, *(INT4 *)pau1FsVxlanMCastGroupAddressLen);\
   pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen = *(INT4 *)pau1FsVxlanMCastGroupAddressLen;\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress = OSIX_FALSE;\
  }\
  if (pau1FsVxlanMCastRowStatus != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus = *(INT4 *) (pau1FsVxlanMCastRowStatus);\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsVxlanVniVlanMapEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANVNIVLANMAPTABLE_ARGS(pVxlanFsVxlanVniVlanMapEntry,\
   pVxlanIsSetFsVxlanVniVlanMapEntry,\
   pau1FsVxlanVniVlanMapVlanId,\
   pau1FsVxlanVniVlanMapVniNumber,\
   pau1FsVxlanVniVlanMapPktSent,\
   pau1FsVxlanVniVlanMapPktRcvd,\
   pau1FsVxlanVniVlanMapPktDrpd,\
   pau1FsVxlanVniVlanMapRowStatus,\
   pau1FsVxlanVniVlanTaggedVlan)\
  {\
  if (pau1FsVxlanVniVlanMapVlanId != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId = *(INT4 *) (pau1FsVxlanVniVlanMapVlanId);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanMapVniNumber != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber = *(UINT4 *) (pau1FsVxlanVniVlanMapVniNumber);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanMapPktSent != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent = *(UINT4 *) (pau1FsVxlanVniVlanMapPktSent);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanMapPktRcvd != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd = *(UINT4 *) (pau1FsVxlanVniVlanMapPktRcvd);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanMapPktDrpd != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd = *(UINT4 *) (pau1FsVxlanVniVlanMapPktDrpd);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanMapRowStatus != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus = *(INT4 *) (pau1FsVxlanVniVlanMapRowStatus);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsVxlanVniVlanTaggedVlan != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged = *(UINT1 *) (pau1FsVxlanVniVlanTaggedVlan);\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged = OSIX_FALSE;\
  }\
  }

#ifdef EVPN_VXLAN_WANTED
/* Macro used to fill the CLI structure for FsEvpnVxlanEviVniMapEntry 
 using the input given in def file */
#define  VXLAN_FILL_FSEVPNVXLANEVIVNIMAPTABLE_ARGS(pVxlanFsEvpnVxlanEviVniMapEntry,\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry,\
   pau1FsEvpnVxlanEviVniMapEviIndex,\
   pau1FsEvpnVxlanEviVniMapVniNumber,\
   pau1FsEvpnVxlanEviVniMapBgpRD,\
   pau1FsEvpnVxlanEviVniMapBgpRDLen,\
   pau1FsEvpnVxlanEviVniESI,\
   pau1FsEvpnVxlanEviVniESILen,\
   pau1FsEvpnVxlanEviVniLoadBalance,\
   pau1FsEvpnVxlanEviVniMapSentPkts,\
   pau1FsEvpnVxlanEviVniMapRcvdPkts,\
   pau1FsEvpnVxlanEviVniMapDroppedPkts,\
   pau1FsEvpnVxlanEviVniMapRowStatus,\
   pau1FsEvpnVxlanEviVniMapBgpRDAuto)\
  {\
  if (pau1FsEvpnVxlanEviVniMapEviIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex = *(INT4 *) (pau1FsEvpnVxlanEviVniMapEviIndex);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapVniNumber != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapVniNumber);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapBgpRD != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniMapBgpRD, pau1FsEvpnVxlanEviVniMapBgpRD, *(INT4 *)pau1FsEvpnVxlanEviVniMapBgpRDLen);\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen = *(INT4 *)pau1FsEvpnVxlanEviVniMapBgpRDLen;\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniESI != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniESI, pau1FsEvpnVxlanEviVniESI, *(INT4 *)pau1FsEvpnVxlanEviVniESILen);\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen = *(INT4 *)pau1FsEvpnVxlanEviVniESILen;\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniLoadBalance != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniLoadBalance = *(INT4 *) (pau1FsEvpnVxlanEviVniLoadBalance);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapSentPkts != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapSentPkts = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapSentPkts);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapRcvdPkts != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapRcvdPkts = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapRcvdPkts);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapDroppedPkts != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapDroppedPkts = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapDroppedPkts);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapDroppedPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapDroppedPkts = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapRowStatus != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapRowStatus = *(INT4 *) (pau1FsEvpnVxlanEviVniMapRowStatus);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapBgpRDAuto != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto = *(INT4 *) (pau1FsEvpnVxlanEviVniMapBgpRDAuto);\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsEvpnVxlanBgpRTEntry 
 using the input given in def file */
#define  VXLAN_FILL_FSEVPNVXLANBGPRTTABLE_ARGS(pVxlanFsEvpnVxlanBgpRTEntry,\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry,\
   pau1FsEvpnVxlanBgpRTIndex,\
   pau1FsEvpnVxlanBgpRTType,\
   pau1FsEvpnVxlanBgpRT,\
   pau1FsEvpnVxlanBgpRTLen,\
   pau1FsEvpnVxlanBgpRTRowStatus,\
   pau1FsEvpnVxlanBgpRTAuto,\
   pau1FsEvpnVxlanEviVniMapEviIndex,\
   pau1FsEvpnVxlanEviVniMapVniNumber)\
  {\
  if (pau1FsEvpnVxlanBgpRTIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex = *(UINT4 *) (pau1FsEvpnVxlanBgpRTIndex);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanBgpRTType != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType = *(INT4 *) (pau1FsEvpnVxlanBgpRTType);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanBgpRT != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT, pau1FsEvpnVxlanBgpRT, *(INT4 *)pau1FsEvpnVxlanBgpRTLen);\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen = *(INT4 *)pau1FsEvpnVxlanBgpRTLen;\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanBgpRTRowStatus != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus = *(INT4 *) (pau1FsEvpnVxlanBgpRTRowStatus);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanBgpRTAuto != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto = *(INT4 *) (pau1FsEvpnVxlanBgpRTAuto);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapEviIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex = *(INT4 *) (pau1FsEvpnVxlanEviVniMapEviIndex);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanEviVniMapVniNumber != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapVniNumber);\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsEvpnVxlanVrfEntry 
 using the input given in def file */
#define  VXLAN_FILL_FSEVPNVXLANVRFTABLE_ARGS(pVxlanFsEvpnVxlanVrfEntry,\
   pVxlanIsSetFsEvpnVxlanVrfEntry,\
   pau1FsEvpnVxlanVrfName,\
   pau1FsEvpnVxlanVrfNameLen,\
   pau4FsEvpnVxlanVrfVniNumber,\
   pau1FsEvpnVxlanVrfRD,\
   pau1FsEvpnVxlanVrfRDLen,\
   pau1FsEvpnVxlanVrfRDAuto,\
   pau1FsEvpnVxlanVrfRowStatus,\
   pau4FsEvpnVxlanVrfVniMapSentPkts,\
   pau4FsEvpnVxlanVrfVniMapRcvdPkts,\
   pau4FsEvpnVxlanVrfVniMapDroppedPkts)\
  {\
  if (pau1FsEvpnVxlanVrfName != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName, pau1FsEvpnVxlanVrfName, *(INT4 *)pau1FsEvpnVxlanVrfNameLen);\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen = *(INT4 *)pau1FsEvpnVxlanVrfNameLen;\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_FALSE;\
  }\
  if(pau4FsEvpnVxlanVrfVniNumber!= NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniNumber = *(UINT4 *) pau4FsEvpnVxlanVrfVniNumber;\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRD != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD, pau1FsEvpnVxlanVrfRD, *(INT4 *)pau1FsEvpnVxlanVrfRDLen);\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen = *(INT4 *)pau1FsEvpnVxlanVrfRDLen;\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRDAuto != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto = *(INT4 *) (pau1FsEvpnVxlanVrfRDAuto);\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRowStatus != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus = *(INT4 *) (pau1FsEvpnVxlanVrfRowStatus);\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_FALSE;\
  }\
  if(pau4FsEvpnVxlanVrfVniMapSentPkts!=NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts = *(UINT4 *)(pau4FsEvpnVxlanVrfVniMapSentPkts);\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts = OSIX_FALSE;\
  }\
  if(pau4FsEvpnVxlanVrfVniMapRcvdPkts !=NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts = *(UINT4 *) (pau4FsEvpnVxlanVrfVniMapRcvdPkts);\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts = OSIX_FALSE;\
  }\
  if(pau4FsEvpnVxlanVrfVniMapDroppedPkts != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts = *(UINT4 *)(pau4FsEvpnVxlanVrfVniMapDroppedPkts);\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsEvpnVxlanVrfRTEntry 
 using the input given in def file */
#define  VXLAN_FILL_FSEVPNVXLANVRFRTTABLE_ARGS(pVxlanFsEvpnVxlanVrfRTEntry,\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry,\
   pau1FsEvpnVxlanVrfName,\
   pau1FsEvpnVxlanVrfNameLen,\
   pau4FsEvpnVxlanVrfVniNumber,\
   pau1FsEvpnVxlanVrfRTIndex,\
   pau1FsEvpnVxlanVrfRTType,\
   pau1FsEvpnVxlanVrfRT,\
   pau1FsEvpnVxlanVrfRTLen,\
   pau1FsEvpnVxlanVrfRTAuto,\
   pau1FsEvpnVxlanVrfRTRowStatus )\
  {\
  if (pau1FsEvpnVxlanVrfName != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName, pau1FsEvpnVxlanVrfName, *(INT4 *)pau1FsEvpnVxlanVrfNameLen);\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen = *(INT4 *)pau1FsEvpnVxlanVrfNameLen;\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_FALSE;\
  }\
  if (pau4FsEvpnVxlanVrfVniNumber)\
  {\
    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) = *(UINT4 *) (pau4FsEvpnVxlanVrfVniNumber);\
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;\
  }\
  else\
  {\
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRTIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex = *(UINT4 *) (pau1FsEvpnVxlanVrfRTIndex);\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRTType != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType = *(INT4 *) (pau1FsEvpnVxlanVrfRTType);\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRT != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT, pau1FsEvpnVxlanVrfRT, *(INT4 *)pau1FsEvpnVxlanVrfRTLen);\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen = *(INT4 *)pau1FsEvpnVxlanVrfRTLen;\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRTAuto != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto =  *(INT4 *) (pau1FsEvpnVxlanVrfRTAuto);\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanVrfRTRowStatus != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus = *(INT4 *) (pau1FsEvpnVxlanVrfRTRowStatus);\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsEvpnVxlanMultihomedPeerTable 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANMULTIHOMEDPEERTABLE_ARGS(pVxlanFsEvpnVxlanMultihomedPeerTable,\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,\
   pau1FsEvpnVxlanPeerIpAddressType,\
   pau1FsEvpnVxlanPeerIpAddress,\
   pau1FsEvpnVxlanPeerIpAddressLen,\
   pau1FsEvpnVxlanMHEviVniESI,\
   pau1FsEvpnVxlanMHEviVniESILen,\
   pau1FsEvpnVxlanOrdinalNum,\
   pau1FsEvpnVxlanMultihomedPeerRowStatus)\
  {\
  if (pau1FsEvpnVxlanPeerIpAddressType != NULL)\
  {\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanPeerIpAddressType = *(INT4 *) (pau1FsEvpnVxlanPeerIpAddressType);\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanPeerIpAddress != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.au1FsEvpnVxlanPeerIpAddress, pau1FsEvpnVxlanPeerIpAddress, *(INT4 *)pau1FsEvpnVxlanPeerIpAddressLen);\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanPeerIpAddressLen = *(INT4 *)pau1FsEvpnVxlanPeerIpAddressLen;\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanMHEviVniESI != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.au1FsEvpnVxlanMHEviVniESI, pau1FsEvpnVxlanMHEviVniESI, *(INT4 *)pau1FsEvpnVxlanMHEviVniESILen);\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanMHEviVniESILen = *(INT4 *)pau1FsEvpnVxlanMHEviVniESILen;\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanOrdinalNum != NULL)\
  {\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum = *(UINT4 *) (pau1FsEvpnVxlanOrdinalNum);\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum = OSIX_FALSE;\
  }\
  if (pau1FsEvpnVxlanMultihomedPeerRowStatus != NULL)\
  {\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanMultihomedPeerRowStatus = *(INT4 *) (pau1FsEvpnVxlanMultihomedPeerRowStatus);\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_FALSE;\
  }\
  }
#endif /* EVPN_VXLAN_WANTED */
/* Macro used to fill the index values in  structure for FsVxlanVtepEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANVTEPTABLE_INDEX(pVxlanFsVxlanVtepEntry,\
   pau1FsVxlanVtepNveIfIndex)\
  {\
  if (pau1FsVxlanVtepNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex = *(INT4 *) (pau1FsVxlanVtepNveIfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsVxlanNveEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANNVETABLE_INDEX(pVxlanFsVxlanNveEntry,\
   pau1FsVxlanNveIfIndex,\
   pau1FsVxlanNveVniNumber,\
   pau1FsVxlanNveDestVmMac)\
  {\
  if (pau1FsVxlanNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = *(INT4 *) (pau1FsVxlanNveIfIndex);\
  }\
  if (pau1FsVxlanNveVniNumber != NULL)\
  {\
   pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber = *(UINT4 *) (pau1FsVxlanNveVniNumber);\
  }\
  if (pau1FsVxlanNveDestVmMac != NULL)\
  {\
   StrToMac (pau1FsVxlanNveDestVmMac, pVxlanFsVxlanNveEntry->MibOject.FsVxlanNveDestVmMac);\
  }\
 }
/* Macro used to fill the index values in  structure for FsVxlanMCastEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANMCASTTABLE_INDEX(pVxlanFsVxlanMCastEntry,\
   pau1FsVxlanMCastNveIfIndex,\
   pau1FsVxlanMCastVniNumber)\
  {\
  if (pau1FsVxlanMCastNveIfIndex != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex = *(INT4 *) (pau1FsVxlanMCastNveIfIndex);\
  }\
  if (pau1FsVxlanMCastVniNumber != NULL)\
  {\
   pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber = *(UINT4 *) (pau1FsVxlanMCastVniNumber);\
  }\
 }
/* Macro used to fill the index values in  structure for FsVxlanVniVlanMapEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSVXLANVNIVLANMAPTABLE_INDEX(pVxlanFsVxlanVniVlanMapEntry,\
   pau1FsVxlanVniVlanMapVlanId)\
  {\
  if (pau1FsVxlanVniVlanMapVlanId != NULL)\
  {\
   pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId = *(INT4 *) (pau1FsVxlanVniVlanMapVlanId);\
  }\
 }

#ifdef EVPN_VXLAN_WANTED
/* Macro used to fill the index values in  structure for FsEvpnVxlanEviVniMapEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANEVIVNIMAPTABLE_INDEX(pVxlanFsEvpnVxlanEviVniMapEntry,\
   pau1FsEvpnVxlanEviVniMapEviIndex,\
   pau1FsEvpnVxlanEviVniMapVniNumber)\
  {\
  if (pau1FsEvpnVxlanEviVniMapEviIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex = *(INT4 *) (pau1FsEvpnVxlanEviVniMapEviIndex);\
  }\
  if (pau1FsEvpnVxlanEviVniMapVniNumber != NULL)\
  {\
   pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapVniNumber);\
  }\
 }
/* Macro used to fill the index values in  structure for FsEvpnVxlanBgpRTEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANBGPRTTABLE_INDEX(pVxlanFsEvpnVxlanBgpRTEntry,\
   pau1FsEvpnVxlanEviVniMapEviIndex,\
   pau1FsEvpnVxlanEviVniMapVniNumber,\
   pau1FsEvpnVxlanBgpRTIndex,\
   pau1FsEvpnVxlanBgpRTType)\
  {\
  if (pau1FsEvpnVxlanEviVniMapEviIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex = *(INT4 *) (pau1FsEvpnVxlanEviVniMapEviIndex);\
  }\
  if (pau1FsEvpnVxlanEviVniMapVniNumber != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber = *(UINT4 *) (pau1FsEvpnVxlanEviVniMapVniNumber);\
  }\
  if (pau1FsEvpnVxlanBgpRTIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex = *(UINT4 *) (pau1FsEvpnVxlanBgpRTIndex);\
  }\
  if (pau1FsEvpnVxlanBgpRTType != NULL)\
  {\
   pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType = *(INT4 *) (pau1FsEvpnVxlanBgpRTType);\
  }\
 }
/* Macro used to fill the index values in  structure for FsEvpnVxlanVrfEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANVRFTABLE_INDEX(pVxlanFsEvpnVxlanVrfEntry,\
   pau1FsEvpnVxlanVrfName)\
  {\
  if (pau1FsEvpnVxlanVrfName != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName, pau1FsEvpnVxlanVrfName, *(INT4 *)pau1FsEvpnVxlanVrfNameLen);\
   pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen = *(INT4 *)pau1FsEvpnVxlanVrfNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsEvpnVxlanVrfRTEntry 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANVRFRTTABLE_INDEX(pVxlanFsEvpnVxlanVrfRTEntry,\
   pau1FsEvpnVxlanVrfName,\
   pau1FsEvpnVxlanVrfRTIndex,\
   pau1FsEvpnVxlanVrfRTType)\
  {\
  if (pau1FsEvpnVxlanVrfName != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName, pau1FsEvpnVxlanVrfName, *(INT4 *)pau1FsEvpnVxlanVrfNameLen);\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen = *(INT4 *)pau1FsEvpnVxlanVrfNameLen;\
  }\
  if (pau1FsEvpnVxlanVrfRTIndex != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex = *(UINT4 *) (pau1FsEvpnVxlanVrfRTIndex);\
  }\
  if (pau1FsEvpnVxlanVrfRTType != NULL)\
  {\
   pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType = *(INT4 *) (pau1FsEvpnVxlanVrfRTType);\
  }\
 }
/* Macro used to fill the index values in  structure for FsEvpnVxlanMultihomedPeerTable 
 using the input given in def file */

#define  VXLAN_FILL_FSEVPNVXLANMULTIHOMEDPEERTABLE_INDEX(pVxlanFsEvpnVxlanMultihomedPeerTable,\
   pau1FsEvpnVxlanPeerIpAddressType,\
   pau1FsEvpnVxlanPeerIpAddress,\
   pau1FsEvpnVxlanMHEviVniESI)\
  {\
  if (pau1FsEvpnVxlanPeerIpAddressType != NULL)\
  {\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanPeerIpAddressType = *(INT4 *) (pau1FsEvpnVxlanPeerIpAddressType);\
  }\
  if (pau1FsEvpnVxlanPeerIpAddress != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.au1FsEvpnVxlanPeerIpAddress, pau1FsEvpnVxlanPeerIpAddress, *(INT4 *)pau1FsEvpnVxlanPeerIpAddressLen);\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanPeerIpAddressLen = *(INT4 *)pau1FsEvpnVxlanPeerIpAddressLen;\
  }\
  if (pau1FsEvpnVxlanMHEviVniESI != NULL)\
  {\
   MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.au1FsEvpnVxlanMHEviVniESI, pau1FsEvpnVxlanMHEviVniESI, *(INT4 *)pau1FsEvpnVxlanMHEviVniESILen);\
   pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanMHEviVniESILen = *(INT4 *)pau1FsEvpnVxlanMHEviVniESILen;\
  }\
 }
#endif /* EVPN_VXLAN_WANTED */
/* Macro used to convert the input 
  to required datatype for FsVxlanEnable*/

#define  VXLAN_FILL_FSVXLANENABLE(i4FsVxlanEnable,\
   pau1FsVxlanEnable)\
  {\
  if (pau1FsVxlanEnable != NULL)\
  {\
   i4FsVxlanEnable = *(INT4 *) (pau1FsVxlanEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsVxlanUdpPort*/

#define  VXLAN_FILL_FSVXLANUDPPORT(u4FsVxlanUdpPort,\
   pau1FsVxlanUdpPort)\
  {\
  if (pau1FsVxlanUdpPort != NULL)\
  {\
   u4FsVxlanUdpPort = *(UINT4 *) (pau1FsVxlanUdpPort);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsVxlanTraceOption*/

#define  VXLAN_FILL_FSVXLANTRACEOPTION(u4FsVxlanTraceOption,\
   pau1FsVxlanTraceOption)\
  {\
  if (pau1FsVxlanTraceOption != NULL)\
  {\
   u4FsVxlanTraceOption = *(UINT4 *) (pau1FsVxlanTraceOption);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsVxlanNotificationCntl*/

#define  VXLAN_FILL_FSVXLANNOTIFICATIONCNTL(i4FsVxlanNotificationCntl,\
   pau1FsVxlanNotificationCntl)\
  {\
  if (pau1FsVxlanNotificationCntl != NULL)\
  {\
   i4FsVxlanNotificationCntl = *(INT4 *) (pau1FsVxlanNotificationCntl);\
  }\
  }

#ifdef EVPN_VXLAN_WANTED
/* Macro used to convert the input 
  to required datatype for FsEvpnVxlanEnable*/

#define  VXLAN_FILL_FSEVPNVXLANENABLE(i4FsEvpnVxlanEnable,\
   pau1FsEvpnVxlanEnable)\
  {\
  if (pau1FsEvpnVxlanEnable != NULL)\
  {\
   i4FsEvpnVxlanEnable = *(INT4 *) (pau1FsEvpnVxlanEnable);\
  }\
  }
#define  VXLAN_FILL_FSEVPNANYCASTGWMAC( FsEvpnAnycastGwMac,\
    pFsEvpnAnycastGwMac)\
{\
    if (pFsEvpnAnycastGwMac!= NULL)\
    {\
           StrToMac ((UINT1 *)pFsEvpnAnycastGwMac, FsEvpnAnycastGwMac);\
    }\
}
#endif /* EVPN_VXLAN_WANTED */

