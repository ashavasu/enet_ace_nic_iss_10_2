/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxsrcdef.h,v 1.4 2015/07/23 11:21:02 siva Exp $
*
* Description: This file contains the default values for Vxlan module.
*********************************************************************/


#ifndef __VXLANSRCDEFN_H__
#define __VXLANSRCDEFN_H__


#define VXLAN_SUCCESS        (OSIX_SUCCESS)
#define VXLAN_FAILURE        (OSIX_FAILURE)
#define  VXLAN_TASK_PRIORITY              100
#define  VXLAN_QUEUE_NAME                 (const UINT1 *) "VXLANQ"
#define  VXLAN_MUT_EXCL_SEM_NAME          (const UINT1 *) "VXLANM"
#define  VXLAN_QUEUE_DEPTH                MAX_VXLAN_QUEUE_MSG
#define  VXLAN_SEM_CREATE_INIT_CNT        1


#define  VXLAN_ENABLED                    1
#define  VXLAN_DISABLED                   2

#ifdef EVPN_VXLAN_WANTED
#define  EVPN_ENABLED                     1
#define  EVPN_DISABLED                    2
#define  EVPN_DEF_FSEVPNENABLE            VXLAN_DEF_FSEVPNVXLANENABLE
#define VXLAN_DEF_FSEVPNVXLANENABLE              2
#define VXLAN_DEF_FSVXLANSUPPRESSARP             2
#define VXLAN_DEF_FSEVPNVXLANEVIVNILOADBALANCE   2
#define VXLAN_DEF_FSEVPNVXLANEVIVNIMAPBGPRDAUTO    2
#define VXLAN_DEF_FSEVPNVXLANBGPRTAUTO             2
#endif /* EVPN_VXLAN_WANTED */

#define  VXLAN_TIMER_EVENT             0x00000001
#define  VXLAN_QUEUE_EVENT             0x00000002
#define  MAX_VXLAN_DUMMY                   1

/*********** macros for scalar objects*********************/

#define VXLAN_DEF_FSVXLANENABLE             2
#define VXLAN_DEF_FSVXLANUDPPORT             4789
#define VXLAN_DEF_FSVXLANTRACEOPTION             0
#define VXLAN_DEF_FSVXLANNOTIFICATIONCNTL             2

/*********** macros for tabular objects*********************/


#endif  /* __VXLANDEFN_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file vxlandefn.h                      */
/*-----------------------------------------------------------------------*/

