/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxsrc.h,v 1.4 2018/01/05 09:57:10 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*     for Vxlan module 
*********************************************************************/

#include "vxinc.h"
INT4 VxlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigFsVxlanVtepTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigFsVxlanNveTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigFsVxlanMCastTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigFsVxlanVniVlanMapTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanShowRunningConfigFsVxlanInReplicaTable(tCliHandle CliHandle, UINT4 u4Module, ...);
#ifdef EVPN_VXLAN_WANTED
INT4 EvpnShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 EvpnShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 EvpnShowRunningConfigEviVniMapTable (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 VxlanVrfShowRunningConfigVniMapTable(tCliHandle CliHandle,UINT4 u4Module,...);
VOID EvpnVrfCliShowAnycastGwMac(tCliHandle CliHandle,UINT4 u4Module,...);
#endif /* EVPN_VXLAN_WANTED */

