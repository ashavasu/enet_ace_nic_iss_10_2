/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxextn.h,v 1.2 2018/01/05 09:57:10 siva Exp $
 *
 * Description: This file contains extern declaration of global 
 *              variables of the vxlan module.
 *******************************************************************/

#ifndef __VXLANEXTN_H__
#define __VXLANEXTN_H__

PUBLIC tVxlanGlobals gVxlanGlobals;

extern tDbTblDescriptor gVxlanDynInfoList;

extern tDbTblDescriptor gVxlanDynHwInfoList;

extern tVxlanRedGlobalInfo  gVxlanRedGblInfo;

extern tDbDataDescInfo  gaVxlanDynDataDescList[VXLAN_MAX_DYN_INFO_TYPE];

extern tDbOffsetTemplate gaVxlanOffsetTbl[];

#endif/*__VXLANEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file vxlanextn.h                      */
/*-----------------------------------------------------------------------*/

