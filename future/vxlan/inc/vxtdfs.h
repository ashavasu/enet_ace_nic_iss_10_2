/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxtdfs.h,v 1.12 2018/01/05 09:57:10 siva Exp $
*
* Description: This file contains type definitions for Vxlan module.
*********************************************************************/

typedef struct

{

 tRBTree   FsVxlanVtepTable;  /*RBTree for VTEP table */
 tRBTree   FsVxlanNveTable;  /*RBTree for NVE table */
 tRBTree   FsVxlanMCastTable; /*RBTree for MCAST table */
 tRBTree   FsVxlanVniVlanMapTable; /*RBTreee for Vni-Vlan map table */ 
 tRBTree   FsVxlanInReplicaTable; /*RBTree for Ingress Replica table */
 tRBTree   VxlanNvePortTable;
#ifdef EVPN_VXLAN_WANTED
 tRBTree   FsEvpnVxlanEviVniMapTable; /*RBTree for EVPN-VNI map table */
 tRBTree   FsEvpnVxlanBgpRTTable; /* RBTree for BGP-RT table */
 tRBTree   FsEvpnVxlanVrfTable; /* RBTree for Vrf table */
 tRBTree   FsEvpnVxlanVrfRTTable; /* RBTree for Vrf-RT table */
 tRBTree   FsEvpnVxlanMultihomedPeerTable; /* RBTree for Multihomed Peer Table */
 tRBTree   FsVxlanEcmpNveTable; /*RBTree for Ecmp Nve table */
 tRBTree   FsEvpnVxlanArpTable; /*RBTree for Evpn Vxlna Arp table*/
#endif /* EVPN_VXLAN_WANTED */
 INT4      i4FsVxlanEnable; /*For enabling VXLAN feature */
 UINT4     u4FsVxlanUdpPort; /*For enabling UDP port number */
 UINT4     u4FsVxlanTraceOption; /*For enabling trace options */
 INT4      i4FsVxlanNotificationCntl; /*For enabling notification */
#ifdef EVPN_VXLAN_WANTED
 INT4      i4FsEvpnVxlanEnable; /*For enabling EVPN feature */
 tMacAddr  FsEvpnAnycastGwMac;
 UINT1             au1Pad[2]; 
#endif /* EVPN_VXLAN_WANTED */
 INT4      i4AcessportCount[SYS_DEF_MAX_INTERFACES];
} tVxlanGlbMib;

typedef struct TimerParameters
{
    union
    {
        struct VxlanFsVxlanNveEntry     *pVxlanNveEntry;
        struct EvpnMultihomedPeerTable  *pEvpnMultihomedPeerTable;
    }
    u;
    UINT1               u1Id;
    UINT1               u1Rsvd;
    UINT2               u2Rsvd;
}
tTimerParam;


typedef struct
{
 tVxlanMibFsVxlanVtepEntry       MibObject; 
} tVxlanFsVxlanVtepEntry;

typedef  struct
{
    tRBNodeEmbd VxlanVniVlanPortNode;
    UINT4 u4IfIndex;    /* Interface index acts as key */
    UINT4 u4AccessPort;  /* Access port created in BCM */
} tVxlanVniVlanPortEntry;

typedef  struct
{
    tRBNodeEmbd VxlanVniVlanPortMacNode;
    UINT4 u4IfIndex;        /* Interface index acts as key */
    tMacAddr MacAddress;    /* MAC address static/dynamic */
    UINT1      au1Pad[2];
} tVxlanVniVlanPortMacEntry;

typedef  struct
{
    tRBNodeEmbd  VxlanNvePortTableNode;
    UINT1        au1RemoteVtepAddress[VXLAN_IP6_ADDR_LEN];  /* Remote VTEP address acts as key */
    INT4         i4RemoteVtepAddressLen;                    /* remote VTEP address length */
    UINT4        u4NetworkBUMPort;                          /* Network port created in BCM for BUM */
    UINT4        u4NetworkPort;                             /* Network port created in BCM for unicast */
    UINT4        u4RefCount;                                /* To maintain the number of VNIs are using the same Tunnel */
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VxlanTunnelIfIndex;
    INT4         i4FirstCreate;                             /* Used to create network port and L2Table during MBSM */
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        au1Pad[2];
} tVxlanNvePortEntry;
typedef struct VxlanFsVxlanNveEntry
{
 tVxlanMibFsVxlanNveEntry       MibObject;
 INT4                           i4VlanId;
 UINT4                           u4OutVlanId;
 INT4                           i4FsVxlanNveEvpnMacType;/* Denotes the MAC type */
 UINT4                          u4VxlanTunnelIfIndex;
 UINT4                          u4OrgNveIfIndex;
 UINT1                          au1VxlanIRBHostAddress[16];
 UINT2                          u2VrfFlag;
 BOOL1                          bHwSet;
 BOOL1                          bIngOrMcastZeroEntryPresent;
 BOOL1                          b1IsIrbRoute;
 UINT1                          au1Pad[3]; 
 tTimerParam      VxlanRouteTimerParam;
 tTmrAppTimer     VxlanRouteTimer;

} tVxlanFsVxlanNveEntry;


typedef struct
{
 tVxlanMibFsVxlanMCastEntry       MibObject;
 INT4                           i4VlanId;
 UINT4                          u4OutVlanId;
 BOOL1                          bHwSet;                 
 BOOL1                          bDummyNveZeroMAC;
 UINT1                          au1Rsvd[2];
} tVxlanFsVxlanMCastEntry;


typedef struct
{
 tVxlanMibFsVxlanVniVlanMapEntry       MibObject;
 tRBTree   VxlanVniVlanPortTable;
 tRBTree   VxlanVniVlanPortMacTable;
} tVxlanFsVxlanVniVlanMapEntry;

typedef struct
{
 tVxlanMibFsVxlanInReplicaEntry       MibObject;
 INT4                           i4VlanId;
 UINT4                          u4OutVlanId;
 UINT4                          u4VxlanTunnelIfIndex;
 BOOL1                          bHwSet;
 BOOL1                          bDummyNveZeroMAC;
 BOOL1                          bRouteChanged;
 UINT1                          au1Rsvd;
}tVxlanFsVxlanInReplicaEntry;

typedef struct
{
 tVxlanMibFsEvpnVxlanEviVniMapEntry       MibObject;
 BOOL1                 bRDSet;
 BOOL1                 bESISet;
 UINT1                 au1Rsvd[2];
} tVxlanFsEvpnVxlanEviVniMapEntry;


typedef struct
{
 tVxlanMibFsEvpnVxlanBgpRTEntry       MibObject;
} tVxlanFsEvpnVxlanBgpRTEntry;


typedef struct
{
 tVxlanMibFsEvpnVxlanVrfEntry       MibObject;
 BOOL1                 bRDSet;
 BOOL1                 bESISet;
 BOOL1                 bHwSet;                 
 UINT1                 au1Rsvd[1];
} tVxlanFsEvpnVxlanVrfEntry;


typedef struct
{
 tVxlanMibFsEvpnVxlanVrfRTEntry       MibObject;
} tVxlanFsEvpnVxlanVrfRTEntry;


typedef struct EvpnMultihomedPeerTable
{
 tVxlanMibFsEvpnVxlanMultihomedPeerTable       MibObject;
        tTimerParam      EvpnRouteTimerParam;
        tTmrAppTimer     EvpnRouteTimer;
} tVxlanFsEvpnVxlanMultihomedPeerTable;

typedef struct VxlanFsVxlanEcmpNveEntry
{
 tVxlanMibFsVxlanEcmpNveEntry       MibObject;
 INT4                           i4VlanId;
 UINT4                           u4OutVlanId;
 INT4                           i4FsVxlanEcmpNveEvpnMacType;/* Denotes the MAC type */
 UINT4                          u4VxlanTunnelIfIndex;
 BOOL1                          bHwSet;
 UINT1                          au1Rsvd[3];
 tTimerParam      VxlanRouteTimerParam;
 tTmrAppTimer     VxlanRouteTimer;

} tVxlanFsVxlanEcmpNveEntry;

typedef struct VxlanFsEvpnVxlanArpEntry
{
 tVxlanMibFsEvpnVxlanArpEntry MibObject;
} tVxlanFsEvpnVxlanArpEntry;
typedef struct tVxlanEvpnArpSupLocalMacEntry
{
    tRBNodeEmbd        VxlanEvpnArpSupLocalMacNode;
    UINT4              u4Vni;
    UINT1              au1IpAddress [16];
    tMacAddr           SourceMac;
    UINT1              u1AddressLen;
    UINT1              u1Resvd;
}tVxlanEvpnArpSupLocalMacEntry;
    

#ifndef __VXLANTDFS_H__
#define __VXLANTDFS_H__

/* -------------------------------------------------------
 *
 *                VXLAN   Data Structures
 *
 * ------------------------------------------------------*/
#ifdef EVPN_VXLAN_WANTED
#define MAX_EVPN_VRF_ENTRIES               SYS_DEF_MAX_NUM_CONTEXTS
#define EVPN_MAX_U4_RT_BLKS                2
#define EVPN_NUM_OF_BITS_IN_UINT4          32
#define EVPN_MAX_NUM_RT_PER_VRF            50
#define EVPN_THREE_SECOND                  3
typedef long long       ULONG8;
#endif /* EVPN_VXLAN_WANTED */
#define VXLAN_INIT_VAL                     0
#define VXLAN_MAX_U4_RT_BLKS                 2
#define VXLAN_NUM_OF_BITS_IN_UINT4          32
#define VXLAN_MAX_NUM_REPLICA_ENTRY_PER_VNI 10

typedef struct VXLAN_GLOBALS {
 tOsixTaskId         vxlanTaskId;   /* Denotes the VXLAN task id */
 UINT1               au1TaskSemName[8]; /* Denotes the VXLAN sem name */
 tOsixSemId          vxlanTaskSemId; /* Denotes the VXLAN sem id */
 tOsixQId            vxlanQueId; /* Denotes the VXLAN queue id */
 tVxlanGlbMib        VxlanGlbMib; /* Contains the whole VXLAN data structures 
                                      4 tabular objects and 4 scalar objects */
    tTimerListId VxlanTimerList;

    INT4                i4VxlanSockId; /* This socket id is used for receiving
                                          IPV4 VXLAN packet on standard UDP
                                          port number - 4789 */
    INT4                i4VxlanTxSockId; /* This socket id is used for transmitting
                                            IPV4 VXLAN packet on standard UDP
                                             port number betweeen 49152 and 65535 */
    INT4                i4Vxlanv6SockId; /* This socket id is used for receiving
                                          IPV6 VXLAN packet on standard UDP
                                          port number - 4789 */
    INT4                i4Vxlanv6TxSockId; /* This socket id is used for transmitting
                                            IPV6 VXLAN packet on standard UDP
                                             port number betweeen 49152 and 65535 */

    UINT4               gau4VxlanReplicaIndex
                        [MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE + 1];

    UINT4               gau4VxlanReplicaBitMapIdx
                        [MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE][VXLAN_MAX_U4_RT_BLKS];

#ifdef EVPN_VXLAN_WANTED

    UINT4               gau4EvpnRtBitMapIdx[MAX_VXLAN_EVPN_VNI][EVPN_MAX_U4_RT_BLKS];
    UINT4               gau4EvpnVrfRtBitMapIdx[MAX_VXLAN_EVPN_VRF][EVPN_MAX_U4_RT_BLKS];
    /* We can increase the value L3VPM_MAX_U4_RT_BLKS to increase the number of RT's per VNI
     * With Value = 2, Maximum 64 RT entries can be supported
     * This ideally should be MAX_RT_PER_VRF/SIZE_OF_UINT4 + 1 if this number
     * is not a multiple of 32, else it should be (MAX_RT_PER_VRF/SIZE_OF_UINT4) */
    UINT4              gau4EvpnRtIndex[MAX_VXLAN_EVPN_VNI + 1];
    UINT4              gau4EvpnVrfRtIndex[MAX_VXLAN_EVPN_VRF + 1];
    tEvpnBgpRegEntry   EvpnBgpRegEntry; /* Callback structure to send information from VXLAN-EVPN to BGP */
    tRBTree            VxlanEvpnArpSupLocalMacTable; /* For Local IP - MAC mapping for Arp suppression */
#endif /* EVPN_VXLAN_WANTED */
} tVxlanGlobals;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER   *pBuf; /*Pointer to the packet buffer */
    UINT4                   u4IfIndex; /* Index on which packet is received */
}tVxlanL2PktInfo;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER   *pBuf; /*Pointer to the packet buffer */
    UINT4                   u4VrfId; /* Context on which packet is received */
    UINT4                   u4L3VnId; /* L3VNI associated with VRF */
}tVxlanL3PktInfo;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER   *pBuf; /*Pointer to the packet buffer */
    UINT4                   u4AddressType;
}tVxlanPimPktInfo;

typedef struct _VxlanRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tVxlanRmCtrlMsg;

typedef struct _VxlanRmMsg
{
    tVxlanRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tVxlanRmMsg;

typedef struct
{
    tMacAddr       NpMacAddr;
    tVlanId        VlanId2;
    UINT4          u4RemoteVTEPIp;
    UINT1          u1Flag;
    UINT1          au1Pad[3];
}tVxlanNpMacVlanInfo;

#ifdef EVPN_VXLAN_WANTED
typedef struct
{
        tMacAddr   SysID;
        UINT2      u2SysPriority;
        UINT2      u2VlanId;
        UINT1      u1OperStatus;
        UINT1      au1Pad;
}tVlanOper;
#endif
#ifdef  NPAPI_WANTED
typedef struct
{
    UINT4          u4Vni;
    INT4           i4Port;
    tMacAddr       NpMacAddr;
    UINT1          u1Flag;
    UINT1          au1Pad;
}tVxlanNpMacPortInfo;
#endif

typedef struct {
    UINT4 u4MsgType; /* 5 Message types are used
                        VXLAN_L2PKT_RCVD_EVENT - For L2 Packet
                        VXLAN_UDP_IPV4_EVENT   - For IPV4 UDP packet
                        VXLAN_UDP_IPV6_EVENT   - For IPV6 UDP packet
                        VXLAN_NVE_DEL_EVENT    - For deleting NVE database
                        VXLAN_PKT_RCVD_FROM_PIM - For pim
                      */
    UINT4 u4Addr;
    INT4  i4Adminstatus;
    UINT1 u1CmdType;
    UINT1 au1Pad[3];
    union {
        tVxlanL2PktInfo  VxlanL2PktInfo; /* L2 packet information */
#ifdef L2RED_WANTED
        tVxlanRmMsg            VxlanRmMsg;
#endif
#ifdef NPAPI_WANTED
        tVxlanNpMacVlanInfo  VxlanNpMacVlanInfo; 
        tVxlanNpMacPortInfo  VxlanNpMacPortInfo;
#endif
#ifdef  EVPN_VXLAN_WANTED
        tEvpnRoute EvpnRoute;
        tVlanOper  VlanOper;
#endif
        tVxlanL3PktInfo  VxlanL3PktInfo; /* L3 packet information */
        INT4                 i4SockId; /* UDP socket id */
        UINT4                u4IfIndex;
#ifdef MBSM_WANTED
        struct MbsmIndication {
            tMbsmProtoMsg mbsmProtoMsg;
        }MbsmCardUpdate;
#endif
   }uVxlanMsg;
}tVxlanIfMsg;

typedef struct
{
    UINT1              au1VxlanMinMTU[VXLAN_JUMBO_MTU_SIZE]; /* VXLAN MTU size */
}
tVxlanMinMTUSize;

typedef struct
{
    UINT4                u4Flag; /* First four bytes of VXLAN header */
    UINT4                u4VniNumber; /* Second four bytes of VXLAN header */
} tVxlanHdr;

typedef struct _VxlanRedGlobalInfo {
    UINT1   u1BulkUpdStatus;  /* bulk update status
                               * VXLAN_HA_UPD_NOT_STARTED 0
                               * VXLAN_HA_UPD_COMPLETED 1
                               * VXLAN_HA_UPD_IN_PROGRESS 2,
                               * VXLAN_HA_UPD_ABORTED 3 */

    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
}tVxlanRedGlobalInfo;

typedef struct _VxlanRmSyncMsg{
    tDbTblNode  DbNode;    /* To add the RM msgs to DBList */
    UINT4       u4DataLen; /* Length of the RM msg */
    tRmMsg     *pBuf;      /* RM msg to sync with redundant NODE */
    UINT1       u1Event;   /* RM event posted to VXLAN */
    UINT1       au1Pad[3];
}tVxlanRmSyncMsg;

#endif  /* __VXLANTDFS_H__ */
/*-----------------------------------------------------------------------*/


