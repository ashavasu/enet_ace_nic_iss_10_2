/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxglob.h,v 1.3 2018/01/05 09:57:10 siva Exp $
 *
 * Description: This file contains declaration of global variables 
 *              of vxlan module.
 *******************************************************************/

#ifndef __VXLANGLOBAL_H__
#define __VXLANGLOBAL_H__

tVxlanGlobals gVxlanGlobals;

/* RED Global Info */
tVxlanRedGlobalInfo  gVxlanRedGblInfo;

tDbTblDescriptor gVxlanDynInfoList;

tDbTblDescriptor gVxlanDynHwInfoList;

tDbDataDescInfo  gaVxlanDynDataDescList[VXLAN_MAX_DYN_INFO_TYPE];

tDbOffsetTemplate gaVxlanOffsetTbl[] =
{{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, u4FsVxlanNveVniNumber) -
   FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry,i4FsVxlanNveIfIndex) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveVtepAddressType) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveRemoteVtepAddressType) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveStorageType) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveRowStatus) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveVtepAddressLen) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, i4FsVxlanNveRemoteVtepAddressLen) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, au1FsVxlanNveVtepAddress) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 16},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, au1FsVxlanNveRemoteVtepAddress) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 16},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, FsVxlanNveDestVmMac) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 6},
{(FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, u1Rsvd) -
  FSAP_OFFSETOF(tVxlanMibFsVxlanNveEntry, NveDbNode) - sizeof(tDbTblNode)), 2},
{-1, 0}};


#endif  /* __VXLANGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file vxlanglob.h                      */
/*-----------------------------------------------------------------------*/
