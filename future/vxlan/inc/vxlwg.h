/********************************************************************
* Copyright (C) 20142006 Aricent Inc . All Rights Reserved
*
* $Id: vxlwg.h,v 1.6 2018/01/05 09:57:10 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsVxlanVtepTable. */
INT1
nmhValidateIndexInstanceFsVxlanVtepTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsVxlanNveTable. */
INT1
nmhValidateIndexInstanceFsVxlanNveTable ARG_LIST((INT4  , UINT4  , tMacAddr ));

/* Proto Validate Index Instance for FsVxlanMCastTable. */
INT1
nmhValidateIndexInstanceFsVxlanMCastTable ARG_LIST((INT4  , UINT4 ));

/* Proto Validate Index Instance for FsVxlanVniVlanMapTable. */
INT1
nmhValidateIndexInstanceFsVxlanVniVlanMapTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsVxlanInReplicaTable. */
INT1
nmhValidateIndexInstanceFsVxlanInReplicaTable ARG_LIST((INT4  , UINT4 , INT4  ));

/* Proto Validate Index Instance for FsEvpnVxlanEviVniMapTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanEviVniMapTable ARG_LIST((INT4  , UINT4 ));

/* Proto Validate Index Instance for FsEvpnVxlanBgpRTTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanBgpRTTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Validate Index Instance for FsEvpnVxlanVrfTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *,  UINT4));

/* Proto Validate Index Instance for FsEvpnVxlanVrfRTTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4 ));

/* Proto Validate Index Instance for FsEvpnVxlanMultihomedPeerTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanMultihomedPeerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsVxlanVtepTable  */

INT1
nmhGetFirstIndexFsVxlanVtepTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsVxlanNveTable  */

INT1
nmhGetFirstIndexFsVxlanNveTable ARG_LIST((INT4 * , UINT4 * , tMacAddr * ));

/* Proto Type for Low Level GET FIRST fn for FsVxlanMCastTable  */

INT1
nmhGetFirstIndexFsVxlanMCastTable ARG_LIST((INT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsVxlanVniVlanMapTable  */

INT1
nmhGetFirstIndexFsVxlanVniVlanMapTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsVxlanInReplicaTable  */

INT1
nmhGetFirstIndexFsVxlanInReplicaTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanEviVniMapTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanEviVniMapTable ARG_LIST((INT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanBgpRTTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanBgpRTTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanVrfTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *,  UINT4 * ));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanVrfRTTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *, UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanMultihomedPeerTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanMultihomedPeerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVxlanVtepTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVxlanNveTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVxlanMCastTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVxlanVniVlanMapTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVxlanInReplicaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanEviVniMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanBgpRTTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *, UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanMultihomedPeerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanEnable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanUdpPort ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanTraceOption ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNotificationCntl ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEnable ARG_LIST((INT4 *));

INT1
nmhGetFsEvpnAnycastGwMac ARG_LIST((tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVtepAddressType ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVtepAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVtepRowStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveVtepAddressType ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveVtepAddress ARG_LIST((INT4  , UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveRemoteVtepAddressType ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveRemoteVtepAddress ARG_LIST((INT4  , UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveStorageType ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanSuppressArp ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsVxlanNveVrfName ARG_LIST((INT4  , UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanNveRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanMCastGroupAddressType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanMCastGroupAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanMCastVtepAddressType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanMCastVtepAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanMCastRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanMapVniNumber ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanMapPktSent ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanMapPktRcvd ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanMapPktDrpd ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanMapRowStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanVniVlanDfElection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVxlanVniVlanTagStatus ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsVxlanInReplicaVtepAddressType ARG_LIST((INT4  , UINT4 , INT4 , INT4 *));    

INT1
nmhGetFsVxlanInReplicaVtepAddress ARG_LIST((INT4  , UINT4 , INT4 , tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVxlanInReplicaRemoteVtepAddressType ARG_LIST((INT4  , UINT4 , INT4 , INT4 *));

INT1
nmhGetFsVxlanInReplicaRemoteVtepAddress ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVxlanInReplicaRowStatus ARG_LIST((INT4  , UINT4 , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapBgpRD ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniESI ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniLoadBalance ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapSentPkts ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapRcvdPkts ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapDroppedPkts ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapBgpRDAuto ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanEviVniMapTotalPaths ARG_LIST((INT4, UINT4, INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanBgpRT ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanBgpRTRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanBgpRTAuto ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE *, UINT4, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, UINT4, INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfVniMapSentPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfVniMapRcvdPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfVniMapDroppedPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRDAuto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRTRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanVrfRTAuto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4 ,INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanOrdinalNum ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanMultihomedPeerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanEnable ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanUdpPort ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanTraceOption ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanNotificationCntl ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEnable ARG_LIST((INT4 ));

INT1
nmhSetFsEvpnAnycastGwMac ARG_LIST((tMacAddr ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVtepAddressType ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVtepAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVtepRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanNveRemoteVtepAddressType ARG_LIST((INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanNveRemoteVtepAddress ARG_LIST((INT4  , UINT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanSuppressArp ARG_LIST((INT4  , UINT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsVxlanNveVrfName ARG_LIST((INT4  , UINT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanNveRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanMCastGroupAddressType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanMCastGroupAddress ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanMCastRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVniVlanMapVniNumber ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVniVlanMapPktSent ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVniVlanMapPktRcvd ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVniVlanMapPktDrpd ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanVniVlanMapRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVxlanVniVlanTagStatus ARG_LIST((INT4  ,INT4 ));


INT1
nmhSetFsVxlanInReplicaRemoteVtepAddressType ARG_LIST((INT4  , UINT4  ,INT4 , INT4  ));

INT1
nmhSetFsVxlanInReplicaRemoteVtepAddress ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsVxlanInReplicaRowStatus ARG_LIST((INT4  , UINT4  ,INT4 , INT4));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEviVniMapBgpRD ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEviVniESI ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEviVniLoadBalance ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEviVniMapSentPkts ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsEvpnVxlanEviVniMapRcvdPkts ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsEvpnVxlanEviVniMapDroppedPkts ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsEvpnVxlanEviVniMapRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanEviVniMapBgpRDAuto ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanBgpRT ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanBgpRTRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanBgpRTAuto ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE *, UINT4 ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *i, UINT4, INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfVniMapSentPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfVniMapRcvdPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfVniMapDroppedPkts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRDAuto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRTRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanVrfRTAuto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanOrdinalNum ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanMultihomedPeerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanUdpPort ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanTraceOption ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanNotificationCntl ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsEvpnVxlanEnable ARG_LIST((UINT4 * , INT4 ));

INT1
nmhTestv2FsEvpnAnycastGwMac ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVxlanVtepAddressType ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVtepAddress ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVtepRowStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanNveRemoteVtepAddressType ARG_LIST((UINT4 * , INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanNveRemoteVtepAddress ARG_LIST((UINT4 * , INT4  , UINT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanSuppressArp ARG_LIST((UINT4 * , INT4  , UINT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsVxlanNveVrfName ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanNveRowStatus ARG_LIST((UINT4 * , INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanMCastGroupAddressType ARG_LIST((UINT4 * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanMCastGroupAddress ARG_LIST((UINT4 * , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanMCastRowStatus ARG_LIST((UINT4 * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVniVlanMapVniNumber ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVniVlanMapPktSent ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVniVlanMapPktRcvd ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVniVlanMapPktDrpd ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanVniVlanMapRowStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

INT1
nmhTestv2FsVxlanVniVlanTagStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2FsVxlanInReplicaRemoteVtepAddressType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 , INT4 ));

INT1
nmhTestv2FsVxlanInReplicaRemoteVtepAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsVxlanInReplicaRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 , INT4 ));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniMapBgpRD ARG_LIST((UINT4 * , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniESI ARG_LIST((UINT4 * , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniLoadBalance ARG_LIST((UINT4 * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniMapSentPkts ARG_LIST((UINT4 * , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniMapRcvdPkts ARG_LIST((UINT4 * , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsEvpnVxlanEviVniMapDroppedPkts ARG_LIST((UINT4 * , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsEvpnVxlanEviVniMapRowStatus ARG_LIST((UINT4 * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanEviVniMapBgpRDAuto ARG_LIST((UINT4 * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanBgpRT ARG_LIST((UINT4 * , INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanBgpRTRowStatus ARG_LIST((UINT4 * , INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanBgpRTAuto ARG_LIST((UINT4 * , INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRD ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4, tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4, INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfVniMapSentPkts ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfVniMapRcvdPkts ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfVniMapDroppedPkts ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRDAuto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRT ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRTRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4, UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanVrfRTAuto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanOrdinalNum ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanMultihomedPeerRowStatus ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanUdpPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanNotificationCntl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */
INT1
nmhDepv2FsEvpnVxlanEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEvpnAnycastGwMac ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVxlanVtepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanNveTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanMCastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanVniVlanMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVxlanInReplicaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */
INT1
nmhDepv2FsEvpnVxlanEviVniMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvpnVxlanBgpRTTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvpnVxlanVrfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvpnVxlanVrfRTTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvpnVxlanMultihomedPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVxlanEcmpNveTable. */  
INT1 
nmhValidateIndexInstanceFsVxlanEcmpNveTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsVxlanEcmpNveTable  */ 

INT1  
nmhGetFirstIndexFsVxlanEcmpNveTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));                                        
/* Proto type for GET_NEXT Routine.  */ 
INT1
nmhGetNextIndexFsVxlanEcmpNveTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVxlanEcmpNveVtepAddressType ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsVxlanEcmpNveVtepAddress ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVxlanEcmpNveStorageType ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsVxlanEcmpSuppressArp ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsVxlanEcmpMHEviVniESI ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVxlanEcmpActive ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsVxlanEcmpVrfName ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVxlanEcmpVrfName ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVxlanEcmpVrfName ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsEvpnVxlanArpTable. */
INT1
nmhValidateIndexInstanceFsEvpnVxlanArpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsEvpnVxlanArpTable  */

INT1
nmhGetFirstIndexFsEvpnVxlanArpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvpnVxlanArpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvpnVxlanArpDestMac ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,tMacAddr * ));

INT1
nmhGetFsEvpnVxlanArpStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsEvpnVxlanArpRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvpnVxlanArpDestMac ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tMacAddr ));

INT1
nmhSetFsEvpnVxlanArpRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvpnVxlanArpDestMac ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tMacAddr ));

INT1
nmhTestv2FsEvpnVxlanArpRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvpnVxlanArpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

