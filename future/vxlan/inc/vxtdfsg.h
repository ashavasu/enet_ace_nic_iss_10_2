/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxtdfsg.h,v 1.8 2018/01/05 09:57:10 siva Exp $
*
* Description: This file contains data structures defined for Vxlan module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsVxlanVtepEntry */

typedef struct
{
 BOOL1  bFsVxlanVtepNveIfIndex; 
 BOOL1  bFsVxlanVtepAddressType; 
 BOOL1  bFsVxlanVtepAddress; 
 BOOL1  bFsVxlanVtepRowStatus; 
} tVxlanIsSetFsVxlanVtepEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsVxlanNveEntry */

typedef struct
{
 BOOL1  bFsVxlanNveIfIndex;
 BOOL1  bFsVxlanNveVniNumber;
 BOOL1  bFsVxlanNveDestVmMac;
 BOOL1  bFsVxlanNveRemoteVtepAddressType;
 BOOL1  bFsVxlanNveRemoteVtepAddress;
 BOOL1  bFsVxlanNveStorageType;
 BOOL1  bFsVxlanSuppressArp;
 BOOL1  bFsVxlanNveRowStatus; 
} tVxlanIsSetFsVxlanNveEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsVxlanMCastEntry */

typedef struct
{
 BOOL1  bFsVxlanMCastNveIfIndex;
 BOOL1  bFsVxlanMCastVniNumber;
 BOOL1  bFsVxlanMCastGroupAddressType;
 BOOL1  bFsVxlanMCastGroupAddress;
 BOOL1  bFsVxlanMCastRowStatus; 
} tVxlanIsSetFsVxlanMCastEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsVxlanVniVlanMapEntry */

typedef struct
{
 BOOL1  bFsVxlanVniVlanMapVlanId;
 BOOL1  bFsVxlanVniVlanMapVniNumber;
 BOOL1  bFsVxlanVniVlanMapPktSent;
 BOOL1  bFsVxlanVniVlanMapPktRcvd;
 BOOL1  bFsVxlanVniVlanMapPktDrpd;
 BOOL1  bFsVxlanVniVlanMapRowStatus;
 BOOL1  bFsVxlanVniVlanVlanTagged;
} tVxlanIsSetFsVxlanVniVlanMapEntry;

/* Structure used by CLI to indicate which
         all objects to be set in FsVxlanInReplicaEntry */

typedef struct
{
        BOOL1  bFsVxlanInReplicaNveIfIndex;
        BOOL1  bFsVxlanInReplicaVniNumber;
        BOOL1  bFsVxlanInReplicaIndex;
        BOOL1  bFsVxlanInReplicaRemoteVtepAddressType;
        BOOL1  bFsVxlanInReplicaRemoteVtepAddress;
        BOOL1  bFsVxlanInReplicaRowStatus;
} tVxlanIsSetFsVxlanInReplicaEntry;

#ifdef EVPN_VXLAN_WANTED
/* Structure used by CLI to indicate which 
 all objects to be set in FsEvpnVxlanEviVniMapEntry */

typedef struct
{
 BOOL1  bFsEvpnVxlanEviVniMapEviIndex;
 BOOL1  bFsEvpnVxlanEviVniMapVniNumber;
 BOOL1  bFsEvpnVxlanEviVniMapBgpRD;
 BOOL1  bFsEvpnVxlanEviVniESI;
 BOOL1  bFsEvpnVxlanEviVniLoadBalance;
 BOOL1  bFsEvpnVxlanEviVniMapSentPkts;
 BOOL1  bFsEvpnVxlanEviVniMapRcvdPkts;
 BOOL1  bFsEvpnVxlanEviVniMapDroppedPkts;
 BOOL1  bFsEvpnVxlanEviVniMapRowStatus;
 BOOL1  bFsEvpnVxlanEviVniMapBgpRDAuto;
} tVxlanIsSetFsEvpnVxlanEviVniMapEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsEvpnVxlanBgpRTEntry */

typedef struct
{
 BOOL1  bFsEvpnVxlanBgpRTIndex;
 BOOL1  bFsEvpnVxlanBgpRTType;
 BOOL1  bFsEvpnVxlanBgpRT;
 BOOL1  bFsEvpnVxlanBgpRTRowStatus;
 BOOL1  bFsEvpnVxlanBgpRTAuto;
 BOOL1  bFsEvpnVxlanEviVniMapEviIndex;
 BOOL1  bFsEvpnVxlanEviVniMapVniNumber;
} tVxlanIsSetFsEvpnVxlanBgpRTEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsEvpnVxlanVrfEntry */

typedef struct
{
 BOOL1  bFsEvpnVxlanVrfName;
 BOOL1  bFsEvpnVxlanVrfRD;
 BOOL1  bFsEvpnVxlanVrfVniNumber;
 BOOL1  bFsEvpnVxlanVrfRDAuto;
 BOOL1  bFsEvpnVxlanVrfRowStatus;
 BOOL1  bFsEvpnVxlanVrfVniMapSentPkts;
 BOOL1  bFsEvpnVxlanVrfVniMapRcvdPkts;
 BOOL1  bFsEvpnVxlanVrfVniMapDroppedPkts;
} tVxlanIsSetFsEvpnVxlanVrfEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsEvpnVxlanVrfRTEntry */

typedef struct
{
 BOOL1  bFsEvpnVxlanVrfRTIndex;
 BOOL1  bFsEvpnVxlanVrfRTType;
 BOOL1  bFsEvpnVxlanVrfRT;
 BOOL1  bFsEvpnVxlanVrfRTRowStatus;
 BOOL1  bFsEvpnVxlanVrfName;
 BOOL1  bFsEvpnVxlanVrfVniNumber;
 BOOL1  bFsEvpnVxlanVrfRTAuto;
} tVxlanIsSetFsEvpnVxlanVrfRTEntry;

/* Structure used by CLI to indicate which 
 all objects to be set in FsEvpnVxlanMultihomedPeerTable */

typedef struct
{
 BOOL1  bFsEvpnVxlanPeerIpAddressType;
 BOOL1  bFsEvpnVxlanPeerIpAddress;
 BOOL1  bFsEvpnVxlanMHEviVniESI;
 BOOL1  bFsEvpnVxlanOrdinalNum;
 BOOL1  bFsEvpnVxlanMultihomedPeerRowStatus;
} tVxlanIsSetFsEvpnVxlanMultihomedPeerTable;


#endif /* EVPN_VXLAN_WANTED */

/* Structure used by Vxlan protocol for FsVxlanVtepEntry */

typedef struct
{
 tRBNodeEmbd  FsVxlanVtepTableNode; /*RBTree Node */
 INT4 i4FsVxlanVtepNveIfIndex; /* Key for this RB Tree.Denotes the nve if index. */
 INT4 i4FsVxlanVtepAddressType; /* Denotes the VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanVtepRowStatus; /* Denotes the Row status value */
 INT4 i4FsVxlanVtepAddressLen; /* Denotes the VTEP Address len - 4 for IPV4, 16 for IPV6 */
 UINT1 au1FsVxlanVtepAddress[16]; /* Denotes the VTEP Address */
} tVxlanMibFsVxlanVtepEntry;

/* Structure used by Vxlan protocol for FsVxlanNveEntry */

typedef struct
{
 tRBNodeEmbd  FsVxlanNveTableNode; /*RBTree Node */
    tDbTblNode    NveDbNode;
 UINT4 u4FsVxlanNveVniNumber; /* Key for this RB Tree.Denotes the VNI number. */
 INT4 i4FsVxlanNveIfIndex; /* Key for this RB Tree.Denotes the nve if index. */
 INT4 i4FsVxlanNveVtepAddressType; /* Denotes the VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanNveRemoteVtepAddressType; /* Denotes the Remote VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanNveStorageType; /* Denotes the storage type */
 INT4 i4FsVxlanSuppressArp;    /* Denotes suppress ARP */
 INT4 i4FsVxlanNveRowStatus; /* Denotes the VTEP Address */
 INT4 i4FsVxlanNveVtepAddressLen; /* Denotes the VTEP Address len - 4 for IPV4, 16 for IPV6 */
 INT4 i4FsVxlanNveRemoteVtepAddressLen; /* Denotes the Remote VTEP Address len - 4 for IPV4, 16 for IPV6 */
 UINT1 au1FsVxlanNveVtepAddress[16];  /* Denotes the VTEP Address */
 UINT1 au1FsVxlanNveRemoteVtepAddress[16];  /* Denotes the Remote VTEP Address */
 UINT1 au1FsEvpnVxlanVrfName[EVPN_MAX_VRF_NAME_LEN];
 tMacAddr FsVxlanNveDestVmMac; /* Key for this RB Tree.Denotes the destination VM mac. */
 UINT1    u1Rsvd[2];
} tVxlanMibFsVxlanNveEntry;

/* Structure used by Vxlan protocol for FsVxlanEcmpNveEntry */

typedef struct
{
 tRBNodeEmbd  FsVxlanEcmpNveTableNode; /*RBTree Node */
    tDbTblNode    EcmpNveDbNode;
 UINT4 u4FsVxlanEcmpNveVniNumber; /* Key for this RB Tree.Denotes the VNI number. */
 INT4 i4FsVxlanEcmpNveIfIndex; /* Key for this RB Tree.Denotes the nve if index. */
 INT4 i4FsVxlanEcmpNveVtepAddressType; /* Denotes the VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanEcmpNveRemoteVtepAddressType; /* Denotes the Remote VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanEcmpNveStorageType; /* Denotes the storage type */
 INT4 i4FsVxlanEcmpSuppressArp;    /* Denotes suppress ARP */
 INT4 i4FsVxlanEcmpNveVtepAddressLen; /* Denotes the VTEP Address len - 4 for IPV4, 16 for IPV6 */
 INT4 i4FsVxlanEcmpNveRemoteVtepAddressLen; /* Denotes the Remote VTEP Address len - 4 for IPV4, 16 for IPV6 */
 UINT1 au1FsVxlanEcmpNveVtepAddress[16];  /* Denotes the VTEP Address */
 UINT1 au1FsVxlanEcmpNveRemoteVtepAddress[16];  /* Denotes the Remote VTEP Address */
 UINT1 au1FsVxlanEcmpMHEviVniESI[10];
 UINT2    u2Rsvd;
 tMacAddr FsVxlanEcmpNveDestVmMac; /* Key for this RB Tree.Denotes the destination VM mac. */
 BOOL1    bFsVxlanEcmpActive;      /* Denotes whether this Peer is Active or standby */ 
 BOOL1    bFsVxlanEcmpMacLearnt;   /* Denotes whether Mac learnt from this Remote Peer or not */
} tVxlanMibFsVxlanEcmpNveEntry;


/* Structure used by Vxlan protocol for FsVxlanMCastEntry */

typedef struct
{
 tRBNodeEmbd  FsVxlanMCastTableNode; /*RBTree Node */
 UINT4 u4FsVxlanMCastVniNumber; /* Key for this RB Tree.Denotes the VNI number. */
 INT4 i4FsVxlanMCastNveIfIndex; /* Key for this RB Tree.Denotes the nve if index. */
 INT4 i4FsVxlanMCastGroupAddressType; /* Denotes the Mcast Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanMCastVtepAddressType; /* Denotes the VTEP Address type - IPV4 or IPV6 */
 INT4 i4FsVxlanMCastRowStatus; /* Denotes the Row status value */
    INT4 i4FsVxlanMCastVtepAddressLen; /* Denotes the VTEP Address len - 4 for IPV4, 16 for IPV6 */
 INT4 i4FsVxlanMCastGroupAddressLen; /* Denotes the Mcast Address len - 4 for IPV4, 16 for IPV6 */
 UINT1 au1FsVxlanMCastGroupAddress[16]; /* Denotes the VTEP Address */
    UINT1 au1FsVxlanMCastVtepAddress[16]; /* Denotes the Mcast Address */
} tVxlanMibFsVxlanMCastEntry;

/* Structure used by Vxlan protocol for FsVxlanVniVlanMapEntry */

typedef struct
{
 tRBNodeEmbd  FsVxlanVniVlanMapTableNode; /*RBTree Node */
 UINT4 u4FsVxlanVniVlanMapVniNumber; /*Denotes the VNI number. */
 INT4 i4FsVxlanVniVlanMapVlanId; /* Key for this RB Tree.Denotes the VLAN id. */
 UINT4 u4FsVxlanVniVlanMapPktSent; /* Denotes the VXLAN packet sent counter */
 UINT4 u4FsVxlanVniVlanMapPktRcvd; /* Denotes the VXLAN packet received counter */
 UINT4 u4FsVxlanVniVlanMapPktDrpd; /* Denotes the VXLAN packet dropped counter */
 INT4 i4FsVxlanVniVlanMapRowStatus; /* Denotes the Row status value */
 INT4 i4FsVxlanVniVlanDfElection; /* Denotes the DF Election Flag */
 UINT1 au1FsVxlanVniVlanVrfName[36];  /* Vrf Name associated with Vlan */
 INT4 i4FsVxlanVniVlanVrfNameLen;
 UINT1 u1OperStatus;             /* Denotes the Oper status of the Vlan */
 UINT1 u1IsVlanTagged;
 UINT1 au1Pad[2];
} tVxlanMibFsVxlanVniVlanMapEntry;

/* Structure used by Vxlan protocol for FsVxlanInReplicaEntry */

typedef struct
{
        tRBNodeEmbd  FsVxlanInReplicaTableNode; /*RBTree Node */
        INT4 i4FsVxlanInReplicaNveIfIndex; /* Key for this RB Tree.Denotes the nve if index. */
        UINT4 u4FsVxlanInReplicaVniNumber; /* Key for this RB Tree.Denotes the VNI number. */
        INT4 i4FsVxlanInReplicaIndex;     /* Key for this RB Tree.Denotes the index for getting Remote VTEP address. */
        INT4 i4FsVxlanInReplicaVtepAddressType; /* Denotes the VTEP Address type - IPV4 or IPV6 */
        INT4 i4FsVxlanInReplicaRemoteVtepAddressType; /* Denotes the Remote VTEP Address type - IPV4 or IPV6 */
        INT4 i4FsVxlanInReplicaVtepAddressLen; /* Denotes the VTEP Address len - 4 for IPV4, 16 for IPV6 */
        INT4 i4FsVxlanInReplicaRemoteVtepAddressLen; /* Denotes the Remote VTEP Address len - 4 for IPV4, 16 for IPV6 */
        UINT1 au1FsVxlanInReplicaVtepAddress[16];  /* Denotes the VTEP Address */
        UINT1 au1FsVxlanInReplicaRemoteVtepAddress[16];/* Denotes Remote VTEP Address */
        INT4 i4FsVxlanInReplicaRowStatus;
}tVxlanMibFsVxlanInReplicaEntry;       

/* Structure used by Vxlan protocol for FsEvpnVxlanEviVniMapEntry */

typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanEviVniMapTableNode;
 UINT4 u4FsEvpnVxlanEviVniMapVniNumber;
 UINT4 u4FsEvpnVxlanEviVniMapSentPkts;
 UINT4 u4FsEvpnVxlanEviVniMapRcvdPkts;
 UINT4 u4FsEvpnVxlanEviVniMapDroppedPkts;
 INT4 i4FsEvpnVxlanEviVniMapEviIndex;
 INT4 i4FsEvpnVxlanEviVniLoadBalance;
 INT4 i4FsEvpnVxlanEviVniMapRowStatus;
 INT4 i4FsEvpnVxlanEviVniMapBgpRDAuto;
 INT4 i4FsEvpnVxlanEviVniMapBgpRDLen;
 INT4 i4FsEvpnVxlanEviVniESILen;
 INT4 i4FsEvpnVxlanEviVniMapTotalPaths;
 UINT1 au1FsEvpnVxlanEviVniMapBgpRD[8];
 UINT1 au1FsEvpnVxlanEviVniESI[10];
 UINT1 au1Padding[2];
} tVxlanMibFsEvpnVxlanEviVniMapEntry;

/* Structure used by Vxlan protocol for FsEvpnVxlanBgpRTEntry */

typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanBgpRTTableNode;
 UINT4 u4FsEvpnVxlanBgpRTIndex;
 UINT4 u4FsEvpnVxlanEviVniMapVniNumber;
 INT4 i4FsEvpnVxlanBgpRTType;
 INT4 i4FsEvpnVxlanBgpRTRowStatus;
 INT4 i4FsEvpnVxlanBgpRTAuto;
 INT4 i4FsEvpnVxlanEviVniMapEviIndex;
 INT4 i4FsEvpnVxlanBgpRTLen;
 UINT1 au1FsEvpnVxlanBgpRT[8];
} tVxlanMibFsEvpnVxlanBgpRTEntry;

/* Structure used by Vxlan protocol for FsEvpnVxlanVrfEntry */

typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanVrfTableNode;
 INT4 i4FsEvpnVxlanVrfRowStatus;
 INT4 i4FsEvpnVxlanVrfNameLen;
 INT4 i4FsEvpnVxlanVrfRDLen;
 UINT4 u4FsEvpnVxlanVrfVniNumber;
 INT4 i4FsEvpnVxlanVrfRDAuto;
 UINT4 u4FsEvpnVxlanVrfVniMapSentPkts;
 UINT4 u4FsEvpnVxlanVrfVniMapRcvdPkts;
 UINT4 u4FsEvpnVxlanVrfVniMapDroppedPkts;
 UINT1 au1FsEvpnVxlanVrfName[36];
 UINT1 au1FsEvpnVxlanVrfRD[8];
} tVxlanMibFsEvpnVxlanVrfEntry;

/* Structure used by Vxlan protocol for FsEvpnVxlanVrfRTEntry */

typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanVrfRTTableNode;
 UINT4 u4FsEvpnVxlanVrfRTIndex;
 INT4 i4FsEvpnVxlanVrfRTType;
 INT4 i4FsEvpnVxlanVrfRTRowStatus;
 INT4 i4FsEvpnVxlanVrfRTLen;
 INT4 i4FsEvpnVxlanVrfNameLen;
 INT4 i4FsEvpnVxlanVrfRTAuto;
 UINT4 u4FsEvpnVxlanVrfVniNumber;
 UINT1 au1FsEvpnVxlanVrfRT[8];
 UINT1 au1FsEvpnVxlanVrfName[36];
} tVxlanMibFsEvpnVxlanVrfRTEntry;

/* Structure used by Vxlan protocol for FsEvpnVxlanMultihomedPeerTable */

typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanMultihomedPeerTableNode;
 UINT4 u4FsEvpnVxlanOrdinalNum;
 INT4 i4FsEvpnVxlanPeerIpAddressType;
 INT4 i4FsEvpnVxlanMultihomedPeerRowStatus;
 INT4 i4FsEvpnVxlanPeerIpAddressLen;
 INT4 i4FsEvpnVxlanMHEviVniESILen;
 UINT1 au1FsEvpnVxlanPeerIpAddress[16];
 UINT1 au1FsEvpnVxlanMHEviVniESI[10];
 UINT1 au1Padding[6];
} tVxlanMibFsEvpnVxlanMultihomedPeerTable;

/* Structure used by Vxlan protocol for FsEvpnVxlanArpTable */
typedef struct
{
 tRBNodeEmbd  FsEvpnVxlanArpTableNode;
 INT4 i4FsEvpnVxlanVrfNameLen;
 UINT1 au1FsEvpnVxlanVrfName[36];
 INT4  i4FsEvpnVxlanArpDestAddressType; /* Denotes the Remote VM's Address type - IPV4 or IPV6 */
 UINT1 au1FsEvpnVxlanArpDestAddres[16];  /* Denotes the Destination VM's Address */
 INT4  i4FsEvpnVxlanArpDestAddresLen;
 INT4 i4FsVxlanEvpnArpStorageType; /* Denotes the storage type */
 INT4 i4FsVxlanArpRowStatus; /* Denotes the VTEP Address */
 tMacAddr FsEvpnVxlanArpDestMac;  /* Denotes the Remote VTEP's Mac Address*/
 UINT1   au1Pad[2];
} tVxlanMibFsEvpnVxlanArpEntry;

