/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: fsvxdb.h,v 1.8 2018/01/05 09:57:09 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/


#ifndef _FSVXLADB_H
#define _FSVXLADB_H

UINT1 FsVxlanVtepTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsVxlanNveTableINDEX [] = {SNMP_DATA_TYPE_INTEGER,SNMP_DATA_TYPE_UNSIGNED32,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsVxlanMCastTableINDEX [] = {SNMP_DATA_TYPE_INTEGER,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsVxlanVniVlanMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsVxlanInReplicaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvpnVxlanEviVniMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEvpnVxlanBgpRTTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32,SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvpnVxlanVrfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsEvpnVxlanVrfRTTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvpnVxlanMultihomedPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER,SNMP_DATA_TYPE_OCTET_PRIM,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVxlanEcmpNveTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsEvpnVxlanArpTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsvxla [] ={1,3,6,1,4,1,29601,2,89};
tSNMP_OID_TYPE fsvxlaOID = {9, fsvxla};


UINT4 FsVxlanEnable [ ] ={1,3,6,1,4,1,29601,2,89,1,1,1};
UINT4 FsVxlanUdpPort [ ] ={1,3,6,1,4,1,29601,2,89,1,1,2};
UINT4 FsVxlanTraceOption [ ] ={1,3,6,1,4,1,29601,2,89,1,1,3};
UINT4 FsVxlanNotificationCntl [ ] ={1,3,6,1,4,1,29601,2,89,1,1,4};
UINT4 FsEvpnVxlanEnable [ ] ={1,3,6,1,4,1,29601,2,89,1,1,5};
UINT4 FsEvpnAnycastGwMac [ ] ={1,3,6,1,4,1,29601,2,89,1,1,6};
UINT4 FsVxlanVtepNveIfIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,1,1,1};
UINT4 FsVxlanVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,1,1,2};
UINT4 FsVxlanVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,1,1,3};
UINT4 FsVxlanVtepRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,1,1,4};
UINT4 FsVxlanNveIfIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,1};
UINT4 FsVxlanNveVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,2};
UINT4 FsVxlanNveDestVmMac [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,3};
UINT4 FsVxlanNveVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,4};
UINT4 FsVxlanNveVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,5};
UINT4 FsVxlanNveRemoteVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,6};
UINT4 FsVxlanNveRemoteVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,7};
UINT4 FsVxlanNveStorageType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,8};
UINT4 FsVxlanNveRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,9};
UINT4 FsVxlanSuppressArp [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,10};
UINT4 FsVxlanNveVrfName [ ] ={1,3,6,1,4,1,29601,2,89,1,2,2,1,11};
UINT4 FsVxlanMCastNveIfIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,1};
UINT4 FsVxlanMCastVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,2};
UINT4 FsVxlanMCastGroupAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,3};
UINT4 FsVxlanMCastGroupAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,4};
UINT4 FsVxlanMCastVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,5};
UINT4 FsVxlanMCastVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,6};
UINT4 FsVxlanMCastRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,3,1,7};
UINT4 FsVxlanVniVlanMapVlanId [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,1};
UINT4 FsVxlanVniVlanMapVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,2};
UINT4 FsVxlanVniVlanMapPktSent [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,3};
UINT4 FsVxlanVniVlanMapPktRcvd [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,4};
UINT4 FsVxlanVniVlanMapPktDrpd [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,5};
UINT4 FsVxlanVniVlanMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,6};
UINT4 FsVxlanVniVlanDfElection [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,7};
UINT4 FsVxlanVniVlanTagStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,4,1,8};
UINT4 FsVxlanInReplicaNveIfIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,1};
UINT4 FsVxlanInReplicaVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,2};
UINT4 FsVxlanInReplicaIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,3};
UINT4 FsVxlanInReplicaVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,4};
UINT4 FsVxlanInReplicaVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,5};
UINT4 FsVxlanInReplicaRemoteVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,6};
UINT4 FsVxlanInReplicaRemoteVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,7};
UINT4 FsVxlanInReplicaRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,5,1,8};
UINT4 FsEvpnVxlanEviVniMapEviIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,1};
UINT4 FsEvpnVxlanEviVniMapVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,2};
UINT4 FsEvpnVxlanEviVniMapBgpRD [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,3};
UINT4 FsEvpnVxlanEviVniESI [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,4};
UINT4 FsEvpnVxlanEviVniLoadBalance [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,5};
UINT4 FsEvpnVxlanEviVniMapSentPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,6};
UINT4 FsEvpnVxlanEviVniMapRcvdPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,7};
UINT4 FsEvpnVxlanEviVniMapDroppedPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,8};
UINT4 FsEvpnVxlanEviVniMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,9};
UINT4 FsEvpnVxlanEviVniMapBgpRDAuto [ ] ={1,3,6,1,4,1,29601,2,89,1,2,6,1,10};
UINT4 FsEvpnVxlanBgpRTIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,7,1,1};
UINT4 FsEvpnVxlanBgpRTType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,7,1,2};
UINT4 FsEvpnVxlanBgpRT [ ] ={1,3,6,1,4,1,29601,2,89,1,2,7,1,3};
UINT4 FsEvpnVxlanBgpRTRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,7,1,4};
UINT4 FsEvpnVxlanBgpRTAuto [ ] ={1,3,6,1,4,1,29601,2,89,1,2,7,1,5};
UINT4 FsEvpnVxlanVrfName [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,1};
UINT4 FsEvpnVxlanVrfRD [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,2};
UINT4 FsEvpnVxlanVrfRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,3};
UINT4 FsEvpnVxlanVrfVniMapSentPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,4};
UINT4 FsEvpnVxlanVrfVniMapRcvdPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,5};
UINT4 FsEvpnVxlanVrfVniMapDroppedPkts [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,6};
UINT4 FsEvpnVxlanVrfRDAuto [ ] ={1,3,6,1,4,1,29601,2,89,1,2,8,1,7};
UINT4 FsEvpnVxlanVrfRTIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,9,1,1};
UINT4 FsEvpnVxlanVrfRTType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,9,1,2};
UINT4 FsEvpnVxlanVrfRT [ ] ={1,3,6,1,4,1,29601,2,89,1,2,9,1,3};
UINT4 FsEvpnVxlanVrfRTRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,9,1,4};
UINT4 FsEvpnVxlanVrfRTAuto [ ] ={1,3,6,1,4,1,29601,2,89,1,2,9,1,5};
UINT4 FsEvpnVxlanPeerIpAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,10,1,1};
UINT4 FsEvpnVxlanPeerIpAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,10,1,2};
UINT4 FsEvpnVxlanMHEviVniESI [ ] ={1,3,6,1,4,1,29601,2,89,1,2,10,1,3};
UINT4 FsEvpnVxlanOrdinalNum [ ] ={1,3,6,1,4,1,29601,2,89,1,2,10,1,4};
UINT4 FsEvpnVxlanMultihomedPeerRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,10,1,5};
UINT4 FsVxlanEcmpNveIfIndex [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,1};
UINT4 FsVxlanEcmpNveVniNumber [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,2};  
UINT4 FsVxlanEcmpNveDestVmMac [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,3};
UINT4 FsVxlanEcmpNveVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,4};  
UINT4 FsVxlanEcmpNveVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,5};   
UINT4 FsVxlanEcmpNveRemoteVtepAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,6};
UINT4 FsVxlanEcmpNveRemoteVtepAddress [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,7};
UINT4 FsVxlanEcmpNveStorageType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,8};  
UINT4 FsVxlanEcmpSuppressArp [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,9};
UINT4 FsVxlanEcmpMHEviVniESI [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,10};
UINT4 FsVxlanEcmpActive [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,11};
UINT4 FsVxlanEcmpVrfName [ ] ={1,3,6,1,4,1,29601,2,89,1,2,11,1,12};
UINT4 FsEvpnVxlanArpDestAddressType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,12,1,1};
UINT4 FsEvpnVxlanArpDestAddres [ ] ={1,3,6,1,4,1,29601,2,89,1,2,12,1,2};
UINT4 FsEvpnVxlanArpDestMac [ ] ={1,3,6,1,4,1,29601,2,89,1,2,12,1,3};
UINT4 FsEvpnVxlanArpStorageType [ ] ={1,3,6,1,4,1,29601,2,89,1,2,12,1,4};
UINT4 FsEvpnVxlanArpRowStatus [ ] ={1,3,6,1,4,1,29601,2,89,1,2,12,1,5};




tMbDbEntry fsvxlaMibEntry[]= {
{{12,FsVxlanEnable}, NULL, FsVxlanEnableGet, FsVxlanEnableSet, FsVxlanEnableTest, FsVxlanEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsVxlanUdpPort}, NULL, FsVxlanUdpPortGet, FsVxlanUdpPortSet, FsVxlanUdpPortTest, FsVxlanUdpPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "4789"},

{{12,FsVxlanTraceOption}, NULL, FsVxlanTraceOptionGet, FsVxlanTraceOptionSet, FsVxlanTraceOptionTest, FsVxlanTraceOptionDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsVxlanNotificationCntl}, NULL, FsVxlanNotificationCntlGet, FsVxlanNotificationCntlSet, FsVxlanNotificationCntlTest, FsVxlanNotificationCntlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsEvpnVxlanEnable}, NULL, FsEvpnVxlanEnableGet, FsEvpnVxlanEnableSet, FsEvpnVxlanEnableTest, FsEvpnVxlanEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsEvpnAnycastGwMac}, NULL, FsEvpnAnycastGwMacGet, FsEvpnAnycastGwMacSet, FsEvpnAnycastGwMacTest, FsEvpnAnycastGwMacDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsVxlanVtepNveIfIndex}, GetNextIndexFsVxlanVtepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanVtepTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVtepAddressType}, GetNextIndexFsVxlanVtepTable, FsVxlanVtepAddressTypeGet, FsVxlanVtepAddressTypeSet, FsVxlanVtepAddressTypeTest, FsVxlanVtepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanVtepTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVtepAddress}, GetNextIndexFsVxlanVtepTable, FsVxlanVtepAddressGet, FsVxlanVtepAddressSet, FsVxlanVtepAddressTest, FsVxlanVtepTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVxlanVtepTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVtepRowStatus}, GetNextIndexFsVxlanVtepTable, FsVxlanVtepRowStatusGet, FsVxlanVtepRowStatusSet, FsVxlanVtepRowStatusTest, FsVxlanVtepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanVtepTableINDEX, 1, 0, 1, NULL},

{{14,FsVxlanNveIfIndex}, GetNextIndexFsVxlanNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveVniNumber}, GetNextIndexFsVxlanNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveDestVmMac}, GetNextIndexFsVxlanNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveVtepAddressType}, GetNextIndexFsVxlanNveTable, FsVxlanNveVtepAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveVtepAddress}, GetNextIndexFsVxlanNveTable, FsVxlanNveVtepAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveRemoteVtepAddressType}, GetNextIndexFsVxlanNveTable, FsVxlanNveRemoteVtepAddressTypeGet, FsVxlanNveRemoteVtepAddressTypeSet, FsVxlanNveRemoteVtepAddressTypeTest, FsVxlanNveTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveRemoteVtepAddress}, GetNextIndexFsVxlanNveTable, FsVxlanNveRemoteVtepAddressGet, FsVxlanNveRemoteVtepAddressSet, FsVxlanNveRemoteVtepAddressTest, FsVxlanNveTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveStorageType}, GetNextIndexFsVxlanNveTable, FsVxlanNveStorageTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanNveRowStatus}, GetNextIndexFsVxlanNveTable, FsVxlanNveRowStatusGet, FsVxlanNveRowStatusSet, FsVxlanNveRowStatusTest, FsVxlanNveTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanNveTableINDEX, 3, 0, 1, NULL},

{{14,FsVxlanSuppressArp}, GetNextIndexFsVxlanNveTable, FsVxlanSuppressArpGet, FsVxlanSuppressArpSet, FsVxlanSuppressArpTest, FsVxlanNveTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanNveTableINDEX, 3, 0, 0, "2"},

{{14,FsVxlanNveVrfName}, GetNextIndexFsVxlanNveTable, FsVxlanNveVrfNameGet, FsVxlanNveVrfNameSet, FsVxlanNveVrfNameTest, FsVxlanNveTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVxlanNveTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanMCastNveIfIndex}, GetNextIndexFsVxlanMCastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastVniNumber}, GetNextIndexFsVxlanMCastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastGroupAddressType}, GetNextIndexFsVxlanMCastTable, FsVxlanMCastGroupAddressTypeGet, FsVxlanMCastGroupAddressTypeSet, FsVxlanMCastGroupAddressTypeTest, FsVxlanMCastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastGroupAddress}, GetNextIndexFsVxlanMCastTable, FsVxlanMCastGroupAddressGet, FsVxlanMCastGroupAddressSet, FsVxlanMCastGroupAddressTest, FsVxlanMCastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastVtepAddressType}, GetNextIndexFsVxlanMCastTable, FsVxlanMCastVtepAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastVtepAddress}, GetNextIndexFsVxlanMCastTable, FsVxlanMCastVtepAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanMCastTableINDEX, 2, 0, 0, NULL},

{{14,FsVxlanMCastRowStatus}, GetNextIndexFsVxlanMCastTable, FsVxlanMCastRowStatusGet, FsVxlanMCastRowStatusSet, FsVxlanMCastRowStatusTest, FsVxlanMCastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanMCastTableINDEX, 2, 0, 1, NULL},

{{14,FsVxlanVniVlanMapVlanId}, GetNextIndexFsVxlanVniVlanMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVniVlanMapVniNumber}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanMapVniNumberGet, FsVxlanVniVlanMapVniNumberSet, FsVxlanVniVlanMapVniNumberTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVniVlanMapPktSent}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanMapPktSentGet, FsVxlanVniVlanMapPktSentSet, FsVxlanVniVlanMapPktSentTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVniVlanMapPktRcvd}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanMapPktRcvdGet, FsVxlanVniVlanMapPktRcvdSet, FsVxlanVniVlanMapPktRcvdTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVniVlanMapPktDrpd}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanMapPktDrpdGet, FsVxlanVniVlanMapPktDrpdSet, FsVxlanVniVlanMapPktDrpdTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanVniVlanMapRowStatus}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanMapRowStatusGet, FsVxlanVniVlanMapRowStatusSet, FsVxlanVniVlanMapRowStatusTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 1, NULL},

{{14,FsVxlanVniVlanDfElection}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanDfElectionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, "2"},

{{14,FsVxlanVniVlanTagStatus}, GetNextIndexFsVxlanVniVlanMapTable, FsVxlanVniVlanTagStatusGet, FsVxlanVniVlanTagStatusSet, FsVxlanVniVlanTagStatusTest, FsVxlanVniVlanMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanVniVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsVxlanInReplicaNveIfIndex}, GetNextIndexFsVxlanInReplicaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanInReplicaVniNumber}, GetNextIndexFsVxlanInReplicaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanInReplicaIndex}, GetNextIndexFsVxlanInReplicaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER,SNMP_NOACCESS, FsVxlanInReplicaTableINDEX, 3, 0, 0,NULL},

{{14,FsVxlanInReplicaVtepAddressType}, GetNextIndexFsVxlanInReplicaTable, FsVxlanInReplicaVtepAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanInReplicaVtepAddress}, GetNextIndexFsVxlanInReplicaTable, FsVxlanInReplicaVtepAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanInReplicaRemoteVtepAddressType}, GetNextIndexFsVxlanInReplicaTable, FsVxlanInReplicaRemoteVtepAddressTypeGet, FsVxlanInReplicaRemoteVtepAddressTypeSet, FsVxlanInReplicaRemoteVtepAddressTypeTest, FsVxlanInReplicaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},
{{14,FsVxlanInReplicaRemoteVtepAddress}, GetNextIndexFsVxlanInReplicaTable, FsVxlanInReplicaRemoteVtepAddressGet,FsVxlanInReplicaRemoteVtepAddressSet, FsVxlanInReplicaRemoteVtepAddressTest, FsVxlanInReplicaTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVxlanInReplicaTableINDEX, 3, 0, 0, NULL},

{{14,FsVxlanInReplicaRowStatus}, GetNextIndexFsVxlanInReplicaTable, FsVxlanInReplicaRowStatusGet, FsVxlanInReplicaRowStatusSet, FsVxlanInReplicaRowStatusTest, FsVxlanInReplicaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVxlanInReplicaTableINDEX, 3, 0, 1, NULL},

{{14,FsEvpnVxlanEviVniMapEviIndex}, GetNextIndexFsEvpnVxlanEviVniMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniMapVniNumber}, GetNextIndexFsEvpnVxlanEviVniMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniMapBgpRD}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapBgpRDGet, FsEvpnVxlanEviVniMapBgpRDSet, FsEvpnVxlanEviVniMapBgpRDTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniESI}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniESIGet, FsEvpnVxlanEviVniESISet, FsEvpnVxlanEviVniESITest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniLoadBalance}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniLoadBalanceGet, FsEvpnVxlanEviVniLoadBalanceSet, FsEvpnVxlanEviVniLoadBalanceTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, "2"},

{{14,FsEvpnVxlanEviVniMapSentPkts}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapSentPktsGet, FsEvpnVxlanEviVniMapSentPktsSet, FsEvpnVxlanEviVniMapSentPktsTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniMapRcvdPkts}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapRcvdPktsGet, FsEvpnVxlanEviVniMapRcvdPktsSet, FsEvpnVxlanEviVniMapRcvdPktsTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniMapDroppedPkts}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapDroppedPktsGet, FsEvpnVxlanEviVniMapDroppedPktsSet, FsEvpnVxlanEviVniMapDroppedPktsTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanEviVniMapRowStatus}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapRowStatusGet, FsEvpnVxlanEviVniMapRowStatusSet, FsEvpnVxlanEviVniMapRowStatusTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 1, NULL},

{{14,FsEvpnVxlanEviVniMapBgpRDAuto}, GetNextIndexFsEvpnVxlanEviVniMapTable, FsEvpnVxlanEviVniMapBgpRDAutoGet, FsEvpnVxlanEviVniMapBgpRDAutoSet, FsEvpnVxlanEviVniMapBgpRDAutoTest, FsEvpnVxlanEviVniMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanEviVniMapTableINDEX, 2, 0, 0, "2"},

{{14,FsEvpnVxlanBgpRTIndex}, GetNextIndexFsEvpnVxlanBgpRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEvpnVxlanBgpRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanBgpRTType}, GetNextIndexFsEvpnVxlanBgpRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvpnVxlanBgpRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanBgpRT}, GetNextIndexFsEvpnVxlanBgpRTTable, FsEvpnVxlanBgpRTGet, FsEvpnVxlanBgpRTSet, FsEvpnVxlanBgpRTTest, FsEvpnVxlanBgpRTTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvpnVxlanBgpRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanBgpRTRowStatus}, GetNextIndexFsEvpnVxlanBgpRTTable, FsEvpnVxlanBgpRTRowStatusGet, FsEvpnVxlanBgpRTRowStatusSet, FsEvpnVxlanBgpRTRowStatusTest, FsEvpnVxlanBgpRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanBgpRTTableINDEX, 4, 0, 1, NULL},

{{14,FsEvpnVxlanBgpRTAuto}, GetNextIndexFsEvpnVxlanBgpRTTable, FsEvpnVxlanBgpRTAutoGet, FsEvpnVxlanBgpRTAutoSet, FsEvpnVxlanBgpRTAutoTest, FsEvpnVxlanBgpRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanBgpRTTableINDEX, 4, 0, 0, "2"},

{{14,FsEvpnVxlanVrfName}, GetNextIndexFsEvpnVxlanVrfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRD}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfRDGet, FsEvpnVxlanVrfRDSet, FsEvpnVxlanVrfRDTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRowStatus}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfRowStatusGet, FsEvpnVxlanVrfRowStatusSet, FsEvpnVxlanVrfRowStatusTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 1, NULL},

{{14,FsEvpnVxlanVrfVniMapSentPkts}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfVniMapSentPktsGet, FsEvpnVxlanVrfVniMapSentPktsSet, FsEvpnVxlanVrfVniMapSentPktsTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanVrfVniMapRcvdPkts}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfVniMapRcvdPktsGet, FsEvpnVxlanVrfVniMapRcvdPktsSet, FsEvpnVxlanVrfVniMapRcvdPktsTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanVrfVniMapDroppedPkts}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfVniMapDroppedPktsGet, FsEvpnVxlanVrfVniMapDroppedPktsSet, FsEvpnVxlanVrfVniMapDroppedPktsTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRDAuto}, GetNextIndexFsEvpnVxlanVrfTable, FsEvpnVxlanVrfRDAutoGet, FsEvpnVxlanVrfRDAutoSet, FsEvpnVxlanVrfRDAutoTest, FsEvpnVxlanVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanVrfTableINDEX, 2, 0, 0, "2"},

{{14,FsEvpnVxlanVrfRTIndex}, GetNextIndexFsEvpnVxlanVrfRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsEvpnVxlanVrfRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRTType}, GetNextIndexFsEvpnVxlanVrfRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvpnVxlanVrfRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRT}, GetNextIndexFsEvpnVxlanVrfRTTable, FsEvpnVxlanVrfRTGet, FsEvpnVxlanVrfRTSet, FsEvpnVxlanVrfRTTest, FsEvpnVxlanVrfRTTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvpnVxlanVrfRTTableINDEX, 4, 0, 0, NULL},

{{14,FsEvpnVxlanVrfRTRowStatus}, GetNextIndexFsEvpnVxlanVrfRTTable, FsEvpnVxlanVrfRTRowStatusGet, FsEvpnVxlanVrfRTRowStatusSet, FsEvpnVxlanVrfRTRowStatusTest, FsEvpnVxlanVrfRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanVrfRTTableINDEX, 4, 0, 1, NULL},

{{14,FsEvpnVxlanVrfRTAuto}, GetNextIndexFsEvpnVxlanVrfRTTable, FsEvpnVxlanVrfRTAutoGet, FsEvpnVxlanVrfRTAutoSet, FsEvpnVxlanVrfRTAutoTest, FsEvpnVxlanVrfRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanVrfRTTableINDEX, 4, 0, 0, "2"},

{{14,FsEvpnVxlanPeerIpAddressType}, GetNextIndexFsEvpnVxlanMultihomedPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvpnVxlanMultihomedPeerTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanPeerIpAddress}, GetNextIndexFsEvpnVxlanMultihomedPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsEvpnVxlanMultihomedPeerTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanMHEviVniESI}, GetNextIndexFsEvpnVxlanMultihomedPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsEvpnVxlanMultihomedPeerTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanOrdinalNum}, GetNextIndexFsEvpnVxlanMultihomedPeerTable, FsEvpnVxlanOrdinalNumGet, FsEvpnVxlanOrdinalNumSet, FsEvpnVxlanOrdinalNumTest, FsEvpnVxlanMultihomedPeerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsEvpnVxlanMultihomedPeerTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanMultihomedPeerRowStatus}, GetNextIndexFsEvpnVxlanMultihomedPeerTable, FsEvpnVxlanMultihomedPeerRowStatusGet, FsEvpnVxlanMultihomedPeerRowStatusSet, FsEvpnVxlanMultihomedPeerRowStatusTest, FsEvpnVxlanMultihomedPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanMultihomedPeerTableINDEX, 3, 0, 1, NULL},

{{14,FsVxlanEcmpNveIfIndex}, GetNextIndexFsVxlanEcmpNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveVniNumber}, GetNextIndexFsVxlanEcmpNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveDestVmMac}, GetNextIndexFsVxlanEcmpNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveVtepAddressType}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpNveVtepAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveVtepAddress}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpNveVtepAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveRemoteVtepAddressType}, GetNextIndexFsVxlanEcmpNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveRemoteVtepAddress}, GetNextIndexFsVxlanEcmpNveTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpNveStorageType}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpNveStorageTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpSuppressArp}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpSuppressArpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, "2"},

{{14,FsVxlanEcmpMHEviVniESI}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpMHEviVniESIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsVxlanEcmpActive}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, "2"},

{{14,FsVxlanEcmpVrfName}, GetNextIndexFsVxlanEcmpNveTable, FsVxlanEcmpVrfNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVxlanEcmpNveTableINDEX, 5, 0, 0, NULL},

{{14,FsEvpnVxlanArpDestAddressType}, GetNextIndexFsEvpnVxlanArpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvpnVxlanArpTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanArpDestAddres}, GetNextIndexFsEvpnVxlanArpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsEvpnVxlanArpTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanArpDestMac}, GetNextIndexFsEvpnVxlanArpTable, FsEvpnVxlanArpDestMacGet, FsEvpnVxlanArpDestMacSet, FsEvpnVxlanArpDestMacTest, FsEvpnVxlanArpTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsEvpnVxlanArpTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanArpStorageType}, GetNextIndexFsEvpnVxlanArpTable, FsEvpnVxlanArpStorageTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsEvpnVxlanArpTableINDEX, 3, 0, 0, NULL},

{{14,FsEvpnVxlanArpRowStatus}, GetNextIndexFsEvpnVxlanArpTable, FsEvpnVxlanArpRowStatusGet, FsEvpnVxlanArpRowStatusSet, FsEvpnVxlanArpRowStatusTest, FsEvpnVxlanArpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvpnVxlanArpTableINDEX, 3, 0, 1, NULL},
};
tMibData fsvxlaEntry = {91, fsvxlaMibEntry };

#endif /* _FSVXLADB_H */


