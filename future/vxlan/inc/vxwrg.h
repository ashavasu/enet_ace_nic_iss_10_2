/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxwrg.h,v 1.6 2018/01/05 09:57:10 siva Exp $
*
* Description: This file contains the prototype(wr) for Vxlan 
*********************************************************************/
#ifndef _VXLANWR_H
#define _VXLANWR_H


INT4 GetNextIndexFsVxlanVtepTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsVxlanNveTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsVxlanMCastTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsVxlanVniVlanMapTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsVxlanInReplicaTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsEvpnVxlanEviVniMapTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsEvpnVxlanBgpRTTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsEvpnVxlanVrfTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsEvpnVxlanVrfRTTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsEvpnVxlanMultihomedPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVxlanEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanUdpPortGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNotificationCntlGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnAnycastGwMacGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanSuppressArpGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveVrfNameGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapVniNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktDrpdGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanDfElectionGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanTagStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniESIGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniLoadBalanceGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapSentPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRcvdPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapDroppedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDAutoGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTAutoGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapSentPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapRcvdPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapDroppedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDAutoGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTAutoGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanOrdinalNumGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanMultihomedPeerRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanUdpPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanNotificationCntlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnAnycastGwMacTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanSuppressArpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveVrfNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapVniNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktSentTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktRcvdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktDrpdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanTagStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniESITest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniLoadBalanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapSentPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRcvdPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapDroppedPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDAutoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTAutoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapSentPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapRcvdPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapDroppedPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDAutoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTAutoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanOrdinalNumTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanMultihomedPeerRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVxlanEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanUdpPortSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNotificationCntlSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnAnycastGwMacSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVtepRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRemoteVtepAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanSuppressArpSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveVrfNameSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanNveRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastGroupAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanMCastRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapVniNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktSentSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktRcvdSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapPktDrpdSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanVniVlanTagStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRemoteVtepAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanInReplicaRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniESISet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniLoadBalanceSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapSentPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRcvdPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapDroppedPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanEviVniMapBgpRDAutoSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanBgpRTAutoSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapSentPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapRcvdPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfVniMapDroppedPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRDAutoSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanVrfRTAutoSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanOrdinalNumSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanMultihomedPeerRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVxlanUdpPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVxlanTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVxlanNotificationCntlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEvpnVxlanEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEvpnAnycastGwMacDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVxlanVtepTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsVxlanNveTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsVxlanMCastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsVxlanVniVlanMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsVxlanInReplicaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEvpnVxlanEviVniMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEvpnVxlanBgpRTTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEvpnVxlanVrfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEvpnVxlanVrfRTTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsEvpnVxlanMultihomedPeerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVxlanEcmpNveTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVxlanEcmpNveVtepAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpNveVtepAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpNveStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpSuppressArpGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpMHEviVniESIGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpActiveGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpVrfNameGet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpVrfNameSet(tSnmpIndex *, tRetVal *);
INT4 FsVxlanEcmpVrfNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsEvpnVxlanArpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsEvpnVxlanArpDestMacGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpDestMacSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpDestMacTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEvpnVxlanArpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterFSVXLA PROTO ((VOID));
VOID  UnRegisterFSVXLA PROTO ((VOID));

#endif /* _VXLANWR_H */
