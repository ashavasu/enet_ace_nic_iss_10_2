/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxinc.h,v 1.10 2018/01/05 09:57:10 siva Exp $
 *
 * Description: This file contains include files of vxlan module.
 *******************************************************************/

#ifndef __VXLANINC_H__
#define __VXLANINC_H__ 

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "tcp.h"
#include "vcm.h"
#include "cfa.h"
#include "cfacli.h"
#include "ip.h"
#include "ipv6.h"
#include "ip6util.h"
#include "pim.h"
#include "igmp.h"
#include "fsvlan.h"
#include "bgp.h"
#include "arp.h"
#include "rtm.h"
#include "dbutil.h"
#include "utilcli.h"

#include "fsvxlan.h"
#include "vxdefn.h"
#include "vxtdfsg.h"
#include "vxtdfs.h"
#include "vxred.h"
#include "vxtrc.h"

#include "vxcli.h"
#ifdef __VXLANMAIN_C__

#include "vxglob.h"
#include "vxlwg.h"
#include "vxdefg.h"
#include "vxsz.h"
#include "vxwrg.h"

#else

#include "vxextn.h"
#include "vxmibcli.h"
#include "vxlwg.h"
#include "vxdefg.h"
#include "vxsz.h"
#include "vxwrg.h"
#endif /* __VXLANMAIN_C__*/

#include "vxprot.h"
#include "vxprotg.h"

#include "vxsrcdef.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif


#endif   /* __VXLANINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file vxlaninc.h                       */
/*-----------------------------------------------------------------------*/

