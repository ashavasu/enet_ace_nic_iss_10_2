/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* $Id: vxprotg.h,v 1.9 2018/01/05 09:57:10 siva Exp $
*
* This file contains prototypes for functions defined in Vxlan.
*********************************************************************/


#include "cli.h"

INT4 VxlanCliSetFsVxlanEnable (tCliHandle CliHandle,UINT4 *pFsVxlanEnable);


INT4 VxlanCliSetFsVxlanUdpPort (tCliHandle CliHandle,UINT4 *pFsVxlanUdpPort);


INT4 VxlanCliSetFsVxlanTraceOption (tCliHandle CliHandle,UINT4 *pFsVxlanTraceOption, INT1);


INT4 VxlanCliSetFsVxlanNotificationCntl (tCliHandle CliHandle,UINT4 *pFsVxlanNotificationCntl);

#ifdef EVPN_VXLAN_WANTED
INT4 VxlanCliSetFsEvpnVxlanEnable (tCliHandle CliHandle,UINT4 *pFsEvpnVxlanEnable);

INT4 VxlanCliDelFsEviEntry (tCliHandle CliHandle,INT4 i4FsEvpnIdx);
INT4 VxlanCliDelFsVrfEntry (tCliHandle CliHandle, UINT1 *pu1EvpnVrfName);
INT4 VxlanCliSetFsEvpnAnycastGwMac (tCliHandle CliHandle, UINT4 *pu4FsEvpnAnycastGwMac);
#endif
INT4 VxlanCliSetFsVxlanVtepTable (tCliHandle CliHandle,tVxlanFsVxlanVtepEntry * pVxlanSetFsVxlanVtepEntry,tVxlanIsSetFsVxlanVtepEntry * pVxlanIsSetFsVxlanVtepEntry);

INT4 VxlanCliSetFsVxlanNveTable (tCliHandle CliHandle,tVxlanFsVxlanNveEntry * pVxlanSetFsVxlanNveEntry,tVxlanIsSetFsVxlanNveEntry * pVxlanIsSetFsVxlanNveEntry);

INT4 VxlanCliSetFsVxlanMCastTable (tCliHandle CliHandle,tVxlanFsVxlanMCastEntry * pVxlanSetFsVxlanMCastEntry,tVxlanIsSetFsVxlanMCastEntry * pVxlanIsSetFsVxlanMCastEntry);

INT4 VxlanCliSetFsVxlanVniVlanMapTable (tCliHandle CliHandle,tVxlanFsVxlanVniVlanMapEntry * pVxlanSetFsVxlanVniVlanMapEntry,tVxlanIsSetFsVxlanVniVlanMapEntry * pVxlanIsSetFsVxlanVniVlanMapEntry);

INT4 VxlanCliSetFsVxlanInReplicaTable (tCliHandle CliHandle,tVxlanFsVxlanInReplicaEntry * pVxlanSetFsVxlanInReplicaEntry,tVxlanIsSetFsVxlanInReplicaEntry * pVxlanIsSetFsVxlanInReplicaEntry);
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanCliSetFsEvpnVxlanEviVniMapTable (tCliHandle CliHandle,tVxlanFsEvpnVxlanEviVniMapEntry * pVxlanSetFsEvpnVxlanEviVniMapEntry,tVxlanIsSetFsEvpnVxlanEviVniMapEntry * pVxlanIsSetFsEvpnVxlanEviVniMapEntry);

INT4 VxlanCliSetFsEvpnVxlanBgpRTTable (tCliHandle CliHandle,tVxlanFsEvpnVxlanBgpRTEntry * pVxlanSetFsEvpnVxlanBgpRTEntry,tVxlanIsSetFsEvpnVxlanBgpRTEntry * pVxlanIsSetFsEvpnVxlanBgpRTEntry);

INT4 VxlanCliSetFsEvpnVxlanVrfTable (tCliHandle CliHandle,tVxlanFsEvpnVxlanVrfEntry * pVxlanSetFsEvpnVxlanVrfEntry,tVxlanIsSetFsEvpnVxlanVrfEntry * pVxlanIsSetFsEvpnVxlanVrfEntry);

INT4 VxlanCliSetFsEvpnVxlanVrfRTTable (tCliHandle CliHandle,tVxlanFsEvpnVxlanVrfRTEntry * pVxlanSetFsEvpnVxlanVrfRTEntry,tVxlanIsSetFsEvpnVxlanVrfRTEntry * pVxlanIsSetFsEvpnVxlanVrfRTEntry);

INT4 VxlanCliSetFsEvpnVxlanMultihomedPeerTable (tCliHandle CliHandle,tVxlanFsEvpnVxlanMultihomedPeerTable * pVxlanSetFsEvpnVxlanMultihomedPeerTable,tVxlanIsSetFsEvpnVxlanMultihomedPeerTable * pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
#endif

PUBLIC INT4 VxlanUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 VxlanLock (VOID);

PUBLIC INT4 VxlanUnLock (VOID);
PUBLIC INT4 FsVxlanVtepTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanVtepTableCreate PROTO ((VOID));

tVxlanFsVxlanVtepEntry * VxlanFsVxlanVtepTableCreateApi PROTO ((tVxlanFsVxlanVtepEntry *));

PUBLIC INT4 FsVxlanNveTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanNveTableCreate PROTO ((VOID));

tVxlanFsVxlanNveEntry * VxlanFsVxlanNveTableCreateApi PROTO ((tVxlanFsVxlanNveEntry *));

PUBLIC INT4 FsVxlanMCastTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanMCastTableCreate PROTO ((VOID));

tVxlanFsVxlanMCastEntry * VxlanFsVxlanMCastTableCreateApi PROTO ((tVxlanFsVxlanMCastEntry *));

PUBLIC INT4 FsVxlanVniVlanMapTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanVniVlanMapTableCreate PROTO ((VOID));

tVxlanFsVxlanVniVlanMapEntry * VxlanFsVxlanVniVlanMapTableCreateApi PROTO ((tVxlanFsVxlanVniVlanMapEntry *));

PUBLIC INT4 FsVxlanInReplicaTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanInReplicaTableCreate PROTO ((VOID));

PUBLIC INT4 VxlanFsVxlanVniVlanPortTableCreate PROTO ((tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry));

PUBLIC INT4 VxlanFsVxlanVniVlanPortMacTableCreate PROTO ((tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry));

PUBLIC INT4 VxlanFsVxlanNvePortTableCreate PROTO ((VOID));

PUBLIC INT4 FsVxlanVniVlanPortTableRBCmp PROTO((tRBElem * pRBElem1, tRBElem * pRBElem2));

PUBLIC INT4 FsVxlanVniVlanPortMacTableRBCmp PROTO((tRBElem * pRBElem1, tRBElem * pRBElem2));

PUBLIC INT4 FsVxlanNvePortTableRBCmp PROTO((tRBElem * pRBElem1, tRBElem * pRBElem2));

#ifdef EVPN_VXLAN_WANTED
PUBLIC INT4 FsEvpnVxlanEviVniMapTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsEvpnVxlanEviVniMapTableCreate PROTO ((VOID));

tVxlanFsEvpnVxlanEviVniMapEntry * VxlanFsEvpnVxlanEviVniMapTableCreateApi PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));

PUBLIC INT4 FsEvpnVxlanBgpRTTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsEvpnVxlanBgpRTTableCreate PROTO ((VOID));

tVxlanFsEvpnVxlanBgpRTEntry * VxlanFsEvpnVxlanBgpRTTableCreateApi PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));

PUBLIC INT4 FsEvpnVxlanVrfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsEvpnVxlanVrfTableCreate PROTO ((VOID));

tVxlanFsEvpnVxlanVrfEntry * VxlanFsEvpnVxlanVrfTableCreateApi PROTO ((tVxlanFsEvpnVxlanVrfEntry *));

PUBLIC INT4 FsEvpnVxlanVrfRTTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsEvpnVxlanVrfRTTableCreate PROTO ((VOID));

tVxlanFsEvpnVxlanVrfRTEntry * VxlanFsEvpnVxlanVrfRTTableCreateApi PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));

PUBLIC INT4 FsEvpnVxlanMultihomedPeerTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsEvpnVxlanMultihomedPeerTableCreate PROTO ((VOID));

tVxlanFsEvpnVxlanMultihomedPeerTable * VxlanFsEvpnVxlanMultihomedPeerTableCreateApi PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));

PUBLIC INT4 FsVxlanEcmpNveTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 VxlanFsVxlanEcmpNveTableCreate PROTO ((VOID));

tVxlanFsVxlanEcmpNveEntry * VxlanFsVxlanEcmpNveTableCreateApi PROTO ((tVxlanFsVxlanEcmpNveEntry *));

PUBLIC INT4 VxlanEvpnArpSupLocalMacTableCreate PROTO ((VOID));

PUBLIC INT4 VxlanEvpnArpSupLocalMacTableRBCmp PROTO ((tRBElem *, tRBElem *));

#endif
INT4 VxlanGetFsVxlanEnable PROTO ((INT4 *));
INT4 VxlanGetFsVxlanUdpPort PROTO ((UINT4 *));
INT4 VxlanGetFsVxlanTraceOption PROTO ((UINT4 *));
INT4 VxlanGetFsVxlanNotificationCntl PROTO ((INT4 *));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanGetFsEvpnVxlanEnable PROTO ((INT4 *));
INT4 VxlanGetFsEvpnAnycastGwMac PROTO ((tMacAddr *));
#endif
INT4 VxlanGetAllFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *));
INT4 VxlanGetAllUtlFsVxlanVtepTable PROTO((tVxlanFsVxlanVtepEntry *, tVxlanFsVxlanVtepEntry *));
INT4 VxlanGetAllFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *));
INT4 VxlanGetAllUtlFsVxlanNveTable PROTO((tVxlanFsVxlanNveEntry *, tVxlanFsVxlanNveEntry *));
INT4 VxlanGetAllFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *));
INT4 VxlanGetAllUtlFsVxlanMCastTable PROTO((tVxlanFsVxlanMCastEntry *, tVxlanFsVxlanMCastEntry *));
INT4 VxlanGetAllFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *));
INT4 VxlanGetAllUtlFsVxlanVniVlanMapTable PROTO((tVxlanFsVxlanVniVlanMapEntry *, tVxlanFsVxlanVniVlanMapEntry *));
INT4 VxlanGetAllFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanGetAllFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));
INT4 VxlanGetAllUtlFsEvpnVxlanEviVniMapTable PROTO((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanFsEvpnVxlanEviVniMapEntry *));
INT4 VxlanGetAllFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));
INT4 VxlanGetAllUtlFsEvpnVxlanBgpRTTable PROTO((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanFsEvpnVxlanBgpRTEntry *));
INT4 VxlanGetAllFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *));
INT4 VxlanGetAllUtlFsEvpnVxlanVrfTable PROTO((tVxlanFsEvpnVxlanVrfEntry *, tVxlanFsEvpnVxlanVrfEntry *));
INT4 VxlanGetAllFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));
INT4 VxlanGetAllUtlFsEvpnVxlanVrfRTTable PROTO((tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanFsEvpnVxlanVrfRTEntry *));
INT4 VxlanGetAllFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));
INT4 VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable PROTO((tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanFsEvpnVxlanMultihomedPeerTable *));
INT4 VxlanGetAllFsVxlanEcmpNveTable PROTO((tVxlanFsVxlanEcmpNveEntry * pVxlanGetFsVxlanEcmpNveEntry));
INT4 VxlanGetAllFsEvpnVxlanArpTable PROTO((tVxlanFsEvpnVxlanArpEntry * pVxlanGetFsEvpnVxlanArpEntry));
#endif
INT4 VxlanSetFsVxlanEnable PROTO ((INT4 ));
INT4 VxlanSetFsVxlanUdpPort PROTO ((UINT4 ));
INT4 VxlanSetFsVxlanTraceOption PROTO ((UINT4 , INT1));
INT4 VxlanSetFsVxlanNotificationCntl PROTO ((INT4 ));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanSetFsEvpnVxlanEnable PROTO ((INT4 ));
INT4 VxlanSetFsEvpnAnycastGwMac PROTO ((tMacAddr));
#endif
INT4 VxlanSetAllFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *, tVxlanIsSetFsVxlanVtepEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *, tVxlanIsSetFsVxlanNveEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *, tVxlanIsSetFsVxlanMCastEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *, tVxlanIsSetFsVxlanVniVlanMapEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *, tVxlanIsSetFsVxlanInReplicaEntry *, INT4 , INT4 ));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanSetAllFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *, tVxlanIsSetFsEvpnVxlanVrfEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanIsSetFsEvpnVxlanVrfRTEntry *, INT4 , INT4 ));
INT4 VxlanSetAllFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *, INT4 , INT4 ));
#endif
INT4 VxlanInitializeFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *));

INT4 VxlanInitializeMibFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *));

INT4 VxlanInitializeFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *, INT4));

INT4 VxlanInitializeMibFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *, INT4));

INT4 VxlanInitializeFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *, INT4));

INT4 VxlanInitializeMibFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *,INT4));

INT4 VxlanInitializeFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *, INT4));

INT4 VxlanInitializeMibFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *, INT4 ));

INT4 VxlanInitializeFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *, INT4));

INT4 VxlanInitializeMibFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *, INT4));

#ifdef EVPN_VXLAN_WANTED
INT4 VxlanInitializeFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));

INT4 VxlanInitializeMibFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));

INT4 VxlanInitializeFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));

INT4 VxlanInitializeMibFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));

INT4 VxlanInitializeFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *));

INT4 VxlanInitializeMibFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *));

INT4 VxlanInitializeFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));

INT4 VxlanInitializeMibFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));

INT4 VxlanInitializeFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));

INT4 VxlanInitializeMibFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));

INT4 VxlanInitializeMibFsVxlanEcmpNveTable PROTO ((tVxlanFsVxlanEcmpNveEntry *, INT4));
#endif
INT4 VxlanTestFsVxlanEnable PROTO ((UINT4 *,INT4 ));
INT4 VxlanTestFsVxlanUdpPort PROTO ((UINT4 *,UINT4 ));
INT4 VxlanTestFsVxlanTraceOption PROTO ((UINT4 *,UINT4 ));
INT4 VxlanTestFsVxlanNotificationCntl PROTO ((UINT4 *,INT4 ));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanTestFsEvpnVxlanEnable PROTO ((UINT4 *,INT4 ));
INT4 VxlanTestFsEvpnAnycastGwMac PROTO ((UINT4 *, tMacAddr));
#endif
INT4 VxlanTestAllFsVxlanVtepTable PROTO ((UINT4 *, tVxlanFsVxlanVtepEntry *, tVxlanIsSetFsVxlanVtepEntry *, INT4 , INT4));
INT4 VxlanTestAllFsVxlanNveTable PROTO ((UINT4 *, tVxlanFsVxlanNveEntry *, tVxlanIsSetFsVxlanNveEntry *, INT4 , INT4));
INT4 VxlanTestAllFsVxlanMCastTable PROTO ((UINT4 *, tVxlanFsVxlanMCastEntry *, tVxlanIsSetFsVxlanMCastEntry *, INT4 , INT4));
INT4 VxlanTestAllFsVxlanVniVlanMapTable PROTO ((UINT4 *, tVxlanFsVxlanVniVlanMapEntry *, tVxlanIsSetFsVxlanVniVlanMapEntry *, INT4 , INT4));
INT4 VxlanTestAllFsVxlanInReplicaTable PROTO ((UINT4 *, tVxlanFsVxlanInReplicaEntry *, tVxlanIsSetFsVxlanInReplicaEntry *, INT4, INT4));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanTestAllFsEvpnVxlanEviVniMapTable PROTO ((UINT4 *, tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *, INT4 , INT4));
INT4 VxlanTestAllFsEvpnVxlanBgpRTTable PROTO ((UINT4 *, tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *, INT4 , INT4));
INT4 VxlanTestAllFsEvpnVxlanVrfTable PROTO ((UINT4 *, tVxlanFsEvpnVxlanVrfEntry *, tVxlanIsSetFsEvpnVxlanVrfEntry *, INT4 , INT4));
INT4 VxlanTestAllFsEvpnVxlanVrfRTTable PROTO ((UINT4 *, tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanIsSetFsEvpnVxlanVrfRTEntry *, INT4 , INT4));
INT4 VxlanTestAllFsEvpnVxlanMultihomedPeerTable PROTO ((UINT4 *, tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *, INT4 , INT4));
#endif
tVxlanFsVxlanVtepEntry * VxlanGetFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *));
tVxlanFsVxlanNveEntry * VxlanGetFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *));
tVxlanFsVxlanMCastEntry * VxlanGetFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *));
tVxlanFsVxlanVniVlanMapEntry * VxlanGetFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *));
tVxlanFsVxlanInReplicaEntry * VxlanGetFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *));
#ifdef EVPN_VXLAN_WANTED
tVxlanFsEvpnVxlanEviVniMapEntry * VxlanGetFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));
tVxlanFsEvpnVxlanBgpRTEntry * VxlanGetFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));
tVxlanFsEvpnVxlanVrfEntry * VxlanGetFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *));
tVxlanFsEvpnVxlanVrfRTEntry * VxlanGetFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));
tVxlanFsEvpnVxlanMultihomedPeerTable * VxlanGetFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));
#endif
tVxlanFsVxlanVtepEntry * VxlanGetFirstFsVxlanVtepTable PROTO ((VOID));
tVxlanFsVxlanNveEntry * VxlanGetFirstFsVxlanNveTable PROTO ((VOID));
tVxlanFsVxlanMCastEntry * VxlanGetFirstFsVxlanMCastTable PROTO ((VOID));
tVxlanFsVxlanVniVlanMapEntry * VxlanGetFirstFsVxlanVniVlanMapTable PROTO ((VOID));
tVxlanFsVxlanInReplicaEntry * VxlanGetFirstFsVxlanInReplicaTable PROTO ((VOID));
#ifdef EVPN_VXLAN_WANTED
tVxlanFsEvpnVxlanEviVniMapEntry * VxlanGetFirstFsEvpnVxlanEviVniMapTable PROTO ((VOID));
tVxlanFsEvpnVxlanBgpRTEntry * VxlanGetFirstFsEvpnVxlanBgpRTTable PROTO ((VOID));
tVxlanFsEvpnVxlanVrfEntry * VxlanGetFirstFsEvpnVxlanVrfTable PROTO ((VOID));
tVxlanFsEvpnVxlanVrfRTEntry * VxlanGetFirstFsEvpnVxlanVrfRTTable PROTO ((VOID));
tVxlanFsEvpnVxlanMultihomedPeerTable * VxlanGetFirstFsEvpnVxlanMultihomedPeerTable PROTO ((VOID));
tVxlanFsVxlanEcmpNveEntry * VxlanGetFirstFsVxlanEcmpNveTable PROTO ((VOID));
tVxlanFsEvpnVxlanArpEntry * VxlanGetFirstFsEvpnVxlanArpTable PROTO ((VOID));
#endif
tVxlanFsVxlanVtepEntry * VxlanGetNextFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *));
tVxlanFsVxlanNveEntry * VxlanGetNextFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *));
tVxlanFsVxlanMCastEntry * VxlanGetNextFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *));
tVxlanFsVxlanVniVlanMapEntry * VxlanGetNextFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *));
tVxlanFsVxlanInReplicaEntry * VxlanGetNextFsVxlanInReplicaTable PROTO ((tVxlanFsVxlanInReplicaEntry *));
#ifdef EVPN_VXLAN_WANTED
tVxlanFsEvpnVxlanEviVniMapEntry * VxlanGetNextFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *));
tVxlanFsEvpnVxlanBgpRTEntry * VxlanGetNextFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *));
tVxlanFsEvpnVxlanVrfEntry * VxlanGetNextFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *));
tVxlanFsEvpnVxlanVrfRTEntry * VxlanGetNextFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *));
tVxlanFsEvpnVxlanMultihomedPeerTable * VxlanGetNextFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *));
tVxlanFsVxlanEcmpNveEntry * VxlanGetNextFsVxlanEcmpNveTable PROTO ((tVxlanFsVxlanEcmpNveEntry * pCurrentVxlanFsVxlanEcmpNveEntry));
tVxlanFsVxlanEcmpNveEntry * VxlanGetFsVxlanEcmpNveTable PROTO ((tVxlanFsVxlanEcmpNveEntry * pVxlanFsVxlanEcmpNveEntry));
tVxlanFsEvpnVxlanArpEntry * VxlanGetNextFsEvpnVxlanArpTable PROTO ((tVxlanFsEvpnVxlanArpEntry * pCurrentVxlanFsEvpnVxlanArpEntry));
#endif
INT4 FsVxlanVtepTableFilterInputs PROTO ((tVxlanFsVxlanVtepEntry *, tVxlanFsVxlanVtepEntry *, tVxlanIsSetFsVxlanVtepEntry *));
INT4 FsVxlanNveTableFilterInputs PROTO ((tVxlanFsVxlanNveEntry *, tVxlanFsVxlanNveEntry *, tVxlanIsSetFsVxlanNveEntry *));
INT4 FsVxlanMCastTableFilterInputs PROTO ((tVxlanFsVxlanMCastEntry *, tVxlanFsVxlanMCastEntry *, tVxlanIsSetFsVxlanMCastEntry *));
INT4 FsVxlanVniVlanMapTableFilterInputs PROTO ((tVxlanFsVxlanVniVlanMapEntry *, tVxlanFsVxlanVniVlanMapEntry *, tVxlanIsSetFsVxlanVniVlanMapEntry *));
INT4 FsVxlanInReplicaTableFilterInputs PROTO ((tVxlanFsVxlanInReplicaEntry *, tVxlanFsVxlanInReplicaEntry *, tVxlanIsSetFsVxlanInReplicaEntry *));
#ifdef EVPN_VXLAN_WANTED
INT4 FsEvpnVxlanEviVniMapTableFilterInputs PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *));
INT4 FsEvpnVxlanBgpRTTableFilterInputs PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *));
INT4 FsEvpnVxlanVrfTableFilterInputs PROTO ((tVxlanFsEvpnVxlanVrfEntry *, tVxlanFsEvpnVxlanVrfEntry *, tVxlanIsSetFsEvpnVxlanVrfEntry *));
INT4 FsEvpnVxlanVrfRTTableFilterInputs PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanIsSetFsEvpnVxlanVrfRTEntry *));
INT4 FsEvpnVxlanMultihomedPeerTableFilterInputs PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *));
#endif
INT4 VxlanUtilUpdateFsVxlanVtepTable PROTO ((tVxlanFsVxlanVtepEntry *, tVxlanFsVxlanVtepEntry *, tVxlanIsSetFsVxlanVtepEntry *));
INT4 VxlanUtilUpdateFsVxlanNveTable PROTO ((tVxlanFsVxlanNveEntry *, tVxlanFsVxlanNveEntry *, tVxlanIsSetFsVxlanNveEntry *));
INT4 VxlanUtilUpdateFsVxlanMCastTable PROTO ((tVxlanFsVxlanMCastEntry *, tVxlanFsVxlanMCastEntry *, tVxlanIsSetFsVxlanMCastEntry *));
INT4 VxlanUtilUpdateFsVxlanVniVlanMapTable PROTO ((tVxlanFsVxlanVniVlanMapEntry *, tVxlanFsVxlanVniVlanMapEntry *, tVxlanIsSetFsVxlanVniVlanMapEntry *));
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanUtilUpdateFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *));
INT4 VxlanUtilUpdateFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *));
INT4 VxlanUtilUpdateFsEvpnVxlanVrfTable PROTO ((tVxlanFsEvpnVxlanVrfEntry *, tVxlanFsEvpnVxlanVrfEntry *, tVxlanIsSetFsEvpnVxlanVrfEntry *));
INT4 VxlanUtilUpdateFsEvpnVxlanVrfRTTable PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanIsSetFsEvpnVxlanVrfRTEntry *));
INT4 VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *));
#endif
INT4 VxlanSetAllFsVxlanVtepTableTrigger PROTO ((tVxlanFsVxlanVtepEntry *, tVxlanIsSetFsVxlanVtepEntry *, INT4));
INT4 VxlanSetAllFsVxlanNveTableTrigger PROTO ((tVxlanFsVxlanNveEntry *, tVxlanIsSetFsVxlanNveEntry *, INT4));
INT4 VxlanSetAllFsVxlanMCastTableTrigger PROTO ((tVxlanFsVxlanMCastEntry *, tVxlanIsSetFsVxlanMCastEntry *, INT4));
INT4 VxlanSetAllFsVxlanVniVlanMapTableTrigger PROTO ((tVxlanFsVxlanVniVlanMapEntry *, tVxlanIsSetFsVxlanVniVlanMapEntry *, INT4));
INT4 VxlanSetAllFsVxlanInReplicaTableTrigger PROTO ((tVxlanFsVxlanInReplicaEntry *, tVxlanIsSetFsVxlanInReplicaEntry *, INT4));

INT4 VxlanCliShowNveInterface (tCliHandle CliHandle, UINT4 * pu4Val,
                                   INT4 * pi4IfIndex);
INT4 VxlanCliShowNvePeer (tCliHandle CliHandle, UINT4 * pu4Val);
INT4 VxlanCliShowNveVni (tCliHandle CliHandle); 
INT4 VxlanCliShowVlanVni (tCliHandle CliHandle, INT4 *pi4VlanId);
INT4 VxlanCliShowVniStatistics (tCliHandle CliHandle,
                                UINT4   *pu4VniID);
INT4 VxlanCliShowUdpPort (tCliHandle CliHandle);

INT4 VxlanCliShowPacketHeaders (tCliHandle CliHandle, UINT4 * pSrcMac,
                                UINT4 * pDestMac,
                                UINT4 * pu4VlanID,
                                UINT4 * pu4Vni,
                                UINT1 *pSrcIP,
                                UINT1 *pDestIP,
                                UINT1 *pMcastIP,
                                INT1  *pAddrType);

INT4 VxlanCliDelIngressReplica (tCliHandle CliHandle,UINT4 u4FsVxlanNveIfIndex,
                                   UINT4 u4FsVxlanNveVniNumber);
#ifdef EVPN_VXLAN_WANTED
INT4 VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *, INT4));
INT4 VxlanSetAllFsEvpnVxlanBgpRTTableTrigger PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *, INT4));
INT4 VxlanSetAllFsEvpnVxlanBgpRTTableTrigger PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *, INT4));
INT4 VxlanSetAllFsEvpnVxlanVrfTableTrigger PROTO ((tVxlanFsEvpnVxlanVrfEntry *, tVxlanIsSetFsEvpnVxlanVrfEntry *, INT4));
INT4 VxlanSetAllFsEvpnVxlanVrfRTTableTrigger PROTO ((tVxlanFsEvpnVxlanVrfRTEntry *, tVxlanIsSetFsEvpnVxlanVrfRTEntry *, INT4));
INT4 VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger PROTO ((tVxlanFsEvpnVxlanMultihomedPeerTable *, tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *, INT4));
INT4 VxlanUtilUpdateFsEvpnVxlanEviVniMapTable PROTO ((tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanFsEvpnVxlanEviVniMapEntry *, tVxlanIsSetFsEvpnVxlanEviVniMapEntry *));
INT4 VxlanUtilUpdateFsEvpnVxlanBgpRTTable PROTO ((tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanFsEvpnVxlanBgpRTEntry *, tVxlanIsSetFsEvpnVxlanBgpRTEntry *));
INT4 VxlanCliShowEviVniStatistics (tCliHandle CliHandle, INT4 *pi4EviID, UINT4 *pu4VniID);
INT4 VxlanCliShowEvpnVni (tCliHandle CliHandle);
INT4 VxlanCliShowEvpnMac (tCliHandle CliHandle, UINT4 u4EviIdx, INT4 i4MacType);
INT4 VxlanCliShowEvpnStatus (tCliHandle CliHandle);
INT4 VxlanCliShowNeighborsStatus (tCliHandle CliHandle);
INT4 VxlanCliShowEcmpNvePeer (tCliHandle CliHandle, UINT4 *pu4Val, INT4 *pi4EcmpNveCount);
INT4 VxlanCliShowVrf (tCliHandle CliHandle);
INT4 VxlanVniIsL2Vni ( UINT4 u4VniId);


#endif /* EVPN_VXLAN_WANTED */

