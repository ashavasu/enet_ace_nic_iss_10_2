/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxdefn.h,v 1.17 2018/01/05 09:57:10 siva Exp $
 *
 * Description: This file contains definitions for vxlan module.
 *******************************************************************/

#ifndef __VXLANDEFN_H__
#define __VXLANDEFN_H__

#define VXLAN_SUCCESS        (OSIX_SUCCESS)
#define VXLAN_FAILURE        (OSIX_FAILURE)

#define  VXLAN_TASK_PRIORITY              100
#define  VXLAN_TASK_NAME                  "VXLAN"
#define  VXLAN_QUEUE_NAME                 (const UINT1 *) "VXLANQ"
#define  VXLAN_MUT_EXCL_SEM_NAME          (const UINT1 *) "VXLANM"
#define  VXLAN_QUEUE_DEPTH                MAX_VXLAN_QUEUE_MSG
#define  VXLAN_SEM_CREATE_INIT_CNT        1

#define VXLAN_QUEUE_POOLID                VXLANMemPoolIds[MAX_VXLAN_QUEUE_SIZING_ID]
#define VXLAN_UDP_RECV_BUF_POOLID         VXLANMemPoolIds[MAX_VXLAN_UDP_RECV_BUF_SIZING_ID]
#define VXLAN_UDP_SEND_BUF_POOLID         VXLANMemPoolIds[MAX_VXLAN_UDP_SEND_BUF_SIZING_ID]
#define VXLAN_RED_MSG_MEMPOOL_ID          VXLANMemPoolIds[MAX_VXLAN_RED_MSG_SIZING_ID]


#define  VXLAN_ENABLED                    1
#define  VXLAN_DISABLED                   2
#define  VXLAN_UTIL_UINT4_MAX              0xFFFFFFFF

#define EVPN_MAX_VRF_NAME_LEN             VCMALIAS_MAX_ARRAY_LEN
/*#define EVPN_MAX_VRF_NAME_LEN             36*/
#ifdef EVPN_VXLAN_WANTED
#define  EVPN_ENABLED                     1
#define  EVPN_DISABLED                    2
#define MIN_EVPN_VALUE                    1
#define MAX_EVPN_VALUE                    4096
#define MIN_VNI_VALUE                     4096
#define MAX_VNI_VALUE                     8191
#define EVPN_UTIL_UINT4_MAX               0xFFFFFFFF
#define EVPN_ZERO                         0
#define EVPN_ONE                          1
#define EVPN_MAX_RD_RT_STRING_LEN         32
#define EVPN_MAX_ESI_STR_LEN              30
#define EVPN_IPV4_ADDR_LENGTH             4
#define EVPN_MAX_RT_LEN                   8
#define EVPN_MAX_RD_LEN                   8
#define EVPN_MAX_ESI_LEN                  10
#define EVPN_RD_SUBTYPE                   0
#define EVPN_RT_SUBTYPE                   2
#define EVPN_ESI_TYPE                     1
#define EVPN_UINT1_MAX_VALUE              0xFF
#define EVPN_UINT2_MAX_VALUE              0xFFFF
#define EVPN_UINT4_MAX_VALUE              0xFFFFFFFF
#define EVPN_MAX_DIGITS_IN_UINT2       5
#define EVPN_MAX_DIGITS_IN_UINT4       10
#define EVPN_P_PARAM_RD              1
#define EVPN_BGP4_IMPORT_RT          2
#define EVPN_BGP4_EXPORT_RT          3
#define EVPN_BGP4_BOTH_RT            4
#define EVPN_P_PARAM_ESI             5

#define EVPN_OPER_UP                 1
#define EVPN_OPER_DOWN               2
#define EVPN_OPER_CREATED            3
#define EVPN_OPER_DELETED            4
#define EVPN_PARAM_RD_ADD       1
#define EVPN_PARAM_RD_DEL       2
#define EVPN_PARAM_RT_ADD       3
#define EVPN_PARAM_RT_DEL       4

#define EVPN_PARAM_ESI_ADD      1
#define EVPN_PARAM_ESI_DEL      2

#define EVPN_PARAM_ALL_ACTIVE       0
#define EVPN_PARAM_SINGLE_ACTIVE    1

#define EVPN_PARAM_AD_ROUTE         1
#define MAX_RT_TYPE_LEN              7
#define EVPN_PARAM_DEF_VRF_ID   0

#define EVPN_ECMP_NVE_ADD       1
#define EVPN_ECMP_NVE_DEL       2

#define EVPN_IF_OPER_UP       1
#define EVPN_IF_OPER_DOWN     2
#define EVPN_L2VNI_RT          1
#define EVPN_VRF_VNI_RT        2

#define EVPN_L3VNI_ADD         1
#define EVPN_L3VNI_DEL         2

#define EVPN_L2VNI_TYPE     1
#define EVPN_L3VNI_TYPE     2

#define VXLAN_EVPN_UPDATE_MAC 5
#define VXLAN_EVPN_REMOVE_MAC 6

#define EVPN_IF_OPER_UP       1
#define EVPN_IF_OPER_DOWN     2
#define EVPN_L2VNI_RT          1
#define EVPN_VRF_VNI_RT        2

#define EVPN_L3VNI_ADD         1
#define EVPN_L3VNI_DEL         2

#define EVPN_L2VNI_TYPE     1
#define EVPN_L3VNI_TYPE     2

#define VXLAN_EVPN_UPDATE_MAC 5
#define VXLAN_EVPN_REMOVE_MAC 6

enum
{
   EVPN_STAT_PKT_SENT = 1,
   EVPN_STAT_PKT_RCVD,
   EVPN_STAT_PKT_DRPD
};

enum
{
   EVPN_RD_TYPE_0 = 0,
   EVPN_RD_TYPE_1,
   EVPN_RD_TYPE_2
};

enum
{
   EVPN_RT_TYPE_0 = 0,
   EVPN_RT_TYPE_1,
   EVPN_RT_TYPE_2
};

enum
{
   EVPN_ESI_TYPE_0 = 0,
   EVPN_ESI_TYPE_1,
   EVPN_ESI_TYPE_2
};
enum
{
   VRF_STAT_PKT_SENT = 1,
   VRF_STAT_PKT_RCVD,
   VRF_STAT_PKT_DRPD
};
enum
{
   VRF_STAT_PKT_SENT = 1,
   VRF_STAT_PKT_RCVD,
   VRF_STAT_PKT_DRPD
};

#define EVPN_NOTIFY_ROUTE_PARAMS_CB        gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyRouteParams
#define EVPN_NOTIFY_OPER_STATUS_CHANGE_CB  gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyAdminStatusChange
#define EVPN_NOTIFY_MAC_UPDATE_CB          gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyMACUpdate
#define EVPN_NOTIFY_GET_MAC_CB             gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnGetMACNotification
#define EVPN_NOTIFY_ESI_UPDATE_CB          gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyESIUpdate
#define EVPN_NOTIFY_AD_ROUTE_CB            gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyADRoute
#define EVPN_NOTIFY_L3VNI                  gVxlanGlobals.EvpnBgpRegEntry.pBgp4EvpnNotifyL3Vni

/* EVI-VNI Map Table */
#define EVPN_P_BGP_RD(x)                   x->MibObject.au1FsEvpnVxlanEviVniMapBgpRD
#define EVPN_P_BGP_RD_LEN(x)               x->MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen
#define EVPN_P_BGP_VNI(x)                  x->MibObject.u4FsEvpnVxlanEviVniMapVniNumber
#define EVPN_P_BGP_EVI(x)                  x->MibObject.i4FsEvpnVxlanEviVniMapEviIndex
#define EVPN_P_BGP_ESI(x)                  x->MibObject.au1FsEvpnVxlanEviVniESI
#define EVPN_P_BGP_ESI_LEN(x)              x->MibObject.i4FsEvpnVxlanEviVniESILen
#define EVPN_P_BGP_STATUS(x)               x->MibObject.i4FsEvpnVxlanEviVniMapRowStatus
#define EVPN_P_BGP_SENT_PKTS(x)            x->MibObject.u4FsEvpnVxlanEviVniMapSentPkts
#define EVPN_P_BGP_RCVD_PKTS(x)            x->MibObject.u4FsEvpnVxlanEviVniMapRcvdPkts
#define EVPN_P_BGP_DRP_PKTS(x)             x->MibObject.u4FsEvpnVxlanEviVniMapDroppedPkts
#define EVPN_P_BGP_RD_AUTO(x)              x->MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto
#define EVPN_P_BGP_LOAD_BALANCE(x)         x->MibObject.i4FsEvpnVxlanEviVniLoadBalance

/* BGP-RT Table */
#define EVPN_P_BGP_RT(x)                   x->MibObject.au1FsEvpnVxlanBgpRT
#define EVPN_P_BGP_RT_INDEX(x)             x->MibObject.u4FsEvpnVxlanBgpRTIndex
#define EVPN_P_BGP_RT_LEN(x)               x->MibObject.i4FsEvpnVxlanBgpRTLen
#define EVPN_P_BGP_RT_TYPE(x)              x->MibObject.i4FsEvpnVxlanBgpRTType
#define EVPN_P_BGP_RT_ROW_STATUS(x)        x->MibObject.i4FsEvpnVxlanBgpRTRowStatus
#define EVPN_P_BGP_RT_AUTO(x)              x->MibObject.i4FsEvpnVxlanBgpRTAuto

#define EVPN_RT_TABLE                      gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable
#define EVPN_BGP_RD(x)                     x.MibObject.au1FsEvpnVxlanEviVniMapBgpRD
#define EVPN_BGP_RT_AUTO(x)                x.MibObject.i4FsEvpnVxlanBgpRTAuto
#define EVPN_BGP_RD_AUTO(x)                x.MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto
#define EVPN_BGP_RT(x)                     x.MibObject.au1FsEvpnVxlanBgpRT
#define EVPN_BGP_ESI(x)                    x.MibObject.au1FsEvpnVxlanEviVniESI

/* Vrf-VNI Table */
#define EVPN_P_VRF_NAME(x)                 x->MibObject.au1FsEvpnVxlanVrfName
#define EVPN_P_VRF_NAME_LEN(x)             x->MibObject.i4FsEvpnVxlanVrfNameLen
#define EVPN_P_VRF_RD(x)                   x->MibObject.au1FsEvpnVxlanVrfRD
#define EVPN_P_VRF_RD_LEN(x)               x->MibObject.i4FsEvpnVxlanVrfRDLen
#define EVPN_P_VRF_VNI(x)                  x->MibObject.u4FsEvpnVxlanVrfVniNumber
#define EVPN_P_VRF_ROW_STATUS(x)           x->MibObject.i4FsEvpnVxlanVrfRowStatus
#define EVPN_P_VRF_RD_AUTO(x)              x->MibObject.i4FsEvpnVxlanVrfRDAuto
#define EVPN_P_VRF_SENT_PKTS(x)            x->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts
#define EVPN_P_VRF_RCVD_PKTS(x)            x->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts
#define EVPN_P_VRF_DRP_PKTS(x)             x->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts

#define EVPN_VRF_NAME(x)                 x.MibObject.au1FsEvpnVxlanVrfName
#define EVPN_VRF_NAME_LEN(x)             x.MibObject.i4FsEvpnVxlanVrfNameLen
#define EVPN_VRF_RD(x)                   x.MibObject.au1FsEvpnVxlanVrfRD
#define EVPN_VRF_RD_LEN(x)               x.MibObject.i4FsEvpnVxlanVrfRDLen
#define EVPN_VRF_VNI(x)                  x.MibObject.u4FsEvpnVxlanVrfVniNumber
#define EVPN_VRF_ROW_STATUS(x)               x.MibObject.i4FsEvpnVxlanVrfRowStatus
#define EVPN_VRF_RD_AUTO(x)              x.MibObject.i4FsEvpnVxlanVrfRDAuto

/* VRF-RT Table */
#define EVPN_P_VRF_RT_NAME(x)              x->MibObject.au1FsEvpnVxlanVrfName
#define EVPN_P_VRF_RT_NAME_LEN(x)          x->MibObject.i4FsEvpnVxlanVrfNameLen
#define EVPN_P_VRF_RT(x)                   x->MibObject.au1FsEvpnVxlanVrfRT
#define EVPN_P_VRF_RT_INDEX(x)             x->MibObject.u4FsEvpnVxlanVrfRTIndex
#define EVPN_P_VRF_RT_LEN(x)               x->MibObject.i4FsEvpnVxlanVrfRTLen
#define EVPN_P_VRF_RT_TYPE(x)              x->MibObject.i4FsEvpnVxlanVrfRTType
#define EVPN_P_VRF_RT_ROW_STATUS(x)        x->MibObject.i4FsEvpnVxlanVrfRTRowStatus
#define EVPN_P_VRF_RT_AUTO(x)              x->MibObject.i4FsEvpnVxlanVrfRTAuto
#define EVPN_P_VRF_RT_VNI(x)              x->MibObject.u4FsEvpnVxlanVrfVniNumber

#define EVPN_VRF_RT_NAME(x)              x.MibObject.au1FsEvpnVxlanVrfName
#define EVPN_VRF_RT_NAME_LEN(x)          x.MibObject.i4FsEvpnVxlanVrfNameLen
#define EVPN_VRF_RT(x)                   x.MibObject.au1FsEvpnVxlanVrfRT
#define EVPN_VRF_RT_INDEX(x)             x.MibObject.u4FsEvpnVxlanVrfRTIndex
#define EVPN_VRF_RT_LEN(x)               x.MibObject.i4FsEvpnVxlanVrfRTLen
#define EVPN_VRF_RT_TYPE(x)              x.MibObject.i4FsEvpnVxlanVrfRTType
#define EVPN_VRF_RT_ROW_STATUS(x)        x.MibObject.i4FsEvpnVxlanVrfRTRowStatus
#define EVPN_VRF_RT_AUTO(x)              x.MibObject.i4FsEvpnVxlanVrfRTAuto
#define EVPN_VRF_RT_VNI(x)              x.MibObject.u4FsEvpnVxlanVrfVniNumber


#define EVPN_IPV4_TYPE     1
#define EVPN_IPV6_TYPE     2

#define EVPN_AD_ROUTE              1
#define EVPN_MAC_ROUTE             2
#define EVPN_ETH_SEGEMENT_ROUTE    4

#define VXLAN_AD_WITHDRAW  2

#endif /* EVPN_VXLAN_WANTED */

#define  VXLAN_TIMER_EVENT             0x00000001 
#define  VXLAN_QUEUE_EVENT             0x00000002
#define  MAX_VXLAN_DUMMY                   1


#define MIN_VALUE_UDPPORT  1000
#define MAX_VALUE_UDPPORT  65535

#define VXLAN_IPV4_UNICAST 1
#define VXLAN_IPV6_UNICAST 2

#define  VXLAN_IP4_ADDR_LEN    4
#define  VXLAN_IP6_ADDR_LEN    16
#define  VXLAN_MAC_STR_LEN     24

#define  VXLAN_ONE_BYTE_BITS     8

#define  VXLAN_IP4_ADDR_LEN_BITS    32
#define  VXLAN_IP6_ADDR_LEN_BITS    128

#define  VXLAN_IPV6_ADDR_BYTES   256
#define VXLAN_UDP_DEST_PORT           4789

#define VXLAN_JUMBO_MTU_SIZE          9000

#define VXLAN_HDR_FLAG                8

#define VXLAN_ETHERNET_ADDR_SIZE      6
#define VXLAN_ETHERNET_HEADER_LEN     14

#define VXLAN_IP_HDR_SIZE             20
#define VXALN_UDP_HDR_SIZE            8
#define VXLAN_IPV6_HDR_SIZE           40

#define VXLAN_HDR_TOTAL_OFFSET        8

#define MIN_VALUE_VNI_IDENTIFIER  4096
#define MAX_VALUE_VNI_IDENTIFIER  8191

#define  VXLAN_STRG_TYPE_NON_VOL 1
#define  VXLAN_STRG_TYPE_VOL 2

#define VXLAN_NVE_DYNAMIC_MAC 0
#define VXLAN_NVE_STATIC_MAC 1
#define VXLAN_NVE_EVPN_MAC 2

#define VXLAN_INV_IPADDRESS 0x00000000

#define VXLAN_TRUE 1
#define VXLAN_FALSE 0

#define VXLAN_TABLE_MIB_OID_LENGTH    14
#define VXLAN_SCALAR_MIB_OID_LENGTH   12

#define VXLAN_MSG_LENGTH    24
#define VXLAN_RM_BYTE       12
#define VXLAN_SNX_IPv4                    1
#define VXLAN_SNX_IPv6                    2
#define VXLAN_IS_VALID_ADDRESS(u4Addr) \
            (((u4Addr == VXLAN_INV_IPADDRESS) || \
                        ((u4Addr & 0xff000000) == 0x00000000) || \
                        ((u4Addr & 0xff000000) >= 0xE0000000) || \
                        ((u4Addr & 0xff000000) == 0x7f000000)) ? 0 : 1)

#define VXLAN_IS_BROADCAST_ADDRESS(u4Addr) \
        ((((u4Addr & 0x000000ff) == 0x000000ff) || \
                ((u4Addr & 0x0000ffff) == 0x0000ffff) || \
                ((u4Addr & 0x00ffffff) == 0x00ffffff) || \
                ((u4Addr & 0xffffffff) == 0xffffffff)) ? 1 : 0)

#define VXLAN_IS_MCAST_LOOP_ADDR(u4Addr) \
        ((((u4Addr & 0xff000000) < 0xE0000000) || \
                ((u4Addr & 0xff000000) > 0xEF000000)) ? 0 : 1)


#define VXLAN_IS_MAC_MULTICAST(pMacAddr)\
                    (pMacAddr[0] & 0x01)

#define VXLAN_INET_HTONL(GrpIp1)\
{\
        UINT4   u4TmpGrpAddr = 0;\
        UINT4   u4Count1 = 0;\
        UINT1   u1Index = 0;\
        UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
          \
        MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
        \
        for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
                         u1Index++, u4Count1 = u4Count1 + 4)\
        {\
               MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
                    u4TmpGrpAddr = (UINT4 )OSIX_HTONL (u4TmpGrpAddr);\
               MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
                }\
        MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define VXLAN_IP_PRINT_ADDR(Addr) VxlanPrintIpAddr (Addr)
enum 
{
    VXLAN_L2PKT_RCVD_EVENT = 1,
    VXLAN_UDP_IPV4_EVENT,
    VXLAN_UDP_IPV6_EVENT, 
    VXLAN_RM_MSG_RECVD,
    VXLAN_VLAN_VNI_DEL_EVENT,
    VXLAN_NVE_DEL_EVENT,
    VXLAN_ROUTE_DEL_EVENT,
    VXLAN_NP_MAC_VLAN_EVENT,
    VXLAN_EVPN_MAC_ADD_EVENT,
    VXLAN_EVPN_MAC_DEL_EVENT,
    VXLAN_NVE_ADMIN_EVENT,
    VXLAN_EVPN_ESI_IF_OPER_CHNG,
    VXLAN_L3PKT_RCVD_EVENT,
    VXLAN_NP_MAC_PORT_EVENT,
    VXLAN_PORT_OPER_DOWN,
    VXLAN_PORT_AGEOUT
};


enum
{
   VXLAN_STAT_PKT_SENT = 1,
   VXLAN_STAT_PKT_RCVD,
   VXLAN_STAT_PKT_DRPD
};

#define VXLAN_IP_INVALID_INDEX        IPIF_INVALID_INDEX
#define VXLAN_IP_TTL                  64

#ifdef L2RED_WANTED
#define VXLAN_GET_NODE_STATUS()  gVxlanRedGblInfo.u1NodeStatus
#else
#define VXLAN_GET_NODE_STATUS()  RM_ACTIVE
#endif

#define VXLAN_HW_ADD 1
#define VXLAN_HW_DEL 2

#define VXLAN_TIMER_ROUTE 1
#define EVPN_TIMER_ROUTE  2

#define VXLAN_HW_REPLICA_LIST          10
#define VXLAN_HW_MH_PEER_LIST          10

#endif  /* __VXLANDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file vxlandefn.h                      */
/*-----------------------------------------------------------------------*/

