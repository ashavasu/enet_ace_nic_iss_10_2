/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxprot.h,v 1.20 2018/01/05 15:26:48 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of vxlan module.
 *******************************************************************/

#ifndef __VXLANPROT_H__
#define __VXLANPROT_H__ 

#include "vxlannp.h"

/* Add Prototypes here */

/**************************vxlanmain.c*******************************/
PUBLIC VOID VxlanMainTask             PROTO ((VOID));
PUBLIC UINT4 VxlanMainTaskInit         PROTO ((VOID));
PUBLIC VOID VxlanMainDeInit            PROTO ((VOID));
PUBLIC INT4 VxlanMainTaskLock          PROTO ((VOID));
PUBLIC INT4 VxlanMainTaskUnLock        PROTO ((VOID));



/**************************vxlanque.c********************************/
PUBLIC VOID VxlanQueProcessMsgs        PROTO ((VOID));

/**************************vxlanpkt.c*******************************/
#if 0
PUBLIC tVxlanPkt *VxlanPktAlloc        PROTO(( VOID ));
PUBLIC VOID VxlanPktFree              PROTO(( tVxlanPkt *pVxlanPkt ));
PUBLIC UINT4  VxlanPktSend            PROTO(( tVxlanPkt *pVxlanPkt ));
PUBLIC UINT4 VxlanPktRecv             PROTO(( tVxlanPkt *pVxlanPkt));
#endif

/**************************vxlantrc.c*******************************/


CHR1  *VxlanTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   VxlanTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   VxlanTrcWrite      PROTO(( CHR1 *s));

/**************************vxlantask.c*******************************/

PUBLIC INT4 VxlanTaskRegisterVxlanMibs (VOID);

UINT4
VxlanEnqueMsg (tVxlanIfMsg *pVxlanIfMsg);
INT4
VxlanEncapAndFwdPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex);
INT4
VxlanEncapAndFwdL3Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VrfId, UINT4 u4L3VnId);
INT4
VxlanUtilGetVniFromVlan (UINT2 u2VlanId, UINT4 *pu4VniId);
INT4
VxlanUtilGetVrfFromVlan (UINT2 u2VlanId, UINT1 *pu1VrfName);
INT4
VxlanUtilGetVlanFromVni (UINT4 u4VniId, UINT2 *pu2VlanId);
tVxlanFsVxlanNveEntry *
VxlanUtilGetNveDatabase (UINT4 u4VniId, tMacAddr DestVmAddr, UINT4 u4IfIndex);

tVxlanFsVxlanMCastEntry *
VxlanUtilGetMCastDatabase (UINT4 u4VniId, UINT4 u4IfIndex);

tVxlanFsVxlanInReplicaEntry *
VxlanUtilGetInReplicaDatabase (UINT4 u4VniId, UINT4 u4IfIndex);

PUBLIC INT4
VxlanUdpInitSock (VOID);
PUBLIC INT4
VxlanUdpAddOrRemoveFd (BOOL1 bIsAddFd);
UINT1
VxlanUdpTransmitVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4SrcAddr,
                          UINT4 u4DestAddr, UINT2 u2BufLen, UINT4);
INT4
VxlanSendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4);
PUBLIC INT4
VxlanUdpProcessPktOnSocket (INT4 i4UdpSockFd);
PUBLIC VOID
VxlanUdpDeInitParams (INT4 i4SockId);
PUBLIC VOID
VxlanPktRcvdOnSocket (INT4 i4SockId);

VOID
VxlanProcessUdpMsg (tCRU_BUF_CHAIN_HEADER *pMsg, UINT4 u4AddrType,
                    tIpAddr *pPeerAddr);
INT4
VxlanHandleVxLanMsg (tCRU_BUF_CHAIN_HEADER *pMsg, UINT4 u4AddrType,
                     tIpAddr *pPeerAddr, UINT2 *u2VlanId);
INT4
VxlanHandleIRBMsg (tCRU_BUF_CHAIN_HEADER * pMsg, UINT1 *pu1VrfName);
VOID
VxlanUtilAddHdr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VniNumber);

INT4
VxlanUtilValidateHdr (tVxlanHdr *pVxlanHdr, UINT4 *pu4VniNumber);

tVxlanFsVxlanNveEntry *
VxlanUtilGetNveIndexFromNveTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex);
tVxlanFsVxlanMCastEntry *
VxlanUtilGetNveIndexFromMcastTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex);
tVxlanFsVxlanInReplicaEntry *
VxlanUtilGetNveIndexFromInReplicaTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex);

VOID
VxlanDeleteVlanVniDatabase (UINT2 u2IfVlanId);

VOID
VxlanUtilDelNveIntDatabases (UINT4 u4IfIndex);

VOID
VxlanUdpJoinMulticast (tVxlanFsVxlanMCastEntry *pVxlanMCastEntry);

VOID
VxlanUdpLeaveMulticast (tVxlanFsVxlanMCastEntry *pVxlanMCastEntry);

UINT4
VxlanUtilFindNextFreeReplicaIndex (UINT4 u4FsVxlanNveVniNumber, 
                                                 UINT4 *pu4FreeIndex);

INT4
VxlanUtilGetReplicaIndex (UINT4 u4FsVxlanNveVniNumber, UINT4 *pu4ReplicaIndex);

UINT4
VxlanUtilSetReplicaIndex (UINT4 u4FsVxlanNveVniNumber, UINT4 u4ReplicaIndex);

UINT4
VxlanUtilReleaseReplicaIndex (UINT4 u4FsVxlanNveVniNumber, 
                                             UINT4 u4ReplicaIndex);

tVxlanFsVxlanInReplicaEntry *
VxlanGetReplicaEntry(INT4 i4FsVxlanNveIfIndex, UINT4 u4FsVxlanNveVniNumber,
                                      UINT4 *pu4FsVxlanRemoteVtepAddressLen,
                                    UINT1 *pu1FsVxlanRemoteVtepAddress);
VOID
VxlanInitReplicaIndexMgr PROTO ((VOID));

VOID
VxlanDelIngressReplica (UINT4 u4FsVxlanNveIfIndex,UINT4 u4FsVxlanNveVniNumber);
#ifdef IP6_WANTED
PUBLIC INT4
VxlanUdpv6InitSock (VOID);
PUBLIC INT4
VxlanUdpv6AddOrRemoveFd (BOOL1 bIsAddFd);
UINT1
VxlanUdpv6TransmitVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIp6Addr *pu4SrcAddr,
                            tIp6Addr *pu4DestAddr, UINT2 u2BufLen, UINT4);
INT4
Vxlanv6SendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                    tIp6Addr *pu4SrcAddr , tIp6Addr *pu4DestAddr, UINT4);
PUBLIC INT4
VxlanUdpv6ProcessPktOnSocket (INT4 i4UdpSockFd);
PUBLIC VOID
Vxlanv6PktRcvdOnSocket (INT4 i4SockId);
#endif

VOID
VxlanUdpInit (VOID);
VOID
VxlanUdpDeInit (VOID);
PUBLIC VOID
VxlanDeleteRBTreeEntries (VOID);
VOID
VxlanEvpnRegisterVlanInfo (tVlanBasicInfo * pVlanBasicInfo);


#ifdef EVPN_VXLAN_WANTED
PUBLIC VOID
EvpnDeleteRBTreeEntries (VOID);
INT4 
EvpnVxlanPortCheckEviVniMap (UINT4 u4EviId);
INT4
EvpnUtilIsSetVniMapEntry (UINT4 u4VniIndex);
VOID
EvpnUtilUpdateStats (UINT4 u4VniIndex, INT4 i4EviIndex, UINT1 u1StatType);
INT4
EvpnVxlanUtilGetVniFromEvi (UINT4 u4EviId, UINT4 *pu4VniId);
INT4
EvpnUtilGetEviFromVni (UINT4 u4VniIndex, INT4 *pi4EviIndex);
UINT4
EvpnUtilFindNextFreeRtIndex (UINT4 u4ContextId, UINT4 *pu4FreeIndex, INT4 i4RTType);
INT4
EvpnUtilParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt);
INT4
EvpnUtilParseAndGenerateESI (UINT1 *pu1RandomString, UINT1 *pu1ESIVal);
UINT4
EvpnUtilGetRdFromVniMapTable (UINT4 u4EviIdx, UINT4 u4VniIdx, 
                              UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto);
UINT4
EvpnUtilGetESIFromVniMapTable (UINT4 u4EviIndex, UINT4 u4VniIndex,
                               UINT1 *pu1ESIVal);
UINT4 
EvpnUtilSetRtIndex (UINT4 contextId, UINT4 u4RtIndex, INT4 i4RTType);
UINT4 
EvpnUtilReleaseRtIndex (UINT4 contextId, UINT4 u4RtIndex, INT4 i4RTType);
tVxlanFsEvpnVxlanVrfEntry *
EvpnApiGetVrfTableEntry (UINT1 *pu1EvpnVrfName, UINT2 u2EvpnVrfNameLen,UINT4 u4Vnid);
VOID
EvpnVxlanInitRTIndexMgr PROTO ((VOID));
INT4
EvpnBgp4NotifyASN (UINT1 u1AsnType, UINT4 u4AsnValue);
VOID
EvpnCliShowBgpRD (tCliHandle CliHandle,
                  tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry);
VOID
EvpnCliShowBgpRT (tCliHandle CliHandle,
                  tVxlanFsEvpnVxlanBgpRTEntry *pVxlanMibFsEvpnVxlanBgpRTEntry);
UINT4
EvpnUtlHandleRowStatusActive (tVxlanFsEvpnVxlanEviVniMapEntry *
                              pVxlanFsEvpnVxlanEviVniMapEntry,
                              tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                              pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
UINT4
EvpnUtlProcessTableCreation (tVxlanFsEvpnVxlanEviVniMapEntry *
                             pVxlanFsEvpnVxlanEviVniMapEntry,
                             tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
UINT4
EvpnUtlHandleRowStatusNotInService (tVxlanFsEvpnVxlanEviVniMapEntry *
                                    pVxlanFsEvpnVxlanEviVniMapEntry,
                                    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                    pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
UINT4
EvpnUtlProcessTableDeletion (tVxlanFsEvpnVxlanEviVniMapEntry *
                             pVxlanFsEvpnVxlanEviVniMapEntry,
                             tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
INT4
EvpnUtilGetRtIndex (UINT4 u4ContextId, UINT4 *pu4RtIndex, INT4 i4RTType);

tVxlanFsEvpnVxlanBgpRTEntry *
EvpnGetRTEntryFromValue (INT4 i4EviIndex, UINT4 u4VniIndex, UINT1 *pu1RtValue);
tVxlanFsEvpnVxlanBgpRTEntry *
EvpnGetRTTableFromEviVni (INT4 i4EviIndex, UINT4 u4VniIndex);
tVxlanFsVxlanVtepEntry *
EvpnGetVtepTableFromNveIdx (UINT4 u4NveIfIndex);
VOID 
VxlanDelEviVniMapEntry (INT4 i4FsEvpnIdx);
VOID 
VxlanDelBgpRTEntry (INT4 i4FsEvpnIdx, UINT4 u4VniIdx);
INT4
EvpnBgpUtlGetDefaultRdValue (INT4 i4EviIndex, UINT4 u4VniIndex,
                             UINT1 *pu1RouteDistinguisher);
INT4
EvpnBgpUtlGenerateESIValue (INT4 i4EviIndex, UINT4 u4VniIndex, 
                            UINT1 *pu1ESIValue);
INT4
EvpnBgpUtlGetDefaultRtValue (INT4 i4EviIndex, UINT4 u4VniIndex, UINT4 u4RtIndex,
                             INT4 i4RtType, UINT1 *pu1RouteTarget);
INT4
EvpnUtilValidateNveTable (INT4 i4NveIndex, UINT4 u4VniNumber);
VOID
EvpnBgpGetASN (UINT1 *pu1AsnType, UINT4 *pu4AsnValue);
VOID
EvpnBgpUpdateASN (UINT1 u1AsnType, UINT4 u4AsnValue);
INT4
VxlanEvpnUtilValidateRDRT (UINT1 *pau1RdRt);
VOID EvpnVxlanAddNvedatabase (tEvpnRoute * pEvpnRoute);
VOID VxlanUtilHandleSymmerticAddIrbRoute (tEvpnRoute * pEvpnRoute, 
                                          UINT4 u4NveIndex);
VOID EvpnVxlanDelNvedatabase (tEvpnRoute * pEvpnRoute);
VOID VxlanUtilHandleSymmerticDelIrbRoute (tEvpnRoute * pEvpnRoute,
                                          UINT4 u4NveIndex);
VOID
VxlanUtilAddESInfoInMHPeer (INT4 i4VtepAddressLen, UINT1 *pu1VtepAddress,
                            UINT1 *pu1EviVniEsi);
VOID
VxlanUtilDelESInfoInMHPeer (INT4 i4VtepAddressLen, UINT1 *pu1VtepAddress,
                            UINT1 *pu1EviVniEsi);

INT4
VxlanEvpnDFElection(UINT1 *pau1FsEvpnVxlanMHEviVniESI,
                    UINT1 *pau1FsEvpnVxlanPeerIpAddress);
INT4
EvpnUtilGetVniFromEsiEntry (UINT1 *pu1EviVniEsi, UINT4 *pu4VniIndex);
INT4
EvpnUtilIsEsiPresent (UINT4 u4VniNumber, INT4 i4EviIndex);
INT4
EvpnUtilMultihomedFwdPkt (UINT4 u4VniNumber, INT4 i4EviIndex,
                          tIpAddr * pPeerAddr, UINT2 u2VlanId,
                          tMacAddr DestVmAddr, INT4 i4DfElection);
INT4
EvpnUtilSplitHorizon (tIpAddr * pPeerAddr, UINT1 *pu1ESIString);
tVxlanFsEvpnVxlanMultihomedPeerTable *
EvpnUtilGetMHPeerTable (INT4 i4VtepAddressType, UINT1 *pu1VtepAddress,
                        UINT1 *pu1EviVniEsi);
INT4
EvpnUtilADUpdateToBgp (UINT1 *pau1FsEvpnVxlanMHEviVniESI);
extern INT4 LaUtilEvpnGetMCLAGSystemID (tMacAddr * pRetValEvpnMCLAGSystemID,
                                        INT4 *pi4EvpnMCLAGSystemPriority);
extern INT4
VlanUtilEvpnGetMCLAGSystemID (UINT2 u2VlanId, tMacAddr * pRetValEvpnMCLAGSystemID,
                                                          INT4 *pi4EvpnMCLAGSystemPriority);

tVxlanFsVxlanEcmpNveEntry *
VxlanUtilGetEcmpNveDatabase (UINT4 u4VniId, tMacAddr DestVmAddr, UINT4 u4IfIndex,
                             INT4 i4RemoteVtepAddressType, UINT1 *pu1RemoteVtepAddress);
tVxlanFsVxlanEcmpNveEntry *
VxlanUtilGetNveIndexFromEcmpNveTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex);

UINT1
VxlanHwUpdateEcmpNveDatabase (tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry, UINT1 u1Flag);

INT4 EvpnVxlanIsEcmpRouteUpdate(tEvpnRoute * pEvpnRoute, INT4 i4VlanId);

INT4
EvpnVxlanIsMacLearntFlagSet (UINT4 u4Vni, tMacAddr DestVmMac, UINT4 u4NveIndex);

VOID
EvpnVxlanDeleteMacEntries (UINT4 u4Vni, tMacAddr DestVmMac, UINT4 u4NveIndex);

INT4
EvpnNveEntryTableHashFn (tMacAddr* pMacAddress, UINT4 *pu4HashIndex, UINT4 u4VniId, INT4 i4EviIndex);

VOID
EvpnVxlanIsThisFirstEcmpEntry (tEvpnRoute * pEvpnRoute, UINT4 u4NveIndex, BOOL1 * pbFirstEntryFlag);

UINT4
EvpnUtilGetRdFromVrfTable (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex, UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto);

tVxlanFsEvpnVxlanVrfRTEntry *
EvpnGetVrfRTTableFromVrf (UINT1 * pu1VxlanVrfName, UINT4 u4VniId);

tVxlanFsEvpnVxlanVrfRTEntry *
EvpnGetVrfRTEntryFromValue (UINT1 * pu1VxlanVrfName, UINT1 *pu1RtValue);

INT4
EvpnVrfUtlGetDefaultRdValue (UINT1 * pu1VxlanVrfName,
UINT4  u4VniId,UINT1 *pu1RouteDistinguisher);
INT4
EvpnVrfUtlGetDefaultRtValue (UINT1 * pu1VxlanVrfName,UINT4 u4VniId,  UINT4 u4RtIndex,
                             INT4 i4RtType, UINT1 *pu1RouteTarget);
UINT4
EvpnUtlHandleRowStatusActiveVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                              pVxlanFsEvpnVxlanVrfEntry,
                              tVxlanIsSetFsEvpnVxlanVrfEntry *
                              pVxlanIsSetFsEvpnVxlanVrfEntry);
UINT4
EvpnUtlProcessTableCreationVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                             pVxlanFsEvpnVxlanVrfEntry,
                             tVxlanIsSetFsEvpnVxlanVrfEntry *
                             pVxlanIsSetFsEvpnVxlanVrfEntry);
UINT4
EvpnUtlHandleRowStatusNotInServiceVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                    pVxlanFsEvpnVxlanVrfEntry,
                                    tVxlanIsSetFsEvpnVxlanVrfEntry *
                                    pVxlanIsSetFsEvpnVxlanVrfEntry);
UINT4 
EvpnUtlProcessTableDeletionVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                     pVxlanFsEvpnVxlanVrfEntry, 
                                     tVxlanIsSetFsEvpnVxlanVrfEntry * 
                                     pVxlanIsSetFsEvpnVxlanVrfEntry);      
VOID
EvpnVxlanProcessEsiAndIfStateChange(UINT2 u2VlanId,tMacAddr SysID,
           UINT2 u2SysPriority, UINT1 u1OperStatus);
VOID
VxlanHwUpdateMHPeerDatabase (UINT1 u1DFFlag, UINT4 u4Vni, UINT1 u1Flag);
VOID
VxlanHwUpdateArpSuppression (tVxlanFsVxlanNveEntry *pVxlanNveEntry, UINT1 u1Flag);
VOID
VxlanDelVrfRTEntry (UINT1 *pu1VxlanVrfName, UINT4 u4VniIdx);

VOID
VxlanDelVrfEntry (UINT1 *pu1VxlanVrfName);

INT4
EvpnUtilGetVrfFromVni (UINT4 u4VniId, UINT4 *pu4VrfId);

VOID
VxlanEvpnRegisterArpInfo (tArpBasicInfo * pArpBasicInfo);

INT4
EvpnUtilGetL3VniFromVrfEntry (UINT1 *pu1VrfName, UINT4 *pu4L3VniId);

INT4
EvpnUtilGetVrfNameFromVrfEntry (UINT4 u4L3VniId, UINT1 *pu1VrfName);

VOID
EvpnVrfCliShowBgpRT (tCliHandle CliHandle,
                     tVxlanFsEvpnVxlanVrfRTEntry *pVxlanMibFsEvpnVxlanVrfRTEntry);
VOID
EvpnVrfCliShowBgpRD (tCliHandle CliHandle,
                     tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfVniMapEntry);
VOID
VrfUtilUpdateStats (UINT4 u4VniNumber, UINT1 *pVrfName, UINT1 u1StatType);
INT4
VxlanCliShowVxlanVrfStatistics (tCliHandle CliHandle,UINT1 *pu1VrfName,UINT4 u4Vnid,UINT4 u4VrfNameLength);

INT4
EvpnCliCheckRdInVniMapTable(UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                            UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto);

#endif /* EVPN_VXLAN_WANTED */
VOID
VxlanUtilUpdateStats (UINT2 u2VlanId, UINT1 u1StatType);

VOID
VxlanHandlPktFromPim(tCRU_BUF_CHAIN_HEADER *pMsg, UINT4 u4AddrType);

/****************************** Prototypes for redundancy ******************/
UINT4
VxlanRedInitGlobalInfo PROTO ((VOID));

VOID
VxlanRedHandleRmEvents PROTO ((tVxlanRmMsg *pBfdRmMsg));

INT4
VxlanRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));

UINT4
VxlanRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

VOID
VxlanRedHandleGoActive PROTO ((VOID));

VOID
VxlanRedHandleIdleToActive PROTO ((VOID));

VOID
VxlanRedHandleStandbyToActive PROTO ((VOID));

VOID
VxlanRedSendBulkUpdMsg PROTO ((VOID));

VOID
VxlanRedSyncDynInfo PROTO ((VOID));

VOID
VxlanRedAddAllNodeInDbTbl PROTO ((VOID));

VOID
VxlanRedDbUtilAddTblNode PROTO ((tDbTblDescriptor * pVxlanDataDesc,
                         tDbTblNode * pVxlanSessDbNode));

UINT4
VxlanRedSendMsgToRm PROTO ((tRmMsg * pMsg, UINT4 u4Length));

VOID
VxlanRedSendBulkUpdTailMsg PROTO ((VOID));

VOID
VxlanRedHandleGoStandby PROTO ((tVxlanRmMsg * pMsg));

VOID
VxlanRedHandleActiveToStandby PROTO ((VOID));

VOID
VxlanRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

VOID
VxlanRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

VOID
VxlanRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));

VOID
VxlanRedProcessDynamicInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));

VOID
VxlanRedHandleIdleToStandby PROTO ((VOID));

VOID
VxlanRedSendBulkReqMsg PROTO ((VOID));

VOID
VxlanRedDynDataDescInit PROTO ((VOID));

VOID
VxlanRedDescrTblInit PROTO ((VOID));

VOID
VxlanRedDbNodeInit PROTO ((tDbTblNode * pVxlanDbTblNode));

INT4
VxlanRedDeInitGlobalInfo PROTO ((VOID));

INT4
VxlanRedGetRmMsg (tVxlanRmSyncMsg ** pMsg, UINT1 u1Message, UINT4 *pu4OffSet);

VOID
VxlanRedSyncDynPortInfo(VOID);

VOID
VxlanRedSyncDynNvePortInfo(tVxlanNvePortEntry *pVxlanNvePortEntry);

VOID
VxlanRedSyncDynVlanVniPortInfo(tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry,
                                       tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry);

VOID
VxlanRedSyncDynVlanVniPortMacInfo(tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry,
                                          tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry,
                                 UINT4 u4AccessPort);
VOID
VxlanRedProcessDynamicNvePortInfo(tRmMsg * pMsg, UINT4 *pu4OffSet);

VOID
VxlanRedProcessDynamicMacPortInfo(tRmMsg * pMsg, UINT4 *pu4OffSet);

VOID
VxlanRedProcessDynamicAccessPortInfo(tRmMsg * pMsg, UINT4 *pu4OffSet);

PUBLIC VOID
VxlanRedSyncDynBufferInfo (VOID);

/*VXLAN NPAPI related functions */

INT4 VxlanHwGetVniStats(UINT4 u4VniId);
INT4 VxlanHwGetVniRcvdStats(UINT4 u4VniId);

UINT1
VxlanHwInit (UINT1 u1Flag);

UINT1
VxlanHwUpdateUdpPort (UINT4 u4FsVxlanUdpPort, UINT1 u1Flag);

UINT1
VxlanHwUpdateNveDatabase (tVxlanFsVxlanNveEntry *pVxlanNveEntry, UINT1 u1Flag);

UINT1
VxlanHwUpdateMcastDatabase (tVxlanFsVxlanMCastEntry *pVxlanMCastEntry, UINT1 u1Flag);

UINT1
VxlanHwUpdateVniVlanMapDatabase (tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry,
                                                                  UINT1 u1Flag);
UINT1
VxlanHwUpdateInReplicaDatabase (tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry, UINT1 u1Flag);

UINT1
VxlanHwAddLoopBackPortToVlan (tVxlanFsEvpnVxlanVrfEntry *pVxlanVrfEntry, UINT1 u1Flag);

VOID
VxlanProcessTimerExpiry (VOID);
INT4
VxlanRemoveVniMacEntries (tCliHandle CliHandle, UINT4 *pu4VniID);
INT4
VxlanRemoveVniNveEntries (tCliHandle CliHandle, UINT4 *pu4VniID);
VOID
VxlanIpRtChgEventHandler (tNetIpv4RtInfo * pNetIpv4RtInfo, tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType);
VOID
VxlanRegisterWithIP (VOID);
VOID
VxlanHandleRtChgEvent (UINT4 u4DestNet, UINT4 u4RtIfIndex, UINT1 u1CmdType);
VOID
VxlanHandleNveIntAdminStatus(UINT4 u4IfIndex, INT4 i4IfMainAdminStatus);

VOID
VxlanHandleVlanVniStatus(UINT4 u4IfIndex, UINT4 u4VniNumber,INT4 i4VlanId,
                         UINT4 u4Status);

UINT4
EvpnUtilCheckRdVniMapTable (UINT4 u4EviIndex, UINT4 u4VniIndex,
                           UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto);

UINT4
EvpnUtilCheckRdVrfVniMapTable (UINT1 *pu1VrfName, UINT4 u4VniIndex,
                               UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto);

UINT1 *  VxlanPrintIpAddr (UINT1 *pu1IpAddr);
UINT1
VxlanHwUpdateVniVlanMapDbBasedOnVlanPorts (tVxlanFsVxlanVniVlanMapEntry
                                           *pVxlanVniVlantEntry,
                                           UINT1 u1Flag,
                                           tLocalPortList LocalPortList);
VOID 
VxlanL2TableFlushIndication(VOID);

#ifdef MBSM_WANTED
INT4 VxlanMbsmCardInsertRemove(tMbsmSlotInfo *pSlotInfo, UINT4 u4Flag);
INT4 VxlanMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);

INT4 VxlanHandleMbsmVniVlanMapDB (tMbsmSlotInfo *pSlotInfo, UINT4 u4Flag);
INT4 VxlanHandleMbsmNveDB(tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag);
UINT1 VxlanHandleMbsmInReplicaDB (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag);

UINT1 
VxlanMbsmHwInit (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag);
UINT1
VxlanMbsmHwUpdateUdpPort (UINT4 u4VxlanUdpPort, tMbsmSlotInfo * pSlotInfo,
        UINT4 u4Flag);
UINT1
VxlanMbsmHwUpdateNveDatabase (tVxlanHwNveInfo *pVxlanHwNveInfo,
        tMbsmSlotInfo * pSlotInfo,
        UINT4 u4Flag);
UINT1
VxlanMbsmHwUpdateL2Table (tVxlanHwNveInfo * pVxlanHwNveInfo,
                          tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag);
UINT1
VxlanMbsmVniVlanPortMapDatabase (tVxlanHwVniVlanInfo *pVxlanHwVniVlanInfo,
        tMbsmSlotInfo *pSlotInfo, UINT4 u4Flag);
UINT1
VxlanMbsmHwUpdateInReplicaDatabase (tVxlanHwBUMReplicaInfo *pVxlanHwRepInfo,
        tMbsmSlotInfo *pSlotInfo, UINT4 u4Flag);
UINT1
VxlanMbsmHwUpdateReplicaMCast (tVxlanHwBUMReplicaInfo * pVxlanHwRepInfo,
                             tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag);

UINT1
VxlanMbsmHwUpdateEcmpNveDatabase (tVxlanHwNveInfo *pVxlanHwNveInfo,
        tMbsmSlotInfo * pSlotInfo,
        UINT4 u4Flag);

INT4
VxlanHandleMbsmUpdateNveReplicaDB(tMbsmSlotInfo * pSlotInfo); 
#endif

VOID
VxlanAddNveDatabase (tMacAddr NpMacAddr, tVlanId VlanId2,UINT4 u4RemoteVTEPIp);
VOID
VxlanDelNveDatabase (tMacAddr NpMacAddr, tVlanId VlanId2);
VOID
VxlanNvePortEntryRem(UINT1 *pau1Address,INT4 i4AddrLen);
tVxlanNvePortEntry *
VxlanGetNvePortEntry(UINT1 *pau1Address,INT4 i4AddrLen);
tVxlanNvePortEntry *
VxlanCheckRouteAndUpdateTunnelMapTable (UINT1 *pau1Address,INT4 i4AddrLen,UINT4 u4VxlanTunnelIfIndex);
tVxlanVniVlanPortEntry *
VxlanVniVlanPortTableAdd (tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry,
        UINT4 u4IfIndex);
tVxlanVniVlanPortMacEntry *
VxlanVniVlanPortMacTableAdd (tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry,
        UINT4 u4IfIndex, tMacAddr VxlanMac);
tVxlanNvePortEntry * VxlanNvePortTableAdd (UINT1 *pau1Address,INT4 i4AddrLen);
UINT1
VxlanMbsmVniVlanMapDatabase (tVxlanHwVniVlanInfo *pVxlanHwVniVlanInfo,
                                 tMbsmSlotInfo *pSlotInfo, UINT4 u4Flag);
#endif   /* __VXLANPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlanprot.h                     */
/*-----------------------------------------------------------------------*/
