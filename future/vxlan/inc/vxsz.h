/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxsz.h,v 1.5 2018/01/05 09:57:10 siva Exp $
*
*********************************************************************/

enum {
    MAX_VXLAN_FSVXLANNVETABLE_SIZING_ID,
    MAX_VXLAN_FSVXLANMCASTTABLE_SIZING_ID,
    MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_SIZING_ID,
    MAX_VXLAN_FSVXLANVTEPTABLE_SIZING_ID,
    MAX_VXLAN_FSVXLANINREPLICATABLE_SIZING_ID,
    MAX_VXLAN_FSVXLANNVETABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSVXLANMCASTTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSVXLANVTEPTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSVXLANINREPLICATABLE_ISSET_SIZING_ID,
    MAX_VXLAN_QUEUE_SIZING_ID,
    MAX_VXLAN_UDP_RECV_BUF_SIZING_ID,
    MAX_VXLAN_UDP_SEND_BUF_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANVRFTABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANVRFTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_SIZING_ID,
    MAX_VXLAN_FSVXLANECMPNVETABLE_SIZING_ID,
    MAX_VXLAN_FSEVPNVXLANARPTABLE_SIZING_ID,
    MAX_VXLAN_EVPNARPSUPMACTABLE_SIZING_ID,
    MAX_VXLAN_VNIVLANPORTTABLE_SIZING_ID,
    MAX_VXLAN_VNIVLANPORTMACTABLE_SIZING_ID,
    MAX_VXLAN_NVEPORTTABLE_SIZING_ID,
    MAX_VXLAN_RED_MSG_SIZING_ID,
    VXLAN_MAX_SIZING_ID
};


#ifdef  _VXLANSZ_C
tMemPoolId VXLANMemPoolIds[ VXLAN_MAX_SIZING_ID];
INT4  VxlanSizingMemCreateMemPools(VOID);
VOID  VxlanSizingMemDeleteMemPools(VOID);
INT4  VxlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _VXLANSZ_C  */
extern tMemPoolId VXLANMemPoolIds[ ];
extern INT4  VxlanSizingMemCreateMemPools(VOID);
extern VOID  VxlanSizingMemDeleteMemPools(VOID);
extern INT4  VxlanSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _VXLANSZ_C  */


#ifdef  _VXLANSZ_C
tFsModSizingParams FsVXLANSizingParams [] = {
{ "tVxlanFsVxlanNveEntry", "MAX_VXLAN_FSVXLANNVETABLE", sizeof(tVxlanFsVxlanNveEntry),MAX_VXLAN_FSVXLANNVETABLE, MAX_VXLAN_FSVXLANNVETABLE,0 },
{ "tVxlanFsVxlanMCastEntry", "MAX_VXLAN_FSVXLANMCASTTABLE",  sizeof(tVxlanFsVxlanMCastEntry), MAX_VXLAN_FSVXLANMCASTTABLE, MAX_VXLAN_FSVXLANMCASTTABLE,0 },  
{ "tVxlanFsVxlanVniVlanMapEntry", "MAX_VXLAN_FSVXLANVNIVLANMAPTABLE", sizeof(tVxlanFsVxlanVniVlanMapEntry), MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, MAX_VXLAN_FSVXLANVNIVLANMAPTABLE,0 },
{"tVxlanFsVxlanVtepEntry", "MAX_VXLAN_FSVXLANVTEPTABLE", sizeof(tVxlanFsVxlanVtepEntry), MAX_VXLAN_FSVXLANVTEPTABLE, MAX_VXLAN_FSVXLANVTEPTABLE, 0},    
{ "tVxlanFsVxlanInReplicaEntry", "MAX_VXLAN_FSVXLANINREPLICATABLE", sizeof(tVxlanFsVxlanInReplicaEntry),MAX_VXLAN_FSVXLANINREPLICATABLE, MAX_VXLAN_FSVXLANINREPLICATABLE,0 },
{ "tVxlanFsVxlanNveEntry", "MAX_VXLAN_FSVXLANNVETABLE_ISSET", sizeof(tVxlanFsVxlanNveEntry),MAX_VXLAN_FSVXLANNVETABLE, MAX_VXLAN_FSVXLANNVETABLE,0 },
{ "tVxlanFsVxlanMCastEntry", "MAX_VXLAN_FSVXLANMCASTTABLE_ISSET",  sizeof(tVxlanFsVxlanMCastEntry), MAX_VXLAN_FSVXLANMCASTTABLE, MAX_VXLAN_FSVXLANMCASTTABLE,0 },
{ "tVxlanFsVxlanVniVlanMapEntry", "MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET", sizeof(tVxlanFsVxlanVniVlanMapEntry), MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, MAX_VXLAN_FSVXLANVNIVLANMAPTABLE,0 },
{"tVxlanFsVxlanVtepEntry", "MAX_VXLAN_FSVXLANVTEPTABLE_ISSET", sizeof(tVxlanFsVxlanVtepEntry), MAX_VXLAN_FSVXLANVTEPTABLE, MAX_VXLAN_FSVXLANVTEPTABLE, 0},
{ "tVxlanFsVxlanInReplicaEntry", "MAX_VXLAN_FSVXLANINREPLICATABLE_ISSET", sizeof(tVxlanFsVxlanInReplicaEntry),MAX_VXLAN_FSVXLANINREPLICATABLE, MAX_VXLAN_FSVXLANINREPLICATABLE,0 },
{"tVxlanIfMsg", "MAX_VXLAN_QUEUE_MSG", sizeof (tVxlanIfMsg), MAX_VXLAN_QUEUE_MSG, MAX_VXLAN_QUEUE_MSG, 0},
{ "tVxlanMinMTUSize", "MAX_VXLAN_UDP_RECV_BUF", sizeof(tVxlanMinMTUSize),MAX_VXLAN_UDP_RECV_BUF, MAX_VXLAN_UDP_RECV_BUF,0 },
{ "tVxlanMinMTUSize", "MAX_VXLAN_UDP_SEND_BUF", sizeof(tVxlanMinMTUSize),MAX_VXLAN_UDP_SEND_BUF, MAX_VXLAN_UDP_SEND_BUF,0 },
{ "tVxlanFsEvpnVxlanEviVniMapEntry", "MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE", sizeof(tVxlanFsEvpnVxlanEviVniMapEntry),MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE, MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE,0 },
{ "tVxlanFsEvpnVxlanEviVniMapEntry", "MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET", sizeof(tVxlanFsEvpnVxlanEviVniMapEntry),MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE, MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE,0 },
{ "tVxlanFsEvpnVxlanBgpRTEntry", "MAX_VXLAN_FSEVPNVXLANBGPRTTABLE", sizeof(tVxlanFsEvpnVxlanBgpRTEntry),MAX_VXLAN_FSEVPNVXLANBGPRTTABLE, MAX_VXLAN_FSEVPNVXLANBGPRTTABLE,0 },
{ "tVxlanFsEvpnVxlanBgpRTEntry", "MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET", sizeof(tVxlanFsEvpnVxlanBgpRTEntry),MAX_VXLAN_FSEVPNVXLANBGPRTTABLE, MAX_VXLAN_FSEVPNVXLANBGPRTTABLE,0 },
{ "tVxlanFsEvpnVxlanVrfEntry", "MAX_VXLAN_FSEVPNVXLANVRFTABLE", sizeof(tVxlanFsEvpnVxlanVrfEntry),MAX_VXLAN_FSEVPNVXLANVRFTABLE, MAX_VXLAN_FSEVPNVXLANVRFTABLE,0 },
{ "tVxlanFsEvpnVxlanVrfEntry", "MAX_VXLAN_FSEVPNVXLANVRFTABLE_ISSET", sizeof(tVxlanFsEvpnVxlanVrfEntry),MAX_VXLAN_FSEVPNVXLANVRFTABLE, MAX_VXLAN_FSEVPNVXLANVRFTABLE,0 },
{ "tVxlanFsEvpnVxlanVrfRTEntry", "MAX_VXLAN_FSEVPNVXLANVRFRTTABLE", sizeof(tVxlanFsEvpnVxlanVrfRTEntry),MAX_VXLAN_FSEVPNVXLANVRFRTTABLE, MAX_VXLAN_FSEVPNVXLANVRFRTTABLE,0 },
{ "tVxlanFsEvpnVxlanVrfRTEntry", "MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET", sizeof(tVxlanFsEvpnVxlanVrfRTEntry),MAX_VXLAN_FSEVPNVXLANVRFRTTABLE, MAX_VXLAN_FSEVPNVXLANVRFRTTABLE,0 },
{ "tVxlanFsEvpnVxlanMultihomedPeerTable", "MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE", sizeof(tVxlanFsEvpnVxlanMultihomedPeerTable),MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE, MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE,0 },
{ "tVxlanFsEvpnVxlanMultihomedPeerTable", "MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET", sizeof(tVxlanFsEvpnVxlanMultihomedPeerTable),MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE, MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE,0 },
{ "tVxlanFsVxlanEcmpNveEntry", "MAX_VXLAN_FSVXLANECMPNVETABLE", sizeof(tVxlanFsVxlanEcmpNveEntry),MAX_VXLAN_FSVXLANECMPNVETABLE, MAX_VXLAN_FSVXLANECMPNVETABLE,0 },
{ "tVxlanFsEvpnVxlanArpEntry", "MAX_VXLAN_FSEVPNVXLANARPTABLE", sizeof(tVxlanFsEvpnVxlanArpEntry),MAX_VXLAN_FSEVPNVXLANARPTABLE, MAX_VXLAN_FSEVPNVXLANARPTABLE,0 },
{ "tVxlanEvpnArpSupLocalMacEntry", "MAX_VXLAN_FSVXLANNVETABLE", sizeof(tVxlanEvpnArpSupLocalMacEntry),MAX_VXLAN_FSVXLANNVETABLE, MAX_VXLAN_FSVXLANNVETABLE,0 },
{ "tVxlanVniVlanPortEntry", "MAX_VXLAN_FSVXLANVNIVLANMAPTABLE", sizeof(tVxlanVniVlanPortEntry), MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, 0},
{ "tVxlanVniVlanPortMacEntry", "MAX_VXLAN_FSVXLANNVETABLE", sizeof(tVxlanVniVlanPortMacEntry), MAX_VXLAN_FSVXLANNVETABLE, MAX_VXLAN_FSVXLANNVETABLE, 0},
{ "tVxlanNvePortEntry", "MAX_VXLAN_FSVXLANVNIVLANMAPTABLE", sizeof(tVxlanNvePortEntry), MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, MAX_VXLAN_FSVXLANVNIVLANMAPTABLE, 0},
{ "tVxlanRmSyncMsg", "MAX_VXLAN_RED_MSGS", sizeof(tVxlanRmSyncMsg), MAX_VXLAN_RED_MSGS, MAX_VXLAN_RED_MSGS, 0},

{"\0","\0",0,0,0,0}
};
#else  /*  _VXLANSZ_C  */
extern tFsModSizingParams FsVXLANSizingParams [];
#endif /*  _VXLANSZ_C  */


