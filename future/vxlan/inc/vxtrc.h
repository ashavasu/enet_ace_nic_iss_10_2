/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: vxtrc.h,v 1.2 2015/07/02 10:54:09 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __VXLANTRC_H__
#define __VXLANTRC_H__

#define  VXLAN_TRC_FLAG  gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption
#define  VXLAN_NAME      "VXLAN"               


#define  VXLAN_TRC(x)       VxlanTrcPrint( __FILE__, __LINE__, VxlanTrc x)
#define  VXLAN_TRC_FUNC(x)  VxlanTrcPrint( __FILE__, __LINE__, VxlanTrc x)
#define  VXLAN_TRC_CRIT(x)  VxlanTrcPrint( __FILE__, __LINE__, VxlanTrc x)
#define  VXLAN_TRC_PKT(x)   VxlanTrcWrite( VxlanTrc x)

#define   VXLAN_ALL_TRC    (0xffffffff)
#define   VXLAN_CRT_TRC    (0x00000001)
#define   VXLAN_UTIL_TRC    (0x00000002)
#define   VXLAN_CLI_TRC    (0x00000004)
#define   VXLAN_ENTRY_EXIT_TRC    (0x00000008)
#define   VXLAN_FAIL_TRC    (0x00000010)
#define   VXLAN_MEMORY_TRC    (0x00000020)
#define   VXLAN_PKT_TRC    (0x00000040)
#define   VXLAN_MAIN_TRC   (0x00000080)
#define   VXLAN_EVPN_TRC   (0x00000100)

#if 0
#define  VXLAN_FN_ENTRY  (0x1 << 9)
#define  VXLAN_FN_EXIT   (0x1 << 10)
#define  VXLAN_CLI_TRC   (0x1 << 11)
#define  VXLAN_MAIN_TRC  (0x1 << 12)
#define  VXLAN_PKT_TRC   (0x1 << 13)
#define  VXLAN_QUE_TRC   (0x1 << 14)
#define  VXLAN_TASK_TRC  (0x1 << 15)
#define  VXLAN_TMR_TRC   (0x1 << 16)
#define  VXLAN_UTIL_TRC  (0x1 << 17)
#define  VXLAN_ALL_TRC   VXLAN_FN_ENTRY |\
                        VXLAN_FN_EXIT  |\
                        VXLAN_CLI_TRC  |\
                        VXLAN_MAIN_TRC |\
                        VXLAN_PKT_TRC  |\
                        VXLAN_QUE_TRC  |\
                        VXLAN_TASK_TRC |\
                        VXLAN_TMR_TRC  |\
                        VXLAN_UTIL_TRC
#endif

#endif /* _VXLANTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlantrc.h                      */
/*-----------------------------------------------------------------------*/
