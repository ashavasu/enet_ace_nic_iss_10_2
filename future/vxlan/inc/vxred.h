/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vxred.h,v 1.2 2018/01/05 09:57:10 siva Exp $
 *
 * Description: This file contains all macro definitions and
 *              function prototypes for Vxlan module.
 *
 *******************************************************************/
#ifndef __VXLAN_RED_H
#define __VXLAN_RED_H

enum{
    VXLAN_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type  */
    VXLAN_MAX_DYN_INFO_TYPE
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
};

/* Represents the message types encoded in the update messages */
typedef enum {
    VXLAN_RED_DYN_CACHE_INFO  = VXLAN_DYN_INFO,
    VXLAN_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    VXLAN_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    VXLAN_RED_NVE_PORT_ENTRY,
    VXLAN_RED_MAC_PORT_ENTRY,
    VXLAN_RED_ACCESS_PORT_ENTRY
}eVxlanRedRmMsgType;

typedef enum{
    VXLAN_HA_UPD_NOT_STARTED = 1,/* 1 */
    VXLAN_HA_UPD_IN_PROGRESS,    /* 2 */
    VXLAN_HA_UPD_COMPLETED,      /* 3 */
    VXLAN_HA_UPD_ABORTED,        /* 4 */
    VXLAN_HA_MAX_BLK_UPD_STATUS
} eVxlanHaBulkUpdStatus;


#define VXLAN_RED_BULK_UPD_TAIL_MSG_SIZE   3 /* MsgType(1Byte) + Length (2Byte) */
#define VXLAN_RED_BULK_REQ_MSG_SIZE        3
#define VXLAN_RED_TYPE_FIELD_SIZE          1
#define VXLAN_RED_LEN_FIELD_SIZE           2
#define VXLAN_RED_MSG_MAX_LEN              1500

#ifdef L2RED_WANTED
#define VXLAN_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define VXLAN_GET_RMNODE_STATUS()  RM_ACTIVE
#endif


#define VXLAN_NUM_STANDBY_NODES() gVxlanRedGblInfo.u1NumPeersUp

#define VXLAN_RM_BULK_REQ_RCVD() gVxlanRedGblInfo.bBulkReqRcvd

#define VXLAN_RM_GET_NUM_STANDBY_NODES_UP() \
          gVxlanRedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define VXLAN_IS_STANDBY_UP() \
          ((gVxlanRedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)


#define VXLAN_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define VXLAN_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define VXLAN_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define VXLAN_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define VXLAN_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
            RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                            *(pu4Offset) += 1;\
}while (0)

#define VXLAN_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
            RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                            *(pu4Offset) += 2;\
}while (0)

#define VXLAN_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
            RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                            *(pu4Offset) += 4;\
}while (0)

#define VXLAN_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
            RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                            *(pu4Offset) += u4Size;\
}while (0)



#endif 

