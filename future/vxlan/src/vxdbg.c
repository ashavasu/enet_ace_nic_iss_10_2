/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdbg.c,v 1.25 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Vxlan 
*********************************************************************/

#include "vxinc.h"
#include "vxlannp.h"

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanVtepTable
 Input       :  pVxlanGetFsVxlanVtepEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanVtepTable (tVxlanFsVxlanVtepEntry * pVxlanGetFsVxlanVtepEntry)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) pVxlanGetFsVxlanVtepEntry);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanVtepTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
            pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
            pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);

    pVxlanGetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

    pVxlanGetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanNveTable
 Input       :  pVxlanGetFsVxlanNveEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanNveTable (tVxlanFsVxlanNveEntry * pVxlanGetFsVxlanNveEntry)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   (tRBElem *) pVxlanGetFsVxlanNveEntry);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanNveTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen;

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen;

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType;

    pVxlanGetFsVxlanNveEntry->i4FsVxlanNveEvpnMacType =
        pVxlanFsVxlanNveEntry->i4FsVxlanNveEvpnMacType;

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;

    pVxlanGetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanMCastTable
 Input       :  pVxlanGetFsVxlanMCastEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanMCastTable (tVxlanFsVxlanMCastEntry *
                              pVxlanGetFsVxlanMCastEntry)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanMCastEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   (tRBElem *) pVxlanGetFsVxlanMCastEntry);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanMCastTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType;

    MEMCPY (pVxlanGetFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
            pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
            pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen);

    pVxlanGetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen;

    pVxlanGetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
            pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
            pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen);

    pVxlanGetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen;

    pVxlanGetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanVniVlanMapTable
 Input       :  pVxlanGetFsVxlanVniVlanMapEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                   pVxlanGetFsVxlanVniVlanMapEntry)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) pVxlanGetFsVxlanVniVlanMapEntry);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanVniVlanMapTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanDfElection =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanDfElection;

    pVxlanGetFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanInReplicaTable
 Input       :  pVxlanGetFsVxlanInReplicaEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                                  pVxlanGetFsVxlanInReplicaEntry)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanInReplicaEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   (tRBElem *) pVxlanGetFsVxlanInReplicaEntry);

    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanInReplicaTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaVtepAddressType =
        pVxlanFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanInReplicaEntry->
            MibObject.au1FsVxlanInReplicaVtepAddress,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.au1FsVxlanInReplicaVtepAddress,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.i4FsVxlanInReplicaVtepAddressLen);

    pVxlanGetFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaVtepAddressLen =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen;

    pVxlanGetFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressType =
        pVxlanFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanInReplicaEntry->
            MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen);

    pVxlanGetFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen =
        pVxlanFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen;

    pVxlanGetFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRowStatus =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus;
    return OSIX_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanEviVniMapTable
 Input       :  pVxlanGetFsEvpnVxlanEviVniMapEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                      pVxlanGetFsEvpnVxlanEviVniMapEntry)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanEviVniMapEntry);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanEviVniMapTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pVxlanGetFsEvpnVxlanEviVniMapEntry->
            MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);

    pVxlanGetFsEvpnVxlanEviVniMapEntry->
        MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen =
        pVxlanFsEvpnVxlanEviVniMapEntry->
        MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen;

    MEMCPY (pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
            au1FsEvpnVxlanEviVniESI,
            pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniESI,
            pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniESILen);

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniLoadBalance =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniLoadBalance;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapSentPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapSentPkts;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapRcvdPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapRcvdPkts;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapDroppedPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapDroppedPkts;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapRowStatus =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapRowStatus;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapBgpRDAuto =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapBgpRDAuto;

    pVxlanGetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapTotalPaths =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapTotalPaths;

    if (VxlanGetAllUtlFsEvpnVxlanEviVniMapTable
        (pVxlanGetFsEvpnVxlanEviVniMapEntry,
         pVxlanFsEvpnVxlanEviVniMapEntry) == OSIX_FAILURE)

    {
        VXLAN_TRC ((VXLAN_UTIL_TRC, "VxlanGetAllFsEvpnVxlanEviVniMapTable:"
                    "VxlanGetAllUtlFsEvpnVxlanEviVniMapTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanBgpRTTable
 Input       :  pVxlanGetFsEvpnVxlanBgpRTEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                  pVxlanGetFsEvpnVxlanBgpRTEntry)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanBgpRTEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanBgpRTEntry);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanBgpRTTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pVxlanGetFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
            pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
            pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen);

    pVxlanGetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen;

    pVxlanGetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus;

    pVxlanGetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto;

    if (VxlanGetAllUtlFsEvpnVxlanBgpRTTable
        (pVxlanGetFsEvpnVxlanBgpRTEntry,
         pVxlanFsEvpnVxlanBgpRTEntry) == OSIX_FAILURE)

    {
        VXLAN_TRC ((VXLAN_UTIL_TRC, "VxlanGetAllFsEvpnVxlanBgpRTTable:"
                    "VxlanGetAllUtlFsEvpnVxlanBgpRTTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanVrfTable
 Input       :  pVxlanGetFsEvpnVxlanVrfEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                pVxlanGetFsEvpnVxlanVrfEntry)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanVrfEntry);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanVrfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pVxlanGetFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
            pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
            pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen);

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen;

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus;

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto;

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts;

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts;

    pVxlanGetFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts;

    if (VxlanGetAllUtlFsEvpnVxlanVrfTable
        (pVxlanGetFsEvpnVxlanVrfEntry,
         pVxlanFsEvpnVxlanVrfEntry) == OSIX_FAILURE)

    {
        VXLAN_TRC ((VXLAN_UTIL_TRC, "VxlanGetAllFsEvpnVxlanVrfTable:"
                    "VxlanGetAllUtlFsEvpnVxlanVrfTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanVrfRTTable
 Input       :  pVxlanGetFsEvpnVxlanVrfRTEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                  pVxlanGetFsEvpnVxlanVrfRTEntry)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanVrfRTEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanVrfRTEntry);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanVrfRTTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pVxlanGetFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
            pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
            pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen);

    pVxlanGetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen =
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen;

    pVxlanGetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus;

    pVxlanGetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto =
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto;

    if (VxlanGetAllUtlFsEvpnVxlanVrfRTTable
        (pVxlanGetFsEvpnVxlanVrfRTEntry,
         pVxlanFsEvpnVxlanVrfRTEntry) == OSIX_FAILURE)

    {
        VXLAN_TRC ((VXLAN_UTIL_TRC, "VxlanGetAllFsEvpnVxlanVrfRTTable:"
                    "VxlanGetAllUtlFsEvpnVxlanVrfRTTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanMultihomedPeerTable
 Input       :  pVxlanGetFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanMultihomedPeerTable (tVxlanFsEvpnVxlanMultihomedPeerTable
                                           *
                                           pVxlanGetFsEvpnVxlanMultihomedPeerTable)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanMultihomedPeerTable =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanMultihomedPeerTable);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanMultihomedPeerTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum;

    pVxlanGetFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus;

    if (VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable
        (pVxlanGetFsEvpnVxlanMultihomedPeerTable,
         pVxlanFsEvpnVxlanMultihomedPeerTable) == OSIX_FAILURE)

    {
        VXLAN_TRC ((VXLAN_UTIL_TRC, "VxlanGetAllFsEvpnVxlanMultihomedPeerTable:"
                    "VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllFsVxlanEcmpNveTable
 Input       :  pVxlanGetFsVxlanEcmpNveEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsVxlanEcmpNveTable (tVxlanFsVxlanEcmpNveEntry *
                                pVxlanGetFsVxlanEcmpNveEntry)
{
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsVxlanEcmpNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                   (tRBElem *) pVxlanGetFsVxlanEcmpNveEntry);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsVxlanEcmpNveTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpNveVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            i4FsVxlanEcmpNveVtepAddressLen);

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen;

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressType;

    MEMCPY (pVxlanGetFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            i4FsVxlanEcmpNveRemoteVtepAddressLen);

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressLen =
        pVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressLen;

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveStorageType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveStorageType;

    pVxlanGetFsVxlanEcmpNveEntry->i4FsVxlanEcmpNveEvpnMacType =
        pVxlanFsVxlanEcmpNveEntry->i4FsVxlanEcmpNveEvpnMacType;

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpSuppressArp =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpSuppressArp;

    MEMCPY (pVxlanGetFsVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
            pVxlanFsVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
            EVPN_MAX_ESI_LEN);

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
        pVxlanFsVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpActive;

    pVxlanGetFsVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt =
        pVxlanFsVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt;

    return OSIX_SUCCESS;
}

#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanVtepTable
 Input       :  pVxlanSetFsVxlanVtepEntry
                pVxlanIsSetFsVxlanVtepEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsVxlanVtepTable (tVxlanFsVxlanVtepEntry * pVxlanSetFsVxlanVtepEntry,
                             tVxlanIsSetFsVxlanVtepEntry *
                             pVxlanIsSetFsVxlanVtepEntry, INT4 i4RowStatusLogic,
                             INT4 i4RowCreateOption)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanFsVxlanVtepEntry *pVxlanOldFsVxlanVtepEntry = NULL;
    tVxlanFsVxlanVtepEntry *pVxlanTrgFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanTrgIsSetFsVxlanVtepEntry = NULL;
    INT4                i4RowMakeActive = FALSE;
    tVxlanHwInfo        VxlanHwInfo;
    UINT4               u4IpTmp = 0;

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    pVxlanOldFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanOldFsVxlanVtepEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanTrgFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVtepEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanTrgFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanTrgIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Check whether the node is already present */
    pVxlanFsVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) pVxlanSetFsVxlanVtepEntry);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
             CREATE_AND_WAIT)
            || (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
                CREATE_AND_GO)
            ||
            ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
              ACTIVE) && (i4RowCreateOption == OSIX_TRUE)))
        {
            /* Allocate memory for the new node */
            pVxlanFsVxlanVtepEntry =
                (tVxlanFsVxlanVtepEntry *)
                MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
            if (pVxlanFsVxlanVtepEntry == NULL)
            {
                if (VxlanSetAllFsVxlanVtepTableTrigger
                    (pVxlanSetFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVtepTable:VxlanSetAllFsVxlanVtepTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVtepTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
            if ((VxlanInitializeFsVxlanVtepTable (pVxlanFsVxlanVtepEntry)) ==
                OSIX_FAILURE)
            {
                if (VxlanSetAllFsVxlanVtepTableTrigger
                    (pVxlanSetFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVtepTable:VxlanSetAllFsVxlanVtepTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVtepTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
                CLI_SET_ERR (CLI_VXLAN_VTEP_OBJ_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
                    pVxlanSetFsVxlanVtepEntry->MibObject.
                    i4FsVxlanVtepNveIfIndex;
            }

            if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
                    pVxlanSetFsVxlanVtepEntry->MibObject.
                    i4FsVxlanVtepAddressType;
            }

            if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress != OSIX_FALSE)
            {
                MEMCPY (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                        pVxlanSetFsVxlanVtepEntry->MibObject.
                        au1FsVxlanVtepAddress, VXLAN_IP6_ADDR_LEN);

                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
                    pVxlanSetFsVxlanVtepEntry->MibObject.
                    i4FsVxlanVtepAddressLen;
            }

            if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus;
            }

            if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == OSIX_TRUE)
                                    && (pVxlanSetFsVxlanVtepEntry->MibObject.
                                        i4FsVxlanVtepRowStatus == ACTIVE)))
            {
                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    ACTIVE;
            }
            else if (pVxlanSetFsVxlanVtepEntry->MibObject.
                     i4FsVxlanVtepRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                 (tRBElem *) pVxlanFsVxlanVtepEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsVxlanVtepTableTrigger
                    (pVxlanSetFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVtepTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == OSIX_TRUE)
                                    && (pVxlanSetFsVxlanVtepEntry->MibObject.
                                        i4FsVxlanVtepRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
                    pVxlanSetFsVxlanVtepEntry->MibObject.
                    i4FsVxlanVtepNveIfIndex;
                pVxlanTrgFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsVxlanVtepTableTrigger
                    (pVxlanTrgFsVxlanVtepEntry, pVxlanTrgIsSetFsVxlanVtepEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanVtepEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsVxlanVtepEntry->MibObject.
                     i4FsVxlanVtepRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsVxlanVtepTableTrigger
                    (pVxlanTrgFsVxlanVtepEntry, pVxlanTrgIsSetFsVxlanVtepEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanVtepEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
                CREATE_AND_GO)
            {
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
                    ACTIVE;
            }

            if (VxlanSetAllFsVxlanVtepTableTrigger (pVxlanSetFsVxlanVtepEntry,
                                                    pVxlanIsSetFsVxlanVtepEntry,
                                                    SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVtepTable:  VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsVxlanVtepTableTrigger (pVxlanSetFsVxlanVtepEntry,
                                                    pVxlanIsSetFsVxlanVtepEntry,
                                                    SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVtepTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
            CLI_SET_ERR (CLI_VXLAN_VTEP_ENTRY_NOT_EXIST);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
              CREATE_AND_WAIT)
             || (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
                 CREATE_AND_GO))
    {
        if (VxlanSetAllFsVxlanVtepTableTrigger (pVxlanSetFsVxlanVtepEntry,
                                                pVxlanIsSetFsVxlanVtepEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanVtepTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
        CLI_SET_ERR (CLI_VXLAN_VTEP_ROW_EXISTS);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsVxlanVtepEntry, pVxlanFsVxlanVtepEntry,
            sizeof (tVxlanFsVxlanVtepEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus == DESTROY)
    {

        if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress != OSIX_FALSE)
        {
            if (MEMCMP (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                        pVxlanSetFsVxlanVtepEntry->MibObject.
                        au1FsVxlanVtepAddress,
                        pVxlanSetFsVxlanVtepEntry->MibObject.
                        i4FsVxlanVtepAddressLen) != 0)

            {
                if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen
                    == VXLAN_IP4_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_SRC_IPV4_ADDR);
                }
                else
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_SRC_IPV6_ADDR);
                }
                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VTEP entry\n"));
                return OSIX_FAILURE;
            }
        }
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus = DESTROY;

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   pVxlanFsVxlanVtepEntry);
        if (VxlanSetAllFsVxlanVtepTableTrigger
            (pVxlanSetFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsVxlanVtepTableFilterInputs
        (pVxlanFsVxlanVtepEntry, pVxlanSetFsVxlanVtepEntry,
         pVxlanIsSetFsVxlanVtepEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus == ACTIVE) &&
        (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus !=
         NOT_IN_SERVICE))
    {
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_TRUE;

        if (VxlanSetAllFsVxlanVtepTableTrigger (pVxlanTrgFsVxlanVtepEntry,
                                                pVxlanTrgIsSetFsVxlanVtepEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType != OSIX_FALSE)
    {
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanSetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                VXLAN_IP6_ADDR_LEN);

        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        MEMCPY (&u4IpTmp, pVxlanSetFsVxlanVtepEntry->MibObject.
                au1FsVxlanVtepAddress, VXLAN_IP4_ADDR_LEN);
        MEMCPY (VxlanHwInfo.unHwParam.SwitchVxlanDipAddr.au1Addr,
                &u4IpTmp, VXLAN_IP4_ADDR_LEN);
        VxlanHwInfo.u4InfoType = VXLAN_HW_LOCAL_IP;
#ifdef NPAPI_WANTED
        FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE);
#endif
#ifdef VRF_WANTED
        IssGetVrfUnqMacAddress (0, &VxlanHwInfo.unHwParam.SwitchVxlanMac);
#else
        CfaGetSysMacAddress (VxlanHwInfo.unHwParam.SwitchVxlanMac);
#endif
        VxlanHwInfo.u4InfoType = VXLAN_HW_MAC;
#ifdef NPAPI_WANTED
        FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE);
#endif

    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus != OSIX_FALSE)
    {
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus = ACTIVE;
    }

    if (VxlanSetAllFsVxlanVtepTableTrigger (pVxlanSetFsVxlanVtepEntry,
                                            pVxlanIsSetFsVxlanVtepEntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanVtepTable: VxlanSetAllFsVxlanVtepTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsVxlanVtepEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanNveTable
 Input       :  pVxlanSetFsVxlanNveEntry
                pVxlanIsSetFsVxlanNveEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsVxlanNveTable (tVxlanFsVxlanNveEntry * pVxlanSetFsVxlanNveEntry,
                            tVxlanIsSetFsVxlanNveEntry *
                            pVxlanIsSetFsVxlanNveEntry, INT4 i4RowStatusLogic,
                            INT4 i4RowCreateOption)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanOldFsVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanTrgFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanTrgIsSetFsVxlanNveEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanOtherNveEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanTmpNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextTmpNveEntry = NULL;
#ifdef L2RED_WANTED
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
#endif
    INT4                i4RowMakeActive = FALSE;
    UINT4               u4NveIndex = 0;
    BOOL1               bNveStaticEntryExists = VXLAN_FALSE;
#ifdef EVPN_VXLAN_WANTED
    INT4                i4EvpnIndex = 0;
    UINT2               u2VlanId = 0;
    UINT4               u4VrfId = 0;
    UINT1               au1VrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;
#endif
    tMacAddr            zeroAddr;

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    pVxlanOldFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanOldFsVxlanNveEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanTrgFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanNveEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanNveEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanTrgFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanTrgIsSetFsVxlanNveEntry, 0,
            sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Check whether the node is already present */
    pVxlanFsVxlanNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   (tRBElem *) pVxlanSetFsVxlanNveEntry);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
             CREATE_AND_WAIT)
            || (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
                CREATE_AND_GO)
            ||
            ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
              ACTIVE) && (i4RowCreateOption == OSIX_TRUE)))
        {
            /* Allocate memory for the new node */
            pVxlanFsVxlanNveEntry =
                (tVxlanFsVxlanNveEntry *)
                MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
            if (pVxlanFsVxlanNveEntry == NULL)
            {
                if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                                       pVxlanIsSetFsVxlanNveEntry,
                                                       SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable:VxlanSetAllFsVxlanNveTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanNveTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

            if ((VxlanInitializeFsVxlanNveTable (pVxlanFsVxlanNveEntry,
                                                 pVxlanSetFsVxlanNveEntry->
                                                 MibObject.
                                                 i4FsVxlanNveIfIndex)) ==
                OSIX_FAILURE)
            {
                if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                                       pVxlanIsSetFsVxlanNveEntry,
                                                       SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable:VxlanSetAllFsVxlanNveTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanNveTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
                CLI_SET_ERR (CLI_VXLAN_NVE_OBJ_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex != OSIX_FALSE)
            {
                pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex =
                    pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber != OSIX_FALSE)
            {
                pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
                    pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac != OSIX_FALSE)
            {
                MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                        &(pVxlanSetFsVxlanNveEntry->MibObject.
                          FsVxlanNveDestVmMac), 6);
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressType =
                    pVxlanSetFsVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressType;
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsVxlanNveEntry->MibObject.
                        au1FsVxlanNveRemoteVtepAddress,
                        pVxlanSetFsVxlanNveEntry->MibObject.
                        au1FsVxlanNveRemoteVtepAddress, VXLAN_IP6_ADDR_LEN);

                pVxlanFsVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressLen =
                    pVxlanSetFsVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressLen;
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp != OSIX_FALSE)
            {
                pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
                    pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;
            }

            if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus != OSIX_FALSE)
            {
                pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
                    pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus;
            }

            if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == OSIX_TRUE)
                                    && (pVxlanSetFsVxlanNveEntry->MibObject.
                                        i4FsVxlanNveRowStatus == ACTIVE)))
            {
                pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsVxlanNveEntry->MibObject.
                     i4FsVxlanNveRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
                    NOT_READY;
            }
#ifdef EVPN_VXLAN_WANTED
            if (EvpnUtilGetEviFromVni ((pVxlanFsVxlanNveEntry->MibObject.
                                        u4FsVxlanNveVniNumber), &i4EvpnIndex)
                != VXLAN_FAILURE)
            {
                if (VxlanUtilGetVlanFromVni (pVxlanFsVxlanNveEntry->MibObject.
                                             u4FsVxlanNveVniNumber,
                                             &u2VlanId) == VXLAN_SUCCESS)
                {
                    if (VxlanUtilGetVrfFromVlan (u2VlanId, (UINT1 *) au1VrfName)
                        == VXLAN_SUCCESS)
                    {
                        if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
                        {
                            VXLAN_TRC ((VXLAN_EVPN_TRC,
                                        " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
                        }
                    }

                }
                /* Send Notification to BGP to get MAC routes, when member VNI
                 * is added to an NVE interface */
                if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                {
                    EVPN_NOTIFY_GET_MAC_CB (u4VrfId,
                                            pVxlanFsVxlanNveEntry->MibObject.
                                            u4FsVxlanNveVniNumber, zeroAddr,
                                            EVPN_BGP4_MAC_ADD);
                }
            }
#endif
            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanFsVxlanNveEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                                       pVxlanIsSetFsVxlanNveEntry,
                                                       SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanNveTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == OSIX_TRUE)
                                    && (pVxlanSetFsVxlanNveEntry->MibObject.
                                        i4FsVxlanNveRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex =
                    pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
                pVxlanTrgFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
                    pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
                MEMCPY (&
                        (pVxlanTrgFsVxlanNveEntry->MibObject.
                         FsVxlanNveDestVmMac),
                        &(pVxlanSetFsVxlanNveEntry->MibObject.
                          FsVxlanNveDestVmMac), 6);
                pVxlanTrgFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanTrgFsVxlanNveEntry,
                                                       pVxlanTrgIsSetFsVxlanNveEntry,
                                                       SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanNveEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsVxlanNveEntry->MibObject.
                     i4FsVxlanNveRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanTrgFsVxlanNveEntry,
                                                       pVxlanTrgIsSetFsVxlanNveEntry,
                                                       SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanNveEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
                CREATE_AND_GO)
            {
                pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
                    ACTIVE;
            }

            if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                                   pVxlanIsSetFsVxlanNveEntry,
                                                   SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanNveTable:  VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                                   pVxlanIsSetFsVxlanNveEntry,
                                                   SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanNveTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
            CLI_SET_ERR (CLI_VXLAN_NVE_ENTRY_NOT_EXIST);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
              CREATE_AND_WAIT)
             || (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
                 CREATE_AND_GO))
    {
        if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                               pVxlanIsSetFsVxlanNveEntry,
                                               SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanNveTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
        CLI_SET_ERR (CLI_VXLAN_NVE_ROW_EXISTS);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsVxlanNveEntry, pVxlanFsVxlanNveEntry,
            sizeof (tVxlanFsVxlanNveEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus == DESTROY)
    {
        if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress !=
            OSIX_FALSE)
        {
            if (MEMCMP (pVxlanFsVxlanNveEntry->MibObject.
                        au1FsVxlanNveRemoteVtepAddress,
                        pVxlanSetFsVxlanNveEntry->MibObject.
                        au1FsVxlanNveRemoteVtepAddress,
                        pVxlanSetFsVxlanNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressLen) != 0)

            {
                if (pVxlanSetFsVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressLen == VXLAN_IP4_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV4_ADDR);
                }
                else
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV6_ADDR);
                }

                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid NVE entry\n"));
                return OSIX_FAILURE;
            }
        }

        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus = DESTROY;

        /* Remove the Member port (NVE) from VLAN */
        MEMSET (&VxlanVniVlanMapEntry, 0,
                sizeof (tVxlanFsVxlanVniVlanMapEntry));

        VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
            pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) & VxlanVniVlanMapEntry, NULL);

        while (pVxlanVniVlanMapEntry != NULL)
        {
            if (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber
                !=
                pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber)
            {
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);

                continue;
            }

            /* Nve Entry */
            pVxlanOtherNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanNveEntry->
                                                  MibObject.
                                                  u4FsVxlanNveVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanOtherNveEntry != NULL)
                   && (pVxlanOtherNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                       pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex)
                   && (pVxlanOtherNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber))
            {
                if ((pVxlanOtherNveEntry->MibObject.i4FsVxlanNveStorageType ==
                     VXLAN_STRG_TYPE_NON_VOL) &&
                    (pVxlanOtherNveEntry != pVxlanFsVxlanNveEntry))
                {
                    bNveStaticEntryExists = VXLAN_TRUE;
                    break;
                }
                pVxlanOtherNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanOtherNveEntry, NULL);

            }

            pVxlanFsVxlanMCastEntry =
                VxlanUtilGetNveIndexFromMcastTable (pVxlanFsVxlanNveEntry->
                                                    MibObject.
                                                    u4FsVxlanNveVniNumber,
                                                    &u4NveIndex);
            if (pVxlanFsVxlanMCastEntry == NULL)
            {
                pVxlanFsVxlanInReplicaEntry =
                    VxlanUtilGetNveIndexFromInReplicaTable
                    (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                     &u4NveIndex);
            }
            if ((bNveStaticEntryExists == VXLAN_FALSE) &&
                ((pVxlanFsVxlanMCastEntry == NULL) &&
                 (pVxlanFsVxlanInReplicaEntry == NULL)))
            {

#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort
                    ((UINT4) pVxlanVniVlanMapEntry->MibObject.
                     i4FsVxlanVniVlanMapVlanId,
                     (UINT4) pVxlanFsVxlanNveEntry->MibObject.
                     i4FsVxlanNveIfIndex, VLAN_DELETE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: Fail to"
                                "unamp NVE and VLAN.\r\n"));

                }
#endif
            }
            pVxlanFsVxlanNveEntry->i4VlanId = pVxlanVniVlanMapEntry->MibObject.
                i4FsVxlanVniVlanMapVlanId;
            if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanNveEntry,
                                          VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }

            pVxlanFsVxlanTmpNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanNveEntry->
                                                  MibObject.
                                                  u4FsVxlanNveVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanFsVxlanTmpNveEntry != NULL)
                   && (pVxlanFsVxlanTmpNveEntry->MibObject.
                       i4FsVxlanNveIfIndex ==
                       pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex)
                   && (pVxlanFsVxlanTmpNveEntry->MibObject.
                       u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber))
            {
                pVxlanFsVxlanNextTmpNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);
                if (pVxlanFsVxlanTmpNveEntry->MibObject.
                    i4FsVxlanNveStorageType == VXLAN_STRG_TYPE_VOL)
                {
                    if (VxlanHwUpdateNveDatabase
                        (pVxlanFsVxlanTmpNveEntry,
                         VXLAN_HW_DEL) == VXLAN_FAILURE)
                    {
                        VXLAN_TRC ((VXLAN_FAIL_TRC,
                                    "Failed to delete Nve entry in h/w\n"));
                    }
                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanFsVxlanTmpNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanTmpNveEntry);
                }
                pVxlanFsVxlanTmpNveEntry = pVxlanFsVxlanNextTmpNveEntry;
            }

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanVniVlanMapEntry, NULL);
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   pVxlanFsVxlanNveEntry);
        if (VxlanSetAllFsVxlanNveTableTrigger
            (pVxlanSetFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
#ifdef EVPN_VXLAN_WANTED
        if (EvpnUtilGetEviFromVni ((pVxlanSetFsVxlanNveEntry->MibObject.
                                    u4FsVxlanNveVniNumber),
                                   &i4EvpnIndex) != VXLAN_FAILURE)
        {
            if (VxlanUtilGetVlanFromVni (pVxlanSetFsVxlanNveEntry->MibObject.
                                         u4FsVxlanNveVniNumber,
                                         &u2VlanId) == VXLAN_SUCCESS)
            {
                if (VxlanUtilGetVrfFromVlan (u2VlanId, (UINT1 *) au1VrfName) ==
                    VXLAN_SUCCESS)
                {
                    if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
                    {
                        VXLAN_TRC ((VXLAN_EVPN_TRC,
                                    " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
                    }
                }

            }
            /* Send Notification to BGP to get MAC routes, when member VNI
             * is added to an NVE interface */
            if (EVPN_NOTIFY_GET_MAC_CB != NULL)
            {
                EVPN_NOTIFY_GET_MAC_CB (u4VrfId,
                                        pVxlanSetFsVxlanNveEntry->MibObject.
                                        u4FsVxlanNveVniNumber, zeroAddr,
                                        EVPN_BGP4_MAC_DEL);
            }
        }
#endif
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsVxlanNveTableFilterInputs
        (pVxlanFsVxlanNveEntry, pVxlanSetFsVxlanNveEntry,
         pVxlanIsSetFsVxlanNveEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus == ACTIVE) &&
        (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus !=
         NOT_IN_SERVICE))
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;

        if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanTrgFsVxlanNveEntry,
                                               pVxlanTrgIsSetFsVxlanNveEntry,
                                               SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType =
            pVxlanSetFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressType;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                pVxlanSetFsVxlanNveEntry->MibObject.
                au1FsVxlanNveRemoteVtepAddress, VXLAN_IP6_ADDR_LEN);

        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen =
            pVxlanSetFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressLen;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveStorageType != OSIX_FALSE)
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType;
    }
#ifdef EVPN_VXLAN_WANTED
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp != OSIX_FALSE)
    {
        if ((pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
             EVPN_CLI_ARP_SUPPRESS)
            && (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
                EVPN_CLI_ARP_ALLOW))
        {
            if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanNveEntry,
                                          VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
            pVxlanEvpnArpSupLocalMacEntry = RBTreeGetFirst (gVxlanGlobals.
                                                            VxlanEvpnArpSupLocalMacTable);
            while (pVxlanEvpnArpSupLocalMacEntry != NULL)
            {
                if (pVxlanEvpnArpSupLocalMacEntry->u4Vni ==
                    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber)
                {
                    RBTreeRem (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                               pVxlanEvpnArpSupLocalMacEntry);
                    MemReleaseMemBlock (VXLAN_EVPNARPSUPMACTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanEvpnArpSupLocalMacEntry);

                }
                pVxlanEvpnArpSupLocalMacEntry = RBTreeGetNext
                    (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                     (tRBElem *) pVxlanEvpnArpSupLocalMacEntry, NULL);
            }

        }
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;

        pVxlanFsVxlanTmpNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);

        while (pVxlanFsVxlanTmpNveEntry != NULL)
        {
            if ((pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                 pVxlanFsVxlanTmpNveEntry->MibObject.u4FsVxlanNveVniNumber) &&
                (MEMCMP
                 (pVxlanFsVxlanTmpNveEntry->MibObject.FsVxlanNveDestVmMac,
                  zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0))
            {
                pVxlanFsVxlanTmpNveEntry->MibObject.i4FsVxlanSuppressArp =
                    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;
            }
            pVxlanFsVxlanNextTmpNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);

            pVxlanFsVxlanTmpNveEntry = pVxlanFsVxlanNextTmpNveEntry;
        }
    }
#endif
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus != OSIX_FALSE)
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus;

        /* Get the VLAN id from VLAN- VNI mapping table and associate
         * the NVE interface with that VLANS
         * */
        if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus == ACTIVE)
        {
            MEMSET (&VxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanFsVxlanVniVlanMapEntry));

            VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
                pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) & VxlanVniVlanMapEntry, NULL);

            while (pVxlanVniVlanMapEntry != NULL)
            {
                if (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber
                    !=
                    pVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber)
                {
                    pVxlanVniVlanMapEntry =
                        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                       FsVxlanVniVlanMapTable,
                                       (tRBElem *) pVxlanVniVlanMapEntry, NULL);
                    continue;
                }
#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort ((UINT4) pVxlanVniVlanMapEntry->
                                          MibObject.i4FsVxlanVniVlanMapVlanId,
                                          (UINT4) pVxlanFsVxlanNveEntry->
                                          MibObject.i4FsVxlanNveIfIndex,
                                          VLAN_UPDATE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: Fail to"
                                "map NVE and VLAN.\r\n"));
                }

#endif
                pVxlanFsVxlanNveEntry->i4VlanId =
                    pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
                if (VxlanHwUpdateNveDatabase
                    (pVxlanFsVxlanNveEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VXLAN_HW_ADD_FAIL);
                    return OSIX_FAILURE;
                }
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);
            }

        }
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus = ACTIVE;
    }

    if (VxlanSetAllFsVxlanNveTableTrigger (pVxlanSetFsVxlanNveEntry,
                                           pVxlanIsSetFsVxlanNveEntry,
                                           SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanNveTable: VxlanSetAllFsVxlanNveTableTrigger function returns failure.\r\n"));
    }
#ifdef L2RED_WANTED
    pVxlanNvePortEntry = VxlanGetNvePortEntry (pVxlanFsVxlanNveEntry->MibObject.
                                               au1FsVxlanNveRemoteVtepAddress,
                                               pVxlanFsVxlanNveEntry->MibObject.
                                               i4FsVxlanNveRemoteVtepAddressLen);
    if ((pVxlanNvePortEntry != NULL) && (pVxlanNvePortEntry->u4RefCount == 1))
    {
        VxlanRedSyncDynNvePortInfo (pVxlanNvePortEntry);
    }
#endif

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanOldFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsVxlanNveEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanMCastTable
 Input       :  pVxlanSetFsVxlanMCastEntry
                pVxlanIsSetFsVxlanMCastEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsVxlanMCastTable (tVxlanFsVxlanMCastEntry *
                              pVxlanSetFsVxlanMCastEntry,
                              tVxlanIsSetFsVxlanMCastEntry *
                              pVxlanIsSetFsVxlanMCastEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanOldFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanTrgFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastGrpEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastGrpEntry;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanTrgIsSetFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextNveEntry = NULL;

    INT4                i4RowMakeActive = FALSE;
    BOOL1               bMcastEntryExists = VXLAN_FALSE;
    UINT4               u4NveIndex = 0;
    BOOL1               bNveStaticEntryExists = VXLAN_FALSE;

    MEMSET (&VxlanMCastGrpEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    pVxlanOldFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanOldFsVxlanMCastEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanTrgFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanMCastEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanTrgFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanTrgIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Check whether the node is already present */
    pVxlanFsVxlanMCastEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   (tRBElem *) pVxlanSetFsVxlanMCastEntry);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
             CREATE_AND_WAIT)
            || (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
                CREATE_AND_GO)
            ||
            ((pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
              ACTIVE) && (i4RowCreateOption == OSIX_TRUE)))
        {
            /* Allocate memory for the new node */
            pVxlanFsVxlanMCastEntry =
                (tVxlanFsVxlanMCastEntry *)
                MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
            if (pVxlanFsVxlanMCastEntry == NULL)
            {
                if (VxlanSetAllFsVxlanMCastTableTrigger
                    (pVxlanSetFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanMCastTable:VxlanSetAllFsVxlanMCastTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanMCastTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsVxlanMCastEntry, 0,
                    sizeof (tVxlanFsVxlanMCastEntry));
            if ((VxlanInitializeFsVxlanMCastTable (pVxlanFsVxlanMCastEntry,
                                                   pVxlanSetFsVxlanMCastEntry->
                                                   MibObject.
                                                   i4FsVxlanMCastNveIfIndex)) ==
                OSIX_FAILURE)
            {
                if (VxlanSetAllFsVxlanMCastTableTrigger
                    (pVxlanSetFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanMCastTable:VxlanSetAllFsVxlanMCastTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanMCastTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
                CLI_SET_ERR (CLI_VXLAN_MCAST_OBJ_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastNveIfIndex;
            }

            if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    u4FsVxlanMCastVniNumber;
            }

            if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastGroupAddressType =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastGroupAddressType;
            }

            if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.
                        au1FsVxlanMCastGroupAddress,
                        pVxlanSetFsVxlanMCastEntry->MibObject.
                        au1FsVxlanMCastGroupAddress, VXLAN_IP6_ADDR_LEN);

                pVxlanFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastGroupAddressLen =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastGroupAddressLen;
            }

            if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastRowStatus;
            }

            if ((pVxlanSetFsVxlanMCastEntry->MibObject.
                 i4FsVxlanMCastRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastRowStatus == ACTIVE)))
            {
                pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    ACTIVE;
            }
            else if (pVxlanSetFsVxlanMCastEntry->MibObject.
                     i4FsVxlanMCastRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                 (tRBElem *) pVxlanFsVxlanMCastEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsVxlanMCastTableTrigger
                    (pVxlanSetFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanMCastTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                    (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsVxlanMCastEntry->MibObject.
                 i4FsVxlanMCastRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastNveIfIndex;
                pVxlanTrgFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    u4FsVxlanMCastVniNumber;
                pVxlanTrgFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsVxlanMCastTableTrigger
                    (pVxlanTrgFsVxlanMCastEntry,
                     pVxlanTrgIsSetFsVxlanMCastEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanMCastEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsVxlanMCastEntry->MibObject.
                     i4FsVxlanMCastRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsVxlanMCastTableTrigger
                    (pVxlanTrgFsVxlanMCastEntry,
                     pVxlanTrgIsSetFsVxlanMCastEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsVxlanMCastEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
                CREATE_AND_GO)
            {
                pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
                    ACTIVE;
            }

            if (VxlanSetAllFsVxlanMCastTableTrigger (pVxlanSetFsVxlanMCastEntry,
                                                     pVxlanIsSetFsVxlanMCastEntry,
                                                     SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanMCastTable:  VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsVxlanMCastTableTrigger (pVxlanSetFsVxlanMCastEntry,
                                                     pVxlanIsSetFsVxlanMCastEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanMCastTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
            CLI_SET_ERR (CLI_VXLAN_MCAST_ENTRY_NOT_EXIST);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
              CREATE_AND_WAIT)
             || (pVxlanSetFsVxlanMCastEntry->MibObject.
                 i4FsVxlanMCastRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsVxlanMCastTableTrigger (pVxlanSetFsVxlanMCastEntry,
                                                 pVxlanIsSetFsVxlanMCastEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanMCastTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
        CLI_SET_ERR (CLI_VXLAN_MCAST_ROW_EXISTS);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsVxlanMCastEntry, pVxlanFsVxlanMCastEntry,
            sizeof (tVxlanFsVxlanMCastEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
        DESTROY)
    {
        if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress !=
            OSIX_FALSE)
        {
            if (MEMCMP (pVxlanFsVxlanMCastEntry->MibObject.
                        au1FsVxlanMCastGroupAddress,
                        pVxlanSetFsVxlanMCastEntry->MibObject.
                        au1FsVxlanMCastGroupAddress,
                        pVxlanSetFsVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastGroupAddressLen) != 0)

            {
                if (pVxlanFsVxlanMCastEntry->MibObject.
                    i4FsVxlanMCastGroupAddressLen == VXLAN_IP4_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_IPV4_ADDR);
                }
                else
                {
                    CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_IPV6_ADDR);
                }

                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid MCAST entry\n"));
                return OSIX_FAILURE;
            }
        }

        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus = DESTROY;

        /* Remove the Member port (NVE) from VLAN */
        MEMSET (&VxlanVniVlanMapEntry, 0,
                sizeof (tVxlanFsVxlanVniVlanMapEntry));

        VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
            pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;
        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) & VxlanVniVlanMapEntry, NULL);

        while (pVxlanVniVlanMapEntry != NULL)
        {
            if (pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber
                !=
                pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber)
            {
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);
                continue;
            }
            pVxlanFsVxlanNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanMCastEntry->
                                                  MibObject.
                                                  u4FsVxlanMCastVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanFsVxlanNveEntry != NULL)
                   && (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                       pVxlanFsVxlanMCastEntry->MibObject.
                       i4FsVxlanMCastNveIfIndex)
                   && (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanMCastEntry->MibObject.
                       u4FsVxlanMCastVniNumber))
            {
                if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                    VXLAN_STRG_TYPE_NON_VOL)
                {
                    bNveStaticEntryExists = VXLAN_TRUE;
                    break;
                }
                pVxlanFsVxlanNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanFsVxlanNveEntry, NULL);

            }
            if (bNveStaticEntryExists == VXLAN_FALSE)
            {
#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort
                    ((UINT4) pVxlanVniVlanMapEntry->MibObject.
                     i4FsVxlanVniVlanMapVlanId,
                     (UINT4) pVxlanFsVxlanMCastEntry->MibObject.
                     i4FsVxlanMCastNveIfIndex, VLAN_DELETE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: Fail to"
                                "unamp NVE and VLAN.\r\n"));

                }
#endif
            }
            pVxlanFsVxlanMCastEntry->i4VlanId =
                pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
            if (VxlanHwUpdateMcastDatabase
                (pVxlanFsVxlanMCastEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Multicast entry in h/w\n"));
            }
            pVxlanFsVxlanNveEntry = NULL;

            pVxlanFsVxlanNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanMCastEntry->
                                                  MibObject.
                                                  u4FsVxlanMCastVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanFsVxlanNveEntry != NULL)
                   &&
                   ((pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                     pVxlanFsVxlanMCastEntry->MibObject.
                     i4FsVxlanMCastNveIfIndex)
                    || ((INT4) pVxlanFsVxlanNveEntry->u4OrgNveIfIndex ==
                        pVxlanFsVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastNveIfIndex))
                   && (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanMCastEntry->MibObject.
                       u4FsVxlanMCastVniNumber))
            {
                pVxlanFsVxlanNextNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanFsVxlanNveEntry, NULL);
                if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                    VXLAN_STRG_TYPE_VOL)
                {
                    if (VxlanHwUpdateNveDatabase
                        (pVxlanFsVxlanNveEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
                    {
                        VXLAN_TRC ((VXLAN_FAIL_TRC,
                                    "Failed to delete Nve entry in h/w\n"));
                    }
                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanNveEntry);
                }
                pVxlanFsVxlanNveEntry = pVxlanFsVxlanNextNveEntry;
            }
            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanVniVlanMapEntry, NULL);
        }

        MEMCPY (VxlanMCastGrpEntry.MibObject.au1FsVxlanMCastGroupAddress,
                pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                pVxlanFsVxlanMCastEntry->MibObject.
                i4FsVxlanMCastGroupAddressLen);
        pVxlanMCastGrpEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                           (tRBElem *) & VxlanMCastGrpEntry, NULL);
        while (pVxlanMCastGrpEntry != NULL)
        {
            if ((pVxlanMCastGrpEntry->MibObject.u4FsVxlanMCastVniNumber !=
                 pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber) &&
                (MEMCMP
                 (pVxlanMCastGrpEntry->MibObject.au1FsVxlanMCastGroupAddress,
                  pVxlanFsVxlanMCastEntry->MibObject.
                  au1FsVxlanMCastGroupAddress,
                  pVxlanFsVxlanMCastEntry->MibObject.
                  i4FsVxlanMCastGroupAddressLen) == 0))

            {
                bMcastEntryExists = VXLAN_TRUE;
                break;
            }
            pVxlanMCastGrpEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                               (tRBElem *) pVxlanMCastGrpEntry, NULL);
        }

        if (bMcastEntryExists == VXLAN_FALSE)
        {
            VxlanUdpLeaveMulticast (pVxlanFsVxlanMCastEntry);
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   pVxlanFsVxlanMCastEntry);
        if (VxlanSetAllFsVxlanMCastTableTrigger
            (pVxlanSetFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsVxlanMCastTableFilterInputs
        (pVxlanFsVxlanMCastEntry, pVxlanSetFsVxlanMCastEntry,
         pVxlanIsSetFsVxlanMCastEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus == ACTIVE)
        && (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus !=
            NOT_IN_SERVICE))
    {
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_TRUE;

        if (VxlanSetAllFsVxlanMCastTableTrigger (pVxlanTrgFsVxlanMCastEntry,
                                                 pVxlanTrgIsSetFsVxlanMCastEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType =
            pVxlanSetFsVxlanMCastEntry->MibObject.
            i4FsVxlanMCastGroupAddressType;
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                pVxlanSetFsVxlanMCastEntry->MibObject.
                au1FsVxlanMCastGroupAddress, VXLAN_IP6_ADDR_LEN);

        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen =
            pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen;
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus != OSIX_FALSE)
    {
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
            pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus;
        /* Get the VLAN id from VLAN- VNI mapping table and associate
         * the NVE interface with that VLANS
         * */
        if (pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
            ACTIVE)
        {
            MEMSET (&VxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanFsVxlanVniVlanMapEntry));

            VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
                pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;
            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) & VxlanVniVlanMapEntry, NULL);

            while (pVxlanVniVlanMapEntry != NULL)
            {
                if (pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber
                    !=
                    pVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber)
                {
                    pVxlanVniVlanMapEntry =
                        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                       FsVxlanVniVlanMapTable,
                                       (tRBElem *) pVxlanVniVlanMapEntry, NULL);
                    continue;
                }
#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort ((UINT4) pVxlanVniVlanMapEntry->
                                          MibObject.i4FsVxlanVniVlanMapVlanId,
                                          (UINT4) pVxlanFsVxlanMCastEntry->
                                          MibObject.i4FsVxlanMCastNveIfIndex,
                                          VLAN_UPDATE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanNveTable: Fail to"
                                "map NVE and VLAN.\r\n"));
                }

#endif
                pVxlanFsVxlanMCastEntry->i4VlanId =
                    pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
                if (VxlanHwUpdateMcastDatabase
                    (pVxlanFsVxlanMCastEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VXLAN_HW_ADD_FAIL);
                    return OSIX_FAILURE;
                }
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);
            }
        }

        VxlanUdpJoinMulticast (pVxlanFsVxlanMCastEntry);
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus = ACTIVE;
    }

    if (VxlanSetAllFsVxlanMCastTableTrigger (pVxlanSetFsVxlanMCastEntry,
                                             pVxlanIsSetFsVxlanMCastEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanMCastTable: VxlanSetAllFsVxlanMCastTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsVxlanMCastEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanVniVlanMapTable
 Input       :  pVxlanSetFsVxlanVniVlanMapEntry
                pVxlanIsSetFsVxlanVniVlanMapEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                   pVxlanSetFsVxlanVniVlanMapEntry,
                                   tVxlanIsSetFsVxlanVniVlanMapEntry *
                                   pVxlanIsSetFsVxlanVniVlanMapEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanOldFsVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanTrgFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanTrgIsSetFsVxlanVniVlanMapEntry =
        NULL;
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    INT4                i4RowMakeActive = FALSE;
    UINT4               u4NveIfIndex = 0;
    UINT4               u4NveRepIfIndex = 0;
    UINT2               u2VlanId = 0;
#ifdef EVPN_VXLAN_WANTED
    tMacAddr            zeroAddr;
#endif
#ifdef L2RED_WANTED
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;
#endif

#ifdef EVPN_VXLAN_WANTED
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#endif

    pVxlanOldFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanOldFsVxlanVniVlanMapEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanTrgFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanTrgFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanTrgIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Check whether the node is already present */
    pVxlanFsVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) pVxlanSetFsVxlanVniVlanMapEntry);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             i4FsVxlanVniVlanMapRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                i4FsVxlanVniVlanMapRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
              i4FsVxlanVniVlanMapRowStatus == ACTIVE)
             && (i4RowCreateOption == OSIX_TRUE)))
        {
            /* Allocate memory for the new node */
            pVxlanFsVxlanVniVlanMapEntry =
                (tVxlanFsVxlanVniVlanMapEntry *)
                MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
            if (pVxlanFsVxlanVniVlanMapEntry == NULL)
            {
                if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                    (pVxlanSetFsVxlanVniVlanMapEntry,
                     pVxlanIsSetFsVxlanVniVlanMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVniVlanMapTable:"
                                "VxlanSetAllFsVxlanVniVlanMapTableTrigger"
                                " function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVniVlanMapTable: Fail to "
                            "Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanFsVxlanVniVlanMapEntry));
            if ((VxlanInitializeFsVxlanVniVlanMapTable
                 (pVxlanFsVxlanVniVlanMapEntry,
                  pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                  i4FsVxlanVniVlanMapVlanId)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                    (pVxlanSetFsVxlanVniVlanMapEntry,
                     pVxlanIsSetFsVxlanVniVlanMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVniVlanMapTable:VxlanSetAllFsVxlanVniVlanMapTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVniVlanMapTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
                CLI_SET_ERR (CLI_VXLAN_VNIVLAN_OBJ_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapVlanId =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapVlanId;
            }

            if (pVxlanIsSetFsVxlanVniVlanMapEntry->
                bFsVxlanVniVlanMapVniNumber != OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber;
            }

            if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktSent =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktSent;
            }

            if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktRcvd =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktRcvd;
            }

            if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktDrpd =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapPktDrpd;
            }
            if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u1IsVlanTagged =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged;
            }

            if (pVxlanIsSetFsVxlanVniVlanMapEntry->
                bFsVxlanVniVlanMapRowStatus != OSIX_FALSE)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus;
            }

            if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                 i4FsVxlanVniVlanMapRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                        i4FsVxlanVniVlanMapRowStatus == ACTIVE)))
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                     i4FsVxlanVniVlanMapRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                 (tRBElem *) pVxlanFsVxlanVniVlanMapEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                    (pVxlanSetFsVxlanVniVlanMapEntry,
                     pVxlanIsSetFsVxlanVniVlanMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVniVlanMapTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                 i4FsVxlanVniVlanMapRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                        i4FsVxlanVniVlanMapRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapVlanId =
                    pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapVlanId;
                pVxlanTrgFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanVniVlanMapEntry->
                    bFsVxlanVniVlanMapRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                    (pVxlanTrgFsVxlanVniVlanMapEntry,
                     pVxlanTrgIsSetFsVxlanVniVlanMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                     i4FsVxlanVniVlanMapRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanVniVlanMapEntry->
                    bFsVxlanVniVlanMapRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                    (pVxlanTrgFsVxlanVniVlanMapEntry,
                     pVxlanTrgIsSetFsVxlanVniVlanMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsVxlanVniVlanMapEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                i4FsVxlanVniVlanMapRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                (pVxlanSetFsVxlanVniVlanMapEntry,
                 pVxlanIsSetFsVxlanVniVlanMapEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVniVlanMapTable:  VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
                (pVxlanSetFsVxlanVniVlanMapEntry,
                 pVxlanIsSetFsVxlanVniVlanMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
            CLI_SET_ERR (CLI_VXLAN_VNIVLAN_ENTRY_NOT_EXIST);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
              i4FsVxlanVniVlanMapRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                 i4FsVxlanVniVlanMapRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
            (pVxlanSetFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanVniVlanMapTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
        CLI_SET_ERR (CLI_VXLAN_VNIVLAN_ROW_EXISTS);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsVxlanVniVlanMapEntry, pVxlanFsVxlanVniVlanMapEntry,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
        i4FsVxlanVniVlanMapRowStatus == DESTROY)
    {
        if (pVxlanIsSetFsVxlanVniVlanMapEntry->
            bFsVxlanVniVlanMapVniNumber != OSIX_FALSE)
        {
            if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
                u4FsVxlanVniVlanMapVniNumber !=
                pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                u4FsVxlanVniVlanMapVniNumber)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_VNI);
                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VNI-VLAN map entry\n"));
                return OSIX_FAILURE;
            }

        }

        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
            DESTROY;

        pVxlanFsVxlanNveEntry = VxlanUtilGetNveIndexFromNveTable
            (pVxlanFsVxlanVniVlanMapEntry->
             MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
        pVxlanFsVxlanInReplicaEntry = VxlanUtilGetNveIndexFromInReplicaTable
            (pVxlanFsVxlanVniVlanMapEntry->
             MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveRepIfIndex);
        if ((u4NveIfIndex == 0) || (pVxlanFsVxlanInReplicaEntry == NULL))
        {
            pVxlanFsVxlanMCastEntry = VxlanUtilGetNveIndexFromMcastTable
                (pVxlanFsVxlanVniVlanMapEntry->
                 MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
        }
        if (u4NveIfIndex == 0)
        {
            u4NveIfIndex = u4NveRepIfIndex;
        }
#ifdef VLAN_WANTED
        if (VlanApiSetMemberPort ((UINT4) pVxlanFsVxlanVniVlanMapEntry->
                                  MibObject.i4FsVxlanVniVlanMapVlanId,
                                  u4NveIfIndex, VLAN_DELETE) == VLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable: Fail to"
                        "unamp NVE and VLAN.\r\n"));

        }

#endif
        VxlanHandleVlanVniStatus (u4NveIfIndex,
                                  pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                  u4FsVxlanVniVlanMapVniNumber,
                                  pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                  i4FsVxlanVniVlanMapVlanId, VXLAN_HW_DEL);

        if (VxlanHwUpdateVniVlanMapDatabase (pVxlanFsVxlanVniVlanMapEntry,
                                             VXLAN_HW_DEL) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC, "Failed to delete VNI-VLAN mapping"
                        " entry in h/w\n"));
        }

        UNUSED_PARAM (pVxlanFsVxlanMCastEntry);
        UNUSED_PARAM (pVxlanFsVxlanNveEntry);
        UNUSED_PARAM (pVxlanFsVxlanInReplicaEntry);
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   pVxlanFsVxlanVniVlanMapEntry);
        if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
            (pVxlanSetFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable:"
                        "VxlanSetAllFsVxlanVniVlanMapTableTrigger "
                        "function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsVxlanVniVlanMapTableFilterInputs
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanSetFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
        return OSIX_SUCCESS;
    }
    if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
        i4FsVxlanVniVlanMapRowStatus == ACTIVE)
    {
        if ((VxlanUtilGetVlanFromVni (pVxlanSetFsVxlanVniVlanMapEntry->
                                      MibObject.u4FsVxlanVniVlanMapVniNumber,
                                      &u2VlanId) == VXLAN_SUCCESS) &&
            (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             u4FsVxlanVniVlanMapPktSent != 0))
        {
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);

            CLI_SET_ERR (CLI_VXLAN_VNIVLAN_MAPPED_WITH_OTHER_VLAN);
            return OSIX_FAILURE;
        }
    }
    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus ==
         ACTIVE)
        && (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus == NOT_IN_SERVICE))
    {
        if (pVxlanIsSetFsVxlanVniVlanMapEntry->
            bFsVxlanVniVlanMapVniNumber != OSIX_FALSE)
        {
            if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
                u4FsVxlanVniVlanMapVniNumber !=
                pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                u4FsVxlanVniVlanMapVniNumber)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_VNI);
                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VNI-VLAN map entry\n"));
                return OSIX_FAILURE;
            }
        }

        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus = NOT_IN_SERVICE;
        pVxlanTrgIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus =
            OSIX_TRUE;

        if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
            (pVxlanTrgFsVxlanVniVlanMapEntry,
             pVxlanTrgIsSetFsVxlanVniVlanMapEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable: "
                        "VxlanSetAllFsVxlanVniVlanMapTableTrigger "
                        "function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
        i4FsVxlanVniVlanMapRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapVniNumber;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktSent;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktRcvd;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktDrpd;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus;

        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus == ACTIVE)
        {
            pVxlanFsVxlanNveEntry = VxlanUtilGetNveIndexFromNveTable
                (pVxlanFsVxlanVniVlanMapEntry->
                 MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);

            pVxlanFsVxlanInReplicaEntry = VxlanUtilGetNveIndexFromInReplicaTable
                (pVxlanFsVxlanVniVlanMapEntry->
                 MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveRepIfIndex);

            if ((u4NveIfIndex == 0) || (pVxlanFsVxlanInReplicaEntry == NULL))
            {
                pVxlanFsVxlanMCastEntry = VxlanUtilGetNveIndexFromMcastTable
                    (pVxlanFsVxlanVniVlanMapEntry->MibObject.
                     u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
            }
            if (u4NveIfIndex == 0)
            {
                u4NveIfIndex = u4NveRepIfIndex;
            }
#ifdef VLAN_WANTED
            if (VlanApiSetMemberPort ((UINT4) pVxlanSetFsVxlanVniVlanMapEntry->
                                      MibObject.i4FsVxlanVniVlanMapVlanId,
                                      u4NveIfIndex, VLAN_UPDATE) ==
                VLAN_FAILURE)
            {
                CLI_SET_ERR (CLI_VXLAN_SET_NVE_PORT_FAIL);
                return OSIX_FAILURE;
            }
#endif
        }
        UNUSED_PARAM (pVxlanFsVxlanMCastEntry);
        UNUSED_PARAM (pVxlanFsVxlanNveEntry);

        if (VxlanHwUpdateVniVlanMapDatabase
            (pVxlanFsVxlanVniVlanMapEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
        {
            CLI_SET_ERR (CLI_VXLAN_HW_ADD_FAIL);
            return OSIX_FAILURE;
        }

        VxlanHandleVlanVniStatus (u4NveIfIndex,
                                  pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                  u4FsVxlanVniVlanMapVniNumber,
                                  pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                  i4FsVxlanVniVlanMapVlanId, VXLAN_HW_ADD);
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
            ACTIVE;
    }

    if (VxlanSetAllFsVxlanVniVlanMapTableTrigger
        (pVxlanSetFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanVniVlanMapTable: VxlanSetAllFsVxlanVniVlanMapTableTrigger function returns failure.\r\n"));
    }
#ifdef L2RED_WANTED
    pVxlanVniVlanPortEntry =
        RBTreeGetFirst (pVxlanFsVxlanVniVlanMapEntry->VxlanVniVlanPortTable);
    while (pVxlanVniVlanPortEntry != NULL)
    {
        VxlanRedSyncDynVlanVniPortInfo (pVxlanVniVlanPortEntry,
                                        pVxlanFsVxlanVniVlanMapEntry);
        pVxlanVniVlanPortEntry =
            RBTreeGetNext (pVxlanFsVxlanVniVlanMapEntry->VxlanVniVlanPortTable,
                           pVxlanVniVlanPortEntry, NULL);
    }
#endif
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsVxlanVniVlanMapEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanInReplicaTable
 Input       :  pVxlanSetFsVxlanInReplicaEntry
                pVxlanIsSetFsVxlanInReplicaEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                                  pVxlanSetFsVxlanInReplicaEntry,
                                  tVxlanIsSetFsVxlanInReplicaEntry *
                                  pVxlanIsSetFsVxlanInReplicaEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanOldFsVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanTrgFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanTrgIsSetFsVxlanInReplicaEntry =
        NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextNveEntry = NULL;
#ifdef L2RED_WANTED
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
#endif
    INT4                i4RowMakeActive = FALSE;
    UINT4               u4NveIndex = 0;
    BOOL1               bNveStaticEntryExists = VXLAN_FALSE;
    UINT4               u4ReplicaIndex = 0;
    pVxlanOldFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanOldFsVxlanInReplicaEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanTrgFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsVxlanInReplicaEntry =
        (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanTrgFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanTrgIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Check whether the node is already present */
    pVxlanFsVxlanInReplicaEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   (tRBElem *) pVxlanSetFsVxlanInReplicaEntry);

    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
             i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
              i4FsVxlanInReplicaRowStatus == ACTIVE)
             && (i4RowCreateOption == OSIX_TRUE)))
        {
            /* Allocate memory for the new node */
            pVxlanFsVxlanInReplicaEntry =
                (tVxlanFsVxlanInReplicaEntry *)
                MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
            if (pVxlanFsVxlanInReplicaEntry == NULL)
            {

                if (VxlanSetAllFsVxlanInReplicaTableTrigger
                    (pVxlanSetFsVxlanInReplicaEntry,
                     pVxlanIsSetFsVxlanInReplicaEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable:VxlanSetAllFsVxlanInReplicaTableTrigger function fails\r\n"));

                }

                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanInReplicaTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanInReplicaEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
                    sizeof (tVxlanFsVxlanInReplicaEntry));

            if ((VxlanInitializeFsVxlanInReplicaTable
                 (pVxlanFsVxlanInReplicaEntry,
                  pVxlanSetFsVxlanInReplicaEntry->MibObject.
                  i4FsVxlanInReplicaNveIfIndex)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsVxlanInReplicaTableTrigger
                    (pVxlanSetFsVxlanInReplicaEntry,
                     pVxlanIsSetFsVxlanInReplicaEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable:VxlanSetAllFsVxlanInReplicaTableTrigger function fails\r\n"));

                }

                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanInReplicaTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanInReplicaEntry);
                CLI_SET_ERR (CLI_VXLAN_INREPLICA_OBJ_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaNveIfIndex =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaNveIfIndex;
            }

            if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    u4FsVxlanInReplicaVniNumber =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    u4FsVxlanInReplicaVniNumber;
            }

            if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaIndex;
            }

            if (pVxlanIsSetFsVxlanInReplicaEntry->
                bFsVxlanInReplicaRemoteVtepAddressType != OSIX_FALSE)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRemoteVtepAddressType =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRemoteVtepAddressType;
            }

            if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus;
            }

            if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
                 i4FsVxlanInReplicaRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                        i4FsVxlanInReplicaRowStatus == ACTIVE)))
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                 (tRBElem *) pVxlanFsVxlanInReplicaEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsVxlanInReplicaTableTrigger
                    (pVxlanSetFsVxlanInReplicaEntry,
                     pVxlanIsSetFsVxlanInReplicaEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanInReplicaTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsVxlanInReplicaEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }

            VxlanUtilGetReplicaIndex (pVxlanFsVxlanInReplicaEntry->
                                      MibObject.u4FsVxlanInReplicaVniNumber,
                                      &u4ReplicaIndex);
            /* Reserving the RT Index */
            VxlanUtilSetReplicaIndex (u4ReplicaIndex,
                                      (UINT4) pVxlanFsVxlanInReplicaEntry->
                                      MibObject.i4FsVxlanInReplicaIndex);

            if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
                 i4FsVxlanInReplicaRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == OSIX_TRUE)
                    && (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                        i4FsVxlanInReplicaRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaNveIfIndex =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaNveIfIndex;
                pVxlanTrgFsVxlanInReplicaEntry->MibObject.
                    u4FsVxlanInReplicaVniNumber =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    u4FsVxlanInReplicaVniNumber;
                pVxlanTrgFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaIndex =
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaIndex;
                pVxlanTrgFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanInReplicaEntry->
                    bFsVxlanInReplicaRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanInReplicaTableTrigger
                    (pVxlanTrgFsVxlanInReplicaEntry,
                     pVxlanTrgIsSetFsVxlanInReplicaEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsVxlanInReplicaEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsVxlanInReplicaEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsVxlanInReplicaEntry->
                    bFsVxlanInReplicaRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsVxlanInReplicaTableTrigger
                    (pVxlanTrgFsVxlanInReplicaEntry,
                     pVxlanTrgIsSetFsVxlanInReplicaEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsVxlanInReplicaEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsVxlanInReplicaEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    i4FsVxlanInReplicaRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsVxlanInReplicaTableTrigger
                (pVxlanSetFsVxlanInReplicaEntry,
                 pVxlanIsSetFsVxlanInReplicaEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanInReplicaTable:  VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsVxlanInReplicaTableTrigger
                (pVxlanSetFsVxlanInReplicaEntry,
                 pVxlanIsSetFsVxlanInReplicaEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanInReplicaTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
            CLI_SET_ERR (CLI_VXLAN_INREPLICA_ENTRY_NOT_EXIST);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
              i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                 i4FsVxlanInReplicaRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsVxlanInReplicaTableTrigger
            (pVxlanSetFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanInReplicaTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
        CLI_SET_ERR (CLI_VXLAN_INREPLICA_ROW_EXISTS);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsVxlanInReplicaEntry, pVxlanFsVxlanInReplicaEntry,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus ==
        DESTROY)
    {

        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
            DESTROY;

        /* Remove the Member port (NVE) from VLAN */
        MEMSET (&VxlanVniVlanMapEntry, 0,
                sizeof (tVxlanFsVxlanVniVlanMapEntry));

        VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
            pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;
        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) & VxlanVniVlanMapEntry, NULL);

        while (pVxlanVniVlanMapEntry != NULL)
        {
            if (pVxlanFsVxlanInReplicaEntry->MibObject.
                u4FsVxlanInReplicaVniNumber !=
                pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber)
            {
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);

                continue;
            }
            pVxlanFsVxlanNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanInReplicaEntry->
                                                  MibObject.
                                                  u4FsVxlanInReplicaVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanFsVxlanNveEntry != NULL)
                   && (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                       pVxlanFsVxlanInReplicaEntry->MibObject.
                       i4FsVxlanInReplicaNveIfIndex)
                   && (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanInReplicaEntry->MibObject.
                       u4FsVxlanInReplicaVniNumber))
            {
                if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                    VXLAN_STRG_TYPE_NON_VOL)
                {
                    bNveStaticEntryExists = VXLAN_TRUE;
                    break;
                }
                pVxlanFsVxlanNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanFsVxlanNveEntry, NULL);

            }
            pVxlanFsVxlanNveEntry = NULL;

            pVxlanFsVxlanNveEntry =
                VxlanUtilGetNveIndexFromNveTable (pVxlanFsVxlanInReplicaEntry->
                                                  MibObject.
                                                  u4FsVxlanInReplicaVniNumber,
                                                  &u4NveIndex);
            while ((pVxlanFsVxlanNveEntry != NULL)
                   &&
                   ((pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                     pVxlanFsVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaNveIfIndex)
                    || ((INT4) pVxlanFsVxlanNveEntry->u4OrgNveIfIndex ==
                        pVxlanFsVxlanInReplicaEntry->MibObject.
                        i4FsVxlanInReplicaNveIfIndex))
                   && (pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                       pVxlanFsVxlanInReplicaEntry->MibObject.
                       u4FsVxlanInReplicaVniNumber))
            {
                pVxlanFsVxlanNextNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) pVxlanFsVxlanNveEntry, NULL);

                if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                    VXLAN_STRG_TYPE_VOL)
                {
                    if (VxlanHwUpdateNveDatabase
                        (pVxlanFsVxlanNveEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
                    {
                        VXLAN_TRC ((VXLAN_FAIL_TRC,
                                    "Failed to delete Nve entry in h/w\n"));
                    }
                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanFsVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanFsVxlanNveEntry);

                }
                pVxlanFsVxlanNveEntry = pVxlanFsVxlanNextNveEntry;
            }
            pVxlanFsVxlanInReplicaEntry->i4VlanId =
                pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
            if (VxlanHwUpdateInReplicaDatabase
                (pVxlanFsVxlanInReplicaEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Ingress-Replica entry in h/w\n"));
            }
            if (bNveStaticEntryExists == VXLAN_FALSE)
            {
#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort
                    ((UINT4) pVxlanVniVlanMapEntry->MibObject.
                     i4FsVxlanVniVlanMapVlanId,
                     (UINT4) pVxlanFsVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaNveIfIndex, VLAN_DELETE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable: Fail to"
                                "unamp NVE and VLAN.\r\n"));

                }
#endif
            }

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanVniVlanMapEntry, NULL);
        }
        if (VxlanUtilGetReplicaIndex (pVxlanFsVxlanInReplicaEntry->
                                      MibObject.u4FsVxlanInReplicaVniNumber,
                                      &u4ReplicaIndex) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanInReplicaTable: Fail to"
                        "get free replica index\r\n"));
        }

        VxlanUtilReleaseReplicaIndex (u4ReplicaIndex,
                                      (UINT4) pVxlanFsVxlanInReplicaEntry->
                                      MibObject.i4FsVxlanInReplicaIndex);

        pVxlanFsVxlanInReplicaEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       (tRBElem *) pVxlanFsVxlanInReplicaEntry);
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   pVxlanFsVxlanInReplicaEntry);
        if (VxlanSetAllFsVxlanInReplicaTableTrigger
            (pVxlanSetFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger"
                        "function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsVxlanInReplicaTableFilterInputs
        (pVxlanFsVxlanInReplicaEntry, pVxlanSetFsVxlanInReplicaEntry,
         pVxlanIsSetFsVxlanInReplicaEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus ==
         ACTIVE)
        && (pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus != NOT_IN_SERVICE))
    {
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus =
            OSIX_TRUE;

        if (VxlanSetAllFsVxlanInReplicaTableTrigger
            (pVxlanTrgFsVxlanInReplicaEntry,
             pVxlanTrgIsSetFsVxlanInReplicaEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsVxlanInReplicaEntry->
        bFsVxlanInReplicaRemoteVtepAddressType != OSIX_FALSE)
    {
        pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType =
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType;
    }

    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress !=
        OSIX_FALSE)
    {
        MEMCPY (pVxlanFsVxlanInReplicaEntry->
                MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
                pVxlanSetFsVxlanInReplicaEntry->
                MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
                pVxlanSetFsVxlanInReplicaEntry->
                MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen);
        pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressLen =
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressLen;
    }

    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus !=
        OSIX_FALSE)
    {
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus;

        /* Get the VLAN id from VLAN- VNI mapping table and associate
         * the NVE interface with that VLANS
         * */
        if (pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus == ACTIVE)
        {
            MEMSET (&VxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanFsVxlanVniVlanMapEntry));

            VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
                pVxlanFsVxlanInReplicaEntry->MibObject.
                u4FsVxlanInReplicaVniNumber;
            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) & VxlanVniVlanMapEntry, NULL);

            while (pVxlanVniVlanMapEntry != NULL)
            {
                if (pVxlanFsVxlanInReplicaEntry->MibObject.
                    u4FsVxlanInReplicaVniNumber !=
                    pVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber)
                {
                    pVxlanVniVlanMapEntry =
                        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                       FsVxlanVniVlanMapTable,
                                       (tRBElem *) pVxlanVniVlanMapEntry, NULL);
                    continue;
                }
#ifdef VLAN_WANTED
                if (VlanApiSetMemberPort ((UINT4) pVxlanVniVlanMapEntry->
                                          MibObject.i4FsVxlanVniVlanMapVlanId,
                                          (UINT4) pVxlanFsVxlanInReplicaEntry->
                                          MibObject.
                                          i4FsVxlanInReplicaNveIfIndex,
                                          VLAN_UPDATE) == VLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsVxlanInReplicaTable: Fail to"
                                "map NVE and VLAN.\r\n"));
                }

#endif
                pVxlanFsVxlanInReplicaEntry->i4VlanId =
                    pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
                if (VxlanHwUpdateInReplicaDatabase
                    (pVxlanFsVxlanInReplicaEntry,
                     VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VXLAN_HW_ADD_FAIL);
                    return OSIX_FAILURE;
                }
                pVxlanVniVlanMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanVniVlanMapTable,
                                   (tRBElem *) pVxlanVniVlanMapEntry, NULL);
            }

        }
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
            ACTIVE;
    }

    if (VxlanSetAllFsVxlanInReplicaTableTrigger (pVxlanSetFsVxlanInReplicaEntry,
                                                 pVxlanIsSetFsVxlanInReplicaEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsVxlanInReplicaTable: VxlanSetAllFsVxlanInReplicaTableTrigger function returns failure.\r\n"));
    }
#ifdef L2RED_WANTED
    pVxlanNvePortEntry =
        VxlanGetNvePortEntry (pVxlanFsVxlanInReplicaEntry->MibObject.
                              au1FsVxlanInReplicaRemoteVtepAddress,
                              pVxlanFsVxlanInReplicaEntry->MibObject.
                              i4FsVxlanInReplicaRemoteVtepAddressLen);
    if ((pVxlanNvePortEntry != NULL) && (pVxlanNvePortEntry->u4RefCount == 1))
    {
        VxlanRedSyncDynNvePortInfo (pVxlanNvePortEntry);
    }
#endif
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanOldFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsVxlanInReplicaEntry);
    return OSIX_SUCCESS;

}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanEviVniMapTable
 Input       :  pVxlanSetFsEvpnVxlanEviVniMapEntry
                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                      pVxlanSetFsEvpnVxlanEviVniMapEntry,
                                      tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                      pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanOldFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanTrgFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry
        * pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pVxlanOldFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanOldFsEvpnVxlanEviVniMapEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanTrgFsEvpnVxlanEviVniMapEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanTrgFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) pVxlanSetFsEvpnVxlanEviVniMapEntry);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
             i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
              i4FsEvpnVxlanEviVniMapRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pVxlanFsEvpnVxlanEviVniMapEntry =
                (tVxlanFsEvpnVxlanEviVniMapEntry *)
                MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
            if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
            {
                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable:VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
            if ((VxlanInitializeFsEvpnVxlanEviVniMapTable
                 (pVxlanFsEvpnVxlanEviVniMapEntry)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable:VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapEviIndex != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapEviIndex =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapEviIndex;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapVniNumber != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapVniNumber =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapVniNumber;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapBgpRD != OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->
                        MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
                        pVxlanSetFsEvpnVxlanEviVniMapEntry->
                        MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
                        pVxlanSetFsEvpnVxlanEviVniMapEntry->
                        MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);

                pVxlanFsEvpnVxlanEviVniMapEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                        au1FsEvpnVxlanEviVniESI,
                        pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                        au1FsEvpnVxlanEviVniESI,
                        pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                        i4FsEvpnVxlanEviVniESILen);

                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniESILen =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniESILen;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniLoadBalance != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapSentPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapSentPkts =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapSentPkts;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapRcvdPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapRcvdPkts =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapRcvdPkts;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapDroppedPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapDroppedPkts =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapDroppedPkts;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapRowStatus != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus;
            }

            if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapBgpRDAuto != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapBgpRDAuto =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapBgpRDAuto;
            }

            if ((pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                 i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                        i4FsEvpnVxlanEviVniMapRowStatus == ACTIVE)))
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                     i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                 (tRBElem *) pVxlanFsEvpnVxlanEviVniMapEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }
            if (VxlanUtilUpdateFsEvpnVxlanEviVniMapTable
                (NULL, pVxlanFsEvpnVxlanEviVniMapEntry,
                 pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanUtilUpdateFsEvpnVxlanEviVniMapTable function returns failure.\r\n"));

                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           pVxlanFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                 i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                        i4FsEvpnVxlanEviVniMapRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapEviIndex =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapEviIndex;
                pVxlanTrgFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapVniNumber =
                    pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    u4FsEvpnVxlanEviVniMapVniNumber;
                pVxlanTrgFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry->
                    bFsEvpnVxlanEviVniMapRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanTrgFsEvpnVxlanEviVniMapEntry,
                     pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                     i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry->
                    bFsEvpnVxlanEviVniMapRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                    (pVxlanTrgFsEvpnVxlanEviVniMapEntry,
                     pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanEviVniMapEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                 pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable:  VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                 pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
              i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                 i4FsEvpnVxlanEviVniMapRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
            (pVxlanSetFsEvpnVxlanEviVniMapEntry,
             pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanEviVniMapTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsEvpnVxlanEviVniMapEntry, pVxlanFsEvpnVxlanEviVniMapEntry,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapRowStatus == DESTROY)
    {
        /* Delete all RT Entries exists in EVI_MAP Table */
        VxlanDelBgpRTEntry (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniMapEviIndex,
                            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                            u4FsEvpnVxlanEviVniMapVniNumber);
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus = DESTROY;

        if (VxlanUtilUpdateFsEvpnVxlanEviVniMapTable
            (pVxlanOldFsEvpnVxlanEviVniMapEntry,
             pVxlanFsEvpnVxlanEviVniMapEntry,
             pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != OSIX_SUCCESS)
        {

            if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                 pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanUtilUpdateFsEvpnVxlanEviVniMapTable function returns failure.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   pVxlanFsEvpnVxlanEviVniMapEntry);
        if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
            (pVxlanSetFsEvpnVxlanEviVniMapEntry,
             pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsEvpnVxlanEviVniMapTableFilterInputs
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanSetFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
         i4FsEvpnVxlanEviVniMapRowStatus == ACTIVE)
        && (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus != NOT_IN_SERVICE))
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus = NOT_IN_SERVICE;
        pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapRowStatus = OSIX_TRUE;

        if (VxlanUtilUpdateFsEvpnVxlanEviVniMapTable
            (pVxlanOldFsEvpnVxlanEviVniMapEntry,
             pVxlanFsEvpnVxlanEviVniMapEntry,
             pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry,
                    pVxlanOldFsEvpnVxlanEviVniMapEntry,
                    sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable:                 VxlanUtilUpdateFsEvpnVxlanEviVniMapTable Function returns failure.\r\n"));

            if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
                (pVxlanSetFsEvpnVxlanEviVniMapEntry,
                 pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
            return OSIX_FAILURE;
        }

        if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
            (pVxlanTrgFsEvpnVxlanEviVniMapEntry,
             pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD !=
        OSIX_FALSE)
    {
        MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->
                MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->
                MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->
                MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);

        pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->
            MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI !=
        OSIX_FALSE)
    {
        MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                au1FsEvpnVxlanEviVniESI,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                au1FsEvpnVxlanEviVniESI,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniESILen);

        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniESILen;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniLoadBalance =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniLoadBalance;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapSentPkts =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapSentPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapRcvdPkts =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapRcvdPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
        bFsEvpnVxlanEviVniMapDroppedPkts != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapDroppedPkts =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapDroppedPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapBgpRDAuto =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapBgpRDAuto;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus = ACTIVE;
    }

    if (VxlanUtilUpdateFsEvpnVxlanEviVniMapTable
        (pVxlanOldFsEvpnVxlanEviVniMapEntry, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != OSIX_SUCCESS)
    {

        if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
            (pVxlanSetFsEvpnVxlanEviVniMapEntry,
             pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));

        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanUtilUpdateFsEvpnVxlanEviVniMapTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry,
                pVxlanOldFsEvpnVxlanEviVniMapEntry,
                sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
        return OSIX_FAILURE;

    }
    if (VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
        (pVxlanSetFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanEviVniMapTable: VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanEviVniMapEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanBgpRTTable
 Input       :  pVxlanSetFsEvpnVxlanBgpRTEntry
                pVxlanIsSetFsEvpnVxlanBgpRTEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                  pVxlanSetFsEvpnVxlanBgpRTEntry,
                                  tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                  pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanOldFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanTrgFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry
        * pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pVxlanOldFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanOldFsEvpnVxlanBgpRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanTrgFsEvpnVxlanBgpRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanTrgFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanBgpRTEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                   (tRBElem *) pVxlanSetFsEvpnVxlanBgpRTEntry);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsEvpnVxlanBgpRTEntry->
             MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsEvpnVxlanBgpRTEntry->
                MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsEvpnVxlanBgpRTEntry->
              MibObject.i4FsEvpnVxlanBgpRTRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pVxlanFsEvpnVxlanBgpRTEntry =
                (tVxlanFsEvpnVxlanBgpRTEntry *)
                MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
            if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
            {
                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanSetFsEvpnVxlanBgpRTEntry,
                     pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable:VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
            if ((VxlanInitializeFsEvpnVxlanBgpRTTable
                 (pVxlanFsEvpnVxlanBgpRTEntry)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanSetFsEvpnVxlanBgpRTEntry,
                     pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable:VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanBgpRTIndex;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTType;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry->
                        MibObject.au1FsEvpnVxlanBgpRT,
                        pVxlanSetFsEvpnVxlanBgpRTEntry->
                        MibObject.au1FsEvpnVxlanBgpRT,
                        pVxlanSetFsEvpnVxlanBgpRTEntry->
                        MibObject.i4FsEvpnVxlanBgpRTLen);

                pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTLen;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                    i4FsEvpnVxlanBgpRTAuto;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex
                != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapEviIndex;
            }

            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber
                != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanEviVniMapVniNumber;
            }

            if ((pVxlanSetFsEvpnVxlanBgpRTEntry->
                 MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanBgpRTEntry->
                        MibObject.i4FsEvpnVxlanBgpRTRowStatus == ACTIVE)))
            {
                pVxlanFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsEvpnVxlanBgpRTEntry->
                     MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                 (tRBElem *) pVxlanFsEvpnVxlanBgpRTEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanSetFsEvpnVxlanBgpRTEntry,
                     pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }
            if (VxlanUtilUpdateFsEvpnVxlanBgpRTTable
                (NULL, pVxlanFsEvpnVxlanBgpRTEntry,
                 pVxlanIsSetFsEvpnVxlanBgpRTEntry) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanUtilUpdateFsEvpnVxlanBgpRTTable function returns failure.\r\n"));

                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanSetFsEvpnVxlanBgpRTEntry,
                     pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                           pVxlanFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsEvpnVxlanBgpRTEntry->
                 MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanBgpRTEntry->
                        MibObject.i4FsEvpnVxlanBgpRTRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanBgpRTIndex =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanBgpRTIndex;
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTType =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTType;
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanEviVniMapEviIndex;
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
                    pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.u4FsEvpnVxlanEviVniMapVniNumber;
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus
                    = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanTrgFsEvpnVxlanBgpRTEntry,
                     pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsEvpnVxlanBgpRTEntry->
                     MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus
                    = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                    (pVxlanTrgFsEvpnVxlanBgpRTEntry,
                     pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanBgpRTEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsEvpnVxlanBgpRTEntry->
                MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsEvpnVxlanBgpRTEntry->
                    MibObject.i4FsEvpnVxlanBgpRTRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                (pVxlanSetFsEvpnVxlanBgpRTEntry,
                 pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable:  VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                (pVxlanSetFsEvpnVxlanBgpRTEntry,
                 pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsEvpnVxlanBgpRTEntry->
              MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsEvpnVxlanBgpRTEntry->
                 MibObject.i4FsEvpnVxlanBgpRTRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
            (pVxlanSetFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanBgpRTTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsEvpnVxlanBgpRTEntry, pVxlanFsEvpnVxlanBgpRTEntry,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus ==
        DESTROY)
    {
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
            DESTROY;

        if (VxlanUtilUpdateFsEvpnVxlanBgpRTTable
            (pVxlanOldFsEvpnVxlanBgpRTEntry, pVxlanFsEvpnVxlanBgpRTEntry,
             pVxlanIsSetFsEvpnVxlanBgpRTEntry) != OSIX_SUCCESS)
        {

            if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                (pVxlanSetFsEvpnVxlanBgpRTEntry,
                 pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanUtilUpdateFsEvpnVxlanBgpRTTable function returns failure.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                   pVxlanFsEvpnVxlanBgpRTEntry);
        if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
            (pVxlanSetFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsEvpnVxlanBgpRTTableFilterInputs
        (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanSetFsEvpnVxlanBgpRTEntry,
         pVxlanIsSetFsEvpnVxlanBgpRTEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus ==
         ACTIVE)
        && (pVxlanSetFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanBgpRTRowStatus != NOT_IN_SERVICE))
    {
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus =
            OSIX_TRUE;

        if (VxlanUtilUpdateFsEvpnVxlanBgpRTTable
            (pVxlanOldFsEvpnVxlanBgpRTEntry, pVxlanFsEvpnVxlanBgpRTEntry,
             pVxlanIsSetFsEvpnVxlanBgpRTEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanOldFsEvpnVxlanBgpRTEntry,
                    sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable:                 VxlanUtilUpdateFsEvpnVxlanBgpRTTable Function returns failure.\r\n"));

            if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
                (pVxlanSetFsEvpnVxlanBgpRTEntry,
                 pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
            return OSIX_FAILURE;
        }

        if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
            (pVxlanTrgFsEvpnVxlanBgpRTEntry,
             pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
                pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
                pVxlanSetFsEvpnVxlanBgpRTEntry->
                MibObject.i4FsEvpnVxlanBgpRTLen);

        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen =
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
            pVxlanSetFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanBgpRTRowStatus;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto =
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
            ACTIVE;
    }

    if (VxlanUtilUpdateFsEvpnVxlanBgpRTTable (pVxlanOldFsEvpnVxlanBgpRTEntry,
                                              pVxlanFsEvpnVxlanBgpRTEntry,
                                              pVxlanIsSetFsEvpnVxlanBgpRTEntry)
        != OSIX_SUCCESS)
    {

        if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
            (pVxlanSetFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));

        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanUtilUpdateFsEvpnVxlanBgpRTTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanOldFsEvpnVxlanBgpRTEntry,
                sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
        return OSIX_FAILURE;

    }
    if (VxlanSetAllFsEvpnVxlanBgpRTTableTrigger (pVxlanSetFsEvpnVxlanBgpRTEntry,
                                                 pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanBgpRTTable: VxlanSetAllFsEvpnVxlanBgpRTTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanBgpRTEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanVrfTable
 Input       :  pVxlanSetFsEvpnVxlanVrfEntry
                pVxlanIsSetFsEvpnVxlanVrfEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                pVxlanSetFsEvpnVxlanVrfEntry,
                                tVxlanIsSetFsEvpnVxlanVrfEntry *
                                pVxlanIsSetFsEvpnVxlanVrfEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanOldFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanTrgFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanTrgIsSetFsEvpnVxlanVrfEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pVxlanOldFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanOldFsEvpnVxlanVrfEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanTrgFsEvpnVxlanVrfEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanTrgFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanTrgIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) pVxlanSetFsEvpnVxlanVrfEntry);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
             i4FsEvpnVxlanVrfRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                i4FsEvpnVxlanVrfRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
              i4FsEvpnVxlanVrfRowStatus == ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pVxlanFsEvpnVxlanVrfEntry =
                (tVxlanFsEvpnVxlanVrfEntry *)
                MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
            if (pVxlanFsEvpnVxlanVrfEntry == NULL)
            {
                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable:VxlanSetAllFsEvpnVxlanVrfTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanVrfEntry));
            if ((VxlanInitializeFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry))
                == OSIX_FAILURE)
            {
                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable:VxlanSetAllFsEvpnVxlanVrfTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        i4FsEvpnVxlanVrfNameLen);

                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniNumber =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniNumber;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD != OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfRD,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfRD,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        i4FsEvpnVxlanVrfRDLen);

                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRDLen;
            }

            if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRowStatus;
            }

            if ((pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                 i4FsEvpnVxlanVrfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        i4FsEvpnVxlanVrfRowStatus == ACTIVE)))
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
                    ACTIVE;
            }
            else if (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                     i4FsEvpnVxlanVrfRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
                    NOT_READY;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->
                bFsEvpnVxlanVrfVniMapSentPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapSentPkts =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapSentPkts;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->
                bFsEvpnVxlanVrfVniMapRcvdPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapRcvdPkts =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapRcvdPkts;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->
                bFsEvpnVxlanVrfVniMapDroppedPkts != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapDroppedPkts =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    u4FsEvpnVxlanVrfVniMapDroppedPkts;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRDAuto;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                 (tRBElem *) pVxlanFsEvpnVxlanVrfEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }
            if (VxlanUtilUpdateFsEvpnVxlanVrfTable
                (NULL, pVxlanFsEvpnVxlanVrfEntry,
                 pVxlanIsSetFsEvpnVxlanVrfEntry) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: VxlanUtilUpdateFsEvpnVxlanVrfTable function returns failure.\r\n"));

                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           pVxlanFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                 i4FsEvpnVxlanVrfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        i4FsEvpnVxlanVrfRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pVxlanTrgFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                        i4FsEvpnVxlanVrfNameLen);

                pVxlanTrgFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen =
                    pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen;
                pVxlanTrgFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanTrgFsEvpnVxlanVrfEntry,
                     pVxlanTrgIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                     i4FsEvpnVxlanVrfRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus =
                    OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                    (pVxlanTrgFsEvpnVxlanVrfEntry,
                     pVxlanTrgIsSetFsEvpnVxlanVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                        (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                i4FsEvpnVxlanVrfRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                    i4FsEvpnVxlanVrfRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable:  VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
              i4FsEvpnVxlanVrfRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                 i4FsEvpnVxlanVrfRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsEvpnVxlanVrfTableTrigger (pVxlanSetFsEvpnVxlanVrfEntry,
                                                   pVxlanIsSetFsEvpnVxlanVrfEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsEvpnVxlanVrfEntry, pVxlanFsEvpnVxlanVrfEntry,
            sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus ==
        DESTROY)
    {
        /* Delete all RT Entries exists in VRF-RT Table */
        VxlanDelVrfRTEntry (EVPN_P_VRF_NAME (pVxlanSetFsEvpnVxlanVrfEntry),
                            EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry));

        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
            DESTROY;

        if (VxlanUtilUpdateFsEvpnVxlanVrfTable (pVxlanOldFsEvpnVxlanVrfEntry,
                                                pVxlanFsEvpnVxlanVrfEntry,
                                                pVxlanIsSetFsEvpnVxlanVrfEntry)
            != OSIX_SUCCESS)
        {

            if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: VxlanUtilUpdateFsEvpnVxlanVrfTable function returns failure.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   pVxlanFsEvpnVxlanVrfEntry);
        if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
            (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsEvpnVxlanVrfTableFilterInputs
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanSetFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus ==
         ACTIVE)
        && (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus !=
            NOT_IN_SERVICE))
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

        if (VxlanUtilUpdateFsEvpnVxlanVrfTable (pVxlanOldFsEvpnVxlanVrfEntry,
                                                pVxlanFsEvpnVxlanVrfEntry,
                                                pVxlanIsSetFsEvpnVxlanVrfEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pVxlanFsEvpnVxlanVrfEntry, pVxlanOldFsEvpnVxlanVrfEntry,
                    sizeof (tVxlanFsEvpnVxlanVrfEntry));
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable:                 VxlanUtilUpdateFsEvpnVxlanVrfTable Function returns failure.\r\n"));

            if (VxlanSetAllFsEvpnVxlanVrfTableTrigger
                (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
            return OSIX_FAILURE;
        }

        if (VxlanSetAllFsEvpnVxlanVrfTableTrigger (pVxlanTrgFsEvpnVxlanVrfEntry,
                                                   pVxlanTrgIsSetFsEvpnVxlanVrfEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniNumber =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniNumber;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
                EVPN_MAX_RD_LEN);

        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapSentPkts =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapSentPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapRcvdPkts =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapRcvdPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->
        bFsEvpnVxlanVrfVniMapDroppedPkts != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapDroppedPkts =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapDroppedPkts;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus = ACTIVE;
    }

    if (VxlanUtilUpdateFsEvpnVxlanVrfTable (pVxlanOldFsEvpnVxlanVrfEntry,
                                            pVxlanFsEvpnVxlanVrfEntry,
                                            pVxlanIsSetFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {

        if (VxlanSetAllFsEvpnVxlanVrfTableTrigger (pVxlanSetFsEvpnVxlanVrfEntry,
                                                   pVxlanIsSetFsEvpnVxlanVrfEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));

        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfTable: VxlanUtilUpdateFsEvpnVxlanVrfTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pVxlanFsEvpnVxlanVrfEntry, pVxlanOldFsEvpnVxlanVrfEntry,
                sizeof (tVxlanFsEvpnVxlanVrfEntry));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
        return OSIX_FAILURE;

    }
    if (VxlanSetAllFsEvpnVxlanVrfTableTrigger (pVxlanSetFsEvpnVxlanVrfEntry,
                                               pVxlanIsSetFsEvpnVxlanVrfEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfTable: VxlanSetAllFsEvpnVxlanVrfTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanVrfRTTable
 Input       :  pVxlanSetFsEvpnVxlanVrfRTEntry
                pVxlanIsSetFsEvpnVxlanVrfRTEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                  pVxlanSetFsEvpnVxlanVrfRTEntry,
                                  tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                  pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanOldFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanTrgFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pVxlanOldFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanOldFsEvpnVxlanVrfRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        return OSIX_FAILURE;
    }
    pVxlanTrgFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanTrgFsEvpnVxlanVrfRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanTrgFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanVrfRTEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   (tRBElem *) pVxlanSetFsEvpnVxlanVrfRTEntry);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
             i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
              i4FsEvpnVxlanVrfRTRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pVxlanFsEvpnVxlanVrfRTEntry =
                (tVxlanFsEvpnVxlanVrfRTEntry *)
                MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
            if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
            {
                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfRTEntry,
                     pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable:VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                CLI_SET_ERR (CLI_VXLAN_MEMORY_FAILURE);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
            if ((VxlanInitializeFsEvpnVxlanVrfRTTable
                 (pVxlanFsEvpnVxlanVrfRTEntry)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfRTEntry,
                     pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable:VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                CLI_SET_ERR (CLI_VXLAN_VRF_RT_INIT_FAIL);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    u4FsEvpnVxlanVrfRTIndex;
            }

            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTType;
            }

            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfRT,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfRT,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        i4FsEvpnVxlanVrfRTLen);

                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTLen;
            }

            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus !=
                OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus;
            }

            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName !=
                OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        i4FsEvpnVxlanVrfNameLen);

                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen;
            }
            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber !=
                OSIX_FALSE)
            {
                EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
                    EVPN_P_VRF_RT_VNI (pVxlanSetFsEvpnVxlanVrfRTEntry);
            }
            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto !=
                OSIX_FALSE)
            {
                EVPN_P_VRF_RT_AUTO (pVxlanFsEvpnVxlanVrfRTEntry) =
                    EVPN_P_VRF_RT_AUTO (pVxlanSetFsEvpnVxlanVrfRTEntry);
            }
            if ((pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                 i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        i4FsEvpnVxlanVrfRTRowStatus == ACTIVE)))
            {
                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                     i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                 (tRBElem *) pVxlanFsEvpnVxlanVrfRTEntry) != RB_SUCCESS)
            {
                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfRTEntry,
                     pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                CLI_SET_ERR (CLI_VXLAN_RBTREE_ADD_FAIL);
                return OSIX_FAILURE;
            }
            if (VxlanUtilUpdateFsEvpnVxlanVrfRTTable
                (NULL, pVxlanFsEvpnVxlanVrfRTEntry,
                 pVxlanIsSetFsEvpnVxlanVrfRTEntry) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanUtilUpdateFsEvpnVxlanVrfRTTable function returns failure.\r\n"));

                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanSetFsEvpnVxlanVrfRTEntry,
                     pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                           pVxlanFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                    (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                 i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        i4FsEvpnVxlanVrfRTRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                    u4FsEvpnVxlanVrfRTIndex =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    u4FsEvpnVxlanVrfRTIndex;
                pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTType =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTType;
                MEMCPY (pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        au1FsEvpnVxlanVrfName,
                        pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                        i4FsEvpnVxlanVrfNameLen);

                pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen =
                    pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfNameLen;
                pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry->
                    bFsEvpnVxlanVrfRTRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanTrgFsEvpnVxlanVrfRTEntry,
                     pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                     i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry->
                    bFsEvpnVxlanVrfRTRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                    (pVxlanTrgFsEvpnVxlanVrfRTEntry,
                     pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanOldFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanTrgFsEvpnVxlanVrfRTEntry);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                         (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                    i4FsEvpnVxlanVrfRTRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                (pVxlanSetFsEvpnVxlanVrfRTEntry,
                 pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable:  VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                (pVxlanSetFsEvpnVxlanVrfRTEntry,
                 pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
              i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                 i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
            (pVxlanSetFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfRTTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsEvpnVxlanVrfRTEntry, pVxlanFsEvpnVxlanVrfRTEntry,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus ==
        DESTROY)
    {
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
            DESTROY;

        if (VxlanUtilUpdateFsEvpnVxlanVrfRTTable
            (pVxlanOldFsEvpnVxlanVrfRTEntry, pVxlanFsEvpnVxlanVrfRTEntry,
             pVxlanIsSetFsEvpnVxlanVrfRTEntry) != OSIX_SUCCESS)
        {

            if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                (pVxlanSetFsEvpnVxlanVrfRTEntry,
                 pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanUtilUpdateFsEvpnVxlanVrfRTTable function returns failure.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   pVxlanFsEvpnVxlanVrfRTEntry);
        if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
            (pVxlanSetFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsEvpnVxlanVrfRTTableFilterInputs
        (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanSetFsEvpnVxlanVrfRTEntry,
         pVxlanIsSetFsEvpnVxlanVrfRTEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus ==
         ACTIVE)
        && (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
            i4FsEvpnVxlanVrfRTRowStatus != NOT_IN_SERVICE))
    {
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
            NOT_IN_SERVICE;
        pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus =
            OSIX_TRUE;

        if (VxlanUtilUpdateFsEvpnVxlanVrfRTTable
            (pVxlanOldFsEvpnVxlanVrfRTEntry, pVxlanFsEvpnVxlanVrfRTEntry,
             pVxlanIsSetFsEvpnVxlanVrfRTEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanOldFsEvpnVxlanVrfRTEntry,
                    sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable:                 VxlanUtilUpdateFsEvpnVxlanVrfRTTable Function returns failure.\r\n"));

            if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
                (pVxlanSetFsEvpnVxlanVrfRTEntry,
                 pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
            return OSIX_FAILURE;
        }

        if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
            (pVxlanTrgFsEvpnVxlanVrfRTEntry,
             pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT != OSIX_FALSE)
    {
        MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfRTLen);

        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen =
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
            i4FsEvpnVxlanVrfRTRowStatus;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto =
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTRowStatus =
            ACTIVE;
    }

    if (VxlanUtilUpdateFsEvpnVxlanVrfRTTable (pVxlanOldFsEvpnVxlanVrfRTEntry,
                                              pVxlanFsEvpnVxlanVrfRTEntry,
                                              pVxlanIsSetFsEvpnVxlanVrfRTEntry)
        != OSIX_SUCCESS)
    {

        if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
            (pVxlanSetFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));

        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanUtilUpdateFsEvpnVxlanVrfRTTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanOldFsEvpnVxlanVrfRTEntry,
                sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
        return OSIX_FAILURE;

    }
    if (VxlanSetAllFsEvpnVxlanVrfRTTableTrigger (pVxlanSetFsEvpnVxlanVrfRTEntry,
                                                 pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanVrfRTTable: VxlanSetAllFsEvpnVxlanVrfRTTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanVrfRTEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanMultihomedPeerTable
 Input       :  pVxlanSetFsEvpnVxlanMultihomedPeerTable
                pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanMultihomedPeerTable (tVxlanFsEvpnVxlanMultihomedPeerTable
                                           *
                                           pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                                           tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
                                           *
                                           pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                                           INT4 i4RowStatusLogic,
                                           INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable
        * pVxlanOldFsEvpnVxlanMultihomedPeerTable = NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable
        * pVxlanTrgFsEvpnVxlanMultihomedPeerTable = NULL;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        * pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable = NULL;
    INT4                i4RowMakeActive = FALSE;

    pVxlanOldFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pVxlanOldFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return OSIX_FAILURE;
    }
    pVxlanTrgFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pVxlanTrgFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        return OSIX_FAILURE;
    }
    pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable =
        (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID);
    if (pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
        return OSIX_FAILURE;
    }
    MEMSET (pVxlanOldFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanTrgFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanMultihomedPeerTable =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   (tRBElem *) pVxlanSetFsEvpnVxlanMultihomedPeerTable);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
             i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_WAIT)
            || (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_GO)
            ||
            ((pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              i4FsEvpnVxlanMultihomedPeerRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pVxlanFsEvpnVxlanMultihomedPeerTable =
                (tVxlanFsEvpnVxlanMultihomedPeerTable *)
                MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
            if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
            {
                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable:VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                return OSIX_FAILURE;
            }

            MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
                    sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
            if ((VxlanInitializeFsEvpnVxlanMultihomedPeerTable
                 (pVxlanFsEvpnVxlanMultihomedPeerTable)) == OSIX_FAILURE)
            {
                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable:VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function fails\r\n"));

                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanPeerIpAddressType != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressType =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressType;
            }

            if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanPeerIpAddress != OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanPeerIpAddress,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanPeerIpAddress,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanPeerIpAddressLen);

                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressLen =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressLen;
            }

            if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanMHEviVniESI != OSIX_FALSE)
            {
                MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanMHEviVniESI,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanMHEviVniESI,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanMHEviVniESILen);

                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMHEviVniESILen =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMHEviVniESILen;
            }

            if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanOrdinalNum != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    u4FsEvpnVxlanOrdinalNum =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    u4FsEvpnVxlanOrdinalNum;
            }

            if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanMultihomedPeerRowStatus != OSIX_FALSE)
            {
                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus;
            }

            if ((pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                 i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanMultihomedPeerRowStatus == ACTIVE)))
            {
                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus = ACTIVE;
            }
            else if (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                     i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_WAIT)
            {
                pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                 (tRBElem *) pVxlanFsEvpnVxlanMultihomedPeerTable) !=
                RB_SUCCESS)
            {
                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
                }
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                return OSIX_FAILURE;
            }
            if (VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
                (NULL, pVxlanFsEvpnVxlanMultihomedPeerTable,
                 pVxlanIsSetFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable function returns failure.\r\n"));

                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.
                           FsEvpnVxlanMultihomedPeerTable,
                           pVxlanFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                    (UINT1 *)
                                    pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                MemReleaseMemBlock
                    (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                     (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                return OSIX_FAILURE;
            }

            if ((pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                 i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanMultihomedPeerRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressType =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressType;
                MEMCPY (pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanPeerIpAddress,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanPeerIpAddress,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanPeerIpAddressLen);

                pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressLen =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanPeerIpAddressLen;
                MEMCPY (pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanMHEviVniESI,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        au1FsEvpnVxlanMHEviVniESI,
                        pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                        i4FsEvpnVxlanMHEviVniESILen);

                pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMHEviVniESILen =
                    pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMHEviVniESILen;
                pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable->
                    bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanTrgFsEvpnVxlanMultihomedPeerTable,
                     pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                    return OSIX_FAILURE;
                }
            }
            else if (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                     i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus = CREATE_AND_WAIT;
                pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable->
                    bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;

                if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                    (pVxlanTrgFsEvpnVxlanMultihomedPeerTable,
                     pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                         (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
                    MemReleaseMemBlock
                        (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
                    return OSIX_FAILURE;
                }
            }

            if (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_GO)
            {
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                    i4FsEvpnVxlanMultihomedPeerRowStatus = ACTIVE;
            }

            if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                 pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable:  VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanOldFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock
                (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                 (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
            return OSIX_SUCCESS;

        }
        else
        {
            if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                 pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: Failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanOldFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock
                (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                 (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
            return OSIX_FAILURE;
        }
    }
    else if ((pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_WAIT)
             || (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                 i4FsEvpnVxlanMultihomedPeerRowStatus == CREATE_AND_GO))
    {
        if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
            (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
             pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: The row is already present.\r\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pVxlanOldFsEvpnVxlanMultihomedPeerTable,
            pVxlanFsEvpnVxlanMultihomedPeerTable,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus == DESTROY)
    {
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus = DESTROY;

        if (VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
            (pVxlanOldFsEvpnVxlanMultihomedPeerTable,
             pVxlanFsEvpnVxlanMultihomedPeerTable,
             pVxlanIsSetFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
        {

            if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                 pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
            }
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable function returns failure.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   pVxlanFsEvpnVxlanMultihomedPeerTable);
        if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
            (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
             pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanOldFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock
                (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                 (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsEvpnVxlanMultihomedPeerTableFilterInputs
        (pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanSetFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable) != OSIX_TRUE)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
         i4FsEvpnVxlanMultihomedPeerRowStatus == ACTIVE)
        && (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus != NOT_IN_SERVICE))
    {
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pVxlanTrgFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus = NOT_IN_SERVICE;
        pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable->
            bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;

        if (VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
            (pVxlanOldFsEvpnVxlanMultihomedPeerTable,
             pVxlanFsEvpnVxlanMultihomedPeerTable,
             pVxlanIsSetFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable,
                    pVxlanOldFsEvpnVxlanMultihomedPeerTable,
                    sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable:                 VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable Function returns failure.\r\n"));

            if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
                (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                 pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanOldFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *)
                                pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
            MemReleaseMemBlock
                (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                 (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
            return OSIX_FAILURE;
        }

        if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
            (pVxlanTrgFsEvpnVxlanMultihomedPeerTable,
             pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
        }
    }

    if (pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum !=
        OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            u4FsEvpnVxlanOrdinalNum =
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            u4FsEvpnVxlanOrdinalNum;
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus =
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus = ACTIVE;
    }

    if (VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
        (pVxlanOldFsEvpnVxlanMultihomedPeerTable,
         pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
    {

        if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
            (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
             pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));

        }
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable,
                pVxlanOldFsEvpnVxlanMultihomedPeerTable,
                sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
        return OSIX_FAILURE;

    }
    if (VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
        (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanSetAllFsEvpnVxlanMultihomedPeerTable: VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanOldFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanTrgFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanTrgIsSetFsEvpnVxlanMultihomedPeerTable);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanGetAllFsEvpnVxlanArpTable                                                                                           
 Input       :  pVxlanGetFsEvpnVxlanArpEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllFsEvpnVxlanArpTable (tVxlanFsEvpnVxlanArpEntry *
                                pVxlanGetFsEvpnVxlanArpEntry)
{
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    /* Check whether the node is already present */
    pVxlanFsEvpnVxlanArpEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanArpTable,
                   (tRBElem *) pVxlanGetFsEvpnVxlanArpEntry);

    if (pVxlanFsEvpnVxlanArpEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanGetAllFsEvpnVxlanArpTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pVxlanGetFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanVrfNameLen =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanVrfNameLen;
    pVxlanGetFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddresLen =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddresLen;
    MEMCPY (pVxlanGetFsEvpnVxlanArpEntry->MibObject.FsEvpnVxlanArpDestMac,
            pVxlanFsEvpnVxlanArpEntry->MibObject.FsEvpnVxlanArpDestMac,
            VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanGetFsEvpnVxlanArpEntry->MibObject.i4FsVxlanEvpnArpStorageType =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsVxlanEvpnArpStorageType;
    pVxlanGetFsEvpnVxlanArpEntry->MibObject.i4FsVxlanArpRowStatus =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsVxlanArpRowStatus;

    return OSIX_SUCCESS;
}
#endif /* EVPN_VXLAN_WANTED */
