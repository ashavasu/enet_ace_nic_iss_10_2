/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdb.c,v 1.18 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Vxlan 
*********************************************************************/
#include "vxinc.h"

/****************************************************************************
 Function    :  VxlanTestAllFsVxlanVtepTable
 Input       :  pu4ErrorCode
                pVxlanSetFsVxlanVtepEntry
                pVxlanIsSetFsVxlanVtepEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsVxlanVtepTable (UINT4 *pu4ErrorCode,
                              tVxlanFsVxlanVtepEntry *
                              pVxlanSetFsVxlanVtepEntry,
                              tVxlanIsSetFsVxlanVtepEntry *
                              pVxlanIsSetFsVxlanVtepEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNextNveEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;

    UINT4               u4VtepAddr = 0;
    UINT4               u4Count = 0;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
    INT4                i4Status = 0;
    UINT4               u4Val = 0;
#endif
    INT4                i4FsVxlanEnable = 0;
    BOOL1               bDynamicEntryExists = VXLAN_FALSE;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

#ifdef IP6_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif

    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* VXLAN feature must be enabled, to test VXLAN-VTEP table Elements */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSVXLANVTEPTABLE - 3)) &&
        (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
         CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex != OSIX_FALSE)
    {
        /* Check the VXLAN Interface index value */
        if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex <
             CFA_MIN_NVE_IF_INDEX) ||
            (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex >
             CFA_MAX_NVE_IF_INDEX))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_INT_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid NVE interface index\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType != OSIX_FALSE)
    {
        /* Check the VXLAN Source VTEP Address Type */
        if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType !=
             VXLAN_CLI_ADDRTYPE_IPV4) &&
            (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType !=
             VXLAN_CLI_ADDRTYPE_IPV6))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_SRC_ADDR_TYPE);
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN VTEP Address Type\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress != OSIX_FALSE)
    {
        /* Check for IPV4 address of VXLAN Source VTEP */
        if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen ==
            VXLAN_IP4_ADDR_LEN)
        {
            MEMCPY (&u4VtepAddr,
                    pVxlanSetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                    sizeof (UINT4));
            u4VtepAddr = OSIX_NTOHL (u4VtepAddr);
            if ((VXLAN_IS_VALID_ADDRESS (u4VtepAddr) == VXLAN_FALSE) ||
                ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus !=
                  DESTROY)
                 && (NetIpv4IfIsOurAddress (u4VtepAddr) == NETIPV4_FAILURE)))
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_SRC_IPV4_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN VTEP IPV4 Address\n"));
                return OSIX_FAILURE;
            }
        }
#ifdef IP6_WANTED
        /* Check for IPV6 address of VXLAN Source VTEP */
        else
        {
            MEMCPY (&Ip6Addr,
                    pVxlanSetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                    sizeof (tIp6Addr));
            i4Status = Ip6AddrType (&Ip6Addr);
            if ((i4Status != ADDR6_UNICAST) ||
                (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) == NETIPV6_FAILURE))
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_SRC_IPV6_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN VTEP IPV6 Address\n"));
                return OSIX_FAILURE;
            }
        }
#endif
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus != OSIX_FALSE)
    {
        /* Check the row Status of VTEP Table Entry */
        if ((pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus <
             ACTIVE) ||
            (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus >
             DESTROY))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VTEP_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN VTEP Rowstatus\n"));
            return OSIX_FAILURE;
        }
        /* Check the Conditions to delete an entry of VTEP Table */
        if (pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
            DESTROY)
        {
            /* To delete an entry in VTEP table,
             * there should not be a statis entry in NVE table with this
             * interface index */
            VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex =
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex;
            /* Get the next entry in Nve Table with the Interface Index
             * associated with Source VTEP that is to be deleted*/
            pVxlanNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) & VxlanNveEntry, NULL);
            while ((pVxlanNveEntry != NULL) &&
                   (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                    pVxlanSetFsVxlanVtepEntry->MibObject.
                    i4FsVxlanVtepNveIfIndex))
            {
                if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                    VXLAN_STRG_TYPE_NON_VOL)
                {
                    /* Do not delete the VTEP entry if there is atleast one 
                     * static Nve entry exists with this Nve-interface index*/
                    CLI_SET_ERR (CLI_VXLAN_NVE_ENTRY_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    VXLAN_TRC ((VXLAN_CLI_TRC,
                                "NVETable entry exists with this source-IP\n"));
                    return OSIX_FAILURE;
                }
                else if (!bDynamicEntryExists)
                {
                    /* Dynamic Entries present with this Nve Interface */
                    bDynamicEntryExists = VXLAN_TRUE;
                }
                pVxlanNveEntry = RBTreeGetNext
                    (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                     (tRBElem *) pVxlanNveEntry, NULL);
            }

            /* To delete an entry in VTEP table,
             * there should not be an entry in Multicast table with this
             * interface index */
            VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex;
            /* Get the next entry in Multicast Table with the Interface Index
             * associated with Source VTEP that is to be deleted*/
            pVxlanMCastEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                 (tRBElem *) & VxlanMCastEntry, NULL);
            if ((pVxlanMCastEntry != NULL) &&
                (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                 pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex))
            {
                CLI_SET_ERR (CLI_VXLAN_MCAST_ENTRY_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Mcast Table entry exists with this source-IP\n"));
                return OSIX_FAILURE;
            }
            /* To delete an entry in VTEP table,
             * there should not be an entry in Replica table with this
             * interface index */
            VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex;
            /* Get the next entry in Ingree Replica Table with the Interface Index
             * associated with Source VTEP that is to be deleted*/
            pVxlanInReplicaEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                 (tRBElem *) & VxlanInReplicaEntry, NULL);
            if ((pVxlanInReplicaEntry != NULL) &&
                (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
                 pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex))
            {
                CLI_SET_ERR (CLI_VXLAN_INREPLICA_ENTRY_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Ingress Replica Table entry exists with this source-IP\n"));
                return OSIX_FAILURE;
            }

            /* Delete the Dynamic Nve Entries before deleting VTEP Entry */
            if (bDynamicEntryExists)
            {
                pVxlanNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   (tRBElem *) & VxlanNveEntry, NULL);
                while ((pVxlanNveEntry != NULL)
                       && (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                           pVxlanSetFsVxlanVtepEntry->MibObject.
                           i4FsVxlanVtepNveIfIndex))
                {
                    pVxlanNextNveEntry =
                        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                       FsVxlanNveTable, pVxlanNveEntry, NULL);
                    if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                        VXLAN_STRG_TYPE_VOL)
                    {
                        if (VxlanHwUpdateNveDatabase
                            (pVxlanNveEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
                        {
                            VXLAN_TRC ((VXLAN_CLI_TRC,
                                        "Failed to delete Nve entry in h/w\n"));
                        }
                        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   pVxlanNveEntry);
                        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                            (UINT1 *) pVxlanNveEntry);
                    }
                    pVxlanNveEntry = pVxlanNextNveEntry;
                }

            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsVxlanNveTable
 Input       :  pu4ErrorCode
                pVxlanSetFsVxlanNveEntry
                pVxlanIsSetFsVxlanNveEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsVxlanNveTable (UINT4 *pu4ErrorCode,
                             tVxlanFsVxlanNveEntry * pVxlanSetFsVxlanNveEntry,
                             tVxlanIsSetFsVxlanNveEntry *
                             pVxlanIsSetFsVxlanNveEntry, INT4 i4RowStatusLogic,
                             INT4 i4RowCreateOption)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
#endif
    UINT4               u4RemVtepAddr = 0;
    INT4                i4Status = 0;
    tIp6Addr            Ip6Addr;
    tMacAddr            zeroAddr;
    tMacAddr            BcastMacAddr;
    INT4                i4FsVxlanEnable = 0;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (BcastMacAddr, 0xff, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
#ifdef EVPN_VXLAN_WANTED
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
#endif

    /* VXLAN feature must be enabled, to test VXLAN-Nve table Elements */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex != OSIX_FALSE)
    {
        /* Check the VXLAN Interface index value */
        if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex <
             CFA_MIN_NVE_IF_INDEX) ||
            (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex >
             CFA_MAX_NVE_IF_INDEX))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_INT_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid NVE interface Index\n"));
            return OSIX_FAILURE;
        }
    }
    /* Check the VXLAN Identifier(VNI) value */
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber != OSIX_FALSE)
    {
        if ((pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber <
             MIN_VALUE_VNI_IDENTIFIER) ||
            (pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber >
             MAX_VALUE_VNI_IDENTIFIER))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VNI);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Identifier\n"));
            return OSIX_FAILURE;
        }
        /* VNI number should not be associated with
         * two Nve Interfaces */
        VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber =
            pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
        /* Get the Nve entry with current VNI number if exists */
        pVxlanNveEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) & VxlanNveEntry, NULL);
        /* Error is to be returned, if there is an Nve entry existing with
         * current VNI number and with Different Nve Interface Index */
        while (pVxlanNveEntry != NULL)
        {
            if ((pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                 pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber) &&
                (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex !=
                 pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex))
            {
                CLI_SET_ERR (CLI_VXLAN_VNI_EXIST_NVE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "VXLAN Id is associated with another NVE\n"));
                return OSIX_FAILURE;
            }
            pVxlanNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanNveEntry, NULL);
        }

    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType !=
        OSIX_FALSE)
    {
        /* Check the VXLAN Remote VTEP Address Type */
        if ((pVxlanSetFsVxlanNveEntry->MibObject.
             i4FsVxlanNveRemoteVtepAddressType != VXLAN_CLI_ADDRTYPE_IPV4) &&
            (pVxlanSetFsVxlanNveEntry->MibObject.
             i4FsVxlanNveRemoteVtepAddressType != VXLAN_CLI_ADDRTYPE_IPV6))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_REM_ADDR_TYPE);
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Invalid VXLAN Remote VTEP Address Type\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress != OSIX_FALSE)
    {
        /* Check for IPV4 address of VXLAN Remote VTEP */
        if (pVxlanSetFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressLen == VXLAN_IP4_ADDR_LEN)
        {
            MEMCPY (&u4RemVtepAddr,
                    pVxlanSetFsVxlanNveEntry->MibObject.
                    au1FsVxlanNveRemoteVtepAddress, sizeof (UINT4));
            u4RemVtepAddr = OSIX_NTOHL (u4RemVtepAddr);

            if ((VXLAN_IS_VALID_ADDRESS (u4RemVtepAddr) == VXLAN_FALSE) ||
                (NetIpv4IfIsOurAddress (u4RemVtepAddr) == NETIPV4_SUCCESS))
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV4_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN Remote VTEP IPv4 Address\n"));
                return OSIX_FAILURE;
            }
        }
        /* Check for IPV6 address of VXLAN Remote VTEP */
        else
        {
            MEMCPY (&Ip6Addr,
                    pVxlanSetFsVxlanNveEntry->MibObject.
                    au1FsVxlanNveRemoteVtepAddress, sizeof (tIp6Addr));
            i4Status = Ip6AddrType (&Ip6Addr);
            if (i4Status != ADDR6_UNICAST)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV6_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN remote VTEP IPV6 Address\n"));
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp != OSIX_FALSE)
    {
        if (!(pVxlanSetFsVxlanNveEntry->MibObject.
              i4FsVxlanSuppressArp >= EVPN_CLI_ARP_SUPPRESS) &&
            (pVxlanSetFsVxlanNveEntry->MibObject.
             i4FsVxlanSuppressArp <= EVPN_CLI_ARP_ALLOW))
        {
            CLI_SET_ERR (CLI_EVPN_WRONG_ARP_VALUE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Wrong ARP Suppress value is configured.\n"));
            return OSIX_FAILURE;
        }
#ifdef EVPN_VXLAN_WANTED
        /* Check for multicast Entry if configured with this VNI number */
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
        VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
            pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
        pVxlanMCastEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                       (tRBElem *) & VxlanMCastEntry);
        if (pVxlanMCastEntry != NULL)
        {
            CLI_SET_ERR (CLI_EVPN_ARP_SUP_MCAST_ENABLED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Multicast Entry exists for this VNI\n"));
            return OSIX_FAILURE;
        }

        /* Check for Ingress-Replica Entry if configured with this VNI number */
        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
        VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
            pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
        pVxlanInReplicaEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       (tRBElem *) & VxlanInReplicaEntry);
        if (pVxlanInReplicaEntry != NULL)
        {
            CLI_SET_ERR (CLI_EVPN_ARP_SUP_ING_REP_ENABLED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Ingress-Replica Entry exists for this VNI\n"));
            return OSIX_FAILURE;
        }
#endif

        /* Get the Nve entry with current Nve-Index and VNI number if exists */
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac != OSIX_FALSE)
    {
        /* Check the Destination VM MAC address */
        if ((MEMCMP (pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                     zeroAddr,
                     VXLAN_ETHERNET_ADDR_SIZE) == 0) ||
            (VXLAN_IS_MAC_MULTICAST
             (pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac)) ||
            (MEMCMP (pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                     BcastMacAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VM_MAC_ADDR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Invalid VXLAN Destination VM MAC Address\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus != OSIX_FALSE)
    {
        /* Check the row Status of Nve Table Entry */
        if ((pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus <
             ACTIVE) ||
            (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus >
             DESTROY))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Nve Rowstatus\n"));
            return OSIX_FAILURE;
        }
        /* Check the conditions to add an entry of Nve Table */
        if (pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
            CREATE_AND_WAIT)
        {
            /* To add an Entry in Nve Table,
             * The respective Source VTEP entry should have been configured */
            VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
                pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
            pVxlanVtepEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                           (tRBElem *) & VxlanVtepEntry);
            if (pVxlanVtepEntry == NULL)
            {
                CLI_SET_ERR (CLI_VXLAN_NO_SRC_VTEP_IP);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Source IP Entry does not exist\n"));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsVxlanMCastTable
 Input       :  pu4ErrorCode
                pVxlanSetFsVxlanMCastEntry
                pVxlanIsSetFsVxlanMCastEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsVxlanMCastTable (UINT4 *pu4ErrorCode,
                               tVxlanFsVxlanMCastEntry *
                               pVxlanSetFsVxlanMCastEntry,
                               tVxlanIsSetFsVxlanMCastEntry *
                               pVxlanIsSetFsVxlanMCastEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tMacAddr            ZeroMacAddr;
#endif
    UINT4               u4MultAddr = 0;
    UINT4               u4Count = 0;
    INT4                i4Status = 0;
    tIp6Addr            Ip6Addr;
    INT4                i4FsVxlanEnable = 0;
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

#ifdef EVPN_VXLAN_WANTED
    MEMSET (&ZeroMacAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#endif
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
    /* VXLAN feature must be enabled, to test VXLAN-Multicast table Elements */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSVXLANMCASTTABLE - 3)) &&
        (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
         CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex != OSIX_FALSE)
    {
        /* Check the VXLAN Interface index value */
        if ((pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex <
             CFA_MIN_NVE_IF_INDEX) ||
            (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex >
             CFA_MAX_NVE_IF_INDEX))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_INT_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid NVE interface Index\n"));
            return OSIX_FAILURE;
        }
    }
    /* Check the VXLAN Identifier(VNI) value */
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber != OSIX_FALSE)
    {
        if ((pVxlanSetFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber <
             MIN_VALUE_VNI_IDENTIFIER) ||
            (pVxlanSetFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber >
             MAX_VALUE_VNI_IDENTIFIER))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VNI);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Identifier\n"));
            return OSIX_FAILURE;
        }
        /* VNI number should not be associated with
         * two Nve Interfaces */
        VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
            pVxlanSetFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;
        /* Get the MCast entry with current VNI number if exists */
        pVxlanMCastEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                           (tRBElem *) & VxlanMCastEntry, NULL);
        /* Error is to be returned, if there is an Nve entry existing with
         * current VNI number and with Different Nve Interface Index */
        if ((pVxlanMCastEntry != NULL) &&
            (pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber ==
             pVxlanSetFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber) &&
            (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex !=
             pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex))
        {
            CLI_SET_ERR (CLI_VXLAN_VNI_EXIST_NVE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "VXLAN Identifier is associated with another NVE\n"));
            return OSIX_FAILURE;
        }
    }

    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType !=
        OSIX_FALSE)
    {
        /* Check the VXLAN Multicast Group Address Type */
        if ((pVxlanSetFsVxlanMCastEntry->MibObject.
             i4FsVxlanMCastGroupAddressType != VXLAN_CLI_ADDRTYPE_IPV4) &&
            (pVxlanSetFsVxlanMCastEntry->MibObject.
             i4FsVxlanMCastGroupAddressType != VXLAN_CLI_ADDRTYPE_IPV6))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_ADDR_TYPE);
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Invalid VXLAN Multicast Address Type\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress != OSIX_FALSE)
    {
        /* Check for IPV4 address of VXLAN Multicast Group */
        if (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen
            == VXLAN_IP4_ADDR_LEN)
        {
            MEMCPY (&u4MultAddr,
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    au1FsVxlanMCastGroupAddress, sizeof (UINT4));
            u4MultAddr = OSIX_HTONL (u4MultAddr);
            if (VXLAN_IS_MCAST_LOOP_ADDR (u4MultAddr) == 0)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_IPV4_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN Multicast IPv4 Address\n"));
                return OSIX_FAILURE;
            }
        }
        /* Check for IPV6 address of VXLAN Multicast Group */
        else
        {
            MEMCPY (&Ip6Addr,
                    pVxlanSetFsVxlanMCastEntry->MibObject.
                    au1FsVxlanMCastGroupAddress, sizeof (tIp6Addr));
            i4Status = Ip6AddrType (&Ip6Addr);
            if (i4Status != ADDR6_MULTI)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_IPV6_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN Multicast IPV6 Address\n"));
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus != OSIX_FALSE)
    {
        /* Check the row Status of Mulricast Table Entry */
        if ((pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus <
             ACTIVE) ||
            (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus >
             DESTROY))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_MCAST_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Multicast Rowstatus\n"));
            return OSIX_FAILURE;
        }
        /* Check the conditions to add an entry of MCast table */
        if (pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
            CREATE_AND_WAIT)
        {
            /* To add an Entry in MCast Table,
             * The respective Source VTEP entry should have been configured */
            VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
                pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;
            pVxlanVtepEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                           (tRBElem *) & VxlanVtepEntry);
            if (pVxlanVtepEntry == NULL)
            {
                CLI_SET_ERR (CLI_VXLAN_NO_SRC_VTEP_IP);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Source IP Entry does not exist\n"));
                return OSIX_FAILURE;
            }
#ifdef EVPN_VXLAN_WANTED
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (pVxlanSetFsVxlanMCastEntry->MibObject.
                                         u4FsVxlanMCastVniNumber, ZeroMacAddr,
                                         pVxlanSetFsVxlanMCastEntry->MibObject.
                                         i4FsVxlanMCastNveIfIndex);
            if ((pVxlanNveEntry != NULL)
                && (pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
                    EVPN_CLI_ARP_SUPPRESS))
            {
                CLI_SET_ERR (CLI_EVPN_ARP_SUP_ENABLED);
                VXLAN_TRC ((VXLAN_CLI_TRC, "Arp Suppression is enabled.\n"));
                return OSIX_FAILURE;
            }
#endif
            /* Check for Ingress-Replica Entry if configured with this VNI number */
            VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
                pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;
            VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
                pVxlanSetFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;
            pVxlanInReplicaEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                               (tRBElem *) & VxlanInReplicaEntry, NULL);
            while (pVxlanInReplicaEntry != NULL)
            {
                if ((VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex
                     ==
                     pVxlanSetFsVxlanMCastEntry->MibObject.
                     i4FsVxlanMCastNveIfIndex)
                    && (VxlanInReplicaEntry.MibObject.
                        u4FsVxlanInReplicaVniNumber ==
                        pVxlanSetFsVxlanMCastEntry->MibObject.
                        u4FsVxlanMCastVniNumber))
                {
                    CLI_SET_ERR (CLI_VXLAN_INREPLICA_ROW_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    VXLAN_TRC ((VXLAN_CLI_TRC,
                                "Ingress-Replica Entry exists for this VNI\n"));
                    return OSIX_FAILURE;
                }

                pVxlanInReplicaEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanInReplicaTable,
                                   (tRBElem *) & VxlanInReplicaEntry, NULL);

            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsVxlanVniVlanMapTable
 Input       :  pu4ErrorCode
                pVxlanSetFsVxlanVniVlanMapEntry
                pVxlanIsSetFsVxlanVniVlanMapEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsVxlanVniVlanMapTable (UINT4 *pu4ErrorCode,
                                    tVxlanFsVxlanVniVlanMapEntry *
                                    pVxlanSetFsVxlanVniVlanMapEntry,
                                    tVxlanIsSetFsVxlanVniVlanMapEntry *
                                    pVxlanIsSetFsVxlanVniVlanMapEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    INT4                i4FsVxlanEnable = 0;
    UINT4               u4Count = 0;
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    /* VXLAN feature must be enabled, to test Vni-Vlan Map table Elements */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSVXLANVNIVLANMAPTABLE - 3)) &&
        (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus
         == CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber !=
        OSIX_FALSE)
    {
        /* Check the VXLAN Identifier value */
        if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             u4FsVxlanVniVlanMapVniNumber < MIN_VALUE_VNI_IDENTIFIER) ||
            (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             u4FsVxlanVniVlanMapVniNumber > MAX_VALUE_VNI_IDENTIFIER))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_INT_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Identifier\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent !=
        OSIX_FALSE)
    {
        /* Check the Packets Sent Statistics, it can only be cleared */
        if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktSent != VXLAN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_VXLAN_NO_PKT_SEND);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Packet send count should be updated only when a packet is sent out. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd !=
        OSIX_FALSE)
    {
        /* Check the Packets Recieved Statistics, it can only be cleared */
        if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktRcvd != VXLAN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_VXLAN_NO_PKT_RECEIVE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Packet Recieved count should be updated only when a packet is recieved. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd !=
        OSIX_FALSE)
    {
        /* Check the Packets Dropped Statistics, it can only be cleared */
        if (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktDrpd != VXLAN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_VXLAN_NO_PKT_DROP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Packet dropped count should be updated only when a packet is dropped. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus !=
        OSIX_FALSE)
    {
        /* Check the row Status of VniVlanMap Table Entry */
        if ((pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             i4FsVxlanVniVlanMapRowStatus < ACTIVE) ||
            (pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
             i4FsVxlanVniVlanMapRowStatus > DESTROY))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VNIVLAN_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN VniVlanMap Rowstatus\n"));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsVxlanInReplicaTable
 Input       :  pu4ErrorCode
                pVxlanSetFsVxlanInReplicaEntry
                pVxlanIsSetFsVxlanInReplicaEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsVxlanInReplicaTable (UINT4 *pu4ErrorCode,
                                   tVxlanFsVxlanInReplicaEntry *
                                   pVxlanSetFsVxlanInReplicaEntry,
                                   tVxlanIsSetFsVxlanInReplicaEntry *
                                   pVxlanIsSetFsVxlanInReplicaEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tMacAddr            ZeroMacAddr;
#endif
    UINT4               u4RemVtepAddr = 0;
    INT4                i4Status = 0;
    tIp6Addr            Ip6Addr;
    INT4                i4FsVxlanEnable = 0;
    UINT4               u4Count = 0;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanMibFsVxlanMCastEntry));
#ifdef EVPN_VXLAN_WANTED
    MEMSET (&ZeroMacAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#endif
    /* VXLAN feature must be enabled, to test VXLAN-InReplica table Elements */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSVXLANINREPLICATABLE - 3)) &&
        (pVxlanSetFsVxlanInReplicaEntry->MibObject.
         i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex !=
        OSIX_FALSE)
    {
        /* Check the VXLAN Interface index value */
        if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
             i4FsVxlanInReplicaNveIfIndex < CFA_MIN_NVE_IF_INDEX)
            || (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaNveIfIndex > CFA_MAX_NVE_IF_INDEX))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_INT_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid NVE interface Index\n"));
            return OSIX_FAILURE;
        }
    }
    /* Check the VXLAN Identifier(VNI) value */
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber !=
        OSIX_FALSE)
    {
        if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
             u4FsVxlanInReplicaVniNumber < MIN_VALUE_VNI_IDENTIFIER)
            || (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                u4FsVxlanInReplicaVniNumber > MAX_VALUE_VNI_IDENTIFIER))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_VNI);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN Identifier\n"));
            return OSIX_FAILURE;
        }
        /* VNI number should not be associated with
         * two Nve Interfaces */
        VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            u4FsVxlanInReplicaVniNumber;
        /* Get the Ingress-Replica entry with current VNI number if exists */
        pVxlanInReplicaEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) & VxlanInReplicaEntry, NULL);
        /* Error is to be returned, if there is an Ingress-Replica entry existing with
         * current VNI number and with Different Nve Interface Index */
        if ((pVxlanInReplicaEntry != NULL) &&
            (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber ==
             pVxlanSetFsVxlanInReplicaEntry->MibObject.
             u4FsVxlanInReplicaVniNumber)
            && (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex !=
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaNveIfIndex))
        {
            CLI_SET_ERR (CLI_VXLAN_VNI_EXIST_NVE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "VXLAN Id is associated with another NVE\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->
        bFsVxlanInReplicaRemoteVtepAddressType != OSIX_FALSE)
    {
        /* Check the VXLAN Remote VTEP Address Type */
        if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
             i4FsVxlanInReplicaRemoteVtepAddressType != VXLAN_CLI_ADDRTYPE_IPV4)
            && (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRemoteVtepAddressType !=
                VXLAN_CLI_ADDRTYPE_IPV6))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_REM_ADDR_TYPE);
            *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Invalid VXLAN Ingress Replica Remote VTEP Address Type\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress !=
        OSIX_FALSE)
    {
        /* Check for IPV4 address of VXLAN Remote VTEP */
        if (pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressLen == VXLAN_IP4_ADDR_LEN)
        {
            MEMCPY (&u4RemVtepAddr,
                    &(pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      au1FsVxlanInReplicaRemoteVtepAddress), sizeof (UINT4));
            u4RemVtepAddr = OSIX_NTOHL (u4RemVtepAddr);

            if ((VXLAN_IS_VALID_ADDRESS (u4RemVtepAddr) == VXLAN_FALSE) ||
                (NetIpv4IfIsOurAddress (u4RemVtepAddr) == NETIPV4_SUCCESS))
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV4_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN Ingress Replica Remote VTEP IPv4 Address\n"));
                return OSIX_FAILURE;
            }
        }

        /* Check for IPV6 address of VXLAN Remote VTEP */
        else
        {
            MEMCPY (&Ip6Addr,
                    pVxlanSetFsVxlanInReplicaEntry->MibObject.
                    au1FsVxlanInReplicaRemoteVtepAddress, sizeof (tIp6Addr));
            i4Status = Ip6AddrType (&Ip6Addr);
            if (i4Status != ADDR6_UNICAST)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_REM_IPV6_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Invalid VXLAN Ingress Replica remote VTEP IPV6 Address\n"));
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus !=
        OSIX_FALSE)
    {
        /* Check the row Status of Ingress-Replica Table Entry */
        if ((pVxlanSetFsVxlanInReplicaEntry->MibObject.
             i4FsVxlanInReplicaRowStatus < ACTIVE)
            || (pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRowStatus > DESTROY))
        {
            CLI_SET_ERR (CLI_VXLAN_INVALID_NVE_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Invalid VXLAN Ingress Replica Rowstatus\n"));
            return OSIX_FAILURE;
        }
        /* Check the conditions to add an entry of Ingress-Replica Table */
        if (pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus == CREATE_AND_WAIT)
        {
            /* To add an Entry in Ingress-Replica Table,
             * The respective Source VTEP entry should have been configured */
            VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaNveIfIndex;
            pVxlanVtepEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                           (tRBElem *) & VxlanVtepEntry);
            if (pVxlanVtepEntry == NULL)
            {
                CLI_SET_ERR (CLI_VXLAN_NO_SRC_VTEP_IP);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Source IP Entry does not exist\n"));
                return OSIX_FAILURE;
            }
#ifdef EVPN_VXLAN_WANTED
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (pVxlanSetFsVxlanInReplicaEntry->
                                         MibObject.u4FsVxlanInReplicaVniNumber,
                                         ZeroMacAddr,
                                         pVxlanSetFsVxlanInReplicaEntry->
                                         MibObject.
                                         i4FsVxlanInReplicaNveIfIndex);
            if ((pVxlanNveEntry != NULL)
                && (pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
                    EVPN_CLI_ARP_SUPPRESS))
            {
                CLI_SET_ERR (CLI_EVPN_ARP_SUP_ENABLED);
                VXLAN_TRC ((VXLAN_CLI_TRC, "Arp Suppression is enabled\n"));
                return OSIX_FAILURE;
            }
#endif
            /* Check for multicast Entry if configured with this VNI number */
            VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaNveIfIndex;
            VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                u4FsVxlanInReplicaVniNumber;
            pVxlanMCastEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                           (tRBElem *) & VxlanMCastEntry);
            if (pVxlanMCastEntry != NULL)
            {
                CLI_SET_ERR (CLI_VXLAN_MCAST_ROW_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "Multicast Entry exists for this VNI\n"));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanTestAllFsEvpnVxlanEviVniMapTable
 Input       :  pu4ErrorCode
                pVxlanSetFsEvpnVxlanEviVniMapEntry
                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsEvpnVxlanEviVniMapTable (UINT4 *pu4ErrorCode,
                                       tVxlanFsEvpnVxlanEviVniMapEntry *
                                       pVxlanSetFsEvpnVxlanEviVniMapEntry,
                                       tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                       pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{

    INT4                i4FsEvpnVxlanEnable = EVPN_ZERO;
    ULONG8              u8RdValue = EVPN_ZERO;
    UINT4               u4Count = 0;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnVxlanEviVniMapEntry = NULL;
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    tVxlanFsEvpnVxlanEviVniMapEntry * pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        VxlanGetFsEvpnVxlanEviVniMapTable (pVxlanSetFsEvpnVxlanEviVniMapEntry);

    /* EVPN feature must be enabled, to test Encapsulation mode */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        CLI_SET_ERR (CLI_EVPN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return OSIX_FAILURE;
    }

    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE - 3)) &&
        (EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
         CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    /* if the row does not exist and the user intends to set any other 
     * parameter other than the row status return failure  */
    if ((pVxlanFsEvpnVxlanEviVniMapEntry == NULL) &&
        (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
         bFsEvpnVxlanEviVniMapRowStatus == OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    /* Check for the VLAN-VNI map Entry */
    if (EvpnUtilIsSetVniMapEntry
        (EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry)) != OSIX_FALSE)
    {
        CLI_SET_ERR (CLI_EVPN_NO_VNI_MAP_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "No VNI-Vlan map Entry \n"));
        return OSIX_FAILURE;
    }
    /* Check the EVI and VNI value */
    if (((EVPN_P_BGP_EVI (pVxlanSetFsEvpnVxlanEviVniMapEntry) >= MIN_EVPN_VALUE
          && EVPN_P_BGP_EVI (pVxlanSetFsEvpnVxlanEviVniMapEntry) <=
          MAX_EVPN_VALUE)
         && (EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry) >=
             MIN_VNI_VALUE
             && EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry) <=
             MAX_VNI_VALUE)) != OSIX_TRUE)
    {
        CLI_SET_ERR (CLI_EVPN_INVALID_EVI_VNI);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid EVI/VNI number\n"));
        return OSIX_FAILURE;
    }
    /* Check if the same Vni is associated with other Evi already */
    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry);
    pEvpnVxlanEviVniMapEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       (tRBElem *) & EvpnVxlanEviVniMapEntry, NULL);
    while (pEvpnVxlanEviVniMapEntry != NULL)
    {
        if ((EVPN_P_BGP_VNI (pEvpnVxlanEviVniMapEntry) ==
             EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry)) &&
            (EVPN_P_BGP_EVI (pEvpnVxlanEviVniMapEntry) !=
             EVPN_P_BGP_EVI (pVxlanSetFsEvpnVxlanEviVniMapEntry)))
        {
            CLI_SET_ERR (CLI_EVPN_VNI_EXISTS);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Vni associated with another Evi\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;

        }
        pEvpnVxlanEviVniMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           (tRBElem *) pEvpnVxlanEviVniMapEntry, NULL);
    }
    /* Check if the Same Vni Number is used as L3VNI */
    if (VxlanVniIsL3Vni (EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanEviVniMapEntry))
        == OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_VXLAN_L3VNI_EXIST);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "This Vni is used as L3VNI\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD !=
        OSIX_FALSE)
    {
        /* Check the RD Length */
        if (!
            ((EVPN_P_BGP_RD_LEN (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
              EVPN_MAX_RD_LEN)
             || (EVPN_P_BGP_RD_LEN (pVxlanSetFsEvpnVxlanEviVniMapEntry) == 0)))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RD_LEN);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RD Length \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* Check for individual bytes */
        if (EVPN_P_BGP_RD_LEN (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
            EVPN_MAX_RD_LEN)
        {
            if ((EVPN_P_BGP_RD (pVxlanSetFsEvpnVxlanEviVniMapEntry)[0] >
                 EVPN_RD_TYPE_2)
                || (EVPN_P_BGP_RD (pVxlanSetFsEvpnVxlanEviVniMapEntry)[1] !=
                    EVPN_RD_SUBTYPE)
                ||
                (VxlanEvpnUtilValidateRDRT
                 (EVPN_P_BGP_RD (pVxlanSetFsEvpnVxlanEviVniMapEntry)) ==
                 VXLAN_FAILURE))
            {
                CLI_SET_ERR (CLI_EVPN_INVALID_RD_VALUE);
                VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RD Value \n"));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    /* Check the ESI value */
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI !=
        OSIX_FALSE)
    {
        /* Check the ESI Length */
        if (EVPN_P_BGP_ESI_LEN (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
            EVPN_MAX_ESI_LEN)
        {
            if (EVPN_P_BGP_ESI (pVxlanSetFsEvpnVxlanEviVniMapEntry)[0] != 0)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_ESI_TYPE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid ESI Type\n"));
                return OSIX_FAILURE;
            }
            MEMCPY (&u8RdValue, &EVPN_P_BGP_ESI (pVxlanSetFsEvpnVxlanEviVniMapEntry), 8);    /*EVPN_MAX_ESI_LEN); */
            if (EVPN_ZERO == u8RdValue)
            {
                CLI_SET_ERR (CLI_VXLAN_INVALID_ESI_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid ESI Value\n"));
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance !=
        OSIX_FALSE)
    {
        /* Check the LoadBalance value */
        if ((EVPN_P_BGP_LOAD_BALANCE (pVxlanSetFsEvpnVxlanEviVniMapEntry) <
             EVPN_CLI_LB_ENABLE)
            || (EVPN_P_BGP_LOAD_BALANCE (pVxlanSetFsEvpnVxlanEviVniMapEntry) >
                EVPN_CLI_LB_DISABLE))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_LB_INPUT);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Invalid input for enabling/disabling LoadBalance\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts !=
        OSIX_FALSE)
    {
        /* Check the Packets Sent Statistics, it can only be cleared */
        if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapSentPkts != EVPN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_SEND);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Send count should be updated only when a packet "
                        "is sent out. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts !=
        OSIX_FALSE)
    {
        /* Check the Packets Recieved Statistics, it can only be cleared */
        if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapRcvdPkts != EVPN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_RECEIVE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Recieved count should be updated only when a "
                        "packet is recieved. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
        bFsEvpnVxlanEviVniMapDroppedPkts != OSIX_FALSE)
    {
        /* Check the Packets Dropped Statistics, it can only be cleared */
        if (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapDroppedPkts != EVPN_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_DROP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Dropped count should be updated only when a "
                        "packet is dropped. So it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus !=
        OSIX_FALSE)
    {
        /* Check the row Status of EviVniMap Table Entry */
        if ((EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) < ACTIVE) ||
            (EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) > DESTROY))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_EVIVNI_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid Evi-Vni Map Rowstatus\n"));
            return OSIX_FAILURE;
        }
        if ((EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
             NOT_READY)
            || (EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
                NOT_IN_SERVICE)
            || (EVPN_P_BGP_STATUS (pVxlanSetFsEvpnVxlanEviVniMapEntry) ==
                DESTROY))
        {
            if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsEvpnVxlanBgpRTTable
 Input       :  pu4ErrorCode
                pVxlanSetFsEvpnVxlanBgpRTEntry
                pVxlanIsSetFsEvpnVxlanBgpRTEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsEvpnVxlanBgpRTTable (UINT4 *pu4ErrorCode,
                                   tVxlanFsEvpnVxlanBgpRTEntry *
                                   pVxlanSetFsEvpnVxlanBgpRTEntry,
                                   tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                   pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry *pRtEntry = NULL;
    UINT4               u4Count = 0;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSEVPNVXLANBGPRTTABLE - 3)) &&
        (EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) ==
         CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (EvpnUtilIsSetVniMapEntry
        (EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanBgpRTEntry)) != OSIX_FALSE)
    {
        CLI_SET_ERR (CLI_EVPN_NO_VNI_MAP_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "No VNI-Vlan map Entry \n"));
        return OSIX_FAILURE;
    }

    /* The RT Type will not be passed when RT is deleted from CLI
     * Deletion is based on the value of RT, Thus this check is valid */
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType != OSIX_FALSE)
    {
        if ((EVPN_P_BGP_RT_TYPE (pVxlanSetFsEvpnVxlanBgpRTEntry) <
             EVPN_BGP_RT_IMPORT)
            || (EVPN_P_BGP_RT_TYPE (pVxlanSetFsEvpnVxlanBgpRTEntry) >=
                EVPN_BGP_MAX_RT_TYPE))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_TYPE);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Type \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }

    if (EVPN_P_BGP_RT_INDEX (pVxlanSetFsEvpnVxlanBgpRTEntry) >
        EVPN_MAX_NUM_RT_PER_VRF)
    {
        CLI_SET_ERR (CLI_EVPN_INVALID_RT_INDEX);
        VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Index \n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }

    pVxlanFsEvpnVxlanBgpRTEntry =
        VxlanGetFsEvpnVxlanBgpRTTable (pVxlanSetFsEvpnVxlanBgpRTEntry);

    /* if the row does not exist and the user intends to set any other parameter 
     * other than the row status return failure  */
    if ((pVxlanFsEvpnVxlanBgpRTEntry == NULL) &&
        (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
         OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT != OSIX_FALSE)
    {
        /* Check the RD Length */
        if (EVPN_P_BGP_RT_LEN (pVxlanSetFsEvpnVxlanBgpRTEntry) !=
            EVPN_MAX_RT_LEN)
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_LEN);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Length\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* Check for individual bytes */
        if ((EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry)[0] > EVPN_RT_TYPE_2)
            || (EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry)[1] !=
                EVPN_RT_SUBTYPE)
            ||
            (VxlanEvpnUtilValidateRDRT
             (EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry)) == VXLAN_FAILURE))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_VALUE);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Value\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* RT cannot be changed when the row is ACTIVE */
        if (pVxlanFsEvpnVxlanBgpRTEntry != NULL)
        {
            if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
                ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        /* From the CLI: RT will be set in two scnearios - ACTIVE and DESTROY
         * Below check is not required while destroying the row
         * From SNMP, below check will not hit since the row status will be 0
         * Only RT will be set in setEntry structure  */
        if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) !=
            DESTROY)
        {
            /* Call function from EVPN-BGP to check for RT validity */
            if (EvpnGetRTEntryFromValue
                (EVPN_P_BGP_EVI (pVxlanSetFsEvpnVxlanBgpRTEntry),
                 EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanBgpRTEntry),
                 EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry)) != NULL)
            {
                CLI_SET_ERR (CLI_EVPN_RT_DUPLICATE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
        OSIX_TRUE)
    {
        /* Check the row Status of BGP RT Table Entry */
        if ((EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) < ACTIVE)
            || (EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) >
                DESTROY))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_ROW_STATUS);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Rowstatus\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) ==
            DESTROY)
        {
            /* Destroy from CLI -- No indices will be provided, rather RT vaule will be provided */
            if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT !=
                OSIX_FALSE)
            {
                pRtEntry = EvpnGetRTEntryFromValue
                    (EVPN_P_BGP_EVI (pVxlanSetFsEvpnVxlanBgpRTEntry),
                     EVPN_P_BGP_VNI (pVxlanSetFsEvpnVxlanBgpRTEntry),
                     EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry));
                if (pRtEntry == NULL)
                {
                    CLI_SET_ERR (CLI_EVPN_NO_SUCH_RT);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                /* Fill in the indices here because SetAll will delete on the basis on indices */
                else
                {
                    EVPN_P_BGP_RT_INDEX (pVxlanSetFsEvpnVxlanBgpRTEntry) =
                        EVPN_P_BGP_RT_INDEX (pRtEntry);
                    EVPN_P_BGP_RT_TYPE (pVxlanSetFsEvpnVxlanBgpRTEntry) =
                        EVPN_P_BGP_RT_TYPE (pRtEntry);
                    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType =
                        OSIX_TRUE;
                    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex =
                        OSIX_TRUE;
                }
            }
        }
        if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) ==
            NOT_IN_SERVICE
            || EVPN_P_BGP_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanBgpRTEntry) ==
            NOT_READY)
        {
            if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto != OSIX_FALSE)
    {
        if (STRNCMP (EVPN_P_BGP_RT (pVxlanSetFsEvpnVxlanBgpRTEntry), "auto", 4)
            == 0)
        {
            EvpnBgpUtlGetDefaultRtValue (EVPN_P_BGP_EVI
                                         (pVxlanSetFsEvpnVxlanBgpRTEntry),
                                         EVPN_P_BGP_VNI
                                         (pVxlanSetFsEvpnVxlanBgpRTEntry),
                                         EVPN_P_BGP_RT_INDEX
                                         (pVxlanSetFsEvpnVxlanBgpRTEntry),
                                         EVPN_P_BGP_RT_TYPE
                                         (pVxlanSetFsEvpnVxlanBgpRTEntry),
                                         EVPN_P_BGP_RT
                                         (pVxlanSetFsEvpnVxlanBgpRTEntry));
            EVPN_P_BGP_RT_LEN (pVxlanSetFsEvpnVxlanBgpRTEntry) =
                EVPN_MAX_RT_LEN;
            EVPN_P_BGP_RT_AUTO (pVxlanSetFsEvpnVxlanBgpRTEntry) = OSIX_TRUE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestFsEvpnVxlanEnable
 Input       :  pu4ErrorCode
                i4FsEvpnVxlanEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanTestFsEvpnVxlanEnable (UINT4 *pu4ErrorCode, INT4 i4FsEvpnVxlanEnable)
{
    INT4                i4FsVxlanEnable = 0;

    /* VXLAN feature must be enabled, to test EVPN feature */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }

    if ((i4FsEvpnVxlanEnable == EVPN_CLI_STATUS_ENABLE) ||
        (i4FsEvpnVxlanEnable == EVPN_CLI_STATUS_DISABLE))
    {
        return OSIX_SUCCESS;
    }
    CLI_SET_ERR (CLI_EVPN_INVALID_ENABLE_INPUT);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid input for enabling/disabling EVPN\n"));

    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  VxlanTestFsEvpnAnycastGwMac
 Input       :  pu4ErrorCode
                FsEvpnAnycastGwMac 
 Description :  This Routine will Test
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/ INT4
VxlanTestFsEvpnAnycastGwMac (UINT4 *pu4ErrorCode, tMacAddr FsEvpnAnycastGwMac)
{
    INT4                i4FsEvpnVxlanEnable = EVPN_ZERO;
    tMacAddr            zeroAddr;
    tMacAddr            BcastMacAddr;

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (BcastMacAddr, 0xff, VXLAN_ETHERNET_ADDR_SIZE);
    /* EVPN feature must be enabled, to test Encapsulation mode */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        CLI_SET_ERR (CLI_EVPN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return OSIX_FAILURE;
    }
    if ((MEMCMP (FsEvpnAnycastGwMac,
                 zeroAddr,
                 VXLAN_ETHERNET_ADDR_SIZE) == 0) ||
        (VXLAN_IS_MAC_MULTICAST
         (FsEvpnAnycastGwMac)) ||
        (MEMCMP (FsEvpnAnycastGwMac,
                 BcastMacAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0))
    {
        CLI_SET_ERR (CLI_VXLAN_INVALID_VM_MAC_ADDR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC,
                    "Invalid EVPN Anycast Gateway MAC Address\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsEvpnVxlanVrfTable
 Input       :  pu4ErrorCode
                pVxlanSetFsEvpnVxlanVrfEntry
                pVxlanIsSetFsEvpnVxlanVrfEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsEvpnVxlanVrfTable (UINT4 *pu4ErrorCode,
                                 tVxlanFsEvpnVxlanVrfEntry *
                                 pVxlanSetFsEvpnVxlanVrfEntry,
                                 tVxlanIsSetFsEvpnVxlanVrfEntry *
                                 pVxlanIsSetFsEvpnVxlanVrfEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{

    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry VrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVrfEntry = NULL;
    INT4                i4FsEvpnVxlanEnable = EVPN_ZERO;
    UINT4               u4Count = 0;
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    MEMSET (&VrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    MEMCPY (EVPN_VRF_NAME (VrfEntry),
            EVPN_P_VRF_NAME (pVxlanSetFsEvpnVxlanVrfEntry),
            EVPN_P_VRF_NAME_LEN (pVxlanSetFsEvpnVxlanVrfEntry));
    EVPN_VRF_NAME_LEN (VrfEntry) =
        EVPN_P_VRF_NAME_LEN (pVxlanSetFsEvpnVxlanVrfEntry);
    EVPN_VRF_VNI (VrfEntry) = EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry);

    pVrfEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           (tRBElem *) & VrfEntry);

    /* EVPN feature must be enabled, to test Encapsulation mode */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        CLI_SET_ERR (CLI_EVPN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return OSIX_FAILURE;
    }
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSEVPNVXLANVRFTABLE - 3)) &&
        (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus ==
         CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    /* Check for the VLAN-VNI map Entry */
    if ((EvpnUtilIsSetVniMapEntry
         (EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry))) != OSIX_FALSE)
    {
        CLI_SET_ERR (CLI_EVPN_NO_VNI_MAP_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        VXLAN_TRC ((VXLAN_EVPN_TRC, "No VNI-Vlan map Entry \n"));
        return OSIX_FAILURE;
    }

    VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber =
        EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry);
    pVxlanNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       (tRBElem *) & VxlanNveEntry, NULL);
    while (pVxlanNveEntry != NULL)
    {
        if ((EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry) ==
             pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber)
            && (pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
                EVPN_CLI_ARP_SUPPRESS))
        {
            CLI_SET_ERR (CLI_VXLAN__ARP_SUPPRESSION_MAP_L3VNI_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "VNI is mapped with Suppress-ARP \n"));
            return OSIX_FAILURE;
        }
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) pVxlanNveEntry, NULL);
    }

    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus != OSIX_FALSE)
    {
        /* Check the row Status */
        if ((EVPN_P_VRF_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfEntry) < ACTIVE) ||
            (EVPN_P_VRF_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfEntry) > DESTROY))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_EVIVNI_ROW_STATUS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid Vrf-Vni Rowstatus\n"));
            return OSIX_FAILURE;
        }
        if ((EVPN_P_VRF_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfEntry) == NOT_READY)
            || (EVPN_P_VRF_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfEntry) ==
                NOT_IN_SERVICE)
            || (EVPN_P_VRF_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfEntry) ==
                DESTROY))
        {
            if (pVrfEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }

    }

    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber != OSIX_FALSE)
    {
        pVxlanFsEvpnVxlanVrfEntry =
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
        while (pVxlanFsEvpnVxlanVrfEntry != NULL)
        {
            /* Check if Vni is associated with another VRF */
            if ((pVxlanFsEvpnVxlanVrfEntry->MibObject.
                 u4FsEvpnVxlanVrfVniNumber ==
                 pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                 u4FsEvpnVxlanVrfVniNumber)
                &&
                (MEMCMP
                 (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName,
                  pVxlanSetFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName,
                  pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                  i4FsEvpnVxlanVrfNameLen) != 0))
            {
                /* Vni already mapped with a different Vrf */
                CLI_SET_ERR (CLI_VXLAN_VNI_EXIST_VRF);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC,
                            "VXLAN Id is associated with another VRF\n"));
                return OSIX_FAILURE;

            }
            /* Check if Vrf is associated witha Vni Already */
            if ((EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) ==
                 EVPN_P_VRF_NAME_LEN (pVxlanSetFsEvpnVxlanVrfEntry)) &&
                (MEMCMP (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
                         EVPN_P_VRF_NAME (pVxlanSetFsEvpnVxlanVrfEntry),
                         EVPN_P_VRF_NAME_LEN (pVxlanSetFsEvpnVxlanVrfEntry)) ==
                 0)
                && (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) !=
                    EVPN_P_VRF_VNI (pVxlanSetFsEvpnVxlanVrfEntry)))
            {
                /* Vrf is already mapped with a Vni */
                CLI_SET_ERR (CLI_VXLAN_VRF_EXIST_VNI);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VXLAN_TRC ((VXLAN_CLI_TRC, "Vrf is mapped with a Vni\n"));
                return OSIX_FAILURE;
            }

            pVxlanFsEvpnVxlanVrfEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                               (tRBElem *) pVxlanFsEvpnVxlanVrfEntry, NULL);
        }

        /* Check if the Same Vni Number is used as L2VNI */
        if (VxlanVniIsL2Vni (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                             u4FsEvpnVxlanVrfVniNumber) == OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_VXLAN_L2VNI_EXIST);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_CLI_TRC, "This Vni is used as L2VNI\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD != OSIX_FALSE)
    {
        /* Check the RD Length */
        if (!
            ((EVPN_P_VRF_RD_LEN (pVxlanSetFsEvpnVxlanVrfEntry) ==
              EVPN_MAX_RD_LEN)
             || (EVPN_P_VRF_RD_LEN (pVxlanSetFsEvpnVxlanVrfEntry) == 0)))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RD_LEN);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RD Length \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* Check for individual bytes */
        if (EVPN_P_VRF_RD_LEN (pVxlanSetFsEvpnVxlanVrfEntry) == EVPN_MAX_RD_LEN)
        {
            if ((EVPN_P_VRF_RD (pVxlanSetFsEvpnVxlanVrfEntry)[0] >
                 EVPN_RD_TYPE_2)
                || (EVPN_P_VRF_RD (pVxlanSetFsEvpnVxlanVrfEntry)[1] !=
                    EVPN_RD_SUBTYPE)
                ||
                (VxlanEvpnUtilValidateRDRT
                 (EVPN_P_VRF_RD (pVxlanSetFsEvpnVxlanVrfEntry)) ==
                 VXLAN_FAILURE))
            {
                CLI_SET_ERR (CLI_EVPN_INVALID_RD_VALUE);
                VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RD Value \n"));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts !=
        OSIX_FALSE)
    {
        /* Check the Packets Sent Statistics, it can only be cleared */
        if (EVPN_P_VRF_SENT_PKTS (pVxlanSetFsEvpnVxlanVrfEntry)
            != VXLAN_VRF_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_SEND);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Send count should be updated only when a packet "
                        "is sent out. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts !=
        OSIX_FALSE)
    {
        /* Check the Packets Recieved Statistics, it can only be cleared */
        if (EVPN_P_VRF_RCVD_PKTS (pVxlanSetFsEvpnVxlanVrfEntry)
            != VXLAN_VRF_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_RECEIVE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Recieved count should be updated only when a "
                        "packet is recieved. So, it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts !=
        OSIX_FALSE)
    {
        /* Check the Packets Dropped Statistics, it can only be cleared */
        if (EVPN_P_VRF_DRP_PKTS (pVxlanSetFsEvpnVxlanVrfEntry)
            != VXLAN_VRF_CLI_VNI_CLEAR_COUNTER)
        {
            CLI_SET_ERR (CLI_EVPN_NO_PKT_DROP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Packet Dropped count should be updated only when a "
                        "packet is dropped. So it can only be cleared using command\n"));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsEvpnVxlanVrfRTTable
 Input       :  pu4ErrorCode
                pVxlanSetFsEvpnVxlanVrfRTEntry
                pVxlanIsSetFsEvpnVxlanVrfRTEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsEvpnVxlanVrfRTTable (UINT4 *pu4ErrorCode,
                                   tVxlanFsEvpnVxlanVrfRTEntry *
                                   pVxlanSetFsEvpnVxlanVrfRTEntry,
                                   tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                   pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pRtEntry = NULL;
    UINT4               u4Count = 0;
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable, &u4Count);

    if ((u4Count == (MAX_VXLAN_FSEVPNVXLANVRFRTTABLE - 3)) &&
        (pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
         i4FsEvpnVxlanVrfRTRowStatus == CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_VXLAN_MAX_COUNT_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "Max Count is reached\n"));
        return OSIX_FAILURE;
    }

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType != OSIX_FALSE)
    {
        if ((EVPN_P_VRF_RT_TYPE (pVxlanSetFsEvpnVxlanVrfRTEntry) <
             EVPN_BGP_RT_IMPORT)
            || (EVPN_P_VRF_RT_TYPE (pVxlanSetFsEvpnVxlanVrfRTEntry) >=
                EVPN_BGP_MAX_RT_TYPE))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_TYPE);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Type \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex != OSIX_FALSE)
    {
        if (EVPN_P_VRF_RT_INDEX (pVxlanSetFsEvpnVxlanVrfRTEntry) >
            EVPN_MAX_NUM_RT_PER_VRF)
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_INDEX);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Index \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT != OSIX_FALSE)
    {
        /* Check the RT Length */
        if (EVPN_P_VRF_RT_LEN (pVxlanSetFsEvpnVxlanVrfRTEntry) !=
            EVPN_MAX_RT_LEN)
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_LEN);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Length\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* Check for individual bytes */
        if ((EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry)[0] > EVPN_RT_TYPE_2)
            || (EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry)[1] !=
                EVPN_RT_SUBTYPE)
            ||
            (VxlanEvpnUtilValidateRDRT
             (EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry)) == VXLAN_FAILURE))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_VALUE);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Value\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        pVxlanFsEvpnVxlanVrfRTEntry =
            VxlanGetFsEvpnVxlanVrfRTTable (pVxlanSetFsEvpnVxlanVrfRTEntry);
        /* RT cannot be changed when the row is ACTIVE */
        if (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
        {
            if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
                ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        /* From the CLI: RT will be set in two scnearios - ACTIVE and DESTROY
         * Below check is not required while destroying the row
         * From SNMP, below check will not hit since the row status will be 0
         * Only RT will be set in setEntry structure  */

        if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) !=
            DESTROY)
        {
            /* Call function from EVPN-BGP to check for RT validity */
            if (EvpnGetVrfRTEntryFromValue
                (EVPN_P_VRF_NAME (pVxlanSetFsEvpnVxlanVrfRTEntry),
                 EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry)) != NULL)
            {
                CLI_SET_ERR (CLI_EVPN_RT_DUPLICATE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus ==
        OSIX_TRUE)
    {
        /* Check the row Status of BGP RT Table Entry */
        if ((EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) < ACTIVE)
            || (EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) >
                DESTROY))
        {
            CLI_SET_ERR (CLI_EVPN_INVALID_RT_ROW_STATUS);
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Invalid RT Rowstatus\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) ==
            DESTROY)
        {
            /* Destroy from CLI -- No indices will be provided, rather RT vaule will be provided */
            if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT !=
                OSIX_FALSE)
            {
                pRtEntry = EvpnGetVrfRTEntryFromValue
                    (EVPN_P_VRF_NAME (pVxlanSetFsEvpnVxlanVrfRTEntry),
                     EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry));
                if (pRtEntry == NULL)
                {
                    CLI_SET_ERR (CLI_EVPN_NO_SUCH_RT);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                /* Fill in the indices here because SetAll will delete on the basis on indices */
                else
                {
                    EVPN_P_VRF_RT_INDEX (pVxlanSetFsEvpnVxlanVrfRTEntry) =
                        EVPN_P_VRF_RT_INDEX (pRtEntry);
                    EVPN_P_VRF_RT_TYPE (pVxlanSetFsEvpnVxlanVrfRTEntry) =
                        EVPN_P_VRF_RT_TYPE (pRtEntry);
                    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType =
                        OSIX_TRUE;
                    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex =
                        OSIX_TRUE;
                }
            }
        }
        if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) ==
            NOT_IN_SERVICE
            || EVPN_P_VRF_RT_ROW_STATUS (pVxlanSetFsEvpnVxlanVrfRTEntry) ==
            NOT_READY)
        {
            if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if (STRNCMP (EVPN_P_VRF_RT (pVxlanSetFsEvpnVxlanVrfRTEntry), "auto", 4)
            == 0)
        {
            EvpnVrfUtlGetDefaultRtValue (EVPN_P_VRF_NAME
                                         (pVxlanSetFsEvpnVxlanVrfRTEntry),
                                         EVPN_P_VRF_VNI
                                         (pVxlanSetFsEvpnVxlanVrfRTEntry),
                                         EVPN_P_VRF_RT_INDEX
                                         (pVxlanSetFsEvpnVxlanVrfRTEntry),
                                         EVPN_P_VRF_RT_TYPE
                                         (pVxlanSetFsEvpnVxlanVrfRTEntry),
                                         EVPN_P_VRF_RT
                                         (pVxlanSetFsEvpnVxlanVrfRTEntry));
            EVPN_P_VRF_RT_LEN (pVxlanSetFsEvpnVxlanVrfRTEntry) =
                EVPN_MAX_RT_LEN;
            EVPN_P_VRF_RT_AUTO (pVxlanSetFsEvpnVxlanVrfRTEntry) = OSIX_TRUE;
        }

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestAllFsEvpnVxlanMultihomedPeerTable
 Input       :  pu4ErrorCode
                pVxlanSetFsEvpnVxlanMultihomedPeerTable
                pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanTestAllFsEvpnVxlanMultihomedPeerTable (UINT4 *pu4ErrorCode,
                                            tVxlanFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                                            tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable,
                                            INT4 i4RowStatusLogic,
                                            INT4 i4RowCreateOption)
{
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pVxlanSetFsEvpnVxlanMultihomedPeerTable);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}
#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  VxlanTestFsVxlanEnable
 Input       :  pu4ErrorCode
                i4FsVxlanEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanTestFsVxlanEnable (UINT4 *pu4ErrorCode, INT4 i4FsVxlanEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
#ifdef EVPN_VXLAN_WANTED
    INT4                i4FsEvpnEnable = 0;

    if (i4FsVxlanEnable == VXLAN_CLI_STATUS_DISABLE)
    {
        /* EVPN feature must be disabled, before VXLAN disable */
        VxlanGetFsEvpnVxlanEnable (&i4FsEvpnEnable);
        if (i4FsEvpnEnable == EVPN_ENABLED)
        {
            CLI_SET_ERR (CLI_EVPN_NOT_DISABLED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "EVPN is enabled over VXLAN. Disable EVPN before disabling VXLAN\n"));
            return OSIX_FAILURE;
        }
    }
#endif /* EVPN_VXLAN_WANTED */

    if ((i4FsVxlanEnable == VXLAN_CLI_STATUS_ENABLE) ||
        (i4FsVxlanEnable == VXLAN_CLI_STATUS_DISABLE))
    {
        return OSIX_SUCCESS;
    }
    CLI_SET_ERR (CLI_VXLAN_INVALID_ENABLE_INPUT);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid input for enabling/disabling VXLAN\n"));
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  VxlanTestFsVxlanUdpPort
 Input       :  pu4ErrorCode
                u4FsVxlanUdpPort
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanTestFsVxlanUdpPort (UINT4 *pu4ErrorCode, UINT4 u4FsVxlanUdpPort)
{
    INT4                i4FsVxlanEnable = 0;
    /* VXLAN feature must be enabled, to test UDP-port Element */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }

    /* Check the VXLAN UDP port value */
    if ((u4FsVxlanUdpPort >= MIN_VALUE_UDPPORT) &&
        (u4FsVxlanUdpPort <= MAX_VALUE_UDPPORT))
    {
        return OSIX_SUCCESS;
    }
    CLI_SET_ERR (CLI_VXLAN_INVALID_UDP_PORT);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    VXLAN_TRC ((VXLAN_CLI_TRC, "Invalid VXLAN UDP port number\n"));
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  VxlanTestFsVxlanTraceOption
 Input       :  pu4ErrorCode
                i4FsVxlanTraceOption
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanTestFsVxlanTraceOption (UINT4 *pu4ErrorCode, UINT4 u4FsVxlanTraceOption)
{
    INT4                i4FsVxlanEnable = 0;
    /* VXLAN feature must be enabled, to test VXLAN trace option Element */
    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CLI_SET_ERR (CLI_VXLAN_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        VXLAN_TRC ((VXLAN_CLI_TRC, "VXLAN is not enabled\n"));
        return OSIX_FAILURE;
    }

    /* Check the VXLAN Debug trace Value */
    if (u4FsVxlanTraceOption != VXLAN_CLI_TRACE_ALL)
    {
        if ((u4FsVxlanTraceOption & VXLAN_CLI_TRACE_CRITICAL) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_UTILITIES) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_MGMT) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_ENTRY_EXIT) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_FAILURES) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_MEMORY) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_PKT) ||
            (u4FsVxlanTraceOption & VXLAN_CLI_TRACE_EVPN))
        {
            return OSIX_SUCCESS;
        }
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanTestFsVxlanNotificationCntl
 Input       :  pu4ErrorCode
                i4FsVxlanNotificationCntl
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanTestFsVxlanNotificationCntl (UINT4 *pu4ErrorCode,
                                  INT4 i4FsVxlanNotificationCntl)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsVxlanNotificationCntl);
    return OSIX_SUCCESS;
}
