/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: vxnpwr.c,v 1.19 2018/01/05 15:26:48 siva Exp $
 *
 * Description: This file contains the VXLAN NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#include "vxinc.h"
#include "vxlannp.h"

VOID                VxlanGetIpv6McastMaddr (tIp6Addr * pAddr6, UINT1 *pMacaddr);

VOID                VxlanGetIpv4McastMaddr (UINT4 u4Mcast, UINT1 *pMacaddr);

INT4                VxlanHwUpdateAllVlanEgrPorts (UINT4 u4VlanIfIndex,
                                                  tVxlanFsVxlanNveEntry *
                                                  pVxlanNveEntry,
                                                  tVxlanHwInfo * pVxlanHwInfo,
                                                  UINT1 u1Flag);

extern INT4 CfaGetIfIpPortVlanId PROTO ((UINT4 u4IfIndex, INT4 *PortVlanId));
/***************************************************************************
 * Function Name             : VxlanHwInit
 *
 * Description               : This function init/deinit VXLAN  in H/W
 *
 * Input                     : u4FsVxlanUdpPort - VXLAN Udp port number.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanHwInit (UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u1Flag == VXLAN_HW_ADD)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_INIT;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_INIT;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u1Flag);
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanHwInit: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateUdpPort
 *
 * Description               : This function sets VXLAN udp port number in H/W
 *
 * Input                     : u4FsVxlanUdpPort - VXLAN Udp port number.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanHwUpdateUdpPort (UINT4 u4FsVxlanUdpPort, UINT1 u1Flag)
{

#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u1Flag == VXLAN_HW_ADD)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_UDP_PORT;
        pVxlanNpModInfo->unHwParam.u4VxlanUdpPortNo = u4FsVxlanUdpPort;

        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_UDP_PORT;
        pVxlanNpModInfo->unHwParam.u4VxlanUdpPortNo = u4FsVxlanUdpPort;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (u4FsVxlanUdpPort);
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateUdpPort: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateNveDatabase
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanNveEntry - VXLAN nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanHwUpdateNveDatabase (tVxlanFsVxlanNveEntry * pVxlanNveEntry, UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT4               u4IfaceIndex = 0;
    UINT1               au1DstMac[MAC_ADDR_LEN];
#if defined (SW_LEARNING)
    UINT1               u1Status = 0;
#endif
#ifdef NPAPI_WANTED
    tMacAddr            zeroAddr;
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    UINT1               u1IfAdminStatus = 0;
#endif
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateNveDatabase: Entry\n"));
    if (((u1Flag == VXLAN_HW_DEL) && (pVxlanNveEntry->bHwSet == FALSE)) ||
        ((u1Flag == VXLAN_HW_ADD) && (pVxlanNveEntry->bHwSet == TRUE)))
    {
        return VXLAN_SUCCESS;
    }
#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        /* HW programming for NVE database should not happen when nve interface
         * is down */
        CfaApiGetIfAdminStatus ((UINT4)
                                (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex),
                                &u1IfAdminStatus);
        if (u1IfAdminStatus != CFA_IF_UP)
        {
            return VXLAN_SUCCESS;
        }
    }

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    if (pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType == 0)
    {
#ifdef EVPN_VXLAN_WANTED
        if (pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
            EVPN_CLI_ARP_SUPPRESS)
        {
            VxlanHwUpdateArpSuppression (pVxlanNveEntry, u1Flag);
        }
#endif
        return VXLAN_SUCCESS;
    }

#ifndef NPAPI_WANTED
#ifdef EVPN_VXLAN_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        VlanAddEvpnFdbEntry (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                             (UINT2) pVxlanNveEntry->i4VlanId,
                             pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);
    }
    else
    {
        VlanRemoveFdbEntry (0, (UINT2) pVxlanNveEntry->i4VlanId,
                            pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);
    }
#endif
#endif

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (au1DstMac, 0, MAC_ADDR_LEN);

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
        pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

    if (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex > CFA_MAX_NVE_IF_INDEX)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
            (INT4) pVxlanNveEntry->u4OrgNveIfIndex;
    }

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
        pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
        (UINT4) pVxlanNveEntry->i4VlanId;

    if ((pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
         VXLAN_STRG_TYPE_VOL)
        && (pVxlanNveEntry->i4FsVxlanNveEvpnMacType != VXLAN_NVE_EVPN_MAC))
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = VXLAN_NVE_DYNAMIC_MAC;
    }
    else
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = VXLAN_NVE_STATIC_MAC;
    }

    if ((pVxlanNveEntry->bIngOrMcastZeroEntryPresent == TRUE) &&
        (pVxlanNveEntry->i4FsVxlanNveEvpnMacType == VXLAN_NVE_EVPN_MAC) &&
        (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
         VXLAN_STRG_TYPE_VOL))
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = VXLAN_NVE_DYNAMIC_MAC;
    }

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
        pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
        pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
            &(pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress),
            pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1RemoteVtepAddress),
            &(pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress),
            pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac),
            &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);

    if (u1Flag == VXLAN_HW_DEL)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel =
            pVxlanNveEntry->u4OutVlanId;

        pVxlanNvePortEntry =
            VxlanGetNvePortEntry (pVxlanNveEntry->MibObject.
                                  au1FsVxlanNveRemoteVtepAddress,
                                  pVxlanNveEntry->MibObject.
                                  i4FsVxlanNveRemoteVtepAddressLen);
        if (pVxlanNvePortEntry != NULL)
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
                pVxlanNvePortEntry->u4NetworkPort;
        }
        else
        {
            VXLAN_TRC ((VXLAN_ALL_TRC,
                        "No entry in NVE port entry table for deletion\r\n"));
            return VXLAN_SUCCESS;
        }
    }
    else
    {
        pVxlanNvePortEntry =
            VxlanCheckRouteAndUpdateTunnelMapTable (pVxlanNveEntry->MibObject.
                                                    au1FsVxlanNveRemoteVtepAddress,
                                                    pVxlanNveEntry->MibObject.
                                                    i4FsVxlanNveVtepAddressLen,
                                                    pVxlanNveEntry->
                                                    u4VxlanTunnelIfIndex);

        if (pVxlanNvePortEntry == NULL)
        {
            VXLAN_TRC ((VXLAN_ALL_TRC,
                        "No entry in NVE port entry table for \r\n"));
            return VXLAN_SUCCESS;
        }
        pVxlanNveEntry->u4OutVlanId = pVxlanNvePortEntry->u4VlanIdOutTunnel;
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel =
            pVxlanNveEntry->u4OutVlanId;
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
            pVxlanNvePortEntry->u4NetworkPort;
        pVxlanNveEntry->u4VxlanTunnelIfIndex =
            pVxlanNvePortEntry->u4VxlanTunnelIfIndex;
        MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1NextHopMac,
                pVxlanNvePortEntry->au1NextHopMac, MAC_ADDR_LEN);
    }

    if (NetIpv4GetCfaIfIndexFromPort (pVxlanNveEntry->u4VxlanTunnelIfIndex,
                                      &u4IfaceIndex) == NETIPV4_FAILURE)
    {
        return VXLAN_FAILURE;
    }
    if (u1Flag == VXLAN_HW_DEL)
    {
        pVxlanNveEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;
    }
    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex = u4IfaceIndex;
#ifdef NPAPI_WANTED
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    if (u1Flag == VXLAN_HW_ADD)
    {
        if (pVxlanNvePortEntry->u4RefCount == 1)
        {
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                 u4IfaceIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
            gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex]++;
            pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_DATABASE;
            if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Updating NVE database : HW programming failed!\r\n"));
                return VXLAN_FAILURE;
            }
            pVxlanNvePortEntry->u4NetworkPort =
                pVxlanNpModInfo->unHwParam.VxlanHwNveInfo.u4NetworkPort;
#ifdef L2RED_WANTED
            VxlanRedSyncDynNvePortInfo (pVxlanNvePortEntry);
#endif
        }
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             u4IfaceIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
            pVxlanNvePortEntry->u4NetworkPort;
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Updating L2 table : HW programming failed! \r\n"));
            return VXLAN_FAILURE;
        }
        pVxlanNveEntry->bHwSet = TRUE;
        if (MEMCMP (pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                    zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0)
        {
#if defined (SW_LEARNING)
            u4IfaceIndex = 0;
            if (VlanGetFdbEntryDetails ((UINT4) pVxlanNveEntry->i4VlanId,
                                        pVxlanNveEntry->MibObject.
                                        FsVxlanNveDestVmMac,
                                        (UINT2 *) &u4IfaceIndex,
                                        &u1Status) == VLAN_SUCCESS)
            {
                VlanHwDelFdbEntry (u4IfaceIndex,
                                   pVxlanNveEntry->MibObject.
                                   FsVxlanNveDestVmMac,
                                   (UINT2) pVxlanNveEntry->i4VlanId);
            }
            VlanFdbTableAdd ((UINT4)
                             (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex),
                             pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                             (UINT2) pVxlanNveEntry->i4VlanId, VLAN_FDB_LEARNT,
                             FNP_ZERO);
#endif
        }

    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             u4IfaceIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Removing L2 table : HW programming failed! \r\n"));
            return VXLAN_FAILURE;
        }
        if (pVxlanNvePortEntry->u4RefCount == 1)
        {
            gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex]--;
            if (gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex] < 1)
            {
                VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1IsEgIntfTobeDeleted =
                    VXLAN_TRUE;
            }
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CLEAR,    /* Function/OpCode */
                                 u4IfaceIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
            pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_DATABASE;
            if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Removing NVE database : HW programming failed! \r\n"));
                return VXLAN_FAILURE;
            }
        }
        VxlanNvePortEntryRem (pVxlanNveEntry->MibObject.
                              au1FsVxlanNveRemoteVtepAddress,
                              pVxlanNveEntry->MibObject.
                              i4FsVxlanNveRemoteVtepAddressLen);
        pVxlanNveEntry->bHwSet = FALSE;
        if (MEMCMP (pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                    zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0)
        {
#if defined (SW_LEARNING)
            VlanRemoveFdbEntry (0, (UINT2) pVxlanNveEntry->i4VlanId,
                                pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);
#endif
        }

    }
#endif
    pVxlanNveEntry->u4OutVlanId =
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel;
    if ((u1Flag == VXLAN_HW_ADD)
        && (VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel != 0))
    {
        pVxlanMCastEntry =
            VxlanUtilGetMCastDatabase (pVxlanNveEntry->MibObject.
                                       u4FsVxlanNveVniNumber,
                                       (UINT4) pVxlanNveEntry->MibObject.
                                       i4FsVxlanNveIfIndex);
        if (pVxlanMCastEntry != NULL)
        {
            pVxlanMCastEntry->u4OutVlanId = pVxlanNveEntry->u4OutVlanId;
            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_ADD) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Multicast entry in h/w\n"));
            }
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateNveDatabase: Exit\n"));

    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateMcastDatabase
 *
 * Description               : This function sets VXLAN multicast database in H/W
 *
 * Input                     : pVxlanMCastEntry - VXLAN multicast database.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanHwUpdateMcastDatabase (tVxlanFsVxlanMCastEntry * pVxlanMCastEntry,
                            UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    tVxlanHwInfo        VxlanTempHwInfo;
    tIp6Addr            GrpAddr;
    UINT4               u4GrpAddr = 0;
    tNetIpv4IfInfo      Ip4Info;
    UINT4               u4Port = CFA_INVALID_INDEX;
    UINT4               u4PrevPort = 0;
    UINT4               u4IfaceIndex = 0;
#ifdef IGMP_WANTED
    INT4                i4SlotId = 0;
    INT4                i4PortVlanId;
#endif
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanHwInfo       *pVxlanTempNpModInfo = NULL;
#endif
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4NveIndex = 0;
    UINT4               u4IfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateMcastDatabase: Entry\n"));
    if (((u1Flag == VXLAN_HW_DEL) && (pVxlanMCastEntry->bHwSet == FALSE)) ||
        ((u1Flag == VXLAN_HW_ADD) && (pVxlanMCastEntry->bHwSet == TRUE)))
    {
        return VXLAN_SUCCESS;
    }

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (&VxlanTempHwInfo, 0, sizeof (tVxlanHwInfo));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    pVxlanTempNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.i4NveIfIndex =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;

    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.u4VniNumber =
        pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.u4VlanId =
        (UINT4) pVxlanMCastEntry->i4VlanId;

    if (u1Flag == VXLAN_HW_ADD)
    {
        pVxlanNveEntry =
            VxlanUtilGetNveIndexFromNveTable (pVxlanMCastEntry->MibObject.
                                              u4FsVxlanMCastVniNumber,
                                              &u4NveIndex);
        if (pVxlanNveEntry != NULL)
        {
            if (pVxlanNveEntry->u4OutVlanId != 0)
            {
                pVxlanMCastEntry->u4OutVlanId = pVxlanNveEntry->u4OutVlanId;
                VxlanHwInfo.unHwParam.VxlanHwMcastInfo.u4VlanIdOutTunnel =
                    pVxlanNveEntry->u4OutVlanId;
            }
            else
            {
                return VXLAN_SUCCESS;
            }
        }

        else
        {
            while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
            {
                if (NetIpv4GetPortFromIfIndex (u4Port, &u4IfaceIndex) ==
                    NETIPV4_SUCCESS)
                {
                    if (NetIpv4GetIfInfo (u4IfaceIndex, &Ip4Info) ==
                        NETIPV4_SUCCESS)
                    {
#ifdef IGMP_WANTED
                        if (IgmpIsEnableOnInterface (u4IfaceIndex) == TRUE)
                        {
                            if (Ip4Info.u4IfType == CFA_L3IPVLAN)
                            {
                                CfaGetSlotAndPortFromIfIndex (u4Port, &i4SlotId,
                                                              (INT4 *)
                                                              &VxlanHwInfo.
                                                              unHwParam.
                                                              VxlanHwMcastInfo.
                                                              u4VlanIdOutTunnel);
                            }
                            else if (Ip4Info.u4IfType == CFA_ENET)
                            {
                                if (CfaGetIfIpPortVlanId
                                    (u4IfaceIndex,
                                     &i4PortVlanId) == CFA_SUCCESS)
                                {
                                    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.
                                        u4VlanIdOutTunnel =
                                        (UINT4) i4PortVlanId;
                                }

                            }
                            if (VxlanHwInfo.unHwParam.VxlanHwMcastInfo.
                                u4VlanId ==
                                VxlanHwInfo.unHwParam.VxlanHwMcastInfo.
                                u4VlanIdOutTunnel)
                            {
                                u4PrevPort = u4Port;
                                continue;
                            }
                            break;
                        }
#endif
                    }
                }
                u4PrevPort = u4Port;
            }
            pVxlanMCastEntry->u4OutVlanId = VxlanHwInfo.unHwParam.
                VxlanHwMcastInfo.u4VlanIdOutTunnel;
        }
    }
    else
    {
        VxlanHwInfo.unHwParam.VxlanHwMcastInfo.u4VlanIdOutTunnel =
            pVxlanMCastEntry->u4OutVlanId;
    }

    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.i4VtepAddressType =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType;

    VxlanHwInfo.unHwParam.VxlanHwMcastInfo.i4GroupAddressType =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType;

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwMcastInfo.au1VtepAddress),
            &(pVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress),
            pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen);

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwMcastInfo.au1GroupAddress),
            &(pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress),
            pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen);

    if (pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType ==
        VXLAN_IPV4_UNICAST)
    {
        MEMCPY (&u4GrpAddr,
                pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                VXLAN_IP4_ADDR_LEN);
        u4GrpAddr = OSIX_HTONL (u4GrpAddr);

        VxlanGetIpv4McastMaddr (u4GrpAddr, (UINT1 *)
                                VxlanHwInfo.unHwParam.VxlanHwMcastInfo.
                                McastMac);
    }
    else
    {
        MEMSET (&GrpAddr, 0, sizeof (tIp6Addr));
        MEMCPY (&GrpAddr,
                pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                sizeof (GrpAddr));
        VxlanGetIpv6McastMaddr (&GrpAddr,
                                (UINT1 *) VxlanHwInfo.unHwParam.
                                VxlanHwMcastInfo.McastMac);
    }

    pVxlanMCastEntry->bHwSet = TRUE;
    VxlanHwInfo.u4InfoType = VXLAN_HW_MCAST_DATABASE;
    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
        pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
        (UINT4) pVxlanMCastEntry->i4VlanId;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
        pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType;

    MEMCPY (&(VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
            &(pVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress),
            pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen);

    VxlanTempHwInfo.u4InfoType = VXLAN_HW_NVE_DATABASE;
    u4IfIndex = VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        if (pVxlanNveEntry == NULL)
        {
            u4IfIndex = VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                 u4IfIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanTempNpModInfo, &VxlanTempHwInfo,
                    sizeof (tVxlanHwInfo));
            if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Nve entry in h/w\n"));
            }
            else
            {
                pVxlanMCastEntry->bDummyNveZeroMAC = TRUE;
            }
            u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                 u4IfIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
            if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
            {
                return VXLAN_FAILURE;
            }

        }
    }
    else
    {
        u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             u4IfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
        if (pVxlanMCastEntry->bDummyNveZeroMAC == TRUE)
        {
            u4IfIndex = VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CLEAR,    /* Function/OpCode */
                                 u4IfIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanTempNpModInfo, &VxlanTempHwInfo,
                    sizeof (tVxlanHwInfo));
            if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
        }
        pVxlanMCastEntry->bHwSet = FALSE;
    }
#else
    UNUSED_PARAM (u1Flag);
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateMcastDatabase: Exit\n"));
    UNUSED_PARAM (u4IfIndex);
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateVniVlanMapDatabase
 *
 * Description               : This function sets VXLAN vni vlan map database in H/W
 *
 * Input                     : pVxlanVniVlantEntry - VXLAN vni vlan map database.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanHwUpdateVniVlanMapDatabase (tVxlanFsVxlanVniVlanMapEntry *
                                 pVxlanVniVlantEntry, UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    INT4                i4ByteIndex = -1;
    tPortList          *pVlanPortList = NULL;
    UINT4               u4IfIndex = 1;
    UINT1               u1Result = VXLAN_INIT_VAL;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortGetEntry = NULL;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateVniVlanMapDatabase: Entry\n"));

#ifdef NPAPI_WANTED
    pVxlanVniVlanPortGetEntry = (tVxlanVniVlanPortEntry *)
        MemAllocMemBlk (VXLAN_VNIVLANPORTTABLE_POOLID);

    if (pVxlanVniVlanPortGetEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanVniVlanPortTableCreate: Fail to Allocate Memory.\r\n"));
        return VXLAN_FAILURE;
    }

    MEMSET (pVxlanVniVlanPortGetEntry, 0, sizeof (tVxlanVniVlanPortEntry));
#endif

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VniNumber =
        pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VlanId = (UINT4)
        pVxlanVniVlantEntry->MibObject.i4FsVxlanVniVlanMapVlanId;

    /* Create access port for member ports of VLAN */

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktSent =
        pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktSent;

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktRcvd =
        pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd;

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktDrpd =
        pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd;

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u1IsVlanTagged =
        pVxlanVniVlantEntry->MibObject.u1IsVlanTagged;

#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        if (VxlanFsVxlanVniVlanPortTableCreate (pVxlanVniVlantEntry) ==
            OSIX_FAILURE)
        {
            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortGetEntry);

            return (OSIX_FAILURE);
        }
        if (VxlanFsVxlanVniVlanPortMacTableCreate (pVxlanVniVlantEntry) ==
            OSIX_FAILURE)
        {
            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortGetEntry);
            return (OSIX_FAILURE);
        }

        VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;

        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));

        if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)

        {
            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortGetEntry);
            return VXLAN_FAILURE;
        }
    }
#endif
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pVlanPortList == NULL)
    {
        MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                            (UINT1 *) pVxlanVniVlanPortGetEntry);
        VXLAN_TRC ((VXLAN_FAIL_TRC,
                    "Error in allocating memory for pVlanPortList\r\n"));
        return VXLAN_FAILURE;
    }
    MEMSET (pVlanPortList, VXLAN_INIT_VAL, BRG_PORT_LIST_SIZE);

    L2IwfGetVlanEgressPorts ((UINT2)
                             (pVxlanVniVlantEntry->MibObject.
                              i4FsVxlanVniVlanMapVlanId), *pVlanPortList);

    while (u4IfIndex <= (sizeof (tPortList) * BITS_PER_BYTE))
    {
        VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_PORT_MAPPING;
        /* If the bitmap contain 8 concecutive zeros, starting from position
         * zero of the byte, then entire byte will be zero, and there is no
         * need for looping through it */
        if (((u4IfIndex - 1) % BITS_PER_BYTE) == 0)
        {
            i4ByteIndex++;
        }
        if ((*pVlanPortList)[i4ByteIndex] == 0)
        {
            u4IfIndex = u4IfIndex + BITS_PER_BYTE;
            continue;
        }

        OSIX_BITLIST_IS_BIT_SET ((*pVlanPortList),
                                 u4IfIndex, sizeof (tPortList) - 1, u1Result);

        if ((u1Result == OSIX_TRUE) && ((u4IfIndex < CFA_MIN_NVE_IF_INDEX) ||
                                        (u4IfIndex > CFA_MAX_NVE_IF_INDEX)))
        {
            VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4IfIndex = u4IfIndex;
#ifdef NPAPI_WANTED
            if (u1Flag == VXLAN_HW_ADD)
            {
                gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfIndex]++;
                VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4AccessPort = 0;
                NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                     NP_VXLAN_MOD,    /* Module ID */
                                     VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                     0,    /* IfIndex value if applicable */
                                     0,    /* No. of Port Params */
                                     0);    /* No. of PortList Parms */
                MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
                if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)

                {
                    u4IfIndex++;
                    continue;
                }
                pVxlanVniVlanPortEntry = VxlanVniVlanPortTableAdd
                    (pVxlanVniVlantEntry, u4IfIndex);
                if (pVxlanVniVlanPortEntry != NULL)
                {
                    pVxlanVniVlanPortEntry->u4AccessPort =
                        pVxlanNpModInfo->unHwParam.VxlanHwVniVlanInfo.
                        u4AccessPort;
                }
            }
            else
            {
                pVxlanVniVlanPortGetEntry->u4IfIndex = u4IfIndex;
                pVxlanVniVlanPortEntry =
                    RBTreeGet (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                               (tRBElem *) pVxlanVniVlanPortGetEntry);
                if (pVxlanVniVlanPortEntry != NULL)
                {
                    gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfIndex]--;
                    if (gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfIndex] <
                        1)
                    {
                        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.
                            u1IsEgIntfTobeDeleted = VXLAN_TRUE;
                    }
                    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4AccessPort =
                        pVxlanVniVlanPortEntry->u4AccessPort;

                    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                         NP_VXLAN_MOD,    /* Module ID */
                                         VXLAN_HW_CLEAR,    /* Function/OpCode */
                                         0,    /* IfIndex value if applicable */
                                         0,    /* No. of Port Params */
                                         0);    /* No. of PortList Parms */
                    MEMCPY (pVxlanNpModInfo, &VxlanHwInfo,
                            sizeof (tVxlanHwInfo));
                    if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
                    {
                        u4IfIndex++;
                        continue;
                    }

                    pVxlanVniVlanPortMacEntry =
                        RBTreeGetFirst (pVxlanVniVlantEntry->
                                        VxlanVniVlanPortMacTable);
                    while (pVxlanVniVlanPortMacEntry != NULL)
                    {
                        MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac,
                                pVxlanVniVlanPortMacEntry->MacAddress,
                                sizeof (tMacAddr));

                        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
                            pVxlanVniVlantEntry->MibObject.
                            u4FsVxlanVniVlanMapVniNumber;

                        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = 0;    /* for dynamic */

                        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
                            pVxlanVniVlanPortEntry->u4AccessPort;

                        VxlanHwInfo.u4InfoType = VXLAN_HW_NVE_L2_TABLE;

                        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                             NP_VXLAN_MOD,    /* Module ID */
                                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                                             0,    /* IfIndex value if applicable */
                                             0,    /* No. of Port Params */
                                             0);    /* No. of PortList Parms */

                        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo,
                                sizeof (tVxlanHwInfo));

                        if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
                        {
                            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                                (UINT1 *)
                                                pVxlanVniVlanPortGetEntry);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return VXLAN_FAILURE;
                        }
                        VlanRemoveFdbEntry (0,
                                            (UINT2) pVxlanVniVlantEntry->
                                            MibObject.i4FsVxlanVniVlanMapVlanId,
                                            pVxlanVniVlanPortMacEntry->
                                            MacAddress);
                        /* remove the entry from control plane DB - VniVlanPortMacEntry */
                        RBTreeRem (pVxlanVniVlantEntry->
                                   VxlanVniVlanPortMacTable,
                                   pVxlanVniVlanPortMacEntry);
                        MemReleaseMemBlock (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                            (UINT1 *)
                                            pVxlanVniVlanPortMacEntry);
                        pVxlanVniVlanPortMacEntry =
                            RBTreeGetFirst (pVxlanVniVlantEntry->
                                            VxlanVniVlanPortMacTable);
                    }
                    /* remove the entry from control plane DB - VniVlanPortEntry */
                    RBTreeRem (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                               pVxlanVniVlanPortEntry);
                    MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                        (UINT1 *) pVxlanVniVlanPortEntry);
                }
            }
#else
            UNUSED_PARAM (u1Flag);
#endif
        }
        u4IfIndex++;
    }

    if (u1Flag == VXLAN_HW_DEL)
    {
        VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;

#ifdef NPAPI_WANTED
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)

        {
            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortGetEntry);
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            return VXLAN_FAILURE;
        }
        /* Every VLAN-VNI mapping has two RBTrees - port table and portMac table,
         * which should be deleted when VLAN-VNI mapping is removed */
        if (pVxlanVniVlantEntry->VxlanVniVlanPortTable != NULL)
        {
            RBTreeDelete (pVxlanVniVlantEntry->VxlanVniVlanPortTable);
            pVxlanVniVlantEntry->VxlanVniVlanPortTable = NULL;
        }
        if (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable != NULL)
        {
            RBTreeDelete (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable);
            pVxlanVniVlantEntry->VxlanVniVlanPortMacTable = NULL;
        }
#endif
    }

    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);

    MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                        (UINT1 *) pVxlanVniVlanPortGetEntry);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateVniVlanMapDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateVniVlanMapDatabase
 *
 * Description               : This function sets VXLAN vni vlan map database in H/W
 *
 * Input                     : pVxlanVniVlantEntry - VXLAN vni vlan map database.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanHwUpdateVniVlanMapDbBasedOnVlanPorts (tVxlanFsVxlanVniVlanMapEntry
                                           * pVxlanVniVlantEntry,
                                           UINT1 u1Action,
                                           tLocalPortList LocalPortList)
{
    UINT1               u1PortFlag = 0;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    tLocalPortList      NullPortList;
    tVxlanHwInfo        VxlanHwInfo;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortGetEntry = NULL;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateVniVlanMapDatabase: Entry\n"));

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));
#ifdef NPAPI_WANTED
    pVxlanVniVlanPortGetEntry = (tVxlanVniVlanPortEntry *)
        MemAllocMemBlk (VXLAN_VNIVLANPORTTABLE_POOLID);

    if (pVxlanVniVlanPortGetEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanVniVlanPortTableCreate: Fail to Allocate Memory.\r\n"));
        return VXLAN_FAILURE;
    }

    MEMSET (pVxlanVniVlanPortGetEntry, 0, sizeof (tVxlanVniVlanPortEntry));
#endif

    if (MEMCMP (NullPortList, LocalPortList, sizeof (tLocalPortList)) == 0)
    {
        MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                            (UINT1 *) pVxlanVniVlanPortGetEntry);
        return VXLAN_SUCCESS;
    }

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VniNumber =
        pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VlanId = (UINT4)
        pVxlanVniVlantEntry->MibObject.i4FsVxlanVniVlanMapVlanId;

    for (u2ByteIndex = 0; u2ByteIndex < CONTEXT_PORT_LIST_SIZE; u2ByteIndex++)
    {
        /* Looping to find for Index in DeletedPortList */
        u1PortFlag = LocalPortList[u2ByteIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (((VcmGetIfIndexFromLocalPort
                      (0, (UINT2) u4Port, &u4IfIndex)) == VCM_SUCCESS)
                    && ((u4IfIndex < CFA_MIN_NVE_IF_INDEX)
                        || (u4IfIndex > CFA_MAX_NVE_IF_INDEX)))
                {
                    VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4IfIndex =
                        u4IfIndex;
#ifdef NPAPI_WANTED
                    VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_PORT_MAPPING;
                    if (u1Action == VLAN_CB_ADD_EGRESS_PORTS)
                    {
                        gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfIndex]++;
                        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4AccessPort =
                            0;
                        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                             NP_VXLAN_MOD,    /* Module ID */
                                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                             0,    /* IfIndex value if applicable */
                                             0,    /* No. of Port Params */
                                             0);    /* No. of PortList Parms */
                        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo,
                                sizeof (tVxlanHwInfo));
                        if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)

                        {
                            continue;
                        }
                        pVxlanVniVlanPortEntry = VxlanVniVlanPortTableAdd
                            (pVxlanVniVlantEntry, u4IfIndex);
                        if (pVxlanVniVlanPortEntry != NULL)
                        {
                            pVxlanVniVlanPortEntry->u4AccessPort =
                                pVxlanNpModInfo->unHwParam.VxlanHwVniVlanInfo.
                                u4AccessPort;
                        }
                    }
                    else
                    {
                        pVxlanVniVlanPortGetEntry->u4IfIndex = u4IfIndex;
                        pVxlanVniVlanPortEntry =
                            RBTreeGet (pVxlanVniVlantEntry->
                                       VxlanVniVlanPortTable,
                                       (tRBElem *) pVxlanVniVlanPortGetEntry);
                        if (pVxlanVniVlanPortEntry != NULL)
                        {
                            gVxlanGlobals.VxlanGlbMib.
                                i4AcessportCount[u4IfIndex]--;
                            if (gVxlanGlobals.VxlanGlbMib.
                                i4AcessportCount[u4IfIndex] < 1)
                            {
                                VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.
                                    u1IsEgIntfTobeDeleted = VXLAN_TRUE;
                            }
                            VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.
                                u4AccessPort =
                                pVxlanVniVlanPortEntry->u4AccessPort;

                            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                                 NP_VXLAN_MOD,    /* Module ID */
                                                 VXLAN_HW_CLEAR,    /* Function/OpCode */
                                                 0,    /* IfIndex value if applicable */
                                                 0,    /* No. of Port Params */
                                                 0);    /* No. of PortList Parms */
                            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo,
                                    sizeof (tVxlanHwInfo));
                            if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
                            {
                                continue;
                            }
                            RBTreeRem (pVxlanVniVlantEntry->
                                       VxlanVniVlanPortTable,
                                       pVxlanVniVlanPortEntry);
                            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                                (UINT1 *)
                                                pVxlanVniVlanPortEntry);
                            pVxlanVniVlanPortMacEntry =
                                RBTreeGetFirst (pVxlanVniVlantEntry->
                                                VxlanVniVlanPortMacTable);
                            while (pVxlanVniVlanPortMacEntry != NULL)
                            {
                                RBTreeRem (pVxlanVniVlantEntry->
                                           VxlanVniVlanPortMacTable,
                                           pVxlanVniVlanPortMacEntry);
                                MemReleaseMemBlock
                                    (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                     (UINT1 *) pVxlanVniVlanPortMacEntry);
                                pVxlanVniVlanPortMacEntry =
                                    RBTreeGetFirst (pVxlanVniVlantEntry->
                                                    VxlanVniVlanPortMacTable);
                            }
                        }

                    }

#else
                    UNUSED_PARAM (u1Action);
#endif

                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                        (UINT1 *) pVxlanVniVlanPortGetEntry);
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateInReplicaDatabase
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanInReplicaEntry - VXLAN nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanHwUpdateInReplicaDatabase (tVxlanFsVxlanInReplicaEntry *
                                pVxlanInReplicaEntry, UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT4               u4IfaceIndex = 0;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#ifdef ALTA_WANTED
    tVxlanHwInfo        VxlanTempHwInfo;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4NveIndex = 0;
#endif
#endif
    if (((u1Flag == VXLAN_HW_DEL) && (pVxlanInReplicaEntry->bHwSet == FALSE)) ||
        ((u1Flag == VXLAN_HW_ADD) && (pVxlanInReplicaEntry->bHwSet == TRUE)))
    {
        VXLAN_TRC ((VXLAN_ALL_TRC,
                    "Ingress Replica: HW programming not required! \r\n"));
        return VXLAN_SUCCESS;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateInReplicaDatabase: Entry\n"));
    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
#if defined (NPAPI_WANTED) && (ALTA_WANTED)
    MEMSET (&VxlanTempHwInfo, 0, sizeof (tVxlanHwInfo));
    pVxlanNveEntry = VxlanUtilGetNveIndexFromNveTable
        (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber,
         &u4NveIndex);
#endif

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4NveIfIndex =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VniNumber =
        pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanId =
        (UINT4) pVxlanInReplicaEntry->i4VlanId;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4VtepAddressType =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressType;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4RemoteVtepAddressType =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressType;

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1VtepAddress),
            &(pVxlanInReplicaEntry->MibObject.au1FsVxlanInReplicaVtepAddress),
            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen);
    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1RemoteVtepAddress),
            &(pVxlanInReplicaEntry->
              MibObject.au1FsVxlanInReplicaRemoteVtepAddress),
            (pVxlanInReplicaEntry->
             MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen));

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.pau1RemoteVtepsReplicaTo =
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1RemoteVtepAddress;
    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NoRemoteVtepsReplicaTo = 1;

#ifdef ALTA_WANTED
    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
        pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
        (UINT4) pVxlanInReplicaEntry->i4VlanId;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressType;

    VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
        pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressType;

    MEMCPY (&(VxlanTempHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
            &(pVxlanInReplicaEntry->MibObject.au1FsVxlanInReplicaVtepAddress),
            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen);

#endif

    if (u1Flag == VXLAN_HW_DEL)
    {
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanIdOutTunnel =
            pVxlanInReplicaEntry->u4OutVlanId;
        pVxlanNvePortEntry =
            VxlanGetNvePortEntry (pVxlanInReplicaEntry->MibObject.
                                  au1FsVxlanInReplicaRemoteVtepAddress,
                                  pVxlanInReplicaEntry->MibObject.
                                  i4FsVxlanInReplicaRemoteVtepAddressLen);

        if (pVxlanNvePortEntry != NULL)
        {
            VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkPort =
                pVxlanNvePortEntry->u4NetworkPort;
            VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkBUMPort =
                pVxlanNvePortEntry->u4NetworkBUMPort;
        }
        else
        {
            VXLAN_TRC ((VXLAN_ALL_TRC,
                        "No entry in NVE port table for deletion\r\n"));
            return VXLAN_SUCCESS;
        }
    }
    else
    {
        pVxlanNvePortEntry =
            VxlanCheckRouteAndUpdateTunnelMapTable (pVxlanInReplicaEntry->
                                                    MibObject.
                                                    au1FsVxlanInReplicaRemoteVtepAddress,
                                                    pVxlanInReplicaEntry->
                                                    MibObject.
                                                    i4FsVxlanInReplicaRemoteVtepAddressLen,
                                                    pVxlanInReplicaEntry->
                                                    u4VxlanTunnelIfIndex);
        if (pVxlanNvePortEntry == NULL)
        {
            VXLAN_TRC ((VXLAN_ALL_TRC,
                        "No entry in NVE port table to add\r\n"));
            return VXLAN_SUCCESS;
        }

        MEMCPY (VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1NextHopMac,
                pVxlanNvePortEntry->au1NextHopMac, MAC_ADDR_LEN);

        pVxlanInReplicaEntry->u4OutVlanId =
            pVxlanNvePortEntry->u4VlanIdOutTunnel;
        pVxlanInReplicaEntry->u4VxlanTunnelIfIndex =
            pVxlanNvePortEntry->u4VxlanTunnelIfIndex;
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanIdOutTunnel =
            pVxlanInReplicaEntry->u4OutVlanId;
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkPort =
            pVxlanNvePortEntry->u4NetworkPort;
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkBUMPort =
            pVxlanNvePortEntry->u4NetworkBUMPort;

    }

    if (NetIpv4GetCfaIfIndexFromPort
        (pVxlanInReplicaEntry->u4VxlanTunnelIfIndex,
         &u4IfaceIndex) == NETIPV4_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex = u4IfaceIndex;
#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
#ifdef ALTA_WANTED
        VxlanTempHwInfo.u4InfoType = VXLAN_HW_NVE_DATABASE;
        if (pVxlanNveEntry == NULL)
        {
            if (FsNpHwVxlanInfo (&VxlanTempHwInfo, VXLAN_HW_CONFIGURE) ==
                FNP_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Nve entry in h/w\n"));
            }
            else
            {
                pVxlanInReplicaEntry->bDummyNveZeroMAC = TRUE;
            }
        }
#endif

        if (pVxlanNvePortEntry->u4RefCount == 1)
        {
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                                 u4IfaceIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));

            pVxlanNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_SOFTWARE;

            gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex]++;

            if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Updating BUM replica database : HW programming failed!\r\n"));
                return VXLAN_FAILURE;
            }
            pVxlanNvePortEntry->u4NetworkPort =
                pVxlanNpModInfo->unHwParam.VxlanHwBUMReplicaInfo.u4NetworkPort;
            pVxlanNvePortEntry->u4NetworkBUMPort =
                pVxlanNpModInfo->unHwParam.VxlanHwBUMReplicaInfo.
                u4NetworkBUMPort;

#ifdef L2RED_WANTED
            VxlanRedSyncDynNvePortInfo (pVxlanNvePortEntry);
#endif
        }
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             u4IfaceIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkPort =
            pVxlanNvePortEntry->u4NetworkPort;
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkBUMPort =
            pVxlanNvePortEntry->u4NetworkBUMPort;
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_MCAST;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Updating BUM replica Mcast database : HW programming failed!\r\n"));
            return VXLAN_FAILURE;
        }
        pVxlanInReplicaEntry->bHwSet = TRUE;
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             u4IfaceIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        pVxlanNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_MCAST;
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Removing BUM replica Mcast database : HW programming failed!\r\n"));
            return VXLAN_FAILURE;
        }
        if (pVxlanNvePortEntry->u4RefCount == 1)
        {
            gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex]--;
            if (gVxlanGlobals.VxlanGlbMib.i4AcessportCount[u4IfaceIndex] < 1)
            {
                VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.
                    u1IsEgIntfTobeDeleted = VXLAN_TRUE;
            }
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CLEAR,    /* Function/OpCode */
                                 u4IfaceIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
            pVxlanNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_SOFTWARE;
            if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Updating BUM replica Mcast database : HW programming failed!\r\n"));
                return VXLAN_FAILURE;
            }
        }
        VxlanNvePortEntryRem (pVxlanInReplicaEntry->MibObject.
                              au1FsVxlanInReplicaRemoteVtepAddress,
                              pVxlanInReplicaEntry->MibObject.
                              i4FsVxlanInReplicaRemoteVtepAddressLen);

#ifdef ALTA_WANTED
        if (pVxlanInReplicaEntry->bDummyNveZeroMAC == TRUE)
        {
            if (FsNpHwVxlanInfo (&VxlanTempHwInfo, VXLAN_HW_CLEAR) ==
                FNP_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
        }
#endif
        pVxlanInReplicaEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;
        pVxlanInReplicaEntry->bHwSet = FALSE;
    }
#else
    UNUSED_PARAM (u1Flag);
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateInReplicaDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/***************************************************************************
 * Function Name             : VxlanHwUpdateInReplicaDatabase
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanInReplicaEntry - VXLAN nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
VOID
VxlanHwUpdateMHPeerDatabase (UINT1 u1DFFlag, UINT4 u4Vni, UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT1               au1RemoteMHPeerList[VXLAN_HW_MH_PEER_LIST *
                                            (sizeof (UINT4))];
    UINT4               u4Len = 0;
    UINT4               u4NoOfMHPeer = 0;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMHPeerEntry = NULL;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateInReplicaDatabase: Entry\n"));

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (au1RemoteMHPeerList, 0, sizeof (au1RemoteMHPeerList));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    pVxlanVtepEntry = (tVxlanFsVxlanVtepEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable);

    if (pVxlanVtepEntry == NULL)
    {
        return;
    }

    pEvpnVxlanMHPeerEntry =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst (gVxlanGlobals.
                                                                 VxlanGlbMib.
                                                                 FsEvpnVxlanMultihomedPeerTable);
    while (pEvpnVxlanMHPeerEntry != NULL)
    {
        if (MEMCMP (pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                    pEvpnVxlanMHPeerEntry->MibObject.
                    au1FsEvpnVxlanPeerIpAddress,
                    pEvpnVxlanMHPeerEntry->MibObject.
                    i4FsEvpnVxlanPeerIpAddressLen) == 0)
        {
            pEvpnVxlanMHPeerEntry =
                (tVxlanFsEvpnVxlanMultihomedPeerTable *)
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                               FsEvpnVxlanMultihomedPeerTable,
                               (tRBElem *) pEvpnVxlanMHPeerEntry, NULL);
            continue;
        }

        MEMCPY ((&au1RemoteMHPeerList) + u4Len, &(pEvpnVxlanMHPeerEntry->
                                                  MibObject.
                                                  au1FsEvpnVxlanPeerIpAddress),
                pEvpnVxlanMHPeerEntry->MibObject.i4FsEvpnVxlanPeerIpAddressLen);
        u4Len =
            u4Len +
            pEvpnVxlanMHPeerEntry->MibObject.i4FsEvpnVxlanPeerIpAddressLen;
        pEvpnVxlanMHPeerEntry =
            (tVxlanFsEvpnVxlanMultihomedPeerTable *)
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                           FsEvpnVxlanMultihomedPeerTable,
                           (tRBElem *) pEvpnVxlanMHPeerEntry, NULL);
        u4NoOfMHPeer++;
    }
    if (u4NoOfMHPeer == 0)
    {
        return;
    }
    VxlanHwInfo.unHwParam.VxlanHwBUMMHPeerInfo.pau1BumMHPeers =
        au1RemoteMHPeerList;
    VxlanHwInfo.unHwParam.VxlanHwBUMMHPeerInfo.u4NoBumMHPeers = u4NoOfMHPeer;
    VxlanHwInfo.unHwParam.VxlanHwBUMMHPeerInfo.u4Vni = u4Vni;
    VxlanHwInfo.unHwParam.VxlanHwBUMMHPeerInfo.u1DFFlag = u1DFFlag;
    VxlanHwInfo.u4InfoType = VXLAN_HW_MH_PEER_DATABASE;

#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return;
        }
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return;
        }
    }
#else
    UNUSED_PARAM (u1Flag);
#endif
    return;
}

/***************************************************************************
 * Function Name             : VxlanHwUpdateInReplicaDatabase
 *
 * Description               : This function sets dummy replica database to
 trap the arp packet in ARP suppression case
 *
 * Input                     : pVxlanNveEntry - VXLAN nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
VOID
VxlanHwUpdateArpSuppression (tVxlanFsVxlanNveEntry * pVxlanNveEntry,
                             UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT1               au1RemoteVTEPAddr[4];
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
    UINT4               u4IfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateInReplicaDatabase: Entry\n"));

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (au1RemoteVTEPAddr, 0, sizeof (au1RemoteVTEPAddr));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4NveIfIndex =
        pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

    if (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex > CFA_MAX_NVE_IF_INDEX)
    {
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4NveIfIndex =
            (INT4) pVxlanNveEntry->u4OrgNveIfIndex;
    }

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VniNumber =
        pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanId =
        (UINT4) pVxlanNveEntry->i4VlanId;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4VtepAddressType =
        pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4RemoteVtepAddressType =
        pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1VtepAddress),
            &(pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress),
            pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.pau1RemoteVtepsReplicaTo =
        au1RemoteVTEPAddr;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NoRemoteVtepsReplicaTo = 1;

    VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u1ArpSupFlag = TRUE;

    VxlanHwInfo.u4InfoType = VXLAN_HW_BUM_REPLICA_SOFTWARE;

#ifdef NPAPI_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4IfIndex;
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             u4IfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return;
        }
        pVxlanNveEntry->bHwSet = TRUE;
    }
    else
    {
        u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4IfIndex;
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             u4IfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return;
        }
        pVxlanNveEntry->bHwSet = FALSE;
    }
#else
    UNUSED_PARAM (u1Flag);
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateInReplicaDatabase: Exit\n"));
    return;
}
#endif
VOID
VxlanGetIpv6McastMaddr (tIp6Addr * pAddr6, UINT1 *pMacaddr)
{
    pMacaddr[0] = 0x33;
    pMacaddr[1] = 0x33;

    MEMCPY (pMacaddr + 2, &pAddr6->u4_addr[3], sizeof (UINT4));
    return;

}

VOID
VxlanGetIpv4McastMaddr (UINT4 u4Dest, UINT1 *pMacaddr)
{
    UINT4               u4Mcast = 0;

    pMacaddr[0] = 0x01;
    pMacaddr[1] = 0x00;
    u4Mcast = u4Dest & (UINT4) 0xffffff;
    u4Mcast = u4Mcast | (UINT4) 0x5e000000;
    u4Mcast = OSIX_HTONL (u4Mcast);
    MEMCPY (&pMacaddr[2], &u4Mcast, sizeof (UINT4));
    return;
}

VOID
VxlanUpdateMacEntry (UINT4 u4Vni, tMacAddr VxlanMac, UINT4 u4Port, UINT1 u1Flag)
{
    UINT2               u2VlanId = 0;
    UINT4               u4IpAddress = 0;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanEntry = NULL;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
    tVxlanVniVlanPortMacEntry VxlanVniVlanPortMacEntry;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;

    MEMSET (&VxlanVniVlanEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VxlanUtilGetVlanFromVni (u4Vni, &u2VlanId);
    pVxlanNvePortEntry =
        (tVxlanNvePortEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                               VxlanNvePortTable);
    while (pVxlanNvePortEntry != NULL)
    {
        if (pVxlanNvePortEntry->u4NetworkPort == u4Port)
        {
            if (u1Flag == VXLAN_HW_ADD)
            {
                MEMCPY (&u4IpAddress, pVxlanNvePortEntry->au1RemoteVtepAddress,
                        VXLAN_IP4_ADDR_LEN);
#ifdef NPAPI_WANTED
                VxlanAddNveDatabase (VxlanMac, u2VlanId, u4IpAddress);
#endif
            }
            else
            {
#ifdef NPAPI_WANTED
                VxlanDelNveDatabase (VxlanMac, u2VlanId);
#endif
            }
            return;
        }
        pVxlanNvePortEntry =
            (tVxlanNvePortEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  VxlanNvePortTable,
                                                  (tRBElem *)
                                                  pVxlanNvePortEntry, NULL);
    }

    VxlanVniVlanEntry.MibObject.i4FsVxlanVniVlanMapVlanId = (INT4) u2VlanId;

    pVxlanVniVlanEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanEntry);
    if (pVxlanVniVlanEntry != NULL)
    {

        pVxlanVniVlanPortEntry =
            RBTreeGetFirst (pVxlanVniVlanEntry->VxlanVniVlanPortTable);
        while (pVxlanVniVlanPortEntry != NULL)
        {
            if (pVxlanVniVlanPortEntry->u4AccessPort == u4Port)
            {
                break;

            }
            pVxlanVniVlanPortEntry =
                RBTreeGetNext (pVxlanVniVlanEntry->VxlanVniVlanPortTable,
                               (tRBElem *) pVxlanVniVlanPortEntry, NULL);
        }
        if (pVxlanVniVlanPortEntry == NULL)
        {
            return;
        }
        VxlanVniVlanPortMacEntry.u4IfIndex = pVxlanVniVlanPortEntry->u4IfIndex;
        MEMCPY (VxlanVniVlanPortMacEntry.MacAddress, VxlanMac,
                sizeof (tMacAddr));
        pVxlanVniVlanPortMacEntry =
            RBTreeGet (pVxlanVniVlanEntry->VxlanVniVlanPortMacTable,
                       (tRBElem *) & VxlanVniVlanPortMacEntry);
        if (u1Flag == VXLAN_HW_ADD)
        {
            if (pVxlanVniVlanPortMacEntry == NULL)
            {
                pVxlanVniVlanPortMacEntry = VxlanVniVlanPortMacTableAdd
                    (pVxlanVniVlanEntry, pVxlanVniVlanPortEntry->u4IfIndex,
                     VxlanMac);

                if (pVxlanVniVlanPortMacEntry != NULL)
                {
#ifdef L2RED_WANTED
                    VxlanRedSyncDynVlanVniPortMacInfo
                        (pVxlanVniVlanPortMacEntry, pVxlanVniVlanEntry, u4Port);
#endif
#if defined (SW_LEARNING)
                    VlanFdbTableAdd (VxlanVniVlanPortMacEntry.u4IfIndex,
                                     VxlanMac, u2VlanId, VLAN_FDB_LEARNT,
                                     FNP_ZERO);
#endif
                }
            }
            else
            {
                return;
            }
        }
        else
        {
            if (pVxlanVniVlanPortMacEntry != NULL)
            {
                VlanRemoveFdbEntry (0, u2VlanId, VxlanMac);
                RBTreeRem (pVxlanVniVlanEntry->VxlanVniVlanPortMacTable,
                           pVxlanVniVlanPortMacEntry);
                MemReleaseMemBlock (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                    (UINT1 *) pVxlanVniVlanPortMacEntry);
            }
            else
            {
                return;
            }
        }
    }
    return;
}

#ifdef NPAPI_WANTED
INT4
VxlanUpdateMacEntryInQueue (UINT4 u4Vni, tMacAddr VxlanMac, INT4 i4Port,
                            UINT4 u4Flag)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    MEMCPY (VxlanIfMsg.uVxlanMsg.VxlanNpMacPortInfo.NpMacAddr,
            VxlanMac, VXLAN_ETHERNET_ADDR_SIZE);
    VxlanIfMsg.uVxlanMsg.VxlanNpMacPortInfo.i4Port = i4Port;
    VxlanIfMsg.uVxlanMsg.VxlanNpMacPortInfo.u1Flag = (UINT1) u4Flag;
    VxlanIfMsg.uVxlanMsg.VxlanNpMacPortInfo.u4Vni = u4Vni;
    VxlanIfMsg.u4MsgType = VXLAN_NP_MAC_PORT_EVENT;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        return FNP_FAILURE;
    }
    return FNP_SUCCESS;
}

INT4
VxlanAddDmacEntry (tMacAddr NpMacAddr, tVlanId VlanId2, UINT4 u4RemoteVTEPIp)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    MEMCPY (VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.NpMacAddr,
            NpMacAddr, VXLAN_ETHERNET_ADDR_SIZE);
    VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.VlanId2 = VlanId2;
    VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.u1Flag = VXLAN_HW_ADD;
    VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.u4RemoteVTEPIp = u4RemoteVTEPIp;
    VxlanIfMsg.u4MsgType = VXLAN_NP_MAC_VLAN_EVENT;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        return FNP_FAILURE;
    }
    return FNP_SUCCESS;
}

VOID
VxlanAddNveDatabase (tMacAddr NpMacAddr, tVlanId VlanId2, UINT4 u4RemoteVTEPIp)
{

    tVxlanHwInfo        VxlanHwInfo;
    tVxlanFsVxlanNveEntry *pVxlanAddNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4VniNumber = 0;
    UINT4               u4NveIndex = 0;
    UINT1               u1IfAdminStatus = 0;
    tMacAddr            ZeroMac;
#ifdef EVPN_VXLAN_WANTED
    UINT4               u4TmpVni = 0;
    UINT4               u4TempAddr = 0;
    tVxlanFsVxlanEcmpNveEntry *pVxlanTmpEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMHPeerEntry = NULL;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanAddNveDatabase: Entry\n"));

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId = (UINT4) VlanId2;
    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac),
            &(NpMacAddr), VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&ZeroMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    VxlanHwInfo.u4InfoType = VXLAN_HW_DEL_DMAC;

    if (VxlanUtilGetVniFromVlan (VlanId2, &u4VniNumber) == VXLAN_SUCCESS)
    {
#ifdef EVPN_VXLAN_WANTED
        pEvpnVxlanMHPeerEntry =
            (tVxlanFsEvpnVxlanMultihomedPeerTable *)
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                            FsEvpnVxlanMultihomedPeerTable);
        while (pEvpnVxlanMHPeerEntry != NULL)
        {
            u4TempAddr = 0;
            u4TmpVni = 0;
            EvpnUtilGetVniFromEsiEntry (pEvpnVxlanMHPeerEntry->MibObject.
                                        au1FsEvpnVxlanMHEviVniESI, &u4TmpVni);

            MEMCPY (&u4TempAddr,
                    pEvpnVxlanMHPeerEntry->MibObject.
                    au1FsEvpnVxlanPeerIpAddress, VXLAN_IP4_ADDR_LEN);
            u4TempAddr = OSIX_HTONL (u4TempAddr);

            if ((u4RemoteVTEPIp == u4TempAddr) && (u4VniNumber == u4TmpVni))
            {
                FsNpHwVxlanInfo (&VxlanHwInfo, 0);
                return;
            }
            pEvpnVxlanMHPeerEntry =
                (tVxlanFsEvpnVxlanMultihomedPeerTable *)
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                               FsEvpnVxlanMultihomedPeerTable,
                               (tRBElem *) pEvpnVxlanMHPeerEntry, NULL);
        }
#endif

        pVxlanNveEntry =
            VxlanUtilGetNveIndexFromNveTable (u4VniNumber, &u4NveIndex);

        if (pVxlanNveEntry == NULL)
        {
            if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniNumber,
                                                         &u4NveIndex)) == NULL)
            {
                if ((VxlanUtilGetNveIndexFromMcastTable (u4VniNumber,
                                                         &u4NveIndex)) == NULL)
                {
                    FsNpHwVxlanInfo (&VxlanHwInfo, 0);
                    return;
                }
            }
        }
#ifdef EVPN_VXLAN_WANTED
        if ((pVxlanNveEntry != NULL) &&
            (pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType == 0))
        {
            pVxlanNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               (tRBElem *) pVxlanNveEntry, NULL);
            if ((pVxlanNveEntry == NULL) ||
                (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber !=
                 u4VniNumber))
            {
                return;
            }
        }
        pVxlanEcmpNveEntry =
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable);

        while ((pVxlanEcmpNveEntry != NULL) &&
               (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                        ZeroMac, sizeof (tMacAddr)) == 0))

        {
            pVxlanTmpEcmpNveEntry = NULL;

            pVxlanTmpEcmpNveEntry = VxlanUtilGetEcmpNveDatabase
                (u4VniNumber, NpMacAddr, u4NveIndex,
                 pVxlanEcmpNveEntry->MibObject.
                 i4FsVxlanEcmpNveRemoteVtepAddressType,
                 pVxlanEcmpNveEntry->MibObject.
                 au1FsVxlanEcmpNveRemoteVtepAddress);

            if (pVxlanTmpEcmpNveEntry != NULL)
            {
                return;
            }
            pVxlanEcmpNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                               (tRBElem *) pVxlanEcmpNveEntry, NULL);

        }

#endif
        if ((pVxlanNveEntry != NULL)
            && (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex >
                CFA_MAX_NVE_IF_INDEX))
        {
            u4NveIndex = pVxlanNveEntry->u4OrgNveIfIndex;
        }

        if (VxlanUtilGetNveDatabase (u4VniNumber, NpMacAddr, u4NveIndex) !=
            NULL)
        {
            return;
        }

        CfaApiGetIfAdminStatus (u4NveIndex, &u1IfAdminStatus);
        if (u1IfAdminStatus != CFA_IF_UP)
        {
            FsNpHwVxlanInfo (&VxlanHwInfo, 0);
            return;
        }

        pVxlanAddNveEntry =
            (tVxlanFsVxlanNveEntry *)
            MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

        if (pVxlanAddNveEntry == NULL)
        {
            FsNpHwVxlanInfo (&VxlanHwInfo, 0);
            return;
        }
        VxlanInitializeMibFsVxlanNveTable (pVxlanAddNveEntry,
                                           (INT4) u4NveIndex);
        /* Fill the NVE if index */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveIfIndex = (INT4) u4NveIndex;

        /* Fill the VNI number */
        pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber = u4VniNumber;

        /* Fill the destination VM mac address */
        MEMCPY (pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac,
                NpMacAddr, VXLAN_ETHERNET_ADDR_SIZE);

        /* Fill the Vlan-ID */
        pVxlanAddNveEntry->i4VlanId = (INT4) VlanId2;
        /* Fill the Remote VTEP IP addres type */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
            = VXLAN_IPV4_UNICAST;

        pVxlanAddNveEntry->i4FsVxlanNveEvpnMacType = VXLAN_NVE_DYNAMIC_MAC;

        /* Fill the Remote VTEP IP address */
        if (pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
            == VXLAN_IPV4_UNICAST)
        {
            pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen
                = VXLAN_IP4_ADDR_LEN;
            MEMCPY (pVxlanAddNveEntry->MibObject.
                    au1FsVxlanNveRemoteVtepAddress, &u4RemoteVTEPIp,
                    VXLAN_IP4_ADDR_LEN);
        }
        /* Fill the storage type as volatile since the entry is learnt dynamically */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType =
            VXLAN_STRG_TYPE_VOL;

        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanAddNveEntry) != RB_SUCCESS)
        {
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanAddNveEntry);
            return;
        }

        if (VxlanHwUpdateNveDatabase (pVxlanAddNveEntry, VXLAN_HW_ADD) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC, "Failed to add Nve entry in h/w\n"));
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       pVxlanAddNveEntry);

            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanAddNveEntry);
            return;
        }

        VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                  &(pVxlanAddNveEntry->MibObject.NveDbNode));
        VxlanRedSyncDynInfo ();

        return;

    }
    FsNpHwVxlanInfo (&VxlanHwInfo, 0);
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanAddNveDatabase: Exit\n"));
    return;
}

VOID
VxlanRemoveDmacEntry (tMacAddr NpMacAddr, tVlanId VlanId2)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    MEMCPY (VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.NpMacAddr,
            NpMacAddr, VXLAN_ETHERNET_ADDR_SIZE);
    VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.VlanId2 = VlanId2;
    VxlanIfMsg.uVxlanMsg.VxlanNpMacVlanInfo.u1Flag = VXLAN_HW_DEL;
    VxlanIfMsg.u4MsgType = VXLAN_NP_MAC_VLAN_EVENT;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        VxlanDelNveDatabase (NpMacAddr, VlanId2);
        return;
    }
    return;
}

VOID
VxlanDelNveDatabase (tMacAddr NpMacAddr, tVlanId VlanId2)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4VniNumber = 0;
    UINT4               u4NveIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanDelNveDatabase: Entry\n"));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    if (VxlanUtilGetVniFromVlan (VlanId2, &u4VniNumber) == VXLAN_SUCCESS)
    {
        if (VxlanUtilGetNveIndexFromNveTable (u4VniNumber, &u4NveIndex) != NULL)
        {
            VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4NveIndex;
            VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4VniNumber;
            MEMCPY (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac, NpMacAddr,
                    VXLAN_ETHERNET_ADDR_SIZE);

            pVxlanNveEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry);

            if ((pVxlanNveEntry != NULL) &&
                (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                 VXLAN_STRG_TYPE_VOL) &&
                (pVxlanNveEntry->i4FsVxlanNveEvpnMacType != VXLAN_NVE_EVPN_MAC))
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry from h/w\n"));
                    return;
                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           pVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanNveEntry);
            }
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanDelNveDatabase: Exit\n"));
    return;
}
#endif

INT4
VxlanRemoveVniMacEntries (tCliHandle CliHandle, UINT4 *pu4VniID)
{
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextNveEntry = NULL;
    UINT4               u4NveIndex = 0;
    tVxlanHwInfo        VxlanHwInfo;
    tMacAddr            zeroAddr;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;

#endif

#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    if (pu4VniID != NULL)
    {
        pVxlanNveEntry =
            VxlanUtilGetNveIndexFromNveTable (*pu4VniID, &u4NveIndex);

    }
    else
    {
        pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
    }
    if (pVxlanNveEntry == NULL)
    {
        CliPrintf (CliHandle, " No NVE database is availabe for this VNI \r\n");
        return CLI_FAILURE;
    }
    do
    {
        MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
        MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType == 0)
        {
            /* EVPN mac entires should not be cleared */
            pVxlanFsVxlanNextNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanNveEntry, NULL);
            pVxlanNveEntry = pVxlanFsVxlanNextNveEntry;
            continue;
        }
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
            pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
            pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
            (UINT4) pVxlanNveEntry->i4VlanId;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
            pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
            pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
                &(pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress),
                pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1RemoteVtepAddress),
                &(pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress),
                pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);
        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac),
                &(zeroAddr), VXLAN_ETHERNET_ADDR_SIZE);

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel =
            pVxlanNveEntry->u4OutVlanId;

        VxlanHwInfo.u4InfoType = VXLAN_HW_NVE_DATABASE;

#ifdef NPAPI_WANTED
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.b1NveFlushAllVmMacOnly = OSIX_TRUE;

        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (NpUtilHwProgram (&FsHwNp) == FNP_FAILURE)
        {
            CliPrintf (CliHandle,
                       " Failed to remove Mac entries from h/w \r\n");
            return CLI_FAILURE;
        }
#endif

        pVxlanFsVxlanNextNveEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanNveEntry, NULL);
        pVxlanNveEntry = pVxlanFsVxlanNextNveEntry;
    }                            /* }Do while loop */
    while ((pu4VniID == NULL) && (pVxlanFsVxlanNextNveEntry != NULL));
    return CLI_SUCCESS;
}

INT4
VxlanRemoveVniNveEntries (tCliHandle CliHandle, UINT4 *pu4VniID)
{
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextNveEntry = NULL;
    UINT4               u4NveIndex = 0;
    tMacAddr            zeroAddr;

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    if (pu4VniID != NULL)
    {
        VxlanUtilGetNveIndexFromNveTable (*pu4VniID, &u4NveIndex);

        if (u4NveIndex == 0)
        {
            CliPrintf (CliHandle,
                       " No NVE database is availabe for this VNI \r\n");
            return CLI_FAILURE;
        }
    }
    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);

    while (pVxlanNveEntry != NULL)
    {
        pVxlanFsVxlanNextNveEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanNveEntry, NULL);

        if ((MEMCMP (pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                     zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0) &&
            (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
             VXLAN_STRG_TYPE_VOL))

        {
            if (((pu4VniID != 0) &&
                 (pVxlanNveEntry->
                  MibObject.u4FsVxlanNveVniNumber == *pu4VniID) &&
                 (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                  (INT4) u4NveIndex)) || (pu4VniID == 0))
            {
                if (pVxlanNveEntry->i4FsVxlanNveEvpnMacType !=
                    VXLAN_NVE_EVPN_MAC)
                {
                    if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
                        == VXLAN_FAILURE)
                    {
                        VXLAN_TRC ((VXLAN_UTIL_TRC,
                                    "Failed to delete Nve entry from h/w\r\n"));
                    }

                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanNveEntry);
                }
            }
        }
        pVxlanNveEntry = pVxlanFsVxlanNextNveEntry;
    }

    return CLI_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/*******************************************************************************
 * Function Name             : VxlanHwUpdateEcmpNveDatabase
 *
 * Description               : This function sets VXLAN Ecmp nve databse in H/W
 *
 * Input                     : pVxlanEcmpNveEntry - VXLAN Ecmp nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 *******************************************************************************/
UINT1
VxlanHwUpdateEcmpNveDatabase (tVxlanFsVxlanEcmpNveEntry * pVxlanEcmpNveEntry,
                              UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4IpAddress = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tIpConfigInfo       IpIfInfo;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4RouteMask = 0xffffffff;
    INT4                i4SlotId = 0;
    UINT1               u1IfType = 0;
#ifdef NPAPI_WANTED
    tMacAddr            zeroAddr;
    UINT1               u1Status = 0;
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
    UINT4               u4IfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateNveDatabase: Entry\n"));
    if (((u1Flag == VXLAN_HW_DEL) &&
         (pVxlanEcmpNveEntry->bHwSet == FALSE)) ||
        ((u1Flag == VXLAN_HW_ADD) && (pVxlanEcmpNveEntry->bHwSet == TRUE)))
    {
        return VXLAN_SUCCESS;
    }

    if (pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType == 0)
    {
#ifdef EVPN_VXLAN_WANTED
        if (pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
            EVPN_CLI_ARP_SUPPRESS)
        {
            VxlanHwUpdateArpSuppression (pVxlanNveEntry, u1Flag);
        }
#endif
        return VXLAN_SUCCESS;
    }

#ifndef NPAPI_WANTED
#ifdef EVPN_VXLAN_WANTED
    if (u1Flag == VXLAN_HW_ADD)
    {
        VlanAddEvpnFdbEntry (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                             (UINT2) pVxlanNveEntry->i4VlanId,
                             pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);
    }
    else
    {
        VlanRemoveFdbEntry (0, (UINT2) pVxlanNveEntry->i4VlanId,
                            pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac);
    }
#endif
#endif

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
        pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex;

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
        pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber;

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
        (UINT4) pVxlanEcmpNveEntry->i4VlanId;
    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = VXLAN_NVE_STATIC_MAC;
    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
        pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType;

    VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
        pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType;

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
            &(pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpNveVtepAddress),
            pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen);

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1RemoteVtepAddress),
            &(pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpNveRemoteVtepAddress),
            pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressLen);

    MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac),
            &(pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);

    MEMCPY (&u4IpAddress,
            pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
            VXLAN_IP4_ADDR_LEN);

    RtQuery.u4DestinationSubnetMask = u4RouteMask;
    RtQuery.u1QueryFlag = 0x01;
    RtQuery.u4DestinationIpAddress = OSIX_HTONL (u4IpAddress);

    if (u1Flag == VXLAN_HW_DEL)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel =
            pVxlanEcmpNveEntry->u4OutVlanId;
    }
    else
    {
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4IfaceIndex) == NETIPV4_SUCCESS)
            {
                CfaGetIfType (u4IfaceIndex, &u1IfType);

                pVxlanEcmpNveEntry->u4VxlanTunnelIfIndex =
                    NetIpRtInfo.u4RtIfIndx;

                if (u1IfType == CFA_L3IPVLAN)
                {
                    CfaGetSlotAndPortFromIfIndex (u4IfaceIndex, &i4SlotId,
                                                  (INT4 *) &VxlanHwInfo.
                                                  unHwParam.VxlanHwNveInfo.
                                                  u4VlanIdOutTunnel);
                }
                else if (u1IfType == CFA_ENET)
                {
                    if (CfaIpIfGetIfInfo (u4IfaceIndex, &IpIfInfo) ==
                        CFA_SUCCESS)
                    {
                        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel
                            = IpIfInfo.u2PortVlanId;
                    }
                }
                if (u1OperStatus == CFA_IF_DOWN)
                {
                    VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel = 0;
                }

            }
        }
    }
#ifdef NPAPI_WANTED
    VxlanHwInfo.u4InfoType = VXLAN_HW_NVE_DATABASE;
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    if (u1Flag == VXLAN_HW_ADD)
    {
        u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             u4IfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
        pVxlanEcmpNveEntry->bHwSet = TRUE;
        if (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                    zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0)
        {
#if defined (SW_LEARNING)

            u4IfaceIndex = 0;
            if (VlanGetFdbEntryDetails ((UINT4) pVxlanEcmpNveEntry->i4VlanId,
                                        pVxlanEcmpNveEntry->MibObject.
                                        FsVxlanEcmpNveDestVmMac,
                                        (UINT2 *) &u4IfaceIndex,
                                        &u1Status) == VLAN_SUCCESS)
            {
                VlanHwDelFdbEntry (u4IfaceIndex, pVxlanEcmpNveEntry->MibObject.
                                   FsVxlanEcmpNveDestVmMac,
                                   (UINT2) pVxlanEcmpNveEntry->i4VlanId);
            }

            VlanFdbTableAdd (pVxlanEcmpNveEntry->MibObject.
                             i4FsVxlanEcmpNveIfIndex,
                             pVxlanEcmpNveEntry->MibObject.
                             FsVxlanEcmpNveDestVmMac,
                             (UINT2) pVxlanEcmpNveEntry->i4VlanId,
                             VLAN_FDB_LEARNT, FNP_ZERO);
#endif
        }
#ifdef EVPN_VXLAN_WANTED
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType == 0)
        {
            pVxlanNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               (tRBElem *) pVxlanNveEntry, NULL);
            if ((pVxlanNveEntry == NULL) ||
                (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber !=
                 u4VniNumber))
            {
                return;
            }
        }
        pVxlanEcmpNveEntry =
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable);

        while ((pVxlanEcmpNveEntry != NULL) &&
               (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                        ZeroMac, sizeof (tMacAddr)) == 0))

        {
            pVxlanTmpEcmpNveEntry = NULL;

            pVxlanTmpEcmpNveEntry = VxlanUtilGetEcmpNveDatabase
                (u4VniNumber, NpMacAddr, u4NveIndex,
                 pVxlanEcmpNveEntry->MibObject.
                 i4FsVxlanEcmpNveRemoteVtepAddressType,
                 pVxlanEcmpNveEntry->MibObject.
                 au1FsVxlanEcmpNveRemoteVtepAddress);

            if (pVxlanTmpEcmpNveEntry != NULL)
            {
                return;
            }
            pVxlanEcmpNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                               (tRBElem *) pVxlanEcmpNveEntry, NULL);

        }

#endif
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex >
            CFA_MAX_NVE_IF_INDEX)
        {
            u4NveIndex = pVxlanNveEntry->u4OrgNveIfIndex;
        }

        if (VxlanUtilGetNveDatabase (u4VniNumber, NpMacAddr, u4NveIndex) !=
            NULL)
        {
            return;
        }

        CfaApiGetIfAdminStatus (u4NveIndex, &u1IfAdminStatus);
        if (u1IfAdminStatus != CFA_IF_UP)
        {
            FsNpHwVxlanInfo (&VxlanHwInfo, 0);
            return;
        }

    }
    else
    {
        u4IfIndex = VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex;
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             u4IfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
        pVxlanEcmpNveEntry->bHwSet = FALSE;
        if (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                    zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0)
        {
#if defined (SW_LEARNING)
            u4IfaceIndex = 0;
            VlanGetFdbEntryDetails ((UINT4) pVxlanEcmpNveEntry->i4VlanId,
                                    pVxlanEcmpNveEntry->MibObject.
                                    FsVxlanEcmpNveDestVmMac,
                                    (UINT2 *) &u4IfaceIndex, &u1Status);

            if (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
                (INT4) u4IfaceIndex)
            {

                VlanRemoveFdbEntry (0, (UINT2) pVxlanEcmpNveEntry->i4VlanId,
                                    pVxlanEcmpNveEntry->MibObject.
                                    FsVxlanEcmpNveDestVmMac);
            }
#endif
        }

    }
#endif
    pVxlanEcmpNveEntry->u4OutVlanId =
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel;
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateEcmpNveDatabase: Exit\n"));

    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHwAddLoopBackPortToVlan
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanNveEntry - VXLAN nve databse.
 *                             u1Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanHwAddLoopBackPortToVlan (tVxlanFsEvpnVxlanVrfEntry * pVxlanVrfEntry,
                              UINT1 u1Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT2               u2VlanId = 0;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwAddLoopBackPortToVlan: Entry\n"));
    if (((u1Flag == VXLAN_HW_DEL) && (pVxlanVrfEntry->bHwSet == FALSE)) ||
        ((u1Flag == VXLAN_HW_ADD) && (pVxlanVrfEntry->bHwSet == TRUE)))
    {
        return VXLAN_SUCCESS;
    }
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif
    VxlanHwInfo.unHwParam.VxlanHwVlanPortInfo.u4VniNumber =
        EVPN_P_VRF_VNI (pVxlanVrfEntry);

    if (VxlanUtilGetVlanFromVni (EVPN_P_VRF_VNI (pVxlanVrfEntry), &u2VlanId)
        != VXLAN_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:VxlanHwAddLoopBackPortToVlan: Failed to get VLAN\n"));
    }
    VxlanHwInfo.unHwParam.VxlanHwVlanPortInfo.u4VlanId = u2VlanId;

#ifdef NPAPI_WANTED
    VxlanHwInfo.u4InfoType = VXLAN_HW_VLAN_PORT;
    if (u1Flag == VXLAN_HW_ADD)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
        pVxlanVrfEntry->bHwSet = TRUE;

    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));
        if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
        {
            return VXLAN_FAILURE;
        }
        pVxlanVrfEntry->bHwSet = FALSE;

    }
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwAddLoopBackPortToVlan: Exit\n"));

    return VXLAN_SUCCESS;
}

#endif
/***************************************************************************
 *
 *    Function Name       : VxlanNpWrHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tVxlanNpModInfo
 *
 *    Input(s)            : FsHwNpParam of type tfsHwNp
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

#ifdef NPAPI_WANTED
PUBLIC UINT1
VxlanNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    tVxlanHwInfo       *pVxlanHwInfo;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }
    pVxlanHwInfo = &(pFsHwNp->VxlanNpModInfo);
    if (FsNpHwVxlanInfo (pVxlanHwInfo, (UINT1) (pFsHwNp->u4Opcode)) ==
        FNP_FAILURE)
    {
        return FNP_FAILURE;
    }
    return FNP_SUCCESS;
}
#endif

INT4
VxlanHwGetVniStats (UINT4 u4VniId)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VXLAN_MOD,    /* Module ID */
                         VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pVxlanNpModInfo->u4InfoType = VXLAN_HW_GET_STATS;
    pVxlanNpModInfo->VxlanNpHwVniVlanInfo.u4VniNumber = u4VniId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return VXLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4VniId);
#endif
    return VXLAN_SUCCESS;
}

INT4
VxlanHwGetVniRcvdStats (UINT4 u4VniId)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VXLAN_MOD,    /* Module ID */
                         VXLAN_HW_CLEAR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pVxlanNpModInfo->u4InfoType = VXLAN_HW_GET_STATS;
    pVxlanNpModInfo->VxlanNpHwVniVlanInfo.u4VniNumber = u4VniId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return VXLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4VniId);
#endif
    return VXLAN_SUCCESS;
}

#ifdef MBSM_WANTED
#ifdef NPAPI_WANTED
/***************************************************************************
 * Function Name             : VxlanMbsmHwInit
 *
 * Description               : This function init/deinit VXLAN  in H/W
 *
 * Input                     : u4FsVxlanUdpPort - VXLAN Udp port number.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanMbsmHwInit (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    /*tVxlanMbsmHwInitInfo    *pEntry = NULL; */

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    /* Disabling VXLAN is not required for card removal */
    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_INIT,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    /*    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwInitInfo;
       pEntry->pSlotInfo = pSlotInfo; */

    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_INIT;
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanHwInit: Exit\n"));

    return VXLAN_SUCCESS;
    UNUSED_PARAM (pSlotInfo);
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateUdpPort
 *
 * Description               : This function sets VXLAN udp port number in H/W
 *
 * Input                     : u4FsVxlanUdpPort - VXLAN Udp port number.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanMbsmHwUpdateUdpPort (UINT4 u4VxlanUdpPort, tMbsmSlotInfo * pSlotInfo,
                          UINT4 u4Flag)
{

    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwUdpPortInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    /* Clearing the UDP config is not required for card removal */
    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_UDP_PORT,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_UDP_PORT;
    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwUdpPortInfo;
    pEntry->u4VxlanUdpPortNo = u4VxlanUdpPort;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateUdpPort: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateNveDatabase
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanHwNveInfo - VXLAN nve databse.
 *                             pSlotInfo - Slot Info of the card which is 
 *                             inserted or removed. 
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanMbsmHwUpdateNveDatabase (tVxlanHwNveInfo * pVxlanHwNveInfo,
                              tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwNveInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_NVE_DATABASE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

    }
    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_NVE_DATABASE;

    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;

    pEntry->i4NveIfIndex = pVxlanHwNveInfo->i4NveIfIndex;
    pEntry->u4IfIndex = pVxlanHwNveInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwNveInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwNveInfo->u4VlanId;
    pEntry->u4VlanIdOutTunnel = pVxlanHwNveInfo->u4VlanIdOutTunnel;
    pEntry->u4VrId = pVxlanHwNveInfo->u4VrId;
    pEntry->i4VtepAddressType = pVxlanHwNveInfo->i4VtepAddressType;
    pEntry->i4RemoteVtepAddressType = pVxlanHwNveInfo->i4RemoteVtepAddressType;
    pEntry->u4NetworkPort = pVxlanHwNveInfo->u4NetworkPort;

    MEMCPY (pEntry->au1VtepAddress, pVxlanHwNveInfo->au1VtepAddress,
            sizeof (pVxlanHwNveInfo->au1VtepAddress));

    MEMCPY (pEntry->au1RemoteVtepAddress, pVxlanHwNveInfo->au1RemoteVtepAddress,
            sizeof (pVxlanHwNveInfo->au1RemoteVtepAddress));

    MEMCPY (pEntry->DestVmMac, pVxlanHwNveInfo->DestVmMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    pEntry->b1NveFlushAllVmMacOnly = pVxlanHwNveInfo->b1NveFlushAllVmMacOnly;
    pEntry->u1MacType = pVxlanHwNveInfo->u1MacType;
    MEMCPY (pEntry->au1NextHopMac, pVxlanHwNveInfo->au1NextHopMac,
            MAC_ADDR_LEN);

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "FUNC:: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateNveDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateL2Table
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanHwNveInfo - VXLAN nve databse.
 *                             pSlotInfo - Slot Info of the card which is 
 *                             inserted or removed. 
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanMbsmHwUpdateL2Table (tVxlanHwNveInfo * pVxlanHwNveInfo,
                          tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwNveInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_NVE_L2_TABLE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

    }
    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_NVE_L2_TABLE;

    /*pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;

       pEntry->u4VniNumber = pVxlanHwNveInfo->u4VniNumber;

       MEMCPY(pEntry->DestVmMac, pVxlanHwNveInfo->DestVmMac,
       VXLAN_ETHERNET_ADDR_SIZE);

       pEntry->u1MacType = pVxlanHwNveInfo->u1MacType;

       pEntry->pSlotInfo = pSlotInfo; */

    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;

    pEntry->i4NveIfIndex = pVxlanHwNveInfo->i4NveIfIndex;
    pEntry->u4IfIndex = pVxlanHwNveInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwNveInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwNveInfo->u4VlanId;
    pEntry->u4VlanIdOutTunnel = pVxlanHwNveInfo->u4VlanIdOutTunnel;
    pEntry->u4VrId = pVxlanHwNveInfo->u4VrId;
    pEntry->i4VtepAddressType = pVxlanHwNveInfo->i4VtepAddressType;
    pEntry->i4RemoteVtepAddressType = pVxlanHwNveInfo->i4RemoteVtepAddressType;

    MEMCPY (pEntry->au1VtepAddress, pVxlanHwNveInfo->au1VtepAddress,
            sizeof (pVxlanHwNveInfo->au1VtepAddress));

    MEMCPY (pEntry->au1RemoteVtepAddress, pVxlanHwNveInfo->au1RemoteVtepAddress,
            sizeof (pVxlanHwNveInfo->au1RemoteVtepAddress));

    MEMCPY (pEntry->DestVmMac, pVxlanHwNveInfo->DestVmMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    pEntry->b1NveFlushAllVmMacOnly = pVxlanHwNveInfo->b1NveFlushAllVmMacOnly;
    pEntry->u1MacType = pVxlanHwNveInfo->u1MacType;
    MEMCPY (pEntry->au1NextHopMac, pVxlanHwNveInfo->au1NextHopMac,
            MAC_ADDR_LEN);

    pEntry->u4NetworkPort = pVxlanHwNveInfo->u4NetworkPort;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "FUNC:: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateNveDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmVniVlanPortMapDatabase
 *
 * Description               : This function sets VXLAN vni vlan map database in H/W
 *
 * Input                     : pVxlanVniVlantEntry - VXLAN vni vlan map database.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanMbsmVniVlanPortMapDatabase (tVxlanHwVniVlanInfo * pVxlanHwVniVlanInfo,
                                 tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwVniVlanInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

    }
    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING;

    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwVniVlanInfo;

    pEntry->u4IfIndex = pVxlanHwVniVlanInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwVniVlanInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwVniVlanInfo->u4VlanId;
    pEntry->u4PktSent = pVxlanHwVniVlanInfo->u4PktSent;
    pEntry->u4PktRcvd = pVxlanHwVniVlanInfo->u4PktRcvd;
    pEntry->u4VrId = pVxlanHwVniVlanInfo->u4VrId;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "FUNC:VxlanMbsmVniVlanPortMapDatabase: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateVniVlanPortMapDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmVniVlanMapDatabase  
 *
 * Description               : This function sets VXLAN vni vlan map database in H/W
 *
 * Input                     : pVxlanVniVlantEntry - VXLAN vni vlan map database.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanMbsmVniVlanMapDatabase (tVxlanHwVniVlanInfo * pVxlanHwVniVlanInfo,
                             tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwVniVlanInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_VNI_VLAN_MAPPING,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */

    }
    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_VNI_VLAN_MAPPING;

    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwVniVlanInfo;

    pEntry->u4VniNumber = pVxlanHwVniVlanInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwVniVlanInfo->u4VlanId;
    pEntry->u4PktSent = pVxlanHwVniVlanInfo->u4PktSent;
    pEntry->u4PktRcvd = pVxlanHwVniVlanInfo->u4PktRcvd;
    pEntry->u4VrId = pVxlanHwVniVlanInfo->u4VrId;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "FUNC:VxlanMbsmHwUpdateVniVlanMapDatabase: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateVniVlanMapDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateInReplicaDatabase
 *
 * Description               : This function sets VXLAN Replica databse in H/W
 *
 * Input                     :  - VXLAN replica databse.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanMbsmHwUpdateInReplicaDatabase (tVxlanHwBUMReplicaInfo * pVxlanHwRepInfo,
                                    tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwBUMReplicaInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }

    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE;
    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwBUMReplicaInfo;

    pEntry->i4NveIfIndex = pVxlanHwRepInfo->i4NveIfIndex;
    pEntry->u4IfIndex = pVxlanHwRepInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwRepInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwRepInfo->u4VlanId;
    pEntry->u4VlanIdOutTunnel = pVxlanHwRepInfo->u4VlanIdOutTunnel;
    pEntry->u4VrId = pVxlanHwRepInfo->u4VrId;
    pEntry->i4VtepAddressType = pVxlanHwRepInfo->i4VtepAddressType;
    pEntry->i4RemoteVtepAddressType = pVxlanHwRepInfo->i4RemoteVtepAddressType;
    pEntry->u4NetworkPort = pVxlanHwRepInfo->u4NetworkPort;
    pEntry->u4NetworkBUMPort = pVxlanHwRepInfo->u4NetworkBUMPort;

    MEMCPY (pEntry->au1VtepAddress, pVxlanHwRepInfo->au1VtepAddress,
            sizeof (pVxlanHwRepInfo->au1VtepAddress));

    MEMCPY (pEntry->au1RemoteVtepAddress, pVxlanHwRepInfo->au1RemoteVtepAddress,
            sizeof (pVxlanHwRepInfo->au1RemoteVtepAddress));

    pEntry->pau1RemoteVtepsReplicaTo =
        pVxlanHwRepInfo->pau1RemoteVtepsReplicaTo;
    pEntry->u1ArpSupFlag = pVxlanHwRepInfo->u1ArpSupFlag;
    MEMCPY (pEntry->au1NextHopMac, pVxlanHwRepInfo->au1NextHopMac,
            MAC_ADDR_LEN);

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "FUNC:VxlanHwUpdateMcastDatabase: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateMcastDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateReplicaMCast
 *
 * Description               : This function sets VXLAN Replica databse in H/W
 *
 * Input                     :  - VXLAN replica databse.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanMbsmHwUpdateReplicaMCast (tVxlanHwBUMReplicaInfo * pVxlanHwRepInfo,
                               tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwBUMReplicaInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_MBSM_HW_BUM_REPLICA_MCAST,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }

    pVxlanNpModInfo->u4InfoType = VXLAN_MBSM_HW_BUM_REPLICA_MCAST;
    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwBUMReplicaInfo;

    pEntry->u4IfIndex = pVxlanHwRepInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwRepInfo->u4VniNumber;
    pEntry->u4NetworkPort = pVxlanHwRepInfo->u4NetworkPort;
    pEntry->u4NetworkBUMPort = pVxlanHwRepInfo->u4NetworkBUMPort;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "FUNC:VxlanHwUpdateMcastDatabase: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateMcastDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}
#endif
#endif
#if 0
/*******************************************************************************
 * Function Name             : VxlanMbsmHwUpdateEcmpNveDatabase
 *
 * Description               : This function sets VXLAN Ecmp nve databse in H/W
 *
 * Input                     : pVxlanEcmpNveEntry - VXLAN Ecmp nve databse.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 *******************************************************************************/
UINT1
VxlanMbsmHwUpdateEcmpNveDatabase (tVxlanHwNveInfo * pVxlanHwNveInfo,
                                  tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwNveInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CONFIGURE,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    else
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_VXLAN_MOD,    /* Module ID */
                             VXLAN_HW_CLEAR,    /* Function/OpCode */
                             0,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
    }
    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;

    pEntry->i4NveIfIndex = pVxlanHwNveInfo->i4NveIfIndex;
    pEntry->u4IfIndex = pVxlanHwNveInfo->u4IfIndex;
    pEntry->u4VniNumber = pVxlanHwNveInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwNveInfo->u4VlanId;
    pEntry->u4VlanIdOutTunnel = pVxlanHwNveInfo->u4VlanIdOutTunnel;
    pEntry->u4VrId = pVxlanHwNveInfo->u4VrId;
    pEntry->i4VtepAddressType = pVxlanHwNveInfo->i4VtepAddressType;
    pEntry->i4RemoteVtepAddressType = pVxlanHwNveInfo->i4RemoteVtepAddressType;

    MEMCPY (pEntry->au1VtepAddress, pVxlanHwNveInfo->au1VtepAddress,
            sizeof (pVxlanHwNveInfo->au1VtepAddress));

    MEMCPY (pEntry->au1RemoteVtepAddress, pVxlanHwNveInfo->au1RemoteVtepAddress,
            sizeof (pVxlanHwNveInfo->au1RemoteVtepAddress));

    MEMCPY (pEntry->DestVmMac, pVxlanHwNveInfo->DestVmMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    pEntry->b1NveFlushAllVmMacOnly = pVxlanHwNveInfo->b1NveFlushAllVmMacOnly;
    pEntry->u1MacType = pVxlanHwNveInfo->u1MacType;
    MEMCPY (pEntry->au1NextHopMac, pVxlanHwNveInfo->au1NextHopMac,
            MAC_ADDR_LEN);

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "FUNC:: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanMbsmHwUpdateUdpPort: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanMbsmHwUpdateMcastDatabase
 *
 * Description               : This function sets VXLAN multicast database in H/W
 *
 * Input                     : pVxlanMCastEntry - VXLAN multicast database.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

UINT1
VxlanMbsmHwUpdateMcastDatabase (tVxlanHwMcastInfo pVxlanHwMcastInfo,
                                tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanMbsmHwMcastInfo *pEntry = NULL;

    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VXLAN_MOD,    /* Module ID */
                         VXLAN_MBSM_HW_MCAST_DATABASE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pEntry = &pVxlanNpModInfo->VxlanNpMbsmHwMcastInfo;

    pEntry->i4NveIfIndex = pVxlanHwMcastInfo->i4NveIfIndex;
    pEntry->u4VniNumber = pVxlanHwMcastInfo->u4VniNumber;
    pEntry->u4VlanId = pVxlanHwMcastInfo->u4VlanId;
    pEntry->u4VlanIdOutTunnel = pVxlanHwMcastInfo->u4VlanIdOutTunnel;
    pEntry->u4VrId = pVxlanHwMcastInfo->u4VrId;
    pEntry->i4VtepAddressType = pVxlanHwMcastInfo->i4VtepAddressType;
    pEntry->i4GroupAddressType = pVxlanHwMcastInfo->i4GroupAddressType;

    MEMCPY (pEntry->au1VtepAddress, pVxlanHwMcastInfo->au1VtepAddress,
            sizeof (pVxlanHwMcastInfo->au1VtepAddress));

    MEMCPY (pEntry->au1GroupAddress, pVxlanHwMcastInfo->au1GroupAddress,
            sizeof (pVxlanHwMcastInfo->au1RemoteVtepAddress));

    MEMCPY (pEntry->McastMac, pVxlanHwMcastInfo->McastMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "FUNC:VxlanHwUpdateMcastDatabase: NpUtilHwProgram failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateMcastDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}
#endif
tVxlanNvePortEntry *
VxlanCheckRouteAndUpdateTunnelMapTable (UINT1 *pau1Address, INT4 i4AddrLen,
                                        UINT4 u4VxlanTunnelIfIndex)
{
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4IpAddress = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;
    UINT1               au1DstMac[MAC_ADDR_LEN];
    UINT4               u4RouteMask = 0xffffffff;
    UINT4               u4IfaceIndex = 0;
    INT4                i4SlotId = 0;
    INT4                i4PortVlanId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1EncapType = 0;
    UINT1               u1OperStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));
    MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, pau1Address, i4AddrLen);
    VxlanNvePortEntry.i4RemoteVtepAddressLen = i4AddrLen;

    pVxlanNvePortEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                                    (tRBElem *) & VxlanNvePortEntry);

    if ((pVxlanNvePortEntry != NULL) &&
        (pVxlanNvePortEntry->u4VxlanTunnelIfIndex != CFA_INVALID_INDEX))
    {
        pVxlanNvePortEntry->u4RefCount++;
        return pVxlanNvePortEntry;
    }

    MEMCPY (&u4IpAddress, pau1Address, i4AddrLen);
    RtQuery.u4DestinationSubnetMask = u4RouteMask;
    RtQuery.u1QueryFlag = 0x01;
    RtQuery.u4DestinationIpAddress = OSIX_HTONL (u4IpAddress);
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if ((u4VxlanTunnelIfIndex != NetIpRtInfo.u4RtIfIndx) &&
            (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                           &u4IfaceIndex) == NETIPV4_SUCCESS))
        {
            CfaGetIfType (u4IfaceIndex, &u1IfType);
            CfaGetIfOperStatus (u4IfaceIndex, &u1OperStatus);

            if (u1IfType == CFA_L3IPVLAN)
            {
                i4RetVal = CfaGetSlotAndPortFromIfIndex (u4IfaceIndex,
                                                         &i4SlotId,
                                                         &i4PortVlanId);
            }
            else if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
            {
                i4RetVal = CfaGetIfIpPortVlanId (u4IfaceIndex, &i4PortVlanId);
            }
            if ((u1OperStatus == CFA_IF_DOWN) || (i4PortVlanId == 0))
            {
                return NULL;
            }

            if (NetIpRtInfo.u4NextHop == 0)
            {
                NetIpRtInfo.u4NextHop = RtQuery.u4DestinationIpAddress;
            }
            if (ArpResolve (NetIpRtInfo.u4NextHop, (INT1 *) au1DstMac,
                            &u1EncapType) != ARP_SUCCESS)
            {
                return NULL;
            }

            if ((pVxlanNvePortEntry != NULL) &&
                (pVxlanNvePortEntry->u4VxlanTunnelIfIndex == CFA_INVALID_INDEX))
            {
                MEMCPY (pVxlanNvePortEntry->au1NextHopMac,
                        au1DstMac, MAC_ADDR_LEN);
                pVxlanNvePortEntry->u4VxlanTunnelIfIndex =
                    NetIpRtInfo.u4RtIfIndx;
                pVxlanNvePortEntry->u4VlanIdOutTunnel = (UINT4) i4PortVlanId;
                pVxlanNvePortEntry->u4RefCount = 1;
            }
            else
            {
                pVxlanNvePortEntry = VxlanNvePortTableAdd (pau1Address,
                                                           i4AddrLen);

                if (pVxlanNvePortEntry != NULL)
                {
                    MEMCPY (pVxlanNvePortEntry->au1NextHopMac, au1DstMac,
                            MAC_ADDR_LEN);

                    pVxlanNvePortEntry->u4VxlanTunnelIfIndex =
                        NetIpRtInfo.u4RtIfIndx;
                    pVxlanNvePortEntry->u4VlanIdOutTunnel =
                        (UINT4) i4PortVlanId;
                    pVxlanNvePortEntry->u4RefCount = 1;
                    pVxlanNvePortEntry->u4NetworkPort = 0;
                    pVxlanNvePortEntry->u4NetworkBUMPort = 0;
                }
            }
        }
    }
    UNUSED_PARAM (i4RetVal);
    return pVxlanNvePortEntry;
}
