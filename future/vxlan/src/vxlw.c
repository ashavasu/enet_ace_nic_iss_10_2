/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxlw.c,v 1.5 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains some of low level functions used by protocol in Vxlan module
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVxlanVtepTable
 Input       :  The Indices
                FsVxlanVtepNveIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanVtepTable (INT4 i4FsVxlanVtepNveIfIndex)
{
    UNUSED_PARAM (i4FsVxlanVtepNveIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVxlanNveTable
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanNveTable (INT4 i4FsVxlanNveIfIndex,
                                         UINT4 u4FsVxlanNveVniNumber,
                                         tMacAddr u1FsVxlanNveDestVmMac)
{
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVxlanMCastTable
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanMCastTable (INT4 i4FsVxlanMCastNveIfIndex,
                                           UINT4 u4FsVxlanMCastVniNumber)
{
    UNUSED_PARAM (i4FsVxlanMCastNveIfIndex);
    UNUSED_PARAM (u4FsVxlanMCastVniNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVxlanVniVlanMapTable
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanVniVlanMapTable (INT4 i4FsVxlanVniVlanMapVlanId)
{
    UNUSED_PARAM (i4FsVxlanVniVlanMapVlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVxlanInReplicaTable
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanInReplicaTable (INT4
                                               i4FsVxlanInReplicaNveIfIndex,
                                               UINT4
                                               u4FsVxlanInReplicaVniNumber,
                                               INT4 i4FsVxlanInReplicaIndex)
{
    UNUSED_PARAM (i4FsVxlanInReplicaNveIfIndex);
    UNUSED_PARAM (u4FsVxlanInReplicaVniNumber);
    UNUSED_PARAM (i4FsVxlanInReplicaIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanEviVniMapTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanEviVniMapTable (INT4
                                                   i4FsEvpnVxlanEviVniMapEviIndex,
                                                   UINT4
                                                   u4FsEvpnVxlanEviVniMapVniNumber)
{
#ifdef EVPN_VXLAN_WANTED
    if (((i4FsEvpnVxlanEviVniMapEviIndex >= MIN_EVPN_VALUE &&
          i4FsEvpnVxlanEviVniMapEviIndex <= MAX_EVPN_VALUE) &&
         (u4FsEvpnVxlanEviVniMapVniNumber >= MIN_VNI_VALUE &&
          u4FsEvpnVxlanEviVniMapVniNumber <= MAX_VNI_VALUE)) != OSIX_TRUE)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanBgpRTTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanBgpRTTable (INT4
                                               i4FsEvpnVxlanEviVniMapEviIndex,
                                               UINT4
                                               u4FsEvpnVxlanEviVniMapVniNumber,
                                               UINT4 u4FsEvpnVxlanBgpRTIndex,
                                               INT4 i4FsEvpnVxlanBgpRTType)
{
#ifdef EVPN_VXLAN_WANTED
    if (((i4FsEvpnVxlanEviVniMapEviIndex >= MIN_EVPN_VALUE &&
          i4FsEvpnVxlanEviVniMapEviIndex <= MAX_EVPN_VALUE) &&
         (u4FsEvpnVxlanEviVniMapVniNumber >= MIN_VNI_VALUE &&
          u4FsEvpnVxlanEviVniMapVniNumber <= MAX_VNI_VALUE)) != OSIX_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (u4FsEvpnVxlanBgpRTIndex <= 0)
    {
        return SNMP_FAILURE;
    }
    if ((i4FsEvpnVxlanBgpRTType < EVPN_BGP_RT_IMPORT) ||
        (i4FsEvpnVxlanBgpRTType >= EVPN_BGP_MAX_RT_TYPE))
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanVrfTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanVrfTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsEvpnVxlanVrfName,
                                             UINT4
                                             u4FsEvpnVxlanEviVniMapVniNumber)
{
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanVrfRTTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanVrfRTTable (tSNMP_OCTET_STRING_TYPE *
                                               pFsEvpnVxlanVrfName,
                                               UINT4
                                               u4FsEvpnVxlanEviVniMapVniNumber,
                                               UINT4 u4FsEvpnVxlanVrfRTIndex,
                                               INT4 i4FsEvpnVxlanVrfRTType)
{
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanMultihomedPeerTable
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanMultihomedPeerTable (INT4
                                                        i4FsEvpnVxlanPeerIpAddressType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsEvpnVxlanPeerIpAddress,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsEvpnVxlanMHEviVniESI)
{
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (pFsEvpnVxlanMHEviVniESI);
    return SNMP_SUCCESS;
}
