/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxlwg.c,v 1.8 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Vxlan 
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanVtepTable
 Input       :  The Indices
                FsVxlanVtepNveIfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanVtepTable (INT4 *pi4FsVxlanVtepNveIfIndex)
{

    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry = VxlanGetFirstFsVxlanVtepTable ();

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanVtepNveIfIndex =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanNveTable
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanNveTable (INT4 *pi4FsVxlanNveIfIndex,
                                 UINT4 *pu4FsVxlanNveVniNumber,
                                 tMacAddr * pFsVxlanNveDestVmMac)
{

    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry = VxlanGetFirstFsVxlanNveTable ();

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanNveIfIndex =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

    *pu4FsVxlanNveVniNumber =
        pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

    MEMCPY (pFsVxlanNveDestVmMac,
            &(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanMCastTable
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanMCastTable (INT4 *pi4FsVxlanMCastNveIfIndex,
                                   UINT4 *pu4FsVxlanMCastVniNumber)
{

    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry = VxlanGetFirstFsVxlanMCastTable ();

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanMCastNveIfIndex =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;

    *pu4FsVxlanMCastVniNumber =
        pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanVniVlanMapTable
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanVniVlanMapTable (INT4 *pi4FsVxlanVniVlanMapVlanId)
{

    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry = VxlanGetFirstFsVxlanVniVlanMapTable ();

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanVniVlanMapVlanId =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanInReplicaTable
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber                
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanInReplicaTable (INT4 *pi4FsVxlanInReplicaNveIfIndex,
                                       UINT4 *pu4FsVxlanInReplicaVniNumber,
                                       INT4 *pi4FsVxlanInReplicaIndex)
{

    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry = VxlanGetFirstFsVxlanInReplicaTable ();

    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanInReplicaNveIfIndex =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex;
    *pu4FsVxlanInReplicaVniNumber =
        pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;
    *pi4FsVxlanInReplicaIndex =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanEviVniMapTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanEviVniMapTable (INT4
                                           *pi4FsEvpnVxlanEviVniMapEviIndex,
                                           UINT4
                                           *pu4FsEvpnVxlanEviVniMapVniNumber)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry = VxlanGetFirstFsEvpnVxlanEviVniMapTable ();

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsEvpnVxlanEviVniMapEviIndex =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex;

    *pu4FsEvpnVxlanEviVniMapVniNumber =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (pu4FsEvpnVxlanEviVniMapVniNumber);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanBgpRTTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanBgpRTTable (INT4 *pi4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 *pu4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4 *pu4FsEvpnVxlanBgpRTIndex,
                                       INT4 *pi4FsEvpnVxlanBgpRTType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry = VxlanGetFirstFsEvpnVxlanBgpRTTable ();

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsEvpnVxlanEviVniMapEviIndex =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex;

    *pu4FsEvpnVxlanEviVniMapVniNumber =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber;

    *pu4FsEvpnVxlanBgpRTIndex =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex;

    *pi4FsEvpnVxlanBgpRTType =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (*pi4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (*pu4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pu4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (*pi4FsEvpnVxlanBgpRTType);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanVrfTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanVrfTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsEvpnVxlanVrfName,
                                     UINT4 *pu4FsEvpnVxlanEviVniMapVniNumber)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry = VxlanGetFirstFsEvpnVxlanVrfTable ();

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pFsEvpnVxlanVrfName->pu1_OctetList,
            EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry));
    pFsEvpnVxlanVrfName->i4_Length =
        EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry);
    *pu4FsEvpnVxlanEviVniMapVniNumber =
        EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pu4FsEvpnVxlanEviVniMapVniNumber);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanVrfRTTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanVrfRTTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsEvpnVxlanVrfName,
                                       UINT4 *pu4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4 *pu4FsEvpnVxlanVrfRTIndex,
                                       INT4 *pi4FsEvpnVxlanVrfRTType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry = VxlanGetFirstFsEvpnVxlanVrfRTTable ();

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pFsEvpnVxlanVrfName->pu1_OctetList,
            EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry));
    pFsEvpnVxlanVrfName->i4_Length =
        EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry);
    *pu4FsEvpnVxlanEviVniMapVniNumber =
        EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry);

    *pu4FsEvpnVxlanVrfRTIndex =
        EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry);

    *pi4FsEvpnVxlanVrfRTType = EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pu4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (pi4FsEvpnVxlanVrfRTType);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanMultihomedPeerTable
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanMultihomedPeerTable (INT4
                                                *pi4FsEvpnVxlanPeerIpAddressType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsEvpnVxlanPeerIpAddress,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsEvpnVxlanMHEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        VxlanGetFirstFsEvpnVxlanMultihomedPeerTable ();

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsEvpnVxlanPeerIpAddressType =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType;

    MEMCPY (pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressLen);
    pFsEvpnVxlanPeerIpAddress->i4_Length =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen;

    MEMCPY (pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI,
            pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMHEviVniESILen);
    pFsEvpnVxlanMHEviVniESI->i4_Length =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (*pi4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanVtepTable
 Input       :  The Indices
                FsVxlanVtepNveIfIndex
                nextFsVxlanVtepNveIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsVxlanVtepTable (INT4 i4FsVxlanVtepNveIfIndex,
                                 INT4 *pi4NextFsVxlanVtepNveIfIndex)
{

    tVxlanFsVxlanVtepEntry VxlanFsVxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pNextVxlanFsVxlanVtepEntry = NULL;

    MEMSET (&VxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    /* Assign the index */
    VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;

    pNextVxlanFsVxlanVtepEntry =
        VxlanGetNextFsVxlanVtepTable (&VxlanFsVxlanVtepEntry);

    if (pNextVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanVtepNveIfIndex =
        pNextVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanNveTable
 Input       :  The Indices
                FsVxlanNveIfIndex
                nextFsVxlanNveIfIndex
                FsVxlanNveVniNumber
                nextFsVxlanNveVniNumber
                FsVxlanNveDestVmMac
                nextFsVxlanNveDestVmMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsVxlanNveTable (INT4 i4FsVxlanNveIfIndex,
                                INT4 *pi4NextFsVxlanNveIfIndex,
                                UINT4 u4FsVxlanNveVniNumber,
                                UINT4 *pu4NextFsVxlanNveVniNumber,
                                tMacAddr u1FsVxlanNveDestVmMac,
                                tMacAddr * pNextFsVxlanNveDestVmMac)
{

    tVxlanFsVxlanNveEntry VxlanFsVxlanNveEntry;
    tVxlanFsVxlanNveEntry *pNextVxlanFsVxlanNveEntry = NULL;

    MEMSET (&VxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    VxlanFsVxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    VxlanFsVxlanNveEntry.MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(VxlanFsVxlanNveEntry.MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    pNextVxlanFsVxlanNveEntry =
        VxlanGetNextFsVxlanNveTable (&VxlanFsVxlanNveEntry);

    if (pNextVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanNveIfIndex =
        pNextVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

    *pu4NextFsVxlanNveVniNumber =
        pNextVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

    MEMCPY (pNextFsVxlanNveDestVmMac,
            &(pNextVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanMCastTable
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                nextFsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber
                nextFsVxlanMCastVniNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsVxlanMCastTable (INT4 i4FsVxlanMCastNveIfIndex,
                                  INT4 *pi4NextFsVxlanMCastNveIfIndex,
                                  UINT4 u4FsVxlanMCastVniNumber,
                                  UINT4 *pu4NextFsVxlanMCastVniNumber)
{

    tVxlanFsVxlanMCastEntry VxlanFsVxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pNextVxlanFsVxlanMCastEntry = NULL;

    MEMSET (&VxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    VxlanFsVxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    VxlanFsVxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    pNextVxlanFsVxlanMCastEntry =
        VxlanGetNextFsVxlanMCastTable (&VxlanFsVxlanMCastEntry);

    if (pNextVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanMCastNveIfIndex =
        pNextVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;

    *pu4NextFsVxlanMCastVniNumber =
        pNextVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanVniVlanMapTable
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId
                nextFsVxlanVniVlanMapVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsVxlanVniVlanMapTable (INT4 i4FsVxlanVniVlanMapVlanId,
                                       INT4 *pi4NextFsVxlanVniVlanMapVlanId)
{

    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pNextVxlanFsVxlanVniVlanMapEntry = NULL;

    MEMSET (&VxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    pNextVxlanFsVxlanVniVlanMapEntry =
        VxlanGetNextFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry);

    if (pNextVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanVniVlanMapVlanId =
        pNextVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanInReplicaTable
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                nextFsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                nextFsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                nextFsVxlanInReplicaIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsVxlanInReplicaTable (INT4 i4FsVxlanInReplicaNveIfIndex,
                                      INT4 *pi4NextFsVxlanInReplicaNveIfIndex,
                                      UINT4 u4FsVxlanInReplicaVniNumber,
                                      UINT4 *pu4NextFsVxlanInReplicaVniNumber,
                                      INT4 i4FsVxlanInReplicaIndex,
                                      INT4 *pi4NextFsVxlanInReplicaIndex)
{

    tVxlanFsVxlanInReplicaEntry VxlanFsVxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pNextVxlanFsVxlanInReplicaEntry = NULL;

    MEMSET (&VxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    VxlanFsVxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    VxlanFsVxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    VxlanFsVxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    pNextVxlanFsVxlanInReplicaEntry =
        VxlanGetNextFsVxlanInReplicaTable (&VxlanFsVxlanInReplicaEntry);

    if (pNextVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanInReplicaNveIfIndex =
        pNextVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex;

    *pu4NextFsVxlanInReplicaVniNumber =
        pNextVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;
    *pi4NextFsVxlanInReplicaIndex =
        pNextVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanEviVniMapTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                nextFsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                nextFsEvpnVxlanEviVniMapVniNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsEvpnVxlanEviVniMapTable (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                          INT4
                                          *pi4NextFsEvpnVxlanEviVniMapEviIndex,
                                          UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                          UINT4
                                          *pu4NextFsEvpnVxlanEviVniMapVniNumber)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry VxlanFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pNextVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    VxlanFsEvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    VxlanFsEvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    pNextVxlanFsEvpnVxlanEviVniMapEntry =
        VxlanGetNextFsEvpnVxlanEviVniMapTable (&VxlanFsEvpnVxlanEviVniMapEntry);

    if (pNextVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsEvpnVxlanEviVniMapEviIndex =
        pNextVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex;

    *pu4NextFsEvpnVxlanEviVniMapVniNumber =
        pNextVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (pi4NextFsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4NextFsEvpnVxlanEviVniMapVniNumber);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanBgpRTTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                nextFsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                nextFsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                nextFsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType
                nextFsEvpnVxlanBgpRTType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsEvpnVxlanBgpRTTable (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                      INT4 *pi4NextFsEvpnVxlanEviVniMapEviIndex,
                                      UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                      UINT4
                                      *pu4NextFsEvpnVxlanEviVniMapVniNumber,
                                      UINT4 u4FsEvpnVxlanBgpRTIndex,
                                      UINT4 *pu4NextFsEvpnVxlanBgpRTIndex,
                                      INT4 i4FsEvpnVxlanBgpRTType,
                                      INT4 *pi4NextFsEvpnVxlanBgpRTType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry VxlanFsEvpnVxlanBgpRTEntry;
    tVxlanFsEvpnVxlanBgpRTEntry *pNextVxlanFsEvpnVxlanBgpRTEntry = NULL;

    MEMSET (&VxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    VxlanFsEvpnVxlanBgpRTEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    VxlanFsEvpnVxlanBgpRTEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    VxlanFsEvpnVxlanBgpRTEntry.MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;

    VxlanFsEvpnVxlanBgpRTEntry.MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;

    pNextVxlanFsEvpnVxlanBgpRTEntry =
        VxlanGetNextFsEvpnVxlanBgpRTTable (&VxlanFsEvpnVxlanBgpRTEntry);

    if (pNextVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsEvpnVxlanEviVniMapEviIndex =
        pNextVxlanFsEvpnVxlanBgpRTEntry->
        MibObject.i4FsEvpnVxlanEviVniMapEviIndex;

    *pu4NextFsEvpnVxlanEviVniMapVniNumber =
        pNextVxlanFsEvpnVxlanBgpRTEntry->
        MibObject.u4FsEvpnVxlanEviVniMapVniNumber;

    *pu4NextFsEvpnVxlanBgpRTIndex =
        pNextVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex;

    *pi4NextFsEvpnVxlanBgpRTType =
        pNextVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (*pi4NextFsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pu4NextFsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (*pu4NextFsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pi4NextFsEvpnVxlanBgpRTType);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanVrfTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                nextFsEvpnVxlanVrfName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsEvpnVxlanVrfTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 *pu4NextFsEvpnVxlanEviVniMapVniNumber)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pNextVxlanFsEvpnVxlanVrfEntry = NULL;

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_VRF_NAME (VxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_VRF_NAME_LEN (VxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_VRF_VNI (VxlanFsEvpnVxlanVrfEntry) = u4FsEvpnVxlanEviVniMapVniNumber;

    pNextVxlanFsEvpnVxlanVrfEntry =
        VxlanGetNextFsEvpnVxlanVrfTable (&VxlanFsEvpnVxlanVrfEntry);

    if (pNextVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextFsEvpnVxlanVrfName->pu1_OctetList,
            EVPN_P_VRF_NAME (pNextVxlanFsEvpnVxlanVrfEntry),
            EVPN_P_VRF_NAME_LEN (pNextVxlanFsEvpnVxlanVrfEntry));
    pNextFsEvpnVxlanVrfName->i4_Length =
        EVPN_P_VRF_NAME_LEN (pNextVxlanFsEvpnVxlanVrfEntry);
    *pu4NextFsEvpnVxlanEviVniMapVniNumber =
        EVPN_P_VRF_VNI (pNextVxlanFsEvpnVxlanVrfEntry);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pNextFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4NextFsEvpnVxlanEviVniMapVniNumber);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanVrfRTTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                nextFsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                nextFsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType
                nextFsEvpnVxlanVrfRTType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsEvpnVxlanVrfRTTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsEvpnVxlanVrfName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsEvpnVxlanVrfName,
                                      UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                      UINT4
                                      *pu4NextFsEvpnVxlanEviVniMapVniNumber,
                                      UINT4 u4FsEvpnVxlanVrfRTIndex,
                                      UINT4 *pu4NextFsEvpnVxlanVrfRTIndex,
                                      INT4 i4FsEvpnVxlanVrfRTType,
                                      INT4 *pi4NextFsEvpnVxlanVrfRTType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry VxlanFsEvpnVxlanVrfRTEntry;
    tVxlanFsEvpnVxlanVrfRTEntry *pNextVxlanFsEvpnVxlanVrfRTEntry = NULL;

    MEMSET (&VxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_VRF_RT_NAME (VxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_VRF_RT_NAME_LEN (VxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_VRF_RT_VNI (VxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    EVPN_VRF_RT_INDEX (VxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;

    EVPN_VRF_RT_TYPE (VxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;

    pNextVxlanFsEvpnVxlanVrfRTEntry =
        VxlanGetNextFsEvpnVxlanVrfRTTable (&VxlanFsEvpnVxlanVrfRTEntry);

    if (pNextVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextFsEvpnVxlanVrfName->pu1_OctetList,
            pNextVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName,
            pNextVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen);
    pNextFsEvpnVxlanVrfName->i4_Length =
        pNextVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen;

    *pu4NextFsEvpnVxlanEviVniMapVniNumber =
        EVPN_P_VRF_RT_VNI (pNextVxlanFsEvpnVxlanVrfRTEntry);
    *pu4NextFsEvpnVxlanVrfRTIndex =
        pNextVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex;

    *pi4NextFsEvpnVxlanVrfRTType =
        pNextVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (*pNextFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4NextFsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (*pu4NextFsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (*pi4NextFsEvpnVxlanVrfRTType);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanMultihomedPeerTable
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                nextFsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                nextFsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI
                nextFsEvpnVxlanMHEviVniESI
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsEvpnVxlanMultihomedPeerTable (INT4
                                               i4FsEvpnVxlanPeerIpAddressType,
                                               INT4
                                               *pi4NextFsEvpnVxlanPeerIpAddressType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsEvpnVxlanPeerIpAddress,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsEvpnVxlanPeerIpAddress,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsEvpnVxlanMHEviVniESI,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsEvpnVxlanMHEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable VxlanFsEvpnVxlanMultihomedPeerTable;
    tVxlanFsEvpnVxlanMultihomedPeerTable
        * pNextVxlanFsEvpnVxlanMultihomedPeerTable = NULL;

    MEMSET (&VxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    VxlanFsEvpnVxlanMultihomedPeerTable.MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;

    MEMCPY (VxlanFsEvpnVxlanMultihomedPeerTable.MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    VxlanFsEvpnVxlanMultihomedPeerTable.MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    MEMCPY (VxlanFsEvpnVxlanMultihomedPeerTable.MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    VxlanFsEvpnVxlanMultihomedPeerTable.MibObject.i4FsEvpnVxlanMHEviVniESILen =
        pFsEvpnVxlanMHEviVniESI->i4_Length;

    pNextVxlanFsEvpnVxlanMultihomedPeerTable =
        VxlanGetNextFsEvpnVxlanMultihomedPeerTable
        (&VxlanFsEvpnVxlanMultihomedPeerTable);

    if (pNextVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsEvpnVxlanPeerIpAddressType =
        pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType;

    MEMCPY (pNextFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressLen);
    pNextFsEvpnVxlanPeerIpAddress->i4_Length =
        pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen;

    MEMCPY (pNextFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI,
            pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMHEviVniESILen);
    pNextFsEvpnVxlanMHEviVniESI->i4_Length =
        pNextVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pi4NextFsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pNextFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (*pNextFsEvpnVxlanMHEviVniESI);
    return SNMP_FAILURE;
#endif /* EVPN_VXLAN_WANTED */
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanEnable
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsVxlanEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEnable (INT4 *pi4RetValFsVxlanEnable)
{
    if (VxlanGetFsVxlanEnable (pi4RetValFsVxlanEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanUdpPort
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsVxlanUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanUdpPort (UINT4 *pu4RetValFsVxlanUdpPort)
{
    if (VxlanGetFsVxlanUdpPort (pu4RetValFsVxlanUdpPort) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanTraceOption
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsVxlanTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanTraceOption (UINT4 *pu4RetValFsVxlanTraceOption)
{
    if (VxlanGetFsVxlanTraceOption (pu4RetValFsVxlanTraceOption) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNotificationCntl
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsVxlanNotificationCntl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNotificationCntl (INT4 *pi4RetValFsVxlanNotificationCntl)
{
    if (VxlanGetFsVxlanNotificationCntl (pi4RetValFsVxlanNotificationCntl) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEnable
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsEvpnVxlanEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEnable (INT4 *pi4RetValFsEvpnVxlanEnable)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanGetFsEvpnVxlanEnable (pi4RetValFsEvpnVxlanEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pi4RetValFsEvpnVxlanEnable);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvpnAnycastGwMac
 Input       :  The Indices

                The Object
                retValFsEvpnAnycastGwMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnAnycastGwMac (tMacAddr * pRetValFsEvpnAnycastGwMac)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanGetFsEvpnAnycastGwMac (pRetValFsEvpnAnycastGwMac) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pRetValFsEvpnAnycastGwMac);
#endif
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVtepAddressType
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                INT4 *pi4RetValFsVxlanVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVtepAddressType (INT4 i4FsVxlanVtepNveIfIndex,
                              INT4 *pi4RetValFsVxlanVtepAddressType)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;

    if (VxlanGetAllFsVxlanVtepTable (pVxlanFsVxlanVtepEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanVtepAddressType =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVtepAddress
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVtepAddress (INT4 i4FsVxlanVtepNveIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanVtepAddress)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;

    if (VxlanGetAllFsVxlanVtepTable (pVxlanFsVxlanVtepEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanVtepAddress->pu1_OctetList,
            pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
            pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);
    pRetValFsVxlanVtepAddress->i4_Length =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVtepRowStatus
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                INT4 *pi4RetValFsVxlanVtepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVtepRowStatus (INT4 i4FsVxlanVtepNveIfIndex,
                            INT4 *pi4RetValFsVxlanVtepRowStatus)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;

    if (VxlanGetAllFsVxlanVtepTable (pVxlanFsVxlanVtepEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanVtepRowStatus =
        pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus;

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveVtepAddressType
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                INT4 *pi4RetValFsVxlanNveVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveVtepAddressType (INT4 i4FsVxlanNveIfIndex,
                                 UINT4 u4FsVxlanNveVniNumber,
                                 tMacAddr u1FsVxlanNveDestVmMac,
                                 INT4 *pi4RetValFsVxlanNveVtepAddressType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanNveVtepAddressType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveVtepAddress
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanNveVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveVtepAddress (INT4 i4FsVxlanNveIfIndex,
                             UINT4 u4FsVxlanNveVniNumber,
                             tMacAddr u1FsVxlanNveDestVmMac,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsVxlanNveVtepAddress)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanNveVtepAddress->pu1_OctetList,
            pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);
    pRetValFsVxlanNveVtepAddress->i4_Length =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                INT4 *pi4RetValFsVxlanNveRemoteVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveRemoteVtepAddressType (INT4 i4FsVxlanNveIfIndex,
                                       UINT4 u4FsVxlanNveVniNumber,
                                       tMacAddr u1FsVxlanNveDestVmMac,
                                       INT4
                                       *pi4RetValFsVxlanNveRemoteVtepAddressType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanNveRemoteVtepAddressType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveRemoteVtepAddress
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanNveRemoteVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveRemoteVtepAddress (INT4 i4FsVxlanNveIfIndex,
                                   UINT4 u4FsVxlanNveVniNumber,
                                   tMacAddr u1FsVxlanNveDestVmMac,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsVxlanNveRemoteVtepAddress)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanNveRemoteVtepAddress->pu1_OctetList,
            pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
            pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);
    pRetValFsVxlanNveRemoteVtepAddress->i4_Length =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveStorageType
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                INT4 *pi4RetValFsVxlanNveStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveStorageType (INT4 i4FsVxlanNveIfIndex,
                             UINT4 u4FsVxlanNveVniNumber,
                             tMacAddr u1FsVxlanNveDestVmMac,
                             INT4 *pi4RetValFsVxlanNveStorageType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanNveStorageType =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanSuppressArp
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                INT4 *pi4RetValFsVxlanSuppressArp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanSuppressArp (INT4 i4FsVxlanNveIfIndex,
                          UINT4 u4FsVxlanNveVniNumber,
                          tMacAddr u1FsVxlanNveDestVmMac,
                          INT4 *pi4RetValFsVxlanSuppressArp)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, 6);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanSuppressArp =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (*pi4RetValFsVxlanSuppressArp);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanNveVrfName
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object
                retValFsVxlanNveVrfName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveVrfName (INT4 i4FsVxlanNveIfIndex, UINT4 u4FsVxlanNveVniNumber,
                         tMacAddr u1FsVxlanNveDestVmMac,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanNveVrfName)
{
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (pRetValFsVxlanNveVrfName);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanNveRowStatus
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                INT4 *pi4RetValFsVxlanNveRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanNveRowStatus (INT4 i4FsVxlanNveIfIndex,
                           UINT4 u4FsVxlanNveVniNumber,
                           tMacAddr u1FsVxlanNveDestVmMac,
                           INT4 *pi4RetValFsVxlanNveRowStatus)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);

    if (VxlanGetAllFsVxlanNveTable (pVxlanFsVxlanNveEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanNveRowStatus =
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus;

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanMCastGroupAddressType
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                INT4 *pi4RetValFsVxlanMCastGroupAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanMCastGroupAddressType (INT4 i4FsVxlanMCastNveIfIndex,
                                    UINT4 u4FsVxlanMCastVniNumber,
                                    INT4 *pi4RetValFsVxlanMCastGroupAddressType)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    if (VxlanGetAllFsVxlanMCastTable (pVxlanFsVxlanMCastEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanMCastGroupAddressType =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanMCastGroupAddress
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanMCastGroupAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanMCastGroupAddress (INT4 i4FsVxlanMCastNveIfIndex,
                                UINT4 u4FsVxlanMCastVniNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsVxlanMCastGroupAddress)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    if (VxlanGetAllFsVxlanMCastTable (pVxlanFsVxlanMCastEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanMCastGroupAddress->pu1_OctetList,
            pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
            pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen);
    pRetValFsVxlanMCastGroupAddress->i4_Length =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanMCastVtepAddressType
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                INT4 *pi4RetValFsVxlanMCastVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanMCastVtepAddressType (INT4 i4FsVxlanMCastNveIfIndex,
                                   UINT4 u4FsVxlanMCastVniNumber,
                                   INT4 *pi4RetValFsVxlanMCastVtepAddressType)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    if (VxlanGetAllFsVxlanMCastTable (pVxlanFsVxlanMCastEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */

    *pi4RetValFsVxlanMCastVtepAddressType =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanMCastVtepAddress
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanMCastVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanMCastVtepAddress (INT4 i4FsVxlanMCastNveIfIndex,
                               UINT4 u4FsVxlanMCastVniNumber,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsVxlanMCastVtepAddress)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    if (VxlanGetAllFsVxlanMCastTable (pVxlanFsVxlanMCastEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanMCastVtepAddress->pu1_OctetList,
            pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
            pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen);
    pRetValFsVxlanMCastVtepAddress->i4_Length =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanMCastRowStatus
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                INT4 *pi4RetValFsVxlanMCastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanMCastRowStatus (INT4 i4FsVxlanMCastNveIfIndex,
                             UINT4 u4FsVxlanMCastVniNumber,
                             INT4 *pi4RetValFsVxlanMCastRowStatus)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    if (VxlanGetAllFsVxlanMCastTable (pVxlanFsVxlanMCastEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanMCastRowStatus =
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus;

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanMapVniNumber
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                UINT4 *pu4RetValFsVxlanVniVlanMapVniNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanMapVniNumber (INT4 i4FsVxlanVniVlanMapVlanId,
                                  UINT4 *pu4RetValFsVxlanVniVlanMapVniNumber)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsVxlanVniVlanMapVniNumber =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanMapPktSent
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                UINT4 *pu4RetValFsVxlanVniVlanMapPktSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanMapPktSent (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 *pu4RetValFsVxlanVniVlanMapPktSent)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsVxlanVniVlanMapPktSent =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanMapPktRcvd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                UINT4 *pu4RetValFsVxlanVniVlanMapPktRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanMapPktRcvd (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 *pu4RetValFsVxlanVniVlanMapPktRcvd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsVxlanVniVlanMapPktRcvd =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanMapPktDrpd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                UINT4 *pu4RetValFsVxlanVniVlanMapPktDrpd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanMapPktDrpd (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 *pu4RetValFsVxlanVniVlanMapPktDrpd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsVxlanVniVlanMapPktDrpd =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanMapRowStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                INT4 *pi4RetValFsVxlanVniVlanMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanMapRowStatus (INT4 i4FsVxlanVniVlanMapVlanId,
                                  INT4 *pi4RetValFsVxlanVniVlanMapRowStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanVniVlanMapRowStatus =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanDfElection
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                INT4 *pi4RetValFsVxlanVniVlanDfElection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanDfElection (INT4 i4FsVxlanVniVlanMapVlanId,
                                INT4 *pi4RetValFsVxlanVniVlanDfElection)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanVniVlanDfElection =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanDfElection;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanVniVlanTagStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object
                retValFsVxlanVniVlanTagStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanVniVlanTagStatus (INT4 i4FsVxlanVniVlanMapVlanId,
                               INT4 *pi4RetValFsVxlanVniVlanTagStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    if (VxlanGetAllFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanVniVlanTagStatus =
        pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged;

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanInReplicaVtepAddressType
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                INT4 *pi4RetValFsVxlanInReplicaVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanInReplicaVtepAddressType (INT4 i4FsVxlanInReplicaNveIfIndex,
                                       UINT4 u4FsVxlanInReplicaVniNumber,
                                       INT4 i4FsVxlanInReplicaIndex,
                                       INT4
                                       *pi4RetValFsVxlanInReplicaVtepAddressType)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);

    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    if (VxlanGetAllFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */

    *pi4RetValFsVxlanInReplicaVtepAddressType =
        pVxlanFsVxlanInReplicaEntry->MibObject.
        i4FsVxlanInReplicaVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanInReplicaVtepAddress
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                retValFsVxlanInReplicaVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanInReplicaVtepAddress (INT4 i4FsVxlanInReplicaNveIfIndex,
                                   UINT4 u4FsVxlanInReplicaVniNumber,
                                   INT4 i4FsVxlanInReplicaIndex,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pRetValFsVxlanInReplicaVtepAddress)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    if (VxlanGetAllFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    /* Assign the value */
    MEMCPY (pRetValFsVxlanInReplicaVtepAddress->pu1_OctetList,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.au1FsVxlanInReplicaVtepAddress,
            pVxlanFsVxlanInReplicaEntry->
            MibObject.i4FsVxlanInReplicaVtepAddressLen);
    pRetValFsVxlanInReplicaVtepAddress->i4_Length =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVxlanInReplicaRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                retValFsVxlanInReplicaRemoteVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanInReplicaRemoteVtepAddressType (INT4 i4FsVxlanInReplicaNveIfIndex,
                                             UINT4 u4FsVxlanInReplicaVniNumber,
                                             INT4 i4FsVxlanInReplicaIndex,
                                             INT4
                                             *pi4RetValFsVxlanInReplicaRemoteVtepAddressType)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);

    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    if (VxlanGetAllFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */

    *pi4RetValFsVxlanInReplicaRemoteVtepAddressType =
        pVxlanFsVxlanInReplicaEntry->MibObject.
        i4FsVxlanInReplicaRemoteVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanInReplicaRemoteVtepAddress
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                retValFsVxlanInReplicaRemoteVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanInReplicaRemoteVtepAddress (INT4 i4FsVxlanInReplicaNveIfIndex,
                                         UINT4 u4FsVxlanInReplicaVniNumber,
                                         INT4 i4FsVxlanInReplicaIndex,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValFsVxlanInReplicaRemoteVtepAddress)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    if (VxlanGetAllFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    /* Assign the value */
    MEMCPY (pRetValFsVxlanInReplicaRemoteVtepAddress->pu1_OctetList,
            &(pVxlanFsVxlanInReplicaEntry->
              MibObject.au1FsVxlanInReplicaRemoteVtepAddress),
            pVxlanFsVxlanInReplicaEntry->
            MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen);
    pRetValFsVxlanInReplicaRemoteVtepAddress->i4_Length =
        pVxlanFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanInReplicaRowStatus
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                retValFsVxlanInReplicaRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanInReplicaRowStatus (INT4 i4FsVxlanInReplicaNveIfIndex,
                                 UINT4 u4FsVxlanInReplicaVniNumber,
                                 INT4 i4FsVxlanInReplicaIndex,
                                 INT4 *pi4RetValFsVxlanInReplicaRowStatus)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;

    if (VxlanGetAllFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pi4RetValFsVxlanInReplicaRowStatus =
        pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus;

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapBgpRD
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanEviVniMapBgpRD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapBgpRD (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsEvpnVxlanEviVniMapBgpRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanEviVniMapBgpRD->pu1_OctetList,
            pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);
    pRetValFsEvpnVxlanEviVniMapBgpRD->i4_Length =
        pVxlanFsEvpnVxlanEviVniMapEntry->
        MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pRetValFsEvpnVxlanEviVniMapBgpRD);
#endif /* EVPN_VXLAN_WANTED */

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniESI
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanEviVniESI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniESI (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsEvpnVxlanEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanEviVniESI->pu1_OctetList,
            pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniESI,
            pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniESILen);
    pRetValFsEvpnVxlanEviVniESI->i4_Length =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pRetValFsEvpnVxlanEviVniESI);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniLoadBalance
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                INT4 *pi4RetValFsEvpnVxlanEviVniLoadBalance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniLoadBalance (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    INT4 *pi4RetValFsEvpnVxlanEviVniLoadBalance)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanEviVniLoadBalance =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniLoadBalance;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanEviVniLoadBalance);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapSentPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                UINT4 *pu4RetValFsEvpnVxlanEviVniMapSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapSentPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4
                                    *pu4RetValFsEvpnVxlanEviVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanEviVniMapSentPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapSentPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4RetValFsEvpnVxlanEviVniMapSentPkts);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapRcvdPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                UINT4 *pu4RetValFsEvpnVxlanEviVniMapRcvdPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapRcvdPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4
                                    *pu4RetValFsEvpnVxlanEviVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanEviVniMapRcvdPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapRcvdPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4RetValFsEvpnVxlanEviVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapDroppedPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                UINT4 *pu4RetValFsEvpnVxlanEviVniMapDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapDroppedPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       *pu4RetValFsEvpnVxlanEviVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanEviVniMapDroppedPkts =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapDroppedPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pu4RetValFsEvpnVxlanEviVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                INT4 *pi4RetValFsEvpnVxlanEviVniMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapRowStatus (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                     UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                     INT4
                                     *pi4RetValFsEvpnVxlanEviVniMapRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanEviVniMapRowStatus =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapRowStatus;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (pi4RetValFsEvpnVxlanEviVniMapRowStatus);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapBgpRDAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                INT4 *pi4RetValFsEvpnVxlanEviVniMapBgpRDAuto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapBgpRDAuto (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                     UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                     INT4
                                     *pi4RetValFsEvpnVxlanEviVniMapBgpRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanEviVniMapBgpRDAuto =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapBgpRDAuto;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanEviVniMapBgpRDAuto);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanEviVniMapTotalPaths
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                INT4 *pi4RetValFsEvpnVxlanEviVniMapBgpRDAuto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanEviVniMapTotalPaths (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                      UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                      INT4
                                      *pi4RetValFsEvpnVxlanEviVniMapTotalPaths)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanEviVniMapTable (pVxlanFsEvpnVxlanEviVniMapEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanEviVniMapTotalPaths =
        pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        i4FsEvpnVxlanEviVniMapTotalPaths;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanEviVniMapTotalPaths);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanBgpRT
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanBgpRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanBgpRT (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        UINT4 u4FsEvpnVxlanBgpRTIndex,
                        INT4 i4FsEvpnVxlanBgpRTType,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanBgpRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;

    if (VxlanGetAllFsEvpnVxlanBgpRTTable (pVxlanFsEvpnVxlanBgpRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanBgpRT->pu1_OctetList,
            pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
            pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen);
    pRetValFsEvpnVxlanBgpRT->i4_Length =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pRetValFsEvpnVxlanBgpRT);
#endif /* EVPN_VXLAN_WANTED */

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanBgpRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                INT4 *pi4RetValFsEvpnVxlanBgpRTRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanBgpRTRowStatus (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 UINT4 u4FsEvpnVxlanBgpRTIndex,
                                 INT4 i4FsEvpnVxlanBgpRTType,
                                 INT4 *pi4RetValFsEvpnVxlanBgpRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;

    if (VxlanGetAllFsEvpnVxlanBgpRTTable (pVxlanFsEvpnVxlanBgpRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanBgpRTRowStatus =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanBgpRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanBgpRTAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                INT4 *pi4RetValFsEvpnVxlanBgpRTAuto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanBgpRTAuto (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            UINT4 u4FsEvpnVxlanBgpRTIndex,
                            INT4 i4FsEvpnVxlanBgpRTType,
                            INT4 *pi4RetValFsEvpnVxlanBgpRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;

    if (VxlanGetAllFsEvpnVxlanBgpRTTable (pVxlanFsEvpnVxlanBgpRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanBgpRTAuto =
        pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanBgpRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRD
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanVrfRD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRD (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanVrfRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanVrfRD->pu1_OctetList,
            EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry),
            EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry));
    pRetValFsEvpnVxlanVrfRD->i4_Length =
        EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry);

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pRetValFsEvpnVxlanVrfRD);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
                INT4 *pi4RetValFsEvpnVxlanVrfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               INT4 *pi4RetValFsEvpnVxlanVrfRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanVrfRowStatus =
        EVPN_P_VRF_ROW_STATUS (pVxlanFsEvpnVxlanVrfEntry);

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanVrfRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRT
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object
                tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanVrfRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRT (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        UINT4 u4FsEvpnVxlanVrfRTIndex,
                        INT4 i4FsEvpnVxlanVrfRTType,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsEvpnVxlanVrfRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;

    if (VxlanGetAllFsEvpnVxlanVrfRTTable (pVxlanFsEvpnVxlanVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanVrfRT->pu1_OctetList,
            EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
            EVPN_P_VRF_RT_LEN (pVxlanFsEvpnVxlanVrfRTEntry));
    pRetValFsEvpnVxlanVrfRT->i4_Length =
        EVPN_P_VRF_RT_LEN (pVxlanFsEvpnVxlanVrfRTEntry);

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (pRetValFsEvpnVxlanVrfRT);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRTAuto
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                INT4 *pi4RetValFsEvpnVxlanBgpRTAuto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRTAuto (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            UINT4 u4FsEvpnVxlanBgpRTIndex,
                            INT4 i4FsEvpnVxlanBgpRTType,
                            INT4 *pi4RetValFsEvpnVxlanVrfRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanBgpRTIndex;
    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanBgpRTType;

    if (VxlanGetAllFsEvpnVxlanVrfRTTable (pVxlanFsEvpnVxlanVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanVrfRTAuto =
        pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanVrfRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
                INT4 *pi4RetValFsEvpnVxlanVrfRTRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRTRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 UINT4 u4FsEvpnVxlanVrfRTIndex,
                                 INT4 i4FsEvpnVxlanVrfRTType,
                                 INT4 *pi4RetValFsEvpnVxlanVrfRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;

    if (VxlanGetAllFsEvpnVxlanVrfRTTable (pVxlanFsEvpnVxlanVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanVrfRTRowStatus =
        EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry);

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (pi4RetValFsEvpnVxlanVrfRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfRDAuto
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                retValFsEvpnVxlanVrfRDAuto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfRDAuto (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            INT4 *pi4RetValFsEvpnVxlanEviVniMapVrfRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanEviVniMapVrfRDAuto =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanEviVniMapVrfRDAuto);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfVniMapSentPkts
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                retValFsEvpnVxlanVrfVniMapSentPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfVniMapSentPkts (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4
                                    *pu4RetValFsEvpnVxlanVrfVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanVrfVniMapSentPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pu4RetValFsEvpnVxlanVrfVniMapSentPkts);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfVniMapRcvdPkts
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                retValFsEvpnVxlanVrfVniMapDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfVniMapRcvdPkts (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4
                                    *pu4RetValFsEvpnVxlanVrfVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanVrfVniMapRcvdPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pu4RetValFsEvpnVxlanVrfVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanVrfVniMapDroppedPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                UINT4 *pu4RetValFsEvpnVxlanEviVniMapDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanVrfVniMapDroppedPkts (tSNMP_OCTET_STRING_TYPE *
                                       pFsEvpnVxlanVrfName,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       *pu4RetValFsEvpnVxlanVrfVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;

    if (VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanVrfVniMapDroppedPkts =
        pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pu4RetValFsEvpnVxlanVrfVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanOrdinalNum
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI

                The Object 
                UINT4 *pu4RetValFsEvpnVxlanOrdinalNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanOrdinalNum (INT4 i4FsEvpnVxlanPeerIpAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsEvpnVxlanPeerIpAddress,
                             tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanMHEviVniESI,
                             UINT4 *pu4RetValFsEvpnVxlanOrdinalNum)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;

    if (VxlanGetAllFsEvpnVxlanMultihomedPeerTable
        (pVxlanFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsEvpnVxlanOrdinalNum =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (*pu4RetValFsEvpnVxlanOrdinalNum);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanMultihomedPeerRowStatus
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI

                The Object 
                INT4 *pi4RetValFsEvpnVxlanMultihomedPeerRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanMultihomedPeerRowStatus (INT4 i4FsEvpnVxlanPeerIpAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsEvpnVxlanPeerIpAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsEvpnVxlanMHEviVniESI,
                                          INT4
                                          *pi4RetValFsEvpnVxlanMultihomedPeerRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;

    if (VxlanGetAllFsEvpnVxlanMultihomedPeerTable
        (pVxlanFsEvpnVxlanMultihomedPeerTable) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanMultihomedPeerRowStatus =
        pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus;

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (*pi4RetValFsEvpnVxlanMultihomedPeerRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanEnable
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsVxlanEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanEnable (INT4 i4SetValFsVxlanEnable)
{
    if (VxlanSetFsVxlanEnable (i4SetValFsVxlanEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanUdpPort
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsVxlanUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanUdpPort (UINT4 u4SetValFsVxlanUdpPort)
{
    if (VxlanSetFsVxlanUdpPort (u4SetValFsVxlanUdpPort) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanTraceOption
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsVxlanTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanTraceOption (UINT4 u4SetValFsVxlanTraceOption)
{
    if (VxlanSetFsVxlanTraceOption (u4SetValFsVxlanTraceOption, CLI_ENABLE) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanNotificationCntl
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsVxlanNotificationCntl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanNotificationCntl (INT4 i4SetValFsVxlanNotificationCntl)
{
    if (VxlanSetFsVxlanNotificationCntl (i4SetValFsVxlanNotificationCntl) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEnable
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsEvpnVxlanEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEnable (INT4 i4SetValFsEvpnVxlanEnable)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanSetFsEvpnVxlanEnable (i4SetValFsEvpnVxlanEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4SetValFsEvpnVxlanEnable);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEvpnAnycastGwMac
 Input       :  The Indices

                The Object
                setValFsEvpnAnycastGwMac
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnAnycastGwMac (tMacAddr SetValFsEvpnAnycastGwMac)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanSetFsEvpnAnycastGwMac (SetValFsEvpnAnycastGwMac) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (SetValFsEvpnAnycastGwMac);
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVtepAddressType
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
             :  INT4 i4SetValFsVxlanVtepAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVtepAddressType (INT4 i4FsVxlanVtepNveIfIndex,
                              INT4 i4SetValFsVxlanVtepAddressType)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
        i4SetValFsVxlanVtepAddressType;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVtepTable
        (pVxlanFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVtepAddress
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsVxlanVtepAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVtepAddress (INT4 i4FsVxlanVtepNveIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsVxlanVtepAddress)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
            pSetValFsVxlanVtepAddress->pu1_OctetList,
            pSetValFsVxlanVtepAddress->i4_Length);
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
        pSetValFsVxlanVtepAddress->i4_Length;

    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVtepTable
        (pVxlanFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVtepRowStatus
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
             :  INT4 i4SetValFsVxlanVtepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVtepRowStatus (INT4 i4FsVxlanVtepNveIfIndex,
                            INT4 i4SetValFsVxlanVtepRowStatus)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
        i4SetValFsVxlanVtepRowStatus;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVtepTable
        (pVxlanFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanNveRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
             :  INT4 i4SetValFsVxlanNveRemoteVtepAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanNveRemoteVtepAddressType (INT4 i4FsVxlanNveIfIndex,
                                       UINT4 u4FsVxlanNveVniNumber,
                                       tMacAddr u1FsVxlanNveDestVmMac,
                                       INT4
                                       i4SetValFsVxlanNveRemoteVtepAddressType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType =
        i4SetValFsVxlanNveRemoteVtepAddressType;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType = OSIX_TRUE;

    if (VxlanSetAllFsVxlanNveTable
        (pVxlanFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanNveRemoteVtepAddress
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsVxlanNveRemoteVtepAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanNveRemoteVtepAddress (INT4 i4FsVxlanNveIfIndex,
                                   UINT4 u4FsVxlanNveVniNumber,
                                   tMacAddr u1FsVxlanNveDestVmMac,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsVxlanNveRemoteVtepAddress)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
            pSetValFsVxlanNveRemoteVtepAddress->pu1_OctetList,
            pSetValFsVxlanNveRemoteVtepAddress->i4_Length);
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen =
        pSetValFsVxlanNveRemoteVtepAddress->i4_Length;

    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress = OSIX_TRUE;

    if (VxlanSetAllFsVxlanNveTable
        (pVxlanFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanSuppressArp
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
             :  INT4 i4SetValFsVxlanSuppressArp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanSuppressArp (INT4 i4FsVxlanNveIfIndex,
                          UINT4 u4FsVxlanNveVniNumber,
                          tMacAddr u1FsVxlanNveDestVmMac,
                          INT4 i4SetValFsVxlanSuppressArp)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, 6);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
        i4SetValFsVxlanSuppressArp;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp = OSIX_TRUE;

    if (VxlanSetAllFsVxlanNveTable
        (pVxlanFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (i4SetValFsVxlanSuppressArp);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVxlanNveVrfName
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object
                setValFsVxlanNveVrfName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanNveVrfName (INT4 i4FsVxlanNveIfIndex,
                         UINT4 u4FsVxlanNveVniNumber,
                         tMacAddr u1FsVxlanNveDestVmMac,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsVxlanNveVrfName)
{
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (pSetValFsVxlanNveVrfName);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanNveRowStatus
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
             :  INT4 i4SetValFsVxlanNveRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanNveRowStatus (INT4 i4FsVxlanNveIfIndex,
                           UINT4 u4FsVxlanNveVniNumber,
                           tMacAddr u1FsVxlanNveDestVmMac,
                           INT4 i4SetValFsVxlanNveRowStatus)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
        i4SetValFsVxlanNveRowStatus;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsVxlanNveTable
        (pVxlanFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanMCastGroupAddressType
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
             :  INT4 i4SetValFsVxlanMCastGroupAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanMCastGroupAddressType (INT4 i4FsVxlanMCastNveIfIndex,
                                    UINT4 u4FsVxlanMCastVniNumber,
                                    INT4 i4SetValFsVxlanMCastGroupAddressType)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType =
        i4SetValFsVxlanMCastGroupAddressType;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType = OSIX_TRUE;

    if (VxlanSetAllFsVxlanMCastTable
        (pVxlanFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanMCastGroupAddress
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsVxlanMCastGroupAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanMCastGroupAddress (INT4 i4FsVxlanMCastNveIfIndex,
                                UINT4 u4FsVxlanMCastVniNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsVxlanMCastGroupAddress)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
            pSetValFsVxlanMCastGroupAddress->pu1_OctetList,
            pSetValFsVxlanMCastGroupAddress->i4_Length);
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen =
        pSetValFsVxlanMCastGroupAddress->i4_Length;

    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress = OSIX_TRUE;

    if (VxlanSetAllFsVxlanMCastTable
        (pVxlanFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanMCastRowStatus
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
             :  INT4 i4SetValFsVxlanMCastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanMCastRowStatus (INT4 i4FsVxlanMCastNveIfIndex,
                             UINT4 u4FsVxlanMCastVniNumber,
                             INT4 i4SetValFsVxlanMCastRowStatus)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
        i4SetValFsVxlanMCastRowStatus;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsVxlanMCastTable
        (pVxlanFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanMapVniNumber
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
             :  UINT4 u4SetValFsVxlanVniVlanMapVniNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanMapVniNumber (INT4 i4FsVxlanVniVlanMapVlanId,
                                  UINT4 u4SetValFsVxlanVniVlanMapVniNumber)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber =
        u4SetValFsVxlanVniVlanMapVniNumber;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanMapPktSent
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
             :  INT4 i4SetValFsVxlanVniVlanMapPktSent
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanMapPktSent (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 u4SetValFsVxlanVniVlanMapPktSent)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent =
        u4SetValFsVxlanVniVlanMapPktSent;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanMapPktRcvd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
             :  INT4 i4SetValFsVxlanVniVlanMapPktRcvd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanMapPktRcvd (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 u4SetValFsVxlanVniVlanMapPktRcvd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd =
        u4SetValFsVxlanVniVlanMapPktRcvd;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanMapPktDrpd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
             :  INT4 i4SetValFsVxlanVniVlanMapPktDrpd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanMapPktDrpd (INT4 i4FsVxlanVniVlanMapVlanId,
                                UINT4 u4SetValFsVxlanVniVlanMapPktDrpd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd =
        u4SetValFsVxlanVniVlanMapPktDrpd;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanMapRowStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
             :  INT4 i4SetValFsVxlanVniVlanMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanMapRowStatus (INT4 i4FsVxlanVniVlanMapVlanId,
                                  INT4 i4SetValFsVxlanVniVlanMapRowStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
        i4SetValFsVxlanVniVlanMapRowStatus;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVxlanVniVlanTagStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object
                setValFsVxlanVniVlanTagStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanVniVlanTagStatus (INT4 i4FsVxlanVniVlanMapVlanId,
                               INT4 i4SetValFsVxlanVniVlanTagStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged =
        (UINT1) i4SetValFsVxlanVniVlanTagStatus;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged = OSIX_TRUE;

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVxlanInReplicaRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                setValFsVxlanInReplicaRemoteVtepAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanInReplicaRemoteVtepAddressType (INT4 i4FsVxlanInReplicaNveIfIndex,
                                             UINT4 u4FsVxlanInReplicaVniNumber,
                                             INT4 i4FsVxlanInReplicaIndex,
                                             INT4
                                             i4SetValFsVxlanInReplicaRemoteVtepAddressType)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry =
        (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanInReplicaEntry->MibObject.
        i4FsVxlanInReplicaRemoteVtepAddressType =
        i4SetValFsVxlanInReplicaRemoteVtepAddressType;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddressType =
        OSIX_TRUE;

    if (VxlanSetAllFsVxlanInReplicaTable
        (pVxlanFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVxlanInReplicaRemoteVtepAddress
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                setValFsVxlanInReplicaRemoteVtepAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanInReplicaRemoteVtepAddress (INT4 i4FsVxlanInReplicaNveIfIndex,
                                         UINT4 u4FsVxlanInReplicaVniNumber,
                                         INT4 i4FsVxlanInReplicaIndex,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValFsVxlanInReplicaRemoteVtepAddress)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry =
        (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (&(pVxlanFsVxlanInReplicaEntry->
              MibObject.au1FsVxlanInReplicaRemoteVtepAddress),
            pSetValFsVxlanInReplicaRemoteVtepAddress->pu1_OctetList,
            pSetValFsVxlanInReplicaRemoteVtepAddress->i4_Length);
    pVxlanFsVxlanInReplicaEntry->
        MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen =
        pSetValFsVxlanInReplicaRemoteVtepAddress->i4_Length;

    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress =
        OSIX_TRUE;

    if (VxlanSetAllFsVxlanInReplicaTable
        (pVxlanFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVxlanInReplicaRowStatus
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                setValFsVxlanInReplicaRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanInReplicaRowStatus (INT4 i4FsVxlanInReplicaNveIfIndex,
                                 UINT4 u4FsVxlanInReplicaVniNumber,
                                 INT4 i4FsVxlanInReplicaIndex,
                                 INT4 i4SetValFsVxlanInReplicaRowStatus)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry =
        (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
        i4SetValFsVxlanInReplicaRowStatus;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsVxlanInReplicaTable
        (pVxlanFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapBgpRD
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsEvpnVxlanEviVniMapBgpRD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapBgpRD (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsEvpnVxlanEviVniMapBgpRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pSetValFsEvpnVxlanEviVniMapBgpRD->pu1_OctetList,
            pSetValFsEvpnVxlanEviVniMapBgpRD->i4_Length);
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen =
        pSetValFsEvpnVxlanEviVniMapBgpRD->i4_Length;

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pSetValFsEvpnVxlanEviVniMapBgpRD);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniESI
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsEvpnVxlanEviVniESI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniESI (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsEvpnVxlanEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniESI,
            pSetValFsEvpnVxlanEviVniESI->pu1_OctetList,
            pSetValFsEvpnVxlanEviVniESI->i4_Length);
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen =
        pSetValFsEvpnVxlanEviVniESI->i4_Length;

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pSetValFsEvpnVxlanEviVniESI);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniLoadBalance
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  INT4 i4SetValFsEvpnVxlanEviVniLoadBalance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniLoadBalance (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    INT4 i4SetValFsEvpnVxlanEviVniLoadBalance)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniLoadBalance =
        i4SetValFsEvpnVxlanEviVniLoadBalance;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4SetValFsEvpnVxlanEviVniLoadBalance);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapSentPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  UINT4 u4SetValFsEvpnVxlanEviVniMapSentPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapSentPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4SetValFsEvpnVxlanEviVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapSentPkts =
        u4SetValFsEvpnVxlanEviVniMapSentPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanEviVniMapSentPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapRcvdPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  UINT4 u4SetValFsEvpnVxlanEviVniMapRcvdPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapRcvdPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4SetValFsEvpnVxlanEviVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapRcvdPkts =
        u4SetValFsEvpnVxlanEviVniMapRcvdPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanEviVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapDroppedPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  UINT4 u4SetValFsEvpnVxlanEviVniMapDroppedPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapDroppedPkts (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4SetValFsEvpnVxlanEviVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapDroppedPkts =
        u4SetValFsEvpnVxlanEviVniMapDroppedPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapDroppedPkts =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanEviVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
             :  INT4 i4SetValFsEvpnVxlanEviVniMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapRowStatus (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                     UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                     INT4 i4SetValFsEvpnVxlanEviVniMapRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapRowStatus =
        i4SetValFsEvpnVxlanEviVniMapRowStatus;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4SetValFsEvpnVxlanEviVniMapRowStatus);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanEviVniMapBgpRDAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
             :  INT4 i4SetValFsEvpnVxlanEviVniMapBgpRDAuto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanEviVniMapBgpRDAuto (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                     UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                     INT4 i4SetValFsEvpnVxlanEviVniMapBgpRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto =
        i4SetValFsEvpnVxlanEviVniMapBgpRDAuto;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanFsEvpnVxlanEviVniMapEntry, pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4SetValFsEvpnVxlanEviVniMapBgpRDAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanBgpRT
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsEvpnVxlanBgpRT
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanBgpRT (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        UINT4 u4FsEvpnVxlanBgpRTIndex,
                        INT4 i4FsEvpnVxlanBgpRTType,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsEvpnVxlanBgpRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
            pSetValFsEvpnVxlanBgpRT->pu1_OctetList,
            pSetValFsEvpnVxlanBgpRT->i4_Length);
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen =
        pSetValFsEvpnVxlanBgpRT->i4_Length;

    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanBgpRTTable
        (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pSetValFsEvpnVxlanBgpRT);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanBgpRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
             :  INT4 i4SetValFsEvpnVxlanBgpRTRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanBgpRTRowStatus (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 UINT4 u4FsEvpnVxlanBgpRTIndex,
                                 INT4 i4FsEvpnVxlanBgpRTType,
                                 INT4 i4SetValFsEvpnVxlanBgpRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
        i4SetValFsEvpnVxlanBgpRTRowStatus;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanBgpRTTable
        (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (i4SetValFsEvpnVxlanBgpRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanBgpRTAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
             :  INT4 i4SetValFsEvpnVxlanBgpRTAuto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanBgpRTAuto (INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            UINT4 u4FsEvpnVxlanBgpRTIndex,
                            INT4 i4FsEvpnVxlanBgpRTType,
                            INT4 i4SetValFsEvpnVxlanBgpRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto =
        i4SetValFsEvpnVxlanBgpRTAuto;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanBgpRTTable
        (pVxlanFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (i4SetValFsEvpnVxlanBgpRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRD
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsEvpnVxlanVrfRD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRD (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsEvpnVxlanVrfRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry),
            pSetValFsEvpnVxlanVrfRD->pu1_OctetList,
            pSetValFsEvpnVxlanVrfRD->i4_Length);
    EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pSetValFsEvpnVxlanVrfRD->i4_Length;

    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pSetValFsEvpnVxlanVrfRD);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
             :  INT4 i4SetValFsEvpnVxlanVrfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               INT4 i4SetValFsEvpnVxlanVrfRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    EVPN_P_VRF_ROW_STATUS (pVxlanFsEvpnVxlanVrfEntry) =
        i4SetValFsEvpnVxlanVrfRowStatus;

    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4SetValFsEvpnVxlanVrfRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRDAuto
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                setValFsEvpnVxlanVrfRDAuto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRDAuto (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            INT4 i4SetValFsEvpnVxlanVrfRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto =
        i4SetValFsEvpnVxlanVrfRDAuto;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4SetValFsEvpnVxlanVrfRDAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRT
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsEvpnVxlanVrfRT
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRT (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                        UINT4 u4FsEvpnVxlanVrfRTIndex,
                        INT4 i4FsEvpnVxlanVrfRTType,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsEvpnVxlanVrfRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
            pSetValFsEvpnVxlanVrfRT->pu1_OctetList,
            pSetValFsEvpnVxlanVrfRT->i4_Length);
    EVPN_P_VRF_RT_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pSetValFsEvpnVxlanVrfRT->i4_Length;

    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfRTTable
        (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (*pSetValFsEvpnVxlanVrfRT);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
             :  INT4 i4SetValFsEvpnVxlanVrfRTRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRTRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                 UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                 UINT4 u4FsEvpnVxlanVrfRTIndex,
                                 INT4 i4FsEvpnVxlanVrfRTType,
                                 INT4 i4SetValFsEvpnVxlanVrfRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) =
        i4SetValFsEvpnVxlanVrfRTRowStatus;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfRTTable
        (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (i4SetValFsEvpnVxlanVrfRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfRTAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
             :  INT4 i4SetValFsEvpnVxlanBgpRTAuto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfRTAuto (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                            UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                            UINT4 u4FsEvpnVxlanVrfRTIndex,
                            INT4 i4FsEvpnVxlanVrfRTType,
                            INT4 i4SetValFsEvpnVxlanVrfRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto =
        i4SetValFsEvpnVxlanVrfRTAuto;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfRTTable
        (pVxlanFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (i4SetValFsEvpnVxlanVrfRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfVniMapSentPkts
 Input       :  The Indices
                pFsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
             :  UINT4 u4SetValFsEvpnVxlanVrfVniMapSentPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfVniMapSentPkts (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4SetValFsEvpnVxlanVrfVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts =
        u4SetValFsEvpnVxlanVrfVniMapSentPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanVrfVniMapSentPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfVniMapRcvdPkts
 Input       :  The Indices
                pFsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
             :  UINT4 u4SetValFsEvpnVxlanEviVniMapRcvdPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfVniMapRcvdPkts (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4SetValFsEvpnVxlanVrfVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts =
        u4SetValFsEvpnVxlanVrfVniMapRcvdPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanVrfVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanVrfVniMapDroppedPkts
 Input       :  The Indices
                pFsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
             :  UINT4 u4SetValFsEvpnVxlanEviVniMapDroppedPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanVrfVniMapDroppedPkts (tSNMP_OCTET_STRING_TYPE *
                                       pFsEvpnVxlanVrfName,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4SetValFsEvpnVxlanVrfVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.
        u4FsEvpnVxlanVrfVniMapDroppedPkts =
        u4SetValFsEvpnVxlanVrfVniMapDroppedPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4SetValFsEvpnVxlanVrfVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanOrdinalNum
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI

                The Object 
             :  UINT4 u4SetValFsEvpnVxlanOrdinalNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanOrdinalNum (INT4 i4FsEvpnVxlanPeerIpAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsEvpnVxlanPeerIpAddress,
                             tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanMHEviVniESI,
                             UINT4 u4SetValFsEvpnVxlanOrdinalNum)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        * pVxlanIsSetFsEvpnVxlanMultihomedPeerTable = NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable =
        (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum =
        u4SetValFsEvpnVxlanOrdinalNum;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum =
        OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanMultihomedPeerTable
        (pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (u4SetValFsEvpnVxlanOrdinalNum);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanMultihomedPeerRowStatus
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI

                The Object 
             :  INT4 i4SetValFsEvpnVxlanMultihomedPeerRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanMultihomedPeerRowStatus (INT4 i4FsEvpnVxlanPeerIpAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsEvpnVxlanPeerIpAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsEvpnVxlanMHEviVniESI,
                                          INT4
                                          i4SetValFsEvpnVxlanMultihomedPeerRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        * pVxlanIsSetFsEvpnVxlanMultihomedPeerTable = NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable =
        (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID);
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus =
        i4SetValFsEvpnVxlanMultihomedPeerRowStatus;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;

    if (VxlanSetAllFsEvpnVxlanMultihomedPeerTable
        (pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (i4SetValFsEvpnVxlanMultihomedPeerRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanEnable
 Input       :  The Indices

                The Object 
                testValFsVxlanEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanEnable (UINT4 *pu4ErrorCode, INT4 i4TestValFsVxlanEnable)
{
    if (VxlanTestFsVxlanEnable (pu4ErrorCode, i4TestValFsVxlanEnable) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanUdpPort
 Input       :  The Indices

                The Object 
                testValFsVxlanUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanUdpPort (UINT4 *pu4ErrorCode, UINT4 u4TestValFsVxlanUdpPort)
{
    if (VxlanTestFsVxlanUdpPort (pu4ErrorCode, u4TestValFsVxlanUdpPort) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanTraceOption
 Input       :  The Indices

                The Object 
                testValFsVxlanTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanTraceOption (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsVxlanTraceOption)
{
    if (VxlanTestFsVxlanTraceOption (pu4ErrorCode, u4TestValFsVxlanTraceOption)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanNotificationCntl
 Input       :  The Indices

                The Object 
                testValFsVxlanNotificationCntl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanNotificationCntl (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsVxlanNotificationCntl)
{
    if (VxlanTestFsVxlanNotificationCntl
        (pu4ErrorCode, i4TestValFsVxlanNotificationCntl) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEnable
 Input       :  The Indices

                The Object 
                testValFsEvpnVxlanEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEnable (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsEvpnVxlanEnable)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanTestFsEvpnVxlanEnable (pu4ErrorCode, i4TestValFsEvpnVxlanEnable) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsEvpnVxlanEnable);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnAnycastGwMac
 Input       :  The Indices

                The Object
                testValFsEvpnAnycastGwMac
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnAnycastGwMac (UINT4 *pu4ErrorCode,
                             tMacAddr TestValFsEvpnAnycastGwMac)
{
#ifdef EVPN_VXLAN_WANTED
    if (VxlanTestFsEvpnAnycastGwMac (pu4ErrorCode, TestValFsEvpnAnycastGwMac) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (TestValFsEvpnAnycastGwMac);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVtepAddressType
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                testValFsVxlanVtepAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVtepAddressType (UINT4 *pu4ErrorCode,
                                 INT4 i4FsVxlanVtepNveIfIndex,
                                 INT4 i4TestValFsVxlanVtepAddressType)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
        i4TestValFsVxlanVtepAddressType;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVtepTable (pu4ErrorCode, pVxlanFsVxlanVtepEntry,
                                      pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
                                      OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVtepAddress
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                testValFsVxlanVtepAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVtepAddress (UINT4 *pu4ErrorCode, INT4 i4FsVxlanVtepNveIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsVxlanVtepAddress)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
            pTestValFsVxlanVtepAddress->pu1_OctetList,
            pTestValFsVxlanVtepAddress->i4_Length);
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
        pTestValFsVxlanVtepAddress->i4_Length;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVtepTable (pu4ErrorCode, pVxlanFsVxlanVtepEntry,
                                      pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
                                      OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVtepRowStatus
 Input       :  The Indices
                FsVxlanVtepNveIfIndex

                The Object 
                testValFsVxlanVtepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVtepRowStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsVxlanVtepNveIfIndex,
                               INT4 i4TestValFsVxlanVtepRowStatus)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;
    tVxlanIsSetFsVxlanVtepEntry *pVxlanIsSetFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);

    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVtepEntry =
        (tVxlanIsSetFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVtepEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (pVxlanIsSetFsVxlanVtepEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVtepEntry));

    /* Assign the index */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus =
        i4TestValFsVxlanVtepRowStatus;
    pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVtepTable (pu4ErrorCode, pVxlanFsVxlanVtepEntry,
                                      pVxlanIsSetFsVxlanVtepEntry, OSIX_FALSE,
                                      OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVtepEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVtepEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanNveRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                testValFsVxlanNveRemoteVtepAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanNveRemoteVtepAddressType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsVxlanNveIfIndex,
                                          UINT4 u4FsVxlanNveVniNumber,
                                          tMacAddr u1FsVxlanNveDestVmMac,
                                          INT4
                                          i4TestValFsVxlanNveRemoteVtepAddressType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType =
        i4TestValFsVxlanNveRemoteVtepAddressType;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType = OSIX_TRUE;

    if (VxlanTestAllFsVxlanNveTable (pu4ErrorCode, pVxlanFsVxlanNveEntry,
                                     pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
                                     OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanNveRemoteVtepAddress
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                testValFsVxlanNveRemoteVtepAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanNveRemoteVtepAddress (UINT4 *pu4ErrorCode,
                                      INT4 i4FsVxlanNveIfIndex,
                                      UINT4 u4FsVxlanNveVniNumber,
                                      tMacAddr u1FsVxlanNveDestVmMac,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsVxlanNveRemoteVtepAddress)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
            pTestValFsVxlanNveRemoteVtepAddress->pu1_OctetList,
            pTestValFsVxlanNveRemoteVtepAddress->i4_Length);
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen =
        pTestValFsVxlanNveRemoteVtepAddress->i4_Length;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress = OSIX_TRUE;

    if (VxlanTestAllFsVxlanNveTable (pu4ErrorCode, pVxlanFsVxlanNveEntry,
                                     pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
                                     OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanSuppressArp
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                testValFsVxlanSuppressArp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanSuppressArp (UINT4 *pu4ErrorCode,
                             INT4 i4FsVxlanNveIfIndex,
                             UINT4 u4FsVxlanNveVniNumber,
                             tMacAddr u1FsVxlanNveDestVmMac,
                             INT4 i4TestValFsVxlanSuppressArp)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, 6);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
        i4TestValFsVxlanSuppressArp;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp = OSIX_TRUE;

    if (VxlanTestAllFsVxlanNveTable (pu4ErrorCode, pVxlanFsVxlanNveEntry,
                                     pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
                                     OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (i4TestValFsVxlanSuppressArp);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVxlanNveVrfName
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                testValFsVxlanNveVrfName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned                                                                                                                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanNveVrfName (UINT4 *pu4ErrorCode,
                            INT4 i4FsVxlanNveIfIndex,
                            UINT4 u4FsVxlanNveVniNumber,
                            tMacAddr u1FsVxlanNveDestVmMac,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsVxlanNveVrfName)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsVxlanNveIfIndex);
    UNUSED_PARAM (u4FsVxlanNveVniNumber);
    UNUSED_PARAM (u1FsVxlanNveDestVmMac);
    UNUSED_PARAM (pTestValFsVxlanNveVrfName);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanNveRowStatus
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac

                The Object 
                testValFsVxlanNveRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanNveRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsVxlanNveIfIndex,
                              UINT4 u4FsVxlanNveVniNumber,
                              tMacAddr u1FsVxlanNveDestVmMac,
                              INT4 i4TestValFsVxlanNveRowStatus)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanIsSetFsVxlanNveEntry *pVxlanIsSetFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanNveEntry =
        (tVxlanIsSetFsVxlanNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanNveEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (pVxlanIsSetFsVxlanNveEntry, 0, sizeof (tVxlanIsSetFsVxlanNveEntry));

    /* Assign the index */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber = OSIX_TRUE;

    MEMCPY (&(pVxlanFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
            u1FsVxlanNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus =
        i4TestValFsVxlanNveRowStatus;
    pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsVxlanNveTable (pu4ErrorCode, pVxlanFsVxlanNveEntry,
                                     pVxlanIsSetFsVxlanNveEntry, OSIX_FALSE,
                                     OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanNveEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanNveEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanMCastGroupAddressType
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                testValFsVxlanMCastGroupAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanMCastGroupAddressType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsVxlanMCastNveIfIndex,
                                       UINT4 u4FsVxlanMCastVniNumber,
                                       INT4
                                       i4TestValFsVxlanMCastGroupAddressType)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType =
        i4TestValFsVxlanMCastGroupAddressType;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType = OSIX_TRUE;

    if (VxlanTestAllFsVxlanMCastTable (pu4ErrorCode, pVxlanFsVxlanMCastEntry,
                                       pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanMCastGroupAddress
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                testValFsVxlanMCastGroupAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanMCastGroupAddress (UINT4 *pu4ErrorCode,
                                   INT4 i4FsVxlanMCastNveIfIndex,
                                   UINT4 u4FsVxlanMCastVniNumber,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsVxlanMCastGroupAddress)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
            pTestValFsVxlanMCastGroupAddress->pu1_OctetList,
            pTestValFsVxlanMCastGroupAddress->i4_Length);
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen =
        pTestValFsVxlanMCastGroupAddress->i4_Length;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress = OSIX_TRUE;

    if (VxlanTestAllFsVxlanMCastTable (pu4ErrorCode, pVxlanFsVxlanMCastEntry,
                                       pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanMCastRowStatus
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber

                The Object 
                testValFsVxlanMCastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanMCastRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsVxlanMCastNveIfIndex,
                                UINT4 u4FsVxlanMCastVniNumber,
                                INT4 i4TestValFsVxlanMCastRowStatus)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanIsSetFsVxlanMCastEntry *pVxlanIsSetFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);

    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanMCastEntry =
        (tVxlanIsSetFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanMCastEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (pVxlanIsSetFsVxlanMCastEntry, 0,
            sizeof (tVxlanIsSetFsVxlanMCastEntry));

    /* Assign the index */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus =
        i4TestValFsVxlanMCastRowStatus;
    pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsVxlanMCastTable (pu4ErrorCode, pVxlanFsVxlanMCastEntry,
                                       pVxlanIsSetFsVxlanMCastEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanMCastEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanMCastEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanMapVniNumber
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                testValFsVxlanVniVlanMapVniNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanMapVniNumber (UINT4 *pu4ErrorCode,
                                     INT4 i4FsVxlanVniVlanMapVlanId,
                                     UINT4 u4TestValFsVxlanVniVlanMapVniNumber)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber =
        u4TestValFsVxlanVniVlanMapVniNumber;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanMapPktSent
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                testValFsVxlanVniVlanMapPktSent
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanMapPktSent (UINT4 *pu4ErrorCode,
                                   INT4 i4FsVxlanVniVlanMapVlanId,
                                   UINT4 u4TestValFsVxlanVniVlanMapPktSent)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent =
        u4TestValFsVxlanVniVlanMapPktSent;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanMapPktRcvd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                testValFsVxlanVniVlanMapPktRcvd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanMapPktRcvd (UINT4 *pu4ErrorCode,
                                   INT4 i4FsVxlanVniVlanMapVlanId,
                                   UINT4 u4TestValFsVxlanVniVlanMapPktRcvd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd =
        u4TestValFsVxlanVniVlanMapPktRcvd;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanMapPktDrpd
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                testValFsVxlanVniVlanMapPktDrpd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanMapPktDrpd (UINT4 *pu4ErrorCode,
                                   INT4 i4FsVxlanVniVlanMapVlanId,
                                   UINT4 u4TestValFsVxlanVniVlanMapPktDrpd)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd =
        u4TestValFsVxlanVniVlanMapPktDrpd;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanMapRowStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object 
                testValFsVxlanVniVlanMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanMapRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsVxlanVniVlanMapVlanId,
                                     INT4 i4TestValFsVxlanVniVlanMapRowStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapRowStatus =
        i4TestValFsVxlanVniVlanMapRowStatus;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVxlanVniVlanTagStatus
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId

                The Object
                testValFsVxlanVniVlanTagStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanVniVlanTagStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsVxlanVniVlanMapVlanId,
                                  INT4 i4TestValFsVxlanVniVlanTagStatus)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanIsSetFsVxlanVniVlanMapEntry *pVxlanIsSetFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);

    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsVxlanVniVlanMapEntry =
        (tVxlanIsSetFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsVxlanVniVlanMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (pVxlanIsSetFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

    /* Assign the index */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged =
        (UINT1) i4TestValFsVxlanVniVlanTagStatus;
    pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged = OSIX_TRUE;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (pu4ErrorCode, pVxlanFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanVniVlanMapEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVxlanInReplicaRemoteVtepAddressType
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                testValFsVxlanInReplicaRemoteVtepAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val                                                                                              Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                                                                       SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanInReplicaRemoteVtepAddressType (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsVxlanInReplicaNveIfIndex,
                                                UINT4
                                                u4FsVxlanInReplicaVniNumber,
                                                INT4 i4FsVxlanInReplicaIndex,
                                                INT4
                                                i4TestValFsVxlanInReplicaRemoteVtepAddressType)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry = (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsVxlanInReplicaEntry->MibObject.
        i4FsVxlanInReplicaRemoteVtepAddressType =
        i4TestValFsVxlanInReplicaRemoteVtepAddressType;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddressType =
        OSIX_TRUE;

    if (VxlanTestAllFsVxlanInReplicaTable
        (pu4ErrorCode, pVxlanFsVxlanInReplicaEntry,
         pVxlanIsSetFsVxlanInReplicaEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVxlanInReplicaRemoteVtepAddress
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                testValFsVxlanInReplicaRemoteVtepAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanInReplicaRemoteVtepAddress (UINT4 *pu4ErrorCode,
                                            INT4 i4FsVxlanInReplicaNveIfIndex,
                                            UINT4 u4FsVxlanInReplicaVniNumber,
                                            INT4 i4FsVxlanInReplicaIndex,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValFsVxlanInReplicaRemoteVtepAddress)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry = (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */

    MEMCPY (pVxlanFsVxlanInReplicaEntry->MibObject.
            au1FsVxlanInReplicaRemoteVtepAddress,
            pTestValFsVxlanInReplicaRemoteVtepAddress->pu1_OctetList,
            pTestValFsVxlanInReplicaRemoteVtepAddress->i4_Length);
    pVxlanFsVxlanInReplicaEntry->MibObject.
        i4FsVxlanInReplicaRemoteVtepAddressLen =
        pTestValFsVxlanInReplicaRemoteVtepAddress->i4_Length;

    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress =
        OSIX_TRUE;

    if (VxlanTestAllFsVxlanInReplicaTable
        (pu4ErrorCode, pVxlanFsVxlanInReplicaEntry,
         pVxlanIsSetFsVxlanInReplicaEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVxlanInReplicaRowStatus
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
                The Object
                testValFsVxlanInReplicaRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanInReplicaRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsVxlanInReplicaNveIfIndex,
                                    UINT4 u4FsVxlanInReplicaVniNumber,
                                    INT4 i4FsVxlanInReplicaIndex,
                                    INT4 i4TestValFsVxlanInReplicaRowStatus)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    tVxlanIsSetFsVxlanInReplicaEntry *pVxlanIsSetFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_POOLID);
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pVxlanIsSetFsVxlanInReplicaEntry =
        (tVxlanIsSetFsVxlanInReplicaEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID);
    if (pVxlanIsSetFsVxlanInReplicaEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (pVxlanIsSetFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

    /* Assign the index */
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanInReplicaNveIfIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex = OSIX_TRUE;
    pVxlanFsVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanInReplicaVniNumber;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber = OSIX_TRUE;

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaIndex =
        i4FsVxlanInReplicaIndex;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaIndex = OSIX_TRUE;

    /* Assign the value */

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRowStatus =
        i4TestValFsVxlanInReplicaRowStatus;
    pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsVxlanInReplicaTable
        (pu4ErrorCode, pVxlanFsVxlanInReplicaEntry,
         pVxlanIsSetFsVxlanInReplicaEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanInReplicaEntry);
    MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsVxlanInReplicaEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapBgpRD
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                                                                                                                                                                             The Object
                testValFsEvpnVxlanEviVniMapBgpRD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapBgpRD (UINT4 *pu4ErrorCode,
                                    INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsEvpnVxlanEviVniMapBgpRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->
            MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pTestValFsEvpnVxlanEviVniMapBgpRD->pu1_OctetList,
            pTestValFsEvpnVxlanEviVniMapBgpRD->i4_Length);
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen =
        pTestValFsEvpnVxlanEviVniMapBgpRD->i4_Length;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pTestValFsEvpnVxlanEviVniMapBgpRD);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniESI
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                                                                                                                                                                             The Object
                testValFsEvpnVxlanEviVniESI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniESI (UINT4 *pu4ErrorCode,
                               INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsEvpnVxlanEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniESI,
            pTestValFsEvpnVxlanEviVniESI->pu1_OctetList,
            pTestValFsEvpnVxlanEviVniESI->i4_Length);
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniESILen =
        pTestValFsEvpnVxlanEviVniESI->i4_Length;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pTestValFsEvpnVxlanEviVniESI);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniLoadBalance
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                testValFsEvpnVxlanEviVniLoadBalance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniLoadBalance (UINT4 *pu4ErrorCode,
                                       INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       INT4
                                       i4TestValFsEvpnVxlanEviVniLoadBalance)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniLoadBalance =
        i4TestValFsEvpnVxlanEviVniLoadBalance;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4TestValFsEvpnVxlanEviVniLoadBalance);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapSentPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                testValFsEvpnVxlanEviVniMapSentPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapSentPkts (UINT4 *pu4ErrorCode,
                                       INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4TestValFsEvpnVxlanEviVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapSentPkts =
        u4TestValFsEvpnVxlanEviVniMapSentPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanEviVniMapSentPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapRcvdPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object
                testValFsEvpnVxlanEviVniMapRcvdPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapRcvdPkts (UINT4 *pu4ErrorCode,
                                       INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4TestValFsEvpnVxlanEviVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapRcvdPkts =
        u4TestValFsEvpnVxlanEviVniMapRcvdPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanEviVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapDroppedPkts
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanEviVniMapDroppedPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapDroppedPkts (UINT4 *pu4ErrorCode,
                                          INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                          UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                          UINT4
                                          u4TestValFsEvpnVxlanEviVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
        u4FsEvpnVxlanEviVniMapDroppedPkts =
        u4TestValFsEvpnVxlanEviVniMapDroppedPkts;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapDroppedPkts =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanEviVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanEviVniMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                        INT4
                                        i4TestValFsEvpnVxlanEviVniMapRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapRowStatus =
        i4TestValFsEvpnVxlanEviVniMapRowStatus;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4TestValFsEvpnVxlanEviVniMapRowStatus);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapBgpRDAuto
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanEviVniMapBgpRDAuto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanEviVniMapBgpRDAuto (UINT4 *pu4ErrorCode,
                                        INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                        UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                        INT4
                                        i4TestValFsEvpnVxlanEviVniMapBgpRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanEviVniMapEntry =
        (tVxlanIsSetFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto =
        i4TestValFsEvpnVxlanEviVniMapBgpRDAuto;
    pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4TestValFsEvpnVxlanEviVniMapBgpRDAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanBgpRT
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                testValFsEvpnVxlanBgpRT
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanBgpRT (UINT4 *pu4ErrorCode,
                           INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                           UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                           UINT4 u4FsEvpnVxlanBgpRTIndex,
                           INT4 i4FsEvpnVxlanBgpRTType,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsEvpnVxlanBgpRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
            pTestValFsEvpnVxlanBgpRT->pu1_OctetList,
            pTestValFsEvpnVxlanBgpRT->i4_Length);
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen =
        pTestValFsEvpnVxlanBgpRT->i4_Length;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanBgpRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanBgpRTEntry,
         pVxlanIsSetFsEvpnVxlanBgpRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (*pTestValFsEvpnVxlanBgpRT);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanBgpRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                testValFsEvpnVxlanBgpRTRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanBgpRTRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4FsEvpnVxlanBgpRTIndex,
                                    INT4 i4FsEvpnVxlanBgpRTType,
                                    INT4 i4TestValFsEvpnVxlanBgpRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTRowStatus =
        i4TestValFsEvpnVxlanBgpRTRowStatus;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanBgpRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanBgpRTEntry,
         pVxlanIsSetFsEvpnVxlanBgpRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (i4TestValFsEvpnVxlanBgpRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanEviVniMapRowStatus
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType

                The Object 
                testValFsEvpnVxlanBgpRTAuto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanBgpRTAuto (UINT4 *pu4ErrorCode,
                               INT4 i4FsEvpnVxlanEviVniMapEviIndex,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               UINT4 u4FsEvpnVxlanBgpRTIndex,
                               INT4 i4FsEvpnVxlanBgpRTType,
                               INT4 i4TestValFsEvpnVxlanBgpRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry *pVxlanIsSetFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanBgpRTEntry =
        (tVxlanIsSetFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

    /* Assign the index */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
        OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex =
        u4FsEvpnVxlanBgpRTIndex;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex = OSIX_TRUE;

    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType =
        i4FsEvpnVxlanBgpRTType;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto =
        i4TestValFsEvpnVxlanBgpRTAuto;
    pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanBgpRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanBgpRTEntry,
         pVxlanIsSetFsEvpnVxlanBgpRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanBgpRTEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanEviVniMapEviIndex);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanBgpRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanBgpRTType);
    UNUSED_PARAM (i4TestValFsEvpnVxlanBgpRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRD
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
                testValFsEvpnVxlanVrfRD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRD (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                           UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsEvpnVxlanVrfRD)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry),
            pTestValFsEvpnVxlanVrfRD->pu1_OctetList,
            pTestValFsEvpnVxlanVrfRD->i4_Length);
    EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pTestValFsEvpnVxlanVrfRD->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (*pTestValFsEvpnVxlanVrfRD);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName

                The Object 
                testValFsEvpnVxlanVrfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                  UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                  INT4 i4TestValFsEvpnVxlanVrfRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;
    /* Assign the value */
    EVPN_P_VRF_ROW_STATUS (pVxlanFsEvpnVxlanVrfEntry) =
        i4TestValFsEvpnVxlanVrfRowStatus;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4TestValFsEvpnVxlanVrfRowStatus);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfVniMapSentPkts
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanVrfVniMapSentPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfVniMapSentPkts (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsEvpnVxlanVrfName,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4TestValFsEvpnVxlanVrfVniMapSentPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts =
        u4TestValFsEvpnVxlanVrfVniMapSentPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanVrfVniMapSentPkts);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfVniMapRcvdPkts
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanVrfVniMapRcvdPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfVniMapRcvdPkts (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsEvpnVxlanVrfName,
                                       UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                       UINT4
                                       u4TestValFsEvpnVxlanVrfVniMapRcvdPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts =
        u4TestValFsEvpnVxlanVrfVniMapRcvdPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanVrfVniMapRcvdPkts);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfVniMapDroppedPkts
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanVrfVniMapDroppedPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfVniMapDroppedPkts (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsEvpnVxlanVrfName,
                                          UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                          UINT4
                                          u4TestValFsEvpnVxlanVrfVniMapDroppedPkts)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts =
        u4TestValFsEvpnVxlanVrfVniMapDroppedPkts;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4TestValFsEvpnVxlanVrfVniMapDroppedPkts);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRDAuto
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber

                The Object 
                testValFsEvpnVxlanVrfRDAuto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRDAuto (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               INT4 i4TestValFsEvpnVxlanVrfRDAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfEntry *pVxlanIsSetFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfEntry =
        (tVxlanIsSetFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto =
        i4TestValFsEvpnVxlanVrfRDAuto;
    pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (i4TestValFsEvpnVxlanVrfRDAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRT
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
                testValFsEvpnVxlanVrfRT
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRT (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                           UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                           UINT4 u4FsEvpnVxlanVrfRTIndex,
                           INT4 i4FsEvpnVxlanVrfRTType,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsEvpnVxlanVrfRT)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
            pTestValFsEvpnVxlanVrfRT->pu1_OctetList,
            pTestValFsEvpnVxlanVrfRT->i4_Length);
    EVPN_P_VRF_RT_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pTestValFsEvpnVxlanVrfRT->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfRTEntry,
         pVxlanIsSetFsEvpnVxlanVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (*pTestValFsEvpnVxlanVrfRT);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRTRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
                testValFsEvpnVxlanVrfRTRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRTRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                                    UINT4 u4FsEvpnVxlanVrfRTIndex,
                                    INT4 i4FsEvpnVxlanVrfRTType,
                                    INT4 i4TestValFsEvpnVxlanVrfRTRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) =
        i4TestValFsEvpnVxlanVrfRTRowStatus;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfRTEntry,
         pVxlanIsSetFsEvpnVxlanVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (i4TestValFsEvpnVxlanVrfRTRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanVrfRTAuto
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType

                The Object 
                testValFsEvpnVxlanVrfRTAuto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                                                  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanVrfRTAuto (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               UINT4 u4FsEvpnVxlanEviVniMapVniNumber,
                               UINT4 u4FsEvpnVxlanVrfRTIndex,
                               INT4 i4FsEvpnVxlanVrfRTType,
                               INT4 i4TestValFsEvpnVxlanVrfRTAuto)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry *pVxlanIsSetFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);

    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanVrfRTEntry =
        (tVxlanIsSetFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
    MEMSET (pVxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

    /* Assign the index */
    MEMCPY (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);

    EVPN_P_VRF_RT_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry) =
        pFsEvpnVxlanVrfName->i4_Length;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_TRUE;

    EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry) =
        u4FsEvpnVxlanEviVniMapVniNumber;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfVniNumber = OSIX_TRUE;

    EVPN_P_VRF_RT_INDEX (pVxlanFsEvpnVxlanVrfRTEntry) = u4FsEvpnVxlanVrfRTIndex;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex = OSIX_TRUE;

    EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry) = i4FsEvpnVxlanVrfRTType;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto =
        i4TestValFsEvpnVxlanVrfRTAuto;
    pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanVrfRTTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanVrfRTEntry,
         pVxlanIsSetFsEvpnVxlanVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanVrfRTEntry);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pFsEvpnVxlanVrfName);
    UNUSED_PARAM (u4FsEvpnVxlanEviVniMapVniNumber);
    UNUSED_PARAM (u4FsEvpnVxlanVrfRTIndex);
    UNUSED_PARAM (i4FsEvpnVxlanVrfRTType);
    UNUSED_PARAM (i4TestValFsEvpnVxlanVrfRTAuto);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanOrdinalNum
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddress

                The Object 
                testValFsEvpnVxlanOrdinalNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanOrdinalNum (UINT4 *pu4ErrorCode,
                                INT4 i4FsEvpnVxlanPeerIpAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsEvpnVxlanPeerIpAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsEvpnVxlanMHEviVniESI,
                                UINT4 u4TestValFsEvpnVxlanOrdinalNum)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        * pVxlanIsSetFsEvpnVxlanMultihomedPeerTable = NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable =
        (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.u4FsEvpnVxlanOrdinalNum =
        u4TestValFsEvpnVxlanOrdinalNum;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum =
        OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanMultihomedPeerTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (u4TestValFsEvpnVxlanOrdinalNum);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanMultihomedPeerRowStatus
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI

                The Object 
                testValFsEvpnVxlanMultihomedPeerRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanMultihomedPeerRowStatus (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsEvpnVxlanPeerIpAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsEvpnVxlanPeerIpAddress,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsEvpnVxlanMHEviVniESI,
                                             INT4
                                             i4TestValFsEvpnVxlanMultihomedPeerRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        * pVxlanIsSetFsEvpnVxlanMultihomedPeerTable = NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);

    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return SNMP_FAILURE;
    }

    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable =
        (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID);

    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
    MEMSET (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

    /* Assign the index */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4FsEvpnVxlanPeerIpAddressType;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddressType =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanPeerIpAddress,
            pFsEvpnVxlanPeerIpAddress->pu1_OctetList,
            pFsEvpnVxlanPeerIpAddress->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = pFsEvpnVxlanPeerIpAddress->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress =
        OSIX_TRUE;

    MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pFsEvpnVxlanMHEviVniESI->pu1_OctetList,
            pFsEvpnVxlanMHEviVniESI->i4_Length);

    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = pFsEvpnVxlanMHEviVniESI->i4_Length;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI =
        OSIX_TRUE;

    /* Assign the value */
    pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMultihomedPeerRowStatus =
        i4TestValFsEvpnVxlanMultihomedPeerRowStatus;
    pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_TRUE;

    if (VxlanTestAllFsEvpnVxlanMultihomedPeerTable
        (pu4ErrorCode, pVxlanFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                            (UINT1 *)
                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
#else
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4FsEvpnVxlanPeerIpAddressType);
    UNUSED_PARAM (*pFsEvpnVxlanPeerIpAddress);
    UNUSED_PARAM (*pFsEvpnVxlanMHEviVniESI);
    UNUSED_PARAM (i4TestValFsEvpnVxlanMultihomedPeerRowStatus);
#endif /* EVPN_VXLAN_WANTED */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsVxlanEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanUdpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsVxlanUdpPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsVxlanTraceOption (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanNotificationCntl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsVxlanNotificationCntl (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsEvpnVxlanEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnAnycastGwMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnAnycastGwMac (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanVtepTable
 Input       :  The Indices
                FsVxlanVtepNveIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVxlanVtepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanNveTable
 Input       :  The Indices
                FsVxlanNveIfIndex
                FsVxlanNveVniNumber
                FsVxlanNveDestVmMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVxlanNveTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanMCastTable
 Input       :  The Indices
                FsVxlanMCastNveIfIndex
                FsVxlanMCastVniNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVxlanMCastTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanVniVlanMapTable
 Input       :  The Indices
                FsVxlanVniVlanMapVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVxlanVniVlanMapTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVxlanInReplicaTable
 Input       :  The Indices
                FsVxlanInReplicaNveIfIndex
                FsVxlanInReplicaVniNumber
                FsVxlanInReplicaIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVxlanInReplicaTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanEviVniMapTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanEviVniMapTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanBgpRTTable
 Input       :  The Indices
                FsEvpnVxlanEviVniMapEviIndex
                FsEvpnVxlanEviVniMapVniNumber
                FsEvpnVxlanBgpRTIndex
                FsEvpnVxlanBgpRTType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanBgpRTTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanVrfTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanVrfTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanVrfRTTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanVrfRTIndex
                FsEvpnVxlanVrfRTType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanVrfRTTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanMultihomedPeerTable
 Input       :  The Indices
                FsEvpnVxlanPeerIpAddressType
                FsEvpnVxlanPeerIpAddress
                FsEvpnVxlanMHEviVniESI
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanMultihomedPeerTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVxlanEcmpNveTable. */
/****************************************************************************                                                                                 Function    :  nmhValidateIndexInstanceFsVxlanEcmpNveTable                                                                                                   Input       :  The Indices
                FsVxlanEcmpNveIfIndex                                                                                                                                        FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVxlanEcmpNveTable (INT4 i4FsVxlanEcmpNveIfIndex,
                                             UINT4 u4FsVxlanEcmpNveVniNumber,
                                             tMacAddr u1FsVxlanEcmpNveDestVmMac,
                                             INT4
                                             i4FsVxlanEcmpNveRemoteVtepAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsVxlanEcmpNveRemoteVtepAddress)
{
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVxlanEcmpNveTable
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVxlanEcmpNveTable (INT4 *pi4FsVxlanEcmpNveIfIndex,
                                     UINT4 *pu4FsVxlanEcmpNveVniNumber,
                                     tMacAddr * pFsVxlanEcmpNveDestVmMac,
                                     INT4
                                     *pi4FsVxlanEcmpNveRemoteVtepAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsVxlanEcmpNveRemoteVtepAddress)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry = VxlanGetFirstFsVxlanEcmpNveTable ();

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsVxlanEcmpNveIfIndex =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex;

    *pu4FsVxlanEcmpNveVniNumber =
        pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber;

    MEMCPY (pFsVxlanEcmpNveDestVmMac,
            &(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);
    *pi4FsVxlanEcmpNveRemoteVtepAddressType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            i4FsVxlanEcmpNveRemoteVtepAddressLen);
    pFsVxlanEcmpNveRemoteVtepAddress->i4_Length =
        pVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressLen;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (pu4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (pFsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (pi4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVxlanEcmpNveTable
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                nextFsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                nextFsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                nextFsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                nextFsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress
                nextFsVxlanEcmpNveRemoteVtepAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVxlanEcmpNveTable (INT4 i4FsVxlanEcmpNveIfIndex,
                                    INT4 *pi4NextFsVxlanEcmpNveIfIndex,
                                    UINT4 u4FsVxlanEcmpNveVniNumber,
                                    UINT4 *pu4NextFsVxlanEcmpNveVniNumber,
                                    tMacAddr u1FsVxlanEcmpNveDestVmMac,
                                    tMacAddr * pNextFsVxlanEcmpNveDestVmMac,
                                    INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                                    INT4
                                    *pi4NextFsVxlanEcmpNveRemoteVtepAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsVxlanEcmpNveRemoteVtepAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsVxlanEcmpNveRemoteVtepAddress)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry VxlanFsVxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pNextVxlanFsVxlanEcmpNveEntry = NULL;
    MEMSET (&VxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    /* Assign the index */
    VxlanFsVxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    VxlanFsVxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(VxlanFsVxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    VxlanFsVxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (VxlanFsVxlanEcmpNveEntry.MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    pNextVxlanFsVxlanEcmpNveEntry =
        VxlanGetNextFsVxlanEcmpNveTable (&VxlanFsVxlanEcmpNveEntry);

    if (pNextVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsVxlanEcmpNveIfIndex =
        pNextVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex;

    *pu4NextFsVxlanEcmpNveVniNumber =
        pNextVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber;

    MEMCPY (pNextFsVxlanEcmpNveDestVmMac,
            &(pNextVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            VXLAN_ETHERNET_ADDR_SIZE);
    *pi4NextFsVxlanEcmpNveRemoteVtepAddressType =
        pNextVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pNextFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pNextVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pNextVxlanFsVxlanEcmpNveEntry->MibObject.
            i4FsVxlanEcmpNveRemoteVtepAddressLen);

    pNextFsVxlanEcmpNveRemoteVtepAddress->i4_Length =
        pNextVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveRemoteVtepAddressLen;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (pi4NextFsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (pu4NextFsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (pNextFsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pi4NextFsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pNextFsVxlanEcmpNveRemoteVtepAddress);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpNveVtepAddressType
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpNveVtepAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpNveVtepAddressType (INT4 i4FsVxlanEcmpNveIfIndex,
                                     UINT4 u4FsVxlanEcmpNveVniNumber,
                                     tMacAddr u1FsVxlanEcmpNveDestVmMac,
                                     INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsVxlanEcmpNveRemoteVtepAddress,
                                     INT4
                                     *pi4RetValFsVxlanEcmpNveVtepAddressType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    /* Assign the index */
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanEcmpNveVtepAddressType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType;

    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pi4RetValFsVxlanEcmpNveVtepAddressType);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpNveVtepAddress
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpNveVtepAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpNveVtepAddress (INT4 i4FsVxlanEcmpNveIfIndex,
                                 UINT4 u4FsVxlanEcmpNveVniNumber,
                                 tMacAddr u1FsVxlanEcmpNveDestVmMac,
                                 INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsVxlanEcmpNveRemoteVtepAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsVxlanEcmpNveVtepAddress)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    /* Assign the index */
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanEcmpNveVtepAddress->pu1_OctetList,
            pVxlanFsVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpNveVtepAddress,
            pVxlanFsVxlanEcmpNveEntry->MibObject.
            i4FsVxlanEcmpNveVtepAddressLen);
    pRetValFsVxlanEcmpNveVtepAddress->i4_Length =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen;

    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else

    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pRetValFsVxlanEcmpNveVtepAddress);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpNveStorageType
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpNveStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpNveStorageType (INT4 i4FsVxlanEcmpNveIfIndex,
                                 UINT4 u4FsVxlanEcmpNveVniNumber,
                                 tMacAddr u1FsVxlanEcmpNveDestVmMac,
                                 INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsVxlanEcmpNveRemoteVtepAddress,
                                 INT4 *pi4RetValFsVxlanEcmpNveStorageType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    /* Assign the index */ pVxlanFsVxlanEcmpNveEntry->MibObject.
        i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanEcmpNveStorageType =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveStorageType;
    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else

    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pi4RetValFsVxlanEcmpNveStorageType);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpSuppressArp
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpSuppressArp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpSuppressArp (INT4 i4FsVxlanEcmpNveIfIndex,
                              UINT4 u4FsVxlanEcmpNveVniNumber,
                              tMacAddr u1FsVxlanEcmpNveDestVmMac,
                              INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsVxlanEcmpNveRemoteVtepAddress,
                              INT4 *pi4RetValFsVxlanEcmpSuppressArp)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    /* Assign the index */
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanEcmpSuppressArp =
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpSuppressArp;
    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pi4RetValFsVxlanEcmpSuppressArp);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpMHEviVniESI
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                u1FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpMHEviVniESI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpMHEviVniESI (INT4 i4FsVxlanEcmpNveIfIndex,
                              UINT4 u4FsVxlanEcmpNveVniNumber,
                              tMacAddr u1FsVxlanEcmpNveDestVmMac,
                              INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsVxlanEcmpNveRemoteVtepAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsVxlanEcmpMHEviVniESI)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));    /* Assign the index */
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsVxlanEcmpMHEviVniESI->pu1_OctetList,
            pVxlanFsVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
            EVPN_MAX_ESI_LEN);
    pRetValFsVxlanEcmpMHEviVniESI->i4_Length = EVPN_MAX_ESI_LEN;
    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pRetValFsVxlanEcmpMHEviVniESI);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpActive
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpActive (INT4 i4FsVxlanEcmpNveIfIndex,
                         UINT4 u4FsVxlanEcmpNveVniNumber,
                         tMacAddr u1FsVxlanEcmpNveDestVmMac,
                         INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                         tSNMP_OCTET_STRING_TYPE *
                         pFsVxlanEcmpNveRemoteVtepAddress,
                         INT4 *pi4RetValFsVxlanEcmpActive)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));    /* Assign the index */
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
        i4FsVxlanEcmpNveIfIndex;

    pVxlanFsVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
        u4FsVxlanEcmpNveVniNumber;

    MEMCPY (&(pVxlanFsVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
            u1FsVxlanEcmpNveDestVmMac, VXLAN_ETHERNET_ADDR_SIZE);
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4FsVxlanEcmpNveRemoteVtepAddressType;
    MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
            au1FsVxlanEcmpNveRemoteVtepAddress,
            pFsVxlanEcmpNveRemoteVtepAddress->pu1_OctetList,
            pFsVxlanEcmpNveRemoteVtepAddress->i4_Length);

    if (VxlanGetAllFsVxlanEcmpNveTable (pVxlanFsVxlanEcmpNveEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsVxlanEcmpActive =
        pVxlanFsVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpActive;
    MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                        (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
#else
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pi4RetValFsVxlanEcmpActive);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVxlanEcmpVrfName
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                retValFsVxlanEcmpVrfName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVxlanEcmpVrfName (INT4 i4FsVxlanEcmpNveIfIndex,
                          UINT4 u4FsVxlanEcmpNveVniNumber,
                          tMacAddr u1FsVxlanEcmpNveDestVmMac,
                          INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsVxlanEcmpNveRemoteVtepAddress,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsVxlanEcmpVrfName)
{
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pRetValFsVxlanEcmpVrfName);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVxlanEcmpVrfName
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                setValFsVxlanEcmpVrfName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVxlanEcmpVrfName (INT4 i4FsVxlanEcmpNveIfIndex,
                          UINT4 u4FsVxlanEcmpNveVniNumber,
                          tMacAddr u1FsVxlanEcmpNveDestVmMac,
                          INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsVxlanEcmpNveRemoteVtepAddress,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsVxlanEcmpVrfName)
{
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pSetValFsVxlanEcmpVrfName);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVxlanEcmpVrfName
 Input       :  The Indices
                FsVxlanEcmpNveIfIndex
                FsVxlanEcmpNveVniNumber
                FsVxlanEcmpNveDestVmMac
                FsVxlanEcmpNveRemoteVtepAddressType
                FsVxlanEcmpNveRemoteVtepAddress

                The Object
                testValFsVxlanEcmpVrfName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVxlanEcmpVrfName (UINT4 *pu4ErrorCode, INT4 i4FsVxlanEcmpNveIfIndex,
                             UINT4 u4FsVxlanEcmpNveVniNumber,
                             tMacAddr u1FsVxlanEcmpNveDestVmMac,
                             INT4 i4FsVxlanEcmpNveRemoteVtepAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsVxlanEcmpNveRemoteVtepAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsVxlanEcmpVrfName)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsVxlanEcmpNveIfIndex);
    UNUSED_PARAM (u4FsVxlanEcmpNveVniNumber);
    UNUSED_PARAM (u1FsVxlanEcmpNveDestVmMac);
    UNUSED_PARAM (i4FsVxlanEcmpNveRemoteVtepAddressType);
    UNUSED_PARAM (pFsVxlanEcmpNveRemoteVtepAddress);
    UNUSED_PARAM (pTestValFsVxlanEcmpVrfName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvpnVxlanArpTable
 Input       :  The Indices
                FsEvpnVxlanVrfName                                                                                                                                           FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
                                                                                                                                                             ****************************************************************************//* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvpnVxlanArpTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsEvpnVxlanVrfName,
                                             INT4
                                             i4FsEvpnVxlanArpDestAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsEvpnVxlanArpDestAddres)
{
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvpnVxlanArpTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvpnVxlanArpTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsEvpnVxlanVrfName,
                                     INT4 *pi4FsEvpnVxlanArpDestAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsEvpnVxlanArpDestAddres)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    pVxlanFsEvpnVxlanArpEntry = VxlanGetFirstFsEvpnVxlanArpTable ();

    if (pVxlanFsEvpnVxlanArpEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    /* Assign Vrf name */
    MEMCPY (pFsEvpnVxlanVrfName->pu1_OctetList,
            pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanVrfName,
            pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanVrfNameLen);
    pFsEvpnVxlanVrfName->i4_Length =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanVrfNameLen;
    /* Assign Address Type */
    *pi4FsEvpnVxlanArpDestAddressType = pVxlanFsEvpnVxlanArpEntry->MibObject.
        i4FsEvpnVxlanArpDestAddressType;
    /* Assign IP Address */
    MEMCPY (pFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanArpDestAddres,
            pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddresLen);
    pFsEvpnVxlanArpDestAddres->i4_Length =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddresLen;
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pi4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvpnVxlanArpTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                nextFsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                nextFsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres
                nextFsEvpnVxlanArpDestAddres
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEvpnVxlanArpTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanVrfName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsEvpnVxlanVrfName,
                                    INT4 i4FsEvpnVxlanArpDestAddressType,
                                    INT4 *pi4NextFsEvpnVxlanArpDestAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsEvpnVxlanArpDestAddres,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsEvpnVxlanArpDestAddres)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanArpEntry VxlanFsEvpnVxlanArpEntry;
    tVxlanFsEvpnVxlanArpEntry *pNextVxlanFsEvpnVxlanArpEntry = NULL;
    MEMSET (&VxlanFsEvpnVxlanArpEntry, 0, sizeof (tVxlanFsEvpnVxlanArpEntry));

    /* Assign the index */
    /* Assign Vrf name */
    MEMCPY (VxlanFsEvpnVxlanArpEntry.MibObject.au1FsEvpnVxlanVrfName,
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);
    /* Assign Address Type */
    VxlanFsEvpnVxlanArpEntry.MibObject.i4FsEvpnVxlanArpDestAddressType =
        i4FsEvpnVxlanArpDestAddressType;
    /* Assign IP Address */
    MEMCPY (VxlanFsEvpnVxlanArpEntry.MibObject.au1FsEvpnVxlanArpDestAddres,
            pFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pFsEvpnVxlanArpDestAddres->i4_Length);

    pNextVxlanFsEvpnVxlanArpEntry =
        VxlanGetNextFsEvpnVxlanArpTable (&VxlanFsEvpnVxlanArpEntry);

    if (pNextVxlanFsEvpnVxlanArpEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    /* Assign Vrf name */
    MEMCPY (pNextFsEvpnVxlanVrfName->pu1_OctetList,
            pNextVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanVrfName,
            pNextVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanVrfNameLen);
    /* Assign Address Type */
    *pi4NextFsEvpnVxlanArpDestAddressType =
        pNextVxlanFsEvpnVxlanArpEntry->MibObject.
        i4FsEvpnVxlanArpDestAddressType;
    /* Assign IP Address */
    MEMCPY (pNextFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pNextVxlanFsEvpnVxlanArpEntry->MibObject.
            au1FsEvpnVxlanArpDestAddres,
            pNextVxlanFsEvpnVxlanArpEntry->MibObject.
            i4FsEvpnVxlanArpDestAddresLen);
    pNextFsEvpnVxlanArpDestAddres->i4_Length =
        pNextVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddresLen;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (pNextFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pi4NextFsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (pNextFsEvpnVxlanArpDestAddres);
    return SNMP_FAILURE;
#endif

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanArpDestMac
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                retValFsEvpnVxlanArpDestMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanArpDestMac (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                             INT4 i4FsEvpnVxlanArpDestAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsEvpnVxlanArpDestAddres,
                             tMacAddr * pRetValFsEvpnVxlanArpDestMac)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    pVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANARPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanArpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanArpEntry, 0, sizeof (tVxlanFsEvpnVxlanArpEntry));

    /* Assign the index */
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanVrfName,
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);
    pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddressType =
        i4FsEvpnVxlanArpDestAddressType;
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanArpDestAddres,
            pFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pFsEvpnVxlanArpDestAddres->i4_Length);

    if (VxlanGetAllFsEvpnVxlanArpTable (pVxlanFsEvpnVxlanArpEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsEvpnVxlanArpDestMac,
            pVxlanFsEvpnVxlanArpEntry->MibObject.FsEvpnVxlanArpDestMac,
            VXLAN_ETHERNET_ADDR_SIZE);
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (pRetValFsEvpnVxlanArpDestMac);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanArpStorageType
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                retValFsEvpnVxlanArpStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanArpStorageType (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                 INT4 i4FsEvpnVxlanArpDestAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsEvpnVxlanArpDestAddres,
                                 INT4 *pi4RetValFsEvpnVxlanArpStorageType)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    pVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANARPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanArpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanArpEntry, 0, sizeof (tVxlanFsEvpnVxlanArpEntry));

    /* Assign the index */
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanVrfName,
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);
    pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddressType =
        i4FsEvpnVxlanArpDestAddressType;
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanArpDestAddres,
            pFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pFsEvpnVxlanArpDestAddres->i4_Length);

    if (VxlanGetAllFsEvpnVxlanArpTable (pVxlanFsEvpnVxlanArpEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanArpStorageType =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsVxlanEvpnArpStorageType;
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (pi4RetValFsEvpnVxlanArpStorageType);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsEvpnVxlanArpRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                retValFsEvpnVxlanArpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvpnVxlanArpRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               INT4 i4FsEvpnVxlanArpDestAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsEvpnVxlanArpDestAddres,
                               INT4 *pi4RetValFsEvpnVxlanArpRowStatus)
{
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    pVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANARPTABLE_POOLID);

    if (pVxlanFsEvpnVxlanArpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pVxlanFsEvpnVxlanArpEntry, 0, sizeof (tVxlanFsEvpnVxlanArpEntry));

    /* Assign the index */
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanVrfName,
            pFsEvpnVxlanVrfName->pu1_OctetList, pFsEvpnVxlanVrfName->i4_Length);
    pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsEvpnVxlanArpDestAddressType =
        i4FsEvpnVxlanArpDestAddressType;
    MEMCPY (pVxlanFsEvpnVxlanArpEntry->MibObject.au1FsEvpnVxlanArpDestAddres,
            pFsEvpnVxlanArpDestAddres->pu1_OctetList,
            pFsEvpnVxlanArpDestAddres->i4_Length);

    if (VxlanGetAllFsEvpnVxlanArpTable (pVxlanFsEvpnVxlanArpEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsEvpnVxlanArpRowStatus =
        pVxlanFsEvpnVxlanArpEntry->MibObject.i4FsVxlanArpRowStatus;
    MemReleaseMemBlock (VXLAN_FSEVPNVXLANARPTABLE_POOLID,
                        (UINT1 *) pVxlanFsEvpnVxlanArpEntry);
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (pi4RetValFsEvpnVxlanArpRowStatus);
    return SNMP_FAILURE;
#endif

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanArpDestMac
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                setValFsEvpnVxlanArpDestMac
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanArpDestMac (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                             INT4 i4FsEvpnVxlanArpDestAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsEvpnVxlanArpDestAddres,
                             tMacAddr SetValFsEvpnVxlanArpDestMac)
{
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (SetValFsEvpnVxlanArpDestMac);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvpnVxlanArpRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                setValFsEvpnVxlanArpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvpnVxlanArpRowStatus (tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                               INT4 i4FsEvpnVxlanArpDestAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsEvpnVxlanArpDestAddres,
                               INT4 i4SetValFsEvpnVxlanArpRowStatus)
{
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (i4SetValFsEvpnVxlanArpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanArpDestMac
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                testValFsEvpnVxlanArpDestMac
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanArpDestMac (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                INT4 i4FsEvpnVxlanArpDestAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsEvpnVxlanArpDestAddres,
                                tMacAddr TestValFsEvpnVxlanArpDestMac)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (TestValFsEvpnVxlanArpDestMac);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvpnVxlanArpRowStatus
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres

                The Object
                testValFsEvpnVxlanArpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvpnVxlanArpRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsEvpnVxlanVrfName,
                                  INT4 i4FsEvpnVxlanArpDestAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsEvpnVxlanArpDestAddres,
                                  INT4 i4TestValFsEvpnVxlanArpRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsEvpnVxlanVrfName);
    UNUSED_PARAM (i4FsEvpnVxlanArpDestAddressType);
    UNUSED_PARAM (pFsEvpnVxlanArpDestAddres);
    UNUSED_PARAM (i4TestValFsEvpnVxlanArpRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEvpnVxlanArpTable
 Input       :  The Indices
                FsEvpnVxlanVrfName
                FsEvpnVxlanArpDestAddressType
                FsEvpnVxlanArpDestAddres
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvpnVxlanArpTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
