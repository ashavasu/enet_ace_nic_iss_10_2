/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxfwd.c,v 1.24 2018/01/31 11:33:30 siva Exp $
*
*********************************************************************/

#include "vxinc.h"

extern INT4
        CfaExtractIpHdr (t_IP_HEADER * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);
/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanEncapAndFwdPkt                                        */
/*                                                                           */
/* Description  : Adds the VXLAN header to the given packet and sends on     */
/*                the destination port                                       */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*                u4IfIndex  - Interface on which the packet has come        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VXLAN_SUCCESS or VXLAN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
VxlanEncapAndFwdPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    UINT2               u2VlanId = 0;
    tMacAddr            DestVmAddr;
    tMacAddr            ZeroDestVmAddr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT4               u4VniId = 0;
    UINT4               u4NveIndex = 0;
    UINT4               u4Tmp = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DestAddr = 0;
    UINT4               u4TempAddr = 0;
    UINT4               u4Port = 0;
    UINT4               u4PrevPort = 0;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tNetIpv6IfInfo      Ip6Info;
    UINT4               u4PhyIfIndex = 0;
#ifdef EVPN_VXLAN_WANTED
    UINT2               u2Prot = 0;
    INT4                i4EviIndex = 0;
    UINT1               au1IpAddress[16];
    UINT4               u4HashIndex = 0;
    INT4                i4EviVniLoadBalance = 0;
    UINT4               u4Index = 0;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;
    UINT1               au1VrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    UINT4               u4VrfId = 0;
    UINT4               u4TempAddress = 0;
#ifdef IP6_WANTED
    tIcmp6PktHdr       *pIcmp6 = NULL;
    tIp6Hdr            *pIp6Hdr = NULL;
#endif
#endif
#ifdef NPAPI_WANTED
    UINT2               u2IfaceIndex = 0;
    UINT1               u1Status = 0;
#endif
    tMacAddr            SrcVmAddr;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
#ifdef IP6_WANTED
    tIp6Addr            SrcAddr;
    tIp6Addr            DstAddr;

    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanEncapAndFwdPkt: Entry\n"));

    MEMSET (&DestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&ZeroDestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&au1MacStr, 0, VXLAN_MAC_STR_LEN);

    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));
#ifdef EVPN_VXLAN_WANTED
    MEMSET (&SrcVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    MEMSET (au1VrfName, 0, VXLAN_CLI_MAX_VRF_NAME_LEN);
#endif
    /* Get the Destination mac address from the frame */
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) (&DestVmAddr), 0,
                                   VXLAN_ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting destination address from"
                         "L2 packet failed \n"));
        return VXLAN_FAILURE;
    }

    /* Get the VLAN id from the frame */
    u4Tmp = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId, u4Tmp, 2) ==
        CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting VLAN id from L2"
                         "packet failed \n"));
        return VXLAN_FAILURE;
    }

    u2VlanId = OSIX_NTOHS (u2VlanId);
    u2VlanId = 0x0fff & u2VlanId;

    /*Get the VNI id for the VLAN */
    if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniId) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         "VNI is not mapped for the VLAN %d\n", u4VniId));
        return VXLAN_FAILURE;
    }
#ifdef EVPN_VXLAN_WANTED
    /* Get the Vrf name from Vlan ID */
    if (VxlanUtilGetVrfFromVlan (u2VlanId, (UINT1 *) au1VrfName) ==
        VXLAN_SUCCESS)
    {
        if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
        }
    }
#endif
#ifdef EVPN_VXLAN_WANTED
    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) (&SrcVmAddr), VXLAN_ETHERNET_ADDR_SIZE,
         VXLAN_ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting destination address from"
                         "L2 packet failed \n"));
        return VXLAN_FAILURE;
    }
    u4Tmp += CFA_VLAN_PROTOCOL_SIZE;
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Prot, u4Tmp, 2) ==
        CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting Protocol id from L3"
                         "packet failed \n"));
        return VXLAN_FAILURE;
    }
    u2Prot = OSIX_NTOHS (u2Prot);
    MEMSET (au1IpAddress, 0, 16);
    if (u2Prot == CFA_ENET_ARP)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, au1IpAddress, 12 + IP_HDR_LEN, 4)
            == CRU_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting Protocol id from L3"
                             "packet failed \n"));
            return VXLAN_FAILURE;
        }
    }
    else if (u2Prot == CFA_ENET_IPV6)
    {
#ifdef IP6_WANTED
        /* Buffer read offset should be at the start of the icmp6 header */
        pIcmp6 = (tIcmp6PktHdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                                         (18 +
                                                                          IPV6_HEADER_LEN),
                                                                         sizeof
                                                                         (tIcmp6PktHdr));
        if (pIcmp6 == NULL)
        {
            return VXLAN_FAILURE;
        }
        if ((pIcmp6->u1Type == ND6_NEIGHBOR_SOLICITATION) ||
            (pIcmp6->u1Type == ND6_NEIGHBOR_ADVERTISEMENT))
        {
            pIp6Hdr =
                (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 18,
                                                                   sizeof
                                                                   (tIp6Hdr));
            if (pIp6Hdr != NULL)
            {
                MEMCPY (au1IpAddress, (UINT1 *) &pIp6Hdr->srcAddr,
                        sizeof (tIp6Addr));
            }
        }
#endif
    }
    if (u2Prot == CFA_ENET_ARP)
    {
        PTR_FETCH4 (u4TempAddress, au1IpAddress);
        MEMCPY (au1IpAddress, &u4TempAddress, 4);
        pVxlanNveEntry =
            VxlanUtilGetNveDatabase (u4VniId, ZeroDestVmAddr, u4IfIndex);
        if (pVxlanNveEntry != NULL)
        {
            if (EVPN_CLI_ARP_SUPPRESS ==
                pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp)
            {
                if (EVPN_NOTIFY_MAC_UPDATE_CB != NULL)
                {
                    EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0, SrcVmAddr,
                                               0, NULL, VLAN_CB_REMOVE_MAC,
                                               VLAN_FDB_LEARNT);
                    EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0, SrcVmAddr,
                                               4, au1IpAddress,
                                               VLAN_CB_UPDATE_MAC,
                                               VLAN_FDB_LEARNT);
                    pVxlanEvpnArpSupLocalMacEntry =
                        (tVxlanEvpnArpSupLocalMacEntry *)
                        MemAllocMemBlk (VXLAN_EVPNARPSUPMACTABLE_POOLID);
                    if (pVxlanEvpnArpSupLocalMacEntry != NULL)
                    {
                        MEMCPY (pVxlanEvpnArpSupLocalMacEntry->SourceMac,
                                SrcVmAddr, sizeof (tMacAddr));
                        pVxlanEvpnArpSupLocalMacEntry->u4Vni = u4VniId;
                        pVxlanEvpnArpSupLocalMacEntry->u1AddressLen = 4;
                        MEMCPY (pVxlanEvpnArpSupLocalMacEntry->au1IpAddress,
                                au1IpAddress, 4);
                        if (RBTreeAdd
                            (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                             (tRBElem *) pVxlanEvpnArpSupLocalMacEntry) !=
                            RB_SUCCESS)
                        {
                            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                             "RB Tree addition failed\n"));
                        }
                    }
                }
            }
        }
    }
    else if (u2Prot == CFA_ENET_IPV6)
    {
#ifdef IP6_WANTED
        pVxlanNveEntry =
            VxlanUtilGetNveDatabase (u4VniId, ZeroDestVmAddr, u4IfIndex);
        if (pVxlanNveEntry != NULL)
        {
            if ((EVPN_CLI_ARP_SUPPRESS ==
                 pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp)
                && ((pIcmp6->u1Type == ND6_NEIGHBOR_SOLICITATION)
                    || (pIcmp6->u1Type == ND6_NEIGHBOR_ADVERTISEMENT)))
            {
                if (pIp6Hdr != NULL)
                {
                    if (IS_ADDR_MULTI (pIp6Hdr->dstAddr))
                    {
                        if (EVPN_NOTIFY_MAC_UPDATE_CB != NULL)
                        {
                            EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                                       SrcVmAddr, 0, NULL,
                                                       VLAN_CB_REMOVE_MAC,
                                                       VLAN_FDB_LEARNT);
                            EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                                       SrcVmAddr, 16,
                                                       au1IpAddress,
                                                       VLAN_CB_UPDATE_MAC,
                                                       VLAN_FDB_LEARNT);
                            pVxlanEvpnArpSupLocalMacEntry =
                                (tVxlanEvpnArpSupLocalMacEntry *)
                                MemAllocMemBlk
                                (VXLAN_EVPNARPSUPMACTABLE_POOLID);
                            if (pVxlanEvpnArpSupLocalMacEntry != NULL)
                            {
                                MEMCPY (pVxlanEvpnArpSupLocalMacEntry->
                                        SourceMac, SrcVmAddr,
                                        sizeof (tMacAddr));
                                pVxlanEvpnArpSupLocalMacEntry->u1AddressLen =
                                    16;
                                MEMCPY (pVxlanEvpnArpSupLocalMacEntry->
                                        au1IpAddress, au1IpAddress, 16);
                                if (RBTreeAdd
                                    (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                                     (tRBElem *) pVxlanEvpnArpSupLocalMacEntry)
                                    != RB_SUCCESS)
                                {
                                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                                     "RB Tree addition failed\n"));
                                }
                            }
                        }
                    }
                }
            }
        }
#endif
    }
#endif
#ifdef NPAPI_WANTED
    /* Tag support is not available in Alta board, Hence removed the inner tag */
    VlanUnTagFrame (pBuf);
#endif

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    CliMacToStr (DestVmAddr, au1MacStr);
    au1MacStr[17] = '\0';

    /* Get the NVE database using VNI id, Destination VM mac address
     * and NVE if index
     * */
    pVxlanNveEntry = VxlanUtilGetNveDatabase (u4VniId, DestVmAddr, u4IfIndex);

    /* If nve datbase is not available for the destination mac address, check
     * the Ingress Replica database, If available it is unicast transmission.
     * Get reomote vtep information from the Ingress Replica
     * database and give the packet to SLI module */

    if (pVxlanNveEntry == NULL)
    {
        /* Nve entry is not available, check the EcmpNve Entry */
        /* Get the Evi Index */

#ifdef EVPN_VXLAN_WANTED
        if (EvpnUtilGetEviFromVni (u4VniId, &i4EviIndex) == VXLAN_SUCCESS)
        {
            VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4VniId;
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
                (INT4) u4IfIndex;
            MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                    DestVmAddr, sizeof (tMacAddr));
            pVxlanEcmpNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                               (tRBElem *) & VxlanEcmpNveEntry, NULL);
            if (pVxlanEcmpNveEntry != NULL)
            {
                nmhGetFsEvpnVxlanEviVniLoadBalance (i4EviIndex, u4VniId,
                                                    &i4EviVniLoadBalance);

                if (i4EviVniLoadBalance == EVPN_CLI_LB_DISABLE)
                {
                    while ((pVxlanEcmpNveEntry != NULL) &&
                           (pVxlanEcmpNveEntry->MibObject.
                            u4FsVxlanEcmpNveVniNumber == u4VniId)
                           && (pVxlanEcmpNveEntry->MibObject.
                               i4FsVxlanEcmpNveIfIndex == (INT4) u4IfIndex)
                           &&
                           (MEMCMP
                            (pVxlanEcmpNveEntry->MibObject.
                             FsVxlanEcmpNveDestVmMac, DestVmAddr,
                             sizeof (tMacAddr)) == 0))
                    {
                        if (pVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpActive ==
                            VXLAN_TRUE)
                        {
                            break;
                        }
                        pVxlanEcmpNveEntry =
                            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                           FsVxlanEcmpNveTable,
                                           (tRBElem *) pVxlanEcmpNveEntry,
                                           NULL);
                    }
                }
                else
                {
                    if (EvpnNveEntryTableHashFn
                        (&SrcVmAddr, &u4HashIndex, u4VniId,
                         i4EviIndex) == VXLAN_FAILURE)
                    {
                        pVxlanEcmpNveEntry = 0;
                    }
                    else
                    {
                        while (u4Index < u4HashIndex)
                        {
                            pVxlanEcmpNveEntry =
                                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                               FsVxlanEcmpNveTable,
                                               (tRBElem *) pVxlanEcmpNveEntry,
                                               NULL);
                            u4Index++;
                        }
                    }
                }
                if (pVxlanEcmpNveEntry != NULL)
                {
                    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                     "ECMP NVE database is found for"
                                     "VNI id - %d, Destination VM Mac address - %s, "
                                     "NVE if index - %d\n", u4VniId, au1MacStr,
                                     u4IfIndex));

                    /* Add the VXLAN header */
                    /* draft-mahalingam-dutt-dcops-vxlan section 5 */
                    VxlanUtilAddHdr (pBuf, u4VniId);

                    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                                     "VXLAN header successfully encapsulated\n"));

                    /* Include the VXLAN packet size */
                    u4PktSize = u4PktSize + VXLAN_HDR_TOTAL_OFFSET;

                    /* For IPV4 transmission */
                    if (pVxlanEcmpNveEntry->MibObject.
                        i4FsVxlanEcmpNveVtepAddressType == VXLAN_IPV4_UNICAST)
                    {
                        /* Get the source VTEP address from NVE databse */
                        MEMCPY (&u4TempAddr,
                                pVxlanEcmpNveEntry->MibObject.
                                au1FsVxlanEcmpNveVtepAddress,
                                VXLAN_IP4_ADDR_LEN);
                        u4SrcAddr = OSIX_HTONL (u4TempAddr);

                        /* Get the Remote VTEP address from the NVE database */
                        MEMCPY (&u4TempAddr,
                                pVxlanEcmpNveEntry->MibObject.
                                au1FsVxlanEcmpNveRemoteVtepAddress,
                                VXLAN_IP4_ADDR_LEN);
                        u4DestAddr = OSIX_HTONL (u4TempAddr);

                        /* Give the packet to SLI socket layer to transmit the packet */
                        if (VxlanUdpTransmitVxlanPkt
                            (pBuf, u4SrcAddr, u4DestAddr, (UINT2) u4PktSize,
                             0) == VXLAN_FAILURE)
                        {
                            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                             "Failed in packet transmission\n"));
                            return VXLAN_FAILURE;
                        }
                        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                                         "IPV4 VXLAN packet is successfully"
                                         "given to UDP module\n"));

                    }
#ifdef IP6_WANTED
                    /* For IPV6 transmission */
                    else
                    {
                        /* Get the source VTEP address from NVE databse */
                        MEMCPY (&SrcAddr,
                                pVxlanEcmpNveEntry->MibObject.
                                au1FsVxlanEcmpNveVtepAddress, sizeof (SrcAddr));

                        /* Get the Remote VTEP address from the NVE database */
                        MEMCPY (&DstAddr,
                                pVxlanEcmpNveEntry->MibObject.
                                au1FsVxlanEcmpNveRemoteVtepAddress,
                                sizeof (DstAddr));

                        /* Give the packet to SLI socket layer to transmit the packet */
                        if (VxlanUdpv6TransmitVxlanPkt
                            (pBuf, &SrcAddr, &DstAddr, (UINT2) u4PktSize,
                             0) == VXLAN_FAILURE)
                        {
                            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                             "Failed in packet transmission\n"));
                            return VXLAN_FAILURE;
                        }
                        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                                         "IPV6 VXLAN packet is successfully"
                                         "given to UDP module\n"));
                    }
#endif
                    VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);
                    EvpnUtilUpdateStats (u4VniId, i4EviIndex,
                                         EVPN_STAT_PKT_SENT);

                    return VXLAN_SUCCESS;
                }
            }
        }
#endif

        MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
        if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniId, &u4NveIndex)) !=
            NULL)
        {
#ifdef NPAPI_WANTED
            if ((VlanGetFdbEntryDetails ((UINT4) u2VlanId, SrcVmAddr,
                                         &u2IfaceIndex, &u1Status)
                 != VLAN_SUCCESS) || (u2IfaceIndex == (UINT2) u4IfIndex))
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return VXLAN_SUCCESS;
            }
#endif
            VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
                (INT4) u4IfIndex;
            VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber = u4VniId;

            pVxlanInReplicaEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                               &VxlanInReplicaEntry, NULL);

            while (NULL != pVxlanInReplicaEntry)
            {
                if ((pVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaNveIfIndex == (INT4) u4IfIndex) &&
                    (pVxlanInReplicaEntry->MibObject.
                     u4FsVxlanInReplicaVniNumber == u4VniId))
                {
                    /* For IPV4 transmission */
                    if (pVxlanInReplicaEntry->
                        MibObject.i4FsVxlanInReplicaVtepAddressType ==
                        VXLAN_IPV4_UNICAST)
                    {
                        /* Get the source VTEP address from INREPLICA databse */
                        MEMCPY (&u4TempAddr,
                                pVxlanInReplicaEntry->
                                MibObject.au1FsVxlanInReplicaVtepAddress,
                                VXLAN_IP4_ADDR_LEN);
                        u4SrcAddr = OSIX_HTONL (u4TempAddr);
                        pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                        if (pTmpBuf == NULL)
                        {
                            return VXLAN_FAILURE;
                        }
                        VxlanUtilAddHdr (pTmpBuf, u4VniId);

                        /* Get the Remote VTEP address from the INREPLICA database */
                        MEMCPY (&u4TempAddr,
                                &(pVxlanInReplicaEntry->MibObject.
                                  au1FsVxlanInReplicaRemoteVtepAddress),
                                VXLAN_IP4_ADDR_LEN);
                        u4DestAddr = OSIX_HTONL (u4TempAddr);
                        /* Give the packet to SLI socket layer to transmit the packet */
                        if (VxlanUdpTransmitVxlanPkt
                            (pTmpBuf, u4SrcAddr, u4DestAddr,
                             (UINT2) (u4PktSize + VXLAN_HDR_TOTAL_OFFSET),
                             0) == VXLAN_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
                            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                             "Failed in packet transmission\n"));
                            return VXLAN_FAILURE;
                        }
                        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                                         "IPV4 VXLAN packet is successfully"
                                         "given to UDP module\n"));
                    }
#ifdef IP6_WANTED
                    else if (pVxlanInReplicaEntry->
                             MibObject.i4FsVxlanInReplicaVtepAddressType ==
                             VXLAN_IPV6_UNICAST)
                    {
                        pTmpBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                        if (pTmpBuf == NULL)
                        {
                            return VXLAN_FAILURE;
                        }
                        VxlanUtilAddHdr (pTmpBuf, u4VniId);
                        /* Get the source VTEP address from NVE databse */
                        MEMCPY (&SrcAddr,
                                pVxlanInReplicaEntry->
                                MibObject.au1FsVxlanInReplicaVtepAddress,
                                sizeof (SrcAddr));
                        /* Get the Remote VTEP address from the NVE database */
                        MEMCPY (&DstAddr,
                                pVxlanInReplicaEntry->
                                MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
                                sizeof (DstAddr));
                        /* Give the packet to SLI socket layer
                         * to transmit the packet */
                        if (VxlanUdpv6TransmitVxlanPkt
                            (pTmpBuf, &SrcAddr, &DstAddr,
                             (UINT2) (u4PktSize + VXLAN_HDR_TOTAL_OFFSET),
                             0) == VXLAN_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
                            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                             "Failed in packet transmission\n"));
                            return VXLAN_FAILURE;
                        }
                        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                                         "IPV6 VXLAN packet is successfully"
                                         "given to UDP module\n"));
                    }
#endif
                }
                pVxlanInReplicaEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanInReplicaTable, pVxlanInReplicaEntry,
                                   NULL);
            }

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);

            return VXLAN_SUCCESS;
        }
        else
        {
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (u4VniId, ZeroDestVmAddr, u4IfIndex);
            if ((pVxlanNveEntry != NULL)
                && (pVxlanNveEntry->MibObject.
                    i4FsVxlanNveRemoteVtepAddressType == 0))
            {
                pVxlanNveEntry = NULL;
            }
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "INREPLICA database is not found for"
                     "VNI id - %d, Destination VM Mac address - %s, "
                     "NVE if index - %d\n", u4VniId, au1MacStr, u4IfIndex));
    /* draft-mahalingam-dutt-dcops-vxlan section 4.1 */
    /* If NVE database is available, it is unicast tranmission. Get the 
     * reomote vtep information from NVE database and give the packet to SLI module
     * */
    if (pVxlanNveEntry != NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is found for"
                         "VNI id - %d, Destination VM Mac address - %s, "
                         "NVE if index - %d\n", u4VniId, au1MacStr, u4IfIndex));

        /* Add the VXLAN header */
        /* draft-mahalingam-dutt-dcops-vxlan section 5 */
        VxlanUtilAddHdr (pBuf, u4VniId);

        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                         "VXLAN header successfully encapsulated\n"));

        /* Include the VXLAN packet size */
        u4PktSize = u4PktSize + VXLAN_HDR_TOTAL_OFFSET;

        /* For IPV4 transmission */
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType ==
            VXLAN_IPV4_UNICAST)
        {
            /* Get the source VTEP address from NVE databse */
            MEMCPY (&u4TempAddr,
                    pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
                    VXLAN_IP4_ADDR_LEN);
            u4SrcAddr = OSIX_HTONL (u4TempAddr);

            /* Get the Remote VTEP address from the NVE database */
            MEMCPY (&u4TempAddr,
                    pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                    VXLAN_IP4_ADDR_LEN);
            u4DestAddr = OSIX_HTONL (u4TempAddr);

            /* Give the packet to SLI socket layer to transmit the packet */
            if (VxlanUdpTransmitVxlanPkt (pBuf, u4SrcAddr, u4DestAddr,
                                          (UINT2) u4PktSize,
                                          0) == VXLAN_FAILURE)
            {
                VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                 "Failed in packet transmission\n"));
                return VXLAN_FAILURE;
            }
            VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV4 VXLAN packet is successfully"
                             "given to UDP module\n"));

        }
#ifdef IP6_WANTED
        /* For IPV6 transmission */
        else
        {
            /* Get the source VTEP address from NVE databse */
            MEMCPY (&SrcAddr,
                    pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
                    sizeof (SrcAddr));

            /* Get the Remote VTEP address from the NVE database */
            MEMCPY (&DstAddr,
                    pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                    sizeof (DstAddr));

            /* Give the packet to SLI socket layer to transmit the packet */
            if (VxlanUdpv6TransmitVxlanPkt (pBuf, &SrcAddr, &DstAddr,
                                            (UINT2) u4PktSize,
                                            0) == VXLAN_FAILURE)
            {
                VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                 "Failed in packet transmission\n"));
                return VXLAN_FAILURE;
            }
            VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV6 VXLAN packet is successfully"
                             "given to UDP module\n"));
        }
#endif
        VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);
#ifdef EVPN_VXLAN_WANTED
        /*Get the EVI id from the VNI Index */
        if ((EvpnUtilGetEviFromVni (u4VniId, &i4EviIndex) == VXLAN_SUCCESS) &&
            (pVxlanNveEntry->i4FsVxlanNveEvpnMacType == VXLAN_NVE_EVPN_MAC))
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, " EVI entry found for VNI id -"
                        "%d\n", u4VniId));
            EvpnUtilUpdateStats (u4VniId, i4EviIndex, EVPN_STAT_PKT_SENT);
        }
#endif
        return VXLAN_SUCCESS;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is not found for"
                     "VNI id - %d, Destination VM Mac address - %s, "
                     "NVE if index - %d\n", u4VniId, au1MacStr, u4IfIndex));

    /* draft-mahalingam-dutt-dcops-vxlan section 4.2 */
    /* If nve datbase is not available for the destination mac address, check
     * the multicast database. If multicast database is available,
     * it is mulitcast transmission. Get the multicast address from the 
     * database and give the packet to SLI module */

    pVxlanMCastEntry = VxlanUtilGetMCastDatabase (u4VniId, u4IfIndex);

    if (pVxlanMCastEntry != NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is found for"
                         "VNI id - %d, NVE if index - %d\n", u4VniId,
                         u4IfIndex));

        /* Add the VXLAN header */
        /* draft-mahalingam-dutt-dcops-vxlan section 5 */
        VxlanUtilAddHdr (pBuf, u4VniId);

        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                         "VXLAN header successfully encapsulated\n"));

        /* Include the VXLAN packet size */
        u4PktSize = u4PktSize + VXLAN_HDR_TOTAL_OFFSET;

        /* For IPV4 transmission */
        if (pVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType ==
            VXLAN_IPV4_UNICAST)
        {
            /* Get the source VTEP address from MCAST databse */
            MEMCPY (&u4TempAddr,
                    pVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
                    VXLAN_IP4_ADDR_LEN);
            u4SrcAddr = OSIX_HTONL (u4TempAddr);

            /* Get the remote VTEP address from MCAST databse */
            MEMCPY (&u4TempAddr,
                    pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                    VXLAN_IP4_ADDR_LEN);
            u4DestAddr = OSIX_HTONL (u4TempAddr);
            while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
            {
                if (NetIpv4GetPortFromIfIndex (u4Port, &u4PhyIfIndex) ==
                    NETIPV4_SUCCESS)
                {
#ifdef IGMP_WANTED
                    if (IgmpIsEnableOnInterface (u4PhyIfIndex) != TRUE)
                    {
                        u4PrevPort = u4Port;
                        continue;
                    }
#endif
                    pOutBuf = NULL;
                    pOutBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                    if (pOutBuf == NULL)
                    {
                        return VXLAN_FAILURE;
                    }

                    /* Give the packet to SLI socket layer to transmit the packet */
                    if (VxlanUdpTransmitVxlanPkt
                        (pOutBuf, u4SrcAddr, u4DestAddr, (UINT2) u4PktSize,
                         u4PhyIfIndex) == VXLAN_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
                        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                         "Failed in packet transmission\n"));
                        return VXLAN_FAILURE;
                    }
                }
                u4PrevPort = u4Port;
            }

        }
#ifdef IP6_WANTED
        else
        {
            /* Get the source VTEP address from MCAST databse */
            MEMCPY (&SrcAddr,
                    pVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
                    sizeof (SrcAddr));

            /* Get the remote VTEP address from MCAST databse */
            MEMCPY (&DstAddr,
                    pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                    sizeof (DstAddr));

            /* Give the packet to SLI socket layer to transmit the packet */
            while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
            {
                if ((NetIpv6GetIfInfo (u4Port, &Ip6Info) == NETIPV4_SUCCESS) &&
                    (Ip6Info.u4IpPort != 0))
                {
                    pOutBuf = NULL;
                    pOutBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                    if (pOutBuf == NULL)
                    {
                        return VXLAN_FAILURE;
                    }
                    /* Give the packet to SLI socket layer to transmit the packet */
                    if (VxlanUdpv6TransmitVxlanPkt
                        (pOutBuf, &SrcAddr, &DstAddr, (UINT2) u4PktSize,
                         u4Port) == VXLAN_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
                        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                         "Failed in packet transmission\n"));
                        return VXLAN_FAILURE;
                    }
                }
                u4PrevPort = u4Port;
            }
        }
#endif
        VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return VXLAN_SUCCESS;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "Multicast database is also not found for"
                     "VNI id - %d, NVE if index - %d\n", u4VniId, u4IfIndex));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanEncapAndFwdPkt: Exit\n"));
    UNUSED_PARAM (SrcVmAddr);
    return VXLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanProcessUdpMsg                                         */
/*                                                                           */
/* Description  : Process the UDP message received from the remote VTEP      */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*                u4AddrType  - Denotes the Address type (IPV4/IPV6)         */
/*                pPeerAddr   - Remote VTEP Address                          */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
VxlanProcessUdpMsg (tCRU_BUF_CHAIN_HEADER * pMsg, UINT4 u4AddrType,
                    tIpAddr * pPeerAddr)
{
    UINT4               u4Tmp = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2EthType = 0;
#ifdef EVPN_VXLAN_WANTED
    INT4                i4EviIndex = 0;
    UINT4               u4VniId = 0;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessUdpMsg: Entry\n"));

    /* Get the Ethernet type from the frame */
    u4Tmp = VXLAN_HDR_TOTAL_OFFSET + CFA_VLAN_TAG_OFFSET;

    if (CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *) &u2EthType, u4Tmp, 2) ==
        CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Getting Ethernet type from VXLAN packet failed\n"));
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return;
    }

    u2EthType = OSIX_NTOHS (u2EthType);

    if (u2EthType == CFA_VLAN_PROTOCOL_ID)
    {
        u4Tmp += CFA_VLAN_PROTOCOL_SIZE;
        /* Get the Vlan Id from the frame */
        if (CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *) &u2VlanId, u4Tmp, 2) ==
            CRU_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                             "Getting VLAN Id from VXLAN packet failed\n"));
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return;
        }

        u2VlanId = OSIX_NTOHS (u2VlanId);
        u2VlanId = 0x0fff & u2VlanId;
    }
    if (VxlanHandleVxLanMsg (pMsg, u4AddrType, pPeerAddr, &u2VlanId)
        == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN packet handling failed\n"));

        VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_DRPD);
#ifdef EVPN_VXLAN_WANTED
        /*Get the VNI id from the VLAN */
        if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniId) == VXLAN_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "VNI is mapped for the VLAN %d\n", u4VniId));
            /*Get the EVI id from the VNI Index */
            if (EvpnUtilGetEviFromVni (u4VniId, &i4EviIndex) == VXLAN_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC, " EVI entry found for VNI id -"
                            "%d\n", u4VniId));
                EvpnUtilUpdateStats (u4VniId, i4EviIndex, EVPN_STAT_PKT_DRPD);
            }
        }
#endif
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessUdpMsg: Exit\n"));

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanHandleVxLanMsg                                        */
/*                                                                           */
/* Description  : Process the VXLAN packet received from the remote VTEP     */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*                u4AddrType  - Denotes the Address type (IPV4/IPV6)         */
/*                pPeerAddr   - Remote VTEP Address                          */
/*                u2VlanId    - VLAN id                                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
VxlanHandleVxLanMsg (tCRU_BUF_CHAIN_HEADER * pMsg, UINT4 u4AddrType,
                     tIpAddr * pPeerAddr, UINT2 *u2VlanId)
{
    UINT4               u4VniNumber = 0;
    UINT4               u4TmpVniNumber = 0;
    UINT4               u4NveIndex = 0;
    UINT4               u4OrgNveIfIndex = 0;
#ifdef EVPN_VXLAN_WANTED
    INT4                i4EviIndex = 0;
    tMacAddr            DestVmAddr;
    BOOL1               bEcmpEntryExists = VXLAN_FALSE;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
    UINT2               u2VlanIdCount;
#endif
    BOOL1               bIsTagRequired = FALSE;
#ifdef NPAPI_WANTED
    tMacAddr            ZeroDestVmAddr;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    UINT4               u4Count = 0;
#endif
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanAddNveEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;

    tMacAddr            SrcVmAddr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    tVxlanHdr          *pVxlanHdr = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleVxLanMsg: Entry\n"));

    MEMSET (&SrcVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));
#ifdef EVPN_VXLAN_WANTED
    MEMSET (&DestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1VrfName, 0, EVPN_MAX_VRF_NAME_LEN);
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
#endif
#ifdef NPAPI_WANTED
    MEMSET (&ZeroDestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#endif

#ifdef EVPN_VXLAN_WANTED
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
#endif
    /* Get the VXLAN header */
    pVxlanHdr = (tVxlanHdr *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear ((pMsg), 0, VXLAN_HDR_TOTAL_OFFSET);

    /* Validate the VXLAN header */
    if (VxlanUtilValidateHdr (pVxlanHdr, &u4VniNumber) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN header validation failed\n"));
        return VXLAN_FAILURE;
    }

    /* Remove the VXLAN header */
    if (CRU_BUF_Move_ValidOffset (pMsg, VXLAN_HDR_TOTAL_OFFSET) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "VXLAN header decapsulation failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                     "VXLAN header successfully decapsulated\n"));

    /* Get the source mac address from the frame */
    if (CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *) (&SrcVmAddr),
                                   VXLAN_ETHERNET_ADDR_SIZE,
                                   VXLAN_ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting source mac address from"
                         "VXLAN packet failed\n"));
        return VXLAN_FAILURE;
    }
#ifdef EVPN_VXLAN_WANTED
    /* Get the Destination mac address from the frame */
    if (CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *) (&DestVmAddr), 0,
                                   VXLAN_ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting destination address from"
                         "L2 packet failed \n"));
        return VXLAN_FAILURE;
    }
    if (EvpnUtilGetVrfNameFromVrfEntry (u4VniNumber, au1VrfName)
        == VXLAN_SUCCESS)
    {
        VxlanUtilGetVlanFromVni (u4VniNumber, &u2VlanIdCount);
        if (VxlanHandleIRBMsg (pMsg, au1VrfName) == VXLAN_FAILURE)
        {
            /*VxlanUtilUpdateStats (u2VlanIdCount, VXLAN_STAT_PKT_DRPD);Already done in calling place */
            VrfUtilUpdateStats (u4VniNumber, au1VrfName, VRF_STAT_PKT_DRPD);
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Handling IRB message"
                             "is failed \n"));
            return VXLAN_FAILURE;
        }
        VxlanUtilUpdateStats (u2VlanIdCount, VXLAN_STAT_PKT_RCVD);
        VrfUtilUpdateStats (u4VniNumber, au1VrfName, VXLAN_STAT_PKT_RCVD);
        return VXLAN_SUCCESS;
    }
#endif
    if (*u2VlanId == 0)
    {
        /* If vlan Id is not coming in the packet, get all the vlan mapped with the
           vni and update the counter
         */

        VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
            u4VniNumber;

        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) & VxlanVniVlanMapEntry, NULL);
        if (pVxlanVniVlanMapEntry == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             " No vlan is mapped with the VNI\n"));
            return VXLAN_FAILURE;
        }

        while (pVxlanVniVlanMapEntry != NULL)
        {
            if (pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber
                == u4VniNumber)
            {
                VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                 " Mapping found for VNI id - %d,"
                                 "VLAN id - %d in VLAN VNI mapping table\n",
                                 u4VniNumber,
                                 pVxlanVniVlanMapEntry->MibObject.
                                 i4FsVxlanVniVlanMapVlanId));

                *u2VlanId =
                    (UINT2) pVxlanVniVlanMapEntry->MibObject.
                    i4FsVxlanVniVlanMapVlanId;
                bIsTagRequired = TRUE;

                break;
            }

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanVniVlanMapEntry, NULL);
        }
    }

    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (u4VniNumber, &u4NveIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniNumber, &u4NveIndex))
            == NULL)
        {

            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable (u4VniNumber, &u4NveIndex))
                == NULL)
            {
                /* If MCAST database is also not mapped, dropped the VXLAN packet */
                VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                 "Nve index is not mapped for the VNI - %d"
                                 "in NVE, In-Replica and Multicast database\n",
                                 u4VniNumber));
                return VXLAN_FAILURE;
            }
        }
        u4OrgNveIfIndex = u4NveIndex;
    }
    else
    {
        /* Check whether NVE entry is available for the source mac address 
         * If it is available, no need to add NVE entry for source mac address */
        pVxlanNveEntry =
            VxlanUtilGetNveDatabase (u4VniNumber, SrcVmAddr, u4NveIndex);
        u4OrgNveIfIndex = u4NveIndex;
#ifdef NPAPI_WANTED
        if (pVxlanNveEntry == NULL)
        {
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (u4VniNumber, ZeroDestVmAddr,
                                         u4NveIndex);

            if ((pVxlanNveEntry != NULL) &&
                (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex >
                 CFA_MAX_NVE_IF_INDEX))
            {
                u4NveIndex = pVxlanNveEntry->u4OrgNveIfIndex;
            }
            u4OrgNveIfIndex = u4NveIndex;
            for (u4Count = 0; u4Count < VXLAN_HW_REPLICA_LIST; u4Count++)
            {
                pVxlanNveEntry =
                    VxlanUtilGetNveDatabase (u4VniNumber, ZeroDestVmAddr,
                                             u4NveIndex);
                if (pVxlanNveEntry != NULL)
                {
                    if (MEMCMP
                        (&
                         (pVxlanNveEntry->MibObject.
                          au1FsVxlanNveRemoteVtepAddress), pPeerAddr,
                         VXLAN_IP4_ADDR_LEN) == 0)
                    {
                        return VXLAN_SUCCESS;
                    }
                    u4NveIndex = u4NveIndex + SYS_DEF_MAX_NVE_IFACES;
                }
                else
                {
                    break;
                }
            }
        }

#endif
    }
    /* if NVE entry is not available, need to add NVE entry for source mac address
     * for next transmission
     * */
    CliMacToStr (SrcVmAddr, au1MacStr);
    au1MacStr[17] = '\0';
#ifdef EVPN_VXLAN_WANTED
    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4VniNumber;
    MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
            SrcVmAddr, sizeof (tMacAddr));
    pVxlanEcmpNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       (tRBElem *) & VxlanEcmpNveEntry, NULL);
    if ((pVxlanEcmpNveEntry != NULL)
        && (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
            (INT4) u4NveIndex)
        && (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber ==
            u4VniNumber)
        &&
        (MEMCMP
         (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac, SrcVmAddr,
          sizeof (tMacAddr)) == 0))
    {
        bEcmpEntryExists = VXLAN_TRUE;
    }
#endif

    if ((pVxlanNveEntry == NULL)
#ifdef EVPN_VXLAN_WANTED
        && (bEcmpEntryExists == VXLAN_FALSE)
#endif
        )
    {

        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is not found for"
                         "VNI id - %d, Destination VM Mac address - %s, "
                         "NVE if index - %d\n", u4VniNumber, au1MacStr,
                         u4NveIndex));

        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                         "Entry has to be added in NVE database\n"));

        pVxlanAddNveEntry =
            (tVxlanFsVxlanNveEntry *)
            MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

        if (pVxlanAddNveEntry == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                             "Memory allocation failed for the"
                             "new NVE entry\n"));
            return VXLAN_FAILURE;
        }

        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation succeed for the"
                         "new NVE entry\n"));

        /* Fill the source VTEP address */
        VxlanInitializeMibFsVxlanNveTable (pVxlanAddNveEntry,
                                           (INT4) u4OrgNveIfIndex);
        /* Fill Evpn MAC Type */
        pVxlanAddNveEntry->i4FsVxlanNveEvpnMacType = VXLAN_NVE_DYNAMIC_MAC;

        pVxlanAddNveEntry->u4OrgNveIfIndex = u4OrgNveIfIndex;

        /* Fill the Vlan-Id */
        pVxlanAddNveEntry->i4VlanId = (INT4) *u2VlanId;
        /* Fill the NVE if index */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveIfIndex = (INT4) u4NveIndex;
        pVxlanAddNveEntry->u4OrgNveIfIndex = u4OrgNveIfIndex;
        /* Fill the VNI number */
        pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber = u4VniNumber;

        /* Fill the destination VM mac address */
        MEMCPY (&(pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac),
                (&SrcVmAddr), VXLAN_ETHERNET_ADDR_SIZE);
#ifdef NPAPI_WANTED
        pVxlanInReplicaEntry =
            VxlanUtilGetNveIndexFromInReplicaTable (u4VniNumber, &u4NveIndex);
        pVxlanMCastEntry =
            VxlanUtilGetNveIndexFromMcastTable (u4VniNumber, &u4NveIndex);
        if ((pVxlanInReplicaEntry != NULL) || (pVxlanMCastEntry != NULL))
        {
            MEMCPY (&(pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac),
                    (&ZeroDestVmAddr), VXLAN_ETHERNET_ADDR_SIZE);
            if (pVxlanInReplicaEntry != NULL)
            {
                pVxlanInReplicaEntry->bDummyNveZeroMAC = FALSE;
            }
            if (pVxlanMCastEntry != NULL)
            {
                pVxlanMCastEntry->bDummyNveZeroMAC = FALSE;
            }
        }
#endif

        /* Fill the Remote VTEP IP addres type */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
            = (INT4) u4AddrType;

        /* Fill the Remote VTEP IP address */
        if (u4AddrType == VXLAN_IPV4_UNICAST)
        {
            pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen
                = VXLAN_IP4_ADDR_LEN;
            MEMCPY (&
                    (pVxlanAddNveEntry->MibObject.
                     au1FsVxlanNveRemoteVtepAddress), pPeerAddr,
                    VXLAN_IP4_ADDR_LEN);
        }
        else
        {
            pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen
                = VXLAN_IP6_ADDR_LEN;
            MEMCPY (&
                    (pVxlanAddNveEntry->MibObject.
                     au1FsVxlanNveRemoteVtepAddress), pPeerAddr,
                    VXLAN_IP6_ADDR_LEN);
        }

        /* Fill the storage type as volatile since the entry is learnt dynamically */
        pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType =
            VXLAN_STRG_TYPE_VOL;

        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanAddNveEntry) != RB_SUCCESS)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "RB Tree addition failed\n"));
            return VXLAN_FAILURE;
        }
        VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                  &(pVxlanAddNveEntry->MibObject.NveDbNode));
        VxlanRedSyncDynInfo ();

        if (VxlanHwUpdateNveDatabase (pVxlanAddNveEntry, VXLAN_HW_ADD) ==
            VXLAN_FAILURE)
        {
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       pVxlanAddNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanAddNveEntry);
            return VXLAN_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (pVxlanMCastEntry != NULL)
        {
            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                return VXLAN_FAILURE;
            }
            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_ADD) ==
                VXLAN_FAILURE)
            {
                return VXLAN_FAILURE;
            }
        }
#endif
    }
    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is found for"
                     "VNI id - %d, Destination VM Mac address - %s, "
                     "NVE if index - %d\n", u4VniNumber, au1MacStr,
                     u4NveIndex));

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                     " No Need to the entry in NVE database\n "));

    /*Get the VNI id for the VLAN */
    if (VxlanUtilGetVniFromVlan (*u2VlanId, &u4TmpVniNumber) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         "There is no VNI mapping for VLAN - %d\n", *u2VlanId));
        return VXLAN_FAILURE;
    }

    /* draft-mahalingam-dutt-dcops-vxlan section 6 */
    /* if the received vni number and vni number mapped with the vlan id are 
     * not equal, drop the packet
     * */
    if (u4VniNumber != u4TmpVniNumber)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         " VNI id - %d getting from the packet"
                         "and VNI id - %d mapped with the VLAN are not equal\n",
                         u4VniNumber, u4TmpVniNumber));

        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " Mapping found for VNI id - %d,"
                     "VLAN id - %d in VLAN VNI mapping table\n",
                     u4VniNumber, *u2VlanId));
#ifdef EVPN_VXLAN_WANTED
    /*Get the EVI id from the VNI Index */
    if (EvpnUtilGetEviFromVni (u4VniNumber, &i4EviIndex) == VXLAN_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, " EVI entry found for VNI id -"
                    "%d\n", u4VniNumber));
        VxlanVniVlanMapEntry.MibObject.
            i4FsVxlanVniVlanMapVlanId = (INT4) *u2VlanId;

        pVxlanVniVlanMapEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                       (tRBElem *) & VxlanVniVlanMapEntry);
        if (pVxlanVniVlanMapEntry != NULL)
        {
            if (EvpnUtilMultihomedFwdPkt (u4VniNumber, i4EviIndex, pPeerAddr,
                                          *u2VlanId, DestVmAddr,
                                          pVxlanVniVlanMapEntry->MibObject.
                                          i4FsVxlanVniVlanDfElection) ==
                VXLAN_FAILURE)
            {
                return VXLAN_FAILURE;
            }
            if ((pVxlanNveEntry != NULL) &&
                (pVxlanNveEntry->i4FsVxlanNveEvpnMacType == VXLAN_NVE_EVPN_MAC))
            {
                EvpnUtilUpdateStats (u4VniNumber, i4EviIndex,
                                     EVPN_STAT_PKT_RCVD);
            }
        }
    }
#endif
    VxlanUtilUpdateStats (*u2VlanId, VXLAN_STAT_PKT_RCVD);
    if (bIsTagRequired == TRUE)
    {
        if (pVxlanVniVlanMapEntry != NULL)
        {
            VlanAddOuterTagInFrame (pMsg,
                                    (tVlanId) pVxlanVniVlanMapEntry->MibObject.
                                    i4FsVxlanVniVlanMapVlanId, 0, u4NveIndex);
        }
    }
#ifdef CFA_WANTED
    /* Give the packet to CFA module for further transmission */
    CfaHandlePktFromVxlan (pMsg, u4NveIndex, L2PKT_VXLAN_TO_CFA, 0);
#endif

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                     "decapsulted packet given to CFA module\n"));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanHandleVxLanMsg: Exit\n"));

    return VXLAN_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanEncapAndFwdL3Pkt                                      */
/*                                                                           */
/* Description  : Adds the VXLAN header to the given L3 packet with L3VNI    */
/*                and sends on the destination port                          */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*                u4VrfId  - VrfId on which the packet has received         */
/*                u4L3VnId - L3VNI  associated with VRF                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VXLAN_SUCCESS or VXLAN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
VxlanEncapAndFwdL3Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VrfId,
                       UINT4 u4L3VnId)
{
    tMacAddr            DestVmAddr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4NveIfIndex = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DestAddr = 0;
    UINT2               u2VlanId = 0;
    UINT1               au1VrfName[VCMALIAS_MAX_ARRAY_LEN];
#ifdef IP6_WANTED
    tIp6Addr            SrcAddr;
    tIp6Addr            DstAddr;
#endif
/*UNUSED_PARAM (u4VrfId);*/
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanEncapAndFwdL3Pkt: Entry\n"));

    MEMSET (&DestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (&au1MacStr, 0, VXLAN_MAC_STR_LEN);
#ifdef IP6_WANTED
    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));
#endif

    /* Get the Destination mac address from the frame */
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) (&DestVmAddr), 0,
                                   VXLAN_ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting destination address from"
                         "L3 packet failed \n"));
        return VXLAN_FAILURE;
    }

    if ((VxlanUtilGetNveIndexFromNveTable (u4L3VnId, &u4NveIfIndex)) == NULL)
    {
        return VXLAN_SUCCESS;
    }

    CliMacToStr (DestVmAddr, au1MacStr);
    au1MacStr[17] = '\0';
    /* Get the NVE database using VNI id, Destination VM mac address
     * and NVE if index
     * */
    pVxlanNveEntry =
        VxlanUtilGetNveDatabase (u4L3VnId, DestVmAddr, u4NveIfIndex);

    if (pVxlanNveEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is not found for"
                         "L3VNI id - %d, Destination VM Mac address - %s, "
                         "NVE if index - %d\n", u4L3VnId, au1MacStr,
                         u4NveIfIndex));
        return VXLAN_SUCCESS;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "NVE database is found for"
                     "L3VNI id - %d, Destination VM Mac address - %s, "
                     "NVE if index - %d\n", u4L3VnId, au1MacStr, u4NveIfIndex));

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Add the VXLAN header */
    /* draft-mahalingam-dutt-dcops-vxlan section 5 */
    VxlanUtilAddHdr (pBuf, u4L3VnId);

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                     "VXLAN header successfully encapsulated\n"));

    /* Include the VXLAN packet size */
    u4PktSize = u4PktSize + VXLAN_HDR_TOTAL_OFFSET;

    /* For IPV4 transmission */
    if (pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType ==
        VXLAN_IPV4_UNICAST)
    {
        /* Get the source VTEP address from NVE databse */
        MEMCPY (&u4SrcAddr,
                pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
                VXLAN_IP4_ADDR_LEN);
        u4SrcAddr = OSIX_HTONL (u4SrcAddr);

        /* Get the Remote VTEP address from the NVE database */
        MEMCPY (&u4DestAddr,
                pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                VXLAN_IP4_ADDR_LEN);
        u4DestAddr = OSIX_HTONL (u4DestAddr);

        /* Give the packet to SLI socket layer to transmit the packet */
        if (VxlanUdpTransmitVxlanPkt (pBuf, u4SrcAddr, u4DestAddr,
                                      (UINT2) u4PktSize, 0) == VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "Failed in packet transmission\n"));
            return VXLAN_FAILURE;
        }
        VxlanUtilGetVlanFromVni (u4L3VnId, &u2VlanId);
        VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);
        VcmGetAliasName (u4VrfId, au1VrfName);
        VrfUtilUpdateStats (u4L3VnId, au1VrfName, VRF_STAT_PKT_SENT);
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV4 VXLAN packet is successfully"
                         "given to UDP module\n"));
    }
#ifdef IP6_WANTED
    /* For IPV6 transmission */
    else
    {
        /* Get the source VTEP address from NVE databse */
        MEMCPY (&SrcAddr,
                pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
                sizeof (SrcAddr));

        /* Get the Remote VTEP address from the NVE database */
        MEMCPY (&DstAddr,
                pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                sizeof (DstAddr));

        /* Give the packet to SLI socket layer to transmit the packet */
        if (VxlanUdpv6TransmitVxlanPkt (pBuf, &SrcAddr, &DstAddr,
                                        (UINT2) u4PktSize, 0) == VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "Failed in packet transmission\n"));
            return VXLAN_FAILURE;
        }
        VxlanUtilGetVlanFromVni (u4L3VnId, &u2VlanId);
        VxlanUtilUpdateStats (u2VlanId, VXLAN_STAT_PKT_SENT);
        VcmGetAliasName (u4VrfId, au1VrfName);
        VrfUtilUpdateStats (u4L3VnId, au1VrfName, VRF_STAT_PKT_SENT);
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV6 VXLAN packet is successfully"
                         "given to UDP module\n"));
    }
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanEncapAndFwdL3Pkt: Exit\n"));

    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanHandleIRBMsg                                          */
/*                                                                           */
/* Description  : Process the IRB packet received from the remote VTEP       */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*                u4AddrType  - Denotes the Address type (IPV4/IPV6)         */
/*                pPeerAddr   - Remote VTEP Address                          */
/*                u2VlanId    - VLAN id                                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
VxlanHandleIRBMsg (tCRU_BUF_CHAIN_HEADER * pMsg, UINT1 *pu1VrfName)
{
    UINT4               u4VrfId = 0;
    t_IP_HEADER         IpHdr;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4IfIndex = 0;
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT1               au1IpAddress[16];

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleVxLanMsg: Entry\n"));

    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (au1IpAddress, 0, 16);

    if (VcmIsVrfExist (pu1VrfName, &u4VrfId) == VCM_FALSE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VRF is not exist in VCM for VRF"
                         " Name - %s ", au1VrfName));
        return VXLAN_FAILURE;
    }

    /* Remove the Ethernet header 14 Bytes */
    if (CRU_BUF_Move_ValidOffset (pMsg, VXLAN_ETHERNET_HEADER_LEN) ==
        CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Ethernet header decapsulation failed\n"));
        return VXLAN_FAILURE;
    }

    CfaExtractIpHdr (&IpHdr, pMsg);
    RtQuery.u4DestinationIpAddress = IpHdr.u4Dest;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFC;
    RtQuery.u4ContextId = u4VrfId;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        VXLAN_TRC_FUNC
            ((VXLAN_FAIL_TRC, "%s: IP route not found for the destination",
              ": %d\n", __FUNCTION__, IpHdr.u4Dest));
        return VXLAN_FAILURE;
    }

    if ((NetIpRtInfo.u4NextHop == 0) && (NetIpRtInfo.u4RtIfIndx != 0))
    {
        NetIpRtInfo.u4NextHop = IpHdr.u4Dest;
    }

    /* Resolve ARP for the next hop */
    if (CfaIpIfGetIfIndexForNetInCxt (u4VrfId, NetIpRtInfo.u4NextHop,
                                      &u4IfIndex) == CFA_FAILURE)
    {
        VXLAN_TRC_FUNC
            ((VXLAN_FAIL_TRC, "%s: ARP is not resolved for the next hop",
              ": %d\n", __FUNCTION__, NetIpRtInfo.u4NextHop));
        return VXLAN_FAILURE;
    }
#ifdef CFA_WANTED
    /* Give the packet to CFA module for further transmission */

    CfaHandlePktFromVxlan (pMsg, u4IfIndex, L3PKT_VXLAN_TO_CFA, CFA_LINK_UCAST);
#endif

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC,
                     "decapsulted packet given to CFA module\n"));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanHandleVxLanMsg: Exit\n"));

    return VXLAN_SUCCESS;
}
#endif
