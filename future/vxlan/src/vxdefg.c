/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdefg.c,v 1.8 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Vxlan 
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanVtepTable
* Input       : pVxlanFsVxlanVtepEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanVtepTable (tVxlanFsVxlanVtepEntry *
                                    pVxlanFsVxlanVtepEntry)
{
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType =
        VXLAN_CLI_ADDRTYPE_IPV4;
    pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen =
        VXLAN_CLI_IP4_ADDR_LEN;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanNveTable
* Input       : pVxlanFsVxlanNveEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanNveTable (tVxlanFsVxlanNveEntry *
                                   pVxlanFsVxlanNveEntry, INT4 i4NveIfIndex)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;

    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp = EVPN_CLI_ARP_ALLOW;
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = i4NveIfIndex;

    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
        VXLAN_STRG_TYPE_NON_VOL;
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType =
        VXLAN_CLI_ADDRTYPE_IPV4;
    pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen =
        VXLAN_CLI_IP4_ADDR_LEN;
    pVxlanFsVxlanNveEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;
    pVxlanFsVxlanNveEntry->bIngOrMcastZeroEntryPresent = FALSE;
#ifdef EVPN_VXLAN_WANTED
    MEMSET (pVxlanFsVxlanNveEntry->MibObject.au1FsEvpnVxlanVrfName, 0,
            EVPN_MAX_VRF_NAME_LEN);
    pVxlanFsVxlanNveEntry->b1IsIrbRoute = FALSE;
#endif
    if (pVxlanVtepEntry != NULL)
    {
        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

        pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        MEMCPY (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress,
                pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanMCastTable
* Input       : pVxlanFsVxlanMCastEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanMCastTable (tVxlanFsVxlanMCastEntry *
                                     pVxlanFsVxlanMCastEntry, INT4 i4NveIfIndex)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;

    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = i4NveIfIndex;

    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType =
        VXLAN_CLI_ADDRTYPE_IPV4;
    pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen =
        VXLAN_CLI_IP4_ADDR_LEN;

    if (pVxlanVtepEntry != NULL)
    {
        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressType =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

        pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastVtepAddressLen =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        MEMCPY (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastVtepAddress,
                pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanVniVlanMapTable
* Input       : pVxlanFsVxlanVniVlanMapEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                          pVxlanFsVxlanVniVlanMapEntry,
                                          INT4 i4VlanId)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4VrfId = 0;
    UINT1               au1VxlanVrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    pVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus = CFA_IF_DOWN;
    MEMSET (au1VxlanVrfName, 0, VXLAN_CLI_MAX_VRF_NAME_LEN);
    u4IfIndex = CfaGetVlanInterfaceIndex ((UINT2) i4VlanId);

    if (u4IfIndex == CFA_INVALID_INDEX)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                         "Invalid L3 VLAN interface Index for the VLAN %d\n",
                         i4VlanId));
        return OSIX_SUCCESS;
    }
    if (VcmGetIfMapVcId (u4IfIndex, &u4VrfId) == OSIX_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                         "Unable to get vlan inerface Map Entry\n"));
        return OSIX_SUCCESS;
    }
    VcmGetAliasName (u4VrfId, au1VxlanVrfName);
    MEMCPY (pVxlanFsVxlanVniVlanMapEntry->MibObject.au1FsVxlanVniVlanVrfName,
            au1VxlanVrfName, STRLEN (au1VxlanVrfName));
    pVxlanFsVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanVrfNameLen =
        (INT4) (STRLEN (au1VxlanVrfName));

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanInReplicaTable
* Input       : pVxlanFsVxlanInReplicaEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                                         pVxlanFsVxlanInReplicaEntry,
                                         INT4 i4NveIfIndex)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;

    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = i4NveIfIndex;

    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressType =
        VXLAN_CLI_ADDRTYPE_IPV4;
    pVxlanFsVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen =
        VXLAN_CLI_IP4_ADDR_LEN;

    pVxlanFsVxlanInReplicaEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;
    if (pVxlanVtepEntry != NULL)
    {
        pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaVtepAddressType =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

        pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaVtepAddressLen =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        MEMCPY (pVxlanFsVxlanInReplicaEntry->MibObject.
                au1FsVxlanInReplicaVtepAddress,
                pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
* Function    : VxlanInitializeMibFsEvpnVxlanEviVniMapTable
* Input       : pVxlanFsEvpnVxlanEviVniMapEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                             pVxlanFsEvpnVxlanEviVniMapEntry)
{
    EVPN_P_BGP_SENT_PKTS (pVxlanFsEvpnVxlanEviVniMapEntry) = 0;
    EVPN_P_BGP_RCVD_PKTS (pVxlanFsEvpnVxlanEviVniMapEntry) = 0;
    EVPN_P_BGP_DRP_PKTS (pVxlanFsEvpnVxlanEviVniMapEntry) = 0;
    EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) = 0;
    EVPN_P_BGP_ESI_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) = 0;
    EVPN_P_BGP_RD_AUTO (pVxlanFsEvpnVxlanEviVniMapEntry) = 2;
    EVPN_P_BGP_LOAD_BALANCE (pVxlanFsEvpnVxlanEviVniMapEntry) = 2;
    MEMSET (EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry), 0,
            EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry));
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsEvpnVxlanBgpRTTable
* Input       : pVxlanFsEvpnVxlanBgpRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                         pVxlanFsEvpnVxlanBgpRTEntry)
{
    EVPN_P_BGP_RT_INDEX (pVxlanFsEvpnVxlanBgpRTEntry) = 0;
    EVPN_P_BGP_RT_LEN (pVxlanFsEvpnVxlanBgpRTEntry) = 0;
    EVPN_P_BGP_RT_TYPE (pVxlanFsEvpnVxlanBgpRTEntry) = 0;
    EVPN_P_BGP_RT_AUTO (pVxlanFsEvpnVxlanBgpRTEntry) = 2;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsEvpnVxlanVrfTable
* Input       : pVxlanFsEvpnVxlanVrfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                       pVxlanFsEvpnVxlanVrfEntry)
{
    UNUSED_PARAM (pVxlanFsEvpnVxlanVrfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsEvpnVxlanVrfRTTable
* Input       : pVxlanFsEvpnVxlanVrfRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                         pVxlanFsEvpnVxlanVrfRTEntry)
{
    UNUSED_PARAM (pVxlanFsEvpnVxlanVrfRTEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsEvpnVxlanMultihomedPeerTable
* Input       : pVxlanFsEvpnVxlanMultihomedPeerTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    VxlanInitializeMibFsEvpnVxlanMultihomedPeerTable
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanFsEvpnVxlanMultihomedPeerTable)
{
    UNUSED_PARAM (pVxlanFsEvpnVxlanMultihomedPeerTable);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeMibFsVxlanEcmpNveTable
* Input       : pVxlanFsVxlanEcmpNveEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
VxlanInitializeMibFsVxlanEcmpNveTable (tVxlanFsVxlanEcmpNveEntry *
                                       pVxlanFsVxlanEcmpNveEntry,
                                       INT4 i4NveIfIndex)
{
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpSuppressArp =
        EVPN_CLI_ARP_ALLOW;
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = i4NveIfIndex;

    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveStorageType =
        VXLAN_STRG_TYPE_VOL;
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType =
        VXLAN_CLI_ADDRTYPE_IPV4;
    pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen =
        VXLAN_CLI_IP4_ADDR_LEN;
    pVxlanFsVxlanEcmpNveEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;

    if (pVxlanVtepEntry != NULL)
    {
        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressType =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType;

        pVxlanFsVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveVtepAddressLen =
            pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        MEMCPY (pVxlanFsVxlanEcmpNveEntry->MibObject.
                au1FsVxlanEcmpNveVtepAddress,
                pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

#endif /* EVPN_VXLAN_WANTED */
