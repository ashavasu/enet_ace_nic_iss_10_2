/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxtrc.c,v 1.1.1.1 2014/08/14 11:22:36 siva Exp $
*
*********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "vxinc.h"

/****************************************************************************
*                                                                           *
* Function     : VxlanTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
VxlanTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("VXLAN: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : VxlanTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
VxlanTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : VxlanTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
VxlanTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define VXLAN_TRC_BUF_SIZE    2000
    static CHR1         buf[VXLAN_TRC_BUF_SIZE];

    MEMSET(&ap, 0, sizeof(va_list));
    MEMSET(buf, 0, VXLAN_TRC_BUF_SIZE);
    if (!(u4Flags & VXLAN_TRC_FLAG))
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlantrc.c                      */
/*-----------------------------------------------------------------------*/
