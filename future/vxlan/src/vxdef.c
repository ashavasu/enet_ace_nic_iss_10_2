/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdef.c,v 1.4 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Vxlan 
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
* Function    : VxlanInitializeFsVxlanVtepTable
* Input       : pVxlanFsVxlanVtepEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsVxlanVtepTable (tVxlanFsVxlanVtepEntry *
                                 pVxlanFsVxlanVtepEntry)
{
    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsVxlanVtepTable (pVxlanFsVxlanVtepEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsVxlanNveTable
* Input       : pVxlanFsVxlanNveEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsVxlanNveTable (tVxlanFsVxlanNveEntry * pVxlanFsVxlanNveEntry,
                                INT4 i4NveIfIndex)
{
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsVxlanNveTable
         (pVxlanFsVxlanNveEntry, i4NveIfIndex)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsVxlanMCastTable
* Input       : pVxlanFsVxlanMCastEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsVxlanMCastTable (tVxlanFsVxlanMCastEntry *
                                  pVxlanFsVxlanMCastEntry, INT4 i4NveIfIndex)
{
    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsVxlanMCastTable
         (pVxlanFsVxlanMCastEntry, i4NveIfIndex)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsVxlanVniVlanMapTable
* Input       : pVxlanFsVxlanVniVlanMapEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                       pVxlanFsVxlanVniVlanMapEntry,
                                       INT4 i4VlanId)
{
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsVxlanVniVlanMapTable
         (pVxlanFsVxlanVniVlanMapEntry, i4VlanId)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsVxlanInReplicaTable
* Input       : pVxlanFsVxlanInReplicaEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                                      pVxlanFsVxlanInReplicaEntry,
                                      INT4 i4NveIfIndex)
{
    if (pVxlanFsVxlanInReplicaEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsVxlanInReplicaTable (pVxlanFsVxlanInReplicaEntry,
                                                  i4NveIfIndex)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
* Function    : VxlanInitializeFsEvpnVxlanEviVniMapTable
* Input       : pVxlanFsEvpnVxlanEviVniMapEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                          pVxlanFsEvpnVxlanEviVniMapEntry)
{
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsEvpnVxlanEviVniMapTable
         (pVxlanFsEvpnVxlanEviVniMapEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsEvpnVxlanBgpRTTable
* Input       : pVxlanFsEvpnVxlanBgpRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                      pVxlanFsEvpnVxlanBgpRTEntry)
{
    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsEvpnVxlanBgpRTTable (pVxlanFsEvpnVxlanBgpRTEntry))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsEvpnVxlanVrfTable
* Input       : pVxlanFsEvpnVxlanVrfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                    pVxlanFsEvpnVxlanVrfEntry)
{
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsEvpnVxlanVrfRTTable
* Input       : pVxlanFsEvpnVxlanVrfRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
VxlanInitializeFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                      pVxlanFsEvpnVxlanVrfRTEntry)
{
    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsEvpnVxlanVrfRTTable (pVxlanFsEvpnVxlanVrfRTEntry))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : VxlanInitializeFsEvpnVxlanMultihomedPeerTable
* Input       : pVxlanFsEvpnVxlanMultihomedPeerTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4 
     
     
     
     
     
     
     
    VxlanInitializeFsEvpnVxlanMultihomedPeerTable
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanFsEvpnVxlanMultihomedPeerTable)
{
    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((VxlanInitializeMibFsEvpnVxlanMultihomedPeerTable
         (pVxlanFsEvpnVxlanMultihomedPeerTable)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif /* EVPN_VXLAN_WANTED */
