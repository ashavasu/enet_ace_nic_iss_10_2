/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxdsg.c,v 1.14 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Vxlan 
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
 Function    :  VxlanGetFsVxlanEnable
 Input       :  The Indices
 Input       :  pi4FsVxlanEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsVxlanEnable (INT4 *pi4FsVxlanEnable)
{
    *pi4FsVxlanEnable = gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsVxlanEnable
 Input       :  i4FsVxlanEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanSetFsVxlanEnable (INT4 i4FsVxlanEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != i4FsVxlanEnable)
    {

        if (i4FsVxlanEnable == VXLAN_CLI_STATUS_DISABLE)
        {
            VxlanDeleteRBTreeEntries ();
#ifdef EVPN_VXLAN_WANTED
            EvpnDeleteRBTreeEntries ();
#endif
            if (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort !=
                VXLAN_UDP_DEST_PORT)
            {
                VxlanUdpDeInit ();
                gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort =
                    VXLAN_UDP_DEST_PORT;
                VxlanUdpInit ();
            }
            if (VxlanHwInit (VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                return OSIX_FAILURE;
            }
            gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption = VXLAN_FALSE;
            /* Delete all entries in all RBTrees */
        }
        else
        {
            if (VxlanHwInit (VXLAN_HW_ADD) == VXLAN_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (VxlanHwUpdateUdpPort
                (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort,
                 VXLAN_HW_ADD) == VXLAN_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable = i4FsVxlanEnable;
        nmhSetCmnNew (FsVxlanEnable, VXLAN_SCALAR_MIB_OID_LENGTH,
                      VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                      gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable);
    }
    else
    {
        if (i4FsVxlanEnable == VXLAN_ENABLED)
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "NV Overlay Feature is already enabled\n"));
        }
        else
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "NV Overlay Feature is already disabled\n"));
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanUdpPort
 Input       :  The Indices
 Input       :  pu4FsVxlanUdpPort
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsVxlanUdpPort (UINT4 *pu4FsVxlanUdpPort)
{
    *pu4FsVxlanUdpPort = gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsVxlanUdpPort
 Input       :  u4FsVxlanUdpPort
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanSetFsVxlanUdpPort (UINT4 u4FsVxlanUdpPort)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT4               u4IfIndex = 0;

    if (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort != u4FsVxlanUdpPort)
    {
        VxlanUdpDeInit ();
        if (VxlanHwUpdateUdpPort
            (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort,
             VXLAN_HW_DEL) == VXLAN_FAILURE)
        {
            return OSIX_FAILURE;
        }
        gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort = u4FsVxlanUdpPort;

        if (VxlanHwUpdateUdpPort
            (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort,
             VXLAN_HW_ADD) == VXLAN_FAILURE)
        {
            return OSIX_FAILURE;
        }
        VxlanUdpInit ();

        for (u4IfIndex = CFA_MIN_NVE_IF_INDEX;
             u4IfIndex <= CFA_MAX_NVE_IF_INDEX; u4IfIndex++)
        {
            VxlanHandleNveIntAdminStatus (u4IfIndex, CFA_IF_DOWN);
            VxlanHandleNveIntAdminStatus (u4IfIndex, CFA_IF_UP);
        }

        nmhSetCmnNew (FsVxlanUdpPort, VXLAN_SCALAR_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock,
                      0, 0, 0, i4SetOption, "%u",
                      gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort);
    }
    else
    {
        VXLAN_TRC ((VXLAN_CLI_TRC, "Given UDP port value is already Set\n"));
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanTraceOption
 Input       :  The Indices
 Input       :  pi4FsVxlanTraceOption
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsVxlanTraceOption (UINT4 *pu4FsVxlanTraceOption)
{
    *pu4FsVxlanTraceOption = gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsVxlanTraceOption
 Input       :  i4FsVxlanTraceOption
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanSetFsVxlanTraceOption (UINT4 u4FsVxlanTraceOption, INT1 u1Set)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    if (u1Set == CLI_ENABLE)
    {
        if ((gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption &
             u4FsVxlanTraceOption) != u4FsVxlanTraceOption)
        {
            gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption |=
                u4FsVxlanTraceOption;
            nmhSetCmnNew (FsVxlanTraceOption, VXLAN_SCALAR_MIB_OID_LENGTH,
                          VxlanMainTaskLock,
                          VxlanMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                          gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption);

        }
        else
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Given debug flags are already set. So, no need to enable these flags\n"));
        }
    }
    else
    {
        if (gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption &
            u4FsVxlanTraceOption)
        {
            gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption &=
                (~u4FsVxlanTraceOption);
            nmhSetCmnNew (FsVxlanTraceOption, VXLAN_SCALAR_MIB_OID_LENGTH,
                          VxlanMainTaskLock,
                          VxlanMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                          gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption);
        }
        else
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "Given debug flags are not set. So, no need to disable the flags\n"));
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanNotificationCntl
 Input       :  The Indices
 Input       :  pi4FsVxlanNotificationCntl
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsVxlanNotificationCntl (INT4 *pi4FsVxlanNotificationCntl)
{
    *pi4FsVxlanNotificationCntl =
        gVxlanGlobals.VxlanGlbMib.i4FsVxlanNotificationCntl;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsVxlanNotificationCntl
 Input       :  i4FsVxlanNotificationCntl
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanSetFsVxlanNotificationCntl (INT4 i4FsVxlanNotificationCntl)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gVxlanGlobals.VxlanGlbMib.i4FsVxlanNotificationCntl =
        i4FsVxlanNotificationCntl;

    nmhSetCmnNew (FsVxlanNotificationCntl, VXLAN_SCALAR_MIB_OID_LENGTH,
                  VxlanMainTaskLock,
                  VxlanMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gVxlanGlobals.VxlanGlbMib.i4FsVxlanNotificationCntl);

    return OSIX_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanEnable
 Input       :  The Indices
 Input       :  pi4FsEvpnVxlanEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsEvpnVxlanEnable (INT4 *pi4FsEvpnVxlanEnable)
{
    *pi4FsEvpnVxlanEnable = gVxlanGlobals.VxlanGlbMib.i4FsEvpnVxlanEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnAnycastGwMac
 Input       :  The Indices
 Input       :  FsEvpnAnycastGwMac
 Descritpion :  This Routine will Get
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsEvpnAnycastGwMac (tMacAddr * pFsEvpnAnycastGwMac)
{

    MEMCPY (pFsEvpnAnycastGwMac, gVxlanGlobals.VxlanGlbMib.FsEvpnAnycastGwMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnAnycastGwMac
 Input       :  The Indices
 Input       :  FsEvpnAnycastGwMac
 Descritpion :  This Routine will Get
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetFsEvpnAnycastGwMac (tMacAddr * pFsEvpnAnycastGwMac)
{

    MEMCPY (pFsEvpnAnycastGwMac, gVxlanGlobals.VxlanGlbMib.FsEvpnAnycastGwMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsEvpnVxlanEnable
 Input       :  i4FsEvpnVxlanEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
VxlanSetFsEvpnVxlanEnable (INT4 i4FsEvpnEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gVxlanGlobals.VxlanGlbMib.i4FsEvpnVxlanEnable != i4FsEvpnEnable)
    {
        if (i4FsEvpnEnable == EVPN_CLI_STATUS_DISABLE)
        {
            EvpnDeleteRBTreeEntries ();
            gVxlanGlobals.VxlanGlbMib.u4FsVxlanTraceOption = VXLAN_FALSE;
            /* Delete all entries in all RBTrees */
        }
        gVxlanGlobals.VxlanGlbMib.i4FsEvpnVxlanEnable = i4FsEvpnEnable;

        nmhSetCmnNew (FsEvpnVxlanEnable, 12, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                      gVxlanGlobals.VxlanGlbMib.i4FsEvpnVxlanEnable);
    }
    else
    {
        if (i4FsEvpnEnable == EVPN_ENABLED)
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "EVPN Overlay Feature is already enabled\n"));
        }
        else
        {
            VXLAN_TRC ((VXLAN_CLI_TRC,
                        "EVPN Overlay Feature is already disabled\n"));
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetFsEvpnAnycastGwMac
 Input       :  The Indices
 Input       :  FsEvpnAnycastGwMac
 Descritpion :  This Routine will Get
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetFsEvpnAnycastGwMac (tMacAddr FsEvpnAnycastGwMac)
{
    MEMCPY (gVxlanGlobals.VxlanGlbMib.FsEvpnAnycastGwMac,
            FsEvpnAnycastGwMac, VXLAN_ETHERNET_ADDR_SIZE);
    return OSIX_SUCCESS;
}

#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanVtepTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanVtepEntry or NULL
****************************************************************************/
tVxlanFsVxlanVtepEntry *
VxlanGetFirstFsVxlanVtepTable ()
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                   FsVxlanVtepTable);

    return pVxlanFsVxlanVtepEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanVtepTable
 Input       :  pCurrentVxlanFsVxlanVtepEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanVtepEntry or NULL
****************************************************************************/
tVxlanFsVxlanVtepEntry *
VxlanGetNextFsVxlanVtepTable (tVxlanFsVxlanVtepEntry *
                              pCurrentVxlanFsVxlanVtepEntry)
{
    tVxlanFsVxlanVtepEntry *pNextVxlanFsVxlanVtepEntry = NULL;

    pNextVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanVtepTable,
                                                  (tRBElem *)
                                                  pCurrentVxlanFsVxlanVtepEntry,
                                                  NULL);

    return pNextVxlanFsVxlanVtepEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanVtepTable
 Input       :  pVxlanFsVxlanVtepEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanVtepEntry or NULL
****************************************************************************/
tVxlanFsVxlanVtepEntry *
VxlanGetFsVxlanVtepTable (tVxlanFsVxlanVtepEntry * pVxlanFsVxlanVtepEntry)
{
    tVxlanFsVxlanVtepEntry *pGetVxlanFsVxlanVtepEntry = NULL;

    pGetVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanVtepTable,
                                              (tRBElem *)
                                              pVxlanFsVxlanVtepEntry);

    return pGetVxlanFsVxlanVtepEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanNveTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanNveEntry *
VxlanGetFirstFsVxlanNveTable ()
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanNveTable);

    return pVxlanFsVxlanNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanNveTable
 Input       :  pCurrentVxlanFsVxlanNveEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanNveEntry *
VxlanGetNextFsVxlanNveTable (tVxlanFsVxlanNveEntry *
                             pCurrentVxlanFsVxlanNveEntry)
{
    tVxlanFsVxlanNveEntry *pNextVxlanFsVxlanNveEntry = NULL;

    pNextVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                 FsVxlanNveTable,
                                                 (tRBElem *)
                                                 pCurrentVxlanFsVxlanNveEntry,
                                                 NULL);

    return pNextVxlanFsVxlanNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanNveTable
 Input       :  pVxlanFsVxlanNveEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanNveEntry *
VxlanGetFsVxlanNveTable (tVxlanFsVxlanNveEntry * pVxlanFsVxlanNveEntry)
{
    tVxlanFsVxlanNveEntry *pGetVxlanFsVxlanNveEntry = NULL;

    pGetVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                             FsVxlanNveTable,
                                             (tRBElem *) pVxlanFsVxlanNveEntry);

    return pGetVxlanFsVxlanNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanMCastTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanMCastEntry or NULL
****************************************************************************/
tVxlanFsVxlanMCastEntry *
VxlanGetFirstFsVxlanMCastTable ()
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanMCastTable);

    return pVxlanFsVxlanMCastEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanMCastTable
 Input       :  pCurrentVxlanFsVxlanMCastEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanMCastEntry or NULL
****************************************************************************/
tVxlanFsVxlanMCastEntry *
VxlanGetNextFsVxlanMCastTable (tVxlanFsVxlanMCastEntry *
                               pCurrentVxlanFsVxlanMCastEntry)
{
    tVxlanFsVxlanMCastEntry *pNextVxlanFsVxlanMCastEntry = NULL;

    pNextVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                   FsVxlanMCastTable,
                                                   (tRBElem *)
                                                   pCurrentVxlanFsVxlanMCastEntry,
                                                   NULL);

    return pNextVxlanFsVxlanMCastEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanMCastTable
 Input       :  pVxlanFsVxlanMCastEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanMCastEntry or NULL
****************************************************************************/
tVxlanFsVxlanMCastEntry *
VxlanGetFsVxlanMCastTable (tVxlanFsVxlanMCastEntry * pVxlanFsVxlanMCastEntry)
{
    tVxlanFsVxlanMCastEntry *pGetVxlanFsVxlanMCastEntry = NULL;

    pGetVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                               FsVxlanMCastTable,
                                               (tRBElem *)
                                               pVxlanFsVxlanMCastEntry);

    return pGetVxlanFsVxlanMCastEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanVniVlanMapTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanVniVlanMapEntry or NULL
****************************************************************************/
tVxlanFsVxlanVniVlanMapEntry *
VxlanGetFirstFsVxlanVniVlanMapTable ()
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                         VxlanGlbMib.
                                                         FsVxlanVniVlanMapTable);

    return pVxlanFsVxlanVniVlanMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanVniVlanMapTable
 Input       :  pCurrentVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanVniVlanMapEntry or NULL
****************************************************************************/
tVxlanFsVxlanVniVlanMapEntry *
VxlanGetNextFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                    pCurrentVxlanFsVxlanVniVlanMapEntry)
{
    tVxlanFsVxlanVniVlanMapEntry *pNextVxlanFsVxlanVniVlanMapEntry = NULL;

    pNextVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsVxlanVniVlanMapTable,
                                                        (tRBElem *)
                                                        pCurrentVxlanFsVxlanVniVlanMapEntry,
                                                        NULL);

    return pNextVxlanFsVxlanVniVlanMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanVniVlanMapTable
 Input       :  pVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanVniVlanMapEntry or NULL
****************************************************************************/
tVxlanFsVxlanVniVlanMapEntry *
VxlanGetFsVxlanVniVlanMapTable (tVxlanFsVxlanVniVlanMapEntry *
                                pVxlanFsVxlanVniVlanMapEntry)
{
    tVxlanFsVxlanVniVlanMapEntry *pGetVxlanFsVxlanVniVlanMapEntry = NULL;

    pGetVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanVniVlanMapTable,
                                                    (tRBElem *)
                                                    pVxlanFsVxlanVniVlanMapEntry);

    return pGetVxlanFsVxlanVniVlanMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanInReplicaTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanInReplicaEntry or NULL
****************************************************************************/
tVxlanFsVxlanInReplicaEntry *
VxlanGetFirstFsVxlanInReplicaTable ()
{
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;

    pVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsVxlanInReplicaTable);

    return pVxlanFsVxlanInReplicaEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanInReplicaTable
 Input       :  pCurrentVxlanFsVxlanInReplicaEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanInReplicaEntry or NULL
****************************************************************************/
tVxlanFsVxlanInReplicaEntry *
VxlanGetNextFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                                   pCurrentVxlanFsVxlanInReplicaEntry)
{
    tVxlanFsVxlanInReplicaEntry *pNextVxlanFsVxlanInReplicaEntry = NULL;
    pNextVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *) RBTreeGetNext (gVxlanGlobals.
                                                       VxlanGlbMib.
                                                       FsVxlanInReplicaTable,
                                                       (tRBElem *)
                                                       pCurrentVxlanFsVxlanInReplicaEntry,
                                                       NULL);
    return pNextVxlanFsVxlanInReplicaEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanInReplicaTable
 Input       :  pVxlanFsVxlanInReplicaEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanInReplicaEntry or NULL
****************************************************************************/
tVxlanFsVxlanInReplicaEntry *
VxlanGetFsVxlanInReplicaTable (tVxlanFsVxlanInReplicaEntry *
                               pVxlanFsVxlanInReplicaEntry)
{
    tVxlanFsVxlanInReplicaEntry *pGetVxlanFsVxlanInReplicaEntry = NULL;

    pGetVxlanFsVxlanInReplicaEntry =
        (tVxlanFsVxlanInReplicaEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                   FsVxlanInReplicaTable,
                                                   (tRBElem *)
                                                   pVxlanFsVxlanInReplicaEntry);

    return pGetVxlanFsVxlanInReplicaEntry;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanEviVniMapTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanEviVniMapEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanEviVniMapEntry *
VxlanGetFirstFsEvpnVxlanEviVniMapTable ()
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable);

    return pVxlanFsEvpnVxlanEviVniMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanEviVniMapTable
 Input       :  pCurrentVxlanFsEvpnVxlanEviVniMapEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanEviVniMapEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanEviVniMapEntry *
VxlanGetNextFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                       pCurrentVxlanFsEvpnVxlanEviVniMapEntry)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pNextVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pNextVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       (tRBElem *) pCurrentVxlanFsEvpnVxlanEviVniMapEntry,
                       NULL);

    return pNextVxlanFsEvpnVxlanEviVniMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanEviVniMapTable
 Input       :  pVxlanFsEvpnVxlanEviVniMapEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsEvpnVxlanEviVniMapEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanEviVniMapEntry *
VxlanGetFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                   pVxlanFsEvpnVxlanEviVniMapEntry)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pGetVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    pGetVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) pVxlanFsEvpnVxlanEviVniMapEntry);

    return pGetVxlanFsEvpnVxlanEviVniMapEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanBgpRTTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanBgpRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanBgpRTEntry *
VxlanGetFirstFsEvpnVxlanBgpRTTable ()
{
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable);

    return pVxlanFsEvpnVxlanBgpRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanBgpRTTable
 Input       :  pCurrentVxlanFsEvpnVxlanBgpRTEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanBgpRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanBgpRTEntry *
VxlanGetNextFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                   pCurrentVxlanFsEvpnVxlanBgpRTEntry)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pNextVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pNextVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                       (tRBElem *) pCurrentVxlanFsEvpnVxlanBgpRTEntry, NULL);

    return pNextVxlanFsEvpnVxlanBgpRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanBgpRTTable
 Input       :  pVxlanFsEvpnVxlanBgpRTEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsEvpnVxlanBgpRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanBgpRTEntry *
VxlanGetFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                               pVxlanFsEvpnVxlanBgpRTEntry)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pGetVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pGetVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *) RBTreeGet (gVxlanGlobals.
                                                   VxlanGlbMib.
                                                   FsEvpnVxlanBgpRTTable,
                                                   (tRBElem *)
                                                   pVxlanFsEvpnVxlanBgpRTEntry);

    return pGetVxlanFsEvpnVxlanBgpRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanVrfTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanVrfEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfEntry *
VxlanGetFirstFsEvpnVxlanVrfTable ()
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                      FsEvpnVxlanVrfTable);

    return pVxlanFsEvpnVxlanVrfEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanVrfTable
 Input       :  pCurrentVxlanFsEvpnVxlanVrfEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanVrfEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfEntry *
VxlanGetNextFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                 pCurrentVxlanFsEvpnVxlanVrfEntry)
{
    tVxlanFsEvpnVxlanVrfEntry *pNextVxlanFsEvpnVxlanVrfEntry = NULL;

    pNextVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                     FsEvpnVxlanVrfTable,
                                                     (tRBElem *)
                                                     pCurrentVxlanFsEvpnVxlanVrfEntry,
                                                     NULL);

    return pNextVxlanFsEvpnVxlanVrfEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanVrfTable
 Input       :  pVxlanFsEvpnVxlanVrfEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsEvpnVxlanVrfEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfEntry *
VxlanGetFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                             pVxlanFsEvpnVxlanVrfEntry)
{
    tVxlanFsEvpnVxlanVrfEntry *pGetVxlanFsEvpnVxlanVrfEntry = NULL;

    pGetVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                 FsEvpnVxlanVrfTable,
                                                 (tRBElem *)
                                                 pVxlanFsEvpnVxlanVrfEntry);

    return pGetVxlanFsEvpnVxlanVrfEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanVrfRTTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanVrfRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
VxlanGetFirstFsEvpnVxlanVrfRTTable ()
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsEvpnVxlanVrfRTTable);

    return pVxlanFsEvpnVxlanVrfRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanVrfRTTable
 Input       :  pCurrentVxlanFsEvpnVxlanVrfRTEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanVrfRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
VxlanGetNextFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                   pCurrentVxlanFsEvpnVxlanVrfRTEntry)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pNextVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pNextVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetNext (gVxlanGlobals.
                                                       VxlanGlbMib.
                                                       FsEvpnVxlanVrfRTTable,
                                                       (tRBElem *)
                                                       pCurrentVxlanFsEvpnVxlanVrfRTEntry,
                                                       NULL);

    return pNextVxlanFsEvpnVxlanVrfRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanVrfRTTable
 Input       :  pVxlanFsEvpnVxlanVrfRTEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsEvpnVxlanVrfRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
VxlanGetFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                               pVxlanFsEvpnVxlanVrfRTEntry)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pGetVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pGetVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                   FsEvpnVxlanVrfRTTable,
                                                   (tRBElem *)
                                                   pVxlanFsEvpnVxlanVrfRTEntry);

    return pGetVxlanFsEvpnVxlanVrfRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanMultihomedPeerTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanMultihomedPeerTable or NULL
****************************************************************************/
tVxlanFsEvpnVxlanMultihomedPeerTable *
VxlanGetFirstFsEvpnVxlanMultihomedPeerTable ()
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst (gVxlanGlobals.
                                                                 VxlanGlbMib.
                                                                 FsEvpnVxlanMultihomedPeerTable);

    return pVxlanFsEvpnVxlanMultihomedPeerTable;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanMultihomedPeerTable
 Input       :  pCurrentVxlanFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanMultihomedPeerTable or NULL
****************************************************************************/
tVxlanFsEvpnVxlanMultihomedPeerTable *
VxlanGetNextFsEvpnVxlanMultihomedPeerTable (tVxlanFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pCurrentVxlanFsEvpnVxlanMultihomedPeerTable)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable
        * pNextVxlanFsEvpnVxlanMultihomedPeerTable = NULL;

    pNextVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetNext (gVxlanGlobals.
                                                                VxlanGlbMib.
                                                                FsEvpnVxlanMultihomedPeerTable,
                                                                (tRBElem *)
                                                                pCurrentVxlanFsEvpnVxlanMultihomedPeerTable,
                                                                NULL);

    return pNextVxlanFsEvpnVxlanMultihomedPeerTable;
}

/****************************************************************************
 Function    :  VxlanGetFsEvpnVxlanMultihomedPeerTable
 Input       :  pVxlanFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsEvpnVxlanMultihomedPeerTable or NULL
****************************************************************************/
tVxlanFsEvpnVxlanMultihomedPeerTable *
VxlanGetFsEvpnVxlanMultihomedPeerTable (tVxlanFsEvpnVxlanMultihomedPeerTable *
                                        pVxlanFsEvpnVxlanMultihomedPeerTable)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable
        * pGetVxlanFsEvpnVxlanMultihomedPeerTable = NULL;

    pGetVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGet (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsEvpnVxlanMultihomedPeerTable,
                                                            (tRBElem *)
                                                            pVxlanFsEvpnVxlanMultihomedPeerTable);

    return pGetVxlanFsEvpnVxlanMultihomedPeerTable;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsVxlanEcmpNveTable
 Input       :  NONE                                                                                                                                          Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsVxlanEcmpNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanEcmpNveEntry *
VxlanGetFirstFsVxlanEcmpNveTable ()
{
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;

    pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                      FsVxlanEcmpNveTable);

    return pVxlanFsVxlanEcmpNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsVxlanEcmpNveTable
 Input       :  pCurrentVxlanFsVxlanEcmpNveEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsVxlanEcmpNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanEcmpNveEntry *
VxlanGetNextFsVxlanEcmpNveTable (tVxlanFsVxlanEcmpNveEntry *
                                 pCurrentVxlanFsVxlanEcmpNveEntry)
{
    tVxlanFsVxlanEcmpNveEntry *pNextVxlanFsVxlanEcmpNveEntry = NULL;

    pNextVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                     FsVxlanEcmpNveTable,
                                                     (tRBElem *)
                                                     pCurrentVxlanFsVxlanEcmpNveEntry,
                                                     NULL);

    return pNextVxlanFsVxlanEcmpNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetFsVxlanEcmpNveTable
 Input       :  pVxlanFsVxlanEcmpNveEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetVxlanFsVxlanEcmpNveEntry or NULL
****************************************************************************/
tVxlanFsVxlanEcmpNveEntry *
VxlanGetFsVxlanEcmpNveTable (tVxlanFsVxlanEcmpNveEntry *
                             pVxlanFsVxlanEcmpNveEntry)
{
    tVxlanFsVxlanEcmpNveEntry *pGetVxlanFsVxlanEcmpNveEntry = NULL;

    pGetVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                 FsVxlanEcmpNveTable,
                                                 (tRBElem *)
                                                 pVxlanFsVxlanEcmpNveEntry);

    return pGetVxlanFsVxlanEcmpNveEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanArpTable
 Input       :  NONE                                                                                                                                          Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanArpEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanArpEntry *
VxlanGetFirstFsEvpnVxlanArpTable ()
{
    tVxlanFsEvpnVxlanArpEntry *pVxlanFsEvpnVxlanArpEntry = NULL;

    pVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                      FsEvpnVxlanArpTable);

    return pVxlanFsEvpnVxlanArpEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanArpTable
 Input       :  pCurrentVxlanFsEvpnVxlanArpEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanArpEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanArpEntry *
VxlanGetNextFsEvpnVxlanArpTable (tVxlanFsEvpnVxlanArpEntry *
                                 pCurrentVxlanFsEvpnVxlanArpEntry)
{
    tVxlanFsEvpnVxlanArpEntry *pNextVxlanFsEvpnVxlanArpEntry = NULL;

    pNextVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                     FsEvpnVxlanArpTable,
                                                     (tRBElem *)
                                                     pCurrentVxlanFsEvpnVxlanArpEntry,
                                                     NULL);

    return pNextVxlanFsEvpnVxlanArpEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanVrfRTTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanVrfRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
VxlanGetFirstFsVxlanVniVlanPortEntry ()
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsEvpnVxlanVrfRTTable);

    return pVxlanFsEvpnVxlanVrfRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanArpTable
 Input       :  pCurrentVxlanFsEvpnVxlanArpEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanArpEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanArpEntry *
VxlanGetNextFsVxlanVniVlanPortEntry (tVxlanFsEvpnVxlanArpEntry *
                                     pCurrentVxlanFsEvpnVxlanArpEntry)
{
    tVxlanFsEvpnVxlanArpEntry *pNextVxlanFsEvpnVxlanArpEntry = NULL;

    pNextVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                     FsEvpnVxlanArpTable,
                                                     (tRBElem *)
                                                     pCurrentVxlanFsEvpnVxlanArpEntry,
                                                     NULL);

    return pNextVxlanFsEvpnVxlanArpEntry;
}

/****************************************************************************
 Function    :  VxlanGetFirstFsEvpnVxlanVrfRTTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pVxlanFsEvpnVxlanVrfRTEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
VxlanGetFirstFsVxlanVniVlanPortMacEntry ()
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsEvpnVxlanVrfRTTable);

    return pVxlanFsEvpnVxlanVrfRTEntry;
}

/****************************************************************************
 Function    :  VxlanGetNextFsEvpnVxlanArpTable
 Input       :  pCurrentVxlanFsEvpnVxlanArpEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextVxlanFsEvpnVxlanArpEntry or NULL
****************************************************************************/
tVxlanFsEvpnVxlanArpEntry *
VxlanGetNextFsVxlanVniVlanPortMacEntry (tVxlanFsEvpnVxlanArpEntry *
                                        pCurrentVxlanFsEvpnVxlanArpEntry)
{
    tVxlanFsEvpnVxlanArpEntry *pNextVxlanFsEvpnVxlanArpEntry = NULL;

    pNextVxlanFsEvpnVxlanArpEntry =
        (tVxlanFsEvpnVxlanArpEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                     FsEvpnVxlanArpTable,
                                                     (tRBElem *)
                                                     pCurrentVxlanFsEvpnVxlanArpEntry,
                                                     NULL);

    return pNextVxlanFsEvpnVxlanArpEntry;
}

/****************************************************************************
 Function    :  VxlanSetFsEvpnAnycastGwMac
 Input       :  The Indices
 Input       :  FsEvpnAnycastGwMac
 Descritpion :  This Routine will Get
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanSetFsEvpnAnycastGwMac (tMacAddr FsEvpnAnycastGwMac)
{
    MEMCPY (gVxlanGlobals.VxlanGlbMib.FsEvpnAnycastGwMac,
            FsEvpnAnycastGwMac, VXLAN_ETHERNET_ADDR_SIZE);
    return OSIX_SUCCESS;
}

#endif /* EVPN_VXLAN_WANTED */
