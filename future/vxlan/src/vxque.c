/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxque.c,v 1.7 2018/01/05 15:26:48 siva Exp $
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "vxinc.h"

/* ************************************************************************* *
 *  Function Name   : VxlanEnqueMsg                                          *
 *  Description     : Enques the VXLAN message                               *
 *  Input           : pVxlanIfMsg - Queue message                            *
 *                    u4IfIndex - Incoming interface                         *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *  Output          : NONE                                                   *
 *  Returns         : VPLS_FAILURE / VPLS_SUCCESS                            *
 * ************************************************************************* */

UINT4
VxlanEnqueMsg (tVxlanIfMsg * pVxlanIfMsg)
{
    UINT1              *pu1TmpMsg = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanEnqueMsg: Entry\n"));

    /* Allocate memory for the message */
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (VXLAN_QUEUE_POOLID);

    if (pu1TmpMsg == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation failed "
                         " for the queue message\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation succed "
                     "for the queue message\n"));

    MEMCPY (pu1TmpMsg, (UINT1 *) pVxlanIfMsg, sizeof (tVxlanIfMsg));

    /* Send the Message to VXLAN msg queue */

    if (OsixQueSend (gVxlanGlobals.vxlanQueId, (UINT1 *) (&pu1TmpMsg),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Queue send failed\n"));
        MemReleaseMemBlock (VXLAN_QUEUE_POOLID, (UINT1 *) pu1TmpMsg);
        return VXLAN_FAILURE;
    }
    else
    {
        if (OsixEvtSend (gVxlanGlobals.vxlanTaskId, VXLAN_QUEUE_EVENT) ==
            OSIX_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Queue event send failed\n"));
            MemReleaseMemBlock (VXLAN_QUEUE_POOLID, (UINT1 *) pu1TmpMsg);
            return VXLAN_FAILURE;
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanEnqueMsg: Exit\n"));

    KW_FALSEPOSITIVE_FIX1 (pu1TmpMsg);

    return VXLAN_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : VxlanQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
VxlanQueProcessMsgs ()
{
    tVxlanIfMsg        *pVxlanQueMsg = NULL;
#ifdef MBSM_WANTED
#ifdef NPAPI_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    INT4                i4RetVal = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif
#endif /* MBSM_WANTED */
#ifdef NPAPI_WANTED
#ifdef EVB_WANTED
    tVlanEvbSbpArray    SbpArray;
    UINT1               u1LoopIndex = 0;
#endif
    INT4                i4BrgPortType = 0;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanQueProcessMsgs: Entry\n"));

    while (OsixQueRecv (gVxlanGlobals.vxlanQueId,
                        (UINT1 *) &pVxlanQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        switch (pVxlanQueMsg->u4MsgType)
        {

            case VXLAN_L2PKT_RCVD_EVENT:
                /* Processing the L2 packet received from CFA module */
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "VXLAN Encapsulation process starts\n"));
                if (VxlanEncapAndFwdPkt
                    (pVxlanQueMsg->uVxlanMsg.VxlanL2PktInfo.pBuf,
                     pVxlanQueMsg->uVxlanMsg.VxlanL2PktInfo.u4IfIndex) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                     "VXLAN Encapsulation process failed\n"));
                    CRU_BUF_Release_MsgBufChain (pVxlanQueMsg->uVxlanMsg.
                                                 VxlanL2PktInfo.pBuf, FALSE);
                }
                else
                {
                    VXLAN_TRC ((VXLAN_MAIN_TRC,
                                "VXLAN Encapsulation process ends\n"));
                }
                break;

            case VXLAN_UDP_IPV4_EVENT:
                /* Processing the IPV4 UDP message received on the SLI socket */
                VXLAN_TRC ((VXLAN_MAIN_TRC, "Process IPV4 UDP message \n"));
                if (VxlanUdpProcessPktOnSocket
                    (pVxlanQueMsg->uVxlanMsg.i4SockId) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "IPV4 UDP message handling failed\n"));
                }
                break;
#ifdef IP6_WANTED
            case VXLAN_UDP_IPV6_EVENT:
                /* Processing the IPV6 UDP message received on the SLI socket */
                VXLAN_TRC ((VXLAN_MAIN_TRC, "Process IPV6 UDP message \n"));
                if (VxlanUdpv6ProcessPktOnSocket
                    (pVxlanQueMsg->uVxlanMsg.i4SockId) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "IPV6 UDP message handling failed\n"));
                }
                break;
#endif
#ifdef L2RED_WANTED
            case VXLAN_RM_MSG_RECVD:

                VxlanRedHandleRmEvents (&pVxlanQueMsg->uVxlanMsg.VxlanRmMsg);

                break;
#endif

            case VXLAN_VLAN_VNI_DEL_EVENT:
                VxlanDeleteVlanVniDatabase ((UINT2) pVxlanQueMsg->uVxlanMsg.
                                            u4IfIndex);
                break;

            case VXLAN_NVE_DEL_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE int del indication \n"));
                VxlanUtilDelNveIntDatabases (pVxlanQueMsg->uVxlanMsg.u4IfIndex);
                break;
            case VXLAN_PORT_OPER_DOWN:
            case VXLAN_PORT_AGEOUT:
                VXLAN_TRC ((VXLAN_MAIN_TRC, "Port oper down indication \r\n"));
#ifdef NPAPI_WANTED
                CfaGetInterfaceBrgPortType (pVxlanQueMsg->uVxlanMsg.u4IfIndex,
                                            &i4BrgPortType);
                if (pVxlanQueMsg->uVxlanMsg.u4IfIndex == 0)
                {
                    VxlanL2TableFlushIndication ();
                }
                else
                {

                    if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                    {
#ifdef EVB_WANTED
                        MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
                        if (pVxlanQueMsg->u4MsgType == VXLAN_PORT_OPER_DOWN)
                        {
                            VlanApiEvbGetSbpPortsOnUap (pVxlanQueMsg->uVxlanMsg.
                                                        u4IfIndex, &SbpArray);
                        }
                        else if (pVxlanQueMsg->u4MsgType == VXLAN_PORT_AGEOUT)
                        {
                            VlanApiEvbGetSbpPortsOnUap (pVxlanQueMsg->uVxlanMsg.
                                                        u4IfIndex, &SbpArray);
                        }
                        while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
                        {
                            if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
                            {
                                /* No more SBPs are present on this interface */
                                break;
                            }
                            VxlanDelPortFromL2Table (SbpArray.
                                                     au4SbpArray[u1LoopIndex]);
                            u1LoopIndex++;
                        }
#endif
                    }
                    VxlanDelPortFromL2Table (pVxlanQueMsg->uVxlanMsg.u4IfIndex);
                }
#endif
                break;
            case VXLAN_ROUTE_DEL_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC, "Process Route del indication \n"));
                VxlanHandleRtChgEvent (pVxlanQueMsg->u4Addr,
                                       pVxlanQueMsg->uVxlanMsg.u4IfIndex,
                                       pVxlanQueMsg->u1CmdType);
                break;

#ifdef NPAPI_WANTED
            case VXLAN_NP_MAC_VLAN_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE ADD/DEL event from NPAPI \n"));
                if (pVxlanQueMsg->uVxlanMsg.VxlanNpMacVlanInfo.u1Flag ==
                    VXLAN_HW_ADD)
                {
                    VxlanAddNveDatabase (pVxlanQueMsg->uVxlanMsg.
                                         VxlanNpMacVlanInfo.NpMacAddr,
                                         pVxlanQueMsg->uVxlanMsg.
                                         VxlanNpMacVlanInfo.VlanId2,
                                         pVxlanQueMsg->uVxlanMsg.
                                         VxlanNpMacVlanInfo.u4RemoteVTEPIp);
                }
                else
                {
                    VxlanDelNveDatabase (pVxlanQueMsg->uVxlanMsg.
                                         VxlanNpMacVlanInfo.NpMacAddr,
                                         pVxlanQueMsg->uVxlanMsg.
                                         VxlanNpMacVlanInfo.VlanId2);
                }
                break;
            case VXLAN_NP_MAC_PORT_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE/L2Table ADD/DEL event from NPAPI \n"));
                VxlanUpdateMacEntry (pVxlanQueMsg->uVxlanMsg.VxlanNpMacPortInfo.
                                     u4Vni,
                                     pVxlanQueMsg->uVxlanMsg.VxlanNpMacPortInfo.
                                     NpMacAddr,
                                     (UINT4) (pVxlanQueMsg->uVxlanMsg.
                                              VxlanNpMacPortInfo.i4Port),
                                     pVxlanQueMsg->uVxlanMsg.VxlanNpMacPortInfo.
                                     u1Flag);
                break;
#endif

#ifdef EVPN_VXLAN_WANTED
            case VXLAN_EVPN_MAC_ADD_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE databace addition indication from BGP \n"));
                EvpnVxlanAddNvedatabase (&(pVxlanQueMsg->uVxlanMsg.EvpnRoute));
                break;
            case VXLAN_EVPN_MAC_DEL_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE databace addition indication from BGP \n"));
                EvpnVxlanDelNvedatabase (&(pVxlanQueMsg->uVxlanMsg.EvpnRoute));
                break;
            case VXLAN_EVPN_ESI_IF_OPER_CHNG:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process the ESI and Interface Operstatus indication from BGP \n"));
                EvpnVxlanProcessEsiAndIfStateChange (pVxlanQueMsg->uVxlanMsg.
                                                     VlanOper.u2VlanId,
                                                     pVxlanQueMsg->uVxlanMsg.
                                                     VlanOper.SysID,
                                                     pVxlanQueMsg->uVxlanMsg.
                                                     VlanOper.u2SysPriority,
                                                     pVxlanQueMsg->uVxlanMsg.
                                                     VlanOper.u1OperStatus);
                break;
            case VXLAN_L3PKT_RCVD_EVENT:
                /* Processing the IP packet received from netip module */
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "VXLAN Encapsulation process starts\n"));
                if (VxlanEncapAndFwdL3Pkt
                    (pVxlanQueMsg->uVxlanMsg.VxlanL3PktInfo.pBuf,
                     pVxlanQueMsg->uVxlanMsg.VxlanL3PktInfo.u4VrfId,
                     pVxlanQueMsg->uVxlanMsg.VxlanL3PktInfo.u4L3VnId) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                     "VXLAN Encapsulation process failed\n"));
                    CRU_BUF_Release_MsgBufChain (pVxlanQueMsg->uVxlanMsg.
                                                 VxlanL2PktInfo.pBuf, FALSE);
                }
                else
                {
                    VXLAN_TRC ((VXLAN_MAIN_TRC,
                                "VXLAN Encapsulation process ends\n"));
                }
                break;
#endif

            case VXLAN_NVE_ADMIN_EVENT:
                VXLAN_TRC ((VXLAN_MAIN_TRC,
                            "Process NVE int del indication \n"));
                VxlanHandleNveIntAdminStatus (pVxlanQueMsg->uVxlanMsg.u4IfIndex,
                                              pVxlanQueMsg->i4Adminstatus);
                break;
#ifdef MBSM_WANTED
#ifdef NPAPI_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:

                i4ProtoId =
                    pVxlanQueMsg->uVxlanMsg.MbsmCardUpdate.
                    mbsmProtoMsg.i4ProtoCookie;
                pSlotInfo =
                    &(pVxlanQueMsg->uVxlanMsg.MbsmCardUpdate.
                      mbsmProtoMsg.MbsmSlotInfo);

                i4RetVal = VxlanMbsmCardInsertRemove (pSlotInfo,
                                                      pVxlanQueMsg->u4MsgType);

                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                break;
#endif
#endif

            default:
                break;

        }
        MemReleaseMemBlock (VXLAN_QUEUE_POOLID, (UINT1 *) pVxlanQueMsg);
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanQueProcessMsgs: Exit\n"));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlanque.c                      */
/*-----------------------------------------------------------------------*/
