/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vxred.c,v 1.3 2018/01/05 09:57:11 siva Exp $
 *
 * Description: This file contains Vxlan High Availability related 
 *              Core functionalities
 *******************************************************************/
#ifndef __VxlanRED_C
#define __VxlanRED_C

#include "vxinc.h"
#ifdef L2RED_WANTED

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDynDataDescInit                            */
/*                                                                           */
/*    Description         : This function will initialize session dynamic    */
/*                          info data descriptor.                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pVxlanDynDataDesc = NULL;

    pVxlanDynDataDesc = &(gaVxlanDynDataDescList[VXLAN_DYN_INFO]);

    pVxlanDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pVxlanDynDataDesc->u4DbDataSize =
        sizeof (tVxlanMibFsVxlanNveEntry) -
        FSAP_OFFSETOF (tVxlanMibFsVxlanNveEntry, u4FsVxlanNveVniNumber);

    pVxlanDynDataDesc->pDbDataOffsetTbl = gaVxlanOffsetTbl;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDescrTblInit                               */
/*                                                                           */
/*    Description         : This function will initialize Bfd descriptor     */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDescrTblInit (VOID)
{
    tDbDescrParams      VxlanDescrParams;
    tDbDescrParams      VxlanDescrParams1;
    MEMSET (&VxlanDescrParams, 0, sizeof (tDbDescrParams));
    MEMSET (&VxlanDescrParams1, 0, sizeof (tDbDescrParams));

    VxlanDescrParams.u4ModuleId = RM_VXLAN_APP_ID;

    VxlanDescrParams.pDbDataDescList = gaVxlanDynDataDescList;

    DbUtilTblInit (&gVxlanDynInfoList, &VxlanDescrParams);

    VxlanDescrParams1.u4ModuleId = RM_VXLAN_APP_ID;
    VxlanDescrParams1.pDbDataDescList = NULL;

    DbUtilTblInit (&gVxlanDynHwInfoList, &VxlanDescrParams1);

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : VxlanRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register Vxlan with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
VxlanRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_VXLAN_APP_ID;

    RmRegParams.pFnRcvPkt = VxlanRedRmCallBack;

    /* Register Vxlan with RM */
    if (RmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&gVxlanRedGblInfo, 0, sizeof (tVxlanRedGlobalInfo));

    VXLAN_GET_NODE_STATUS () = RM_INIT;
    VXLAN_NUM_STANDBY_NODES () = 0;
    VXLAN_RM_BULK_REQ_RCVD () = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDbNodeInit                                 */
/*                                                                           */
/*    Description         : This function will initialize Vxlan Oper db node.  */
/*                                                                           */
/*    Input(s)            : pVxlanDbTblNode - pointer to Vxlan Db Entry.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDbNodeInit (tDbTblNode * pVxlanDbTblNode)
{
    DbUtilNodeInit (pVxlanDbTblNode, VXLAN_DYN_INFO);

    return;
}

/************************************************************************
 * Function Name      : VxlanRedDeInitGlobalInfo
 *
 * Description        : This function is invoked by the Vxlan module
 *                      during module shutdown and this function
 *                      deinitializes the redundancy global variables
 *                      and de-register Vxlan with RM.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

INT4
VxlanRedDeInitGlobalInfo (VOID)
{
    if (RmDeRegisterProtocols (RM_VXLAN_APP_ID) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : VxlanRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to Vxlan        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
VxlanRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tVxlanIfMsg         Msg;

    MEMSET (&Msg, 0, sizeof (tVxlanIfMsg));
    /* Callback function for RM events. The event and the message is sent as
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        return OSIX_FAILURE;
    }

    Msg.uVxlanMsg.VxlanRmMsg.RmCtrlMsg.pData = pData;
    Msg.uVxlanMsg.VxlanRmMsg.RmCtrlMsg.u1Event = u1Event;
    Msg.uVxlanMsg.VxlanRmMsg.RmCtrlMsg.u2DataLen = u2DataLen;

    Msg.u4MsgType = VXLAN_RM_MSG_RECVD;
    /* Post event to Vxlan Task. Event - VXLAN_RM_MSG_RECVD */
    VxlanEnqueMsg (&Msg);

    return OSIX_SUCCESS;

}

/************************************************************************/
/* Function Name      : VxlanRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the Vxlan module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
VxlanRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{

    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VxlanRedHandleRmEvents
 *
 * Description        : This function is invoked by the Vxlan module to
 *                      process all the events and messages posted by
 *                      the RM module
 *
 * Input(s)           : pMsg -- Pointer to the Vxlan Q Msg
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/
PUBLIC VOID
VxlanRedHandleRmEvents (tVxlanRmMsg * pVxlanRmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    switch (pVxlanRmMsg->RmCtrlMsg.u1Event)
    {
        case GO_ACTIVE:

            VxlanRedHandleGoActive ();
            break;
        case GO_STANDBY:
            VxlanRedHandleGoStandby (pVxlanRmMsg);
            /* pMsg is passed as a argument to GoStandby function and
             * it is released there. If the transformation is from active
             * to standby then there is no need for releasing the mempool
             * as we are calling DeInit function. This function clears
             * all the mempools, so it is returned here instead of break.*/
            break;
        case RM_STANDBY_UP:

            /* Standby up event, number of standby node is updated.
             * BulkReqRcvd flag is checked, if it is true, then Bulk update
             * message is sent to the standby node and the flag is reset */

            pData = (tRmNodeInfo *) pVxlanRmMsg->RmCtrlMsg.pData;
            VXLAN_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VxlanRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (VXLAN_RM_BULK_REQ_RCVD () == OSIX_TRUE)
            {
                VXLAN_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_NOT_STARTED;
                VxlanRedSendBulkUpdMsg ();
            }
            break;
        case RM_STANDBY_DOWN:
            /* Standby down event, number of standby nodes is updated */
            pData = (tRmNodeInfo *) pVxlanRmMsg->RmCtrlMsg.pData;
            VXLAN_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VxlanRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pVxlanRmMsg->RmCtrlMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pVxlanRmMsg->RmCtrlMsg.pData,
                                 pVxlanRmMsg->RmCtrlMsg.u2DataLen);

            ProtoAck.u4AppId = RM_VXLAN_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gVxlanRedGblInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* Process the message at active */
                VxlanRedProcessPeerMsgAtActive (pVxlanRmMsg->RmCtrlMsg.pData,
                                                pVxlanRmMsg->RmCtrlMsg.
                                                u2DataLen);
            }
            else if (gVxlanRedGblInfo.u1NodeStatus == RM_STANDBY)
            {

                /* Process the message at standby */
                VxlanRedProcessPeerMsgAtStandby (pVxlanRmMsg->RmCtrlMsg.pData,
                                                 pVxlanRmMsg->RmCtrlMsg.
                                                 u2DataLen);
            }

            RM_FREE (pVxlanRmMsg->RmCtrlMsg.pData);
            RmApiSendProtoAckToRM (&ProtoAck);
            break;
        case RM_CONFIG_RESTORE_COMPLETE:
            /* Config restore complete event is sent by the RM on completing
             * the static configurations. If the node status is init,
             * and the rm state is standby, then the VRRP status is changed
             * from idle to standby */
            if (gVxlanRedGblInfo.u1NodeStatus == RM_INIT)
            {
                if (VXLAN_GET_RMNODE_STATUS () == RM_STANDBY)
                {
                    VxlanRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    RmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            /* L2 Initiate bulk update is sent by RM to the standby node
             * to send a bulk update request to the active node */
            VxlanRedSendBulkReqMsg ();
            break;

        default:
            break;
    }
    /*MemReleaseMemBlock (VXLAN_QUEMSG_POOLID, (UINT1 *) pVxlanRmMsg); */
    return;
}

/************************************************************************
 * Function Name      : VxlanRedHandleGoActive
 *
 * Description        : This function is invoked by the Vxlan upon
 *                      receiving the GO_ACTIVE indication from RM
 *                      module. And this function responds to RM with an
 *                      acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
VxlanRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_NOT_STARTED;

    if (VXLAN_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        return;
    }
    if (VXLAN_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        VxlanRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (VXLAN_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node,
         * Do hardware audit, and start the timers for all the arp entries
         * and change the state to active */
        VxlanRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (VXLAN_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        VXLAN_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_NOT_STARTED;
        VxlanRedSendBulkUpdMsg ();
    }

    RmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleIdleToActive (VOID)
{
    /* Node status is set to active and the number of standby nodes
     * are updated */
    VXLAN_GET_NODE_STATUS () = RM_ACTIVE;
    VXLAN_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleStandbyToActive (VOID)
{
    /* Hardware audit is done and the hardware and software entries are
     * checked for synchronization */

    VXLAN_GET_NODE_STATUS () = RM_ACTIVE;
    VXLAN_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedSendBulkUpdMsg (VOID)
{

    if (VXLAN_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }
    if (gVxlanRedGblInfo.u1BulkUpdStatus == VXLAN_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_IN_PROGRESS;
        VxlanRedAddAllNodeInDbTbl ();

        VxlanRedSyncDynInfo ();
        VxlanRedSyncDynPortInfo ();

        RmSetBulkUpdatesStatus (RM_VXLAN_APP_ID);
        gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_COMPLETED;
        VxlanRedSendBulkUpdTailMsg ();
    }
    return;
}

VOID
VxlanRedSyncDynPortInfo ()
{
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanVniVlanPortEntry VxlanVniVlanPortEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
    UINT4               u4LenOffSet = 0;
    tVxlanRmSyncMsg    *pMsg = NULL;
    UINT4               u4OffSet = 0;

    MEMSET (&VxlanVniVlanPortEntry, 0, sizeof (tVxlanVniVlanPortEntry));
    pVxlanNvePortEntry = (tVxlanNvePortEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable);
    while (pVxlanNvePortEntry != NULL)
    {
        u4OffSet = 0;
        if (VxlanRedGetRmMsg (&pMsg, VXLAN_RED_NVE_PORT_ENTRY, &u4OffSet)
            == VXLAN_SUCCESS)
        {
            u4OffSet += 2;
            VXLAN_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                                 pVxlanNvePortEntry->au1RemoteVtepAddress,
                                 VXLAN_IP6_ADDR_LEN);
            VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                 pVxlanNvePortEntry->i4RemoteVtepAddressLen);
            VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                 pVxlanNvePortEntry->u4NetworkPort);
            VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                 pVxlanNvePortEntry->u4NetworkBUMPort);
            u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
            VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
            DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
            pMsg->u4DataLen = u4OffSet;
            VxlanRedSyncDynBufferInfo ();
            KW_FALSEPOSITIVE_FIX1 (pMsg);
        }
        else
        {
            return;
        }
        pVxlanNvePortEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                           pVxlanNvePortEntry, NULL);
    }
    pVxlanFsVxlanVniVlanMapEntry = VxlanGetFirstFsVxlanVniVlanMapTable ();
    while (pVxlanFsVxlanVniVlanMapEntry != NULL)
    {
        pVxlanVniVlanPortMacEntry =
            RBTreeGetFirst (pVxlanFsVxlanVniVlanMapEntry->
                            VxlanVniVlanPortMacTable);
        while (pVxlanVniVlanPortMacEntry != NULL)
        {
            VxlanVniVlanPortEntry.u4IfIndex =
                pVxlanVniVlanPortMacEntry->u4IfIndex;
            pVxlanVniVlanPortEntry =
                RBTreeGet (pVxlanFsVxlanVniVlanMapEntry->VxlanVniVlanPortTable,
                           (tRBElem *) & VxlanVniVlanPortEntry);
            if (pVxlanVniVlanPortEntry != NULL)
            {
                u4OffSet = 0;
                if (VxlanRedGetRmMsg
                    (&pMsg, VXLAN_RED_MAC_PORT_ENTRY,
                     &u4OffSet) == VXLAN_SUCCESS)
                {
                    u4OffSet += 2;
                    VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                         pVxlanFsVxlanVniVlanMapEntry->
                                         MibObject.
                                         u4FsVxlanVniVlanMapVniNumber);
                    VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                         pVxlanVniVlanPortEntry->u4AccessPort);
                    VXLAN_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                                         pVxlanVniVlanPortMacEntry->MacAddress,
                                         VXLAN_ETHERNET_ADDR_SIZE);
                    u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
                    VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
                    DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
                    pMsg->u4DataLen = u4OffSet;
                    VxlanRedSyncDynBufferInfo ();
                    KW_FALSEPOSITIVE_FIX1 (pMsg);
                    pVxlanVniVlanPortMacEntry =
                        RBTreeGetNext (pVxlanFsVxlanVniVlanMapEntry->
                                       VxlanVniVlanPortMacTable,
                                       pVxlanVniVlanPortMacEntry, NULL);
                }
                else
                {
                    return;
                }
            }
        }
        pVxlanVniVlanPortEntry = NULL;
        pVxlanVniVlanPortEntry =
            RBTreeGetFirst (pVxlanFsVxlanVniVlanMapEntry->
                            VxlanVniVlanPortTable);
        while (pVxlanVniVlanPortEntry != NULL)
        {
            u4OffSet = 0;
            if (VxlanRedGetRmMsg (&pMsg, VXLAN_RED_ACCESS_PORT_ENTRY, &u4OffSet)
                == VXLAN_SUCCESS)
            {
                u4OffSet += 2;
                VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                     pVxlanVniVlanPortEntry->u4IfIndex);
                VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                     pVxlanVniVlanPortEntry->u4AccessPort);
                VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                     pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                     u4FsVxlanVniVlanMapVniNumber);
                VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                     pVxlanFsVxlanVniVlanMapEntry->MibObject.
                                     i4FsVxlanVniVlanMapVlanId);
                u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
                VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
                DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
                pMsg->u4DataLen = u4OffSet;
                VxlanRedSyncDynBufferInfo ();
                KW_FALSEPOSITIVE_FIX1 (pMsg);
                pVxlanVniVlanPortEntry =
                    RBTreeGetNext (pVxlanFsVxlanVniVlanMapEntry->
                                   VxlanVniVlanPortTable,
                                   pVxlanVniVlanPortEntry, NULL);
            }
            else
            {
                return;
            }
        }
        pVxlanFsVxlanVniVlanMapEntry =
            VxlanGetNextFsVxlanVniVlanMapTable (pVxlanFsVxlanVniVlanMapEntry);
    }

    return;
}

VOID
VxlanRedSyncDynNvePortInfo (tVxlanNvePortEntry * pVxlanNvePortEntry)
{
    UINT4               u4LenOffSet = 0;
    tVxlanRmSyncMsg    *pMsg = NULL;
    UINT4               u4OffSet = 0;

    if (VxlanRedGetRmMsg (&pMsg, VXLAN_RED_NVE_PORT_ENTRY, &u4OffSet)
        == VXLAN_SUCCESS)
    {
        u4OffSet += 2;
        VXLAN_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanNvePortEntry->au1RemoteVtepAddress,
                             VXLAN_IP6_ADDR_LEN);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanNvePortEntry->i4RemoteVtepAddressLen);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanNvePortEntry->u4NetworkPort);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanNvePortEntry->u4NetworkBUMPort);
        u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
        VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
        DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
        pMsg->u4DataLen = u4OffSet;
        VxlanRedSyncDynBufferInfo ();
        KW_FALSEPOSITIVE_FIX1 (pMsg);
    }
    return;
}

VOID
VxlanRedSyncDynVlanVniPortInfo (tVxlanVniVlanPortEntry * pVxlanVniVlanPortEntry,
                                tVxlanFsVxlanVniVlanMapEntry *
                                pVxlanFsVxlanVniVlanMapEntry)
{
    UINT4               u4LenOffSet = 0;
    tVxlanRmSyncMsg    *pMsg = NULL;
    UINT4               u4OffSet = 0;

    if (VxlanRedGetRmMsg (&pMsg, VXLAN_RED_ACCESS_PORT_ENTRY, &u4OffSet)
        == VXLAN_SUCCESS)
    {
        u4OffSet += 2;
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanVniVlanPortEntry->u4IfIndex);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanVniVlanPortEntry->u4AccessPort);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanFsVxlanVniVlanMapEntry->MibObject.
                             u4FsVxlanVniVlanMapVniNumber);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanFsVxlanVniVlanMapEntry->MibObject.
                             i4FsVxlanVniVlanMapVlanId);
        u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
        VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
        DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
        pMsg->u4DataLen = u4OffSet;
        VxlanRedSyncDynBufferInfo ();
        KW_FALSEPOSITIVE_FIX1 (pMsg);
    }
    return;
}

VOID
VxlanRedSyncDynVlanVniPortMacInfo (tVxlanVniVlanPortMacEntry *
                                   pVxlanVniVlanPortMacEntry,
                                   tVxlanFsVxlanVniVlanMapEntry *
                                   pVxlanFsVxlanVniVlanMapEntry,
                                   UINT4 u4AccessPort)
{
    UINT4               u4LenOffSet = 0;
    tVxlanRmSyncMsg    *pMsg = NULL;
    UINT4               u4OffSet = 0;

    if (VxlanRedGetRmMsg (&pMsg, VXLAN_RED_MAC_PORT_ENTRY, &u4OffSet)
        == VXLAN_SUCCESS)
    {
        u4OffSet += 2;
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanFsVxlanVniVlanMapEntry->MibObject.
                             u4FsVxlanVniVlanMapVniNumber);
        VXLAN_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4AccessPort);
        VXLAN_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                             pVxlanVniVlanPortMacEntry->MacAddress,
                             VXLAN_ETHERNET_ADDR_SIZE);
        u4LenOffSet = VXLAN_RED_TYPE_FIELD_SIZE;
        VXLAN_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
        DbUtilAddTblNode (&gVxlanDynHwInfoList, &pMsg->DbNode);
        pMsg->u4DataLen = u4OffSet;
        VxlanRedSyncDynBufferInfo ();
        KW_FALSEPOSITIVE_FIX1 (pMsg);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  VxlanRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedAddAllNodeInDbTbl (VOID)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    pVxlanFsVxlanNveEntry = VxlanGetFirstFsVxlanNveTable ();
    while (pVxlanFsVxlanNveEntry != NULL)
    {
        if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
            VXLAN_STRG_TYPE_VOL)
        {
            VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                      &(pVxlanFsVxlanNveEntry->MibObject.
                                        NveDbNode));
        }
        pVxlanFsVxlanNveEntry =
            VxlanGetNextFsVxlanNveTable (pVxlanFsVxlanNveEntry);
    }
    return;
}

/*****************************************************************************/
/*    Function Name       : VxlanRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate Vxlan dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedSyncDynInfo (VOID)
{
    if ((VXLAN_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gVxlanRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gVxlanDynInfoList);

    return;
}

/*****************************************************************************/
/*    Function Name       : VxlanRedSyncDynBufferInfo                        */
/*                                                                           */
/*    Description         : This function will initiate Vxlan dynamic info   */
/*                          syncup of DS Other than NVE  from active node.   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedSyncDynBufferInfo (VOID)
{
    if ((VXLAN_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gVxlanRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }
    if (OSIX_FAILURE ==
        DbUtilSyncModuleBuffers (&gVxlanDynHwInfoList,
                                 VXLAN_RED_MSG_MEMPOOL_ID))
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "Vxlan Enquue to rm failed \r\n"));
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pVxlanDataDesc - This is Vxlan sepcific data desri-  */
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pNveDbNode -This is db node defined in the Vxlan*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDbUtilAddTblNode (tDbTblDescriptor * pVxlanDataDesc,
                          tDbTblNode * pNveDbNode)
{

    if ((VXLAN_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gVxlanRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pVxlanDataDesc, pNveDbNode);

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends                             */
/*                                                                      */
/* Input(s)           : pVxlanBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
VxlanRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * VXLAN_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (VXLAN_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u4OffSet = 0;

    VXLAN_RM_PUT_1_BYTE (pMsg, &u4OffSet, VXLAN_RED_BULK_UPD_TAIL_MESSAGE);
    VXLAN_RM_PUT_2_BYTE (pMsg, &u4OffSet, VXLAN_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (VxlanRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
VxlanRedSendMsgToRm (tRmMsg * pMsg, UINT4 u4Length)
{
    UINT4               u4RetVal = 0;

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, (UINT2) u4Length,
                                     RM_VXLAN_APP_ID, RM_VXLAN_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VxlanRedHandleGoStandby
 *
 * Description        : This function is invoked by the Vxlan upon
 *                      receiving the GO_STANBY indication from RM
 *                      module. And this function responds to RM module
 *                      with an acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
VxlanRedHandleGoStandby (tVxlanRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
/*    MemReleaseMemBlock (VXLAN_QUEMSG_POOLID, (UINT1 *) pMsg); */
    UNUSED_PARAM (pMsg);
    gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_NOT_STARTED;

    if (VXLAN_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        return;
    }
    if (VXLAN_GET_NODE_STATUS () == RM_INIT)
    {
        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        VxlanRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
        {
            return;
        }
    }
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleActiveToStandby (VOID)
{

    /*update the statistics */
    VXLAN_GET_NODE_STATUS () = RM_STANDBY;
    VXLAN_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                     */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;

    VXLAN_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    VXLAN_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != VXLAN_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == VXLAN_RED_BULK_REQ_MESSAGE)
    {
        gVxlanRedGblInfo.u1BulkUpdStatus = VXLAN_HA_UPD_NOT_STARTED;
        if (!VXLAN_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gVxlanRedGblInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        VxlanRedSendBulkUpdMsg ();
    }
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    u2MinLen = VXLAN_RED_TYPE_FIELD_SIZE + VXLAN_RED_LEN_FIELD_SIZE;

    while (u4OffSet <= u2DataLen)
    {
        u2ExtractMsgLen = (UINT2) u4OffSet;
        VXLAN_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        VXLAN_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }

        switch (u1MsgType)
        {
            case VXLAN_RED_BULK_UPD_TAIL_MESSAGE:
                VxlanRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case VXLAN_RED_DYN_CACHE_INFO:
                VxlanRedProcessDynamicInfo (pMsg, &u4OffSet);
                break;
            case VXLAN_RED_NVE_PORT_ENTRY:
                VxlanRedProcessDynamicNvePortInfo (pMsg, &u4OffSet);
                break;
            case VXLAN_RED_MAC_PORT_ENTRY:
                VxlanRedProcessDynamicMacPortInfo (pMsg, &u4OffSet);
                break;
            case VXLAN_RED_ACCESS_PORT_ENTRY:
                VxlanRedProcessDynamicAccessPortInfo (pMsg, &u4OffSet);
                break;
            default:
                break;
        }
    }

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VxlanRedProcessDynamicInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{

    tVxlanFsVxlanNveEntry *pVxlanAddNveEntry = NULL;
    UINT1               au1Temp[VXLAN_RM_BYTE];
    UINT4               u4TmpMsgType = 0;
    UINT2               u2VlanId = 0;

    MEMSET (au1Temp, 0, VXLAN_RM_BYTE);
    pVxlanAddNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanAddNveEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation failed for the"
                         "new NVE entry\n"));
        return;
    }

    VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation succeed for the"
                     "new NVE entry\n"));

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet,
                         pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber);

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveIfIndex = (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveVtepAddressType =
        (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType =
        (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType = (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanSuppressArp = (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveRowStatus = (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveVtepAddressLen =
        (INT4) u4TmpMsgType;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TmpMsgType);
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen =
        (INT4) u4TmpMsgType;

    if (pVxlanAddNveEntry->MibObject.i4FsVxlanNveVtepAddressType ==
        VXLAN_IPV4_UNICAST)
    {
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                             pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveVtepAddress, VXLAN_IP4_ADDR_LEN);
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet, au1Temp, VXLAN_RM_BYTE);

    }
    else
    {
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                             pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveVtepAddress, VXLAN_IP6_ADDR_LEN);
    }

    if (pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType ==
        VXLAN_IPV4_UNICAST)
    {
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                             pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress,
                             VXLAN_IP4_ADDR_LEN);
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet, au1Temp, VXLAN_RM_BYTE);

    }
    else
    {
        VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                             pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress,
                             VXLAN_IP6_ADDR_LEN);
    }
    VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                         pVxlanAddNveEntry->MibObject.au1FsEvpnVxlanVrfName,
                         EVPN_MAX_VRF_NAME_LEN);

    VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet,
                         pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac,
                         VXLAN_ETHERNET_ADDR_SIZE);

    if (RBTreeAdd
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
         (tRBElem *) pVxlanAddNveEntry) != RB_SUCCESS)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "RB Tree addition failed\n"));
        return;
    }
    VxlanUtilGetVlanFromVni (pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber,
                             &u2VlanId);
    pVxlanAddNveEntry->i4VlanId = (INT4) u2VlanId;
    if (VxlanHwUpdateNveDatabase (pVxlanAddNveEntry, VXLAN_HW_ADD) ==
        VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_FAIL_TRC, "Failed to delete Nve entry from h/w\n"));
        return;
    }

    return;
}

VOID
VxlanRedProcessDynamicNvePortInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT1               au1Address[VXLAN_IP6_ADDR_LEN];
    UINT4               u4AddessType = 0;
    UINT4               u4NetPort = 0;
    UINT4               u4BUMPort = 0;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;

    VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet, au1Address, VXLAN_IP6_ADDR_LEN);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4AddessType);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4NetPort);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BUMPort);

    if ((u4NetPort == 0) || (u4BUMPort == 0))
    {
        return;
    }
    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));
    MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, &au1Address,
            VXLAN_IP4_ADDR_LEN);
    VxlanNvePortEntry.i4RemoteVtepAddressLen = VXLAN_IP4_ADDR_LEN;

    pVxlanNvePortEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                                    (tRBElem *) & VxlanNvePortEntry);
    if (pVxlanNvePortEntry == NULL)
    {
        pVxlanNvePortEntry =
            VxlanNvePortTableAdd (VxlanNvePortEntry.au1RemoteVtepAddress,
                                  VxlanNvePortEntry.i4RemoteVtepAddressLen);
        if (pVxlanNvePortEntry != NULL)
        {
            pVxlanNvePortEntry->u4VxlanTunnelIfIndex = CFA_INVALID_INDEX;
        }

    }

    VxlanUpdateNvePortEntry (au1Address, VXLAN_IPV4_UNICAST, u4NetPort,
                             u4BUMPort);
    return;

}

VOID
VxlanRedProcessDynamicMacPortInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT4               u4AccessPort = 0;
    UINT4               u4Vni = 0;
    tMacAddr            MacAddress;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Vni);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4AccessPort);
    VXLAN_RM_GET_N_BYTE (pMsg, pu4OffSet, MacAddress, VXLAN_ETHERNET_ADDR_SIZE);
    if (u4AccessPort == 0)
    {
        return;
    }
    VxlanUpdateMacEntry (u4Vni, MacAddress, u4AccessPort, VXLAN_HW_ADD);
    return;

}

VOID
VxlanRedProcessDynamicAccessPortInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4AccessPort = 0;
    UINT4               u4Vni = 0;
    UINT4               i4VlanId = 0;

    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4AccessPort);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Vni);
    VXLAN_RM_GET_4_BYTE (pMsg, pu4OffSet, i4VlanId);
    if (u4AccessPort == 0)
    {
        return;
    }
    VxlanUpdateAccessPort (u4IfIndex, u4AccessPort, u4Vni, i4VlanId);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleIdleToStandby (VOID)
{
    /* the node status is set to standby and no of standby nodes is set to 0 */
    VXLAN_GET_NODE_STATUS () = RM_STANDBY;
    VXLAN_NUM_STANDBY_NODES () = 0;

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    Vxlan Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */
    if ((pMsg = RM_ALLOC_TX_BUF (VXLAN_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    VXLAN_RM_PUT_1_BYTE (pMsg, &u4OffSet, VXLAN_RED_BULK_REQ_MESSAGE);
    VXLAN_RM_PUT_2_BYTE (pMsg, &u4OffSet, VXLAN_RED_BULK_REQ_MSG_SIZE);

    if (VxlanRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    return;
}

/*****************************************************************************/
/*    Function Name       : VxlanRedGetRmMsg                               */
/*    Description         : This function will allocates a VXLAN RM message    */
/*                          writes the message type and add the node to the  */
/*                          VXLAN data descriptor table                        */
/*    Input(s)            : u1Message - VXLAN RM message type                  */
/*    Output(s)           : pu4OffSet - Offset of the written message        */
/*    Returns             : VXLAN_SUCCESS/VXLAN_FAILURE                        */
/*****************************************************************************/
INT4
VxlanRedGetRmMsg (tVxlanRmSyncMsg ** pMsg, UINT1 u1Message, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    tVxlanRmSyncMsg    *pVxlanMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if ((VXLAN_IS_STANDBY_UP () == OSIX_FALSE) ||
        (VXLAN_GET_NODE_STATUS () != RM_ACTIVE))
    {
        return VXLAN_FAILURE;
    }

    if ((pVxlanMsg =
         (tVxlanRmSyncMsg *) MemAllocMemBlk (VXLAN_RED_MSG_MEMPOOL_ID)) == NULL)
    {
        return VXLAN_FAILURE;
    }
    MEMSET (pVxlanMsg, 0, sizeof (tVxlanRmSyncMsg));
    pVxlanMsg->pBuf = RM_ALLOC_TX_BUF (VXLAN_RED_MSG_MAX_LEN);
    if (pVxlanMsg->pBuf == NULL)
    {
        MemReleaseMemBlock (VXLAN_RED_MSG_MEMPOOL_ID, (UINT1 *) pVxlanMsg);
        ProtoEvt.u4AppId = RM_VXLAN_APP_ID;
        ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return VXLAN_FAILURE;
    }
    VXLAN_RM_PUT_1_BYTE (pVxlanMsg->pBuf, pu4OffSet, u1Message);
    DbUtilNodeInit (&(pVxlanMsg->DbNode), 0);

    *pMsg = pVxlanMsg;
    return VXLAN_SUCCESS;
}

#else
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDynDataDescInit                            */
/*                                                                           */
/*    Description         : This function will initialize session dynamic    */
/*                          info data descriptor.                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDescrTblInit                               */
/*                                                                           */
/*    Description         : This function will initialize Bfd descriptor     */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : VxlanRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register Vxlan with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
VxlanRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDbNodeInit                                 */
/*                                                                           */
/*    Description         : This function will initialize Vxlan Oper db node.  */
/*                                                                           */
/*    Input(s)            : pVxlanDbTblNode - pointer to Vxlan Db Entry.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDbNodeInit (tDbTblNode * pVxlanDbTblNode)
{
    UNUSED_PARAM (pVxlanDbTblNode);

    return;
}

/************************************************************************
 * Function Name      : VxlanRedDeInitGlobalInfo
 *
 * Description        : This function is invoked by the Vxlan module
 *                      during module shutdown and this function
 *                      deinitializes the redundancy global variables
 *                      and de-register Vxlan with RM.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

INT4
VxlanRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : VxlanRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to Vxlan        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
VxlanRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;

}

/************************************************************************/
/* Function Name      : VxlanRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the Vxlan module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
VxlanRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VxlanRedHandleRmEvents
 *
 * Description        : This function is invoked by the Vxlan module to
 *                      process all the events and messages posted by
 *                      the RM module
 *
 * Input(s)           : pMsg -- Pointer to the Vxlan Q Msg
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/
PUBLIC VOID
VxlanRedHandleRmEvents (tVxlanRmMsg * pVxlanRmMsg)
{
    UNUSED_PARAM (pVxlanRmMsg);
    return;
}

/************************************************************************
 * Function Name      : VxlanRedHandleGoActive
 *
 * Description        : This function is invoked by the Vxlan upon
 *                      receiving the GO_ACTIVE indication from RM
 *                      module. And this function responds to RM with an
 *                      acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
VxlanRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedSendBulkUpdMsg (VOID)
{

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  VxlanRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedAddAllNodeInDbTbl (VOID)
{
    return;
}

/*****************************************************************************/
/*    Function Name       : VxlanRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate Vxlan dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedSyncDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VxlanRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pVxlanDataDesc - This is Vxlan sepcific data desri-  */
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pNveDbNode -This is db node defined in the Vxlan*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VxlanRedDbUtilAddTblNode (tDbTblDescriptor * pVxlanDataDesc,
                          tDbTblNode * pNveDbNode)
{
    UNUSED_PARAM (pVxlanDataDesc);
    UNUSED_PARAM (pNveDbNode);

    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends                             */
/*                                                                      */
/* Input(s)           : pVxlanBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
VxlanRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
VxlanRedSendMsgToRm (tRmMsg * pMsg, UINT4 u4Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u4Length);
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VxlanRedHandleGoStandby
 *
 * Description        : This function is invoked by the Vxlan upon
 *                      receiving the GO_STANBY indication from RM
 *                      module. And this function responds to RM module
 *                      with an acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
VxlanRedHandleGoStandby (tVxlanRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                     */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
VxlanRedProcessDynamicInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : VxlanRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VxlanRedSendBulkReqMsg (VOID)
{
    return;
}
#endif
#endif
