/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: vxport.c,v 1.32 2018/01/05 09:57:11 siva Exp $
 *
 * Description : This file contains the utility
 *               functions of the VXLAN module
 *****************************************************************************/

#include "vxinc.h"
#define  INET_ADDR_TYPE_IPV4 1

/* ************************************************************************* *
 *  Function Name   : VxlanProcessL2Pkt                                      *
 *  Description     : Called from CFA-VLAN task to post packet to VXLAN if   *
 *                    nve interface info is configured for the particualr    *
 *                    vlan.                                                  *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                            *
 * ************************************************************************* */
INT4
VxlanProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    tVxlanIfMsg         VxlanIfMsg;
    UINT4               u4Tmp = 0;
    UINT2               u2VlanId = 0;
    UINT4               u4VniNumber = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessL2Pkt: Entry\n"));

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));

    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    if (u4IfIndex == 0)
    {
        /* Get the VLAN id from the frame */
        u4Tmp = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;

        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId, u4Tmp, 2) ==
            CRU_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting VLAN id from L2"
                             "packet failed \n"));
            return VXLAN_FAILURE;
        }
        u2VlanId = OSIX_NTOHS (u2VlanId);
        u2VlanId = 0x0fff & u2VlanId;

        /*Get the VNI id for the VLAN */
        if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniNumber) == VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "VNI is not mapped for the VLAN %d\n",
                             u4VniNumber));
            return VXLAN_FAILURE;
        }

        if ((VxlanUtilGetNveIndexFromNveTable
             (u4VniNumber, &u4IfIndex)) == NULL)
        {
            /* If NVE database is not mapped with the VNI number
             * Get the Ingress Replica database from the VNI number */
            if ((VxlanUtilGetNveIndexFromInReplicaTable
                 (u4VniNumber, &u4IfIndex)) == NULL)
            {
                /* If Ingress Replica database is not mapped with the VNI number
                 * Get the MCAST database from the VNI number */
                if ((VxlanUtilGetNveIndexFromMcastTable
                     (u4VniNumber, &u4IfIndex)) == NULL)
                {
                    /* If MCAST database is also not mapped,
                       dropped the VXLAN packet */
                    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                     "Nve index is not mapped for the VNI - %d"
                                     "in NVE, In-Replica and"
                                     "Multicast database\n", u4VniNumber));
                    return VXLAN_FAILURE;
                }
            }
        }
    }

    /* Fille the message type, if index and packet buffer and enqueue the msg
     * to VXLAN queue
     * */
    VxlanIfMsg.u4MsgType = VXLAN_L2PKT_RCVD_EVENT;
    VxlanIfMsg.uVxlanMsg.VxlanL2PktInfo.pBuf = pBuf;
    VxlanIfMsg.uVxlanMsg.VxlanL2PktInfo.u4IfIndex = u4IfIndex;

    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessL2Pkt: Exit\n"));

    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : VxlanProcessL2Pkt                                      *
 *  Description     : Checks whether the packet is VXLAN packet or not       *
 *  Input           : pBuf - Packet Buffer                                   *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

INT4
VxlanPortCheckMcastVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1LengthCheck)
{
    UINT4               u4VniNumber = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4Tmp = 0;
    UINT4               u4IPHdrSize = 0;
    UINT2               u2DstPort = 0;
    UINT4               u4NveIndex = 0;
    tVxlanHdr          *pVxlanHdr = NULL;
    tIpAddr             McastAddr;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortCheckMcastVxlanPkt: Entry\n"));

    /* When VXLAN is packet trapped from dataplane in intermediate node, Header Length
     * check should be skipped. For this case, We need to check whether trapped packet
     * is VXLAN packet or not, We dont need to check whether VXLAN is enabled or not and
     * Destination port, mcast address are matched or not
     * */

    /* Check whether VXLAN is enabled */
    if ((u1LengthCheck != TRUE) &&
        (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED))
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    u4IPHdrSize = VXLAN_IP_HDR_SIZE;

    MEMSET (&McastAddr, 0, sizeof (tIpAddr));

    u4Tmp = u4IPHdrSize + VXALN_UDP_HDR_SIZE + VXLAN_HDR_TOTAL_OFFSET;

    /* If packet size is less than 36, then it is not a VXLAN pakcet.
     * ip header + UDP header + VXLAN header = 36
     * */

    if (u4PktSize < u4Tmp)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         "Received packet is not a VXLAN packet since"
                         "the size is less than 36 \n"));
        return VXLAN_FAILURE;
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) (&McastAddr), 16,
                                   VXLAN_IP4_ADDR_LEN) == CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting IPV4 source address from"
                         "packet failed \n"));
        return VXLAN_FAILURE;

    }

    u4Tmp = u4IPHdrSize + 2;

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2DstPort, u4Tmp, 2) ==
        CRU_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Getting destination port from"
                         "packet failed \n"));
        return VXLAN_FAILURE;
    }

    u2DstPort = OSIX_HTONS (u2DstPort);

    if ((u1LengthCheck != TRUE) &&
        (u2DstPort != gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort))
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         " Destination port is not matched\r\n"));
        return VXLAN_FAILURE;
    }

    u4Tmp = u4IPHdrSize + VXALN_UDP_HDR_SIZE;

    /* Get the VXLAN header from the packet by moving 28 offset (IP+UDP header) */

    pVxlanHdr = (tVxlanHdr *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear ((pBuf), u4Tmp, VXLAN_HDR_TOTAL_OFFSET);

    /* Validate the VXLAN header */

    if (VxlanUtilValidateHdr (pVxlanHdr, &u4VniNumber) == VXLAN_FAILURE)
    {

        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN header validation failed."
                         "Hence Received packet is not a VXLAN packet\n"));
        return VXLAN_FAILURE;
    }

    if (u1LengthCheck == TRUE)
    {
        return VXLAN_SUCCESS;
    }

    VxlanMainTaskLock ();

    VxlanUtilGetNveIndexFromMcastTable (u4VniNumber, &u4NveIndex);

    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber = u4VniNumber;

    VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4NveIndex;

    pVxlanMCastEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   (tRBElem *) & VxlanMCastEntry);

    if (pVxlanMCastEntry == NULL)
    {
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }

    if (pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType ==
        VXLAN_IPV4_UNICAST)
    {
        if (MEMCMP
            (&(pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress),
             &McastAddr, VXLAN_IP4_ADDR_LEN) != 0)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "Received Multicast group is not configured in VTEP\n"));
            VxlanMainTaskUnLock ();
            return VXLAN_FAILURE;
        }
    }
    VxlanMainTaskUnLock ();
    return VXLAN_SUCCESS;
}

#ifdef NPAPI_WANTED
/* ************************************************************************* *
 *  Function Name   : VxlanPortCheckVniVlanMapAndUpdateNveDb                 *
 *  Description     : Checks the VNI mapping for VLAN ID                     *
 *  Input           : u2VlanId - VLAN id                                     *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
VxlanPortCheckVniVlanMapAndUpdateNveDb (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4VniNumber = 0;
    UINT2               u2VlanId = 0;
    UINT4               u4NveIndex = 0;
    tVxlanFsVxlanNveEntry *pVxlanAddNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tMacAddr            ZeroDestVmAddr;
    tIpAddr             PeerAddr;
    UINT4               u4PktSize = 0;
    UINT1               u1IsDiffVTEP = FALSE;
    UINT4               u4OrgNveIfIndex = 0;
    UINT4               u4Count = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortCheckVniVlanMap: Entry\n"));

    VxlanMainTaskLock ();
    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    VXLAN_TRC ((VXLAN_PKT_TRC, "Received Packet Size: %d\n", u4PktSize));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId,
                               VLAN_TAG_VLANID_OFFSET, 2);

    u2VlanId = OSIX_NTOHS (u2VlanId);
    u2VlanId = 0x0fff & u2VlanId;

    MEMSET (&ZeroDestVmAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&PeerAddr, 0, sizeof (tIpAddr));

    /* Check whether VNI is mapped with the received VLAN id */
    if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniNumber) == VXLAN_FAILURE)
    {
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }

    /* Get the Ingress Replica database from the VNI number */
    if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniNumber, &u4NveIndex))
        == NULL)
    {

        /* If Ingress Replica database is not mapped with the VNI number
         * Get the MCAST database from the VNI number */
        if ((VxlanUtilGetNveIndexFromMcastTable (u4VniNumber, &u4NveIndex))
            == NULL)
        {
            /* If MCAST database is also not mapped, dropped the VXLAN packet */
            VxlanMainTaskUnLock ();
            return VXLAN_FAILURE;
        }
    }
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) (&PeerAddr), 30,
                                   VXLAN_IP4_ADDR_LEN) == CRU_FAILURE)
    {
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }
    pVxlanNveEntry = VxlanUtilGetNveDatabase (u4VniNumber, ZeroDestVmAddr,
                                              u4NveIndex);
    if ((pVxlanNveEntry != NULL) &&
        (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex > CFA_MAX_NVE_IF_INDEX))
    {
        u4NveIndex = pVxlanNveEntry->u4OrgNveIfIndex;
    }
    u4OrgNveIfIndex = u4NveIndex;
    for (u4Count = 0; u4Count < VXLAN_HW_REPLICA_LIST; u4Count++)
    {
        pVxlanNveEntry = VxlanUtilGetNveDatabase (u4VniNumber, ZeroDestVmAddr,
                                                  u4NveIndex);
        if (pVxlanNveEntry != NULL)
        {
            if (MEMCMP (&
                        (pVxlanNveEntry->MibObject.
                         au1FsVxlanNveRemoteVtepAddress), &PeerAddr,
                        VXLAN_IP4_ADDR_LEN) == 0)
            {
                VxlanMainTaskUnLock ();
                return VXLAN_SUCCESS;
            }
            u1IsDiffVTEP = TRUE;
            u4NveIndex = u4NveIndex + SYS_DEF_MAX_NVE_IFACES;
        }
        else
        {
            break;
        }
    }

    pVxlanAddNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

    if (pVxlanAddNveEntry == NULL)
    {
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }

    /* Fill the source VTEP address */
    VxlanInitializeMibFsVxlanNveTable (pVxlanAddNveEntry,
                                       (INT4) u4OrgNveIfIndex);

    /* Fill the Vlan-Id */
    pVxlanAddNveEntry->i4VlanId = (INT4) u2VlanId;
    /* Fill the NVE if index */
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveIfIndex = (INT4) u4NveIndex;

    pVxlanAddNveEntry->u4OrgNveIfIndex = u4OrgNveIfIndex;

    /* Fill the VNI number */
    pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber = u4VniNumber;

    /* Fill the destination VM mac address */
    MEMCPY (&(pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac),
            (&ZeroDestVmAddr), VXLAN_ETHERNET_ADDR_SIZE);

    /* Fill the Remote VTEP IP addres type */
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
        = VXLAN_IPV4_UNICAST;

    /* Fill the Remote VTEP IP address */
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen
        = VXLAN_IP4_ADDR_LEN;
    MEMCPY (&
            (pVxlanAddNveEntry->MibObject.
             au1FsVxlanNveRemoteVtepAddress), &PeerAddr, VXLAN_IP4_ADDR_LEN);

    /* Fill the storage type as volatile since the entry is learnt dynamically */
    pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType = VXLAN_STRG_TYPE_VOL;
    if (RBTreeAdd
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
         (tRBElem *) pVxlanAddNveEntry) != RB_SUCCESS)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "RB Tree addition failed\n"));
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }
    if (VxlanHwUpdateNveDatabase (pVxlanAddNveEntry, VXLAN_HW_ADD) ==
        VXLAN_FAILURE)
    {
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   pVxlanAddNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanAddNveEntry);
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }
    pVxlanMCastEntry = VxlanUtilGetNveIndexFromMcastTable (u4VniNumber,
                                                           &u4NveIndex);
    if (pVxlanMCastEntry != NULL)
    {
        pVxlanMCastEntry->bDummyNveZeroMAC = FALSE;
        if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL)
            == VXLAN_FAILURE)
        {
            VxlanMainTaskUnLock ();
            return VXLAN_FAILURE;
        }
        if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_ADD)
            == VXLAN_FAILURE)
        {
            VxlanMainTaskUnLock ();
            return VXLAN_FAILURE;
        }
    }
    VxlanMainTaskUnLock ();
    UNUSED_PARAM (u1IsDiffVTEP);
    return VXLAN_SUCCESS;
}
#endif

/* ************************************************************************* *
 *  Function Name   : VxlanPortDelNveEntry                                   *
 *  Description     : Deletes the dynamic NVE database                       *
 *  Input           : u4IfIndex - Nve if index                               *
 *                    MacAddr - Mac address                                  *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

VOID
VxlanPortDelNveEntry (UINT4 u4IfIndex, tMacAddr MacAddr, UINT2 u2VlanId)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT4               u4VniNumber = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveEntry: Entry\n"));

    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;

    if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniNumber) == VXLAN_SUCCESS)
    {
        VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4VniNumber;
    }

    MEMCPY (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac, MacAddr,
            VXLAN_ETHERNET_ADDR_SIZE);

    pVxlanNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   (tRBElem *) & VxlanNveEntry);

    if ((pVxlanNveEntry != NULL) &&
        (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
         VXLAN_STRG_TYPE_VOL))
    {
        VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                         "Removes the dynamic NVE database\n"));
        if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
            == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Failed to delete Nve entry from h/w\n"));
            return;
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable, pVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanNveEntry);
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveEntry: Exit\n"));
    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortRemoveVniFromVlan                             *
 *  Description     : Deletes the VniVlan Entry                              *
 *  Input           : u2IfVlanId - Vlan Id                                   *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanPortRemoveVniFromVlan (UINT2 u2IfVlanId)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VxlanIfMsg.uVxlanMsg.u4IfIndex = (UINT4) u2IfVlanId;
    VxlanIfMsg.u4MsgType = VXLAN_VLAN_VNI_DEL_EVENT;
    VxlanEnqueMsg (&VxlanIfMsg);
}

/* ************************************************************************* *
 *  Function Name   : VxlanDeleteVlanVniDatabase                             *
 *  Description     : Deletes the VniVlan Entry                              *
 *  Input           : u2IfVlanId - Vlan Id                                   *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanDeleteVlanVniDatabase (UINT2 u2IfVlanId)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanFsVxlanInReplicaEntry = NULL;
    UINT4               u4NveIfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveVniFromVlan: Entry\n"));

    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
        (INT4) u2IfVlanId;
    pVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanMapEntry);
    if (pVxlanVniVlanMapEntry != NULL)
    {

        pVxlanFsVxlanNveEntry = VxlanUtilGetNveIndexFromNveTable
            (pVxlanVniVlanMapEntry->
             MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
        pVxlanFsVxlanInReplicaEntry = VxlanUtilGetNveIndexFromInReplicaTable
            (pVxlanVniVlanMapEntry->
             MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
        if ((u4NveIfIndex == 0) || (pVxlanFsVxlanInReplicaEntry == NULL))
        {
            pVxlanFsVxlanMCastEntry = VxlanUtilGetNveIndexFromMcastTable
                (pVxlanVniVlanMapEntry->
                 MibObject.u4FsVxlanVniVlanMapVniNumber, &u4NveIfIndex);
        }
        VxlanHandleVlanVniStatus (u4NveIfIndex, pVxlanVniVlanMapEntry->
                                  MibObject.u4FsVxlanVniVlanMapVniNumber,
                                  (INT4) u2IfVlanId, VXLAN_HW_DEL);

        if (VxlanHwUpdateVniVlanMapDatabase (pVxlanVniVlanMapEntry,
                                             VXLAN_HW_DEL) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Failed to delete VNI-VLAN mapping entry in h/w\n"));
        }

        VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                         "Removes the VNIVlan entry from VniVlanMap table\n"));
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   pVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanVniVlanMapEntry);
    }
    UNUSED_PARAM (pVxlanFsVxlanMCastEntry);
    UNUSED_PARAM (pVxlanFsVxlanNveEntry);
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveVniFromVlan: Exit\n"));
    return;
}

INT4
VxlanIsIngressReplicaEnabledForVlan (INT4 i4VlanId)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
#endif
    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);
    while (pVxlanInReplicaEntry != NULL)
    {
        if (pVxlanInReplicaEntry->i4VlanId == i4VlanId)
        {
            return VXLAN_SUCCESS;
        }
        pVxlanInReplicaEntry =
            (tVxlanFsVxlanInReplicaEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) pVxlanInReplicaEntry, NULL);
    }

    pVxlanMCastEntry = (tVxlanFsVxlanMCastEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable);
    while (pVxlanMCastEntry != NULL)
    {
        if (pVxlanMCastEntry->i4VlanId == i4VlanId)
        {
            return VXLAN_SUCCESS;
        }
        pVxlanMCastEntry =
            (tVxlanFsVxlanMCastEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
             (tRBElem *) pVxlanMCastEntry, NULL);
    }
#ifdef EVPN_VXLAN_WANTED
    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
    while (pVxlanNveEntry != NULL)
    {
        if ((pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
             EVPN_CLI_ARP_SUPPRESS) && (pVxlanNveEntry->i4VlanId == i4VlanId))
        {
            return VXLAN_SUCCESS;
        }
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) pVxlanNveEntry, NULL);
    }
#endif
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortRemoveDynamicNveEntries                       *
 *  Description     : Deletes the Nve Entries                                *
 *  Input           : u4IfIndex - Nve Interface Index                        *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanPortRemoveDynamicNveEntries (UINT4 u4IfIndex, INT4 i4IfMainAdminStatus)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VxlanIfMsg.uVxlanMsg.u4IfIndex = u4IfIndex;
    VxlanIfMsg.i4Adminstatus = i4IfMainAdminStatus;
    VxlanIfMsg.u4MsgType = VXLAN_NVE_ADMIN_EVENT;
    VxlanEnqueMsg (&VxlanIfMsg);
    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortDelNveInterface                               *
 *  Description     : Called from CFA-VLAN task to post NVE interface        *
 *                    deletion indication to VXLAN module                    *
 *  Input           : u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanPortDelNveInterface (UINT4 u4IfIndex)
{
    tVxlanIfMsg         VxlanIfMsg;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveInterface: Entry\n"));
    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VxlanIfMsg.uVxlanMsg.u4IfIndex = u4IfIndex;
    VxlanIfMsg.u4MsgType = VXLAN_NVE_DEL_EVENT;
    VxlanEnqueMsg (&VxlanIfMsg);
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveInterface: Exit\n"));
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortOperDownIndication                            *
 *  Description     : Called from L2Iwf task to delete learnt MAC on         *
 *                     respective port                                       *
 *  Input           : u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanPortOperDownIndication (UINT4 u4IfIndex, UINT1 u1Status)
{
    tVxlanIfMsg         VxlanIfMsg;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveInterface: Entry\n"));
    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VxlanIfMsg.uVxlanMsg.u4IfIndex = u4IfIndex;
    if (u1Status == CFA_IF_DOWN)
    {
        VxlanIfMsg.u4MsgType = VXLAN_PORT_OPER_DOWN;
    }
    else if (u1Status == CFA_IF_AGEOUT)
    {
        VxlanIfMsg.u4MsgType = VXLAN_PORT_AGEOUT;
    }
    VxlanEnqueMsg (&VxlanIfMsg);
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortDelNveInterface: Exit\n"));
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortOperDownIndication                            *
 *  Description     : Handler routine for VXLAN_PORT_OPER_DOWN to delete     *
 *                    learnt MAC on respective port                          *
 *  Input           : u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */

VOID
VxlanDelPortFromL2Table (UINT4 u4IfIndex)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry = NULL;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;;
    tVxlanVniVlanPortEntry VxlanVniVlanGetPortEntry;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
    tVxlanVniVlanPortMacEntry *pNextVxlanVniVlanPortMacEntry = NULL;
    tVxlanVniVlanPortMacEntry VxlanVniVlanGetPortMacEntry;

    tVxlanHwInfo        VxlanHwInfo;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (&VxlanVniVlanGetPortEntry, 0, sizeof (tVxlanVniVlanPortEntry));
    MEMSET (&VxlanVniVlanGetPortMacEntry, 0,
            sizeof (tVxlanVniVlanPortMacEntry));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif

    pVxlanVniVlantEntry = (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    while (pVxlanVniVlantEntry != NULL)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = 0;

        VxlanVniVlanGetPortEntry.u4IfIndex = u4IfIndex;

        pVxlanVniVlanPortEntry =
            RBTreeGet (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                       (tRBElem *) & VxlanVniVlanGetPortEntry);
        if (pVxlanVniVlanPortEntry == NULL)
        {
            pVxlanVniVlantEntry =
                (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                                VxlanGlbMib.
                                                                FsVxlanVniVlanMapTable,
                                                                (tRBElem *)
                                                                pVxlanVniVlantEntry,
                                                                NULL);
            continue;
        }
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
            pVxlanVniVlanPortEntry->u4AccessPort;
        /* Retrieve port-MAC table */
        VxlanVniVlanGetPortMacEntry.u4IfIndex = u4IfIndex;

        pVxlanVniVlanPortMacEntry =
            RBTreeGetNext (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
                           (tRBElem *) & VxlanVniVlanGetPortMacEntry, NULL);

        while (pVxlanVniVlanPortMacEntry != NULL)
        {
            pNextVxlanVniVlanPortMacEntry =
                RBTreeGetNext (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
                               pVxlanVniVlanPortMacEntry, NULL);

            if (u4IfIndex != pVxlanVniVlanPortMacEntry->u4IfIndex)
            {
                pVxlanVniVlanPortMacEntry = pNextVxlanVniVlanPortMacEntry;
                continue;
            }

            MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac,
                    pVxlanVniVlanPortMacEntry->MacAddress, sizeof (tMacAddr));

            /* Retrieve Access port of Interface */

#ifdef NPAPI_WANTED
            NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                 NP_VXLAN_MOD,    /* Module ID */
                                 VXLAN_HW_CLEAR,    /* Function/OpCode */
                                 u4IfIndex,    /* IfIndex value if applicable */
                                 0,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            MEMCPY (pVxlanNpModInfo, &VxlanHwInfo, sizeof (tVxlanHwInfo));

            pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;
            if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
            {
                pVxlanVniVlanPortMacEntry = pNextVxlanVniVlanPortMacEntry;
                continue;
            }
#endif
            /* Remove entry from MAC-address table - control plane */
            VlanRemoveFdbEntry (0, (UINT2) pVxlanVniVlantEntry->MibObject.
                                i4FsVxlanVniVlanMapVlanId,
                                pVxlanVniVlanPortMacEntry->MacAddress);

            /* delete this node */
            RBTreeRem (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
                       pVxlanVniVlanPortMacEntry);
            MemReleaseMemBlock (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortMacEntry);
            pVxlanVniVlanPortMacEntry = pNextVxlanVniVlanPortMacEntry;
        }

        pVxlanVniVlantEntry =
            (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsVxlanVniVlanMapTable,
                                                            (tRBElem *)
                                                            pVxlanVniVlantEntry,
                                                            NULL);
    }
    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanL2TableFlushIndication                            *
 *  Description     : Handler routine for VXLAN_PORT_OPER_DOWN to delete     *
 *                    learnt MAC on respective port                          *
 *  Input           : u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanL2TableFlushIndication (VOID)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry = NULL;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;;
    tVxlanVniVlanPortEntry VxlanVniVlanGetPortEntry;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;
    tVxlanVniVlanPortMacEntry *pNextVxlanVniVlanPortMacEntry = NULL;
    tVxlanVniVlanPortMacEntry VxlanVniVlanGetPortMacEntry;

    tVxlanHwInfo        VxlanHwInfo;
    tCliHandle          CliHandle = 0;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
#endif

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (&VxlanVniVlanGetPortEntry, 0, sizeof (tVxlanVniVlanPortEntry));
    MEMSET (&VxlanVniVlanGetPortMacEntry, 0,
            sizeof (tVxlanVniVlanPortMacEntry));
#ifdef NPAPI_WANTED
    pVxlanNpModInfo = &(FsHwNp.VxlanNpModInfo);
#endif

    pVxlanVniVlantEntry = (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    while (pVxlanVniVlantEntry != NULL)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = 0;

        pVxlanVniVlanPortEntry =
            RBTreeGetFirst (pVxlanVniVlantEntry->VxlanVniVlanPortTable);
        if (pVxlanVniVlanPortEntry == NULL)
        {
            pVxlanVniVlantEntry =
                (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                                VxlanGlbMib.
                                                                FsVxlanVniVlanMapTable,
                                                                (tRBElem *)
                                                                pVxlanVniVlantEntry,
                                                                NULL);
            continue;
        }
        else
        {
            while (pVxlanVniVlanPortEntry != NULL)
            {
                VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
                    pVxlanVniVlanPortEntry->u4AccessPort;
                /* Retrieve port-MAC table */
                VxlanVniVlanGetPortMacEntry.u4IfIndex =
                    pVxlanVniVlanPortEntry->u4IfIndex;
                pVxlanVniVlanPortMacEntry =
                    RBTreeGetFirst (pVxlanVniVlantEntry->
                                    VxlanVniVlanPortMacTable);
                while (pVxlanVniVlanPortMacEntry != NULL)
                {
                    pNextVxlanVniVlanPortMacEntry =
                        RBTreeGetNext (pVxlanVniVlantEntry->
                                       VxlanVniVlanPortMacTable,
                                       pVxlanVniVlanPortMacEntry, NULL);

                    MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac,
                            pVxlanVniVlanPortMacEntry->MacAddress,
                            sizeof (tMacAddr));

                    /* Retrieve Access port of Interface */

#ifdef NPAPI_WANTED
                    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                         NP_VXLAN_MOD,    /* Module ID */
                                         VXLAN_HW_CLEAR,    /* Function/OpCode */
                                         pVxlanVniVlanPortEntry->u4IfIndex,    /* IfIndex value if applicable */
                                         0,    /* No. of Port Params */
                                         0);    /* No. of PortList Parms */
                    MEMCPY (pVxlanNpModInfo, &VxlanHwInfo,
                            sizeof (tVxlanHwInfo));

                    pVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;
                    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
                    {
                        pVxlanVniVlanPortMacEntry =
                            pNextVxlanVniVlanPortMacEntry;
                        continue;
                    }
#endif
                    /* delete this node */
                    RBTreeRem (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
                               pVxlanVniVlanPortMacEntry);
                    MemReleaseMemBlock (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                        (UINT1 *) pVxlanVniVlanPortMacEntry);
                    pVxlanVniVlanPortMacEntry = pNextVxlanVniVlanPortMacEntry;
                }
                pVxlanVniVlanPortEntry =
                    RBTreeGetNext (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                                   pVxlanVniVlanPortEntry, NULL);
            }
        }
        pVxlanVniVlantEntry =
            (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsVxlanVniVlanMapTable,
                                                            (tRBElem *)
                                                            pVxlanVniVlantEntry,
                                                            NULL);
    }

    VxlanRemoveVniNveEntries (CliHandle, NULL);
    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanPortRemoveDynamicNveEntries                       *
 *  Description     : Deletes the Nve Entries                                *
 *  Input           : u4IfIndex - Nve Interface Index                        *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */

VOID
VxlanHandleNveIntAdminStatus (UINT4 u4IfIndex, INT4 i4IfMainAdminStatus)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNextNveEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tMacAddr            zeroAddr;
    UINT2               u2VlanId = 0;
#ifdef EVPN_VXLAN_WANTED
    INT4                i4EvpnIndex = 0;
    tMacAddr            zeroMac;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanNextEcmpNveEntry = NULL;
    UINT1               au1VrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    UINT4               u4VrfId = 0;
    tEvpnRoute          EvpnRoute;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveDynamicNveEntries: Entry\n"));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#ifdef EVPN_VXLAN_WANTED
    MEMSET (zeroMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1VrfName, 0, VXLAN_CLI_MAX_VRF_NAME_LEN);
    MEMSET (&EvpnRoute, 0, sizeof (tEvpnRoute));
#endif

    if (i4IfMainAdminStatus == CFA_IF_DOWN)
    {
        /* Nve DB */
        /* Deletion of Non zero mac entries */
        VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry, NULL);
        while ((pVxlanNveEntry != NULL)
               && (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                   (INT4) u4IfIndex))
        {
            pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);
            if (MEMCMP (pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                        zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0)
            {
                pVxlanNveEntry = pVxlanNextNveEntry;
                continue;
            }

            if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                VXLAN_STRG_TYPE_VOL)
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
                    == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }

#ifdef EVPN_VXLAN_WANTED
                if (pVxlanNveEntry->b1IsIrbRoute == TRUE)
                {
                    EvpnRoute.u4L3VniNumber =
                        pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
                    MEMCPY (&(EvpnRoute.VxlanDestVmMac),
                            &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                            VXLAN_ETHERNET_ADDR_SIZE);
                    EvpnRoute.i4VxlanRemoteVtepAddressType =
                        pVxlanNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressType;
                    MEMCPY (&(EvpnRoute.au1VxlanDestAddress),
                            &(pVxlanNveEntry->au1VxlanIRBHostAddress),
                            VXLAN_IP4_ADDR_LEN);

                    if (EvpnRoute.i4VxlanRemoteVtepAddressType ==
                        VXLAN_IPV4_UNICAST)
                    {
                        MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                                &(pVxlanNveEntry->MibObject.
                                  au1FsVxlanNveRemoteVtepAddress),
                                VXLAN_IP4_ADDR_LEN);
                    }
                    else
                    {
                        MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                                &(pVxlanNveEntry->MibObject.
                                  au1FsVxlanNveRemoteVtepAddress),
                                VXLAN_IP6_ADDR_LEN);

                    }
                    VxlanUtilHandleSymmerticDelIrbRoute (&EvpnRoute,
                                                         pVxlanNveEntry->
                                                         MibObject.
                                                         i4FsVxlanNveIfIndex);
                    pVxlanNveEntry->b1IsIrbRoute = FALSE;
                }
#endif
                if (RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanNveEntry) != NULL)
                {
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanNveEntry);
                }
            }
            else
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry,
                                              VXLAN_HW_DEL) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
            }
            pVxlanNveEntry = pVxlanNextNveEntry;
        }
        VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry, NULL);
        /* Deletion of Zero mac entries */
        while ((pVxlanNveEntry != NULL) &&
               ((pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                 (INT4) u4IfIndex) ||
                (pVxlanNveEntry->u4OrgNveIfIndex == u4IfIndex)))
        {
            pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);
            if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                VXLAN_STRG_TYPE_VOL)
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
                if (RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanNveEntry) != NULL)
                {
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanNveEntry);
                }
            }
            else
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
            }
            pVxlanNveEntry = pVxlanNextNveEntry;
        }
#ifdef EVPN_VXLAN_WANTED
        /* EcmpNve DB */
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4IfIndex;
        pVxlanEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                            FsVxlanEcmpNveTable,
                                            (tRBElem *) & VxlanEcmpNveEntry,
                                            NULL);
        while ((pVxlanEcmpNveEntry != NULL)
               && (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
                   (INT4) u4IfIndex))
        {
            pVxlanNextEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanEcmpNveTable,
                                                    pVxlanEcmpNveEntry, NULL);
            if (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                        zeroMac, sizeof (tMacAddr)) != 0)
            {
                if (VxlanHwUpdateEcmpNveDatabase (pVxlanEcmpNveEntry,
                                                  VXLAN_HW_DEL) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           pVxlanEcmpNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                    (UINT1 *) pVxlanEcmpNveEntry);
            }
            pVxlanEcmpNveEntry = pVxlanNextEcmpNveEntry;
        }
#endif
        /* Multicast DB */
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;
        pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                          FsVxlanMCastTable,
                                          (tRBElem *) & VxlanMCastEntry, NULL);
        while ((pVxlanMCastEntry != NULL) &&
               (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                (INT4) u4IfIndex))
        {
            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Multicast entry in h/w\n"));
            }
            pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanMCastTable,
                                              pVxlanMCastEntry, NULL);
        }
        /* Ingress Replica DB */
        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            (INT4) u4IfIndex;
        pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanInReplicaTable,
                                              (tRBElem *) & VxlanInReplicaEntry,
                                              NULL);
        while ((pVxlanInReplicaEntry != NULL)
               && (pVxlanInReplicaEntry->MibObject.
                   i4FsVxlanInReplicaNveIfIndex == (INT4) u4IfIndex))
        {
            if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry,
                                                VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Ingress Replica entry in h/w\n"));
            }
            pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanInReplicaTable,
                                                  pVxlanInReplicaEntry, NULL);
        }
    }
    if (i4IfMainAdminStatus == CFA_IF_UP)
    {
        /* Nve DB */
        VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry, NULL);
        while ((pVxlanNveEntry != NULL)
               && (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                   (INT4) u4IfIndex))
        {
#ifdef EVPN_VXLAN_WANTED
            if ((pVxlanNveEntry->MibObject.
                 i4FsVxlanNveRemoteVtepAddressType == 0) &&
                ((EvpnUtilGetEviFromVni ((pVxlanNveEntry->MibObject.
                                          u4FsVxlanNveVniNumber), &i4EvpnIndex)
                  != VXLAN_FAILURE) || (EvpnUtilGetVrfFromVni
                                        (pVxlanNveEntry->
                                         MibObject.u4FsVxlanNveVniNumber,
                                         &u4VrfId) != VXLAN_FAILURE)))
            {
                if (VxlanUtilGetVlanFromVni (pVxlanNveEntry->MibObject.
                                             u4FsVxlanNveVniNumber,
                                             &u2VlanId) == VXLAN_SUCCESS)
                {
                    if (VxlanUtilGetVrfFromVlan (u2VlanId, (UINT1 *) au1VrfName)
                        == VXLAN_SUCCESS)
                    {
                        if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
                        {
                            VXLAN_TRC ((VXLAN_EVPN_TRC,
                                        " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
                        }
                    }

                }
                /* Send Notification to BGP to get MAC routes, when member VNI
                 * is added to an NVE interface */
                if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                {
                    EVPN_NOTIFY_GET_MAC_CB (u4VrfId, pVxlanNveEntry->MibObject.
                                            u4FsVxlanNveVniNumber, zeroAddr,
                                            EVPN_BGP4_MAC_ADD);
                }
            }
#endif
            VxlanUtilGetVlanFromVni (pVxlanNveEntry->MibObject.
                                     u4FsVxlanNveVniNumber, &u2VlanId);
            if (u2VlanId == 0)
            {
                pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);
                continue;
            }
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_ADD) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Nve entry in h/w\n"));
            }
            u2VlanId = 0;
            pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                            FsVxlanNveTable,
                                            pVxlanNveEntry, NULL);
        }
        u2VlanId = 0;
        /* Multicast DB */
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;
        pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                          FsVxlanMCastTable,
                                          (tRBElem *) & VxlanMCastEntry, NULL);
        while ((pVxlanMCastEntry != NULL) &&
               (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                (INT4) u4IfIndex))
        {
            VxlanUtilGetVlanFromVni (pVxlanMCastEntry->MibObject.
                                     u4FsVxlanMCastVniNumber, &u2VlanId);
            if (u2VlanId == 0)
            {
                pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanMCastTable,
                                                  pVxlanMCastEntry, NULL);
                continue;
            }
#ifdef EVPN_VXLAN_WANTED
            if (EvpnUtilGetEviFromVni ((pVxlanMCastEntry->MibObject.
                                        u4FsVxlanMCastVniNumber), &i4EvpnIndex)
                != VXLAN_FAILURE)
            {
                /* Send Notification to BGP to get MAC routes, when member VNI
                 * is added to an NVE interface */
                if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                {
                    EVPN_NOTIFY_GET_MAC_CB (0, pVxlanMCastEntry->MibObject.
                                            u4FsVxlanMCastVniNumber, zeroAddr,
                                            EVPN_BGP4_MAC_ADD);
                }
            }
#endif

            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_ADD) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Multicast entry in h/w\n"));
            }
            u2VlanId = 0;
            pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanMCastTable,
                                              pVxlanMCastEntry, NULL);
        }
        u2VlanId = 0;
        /* Ingress Replica DB */
        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            (INT4) u4IfIndex;
        pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanInReplicaTable,
                                              (tRBElem *) & VxlanInReplicaEntry,
                                              NULL);
        while ((pVxlanInReplicaEntry != NULL) &&
               (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
                (INT4) u4IfIndex))
        {
            VxlanUtilGetVlanFromVni (pVxlanInReplicaEntry->MibObject.
                                     u4FsVxlanInReplicaVniNumber, &u2VlanId);
            if (u2VlanId == 0)
            {
                pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                      FsVxlanInReplicaTable,
                                                      pVxlanInReplicaEntry,
                                                      NULL);
                continue;
            }
#ifdef EVPN_VXLAN_WANTED
            if (EvpnUtilGetEviFromVni ((pVxlanInReplicaEntry->MibObject.
                                        u4FsVxlanInReplicaVniNumber),
                                       &i4EvpnIndex) != VXLAN_FAILURE)
            {
                /* Send Notification to BGP to get MAC routes, when member VNI
                 * is added to an NVE interface */
                if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                {
                    EVPN_NOTIFY_GET_MAC_CB (0, pVxlanInReplicaEntry->MibObject.
                                            u4FsVxlanInReplicaVniNumber,
                                            zeroAddr, EVPN_BGP4_MAC_ADD);
                }
            }
#endif

            if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry,
                                                VXLAN_HW_ADD) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Ingress Replica entry in h/w\n"));
            }
            u2VlanId = 0;
            pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanInReplicaTable,
                                                  pVxlanInReplicaEntry, NULL);
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveDynamicNveEntries: Exit\n"));
    return;
}

#ifdef EVPN_VXLAN_WANTED
/* ************************************************************************* *
 *  Function Name   : EvpnVxlanDelRouteInDp                                  *
 *  Description     : Called from BGP to remove MAC routes in data plane     *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanDelRouteInDp (tEvpnRoute * pEvpnRoute)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddRouteInDp Entry\n"));

    MEMCPY (&(VxlanIfMsg.uVxlanMsg.EvpnRoute), pEvpnRoute, sizeof (tEvpnRoute));
    VxlanIfMsg.u4MsgType = VXLAN_EVPN_MAC_DEL_EVENT;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "VxlanEnqueMsg returns failure\n"));
        return VXLAN_FAILURE;
    }
    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddRouteInDp Exit\n"));
    return VXLAN_SUCCESS;

}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanDelNvedatabase                                *
 *  Description     : Called from BGP to remove MAC routes in data plane     *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */
VOID
EvpnVxlanDelNvedatabase (tEvpnRoute * pEvpnRoute)
{

    UINT4               u4NveIndex = 0;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pTempVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pNextVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pNextActiveEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanZeroEcmpNveEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnVxlanEviVniMapEntry = NULL;
    tMacAddr            zeroMac;
    BOOL1               bIsActiveNextEntryPresent = FALSE;
#ifdef ARP_WANTED
    tARP_DATA           ArpData;
#endif
    UINT4               u4IfIndex = 0;

    MEMSET (zeroMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanDelNvedatabase Entry\n"));
    if (pEvpnRoute->u1RouteType == EVPN_MAC_ROUTE)
    {
        if (pEvpnRoute->u4L3VniNumber != 0)
        {
            /* The incoming route is symmetric IRB case */
            pEvpnRoute->u4VxlanVniNumber = pEvpnRoute->u4L3VniNumber;
        }

        /* Get the NVE if index from the VNI number */
        if ((VxlanUtilGetNveIndexFromNveTable (pEvpnRoute->u4VxlanVniNumber,
                                               &u4NveIndex)) != NULL)
        {
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                         pEvpnRoute->VxlanDestVmMac,
                                         u4NveIndex);
            if ((pVxlanNveEntry != NULL)
                &&
                (MEMCMP
                 (&pEvpnRoute->au1VxlanRemoteVtepAddress,
                  &pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
                  ((pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                    VXLAN_IPV4_UNICAST) ? VXLAN_IP4_ADDR_LEN :
                   VXLAN_IP6_ADDR_LEN)) == 0))
            {
                pVxlanNveEntry->u2VrfFlag &= ~(pEvpnRoute->u2VrfFlag);
                if (pVxlanNveEntry->u2VrfFlag == 0)
                {
                    if (pVxlanNveEntry->bIngOrMcastZeroEntryPresent != TRUE)
                    {
                        pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
                            VXLAN_STRG_TYPE_NON_VOL;
                    }
                }
                if (VXLAN_NVE_EVPN_MAC ==
                    pVxlanNveEntry->i4FsVxlanNveEvpnMacType)
                {
                    if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
                        == VXLAN_FAILURE)
                    {
                        VXLAN_TRC ((VXLAN_FAIL_TRC,
                                    "Failed to delete Nve entry from h/w\n"));

                    }
                    if (pEvpnRoute->u4L3VniNumber != 0)
                    {
                        /* Indication is given to ARP module to Del 
                           an ARP entry, so VXLAN Lock is not taken */
                        VxlanMainTaskUnLock ();
                        /* The incoming route is symmetric IRB case */
                        VxlanUtilHandleSymmerticDelIrbRoute (pEvpnRoute,
                                                             u4NveIndex);
                        VxlanMainTaskLock ();
                    }
                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanNveEntry);
                }
            }
        }
        /* Get the NVE if index from the VNI number */
        if ((VxlanUtilGetNveIndexFromEcmpNveTable (pEvpnRoute->u4VxlanVniNumber,
                                                   &u4NveIndex)) != NULL)
        {
            pTempVxlanEcmpNveEntry = VxlanUtilGetEcmpNveDatabase
                (pEvpnRoute->u4VxlanVniNumber, pEvpnRoute->VxlanDestVmMac,
                 u4NveIndex,
                 pEvpnRoute->i4VxlanRemoteVtepAddressType,
                 pEvpnRoute->au1VxlanRemoteVtepAddress);
            if (pTempVxlanEcmpNveEntry != NULL)
            {
                /* unset the Mac Learnt Flag */
                pTempVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt =
                    VXLAN_FALSE;

                /* check for Mac learnt Flag set for any other peer for this MAC */
                if (EvpnVxlanIsMacLearntFlagSet (pEvpnRoute->u4VxlanVniNumber,
                                                 pEvpnRoute->VxlanDestVmMac,
                                                 u4NveIndex) == VXLAN_FAILURE)
                {
                    EvpnVxlanDeleteMacEntries (pEvpnRoute->u4VxlanVniNumber,
                                               pEvpnRoute->VxlanDestVmMac,
                                               u4NveIndex);
                    pVxlanNveEntry = NULL;
                    pVxlanNveEntry =
                        VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                                 pEvpnRoute->VxlanDestVmMac,
                                                 u4NveIndex);

                    if (pVxlanNveEntry != NULL)
                    {
                        pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
                            VXLAN_STRG_TYPE_NON_VOL;

                        if (VxlanHwUpdateNveDatabase
                            (pVxlanNveEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
                        {
                            VXLAN_TRC ((VXLAN_FAIL_TRC,
                                        "Failed to delete Nve entry from h/w\n"));

                        }
                        pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType =
                            VXLAN_STRG_TYPE_VOL;
                    }
                    else if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                    {
                        EVPN_NOTIFY_GET_MAC_CB (0, pEvpnRoute->u4VxlanVniNumber,
                                                pEvpnRoute->VxlanDestVmMac,
                                                EVPN_BGP4_MAC_ADD);
                    }
                }

            }
        }
        if (pVxlanNveEntry != NULL)
        {
            u4IfIndex =
                CfaGetVlanInterfaceIndex ((tVlanIfaceVlanId) pVxlanNveEntry->
                                          i4VlanId);
            if (u4IfIndex == CFA_INVALID_INDEX)
            {
                VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                 "Invalid L3 VLAN interface Index for the VLAN %d\n",
                                 pVxlanNveEntry->i4VlanId));
            }

            if (EVPN_CLI_ARP_SUPPRESS ==
                pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp)
            {
                if (pEvpnRoute->u1VxlanDestAddressLen == VXLAN_IP4_ADDR_LEN *
                    VXLAN_ONE_BYTE_BITS)
                {
#ifdef ARP_WANTED
                    MEMSET (&ArpData, 0, sizeof (tARP_DATA));
                    MEMCPY (&ArpData.u4IpAddr, pEvpnRoute->au1VxlanDestAddress,
                            sizeof (UINT4));
                    /*ArpData.u4IpAddr = OSIX_HTONL (ArpData.u4IpAddr); */
                    ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
                    ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
                    ArpData.i1Hwalen = MAC_ADDR_LEN;
                    ArpData.i1State = ARP_PROXY_EVPN;
                    MEMCPY (ArpData.i1Hw_addr,
                            (INT1 *) pEvpnRoute->VxlanDestVmMac,
                            ArpData.i1Hwalen);
                    ArpData.u1RowStatus = DESTROY;
                    VxlanMainTaskUnLock ();
                    ArpDeleteEvpnArpCache (ArpData);
                    VxlanMainTaskLock ();
#endif
                }
            }
        }
        else if (pEvpnRoute->u4L3VniNumber != 0)
        {
            /* Indication is given to ARP module to Del an ARP entry, so VXLAN Lock is not
               taken */
            VxlanMainTaskUnLock ();
            /* The incoming route is symmetric IRB case */
            VxlanUtilHandleSymmerticDelIrbRoute (pEvpnRoute, u4NveIndex);
            VxlanMainTaskLock ();

        }
    }
    else if (pEvpnRoute->u1RouteType == EVPN_AD_ROUTE)
    {
        if ((VxlanUtilGetNveIndexFromEcmpNveTable (pEvpnRoute->u4VxlanVniNumber,
                                                   &u4NveIndex)) != NULL)
        {
            /* Retrive the EviVni Map Entry */
            EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
                pEvpnRoute->u4VxlanVniNumber;
            if (VXLAN_FAILURE ==
                EvpnUtilGetEviFromVni (pEvpnRoute->u4VxlanVniNumber,
                                       &(EvpnVxlanEviVniMapEntry.MibObject.
                                         i4FsEvpnVxlanEviVniMapEviIndex)))
            {
                return;
            }
            pEvpnVxlanEviVniMapEntry =
                RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           (tRBElem *) & EvpnVxlanEviVniMapEntry);
            if (pEvpnVxlanEviVniMapEntry == NULL)
            {
                return;
            }

            /* Delete the respective Mac entries */
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
                (INT4) u4NveIndex;
            VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
                pEvpnRoute->u4VxlanVniNumber;
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
                pEvpnRoute->i4VxlanRemoteVtepAddressType;
            MEMCPY (VxlanEcmpNveEntry.MibObject.
                    au1FsVxlanEcmpNveRemoteVtepAddress,
                    pEvpnRoute->au1VxlanRemoteVtepAddress,
                    ((pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                      VXLAN_IPV4_UNICAST) ? VXLAN_IP4_ADDR_LEN :
                     VXLAN_IP6_ADDR_LEN));
            pVxlanEcmpNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                               (tRBElem *) & VxlanEcmpNveEntry, NULL);
            while (pVxlanEcmpNveEntry != NULL)
            {
                pNextVxlanEcmpNveEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanEcmpNveTable,
                                   (tRBElem *) pVxlanEcmpNveEntry, NULL);

                if (MEMCMP (pVxlanEcmpNveEntry->MibObject.
                            au1FsVxlanEcmpNveRemoteVtepAddress,
                            pEvpnRoute->au1VxlanRemoteVtepAddress,
                            ((pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                              VXLAN_IPV4_UNICAST) ? VXLAN_IP4_ADDR_LEN
                             : VXLAN_IP6_ADDR_LEN)) == 0)
                {
                    if (MEMCMP
                        (pVxlanEcmpNveEntry->MibObject.
                         au1FsVxlanEcmpMHEviVniESI,
                         pEvpnRoute->au1FsEvpnMHEviVniESI,
                         EVPN_MAX_ESI_LEN) == 0)
                    {
                        /* Delete the Entry */
                        if (VxlanHwUpdateEcmpNveDatabase (pVxlanEcmpNveEntry,
                                                          VXLAN_HW_DEL) ==
                            VXLAN_FAILURE)
                        {
                            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                             "RB Tree deletion failed\n"));
                        }
                        /* if the current entry is Active,
                         * make the next entry as Active before deleting this Entry*/
                        if (pEvpnVxlanEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniLoadBalance ==
                            EVPN_CLI_LB_DISABLE)
                        {
                            if (pVxlanEcmpNveEntry->MibObject.
                                bFsVxlanEcmpActive == VXLAN_TRUE)
                            {
                                pVxlanZeroEcmpNveEntry =
                                    RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanEcmpNveTable);

                                while ((pVxlanZeroEcmpNveEntry != NULL) &&
                                       (MEMCMP
                                        (pVxlanZeroEcmpNveEntry->MibObject.
                                         FsVxlanEcmpNveDestVmMac, zeroMac,
                                         sizeof (tMacAddr)) == 0))

                                {
                                    pNextActiveEcmpNveEntry = NULL;
                                    pNextActiveEcmpNveEntry =
                                        VxlanUtilGetEcmpNveDatabase
                                        (pEvpnRoute->u4VxlanVniNumber,
                                         pVxlanEcmpNveEntry->MibObject.
                                         FsVxlanEcmpNveDestVmMac, u4NveIndex,
                                         pVxlanZeroEcmpNveEntry->MibObject.
                                         i4FsVxlanEcmpNveRemoteVtepAddressType,
                                         pVxlanZeroEcmpNveEntry->MibObject.
                                         au1FsVxlanEcmpNveRemoteVtepAddress);

                                    if ((pNextActiveEcmpNveEntry != NULL) &&
                                        (pNextActiveEcmpNveEntry !=
                                         pVxlanEcmpNveEntry))
                                    {
                                        pNextActiveEcmpNveEntry->MibObject.
                                            bFsVxlanEcmpActive = VXLAN_TRUE;

                                        bIsActiveNextEntryPresent = TRUE;
                                        /* Add the Active entry in to h/w */
                                        if (VxlanHwUpdateEcmpNveDatabase
                                            (pNextActiveEcmpNveEntry,
                                             VXLAN_HW_ADD) == VXLAN_FAILURE)
                                        {
                                            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                                             "Failed to Add Ecmp Nve entry in h/w\n"));
                                        }
                                        break;
                                    }

                                    pVxlanZeroEcmpNveEntry = RBTreeGetNext
                                        (gVxlanGlobals.VxlanGlbMib.
                                         FsVxlanEcmpNveTable,
                                         (tRBElem *) pVxlanZeroEcmpNveEntry,
                                         NULL);
                                }
                                if (bIsActiveNextEntryPresent == FALSE)
                                {
                                    pVxlanNveEntry = NULL;
                                    pVxlanNveEntry =
                                        VxlanUtilGetNveDatabase (pEvpnRoute->
                                                                 u4VxlanVniNumber,
                                                                 pVxlanEcmpNveEntry->
                                                                 MibObject.
                                                                 FsVxlanEcmpNveDestVmMac,
                                                                 u4NveIndex);
                                    if (pVxlanNveEntry != NULL)
                                    {
                                        pVxlanNveEntry->MibObject.
                                            i4FsVxlanNveStorageType =
                                            VXLAN_STRG_TYPE_NON_VOL;
                                        if (VxlanHwUpdateNveDatabase
                                            (pVxlanNveEntry,
                                             VXLAN_HW_ADD) == VXLAN_FAILURE)
                                        {
                                            VXLAN_TRC ((VXLAN_FAIL_TRC,
                                                        "Failed to delete Nve entry from h/w\n"));

                                        }
                                        pVxlanNveEntry->MibObject.
                                            i4FsVxlanNveStorageType =
                                            VXLAN_STRG_TYPE_VOL;
                                    }
                                    else if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                                    {
                                        EVPN_NOTIFY_GET_MAC_CB
                                            (0, pEvpnRoute->u4VxlanVniNumber,
                                             pVxlanEcmpNveEntry->MibObject.
                                             FsVxlanEcmpNveDestVmMac,
                                             EVPN_BGP4_MAC_ADD);
                                    }
                                }

                            }
                        }
                        RBTreeRem (gVxlanGlobals.VxlanGlbMib.
                                   FsVxlanEcmpNveTable, pVxlanEcmpNveEntry);
                        MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                            (UINT1 *) pVxlanEcmpNveEntry);

                    }
                }
                pVxlanEcmpNveEntry = pNextVxlanEcmpNveEntry;

            }
            pVxlanEcmpNveEntry = VxlanUtilGetEcmpNveDatabase
                (pEvpnRoute->u4VxlanVniNumber, zeroMac,
                 u4NveIndex,
                 pEvpnRoute->i4VxlanRemoteVtepAddressType,
                 pEvpnRoute->au1VxlanRemoteVtepAddress);
            if (pVxlanEcmpNveEntry != NULL)
            {
                /* Delete Ecmp Entry from h/w */
                if (VxlanHwUpdateEcmpNveDatabase (pVxlanEcmpNveEntry,
                                                  VXLAN_HW_DEL) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "Failed to delete Ecmp Nve entry from h/w\n"));
                }
                /* if the current entry is Active,
                 * make the next entry as Active before deleting this Entry*/
                if (pEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance == EVPN_CLI_LB_DISABLE)
                {
                    if (pVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpActive ==
                        VXLAN_TRUE)
                    {
                        pNextActiveEcmpNveEntry = RBTreeGetFirst
                            (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable);
                        while ((pNextActiveEcmpNveEntry != NULL) &&
                               (MEMCMP
                                (pNextActiveEcmpNveEntry->MibObject.
                                 FsVxlanEcmpNveDestVmMac, zeroMac,
                                 sizeof (tMacAddr)) == 0))
                        {
                            if ((pNextActiveEcmpNveEntry != pVxlanEcmpNveEntry)
                                &&
                                (MEMCMP
                                 (pNextActiveEcmpNveEntry->MibObject.
                                  au1FsVxlanEcmpMHEviVniESI,
                                  pVxlanEcmpNveEntry->MibObject.
                                  au1FsVxlanEcmpMHEviVniESI,
                                  EVPN_MAX_ESI_LEN) == 0))
                            {
                                pNextActiveEcmpNveEntry->MibObject.
                                    bFsVxlanEcmpActive = VXLAN_TRUE;
                                break;
                            }

                            pNextActiveEcmpNveEntry = RBTreeGetNext
                                (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                                 (tRBElem *) pNextActiveEcmpNveEntry, NULL);
                        }
                    }

                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           pVxlanEcmpNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                    (UINT1 *) pVxlanEcmpNveEntry);
                pVxlanEcmpNveEntry = NULL;
                /* Decrement the no.of paths for this Esi by 1 */
                pEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniMapTotalPaths--;

            }

        }

    }
    else if (pEvpnRoute->u1RouteType == EVPN_ETH_SEGEMENT_ROUTE)
    {
        VxlanUtilDelESInfoInMHPeer (pEvpnRoute->i4VxlanRemoteVtepAddressType,
                                    pEvpnRoute->au1VxlanDestAddress,
                                    pEvpnRoute->au1FsEvpnMHEviVniESI);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanDelNvedatabase Exit\n"));

    return;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanAddRouteInDp                                  *
 *  Description     : Called from BGP to program MAC routes in data plane    *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanAddRouteInDp (tEvpnRoute * pEvpnRoute)
{
    tVxlanIfMsg         VxlanIfMsg;
    INT4                i4FsEvpnVxlanEnable = 0;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddRouteInDp Entry\n"));

    /* Check whether EVPN is enabled */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    MEMCPY (&(VxlanIfMsg.uVxlanMsg.EvpnRoute), pEvpnRoute, sizeof (tEvpnRoute));
    VxlanIfMsg.u4MsgType = VXLAN_EVPN_MAC_ADD_EVENT;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "VxlanEnqueMsg returns failure\n"));
        return VXLAN_FAILURE;
    }
    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddRouteInDp Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanAddNvedatabase                                *
 *  Description     : Called from BGP to program MAC routes in data plane    *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
VOID
EvpnVxlanAddNvedatabase (tEvpnRoute * pEvpnRoute)
{
    UINT4               u4NveIndex = 0;
    INT4                i4VlanId = 0;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanAddNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pTmpVxlanNveEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT4               u4IfIndex = 0;
    BOOL1               b1IngOrMcastEntryPresent = FALSE;
    tVxlanFsVxlanEcmpNveEntry *pVxlanTmpEcmpNveEntry = NULL;
#ifdef NPAPI_WANTED
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
#endif
#ifdef ARP_WANTED
    tARP_DATA           ArpData;
#endif
    tMacAddr            zeroAddr;

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddNvedatabase Entry\n"));
    if (pEvpnRoute == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "pEvpnRoute is NULL\n"));
        return;
    }
    if (pEvpnRoute->u1RouteType == EVPN_MAC_ROUTE)
    {
        if (pEvpnRoute->u4L3VniNumber != 0)
        {
            /* The incoming route is symmetric IRB case */
            pEvpnRoute->u4VxlanVniNumber = pEvpnRoute->u4L3VniNumber;
        }
        /* Get the NVE if index from the VNI number */
        if ((VxlanUtilGetNveIndexFromNveTable (pEvpnRoute->u4VxlanVniNumber,
                                               &u4NveIndex)) == NULL)
        {
            /* If NVE database is not mapped with the VNI number
             * Get the Ingress Replica database from the VNI number */
            if ((VxlanUtilGetNveIndexFromInReplicaTable (pEvpnRoute->
                                                         u4VxlanVniNumber,
                                                         &u4NveIndex)) == NULL)
            {
                /* If Ingress Replica database is not mapped with the VNI number
                 * Get the MCAST database from the VNI number */
                if ((VxlanUtilGetNveIndexFromMcastTable (pEvpnRoute->
                                                         u4VxlanVniNumber,
                                                         &u4NveIndex)) == NULL)
                {
                    /* If MCAST database is also not mapped, dropped the VXLAN packet */
                    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                     "Nve index is not mapped for the VNI - %d"
                                     "in NVE, In-Replica and Multicast database\n",
                                     pEvpnRoute->u4VxlanVniNumber));
                    return;
                }
                else
                {
                    b1IngOrMcastEntryPresent = TRUE;
                }
            }
            else
            {
                b1IngOrMcastEntryPresent = TRUE;
            }
        }
        else
        {
            /* Check whether NVE entry is available for the source mac address
             * If it is available, no need to add NVE entry for source mac address */
            pVxlanNveEntry =
                VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                         pEvpnRoute->VxlanDestVmMac,
                                         u4NveIndex);

        }

        /* Check whether VLAN - VNI mapping table is available for the VNI */

        VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
            pEvpnRoute->u4VxlanVniNumber;

        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) & VxlanVniVlanMapEntry, NULL);

        while (pVxlanVniVlanMapEntry != NULL)
        {
            if (pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber
                == pEvpnRoute->u4VxlanVniNumber)
            {
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 " Mapping found for VNI id - %d,"
                                 "VLAN id - %d in VLAN VNI mapping table\n",
                                 pEvpnRoute->u4VxlanVniNumber,
                                 pVxlanVniVlanMapEntry->MibObject.
                                 i4FsVxlanVniVlanMapVlanId));

                i4VlanId =
                    pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
                break;
            }

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanVniVlanMapEntry, NULL);
        }

        if (pVxlanVniVlanMapEntry == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             " No vlan is mapped with the VNI\n"));
            return;
        }

        CliMacToStr (pEvpnRoute->VxlanDestVmMac, au1MacStr);
        au1MacStr[17] = '\0';
        if (EvpnVxlanIsEcmpRouteUpdate (pEvpnRoute, i4VlanId) == VXLAN_FAILURE)
        {
            if (pVxlanNveEntry == NULL)
            {

                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "NVE database is not found for"
                                 "VNI id - %d, Destination VM Mac address - %s, "
                                 "NVE if index - %d\n",
                                 pEvpnRoute->u4VxlanVniNumber, au1MacStr,
                                 u4NveIndex));

                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "Entry has to be added in NVE database\n"));

                pVxlanAddNveEntry =
                    (tVxlanFsVxlanNveEntry *)
                    MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);

                if (pVxlanAddNveEntry == NULL)
                {
                    VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                                     "Memory allocation failed for the"
                                     "new NVE entry\n"));
                    return;
                }

                VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                                 "Memory allocation succeed for the"
                                 "new NVE entry\n"));

                /* Fill the source VTEP address */
                VxlanInitializeMibFsVxlanNveTable (pVxlanAddNveEntry,
                                                   (INT4) u4NveIndex);
                /* Fill the Vlan-Id */
                pVxlanAddNveEntry->i4VlanId = i4VlanId;

                /* Fill the NVE if index */
                pVxlanAddNveEntry->MibObject.i4FsVxlanNveIfIndex =
                    (INT4) u4NveIndex;

                /* Fill the VNI number */
                pVxlanAddNveEntry->MibObject.u4FsVxlanNveVniNumber =
                    pEvpnRoute->u4VxlanVniNumber;

                /* Fill the destination VM mac address */
                MEMCPY (&(pVxlanAddNveEntry->MibObject.FsVxlanNveDestVmMac),
                        &(pEvpnRoute->VxlanDestVmMac),
                        VXLAN_ETHERNET_ADDR_SIZE);

                pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
                    = pEvpnRoute->i4VxlanRemoteVtepAddressType;

                /* Fill the Remote VTEP IP address */
                if (pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                    VXLAN_IPV4_UNICAST)
                {
                    pVxlanAddNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressLen = VXLAN_IP4_ADDR_LEN;
                    MEMCPY (&
                            (pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress),
                            &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                            VXLAN_IP4_ADDR_LEN);
                }
                else
                {
                    pVxlanAddNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressLen = VXLAN_IP6_ADDR_LEN;
                    MEMCPY (&
                            (pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress),
                            &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                            VXLAN_IP6_ADDR_LEN);
                }

                /* Fill the storage type as volatile since the entry is learnt dynamically */
                pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType =
                    VXLAN_STRG_TYPE_NON_VOL;
                pVxlanAddNveEntry->i4FsVxlanNveEvpnMacType = VXLAN_NVE_EVPN_MAC;

                pVxlanAddNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType
                    = pEvpnRoute->i4VxlanRemoteVtepAddressType;

                /* Fill the Remote VTEP IP address */
                if (pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                    VXLAN_IPV4_UNICAST)
                {
                    pVxlanAddNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressLen = VXLAN_IP4_ADDR_LEN;
                    MEMCPY (&
                            (pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress),
                            &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                            VXLAN_IP4_ADDR_LEN);
                }
                else
                {
                    pVxlanAddNveEntry->MibObject.
                        i4FsVxlanNveRemoteVtepAddressLen = VXLAN_IP6_ADDR_LEN;
                    MEMCPY (&
                            (pVxlanAddNveEntry->MibObject.
                             au1FsVxlanNveRemoteVtepAddress),
                            &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                            VXLAN_IP6_ADDR_LEN);
                }

                pTmpVxlanNveEntry =
                    VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                             zeroAddr, u4NveIndex);
                if (pTmpVxlanNveEntry != NULL)
                {
                    pVxlanAddNveEntry->MibObject.i4FsVxlanSuppressArp =
                        pTmpVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;
                }
                /* Fill the Vrf Flag */
                pVxlanAddNveEntry->u2VrfFlag = pEvpnRoute->u2VrfFlag;

                if (RBTreeAdd
                    (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                     (tRBElem *) pVxlanAddNveEntry) != RB_SUCCESS)
                {
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "RB Tree addition failed\n"));
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanAddNveEntry);
                    return;
                }
#ifdef NPAPI_WANTED
                if (b1IngOrMcastEntryPresent == TRUE)
                {
                    for (u4IfIndex = 0; u4IfIndex < VXLAN_HW_REPLICA_LIST;
                         u4IfIndex++)
                    {
                        pTmpVxlanNveEntry = VxlanUtilGetNveDatabase
                            (pEvpnRoute->u4VxlanVniNumber, zeroAddr,
                             u4NveIndex);
                        if (pTmpVxlanNveEntry != NULL)
                        {
                            if (MEMCMP (&(pTmpVxlanNveEntry->MibObject.
                                          au1FsVxlanNveRemoteVtepAddress),
                                        &(pEvpnRoute->
                                          au1VxlanRemoteVtepAddress),
                                        VXLAN_IP4_ADDR_LEN) == 0)
                            {
                                pVxlanAddNveEntry->MibObject.
                                    i4FsVxlanNveStorageType =
                                    VXLAN_STRG_TYPE_VOL;
                                pVxlanAddNveEntry->bIngOrMcastZeroEntryPresent =
                                    TRUE;
                            }
                            u4NveIndex = u4NveIndex + SYS_DEF_MAX_NVE_IFACES;
                        }
                    }
                }
                /* Check whether MAC is already programmed in ECMP table */
                pVxlanEcmpNveEntry = RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                     FsVxlanEcmpNveTable);

                while ((pVxlanEcmpNveEntry != NULL) &&
                       (MEMCMP
                        (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                         zeroAddr, sizeof (tMacAddr)) == 0))

                {
                    pVxlanTmpEcmpNveEntry = NULL;
                    pVxlanTmpEcmpNveEntry = VxlanUtilGetEcmpNveDatabase
                        (pEvpnRoute->u4VxlanVniNumber,
                         pEvpnRoute->VxlanDestVmMac, u4NveIndex,
                         pVxlanEcmpNveEntry->MibObject.
                         i4FsVxlanEcmpNveRemoteVtepAddressType,
                         pVxlanEcmpNveEntry->MibObject.
                         au1FsVxlanEcmpNveRemoteVtepAddress);

                    if ((pVxlanTmpEcmpNveEntry != NULL) &&
                        (pVxlanTmpEcmpNveEntry->bHwSet == TRUE))
                    {
                        break;
                    }
                    pVxlanEcmpNveEntry =
                        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                       FsVxlanEcmpNveTable,
                                       (tRBElem *) pVxlanEcmpNveEntry, NULL);

                }

                u4IfIndex = 0;
#endif
                VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                          &(pVxlanAddNveEntry->MibObject.
                                            NveDbNode));
                VxlanRedSyncDynInfo ();

                if ((pVxlanTmpEcmpNveEntry == NULL) &&
                    (VxlanHwUpdateNveDatabase (pVxlanAddNveEntry, VXLAN_HW_ADD)
                     == VXLAN_FAILURE))
                {
                    RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                               pVxlanAddNveEntry);
                    MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                        (UINT1 *) pVxlanAddNveEntry);
                    return;
                }
                pVxlanAddNveEntry->MibObject.i4FsVxlanNveStorageType =
                    VXLAN_STRG_TYPE_VOL;

                if (pEvpnRoute->u1VxlanDestAddressLen == EVPN_ZERO)
                {
                    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                     "Ip Address Length is Zero.Not triggered due to Arp Supression\n"));
                    return;
                }

                u4IfIndex =
                    CfaGetVlanInterfaceIndex ((tVlanIfaceVlanId) i4VlanId);
                if (u4IfIndex == CFA_INVALID_INDEX)
                {
                    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                     "Invalid L3 VLAN interface Index for the VLAN %d\n",
                                     i4VlanId));
                }
                if (EVPN_CLI_ARP_SUPPRESS ==
                    pVxlanAddNveEntry->MibObject.i4FsVxlanSuppressArp)
                {
                    if (pEvpnRoute->u1VxlanDestAddressLen ==
                        VXLAN_IP4_ADDR_LEN * VXLAN_ONE_BYTE_BITS)
                    {
#ifdef ARP_WANTED
                        MEMSET (&ArpData, 0, sizeof (tARP_DATA));
                        MEMCPY (&ArpData.u4IpAddr,
                                pEvpnRoute->au1VxlanDestAddress,
                                sizeof (UINT4));
                        ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
                        ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
                        ArpData.i1Hwalen = MAC_ADDR_LEN;
                        ArpData.i1State = ARP_PROXY_EVPN;
                        MEMCPY (ArpData.i1Hw_addr,
                                (INT1 *) pEvpnRoute->VxlanDestVmMac,
                                ArpData.i1Hwalen);
                        ArpData.u1RowStatus = ACTIVE;
                        VxlanMainTaskUnLock ();
                        arp_add (ArpData);
                        VxlanMainTaskLock ();
#endif
                    }
                    else if (pEvpnRoute->u1VxlanDestAddressLen ==
                             VXLAN_IP6_ADDR_LEN * VXLAN_ONE_BYTE_BITS)
                    {
#ifdef IP6_WANTED
                        Ip6UpdateCacheForAddrFromEvpn (u4IfIndex,
                                                       (tIp6Addr *) (VOID *)
                                                       pEvpnRoute->
                                                       au1VxlanDestAddress,
                                                       pEvpnRoute->
                                                       VxlanDestVmMac);
#endif /* IP6_WANTED */
                    }
                }
            }
            else if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType
                     == VXLAN_STRG_TYPE_VOL)
            {
                pVxlanNveEntry->i4FsVxlanNveEvpnMacType = VXLAN_NVE_EVPN_MAC;
                /* Update the Vrf Flag */
                pVxlanNveEntry->u2VrfFlag |= pEvpnRoute->u2VrfFlag;

                pTmpVxlanNveEntry =
                    VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                             zeroAddr, u4NveIndex);
                if (pTmpVxlanNveEntry != NULL)
                {
                    pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp =
                        pTmpVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;
                }
#ifdef NPAPI_WANTED
                if (b1IngOrMcastEntryPresent == TRUE)
                {
                    for (u4IfIndex = 0; u4IfIndex < VXLAN_HW_REPLICA_LIST;
                         u4IfIndex++)
                    {
                        pTmpVxlanNveEntry = VxlanUtilGetNveDatabase
                            (pEvpnRoute->u4VxlanVniNumber, zeroAddr,
                             u4NveIndex);
                        if (pTmpVxlanNveEntry != NULL)
                        {
                            if (MEMCMP (&(pTmpVxlanNveEntry->MibObject.
                                          au1FsVxlanNveRemoteVtepAddress),
                                        &(pEvpnRoute->
                                          au1VxlanRemoteVtepAddress),
                                        VXLAN_IP4_ADDR_LEN) == 0)
                            {
                                pVxlanAddNveEntry->bIngOrMcastZeroEntryPresent =
                                    TRUE;
                            }
                            u4NveIndex = u4NveIndex + SYS_DEF_MAX_NVE_IFACES;
                        }
                    }
                }
#endif
                if (pEvpnRoute->u1VxlanDestAddressLen == EVPN_ZERO)
                {
                    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                     "Ip Address Length is Zero.Not triggered due to Arp Supression\n"));
                    return;
                }

                u4IfIndex =
                    CfaGetVlanInterfaceIndex ((tVlanIfaceVlanId) i4VlanId);
                if (u4IfIndex == CFA_INVALID_INDEX)
                {
                    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                                     "Invalid L3 VLAN interface Index for the VLAN %d\n",
                                     i4VlanId));
                }
                if (EVPN_CLI_ARP_SUPPRESS ==
                    pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp)
                {
                    if (pEvpnRoute->u1VxlanDestAddressLen ==
                        VXLAN_IP4_ADDR_LEN * VXLAN_ONE_BYTE_BITS)

                    {
#ifdef ARP_WANTED
                        MEMSET (&ArpData, 0, sizeof (tARP_DATA));
                        MEMCPY (&ArpData.u4IpAddr,
                                pEvpnRoute->au1VxlanDestAddress,
                                sizeof (UINT4));
                        ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
                        ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
                        ArpData.i1Hwalen = MAC_ADDR_LEN;
                        ArpData.i1State = ARP_PROXY_EVPN;
                        MEMCPY (ArpData.i1Hw_addr,
                                (INT1 *) pEvpnRoute->VxlanDestVmMac,
                                ArpData.i1Hwalen);
                        ArpData.u1RowStatus = ACTIVE;
                        arp_add (ArpData);
#endif
                    }
                    else if (pEvpnRoute->u1VxlanDestAddressLen ==
                             VXLAN_IP6_ADDR_LEN * VXLAN_ONE_BYTE_BITS)
                    {
#ifdef IP6_WANTED
                        Ip6UpdateCacheForAddrFromEvpn (u4IfIndex,
                                                       (tIp6Addr *) (VOID *)
                                                       pEvpnRoute->
                                                       au1VxlanDestAddress,
                                                       pEvpnRoute->
                                                       VxlanDestVmMac);
#endif /* IP6_WANTED */
                    }

                }
            }
        }
        if (pEvpnRoute->u4L3VniNumber != 0)
        {
            /* Indication is given to ARP module to Add an ARP entry, so VXLAN Lock is not
               taken */
            pVxlanAddNveEntry->b1IsIrbRoute = TRUE;
            MEMCPY (&(pVxlanAddNveEntry->au1VxlanIRBHostAddress),
                    &(pEvpnRoute->au1VxlanDestAddress), VXLAN_IP4_ADDR_LEN);
            /* Indication is given to ARP module to Add an ARP entry,
             * so VXLAN Lock is not taken */
            VxlanMainTaskUnLock ();
            /* The incoming route is symmetric IRB case */
            VxlanUtilHandleSymmerticAddIrbRoute (pEvpnRoute, u4NveIndex);
            VxlanMainTaskLock ();
        }

    }
    else if (pEvpnRoute->u1RouteType == EVPN_ETH_SEGEMENT_ROUTE)
    {
        if (EvpnVxlanAddESRouteInMHPeer (pEvpnRoute) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "FUNC:EvpnVxlanAddESRouteInMHPeer Failed\n"));
            return;
        }
    }
    else if (pEvpnRoute->u1RouteType == EVPN_AD_ROUTE)
    {
        if (EvpnVxlanAddADRouteInMHPeer (pEvpnRoute) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "FUNC:EvpnVxlanAddADRouteInMHPeer Failed\n"));
            return;
        }
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddRouteInDp Exit\n"));

    return;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanAddESRouteInMHPeer                            *
 *  Description     : Called from BGP to program ES routes in data plane     *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanAddESRouteInMHPeer (tEvpnRoute * pEvpnRoute)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable * pEvpnVxlanMultihomedPeerTable = NULL;
    tTimerParam        *pTmrParam = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanTmpNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextTmpNveEntry = NULL;
    UINT4               u4VniIndex = 0;

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddESRouteInMHPeer Entry\n"));

    pEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);

    if (pEvpnVxlanMultihomedPeerTable == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                         "Memory allocation failed for the"
                         "new Multihomed peer entry\n"));
        return VXLAN_FAILURE;
    }

    /* Fill the VTEP IPV4 address */
    if (pEvpnRoute->i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
    {
        pEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType = EVPN_IPV4_TYPE;
        MEMCPY (&(pEvpnVxlanMultihomedPeerTable->MibObject.
                  au1FsEvpnVxlanPeerIpAddress),
                &(pEvpnRoute->au1VxlanDestAddress), VXLAN_IP4_ADDR_LEN);
    }
    /* Fill the VTEP IPV6 address */
    else
    {
        pEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType = EVPN_IPV6_TYPE;
        MEMCPY (&(pEvpnVxlanMultihomedPeerTable->MibObject.
                  au1FsEvpnVxlanPeerIpAddress),
                &(pEvpnRoute->au1VxlanDestAddress), VXLAN_IP6_ADDR_LEN);
    }
    /* Fill the ESI Value */
    MEMCPY (&(pEvpnVxlanMultihomedPeerTable->MibObject.
              au1FsEvpnVxlanMHEviVniESI),
            &(pEvpnRoute->au1FsEvpnMHEviVniESI), EVPN_MAX_ESI_LEN);
    pEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = VXLAN_IP4_ADDR_LEN;
    pEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanMHEviVniESILen = EVPN_MAX_ESI_LEN;

    if (RBTreeAdd
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
         (tRBElem *) pEvpnVxlanMultihomedPeerTable) != RB_SUCCESS)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "RB Tree addition failed\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanMultihomedPeerTable);
        return VXLAN_FAILURE;
    }

    if (EvpnUtilGetVniFromEsiEntry (pEvpnRoute->au1FsEvpnMHEviVniESI,
                                    &u4VniIndex) == VXLAN_SUCCESS)
    {
        pVxlanFsVxlanTmpNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
        while (pVxlanFsVxlanTmpNveEntry != NULL)
        {
            pVxlanFsVxlanNextTmpNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);

            if ((MEMCMP
                 (&
                  (pVxlanFsVxlanTmpNveEntry->MibObject.
                   au1FsVxlanNveRemoteVtepAddress),
                  &(pEvpnRoute->au1VxlanDestAddress), VXLAN_IP4_ADDR_LEN) == 0)
                && (pVxlanFsVxlanTmpNveEntry->MibObject.u4FsVxlanNveVniNumber ==
                    u4VniIndex)
                && (pVxlanFsVxlanTmpNveEntry->MibObject.
                    i4FsVxlanNveStorageType == VXLAN_STRG_TYPE_VOL))
            {
                if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanTmpNveEntry,
                                              VXLAN_HW_DEL) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "Failed to delete Nve entry from h/w\r\n"));
                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           pVxlanFsVxlanTmpNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanFsVxlanTmpNveEntry);
            }
            pVxlanFsVxlanTmpNveEntry = pVxlanFsVxlanNextTmpNveEntry;
        }
    }

    pTmrParam =
        (tTimerParam *) & (pEvpnVxlanMultihomedPeerTable->EvpnRouteTimerParam);
    pTmrParam->u1Id = EVPN_TIMER_ROUTE;
    pTmrParam->u.pEvpnMultihomedPeerTable = pEvpnVxlanMultihomedPeerTable;
    pTmrParam->u.pEvpnMultihomedPeerTable->EvpnRouteTimer.u4Data =
        (FS_ULONG) pTmrParam;

    /* Start the Timer */
    if (TmrStartTimer (gVxlanGlobals.VxlanTimerList,
                       &(pEvpnVxlanMultihomedPeerTable->EvpnRouteTimer),
                       EVPN_THREE_SECOND) != TMR_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "Timer Start Failure\n"));
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddESRouteInMHPeer Exit\n"));

    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanAddADRouteInMHPeer                            *
 *  Description     : Called from BGP to program AD routes in data plane     *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanAddADRouteInMHPeer (tEvpnRoute * pEvpnRoute)
{
    UINT4               u4NveIndex = 0;
    INT4                i4VlanId = 0;
    tMacAddr            ZeroVmMac;
    BOOL1               bFirstEntryFlag = VXLAN_FALSE;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanAddEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry TmpVxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pTmpVxlanEcmpNveEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnVxlanEviVniMapEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanTmpNveEntry = NULL;

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddADRouteInMHPeer Entry\n"));
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (ZeroVmMac, 0, sizeof (tMacAddr));
    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    MEMSET (&TmpVxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (pEvpnRoute->u4VxlanVniNumber,
                                           &u4NveIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable
             (pEvpnRoute->u4VxlanVniNumber, &u4NveIndex)) == NULL)
        {
            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable
                 (pEvpnRoute->u4VxlanVniNumber, &u4NveIndex)) == NULL)
            {
                /* If MCAST database is also not mapped, dropped the VXLAN packet */
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "Nve index is not mapped for the VNI - %d"
                                 "in NVE, In-Replica and Multicast database\n",
                                 pEvpnRoute->u4VxlanVniNumber));
                return VXLAN_SUCCESS;
            }
        }
    }
    VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
        pEvpnRoute->u4VxlanVniNumber;

    pVxlanVniVlanMapEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                       (tRBElem *) & VxlanVniVlanMapEntry, NULL);

    while (pVxlanVniVlanMapEntry != NULL)
    {
        if (pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber
            == pEvpnRoute->u4VxlanVniNumber)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, " Mapping found for VNI id - %d,"
                             "VLAN id - %d in VLAN VNI mapping table\n",
                             pEvpnRoute->u4VxlanVniNumber,
                             pVxlanVniVlanMapEntry->MibObject.
                             i4FsVxlanVniVlanMapVlanId));

            i4VlanId =
                pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanMapVlanId;
            break;
        }

        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) pVxlanVniVlanMapEntry, NULL);
    }

    if (pVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, " No vlan is mapped with the VNI\n"));
        return VXLAN_SUCCESS;
    }

    if (u4NveIndex)
    {
        /* Check if any AD route is present */
        TmpVxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
            (INT4) u4NveIndex;
        TmpVxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
            pEvpnRoute->u4VxlanVniNumber;
        MEMCPY (TmpVxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                ZeroVmMac, sizeof (tMacAddr));
        pTmpVxlanEcmpNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           (tRBElem *) & TmpVxlanEcmpNveEntry, NULL);
        while (pTmpVxlanEcmpNveEntry != NULL)
        {
            if (MEMCMP
                (pTmpVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
                 pEvpnRoute->au1FsEvpnMHEviVniESI, EVPN_MAX_ESI_LEN) == 0)
            {
                break;
            }
            pTmpVxlanEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                   FsVxlanEcmpNveTable,
                                                   (tRBElem *)
                                                   pTmpVxlanEcmpNveEntry, NULL);
        }

        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "Entry has to be added in NVE database\n"));
        pVxlanAddEcmpNveEntry =
            (tVxlanFsVxlanEcmpNveEntry *)
            MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

        if (pVxlanAddEcmpNveEntry == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                             "Memory allocation failed for the"
                             "new NVE entry\n"));
            return VXLAN_FAILURE;
        }

        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "Memory allocation succeed for the"
                         "new NVE entry\n"));

        /* Fill the source VTEP address */
        VxlanInitializeMibFsVxlanEcmpNveTable (pVxlanAddEcmpNveEntry,
                                               (INT4) u4NveIndex);

        /* Fill the Vlan-Id */
        pVxlanAddEcmpNveEntry->i4VlanId = i4VlanId;
        /* Fill the NVE if index */
        pVxlanAddEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex =
            (INT4) u4NveIndex;

        /* Fill the VNI number */
        pVxlanAddEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber =
            pEvpnRoute->u4VxlanVniNumber;

        /* Fill the destination VM mac address */
        MEMCPY (&(pVxlanAddEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac),
                &(ZeroVmMac), VXLAN_ETHERNET_ADDR_SIZE);

        pVxlanAddEcmpNveEntry->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType
            = pEvpnRoute->i4VxlanRemoteVtepAddressType;

        /* Fill the Remote VTEP IP address */
        if (pEvpnRoute->i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
        {
            pVxlanAddEcmpNveEntry->MibObject.
                i4FsVxlanEcmpNveRemoteVtepAddressLen = VXLAN_IP4_ADDR_LEN;
            MEMCPY (&
                    (pVxlanAddEcmpNveEntry->MibObject.
                     au1FsVxlanEcmpNveRemoteVtepAddress),
                    &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                    VXLAN_IP4_ADDR_LEN);
        }
        else
        {
            pVxlanAddEcmpNveEntry->MibObject.
                i4FsVxlanEcmpNveRemoteVtepAddressLen = VXLAN_IP6_ADDR_LEN;
            MEMCPY (&
                    (pVxlanAddEcmpNveEntry->MibObject.
                     au1FsVxlanEcmpNveRemoteVtepAddress),
                    &(pEvpnRoute->au1VxlanRemoteVtepAddress),
                    VXLAN_IP6_ADDR_LEN);
        }
        /* Fill the Esi id */
        MEMCPY (pVxlanAddEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
                pEvpnRoute->au1FsEvpnMHEviVniESI, EVPN_MAX_ESI_LEN);
        /* Fill the storage type as volatile since the entry is learnt dynamically */
        pVxlanAddEcmpNveEntry->MibObject.i4FsVxlanEcmpNveStorageType =
            VXLAN_STRG_TYPE_VOL;

        /* Update the Load-Balancing Flag in EviVni table */
        EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
            pEvpnRoute->u4VxlanVniNumber;
        if (EvpnUtilGetEviFromVni (pEvpnRoute->u4VxlanVniNumber,
                                   &(EvpnVxlanEviVniMapEntry.MibObject.
                                     i4FsEvpnVxlanEviVniMapEviIndex)) ==
            VXLAN_FAILURE)
        {
            return VXLAN_FAILURE;
        }
        pEvpnVxlanEviVniMapEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       (tRBElem *) & EvpnVxlanEviVniMapEntry);
        if (pEvpnVxlanEviVniMapEntry != NULL)
        {
            /* Update the Load-Balancing Flag in EviVni table */
            if (pEvpnRoute->u1LBFlag == 0)
            {
                pEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance = EVPN_CLI_LB_ENABLE;
            }
            else if (pEvpnRoute->u1LBFlag == 1)
            {
                pEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance = EVPN_CLI_LB_DISABLE;
            }
            EvpnVxlanIsThisFirstEcmpEntry (pEvpnRoute, u4NveIndex,
                                           &bFirstEntryFlag);
            if (bFirstEntryFlag == VXLAN_TRUE)
            {
                /* This is First Entry and is Active */
                pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
                    VXLAN_TRUE;
            }
            else
            {
                if (pEvpnVxlanEviVniMapEntry->MibObject.
                    i4FsEvpnVxlanEviVniLoadBalance == EVPN_CLI_LB_ENABLE)
                {
                    /* Active-Active */
                    pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
                        VXLAN_TRUE;
                }
                else if (pEvpnVxlanEviVniMapEntry->MibObject.
                         i4FsEvpnVxlanEviVniLoadBalance == EVPN_CLI_LB_DISABLE)
                {
                    /* Active-Standby */
                    pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
                        VXLAN_FALSE;
                }
            }

        }
        else
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "Evi Vni map Entry not found for Evi:%d, Vni:%d\n",
                             EvpnVxlanEviVniMapEntry.MibObject.
                             i4FsEvpnVxlanEviVniMapEviIndex,
                             EvpnVxlanEviVniMapEntry.MibObject.
                             u4FsEvpnVxlanEviVniMapVniNumber));

        }
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
             (tRBElem *) pVxlanAddEcmpNveEntry) != RB_SUCCESS)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "RB Tree addition failed\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                (UINT1 *) pVxlanAddEcmpNveEntry);
            return VXLAN_FAILURE;
        }
        VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                  &(pVxlanAddEcmpNveEntry->MibObject.
                                    EcmpNveDbNode));
        VxlanRedSyncDynInfo ();

        pVxlanFsVxlanTmpNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
        while (pVxlanFsVxlanTmpNveEntry != NULL)
        {
            if ((MEMCMP (pVxlanFsVxlanTmpNveEntry->MibObject.
                         au1FsVxlanNveRemoteVtepAddress,
                         pVxlanAddEcmpNveEntry->MibObject.
                         au1FsVxlanEcmpNveRemoteVtepAddress,
                         VXLAN_IP4_ADDR_LEN) == 0) &&
                (MEMCMP
                 (pVxlanFsVxlanTmpNveEntry->MibObject.FsVxlanNveDestVmMac,
                  ZeroVmMac, VXLAN_ETHERNET_ADDR_SIZE) == 0))
            {
                break;
            }
            pVxlanFsVxlanTmpNveEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                 (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);
        }
        if (pVxlanFsVxlanTmpNveEntry == NULL)
        {
            if (VxlanHwUpdateEcmpNveDatabase (pVxlanAddEcmpNveEntry,
                                              VXLAN_HW_ADD) == VXLAN_FAILURE)
            {
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           pVxlanAddEcmpNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                    (UINT1 *) pVxlanAddEcmpNveEntry);
                return VXLAN_FAILURE;
            }
        }
        if (pEvpnVxlanEviVniMapEntry != NULL)
        {
            /* Update the Total paths in EviVni table After adding the Ecmp route in RBTree */
            pEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniMapTotalPaths++;
        }
        if (pTmpVxlanEcmpNveEntry != NULL)
        {
            pVxlanAddEcmpNveEntry = NULL;
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
                pTmpVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex;
            VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
                pTmpVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber;
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
                pTmpVxlanEcmpNveEntry->MibObject.
                i4FsVxlanEcmpNveRemoteVtepAddressType;
            MEMCPY (VxlanEcmpNveEntry.MibObject.
                    au1FsVxlanEcmpNveRemoteVtepAddress,
                    pTmpVxlanEcmpNveEntry->MibObject.
                    au1FsVxlanEcmpNveRemoteVtepAddress,
                    pTmpVxlanEcmpNveEntry->MibObject.
                    i4FsVxlanEcmpNveRemoteVtepAddressLen);
            pVxlanEcmpNveEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                               (tRBElem *) & VxlanEcmpNveEntry, NULL);
            while (pVxlanEcmpNveEntry != NULL)
            {
                if ((MEMCMP
                     (pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
                      pTmpVxlanEcmpNveEntry->MibObject.
                      au1FsVxlanEcmpMHEviVniESI, EVPN_MAX_ESI_LEN) == 0)
                    &&
                    (MEMCMP
                     (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                      ZeroVmMac, sizeof (tMacAddr)) != 0))
                {
                    /*Mac Entry found for the Esi, add a new Mac entry for this PE */
                    pVxlanAddEcmpNveEntry =
                        (tVxlanFsVxlanEcmpNveEntry *)
                        MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);

                    if (pVxlanAddEcmpNveEntry == NULL)
                    {
                        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                                         "Memory allocation failed for the"
                                         "new NVE entry\n"));
                        return VXLAN_FAILURE;
                    }

                    VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                                     "Memory allocation succeed for the"
                                     "new NVE entry\n"));
                    /* copy the MAC Entry to New Entry */
                    MEMCPY (pVxlanAddEcmpNveEntry, pVxlanEcmpNveEntry,
                            sizeof (tVxlanFsVxlanEcmpNveEntry));
                    /* Reset the HW Flag */
                    pVxlanAddEcmpNveEntry->bHwSet = FALSE;
                    /* change the Remote VTEP IP */
                    MEMCPY (pVxlanAddEcmpNveEntry->MibObject.
                            au1FsVxlanEcmpNveRemoteVtepAddress,
                            pEvpnRoute->au1VxlanRemoteVtepAddress,
                            (pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                             VXLAN_IPV4_UNICAST) ? VXLAN_IP4_ADDR_LEN :
                            VXLAN_IP6_ADDR_LEN);
                    /* Fill the Mac Learnt flag */
                    pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt =
                        VXLAN_FALSE;
                    /* Fill Active or Standby Flag */
                    if (pEvpnRoute->u1LBFlag == 0)
                    {
                        /* Active-Active */
                        pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
                            VXLAN_TRUE;
                    }
                    else if (pEvpnRoute->u1LBFlag == 1)
                    {
                        /* Active-Standby */
                        pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive =
                            VXLAN_FALSE;
                    }

                    if (RBTreeAdd
                        (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                         (tRBElem *) pVxlanAddEcmpNveEntry) != RB_SUCCESS)
                    {
                        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                         "RB Tree addition failed\n"));
                        MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                            (UINT1 *) pVxlanAddEcmpNveEntry);
                        return VXLAN_FAILURE;
                    }
                    VxlanRedDbUtilAddTblNode (&gVxlanDynInfoList,
                                              &(pVxlanAddEcmpNveEntry->
                                                MibObject.EcmpNveDbNode));
                    VxlanRedSyncDynInfo ();

                    /* In curent ALTA dataplane, ACTIVE - ACTIVE support is not available. Hence this entry is not 
                       programmed in H/w */
                }
                pVxlanEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanEcmpNveTable,
                                                    (tRBElem *)
                                                    pVxlanEcmpNveEntry, NULL);

            }
        }
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanAddADRouteInMHPeer Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanPortCheckEviVniMap                            *
 *  Description     : Checks the EVI mapping for VNI                         *
 *  Input           : u4EviId - EVI id                                       *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanPortCheckEviVniMap (UINT4 u4EviId)
{
    UINT4               u4VniId = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanPortCheckEviVniMap Entry\n"));

    VxlanMainTaskLock ();

    /* Check whether EVI is mapped with the received VNI id */
    if (EvpnVxlanUtilGetVniFromEvi (u4EviId, &u4VniId) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "There is no VNI mapping for EVI - %d\n", u4EviId));
        VxlanMainTaskUnLock ();
        return VXLAN_FAILURE;
    }

    VxlanMainTaskUnLock ();

    VXLAN_TRC ((VXLAN_EVPN_TRC, " Mapping found for EVI id - %d,"
                "VNI id - %d in VNI EVI mapping table\n", u4EviId, u4VniId));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanPortCheckEviVniMap Exit\n"));

    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlBgpGetMacUpdate 
 * Input       : u4VniId 
 * Description : This function is called when MAC entries are 
 *               required for MAC route in BGP-EVPN.
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
INT4
EvpnUtlBgpGetMacUpdate (UINT4 u4VniId)
{
    tVlanId             VlanId;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActive: Entry\n"));
    VxlanUtilGetVlanFromVni (u4VniId, &VlanId);
    VlanApiGetMACUpdate (VLAN_VXLAN_EVPN_PROTO_ID, VlanId);

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActive: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanIsEcmpRouteUpdate                          *
 *  Description     : Called to verify if the existing AD route is to be     *
 *                    updated with MAC                                       *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanIsEcmpRouteUpdate (tEvpnRoute * pEvpnRoute, INT4 i4VlanId)
{
    UINT4               u4NveIndex = 0;
    UINT4               u4IfIndex = 0;
    tMacAddr            zeroAddr;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanAddEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanNveEntry *pTmpVxlanNveEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    UINT1               au1ZeroESI[EVPN_MAX_ESI_LEN];
    BOOL1               bADRouteExists = VXLAN_FALSE;
#ifdef ARP_WANTED
    tARP_DATA           ArpData;
#endif
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanIsADRouteTobeUpdated Entry\n"));
    MEMSET (zeroAddr, 0, sizeof (tMacAddr));
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (au1ZeroESI, 0, EVPN_MAX_ESI_LEN);

    if (pEvpnRoute == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC, "pEvpnRoute is NULL\n"));
        return VXLAN_FAILURE;
    }
    if (MEMCMP (pEvpnRoute->au1FsEvpnMHEviVniESI, au1ZeroESI, EVPN_MAX_ESI_LEN)
        == 0)
    {
        return VXLAN_FAILURE;
    }
    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (pEvpnRoute->u4VxlanVniNumber,
                                           &u4NveIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable
             (pEvpnRoute->u4VxlanVniNumber, &u4NveIndex)) == NULL)
        {
            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable
                 (pEvpnRoute->u4VxlanVniNumber, &u4NveIndex)) == NULL)
            {
                /* If MCAST database is also not mapped, dropped the VXLAN packet */
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "Nve index is not mapped for the VNI - %d"
                                 "in NVE, In-Replica and Multicast database\n",
                                 pEvpnRoute->u4VxlanVniNumber));
                return VXLAN_SUCCESS;
            }
        }
    }

    if (u4NveIndex)
    {
        pTmpVxlanNveEntry =
            VxlanUtilGetNveDatabase (pEvpnRoute->u4VxlanVniNumber,
                                     zeroAddr, u4NveIndex);

        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
        VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
            pEvpnRoute->u4VxlanVniNumber;
        MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                pEvpnRoute->VxlanDestVmMac, sizeof (tMacAddr));
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
            pEvpnRoute->i4VxlanRemoteVtepAddressType;
        MEMCPY (VxlanEcmpNveEntry.MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
                pEvpnRoute->au1VxlanRemoteVtepAddress,
                ((pEvpnRoute->i4VxlanRemoteVtepAddressType ==
                  VXLAN_IPV4_UNICAST) ? VXLAN_IP4_ADDR_LEN :
                 VXLAN_IP6_ADDR_LEN));
        if (pEvpnRoute->i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
        {
            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressLen =
                VXLAN_IP4_ADDR_LEN;
        }
        else
        {

            VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressLen =
                VXLAN_IP6_ADDR_LEN;
        }
        pVxlanEcmpNveEntry = VxlanGetFsVxlanEcmpNveTable (&VxlanEcmpNveEntry);
        if (pVxlanEcmpNveEntry != NULL)
        {
            pVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt = VXLAN_TRUE;
            return VXLAN_SUCCESS;
        }
        /* Zero MAC Entry available, So update the Ecmp entry with the learnt MAC */
        MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
        VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
            pEvpnRoute->u4VxlanVniNumber;
        MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                zeroAddr, sizeof (tMacAddr));

        pVxlanEcmpNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           (tRBElem *) & VxlanEcmpNveEntry, NULL);
        while ((pVxlanEcmpNveEntry != NULL) &&
               (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
                (INT4) u4NveIndex) &&
               (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber ==
                pEvpnRoute->u4VxlanVniNumber) &&
               (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                        zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0))
        {
            if (MEMCMP (pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
                        pEvpnRoute->au1FsEvpnMHEviVniESI,
                        EVPN_MAX_ESI_LEN) == 0)
            {
                bADRouteExists = VXLAN_TRUE;
                pVxlanAddEcmpNveEntry =
                    (tVxlanFsVxlanEcmpNveEntry *)
                    MemAllocMemBlk (VXLAN_FSVXLANECMPNVETABLE_POOLID);
                if (pVxlanAddEcmpNveEntry != NULL)
                {
                    MEMCPY (pVxlanAddEcmpNveEntry, pVxlanEcmpNveEntry,
                            sizeof (tVxlanFsVxlanEcmpNveEntry));
                    MEMCPY (pVxlanAddEcmpNveEntry->MibObject.
                            FsVxlanEcmpNveDestVmMac, pEvpnRoute->VxlanDestVmMac,
                            VXLAN_ETHERNET_ADDR_SIZE);
                    pVxlanAddEcmpNveEntry->bHwSet = FALSE;

                    /* Fill Evpn Mac Type */
                    pVxlanAddEcmpNveEntry->i4FsVxlanEcmpNveEvpnMacType =
                        VXLAN_NVE_EVPN_MAC;
                    /* Fill ARP suppression Enabled/Disabled */
                    if (pTmpVxlanNveEntry != NULL)
                    {
                        pVxlanAddEcmpNveEntry->MibObject.
                            i4FsVxlanEcmpSuppressArp =
                            pTmpVxlanNveEntry->MibObject.i4FsVxlanSuppressArp;
                    }
                    /* Fill Mac Learnt Flag */
                    if (MEMCMP (pVxlanAddEcmpNveEntry->MibObject.
                                au1FsVxlanEcmpNveRemoteVtepAddress,
                                pEvpnRoute->au1VxlanRemoteVtepAddress,
                                pVxlanAddEcmpNveEntry->MibObject.
                                i4FsVxlanEcmpNveRemoteVtepAddressLen) == 0)
                    {
                        pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt =
                            VXLAN_TRUE;
                    }

                    if (RBTreeAdd
                        (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                         (tRBElem *) pVxlanAddEcmpNveEntry) != RB_SUCCESS)
                    {
                        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                         "RB Tree addition failed\n"));
                        pVxlanEcmpNveEntry =
                            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                           FsVxlanEcmpNveTable,
                                           (tRBElem *) pVxlanEcmpNveEntry,
                                           NULL);
                        continue;

                    }
                    /* Delete the Nve entry if any present with same MAC and Remote-VTEP-IP */
                    pVxlanNveEntry = VxlanUtilGetNveDatabase
                        (pEvpnRoute->u4VxlanVniNumber,
                         pEvpnRoute->VxlanDestVmMac, u4NveIndex);
                    if (pVxlanNveEntry != NULL)
                    {
                        if (VxlanHwUpdateNveDatabase (pVxlanNveEntry,
                                                      VXLAN_HW_DEL) ==
                            VXLAN_FAILURE)
                        {
                            VXLAN_TRC ((VXLAN_UTIL_TRC,
                                        "Failed to delete Nve entry from h/w\r\n"));
                        }
                        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                   pVxlanNveEntry);
                        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                            (UINT1 *) pVxlanNveEntry);
                    }

                    if (pVxlanAddEcmpNveEntry->MibObject.bFsVxlanEcmpActive
                        == VXLAN_TRUE)
                    {
                        if (VxlanHwUpdateEcmpNveDatabase (pVxlanAddEcmpNveEntry,
                                                          VXLAN_HW_ADD) ==
                            VXLAN_FAILURE)
                        {
                            VXLAN_TRC ((VXLAN_UTIL_TRC,
                                        "Failed to delete Nve entry from h/w\r\n"));
                        }
                    }
                }
            }
            pVxlanEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanEcmpNveTable,
                                                (tRBElem *) pVxlanEcmpNveEntry,
                                                NULL);

        }                        /* while */
        if (bADRouteExists == VXLAN_FALSE)
        {
            return VXLAN_FAILURE;
        }
        if (pEvpnRoute->u1VxlanDestAddressLen == EVPN_ZERO)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                             "Ip Address Length is Zero.Not triggered due to Arp Supression\n"));
            return VXLAN_SUCCESS;
        }

        u4IfIndex = CfaGetVlanInterfaceIndex ((tVlanIfaceVlanId) i4VlanId);
        if (u4IfIndex == CFA_INVALID_INDEX)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                             "Invalid L3 VLAN interface Index for the VLAN %d\n",
                             i4VlanId));
            return VXLAN_FAILURE;
        }
        if (pEvpnRoute->u1VxlanDestAddressLen == VXLAN_IP4_ADDR_LEN_BITS)
        {
#ifdef ARP_WANTED
            MEMSET (&ArpData, 0, sizeof (tARP_DATA));
            MEMCPY (&ArpData.u4IpAddr, pEvpnRoute->au1VxlanDestAddress,
                    sizeof (UINT4));
            ArpData.u4IpAddr = OSIX_HTONL (ArpData.u4IpAddr);
            ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
            ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
            ArpData.i1Hwalen = MAC_ADDR_LEN;
            ArpData.i1State = ARP_PROXY_EVPN;
            MEMCPY (ArpData.i1Hw_addr, (INT1 *) pEvpnRoute->VxlanDestVmMac,
                    ArpData.i1Hwalen);
            ArpData.u1RowStatus = ACTIVE;
            arp_add (ArpData);
#endif
        }
        else if (pEvpnRoute->u1VxlanDestAddressLen == VXLAN_IP6_ADDR_LEN_BITS)
        {
#ifdef IP6_WANTED
            Ip6UpdateCacheForAddrFromEvpn (u4IfIndex, (tIp6Addr *) (VOID *)
                                           pEvpnRoute->au1VxlanDestAddress,
                                           pEvpnRoute->VxlanDestVmMac);
#endif /* IP6_WANTED */
        }

    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanIsADRouteTobeUpdated Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : VxlanNotifyESIAndIfStatus                              *
 *  Description     : Called to send ESI related information, Vlan Id and    *
 *                    the Interface Oper Status details                      *
 *  Input           : pLaSystem - La System                                  *
 *                    u2VlanId - Vlan Id                                     *
 *                    u1IfOperStatus - Interface Oper Status                 *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
VxlanNotifyESIAndIfStatus (UINT1 *pu1VlanList, tMacAddr SysAddr,
                           UINT2 u2SysPriority, UINT1 u1IfOperStatus)
{
    tVxlanIfMsg         VxlanIfMsg;
    UINT4               u4VniId = 0;
    UINT2               u2VlanIndex = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1VlanFlag = 0;
    UINT4               u4BitCount = 0;
    BOOL1               b1Result = OSIX_FALSE;
    UINT4               u4ByteInd;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));

    for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex, VLAN_LIST_SIZE,
                                 b1Result);
        if (b1Result == OSIX_TRUE)
        {
            for (u4ByteInd = 0; u4ByteInd < 512; u4ByteInd++)
            {
                if (pu1VlanList[u4ByteInd] != 0)
                {
                    u1VlanFlag = pu1VlanList[u4ByteInd];
                    for (u4BitCount = 0; (u4BitCount < VLAN_VLANS_PER_BYTE &&
                                          u1VlanFlag != 0); u4BitCount++)
                    {
                        if (u1VlanFlag & VLAN_BIT8)
                        {
                            u2VlanId =
                                (tVlanId) ((u4ByteInd * VLAN_VLANS_PER_BYTE) +
                                           u4BitCount + 1);
                        }
                        u1VlanFlag = (UINT1) (u1VlanFlag << 1);
                    }
                }
            }
            if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniId) != VXLAN_SUCCESS)
            {
                continue;
            }
            VxlanIfMsg.u4MsgType = VXLAN_EVPN_ESI_IF_OPER_CHNG;
            MEMCPY (&(VxlanIfMsg.uVxlanMsg.VlanOper.SysID), SysAddr,
                    sizeof (tMacAddr));
            VxlanIfMsg.uVxlanMsg.VlanOper.u2SysPriority = u2SysPriority;
            VxlanIfMsg.uVxlanMsg.VlanOper.u2VlanId = u2VlanId;
            VxlanIfMsg.uVxlanMsg.VlanOper.u1OperStatus = u1IfOperStatus;
            if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
            {
                return VXLAN_FAILURE;
            }
        }
    }
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanIsEcmpRouteUpdate                          *
 *  Description     : Called to verify if the existing AD route is to be     *
 *                    updated with MAC                                       *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnVxlanIsMacLearntFlagSet (UINT4 u4Vni, tMacAddr DestVmMac, UINT4 u4NveIndex)
{
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;

    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4Vni;
    MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
            DestVmMac, sizeof (tMacAddr));
    pVxlanEcmpNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       (tRBElem *) & VxlanEcmpNveEntry, NULL);
    while ((pVxlanEcmpNveEntry != NULL)
           && (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
               (INT4) u4NveIndex)
           && (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber == u4Vni)
           &&
           (MEMCMP
            (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac, DestVmMac,
             sizeof (tMacAddr)) == 0))
    {
        if (pVxlanEcmpNveEntry->MibObject.bFsVxlanEcmpMacLearnt == VXLAN_TRUE)
        {
            /* Atleast one Ecmp Entry Exists with Mac Learnt Flag set */
            return VXLAN_SUCCESS;
        }
        pVxlanEcmpNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           (tRBElem *) pVxlanEcmpNveEntry, NULL);
    }
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : EvpnVxlanDeleteMacEntries                              *
 *  Description     : Called to verify if the existing AD route is to be     *
 *                    updated with MAC                                       *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
VOID
EvpnVxlanDeleteMacEntries (UINT4 u4Vni, tMacAddr DestVmMac, UINT4 u4NveIndex)
{
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pNextVxlanEcmpNveEntry = NULL;

    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4Vni;
    MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
            DestVmMac, sizeof (tMacAddr));
    pVxlanEcmpNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       (tRBElem *) & VxlanEcmpNveEntry, NULL);
    while ((pVxlanEcmpNveEntry != NULL)
           && (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
               (INT4) u4NveIndex)
           && (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber == u4Vni)
           &&
           (MEMCMP
            (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac, DestVmMac,
             sizeof (tMacAddr)) == 0))
    {
        pNextVxlanEcmpNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           (tRBElem *) pVxlanEcmpNveEntry, NULL);

        if (VxlanHwUpdateEcmpNveDatabase (pVxlanEcmpNveEntry, VXLAN_HW_DEL) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "RB Tree deletion failed\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                   pVxlanEcmpNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                            (UINT1 *) pVxlanEcmpNveEntry);

        pVxlanEcmpNveEntry = pNextVxlanEcmpNveEntry;
    }

}

/* ************************************************************************* *
 *  Function Name   : VxlanIsL3VniMappedWithContext                          *
 *  Description     : Called from CFA-IP task to check VRF Id is mapped      *
 *                    with L3VNI.                                            *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
VxlanIsL3VniMappedWithContext (UINT4 u4IfIndex, UINT4 *pu4VrfId,
                               UINT4 *pu4L3VniId)
{
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
    UINT2               u2VlanId = 0;
    UINT4               u4VrfId = 0;
    UINT4               u4L3Vni = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanIsL3VniMappedWithContext: Entry\n"));
    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        return VXLAN_FAILURE;
    }

    if (CfaGetVlanId (u4IfIndex, &u2VlanId) != CFA_SUCCESS)
    {
        return VXLAN_FAILURE;
    }
    if ((VxlanUtilGetVniFromVlan (u2VlanId, &u4L3Vni) == VXLAN_SUCCESS) &&
        (EvpnUtilGetVrfNameFromVrfEntry (u4L3Vni, au1VrfName) == VXLAN_SUCCESS)
        && (VcmIsVrfExist (au1VrfName, &u4VrfId) == VCM_TRUE))
    {
        *pu4VrfId = u4VrfId;
        *pu4L3VniId = u4L3Vni;
        VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                         "FUNC:VxlanIsL3VniMappedWithContext: Exit\n"));
        return VXLAN_SUCCESS;
    }
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : VxlanIsL3VniMappedWithVlan                             *
 *  Description     : To check whether L3VNI is mapped with the VLAN         *
 *  Input           : u2VLanId - Vlan Id                                     *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
VxlanIsL3VniMappedWithVlan (UINT2 u2VlanId)
{
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
    UINT4               u4VrfId = 0;
    UINT4               u4L3Vni = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanIsL3VniMappedWithContext: Entry\n"));
    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    if ((VxlanUtilGetVniFromVlan (u2VlanId, &u4L3Vni) == VXLAN_SUCCESS) &&
        (EvpnUtilGetVrfNameFromVrfEntry (u4L3Vni, au1VrfName) == VXLAN_SUCCESS)
        && (VcmIsVrfExist (au1VrfName, &u4VrfId) == VCM_TRUE))
    {
        return VXLAN_SUCCESS;
    }
    UNUSED_PARAM (u4VrfId);
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : VxlanProcessL3Pkt                                      *
 *  Description     : Called from CFA-IP task to post packet to VXLAN if     *
 *                    L3 packet is received and processed in IP module with  *
 *                    VrfId mapped with L3VNI.                               *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
VxlanProcessL3Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VrfId, UINT4 u4L3VniId)
{
    tVxlanIfMsg         VxlanIfMsg;
    UINT1               au1IpAddress[16];

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessL3Pkt: Entry\n"));

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    MEMSET (au1IpAddress, 0, 16);

    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, "VXLAN is not enabled\n"));
        return VXLAN_FAILURE;
    }

    /* Fille the message type, if index and packet buffer and enqueue the msg
     * to VXLAN queue
     * */
    VxlanIfMsg.u4MsgType = VXLAN_L3PKT_RCVD_EVENT;
    VxlanIfMsg.uVxlanMsg.VxlanL3PktInfo.pBuf = pBuf;
    VxlanIfMsg.uVxlanMsg.VxlanL3PktInfo.u4VrfId = u4VrfId;
    VxlanIfMsg.uVxlanMsg.VxlanL3PktInfo.u4L3VnId = u4L3VniId;
    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanProcessL3Pkt: Exit\n"));

    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : VxlanEvpnIsArpSuppressionEnabled                       *
 *  Description     : Called from ARP task to check whether ARP suppression  *
 *                    is enabled.                                            *
 *  Input           : u4IpAddr - Source IP Address                           *
 *  Output          : NONE                                                   *
 *  Returns         : TRUE / FALSE                                           *
 * ************************************************************************* */
INT4
VxlanEvpnIsArpSuppressionEnabled (UINT4 u4IpAddr)
{
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;

    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        return FALSE;
    }
    VxlanMainTaskLock ();
    u4IpAddr = OSIX_NTOHL (u4IpAddr);
    pVxlanEvpnArpSupLocalMacEntry = RBTreeGetFirst (gVxlanGlobals.
                                                    VxlanEvpnArpSupLocalMacTable);
    while (pVxlanEvpnArpSupLocalMacEntry != NULL)
    {
        if (MEMCMP (&u4IpAddr, pVxlanEvpnArpSupLocalMacEntry->au1IpAddress,
                    pVxlanEvpnArpSupLocalMacEntry->u1AddressLen))
        {
            VxlanMainTaskUnLock ();
            return TRUE;
        }
        pVxlanEvpnArpSupLocalMacEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
             (tRBElem *) pVxlanEvpnArpSupLocalMacEntry, NULL);
    }
    VxlanMainTaskUnLock ();
    return FALSE;
}

/* **************************************************************************
*  Function Name   : VxlanHandleL3VniVlanIntAdminStatus                     *
*  Description     : To handle Admin Status of Interface VLAN               * 
*                    Mapped with L3VNI                                      *
*  Input           : u4IfIndex,u4VniNumber,i4VlanNumber                     *
*  Output          : NONE                                                   *
*  Returns         : TRUE / FALSE                                           *
* ************************************************************************* */
VOID
VxlanHandleL3VniVlanIntAdminStatus (UINT4 u4L3Vni, INT4 i4IfMainAdminStatus)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNextNveEntry = NULL;
    tMacAddr            zeroAddr;
    UINT2               u2VlanId = 0;
#ifdef EVPN_VXLAN_WANTED
    UINT1               au1VrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    UINT4               u4VrfId = 0;
    tEvpnRoute          EvpnRoute;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveDynamicNveEntries: Entry\n"));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#ifdef EVPN_VXLAN_WANTED
    MEMSET (au1VrfName, 0, VXLAN_CLI_MAX_VRF_NAME_LEN);
    MEMSET (&EvpnRoute, 0, sizeof (tEvpnRoute));
#endif

    VxlanMainTaskLock ();
    if (i4IfMainAdminStatus == CFA_IF_DOWN)
    {
        /* Nve DB */
        /* Deletion of Non zero mac entries */
        VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4L3Vni;
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry, NULL);
        while (pVxlanNveEntry != NULL)
        {

            pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);
            if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber != u4L3Vni)
            {
                pVxlanNveEntry = pVxlanNextNveEntry;
                continue;
            }

            if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
                VXLAN_STRG_TYPE_VOL)
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
                    == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
                RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           pVxlanNveEntry);
                MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                    (UINT1 *) pVxlanNveEntry);
            }
            else
            {
                if (VxlanHwUpdateNveDatabase (pVxlanNveEntry,
                                              VXLAN_HW_DEL) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to delete Nve entry in h/w\n"));
                }
            }
#ifdef EVPN_VXLAN_WANTED
            if (pVxlanNveEntry->b1IsIrbRoute == TRUE)
            {
                EvpnRoute.u4L3VniNumber =
                    pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
                MEMCPY (&(EvpnRoute.VxlanDestVmMac),
                        &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                        VXLAN_ETHERNET_ADDR_SIZE);
                EvpnRoute.i4VxlanRemoteVtepAddressType =
                    pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;
                MEMCPY (&(EvpnRoute.au1VxlanDestAddress),
                        &(pVxlanNveEntry->au1VxlanIRBHostAddress),
                        VXLAN_IP4_ADDR_LEN);

                if (EvpnRoute.i4VxlanRemoteVtepAddressType ==
                    VXLAN_IPV4_UNICAST)
                {
                    MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                            &(pVxlanNveEntry->MibObject.
                              au1FsVxlanNveRemoteVtepAddress),
                            VXLAN_IP4_ADDR_LEN);
                }
                else
                {
                    MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                            &(pVxlanNveEntry->MibObject.
                              au1FsVxlanNveRemoteVtepAddress),
                            VXLAN_IP6_ADDR_LEN);

                }
                VxlanUtilHandleSymmerticDelIrbRoute (&EvpnRoute,
                                                     pVxlanNveEntry->MibObject.
                                                     i4FsVxlanNveIfIndex);
                pVxlanNveEntry->b1IsIrbRoute = FALSE;
            }
#endif
            pVxlanNveEntry = pVxlanNextNveEntry;
        }
    }
    if (i4IfMainAdminStatus == CFA_IF_UP)
    {
        /* Nve DB */
        VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4L3Vni;
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) & VxlanNveEntry, NULL);
        while (pVxlanNveEntry != NULL)
        {
            pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);

            if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber != u4L3Vni)
            {
                pVxlanNveEntry = pVxlanNextNveEntry;
                continue;
            }

#ifdef EVPN_VXLAN_WANTED        /* Need to update remove EVI */
            if ((pVxlanNveEntry->MibObject.
                 i4FsVxlanNveRemoteVtepAddressType == 0) &&
                (EvpnUtilGetVrfFromVni
                 (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                  &u4VrfId) != VXLAN_FAILURE))
            {
                if (VxlanUtilGetVrfFromVlan (u2VlanId, (UINT1 *) au1VrfName)
                    == VXLAN_SUCCESS)
                {
                    if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
                    {
                        VXLAN_TRC ((VXLAN_EVPN_TRC,
                                    " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
                    }
                }

                /* Send Notification to BGP to get MAC routes, when member VNI
                 * is added to an NVE interface */
                if (EVPN_NOTIFY_GET_MAC_CB != NULL)
                {
                    EVPN_NOTIFY_GET_MAC_CB (u4VrfId, pVxlanNveEntry->MibObject.
                                            u4FsVxlanNveVniNumber, zeroAddr,
                                            EVPN_BGP4_MAC_ADD);
                }
            }
#endif
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_ADD) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Nve entry in h/w\n"));

            }
            pVxlanNveEntry = pVxlanNextNveEntry;
        }

    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveDynamicNveEntries: Exit\n"));
    VxlanMainTaskUnLock ();
    return;
}

#endif /* EVPN_VXLAN_WANTED */

/* **************************************************************************
*  Function Name   : VxlanHandleVlanVniStatus                               *
*  Description     : To handle member vni removal/addition and port removal *
*                    port removal/addition from/to VLAN mapped with VNI     *
*  Input           : u4IfIndex,u4VniNumber,i4VlanNumber, u4Status           *
*  Output          : NONE                                                   *
*  Returns         : TRUE / FALSE                                           *
* ************************************************************************* */
VOID
VxlanHandleVlanVniStatus (UINT4 u4IfIndex, UINT4 u4VniNumber, INT4 i4VlanNumber,
                          UINT4 u4Status)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNextNveEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tMacAddr            zeroAddr;
#ifdef EVPN_VXLAN_WANTED
    tMacAddr            zeroMac;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pVxlanNextEcmpNveEntry = NULL;
    tEvpnRoute          EvpnRoute;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPortRemoveDynamicNveEntries: Entry\n"));
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
#ifdef EVPN_VXLAN_WANTED
    MEMSET (zeroMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (&EvpnRoute, 0, sizeof (tEvpnRoute));
#endif

    if (u4Status == VXLAN_HW_ADD)
    {
        VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
        pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                        FsVxlanNveTable,
                                        (tRBElem *) & VxlanNveEntry, NULL);
        while ((pVxlanNveEntry != NULL) &&
               ((pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
                 (INT4) u4IfIndex)))
        {
            pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanNveTable,
                                                pVxlanNveEntry, NULL);
            if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber != u4VniNumber)
            {
                pVxlanNveEntry = pVxlanNextNveEntry;
                continue;
            }
            pVxlanNveEntry->i4VlanId = i4VlanNumber;
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_ADD)
                == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to add Nve entry in h/w\n"));
            }
            pVxlanNveEntry = pVxlanNextNveEntry;
        }
        /* Multicast DB */
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;
        pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                          FsVxlanMCastTable,
                                          (tRBElem *) & VxlanMCastEntry, NULL);
        while ((pVxlanMCastEntry != NULL) &&
               (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                (INT4) u4IfIndex))
        {
            if (pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber ==
                u4VniNumber)
            {
                pVxlanMCastEntry->i4VlanId = i4VlanNumber;
                if (VxlanHwUpdateMcastDatabase
                    (pVxlanMCastEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to add Multicast entry in h/w\n"));
                }
            }
            pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanMCastTable,
                                              pVxlanMCastEntry, NULL);
        }
        /* Ingress Replica DB */
        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            (INT4) u4IfIndex;
        pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanInReplicaTable,
                                              (tRBElem *) & VxlanInReplicaEntry,
                                              NULL);
        while ((pVxlanInReplicaEntry != NULL) &&
               ((pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
                 (INT4) u4IfIndex)))
        {
            if (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber ==
                u4VniNumber)
            {
                pVxlanInReplicaEntry->i4VlanId = i4VlanNumber;
                if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry,
                                                    VXLAN_HW_ADD) ==
                    VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "Failed to add Ingress Replica entry in h/w\n"));
                }
            }
            pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  FsVxlanInReplicaTable,
                                                  pVxlanInReplicaEntry, NULL);
        }
        return;
    }
    /* Nve DB */
    /* Deletion of Non zero mac entries */
    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
    pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                    FsVxlanNveTable,
                                    (tRBElem *) & VxlanNveEntry, NULL);
    while ((pVxlanNveEntry != NULL) &&
           ((pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
             (INT4) u4IfIndex)))
    {
        pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                            FsVxlanNveTable,
                                            pVxlanNveEntry, NULL);
        if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber != u4VniNumber)
        {
            pVxlanNveEntry = pVxlanNextNveEntry;
            continue;
        }
        if (MEMCMP (pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                    zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0)
        {
            pVxlanNveEntry = pVxlanNextNveEntry;
            continue;
        }
        pVxlanNveEntry->i4VlanId = i4VlanNumber;
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
            VXLAN_STRG_TYPE_VOL)
        {
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL)
                == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
#ifdef EVPN_VXLAN_WANTED
            if (pVxlanNveEntry->b1IsIrbRoute == TRUE)
            {
                EvpnRoute.u4L3VniNumber =
                    pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
                MEMCPY (&(EvpnRoute.VxlanDestVmMac),
                        &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                        VXLAN_ETHERNET_ADDR_SIZE);
                EvpnRoute.i4VxlanRemoteVtepAddressType =
                    pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;
                MEMCPY (&(EvpnRoute.au1VxlanDestAddress),
                        &(pVxlanNveEntry->au1VxlanIRBHostAddress),
                        VXLAN_IP4_ADDR_LEN);

                if (EvpnRoute.i4VxlanRemoteVtepAddressType ==
                    VXLAN_IPV4_UNICAST)
                {
                    MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                            &(pVxlanNveEntry->MibObject.
                              au1FsVxlanNveRemoteVtepAddress),
                            VXLAN_IP4_ADDR_LEN);
                }
                else
                {
                    MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                            &(pVxlanNveEntry->MibObject.
                              au1FsVxlanNveRemoteVtepAddress),
                            VXLAN_IP6_ADDR_LEN);

                }
                VxlanUtilHandleSymmerticDelIrbRoute (&EvpnRoute,
                                                     pVxlanNveEntry->MibObject.
                                                     i4FsVxlanNveIfIndex);
                pVxlanNveEntry->b1IsIrbRoute = FALSE;
            }
#endif
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       pVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanNveEntry);
        }
        else
        {
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry,
                                          VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
        }
        pVxlanNveEntry = pVxlanNextNveEntry;
    }
    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;
    pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                    (tRBElem *) & VxlanNveEntry, NULL);
    /* Deletion of Zero mac entries */
    while ((pVxlanNveEntry != NULL) &&
           ((pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex ==
             (INT4) u4IfIndex) ||
            (pVxlanNveEntry->u4OrgNveIfIndex == u4IfIndex)))
    {
        pVxlanNextNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                            FsVxlanNveTable,
                                            pVxlanNveEntry, NULL);
        if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber != u4VniNumber)
        {
            pVxlanNveEntry = pVxlanNextNveEntry;
            continue;
        }

        pVxlanNveEntry->i4VlanId = i4VlanNumber;
        if (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
            VXLAN_STRG_TYPE_VOL)
        {
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       pVxlanNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanNveEntry);
        }
        else
        {
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
        }
        pVxlanNveEntry = pVxlanNextNveEntry;
    }

#ifdef EVPN_VXLAN_WANTED
    /* EcmpNve DB */
    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4IfIndex;
    pVxlanEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                        FsVxlanEcmpNveTable,
                                        (tRBElem *) & VxlanEcmpNveEntry, NULL);
    while ((pVxlanEcmpNveEntry != NULL) &&
           (pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex ==
            (INT4) u4IfIndex))
    {
        pVxlanNextEcmpNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                FsVxlanEcmpNveTable,
                                                pVxlanEcmpNveEntry, NULL);
        if (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber !=
            u4VniNumber)
        {
            pVxlanEcmpNveEntry = pVxlanNextEcmpNveEntry;
            continue;
        }

        if (MEMCMP (pVxlanEcmpNveEntry->MibObject.FsVxlanEcmpNveDestVmMac,
                    zeroMac, sizeof (tMacAddr)) != 0)
        {
            if (VxlanHwUpdateEcmpNveDatabase (pVxlanEcmpNveEntry,
                                              VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Nve entry in h/w\n"));
            }
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       pVxlanEcmpNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                                (UINT1 *) pVxlanEcmpNveEntry);
        }
        pVxlanEcmpNveEntry = pVxlanNextEcmpNveEntry;
    }
#endif
    /* Multicast DB */
    VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;
    pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                      FsVxlanMCastTable,
                                      (tRBElem *) & VxlanMCastEntry, NULL);
    while ((pVxlanMCastEntry != NULL) &&
           (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
            (INT4) u4IfIndex))
    {
        if (pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber == u4VniNumber)
        {
            pVxlanMCastEntry->i4VlanId = i4VlanNumber;
            if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Multicast entry in h/w\n"));
            }
        }
        pVxlanMCastEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                          FsVxlanMCastTable,
                                          pVxlanMCastEntry, NULL);
    }
    /* Ingress Replica DB */
    VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        (INT4) u4IfIndex;
    pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                          FsVxlanInReplicaTable,
                                          (tRBElem *) & VxlanInReplicaEntry,
                                          NULL);
    while ((pVxlanInReplicaEntry != NULL) &&
           ((pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
             (INT4) u4IfIndex)))
    {
        if (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber ==
            u4VniNumber)
        {
            pVxlanInReplicaEntry->i4VlanId = i4VlanNumber;
            if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry,
                                                VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Ingress Replica entry in h/w\n"));
            }
        }
        pVxlanInReplicaEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                              FsVxlanInReplicaTable,
                                              pVxlanInReplicaEntry, NULL);
    }
}

INT4
VxlanIsLoopbackUsed (UINT4 u4IfIndex)
{
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4Address = 0;
    UINT4               u4Port = 0;

    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        return VXLAN_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    if (NetIpIfInfo.u4Addr == 0)
    {
        return VXLAN_FAILURE;
    }

    u4Address = OSIX_NTOHL (NetIpIfInfo.u4Addr);
    pVxlanVtepEntry = (tVxlanFsVxlanVtepEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable);
    while (pVxlanVtepEntry != NULL)
    {
        if (MEMCMP (pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                    &u4Address,
                    pVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen) == 0)
        {
            return VXLAN_SUCCESS;
        }

        pVxlanVtepEntry =
            (tVxlanFsVxlanVtepEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
             (tRBElem *) pVxlanVtepEntry, NULL);
    }

    return VXLAN_FAILURE;
}

VOID
VxlanIpRtChgEventHandler (tNetIpv4RtInfo * pNetIpv4RtInfo,
                          tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType)
{
    tVxlanIfMsg         VxlanIfMsg;

    /* Check whether VXLAN is enabled */
    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        return;
    }

    if (pNetIpv4RtInfo->u4DestMask != 0xffffffff)
    {
        return;
    }

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    if (u1CmdType == NETIPV4_MODIFY_ROUTE)
    {
        VxlanIfMsg.uVxlanMsg.u4IfIndex = pNetIpv4RtInfo1->u4RtIfIndx;
        VxlanIfMsg.u4Addr = pNetIpv4RtInfo1->u4DestNet;
    }
    else
    {
        VxlanIfMsg.uVxlanMsg.u4IfIndex = pNetIpv4RtInfo->u4RtIfIndx;
        VxlanIfMsg.u4Addr = pNetIpv4RtInfo->u4DestNet;
    }
    VxlanIfMsg.u1CmdType = u1CmdType;
    VxlanIfMsg.u4MsgType = VXLAN_ROUTE_DEL_EVENT;
    VxlanEnqueMsg (&VxlanIfMsg);
    return;
}

VOID
VxlanHandleRtChgEvent (UINT4 u4DestNet, UINT4 u4RtIfIndex, UINT1 u1CmdType)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanTmpNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNextTmpNveEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaNextEntry = NULL;
    UINT4               u4TempAddr = 0;
    tMacAddr            zeroAddr;

    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    /* Remove non zero dmac entries from CP and NP */
    pVxlanFsVxlanTmpNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
    while (pVxlanFsVxlanTmpNveEntry != NULL)
    {
        pVxlanFsVxlanNextTmpNveEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);

        MEMCPY (&u4TempAddr,
                pVxlanFsVxlanTmpNveEntry->MibObject.
                au1FsVxlanNveRemoteVtepAddress, VXLAN_IP4_ADDR_LEN);
        u4TempAddr = OSIX_HTONL (u4TempAddr);

        if ((u4TempAddr == u4DestNet) &&
            (pVxlanFsVxlanTmpNveEntry->u4VxlanTunnelIfIndex == u4RtIfIndex) &&
            (MEMCMP (pVxlanFsVxlanTmpNveEntry->MibObject.FsVxlanNveDestVmMac,
                     zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) != 0) &&
            (pVxlanFsVxlanTmpNveEntry->MibObject.
             i4FsVxlanNveStorageType == VXLAN_STRG_TYPE_VOL) &&
            (u1CmdType == NETIPV4_DELETE_ROUTE))
        {
            if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanTmpNveEntry,
                                          VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "Failed to delete Nve entry from h/w\r\n"));
            }
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       pVxlanFsVxlanTmpNveEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanTmpNveEntry);
        }
        pVxlanFsVxlanTmpNveEntry = pVxlanFsVxlanNextTmpNveEntry;
    }

    /* Reprogram the Zero mac entry in NP */
    pVxlanFsVxlanTmpNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);

    while (pVxlanFsVxlanTmpNveEntry != NULL)
    {
        pVxlanFsVxlanNextTmpNveEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanFsVxlanTmpNveEntry, NULL);

        MEMCPY (&u4TempAddr,
                pVxlanFsVxlanTmpNveEntry->MibObject.
                au1FsVxlanNveRemoteVtepAddress, VXLAN_IP4_ADDR_LEN);
        u4TempAddr = OSIX_HTONL (u4TempAddr);

        if (u4TempAddr == u4DestNet)
        {
            if (u1CmdType == NETIPV4_ADD_ROUTE)
            {
                if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanTmpNveEntry,
                                              VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "Failed to add Nve entry from h/w\r\n"));
                }
            }
            else if ((u1CmdType == NETIPV4_DELETE_ROUTE) &&
                     (pVxlanFsVxlanTmpNveEntry->u4VxlanTunnelIfIndex ==
                      u4RtIfIndex))
            {
                if (VxlanHwUpdateNveDatabase (pVxlanFsVxlanTmpNveEntry,
                                              VXLAN_HW_DEL) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "Failed to add Nve entry from h/w\r\n"));
                }

            }

        }
        pVxlanFsVxlanTmpNveEntry = pVxlanFsVxlanNextTmpNveEntry;
    }
    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);
    while (pVxlanInReplicaEntry != NULL)
    {
        pVxlanInReplicaNextEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) pVxlanInReplicaEntry, NULL);
        MEMCPY (&u4TempAddr,
                pVxlanInReplicaEntry->MibObject.
                au1FsVxlanInReplicaRemoteVtepAddress, VXLAN_IP4_ADDR_LEN);
        u4TempAddr = OSIX_HTONL (u4TempAddr);
        if (u4TempAddr == u4DestNet)
        {
            if (u1CmdType == NETIPV4_ADD_ROUTE)
            {
                if (VxlanHwUpdateInReplicaDatabase
                    (pVxlanInReplicaEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "Failed to add Ingress Replica entry from h/w\r\n"));

                }
            }
            else if ((u1CmdType == NETIPV4_DELETE_ROUTE) &&
                     (pVxlanInReplicaEntry->u4VxlanTunnelIfIndex ==
                      u4RtIfIndex))
            {
                if (VxlanHwUpdateInReplicaDatabase
                    (pVxlanInReplicaEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_UTIL_TRC,
                                "Failed to delete Ingress Replica entry from h/w\r\n"));

                }
                if (VXLAN_GET_NODE_STATUS () == RM_STANDBY)
                {
                    pVxlanInReplicaEntry->bRouteChanged = VXLAN_TRUE;
                }
            }
        }
        pVxlanInReplicaEntry = pVxlanInReplicaNextEntry;
    }
    return;
}

#ifdef EVPN_VXLAN_WANTED
/* ************************************************************************* *
 *  Function Name   : VxlanUtilHandleSymmerticAddIrbRoute                    *
 *  Description     : Called from BGP to program MAC/IP routes in VXLAN NVE  *
 *                    database; and send add L3 route indication to RTM and  *
 *                    ARP module.                                            *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanUtilHandleSymmerticAddIrbRoute (tEvpnRoute * pEvpnRoute, UINT4 u4NveIndex)
{
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
#ifdef ARP_WANTED
    tARP_DATA           ArpData;
#endif
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4VrfId = 0;
    UINT4               u4VlanIfIndex = 0;
    UINT2               u2VlanId = 0;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif
    MEMSET (au1VrfName, 0, EVPN_MAX_VRF_NAME_LEN);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilHandleSymmerticAddIrbRoute: Entry\n"));
    if (VXLAN_FAILURE ==
        EvpnUtilGetVrfNameFromVrfEntry (pEvpnRoute->u4L3VniNumber, au1VrfName))
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VRF Entry is not found for VNI"
                         "id - %d ", pEvpnRoute->u4L3VniNumber));
        return;
    }
    if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VCM_FALSE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VRF is not exist in VCM for VRF"
                         " Name - %s ", au1VrfName));
        return;
    }

    pVxlanNveEntry =
        VxlanUtilGetNveDatabase (pEvpnRoute->u4L3VniNumber,
                                 pEvpnRoute->VxlanDestVmMac, u4NveIndex);

    if (pVxlanNveEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "NVE Entry is not found for L3VNI - "
                         "%d u4NveIndex %d", pEvpnRoute->u4L3VniNumber,
                         u4NveIndex));
        return;
    }

    if (VxlanUtilGetVlanFromVni (pVxlanNveEntry->MibObject.
                                 u4FsVxlanNveVniNumber,
                                 &u2VlanId) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VLAN is not mapped with VNI %su",
                         u2VlanId));
        return;
    }
    u4VlanIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);

    MEMCPY (pVxlanNveEntry->MibObject.au1FsEvpnVxlanVrfName,
            au1VrfName, EVPN_MAX_VRF_NAME_LEN);

    if (pEvpnRoute->i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
    {
#ifdef ARP_WANTED
        MEMSET (&ArpData, 0, sizeof (tARP_DATA));
        MEMCPY (&ArpData.u4IpAddr, pEvpnRoute->au1VxlanRemoteVtepAddress,
                sizeof (UINT4));
        ArpData.u4IpAddr = OSIX_HTONL (ArpData.u4IpAddr);
        ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4VlanIfIndex);
        ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
        ArpData.i1Hwalen = MAC_ADDR_LEN;
        ArpData.i1State = ARP_EVPN;
        MEMCPY (ArpData.i1Hw_addr, (INT1 *) pEvpnRoute->VxlanDestVmMac,
                ArpData.i1Hwalen);
        ArpData.u1RowStatus = ACTIVE;
        arp_add (ArpData);
#endif
        /* Convert Destination IP to Subnet Address */
        NetIpRtInfo.u4DestMask = 0xfffffffc;    /*30 */
        MEMCPY (&(NetIpRtInfo.u4DestNet), pEvpnRoute->au1VxlanDestAddress,
                sizeof (UINT4));
        NetIpRtInfo.u4DestNet = NetIpRtInfo.u4DestNet & NetIpRtInfo.u4DestMask;
        MEMCPY (&(NetIpRtInfo.u4NextHop), pEvpnRoute->au1VxlanRemoteVtepAddress,
                sizeof (UINT4));
        NetIpRtInfo.u4NextHop = OSIX_HTONL (NetIpRtInfo.u4NextHop);
        NetIpRtInfo.u2RtProto = VXLAN_ID;
        NetIpRtInfo.u4RtIfIndx = (UINT2) CFA_IF_IPPORT (u4VlanIfIndex);
        NetIpRtInfo.i4Metric1 = 0;
        NetIpRtInfo.u4Tos = 0;
        NetIpRtInfo.u4RtAge = 0;
        NetIpRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
        NetIpRtInfo.u4RouteTag = 0;
        NetIpRtInfo.u1BitMask = 0;
        NetIpRtInfo.u4RowStatus = ACTIVE;

        if (u4VrfId != 0)
        {
            NetIpRtInfo.u4ContextId = u4VrfId;
        }
        if (NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &NetIpRtInfo) ==
            NETIPV4_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "Route Addition failed for IRB entry "
                             "VNI id - %d, Remote Router IP - %s, NVE if index"
                             "- %d\n", pEvpnRoute->u4L3VniNumber,
                             VXLAN_IP_PRINT_ADDR (pEvpnRoute->
                                                  au1VxlanRemoteVtepAddress),
                             u4NveIndex));
            return;
        }
    }
    else
    {
#ifdef IP6_WANTED
        MEMCPY (Ip6Addr.u1_addr, pEvpnRoute->au1VxlanRemoteVtepAddress,
                VXLAN_IP6_ADDR_LEN);
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "ND/IPv6 Route Addtion is not "
                         "supported for VNI id - %d, Remote Router IP - %s, "
                         "NVE if index - %d\n", pEvpnRoute->u4L3VniNumber,
                         Ip6PrintAddr (&Ip6Addr), u4NveIndex));
        return;
#endif
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilHandleSymmerticAddIrbRoute: Exit\n"));
    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilHandleSymmerticDelIrbRoute                    *
 *  Description     : Called from BGP to delete MAC/IP routes in VXLAN NVE   *
 *                    database; and send del L3 route indication to RTM and  *
 *                    ARP module.                                            *
 *  Input           : pEvpnRoute - MAC route Info                            *
 *  Output          : NONE                                                   *
 *  Returns         : NONE                                                   *
 * ************************************************************************* */
VOID
VxlanUtilHandleSymmerticDelIrbRoute (tEvpnRoute * pEvpnRoute, UINT4 u4NveIndex)
{
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
#ifdef ARP_WANTED
    tARP_DATA           ArpData;
#endif
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4VrfId = 0;
    UINT4               u4VlanIfIndex = 0;
    UINT2               u2VlanId = 0;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif
    MEMSET (au1VrfName, 0, EVPN_MAX_VRF_NAME_LEN);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilHandleSymmerticDelIrbRoute: Entry\n"));

    if (VXLAN_FAILURE ==
        EvpnUtilGetVrfNameFromVrfEntry (pEvpnRoute->u4L3VniNumber, au1VrfName))
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VRF Entry is not found for VNI"
                         "id - %d ", pEvpnRoute->u4L3VniNumber));
        pVxlanNveEntry =
            VxlanUtilGetNveDatabase (pEvpnRoute->u4L3VniNumber,
                                     pEvpnRoute->VxlanDestVmMac, u4NveIndex);
        if ((pVxlanNveEntry == NULL) ||
            (MEMCMP
             (pVxlanNveEntry->MibObject.au1FsEvpnVxlanVrfName, au1VrfName,
              EVPN_MAX_VRF_NAME_LEN) == 0))
        {
            return;
        }
        else
        {
            MEMCPY (au1VrfName, pVxlanNveEntry->MibObject.au1FsEvpnVxlanVrfName,
                    EVPN_MAX_VRF_NAME_LEN);
        }
    }
    if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VCM_FALSE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VRF is not exist in VCM for VRF"
                         " Name - %s ", au1VrfName));
        return;
    }

    if (VxlanUtilGetVlanFromVni (pEvpnRoute->u4L3VniNumber,
                                 &u2VlanId) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "VLAN is not mapped with VNI %su",
                         u2VlanId));
        return;
    }
    u4VlanIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);

    if (pEvpnRoute->i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
    {
#ifdef ARP_WANTED
        MEMSET (&ArpData, 0, sizeof (tARP_DATA));
        MEMCPY (&ArpData.u4IpAddr, pEvpnRoute->au1VxlanRemoteVtepAddress,
                sizeof (UINT4));
        ArpData.u4IpAddr = OSIX_HTONL (ArpData.u4IpAddr);
        ArpData.u2Port = (UINT2) CFA_IF_IPPORT (u4VlanIfIndex);
        ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
        ArpData.i1Hwalen = MAC_ADDR_LEN;
        ArpData.i1State = ARP_EVPN;
        MEMCPY (ArpData.i1Hw_addr, (INT1 *) pEvpnRoute->VxlanDestVmMac,
                ArpData.i1Hwalen);
        ArpData.u1RowStatus = DESTROY;
        ArpDeleteEvpnArpCache (ArpData);
#endif

        /* Convert Destination IP to Subnet Address */
        NetIpRtInfo.u4DestMask = 0xfffffffc;
        MEMCPY (&(NetIpRtInfo.u4DestNet), pEvpnRoute->au1VxlanDestAddress,
                sizeof (UINT4));
        NetIpRtInfo.u4DestNet = NetIpRtInfo.u4DestNet & NetIpRtInfo.u4DestMask;
        MEMCPY (&(NetIpRtInfo.u4NextHop), pEvpnRoute->au1VxlanRemoteVtepAddress,
                sizeof (UINT4));
        NetIpRtInfo.u4NextHop = OSIX_HTONL (NetIpRtInfo.u4NextHop);
        NetIpRtInfo.u2RtProto = VXLAN_ID;
        NetIpRtInfo.u4RtIfIndx = (UINT2) CFA_IF_IPPORT (u4VlanIfIndex);
        NetIpRtInfo.i4Metric1 = 0;
        NetIpRtInfo.u4Tos = 0;
        NetIpRtInfo.u4RtAge = 0;
        NetIpRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
        NetIpRtInfo.u4RouteTag = 0;
        NetIpRtInfo.u1BitMask = 0;
        NetIpRtInfo.u4RowStatus = IPFWD_DESTROY;

        if (u4VrfId != 0)
        {
            NetIpRtInfo.u4ContextId = u4VrfId;
        }
        if (NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetIpRtInfo) ==
            NETIPV4_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "Route Deletion failed for IRB entry "
                             "VNI id - %d, Remote Router IP - %s, NVE if index"
                             "- %d\n", pEvpnRoute->u4L3VniNumber,
                             VXLAN_IP_PRINT_ADDR (pEvpnRoute->
                                                  au1VxlanRemoteVtepAddress),
                             u4NveIndex));
            return;
        }
    }
    else
    {
#ifdef IP6_WANTED
        MEMCPY (Ip6Addr.u1_addr, pEvpnRoute->au1VxlanRemoteVtepAddress,
                VXLAN_IP6_ADDR_LEN);
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "ND/IPv6 Route Deletion is not "
                         "supported for VNI id - %d, Remote Router IP - %s, "
                         "NVE if index - %d\n", pEvpnRoute->u4L3VniNumber,
                         Ip6PrintAddr (&Ip6Addr), u4NveIndex));
        return;
#endif
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilHandleSymmerticDelIrbRoute: Exit\n"));
    return;
}
#endif
