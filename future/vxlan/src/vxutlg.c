/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxutlg.c,v 1.8 2018/01/05 09:57:12 siva Exp $
*
* Description: This file contains utility functions used by protocol Vxlan
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
 Function    :  VxlanUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
VxlanUtlCreateRBTree ()
{

    if (VxlanFsVxlanVtepTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsVxlanNveTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsVxlanMCastTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsVxlanVniVlanMapTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    if (VxlanFsVxlanInReplicaTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
#ifdef EVPN_VXLAN_WANTED
    if (VxlanFsEvpnVxlanEviVniMapTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsEvpnVxlanBgpRTTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsEvpnVxlanVrfTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsEvpnVxlanVrfRTTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsEvpnVxlanMultihomedPeerTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (VxlanFsVxlanEcmpNveTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    if (VxlanEvpnArpSupLocalMacTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

#endif /* EVPN_VXLAN_WANTED */
#ifdef NPAPI_WANTED
    if (VxlanFsVxlanNvePortTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanVtepTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanVtepTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanVtepTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanVtepEntry, MibObject.FsVxlanVtepTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsVxlanVtepTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanNveTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanNveTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanNveTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanNveEntry, MibObject.FsVxlanNveTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsVxlanNveTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanMCastTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanMCastTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanMCastTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanMCastEntry,
                       MibObject.FsVxlanMCastTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsVxlanMCastTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanVniVlanMapTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanVniVlanMapTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanVniVlanMapTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanVniVlanMapEntry,
                       MibObject.FsVxlanVniVlanMapTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanVniVlanMapTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanInReplicaTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanInReplicaTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanInReplicaTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanInReplicaEntry,
                       MibObject.FsVxlanInReplicaTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanInReplicaTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanVniVlanPortTableCreate
 Input       :  None
 Description :  This function creates the VxlanVniVlanPortTable
                i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanVniVlanPortTableCreate (tVxlanFsVxlanVniVlanMapEntry *
                                    pVxlanVniVlantEntry)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanVniVlanPortEntry, VxlanVniVlanPortNode);

    if ((pVxlanVniVlantEntry->VxlanVniVlanPortTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanVniVlanPortTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanVniVlanPortMacTableCreate
 Input       :  None
 Description :  This function creates the VxlanVniVlanPortMacTable
                i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanVniVlanPortMacTableCreate (tVxlanFsVxlanVniVlanMapEntry *
                                       pVxlanVniVlantEntry)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanVniVlanPortMacEntry, VxlanVniVlanPortMacNode);

    if ((pVxlanVniVlantEntry->VxlanVniVlanPortMacTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanVniVlanPortMacTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanNvePortTableCreate
 Input       :  None
 Description :  This function creates the VxlanNvePortTable
                i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanNvePortTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tVxlanNvePortEntry, VxlanNvePortTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanNvePortTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsVxlanVniVlanPortTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                VxlanVniVlanPortTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanVniVlanPortTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVxlanVniVlanPortEntry *pNode1 = (tVxlanVniVlanPortEntry *) pRBElem1;
    tVxlanVniVlanPortEntry *pNode2 = (tVxlanVniVlanPortEntry *) pRBElem2;

    if (pNode1->u4IfIndex > pNode2->u4IfIndex)
    {
        return 1;
    }
    else if (pNode1->u4IfIndex < pNode2->u4IfIndex)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsVxlanVniVlanPortMacTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                VxlanVniVlanPortMacTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanVniVlanPortMacTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVxlanVniVlanPortMacEntry *pNode1 = (tVxlanVniVlanPortMacEntry *) pRBElem1;
    tVxlanVniVlanPortMacEntry *pNode2 = (tVxlanVniVlanPortMacEntry *) pRBElem2;

    if (pNode1->u4IfIndex > pNode2->u4IfIndex)
    {
        return 1;
    }
    else if (pNode1->u4IfIndex < pNode2->u4IfIndex)
    {
        return -1;
    }
    if (MEMCMP (pNode1->MacAddress, pNode2->MacAddress,
                sizeof (pNode1->MacAddress)) > 0)
    {
        return 1;
    }
    else if (MEMCMP (pNode1->MacAddress, pNode2->MacAddress,
                     sizeof (pNode1->MacAddress)) < 0)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsVxlanNvePortTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                VxlanNvePortTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanNvePortTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVxlanNvePortEntry *pNode1 = (tVxlanNvePortEntry *) pRBElem1;
    tVxlanNvePortEntry *pNode2 = (tVxlanNvePortEntry *) pRBElem2;

    if (pNode1->i4RemoteVtepAddressLen > pNode2->i4RemoteVtepAddressLen)
    {
        return 1;
    }
    else if (pNode1->i4RemoteVtepAddressLen < pNode2->i4RemoteVtepAddressLen)
    {
        return -1;
    }
    if (MEMCMP (pNode1->au1RemoteVtepAddress, pNode2->au1RemoteVtepAddress,
                sizeof (pNode1->i4RemoteVtepAddressLen)) > 0)
    {
        return 1;
    }
    if (MEMCMP (pNode1->au1RemoteVtepAddress, pNode2->au1RemoteVtepAddress,
                sizeof (pNode1->i4RemoteVtepAddressLen)) < 0)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  VxlanVniVlanPortTableAdd
 Input       :  pVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
tVxlanVniVlanPortEntry *
VxlanVniVlanPortTableAdd (tVxlanFsVxlanVniVlanMapEntry * pVxlanVniVlantEntry,
                          UINT4 u4IfIndex)
{
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;

    /* Allocate memory for the new node */
    pVxlanVniVlanPortEntry =
        (tVxlanVniVlanPortEntry *)
        MemAllocMemBlk (VXLAN_VNIVLANPORTTABLE_POOLID);

    if (pVxlanVniVlanPortEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanVniVlanPortTableCreate: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        pVxlanVniVlanPortEntry->u4IfIndex = u4IfIndex;
        if (RBTreeAdd
            (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
             (tRBElem *) pVxlanVniVlanPortEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanVniVlanPortTableCreate: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_VNIVLANPORTTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortEntry);
            return NULL;
        }
        return pVxlanVniVlanPortEntry;
    }
}

/****************************************************************************
 Function    :  VxlanVniVlanPortMacTableAdd
 Input       :  pVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanVniVlanPortMacEntry *
VxlanVniVlanPortMacTableAdd (tVxlanFsVxlanVniVlanMapEntry * pVxlanVniVlantEntry,
                             UINT4 u4IfIndex, tMacAddr VxlanMac)
{
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry = NULL;

    /* Allocate memory for the new node */
    pVxlanVniVlanPortMacEntry =
        (tVxlanVniVlanPortMacEntry *)
        MemAllocMemBlk (VXLAN_VNIVLANPORTMACTABLE_POOLID);

    if (pVxlanVniVlanPortMacEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanVniVlanPortMacTableCreate: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        pVxlanVniVlanPortMacEntry->u4IfIndex = u4IfIndex;
        MEMCPY (pVxlanVniVlanPortMacEntry->MacAddress, VxlanMac,
                sizeof (tMacAddr));
        if (RBTreeAdd
            (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
             (tRBElem *) pVxlanVniVlanPortMacEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanVniVlanPortMacTableCreate: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_VNIVLANPORTMACTABLE_POOLID,
                                (UINT1 *) pVxlanVniVlanPortMacEntry);
            return NULL;
        }
        return pVxlanVniVlanPortMacEntry;
    }
}

/****************************************************************************
 Function    :  VxlanNvePortTableAdd
 Input       :  pVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanNvePortEntry *
VxlanNvePortTableAdd (UINT1 *pau1Address, INT4 i4AddrLen)
{
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;

    /* Allocate memory for the new node */
    pVxlanNvePortEntry =
        (tVxlanNvePortEntry *) MemAllocMemBlk (VXLAN_NVEPORTTABLE_POOLID);

    if (pVxlanNvePortEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanNvePortTableCreate: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (&(pVxlanNvePortEntry->au1RemoteVtepAddress), pau1Address,
                i4AddrLen);
        pVxlanNvePortEntry->i4RemoteVtepAddressLen = i4AddrLen;
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
             (tRBElem *) pVxlanNvePortEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanNvePortTableCreate: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_NVEPORTTABLE_POOLID,
                                (UINT1 *) pVxlanNvePortEntry);
            return NULL;
        }
        return pVxlanNvePortEntry;
    }
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanFsEvpnVxlanEviVniMapTableCreate
 Input       :  None
 Description :  This function creates the FsEvpnVxlanEviVniMapTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsEvpnVxlanEviVniMapTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsEvpnVxlanEviVniMapEntry,
                       MibObject.FsEvpnVxlanEviVniMapTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsEvpnVxlanEviVniMapTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanBgpRTTableCreate
 Input       :  None
 Description :  This function creates the FsEvpnVxlanBgpRTTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsEvpnVxlanBgpRTTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsEvpnVxlanBgpRTEntry,
                       MibObject.FsEvpnVxlanBgpRTTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsEvpnVxlanBgpRTTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanVrfTableCreate
 Input       :  None
 Description :  This function creates the FsEvpnVxlanVrfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsEvpnVxlanVrfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsEvpnVxlanVrfEntry,
                       MibObject.FsEvpnVxlanVrfTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsEvpnVxlanVrfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanVrfRTTableCreate
 Input       :  None
 Description :  This function creates the FsEvpnVxlanVrfRTTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsEvpnVxlanVrfRTTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsEvpnVxlanVrfRTEntry,
                       MibObject.FsEvpnVxlanVrfRTTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsEvpnVxlanVrfRTTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanMultihomedPeerTableCreate
 Input       :  None
 Description :  This function creates the FsEvpnVxlanMultihomedPeerTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsEvpnVxlanMultihomedPeerTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsEvpnVxlanMultihomedPeerTable,
                       MibObject.FsEvpnVxlanMultihomedPeerTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsEvpnVxlanMultihomedPeerTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanFsVxlanEcmpNveTableCreate
 Input       :  None
 Description :  This function creates the FsVxlanEcmpNveTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
VxlanFsVxlanEcmpNveTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanFsVxlanEcmpNveEntry,
                       MibObject.FsVxlanEcmpNveTableNode);

    if ((gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVxlanEcmpNveTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanEvpnArpSupLocalMacTableCreate
 Input       :  None
 Description :  This function creates the DecapPktsLogTable
                i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
VxlanEvpnArpSupLocalMacTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tVxlanEvpnArpSupLocalMacEntry,
                       VxlanEvpnArpSupLocalMacNode);

    if ((gVxlanGlobals.VxlanEvpnArpSupLocalMacTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               VxlanEvpnArpSupLocalMacTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  FsVxlanVtepTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVxlanVtepTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanVtepTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanVtepEntry *pFsVxlanVtepEntry1 =
        (tVxlanFsVxlanVtepEntry *) pRBElem1;
    tVxlanFsVxlanVtepEntry *pFsVxlanVtepEntry2 =
        (tVxlanFsVxlanVtepEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanVtepEntry1->MibObject.i4FsVxlanVtepNveIfIndex >
        pFsVxlanVtepEntry2->MibObject.i4FsVxlanVtepNveIfIndex)
    {
        return 1;
    }
    else if (pFsVxlanVtepEntry1->MibObject.i4FsVxlanVtepNveIfIndex <
             pFsVxlanVtepEntry2->MibObject.i4FsVxlanVtepNveIfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsVxlanNveTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVxlanNveTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanNveTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanNveEntry *pFsVxlanNveEntry1 =
        (tVxlanFsVxlanNveEntry *) pRBElem1;
    tVxlanFsVxlanNveEntry *pFsVxlanNveEntry2 =
        (tVxlanFsVxlanNveEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanNveEntry1->MibObject.i4FsVxlanNveIfIndex >
        pFsVxlanNveEntry2->MibObject.i4FsVxlanNveIfIndex)
    {
        return 1;
    }
    else if (pFsVxlanNveEntry1->MibObject.i4FsVxlanNveIfIndex <
             pFsVxlanNveEntry2->MibObject.i4FsVxlanNveIfIndex)
    {
        return -1;
    }

    if (pFsVxlanNveEntry1->MibObject.u4FsVxlanNveVniNumber >
        pFsVxlanNveEntry2->MibObject.u4FsVxlanNveVniNumber)
    {
        return 1;
    }
    else if (pFsVxlanNveEntry1->MibObject.u4FsVxlanNveVniNumber <
             pFsVxlanNveEntry2->MibObject.u4FsVxlanNveVniNumber)
    {
        return -1;
    }

    if (MEMCMP
        (pFsVxlanNveEntry1->MibObject.FsVxlanNveDestVmMac,
         pFsVxlanNveEntry2->MibObject.FsVxlanNveDestVmMac,
         VXLAN_ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsVxlanNveEntry1->MibObject.FsVxlanNveDestVmMac,
              pFsVxlanNveEntry2->MibObject.FsVxlanNveDestVmMac,
              VXLAN_ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsVxlanMCastTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVxlanMCastTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanMCastTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanMCastEntry *pFsVxlanMCastEntry1 =
        (tVxlanFsVxlanMCastEntry *) pRBElem1;
    tVxlanFsVxlanMCastEntry *pFsVxlanMCastEntry2 =
        (tVxlanFsVxlanMCastEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanMCastEntry1->MibObject.i4FsVxlanMCastNveIfIndex >
        pFsVxlanMCastEntry2->MibObject.i4FsVxlanMCastNveIfIndex)
    {
        return 1;
    }
    else if (pFsVxlanMCastEntry1->MibObject.i4FsVxlanMCastNveIfIndex <
             pFsVxlanMCastEntry2->MibObject.i4FsVxlanMCastNveIfIndex)
    {
        return -1;
    }

    if (pFsVxlanMCastEntry1->MibObject.u4FsVxlanMCastVniNumber >
        pFsVxlanMCastEntry2->MibObject.u4FsVxlanMCastVniNumber)
    {
        return 1;
    }
    else if (pFsVxlanMCastEntry1->MibObject.u4FsVxlanMCastVniNumber <
             pFsVxlanMCastEntry2->MibObject.u4FsVxlanMCastVniNumber)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsVxlanVniVlanMapTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVxlanVniVlanMapTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanVniVlanMapTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanVniVlanMapEntry *pFsVxlanVniVlanMapEntry1 =
        (tVxlanFsVxlanVniVlanMapEntry *) pRBElem1;
    tVxlanFsVxlanVniVlanMapEntry *pFsVxlanVniVlanMapEntry2 =
        (tVxlanFsVxlanVniVlanMapEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanVniVlanMapEntry1->MibObject.i4FsVxlanVniVlanMapVlanId >
        pFsVxlanVniVlanMapEntry2->MibObject.i4FsVxlanVniVlanMapVlanId)
    {
        return 1;
    }
    else if (pFsVxlanVniVlanMapEntry1->MibObject.i4FsVxlanVniVlanMapVlanId <
             pFsVxlanVniVlanMapEntry2->MibObject.i4FsVxlanVniVlanMapVlanId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsVxlanInReplicaTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVxlanInReplicaTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVxlanInReplicaTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanInReplicaEntry *pFsVxlanInReplicaEntry1 =
        (tVxlanFsVxlanInReplicaEntry *) pRBElem1;
    tVxlanFsVxlanInReplicaEntry *pFsVxlanInReplicaEntry2 =
        (tVxlanFsVxlanInReplicaEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanInReplicaEntry1->MibObject.i4FsVxlanInReplicaNveIfIndex >
        pFsVxlanInReplicaEntry2->MibObject.i4FsVxlanInReplicaNveIfIndex)
    {
        return 1;
    }
    else if (pFsVxlanInReplicaEntry1->MibObject.i4FsVxlanInReplicaNveIfIndex <
             pFsVxlanInReplicaEntry2->MibObject.i4FsVxlanInReplicaNveIfIndex)
    {
        return -1;
    }

    if (pFsVxlanInReplicaEntry1->MibObject.u4FsVxlanInReplicaVniNumber >
        pFsVxlanInReplicaEntry2->MibObject.u4FsVxlanInReplicaVniNumber)
    {
        return 1;
    }
    else if (pFsVxlanInReplicaEntry1->MibObject.u4FsVxlanInReplicaVniNumber <
             pFsVxlanInReplicaEntry2->MibObject.u4FsVxlanInReplicaVniNumber)
    {
        return -1;
    }
    if (pFsVxlanInReplicaEntry1->MibObject.i4FsVxlanInReplicaIndex >
        pFsVxlanInReplicaEntry2->MibObject.i4FsVxlanInReplicaIndex)
    {
        return 1;
    }
    else if (pFsVxlanInReplicaEntry1->MibObject.i4FsVxlanInReplicaIndex <
             pFsVxlanInReplicaEntry2->MibObject.i4FsVxlanInReplicaIndex)
    {
        return -1;
    }
    UNUSED_PARAM (i4MaxLength);
    return 0;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  FsEvpnVxlanEviVniMapTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanEviVniMapTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsEvpnVxlanEviVniMapTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsEvpnVxlanEviVniMapEntry *pFsEvpnVxlanEviVniMapEntry1 =
        (tVxlanFsEvpnVxlanEviVniMapEntry *) pRBElem1;
    tVxlanFsEvpnVxlanEviVniMapEntry *pFsEvpnVxlanEviVniMapEntry2 =
        (tVxlanFsEvpnVxlanEviVniMapEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsEvpnVxlanEviVniMapEntry1->MibObject.i4FsEvpnVxlanEviVniMapEviIndex >
        pFsEvpnVxlanEviVniMapEntry2->MibObject.i4FsEvpnVxlanEviVniMapEviIndex)
    {
        return 1;
    }
    else if (pFsEvpnVxlanEviVniMapEntry1->MibObject.
             i4FsEvpnVxlanEviVniMapEviIndex <
             pFsEvpnVxlanEviVniMapEntry2->MibObject.
             i4FsEvpnVxlanEviVniMapEviIndex)
    {
        return -1;
    }

    if (pFsEvpnVxlanEviVniMapEntry1->MibObject.u4FsEvpnVxlanEviVniMapVniNumber >
        pFsEvpnVxlanEviVniMapEntry2->MibObject.u4FsEvpnVxlanEviVniMapVniNumber)
    {
        return 1;
    }
    else if (pFsEvpnVxlanEviVniMapEntry1->MibObject.
             u4FsEvpnVxlanEviVniMapVniNumber <
             pFsEvpnVxlanEviVniMapEntry2->MibObject.
             u4FsEvpnVxlanEviVniMapVniNumber)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsEvpnVxlanBgpRTTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanBgpRTTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsEvpnVxlanBgpRTTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsEvpnVxlanBgpRTEntry *pFsEvpnVxlanBgpRTEntry1 =
        (tVxlanFsEvpnVxlanBgpRTEntry *) pRBElem1;
    tVxlanFsEvpnVxlanBgpRTEntry *pFsEvpnVxlanBgpRTEntry2 =
        (tVxlanFsEvpnVxlanBgpRTEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsEvpnVxlanBgpRTEntry1->MibObject.u4FsEvpnVxlanBgpRTIndex >
        pFsEvpnVxlanBgpRTEntry2->MibObject.u4FsEvpnVxlanBgpRTIndex)
    {
        return 1;
    }
    else if (pFsEvpnVxlanBgpRTEntry1->MibObject.u4FsEvpnVxlanBgpRTIndex <
             pFsEvpnVxlanBgpRTEntry2->MibObject.u4FsEvpnVxlanBgpRTIndex)
    {
        return -1;
    }

    if (pFsEvpnVxlanBgpRTEntry1->MibObject.i4FsEvpnVxlanBgpRTType >
        pFsEvpnVxlanBgpRTEntry2->MibObject.i4FsEvpnVxlanBgpRTType)
    {
        return 1;
    }
    else if (pFsEvpnVxlanBgpRTEntry1->MibObject.i4FsEvpnVxlanBgpRTType <
             pFsEvpnVxlanBgpRTEntry2->MibObject.i4FsEvpnVxlanBgpRTType)
    {
        return -1;
    }

    if (pFsEvpnVxlanBgpRTEntry1->MibObject.i4FsEvpnVxlanEviVniMapEviIndex >
        pFsEvpnVxlanBgpRTEntry2->MibObject.i4FsEvpnVxlanEviVniMapEviIndex)
    {
        return 1;
    }
    else if (pFsEvpnVxlanBgpRTEntry1->MibObject.i4FsEvpnVxlanEviVniMapEviIndex <
             pFsEvpnVxlanBgpRTEntry2->MibObject.i4FsEvpnVxlanEviVniMapEviIndex)
    {
        return -1;
    }

    if (pFsEvpnVxlanBgpRTEntry1->MibObject.u4FsEvpnVxlanEviVniMapVniNumber >
        pFsEvpnVxlanBgpRTEntry2->MibObject.u4FsEvpnVxlanEviVniMapVniNumber)
    {
        return 1;
    }
    else if (pFsEvpnVxlanBgpRTEntry1->
             MibObject.u4FsEvpnVxlanEviVniMapVniNumber <
             pFsEvpnVxlanBgpRTEntry2->MibObject.u4FsEvpnVxlanEviVniMapVniNumber)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsEvpnVxlanVrfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanVrfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsEvpnVxlanVrfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsEvpnVxlanVrfEntry *pFsEvpnVxlanVrfEntry1 =
        (tVxlanFsEvpnVxlanVrfEntry *) pRBElem1;
    tVxlanFsEvpnVxlanVrfEntry *pFsEvpnVxlanVrfEntry2 =
        (tVxlanFsEvpnVxlanVrfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsEvpnVxlanVrfEntry1->MibObject.i4FsEvpnVxlanVrfNameLen >
        pFsEvpnVxlanVrfEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        return 1;
    }
    else if (pFsEvpnVxlanVrfEntry1->MibObject.i4FsEvpnVxlanVrfNameLen <
             pFsEvpnVxlanVrfEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        return -1;
    }
    else
    {
        i4MaxLength = pFsEvpnVxlanVrfEntry2->MibObject.i4FsEvpnVxlanVrfNameLen;
    }

    if (MEMCMP
        (pFsEvpnVxlanVrfEntry1->MibObject.au1FsEvpnVxlanVrfName,
         pFsEvpnVxlanVrfEntry2->MibObject.au1FsEvpnVxlanVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsEvpnVxlanVrfEntry1->MibObject.au1FsEvpnVxlanVrfName,
              pFsEvpnVxlanVrfEntry2->MibObject.au1FsEvpnVxlanVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsEvpnVxlanVrfEntry1->MibObject.i4FsEvpnVxlanVrfNameLen >
        pFsEvpnVxlanVrfEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        return 1;
    }
    if (pFsEvpnVxlanVrfEntry1->MibObject.u4FsEvpnVxlanVrfVniNumber >
        pFsEvpnVxlanVrfEntry2->MibObject.u4FsEvpnVxlanVrfVniNumber)
    {
        return 1;
    }
    else if (pFsEvpnVxlanVrfEntry1->MibObject.u4FsEvpnVxlanVrfVniNumber <
             pFsEvpnVxlanVrfEntry2->MibObject.u4FsEvpnVxlanVrfVniNumber)
    {
        return 1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsEvpnVxlanVrfRTTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanVrfRTTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsEvpnVxlanVrfRTTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsEvpnVxlanVrfRTEntry *pFsEvpnVxlanVrfRTEntry1 =
        (tVxlanFsEvpnVxlanVrfRTEntry *) pRBElem1;
    tVxlanFsEvpnVxlanVrfRTEntry *pFsEvpnVxlanVrfRTEntry2 =
        (tVxlanFsEvpnVxlanVrfRTEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsEvpnVxlanVrfRTEntry1->MibObject.u4FsEvpnVxlanVrfRTIndex >
        pFsEvpnVxlanVrfRTEntry2->MibObject.u4FsEvpnVxlanVrfRTIndex)
    {
        return 1;
    }
    else if (pFsEvpnVxlanVrfRTEntry1->MibObject.u4FsEvpnVxlanVrfRTIndex <
             pFsEvpnVxlanVrfRTEntry2->MibObject.u4FsEvpnVxlanVrfRTIndex)
    {
        return -1;
    }

    if (pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfRTType >
        pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfRTType)
    {
        return 1;
    }
    else if (pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfRTType <
             pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfRTType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfNameLen >
        pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        i4MaxLength =
            pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfNameLen;
    }
    else
    {
        i4MaxLength =
            pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfNameLen;
    }

    if (MEMCMP
        (pFsEvpnVxlanVrfRTEntry1->MibObject.au1FsEvpnVxlanVrfName,
         pFsEvpnVxlanVrfRTEntry2->MibObject.au1FsEvpnVxlanVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsEvpnVxlanVrfRTEntry1->MibObject.au1FsEvpnVxlanVrfName,
              pFsEvpnVxlanVrfRTEntry2->MibObject.au1FsEvpnVxlanVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfNameLen >
        pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        return 1;
    }
    else if (pFsEvpnVxlanVrfRTEntry1->MibObject.i4FsEvpnVxlanVrfNameLen <
             pFsEvpnVxlanVrfRTEntry2->MibObject.i4FsEvpnVxlanVrfNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsEvpnVxlanMultihomedPeerTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanMultihomedPeerTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsEvpnVxlanMultihomedPeerTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsEvpnVxlanMultihomedPeerTable *pFsEvpnVxlanMultihomedPeerTable1 =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) pRBElem1;
    tVxlanFsEvpnVxlanMultihomedPeerTable *pFsEvpnVxlanMultihomedPeerTable2 =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
        i4FsEvpnVxlanPeerIpAddressType >
        pFsEvpnVxlanMultihomedPeerTable2->MibObject.
        i4FsEvpnVxlanPeerIpAddressType)
    {
        return 1;
    }
    else if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
             i4FsEvpnVxlanPeerIpAddressType <
             pFsEvpnVxlanMultihomedPeerTable2->MibObject.
             i4FsEvpnVxlanPeerIpAddressType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen >
        pFsEvpnVxlanMultihomedPeerTable2->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen)
    {
        i4MaxLength =
            pFsEvpnVxlanMultihomedPeerTable1->MibObject.
            i4FsEvpnVxlanPeerIpAddressLen;
    }
    else
    {
        i4MaxLength =
            pFsEvpnVxlanMultihomedPeerTable2->MibObject.
            i4FsEvpnVxlanPeerIpAddressLen;
    }

    if (MEMCMP
        (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
         au1FsEvpnVxlanPeerIpAddress,
         pFsEvpnVxlanMultihomedPeerTable2->MibObject.
         au1FsEvpnVxlanPeerIpAddress, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
              au1FsEvpnVxlanPeerIpAddress,
              pFsEvpnVxlanMultihomedPeerTable2->MibObject.
              au1FsEvpnVxlanPeerIpAddress, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen >
        pFsEvpnVxlanMultihomedPeerTable2->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen)
    {
        return 1;
    }
    else if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
             i4FsEvpnVxlanPeerIpAddressLen <
             pFsEvpnVxlanMultihomedPeerTable2->MibObject.
             i4FsEvpnVxlanPeerIpAddressLen)
    {
        return -1;
    }
    i4MaxLength = 0;
    if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
        i4FsEvpnVxlanMHEviVniESILen >
        pFsEvpnVxlanMultihomedPeerTable2->MibObject.i4FsEvpnVxlanMHEviVniESILen)
    {
        i4MaxLength =
            pFsEvpnVxlanMultihomedPeerTable1->MibObject.
            i4FsEvpnVxlanMHEviVniESILen;
    }
    else
    {
        i4MaxLength =
            pFsEvpnVxlanMultihomedPeerTable2->MibObject.
            i4FsEvpnVxlanMHEviVniESILen;
    }

    if (MEMCMP
        (pFsEvpnVxlanMultihomedPeerTable1->MibObject.au1FsEvpnVxlanMHEviVniESI,
         pFsEvpnVxlanMultihomedPeerTable2->MibObject.au1FsEvpnVxlanMHEviVniESI,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
              au1FsEvpnVxlanMHEviVniESI,
              pFsEvpnVxlanMultihomedPeerTable2->MibObject.
              au1FsEvpnVxlanMHEviVniESI, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
        i4FsEvpnVxlanMHEviVniESILen >
        pFsEvpnVxlanMultihomedPeerTable2->MibObject.i4FsEvpnVxlanMHEviVniESILen)
    {
        return 1;
    }
    else if (pFsEvpnVxlanMultihomedPeerTable1->MibObject.
             i4FsEvpnVxlanMHEviVniESILen <
             pFsEvpnVxlanMultihomedPeerTable2->MibObject.
             i4FsEvpnVxlanMHEviVniESILen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsVxlanEcmpNveTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanMultihomedPeerTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/
PUBLIC INT4
FsVxlanEcmpNveTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tVxlanFsVxlanEcmpNveEntry *pFsVxlanEcmpNveEntry1 =
        (tVxlanFsVxlanEcmpNveEntry *) pRBElem1;
    tVxlanFsVxlanEcmpNveEntry *pFsVxlanEcmpNveEntry2 =
        (tVxlanFsVxlanEcmpNveEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVxlanEcmpNveEntry1->MibObject.i4FsVxlanEcmpNveIfIndex >
        pFsVxlanEcmpNveEntry2->MibObject.i4FsVxlanEcmpNveIfIndex)
    {
        return 1;
    }
    else if (pFsVxlanEcmpNveEntry1->MibObject.i4FsVxlanEcmpNveIfIndex <
             pFsVxlanEcmpNveEntry2->MibObject.i4FsVxlanEcmpNveIfIndex)
    {
        return -1;
    }

    if (pFsVxlanEcmpNveEntry1->MibObject.u4FsVxlanEcmpNveVniNumber >
        pFsVxlanEcmpNveEntry2->MibObject.u4FsVxlanEcmpNveVniNumber)
    {
        return 1;
    }
    else if (pFsVxlanEcmpNveEntry1->MibObject.u4FsVxlanEcmpNveVniNumber <
             pFsVxlanEcmpNveEntry2->MibObject.u4FsVxlanEcmpNveVniNumber)
    {
        return -1;
    }

    if (MEMCMP
        (pFsVxlanEcmpNveEntry1->MibObject.FsVxlanEcmpNveDestVmMac,
         pFsVxlanEcmpNveEntry2->MibObject.FsVxlanEcmpNveDestVmMac,
         VXLAN_ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsVxlanEcmpNveEntry1->MibObject.FsVxlanEcmpNveDestVmMac,
              pFsVxlanEcmpNveEntry2->MibObject.FsVxlanEcmpNveDestVmMac,
              VXLAN_ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }
    if (pFsVxlanEcmpNveEntry1->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType >
        pFsVxlanEcmpNveEntry2->MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType)
    {
        return 1;
    }
    else if (pFsVxlanEcmpNveEntry1->MibObject.
             i4FsVxlanEcmpNveRemoteVtepAddressType <
             pFsVxlanEcmpNveEntry2->MibObject.
             i4FsVxlanEcmpNveRemoteVtepAddressType)
    {
        return -1;
    }
    if (MEMCMP
        (pFsVxlanEcmpNveEntry1->MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
         pFsVxlanEcmpNveEntry2->MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
         sizeof (pFsVxlanEcmpNveEntry1->MibObject.
                 i4FsVxlanEcmpNveRemoteVtepAddressLen)) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsVxlanEcmpNveEntry1->MibObject.
              au1FsVxlanEcmpNveRemoteVtepAddress,
              pFsVxlanEcmpNveEntry2->MibObject.
              au1FsVxlanEcmpNveRemoteVtepAddress,
              sizeof (pFsVxlanEcmpNveEntry1->MibObject.
                      i4FsVxlanEcmpNveRemoteVtepAddressLen)) < 0)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  VxlanEvpnArpSupLocalMacTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsEvpnVxlanMultihomedPeerTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/
PUBLIC INT4
VxlanEvpnArpSupLocalMacTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry1 =
        (tVxlanEvpnArpSupLocalMacEntry *) pRBElem1;
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry2 =
        (tVxlanEvpnArpSupLocalMacEntry *) pRBElem2;

    if (MEMCMP
        (pVxlanEvpnArpSupLocalMacEntry1->SourceMac,
         pVxlanEvpnArpSupLocalMacEntry2->SourceMac,
         VXLAN_ETHERNET_ADDR_SIZE) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pVxlanEvpnArpSupLocalMacEntry1->SourceMac,
              pVxlanEvpnArpSupLocalMacEntry2->SourceMac,
              VXLAN_ETHERNET_ADDR_SIZE) < 0)
    {
        return -1;
    }
    return 0;
}
#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  FsVxlanVtepTableFilterInputs
 Input       :  The Indices
                pVxlanFsVxlanVtepEntry
                pVxlanSetFsVxlanVtepEntry
                pVxlanIsSetFsVxlanVtepEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVxlanVtepTableFilterInputs (tVxlanFsVxlanVtepEntry * pVxlanFsVxlanVtepEntry,
                              tVxlanFsVxlanVtepEntry *
                              pVxlanSetFsVxlanVtepEntry,
                              tVxlanIsSetFsVxlanVtepEntry *
                              pVxlanIsSetFsVxlanVtepEntry)
{
    /*
       if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex == OSIX_TRUE)
       {
       if (pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex ==
       pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepNveIfIndex)
       pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex = OSIX_FALSE;
       } */
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType == OSIX_TRUE)
    {
        if (pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType ==
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressType)
            pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress == OSIX_TRUE)
    {
        if ((MEMCMP (pVxlanFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                     pVxlanSetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                     pVxlanSetFsVxlanVtepEntry->MibObject.
                     i4FsVxlanVtepAddressLen) == 0)
            && (pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen ==
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen))
            pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus == OSIX_TRUE)
    {
        if (pVxlanFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus ==
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepRowStatus)
            pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus = OSIX_FALSE;
    }
    if ((pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepNveIfIndex == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsVxlanNveTableFilterInputs
 Input       :  The Indices
                pVxlanFsVxlanNveEntry
                pVxlanSetFsVxlanNveEntry
                pVxlanIsSetFsVxlanNveEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVxlanNveTableFilterInputs (tVxlanFsVxlanNveEntry * pVxlanFsVxlanNveEntry,
                             tVxlanFsVxlanNveEntry * pVxlanSetFsVxlanNveEntry,
                             tVxlanIsSetFsVxlanNveEntry *
                             pVxlanIsSetFsVxlanNveEntry)
{
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressType ==
            pVxlanSetFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressType)
            pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress == OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress,
              pVxlanSetFsVxlanNveEntry->MibObject.
              au1FsVxlanNveRemoteVtepAddress,
              pVxlanSetFsVxlanNveEntry->MibObject.
              i4FsVxlanNveRemoteVtepAddressLen) == 0)
            && (pVxlanFsVxlanNveEntry->MibObject.
                i4FsVxlanNveRemoteVtepAddressLen ==
                pVxlanSetFsVxlanNveEntry->MibObject.
                i4FsVxlanNveRemoteVtepAddressLen))
            pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveStorageType == OSIX_TRUE)
    {
        if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveStorageType)
            pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveStorageType = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp == OSIX_TRUE)
    {
        if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp ==
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp)
            pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus == OSIX_TRUE)
    {
        if (pVxlanFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus ==
            pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveRowStatus)
            pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus = OSIX_FALSE;
    }
    if ((pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveIfIndex == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveVniNumber == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveDestVmMac == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveStorageType == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsVxlanMCastTableFilterInputs
 Input       :  The Indices
                pVxlanFsVxlanMCastEntry
                pVxlanSetFsVxlanMCastEntry
                pVxlanIsSetFsVxlanMCastEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVxlanMCastTableFilterInputs (tVxlanFsVxlanMCastEntry *
                               pVxlanFsVxlanMCastEntry,
                               tVxlanFsVxlanMCastEntry *
                               pVxlanSetFsVxlanMCastEntry,
                               tVxlanIsSetFsVxlanMCastEntry *
                               pVxlanIsSetFsVxlanMCastEntry)
{
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType ==
            pVxlanSetFsVxlanMCastEntry->MibObject.
            i4FsVxlanMCastGroupAddressType)
            pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress == OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
              pVxlanSetFsVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
              pVxlanSetFsVxlanMCastEntry->MibObject.
              i4FsVxlanMCastGroupAddressLen) == 0)
            && (pVxlanFsVxlanMCastEntry->MibObject.
                i4FsVxlanMCastGroupAddressLen ==
                pVxlanSetFsVxlanMCastEntry->MibObject.
                i4FsVxlanMCastGroupAddressLen))
            pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus == OSIX_TRUE)
    {
        if (pVxlanFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus ==
            pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastRowStatus)
            pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus = OSIX_FALSE;
    }
    if ((pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastNveIfIndex == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastVniNumber == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsVxlanVniVlanMapTableFilterInputs
 Input       :  The Indices
                pVxlanFsVxlanVniVlanMapEntry
                pVxlanSetFsVxlanVniVlanMapEntry
                pVxlanIsSetFsVxlanVniVlanMapEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVxlanVniVlanMapTableFilterInputs (tVxlanFsVxlanVniVlanMapEntry *
                                    pVxlanFsVxlanVniVlanMapEntry,
                                    tVxlanFsVxlanVniVlanMapEntry *
                                    pVxlanSetFsVxlanVniVlanMapEntry,
                                    tVxlanIsSetFsVxlanVniVlanMapEntry *
                                    pVxlanIsSetFsVxlanVniVlanMapEntry)
{
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapVniNumber ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapVniNumber)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktSent ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktSent)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktRcvd ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktRcvd)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktDrpd ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            u4FsVxlanVniVlanMapPktDrpd)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            u1IsVlanTagged ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.u1IsVlanTagged)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus ==
            pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
            i4FsVxlanVniVlanMapRowStatus)
            pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus =
                OSIX_FALSE;
    }
    if ((pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVlanId ==
         OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsVxlanInReplicaTableFilterInputs
 Input       :  The Indices
                pVxlanFsVxlanInReplicaEntry
                pVxlanSetFsVxlanInReplicaEntry
                pVxlanIsSetFsVxlanInReplicaEntry
 Description :  This Routine checks set value
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVxlanInReplicaTableFilterInputs (tVxlanFsVxlanInReplicaEntry *
                                   pVxlanFsVxlanInReplicaEntry,
                                   tVxlanFsVxlanInReplicaEntry *
                                   pVxlanSetFsVxlanInReplicaEntry,
                                   tVxlanIsSetFsVxlanInReplicaEntry *
                                   pVxlanIsSetFsVxlanInReplicaEntry)
{
    if (pVxlanIsSetFsVxlanInReplicaEntry->
        bFsVxlanInReplicaRemoteVtepAddressType == OSIX_TRUE)
    {
        if (pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType ==
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType)
            pVxlanIsSetFsVxlanInReplicaEntry->
                bFsVxlanInReplicaRemoteVtepAddressType = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsVxlanInReplicaEntry->MibObject.
              au1FsVxlanInReplicaRemoteVtepAddress,
              pVxlanSetFsVxlanInReplicaEntry->MibObject.
              au1FsVxlanInReplicaRemoteVtepAddress,
              (pVxlanSetFsVxlanInReplicaEntry->MibObject.
               i4FsVxlanInReplicaRemoteVtepAddressLen)) == 0)
            && (pVxlanFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRemoteVtepAddressLen ==
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRemoteVtepAddressLen))
            pVxlanIsSetFsVxlanInReplicaEntry->
                bFsVxlanInReplicaRemoteVtepAddress = OSIX_FALSE;
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus ==
        OSIX_TRUE)
    {
        if (pVxlanFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus ==
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRowStatus)
            pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus =
                OSIX_FALSE;
    }
    if ((pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaNveIfIndex ==
         OSIX_FALSE)
        && (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaVniNumber ==
            OSIX_FALSE)
        && (pVxlanIsSetFsVxlanInReplicaEntry->
            bFsVxlanInReplicaRemoteVtepAddressType == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanInReplicaEntry->
            bFsVxlanInReplicaRemoteVtepAddress == OSIX_FALSE)
        && (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  FsEvpnVxlanEviVniMapTableFilterInputs
 Input       :  The Indices
                pVxlanFsEvpnVxlanEviVniMapEntry
                pVxlanSetFsEvpnVxlanEviVniMapEntry
                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsEvpnVxlanEviVniMapTableFilterInputs (tVxlanFsEvpnVxlanEviVniMapEntry *
                                       pVxlanFsEvpnVxlanEviVniMapEntry,
                                       tVxlanFsEvpnVxlanEviVniMapEntry *
                                       pVxlanSetFsEvpnVxlanEviVniMapEntry,
                                       tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                       pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapEviIndex = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapVniNumber = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsEvpnVxlanEviVniMapEntry->
              MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
              pVxlanSetFsEvpnVxlanEviVniMapEntry->
              MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
              pVxlanSetFsEvpnVxlanEviVniMapEntry->
              MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen) == 0)
            && (pVxlanFsEvpnVxlanEviVniMapEntry->
                MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen ==
                pVxlanSetFsEvpnVxlanEviVniMapEntry->
                MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen))
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
              au1FsEvpnVxlanEviVniESI,
              pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
              au1FsEvpnVxlanEviVniESI,
              pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
              i4FsEvpnVxlanEviVniESILen) == 0)
            && (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniESILen ==
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniESILen))
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniLoadBalance ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniLoadBalance)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniLoadBalance = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapSentPkts ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapSentPkts)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapSentPkts = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapRcvdPkts ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapRcvdPkts)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapRcvdPkts = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
        bFsEvpnVxlanEviVniMapDroppedPkts == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapDroppedPkts ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapDroppedPkts)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapDroppedPkts = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapRowStatus = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapBgpRDAuto ==
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapBgpRDAuto)
            pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
                bFsEvpnVxlanEviVniMapBgpRDAuto = OSIX_FALSE;
    }
    if ((pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex ==
         OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapVniNumber == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapBgpRD == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniLoadBalance == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapSentPkts == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapRcvdPkts == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapDroppedPkts == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapRowStatus == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
            bFsEvpnVxlanEviVniMapBgpRDAuto == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsEvpnVxlanBgpRTTableFilterInputs
 Input       :  The Indices
                pVxlanFsEvpnVxlanBgpRTEntry
                pVxlanSetFsEvpnVxlanBgpRTEntry
                pVxlanIsSetFsEvpnVxlanBgpRTEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsEvpnVxlanBgpRTTableFilterInputs (tVxlanFsEvpnVxlanBgpRTEntry *
                                   pVxlanFsEvpnVxlanBgpRTEntry,
                                   tVxlanFsEvpnVxlanBgpRTEntry *
                                   pVxlanSetFsEvpnVxlanBgpRTEntry,
                                   tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                   pVxlanIsSetFsEvpnVxlanBgpRTEntry)
{
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.u4FsEvpnVxlanBgpRTIndex)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTType)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT == OSIX_TRUE)
    {
        if ((MEMCMP (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
                     pVxlanSetFsEvpnVxlanBgpRTEntry->
                     MibObject.au1FsEvpnVxlanBgpRT,
                     pVxlanSetFsEvpnVxlanBgpRTEntry->
                     MibObject.i4FsEvpnVxlanBgpRTLen) == 0)
            && (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen ==
                pVxlanSetFsEvpnVxlanBgpRTEntry->
                MibObject.i4FsEvpnVxlanBgpRTLen))
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanBgpRTRowStatus ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanBgpRTRowStatus)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTAuto)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanEviVniMapEviIndex ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->
            MibObject.i4FsEvpnVxlanEviVniMapEviIndex)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanBgpRTEntry->
            MibObject.u4FsEvpnVxlanEviVniMapVniNumber ==
            pVxlanSetFsEvpnVxlanBgpRTEntry->
            MibObject.u4FsEvpnVxlanEviVniMapVniNumber)
            pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber =
                OSIX_FALSE;
    }
    if ((pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsEvpnVxlanVrfTableFilterInputs
 Input       :  The Indices
                pVxlanFsEvpnVxlanVrfEntry
                pVxlanSetFsEvpnVxlanVrfEntry
                pVxlanIsSetFsEvpnVxlanVrfEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsEvpnVxlanVrfTableFilterInputs (tVxlanFsEvpnVxlanVrfEntry *
                                 pVxlanFsEvpnVxlanVrfEntry,
                                 tVxlanFsEvpnVxlanVrfEntry *
                                 pVxlanSetFsEvpnVxlanVrfEntry,
                                 tVxlanIsSetFsEvpnVxlanVrfEntry *
                                 pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName == OSIX_TRUE)
    {
        if ((MEMCMP (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName,
                     pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                     au1FsEvpnVxlanVrfName,
                     pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                     i4FsEvpnVxlanVrfNameLen) == 0)
            && (pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen ==
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                i4FsEvpnVxlanVrfNameLen))
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD == OSIX_TRUE)
    {
        if ((pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen ==
             pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen) &&
            (MEMCMP (pVxlanFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
                     pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                     au1FsEvpnVxlanVrfRD,
                     pVxlanFsEvpnVxlanVrfEntry->MibObject.
                     i4FsEvpnVxlanVrfRDLen) == 0))
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus ==
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRowStatus)
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapSentPkts ==
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapSentPkts)
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapRcvdPkts ==
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapRcvdPkts)
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapDroppedPkts ==
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
            u4FsEvpnVxlanVrfVniMapDroppedPkts)
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto ==
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDAuto)
            pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto = OSIX_FALSE;
    }

    if ((pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsEvpnVxlanVrfRTTableFilterInputs
 Input       :  The Indices
                pVxlanFsEvpnVxlanVrfRTEntry
                pVxlanSetFsEvpnVxlanVrfRTEntry
                pVxlanIsSetFsEvpnVxlanVrfRTEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsEvpnVxlanVrfRTTableFilterInputs (tVxlanFsEvpnVxlanVrfRTEntry *
                                   pVxlanFsEvpnVxlanVrfRTEntry,
                                   tVxlanFsEvpnVxlanVrfRTEntry *
                                   pVxlanSetFsEvpnVxlanVrfRTEntry,
                                   tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                   pVxlanIsSetFsEvpnVxlanVrfRTEntry)
{
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex ==
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfRTIndex)
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType ==
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTType)
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT == OSIX_TRUE)
    {
        if ((MEMCMP (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
                     pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                     au1FsEvpnVxlanVrfRT,
                     pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                     i4FsEvpnVxlanVrfRTLen) == 0)
            && (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen ==
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfRTLen))
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
            i4FsEvpnVxlanVrfRTRowStatus ==
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
            i4FsEvpnVxlanVrfRTRowStatus)
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName,
              pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName,
              pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
              i4FsEvpnVxlanVrfNameLen) == 0)
            && (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfNameLen ==
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfNameLen))
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
            i4FsEvpnVxlanVrfRTAuto ==
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto)
            pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto =
                OSIX_FALSE;
    }
    if ((pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsEvpnVxlanMultihomedPeerTableFilterInputs
 Input       :  The Indices
                pVxlanFsEvpnVxlanMultihomedPeerTable
                pVxlanSetFsEvpnVxlanMultihomedPeerTable
                pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsEvpnVxlanMultihomedPeerTableFilterInputs (tVxlanFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pVxlanFsEvpnVxlanMultihomedPeerTable,
                                            tVxlanFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                                            tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
                                            *
                                            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable)
{
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanPeerIpAddressType == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType ==
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType)
            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanPeerIpAddressType = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
              au1FsEvpnVxlanPeerIpAddress,
              pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              au1FsEvpnVxlanPeerIpAddress,
              pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              i4FsEvpnVxlanPeerIpAddressLen) == 0)
            && (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanPeerIpAddressLen ==
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanPeerIpAddressLen))
            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanPeerIpAddress = OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
              au1FsEvpnVxlanMHEviVniESI,
              pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              au1FsEvpnVxlanMHEviVniESI,
              pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
              i4FsEvpnVxlanMHEviVniESILen) == 0)
            && (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanMHEviVniESILen ==
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanMHEviVniESILen))
            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum ==
        OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            u4FsEvpnVxlanOrdinalNum ==
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            u4FsEvpnVxlanOrdinalNum)
            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum =
                OSIX_FALSE;
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus == OSIX_TRUE)
    {
        if (pVxlanFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus ==
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMultihomedPeerRowStatus)
            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
                bFsEvpnVxlanMultihomedPeerRowStatus = OSIX_FALSE;
    }
    if ((pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
         bFsEvpnVxlanPeerIpAddressType == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
            bFsEvpnVxlanPeerIpAddress == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
            bFsEvpnVxlanMHEviVniESI == OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum ==
            OSIX_FALSE)
        && (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
            bFsEvpnVxlanMultihomedPeerRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}
#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanVtepTableTrigger
 Input       :  The Indices
                pVxlanSetFsVxlanVtepEntry
                pVxlanIsSetFsVxlanVtepEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsVxlanVtepTableTrigger (tVxlanFsVxlanVtepEntry *
                                    pVxlanSetFsVxlanVtepEntry,
                                    tVxlanIsSetFsVxlanVtepEntry *
                                    pVxlanIsSetFsVxlanVtepEntry,
                                    INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsVxlanVtepAddressVal;
    UINT1               au1FsVxlanVtepAddressVal[VXLAN_IPV6_ADDR_BYTES];

    MEMSET (&FsVxlanVtepAddressVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsVxlanVtepAddressVal, 0, sizeof (au1FsVxlanVtepAddressVal));
    FsVxlanVtepAddressVal.pu1_OctetList = au1FsVxlanVtepAddressVal;
    FsVxlanVtepAddressVal.i4_Length = 0;

    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddressType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVtepAddressType, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVtepEntry->MibObject.
                      i4FsVxlanVtepNveIfIndex,
                      pVxlanSetFsVxlanVtepEntry->MibObject.
                      i4FsVxlanVtepAddressType);
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepAddress == OSIX_TRUE)
    {
        MEMCPY (FsVxlanVtepAddressVal.pu1_OctetList,
                pVxlanSetFsVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen);
        FsVxlanVtepAddressVal.i4_Length =
            pVxlanSetFsVxlanVtepEntry->MibObject.i4FsVxlanVtepAddressLen;

        nmhSetCmnNew (FsVxlanVtepAddress, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %s",
                      pVxlanSetFsVxlanVtepEntry->MibObject.
                      i4FsVxlanVtepNveIfIndex, &FsVxlanVtepAddressVal);
    }
    if (pVxlanIsSetFsVxlanVtepEntry->bFsVxlanVtepRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVtepRowStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 1, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVtepEntry->MibObject.
                      i4FsVxlanVtepNveIfIndex,
                      pVxlanSetFsVxlanVtepEntry->MibObject.
                      i4FsVxlanVtepRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanNveTableTrigger
 Input       :  The Indices
                pVxlanSetFsVxlanNveEntry
                pVxlanIsSetFsVxlanNveEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsVxlanNveTableTrigger (tVxlanFsVxlanNveEntry *
                                   pVxlanSetFsVxlanNveEntry,
                                   tVxlanIsSetFsVxlanNveEntry *
                                   pVxlanIsSetFsVxlanNveEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsVxlanNveRemoteVtepAddressVal;
    UINT1              
        au1FsVxlanNveRemoteVtepAddressVal[VXLAN_IPV6_ADDR_BYTES];

    MEMSET (au1FsVxlanNveRemoteVtepAddressVal, 0,
            sizeof (au1FsVxlanNveRemoteVtepAddressVal));
    FsVxlanNveRemoteVtepAddressVal.pu1_OctetList =
        au1FsVxlanNveRemoteVtepAddressVal;
    FsVxlanNveRemoteVtepAddressVal.i4_Length = 0;

    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddressType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanNveRemoteVtepAddressType,
                      VXLAN_TABLE_MIB_OID_LENGTH, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %u %m %i",
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                      pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                      pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                      pVxlanSetFsVxlanNveEntry->MibObject.
                      i4FsVxlanNveRemoteVtepAddressType);
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRemoteVtepAddress == OSIX_TRUE)
    {
        MEMCPY (FsVxlanNveRemoteVtepAddressVal.pu1_OctetList,
                pVxlanSetFsVxlanNveEntry->MibObject.
                au1FsVxlanNveRemoteVtepAddress,
                pVxlanSetFsVxlanNveEntry->MibObject.
                i4FsVxlanNveRemoteVtepAddressLen);
        FsVxlanNveRemoteVtepAddressVal.i4_Length =
            pVxlanSetFsVxlanNveEntry->MibObject.
            i4FsVxlanNveRemoteVtepAddressLen;

        nmhSetCmnNew (FsVxlanNveRemoteVtepAddress, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 3,
                      i4SetOption, "%i %u %m %s",
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                      pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                      pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                      &FsVxlanNveRemoteVtepAddressVal);
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveStorageType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanNveStorageType, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 3,
                      i4SetOption, "%i %u %m %i",
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                      pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                      pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                      pVxlanSetFsVxlanNveEntry->MibObject.
                      i4FsVxlanNveStorageType);
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanSuppressArp == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanSuppressArp, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %u %m %i",
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                      pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                      pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanSuppressArp);
    }
    if (pVxlanIsSetFsVxlanNveEntry->bFsVxlanNveRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanNveRowStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 1, 3,
                      i4SetOption, "%i %u %m %i",
                      pVxlanSetFsVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex,
                      pVxlanSetFsVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber,
                      pVxlanSetFsVxlanNveEntry->MibObject.FsVxlanNveDestVmMac,
                      pVxlanSetFsVxlanNveEntry->MibObject.
                      i4FsVxlanNveRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanMCastTableTrigger
 Input       :  The Indices
                pVxlanSetFsVxlanMCastEntry
                pVxlanIsSetFsVxlanMCastEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsVxlanMCastTableTrigger (tVxlanFsVxlanMCastEntry *
                                     pVxlanSetFsVxlanMCastEntry,
                                     tVxlanIsSetFsVxlanMCastEntry *
                                     pVxlanIsSetFsVxlanMCastEntry,
                                     INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsVxlanMCastGroupAddressVal;
    UINT1               au1FsVxlanMCastGroupAddressVal[VXLAN_IPV6_ADDR_BYTES];

    MEMSET (&FsVxlanMCastGroupAddressVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsVxlanMCastGroupAddressVal, 0,
            sizeof (au1FsVxlanMCastGroupAddressVal));
    FsVxlanMCastGroupAddressVal.pu1_OctetList = au1FsVxlanMCastGroupAddressVal;
    FsVxlanMCastGroupAddressVal.i4_Length = 0;

    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddressType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanMCastGroupAddressType, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 2,
                      i4SetOption, "%i %u %i",
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      i4FsVxlanMCastNveIfIndex,
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      u4FsVxlanMCastVniNumber,
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      i4FsVxlanMCastGroupAddressType);
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastGroupAddress == OSIX_TRUE)
    {
        MEMCPY (FsVxlanMCastGroupAddressVal.pu1_OctetList,
                pVxlanSetFsVxlanMCastEntry->MibObject.
                au1FsVxlanMCastGroupAddress,
                pVxlanSetFsVxlanMCastEntry->MibObject.
                i4FsVxlanMCastGroupAddressLen);
        FsVxlanMCastGroupAddressVal.i4_Length =
            pVxlanSetFsVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressLen;

        nmhSetCmnNew (FsVxlanMCastGroupAddress, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 2,
                      i4SetOption, "%i %u %s",
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      i4FsVxlanMCastNveIfIndex,
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      u4FsVxlanMCastVniNumber, &FsVxlanMCastGroupAddressVal);
    }
    if (pVxlanIsSetFsVxlanMCastEntry->bFsVxlanMCastRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanMCastRowStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 1, 2,
                      i4SetOption, "%i %u %i",
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      i4FsVxlanMCastNveIfIndex,
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      u4FsVxlanMCastVniNumber,
                      pVxlanSetFsVxlanMCastEntry->MibObject.
                      i4FsVxlanMCastRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanVniVlanMapTableTrigger
 Input       :  The Indices
                pVxlanSetFsVxlanVniVlanMapEntry
                pVxlanIsSetFsVxlanVniVlanMapEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsVxlanVniVlanMapTableTrigger (tVxlanFsVxlanVniVlanMapEntry *
                                          pVxlanSetFsVxlanVniVlanMapEntry,
                                          tVxlanIsSetFsVxlanVniVlanMapEntry *
                                          pVxlanIsSetFsVxlanVniVlanMapEntry,
                                          INT4 i4SetOption)
{
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapVniNumber ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanMapVniNumber, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %u",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      u4FsVxlanVniVlanMapVniNumber);
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktSent ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanMapPktSent, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      u4FsVxlanVniVlanMapPktSent);
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktRcvd ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanMapPktRcvd, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      u4FsVxlanVniVlanMapPktRcvd);
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapPktDrpd ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanMapPktDrpd, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      u4FsVxlanVniVlanMapPktDrpd);
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanVlanTagged ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanTagStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      u1IsVlanTagged);
    }
    if (pVxlanIsSetFsVxlanVniVlanMapEntry->bFsVxlanVniVlanMapRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanVniVlanMapRowStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 1, 1,
                      i4SetOption, "%i %i",
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapVlanId,
                      pVxlanSetFsVxlanVniVlanMapEntry->MibObject.
                      i4FsVxlanVniVlanMapRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsVxlanInReplicaTableTrigger
 Input       :  The Indices
                pVxlanSetFsVxlanInReplicaEntry
                pVxlanIsSetFsVxlanInReplicaEntry
 Description :  This Routine is used to send
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

INT4
VxlanSetAllFsVxlanInReplicaTableTrigger (tVxlanFsVxlanInReplicaEntry *
                                         pVxlanSetFsVxlanInReplicaEntry,
                                         tVxlanIsSetFsVxlanInReplicaEntry *
                                         pVxlanIsSetFsVxlanInReplicaEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsVxlanInReplicaRemoteVtepAddressVal;

    UINT1              
        au1FsVxlanInReplicaRemoteVtepAddressVal[VXLAN_IP6_ADDR_LEN];

    if (pVxlanIsSetFsVxlanInReplicaEntry->
        bFsVxlanInReplicaRemoteVtepAddressType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanInReplicaRemoteVtepAddressType,
                      VXLAN_TABLE_MIB_OID_LENGTH, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %u %i %i",
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaNveIfIndex,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      u4FsVxlanInReplicaVniNumber,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaIndex,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaRemoteVtepAddressType);
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRemoteVtepAddress ==
        OSIX_TRUE)
    {
        MEMSET (au1FsVxlanInReplicaRemoteVtepAddressVal, 0,
                sizeof (au1FsVxlanInReplicaRemoteVtepAddressVal));
        FsVxlanInReplicaRemoteVtepAddressVal.pu1_OctetList =
            au1FsVxlanInReplicaRemoteVtepAddressVal;
        FsVxlanInReplicaRemoteVtepAddressVal.i4_Length = 0;

        MEMCPY (FsVxlanInReplicaRemoteVtepAddressVal.pu1_OctetList,
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                au1FsVxlanInReplicaRemoteVtepAddress,
                pVxlanSetFsVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaRemoteVtepAddressLen);
        FsVxlanInReplicaRemoteVtepAddressVal.i4_Length =
            pVxlanSetFsVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressLen;
        nmhSetCmnNew (FsVxlanInReplicaRemoteVtepAddress,
                      VXLAN_TABLE_MIB_OID_LENGTH, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %u %i %s",
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaNveIfIndex,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      u4FsVxlanInReplicaVniNumber,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaIndex,
                      &FsVxlanInReplicaRemoteVtepAddressVal);
    }
    if (pVxlanIsSetFsVxlanInReplicaEntry->bFsVxlanInReplicaRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsVxlanInReplicaRowStatus, VXLAN_TABLE_MIB_OID_LENGTH,
                      VxlanMainTaskLock, VxlanMainTaskUnLock, 0, 1, 3,
                      i4SetOption, "%i %u %i %i",
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaNveIfIndex,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      u4FsVxlanInReplicaVniNumber,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaIndex,
                      pVxlanSetFsVxlanInReplicaEntry->MibObject.
                      i4FsVxlanInReplicaRowStatus);
    }

    return OSIX_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger
 Input       :  The Indices
                pVxlanSetFsEvpnVxlanEviVniMapEntry
                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanEviVniMapTableTrigger (tVxlanFsEvpnVxlanEviVniMapEntry *
                                             pVxlanSetFsEvpnVxlanEviVniMapEntry,
                                             tVxlanIsSetFsEvpnVxlanEviVniMapEntry
                                             *
                                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry,
                                             INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanEviVniMapBgpRDVal;
    UINT1               au1FsEvpnVxlanEviVniMapBgpRDVal[8];
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanEviVniESIVal;
    UINT1               au1FsEvpnVxlanEviVniESIVal[10];

    MEMSET (au1FsEvpnVxlanEviVniMapBgpRDVal, 0,
            sizeof (au1FsEvpnVxlanEviVniMapBgpRDVal));
    FsEvpnVxlanEviVniMapBgpRDVal.pu1_OctetList =
        au1FsEvpnVxlanEviVniMapBgpRDVal;
    FsEvpnVxlanEviVniMapBgpRDVal.i4_Length = 0;

    MEMSET (au1FsEvpnVxlanEviVniESIVal, 0, sizeof (au1FsEvpnVxlanEviVniESIVal));
    FsEvpnVxlanEviVniESIVal.pu1_OctetList = au1FsEvpnVxlanEviVniESIVal;
    FsEvpnVxlanEviVniESIVal.i4_Length = 0;

    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapEviIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapEviIndex, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %i",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapVniNumber ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapVniNumber, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %u",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRD ==
        OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanEviVniMapBgpRDVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->
                MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->
                MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);
        FsEvpnVxlanEviVniMapBgpRDVal.i4_Length =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->
            MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen;

        nmhSetCmnNew (FsEvpnVxlanEviVniMapBgpRD, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %s",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      &FsEvpnVxlanEviVniMapBgpRDVal);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniESI ==
        OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanEviVniESIVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                au1FsEvpnVxlanEviVniESI,
                pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniESILen);
        FsEvpnVxlanEviVniESIVal.i4_Length =
            pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniESILen;

        nmhSetCmnNew (FsEvpnVxlanEviVniESI, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %s",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      &FsEvpnVxlanEviVniESIVal);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniLoadBalance ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniLoadBalance, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %i",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniLoadBalance);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapSentPkts ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapSentPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %u",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapSentPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRcvdPkts ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapRcvdPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %u",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapRcvdPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
        bFsEvpnVxlanEviVniMapDroppedPkts == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapDroppedPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %u",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapDroppedPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapRowStatus, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 1, 2, i4SetOption, "%i %u %i",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapRowStatus);
    }
    if (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->bFsEvpnVxlanEviVniMapBgpRDAuto ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapBgpRDAuto, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %u %i",
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapBgpRDAuto);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanBgpRTTableTrigger
 Input       :  The Indices
                pVxlanSetFsEvpnVxlanBgpRTEntry
                pVxlanIsSetFsEvpnVxlanBgpRTEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanBgpRTTableTrigger (tVxlanFsEvpnVxlanBgpRTEntry *
                                         pVxlanSetFsEvpnVxlanBgpRTEntry,
                                         tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                         pVxlanIsSetFsEvpnVxlanBgpRTEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanBgpRTVal;
    UINT1               au1FsEvpnVxlanBgpRTVal[8];

    MEMSET (au1FsEvpnVxlanBgpRTVal, 0, sizeof (au1FsEvpnVxlanBgpRTVal));
    FsEvpnVxlanBgpRTVal.pu1_OctetList = au1FsEvpnVxlanBgpRTVal;
    FsEvpnVxlanBgpRTVal.i4_Length = 0;

    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanBgpRTIndex, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %u",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanBgpRTType, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %i",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRT == OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanBgpRTVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.au1FsEvpnVxlanBgpRT,
                pVxlanSetFsEvpnVxlanBgpRTEntry->
                MibObject.i4FsEvpnVxlanBgpRTLen);
        FsEvpnVxlanBgpRTVal.i4_Length =
            pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.i4FsEvpnVxlanBgpRTLen;

        nmhSetCmnNew (FsEvpnVxlanBgpRT, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %s",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType, &FsEvpnVxlanBgpRTVal);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanBgpRTRowStatus, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 1, 4, i4SetOption,
                      "%i %u %u %i %i",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTRowStatus);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTAuto == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanBgpRTAuto, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %i",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                      i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                      u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                      u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                      i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->MibObject.
                      i4FsEvpnVxlanBgpRTAuto);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapEviIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapEviIndex, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %i",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex);
    }
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanEviVniMapVniNumber ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapVniNumber, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %u %u %i %u",
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanEviVniMapEviIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanBgpRTIndex,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.i4FsEvpnVxlanBgpRTType,
                      pVxlanSetFsEvpnVxlanBgpRTEntry->
                      MibObject.u4FsEvpnVxlanEviVniMapVniNumber);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanVrfTableTrigger
 Input       :  The Indices
                pVxlanSetFsEvpnVxlanVrfEntry
                pVxlanIsSetFsEvpnVxlanVrfEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanVrfTableTrigger (tVxlanFsEvpnVxlanVrfEntry *
                                       pVxlanSetFsEvpnVxlanVrfEntry,
                                       tVxlanIsSetFsEvpnVxlanVrfEntry *
                                       pVxlanIsSetFsEvpnVxlanVrfEntry,
                                       INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanVrfNameVal;
    UINT1               au1FsEvpnVxlanVrfNameVal[EVPN_MAX_VRF_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanVrfRDVal;
    UINT1               au1FsEvpnVxlanVrfRDVal[EVPN_MAX_VRF_NAME_LEN];

    MEMSET (au1FsEvpnVxlanVrfNameVal, 0, sizeof (au1FsEvpnVxlanVrfNameVal));
    FsEvpnVxlanVrfNameVal.pu1_OctetList = au1FsEvpnVxlanVrfNameVal;
    FsEvpnVxlanVrfNameVal.i4_Length = 0;

    MEMSET (au1FsEvpnVxlanVrfRDVal, 0, sizeof (au1FsEvpnVxlanVrfRDVal));
    FsEvpnVxlanVrfRDVal.pu1_OctetList = au1FsEvpnVxlanVrfRDVal;
    FsEvpnVxlanVrfRDVal.i4_Length = 0;

    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfName == OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanVrfNameVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfName,
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                i4FsEvpnVxlanVrfNameLen);
        FsEvpnVxlanVrfNameVal.i4_Length =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen;

        nmhSetCmnNew (FsEvpnVxlanVrfName, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsEvpnVxlanVrfNameVal, &FsEvpnVxlanVrfNameVal);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRD == OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanVrfRDVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.au1FsEvpnVxlanVrfRD,
                pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen);
        FsEvpnVxlanVrfRDVal.i4_Length =
            pVxlanSetFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfRDLen;

        nmhSetCmnNew (FsEvpnVxlanVrfRD, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &FsEvpnVxlanVrfNameVal, &FsEvpnVxlanVrfRDVal);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRowStatus, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 1, 1, i4SetOption, "%s %i",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                      i4FsEvpnVxlanVrfRowStatus);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapSentPkts ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfVniMapSentPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                      u4FsEvpnVxlanVrfVniMapSentPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapRcvdPkts ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanEviVniMapRcvdPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                      u4FsEvpnVxlanVrfVniMapRcvdPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfVniMapDroppedPkts ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfVniMapDroppedPkts, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                      u4FsEvpnVxlanVrfVniMapDroppedPkts);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfEntry->bFsEvpnVxlanVrfRDAuto == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRDAuto, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %i",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
                      i4FsEvpnVxlanVrfRDAuto);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanVrfRTTableTrigger
 Input       :  The Indices
                pVxlanSetFsEvpnVxlanVrfRTEntry
                pVxlanIsSetFsEvpnVxlanVrfRTEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
VxlanSetAllFsEvpnVxlanVrfRTTableTrigger (tVxlanFsEvpnVxlanVrfRTEntry *
                                         pVxlanSetFsEvpnVxlanVrfRTEntry,
                                         tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                         pVxlanIsSetFsEvpnVxlanVrfRTEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanVrfRTVal;
    UINT1               au1FsEvpnVxlanVrfRTVal[EVPN_MAX_VRF_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanVrfNameVal;
    UINT1               au1FsEvpnVxlanVrfNameVal[EVPN_MAX_VRF_NAME_LEN];

    MEMSET (au1FsEvpnVxlanVrfRTVal, 0, sizeof (au1FsEvpnVxlanVrfRTVal));
    FsEvpnVxlanVrfRTVal.pu1_OctetList = au1FsEvpnVxlanVrfRTVal;
    FsEvpnVxlanVrfRTVal.i4_Length = 0;

    MEMSET (au1FsEvpnVxlanVrfNameVal, 0, sizeof (au1FsEvpnVxlanVrfNameVal));
    FsEvpnVxlanVrfNameVal.pu1_OctetList = au1FsEvpnVxlanVrfNameVal;
    FsEvpnVxlanVrfNameVal.i4_Length = 0;

    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRTIndex, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %u",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRTType, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %i",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRT == OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanVrfRTVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfRT,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfRTLen);
        FsEvpnVxlanVrfRTVal.i4_Length =
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTLen;

        nmhSetCmnNew (FsEvpnVxlanVrfRT, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %s",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType, &FsEvpnVxlanVrfRTVal);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRTRowStatus, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 1, 3, i4SetOption, "%s %u %i %i",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTRowStatus);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfName == OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanVrfNameVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName,
                pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                i4FsEvpnVxlanVrfNameLen);
        FsEvpnVxlanVrfNameVal.i4_Length =
            pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfNameLen;

        nmhSetCmnNew (FsEvpnVxlanVrfName, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %s",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType, &FsEvpnVxlanVrfNameVal);
    }
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTAuto == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanVrfRTAuto, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %i",
                      &FsEvpnVxlanVrfNameVal,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      u4FsEvpnVxlanVrfRTIndex,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTType,
                      pVxlanSetFsEvpnVxlanVrfRTEntry->MibObject.
                      i4FsEvpnVxlanVrfRTAuto);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
 Input       :  The Indices
                pVxlanSetFsEvpnVxlanMultihomedPeerTable
                pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    VxlanSetAllFsEvpnVxlanMultihomedPeerTableTrigger
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanSetFsEvpnVxlanMultihomedPeerTable,
     tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *
     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanPeerIpAddressVal;
    UINT1               au1FsEvpnVxlanPeerIpAddressVal[16];
    tSNMP_OCTET_STRING_TYPE FsEvpnVxlanMHEviVniESIVal;
    UINT1               au1FsEvpnVxlanMHEviVniESIVal[10];

    MEMSET (au1FsEvpnVxlanPeerIpAddressVal, 0,
            sizeof (au1FsEvpnVxlanPeerIpAddressVal));
    FsEvpnVxlanPeerIpAddressVal.pu1_OctetList = au1FsEvpnVxlanPeerIpAddressVal;
    FsEvpnVxlanPeerIpAddressVal.i4_Length = 0;

    MEMSET (au1FsEvpnVxlanMHEviVniESIVal, 0,
            sizeof (au1FsEvpnVxlanMHEviVniESIVal));
    FsEvpnVxlanMHEviVniESIVal.pu1_OctetList = au1FsEvpnVxlanMHEviVniESIVal;
    FsEvpnVxlanMHEviVniESIVal.i4_Length = 0;

    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanPeerIpAddressType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanPeerIpAddressType, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %s %s %i",
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType,
                      &FsEvpnVxlanPeerIpAddressVal, &FsEvpnVxlanMHEviVniESIVal,
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType);
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanPeerIpAddress ==
        OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanPeerIpAddressVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                au1FsEvpnVxlanPeerIpAddress,
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanPeerIpAddressLen);
        FsEvpnVxlanPeerIpAddressVal.i4_Length =
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressLen;

        nmhSetCmnNew (FsEvpnVxlanPeerIpAddress, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %s %s %s",
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType,
                      &FsEvpnVxlanPeerIpAddressVal, &FsEvpnVxlanMHEviVniESIVal,
                      &FsEvpnVxlanPeerIpAddressVal);
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanMHEviVniESI ==
        OSIX_TRUE)
    {
        MEMCPY (FsEvpnVxlanMHEviVniESIVal.pu1_OctetList,
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                au1FsEvpnVxlanMHEviVniESI,
                pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                i4FsEvpnVxlanMHEviVniESILen);
        FsEvpnVxlanMHEviVniESIVal.i4_Length =
            pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanMHEviVniESILen;

        nmhSetCmnNew (FsEvpnVxlanMHEviVniESI, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %s %s %s",
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType,
                      &FsEvpnVxlanPeerIpAddressVal, &FsEvpnVxlanMHEviVniESIVal,
                      &FsEvpnVxlanMHEviVniESIVal);
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->bFsEvpnVxlanOrdinalNum ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanOrdinalNum, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 0, 3, i4SetOption, "%i %s %s %u",
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType,
                      &FsEvpnVxlanPeerIpAddressVal, &FsEvpnVxlanMHEviVniESIVal,
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      u4FsEvpnVxlanOrdinalNum);
    }
    if (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable->
        bFsEvpnVxlanMultihomedPeerRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsEvpnVxlanMultihomedPeerRowStatus, 14, VxlanMainTaskLock,
                      VxlanMainTaskUnLock, 0, 1, 3, i4SetOption, "%i %s %s %i",
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanPeerIpAddressType,
                      &FsEvpnVxlanPeerIpAddressVal, &FsEvpnVxlanMHEviVniESIVal,
                      pVxlanSetFsEvpnVxlanMultihomedPeerTable->MibObject.
                      i4FsEvpnVxlanMultihomedPeerRowStatus);
    }

    return OSIX_SUCCESS;
}
#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    :  VxlanFsVxlanVtepTableCreateApi
 Input       :  pVxlanFsVxlanVtepEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsVxlanVtepEntry *
VxlanFsVxlanVtepTableCreateApi (tVxlanFsVxlanVtepEntry *
                                pSetVxlanFsVxlanVtepEntry)
{
    tVxlanFsVxlanVtepEntry *pVxlanFsVxlanVtepEntry = NULL;

    if (pSetVxlanFsVxlanVtepEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanVtepTableCreatApi: pSetVxlanFsVxlanVtepEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsVxlanVtepEntry =
        (tVxlanFsVxlanVtepEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVTEPTABLE_POOLID);
    if (pVxlanFsVxlanVtepEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanVtepTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsVxlanVtepEntry, pSetVxlanFsVxlanVtepEntry,
                sizeof (tVxlanFsVxlanVtepEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
             (tRBElem *) pVxlanFsVxlanVtepEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsVxlanVtepTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanVtepEntry);
            return NULL;
        }
        return pVxlanFsVxlanVtepEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsVxlanNveTableCreateApi
 Input       :  pVxlanFsVxlanNveEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsVxlanNveEntry *
VxlanFsVxlanNveTableCreateApi (tVxlanFsVxlanNveEntry * pSetVxlanFsVxlanNveEntry)
{
    tVxlanFsVxlanNveEntry *pVxlanFsVxlanNveEntry = NULL;

    if (pSetVxlanFsVxlanNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanNveTableCreatApi: pSetVxlanFsVxlanNveEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsVxlanNveEntry =
        (tVxlanFsVxlanNveEntry *) MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanNveTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsVxlanNveEntry, pSetVxlanFsVxlanNveEntry,
                sizeof (tVxlanFsVxlanNveEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanFsVxlanNveEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsVxlanNveTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanNveEntry);
            return NULL;
        }
        return pVxlanFsVxlanNveEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsVxlanMCastTableCreateApi
 Input       :  pVxlanFsVxlanMCastEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsVxlanMCastEntry *
VxlanFsVxlanMCastTableCreateApi (tVxlanFsVxlanMCastEntry *
                                 pSetVxlanFsVxlanMCastEntry)
{
    tVxlanFsVxlanMCastEntry *pVxlanFsVxlanMCastEntry = NULL;

    if (pSetVxlanFsVxlanMCastEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanMCastTableCreatApi: pSetVxlanFsVxlanMCastEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsVxlanMCastEntry =
        (tVxlanFsVxlanMCastEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANMCASTTABLE_POOLID);
    if (pVxlanFsVxlanMCastEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanMCastTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsVxlanMCastEntry, pSetVxlanFsVxlanMCastEntry,
                sizeof (tVxlanFsVxlanMCastEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
             (tRBElem *) pVxlanFsVxlanMCastEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsVxlanMCastTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanMCastEntry);
            return NULL;
        }
        return pVxlanFsVxlanMCastEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsVxlanVniVlanMapTableCreateApi
 Input       :  pVxlanFsVxlanVniVlanMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsVxlanVniVlanMapEntry *
VxlanFsVxlanVniVlanMapTableCreateApi (tVxlanFsVxlanVniVlanMapEntry *
                                      pSetVxlanFsVxlanVniVlanMapEntry)
{
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    if (pSetVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanVniVlanMapTableCreatApi: pSetVxlanFsVxlanVniVlanMapEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID);
    if (pVxlanFsVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanVniVlanMapTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsVxlanVniVlanMapEntry, pSetVxlanFsVxlanVniVlanMapEntry,
                sizeof (tVxlanFsVxlanVniVlanMapEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
             (tRBElem *) pVxlanFsVxlanVniVlanMapEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsVxlanVniVlanMapTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanVniVlanMapEntry);
            return NULL;
        }
        return pVxlanFsVxlanVniVlanMapEntry;
    }
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 Function    :  VxlanFsEvpnVxlanEviVniMapTableCreateApi
 Input       :  pVxlanFsEvpnVxlanEviVniMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsEvpnVxlanEviVniMapEntry *
VxlanFsEvpnVxlanEviVniMapTableCreateApi (tVxlanFsEvpnVxlanEviVniMapEntry *
                                         pSetVxlanFsEvpnVxlanEviVniMapEntry)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    if (pSetVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanEviVniMapTableCreatApi: pSetVxlanFsEvpnVxlanEviVniMapEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsEvpnVxlanEviVniMapEntry =
        (tVxlanFsEvpnVxlanEviVniMapEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID);
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanEviVniMapTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsEvpnVxlanEviVniMapEntry,
                pSetVxlanFsEvpnVxlanEviVniMapEntry,
                sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
             (tRBElem *) pVxlanFsEvpnVxlanEviVniMapEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsEvpnVxlanEviVniMapTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                                (UINT1 *) pVxlanFsEvpnVxlanEviVniMapEntry);
            return NULL;
        }
        return pVxlanFsEvpnVxlanEviVniMapEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanBgpRTTableCreateApi
 Input       :  pVxlanFsEvpnVxlanBgpRTEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsEvpnVxlanBgpRTEntry *
VxlanFsEvpnVxlanBgpRTTableCreateApi (tVxlanFsEvpnVxlanBgpRTEntry *
                                     pSetVxlanFsEvpnVxlanBgpRTEntry)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pVxlanFsEvpnVxlanBgpRTEntry = NULL;

    if (pSetVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanBgpRTTableCreatApi: pSetVxlanFsEvpnVxlanBgpRTEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsEvpnVxlanBgpRTEntry =
        (tVxlanFsEvpnVxlanBgpRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanBgpRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanBgpRTTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsEvpnVxlanBgpRTEntry, pSetVxlanFsEvpnVxlanBgpRTEntry,
                sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
             (tRBElem *) pVxlanFsEvpnVxlanBgpRTEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsEvpnVxlanBgpRTTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                                (UINT1 *) pVxlanFsEvpnVxlanBgpRTEntry);
            return NULL;
        }
        return pVxlanFsEvpnVxlanBgpRTEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanVrfTableCreateApi
 Input       :  pVxlanFsEvpnVxlanVrfEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsEvpnVxlanVrfEntry *
VxlanFsEvpnVxlanVrfTableCreateApi (tVxlanFsEvpnVxlanVrfEntry *
                                   pSetVxlanFsEvpnVxlanVrfEntry)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    if (pSetVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanVrfTableCreatApi: pSetVxlanFsEvpnVxlanVrfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsEvpnVxlanVrfEntry =
        (tVxlanFsEvpnVxlanVrfEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanVrfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsEvpnVxlanVrfEntry, pSetVxlanFsEvpnVxlanVrfEntry,
                sizeof (tVxlanFsEvpnVxlanVrfEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
             (tRBElem *) pVxlanFsEvpnVxlanVrfEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsEvpnVxlanVrfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                                (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
            return NULL;
        }
        return pVxlanFsEvpnVxlanVrfEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanVrfRTTableCreateApi
 Input       :  pVxlanFsEvpnVxlanVrfRTEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsEvpnVxlanVrfRTEntry *
VxlanFsEvpnVxlanVrfRTTableCreateApi (tVxlanFsEvpnVxlanVrfRTEntry *
                                     pSetVxlanFsEvpnVxlanVrfRTEntry)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    if (pSetVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanVrfRTTableCreatApi: pSetVxlanFsEvpnVxlanVrfRTEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsEvpnVxlanVrfRTEntry =
        (tVxlanFsEvpnVxlanVrfRTEntry *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID);
    if (pVxlanFsEvpnVxlanVrfRTEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanVrfRTTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsEvpnVxlanVrfRTEntry, pSetVxlanFsEvpnVxlanVrfRTEntry,
                sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
             (tRBElem *) pVxlanFsEvpnVxlanVrfRTEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsEvpnVxlanVrfRTTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                                (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
            return NULL;
        }
        return pVxlanFsEvpnVxlanVrfRTEntry;
    }
}

/****************************************************************************
 Function    :  VxlanFsEvpnVxlanMultihomedPeerTableCreateApi
 Input       :  pVxlanFsEvpnVxlanMultihomedPeerTable
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tVxlanFsEvpnVxlanMultihomedPeerTable
    *VxlanFsEvpnVxlanMultihomedPeerTableCreateApi
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pSetVxlanFsEvpnVxlanMultihomedPeerTable)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pVxlanFsEvpnVxlanMultihomedPeerTable =
        NULL;

    if (pSetVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanMultihomedPeerTableCreatApi: pSetVxlanFsEvpnVxlanMultihomedPeerTable is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pVxlanFsEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pVxlanFsEvpnVxlanMultihomedPeerTable == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsEvpnVxlanMultihomedPeerTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsEvpnVxlanMultihomedPeerTable,
                pSetVxlanFsEvpnVxlanMultihomedPeerTable,
                sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
             (tRBElem *) pVxlanFsEvpnVxlanMultihomedPeerTable) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsEvpnVxlanMultihomedPeerTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                                (UINT1 *) pVxlanFsEvpnVxlanMultihomedPeerTable);
            return NULL;
        }
        return pVxlanFsEvpnVxlanMultihomedPeerTable;
    }
}

/****************************************************************************
 Function    :  VxlanFsVxlanEcmpNveTableCreateApi
 Input       :  pVxlanFsVxlanEcmpNveEntry
 Description :  This Routine Creates the memory
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE                                                                                                                     
****************************************************************************/
tVxlanFsVxlanEcmpNveEntry *
VxlanFsVxlanEcmpNveTableCreateApi (tVxlanFsVxlanEcmpNveEntry *
                                   pSetVxlanFsVxlanEcmpNveEntry)
{
    tVxlanFsVxlanEcmpNveEntry *pVxlanFsVxlanEcmpNveEntry = NULL;
    if (pSetVxlanFsVxlanEcmpNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanEcmpNveTableCreatApi: pSetVxlanFsVxlanEcmpNveEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */ pVxlanFsVxlanEcmpNveEntry =
        (tVxlanFsVxlanEcmpNveEntry *)
        MemAllocMemBlk (VXLAN_FSVXLANNVETABLE_POOLID);
    if (pVxlanFsVxlanEcmpNveEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "VxlanFsVxlanEcmpNveTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pVxlanFsVxlanEcmpNveEntry, pSetVxlanFsVxlanEcmpNveEntry,
                sizeof (tVxlanFsVxlanEcmpNveEntry));
        if (RBTreeAdd
            (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
             (tRBElem *) pVxlanFsVxlanEcmpNveEntry) != RB_SUCCESS)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanFsVxlanEcmpNveTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanFsVxlanEcmpNveEntry);
            return NULL;
        }
        return pVxlanFsVxlanEcmpNveEntry;
    }
}

#endif /* EVPN_VXLAN_WANTED */
