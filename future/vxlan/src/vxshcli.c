/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxshcli.c,v 1.21 2018/01/11 11:18:07 siva Exp $
*
* Description: This file contains the Vxlan show commands related routines
**********************************************************************/

#include "vxinc.h"
#include "vxlannp.h"
/****************************************************************************
 * Function    :  cli_process_Vxlan_show_cmd
 * Description :  This function is exported to CLI module to handle the
                VXLAN cli show commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Vxlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VXLAN_CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;

    UNUSED_PARAM (u4CmdType);
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, VxlanMainTaskLock, VxlanMainTaskUnLock);
#endif
    VXLAN_LOCK;

    MEMSET (&ap, 0, sizeof (va_list));
    va_start (ap, u4Command);
    va_arg (ap, INT4);

    if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable != VXLAN_ENABLED)
    {
        CliPrintf (CliHandle, "VXLAN Feature is not Enabled");
        va_end (ap);
        VXLAN_UNLOCK;
        return CLI_SUCCESS;
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VXLAN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_VXLAN_NVE_INT_SHOW:
            i4RetStatus = VxlanCliShowNveInterface (CliHandle,
                                                    ((UINT4 *) args[0]),
                                                    ((INT4 *) args[1]));
            break;
        case CLI_VXLAN_NVE_VNI_SHOW:
            i4RetStatus = VxlanCliShowNveVni (CliHandle);
            break;
        case CLI_VXLAN_VLAN_VNI_SHOW:
            i4RetStatus = VxlanCliShowVlanVni (CliHandle, ((INT4 *) args[0]));
            break;
#ifdef EVPN_VXLAN_WANTED
        case CLI_VXLAN_EVPN_VNI_SHOW:
            i4RetStatus = VxlanCliShowEvpnVni (CliHandle);
            break;

        case CLI_VXLAN_EVPN_STATUS_SHOW:
            i4RetStatus = VxlanCliShowEvpnStatus (CliHandle);
            break;

        case CLI_VXLAN_EVPN_NEIGHBORS_SHOW:
            i4RetStatus = VxlanCliShowNeighborsStatus (CliHandle);
            break;

        case CLI_VXLAN_EVPN_MAC_SHOW:
            i4RetStatus = VxlanCliShowEvpnMac (CliHandle,
                                               CLI_PTR_TO_U4 (args[0]),
                                               CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_VXLAN_EVI_VNI_STATISTICS_SHOW:
            i4RetStatus = VxlanCliShowEviVniStatistics (CliHandle,
                                                        ((INT4 *) args[0]),
                                                        ((UINT4 *) args[1]));
            break;
        case CLI_VXLAN_VRF_SHOW:
            i4RetStatus = VxlanCliShowVrf (CliHandle);
            break;
        case CLI_VXLAN_VRF_STATISTICS_SHOW:
            i4RetStatus = VxlanCliShowVxlanVrfStatistics (CliHandle,
                                                          ((UINT1 *) args[0]),
                                                          CLI_PTR_TO_U4 (args
                                                                         [1]),
                                                          CLI_PTR_TO_U4 (args
                                                                         [2]));
            break;
#endif
        case CLI_VXLAN_NVE_PEER_SHOW:
            i4RetStatus = VxlanCliShowNvePeer (CliHandle, ((UINT4 *) args[0]));
            break;
        case CLI_VXLAN_VNI_STATISTICS_SHOW:
            i4RetStatus = VxlanCliShowVniStatistics (CliHandle,
                                                     ((UINT4 *) args[0]));
            break;
        case CLI_VXLAN_UDP_PORT_SHOW:
            i4RetStatus = VxlanCliShowUdpPort (CliHandle);
            break;
        case CLI_VXLAN_PACKET_HEADERS_SHOW:
            i4RetStatus = VxlanCliShowPacketHeaders (CliHandle,
                                                     ((UINT4 *) args[0]),
                                                     ((UINT4 *) args[1]),
                                                     ((UINT4 *) args[2]),
                                                     ((UINT4 *) args[3]),
                                                     ((UINT1 *) args[4]),
                                                     ((UINT1 *) args[5]),
                                                     ((UINT1 *) args[6]),
                                                     ((INT1 *) args[7]));
            break;
        case CLI_VXLAN_CLEAR_MAC:
            i4RetStatus = VxlanRemoveVniMacEntries (CliHandle,
                                                    ((UINT4 *) args[0]));
            break;

        case CLI_VXLAN_CLEAR_NVE:
            i4RetStatus = VxlanRemoveVniNveEntries (CliHandle,
                                                    ((UINT4 *) args[0]));
            break;

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif

    VXLAN_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
 Function    :  VxlanCliShowNveInterface
 Input       :  CliHandle  - cli Handle
                pu4Val     - Brief or detail interface info
                pi4IfIndex - Interface index 
 Description :  This Function will display the Nve interface information.
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowNveInterface (tCliHandle CliHandle, UINT4 *pu4Val, INT4 *pi4IfIndex)
{
    tVxlanFsVxlanVtepEntry VxlanFsVxlanVtepEntry;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT1               u1IfAdminStatus = 0;
    UINT4               u4L3IfIndex = 0;
    UINT4               u4Port = 0;
    UINT2               u2VlanId = 0;
    UINT4               u4TmpVni = 0;
    BOOL1               bCurEntryMCast = VXLAN_FALSE;

    MEMSET (&VxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = *pi4IfIndex;
    if (VxlanGetAllFsVxlanVtepTable (&VxlanFsVxlanVtepEntry) != OSIX_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, " \nNVE interface information \r\n");
    CliPrintf (CliHandle,
               "---------------------------------------------------------------------\r\n");

    CfaCliConfGetIfName ((UINT4) (VxlanFsVxlanVtepEntry.MibObject.
                                  i4FsVxlanVtepNveIfIndex), piIfName);
    CfaApiGetIfAdminStatus ((UINT4) (VxlanFsVxlanVtepEntry.MibObject.
                                     i4FsVxlanVtepNveIfIndex),
                            &u1IfAdminStatus);
    CliPrintf (CliHandle, "Interface: %s,  ", piIfName);
    CliPrintf (CliHandle, "State:%s,  ",
               ((u1IfAdminStatus == CFA_IF_UP) ? "UP" : "DOWN"));
    CliPrintf (CliHandle, "encapsulation:%s \r\n", "VXLAN");
    if (VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepAddressType ==
        VXLAN_CLI_ADDRTYPE_IPV4)
    {
        MEMCPY (&(u4VtepAddr.u4Addr),
                VxlanFsVxlanVtepEntry.MibObject.au1FsVxlanVtepAddress,
                sizeof (UINT4));

        pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
        if (NetIpv4IsLoopbackAddress (OSIX_HTONL (u4VtepAddr.u4Addr)) !=
            NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "source-interface:%s \r\n", pu1Address);
        }
        else
        {
            if (NETIPV4_SUCCESS == NetIpv4GetIfIndexFromAddr (OSIX_HTONL
                                                              (u4VtepAddr.
                                                               u4Addr),
                                                              &u4Port))
            {
                if (NETIPV4_SUCCESS ==
                    NetIpv4GetCfaIfIndexFromPort (u4Port, &u4L3IfIndex))
                {
                    if (CFA_SUCCESS ==
                        CfaCliConfGetIfName (u4L3IfIndex, piIfName))
                    {
                        CliPrintf (CliHandle, "source-interface:%s \r\n",
                                   piIfName);
                    }
                }
            }
        }

    }
    else                        /* IPV6 Address Type */
    {
        MEMCPY (&Ip6Addr,
                VxlanFsVxlanVtepEntry.MibObject.au1FsVxlanVtepAddress,
                sizeof (tUtlIn6Addr));
        pu1Address = (CHR1 *) UtlInetNtoa6 (Ip6Addr);
        CliPrintf (CliHandle, "source-interface:%s \r\n", pu1Address);
    }
    /* Detail information of Nve Interface */
    if (*pu4Val == VXLAN_CLI_DETAIL)
    {
        CliPrintf (CliHandle, "\nDetailed Nve Information\r\n");
        CliPrintf (CliHandle, "--------------------------\r\n");
        /* Multicast info */
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = *pi4IfIndex;
        pVxlanMCastEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
             (tRBElem *) & VxlanMCastEntry, NULL);
        /* Ingress Replica Details */
        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            *pi4IfIndex;
        pVxlanInReplicaEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                           (tRBElem *) & VxlanInReplicaEntry, NULL);

        if ((pVxlanMCastEntry != NULL) || (pVxlanInReplicaEntry != NULL))
        {
            if (((pVxlanMCastEntry != NULL) &&
                 (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                  *pi4IfIndex)) ||
                ((pVxlanInReplicaEntry != NULL) &&
                 (pVxlanInReplicaEntry->MibObject.
                  i4FsVxlanInReplicaNveIfIndex == *pi4IfIndex)))
            {
                CliPrintf (CliHandle, "\n\n%-5s%-10s%-30s%-20s%-30s\r\n",
                           "Vlan", "VNI", "Interface", "Mcast",
                           "Ingress-Replica-VTEP-List");
                CliPrintf (CliHandle,
                           "--------------------------------------------------------------------------------------------\r\n");
            }
            while (((pVxlanMCastEntry != NULL) &&
                    (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
                     *pi4IfIndex)) ||
                   ((pVxlanInReplicaEntry != NULL) &&
                    (pVxlanInReplicaEntry->MibObject.
                     i4FsVxlanInReplicaNveIfIndex == *pi4IfIndex)))
            {
                u2VlanId = 0;
                if (pVxlanMCastEntry == NULL)
                {
                    bCurEntryMCast = VXLAN_FALSE;
                }
                else if (pVxlanInReplicaEntry == NULL)
                {
                    bCurEntryMCast = VXLAN_TRUE;
                }
                else
                {
                    bCurEntryMCast =
                        (pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber <
                         pVxlanInReplicaEntry->MibObject.
                         u4FsVxlanInReplicaVniNumber) ? VXLAN_TRUE :
                        VXLAN_FALSE;
                }
                if (bCurEntryMCast == VXLAN_TRUE)
                {
                    u4TmpVni =
                        pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber;
                }
                else
                {
                    u4TmpVni =
                        pVxlanInReplicaEntry->MibObject.
                        u4FsVxlanInReplicaVniNumber;
                }
                VxlanUtilGetVlanFromVni (u4TmpVni, &u2VlanId);
                if (u2VlanId != 0)
                {
                    CliPrintf (CliHandle, "%-5d", u2VlanId);
                }
                else
                {
                    CliPrintf (CliHandle, "%-5s", "-");
                }
                CliPrintf (CliHandle, "%-10d", u4TmpVni);
                if (bCurEntryMCast == VXLAN_TRUE)
                {
                    if (pVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastGroupAddressType ==
                        VXLAN_CLI_ADDRTYPE_IPV4)
                    {
                        MEMCPY (&(u4VtepAddr.u4Addr),
                                pVxlanMCastEntry->MibObject.
                                au1FsVxlanMCastGroupAddress, sizeof (UINT4));
                        pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
                    }
                    else        /* IPV6 Address Type */
                    {
                        MEMCPY (&Ip6Addr,
                                pVxlanMCastEntry->MibObject.
                                au1FsVxlanMCastGroupAddress,
                                sizeof (tUtlIn6Addr));
                        pu1Address = (CHR1 *) UtlInetNtoa6 (Ip6Addr);
                    }

                    if (pVxlanMCastEntry->MibObject.
                        i4FsVxlanMCastGroupAddressType ==
                        VXLAN_CLI_ADDRTYPE_IPV4)
                    {
                        CliPrintf (CliHandle, "%-30s", piIfName);
                        CliPrintf (CliHandle, "%-20s", pu1Address);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-10s", piIfName);
                        CliPrintf (CliHandle, "%-40s", pu1Address);
                    }
                    CliPrintf (CliHandle, "%-30s", "-");
                    CliPrintf (CliHandle, "\r\n");
                    pVxlanMCastEntry = RBTreeGetNext
                        (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                         (tRBElem *) pVxlanMCastEntry, NULL);
                }
                else
                {
                    if (pVxlanInReplicaEntry->MibObject.
                        i4FsVxlanInReplicaNveIfIndex == *pi4IfIndex)
                    {
                        CliPrintf (CliHandle, "%-30s", piIfName);
                        CliPrintf (CliHandle, "%-20s", "-");

                        while (pVxlanInReplicaEntry != NULL)
                        {
                            if (u4TmpVni != pVxlanInReplicaEntry->MibObject.
                                u4FsVxlanInReplicaVniNumber)
                            {
                                break;
                            }

                            if (pVxlanInReplicaEntry->MibObject.
                                i4FsVxlanInReplicaRemoteVtepAddressType ==
                                VXLAN_CLI_ADDRTYPE_IPV4)
                            {
                                MEMCPY (&(u4VtepAddr.u4Addr),
                                        pVxlanInReplicaEntry->MibObject.
                                        au1FsVxlanInReplicaRemoteVtepAddress,
                                        pVxlanInReplicaEntry->MibObject.
                                        i4FsVxlanInReplicaRemoteVtepAddressLen);

                                pu1Address =
                                    (CHR1 *) CLI_INET_NTOA (u4VtepAddr);

                                CliPrintf (CliHandle, "%s", pu1Address);

                            }
                            else if (pVxlanInReplicaEntry->MibObject.
                                     i4FsVxlanInReplicaRemoteVtepAddressType ==
                                     VXLAN_CLI_ADDRTYPE_IPV6)

                            {
                                MEMCPY (&Ip6Addr,
                                        pVxlanInReplicaEntry->MibObject.
                                        au1FsVxlanInReplicaRemoteVtepAddress,
                                        sizeof (tUtlIn6Addr));
                                CliPrintf (CliHandle, "%s ",
                                           UtlInetNtoa6 (Ip6Addr));

                            }

                            pVxlanInReplicaEntry = RBTreeGetNext
                                (gVxlanGlobals.VxlanGlbMib.
                                 FsVxlanInReplicaTable,
                                 (tRBElem *) pVxlanInReplicaEntry, NULL);

                        }
                    }
                    CliPrintf (CliHandle, "\r\n");

                }
            }
        }

        /* Ingress Replica Details */
/*        VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex = *pi4IfIndex;
        pVxlanInReplicaEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) & VxlanInReplicaEntry, NULL);
        if(pVxlanInReplicaEntry == NULL)
        {
            return CLI_SUCCESS;
        }
        if(pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
                *pi4IfIndex)
        {
            CliPrintf (CliHandle, "\n\n%-5s%-10s%-13s%-30s \r\n",
                       "Vlan", "VNI", "Interface",  "Ingress-Replica-VTEP-List");
            CliPrintf (CliHandle,
                   "-----------------------------------------------------\r\n");
        }

        while ((pVxlanInReplicaEntry != NULL) &&
               (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
                *pi4IfIndex))
        {
            VxlanUtilGetVlanFromVni(pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber,
                                        &u2VlanId);
            CliPrintf (CliHandle, "%-5d", u2VlanId);
            CliPrintf (CliHandle, "%-10d",
                       pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber);
            CliPrintf (CliHandle, "%-13s", piIfName);

            if (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressType ==
                VXLAN_CLI_ADDRTYPE_IPV4)
           {
                for(u4Count = 0; u4Count < pVxlanInReplicaEntry->MibObject.
                                  u4FsVxlanInReplicaRemoteVTEPCount; u4Count++)
                {
                    MEMCPY (&(u4VtepAddr.u4Addr),
                            &(pVxlanInReplicaEntry->MibObject.
                                  au1FsVxlanInReplicaRemoteVtepAddress[(INT4)u4Count*(pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen)]),
                            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaRemoteVtepAddressLen);
                    pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);

                    CliPrintf (CliHandle, "%s", pu1Address);
                    if(u4Count <  (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaRemoteVTEPCount-1))
                    {
                        CliPrintf (CliHandle, ", ", pu1Address);
                    }
                }
                CliPrintf (CliHandle, "\r\n");
            }

            pVxlanInReplicaEntry = RBTreeGetNext
                (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                 (tRBElem *) pVxlanInReplicaEntry, NULL);
        }*/

    }
    return CLI_SUCCESS;

}

/****************************************************************************
 Function    :  VxlanCliShowNvePeer
 Input       :  CliHandle - Cli handle            
 Description :  This Function displays the Nve Peers configured.
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowNvePeer (tCliHandle CliHandle, UINT4 *pu4Val)
{
    INT4                i4RetVal = 0;
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    INT4                i4FsVxlanNveIfIndex = 0;
    INT4                i4NextFsVxlanNveIfIndex = 0;
    UINT4               u4FsVxlanNveVniNumber = 0;
    UINT4               u4NextFsVxlanNveVniNumber = 0;
    tMacAddr            u1FsVxlanNveDestVmMac;
    tMacAddr            NextFsVxlanNveDestVmMac;
    tMacAddr            zeroAddr;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT1               u1Flag = 0;
    UINT4               u4NvePeerCount = 0;
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (&u1FsVxlanNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (&NextFsVxlanNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];
    UINT4               u4DynamicNveCount = 0;
    UINT4               u4DynamiczeroMacCount = 0;
    INT4                i4EcmpNveCount = 0;
#ifdef EVPN_VXLAN_WANTED
    UINT4               u4EcmpNvePeerCount = 0;
#endif
    i4RetVal =
        nmhGetFirstIndexFsVxlanNveTable (&i4FsVxlanNveIfIndex,
                                         &u4FsVxlanNveVniNumber,
                                         &u1FsVxlanNveDestVmMac);
#ifdef EVPN_VXLAN_WANTED
    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                 &u4EcmpNvePeerCount);
#endif
    if ((SNMP_SUCCESS == i4RetVal)
#ifdef EVPN_VXLAN_WANTED
        || (u4EcmpNvePeerCount)
#endif
        )
    {
        if (*pu4Val == 0)
        {
            CliPrintf (CliHandle, " VTEP Peers information \r\n");
            CliPrintf (CliHandle,
                       "--------------------------------------------------------------------------------------------------\r\n");
            CliPrintf (CliHandle, "%-30s%-20s%-10s%-20s%-10s%-30s \r\n",
                       "Interface", "Peer-IP", "VNI", "VM-MAC", "MAC-Type",
                       "Arp-Suppression");
        }
    }
    else
    {
        if (*pu4Val == 0)
        {
            CliPrintf (CliHandle, "VTEP Peers are not configured\r\n");
            return CLI_SUCCESS;
        }
    }
    while (SNMP_SUCCESS == i4RetVal)
    {
        VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = i4FsVxlanNveIfIndex;

        VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4FsVxlanNveVniNumber;

        MEMCPY (&(VxlanNveEntry.MibObject.FsVxlanNveDestVmMac),
                u1FsVxlanNveDestVmMac, 6);

        i4RetVal = VxlanGetAllFsVxlanNveTable (&VxlanNveEntry);
        if (((MEMCMP (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac,
                      zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0) &&
             (VxlanNveEntry.MibObject.i4FsVxlanNveStorageType ==
              VXLAN_STRG_TYPE_VOL)) ||
            (VxlanNveEntry.MibObject.i4FsVxlanNveRemoteVtepAddressType == 0))
        {
            u4DynamiczeroMacCount++;
            i4RetVal = nmhGetNextIndexFsVxlanNveTable (i4FsVxlanNveIfIndex,
                                                       &i4NextFsVxlanNveIfIndex,
                                                       u4FsVxlanNveVniNumber,
                                                       &u4NextFsVxlanNveVniNumber,
                                                       u1FsVxlanNveDestVmMac,
                                                       &NextFsVxlanNveDestVmMac);
            i4FsVxlanNveIfIndex = i4NextFsVxlanNveIfIndex;
            u4FsVxlanNveVniNumber = u4NextFsVxlanNveVniNumber;
            MEMCPY (&u1FsVxlanNveDestVmMac, &NextFsVxlanNveDestVmMac, 6);
            continue;
        }
        if (*pu4Val == 0)
        {
            CliMacToStr (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac,
                         au1MacStr);
            if (VxlanNveEntry.MibObject.i4FsVxlanNveRemoteVtepAddressType ==
                VXLAN_CLI_ADDRTYPE_IPV4)
            {
                MEMCPY (&(u4VtepAddr.u4Addr),
                        VxlanNveEntry.MibObject.
                        au1FsVxlanNveRemoteVtepAddress, sizeof (UINT4));
                pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            }
            else                /* IPV6 Address Type */
            {
                MEMCPY (&Ip6Addr,
                        VxlanNveEntry.MibObject.
                        au1FsVxlanNveRemoteVtepAddress, sizeof (tUtlIn6Addr));
                pu1Address = (CHR1 *) UtlInetNtoa6 (Ip6Addr);
            }
            CfaCliConfGetIfName ((UINT4) VxlanNveEntry.MibObject.
                                 i4FsVxlanNveIfIndex, piIfName);
            if (VxlanNveEntry.MibObject.
                i4FsVxlanNveRemoteVtepAddressType == VXLAN_CLI_ADDRTYPE_IPV4)
            {
                CliPrintf (CliHandle, "%-30s", piIfName);
                CliPrintf (CliHandle, "%-20s", pu1Address);
            }
            else
            {
                CliPrintf (CliHandle, "%-10s", piIfName);
                CliPrintf (CliHandle, "%-40s", pu1Address);
            }
            CliPrintf (CliHandle, "%-10d",
                       VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber);

            if (MEMCMP (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac,
                        zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0)
            {
                CliPrintf (CliHandle, "%-20s", "-");
                u1Flag = 1;
            }
            else
            {
                CliPrintf (CliHandle, "%-13s", au1MacStr);
            }
            if (VxlanNveEntry.MibObject.i4FsVxlanNveStorageType ==
                VXLAN_STRG_TYPE_NON_VOL)
            {
                if (u1Flag == 1)
                {
                    CliPrintf (CliHandle, "%-10s", "-");
                }
                else
                {
                    CliPrintf (CliHandle, "%-10s", "Static");
                }

            }
            else
            {
                if (VxlanNveEntry.i4FsVxlanNveEvpnMacType == VXLAN_NVE_EVPN_MAC)
                {
                    CliPrintf (CliHandle, "%-10s", "EVPN");
                    u4DynamicNveCount++;
                    if (VxlanNveEntry.MibObject.i4FsVxlanSuppressArp ==
                        EVPN_CLI_ARP_SUPPRESS)
                    {
                        CliPrintf (CliHandle, "%-30s\r\n", "Enabled");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-30s\r\n", "Disabled");
                    }

                }
                else
                {
                    CliPrintf (CliHandle, "%-10s", "Dynamic");
                    u4DynamicNveCount++;
                }
            }
            if (VxlanNveEntry.i4FsVxlanNveEvpnMacType != VXLAN_NVE_EVPN_MAC)
            {
                CliPrintf (CliHandle, "%-30s\r\n", "-");

            }
        }
        i4RetVal =
            nmhGetNextIndexFsVxlanNveTable (i4FsVxlanNveIfIndex,
                                            &i4NextFsVxlanNveIfIndex,
                                            u4FsVxlanNveVniNumber,
                                            &u4NextFsVxlanNveVniNumber,
                                            u1FsVxlanNveDestVmMac,
                                            &NextFsVxlanNveDestVmMac);
        i4FsVxlanNveIfIndex = i4NextFsVxlanNveIfIndex;
        u4FsVxlanNveVniNumber = u4NextFsVxlanNveVniNumber;
        MEMCPY (&u1FsVxlanNveDestVmMac, &NextFsVxlanNveDestVmMac, 6);
    }
    /* Display Ecmp Nve entries */
#ifdef EVPN_VXLAN_WANTED
    VxlanCliShowEcmpNvePeer (CliHandle, pu4Val, &i4EcmpNveCount);
#endif
    if (*pu4Val == 0)
    {
        CliPrintf (CliHandle,
                   "Total no.of Nve entries learnt dynamically: %d\r\n",
                   u4DynamicNveCount + (UINT4) i4EcmpNveCount);
    }
    if (*pu4Val == VXLAN_CLI_NVE_PEERS_COUNT)
    {
        /* Count of total Nve Peers */
        RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                     &u4NvePeerCount);
        CliPrintf (CliHandle, " Total VXLAN NVE peers count: %d \r\n",
                   u4NvePeerCount - u4DynamiczeroMacCount +
                   (UINT4) i4EcmpNveCount);
        return CLI_SUCCESS;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowNveVni
 Input       :  CliHandle - Cli handle
 Description :  This Function displays the Nve VNIs configured on the router.
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowNveVni (tCliHandle CliHandle)
{
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    INT4                i4FsVxlanMCastNveIfIndex = 0;
    INT4                i4NextFsVxlanMCastNveIfIndex = 0;
    UINT4               u4FsVxlanMCastVniNumber = 0;
    UINT4               u4NextFsVxlanMCastVniNumber = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT1               u1IfAdminStatus = 0;

    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanMCastTable
        (&i4FsVxlanMCastNveIfIndex, &u4FsVxlanMCastVniNumber))
    {
        CliPrintf (CliHandle, " No NVE VNI Information\r\n");
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, " NVE VNI Information\r\n");
    CliPrintf (CliHandle,
               "-----------------------------------------------------------------------\r\n");
    CliPrintf (CliHandle, "%-12s%-30s%-20s%-10s \r\n",
               "Interface", "VNI", "Mcast", "VNI-State");

    VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber = u4FsVxlanMCastVniNumber;
    while (1)
    {
        if (VxlanGetAllFsVxlanMCastTable (&VxlanMCastEntry) == OSIX_SUCCESS)
        {
            if (VxlanMCastEntry.MibObject.
                i4FsVxlanMCastGroupAddressType == VXLAN_CLI_ADDRTYPE_IPV4)
            {
                MEMCPY (&(u4VtepAddr.u4Addr),
                        VxlanMCastEntry.MibObject.
                        au1FsVxlanMCastGroupAddress, sizeof (UINT4));
                pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            }
            else                /* IPV6 Address Type */
            {
                MEMCPY (&Ip6Addr,
                        VxlanMCastEntry.MibObject.
                        au1FsVxlanMCastGroupAddress, sizeof (tUtlIn6Addr));
                pu1Address = (CHR1 *) UtlInetNtoa6 (Ip6Addr);
            }
            CfaCliConfGetIfName ((UINT4) VxlanMCastEntry.MibObject.
                                 i4FsVxlanMCastNveIfIndex, piIfName);
            CfaApiGetIfAdminStatus ((UINT4) (VxlanMCastEntry.MibObject.
                                             i4FsVxlanMCastNveIfIndex),
                                    &u1IfAdminStatus);
            CliPrintf (CliHandle, "%-12s", piIfName);

            if (VxlanMCastEntry.MibObject.
                i4FsVxlanMCastGroupAddressType == VXLAN_CLI_ADDRTYPE_IPV4)
            {
                CliPrintf (CliHandle, "%-30d",
                           VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber);
                CliPrintf (CliHandle, "%-20s", pu1Address);
            }
            else
            {
                CliPrintf (CliHandle, "%-10d",
                           VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber);
                CliPrintf (CliHandle, "%-40s", pu1Address);
            }

            CliPrintf (CliHandle, "%-10s \r\n",
                       ((u1IfAdminStatus == CFA_IF_UP) ? "UP" : "DOWN"));
        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsVxlanMCastTable
            (i4FsVxlanMCastNveIfIndex, &i4NextFsVxlanMCastNveIfIndex,
             u4FsVxlanMCastVniNumber, &u4NextFsVxlanMCastVniNumber))
        {
            break;
        }
        i4FsVxlanMCastNveIfIndex = i4NextFsVxlanMCastNveIfIndex;
        u4FsVxlanMCastVniNumber = u4NextFsVxlanMCastVniNumber;
        VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
            i4NextFsVxlanMCastNveIfIndex;
        VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
            u4NextFsVxlanMCastVniNumber;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowVniStatistics
 Input       :  CliHandle   - Cli handle
                pu4VniID    - Vxlan Identifier
 Description :  This Function displays the VNI packets 
                Sent/Received/Droppedstatistics
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowVniStatistics (tCliHandle CliHandle, UINT4 *pu4VniID)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    INT4                i4FsVxlanVniVlanMapVlanId = 0;
    INT4                i4NextFsVxlanVniVlanMapVlanId = 0;
    UINT4               u4PktSent = 0;
    UINT4               u4PktRcvd = 0;
    UINT4               u4PktDrpd = 0;
    INT4                i4RetVal = 0;
    BOOL1               bVniEntryExists = VXLAN_FALSE;

    MEMSET (&VxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    i4RetVal =
        nmhGetFirstIndexFsVxlanVniVlanMapTable (&i4FsVxlanVniVlanMapVlanId);
    while (SNMP_SUCCESS == i4RetVal)
    {
        VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            i4FsVxlanVniVlanMapVlanId;
        i4RetVal =
            VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry);
        if (VxlanFsVxlanVniVlanMapEntry.MibObject.
            u4FsVxlanVniVlanMapVniNumber == *pu4VniID)
        {
            VxlanFsVxlanVniVlanMapEntry.MibObject.
                u4FsVxlanVniVlanMapPktSent +=
                (UINT4) VxlanHwGetVniStats (*pu4VniID);

            u4PktSent += VxlanFsVxlanVniVlanMapEntry.MibObject.
                u4FsVxlanVniVlanMapPktSent;

            VxlanFsVxlanVniVlanMapEntry.MibObject.
                u4FsVxlanVniVlanMapPktRcvd +=
                (UINT4) VxlanHwGetVniRcvdStats (*pu4VniID);

            u4PktRcvd += VxlanFsVxlanVniVlanMapEntry.MibObject.
                u4FsVxlanVniVlanMapPktRcvd;

            u4PktDrpd = VxlanFsVxlanVniVlanMapEntry.MibObject.
                u4FsVxlanVniVlanMapPktDrpd;

            if (bVniEntryExists == VXLAN_FALSE)
            {
                bVniEntryExists = VXLAN_TRUE;
            }
        }
        i4RetVal = nmhGetNextIndexFsVxlanVniVlanMapTable
            (i4FsVxlanVniVlanMapVlanId, &i4NextFsVxlanVniVlanMapVlanId);
        if (i4RetVal == SNMP_SUCCESS)
        {
            i4FsVxlanVniVlanMapVlanId = i4NextFsVxlanVniVlanMapVlanId;
        }
    }
    if (bVniEntryExists == VXLAN_TRUE)
    {
        CliPrintf (CliHandle, " VNI Statistics \r\n");
        CliPrintf (CliHandle,
                   "------------------------------------------------------------\r\n");
        CliPrintf (CliHandle, "%-10s%-20s%-25s%-25s \r\n",
                   "VNI",
                   "VXLAN Pkt Sent Cnt ",
                   "VXLAN Pkt Received Cnt ", "VXLAN Pkt Dropped Cnt");
        CliPrintf (CliHandle,
                   "%-10d%-20d%-25d%-25d\r\n",
                   *pu4VniID, u4PktSent, u4PktRcvd, u4PktDrpd);
    }
    else
    {
        CliPrintf (CliHandle, " No VNI-Vlan map Entry \r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowUdpPort
 Input       :  CliHandle  - Cli handle
 Description :  This Function displays the Configured UDP port number
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowUdpPort (tCliHandle CliHandle)
{
    UINT4               u4FsVxlanUdpPort = 0;
    VxlanGetFsVxlanUdpPort (&u4FsVxlanUdpPort);
    CliPrintf (CliHandle, "Udp Port Number:%d\r\n", u4FsVxlanUdpPort);
    return CLI_SUCCESS;
}

/****************************************************************************
Function    :  VxlanCliShowVlanVni
Input       :  CliHandle - Cli handle
Description :  This Function displays the Vlan VNIs configured on the router.
Output      :  None
Returns     :  CLI_SUCCESS or CLI_FAILURE
*****************************************************************************/
INT4
VxlanCliShowVlanVni (tCliHandle CliHandle, INT4 *pi4VlanId)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    INT4                i4FsVxlanVniVlanMapVlanId = 0;
    INT4                i4NextFsVxlanVniVlanMapVlanId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT1               u1IfAdminStatus = 0;
    UINT4               u4NveIndex = 0;
#ifdef EVPN_VXLAN_WANTED
    INT4                i4EviId = 0;
    UINT4               u4VniId = 0;
    UINT1               u1ESIString[EVPN_MAX_ESI_LEN];

    MEMSET (u1ESIString, 0, EVPN_MAX_ESI_LEN);
#endif
    MEMSET (&VxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];
    if (pi4VlanId == NULL)
    {
        if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanVniVlanMapTable
            (&i4FsVxlanVniVlanMapVlanId))
        {
            CliPrintf (CliHandle, " No VLAN VNI Mapping\r\n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, " VLAN VNI Mapping\r\n");
        CliPrintf (CliHandle, "-------------------------\r\n");
        CliPrintf (CliHandle, "%-12s%-30s% \r\n", "Vlan", "VNI");
        VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            i4FsVxlanVniVlanMapVlanId;
        while (1)
        {
            if (VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry)
                == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "%-12d",
                           VxlanFsVxlanVniVlanMapEntry.MibObject.
                           i4FsVxlanVniVlanMapVlanId);
                CliPrintf (CliHandle, "%-30d\r\n",
                           VxlanFsVxlanVniVlanMapEntry.MibObject.
                           u4FsVxlanVniVlanMapVniNumber);
            }
            if (SNMP_SUCCESS != nmhGetNextIndexFsVxlanVniVlanMapTable
                (i4FsVxlanVniVlanMapVlanId, &i4NextFsVxlanVniVlanMapVlanId))
            {
                break;
            }
            i4FsVxlanVniVlanMapVlanId = i4NextFsVxlanVniVlanMapVlanId;
            VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
                i4NextFsVxlanVniVlanMapVlanId;
        }
    }
    else
    {
        VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            *pi4VlanId;
        if (VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry) ==
            OSIX_SUCCESS)
        {
            /* VNIs mapped with Nve interface */
            if ((VxlanUtilGetNveIndexFromNveTable
                 (VxlanFsVxlanVniVlanMapEntry.MibObject.
                  u4FsVxlanVniVlanMapVniNumber, &u4NveIndex) != NULL)
                ||
                (VxlanUtilGetNveIndexFromMcastTable
                 (VxlanFsVxlanVniVlanMapEntry.MibObject.
                  u4FsVxlanVniVlanMapVniNumber, &u4NveIndex) != NULL)
                ||
                (VxlanUtilGetNveIndexFromInReplicaTable
                 (VxlanFsVxlanVniVlanMapEntry.MibObject.
                  u4FsVxlanVniVlanMapVniNumber, &u4NveIndex) != NULL))
            {
                CliPrintf (CliHandle, " VLAN VNI information\r\n");
                CliPrintf (CliHandle, "-------------------------\r\n");
                CliPrintf (CliHandle, "Vlan-Id: %d, ",
                           VxlanFsVxlanVniVlanMapEntry.MibObject.
                           i4FsVxlanVniVlanMapVlanId);
                CliPrintf (CliHandle, "VNI: %d, ",
                           VxlanFsVxlanVniVlanMapEntry.MibObject.
                           u4FsVxlanVniVlanMapVniNumber);
#ifdef EVPN_VXLAN_WANTED
                /* DF election is defined only for Multi-homing scenario */
                u4VniId =
                    VxlanFsVxlanVniVlanMapEntry.MibObject.
                    u4FsVxlanVniVlanMapVniNumber;

                if (EvpnUtilGetEviFromVni (u4VniId, &i4EviId) == VXLAN_SUCCESS)
                {
                    if (EvpnUtilGetESIFromVniMapTable (i4EviId, u4VniId,
                                                       u1ESIString) ==
                        VXLAN_SUCCESS)
                    {
                        if (VxlanFsVxlanVniVlanMapEntry.MibObject.
                            i4FsVxlanVniVlanDfElection == 1)
                        {
                            CliPrintf (CliHandle, "DF State: Enabled, ");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "DF State: Disabled, ");
                        }
                    }
                }
#endif
                CfaCliConfGetIfName (u4NveIndex, piIfName);
                CfaApiGetIfAdminStatus (u4NveIndex, &u1IfAdminStatus);
                CliPrintf (CliHandle, "Interface: %s,  ", piIfName);
                CliPrintf (CliHandle, "State:%s\r\n ",
                           ((u1IfAdminStatus == CFA_IF_UP) ? "UP" : "DOWN"));
            }

        }
        else
        {
            CliPrintf (CliHandle, " VLAN %d is not mapped with VNI\r\n",
                       *pi4VlanId);
        }
    }
    return CLI_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
Function    :  VxlanCliShowEvpnVni
Input       :  CliHandle - Cli handle
Description :  This Function displays the Evpn VNIs configured on the router.
Output      :  None
Returns     :  CLI_SUCCESS or CLI_FAILURE
*****************************************************************************/
INT4
VxlanCliShowEvpnVni (tCliHandle CliHandle)
{
    INT4                i4FsEvpnVxlanEviVniMapEviIndex = 0;
    INT4                i4NextFsEvpnVxlanEviVniMapEviIndex = 0;
    INT4                i4FsEvpnTempVxlanEviVniMapEviIndex = 0;
    INT4                i4FsEvpnVxlanBgpRTType = 0;
    INT4                i4NextFsEvpnVxlanBgpRTType = 0;
    INT4               *pi4NextFsEvpnVxlanBgpRTType = NULL;
    UINT4               u4FsEvpnVxlanEviVniMapVniNumber = 0;
    UINT4               u4NextFsEvpnVxlanEviVniMapVniNumber = 0;
    UINT4               u4NextFsEvpnVxlanBgpRTIndex = 0;
    UINT4              *pu4NextFsEvpnVxlanBgpRTIndex = NULL;
    UINT4               u4FsEvpnVxlanBgpRTIndex = 0;
    UINT4               u4FsEvpnTempVxlanEviVniMapVniNumber = 0;
    UINT4               u4AsnRt = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;
    UINT4               u4AsignedRtNumberVal = 0;
    UINT2               u2AsignedRtNumberVal = 0;
    UINT2               u2AsnRt = 0;
    UINT2               u2ASN = 0;
    UINT1               au1EvpnVxlanBgpRD[EVPN_MAX_RD_RT_STRING_LEN];
    UINT1               au1RtTypeString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT1               au1RTString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT1               au1RDString[EVPN_MAX_RD_RT_STRING_LEN];
    INT4                i4Flag1 = 0;
    INT4                i4Flag2 = 0;

    tVxlanFsEvpnVxlanEviVniMapEntry VxlanFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanBgpRTEntry VxlanFsEvpnVxlanBgpRTEntry;

    MEMSET (au1EvpnVxlanBgpRD, 0, EVPN_MAX_RD_RT_STRING_LEN);
    MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (au1RTString, 0, EVPN_MAX_RD_RT_STRING_LEN);
    MEMSET (au1RDString, 0, EVPN_MAX_RD_RT_STRING_LEN);

    pu4NextFsEvpnVxlanBgpRTIndex = &u4NextFsEvpnVxlanBgpRTIndex;
    pi4NextFsEvpnVxlanBgpRTType = &i4NextFsEvpnVxlanBgpRTType;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsEvpnVxlanEviVniMapTable
        (&i4FsEvpnVxlanEviVniMapEviIndex, &u4FsEvpnVxlanEviVniMapVniNumber))
    {
        CliPrintf (CliHandle, " No EVI VNI Mapping\r\n");
        return CLI_SUCCESS;
    }

    /* Display All */
    CliPrintf (CliHandle, "EVI VNI Map Entries\r\n");
    CliPrintf (CliHandle, "-------------------\r\n");
    CliPrintf (CliHandle, "%-12s%-15s%-20s%-15s %-15s% \r\n",
               "EVI", "VNI", "RD", "RT_Type", "RT");
    while (1)
    {
        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex = i4FsEvpnVxlanEviVniMapEviIndex;
        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber = u4FsEvpnVxlanEviVniMapVniNumber;

        if (OSIX_SUCCESS ==
            VxlanGetAllFsEvpnVxlanEviVniMapTable
            (&VxlanFsEvpnVxlanEviVniMapEntry))
        {
            CliPrintf (CliHandle, "%-12d",
                       VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
                       i4FsEvpnVxlanEviVniMapEviIndex);
            CliPrintf (CliHandle, "%-15d",
                       VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
                       u4FsEvpnVxlanEviVniMapVniNumber);
        }

        /* To retrieve RD value */
        if (0 !=
            MEMCMP (au1EvpnVxlanBgpRD,
                    EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry),
                    EVPN_MAX_RD_RT_STRING_LEN))

        {
            MEMSET (au1RDString, 0, EVPN_MAX_RD_RT_STRING_LEN);
            /* RD is of type  ASN:nn */
            if (EVPN_RD_TYPE_0 ==
                EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[0])
            {
                MEMCPY (&u2ASN,
                        &EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[2],
                        sizeof (UINT2));
                MEMCPY (&u4AsignedNumber,
                        &EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[4],
                        sizeof (UINT4));
                u2ASN = OSIX_NTOHS (u2ASN);
                u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
                SPRINTF ((char *) au1RDString, "%d:%u", u2ASN, u4AsignedNumber);
            }
            /* RT is of type  ASN:nn */
            else if (EVPN_RD_TYPE_1 ==
                     EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[0])
            {
                MEMCPY (&u2AsignedNumber,
                        &EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[6],
                        sizeof (UINT2));
                u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                SPRINTF ((char *) au1RDString, "%d.%d.%d.%d:%d",
                         EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[2],
                         EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[3],
                         EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[4],
                         EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[5],
                         u2AsignedNumber);
            }
            /* RT is of type  ASN.ASN:nn */
            else
            {
                MEMCPY (&u4ASN,
                        &EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[2],
                        sizeof (UINT4));
                MEMCPY (&u2AsignedNumber,
                        &EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry)[6],
                        sizeof (UINT2));
                u4ASN = OSIX_NTOHL (u4ASN);
                u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                SPRINTF ((char *) au1RDString, "%d.%d:%hu",
                         (UINT4) ((u4ASN & 0xffff0000) >> 16),
                         (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
            }
            if (EVPN_BGP_RD_AUTO (VxlanFsEvpnVxlanEviVniMapEntry) == OSIX_TRUE)
            {
                MEMSET (au1RDString, 0, EVPN_MAX_RD_RT_STRING_LEN);
                MEMCPY (au1RDString, "auto", 4);
                CliPrintf (CliHandle, "%-20s", au1RDString);
            }
            else
            {
                CliPrintf (CliHandle, "%-20s", au1RDString);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%-20s", "-");
        }
        i4Flag1 = 0;
        /* To retrieve RT value */
        if (SNMP_SUCCESS ==
            nmhGetNextIndexFsEvpnVxlanBgpRTTable (0,
                                                  &i4NextFsEvpnVxlanEviVniMapEviIndex,
                                                  0,
                                                  &u4NextFsEvpnVxlanEviVniMapVniNumber,
                                                  0,
                                                  pu4NextFsEvpnVxlanBgpRTIndex,
                                                  0,
                                                  pi4NextFsEvpnVxlanBgpRTType))
        {
            i4Flag2 = 0;
            do
            {
                u4FsEvpnTempVxlanEviVniMapVniNumber =
                    u4NextFsEvpnVxlanEviVniMapVniNumber;
                i4FsEvpnTempVxlanEviVniMapEviIndex =
                    i4NextFsEvpnVxlanEviVniMapEviIndex;
                u4FsEvpnVxlanBgpRTIndex = *pu4NextFsEvpnVxlanBgpRTIndex;
                i4FsEvpnVxlanBgpRTType = *pi4NextFsEvpnVxlanBgpRTType;

                /* RT not configure on VNI */
                if ((u4FsEvpnVxlanEviVniMapVniNumber ==
                     u4NextFsEvpnVxlanEviVniMapVniNumber)
                    && (i4FsEvpnVxlanEviVniMapEviIndex ==
                        i4NextFsEvpnVxlanEviVniMapEviIndex))
                {
                    i4Flag2++;
                    MEMSET (&VxlanFsEvpnVxlanBgpRTEntry, 0,
                            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
                    VxlanFsEvpnVxlanBgpRTEntry.
                        MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
                        i4FsEvpnVxlanEviVniMapEviIndex;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        u4FsEvpnVxlanEviVniMapVniNumber =
                        u4FsEvpnVxlanEviVniMapVniNumber;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        u4FsEvpnVxlanBgpRTIndex = u4FsEvpnVxlanBgpRTIndex;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        i4FsEvpnVxlanBgpRTType = i4FsEvpnVxlanBgpRTType;

                    if (OSIX_SUCCESS !=
                        VxlanGetAllFsEvpnVxlanBgpRTTable
                        (&VxlanFsEvpnVxlanBgpRTEntry))
                    {
                        break;
                    }

                    if (EVPN_BGP_RT_AUTO (VxlanFsEvpnVxlanBgpRTEntry) ==
                        OSIX_TRUE)
                    {
                        MEMSET (au1RtTypeString, 0, EVPN_MAX_RD_RT_STRING_LEN);
                        MEMSET (au1RTString, 0, EVPN_MAX_RD_RT_STRING_LEN);
                        /* Retrive RT Type */
                        if (1 == i4FsEvpnVxlanBgpRTType)
                        {
                            MEMCPY (au1RtTypeString, "import", 6);
                        }
                        else if (2 == i4FsEvpnVxlanBgpRTType)
                        {
                            MEMCPY (au1RtTypeString, "export", 6);
                        }
                        else
                        {
                            MEMCPY (au1RtTypeString, "both", 4);
                        }
                        MEMCPY (au1RTString, "auto", 4);
                        CliPrintf (CliHandle, "%-15s %-15s\n", au1RtTypeString,
                                   au1RTString);
                    }
                    else
                    {
                        MEMSET (au1RtTypeString, 0, EVPN_MAX_RD_RT_STRING_LEN);
                        MEMSET (au1RTString, 0, EVPN_MAX_RD_RT_STRING_LEN);
                        /* Retrive RT Type */
                        if (1 == i4FsEvpnVxlanBgpRTType)
                        {
                            MEMCPY (au1RtTypeString, "import", 6);
                        }
                        else if (2 == i4FsEvpnVxlanBgpRTType)
                        {
                            MEMCPY (au1RtTypeString, "export", 6);
                        }
                        else
                        {
                            MEMCPY (au1RtTypeString, "both", 4);
                        }

                        /* RT is of type  ASN:nn */
                        if (EVPN_RT_TYPE_0 ==
                            EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)[0])
                        {
                            MEMCPY (&u2AsnRt,
                                    &EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                    [2], sizeof (UINT2));
                            MEMCPY (&u4AsignedRtNumberVal,
                                    &EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                    [4], sizeof (UINT4));
                            u2AsnRt = OSIX_NTOHS (u2AsnRt);
                            u4AsignedRtNumberVal =
                                OSIX_NTOHL (u4AsignedRtNumberVal);
                            SPRINTF ((char *) au1RTString, "%d:%u", u2AsnRt,
                                     u4AsignedRtNumberVal);
                        }
                        /* RT is of type  ASN:nn */
                        else if (EVPN_RT_TYPE_1 ==
                                 EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)[0])
                        {
                            MEMCPY (&u2AsignedRtNumberVal,
                                    &EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                    [6], sizeof (UINT2));
                            u2AsignedRtNumberVal =
                                OSIX_NTOHS (u2AsignedRtNumberVal);
                            SPRINTF ((char *) au1RTString, "%d.%d.%d.%d:%d",
                                     EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                     [2],
                                     EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                     [3],
                                     EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                     [4],
                                     EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                     [5], u2AsignedRtNumberVal);
                        }
                        /* RT is of type  ASN.ASN:nn */
                        else
                        {
                            MEMCPY (&u4AsnRt,
                                    &EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                    [2], sizeof (UINT4));
                            MEMCPY (&u2AsignedRtNumberVal,
                                    &EVPN_BGP_RT (VxlanFsEvpnVxlanBgpRTEntry)
                                    [6], sizeof (UINT2));
                            u4AsnRt = OSIX_NTOHL (u4AsnRt);
                            u2AsignedRtNumberVal =
                                OSIX_NTOHS (u2AsignedRtNumberVal);
                            SPRINTF ((char *) au1RTString, "%d.%d:%hu",
                                     (UINT4) ((u4AsnRt & 0xffff0000) >> 16),
                                     (UINT4) (u4AsnRt & 0x0000ffff),
                                     u2AsignedRtNumberVal);
                        }
                        i4Flag1++;
                        if (i4Flag1 == 1)
                        {
                            CliPrintf (CliHandle, "%-15s %-15s\n",
                                       au1RtTypeString, au1RTString);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%47s%-15s %-15s\n", " ",
                                       au1RtTypeString, au1RTString);
                        }
                    }
                }
            }
            while (nmhGetNextIndexFsEvpnVxlanBgpRTTable
                   (i4FsEvpnTempVxlanEviVniMapEviIndex,
                    &i4NextFsEvpnVxlanEviVniMapEviIndex,
                    u4FsEvpnTempVxlanEviVniMapVniNumber,
                    &u4NextFsEvpnVxlanEviVniMapVniNumber,
                    u4FsEvpnVxlanBgpRTIndex, pu4NextFsEvpnVxlanBgpRTIndex,
                    i4FsEvpnVxlanBgpRTType, pi4NextFsEvpnVxlanBgpRTType));
        }
        if (i4Flag2 == 0)
        {
            CliPrintf (CliHandle, "%-15s %-15s", "-", "-");
        }
        CliPrintf (CliHandle, "\r\n");
        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsEvpnVxlanEviVniMapTable
            (i4FsEvpnVxlanEviVniMapEviIndex,
             &i4NextFsEvpnVxlanEviVniMapEviIndex,
             u4FsEvpnVxlanEviVniMapVniNumber,
             &u4NextFsEvpnVxlanEviVniMapVniNumber))
        {
            break;
        }
        i4FsEvpnVxlanEviVniMapEviIndex = i4NextFsEvpnVxlanEviVniMapEviIndex;
        u4FsEvpnVxlanEviVniMapVniNumber = u4NextFsEvpnVxlanEviVniMapVniNumber;

        MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
                sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex = i4NextFsEvpnVxlanEviVniMapEviIndex;
        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber =
            u4NextFsEvpnVxlanEviVniMapVniNumber;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowEviVniStatistics
 Input       :  CliHandle   - Cli handle
                pi4EviID    - EVI Identifier
                pu4VniID    - VNI Identifier
 Description :  This Function displays the EVI-VNI map packets 
                Sent/Received/Droppedstatistics
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowEviVniStatistics (tCliHandle CliHandle, INT4 *pi4EviID,
                              UINT4 *pu4VniID)
{
    tVxlanFsEvpnVxlanEviVniMapEntry VxlanSetFsEvpnVxlanEviVniMapEntry;
    INT4                i4EviIndex = 0;
    INT4                i4NextEviIndex = 0;
    UINT4               u4VniNumber = 0;
    UINT4               u4NextVniNumber = 0;
    UINT4               u4PktSent = 0;
    UINT4               u4PktRcvd = 0;
    UINT4               u4PktDrpd = 0;
    INT4                i4RetVal = 0;
    BOOL1               bVniEntryExists = VXLAN_FALSE;

    MEMSET (&VxlanSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    i4RetVal = nmhGetFirstIndexFsEvpnVxlanEviVniMapTable (&i4EviIndex,
                                                          &u4VniNumber);
    while (SNMP_SUCCESS == i4RetVal)
    {
        VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;
        VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber = u4VniNumber;
        i4RetVal =
            VxlanGetAllFsEvpnVxlanEviVniMapTable
            (&VxlanSetFsEvpnVxlanEviVniMapEntry);
        if ((VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
             i4FsEvpnVxlanEviVniMapEviIndex == *pi4EviID)
            && (VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
                u4FsEvpnVxlanEviVniMapVniNumber == *pu4VniID))
        {
            u4PktSent += VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
                u4FsEvpnVxlanEviVniMapSentPkts;
            u4PktRcvd = VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
                u4FsEvpnVxlanEviVniMapRcvdPkts;
            u4PktDrpd = VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
                u4FsEvpnVxlanEviVniMapDroppedPkts;
            if (bVniEntryExists == VXLAN_FALSE)
            {
                bVniEntryExists = VXLAN_TRUE;
            }
        }
        i4RetVal =
            nmhGetNextIndexFsEvpnVxlanEviVniMapTable (i4EviIndex,
                                                      &i4NextEviIndex,
                                                      u4VniNumber,
                                                      &u4NextVniNumber);
        if (i4RetVal == SNMP_SUCCESS)
        {
            i4EviIndex = i4NextEviIndex;
            u4VniNumber = u4NextVniNumber;
        }
    }
    if (bVniEntryExists == VXLAN_TRUE)
    {
        CliPrintf (CliHandle, " EVI-VNI Statistics \r\n");
        CliPrintf (CliHandle,
                   "--------------------------------------------------------------------\r\n");
        CliPrintf (CliHandle, "%-10s%-10s%-20s%-25s%-25s \r\n",
                   "EVI",
                   "VNI",
                   "EVPN Pkt Sent Cnt ",
                   "EVPN Pkt Received Cnt ", "EVPN Pkt Dropped Cnt");
        CliPrintf (CliHandle,
                   "%-10d%-10d%-20d%-25d%-25d\r\n",
                   *pi4EviID, *pu4VniID, u4PktSent, u4PktRcvd, u4PktDrpd);
    }
    else
    {
        CliPrintf (CliHandle, " No EVI VNI map Entry \r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowVxlanVrfStatistics
 Input       :  CliHandle   - Cli handle
 Description :  This Function displays the EVI-VNI map packets 
                Sent/Received/Droppedstatistics
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowVxlanVrfStatistics (tCliHandle CliHandle, UINT1 *pu1VrfName,
                                UINT4 u4Vnid, UINT4 u4VrfNameLength)
{

    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry;
    pVxlanFsEvpnVxlanVrfEntry =
        EvpnApiGetVrfTableEntry (pu1VrfName, u4VrfNameLength, u4Vnid);

    if (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        CliPrintf (CliHandle, " VXLAN-VRF Statistics \r\n");
        CliPrintf (CliHandle,
                   "-----------------------------------------------------------------------------------------------\r\n");
        CliPrintf (CliHandle, "%-20s%-10s%-20s%-25s%-25s \r\n",
                   "VXLAN-VRF",
                   "VNI",
                   "VRF Pkt Sent Cnt ",
                   "VRF Pkt Received Cnt ", "VRF Pkt Dropped Cnt");
        CliPrintf (CliHandle,
                   "%-20s%-10d%-20d%-25d%-25d\r\n",
                   pu1VrfName, u4Vnid,
                   pVxlanFsEvpnVxlanVrfEntry->MibObject.
                   u4FsEvpnVxlanVrfVniMapSentPkts,
                   pVxlanFsEvpnVxlanVrfEntry->MibObject.
                   u4FsEvpnVxlanVrfVniMapRcvdPkts,
                   pVxlanFsEvpnVxlanVrfEntry->MibObject.
                   u4FsEvpnVxlanVrfVniMapDroppedPkts);

    }
    else
    {
        CliPrintf (CliHandle, " No EVI VNI map Entry \r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************
Function    :  VxlanCliShowEvpnMac
Input       :  CliHandle - Cli handle
Description :  This Function displays the MAC details configured on the router.
Output      :  None
Returns     :  CLI_SUCCESS or CLI_FAILURE
*****************************************************************************/
INT4
VxlanCliShowEvpnMac (tCliHandle CliHandle, UINT4 u4EviIdx, INT4 i4MacType)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4EviIdx);
    UNUSED_PARAM (i4MacType);

    /* TBD: Validation need to be done */

    return CLI_SUCCESS;
}

/****************************************************************************
Function    :  VxlanCliShowEvpnStatus
Input       :  CliHandle - Cli handle
Description :  This Function displays EVPN enable status, 
               DF Election details, Split-horizon status.
Output      :  None
Returns     :  CLI_SUCCESS or CLI_FAILURE
*****************************************************************************/
INT4
VxlanCliShowEvpnStatus (tCliHandle CliHandle)
{
    INT4                i4FsEvpnVxlanEnable = 0;
    nmhGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    tMacAddr            au1RetValFsEvpnAnycastGwMac;
    tMacAddr            tZeroFsEvpnAnycastGwMac;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];

    MEMSET (tZeroFsEvpnAnycastGwMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1RetValFsEvpnAnycastGwMac, 0, sizeof (tMacAddr));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    nmhGetFsEvpnAnycastGwMac (&au1RetValFsEvpnAnycastGwMac);

    if (i4FsEvpnVxlanEnable == EVPN_ENABLED)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Evpn System Status", "Enable");
        if (0 != MEMCMP (au1RetValFsEvpnAnycastGwMac,
                         tZeroFsEvpnAnycastGwMac, VXLAN_ETHERNET_ADDR_SIZE))
        {
            CliMacToStr (au1RetValFsEvpnAnycastGwMac, au1MacStr);
            au1MacStr[17] = '\0';
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Anycast Gateway MAC",
                       au1MacStr);
        }

    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Evpn System Status",
                   "Disable");
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
Function    :  VxlanCliShowNeighborsStatus
Input       :  CliHandle - Cli handle
Description :  This Function displays EVPN enable status, 
               DF Election details, Split-horizon status.
Output      :  None
Returns     :  CLI_SUCCESS or CLI_FAILURE
*****************************************************************************/
INT4
VxlanCliShowNeighborsStatus (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    /* TBD: Validation need to be done */

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowEcmpNvePeer
 Input       :  CliHandle - Cli handle
 Description :  This Function displays the Ecmp Nve Peers configured.
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowEcmpNvePeer (tCliHandle CliHandle, UINT4 *pu4Val,
                         INT4 *pi4EcmpNveCount)
{
    INT4                i4RetVal = 0;
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    INT4                i4FsVxlanEcmpNveIfIndex = 0;
    INT4                i4NextFsVxlanEcmpNveIfIndex = 0;
    UINT4               u4FsVxlanEcmpNveVniNumber = 0;
    UINT4               u4NextFsVxlanEcmpNveVniNumber = 0;
    tMacAddr            u1FsVxlanEcmpNveDestVmMac;
    tMacAddr            NextFsVxlanEcmpNveDestVmMac;
    INT4                i4FsVxlanEcmpNveRemoteVtepAddressType = 0;
    INT4                i4NextFsVxlanEcmpNveRemoteVtepAddressType = 0;
    tSNMP_OCTET_STRING_TYPE FsVxlanEcmpNveRemoteVtepAddress;
    tSNMP_OCTET_STRING_TYPE NextFsVxlanEcmpNveRemoteVtepAddress;
    tMacAddr            zeroAddr;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    INT4                i4EcmpNveCount = 0;
    UINT1               au1AddressVal[128];
    UINT1               au1NextAddressVal[128];
    UINT1               u1Flag = 0;
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));
    MEMSET (&u1FsVxlanEcmpNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (&NextFsVxlanEcmpNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (&FsVxlanEcmpNveRemoteVtepAddress, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1AddressVal, 0, sizeof (au1AddressVal));
    FsVxlanEcmpNveRemoteVtepAddress.pu1_OctetList = au1AddressVal;

    MEMSET (&NextFsVxlanEcmpNveRemoteVtepAddress, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextAddressVal, 0, sizeof (au1NextAddressVal));
    NextFsVxlanEcmpNveRemoteVtepAddress.pu1_OctetList = au1NextAddressVal;
    i4RetVal =
        nmhGetFirstIndexFsVxlanEcmpNveTable (&i4FsVxlanEcmpNveIfIndex,
                                             &u4FsVxlanEcmpNveVniNumber,
                                             &u1FsVxlanEcmpNveDestVmMac,
                                             &i4FsVxlanEcmpNveRemoteVtepAddressType,
                                             &FsVxlanEcmpNveRemoteVtepAddress);
    while (SNMP_SUCCESS == i4RetVal)
    {
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex =
            i4FsVxlanEcmpNveIfIndex;

        VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
            u4FsVxlanEcmpNveVniNumber;

        MEMCPY (&(VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac),
                u1FsVxlanEcmpNveDestVmMac, 6);
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
            i4FsVxlanEcmpNveRemoteVtepAddressType;
        MEMCPY (VxlanEcmpNveEntry.MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
                FsVxlanEcmpNveRemoteVtepAddress.pu1_OctetList,
                FsVxlanEcmpNveRemoteVtepAddress.i4_Length);

        i4RetVal = VxlanGetAllFsVxlanEcmpNveTable (&VxlanEcmpNveEntry);
        if (((MEMCMP (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                      zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0) &&
             (VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveStorageType ==
              VXLAN_STRG_TYPE_VOL)))
        {
            i4RetVal =
                nmhGetNextIndexFsVxlanEcmpNveTable (i4FsVxlanEcmpNveIfIndex,
                                                    &i4NextFsVxlanEcmpNveIfIndex,
                                                    u4FsVxlanEcmpNveVniNumber,
                                                    &u4NextFsVxlanEcmpNveVniNumber,
                                                    u1FsVxlanEcmpNveDestVmMac,
                                                    &NextFsVxlanEcmpNveDestVmMac,
                                                    i4FsVxlanEcmpNveRemoteVtepAddressType,
                                                    &i4NextFsVxlanEcmpNveRemoteVtepAddressType,
                                                    &FsVxlanEcmpNveRemoteVtepAddress,
                                                    &NextFsVxlanEcmpNveRemoteVtepAddress);
            i4FsVxlanEcmpNveIfIndex = i4NextFsVxlanEcmpNveIfIndex;
            u4FsVxlanEcmpNveVniNumber = u4NextFsVxlanEcmpNveVniNumber;
            MEMCPY (&u1FsVxlanEcmpNveDestVmMac, &NextFsVxlanEcmpNveDestVmMac,
                    6);
            i4FsVxlanEcmpNveRemoteVtepAddressType =
                i4NextFsVxlanEcmpNveRemoteVtepAddressType;
            MEMCPY (&FsVxlanEcmpNveRemoteVtepAddress,
                    &NextFsVxlanEcmpNveRemoteVtepAddress,
                    sizeof (NextFsVxlanEcmpNveRemoteVtepAddress));
            continue;
        }
        if (*pu4Val == 0)
        {
            CliMacToStr (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                         au1MacStr);
            if (VxlanEcmpNveEntry.MibObject.
                i4FsVxlanEcmpNveRemoteVtepAddressType ==
                VXLAN_CLI_ADDRTYPE_IPV4)
            {
                MEMCPY (&(u4VtepAddr.u4Addr),
                        VxlanEcmpNveEntry.MibObject.
                        au1FsVxlanEcmpNveRemoteVtepAddress, sizeof (UINT4));
                pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            }
            else                /* IPV6 Address Type */
            {
                MEMCPY (&Ip6Addr,
                        VxlanEcmpNveEntry.MibObject.
                        au1FsVxlanEcmpNveRemoteVtepAddress,
                        sizeof (tUtlIn6Addr));
                pu1Address = (CHR1 *) UtlInetNtoa6 (Ip6Addr);
            }
            CfaCliConfGetIfName ((UINT4) VxlanEcmpNveEntry.MibObject.
                                 i4FsVxlanEcmpNveIfIndex, piIfName);
            if (VxlanEcmpNveEntry.MibObject.
                i4FsVxlanEcmpNveRemoteVtepAddressType ==
                VXLAN_CLI_ADDRTYPE_IPV4)
            {
                CliPrintf (CliHandle, "%-30s", piIfName);
                CliPrintf (CliHandle, "(E)%-17s", pu1Address);
            }
            else
            {
                CliPrintf (CliHandle, "%-10s", piIfName);
                CliPrintf (CliHandle, "(E)%-37s", pu1Address);
            }
            CliPrintf (CliHandle, "%-10d",
                       VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber);

            if (MEMCMP (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
                        zeroAddr, VXLAN_ETHERNET_ADDR_SIZE) == 0)
            {
                CliPrintf (CliHandle, "%-20s", "-");
                u1Flag = 1;
            }
            else
            {
                CliPrintf (CliHandle, "%-13s", au1MacStr);
            }
            if (VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveStorageType ==
                VXLAN_STRG_TYPE_NON_VOL)
            {
                if (u1Flag == 1)
                {
                    CliPrintf (CliHandle, "%-10s\r\n", "-");
                }
                else
                {
                    CliPrintf (CliHandle, "%-10s\r\n", "Static");
                }

            }
            else if (VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveStorageType ==
                     VXLAN_STRG_TYPE_VOL)
            {
                CliPrintf (CliHandle, "%-10s", "EVPN");
                if (VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpSuppressArp ==
                    EVPN_CLI_ARP_SUPPRESS)
                {
                    CliPrintf (CliHandle, "%-30s\r\n", "Enabled");
                }
                else
                {
                    CliPrintf (CliHandle, "%-30s\r\n", "Disabled");
                }

            }
            else
            {
                CliPrintf (CliHandle, "%-30s\r\n", "-");

            }

        }
        i4EcmpNveCount++;
        i4RetVal =
            nmhGetNextIndexFsVxlanEcmpNveTable (i4FsVxlanEcmpNveIfIndex,
                                                &i4NextFsVxlanEcmpNveIfIndex,
                                                u4FsVxlanEcmpNveVniNumber,
                                                &u4NextFsVxlanEcmpNveVniNumber,
                                                u1FsVxlanEcmpNveDestVmMac,
                                                &NextFsVxlanEcmpNveDestVmMac,
                                                i4FsVxlanEcmpNveRemoteVtepAddressType,
                                                &i4NextFsVxlanEcmpNveRemoteVtepAddressType,
                                                &FsVxlanEcmpNveRemoteVtepAddress,
                                                &NextFsVxlanEcmpNveRemoteVtepAddress);
        i4FsVxlanEcmpNveIfIndex = i4NextFsVxlanEcmpNveIfIndex;
        u4FsVxlanEcmpNveVniNumber = u4NextFsVxlanEcmpNveVniNumber;
        MEMCPY (&u1FsVxlanEcmpNveDestVmMac, &NextFsVxlanEcmpNveDestVmMac, 6);
        i4FsVxlanEcmpNveRemoteVtepAddressType =
            i4NextFsVxlanEcmpNveRemoteVtepAddressType;
        MEMCPY (&FsVxlanEcmpNveRemoteVtepAddress,
                &NextFsVxlanEcmpNveRemoteVtepAddress,
                sizeof (NextFsVxlanEcmpNveRemoteVtepAddress));
    }
    *pi4EcmpNveCount = i4EcmpNveCount;
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanCliShowVrf
 Input       :  CliHandle - Cli handle
 Description :  This Function displays the Ecmp Nve Peers configured.
 Output      :  None
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
VxlanCliShowVrf (tCliHandle CliHandle)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pNextVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    UINT1               au1RDString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT2               u2ASN = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4AsignedNumber = 0;
    UINT4               u4ASN = 0;
    INT4                i4RtType = 0;
    UINT1               au1RTString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT1               u1Flag = 0;

    MEMSET (au1RDString, 0, sizeof (au1RDString));

    CliPrintf (CliHandle, "%s\r\n", "Evpn Vrf Details");
    CliPrintf (CliHandle, "%s\r\n", "----------------");
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
    CliPrintf (CliHandle, "%-20s %4s %9s %5s %8s %13s \r\n", "VRF-Name ",
               "VNI ", "RD ", " ", "RT_TYPE ", "RT ");
    CliPrintf (CliHandle, "%-20s %4s %9s %5s %8s %13s \r\n", "-------- ",
               "--- ", "-- ", " ", "------- ", "-- ");
    while (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        CliPrintf (CliHandle, "%-20s ",
                   EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry));
        CliPrintf (CliHandle, "%4d ",
                   EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry));
        /* RD is of type  ASN:nn */
        if (EVPN_RD_TYPE_0 == EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[0])
        {
            MEMCPY (&u2ASN, &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[2],
                    sizeof (UINT2));
            MEMCPY (&u4AsignedNumber,
                    &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            SPRINTF ((char *) au1RDString, "%d:%u", u2ASN, u4AsignedNumber);
        }
        /* RD is of type  ip:nn */
        else if (EVPN_RD_TYPE_1 == EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[0])
        {
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((char *) au1RDString, "%d.%d.%d.%d:%d",
                     EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[2],
                     EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[3],
                     EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[4],
                     EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[5],
                     u2AsignedNumber);
        }
        /* RD is of type  ASN.ASN:nn */
        else
        {
            MEMCPY (&u4ASN, &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[2],
                    sizeof (UINT4));
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry)[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((char *) au1RDString, "%d.%d:%hu",
                     (UINT4) ((u4ASN & 0xffff0000) >> 16),
                     (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
        }

        if (EVPN_P_VRF_RD_AUTO (pVxlanFsEvpnVxlanVrfEntry) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " %10s ", "auto");
        }
        else
        {
            CliPrintf (CliHandle, " %10s ", au1RDString);
        }
        pVxlanFsEvpnVxlanVrfRTEntry =
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable);
        while (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
        {
            if (u1Flag != 0)
            {
                CliPrintf (CliHandle, "%38s", "");
            }
            i4RtType = EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry);
            if (EVPN_BGP_RT_IMPORT == i4RtType)
            {
                CliPrintf (CliHandle, "  %6s ", "import");
            }
            else if (EVPN_BGP_RT_EXPORT == i4RtType)
            {
                CliPrintf (CliHandle, "  %6s ", "export");
            }
            else
            {
                CliPrintf (CliHandle, "  %6s ", "both");
            }
            CliPrintf (CliHandle, "%6s", " ");
            if (EVPN_P_VRF_RT_AUTO (pVxlanFsEvpnVxlanVrfRTEntry) == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "%9s \r\n", "auto");
            }
            else
            {
                if (EVPN_RT_TYPE_0 ==
                    EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[0])
                {
                    MEMCPY (&u2ASN,
                            &EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[2],
                            sizeof (UINT2));
                    MEMCPY (&u4AsignedNumber,
                            &EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[4],
                            sizeof (UINT4));
                    u2ASN = OSIX_NTOHS (u2ASN);
                    u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
                    SPRINTF ((char *) au1RTString, "%d:%u", u2ASN,
                             u4AsignedNumber);
                }
                /* RT is of type  ip:nn */
                else if (EVPN_RT_TYPE_1 ==
                         EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[0])
                {
                    MEMCPY (&u2AsignedNumber,
                            &EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[6],
                            sizeof (UINT2));
                    u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                    SPRINTF ((char *) au1RTString, "%d.%d.%d.%d:%d",
                             EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[2],
                             EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[3],
                             EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[4],
                             EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[5],
                             u2AsignedNumber);
                }
                /* RT is of type  ASN.ASN:nn */
                else
                {
                    MEMCPY (&u4ASN,
                            &EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[2],
                            sizeof (UINT4));
                    MEMCPY (&u2AsignedNumber,
                            &EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry)[6],
                            sizeof (UINT2));
                    u4ASN = OSIX_NTOHL (u4ASN);
                    u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                }
                CliPrintf (CliHandle, "%9s\r\n", au1RTString);
                u1Flag = 1;
            }
            pVxlanFsEvpnVxlanVrfRTEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                               (tRBElem *) pVxlanFsEvpnVxlanVrfRTEntry, NULL);
        }
        pNextVxlanFsEvpnVxlanVrfEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           (tRBElem *) pVxlanFsEvpnVxlanVrfEntry, NULL);
        pVxlanFsEvpnVxlanVrfEntry = pNextVxlanFsEvpnVxlanVrfEntry;
        u1Flag = 0;
    }
    return CLI_SUCCESS;
}

#endif /* EVPN_VXLAN_WANTED */

INT4
VxlanCliShowPacketHeaders (tCliHandle CliHandle,
                           UINT4 *pSrcMac, UINT4 *pDestMac,
                           UINT4 *pu4VlanID,
                           UINT4 *pu4Vni,
                           UINT1 *pSrcIP,
                           UINT1 *pDestIP, UINT1 *pMcastIP, INT1 *pAddrType)
{
    tMacAddr            au1DestMac;
    tMacAddr            au1SrcMac;
    UINT4               u4SrcIP = 0;
    UINT4               u4DestIP = 0;
    UINT1               u1EthType[2] = { 0x81, 0x00 };
    UINT1               u1VlanType[2] = { 0x88, 0x58 };
    UINT1               u1Data[46] =
        { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
        0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa
    };
    UINT1               au1EthBuf[64];
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize = 0;
    BOOL1               bIsMulticast = FALSE;
#ifdef IP6_WANTED
    tIp6Addr            SrcIP6;
    tIp6Addr            DestIP6;

    MEMSET (&SrcIP6, 0, sizeof (tIp6Addr));
    MEMSET (&DestIP6, 0, sizeof (tIp6Addr));
#endif

    MEMSET (au1DestMac, 0, sizeof (tMacAddr));
    MEMSET (au1SrcMac, 0, sizeof (tMacAddr));
    StrToMac ((UINT1 *) pDestMac, au1DestMac);
    StrToMac ((UINT1 *) pSrcMac, au1SrcMac);

    MEMCPY (&au1EthBuf[0], au1DestMac, 6);
    MEMCPY (&au1EthBuf[0 + 6], au1SrcMac, 6);
    MEMCPY (&au1EthBuf[0 + 6 + 6], u1EthType, 2);    /* till this length is 6+6+2->14 */
    MEMCPY (&au1EthBuf[14], pu4VlanID, 2);
    MEMCPY (&au1EthBuf[16], u1VlanType, 2);
    MEMCPY (&au1EthBuf[18], u1Data, 46);    /* 42+72 -> 114 */

    pBuf = CRU_BUF_Allocate_MsgBufChain (VXLAN_JUMBO_MTU_SIZE, 0);

    if (pBuf == NULL)
    {
        return CLI_SUCCESS;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, au1EthBuf, 0, 64) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Unable to copy read data to Buffer\n"));
        return CLI_FAILURE;
    }
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    VxlanUtilAddHdr (pBuf, *pu4Vni);

    /* Include the VXLAN packet size */
    u4PktSize = u4PktSize + VXLAN_HDR_TOTAL_OFFSET;

    if (*pAddrType == VXLAN_CLI_ADDRTYPE_IPV4)
    {
        MEMCPY (&u4SrcIP, pSrcIP, VXLAN_IP4_ADDR_LEN);
        u4SrcIP = OSIX_HTONL (u4SrcIP);
        if (pDestIP != NULL)
        {
            MEMCPY (&u4DestIP, pDestIP, VXLAN_IP4_ADDR_LEN);
        }
        else
        {
            MEMCPY (&u4DestIP, pMcastIP, VXLAN_IP4_ADDR_LEN);
            bIsMulticast = TRUE;
        }
        u4DestIP = OSIX_HTONL (u4DestIP);
        if (VxlanUdpTransmitVxlanPkt (pBuf, u4SrcIP, u4DestIP,
                                      (UINT2) u4PktSize,
                                      (UINT4) bIsMulticast) == VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "Failed in packet transmission\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            /*return VXLAN_FAILURE; */
        }
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV4 VXLAN packet is successfully"
                         "given to UDP module\n"));

    }
#ifdef IP6_WANTED
    else                        /* IPV6 Address Type */
    {
        MEMCPY (&SrcIP6, pSrcIP, sizeof (SrcIP6));
        if (pDestIP != NULL)
        {
            MEMCPY (&DestIP6, pDestIP, sizeof (DestIP6));
        }
        else
        {
            MEMCPY (&DestIP6, pMcastIP, sizeof (DestIP6));
            bIsMulticast = TRUE;
        }
        if (VxlanUdpv6TransmitVxlanPkt (pBuf, &SrcIP6, &DestIP6,
                                        (UINT2) u4PktSize,
                                        (UINT4) bIsMulticast) == VXLAN_FAILURE)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                             "Failed in packet transmission\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
/*            return VXLAN_FAILURE;*/
        }
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "IPV6 VXLAN packet is successfully"
                         "given to UDP module\n"));

    }
#endif
    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}
