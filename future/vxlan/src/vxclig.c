#ifndef __VXLANCLIG_C__
#define __VXLANCLIG_C__
/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxclig.c,v 1.12 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the Vxlan CLI related routines 
*********************************************************************/

#include "vxinc.h"

/****************************************************************************
 * Function    :  cli_process_Vxlan_cmd
 * Description :  This function is exported to CLI module to handle the
                VXLAN cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Vxlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VXLAN_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
#ifdef EVPN_VXLAN_WANTED
    UINT1              *pu1EvpnBgpVrfContextName = NULL;
    INT4                i4FsEvpnVxlanVrfNameLen = 0;
    UINT4               u4VcId = 0;
#endif
    tVxlanFsVxlanVtepEntry VxlanSetFsVxlanVtepEntry;
    tVxlanIsSetFsVxlanVtepEntry VxlanIsSetFsVxlanVtepEntry;

    tVxlanFsVxlanNveEntry VxlanSetFsVxlanNveEntry;
    tVxlanIsSetFsVxlanNveEntry VxlanIsSetFsVxlanNveEntry;

    tVxlanFsVxlanMCastEntry VxlanSetFsVxlanMCastEntry;
    tVxlanIsSetFsVxlanMCastEntry VxlanIsSetFsVxlanMCastEntry;

    tVxlanFsVxlanVniVlanMapEntry VxlanSetFsVxlanVniVlanMapEntry;
    tVxlanIsSetFsVxlanVniVlanMapEntry VxlanIsSetFsVxlanVniVlanMapEntry;

    tVxlanFsVxlanInReplicaEntry VxlanSetFsVxlanInReplicaEntry;
    tVxlanIsSetFsVxlanInReplicaEntry VxlanIsSetFsVxlanInReplicaEntry;
#ifdef EVPN_VXLAN_WANTED
    tVxlanFsEvpnVxlanEviVniMapEntry VxlanSetFsEvpnVxlanEviVniMapEntry;
    tVxlanIsSetFsEvpnVxlanEviVniMapEntry VxlanIsSetFsEvpnVxlanEviVniMapEntry;

    tVxlanFsEvpnVxlanBgpRTEntry VxlanSetFsEvpnVxlanBgpRTEntry;
    tVxlanIsSetFsEvpnVxlanBgpRTEntry VxlanIsSetFsEvpnVxlanBgpRTEntry;

    tVxlanFsEvpnVxlanVrfEntry VxlanSetFsEvpnVxlanVrfEntry;
    tVxlanIsSetFsEvpnVxlanVrfEntry VxlanIsSetFsEvpnVxlanVrfEntry;

    tVxlanFsEvpnVxlanVrfRTEntry VxlanSetFsEvpnVxlanVrfRTEntry;
    tVxlanIsSetFsEvpnVxlanVrfRTEntry VxlanIsSetFsEvpnVxlanVrfRTEntry;

    tVxlanFsEvpnVxlanMultihomedPeerTable VxlanSetFsEvpnVxlanMultihomedPeerTable;
    tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
        VxlanIsSetFsEvpnVxlanMultihomedPeerTable;
#endif /* EVPN_VXLAN_WANTED */
    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, VxlanMainTaskLock, VxlanMainTaskUnLock);
    VXLAN_LOCK;

    MEMSET (&ap, 0, sizeof (va_list));
    va_start (ap, u4Command);

    va_arg (ap, INT4);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VXLAN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_VXLAN_FSVXLANVTEPTABLE:
            MEMSET (&VxlanSetFsVxlanVtepEntry, 0,
                    sizeof (tVxlanFsVxlanVtepEntry));
            MEMSET (&VxlanIsSetFsVxlanVtepEntry, 0,
                    sizeof (tVxlanIsSetFsVxlanVtepEntry));

            VXLAN_FILL_FSVXLANVTEPTABLE_ARGS ((&VxlanSetFsVxlanVtepEntry),
                                              (&VxlanIsSetFsVxlanVtepEntry),
                                              args[0], args[1], args[2],
                                              args[3], args[4]);

            i4RetStatus =
                VxlanCliSetFsVxlanVtepTable (CliHandle,
                                             (&VxlanSetFsVxlanVtepEntry),
                                             (&VxlanIsSetFsVxlanVtepEntry));
            break;

        case CLI_VXLAN_FSVXLANNVETABLE:
            MEMSET (&VxlanSetFsVxlanNveEntry, 0,
                    sizeof (tVxlanFsVxlanNveEntry));
            MEMSET (&VxlanIsSetFsVxlanNveEntry, 0,
                    sizeof (tVxlanIsSetFsVxlanNveEntry));

            VXLAN_FILL_FSVXLANNVETABLE_ARGS ((&VxlanSetFsVxlanNveEntry),
                                             (&VxlanIsSetFsVxlanNveEntry),
                                             args[0], args[1], args[2], args[3],
                                             args[4], args[5], args[6],
                                             args[7]);

            i4RetStatus =
                VxlanCliSetFsVxlanNveTable (CliHandle,
                                            (&VxlanSetFsVxlanNveEntry),
                                            (&VxlanIsSetFsVxlanNveEntry));
            break;

        case CLI_VXLAN_FSVXLANMCASTTABLE:
            MEMSET (&VxlanSetFsVxlanMCastEntry, 0,
                    sizeof (tVxlanFsVxlanMCastEntry));
            MEMSET (&VxlanIsSetFsVxlanMCastEntry, 0,
                    sizeof (tVxlanIsSetFsVxlanMCastEntry));

            VXLAN_FILL_FSVXLANMCASTTABLE_ARGS ((&VxlanSetFsVxlanMCastEntry),
                                               (&VxlanIsSetFsVxlanMCastEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5]);

            i4RetStatus =
                VxlanCliSetFsVxlanMCastTable (CliHandle,
                                              (&VxlanSetFsVxlanMCastEntry),
                                              (&VxlanIsSetFsVxlanMCastEntry));
            break;

        case CLI_VXLAN_FSVXLANVNIVLANMAPTABLE:
            MEMSET (&VxlanSetFsVxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanFsVxlanVniVlanMapEntry));
            MEMSET (&VxlanIsSetFsVxlanVniVlanMapEntry, 0,
                    sizeof (tVxlanIsSetFsVxlanVniVlanMapEntry));

            VXLAN_FILL_FSVXLANVNIVLANMAPTABLE_ARGS ((&VxlanSetFsVxlanVniVlanMapEntry), (&VxlanIsSetFsVxlanVniVlanMapEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                VxlanCliSetFsVxlanVniVlanMapTable (CliHandle,
                                                   (&VxlanSetFsVxlanVniVlanMapEntry),
                                                   (&VxlanIsSetFsVxlanVniVlanMapEntry));
            break;

        case CLI_VXLAN_FSVXLANINREPLICATABLE:
            MEMSET (&VxlanSetFsVxlanInReplicaEntry, 0,
                    sizeof (tVxlanFsVxlanInReplicaEntry));
            MEMSET (&VxlanIsSetFsVxlanInReplicaEntry, 0,
                    sizeof (tVxlanIsSetFsVxlanInReplicaEntry));

            VXLAN_FILL_FSVXLANINREPLICATABLE_ARGS ((&VxlanSetFsVxlanInReplicaEntry), (&VxlanIsSetFsVxlanInReplicaEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                VxlanCliSetFsVxlanInReplicaTable (CliHandle,
                                                  (&VxlanSetFsVxlanInReplicaEntry),
                                                  (&VxlanIsSetFsVxlanInReplicaEntry));
            break;

        case CLI_VXLAN_DEL_INGRESS_REPLICA:
            i4RetStatus =
                VxlanCliDelIngressReplica (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                           CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_VXLAN_FSVXLANENABLE:

            i4RetStatus = VxlanCliSetFsVxlanEnable (CliHandle, args[0]);
            break;

        case CLI_VXLAN_FSVXLANUDPPORT:

            i4RetStatus = VxlanCliSetFsVxlanUdpPort (CliHandle, args[0]);
            break;

        case CLI_VXLAN_FSVXLANTRACEOPTION:

            i4RetStatus =
                VxlanCliSetFsVxlanTraceOption (CliHandle, args[0],
                                               (INT1) CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_VXLAN_FSVXLANNOTIFICATIONCNTL:

            i4RetStatus =
                VxlanCliSetFsVxlanNotificationCntl (CliHandle, args[0]);
            break;

#ifdef EVPN_VXLAN_WANTED
        case CLI_VXLAN_FSEVPNID:
            /* args[0] = EVID */
            i4RetStatus =
                VxlanCliSetFsEviEntry (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_VXLAN_FSEVPNENABLE:
            /* args[0] = enable/disable */
            i4RetStatus = VxlanCliSetFsEvpnVxlanEnable (CliHandle, args[0]);
            break;

        case CLI_VXLAN_FSEVPNVNIMAPTABLE:
            /* args[0] = Map EVI Idx     */
            /* args[1] = Map VNI Idx     */
            /* args[2] = Map Bgp RD      */
            /* args[3] = Map Bgp RD Len  */
            /* args[4] = Map Bgp ESI     */
            /* args[5] = Map Bgp ESI Len */
            /* args[6] = Load Balance    */
            /* args[7] = Sent Pkts       */
            /* args[8] = Rcvd Pkts       */
            /* args[9] = Dropped Pkts    */
            /* args[10] = RowStatus      */
            /* args[11] = Bgp RDAuto     */
            MEMSET (&VxlanSetFsEvpnVxlanEviVniMapEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
            MEMSET (&VxlanIsSetFsEvpnVxlanEviVniMapEntry, 0,
                    sizeof (tVxlanIsSetFsEvpnVxlanEviVniMapEntry));

            VXLAN_FILL_FSEVPNVXLANEVIVNIMAPTABLE_ARGS ((&VxlanSetFsEvpnVxlanEviVniMapEntry), (&VxlanIsSetFsEvpnVxlanEviVniMapEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11]);
            i4RetStatus =
                VxlanCliSetFsEvpnVxlanEviVniMapTable (CliHandle,
                                                      (&VxlanSetFsEvpnVxlanEviVniMapEntry),
                                                      (&VxlanIsSetFsEvpnVxlanEviVniMapEntry));
            break;

        case CLI_VXLAN_FSEVPNVXLANBGPRTTABLE:
            /* args[0] = Bgp RT Index        */
            /* args[1] = Bgp RT Type         */
            /* args[2] = Bgp RT              */
            /* args[3] = Bgp RT Len          */
            /* args[4] = Bgp RT RowStatus    */
            /* args[5] = BGP RT Auto Flag    */
            /* args[6] = EviVniMap EviIndex  */
            /* args[7] = EviVniMap VniNumber */
            MEMSET (&VxlanSetFsEvpnVxlanBgpRTEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
            MEMSET (&VxlanIsSetFsEvpnVxlanBgpRTEntry, 0,
                    sizeof (tVxlanIsSetFsEvpnVxlanBgpRTEntry));

            VXLAN_FILL_FSEVPNVXLANBGPRTTABLE_ARGS ((&VxlanSetFsEvpnVxlanBgpRTEntry), (&VxlanIsSetFsEvpnVxlanBgpRTEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
            i4RetStatus =
                VxlanCliSetFsEvpnVxlanBgpRTTable (CliHandle,
                                                  (&VxlanSetFsEvpnVxlanBgpRTEntry),
                                                  (&VxlanIsSetFsEvpnVxlanBgpRTEntry));
            break;

        case CLI_VXLAN_FSEVPNVXLANVRF:
            /* args[0] = Vrf Name      */
            pu1EvpnBgpVrfContextName = (UINT1 *) args[0];
            i4RetStatus =
                VxlanCliSetFsVrfEntry (CliHandle, pu1EvpnBgpVrfContextName);
            break;
        case CLI_VXLAN_DEL_FSEVPNVXLANVRF:
            /* args[0] = Vrf Name      */
            pu1EvpnBgpVrfContextName = (UINT1 *) args[0];
            i4RetStatus =
                VxlanCliDelFsVrfEntry (CliHandle, pu1EvpnBgpVrfContextName);
            break;

        case CLI_VXLAN_FSEVPNVXLANVRFTABLE:
            /* args[0] = Vrf Name      */
            /* args[1] = Vni ID             */
            /* args[2] = Vrf RD        */
            /* args[3] = Vrf RD Len    */
            /* args[4] = Vrf RD Auto        */
            /* args[5] = Vrf RowStatus      */
            /* args[6] = Vrf PacksetSent    */
            /* args[7] = VRf PacketReceived */
            /* args[8] = VRf PacketDropped  */
            pu1EvpnBgpVrfContextName = (UINT1 *) args[0];
            i4FsEvpnVxlanVrfNameLen = (INT4) STRLEN (pu1EvpnBgpVrfContextName);

            if (VcmIsVrfExist (pu1EvpnBgpVrfContextName, &u4VcId) == VCM_FALSE)
            {
                CliPrintf (CliHandle, "\r%% VRF %s does not exist in "
                           "VCM\n", pu1EvpnBgpVrfContextName);
                i4RetStatus = CLI_FAILURE;
            }
            else
            {

                MEMSET (&VxlanSetFsEvpnVxlanVrfEntry, 0,
                        sizeof (tVxlanFsEvpnVxlanVrfEntry));
                MEMSET (&VxlanIsSetFsEvpnVxlanVrfEntry, 0,
                        sizeof (tVxlanIsSetFsEvpnVxlanVrfEntry));

                VXLAN_FILL_FSEVPNVXLANVRFTABLE_ARGS ((&VxlanSetFsEvpnVxlanVrfEntry), (&VxlanIsSetFsEvpnVxlanVrfEntry), pu1EvpnBgpVrfContextName, &i4FsEvpnVxlanVrfNameLen, args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
                i4RetStatus =
                    VxlanCliSetFsEvpnVxlanVrfTable (CliHandle,
                                                    (&VxlanSetFsEvpnVxlanVrfEntry),
                                                    (&VxlanIsSetFsEvpnVxlanVrfEntry));
            }

            break;

        case CLI_VXLAN_FSEVPNVXLANVRFRTTABLE:
            /* args[0] = Vrf RT Index     */
            /* args[1] = Vrf RT Type      */
            /* args[2] = Vrf RT           */
            /* args[3] = Vrf RT Len       */
            /* args[4] = Vrf RT RowStatus */
            /* args[5] = Vrf Name         */
            /* args[6] = Vrf Name Len     */
            MEMSET (&VxlanSetFsEvpnVxlanVrfRTEntry, 0,
                    sizeof (tVxlanFsEvpnVxlanVrfRTEntry));
            MEMSET (&VxlanIsSetFsEvpnVxlanVrfRTEntry, 0,
                    sizeof (tVxlanIsSetFsEvpnVxlanVrfRTEntry));

            pu1EvpnBgpVrfContextName = (UINT1 *) args[0];
            i4FsEvpnVxlanVrfNameLen = (INT4) STRLEN (pu1EvpnBgpVrfContextName);

            if (VcmIsVrfExist (pu1EvpnBgpVrfContextName, &u4VcId) == VCM_FALSE)
            {
                CliPrintf (CliHandle, "\r%% VRF %s does not exist in "
                           "VCM\n", pu1EvpnBgpVrfContextName);
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                VXLAN_FILL_FSEVPNVXLANVRFRTTABLE_ARGS ((&VxlanSetFsEvpnVxlanVrfRTEntry), (&VxlanIsSetFsEvpnVxlanVrfRTEntry), pu1EvpnBgpVrfContextName, &i4FsEvpnVxlanVrfNameLen, args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
                i4RetStatus =
                    VxlanCliSetFsEvpnVxlanVrfRTTable (CliHandle,
                                                      (&VxlanSetFsEvpnVxlanVrfRTEntry),
                                                      (&VxlanIsSetFsEvpnVxlanVrfRTEntry));
            }
            break;

        case CLI_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE:
            MEMSET (&VxlanSetFsEvpnVxlanMultihomedPeerTable, 0,
                    sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));
            MEMSET (&VxlanIsSetFsEvpnVxlanMultihomedPeerTable, 0,
                    sizeof (tVxlanIsSetFsEvpnVxlanMultihomedPeerTable));

            VXLAN_FILL_FSEVPNVXLANMULTIHOMEDPEERTABLE_ARGS ((&VxlanSetFsEvpnVxlanMultihomedPeerTable), (&VxlanIsSetFsEvpnVxlanMultihomedPeerTable), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                VxlanCliSetFsEvpnVxlanMultihomedPeerTable (CliHandle,
                                                           (&VxlanSetFsEvpnVxlanMultihomedPeerTable),
                                                           (&VxlanIsSetFsEvpnVxlanMultihomedPeerTable));
            break;

        case CLI_VXLAN_DEL_FSEVPNID:
            /* args[0] = EVID */
            i4RetStatus =
                VxlanCliDelFsEviEntry (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        case CLI_VXLAN_FSEVPNANYCASTGW:
            i4RetStatus = VxlanCliSetFsEvpnAnycastGwMac (CliHandle, args[0]);
            break;
#endif /* EVPN_VXLAN_WANTED */

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_VXLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", VxlanCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    VXLAN_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanVtepTable
* Description :
* Input       :  CliHandle 
*            pVxlanSetFsVxlanVtepEntry
*            pVxlanIsSetFsVxlanVtepEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanVtepTable (tCliHandle CliHandle,
                             tVxlanFsVxlanVtepEntry * pVxlanSetFsVxlanVtepEntry,
                             tVxlanIsSetFsVxlanVtepEntry *
                             pVxlanIsSetFsVxlanVtepEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsVxlanVtepTable (&u4ErrorCode, pVxlanSetFsVxlanVtepEntry,
                                      pVxlanIsSetFsVxlanVtepEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VTEP table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsVxlanVtepTable
        (pVxlanSetFsVxlanVtepEntry, pVxlanIsSetFsVxlanVtepEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanNveTable
* Description :
* Input       :  CliHandle 
*            pVxlanSetFsVxlanNveEntry
*            pVxlanIsSetFsVxlanNveEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanNveTable (tCliHandle CliHandle,
                            tVxlanFsVxlanNveEntry * pVxlanSetFsVxlanNveEntry,
                            tVxlanIsSetFsVxlanNveEntry *
                            pVxlanIsSetFsVxlanNveEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsVxlanNveTable (&u4ErrorCode, pVxlanSetFsVxlanNveEntry,
                                     pVxlanIsSetFsVxlanNveEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set NVE table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsVxlanNveTable
        (pVxlanSetFsVxlanNveEntry, pVxlanIsSetFsVxlanNveEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanMCastTable
* Description :
* Input       :  CliHandle 
*            pVxlanSetFsVxlanMCastEntry
*            pVxlanIsSetFsVxlanMCastEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanMCastTable (tCliHandle CliHandle,
                              tVxlanFsVxlanMCastEntry *
                              pVxlanSetFsVxlanMCastEntry,
                              tVxlanIsSetFsVxlanMCastEntry *
                              pVxlanIsSetFsVxlanMCastEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsVxlanMCastTable (&u4ErrorCode, pVxlanSetFsVxlanMCastEntry,
                                       pVxlanIsSetFsVxlanMCastEntry, OSIX_TRUE,
                                       OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set MCAST table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsVxlanMCastTable
        (pVxlanSetFsVxlanMCastEntry, pVxlanIsSetFsVxlanMCastEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanVniVlanMapTable
* Description :
* Input       :  CliHandle 
*            pVxlanSetFsVxlanVniVlanMapEntry
*            pVxlanIsSetFsVxlanVniVlanMapEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanVniVlanMapTable (tCliHandle CliHandle,
                                   tVxlanFsVxlanVniVlanMapEntry *
                                   pVxlanSetFsVxlanVniVlanMapEntry,
                                   tVxlanIsSetFsVxlanVniVlanMapEntry *
                                   pVxlanIsSetFsVxlanVniVlanMapEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsVxlanVniVlanMapTable
        (&u4ErrorCode, pVxlanSetFsVxlanVniVlanMapEntry,
         pVxlanIsSetFsVxlanVniVlanMapEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VLAN-VNI table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsVxlanVniVlanMapTable
        (pVxlanSetFsVxlanVniVlanMapEntry, pVxlanIsSetFsVxlanVniVlanMapEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanEnable
* Description :
* Input       :  CliHandle 
*            pVxlanSetScalar
*            pVxlanIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanEnable (tCliHandle CliHandle, UINT4 *pFsVxlanEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsVxlanEnable = 0;

    VXLAN_FILL_FSVXLANENABLE (i4FsVxlanEnable, pFsVxlanEnable);

    if (VxlanTestFsVxlanEnable (&u4ErrorCode, i4FsVxlanEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VxlanSetFsVxlanEnable (i4FsVxlanEnable) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VXLAN Overlay Feature\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanUdpPort
* Description :
* Input       :  CliHandle 
*            pVxlanSetScalar
*            pVxlanIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanUdpPort (tCliHandle CliHandle, UINT4 *pFsVxlanUdpPort)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsVxlanUdpPort = 0;

    VXLAN_FILL_FSVXLANUDPPORT (u4FsVxlanUdpPort, pFsVxlanUdpPort);

    if (VxlanTestFsVxlanUdpPort (&u4ErrorCode, u4FsVxlanUdpPort) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VxlanSetFsVxlanUdpPort (u4FsVxlanUdpPort) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VXLAN Udp-port\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanTraceOption
* Description :
* Input       :  CliHandle 
*            pVxlanSetScalar
*            pVxlanIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanTraceOption (tCliHandle CliHandle, UINT4 *pFsVxlanTraceOption,
                               INT1 u1set)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsVxlanTraceOption = 0;

    VXLAN_FILL_FSVXLANTRACEOPTION (u4FsVxlanTraceOption, pFsVxlanTraceOption);

    if (VxlanTestFsVxlanTraceOption (&u4ErrorCode, u4FsVxlanTraceOption) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VxlanSetFsVxlanTraceOption (u4FsVxlanTraceOption, u1set) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VXLAN debug Flags\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsVxlanNotificationCntl
* Description :
* Input       :  CliHandle 
*            pVxlanSetScalar
*            pVxlanIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanNotificationCntl (tCliHandle CliHandle,
                                    UINT4 *pFsVxlanNotificationCntl)
{
    UINT4               u4ErrorCode;
    INT4                i4FsVxlanNotificationCntl = 0;

    VXLAN_FILL_FSVXLANNOTIFICATIONCNTL (i4FsVxlanNotificationCntl,
                                        pFsVxlanNotificationCntl);

    if (VxlanTestFsVxlanNotificationCntl
        (&u4ErrorCode, i4FsVxlanNotificationCntl) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VxlanSetFsVxlanNotificationCntl (i4FsVxlanNotificationCntl) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VXLAN Notification\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************                                                                                * Function    :  VxlanCliSetFsVxlanInReplicaTable                                                                                                            * Description :                                                                                                                                              * Input       :  CliHandle
*            pVxlanSetFsVxlanInReplicaEntry
*            pVxlanIsSetFsVxlanInReplicaEntry                                                                                                                * Output      :  None                                                                                                                                        * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsVxlanInReplicaTable (tCliHandle CliHandle,
                                  tVxlanFsVxlanInReplicaEntry *
                                  pVxlanSetFsVxlanInReplicaEntry,
                                  tVxlanIsSetFsVxlanInReplicaEntry *
                                  pVxlanIsSetFsVxlanInReplicaEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsVxlanInReplicaTable
        (&u4ErrorCode, pVxlanSetFsVxlanInReplicaEntry,
         pVxlanIsSetFsVxlanInReplicaEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Ingress Replica table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsVxlanInReplicaTable
        (pVxlanSetFsVxlanInReplicaEntry, pVxlanIsSetFsVxlanInReplicaEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : VxlanCliFindNextFreeReplicaIndex                   */
/* DESCRIPTION      : This function takes Vni Number as input and        */
/*                    generate valid Index value                         */
/* INPUT            : u4FsVxlanNveVniNumber -Given Vni number            */
/* OUTPUT           : pu4FreeIndex- Generated Index Value                */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
VxlanCliFindNextFreeReplicaIndex (UINT4 u4FsVxlanNveVniNumber,
                                  UINT4 *pu4FreeIndex)
{
    if (VXLAN_FAILURE == VxlanUtilFindNextFreeReplicaIndex
        (u4FsVxlanNveVniNumber, pu4FreeIndex))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : VxlanCliGetReplicaIndex                            */
/* DESCRIPTION      : This function takes Nve Idx, VNI Idx               */
/*                    as input and return Replica Table                  */
/* INPUT            : UINT4 u4FsVxlanNveIfIndex                          */
/*                    UINT4 u4FsVxlanNveVniNumber                        */
/*                    UINT4 *pu4FsVxlanRemoteVtepAddressLen              */
/*                    UINT1 au1FsVxlanRemoteVtepAddress                  */
/* OUTPUT           : pi4VxlanReplicaIndex                               */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
VxlanCliGetReplicaIndex (INT4 i4FsVxlanNveIfIndex,
                         UINT4 u4FsVxlanNveVniNumber,
                         UINT4 *pu4FsVxlanRemoteVtepAddressLen,
                         UINT1 *pu1FsVxlanRemoteVtepAddress,
                         INT4 *pi4FsVxlanReplicaIndex)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanReplicaEntry = NULL;

    pVxlanReplicaEntry =
        VxlanGetReplicaEntry (i4FsVxlanNveIfIndex, u4FsVxlanNveVniNumber,
                              pu4FsVxlanRemoteVtepAddressLen,
                              pu1FsVxlanRemoteVtepAddress);
    if (pVxlanReplicaEntry == NULL)
    {
        return CLI_FAILURE;
    }
    *pi4FsVxlanReplicaIndex = pVxlanReplicaEntry->
        MibObject.i4FsVxlanInReplicaIndex;

    return CLI_SUCCESS;
}

/****************************************************************************
 *Function    :  VxlanCliDelIngressReplica
 *Description :
 *Input       :  i4FsVxlanNveIfIndex
 *               u4FsVxlanNveVniNumber 
 *               
 *Output      :  None
 *Returns     :  CLI_SUCCESS/CLI_FAILURE
 ** ***************************************************************************/
INT4
VxlanCliDelIngressReplica (tCliHandle CliHandle,
                           UINT4 u4FsVxlanNveIfIndex,
                           UINT4 u4FsVxlanNveVniNumber)
{
    UNUSED_PARAM (CliHandle);
    VxlanDelIngressReplica (u4FsVxlanNveIfIndex, u4FsVxlanNveVniNumber);
    return CLI_SUCCESS;

}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanBgpRTTable
* Description :
* Input       :  CliHandle 
*                pVxlanSetFsEvpnVxlanBgpRTEntry
*                pVxlanIsSetFsEvpnVxlanBgpRTEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanBgpRTTable (tCliHandle CliHandle,
                                  tVxlanFsEvpnVxlanBgpRTEntry *
                                  pVxlanSetFsEvpnVxlanBgpRTEntry,
                                  tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                  pVxlanIsSetFsEvpnVxlanBgpRTEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsEvpnVxlanBgpRTTable
        (&u4ErrorCode, pVxlanSetFsEvpnVxlanBgpRTEntry,
         pVxlanIsSetFsEvpnVxlanBgpRTEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to set BGP RT table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsEvpnVxlanBgpRTTable
        (pVxlanSetFsEvpnVxlanBgpRTEntry, pVxlanIsSetFsEvpnVxlanBgpRTEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanMultihomedPeerTable
* Description :
* Input       :  CliHandle 
*            pVxlanSetFsEvpnVxlanMultihomedPeerTable
*            pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanMultihomedPeerTable (tCliHandle CliHandle,
                                           tVxlanFsEvpnVxlanMultihomedPeerTable
                                           *
                                           pVxlanSetFsEvpnVxlanMultihomedPeerTable,
                                           tVxlanIsSetFsEvpnVxlanMultihomedPeerTable
                                           *
                                           pVxlanIsSetFsEvpnVxlanMultihomedPeerTable)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsEvpnVxlanMultihomedPeerTable
        (&u4ErrorCode, pVxlanSetFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsEvpnVxlanMultihomedPeerTable
        (pVxlanSetFsEvpnVxlanMultihomedPeerTable,
         pVxlanIsSetFsEvpnVxlanMultihomedPeerTable, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanEnable
* Description :
* Input       :  CliHandle 
*                pVxlanSetScalar
*                pVxlanIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanEnable (tCliHandle CliHandle, UINT4 *pFsEvpnVxlanEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsEvpnVxlanEnable = 0;

    VXLAN_FILL_FSEVPNVXLANENABLE (i4FsEvpnVxlanEnable, pFsEvpnVxlanEnable);

    if (VxlanTestFsEvpnVxlanEnable (&u4ErrorCode, i4FsEvpnVxlanEnable) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to change EVPN Status\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetFsEvpnVxlanEnable (i4FsEvpnVxlanEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEviEntry 
* Description :
* Input       :  CliHandle 
*                u4FsEvpnIdx - EVID
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEviEntry (tCliHandle CliHandle, UINT4 u4FsEvpnIdx)
{
    UINT1               au1EvpnMap[VXLAN_CLI_MAX_PROMPT_LENGTH];
    INT4                i4FsEvpnEnable = 0;

    MEMSET (au1EvpnMap, 0, VXLAN_CLI_MAX_PROMPT_LENGTH);

    /* EVPN feature must be enabled, to test Encapsulation mode */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnEnable);
    if (i4FsEvpnEnable != EVPN_ENABLED)
    {
        CLI_SET_ERR (CLI_EVPN_NOT_ENABLED);
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return CLI_FAILURE;
    }

    /* Go to the EVPN Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1EvpnMap, "%s%u", VXLAN_CLI_EVPN_MODE, u4FsEvpnIdx);
    if (CliChangePath ((CHR1 *) au1EvpnMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Evpn mode.\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliDelFsEviEntry 
* Description :
* Input       :  CliHandle 
*                *pu4FsEvpnIdx - EVID
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliDelFsEviEntry (tCliHandle CliHandle, INT4 i4FsEvpnIdx)
{
    UNUSED_PARAM (CliHandle);

    /* FsEvpnVxlanBgpRT Table */
    VxlanDelBgpRTEntry (i4FsEvpnIdx, 0);
    /* FsEvpnVxlanEviVniMap Table */
    VxlanDelEviVniMapEntry (i4FsEvpnIdx);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VxlanEvpnVniMapPrompt                                */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Evpn Table                                           */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
VxlanEvpnVniMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx = 0;
    UINT4               u4Len = STRLEN (VXLAN_CLI_EVPN_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, VXLAN_CLI_EVPN_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = (UINT4) CLI_ATOI (pi1ModeName);
    CLI_SET_EVI_ID (u4Idx);
    STRNCPY (pi1PromptStr, VXLAN_CLI_EVPN_MODE, STRLEN (VXLAN_CLI_EVPN_MODE));
    pi1PromptStr[STRLEN (VXLAN_CLI_EVPN_MODE)] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : VxlanEvpnL2VniPrompt                                 */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Evpn Vni Table                                       */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
VxlanEvpnL2VniPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Len = 0;
    INT4                i4VniId = 0;

    u4Len = STRLEN (VXLAN_CLI_L2VNI_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    /*  The first part if mode name contains the display string and
     *  second part contains the profile id followed by address tye
     */
    if (STRNCMP (pi1ModeName, VXLAN_CLI_L2VNI_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    SSCANF ((CHR1 *) pi1ModeName, "%d", (UINT4 *) &i4VniId);

    CLI_SET_MAP_EVI_VNI_ID (i4VniId);

    STRNCPY (pi1PromptStr, VXLAN_CLI_L2VNI_MODE, STRLEN (VXLAN_CLI_L2VNI_MODE));
    pi1PromptStr[STRLEN (VXLAN_CLI_L2VNI_MODE)] = '\0';

    return TRUE;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanEviVniMapTable
* Description :
* Input       :  CliHandle 
*                pVxlanSetFsEvpnVxlanEviVniMapEntry
*                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanEviVniMapTable (tCliHandle CliHandle,
                                      tVxlanFsEvpnVxlanEviVniMapEntry *
                                      pVxlanSetFsEvpnVxlanEviVniMapEntry,
                                      tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                      pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsEvpnVxlanEviVniMapTable
        (&u4ErrorCode, pVxlanSetFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* Change the path only if VNI MAP failed */
        if ((pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
             bFsEvpnVxlanEviVniMapBgpRD != OSIX_TRUE) &&
            (pVxlanSetFsEvpnVxlanEviVniMapEntry->MibObject.
             i4FsEvpnVxlanEviVniMapRowStatus != DESTROY) &&
            (pVxlanIsSetFsEvpnVxlanEviVniMapEntry->
             bFsEvpnVxlanEviVniESI != OSIX_TRUE))
        {
            CliChangePath ("..");
        }

        CliPrintf (CliHandle, "\r\n%% Unable to set/delete EVPN-VNI table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsEvpnVxlanEviVniMapTable
        (pVxlanSetFsEvpnVxlanEviVniMapEntry,
         pVxlanIsSetFsEvpnVxlanEviVniMapEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanVrfTable
* Description :
* Input       :  CliHandle 
*                pVxlanSetFsEvpnVxlanVrfEntry
*                pVxlanIsSetFsEvpnVxlanVrfEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanVrfTable (tCliHandle CliHandle,
                                tVxlanFsEvpnVxlanVrfEntry *
                                pVxlanSetFsEvpnVxlanVrfEntry,
                                tVxlanIsSetFsEvpnVxlanVrfEntry *
                                pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsEvpnVxlanVrfTable
        (&u4ErrorCode, pVxlanSetFsEvpnVxlanVrfEntry,
         pVxlanIsSetFsEvpnVxlanVrfEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* Change the path only if VNI MAP failed */
        if ((pVxlanIsSetFsEvpnVxlanVrfEntry->
             bFsEvpnVxlanVrfRD != OSIX_TRUE) &&
            (pVxlanSetFsEvpnVxlanVrfEntry->MibObject.
             i4FsEvpnVxlanVrfRowStatus != DESTROY) &&
            (pVxlanIsSetFsEvpnVxlanVrfEntry->
             bFsEvpnVxlanVrfRDAuto != OSIX_TRUE))
        {
            CliChangePath ("..");
        }

        CliPrintf (CliHandle, "\r\n%% Unable to set VRF Table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsEvpnVxlanVrfTable
        (pVxlanSetFsEvpnVxlanVrfEntry, pVxlanIsSetFsEvpnVxlanVrfEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnVxlanVrfRTTable
* Description :
* Input       :  CliHandle 
*                pVxlanSetFsEvpnVxlanVrfRTEntry
*                pVxlanIsSetFsEvpnVxlanVrfRTEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnVxlanVrfRTTable (tCliHandle CliHandle,
                                  tVxlanFsEvpnVxlanVrfRTEntry *
                                  pVxlanSetFsEvpnVxlanVrfRTEntry,
                                  tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                  pVxlanIsSetFsEvpnVxlanVrfRTEntry)
{
    UINT4               u4ErrorCode;

    if (VxlanTestAllFsEvpnVxlanVrfRTTable
        (&u4ErrorCode, pVxlanSetFsEvpnVxlanVrfRTEntry,
         pVxlanIsSetFsEvpnVxlanVrfRTEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set VRF RT Table\r\n");
        return CLI_FAILURE;
    }

    if (VxlanSetAllFsEvpnVxlanVrfRTTable
        (pVxlanSetFsEvpnVxlanVrfRTEntry, pVxlanIsSetFsEvpnVxlanVrfRTEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliParseAndGenerateRdRt                        */
/* DESCRIPTION      : This function takes random string as input and     */
/*                    generate valid RD/RT Value                         */
/* INPUT            : pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt)
{
    if (VXLAN_SUCCESS == EvpnUtilParseAndGenerateRdRt (pu1RandomString,
                                                       pu1RdRt))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliParseAndGenerateESI                         */
/* DESCRIPTION      : This function takes random string as input and     */
/*                    generate valid ESI Value                           */
/* INPUT            : pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/* OUTPUT           : pu1ESIVal - Generated ESI Value                    */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliParseAndGenerateESI (UINT1 *pu1RandomString, UINT1 *pu1ESIVal)
{
    if (VXLAN_SUCCESS == EvpnUtilParseAndGenerateESI (pu1RandomString,
                                                      pu1ESIVal))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnBgpCliGetDefaultRdValue                        */
/* DESCRIPTION      : This function takes EVI Idx, VNI Idx,as input and  */
/*                    generate valid RD Value                            */
/* INPUT            : u4EviIndex - EVI Index                             */
/*                    u4VniIndex - VNI Index                             */
/* OUTPUT           : pu1RouteDistinguisher - Generated Rd Value         */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnBgpCliGetDefaultRdValue (UINT4 u4EviIndex, UINT4 u4VniIndex,
                             UINT1 *pu1RouteDistinguisher)
{
    if (VXLAN_SUCCESS == EvpnBgpUtlGetDefaultRdValue ((INT4) u4EviIndex,
                                                      u4VniIndex,
                                                      pu1RouteDistinguisher))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnBgpCliGetESIValue                              */
/* DESCRIPTION      : This function takes EVI Idx, VNI Idx,as input and  */
/*                    generate valid ESI Value                           */
/* INPUT            : u4EviIndex - EVI Index                             */
/*                    u4VniIndex - VNI Index                             */
/* OUTPUT           : pu1ESIValue - Generated ESI Value                  */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnBgpCliGetESIValue (UINT4 u4EviIndex, UINT4 u4VniIndex, UINT1 *pu1ESIValue)
{
    if (VXLAN_SUCCESS == EvpnBgpUtlGenerateESIValue ((INT4) u4EviIndex,
                                                     u4VniIndex, pu1ESIValue))
    {
        return CLI_SUCCESS;
    }

    CLI_SET_ERR (CLI_EVPN_MCLAG_NOT_PRESENT);
    VXLAN_TRC ((VXLAN_EVPN_TRC, "MC-LAG identifier is not present\n"));
    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnBgpCliGetDefaultRtValue                        */
/* DESCRIPTION      : This function takes EVI Idx, VNI Idx, RT Idx,      */
/*                    RT Type, as input and generate valid RT Value      */
/* INPUT            : u4EviIndex - EVI Index                             */
/*                    u4VniIndex - VNI Index                             */
/*                    u4RtIndex - RT Index                               */
/*                    i4RtType - RT Index                                */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnBgpCliGetDefaultRtValue (UINT4 u4EviIndex, UINT4 u4VniIndex,
                             UINT4 u4RtIndex, INT4 i4RtType,
                             UINT1 *pu1RouteTarget)
{
    if (VXLAN_SUCCESS == EvpnBgpUtlGetDefaultRtValue ((INT4) u4EviIndex,
                                                      u4VniIndex, u4RtIndex,
                                                      i4RtType, pu1RouteTarget))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliValidateNveTable                            */
/* DESCRIPTION      : This function takes NVE Idx, VNI Idx as Input      */
/*                    and validate the NVE Table                         */
/* INPUT            : u4NveIndex - NVE Index                             */
/*                    u4VniIndex - VNI Index                             */
/* OUTPUT           : None                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliValidateNveTable (UINT4 u4NveIndex, UINT4 u4VniIndex)
{
    if (VXLAN_SUCCESS == EvpnUtilValidateNveTable ((INT4) u4NveIndex,
                                                   u4VniIndex))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetRtTypeRtIndex                            */
/* DESCRIPTION      : This function takes EVI Idx, VNI Idx, RT Value,    */
/*                    as input and return RT Index and RT Type           */
/* INPUT            : i4EviIndex - EVI Index                             */
/*                    u4VniIndex - VNI Index                             */
/*                    pu1RtValue - RT Value                              */
/* OUTPUT           : u4RtIndex - RT Index                               */
/*                    i4RtType - RT Type                                 */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetRtTypeRtIndex (INT4 i4EviIndex, UINT4 u4VniIndex,
                         UINT1 *pu1RtValue, UINT4 *pu4RtIndex, INT4 *pi4RtType)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pRtEntry = NULL;

    pRtEntry = EvpnGetRTEntryFromValue (i4EviIndex, u4VniIndex, pu1RtValue);
    if (pRtEntry == NULL)
    {
        return CLI_FAILURE;
    }
    *pu4RtIndex = EVPN_P_BGP_RT_INDEX (pRtEntry);
    *pi4RtType = EVPN_P_BGP_RT_TYPE (pRtEntry);

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetRtTableFromEviVni                        */
/* DESCRIPTION      : This function takes EVI Idx, VNI Idx               */
/*                    as input and return RT Table                      */
/* INPUT            : i4EviIndex - EVI Index                             */
/*                    u4VniIndex - VNI Index                             */
/* OUTPUT           : u4RtIndex - RT Index                               */
/*                    i4RtType - RT Type                                 */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetRtTableFromEviVni (tCliHandle CliHandle, INT4 i4EviIndex,
                             UINT4 u4VniIndex, UINT4 *pu4RtIndex,
                             INT4 *pi4RtType)
{
    tVxlanFsEvpnVxlanBgpRTEntry *pRtEntry = NULL;

    pRtEntry = EvpnGetRTTableFromEviVni (i4EviIndex, u4VniIndex);
    if (pRtEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Auto discover does not configured\r\n");
        return CLI_FAILURE;
    }
    *pu4RtIndex = EVPN_P_BGP_RT_INDEX (pRtEntry);
    *pi4RtType = EVPN_P_BGP_RT_TYPE (pRtEntry);

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetRdFromVniMapTable                        */
/* DESCRIPTION      : This function takes EVI ,VNI ,RD  as input and     */
/*                    checks RD Value in already configured.             */
/* INPUT            : pu4VniIdx,pu4EviIdx,pu1RdRt,pi4BgpRdAuto           */
/* OUTPUT           : None                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetRdFromVniMapTable (UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                             UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto)
{
    if (VXLAN_FAILURE == EvpnUtilGetRdFromVniMapTable (*pu4EviIdx, *pu4VniIdx,
                                                       pu1RdRt, pi4BgpRdAuto))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliCheckRdInVniMapTable                        */
/* DESCRIPTION      : This function takes EVI ,VNI ,RD  as input and     */
/*                    checks RD Value in already configured.             */
/* INPUT            : pu4VniIdx,pu4EviIdx,pu1RdRt,pi4BgpRdAuto           */
/* OUTPUT           : None                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliCheckRdInVniMapTable (UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                             UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto)
{
    if (VXLAN_FAILURE == EvpnUtilCheckRdVniMapTable (*pu4EviIdx, *pu4VniIdx,
                                                     pu1RdRt, pi4BgpRdAuto))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliCheckRdIniVrfVniMapTable                    */
/* DESCRIPTION      : This function takes EVI ,VNI ,RD  as input and     */
/*                    checks RD Value in already configured.             */
/* INPUT            : pu4VniIdx,pu4EviIdx,pu1RdRt,pi4BgpRdAuto           */
/* OUTPUT           : None                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliCheckRdIniVrfVniMapTable (UINT1 *pu1VrfName, UINT4 *pu4VniIdx,
                                 UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto)
{
    if (VXLAN_FAILURE == EvpnUtilCheckRdVrfVniMapTable (pu1VrfName, *pu4VniIdx,
                                                        pu1RdRt, pi4BgpRdAuto))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetESIFromVniMapTable                       */
/* DESCRIPTION      : This function takes pu4EviIdx and pu4VniIdx  as    */
/*                    input and returns valid pu1ESIValue Value          */
/* INPUT            : pu4EviIdx - EVI Index                              */
/*                    pu4VniIdx - VNI Index                              */
/* OUTPUT           : pu1ESIValue - Valid ESI Value                      */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetESIFromVniMapTable (UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                              UINT1 *pu1ESIValue)
{
    if (VXLAN_FAILURE == EvpnUtilGetESIFromVniMapTable (*pu4EviIdx,
                                                        *pu4VniIdx,
                                                        pu1ESIValue))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetEviFromVniMapTable                       */
/* DESCRIPTION      : This function takes random string as input and     */
/*                    generate valid RD/RT Value                         */
/* INPUT            : pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetEviFromVniMapTable (UINT4 *pu4VniIdx, INT4 *pi4EviIdx)
{
    if (VXLAN_FAILURE == (EvpnUtilGetEviFromVni (*pu4VniIdx, pi4EviIdx)))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliFindNextFreeRtIndex                         */
/* DESCRIPTION      : This function takes random string as input and     */
/*                    generate valid RD/RT Value                         */
/* INPUT            : pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliFindNextFreeRtIndex (UINT4 u4ContextId, UINT4 *pu4FreeIndex,
                            INT4 i4RTType)
{
    if (VXLAN_FAILURE == EvpnUtilFindNextFreeRtIndex (u4ContextId,
                                                      pu4FreeIndex, i4RTType))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnBgpCliVrfChangeMode                            */
/* DESCRIPTION      : This function handle to change the mode            */
/* INPUT            : u4VcId                                             */
/* OUTPUT           : NONE                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnBgpCliVrfChangeMode (UINT4 u4VcId)
{
    UINT1               au1VrfMap[VXLAN_CLI_MAX_PROMPT_LENGTH];

    MEMSET (au1VrfMap, 0, VXLAN_CLI_MAX_PROMPT_LENGTH);

#ifdef CLI_WANTED
    /* Go to the EVPN Mode of Cli Prompt */
    SNPRINTF ((CHR1 *) au1VrfMap, VXLAN_CLI_MAX_PROMPT_LENGTH, "%s%d",
              VXLAN_CLI_VRF_MODE, u4VcId);
    if (CliChangePath ((CHR1 *) au1VrfMap) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
#endif
    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnBgpCliVrfVniChangeMode                         */
/* DESCRIPTION      : This function handle to change the mode            */
/* INPUT            : u4VcId                                             */
/* OUTPUT           : NONE                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnBgpCliVrfVniChangeMode (UINT4 u4VniIdx)
{
    UINT1               au1VrfMap[VXLAN_CLI_MAX_PROMPT_LENGTH];

    MEMSET (au1VrfMap, 0, VXLAN_CLI_MAX_PROMPT_LENGTH);

    /* Go to the EVPN Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1VrfMap, "%s%u", VXLAN_CLI_VRF_VNI_MODE, u4VniIdx);
    if (CliChangePath ((CHR1 *) au1VrfMap) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetCurPrompt                                */
/* DESCRIPTION      : API provided to get the current CLI prompt         */
/* INPUT            : u4VcId                                             */
/* OUTPUT           : NONE                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetCurPrompt (UINT1 *pu1Prompt)
{
    INT1                ai1CurPrompt[MAX_PROMPT_LEN] = { 0 };
    INT1               *pi1Pos = NULL;

    if (CliGetCurPrompt (ai1CurPrompt) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pi1Pos = (INT1 *) STRRCHR ((CHR1 *) ai1CurPrompt, '/');
    if (pi1Pos != NULL)
    {
        pi1Pos = (INT1 *) STRRCHR ((CHR1 *) ai1CurPrompt, '-');
        if (pi1Pos != NULL)
        {
            STRCPY (pu1Prompt, pi1Pos + 1);
        }
    }
    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliEviVniChangeMode                            */
/* DESCRIPTION      : This function handle to change the mode            */
/* INPUT            : u4VniIdx                                           */
/* OUTPUT           : NONE                                               */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliEviVniChangeMode (UINT4 u4VniIdx)
{
    UINT1               au1VniMap[VXLAN_CLI_MAX_PROMPT_LENGTH];

    MEMSET (au1VniMap, 0, VXLAN_CLI_MAX_PROMPT_LENGTH);

    /* Go to the EVPN VNI Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1VniMap, "%s%u", VXLAN_CLI_L2VNI_MODE, u4VniIdx);
    if (CliChangePath ((CHR1 *) au1VniMap) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VxlanEvpnVrfVniPrompt                                */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Evpn Vni Table                                       */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
VxlanEvpnVrfVniPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    INT4                i4Idx = 0;
    UINT4               u4Len = STRLEN (VXLAN_CLI_VRF_VNI_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, VXLAN_CLI_VRF_VNI_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    i4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_VRF_VNI_ID (i4Idx);
    STRNCPY (pi1PromptStr, VXLAN_CLI_VRF_VNI_MODE,
             STRLEN (VXLAN_CLI_VRF_VNI_MODE));
    pi1PromptStr[STRLEN (VXLAN_CLI_VRF_VNI_MODE)] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : VxlanEvpnVrfPrompt                                   */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Evpn Vrf Table                                       */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
VxlanEvpnVrfPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    INT4                i4Idx = 0;
    UINT4               u4Len = STRLEN (VXLAN_CLI_VRF_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, VXLAN_CLI_VRF_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    i4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_VRF_ID ((UINT4) i4Idx);
    STRNCPY (pi1PromptStr, VXLAN_CLI_VRF_MODE, STRLEN (VXLAN_CLI_VRF_MODE));
    pi1PromptStr[STRLEN (VXLAN_CLI_VRF_MODE)] = '\0';

    return TRUE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetRdFromVrfTable                           */
/* DESCRIPTION      : This function takes random string as input and     */
/*                    generate valid RD/RT Value                         */
/* INPUT            : pu1VxlanVrfName - Vrf Name                         */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetRdFromVrfTable (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex,
                          UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto)
{

    if (VXLAN_FAILURE == EvpnUtilGetRdFromVrfTable (pu1VxlanVrfName, u4VniIndex,
                                                    pu1RdRt, pi4BgpRdAuto))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetVrfRtTableFromVrf                        */
/* DESCRIPTION      : This function takes Vrf Name                       */
/*                    as input and return VrfRT Table                    */
/* INPUT            : pu1VxlanVrfName - Vrf Name                         */
/* OUTPUT           : u4RtIndex - RT Index                               */
/*                    i4RtType - RT Type                                 */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetVrfRtTableFromVrf (tCliHandle CliHandle, UINT1 *pu1VxlanVrfName,
                             UINT4 u4VniIndex, UINT4 *pu4RtIndex,
                             INT4 *pi4RtType)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVrfRtEntry = NULL;

    pVrfRtEntry = EvpnGetVrfRTTableFromVrf (pu1VxlanVrfName, u4VniIndex);
    if (pVrfRtEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Route-Target not configured\r\n");
        return CLI_FAILURE;
    }
    *pu4RtIndex = pVrfRtEntry->MibObject.u4FsEvpnVxlanVrfRTIndex;
    *pi4RtType = pVrfRtEntry->MibObject.i4FsEvpnVxlanVrfRTType;

    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetVrfRtTypeRtIndex                         */
/* DESCRIPTION      : This function takes Vrf-Name, RT Value,            */
/*                    as input and return RT Index and RT Type           */
/* INPUT            : pu1VxlanVrfName - Vrf Name                         */
/*                    pu1RtValue - RT Value                              */
/* OUTPUT           : u4RtIndex - RT Index                               */
/*                    i4RtType - RT Type                                 */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetVrfRtTypeRtIndex (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex,
                            UINT1 *pu1RtValue, UINT4 *pu4RtIndex,
                            INT4 *pi4RtType)
{
    tVxlanFsEvpnVxlanVrfRTEntry *pVrfRtEntry = NULL;

    pVrfRtEntry = EvpnGetVrfRTEntryFromValue (pu1VxlanVrfName, pu1RtValue);
    if (pVrfRtEntry == NULL)
    {
        return CLI_FAILURE;
    }
    *pu4RtIndex = pVrfRtEntry->MibObject.u4FsEvpnVxlanVrfRTIndex;
    *pi4RtType = pVrfRtEntry->MibObject.i4FsEvpnVxlanVrfRTType;

    UNUSED_PARAM (u4VniIndex);
    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnVrfCliGetDefaultRdValue                        */
/* DESCRIPTION      : This function takes Vrf name,as input and          */
/*                    generate valid RD Value                            */
/* INPUT            : pu1VxlanVrfName - Vrf Name                         */
/* OUTPUT           : pu1RouteDistinguisher - Generated Rd Value         */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnVrfCliGetDefaultRdValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT1 *pu1RouteDistinguisher)
{
    if (VXLAN_SUCCESS == EvpnVrfUtlGetDefaultRdValue (pu1VxlanVrfName, u4VniId,
                                                      pu1RouteDistinguisher))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnVrfCliGetDefaultRtValue                        */
/* DESCRIPTION      : This function takes Vrf name, RT Idx,              */
/*                    RT Type, as input and generate valid RT Value      */
/* INPUT            : pu1VxlanVrfName - Vrf Name                         */
/*                    u4RtIndex - RT Index                               */
/*                    i4RtType - RT Index                                */
/* OUTPUT           : pu1RdRt - Generated Rd Value                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnVrfCliGetDefaultRtValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT4 u4RtIndex, INT4 i4RtType,
                             UINT1 *pu1RouteTarget)
{
    if (VXLAN_SUCCESS ==
        EvpnVrfUtlGetDefaultRtValue (pu1VxlanVrfName, u4VniId, u4RtIndex,
                                     i4RtType, pu1RouteTarget))
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/****************************************************************************
 * Function    :  VxlanCliSetFsVrfEntry
 * Description :
 * Input       :  CliHandle
 *                u4FsEvpnIdx - EVID
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
VxlanCliSetFsVrfEntry (tCliHandle CliHandle, UINT1 *pu1EvpnVrfName)
{
    INT4                i4FsEvpnEnable = 0;
    UINT4               u4VcId;
    /* EVPN feature must be enabled, to test Encapsulation mode */
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnEnable);
    if (i4FsEvpnEnable != EVPN_ENABLED)
    {
        CLI_SET_ERR (CLI_EVPN_NOT_ENABLED);
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return CLI_FAILURE;
    }

    if (VcmIsVrfExist (pu1EvpnVrfName, &u4VcId) == VCM_FALSE)
    {
        CliPrintf (CliHandle, "\r%% VRF %s does not exist in "
                   "VCM\n", pu1EvpnVrfName);
        return (CLI_FAILURE);
    }
    else
    {
        if (EvpnBgpCliVrfChangeMode (u4VcId) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to Change mode\n");
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliDelFsVrfEntry
* Description :
* Input       :  CliHandle
*                *pu4FsEvpnIdx - EVID
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliDelFsVrfEntry (tCliHandle CliHandle, UINT1 *pu1EvpnVrfName)
{
    UNUSED_PARAM (CliHandle);
    /* FsEvpnVxlanBgpRT Table */
    VxlanDelVrfRTEntry (pu1EvpnVrfName, 0);
    /* FsEvpnVxlanEviVniMap Table */
    VxlanDelVrfEntry (pu1EvpnVrfName);
    /* Deinit the RT Index */
    EvpnVxlanInitRTIndexMgr ();

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  VxlanCliSetFsEvpnAnycastGwMac
* Description :                                                                                                               * Input       :  CliHandle
*                pFsEvpnVxlanEnable 
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanCliSetFsEvpnAnycastGwMac (tCliHandle CliHandle,
                               UINT4 *pu4FsEvpnAnycastGwMac)
{
    UINT4               u4ErrorCode;
    tMacAddr            MacAddr;

    MEMSET (MacAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);
    if (pu4FsEvpnAnycastGwMac != NULL)
    {
        VXLAN_FILL_FSEVPNANYCASTGWMAC (MacAddr, pu4FsEvpnAnycastGwMac);
        if (VxlanTestFsEvpnAnycastGwMac (&u4ErrorCode, MacAddr) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to change EVPN Status\r\n");
            return CLI_FAILURE;
        }
    }
    if (VxlanSetFsEvpnAnycastGwMac (MacAddr) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* ********************************************************************* */
/* FUNCTION NAME    : EvpnCliGetVrfFromVniMapTable                       */
/* DESCRIPTION      : This function takes VniNumber as a input and       */
/*                    returns vrf name                                   */
/* INPUT            : pu4VniIdx VNI Number                               */
/* OUTPUT           : pu1VrfName - VRF Name                              */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/* ********************************************************************* */
INT4
EvpnCliGetVrfNameFromVniMapTable (UINT4 *pu4VniIdx, UINT1 *pu1VrfName)
{
    if (VXLAN_FAILURE ==
        (EvpnUtilGetVrfNameFromVrfEntry (*pu4VniIdx, pu1VrfName)))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

#endif /* EVPN_VXLAN_WANTED */

#endif
