/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxweb.c,v 1.7 2015/03/11 10:56:38 siva Exp $
*
*********************************************************************/

#ifdef WEBNM_WANTED
#include "vxinc.h"

PRIVATE VOID IssProcessVxlanNvePageGet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanNvePageSet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanVtepPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanVtepPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanMcastPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanMcastPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanVniVlanPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID IssProcessVxlanVniVlanPageSet PROTO ((tHttp * pHttp));

/*********************************************************************
 *  Function Name : IssProcessVxlanNvePage
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanNvePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessVxlanNvePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessVxlanNvePageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessVxlanNvePageGet
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanNvePageGet (tHttp * pHttp)
{

    INT4                i4IfIndex = 0;
    INT4                i4IfNum = 0;
    UINT4               u4VxlanID = 0;
    tMacAddr            macAddr;
    INT4                i4NextIfindex = 0;
    UINT4               u4NextVxlanID = 0;
    tMacAddr            NextmacAddr;
    UINT1               au1String[20];
    INT4                i4AddrType = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4OutCome = 0;
    UINT1              *pu1String = NULL;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE RemoteVtepAddr;
    INT4                i4SlotId = 0;

    UNUSED_PARAM (i4SlotId);
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    MEMSET (au1IPAddr, 0, sizeof (au1IPAddr));
    MEMSET (&RemoteVtepAddr, 0, sizeof (RemoteVtepAddr));

    /* Get the First Index of the File Table */
    i4OutCome =
        (nmhGetFirstIndexFsVxlanNveTable (&i4IfIndex, &u4VxlanID, &macAddr));

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        MEMSET (&RemoteVtepAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        STRNCPY (pHttp->au1KeyString, "INTERFACE_INDEX_KEY", STRLEN("INTERFACE_INDEX_KEY"));
	pHttp->au1KeyString[STRLEN("INTERFACE_INDEX_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetSlotAndPortFromIfIndex ((UINT4) i4IfIndex, &i4SlotId, &i4IfNum);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4IfNum);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "VXLAN_ID_KEY", STRLEN("VXLAN_ID_KEY"));
	pHttp->au1KeyString[STRLEN("VXLAN_ID_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4VxlanID);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "DEST_MAC_ADDR_KEY", STRLEN("DEST_MAC_ADDR_KEY"));
	pHttp->au1KeyString[STRLEN("DEST_MAC_ADDR_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        IssPrintMacAddress (macAddr, au1String);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "REMOTE_VTEP_IP_ADDRESS_TYPE_KEY", STRLEN("REMOTE_VTEP_IP_ADDRESS_TYPE_KEY"));
	pHttp->au1KeyString[STRLEN("REMOTE_VTEP_IP_ADDRESS_TYPE_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanNveRemoteVtepAddressType (i4IfIndex, u4VxlanID, macAddr,
                                               &i4AddrType);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4AddrType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "REMOTE_VTEP_IP_KEY", STRLEN("REMOTE_VTEP_IP_KEY"));
	pHttp->au1KeyString[STRLEN("REMOTE_VTEP_IP_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        RemoteVtepAddr.pu1_OctetList = au1IPAddr;
        nmhGetFsVxlanNveRemoteVtepAddress (i4IfIndex, u4VxlanID, macAddr,
                                           &RemoteVtepAddr);

        if (i4AddrType == VXLAN_SNX_IPv4)
        {
            MEMCPY (&u4RetVal, RemoteVtepAddr.pu1_OctetList, sizeof (UINT4));
            u4RetVal = OSIX_HTONL (u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pu1String);
        }
        else if (i4AddrType == VXLAN_SNX_IPv6)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   RemoteVtepAddr.pu1_OctetList));
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextIfindex = i4IfIndex;
        u4NextVxlanID = u4VxlanID;
        MEMCPY (&(NextmacAddr), &(macAddr), 6);

    }
    while (nmhGetNextIndexFsVxlanNveTable
           (i4NextIfindex, &i4IfIndex, u4NextVxlanID, &u4VxlanID, NextmacAddr,
            &macAddr) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessVxlanNvePageSet
 *  Description   : This function processes the Set request coming for
 *                  DNS Name Server Page
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanNvePageSet (tHttp * pHttp)
{
    UINT4               u4IfIpAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfIndex = 0;
    INT1                i1IfNum = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT4               u4VxlanId = 0;
    tMacAddr            macAddr;
    tSNMP_OCTET_STRING_TYPE RemoteVtepAddr;
    UINT1               au1IpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddType = 0;
    tIp6Addr           *Ip6Addr = NULL;
    INT4                i4FsVxlanEnable = 0;

    MEMSET (&RemoteVtepAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    STRNCPY (au1IfName, "nve", 3);
    piIfName = (INT1 *) &au1IfName[0];

    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        IssSendError (pHttp, (CONST INT1 *) "VXLAN Feature is not Enabled");
        return;
    }

    STRNCPY (pHttp->au1Name, "INTERFACE_INDEX", STRLEN("INTERFACE_INDEX"));
    pHttp->au1Name[STRLEN("INTERFACE_INDEX")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1IfNum = (INT1) ATOI (pHttp->au1Value);
    CfaCliGetNveIndex (piIfName, &i1IfNum, (UINT4 *) &i4IfIndex);

    STRNCPY (pHttp->au1Name, "VXLAN_ID", STRLEN("VXLAN_ID"));
    pHttp->au1Name[STRLEN("VXLAN_ID")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4VxlanId = (UINT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "DEST_MAC_ADDR", STRLEN("DEST_MAC_ADDR"));
    pHttp->au1Name[STRLEN("DEST_MAC_ADDR")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, macAddr);

    STRNCPY (pHttp->au1Name, "REMOTE_VTEP_IP_ADDRESS_TYPE", STRLEN("REMOTE_VTEP_IP_ADDRESS_TYPE"));
    pHttp->au1Name[STRLEN("REMOTE_VTEP_IP_ADDRESS_TYPE")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AddType = ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "REMOTE_VTEP_IP", STRLEN("REMOTE_VTEP_IP"));
    pHttp->au1Name[STRLEN("REMOTE_VTEP_IP")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    RemoteVtepAddr.pu1_OctetList = au1IpAddr;
    if (i4AddType == VXLAN_SNX_IPv4)
    {
        RemoteVtepAddr.i4_Length = SNX_IP4_LEN;
        u4IfIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        MEMCPY (RemoteVtepAddr.pu1_OctetList, &u4IfIpAddr,
                RemoteVtepAddr.i4_Length);
        VXLAN_INET_HTONL (RemoteVtepAddr.pu1_OctetList);
    }

    else if (i4AddType == VXLAN_SNX_IPv6)
    {
        RemoteVtepAddr.i4_Length = SNX_IP6_LEN;
        issDecodeSpecialChar (pHttp->au1Value);
        Ip6Addr = str_to_ip6addr (pHttp->au1Value);
        if (Ip6Addr != NULL)
        {
            MEMCPY (RemoteVtepAddr.pu1_OctetList, Ip6Addr, VXLAN_IP6_ADDR_LEN);
        }
    }

    STRNCPY (pHttp->au1Name, "ACTION", STRLEN("ACTION"));
    pHttp->au1Name[STRLEN("ACTION")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Add") == 0)
    {
        if (nmhTestv2FsVxlanNveRowStatus
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhSetFsVxlanNveRowStatus
            (i4IfIndex, u4VxlanId, macAddr,
             ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanNveRemoteVtepAddressType
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }
        if (nmhTestv2FsVxlanNveRemoteVtepAddress
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             &RemoteVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Remote VTEP IP");
            return;
        }
        if (nmhSetFsVxlanNveRemoteVtepAddressType
            (i4IfIndex, u4VxlanId, macAddr, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }

        if (nmhSetFsVxlanNveRemoteVtepAddress
            (i4IfIndex, u4VxlanId, macAddr, &RemoteVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Remote VTEP IP");
            return;
        }

        if (nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Could not Create Active State for the Row Status");
            return;
        }

    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        if (nmhTestv2FsVxlanNveRowStatus
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanNveRemoteVtepAddressType
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }
        if (nmhTestv2FsVxlanNveRemoteVtepAddress
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             &RemoteVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Remote VTEP IP");
            return;
        }
        if (nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }

        if (nmhSetFsVxlanNveRemoteVtepAddressType
            (i4IfIndex, u4VxlanId, macAddr, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }

        if (nmhSetFsVxlanNveRemoteVtepAddress
            (i4IfIndex, u4VxlanId, macAddr, &RemoteVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanNveRowStatus (i4IfIndex, u4VxlanId, macAddr,
                                       ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Remote VTEP IP");
            return;
        }

        if (nmhSetFsVxlanNveRowStatus
            (i4IfIndex, u4VxlanId, macAddr, ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Update the Row Status");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Delete") == 0)
    {
        if (nmhTestv2FsVxlanNveRowStatus
            (&u4ErrorCode, i4IfIndex, u4VxlanId, macAddr,
             ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
        if (nmhSetFsVxlanNveRowStatus
            (i4IfIndex, u4VxlanId, macAddr, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
    }
    IssProcessVxlanNvePageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVtepPage
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVtepPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessVxlanVtepPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessVxlanVtepPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVtepPageGet
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVtepPageGet (tHttp * pHttp)
{

    INT4                i4IfIndex = 0;
    INT4                i4IfNum = 0;
    INT4                i4NextIfindex = 0;
    INT4                i4AddrType = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4OutCome = 0;
    UINT1              *pu1String = NULL;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE SourceVtepAddr;
    INT4                i4SlotId = 0;

    UNUSED_PARAM (i4SlotId);
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    MEMSET (au1IPAddr, 0, sizeof (au1IPAddr));
    MEMSET (&SourceVtepAddr, 0, sizeof (SourceVtepAddr));

    /* Get the First Index of the File Table */
    i4OutCome = (nmhGetFirstIndexFsVxlanVtepTable (&i4IfIndex));
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        MEMSET (&SourceVtepAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        STRNCPY (pHttp->au1KeyString, "INTERFACE_INDEX_KEY", STRLEN("INTERFACE_INDEX_KEY"));
	pHttp->au1KeyString[STRLEN("INTERFACE_INDEX_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetSlotAndPortFromIfIndex ((UINT4)i4IfIndex, &i4SlotId, &i4IfNum);

        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4IfNum);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "SOURCE_VTEP_IP_ADDRESS_TYPE_KEY", STRLEN("SOURCE_VTEP_IP_ADDRESS_TYPE_KEY"));
	pHttp->au1KeyString[STRLEN("SOURCE_VTEP_IP_ADDRESS_TYPE_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanVtepAddressType (i4IfIndex, &i4AddrType);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4AddrType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "SOURCE_VTEP_IP_KEY", STRLEN("SOURCE_VTEP_IP_KEY"));
	pHttp->au1KeyString[STRLEN("SOURCE_VTEP_IP_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SourceVtepAddr.pu1_OctetList = au1IPAddr;
        nmhGetFsVxlanVtepAddress (i4IfIndex, &SourceVtepAddr);

        if (i4AddrType == VXLAN_SNX_IPv4)
        {
            MEMCPY (&u4RetVal, SourceVtepAddr.pu1_OctetList, sizeof (UINT4));
            u4RetVal = OSIX_HTONL (u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pu1String);
        }
        else if (i4AddrType == VXLAN_SNX_IPv6)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   SourceVtepAddr.pu1_OctetList));
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextIfindex = i4IfIndex;
    }
    while (nmhGetNextIndexFsVxlanVtepTable
           (i4NextIfindex, &i4IfIndex) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVtepPageSet
 *  Description   : This function processes the Set request coming for
 *                  DNS Name Server Page
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVtepPageSet (tHttp * pHttp)
{
    UINT4               u4IfIpAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfIndex = 0;
    INT1                i1IfNum = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    tSNMP_OCTET_STRING_TYPE SourceVtepAddr;
    UINT1               au1IpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddType = 0;
    tIp6Addr           *Ip6Addr = NULL;
    INT4                i4FsVxlanEnable = 0;

    MEMSET (&SourceVtepAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    STRNCPY (au1IfName, "nve", 3);
    piIfName = (INT1 *) &au1IfName[0];

    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        IssSendError (pHttp, (CONST INT1 *) "VXLAN Feature is not Enabled");
        return;
    }

    STRNCPY (pHttp->au1Name, "INTERFACE_INDEX", STRLEN("INTERFACE_INDEX"));
    pHttp->au1Name[STRLEN("INTERFACE_INDEX")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1IfNum = (INT1) ATOI (pHttp->au1Value);
    CfaCliGetNveIndex (piIfName, &i1IfNum, (UINT4 *) &i4IfIndex);

    STRNCPY (pHttp->au1Name, "SOURCE_VTEP_IP_ADDRESS_TYPE", STRLEN("SOURCE_VTEP_IP_ADDRESS_TYPE"));
    pHttp->au1Name[STRLEN("SOURCE_VTEP_IP_ADDRESS_TYPE")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AddType = ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "SOURCE_VTEP_IP", STRLEN("SOURCE_VTEP_IP"));
    pHttp->au1Name[STRLEN("SOURCE_VTEP_IP")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    SourceVtepAddr.pu1_OctetList = au1IpAddr;
    if (i4AddType == VXLAN_SNX_IPv4)
    {
        SourceVtepAddr.i4_Length = SNX_IP4_LEN;
        u4IfIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        MEMCPY (SourceVtepAddr.pu1_OctetList, &u4IfIpAddr,
                SourceVtepAddr.i4_Length);
        VXLAN_INET_HTONL (SourceVtepAddr.pu1_OctetList);
    }

    else if (i4AddType == VXLAN_SNX_IPv6)
    {
        SourceVtepAddr.i4_Length = SNX_IP6_LEN;
        issDecodeSpecialChar (pHttp->au1Value);
        Ip6Addr = str_to_ip6addr (pHttp->au1Value);
        if (Ip6Addr != NULL)
        {
            MEMCPY (SourceVtepAddr.pu1_OctetList, Ip6Addr, VXLAN_IP6_ADDR_LEN);
        }
    }

    STRNCPY (pHttp->au1Name, "ACTION", STRLEN("ACTION"));
    pHttp->au1Name[STRLEN("ACTION")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Add") == 0)
    {
        if (nmhTestv2FsVxlanVtepRowStatus (&u4ErrorCode, i4IfIndex,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhSetFsVxlanVtepRowStatus
            (i4IfIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanVtepAddressType
            (&u4ErrorCode, i4IfIndex, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }
        if (nmhTestv2FsVxlanVtepAddress
            (&u4ErrorCode, i4IfIndex, &SourceVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Source VTEP IP");
            return;
        }
        if (nmhSetFsVxlanVtepAddressType (i4IfIndex, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }

        if (nmhSetFsVxlanVtepAddress (i4IfIndex, &SourceVtepAddr)
            == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Source VTEP IP");
            return;
        }

        if (nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Could not Create Active State for the Row Status");
            return;
        }

    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        if (nmhTestv2FsVxlanVtepRowStatus (&u4ErrorCode, i4IfIndex,
                                           ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanVtepAddressType
            (&u4ErrorCode, i4IfIndex, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }
        if (nmhTestv2FsVxlanVtepAddress
            (&u4ErrorCode, i4IfIndex, &SourceVtepAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Source VTEP IP");
            return;
        }
        if (nmhSetFsVxlanVtepRowStatus (i4IfIndex,
                                        ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }

        if (nmhSetFsVxlanVtepAddressType (i4IfIndex, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }

        if (nmhSetFsVxlanVtepAddress (i4IfIndex, &SourceVtepAddr)
            == SNMP_FAILURE)
        {
            nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Source VTEP IP");
            return;
        }

        if (nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Update the Row Status");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Delete") == 0)
    {
        if (nmhTestv2FsVxlanVtepRowStatus (&u4ErrorCode, i4IfIndex,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
        if (nmhSetFsVxlanVtepRowStatus (i4IfIndex, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
    }

    IssProcessVxlanVtepPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessVxlanMcastPage
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanMcastPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessVxlanMcastPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessVxlanMcastPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessVxlanMcastPageGet
 *  Description   : This function processes the request coming for the
 *                  Nat Static Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanMcastPageGet (tHttp * pHttp)
{

    INT4                i4IfIndex = 0;
    INT4                i4IfNum = 0;
    UINT4               u4VxlanID = 0;
    INT4                i4NextIfindex = 0;
    UINT4               u4NextVxlanID = 0;
    INT4                i4AddrType = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4OutCome = 0;
    UINT1              *pu1String = NULL;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE McastAddr;
    INT4                i4SlotId = 0;

    UNUSED_PARAM (i4SlotId);
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    MEMSET (au1IPAddr, 0, sizeof (au1IPAddr));
    MEMSET (&McastAddr, 0, sizeof (McastAddr));
    /* Get the First Index of the File Table */
    i4OutCome = (nmhGetFirstIndexFsVxlanMCastTable (&i4IfIndex, &u4VxlanID));

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        MEMSET (&McastAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        STRNCPY (pHttp->au1KeyString, "INTERFACE_INDEX_KEY", STRLEN("INTERFACE_INDEX_KEY"));
	pHttp->au1KeyString[STRLEN("INTERFACE_INDEX_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetSlotAndPortFromIfIndex ((UINT4) i4IfIndex, &i4SlotId, &i4IfNum);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4IfNum);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "VXLAN_ID_KEY", STRLEN("VXLAN_ID_KEY"));
	pHttp->au1KeyString[STRLEN("VXLAN_ID_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4VxlanID);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "MULTICAST_VTEP_IP_ADDRESS_TYPE_KEY", STRLEN("MULTICAST_VTEP_IP_ADDRESS_TYPE_KEY"));
	pHttp->au1KeyString[STRLEN("MULTICAST_VTEP_IP_ADDRESS_TYPE_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanMCastGroupAddressType (i4IfIndex, u4VxlanID, &i4AddrType);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4AddrType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "MULTICAST_VTEP_IP_KEY", STRLEN("MULTICAST_VTEP_IP_KEY"));
	pHttp->au1KeyString[STRLEN("MULTICAST_VTEP_IP_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        McastAddr.pu1_OctetList = au1IPAddr;
        nmhGetFsVxlanMCastGroupAddress (i4IfIndex, u4VxlanID, &McastAddr);

        if (i4AddrType == VXLAN_SNX_IPv4)
        {
            MEMCPY (&u4RetVal, McastAddr.pu1_OctetList, sizeof (UINT4));
            u4RetVal = OSIX_HTONL (u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pu1String);
        }
        else if (i4AddrType == VXLAN_SNX_IPv6)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   McastAddr.pu1_OctetList));
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextIfindex = i4IfIndex;
        u4NextVxlanID = u4VxlanID;

    }
    while (nmhGetNextIndexFsVxlanMCastTable
           (i4NextIfindex, &i4IfIndex, u4NextVxlanID,
            &u4VxlanID) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessVxlanMcastPageSet
 *  Description   : This function processes the Set request coming for
 *                  DNS Name Server Page
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanMcastPageSet (tHttp * pHttp)
{
    UINT4               u4IfIpAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfIndex = 0;
    INT1                i1IfNum = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT4               u4VxlanId = 0;
    tSNMP_OCTET_STRING_TYPE McastAddr;
    UINT1               au1IpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddType = 0;
    tIp6Addr           *Ip6Addr = NULL;
    INT4                i4FsVxlanEnable = 0;
    MEMSET (&McastAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    STRNCPY (au1IfName, "nve", 3);
    piIfName = (INT1 *) &au1IfName[0];

    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        IssSendError (pHttp, (CONST INT1 *) "VXLAN Feature is not Enabled");
        return;
    }
    STRNCPY (pHttp->au1Name, "INTERFACE_INDEX", STRLEN("INTERFACE_INDEX"));
    pHttp->au1Name[STRLEN("INTERFACE_INDEX")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1IfNum = (INT1) ATOI (pHttp->au1Value);
    CfaCliGetNveIndex (piIfName, &i1IfNum, (UINT4 *) &i4IfIndex);

    STRNCPY (pHttp->au1Name, "VXLAN_ID", STRLEN("VXLAN_ID"));
    pHttp->au1Name[STRLEN("VXLAN_ID")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4VxlanId = (UINT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "MULTICAST_VTEP_IP_ADDRESS_TYPE", STRLEN("MULTICAST_VTEP_IP_ADDRESS_TYPE"));
    pHttp->au1Name[STRLEN("MULTICAST_VTEP_IP_ADDRESS_TYPE")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AddType = ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "MULTICAST_VTEP_IP", STRLEN("MULTICAST_VTEP_IP"));
    pHttp->au1Name[STRLEN("MULTICAST_VTEP_IP")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    McastAddr.pu1_OctetList = au1IpAddr;
    if (i4AddType == VXLAN_SNX_IPv4)
    {
        McastAddr.i4_Length = SNX_IP4_LEN;
        u4IfIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        MEMCPY (McastAddr.pu1_OctetList, &u4IfIpAddr, McastAddr.i4_Length);
        VXLAN_INET_HTONL (McastAddr.pu1_OctetList);
    }

    else if (i4AddType == VXLAN_SNX_IPv6)
    {
        McastAddr.i4_Length = SNX_IP6_LEN;
        issDecodeSpecialChar (pHttp->au1Value);
        Ip6Addr = str_to_ip6addr (pHttp->au1Value);
        if (Ip6Addr != NULL)
        {
            MEMCPY (McastAddr.pu1_OctetList, Ip6Addr, VXLAN_IP6_ADDR_LEN);
        }
    }

    STRNCPY (pHttp->au1Name, "ACTION", STRLEN("ACTION"));
    pHttp->au1Name[STRLEN("ACTION")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Add") == 0)
    {
        if (nmhTestv2FsVxlanMCastRowStatus (&u4ErrorCode, i4IfIndex, u4VxlanId,
                                            ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhSetFsVxlanMCastRowStatus
            (i4IfIndex, u4VxlanId, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanMCastGroupAddressType
            (&u4ErrorCode, i4IfIndex, u4VxlanId, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }
        if (nmhTestv2FsVxlanMCastGroupAddress
            (&u4ErrorCode, i4IfIndex, u4VxlanId, &McastAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Multicast Group IP");
            return;
        }
        if (nmhSetFsVxlanMCastGroupAddressType (i4IfIndex, u4VxlanId, i4AddType)
            == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Address Type");
            return;
        }

        if (nmhSetFsVxlanMCastGroupAddress
            (i4IfIndex, u4VxlanId, &McastAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Multicast Group IP");
            return;
        }
        if (nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId,
                                         ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Could not Create Active State for the Row Status");
            return;
        }

    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        if (nmhTestv2FsVxlanMCastRowStatus (&u4ErrorCode, i4IfIndex, u4VxlanId,
                                            ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanMCastGroupAddressType
            (&u4ErrorCode, i4IfIndex, u4VxlanId, i4AddType) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }
        if (nmhTestv2FsVxlanMCastGroupAddress
            (&u4ErrorCode, i4IfIndex, u4VxlanId, &McastAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Multicast Group IP");
            return;
        }
        if (nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId,
                                         ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }

        if (nmhSetFsVxlanMCastGroupAddressType (i4IfIndex, u4VxlanId, i4AddType)
            == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit the Address Type");
            return;
        }
        if (nmhSetFsVxlanMCastGroupAddress
            (i4IfIndex, u4VxlanId, &McastAddr) == SNMP_FAILURE)
        {
            nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Edit Multicast Group IP");
            return;
        }
        if (nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Update the Row Status");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Delete") == 0)
    {
        if (nmhTestv2FsVxlanMCastRowStatus (&u4ErrorCode, i4IfIndex, u4VxlanId,
                                            ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
        if (nmhSetFsVxlanMCastRowStatus (i4IfIndex, u4VxlanId, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
    }

    IssProcessVxlanMcastPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVniVlanPage
 *  Description   : This function processes the request coming for the
 *                  Vni Vlan Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVniVlanPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessVxlanVniVlanPageGet (pHttp);                                                                                                                       }
    else if (pHttp->i4Method == ISS_SET)                                                                                                                         {
        IssProcessVxlanVniVlanPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVniVlanPageGet
 *  Description   : This function processes the request coming for the
 *                  Vni Vlan Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVniVlanPageGet (tHttp * pHttp)
{
    INT4                i4VlanId = 0;
    UINT4               u4VxlanID = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0;
    UINT4               u4PktSent = 0;
    UINT4               u4PktRcvd = 0;
    UINT4               u4PktDrpd = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    /* Get the First Index of the File Table */
    i4OutCome =
        (nmhGetFirstIndexFsVxlanVniVlanMapTable (&i4VlanId));                                                                                                
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;                                                                                                                             do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRNCPY (pHttp->au1KeyString, "VLAN_ID_KEY", STRLEN("VLAN_ID_KEY"));
	pHttp->au1KeyString[STRLEN("VLAN_ID_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", i4VlanId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "VXLAN_ID_KEY", STRLEN("VXLAN_ID_KEY"));
	pHttp->au1KeyString[STRLEN("VXLAN_ID_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanVniVlanMapVniNumber(i4VlanId, &u4VxlanID);                                                                                                      
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4VxlanID);
        WebnmSockWrite (pHttp, pHttp->au1DataString,                                                                                                                                 (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);


        STRNCPY (pHttp->au1KeyString, "PKT_SENT_KEY", STRLEN("PKT_SENT_KEY"));
	pHttp->au1KeyString[STRLEN("PKT_SENT_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanVniVlanMapPktSent(i4VlanId, &u4PktSent);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4PktSent);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "PKT_RECIEVED_KEY", STRLEN("PKT_RECIEVED_KEY"));
	pHttp->au1KeyString[STRLEN("PKT_RECIEVED_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanVniVlanMapPktRcvd(i4VlanId, &u4PktRcvd);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4PktRcvd);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRNCPY (pHttp->au1KeyString, "PKT_DROPPED_KEY", STRLEN("PKT_DROPPED_KEY"));
	pHttp->au1KeyString[STRLEN("PKT_DROPPED_KEY")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsVxlanVniVlanMapPktDrpd(i4VlanId, &u4PktDrpd);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,sizeof(pHttp->au1DataString), "%d", u4PktDrpd);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                      (INT4) STRLEN (pHttp->au1DataString));                                                                                                       STRNCPY (pHttp->au1KeyString, "<! PARAM STOP>", STRLEN("<! PARAM STOP>"));
	pHttp->au1KeyString[STRLEN("<! PARAM STOP>")] = '\0';
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextVlanId = i4VlanId;

    }
    while (nmhGetNextIndexFsVxlanVniVlanMapTable
           (i4NextVlanId, &i4VlanId) == SNMP_SUCCESS);
                                                                                                                                                                 WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessVxlanVniVlanPageSet
 *  Description   : This function processes the Set request coming for
 *                  Vni Vlan Page
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessVxlanVniVlanPageSet (tHttp * pHttp)                                                                                                                {
    INT4                i4VlanId = 0;                                                                                                                            UINT4               u4ErrorCode = 0;
    UINT4               u4VxlanId = 0;
    INT4                i4FsVxlanEnable = 0;
    UINT4               u4PktSent = 0;
    UINT4               u4PktRcvd = 0;
    UINT4               u4PktDrpd = 0;

    VxlanGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_ENABLED)
    {
        IssSendError (pHttp, (CONST INT1 *) "VXLAN Feature is not Enabled");
        return;
    }

    STRNCPY (pHttp->au1Name, "VLAN_ID", STRLEN("VLAN_ID"));
    pHttp->au1Name[STRLEN("VLAN_ID")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = (INT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "VXLAN_ID", STRLEN("VXLAN_ID"));
    pHttp->au1Name[STRLEN("VXLAN_ID")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4VxlanId = (UINT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "PKT_SENT", STRLEN("PKT_SENT"));
    pHttp->au1Name[STRLEN("PKT_SENT")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PktSent = (UINT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "PKT_RECIEVED", STRLEN("PKT_RECIEVED"));
    pHttp->au1Name[STRLEN("PKT_RECIEVED")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PktRcvd = (UINT4) ATOI (pHttp->au1Value);

    STRNCPY (pHttp->au1Name, "PKT_DROPPED", STRLEN("PKT_DROPPED"));
    pHttp->au1Name[STRLEN("PKT_DROPPED")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);                                                                                   u4PktDrpd = (UINT4) ATOI (pHttp->au1Value);
    STRNCPY (pHttp->au1Name, "ACTION", STRLEN("ACTION"));
    pHttp->au1Name[STRLEN("ACTION")] = '\0';
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Add") == 0)
    {
        if (nmhTestv2FsVxlanVniVlanMapRowStatus
            (&u4ErrorCode, i4VlanId, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,                                                                                                                                                       (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapRowStatus
            (i4VlanId, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapVniNumber
            (&u4ErrorCode, i4VlanId, u4VxlanId) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,                                                                                                                                                       (CONST INT1 *) "Could not Create Vxlan ID");
            return;                                                                                                                                                  }
        if (nmhTestv2FsVxlanVniVlanMapPktSent
            (&u4ErrorCode, i4VlanId, u4PktSent) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Sent count");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapPktRcvd
            (&u4ErrorCode, i4VlanId, u4PktRcvd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Recieved count");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapPktDrpd
            (&u4ErrorCode, i4VlanId, u4PktDrpd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Dropped count");
            return;
        }

        if (nmhSetFsVxlanVniVlanMapVniNumber
            (i4VlanId, u4VxlanId) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Vxlan ID");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktSent
            (i4VlanId, u4PktSent) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Sent count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktRcvd
            (i4VlanId, u4PktRcvd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Recieved count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktDrpd
            (i4VlanId, u4PktDrpd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Create Packets Dropped count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Could not Create Active State for the Row Status");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        if (nmhTestv2FsVxlanVniVlanMapRowStatus
            (&u4ErrorCode, i4VlanId, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapVniNumber
            (&u4ErrorCode, i4VlanId, u4VxlanId) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Vxlan ID");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapPktSent
            (&u4ErrorCode, i4VlanId, u4PktSent) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Packets Sent count");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapPktRcvd
            (&u4ErrorCode, i4VlanId, u4PktRcvd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Packets Recieved count");
            return;
        }
        if (nmhTestv2FsVxlanVniVlanMapPktDrpd
            (&u4ErrorCode, i4VlanId, u4PktDrpd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Packets Dropped count");
            return;
        }

        if (nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not update the Row Status");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapVniNumber
            (i4VlanId, u4VxlanId) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Vxlan ID");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktSent
            (i4VlanId, u4PktSent) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not editPackets Sent count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktRcvd
            (i4VlanId, u4PktRcvd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Packets Recieved count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapPktDrpd
            (i4VlanId, u4PktDrpd) == SNMP_FAILURE)
        {
            nmhSetFsVxlanVniVlanMapRowStatus (i4VlanId, ISS_ACTIVE);
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not edit Packets Dropped count");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapRowStatus
            (i4VlanId, ISS_ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Update the Row Status");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Array, "Delete") == 0)
    {
        if (nmhTestv2FsVxlanVniVlanMapRowStatus
            (&u4ErrorCode, i4VlanId, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
        if (nmhSetFsVxlanVniVlanMapRowStatus
            (i4VlanId, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Could not Delete the Row Status");
            return;
        }
    }
    IssProcessVxlanVniVlanPageGet (pHttp);
}

#endif /* WEBNM_WANTED */
