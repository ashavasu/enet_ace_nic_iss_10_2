/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxudp.c,v 1.11 2015/07/23 11:21:04 siva Exp $
*
*********************************************************************/

#include "vxinc.h"
#include "fssocket.h"

#ifndef _VXLANUDP_C_
#define _VXLANUDP_C_

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpInitSock                                             *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during Vxlan module boot up. *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpInitSock (VOID)
{
    INT4                i4Flags = 0;
    UINT1               u1OpnVal = 1;
    UINT1               u1MOpnVal = 1;
    struct sockaddr_in  VxlanLocalAddr;

    MEMSET (&VxlanLocalAddr, 0, sizeof (struct sockaddr_in));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUdpInitSock: Entry\n"));

    /* Open a socket with the standard port 4789 provided in the standard for 
     * reception of Vxlan packet */
    gVxlanGlobals.i4VxlanSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gVxlanGlobals.i4VxlanSockId < 0)
    {
        /* Failure in opening the UDP socket with port 4789 */
        perror ("Vxlanv4 Socket creation failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "UDP Socket open for reception failed\n"));
        return OSIX_FAILURE;
    }

    VxlanLocalAddr.sin_family = AF_INET;
    VxlanLocalAddr.sin_addr.s_addr = 0;
    VxlanLocalAddr.sin_port =
        OSIX_HTONS (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort);

    /* Bind with the socket */
    if (bind (gVxlanGlobals.i4VxlanSockId, (struct sockaddr *) &VxlanLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("Vxlanv4 Socket bind failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "UDP Socket bind failed\n"));
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gVxlanGlobals.i4VxlanSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Acquiring the flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gVxlanGlobals.i4VxlanSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Set flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    if (setsockopt (gVxlanGlobals.i4VxlanSockId, IPPROTO_IP, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt Failed for IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }

    /* Open a socket for sendind Vxlan packet */
    gVxlanGlobals.i4VxlanTxSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gVxlanGlobals.i4VxlanTxSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("Vxlanv4 Socket creation failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "UDP socket open for transmission failed\n"));
        return OSIX_FAILURE;
    }

    VxlanLocalAddr.sin_family = AF_INET;
    VxlanLocalAddr.sin_addr.s_addr = 0;
    VxlanLocalAddr.sin_port = 0;

    /* Bind with the socket */
    if (bind
        (gVxlanGlobals.i4VxlanTxSockId, (struct sockaddr *) &VxlanLocalAddr,
         sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("Vxlanv4 Socket bind failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "UDP Socket bind failed\n"));
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gVxlanGlobals.i4VxlanTxSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Acquiring the flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gVxlanGlobals.i4VxlanTxSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Set flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    if (setsockopt (gVxlanGlobals.i4VxlanTxSockId, IPPROTO_IP, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt Failed for IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }

    if (setsockopt (gVxlanGlobals.i4VxlanTxSockId, IP_MULTICAST_IF, IP_PKTINFO,
                    &u1MOpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "setsockopt Failed for multicast IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }

    if (SelAddFd (gVxlanGlobals.i4VxlanTxSockId,
                  VxlanPktRcvdOnSocket) != OSIX_SUCCESS)
    {
        /* SelAddFd failed for TX socket */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "SelAddFd failed for TX socket\n"));
        return OSIX_FAILURE;
    }

#ifdef IP6_WANTED
    if (VxlanUdpv6InitSock () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUdpInitSock: Exit\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpAddOrRemoveFd                                        *
 *                                                                           *
 * Description  : This routine is called during module start or shutdown.    *
 *                This routine adds/removes the FD from the socket           *
 *                                                                           *
 * Input        : bIsAddFd - If this flag is set Add FD                      *
 *                           Otherwise Remove FD                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpAddOrRemoveFd (BOOL1 bIsAddFd)
{

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpAddOrRemoveFd: Entry\n"));

    if (bIsAddFd == (UINT1) OSIX_TRUE)
    {
        if ((gVxlanGlobals.i4VxlanSockId != -1) &&
            (SelAddFd (gVxlanGlobals.i4VxlanSockId, VxlanPktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "SelAddFd failed for TX socket\n"));
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (gVxlanGlobals.i4VxlanSockId != -1)
        {
            SelRemoveFd (gVxlanGlobals.i4VxlanSockId);
        }
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpAddOrRemoveFd: Exit\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : VxlanUdpTransmitVxlanPkt                                  */
/* Description   : Sends the VXLAN message                                   */
/* Input(s)      : pBuf       - Buffer to Send                               */
/* Input(s)      : u2BufLen   - Buffer Size                                  */
/*                 u4DestAddr - DestinationIP                                */
/*                 u4SrcAddr  - Source IP                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/
UINT1
VxlanUdpTransmitVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4SrcAddr,
                          UINT4 u4DestAddr, UINT2 u2BufLen, UINT4 u4IfIndex)
{
    UINT1              *pu1Data = NULL;

    pu1Data = MemAllocMemBlk (VXLAN_UDP_SEND_BUF_POOLID);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpTransmitVxlanPkt: Entry\n"));

    if (pu1Data != NULL)
    {
        /*
         * A linear buffer is expected to be passed while writing to
         *  socket. So copying the contents of pBuf to a linear buffer.
         */

        if ((CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u2BufLen) ==
             u2BufLen))
        {
            if (VxlanSendMessage (pu1Data, u2BufLen, u4SrcAddr, u4DestAddr,
                                  u4IfIndex) == VXLAN_FAILURE)
            {
                MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID,
                                    (UINT1 *) pu1Data);
                VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                 "IPV4 UDP packet send failed\n"));
                return VXLAN_FAILURE;
            }
            MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID, (UINT1 *) pu1Data);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                             "IPV4 UDP packet successfully sent\n"));
            return VXLAN_SUCCESS;
        }
        MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID, (UINT1 *) pu1Data);
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "IPV4 UDP packet send failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "IPV4 UDP packet send failed\n"));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpTransmitVxlanPkt: Exit\n"));

    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name : VxlanSendMessage                                          */
/* Description   : Function to set IP_INFO for UDP Send                      */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 u4DestAddr - DestinationIP                                */
/*                 u2DestPort - Destination Port                             */
/* Output(s)     : None                                                      */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/
INT4
VxlanSendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                  UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4 u4IfIndex)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct in_addr      SrcIfAddr;
    struct in_addr      DestIfAddr;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[VXLAN_MSG_LENGTH];
    struct sockaddr_in  DestInfo;
    INT4                i4UdpSockFd = gVxlanGlobals.i4VxlanTxSockId;
    INT4                i4TtlVal = 0;
    struct ip_mreqn     Mreqn;

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));

#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    Iov.iov_base = pu1Data;
    Iov.iov_len = u2BufLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufLen);
#endif

    MEMSET (&SrcIfAddr, 0, sizeof (struct in_addr));
    MEMSET (&DestIfAddr, 0, sizeof (struct in_addr));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanSendMessage: Entry\n"));

    i4TtlVal = VXLAN_IP_TTL;

    DestIfAddr.s_addr = OSIX_HTONL (u4DestAddr);
    MEMSET (&Mreqn, 0, sizeof (struct ip_mreqn));
    if (u4IfIndex != 0)
    {
        Mreqn.imr_multiaddr = DestIfAddr;
        Mreqn.imr_ifindex = (INT4) u4IfIndex;
        SrcIfAddr.s_addr = OSIX_HTONL (u4SrcAddr);
        Mreqn.imr_address = SrcIfAddr;
        if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_MULTICAST_IF,
                         (char *) &(Mreqn), sizeof (Mreqn))) < 0)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                             "setsockopt failed for Multicast IP\n"));
            return VXLAN_FAILURE;
        }
    }

    else if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_TTL,
                          &i4TtlVal, sizeof (INT4))) < 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt failed for IP TTL\n"));
        return VXLAN_FAILURE;
    }

    MEMSET ((INT1 *) &DestInfo, 0, sizeof (struct sockaddr_in));
    DestInfo.sin_family = AF_INET;
    DestInfo.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestInfo.sin_port = OSIX_HTONS (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort);

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (VOID *) &DestInfo;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
#ifndef LNXIP4_WANTED
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
#else
    PktInfo.msg_flags = 0;
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_UDP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in_pktinfo));
#endif
    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

     if (u4IfIndex == 0)
     {
         if (NETIPV4_SUCCESS == NetIpv4GetIfIndexFromAddr (u4SrcAddr,
                                                           &u4IfIndex))
         {
             pIpPktInfo->ipi_ifindex = (INT4)u4IfIndex;

             pIpPktInfo->ipi_spec_dst.s_addr = u4SrcAddr;

             if (sendmsg (i4UdpSockFd, &PktInfo, 0) < 0)
             {
                 perror ("VXLAN send message failed");
                 return VXLAN_FAILURE;
             }
         }

    }
    if (sendto (i4UdpSockFd, (INT1 *) pu1Data, (UINT4) u2BufLen, 0,
                (struct sockaddr *) &DestInfo, sizeof (struct sockaddr)) < 0)
    {

        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanSendMessage: Exit\n"));

    return VXLAN_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpProcessPktOnSocket                                   *
 *                                                                           *
 * Description  : This routine reads the data present in the given socket    *
 *                and post the packet into Vxlan queue for processing          *
 *                                                                           *
 * Input        : i4UdpSockFd - Socket Id on which packet is received           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VXLAN_SUCCESS/VXLAN_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpProcessPktOnSocket (INT4 i4UdpSockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Data = NULL;
    INT4                i4BytesRcvd = 0;
    tIpAddr             PeerAddr;
    struct sockaddr_in  PeerInfo;
    struct msghdr       PktInfo;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[VXLAN_MSG_LENGTH];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
    UINT4               u4PeerLen = sizeof (PeerInfo);
    UINT4               u4IpAddr = 0;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpProcessPktOnSocket: Entry\n"));

    MEMSET (&PeerAddr, 0, sizeof (tIpAddr));
    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    pu1Data = MemAllocMemBlk (VXLAN_UDP_RECV_BUF_POOLID);
    if (pu1Data != NULL)
    {
#ifdef BSDCOMP_SLI_WANTED
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
        MEMSET (&Iov, 0, sizeof (struct iovec));

        PktInfo.msg_name = (VOID *) &PeerInfo;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);
        Iov.iov_base = pu1Data;
        Iov.iov_len = VXLAN_JUMBO_MTU_SIZE;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
#ifndef LNXIP4_WANTED
        pCmsgInfo->cmsg_level = SOL_IP;
#else
        pCmsgInfo->cmsg_level = IPPROTO_UDP;
#endif
        pCmsgInfo->cmsg_type = IP_PKTINFO;
        pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        while ((i4BytesRcvd = recvmsg (i4UdpSockFd, &PktInfo, 0)) > 0)
        {
            pBuf = CRU_BUF_Allocate_MsgBufChain (VXLAN_JUMBO_MTU_SIZE, 0);
            if (pBuf != NULL)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pBuf, pu1Data, 0, (UINT4) i4BytesRcvd) == CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "Unable to copy read data to Buffer\n"));
                    continue;
                }

                MEMCPY (&PeerAddr, &(PeerInfo.sin_addr.s_addr),
                        VXLAN_IP4_ADDR_LEN);

                VxlanProcessUdpMsg (pBuf, VXLAN_IPV4_UNICAST, &PeerAddr);
            }
            MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
            PktInfo.msg_name = (VOID *) &PeerInfo;
            PktInfo.msg_namelen = sizeof (struct sockaddr_in);
            Iov.iov_base = pu1Data;
            Iov.iov_len = VXLAN_JUMBO_MTU_SIZE;
            PktInfo.msg_iov = &Iov;
            PktInfo.msg_iovlen = 1;
            PktInfo.msg_control = (VOID *) au1Cmsg;
            PktInfo.msg_controllen = sizeof (au1Cmsg);

            pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
#ifndef LNXIP4_WANTED
            pCmsgInfo->cmsg_level = SOL_IP;
#else
            pCmsgInfo->cmsg_level = IPPROTO_UDP;
#endif

            pCmsgInfo->cmsg_type = IP_PKTINFO;
            pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        }                        /* while end */
#else
        MEMSET ((INT1 *) &CmsgInfo, 0, sizeof (struct cmsghdr));
        PktInfo.msg_control = (VOID *) &CmsgInfo;
        while ((i4BytesRcvd =
                recvfrom (i4UdpSockFd, (INT1 *) pu1Data, VXLAN_JUMBO_MTU_SIZE,
                          0, (struct sockaddr *) &PeerInfo, &u4PeerLen)) > 0)
        {
            if (recvmsg (i4UdpSockFd, &PktInfo, 0) < 0)
            {
                continue;
            }

            pBuf = CRU_BUF_Allocate_MsgBufChain (VXLAN_JUMBO_MTU_SIZE, 0);
            if (pBuf != NULL)

            {
                if (CRU_BUF_Copy_OverBufChain
                    (pBuf, pu1Data, 0, (UINT4) i4BytesRcvd) == CRU_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "Unable to copy read data to Buffer\n"));
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                MEMCPY (&PeerAddr, &(PeerInfo.sin_addr.s_addr),
                        VXLAN_IP4_ADDR_LEN);

                MEMCPY (&u4IpAddr, &PeerAddr, VXLAN_IP4_ADDR_LEN);

                u4IpAddr = OSIX_HTONL (u4IpAddr);

                if (NetIpv4IfIsOurAddress (u4IpAddr) == NETIPV4_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                VxlanProcessUdpMsg (pBuf, VXLAN_IPV4_UNICAST, &PeerAddr);
            }
        }
#endif
        MemReleaseMemBlock (VXLAN_UDP_RECV_BUF_POOLID, (UINT1 *) pu1Data);
    }
    else
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                         "Memory Allocation for Receiving UDP Buffer\n"));
    }

    if (SelAddFd (i4UdpSockFd, VxlanPktRcvdOnSocket) == OSIX_FAILURE)
    {
        close (i4UdpSockFd);
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpProcessPktOnSocket: Exit\n"));

    return VXLAN_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpDeInitParams                                         *
 *                                                                           *
 * Description  : This routine de-initializes the UDP data that are needed   *
 *                by the Vxlan module. This should be invoked whenever Vxlan     *
 *                module is shutdown. This function closes all the socktes   *
 *                and reset the options created for the sockets              *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
VxlanUdpDeInitParams (INT4 i4SockId)
{

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpDeInitParams: Entry\n"));

    /* Remove the FD association from SLI */
    SelRemoveFd (i4SockId);

    /* Close the socket identifier. */
    close (i4SockId);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpDeInitParams: Exit\n"));

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanPktRcvdOnSocket                                         *
 *                                                                           *
 * Description  : This routine should be invoked when packet is received on  *
 *                the socket. This message posts an event with the socket    *
 *                identifier to the Vxlan Task.                                *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
VxlanPktRcvdOnSocket (INT4 i4SockId)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPktRcvdOnSocket: Entry\n"));

    VxlanIfMsg.u4MsgType = VXLAN_UDP_IPV4_EVENT;
    VxlanIfMsg.uVxlanMsg.i4SockId = i4SockId;

    VxlanEnqueMsg (&VxlanIfMsg);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanPktRcvdOnSocket: Exit\n"));

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpJoinMulticast                                      *
 *                                                                           *
 * Description  : This function Sends the igmp join message. Whenever        *
 *                Multicast group is configured, this function will be called*
 *                for sending igmp join to all interface. Also Every 30 sec  *
 *                this function will be called for sending igmp join message *
 *                periodically                                               *
 *                                                                           *
 * Input        : pVxlanMCastEntry - Multicast information                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
VxlanUdpJoinMulticast (tVxlanFsVxlanMCastEntry * pVxlanMCastEntry)
{

#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
    struct ip_mreqn     mreqn;

#else
    tSliIpv6Mreq        ip6Mreq;
#endif
    UINT4               u4Port = 0;
    UINT4               u4PrevPort = 0;
    tIPvXAddr           IPvXAddr;
    UINT4               u4McstAddr = 0;
    UINT4               u4PhyIfIndex = 0;
    INT4                i4UdpSockFd = gVxlanGlobals.i4Vxlanv6TxSockId;
    tNetIpv6IfInfo      Ip6Info;

    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));

    if (pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType
        == VXLAN_IPV6_UNICAST)
    {
        while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
        {
            if (
#ifdef IP6_WANTED
                   (NetIpv6GetIfInfo (u4Port, &Ip6Info) == NETIPV4_SUCCESS) &&
#endif
                   (Ip6Info.u4IpPort != 0))
            {
                MEMSET (&ip6Mreq, 0, sizeof (ip6Mreq));
                MEMCPY (ip6Mreq.ipv6mr_multiaddr.s6_addr,
                        pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                        VXLAN_IP6_ADDR_LEN);
#ifdef BSDCOMP_SLI_WANTED
#ifdef LNXIP6_WANTED
                ip6Mreq.ipv6mr_interface = (UINT4) CfaGetIfIpPort (u4Port);
#else
                ip6Mreq.ipv6mr_interface = u4Port;
#endif
#else
                ip6Mreq.ipv6mr_ifindex = (INT4) u4Port;
#endif
                /*
                 * The Interface(specified by u4LocalIfAddr), joins the multicast group
                 * specified by u4MulticastAddr.
                 * This enables the interface, to receive a pkts with DestAddr of this
                 * multicast address(u4MulticastAddr).
                 */
                if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IPV6_JOIN_GROUP,
                                 (VOID *) &ip6Mreq, sizeof (ip6Mreq))) < 0)
                {
                    u4PrevPort = u4Port;
                }
            }
            u4PrevPort = u4Port;
        }
    }
    else
    {
        MEMSET (&IPvXAddr, 0, sizeof (tIPvXAddr));
        MEMCPY (&u4McstAddr,
                pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                VXLAN_IP4_ADDR_LEN);
        u4McstAddr = OSIX_NTOHL (u4McstAddr);
        MEMCPY (IPvXAddr.au1Addr, &u4McstAddr, VXLAN_IP4_ADDR_LEN);
        while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
        {
            if (NetIpv4GetPortFromIfIndex (u4Port, &u4PhyIfIndex) == NETIPV4_SUCCESS)
            {
#ifdef BSDCOMP_SLI_WANTED
                memset (&mreqn, 0, sizeof (mreqn));
                mreqn.imr_multiaddr.s_addr = u4McstAddr;
                mreqn.imr_ifindex = (INT4) u4Port;
                if (setsockopt (i4UdpSockFd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                                (void *) &mreqn, sizeof (mreqn)) < 0)
                {
                    u4PrevPort = u4Port;
                }
#else
#ifdef IGMP_WANTED
                IgmpSendReportOrLeave (IPvXAddr, FALSE, u4PhyIfIndex);

#endif
#endif
            }
            u4PrevPort = u4Port;
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpLeaveMulticast                                     *
 *                                                                           *
 * Description  : This function Sends the igmp leave message. Whenever       *
 *                Multicast group is remove, this function will be called    *
 *                for sending igmp leave to all interface.                   *
 
 * Input        : pVxlanMCastEntry - Multicast information                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
VxlanUdpLeaveMulticast (tVxlanFsVxlanMCastEntry * pVxlanMCastEntry)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
    struct ip_mreqn     mreqn;
#else
    tSliIpv6Mreq        ip6Mreq;
#endif
    UINT4               u4Port = 0;
    UINT4               u4PrevPort = 0;
    tIPvXAddr           IPvXAddr;
    UINT4               u4McstAddr = 0;
    UINT4               u4PhyIfIndex = 0;
    INT4                i4UdpSockFd = gVxlanGlobals.i4Vxlanv6TxSockId;
    tNetIpv6IfInfo      Ip6Info;

    MEMSET (&Ip6Info, 0, sizeof (tNetIpv6IfInfo));

    if (pVxlanMCastEntry->MibObject.i4FsVxlanMCastGroupAddressType
        == VXLAN_IPV6_UNICAST)
    {
        while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
        {
            if (
#ifdef IP6_WANTED
                   (NetIpv6GetIfInfo (u4Port, &Ip6Info) == NETIPV4_SUCCESS) &&
#endif
                   (Ip6Info.u4IpPort != 0))
            {
                MEMSET (&ip6Mreq, 0, sizeof (ip6Mreq));
                MEMCPY (ip6Mreq.ipv6mr_multiaddr.s6_addr,
                        pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                        VXLAN_IP6_ADDR_LEN);
#ifdef BSDCOMP_SLI_WANTED
#ifdef LNXIP6_WANTED
                ip6Mreq.ipv6mr_interface = (UINT4) CfaGetIfIpPort (u4Port);
#else
                ip6Mreq.ipv6mr_interface = u4Port;
#endif
#else
                ip6Mreq.ipv6mr_ifindex = (INT4) u4Port;
#endif
                /*
                 * The Interface(specified by u4LocalIfAddr), joins the multicast group
                 * specified by u4MulticastAddr.
                 * This enables the interface, to receive a pkts with DestAddr of this
                 * multicast address(u4MulticastAddr).
                 */
                if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IPV6_LEAVE_GROUP,
                                 (VOID *) &ip6Mreq, sizeof (ip6Mreq))) < 0)
                {
                    u4PrevPort = u4Port;
                }
            }
            u4PrevPort = u4Port;
        }
    }
    else
    {
        MEMSET (&IPvXAddr, 0, sizeof (tIPvXAddr));
        MEMCPY (&u4McstAddr,
                pVxlanMCastEntry->MibObject.au1FsVxlanMCastGroupAddress,
                VXLAN_IP4_ADDR_LEN);
        u4McstAddr = OSIX_NTOHL (u4McstAddr);
        MEMCPY (IPvXAddr.au1Addr, &u4McstAddr, VXLAN_IP4_ADDR_LEN);
        while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
        {
            if (NetIpv4GetPortFromIfIndex (u4Port, &u4PhyIfIndex) == NETIPV4_SUCCESS)
            {
#ifdef BSDCOMP_SLI_WANTED
                memset (&mreqn, 0, sizeof (mreqn));
                mreqn.imr_multiaddr.s_addr = u4McstAddr;
                mreqn.imr_ifindex = (INT4) u4Port;
                if (setsockopt (i4UdpSockFd, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                                (void *) &mreqn, sizeof (mreqn)) < 0)
                {
                    u4PrevPort = u4Port;
                }
#else
#ifdef IGMP_WANTED
                IgmpSendReportOrLeave (IPvXAddr, TRUE, u4PhyIfIndex);
#endif
#endif
            }
            u4PrevPort = u4Port;
        }
    }
    return;
}

#ifdef IP6_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpv6InitSock                                         *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during Vxlan module boot up. *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpv6InitSock (VOID)
{
    INT4                i4Flags = 0;
    INT4                i4OptVal = 1;
#ifdef LNXIP6_WANTED
    INT4                i4MOptVal = 1;
#else
    UINT1               u1MOpnVal = 1;
    UINT1               u1OpnVal = 1;
#endif
    struct sockaddr_in6 VxlanLocalAddr;
    MEMSET (&VxlanLocalAddr, 0, sizeof (struct sockaddr_in6));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUdpv6InitSock: Entry\n"));

    /* Open a socket with the standard port 4789 provided in the standard for reception of Vxlan packet */
    gVxlanGlobals.i4Vxlanv6SockId = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

    if (gVxlanGlobals.i4Vxlanv6SockId < 0)
    {
        /* Failure in opening the UDP socket with port 4789 */
        perror ("Vxlanv6 Socket creation failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "UDP Socket open for reception failed\n"));
        return OSIX_FAILURE;
    }

#ifdef LNXIP6_WANTED
    if (setsockopt (gVxlanGlobals.i4Vxlanv6SockId, IPPROTO_IPV6, IPV6_V6ONLY,
                    &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("VXLAN - setsockopt for IPV6_V6ONLY  fails!!");
        return OSIX_FAILURE;
    }

    if (setsockopt
        (gVxlanGlobals.i4Vxlanv6SockId, IPPROTO_IPV6, IPV6_RECVPKTINFO,
         &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        perror ("VXLAN - setsockopt for IPV6_RECVPKTINFO fails!!");
        return OSIX_FAILURE;
    }

#endif

    VxlanLocalAddr.sin6_family = AF_INET6;
    MEMSET (VxlanLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (VxlanLocalAddr.sin6_addr.s6_addr));
    VxlanLocalAddr.sin6_port =
        OSIX_HTONS (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort);

    /* Bind with the socket */
    if (bind
        (gVxlanGlobals.i4Vxlanv6SockId, (struct sockaddr *) &VxlanLocalAddr,
         sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("Vxlanv6 Socket bind failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "UDP Socket bind failed\n"));
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gVxlanGlobals.i4Vxlanv6SockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Acquiring the flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gVxlanGlobals.i4Vxlanv6SockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Set flags for the socket failed\n"));
        return OSIX_FAILURE;
    }
#ifndef LNXIP6_WANTED
    if (setsockopt (gVxlanGlobals.i4Vxlanv6SockId, IPPROTO_IPV6, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt Failed for IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }
#endif
    gVxlanGlobals.i4Vxlanv6TxSockId =
        socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

    if (gVxlanGlobals.i4Vxlanv6TxSockId < 0)
    {
        /* Failure in opening the UDP socket with port */
        perror ("Vxlanv6 Socket creation failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "UDP Socket open for transmission failed\n"));
        return OSIX_FAILURE;
    }

#ifdef LNXIP6_WANTED
    if (setsockopt (gVxlanGlobals.i4Vxlanv6TxSockId, IPPROTO_IPV6, IPV6_V6ONLY,
                    &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("VXLAN - setsockopt for IPV6_V6ONLY fails!!");
        return OSIX_FAILURE;
    }
    if (setsockopt
        (gVxlanGlobals.i4Vxlanv6TxSockId, IPV6_MULTICAST_IF, IPV6_V6ONLY,
         &i4MOptVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "setsockopt Failed for multicast IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }
#endif

    VxlanLocalAddr.sin6_family = AF_INET6;
    MEMSET (VxlanLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (VxlanLocalAddr.sin6_addr.s6_addr));
    VxlanLocalAddr.sin6_port = 0;

    /* Bind with the socket */
    if (bind
        (gVxlanGlobals.i4Vxlanv6TxSockId, (struct sockaddr *) &VxlanLocalAddr,
         sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("Vxlanv6 Socket bind failed");
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "UDP Socket bind failed\n"));
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gVxlanGlobals.i4Vxlanv6TxSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "Acquiring the flags for the socket failed\n"));
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gVxlanGlobals.i4Vxlanv6TxSockId, F_SETFL, i4Flags) < 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "Set flags for the socket failed\n"));
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }
#ifndef LNXIP6_WANTED
    if (setsockopt (gVxlanGlobals.i4Vxlanv6TxSockId, IPPROTO_IPV6, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt Failed for IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }

    if (setsockopt
        (gVxlanGlobals.i4Vxlanv6TxSockId, IPV6_MULTICAST_IF, IP_PKTINFO,
         &u1MOpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                         "setsockopt Failed for multicast IP_PKTINFO\n"));
        return OSIX_FAILURE;
    }
#endif

    i4OptVal = FALSE;
    if (setsockopt
        (gVxlanGlobals.i4Vxlanv6TxSockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
         &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        return OSIX_FAILURE;
    }

    if (SelAddFd (gVxlanGlobals.i4Vxlanv6TxSockId,
                  Vxlanv6PktRcvdOnSocket) != OSIX_SUCCESS)
    {
        /* SelAddFd failed for TX socket */
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "SelAddFd failed for TX socket\n"));
        return OSIX_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUdpv6InitSock: Exit\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpAddv6OrRemoveFd                                        *
 *                                                                           *
 * Description  : This routine is called during module start or shutdown.    *
 *                This routine adds/removes the FD from the socket           *
 *                                                                           *
 * Input        : bIsAddFd - If this flag is set Add FD                      *
 *                           Otherwise Remove FD                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpv6AddOrRemoveFd (BOOL1 bIsAddFd)
{

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpAddv6OrRemoveFd: Entry\n"));

    if (bIsAddFd == (UINT1) OSIX_TRUE)
    {
        if ((gVxlanGlobals.i4Vxlanv6SockId != -1) &&
            (SelAddFd (gVxlanGlobals.i4Vxlanv6SockId, Vxlanv6PktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "SelAddFd failed for TX socket\n"));
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (gVxlanGlobals.i4Vxlanv6SockId != -1)
        {
            SelRemoveFd (gVxlanGlobals.i4Vxlanv6SockId);
        }
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpAddv6OrRemoveFd: Exit\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : VxlanUdpv6TransmitVxlanPkt                                */
/* Description   : Send the VXLAN message                                    */
/* Input(s)      : pBuf - Buffer to Send                                     */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 pSrcAddr - Source Address                                 */
/*                 pDestAddr - Destination Address                           */
/* Output(s)     : None                                                      */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/

UINT1
VxlanUdpv6TransmitVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIp6Addr * pu4SrcAddr,
                            tIp6Addr * pu4DestAddr, UINT2 u2BufLen,
                            UINT4 u4IfIndex)
{
    UINT1              *pu1Data = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpv6TransmitVxlanPkt: Entry\n"));

    pu1Data = MemAllocMemBlk (VXLAN_UDP_SEND_BUF_POOLID);

    if (pu1Data != NULL)
    {
        /*
         * A linear buffer is expected to be passed while writing to
         *  socket. So copying the contents of pBuf to a linear buffer.
         */

        if ((CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u2BufLen) ==
             u2BufLen))
        {
            if (Vxlanv6SendMessage (pu1Data, u2BufLen, pu4SrcAddr, pu4DestAddr,
                                    u4IfIndex) == VXLAN_FAILURE)
            {
                MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID,
                                    (UINT1 *) pu1Data);
                VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                 "IPV6 UDP packet send failed\n"));
                return VXLAN_FAILURE;
            }
            MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID, (UINT1 *) pu1Data);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "IPV6 UDP packet successfully\n"));
            return VXLAN_SUCCESS;
        }
        MemReleaseMemBlock (VXLAN_UDP_SEND_BUF_POOLID, (UINT1 *) pu1Data);
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "IPV6 UDP packet send failed\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "IPV6 UDP packet send failed\n"));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpv6TransmitVxlanPkt: Exit\n"));

    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name : Vxlanv6SendMessage                                       */
/* Description   : Function to set IP_INFO for UDP Send                      */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 pSrcAddr - Source Address                                 */
/*                 pDestAddr - Destination Address                           */
/* Output(s)     : None                                                      */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/
INT4
Vxlanv6SendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                    tIp6Addr * pSrcAddr, tIp6Addr * pDestAddr, UINT4 u4IfIndex)
{
    struct msghdr       PktInfo;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[VXLAN_MSG_LENGTH];
    struct sockaddr_in6 DestInfo;
    INT4                i4UdpSockFd = gVxlanGlobals.i4Vxlanv6TxSockId;
    INT4                i4TtlVal = 0;

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));

#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    Iov.iov_base = pu1Data;
    Iov.iov_len = u2BufLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufLen);
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:Vxlanv6SendMessage: Entry\n"));

    i4TtlVal = VXLAN_IP_TTL;

    if (u4IfIndex != 0)
    {
        if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                         (void *) &u4IfIndex, sizeof (struct in6_addr))) < 0)
        {
            VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                             "setsockopt failed for Multicast IP\n"));
            return VXLAN_FAILURE;
        }
        if (setsockopt (i4UdpSockFd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        &i4TtlVal, sizeof (INT4)) < 0)
        {
            return OSIX_FAILURE;
        }
    }

    else if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IP_TTL,
                          &i4TtlVal, sizeof (INT4))) < 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_CRT_TRC, "setsockopt failed for  IP TTL\n"));
        return VXLAN_FAILURE;
    }

    MEMSET ((INT1 *) &DestInfo, 0, sizeof (struct sockaddr_in6));
    DestInfo.sin6_family = AF_INET6;
    MEMCPY (DestInfo.sin6_addr.s6_addr, pDestAddr,
            sizeof (DestInfo.sin6_addr.s6_addr));
    DestInfo.sin6_port =
        OSIX_HTONS (gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort);

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (VOID *) &DestInfo;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
#ifndef LNXIP6_WANTED
    pCmsgInfo->cmsg_level = SOL_IPV6;
#else
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
#endif
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    if (u4IfIndex != 0)
    {
#ifdef BSDCOMP_SLI_WANTED
        pIpPktInfo->ipi6_ifindex = u4IfIndex;
#else
        pIpPktInfo->ipi6_ifindex = (INT4) u4IfIndex;
#endif
    }
    else
    {
        pIpPktInfo->ipi6_ifindex = VXLAN_IP_INVALID_INDEX;
    }

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIpPktInfo->ipi6_addr.s6_addr),
                 pSrcAddr);

    if (sendmsg (i4UdpSockFd, &PktInfo, 0) < 0)
    {
        return VXLAN_FAILURE;
    }
#ifndef LNXIP6_WANTED
    if (sendto (i4UdpSockFd, (INT1 *) pu1Data, (UINT4) u2BufLen, 0,
                (struct sockaddr *) &DestInfo,
                sizeof (struct sockaddr_in6)) < 0)
    {

        return VXLAN_FAILURE;
    }
#endif
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:Vxlanv6SendMessage: Exit\n"));

    return VXLAN_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanUdpv6ProcessPktOnSocket                                   *
 *                                                                           *
 * Description  : This routine reads the data present in the given socket    *
 *                and post the packet into Vxlan queue for processing          *
 *                                                                           *
 * Input        : i4SockId - Socket Id on which packet is received           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
VxlanUdpv6ProcessPktOnSocket (INT4 i4UdpSockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Data = NULL;
    INT4                i4BytesRcvd = 0;
    tIp6Addr            PeerAddr;
    struct sockaddr_in6 PeerInfo;
    struct msghdr       PktInfo;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[VXLAN_MSG_LENGTH];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
    UINT4               u4PeerLen = sizeof (PeerInfo);
#endif
#ifdef LNXIP6_WANTED
    UINT1               CmsgInfo[112];
#endif

#ifdef LNXIP6_WANTED
    MEMSET (&CmsgInfo, 0, sizeof (CmsgInfo));
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpv6ProcessPktOnSocket: Entry\n"));

    MEMSET (&PeerAddr, 0, sizeof (tIp6Addr));
    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in6));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    pu1Data = MemAllocMemBlk (VXLAN_UDP_RECV_BUF_POOLID);
    if (pu1Data != NULL)
    {
#ifdef BSDCOMP_SLI_WANTED
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
        MEMSET (&Iov, 0, sizeof (struct iovec));

        PktInfo.msg_name = (VOID *) &PeerInfo;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
        Iov.iov_base = pu1Data;
        Iov.iov_len = VXLAN_JUMBO_MTU_SIZE;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
#ifndef LNXIP6_WANTED
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
        pCmsgInfo->cmsg_level = SOL_IP;
        pCmsgInfo->cmsg_type = IP_PKTINFO;
        pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
#else
        PktInfo.msg_control = (VOID *) &CmsgInfo;
        PktInfo.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
        pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
        pCmsgInfo->cmsg_level = IPPROTO_IPV6;
        pCmsgInfo->cmsg_type = IPV6_PKTINFO;
        pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));
#endif
        while ((i4BytesRcvd = recvmsg (i4UdpSockFd, &PktInfo, 0)) > 0)
        {
            pBuf = CRU_BUF_Allocate_MsgBufChain (VXLAN_JUMBO_MTU_SIZE, 0);
            if (pBuf != NULL)
            {
                if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Data, 0,
                                               (UINT4) i4BytesRcvd)
                    == CRU_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "Unable to copy read data to Buffer\n"));
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                MEMCPY (&PeerAddr, &(PeerInfo.sin6_addr.s6_addr),
                        sizeof (PeerAddr));

                VxlanProcessUdpMsg (pBuf, VXLAN_IPV6_UNICAST,
                                    (tIpAddr *) & PeerAddr);
            }
            MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
            PktInfo.msg_name = (VOID *) &PeerInfo;
            PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
            Iov.iov_base = pu1Data;
            Iov.iov_len = VXLAN_JUMBO_MTU_SIZE;
            PktInfo.msg_iov = &Iov;
            PktInfo.msg_iovlen = 1;
#ifndef LNXIP6_WANTED
            PktInfo.msg_control = (VOID *) au1Cmsg;
            PktInfo.msg_controllen = sizeof (au1Cmsg);

            pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
            pCmsgInfo->cmsg_level = SOL_IP;
            pCmsgInfo->cmsg_type = IP_PKTINFO;
            pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
#else
            PktInfo.msg_control = (VOID *) &CmsgInfo;
            PktInfo.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
            pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
            pCmsgInfo->cmsg_level = IPPROTO_IPV6;
            pCmsgInfo->cmsg_type = IPV6_PKTINFO;
            pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));
#endif
        }                        /* while end */
#else
        MEMSET ((INT1 *) &CmsgInfo, 0, sizeof (struct cmsghdr));
        PktInfo.msg_control = (VOID *) &CmsgInfo;
        while ((i4BytesRcvd =
                recvfrom (i4UdpSockFd, (INT1 *) pu1Data, VXLAN_JUMBO_MTU_SIZE,
                          0, (struct sockaddr *) &PeerInfo, &u4PeerLen)) > 0)
        {
            if (recvmsg (i4UdpSockFd, &PktInfo, 0) < 0)
            {
                continue;
            }

            pBuf = CRU_BUF_Allocate_MsgBufChain (VXLAN_JUMBO_MTU_SIZE, 0);
            if (pBuf != NULL)

            {
                if (CRU_BUF_Copy_OverBufChain
                    (pBuf, pu1Data, 0, (UINT4) i4BytesRcvd) == CRU_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_CRT_TRC,
                                     "Unable to copy read data to Buffer\n"));

                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                MEMCPY (&PeerAddr, &(PeerInfo.sin6_addr.s6_addr),
                        sizeof (PeerAddr));
                VxlanProcessUdpMsg (pBuf, VXLAN_IPV6_UNICAST,
                                    (tIpAddr *) & PeerAddr);
            }
        }
#endif
        MemReleaseMemBlock (VXLAN_UDP_RECV_BUF_POOLID, (UINT1 *) pu1Data);
    }
    else
    {
        VXLAN_TRC_FUNC ((VXLAN_MEMORY_TRC,
                         "Memory Allocation for Receiving UDP Buffer\n"));
    }

    if (SelAddFd (i4UdpSockFd, Vxlanv6PktRcvdOnSocket) == OSIX_FAILURE)
    {
        close (i4UdpSockFd);
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUdpv6ProcessPktOnSocket: Exit\n"));

    return VXLAN_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : VxlanPktRcvdOnSocket                                         *
 *                                                                           *
 * Description  : This routine should be invoked when packet is received on  *
 *                the socket. This message posts an event with the socket    *
 *                identifier to the Vxlan Task.                                *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
Vxlanv6PktRcvdOnSocket (INT4 i4SockId)
{
    tVxlanIfMsg         VxlanIfMsg;

    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:Vxlanv6PktRcvdOnSocket: Entry\n"));

    VxlanIfMsg.u4MsgType = VXLAN_UDP_IPV6_EVENT;
    VxlanIfMsg.uVxlanMsg.i4SockId = i4SockId;

    VxlanEnqueMsg (&VxlanIfMsg);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:Vxlanv6PktRcvdOnSocket: Exit\n"));

    return;
}
#endif
#endif /* _VxlanUDP_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  Vxlanudp.c                       */
/*-----------------------------------------------------------------------*/
