/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: vxmbsm.c,v 1.2 2018/01/05 15:26:48 siva Exp $
 *
 * Description: This file contains the VXLAN NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#include "vxinc.h"
#include "vxlannp.h"
#ifdef MBSM_WANTED
#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : VxlanMbsmCardInsertRemove                            */
/*                                                                           */
/* Description        : This function is called during MBSM (card insert     */
/*                      or remove.                                           */
/*                                                                           */
/* Input(s)           : pSlotInfo - contains slot information of the card    */
/*                      which is getting inserted/removed.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
VxlanMbsmCardInsertRemove (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    INT4                i4RetVal = VXLAN_INIT_VAL;

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        if (gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable == VXLAN_ENABLED)
        {
            i4RetVal = VxlanMbsmHwInit (pSlotInfo, u4Flag);

            if (VXLAN_FAILURE == i4RetVal)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC, "VxlanMbsmHwInit returns failure :"
                            "Slot %d \r\n", pSlotInfo->i4SlotId));
            }
        }

        i4RetVal =
            VxlanMbsmHwUpdateUdpPort (gVxlanGlobals.VxlanGlbMib.
                                      u4FsVxlanUdpPort, pSlotInfo, u4Flag);
        if (VXLAN_FAILURE == i4RetVal)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "VxlanMbsmHwUpdateUdpPort returns failure :"
                        "Slot %d \r\n", pSlotInfo->i4SlotId));
        }
    }
    i4RetVal = VxlanHandleMbsmVniVlanMapDB (pSlotInfo, u4Flag);
    if (VXLAN_FAILURE == i4RetVal)
    {
        VXLAN_TRC ((VXLAN_FAIL_TRC,
                    "VxlanHandleMbsmVniVlanMapDB returns failure :"
                    "Slot %d \r\n", pSlotInfo->i4SlotId));
    }

    if (u4Flag == MBSM_MSG_CARD_INSERT)
    {
        i4RetVal = VxlanHandleMbsmInReplicaDB (pSlotInfo, u4Flag);
        if (VXLAN_FAILURE == i4RetVal)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "VxlanHandleMbsmInReplicaDB returns failure :"
                        "Slot %d \r\n", pSlotInfo->i4SlotId));
        }
        i4RetVal = VxlanHandleMbsmNveDB (pSlotInfo, u4Flag);
        if (VXLAN_FAILURE == i4RetVal)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "VxlanHandleMbsmNveDB returns failure : "
                        "Slot %d \r\n", pSlotInfo->i4SlotId));
        }
    }
    else
    {
        VxlanHandleMbsmUpdateNveReplicaDB (pSlotInfo);
    }
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VxlanMbsmUpdateCardStatus                             */
/*                                                                           */
/* Description        : This function constructs the VXLAN Queue Msg and     */
/*                      sends the  VXLAN event to process this Mesg.         */
/*                                                                           */
/*                      This function will be called from the IP module      */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
VxlanMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tVxlanIfMsg         VxlanIfMsg;
    MEMSET (&VxlanIfMsg, 0, sizeof (tVxlanIfMsg));

    VxlanIfMsg.u4MsgType = (UINT4) i4Event;
    MEMCPY (&(VxlanIfMsg.uVxlanMsg.MbsmCardUpdate.mbsmProtoMsg), pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (VxlanEnqueMsg (&VxlanIfMsg) == VXLAN_FAILURE)
    {
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanEnqueMsg: Exit\n"));

    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHandleMbsmVniVlanMapDB
 *
 * Description               : This function sets VXLAN vni vlan map database in 
 *                             HW during MBSM. 
 * Input                     : pSlotInfo - contains slot information of the card 
 *                             which is getting inserted/removed. 
 *                             u4Flag - Card Insert/Remove flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/

INT4
VxlanHandleMbsmVniVlanMapDB (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlantEntry;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry;
    tVxlanVniVlanPortMacEntry *pVxlanVniVlanPortMacEntry;
    tVxlanVniVlanPortEntry VxlanVniVlanGetPortEntry;
    INT4                i4PortSlotId = VXLAN_INIT_VAL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateVniVlanMapDatabase: Entry\n"));

    MEMSET (&VxlanVniVlanGetPortEntry, 0, sizeof (tVxlanVniVlanPortEntry));

    pVxlanVniVlantEntry =
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    while (NULL != pVxlanVniVlantEntry)
    {
        MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));

        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VniNumber =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4VlanId = (UINT4)
            pVxlanVniVlantEntry->MibObject.i4FsVxlanVniVlanMapVlanId;

        /* Create access port for member ports of VLAN */

        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktSent =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktSent;

        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktRcvd =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd;

        VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4PktDrpd =
            pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd;

        if (u4Flag == MBSM_MSG_CARD_INSERT)
        {
            if (VxlanMbsmVniVlanMapDatabase
                (&(VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo), pSlotInfo,
                 VXLAN_HW_CONFIGURE) == VXLAN_FAILURE)
            {
                VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                 "VxlanMbsmVniVlanMapDatabase returns failure"));
                return VXLAN_FAILURE;
            }
        }

        pVxlanVniVlanPortEntry = (tVxlanVniVlanPortEntry *) RBTreeGetFirst
            (pVxlanVniVlantEntry->VxlanVniVlanPortTable);
        while (pVxlanVniVlanPortEntry != NULL)
        {
            VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4IfIndex =
                pVxlanVniVlanPortEntry->u4IfIndex;
            VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo.u4AccessPort =
                pVxlanVniVlanPortEntry->u4AccessPort;

            if (u4Flag == MBSM_MSG_CARD_INSERT)
            {
                if (VxlanMbsmVniVlanPortMapDatabase
                    (&(VxlanHwInfo.unHwParam.VxlanHwVniVlanInfo), pSlotInfo,
                     VXLAN_HW_CONFIGURE) == VXLAN_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                     "VxlanMbsmVniVlanPortMapDatabase Failed for port %d \n",
                                     pVxlanVniVlanPortEntry->u4IfIndex));
                    return VXLAN_FAILURE;
                }
            }
            else
            {
                /* Delete the VNI-VLAN map entry from hardware only 
                   when the slot id matches */
                if (MbsmGetSlotFromPort
                    (pVxlanVniVlanPortEntry->u4IfIndex,
                     &i4PortSlotId) == MBSM_FAILURE)
                {
                    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                     "MbsmGetSlotFromPort Failed for port %d \n",
                                     pVxlanVniVlanPortEntry->u4IfIndex));
                    return VXLAN_FAILURE;
                }
                if (i4PortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))
                {
                    VXLAN_TRC_FUNC ((VXLAN_ALL_TRC, "Slot id matches - "
                                     "deleting VNI VLAN entry for port %d \r\n",
                                     pVxlanVniVlanPortEntry->u4IfIndex));

                    VxlanDelPortFromL2Table (pVxlanVniVlanPortEntry->u4IfIndex);
                }
            }
            pVxlanVniVlanPortEntry =
                RBTreeGetNext (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                               (tRBElem *) pVxlanVniVlanPortEntry, NULL);
        }

        /* Program HW L2 table with the MAC address learnt on access port */
        pVxlanVniVlanPortMacEntry =
            (tVxlanVniVlanPortMacEntry *) RBTreeGetFirst (pVxlanVniVlantEntry->
                                                          VxlanVniVlanPortMacTable);
        while (pVxlanVniVlanPortMacEntry != NULL)
        {
            MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac,
                    pVxlanVniVlanPortMacEntry->MacAddress, sizeof (tMacAddr));

            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
                pVxlanVniVlantEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType = 0;    /* for dynamic */

            /* Retrieve Access port of Interface */
            VxlanVniVlanGetPortEntry.u4IfIndex =
                pVxlanVniVlanPortMacEntry->u4IfIndex;

            pVxlanVniVlanPortEntry =
                RBTreeGet (pVxlanVniVlantEntry->VxlanVniVlanPortTable,
                           (tRBElem *) & VxlanVniVlanGetPortEntry);

            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
                pVxlanVniVlanPortEntry->u4AccessPort;

            if (VxlanMbsmHwUpdateL2Table
                (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo), pSlotInfo,
                 VXLAN_HW_CONFIGURE) == VXLAN_FAILURE)
            {
                VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                                 "VxlanMbsmHwUpdateL2Table FAILURE for access port %d \n",
                                 pVxlanVniVlanPortEntry->u4AccessPort));
                return VXLAN_FAILURE;
            }
            pVxlanVniVlanPortMacEntry =
                RBTreeGetNext (pVxlanVniVlantEntry->VxlanVniVlanPortMacTable,
                               (tRBElem *) pVxlanVniVlanPortMacEntry, NULL);
        }
        pVxlanVniVlantEntry =
            (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetNext (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsVxlanVniVlanMapTable,
                                                            (tRBElem *)
                                                            pVxlanVniVlantEntry,
                                                            NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHwUpdateVniVlanMapDatabase: Exit\n"));
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHandleMbsmNveDB
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanNveEntry - VXLAN nve databse.
 *                             u4Flag - Used for card insert / remove
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
INT4
VxlanHandleMbsmNveDB (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT4               u4IfaceIndex = 0;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleMbsmNveDB: Entry\n"));
    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));

    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);

    while (NULL != pVxlanNveEntry)
    {
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
            pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;

        if (pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex >
            CFA_MAX_NVE_IF_INDEX)
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4NveIfIndex =
                (INT4) pVxlanNveEntry->u4OrgNveIfIndex;
        }

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VniNumber =
            pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanId =
            (UINT4) pVxlanNveEntry->i4VlanId;

        if ((pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
             VXLAN_STRG_TYPE_VOL)
            && (pVxlanNveEntry->i4FsVxlanNveEvpnMacType != VXLAN_NVE_EVPN_MAC))
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType =
                VXLAN_NVE_DYNAMIC_MAC;
        }
        else
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType =
                VXLAN_NVE_STATIC_MAC;
        }

        if ((pVxlanNveEntry->bIngOrMcastZeroEntryPresent == TRUE) &&
            (pVxlanNveEntry->i4FsVxlanNveEvpnMacType == VXLAN_NVE_EVPN_MAC) &&
            (pVxlanNveEntry->MibObject.i4FsVxlanNveStorageType ==
             VXLAN_STRG_TYPE_VOL))
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u1MacType =
                VXLAN_NVE_DYNAMIC_MAC;
        }

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4VtepAddressType =
            pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressType;

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.i4RemoteVtepAddressType =
            pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1VtepAddress),
                &(pVxlanNveEntry->MibObject.au1FsVxlanNveVtepAddress),
                pVxlanNveEntry->MibObject.i4FsVxlanNveVtepAddressLen);

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1RemoteVtepAddress),
                &(pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress),
                pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo.DestVmMac),
                &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                VXLAN_ETHERNET_ADDR_SIZE);

        if (pVxlanNveEntry->u4VxlanTunnelIfIndex != CFA_INVALID_INDEX)
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4VlanIdOutTunnel =
                pVxlanNveEntry->u4OutVlanId;
            NetIpv4GetCfaIfIndexFromPort (pVxlanNveEntry->u4VxlanTunnelIfIndex,
                                          &u4IfaceIndex);
        }

        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex = u4IfaceIndex;

        /* Retrieve network port for the respective remote VTEP */
        MEMCPY (&(VxlanNvePortEntry.au1RemoteVtepAddress),
                &(pVxlanNveEntry->MibObject.au1FsVxlanNveRemoteVtepAddress),
                pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen);

        VxlanNvePortEntry.i4RemoteVtepAddressLen =
            pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressLen;

        pVxlanNvePortEntry = (tVxlanNvePortEntry *) RBTreeGet
            (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
             (tRBElem *) & VxlanNvePortEntry);

        if (pVxlanNvePortEntry != NULL)
        {
            VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4NetworkPort =
                pVxlanNvePortEntry->u4NetworkPort;
            MEMCPY (VxlanHwInfo.unHwParam.VxlanHwNveInfo.au1NextHopMac,
                    pVxlanNvePortEntry->au1NextHopMac, MAC_ADDR_LEN);

            if (pVxlanNvePortEntry->i4FirstCreate == VXLAN_FALSE)
            {
                if (VxlanMbsmHwUpdateNveDatabase
                    (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo), pSlotInfo,
                     u4Flag) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC, "VxlanMbsmHwUpdateNveDatabase: \
                                    Failed to create network ports in h/w "));
                    return VXLAN_FAILURE;
                }
                if (VxlanMbsmHwUpdateL2Table
                    (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo), pSlotInfo,
                     u4Flag) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC, "VxlanMbsmHwUpdateL2Table: \
                                    Failed to create network ports in h/w "));
                    return VXLAN_FAILURE;
                }
                pVxlanNvePortEntry->i4FirstCreate = VXLAN_TRUE;
            }
            else
            {
                if (VxlanMbsmHwUpdateL2Table
                    (&(VxlanHwInfo.unHwParam.VxlanHwNveInfo), pSlotInfo,
                     u4Flag) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC, "VxlanMbsmHwUpdateL2Table: \
                                    Failed to create network ports in h/w "));
                    return VXLAN_FAILURE;
                }
            }

        }

        VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                         "FUNC:VxlanHandleMbsmNveDB: Exit\n"));
        pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanNveEntry, NULL);
    }
    /* loop NvePortEntry and reset i4FirstCreate since it should be 
     * true only during first time */
    pVxlanNvePortEntry = (tVxlanNvePortEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable);

    while (pVxlanNvePortEntry != NULL)
    {
        pVxlanNvePortEntry->i4FirstCreate = VXLAN_FALSE;
        pVxlanNvePortEntry =
            (tVxlanNvePortEntry *) RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                                  VxlanNvePortTable,
                                                  (tRBElem *)
                                                  pVxlanNvePortEntry, NULL);
    }
    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHandleMbsmUpdateNveReplicaDB
 *
 * Description               : This function sets VXLAN nve databse in H/W
 *
 * Input                     : pVxlanNveEntry - VXLAN nve databse.
 *                             u4Flag - Used for card insert / remove
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
INT4
VxlanHandleMbsmUpdateNveReplicaDB (tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4IfaceIndex = 0;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    INT4                i4PortSlotId = VXLAN_INIT_VAL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleMbsmNveDB: Entry\n"));

    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);

    while (NULL != pVxlanNveEntry)
    {
        NetIpv4GetCfaIfIndexFromPort (pVxlanNveEntry->u4VxlanTunnelIfIndex,
                                      &u4IfaceIndex);

        /* Delete the NVE entry from hardware only when the slot id matches */
        if (MbsmGetSlotFromPort (u4IfaceIndex, &i4PortSlotId) == MBSM_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC, "VxlanMbsmHwUpdateNveDatabase : "
                        "MbsmGetSlotFromPort Failed \n"));
            return VXLAN_FAILURE;
        }

        if (i4PortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))
        {
            if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
                VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "Failed to del Nve entry from h/w\r\n"));
            }
        }

        VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                         "FUNC:VxlanHandleMbsmNveDB: Exit\n"));
        pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanNveEntry, NULL);
    }
    /* Ingress replica */
    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);

    while (NULL != pVxlanInReplicaEntry)
    {
        NetIpv4GetCfaIfIndexFromPort (pVxlanInReplicaEntry->
                                      u4VxlanTunnelIfIndex, &u4IfaceIndex);
        /* Delete the ingress replica entry from hardware only when the slot id matches */
        if (MbsmGetSlotFromPort (u4IfaceIndex, &i4PortSlotId) == MBSM_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "MbsmGetSlotFromPort Failed for ifIndex %d\n",
                        u4IfaceIndex));
            return VXLAN_FAILURE;
        }
        if ((i4PortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)) ||
            (pVxlanInReplicaEntry->bRouteChanged == VXLAN_TRUE))
        {
            if (VxlanHwUpdateInReplicaDatabase
                (pVxlanInReplicaEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_UTIL_TRC,
                            "Failed to del Ingress Replica entry from h/w\r\n"));
            }
            pVxlanInReplicaEntry->bRouteChanged = VXLAN_FALSE;
        }
        pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) pVxlanInReplicaEntry, NULL);
    }

    /* Recreating Replica DB and then NVE DB */
    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);

    while (NULL != pVxlanInReplicaEntry)
    {
        if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry, VXLAN_HW_ADD)
            == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to del Ingress Replica entry from h/w\r\n"));
        }
        pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) pVxlanInReplicaEntry, NULL);
    }

    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
    while (NULL != pVxlanNveEntry)
    {
        if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_ADD) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to del Nve entry from h/w\r\n"));
        }

        pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
             (tRBElem *) pVxlanNveEntry, NULL);
    }

    return VXLAN_SUCCESS;
}

/***************************************************************************
 * Function Name             : VxlanHandleMbsmInReplicaDB
 *
 * Description               : This function sets VXLAN Ingress replica 
 *                             nve databse in H/W
 *
 * Input                     : pVxlanInReplicaEntry - VXLAN nve databse.
 *                             u4Flag - Add/Del flag
 *
 * Output                    : None
 *
 * Returns                   : VXLAN_SUCCESS or VXLAN_FAILURE
 *
 **************************************************************************/
UINT1
VxlanHandleMbsmInReplicaDB (tMbsmSlotInfo * pSlotInfo, UINT4 u4Flag)
{
    tVxlanHwInfo        VxlanHwInfo;
    UINT1               au1RemoteVTEPAddr[VXLAN_HW_REPLICA_LIST *
                                          (sizeof (UINT4))];
    UINT4               u4IfaceIndex = 0;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleMbsmInReplicaDB: Entry\n"));

    MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
    MEMSET (au1RemoteVTEPAddr, 0, sizeof (au1RemoteVTEPAddr));
    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));

    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);

    while (NULL != pVxlanInReplicaEntry)
    {
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4NveIfIndex =
            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex;

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VniNumber =
            pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber;

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanId =
            (UINT4) pVxlanInReplicaEntry->i4VlanId;

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4VtepAddressType =
            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressType;

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.i4RemoteVtepAddressType =
            pVxlanInReplicaEntry->MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType;

        MEMCPY (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1VtepAddress),
                &(pVxlanInReplicaEntry->MibObject.
                  au1FsVxlanInReplicaVtepAddress),
                pVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaVtepAddressLen);
        MEMCPY (au1RemoteVTEPAddr,
                &(pVxlanInReplicaEntry->MibObject.
                  au1FsVxlanInReplicaRemoteVtepAddress),
                (pVxlanInReplicaEntry->MibObject.
                 i4FsVxlanInReplicaRemoteVtepAddressLen));

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.pau1RemoteVtepsReplicaTo =
            au1RemoteVTEPAddr;
        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NoRemoteVtepsReplicaTo =
            1;

        MEMCPY (&
                (VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.
                 au1RemoteVtepAddress),
                &(pVxlanInReplicaEntry->MibObject.
                  au1FsVxlanInReplicaRemoteVtepAddress),
                (pVxlanInReplicaEntry->MibObject.
                 i4FsVxlanInReplicaRemoteVtepAddressLen));

        VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4VlanIdOutTunnel =
            pVxlanInReplicaEntry->u4OutVlanId;
        NetIpv4GetCfaIfIndexFromPort (pVxlanInReplicaEntry->
                                      u4VxlanTunnelIfIndex, &u4IfaceIndex);
        VxlanHwInfo.unHwParam.VxlanHwNveInfo.u4IfIndex = u4IfaceIndex;

        /* Retrieve network port for the respective remote VTEP */
        MEMCPY (&(VxlanNvePortEntry.au1RemoteVtepAddress),
                &(pVxlanInReplicaEntry->MibObject.
                  au1FsVxlanInReplicaRemoteVtepAddress),
                pVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaVtepAddressLen);

        VxlanNvePortEntry.i4RemoteVtepAddressLen =
            pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaVtepAddressLen;

        pVxlanNvePortEntry = (tVxlanNvePortEntry *) RBTreeGet
            (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
             (tRBElem *) & VxlanNvePortEntry);

        if (pVxlanNvePortEntry != NULL)
        {
            VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkBUMPort =
                pVxlanNvePortEntry->u4NetworkBUMPort;
            VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.u4NetworkPort =
                pVxlanNvePortEntry->u4NetworkPort;
            MEMCPY (VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo.au1NextHopMac,
                    pVxlanNvePortEntry->au1NextHopMac, MAC_ADDR_LEN);

            if (u4Flag == MBSM_MSG_CARD_INSERT)
            {
                if (VxlanMbsmHwUpdateInReplicaDatabase
                    (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo), pSlotInfo,
                     u4Flag) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "VxlanMbsmHwUpdateInReplicaDatabase failure for "
                                "ifIndex %d \r\n", u4IfaceIndex));
                    return VXLAN_FAILURE;
                }

                if (VxlanMbsmHwUpdateReplicaMCast
                    (&(VxlanHwInfo.unHwParam.VxlanHwBUMReplicaInfo), pSlotInfo,
                     u4Flag) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_FAIL_TRC,
                                "VxlanMbsmHwUpdateInReplicaDatabase failure for "
                                "ifIndex %d \r\n", u4IfaceIndex));
                    return VXLAN_FAILURE;
                }
            }

        }
        pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
             (tRBElem *) pVxlanInReplicaEntry, NULL);
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanHandleMbsmInReplicaDB: Exit\n"));
    return VXLAN_SUCCESS;
}
#endif
#endif
