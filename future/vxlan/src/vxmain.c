/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vxmain.c,v 1.15 2018/01/05 09:57:11 siva Exp $
*
* Description: This file contains the vxlan task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __VXLANMAIN_C__
#include "vxinc.h"

/* Proto types of the functions private to this file only */

PRIVATE UINT4 VxlanMainMemInit PROTO ((VOID));
PRIVATE VOID VxlanMainMemClear PROTO ((VOID));
PRIVATE VOID VxlanDeleteRBTrees PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : VxlanMainTask                                               *
*                                                                           *
* Description  : Main function of VXLAN.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
VxlanMainTask ()
{
    UINT4               u4Event = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanTaskMain: Entry\n"));

    while (OSIX_TRUE == OSIX_TRUE)
    {

        if (OsixEvtRecv
            (gVxlanGlobals.vxlanTaskId,
             (VXLAN_TIMER_EVENT | VXLAN_QUEUE_EVENT), OSIX_WAIT,
             &u4Event) != OSIX_SUCCESS)
        {
            continue;
        }

        VxlanMainTaskLock ();

        VXLAN_TRC ((VXLAN_MAIN_TRC, "Rx Event %d\n", u4Event));

        if ((u4Event & VXLAN_QUEUE_EVENT) == VXLAN_QUEUE_EVENT)
        {
            VXLAN_TRC ((VXLAN_MAIN_TRC, "VXLAN_QUEUE_EVENT\n"));

            VxlanQueProcessMsgs ();
        }
        if ((u4Event & VXLAN_TIMER_EVENT) == VXLAN_TIMER_EVENT)
        {
            VXLAN_TRC ((VXLAN_MAIN_TRC, "VXLAN_TIMER_EVENT\n"));

            VxlanProcessTimerExpiry ();
        }

        /* Mutual exclusion flag OFF */
        VxlanMainTaskUnLock ();
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanTaskMain: Exit\n"));
}

/****************************************************************************
*                                                                           *
* Function     : VxlanMainTaskInit                                           *
*                                                                           *
* Description  : VXLAN initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
VxlanMainTaskInit (VOID)
{

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanMainTaskInit: Entry\n"));

    VXLAN_TRC_FLAG = 0;
    tVlanRegTbl         VlanRegTbl;
#ifdef EVPN_VXLAN_WANTED
    tEvpnBgpRegEntry    EvpnBgpRegEntry;
    tBgp4RegisterASNInfo BgpEvpnRegisterInfo;
    tArpRegTbl          ArpRegTbl;

    MEMSET (&EvpnBgpRegEntry, EVPN_ZERO, sizeof (tEvpnBgpRegEntry));
    MEMCPY (&EvpnBgpRegEntry, &gVxlanGlobals.EvpnBgpRegEntry,
            sizeof (tEvpnBgpRegEntry));
    MEMSET (&gVxlanGlobals, 0, sizeof (gVxlanGlobals));
    MEMCPY (&gVxlanGlobals.EvpnBgpRegEntry, &EvpnBgpRegEntry,
            sizeof (tEvpnBgpRegEntry));
    MEMSET (&ArpRegTbl, EVPN_ZERO, sizeof (tArpRegTbl));
#else /* EVPN_VXLAN_WANTED */
    MEMSET (&gVxlanGlobals, 0, sizeof (gVxlanGlobals));
#endif
    MEMSET (&VlanRegTbl, 0, sizeof (tVlanRegTbl));
    MEMCPY (gVxlanGlobals.au1TaskSemName, VXLAN_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);
    if (OsixCreateSem
        (VXLAN_MUT_EXCL_SEM_NAME, VXLAN_SEM_CREATE_INIT_CNT, 0,
         &gVxlanGlobals.vxlanTaskSemId) == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC,
                    "Seamphore Creation failure for %s \n",
                    VXLAN_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    if (VxlanUtlCreateRBTree () == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "RBTree deletion failed\n"));
        return OSIX_FAILURE;
    }

    if (VxlanRedInitGlobalInfo () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */

    if (VxlanMainMemInit () == OSIX_FAILURE)
    {
        VxlanDeleteRBTrees ();
        VxlanRedDeInitGlobalInfo ();
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (VXLAN_QUEUE_NAME, VXLAN_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gVxlanGlobals.vxlanQueId)) == OSIX_FAILURE)
    {
        VxlanDeleteRBTrees ();
        VxlanRedDeInitGlobalInfo ();
        VxlanMainMemClear ();
        VXLAN_TRC ((VXLAN_MAIN_TRC, "IAPQ Creation Failed\n"));
        return OSIX_FAILURE;
    }
    if (TmrCreateTimerList ((const UINT1 *) VXLAN_TASK_NAME,
                            VXLAN_TIMER_EVENT,
                            NULL,
                            (tTimerListId *) & (gVxlanGlobals.
                                                VxlanTimerList)) != TMR_SUCCESS)
    {

        VXLAN_TRC ((VXLAN_MAIN_TRC, "Timer Creation Failed.\n"));
        return OSIX_FAILURE;
    }

    gVxlanGlobals.VxlanGlbMib.u4FsVxlanUdpPort = VXLAN_UDP_DEST_PORT;
    /* Default UDP port number is set as 4789 in Alta board. Due to this 
     * VXLAN multicast packet is not getting trapped to CPU. To fix that issue,
     * UDP port number is set as 999 (not used for VXLAN)
     * */
    if (VxlanHwUpdateUdpPort (999, VXLAN_HW_ADD) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Dummy UDP port number set failed.\n"));;
    }

    gVxlanGlobals.VxlanGlbMib.i4FsVxlanEnable = VXLAN_DEF_FSVXLANENABLE;
    VxlanInitReplicaIndexMgr ();
    VlanRegTbl.pVlanBasicInfo = VxlanEvpnRegisterVlanInfo;
    VlanRegTbl.u1ProtoId = VLAN_VXLAN_EVPN_PROTO_ID;

    if (VlanRegDeRegProtocol (VLAN_INDICATION_REGISTER,
                              &VlanRegTbl) == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "VxlanMainTaskInit: Vlan Registration Failed\r\n"));
        VxlanDeleteRBTrees ();
        VxlanRedDeInitGlobalInfo ();
        VxlanMainMemClear ();
        return OSIX_FAILURE;
    }
#ifdef EVPN_VXLAN_WANTED
    gVxlanGlobals.VxlanGlbMib.i4FsEvpnVxlanEnable = EVPN_DEF_FSEVPNENABLE;
    MEMSET (gVxlanGlobals.VxlanGlbMib.FsEvpnAnycastGwMac, 0,
            VXLAN_ETHERNET_ADDR_SIZE);
    EvpnVxlanInitRTIndexMgr ();
    /* Register ASN with BGP */
    MEMSET (&BgpEvpnRegisterInfo, 0, sizeof (tBgp4RegisterASNInfo));
    BgpEvpnRegisterInfo.pBgp4NotifyASN = EvpnBgp4NotifyASN;
    ArpRegTbl.pArpBasicInfo = VxlanEvpnRegisterArpInfo;
    ArpRegTbl.u1ProtoId = ARP_VXLAN_EVPN_PROTO_ID;
    if (ArpRegDeRegProtocol (ARP_INDICATION_REGISTER,
                             &ArpRegTbl) == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "VxlanMainTaskInit: Vlan Registration Failed\r\n"));
        VxlanDeleteRBTrees ();
        VxlanRedDeInitGlobalInfo ();
        VxlanMainMemClear ();
        VlanRegDeRegProtocol (VLAN_INDICATION_DEREGISTER, &VlanRegTbl);
        return OSIX_FAILURE;
    }

#endif
#ifdef EVPN_WANTED
    Bgp4EvpnASNRegister (&BgpEvpnRegisterInfo);
#endif

    VxlanRegisterWithIP ();

    VxlanUdpInit ();

    VxlanRedDynDataDescInit ();

    VxlanRedDescrTblInit ();

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanMainTaskInit: Exit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
VxlanMainDeInit (VOID)
{
#ifdef EVPN_VXLAN_WANTED
    tVlanRegTbl         VlanRegTbl;

    MEMSET (&VlanRegTbl, 0, sizeof (tVlanRegTbl));

    VlanRegTbl.u1ProtoId = VLAN_VXLAN_EVPN_PROTO_ID;
    VlanRegDeRegProtocol (VLAN_INDICATION_DEREGISTER, &VlanRegTbl);
#endif /* EVPN_VXLAN_WANTED */

    VxlanDeleteRBTrees ();

    VxlanRedDeInitGlobalInfo ();

    VxlanMainMemClear ();

    VxlanUdpDeInit ();
#ifdef EVPN_VXLAN_WANTED
    /* De Init RT Index */
    EvpnVxlanInitRTIndexMgr ();
#endif
    if (gVxlanGlobals.vxlanTaskSemId)
    {
        OsixSemDel (gVxlanGlobals.vxlanTaskSemId);
    }
    if (gVxlanGlobals.vxlanQueId)
    {
        OsixQueDel (gVxlanGlobals.vxlanQueId);
    }
    if ((gVxlanGlobals.VxlanTimerList != 0) &&
        (TmrDeleteTimerList ((tTimerListId) gVxlanGlobals.VxlanTimerList)
         != TMR_SUCCESS))
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Timer Deletion Failed.\n"));
    }
    gVxlanGlobals.VxlanTimerList = 0;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Vxlan Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
VxlanMainTaskLock (VOID)
{
    /* VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanMainTaskLock\n")); */

    if (OsixSemTake (gVxlanGlobals.vxlanTaskSemId) == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC,
                    "TakeSem failure for %s \n", VXLAN_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

/*    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "EXIT:VxlanMainTaskLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the VXLAN Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
VxlanMainTaskUnLock (VOID)
{
/*    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanMainTaskUnLock\n")); */

    OsixSemGive (gVxlanGlobals.vxlanTaskSemId);

/*    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "EXIT:VxlanMainTaskUnLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for VXLAN         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
VxlanMainMemInit (VOID)
{
    if (VxlanSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
VxlanMainMemClear (VOID)
{
    VxlanSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanUdpInit                                               */
/*                                                                           */
/* Description  : Creating the UDP sockets                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
VxlanUdpInit (VOID)
{
    /* Create the UDP socket */
    if (VxlanUdpInitSock () != OSIX_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Socket Initialization Failed\n"));
        return;
    }

    if (VxlanUdpAddOrRemoveFd (OSIX_TRUE) != OSIX_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Socket creation Failed\n"));
        return;
    }
#ifdef IP6_WANTED
    if (VxlanUdpv6AddOrRemoveFd (OSIX_TRUE) != OSIX_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, "Socket creation Failed\n"));
        return;
    }
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanUdpDeInit                                             */
/*                                                                           */
/* Description  : Deleting the UDP sockets                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
VxlanUdpDeInit (VOID)
{
    if (gVxlanGlobals.i4VxlanSockId > 0)
    {
        VxlanUdpDeInitParams (gVxlanGlobals.i4VxlanSockId);
        gVxlanGlobals.i4VxlanSockId = -1;
    }
    if (gVxlanGlobals.i4VxlanTxSockId > 0)
    {
        /* Close the socket identifier. */
        VxlanUdpDeInitParams (gVxlanGlobals.i4VxlanTxSockId);
        gVxlanGlobals.i4VxlanTxSockId = -1;
    }
#ifdef IP6_WANTED
    if (gVxlanGlobals.i4Vxlanv6SockId > 0)
    {
        VxlanUdpDeInitParams (gVxlanGlobals.i4Vxlanv6SockId);
        gVxlanGlobals.i4Vxlanv6SockId = -1;
    }
    if (gVxlanGlobals.i4Vxlanv6TxSockId > 0)
    {
        /* Close the socket identifier. */
        VxlanUdpDeInitParams (gVxlanGlobals.i4Vxlanv6TxSockId);
        gVxlanGlobals.i4Vxlanv6TxSockId = -1;
    }
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanDeleteRBTrees                                         */
/*                                                                           */
/* Description  : Remove the RBTree                                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
VxlanDeleteRBTrees (VOID)
{
    VxlanDeleteRBTreeEntries ();

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable = NULL;
#ifdef EVPN_VXLAN_WANTED
    EvpnDeleteRBTreeEntries ();
    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable, NULL,
                   0);
    gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable, NULL, 0);
    gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable = NULL;

    RBTreeDestroy (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable, NULL, 0);
    gVxlanGlobals.VxlanEvpnArpSupLocalMacTable = NULL;

#endif /* EVPN_VXLAN_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanProcessTimerExpiry                                    */
/*                                                                           */
/* Description  : Process the timer                                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
VxlanProcessTimerExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTmr = NULL;
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4VxlanTmrId = 0;

    while ((pExpiredTmr = TmrGetNextExpiredTimer (gVxlanGlobals.VxlanTimerList))
           != NULL)
    {
        pTmrParam = (tTimerParam *) (pExpiredTmr->u4Data);
        u4VxlanTmrId = pTmrParam->u1Id;
        switch (u4VxlanTmrId)
        {
            case VXLAN_TIMER_ROUTE:
                if (VxlanHwUpdateNveDatabase
                    (pTmrParam->u.pVxlanNveEntry,
                     VXLAN_HW_ADD) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_MAIN_TRC,
                                "Failed to Add Nve entry in h/w\n"));
                }
                break;
#ifdef EVPN_VXLAN_WANTED
            case EVPN_TIMER_ROUTE:
                if (EvpnUtilADUpdateToBgp
                    (pTmrParam->u.pEvpnMultihomedPeerTable->MibObject.
                     au1FsEvpnVxlanMHEviVniESI) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC, "DF Election Failed\n"));
                }
                break;
#endif /* EVPN_VXLAN_WANTED */
            default:
                break;
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name : VxlanRegisterWithIP                                       */
/* Description   : Function for registering with IP.                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILUR                                */
/*****************************************************************************/
VOID
VxlanRegisterWithIP ()
{
    tNetIpRegInfo       RegInfo;

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u1ProtoId = VXLAN_ID;

    /* Ask for Interface Status Change and Route Change Events */
    RegInfo.u2InfoMask = NETIPV4_ROUTECHG_REQ;
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.pRtChng = VxlanIpRtChgEventHandler;

    if (NetIpv4RegisterHigherLayerProtocol (&RegInfo) == NETIPV4_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:NetIpv4RegisterHigherLayerProtocol returns failure\n"));
    }
    return;
}

/* ************************************************************** *
 *Function   :  VxlanInitReplicaIndexMgr                          *
 * Description: This function initialize the RT Index             *
 * Input      : NONE                                              *
 * Output     : NONE                                              *
 * Returns    : NONE                                              *
 **************************************************************** */
VOID
VxlanInitReplicaIndexMgr ()
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanInitRTIndexMgr Entry\n"));
    UINT4               u4ContextId = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanInitRTIndexMgr Entry\n"));
    for (u4ContextId = 0; u4ContextId < MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE;
         u4ContextId++)
    {
        gVxlanGlobals.gau4VxlanReplicaBitMapIdx[u4ContextId][0] = 1;
    }

    MEMSET (gVxlanGlobals.gau4VxlanReplicaIndex, 0,
            sizeof (gVxlanGlobals.gau4VxlanReplicaIndex));
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanInitRTIndexMgr Exit\n"));
}

#ifdef EVPN_VXLAN_WANTED
/* ************************************************************** *
 * Function   : EvpnVxlanInitRTIndexMgr                           *
 * Description: This function initialize the RT Index             *
 * Input      : NONE                                              *
 * Output     : NONE                                              *
 * Returns    : NONE                                              *
 * ************************************************************** */
VOID
EvpnVxlanInitRTIndexMgr ()
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanInitRTIndexMgr Entry\n"));
    UINT4               u4ContextId = 0;

    /* L2VNI RT Inderx Initialization */
    for (u4ContextId = 0; u4ContextId < MAX_VXLAN_EVPN_VNI; u4ContextId++)
    {
        gVxlanGlobals.gau4EvpnRtBitMapIdx[u4ContextId][0] = 1;
    }

    MEMSET (gVxlanGlobals.gau4EvpnRtIndex, 0,
            sizeof (gVxlanGlobals.gau4EvpnRtIndex));

    /* VRF VNI RT Index Initialization */
    for (u4ContextId = 0; u4ContextId < MAX_VXLAN_EVPN_VRF; u4ContextId++)
    {
        gVxlanGlobals.gau4EvpnVrfRtBitMapIdx[u4ContextId][0] = 1;
    }

    MEMSET (gVxlanGlobals.gau4EvpnVrfRtIndex, 0,
            sizeof (gVxlanGlobals.gau4EvpnVrfRtIndex));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanInitRTIndexMgr Exit\n"));
}

/*****************************************************************************/
/* Function Name : EvpnBgp4NotifyASN                                        */
/* Description   : Callback function used to update ASN value in local BGP   */
/*                 data base.                                                */
/* Input(s)      : u1AsnType  - ASN Type                                     */
/*                 u4AsnValue - ASN Value                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
EvpnBgp4NotifyASN (UINT1 u1AsnType, UINT4 u4AsnValue)
{
    EvpnBgpUpdateASN (u1AsnType, u4AsnValue);
    return VXLAN_SUCCESS;
}

/****************************************************************************/
/* Function Name   : EvpnRegisterCallback                                   */
/* Description     : This function is used by BGP to register it's callback */
/*                   functions with EVPN                                    */
/* Input (s)       : pRegInfo - Pointer to tEvpnRegInfo, with               */
/*                   u2InfoMask and respective function pointer updated     */
/* Output (s)      :                                                        */
/* Returns         : If successful, returns OSIX_SUCCESS. Else returns      */
/*                   OSIX_FAILURE                                           */
/****************************************************************************/
INT4
EvpnRegisterCallback (tEvpnBgpRegEntry * pRegInfo)
{
    UINT2               u2InfoMask = 0;

    if (pRegInfo == NULL)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC,
                    "Registration object is NULL. Registration failed.\r\n"));
        return OSIX_FAILURE;
    }
    u2InfoMask = pRegInfo->u2InfoMask;
    if (u2InfoMask & EVPN_BGP4_ROUTE_PARAMS_REQ)
    {
        EVPN_NOTIFY_ROUTE_PARAMS_CB = pRegInfo->pBgp4EvpnNotifyRouteParams;
    }
    if (u2InfoMask & EVPN_BGP4_VRF_ADMIN_STATUS_REQ)
    {
        EVPN_NOTIFY_OPER_STATUS_CHANGE_CB =
            pRegInfo->pBgp4EvpnNotifyAdminStatusChange;
    }
    if (u2InfoMask & EVPN_BGP4_NOTIFY_MAC)
    {
        EVPN_NOTIFY_MAC_UPDATE_CB = pRegInfo->pBgp4EvpnNotifyMACUpdate;
    }
    if (u2InfoMask & EVPN_BGP4_GET_MAC)
    {
        EVPN_NOTIFY_GET_MAC_CB = pRegInfo->pBgp4EvpnGetMACNotification;
    }
    if (u2InfoMask & EVPN_BGP4_NOTIFY_ESI)
    {
        EVPN_NOTIFY_ESI_UPDATE_CB = pRegInfo->pBgp4EvpnNotifyESIUpdate;
    }

    if (u2InfoMask & EVPN_BGP4_NOTIFY_ETH_AD)
    {
        EVPN_NOTIFY_AD_ROUTE_CB = pRegInfo->pBgp4EvpnNotifyADRoute;
    }

    if (u2InfoMask & EVPN_BGP4_NOTIFY_L3VNI)
    {
        EVPN_NOTIFY_L3VNI = pRegInfo->pBgp4EvpnNotifyL3Vni;
    }

    if (u2InfoMask & EVPN_BGP4_NOTIFY_L3VNI)
    {
        EVPN_NOTIFY_L3VNI = pRegInfo->pBgp4EvpnNotifyL3Vni;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/* Function Name   : EvpnDeRegisterCallback                                 */
/* Description     : This function is used by BGP to de- register it's      */
/*                   callback functions with EVPN                                    */
/* Input (s)       : pRegInfo - Pointer to tEvpnRegInfo, with               */
/*                   u2InfoMask and respective function pointer updated     */
/* Output (s)      :                                                        */
/* Returns         : If successful, returns OSIX_SUCCESS. Else returns      */
/*                   OSIX_FAILURE                                           */
/****************************************************************************/
INT4
EvpnDeRegisterCallback (tEvpnBgpRegEntry * pRegInfo)
{
    UINT2               u2InfoMask = 0;

    if (pRegInfo == NULL)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC,
                    "pRegInfo object is NULL. De-Registration failed.\r\n"));
        return OSIX_FAILURE;
    }

    u2InfoMask = pRegInfo->u2InfoMask;
    if (u2InfoMask & EVPN_BGP4_ROUTE_PARAMS_REQ)
    {
        EVPN_NOTIFY_ROUTE_PARAMS_CB = NULL;
    }

    if (u2InfoMask & EVPN_BGP4_NOTIFY_MAC)
    {
        EVPN_NOTIFY_MAC_UPDATE_CB = NULL;
    }
    if (u2InfoMask & EVPN_BGP4_NOTIFY_ESI)
    {
        EVPN_NOTIFY_ESI_UPDATE_CB = NULL;
    }

    if (u2InfoMask & EVPN_BGP4_NOTIFY_ETH_AD)
    {
        EVPN_NOTIFY_AD_ROUTE_CB = NULL;
    }
    return OSIX_SUCCESS;
}
#endif /* EVPN_VXLAN_WANTED */

/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlanmain.c                     */
/*-----------------------------------------------------------------------*/
