/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: vxutil.c,v 1.29 2018/01/11 11:18:07 siva Exp $
 *
 * Description : This file contains the utility 
 *               functions of the VXLAN module
 *****************************************************************************/

#include "vxinc.h"
#ifdef EVPN_VXLAN_WANTED
static UINT4        gu4BgpRdRtAsnValue = 0;
static UINT1        gu1BgpRdRtAsnType = 3;
UINT1               gu4OrdinalNum = 0;
#endif
/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetVniFromVlan                                *
 *  Description     : Gets VNI number from the VLAN id                       *
 *  Input           : u2VlanId - VLAN id                                     *
 *  Output          : pu4VniId - VNI number                                  *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

INT4
VxlanUtilGetVniFromVlan (UINT2 u2VlanId, UINT4 *pu4VniId)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetVniFromVlan: Entry\n"));

    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId = (INT4) u2VlanId;

    pVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanMapEntry);

    if (pVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No VNI entry found for VLAN id -"
                         "%d\n", u2VlanId));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " VNI entry found for VLAN id -"
                     "%d\n", u2VlanId));

    *pu4VniId = pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapVniNumber;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetVniFromVlan: Exit\n"));

    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetVrfFromVlan                                *
 *  Description     : Gets VRF name from the VLAN id                         *
 *  Input           : u2VlanId - VLAN id                                     *
 *  Output          : pu1VrfName - VRF name                                  *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

INT4
VxlanUtilGetVrfFromVlan (UINT2 u2VlanId, UINT1 *pu1VrfName)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetVrfFromVlan: Entry\n"));

    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId = (INT4) u2VlanId;

    pVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanMapEntry);

    if (pVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                         " No Vlan-Vni Map entry found for VLAN id -" "%d\n",
                         u2VlanId));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " Vlan-Vni map entry found for VLAN id -"
                     "%d\n", u2VlanId));

    MEMCPY (pu1VrfName, pVxlanVniVlanMapEntry->MibObject.
            au1FsVxlanVniVlanVrfName,
            pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanVrfNameLen);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetVrfFromVlan: Exit\n"));

    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetVlanFromVni                                *
 *  Description     : Gets VNI number from the VLAN id                       *
 *  Input           : u4VniId - VNI number                                   *
 *  Output          : pu2VlanId - VLAN id                                    *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

INT4
VxlanUtilGetVlanFromVni (UINT4 u4VniId, UINT2 *pu2VlanId)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    INT4                i4FsVxlanVniVlanMapVlanId = 0;
    INT4                i4NextFsVxlanVniVlanMapVlanId = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetVlanFromVni: Entry\n"));

    if (SNMP_SUCCESS == nmhGetFirstIndexFsVxlanVniVlanMapTable
        (&i4FsVxlanVniVlanMapVlanId))
    {
        VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            i4FsVxlanVniVlanMapVlanId;
        while (1)
        {
            if (VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry)
                == OSIX_SUCCESS)
            {
                if ((VxlanFsVxlanVniVlanMapEntry.MibObject.
                     u4FsVxlanVniVlanMapVniNumber != 0)
                    && (VxlanFsVxlanVniVlanMapEntry.MibObject.
                        u4FsVxlanVniVlanMapVniNumber == u4VniId))
                {
                    *pu2VlanId =
                        (UINT2) VxlanFsVxlanVniVlanMapEntry.MibObject.
                        i4FsVxlanVniVlanMapVlanId;
                    return VXLAN_SUCCESS;
                }
            }
            if (SNMP_SUCCESS != nmhGetNextIndexFsVxlanVniVlanMapTable
                (i4FsVxlanVniVlanMapVlanId, &i4NextFsVxlanVniVlanMapVlanId))
            {
                break;
            }
            i4FsVxlanVniVlanMapVlanId = i4NextFsVxlanVniVlanMapVlanId;
            VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
                i4NextFsVxlanVniVlanMapVlanId;
        }
    }
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetNveDatabase                                *
 *  Description     : Gets the NVE datatbase                                 *
 *  Input           : u4VniId - VNI number                                   *
 *                    DestVmAddr - Destination VM mac address                *
 *                    u4IfIndex  - NVE if index vlaue                        *
 *  Output          : None                                                   *
 *  Returns         : pVxlanNveEntry - NVE database                          *
 * ************************************************************************* */

tVxlanFsVxlanNveEntry *
VxlanUtilGetNveDatabase (UINT4 u4VniId, tMacAddr DestVmAddr, UINT4 u4IfIndex)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveDatabase: Entry\n"));

    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;

    VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4VniId;

    MEMCPY (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac, DestVmAddr,
            VXLAN_ETHERNET_ADDR_SIZE);

    pVxlanNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   (tRBElem *) & VxlanNveEntry);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveDatabase: Exit\n"));

    return pVxlanNveEntry;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetMCastDatabase                              *
 *  Description     : Gets the MCAST datatbase                               *
 *  Input           : u4VniId - VNI number                                   *
 *                    u4IfIndex  - NVE if index vlaue                        *
 *  Output          : None                                                   *
 *  Returns         : pVxlanMcastEntry - MCAST database                      *
 * ************************************************************************* */

tVxlanFsVxlanMCastEntry *
VxlanUtilGetMCastDatabase (UINT4 u4VniId, UINT4 u4IfIndex)
{
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetMCastDatabase: Entry\n"));

    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;

    VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber = u4VniId;

    pVxlanMCastEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   (tRBElem *) & VxlanMCastEntry);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetMCastDatabase: Exit\n"));

    return pVxlanMCastEntry;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetInReplicaDatabase                          *
 *  Description     : Gets the NVE datatbase                                 *
 *  Input           : u4VniId - VNI number                                   *
 *                    u4IfIndex  - NVE if index vlaue                        *
 *  Output          : None                                                   *
 *  Returns         : pVxlanInReplicaEntry - NVE database                    *
 * ************************************************************************* */

tVxlanFsVxlanInReplicaEntry *
VxlanUtilGetInReplicaDatabase (UINT4 u4VniId, UINT4 u4IfIndex)
{
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetInReplicaDatabase: Entry\n"));

    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));

    VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        (INT4) u4IfIndex;

    VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber = u4VniId;

    pVxlanInReplicaEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   (tRBElem *) & VxlanInReplicaEntry);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetInReplicaDatabase: Exit\n"));
    return pVxlanInReplicaEntry;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetNveIndexFromNveTable                       *
 *  Description     : Gets the NVE if index from the NVE table               *
 *  Input           : u4VniNumber - VNI number                               *
 *  Output          : pu4NveIndex - Nve If Index                             *
 *  Returns         : None                                                   *
 * ************************************************************************* */

tVxlanFsVxlanNveEntry *
VxlanUtilGetNveIndexFromNveTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromNveTable: Entry\n"));

    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4VniNumber;

    pVxlanNveEntry = RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                                    (tRBElem *) & VxlanNveEntry, NULL);
    while (pVxlanNveEntry != NULL)
    {
        if (pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber == u4VniNumber)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " NVE entry found for VNI number -"
                             "%d\n", u4VniNumber));
            *pu4NveIndex =
                (UINT4) pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex;
            return pVxlanNveEntry;
        }
        pVxlanNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           (tRBElem *) pVxlanNveEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No NVE entry found for VNI number -"
                     "%d\n", u4VniNumber));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromNveTable: Exit\n"));

    return NULL;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetNveIndexFromMcastTable                     *
 *  Description     : Gets the NVE if index from the Multicast table         *
 *  Input           : u4VniNumber - VNI number                               *
 *  Output          : pu4NveIndex - Nve If Index                             *
 *  Returns         : None                                                   *
 * ************************************************************************* */

tVxlanFsVxlanMCastEntry *
VxlanUtilGetNveIndexFromMcastTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex)
{
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromMcastTable: Entry\n"));

    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));

    VxlanMCastEntry.MibObject.u4FsVxlanMCastVniNumber = u4VniNumber;

    pVxlanMCastEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                       (tRBElem *) & VxlanMCastEntry, NULL);
    while (pVxlanMCastEntry != NULL)
    {
        if (pVxlanMCastEntry->MibObject.u4FsVxlanMCastVniNumber == u4VniNumber)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                             " MCAST entry found for VNI number -" "%d\n",
                             u4VniNumber));
            *pu4NveIndex =
                (UINT4) pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex;
            return pVxlanMCastEntry;
        }
        pVxlanMCastEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                           (tRBElem *) pVxlanMCastEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " NO MCAST entry found for VNI number -"
                     "%d\n", u4VniNumber));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromMcastTable: Exit\n"));

    return NULL;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetNveIndexFromInReplicaTable                 *
 *  Description     : Gets the NVE if index from the NVE table               *
 *  Input           : u4VniNumber - VNI number                               *
 *  Output          : pu4NveIndex - Nve If Index                             *
 *  Returns         : None                                                   *
 * ************************************************************************* */

tVxlanFsVxlanInReplicaEntry *
VxlanUtilGetNveIndexFromInReplicaTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex)
{
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromInReplicaTable: Entry\n"));

    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));

    VxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber = u4VniNumber;

    pVxlanInReplicaEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       (tRBElem *) & VxlanInReplicaEntry, NULL);
    while (pVxlanInReplicaEntry != NULL)
    {
        if (pVxlanInReplicaEntry->MibObject.u4FsVxlanInReplicaVniNumber ==
            u4VniNumber)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " NVE entry found for VNI number -"
                             "%d\n", u4VniNumber));
            *pu4NveIndex =
                (UINT4) pVxlanInReplicaEntry->MibObject.
                i4FsVxlanInReplicaNveIfIndex;
            return pVxlanInReplicaEntry;
        }
        pVxlanInReplicaEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                           (tRBElem *) pVxlanInReplicaEntry, NULL);
    }
    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC,
                     " No NVE entry found for VNI number -" "%d\n",
                     u4VniNumber));
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromInReplicaTable: Exit\n"));
    return NULL;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilAddHdr                                        *
 *  Description     : Adds VXLAN header                                      *
 *  Input           : pBuf - packet buffer                                   *
 *                    u4VniNumber - VNI number                               *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */

VOID
VxlanUtilAddHdr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VniNumber)
{
    UINT4               u4Flag = VXLAN_HDR_FLAG;
    tVxlanHdr           VxlanHdr;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUtilAddHdr: Entry\n"));

    /* Insert First four bytes 
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
       |R|R|R|R|I|R|R|R|            Reserved                           | 
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
     */
    VxlanHdr.u4Flag = OSIX_HTONL ((u4Flag << 24) & 0xff000000);

    /* Insert second four bytes
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
       |                VXLAN Network Identifier (VNI) |   Reserved    | 
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                         
     */

    VxlanHdr.u4VniNumber = OSIX_HTONL ((u4VniNumber << 8) & 0xffffff00);

    if (CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &VxlanHdr,
                                  VXLAN_HDR_TOTAL_OFFSET) == CRU_FAILURE)
    {
        return;
    }

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "VXLAN header encapulated with"
                     "VNI number - %d and Flag value - %d\n", u4VniNumber,
                     u4Flag));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanUtilAddHdr: Exit\n"));

    return;
}

/* ************************************************************************* *
 *  Function Name   : VxlanProcessL2Pkt                                      *
 *  Description     : Validates and Removes the VXLAN header                 *
 *  Input           : u4FirstByte  - First byte of VXLAN header              *
 *                    u4SecondByte - Second byte of VXLAN header             *
 *  Output          : pu4VniNumber - VNI number                              *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */

INT4
VxlanUtilValidateHdr (tVxlanHdr * pVxlanHdr, UINT4 *pu4VniNumber)
{
    UINT4               u4VniNumber = 0;
    UINT4               u1Flag = 0;
    UINT4               u4Rsvd = 0;
    UINT4               u4FirstByte = 0;
    UINT4               u4SecondByte = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilValidateHdr: Entry\n"));

    if (pVxlanHdr == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "VXLAN header is null\n"));
        return VXLAN_FAILURE;
    }

    /* Get the First four bytes from the VXLAN header */
    u4FirstByte = OSIX_NTOHL (pVxlanHdr->u4Flag);

    /* Get the second four bytes from the VXLAN header */
    u4SecondByte = OSIX_NTOHL (pVxlanHdr->u4VniNumber);

    /* Get the Flag value from the first four bytes */
    u1Flag = ((u4FirstByte & 0xff000000) >> 24);

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "Flag Value in VXLAN header - %d\n",
                     u1Flag));

    /* Flag value should be 8 */
    if (u1Flag != VXLAN_HDR_FLAG)
    {
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "Flag value is incorrect\n"));
        return VXLAN_FAILURE;
    }

    /* Get the reserved field value from first four bytes */
    u4Rsvd = (u4FirstByte & 0x00ffffff);

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "First Reserved field Value in "
                     "VXLAN header - %d\n", u4Rsvd));

    /* reserved field value should be 0 */
    if (u4Rsvd != 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "Reserved field value is incorrect\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "VNI Value in VXLAN header - %d\n",
                     u4VniNumber));

    /* Get the vni value from second four bytes */
    u4VniNumber = ((u4SecondByte & 0xffffff00) >> 8);

    /* vni should be in the range between 4096 and 12287 */
    if ((u4VniNumber < MIN_VALUE_VNI_IDENTIFIER) ||
        (u4VniNumber > MAX_VALUE_VNI_IDENTIFIER))
    {
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "VNI value is incorrect\n"));
        return VXLAN_FAILURE;
    }

    /* Get the reserved field value from second four bytes */
    u4Rsvd = (u4SecondByte & 0x000000ff);

    VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "Second Reserved field Value in"
                     "VXLAN header - %d\n", u4Rsvd));

    /* reserved field value should be 0 */
    if (u4Rsvd != 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_PKT_TRC, "Reserved field value is incorrect\n"));
        return VXLAN_FAILURE;
    }

    *pu4VniNumber = u4VniNumber;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilValidateHdr: Exit\n"));

    return VXLAN_SUCCESS;

}

/* ************************************************************************* *
 *  Function Name   : VxlanDeleteRBTreeEntries                               *
 *  Description     : Deletes all the nodes in all VXLAN RBTrees             *
 *  Input           : VOID                                                   *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */

VOID
VxlanDeleteRBTreeEntries ()
{

    UINT4               u4NveIfIndex = 0;
    UINT4               u4ReplicaIndex = 0;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
#ifdef EVPN_VXLAN_WANTED
    tEvpnRoute          EvpnRoute;
#endif

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanDeleteRBTreeEntries: Entry\n"));
#ifdef EVPN_VXLAN_WANTED
    MEMSET (&EvpnRoute, 0, sizeof (tEvpnRoute));
#endif

    /* Delete the Entries from VLAN-VNI map table RBTree */
    pVxlanVniVlanMapEntry = (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    while (pVxlanVniVlanMapEntry != NULL)
    {
        VxlanUtilGetNveIndexFromNveTable (pVxlanVniVlanMapEntry->
                                          MibObject.
                                          u4FsVxlanVniVlanMapVniNumber,
                                          &u4NveIfIndex);
        if (u4NveIfIndex == 0)
        {
            VxlanUtilGetNveIndexFromInReplicaTable (pVxlanVniVlanMapEntry->
                                                    MibObject.
                                                    u4FsVxlanVniVlanMapVniNumber,
                                                    &u4NveIfIndex);
            if (u4NveIfIndex == 0)
            {
                VxlanUtilGetNveIndexFromMcastTable (pVxlanVniVlanMapEntry->
                                                    MibObject.
                                                    u4FsVxlanVniVlanMapVniNumber,
                                                    &u4NveIfIndex);
            }
        }
#ifdef VLAN_WANTED
        if (VlanApiSetMemberPort ((UINT4) pVxlanVniVlanMapEntry->
                                  MibObject.i4FsVxlanVniVlanMapVlanId,
                                  u4NveIfIndex, VLAN_DELETE) == VLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "VxlanSetAllFsVxlanVniVlanMapTable: Fail to"
                        "unamp NVE and VLAN.\r\n"));

        }

#endif
        pVxlanVniVlanMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                           (tRBElem *) pVxlanVniVlanMapEntry, NULL);

    }

    /* Delete the Entries from VTEP table RBTreee */
    pVxlanVtepEntry = (tVxlanFsVxlanVtepEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable);
    while (pVxlanVtepEntry != NULL)
    {
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable, pVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanVtepEntry);
        pVxlanVtepEntry =
            (tVxlanFsVxlanVtepEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                       VxlanGlbMib.
                                                       FsVxlanVtepTable);
    }

    /* Delete the Entries from NVE table RBTree */
    pVxlanNveEntry = (tVxlanFsVxlanNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable);
    while (pVxlanNveEntry != NULL)
    {
        if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Nve entry from h/w\r\n"));
        }
#ifdef EVPN_VXLAN_WANTED
        if (pVxlanNveEntry->b1IsIrbRoute == TRUE)
        {
            EvpnRoute.u4L3VniNumber =
                pVxlanNveEntry->MibObject.u4FsVxlanNveVniNumber;
            MEMCPY (&(EvpnRoute.VxlanDestVmMac),
                    &(pVxlanNveEntry->MibObject.FsVxlanNveDestVmMac),
                    VXLAN_ETHERNET_ADDR_SIZE);
            EvpnRoute.i4VxlanRemoteVtepAddressType =
                pVxlanNveEntry->MibObject.i4FsVxlanNveRemoteVtepAddressType;
            MEMCPY (&(EvpnRoute.au1VxlanDestAddress),
                    &(pVxlanNveEntry->au1VxlanIRBHostAddress),
                    VXLAN_IP4_ADDR_LEN);

            if (EvpnRoute.i4VxlanRemoteVtepAddressType == VXLAN_IPV4_UNICAST)
            {
                MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                        &(pVxlanNveEntry->MibObject.
                          au1FsVxlanNveRemoteVtepAddress), VXLAN_IP4_ADDR_LEN);
            }
            else
            {
                MEMCPY (&(EvpnRoute.au1VxlanRemoteVtepAddress),
                        &(pVxlanNveEntry->MibObject.
                          au1FsVxlanNveRemoteVtepAddress), VXLAN_IP6_ADDR_LEN);
            }
            VxlanUtilHandleSymmerticDelIrbRoute (&EvpnRoute,
                                                 pVxlanNveEntry->MibObject.
                                                 i4FsVxlanNveIfIndex);
        }
#endif

        if (RBTreeRem
            (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable, pVxlanNveEntry) != NULL)
        {
            MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                                (UINT1 *) pVxlanNveEntry);
        }
        pVxlanNveEntry =
            (tVxlanFsVxlanNveEntry *) RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                                                      FsVxlanNveTable);
    }

    /* Delete the Entries from INGRESS REPLICA table RBTree */
    pVxlanInReplicaEntry = (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable);
    while (pVxlanInReplicaEntry != NULL)
    {
        if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry, VXLAN_HW_DEL)
            == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Ingress Replica entry from h/w\r\n"));

        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   pVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanInReplicaEntry);
        /* Release of Replica Index */
        VxlanUtilGetReplicaIndex (pVxlanInReplicaEntry->
                                  MibObject.u4FsVxlanInReplicaVniNumber,
                                  &u4ReplicaIndex);

        VxlanUtilReleaseReplicaIndex (u4ReplicaIndex,
                                      (UINT4) pVxlanInReplicaEntry->MibObject.
                                      i4FsVxlanInReplicaIndex);

        pVxlanInReplicaEntry =
            (tVxlanFsVxlanInReplicaEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsVxlanInReplicaTable);
    }

    /* Delete the Entries from MCAST table RBTree */
    pVxlanMCastEntry = (tVxlanFsVxlanMCastEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable);
    while (pVxlanMCastEntry != NULL)
    {
        if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Multicast entry from h/w\r\n"));
        }

        VxlanUdpLeaveMulticast (pVxlanMCastEntry);
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   pVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanMCastEntry);
        pVxlanMCastEntry =
            (tVxlanFsVxlanMCastEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                        VxlanGlbMib.
                                                        FsVxlanMCastTable);
    }

    /* Delete the Entries from VLAN-VNI map table RBTree */
    pVxlanVniVlanMapEntry = (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    while (pVxlanVniVlanMapEntry != NULL)
    {
        if (VxlanHwUpdateVniVlanMapDatabase
            (pVxlanVniVlanMapEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete VniVlan Mapping entry from h/w\r\n"));
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   pVxlanVniVlanMapEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVNIVLANMAPTABLE_POOLID,
                            (UINT1 *) pVxlanVniVlanMapEntry);
        pVxlanVniVlanMapEntry = (tVxlanFsVxlanVniVlanMapEntry *)
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanDeleteRBTreeEntries: Exit\n"));
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilUpdateStats                                   *
 *  Description     : Updates the Statistics                                 *
 *  Input           : u2VlanId  - VLAN id                                    *
 *                    u1StatType - Statistics type                           *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */

VOID
VxlanUtilUpdateStats (UINT2 u2VlanId, UINT1 u1StatType)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:vxlanUtilUpdateStats: Entry\n"));

    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId = (INT4) u2VlanId;

    pVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanMapEntry);

    if (pVxlanVniVlanMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No VNI entry found for VLAN id -"
                         "%d\n", u2VlanId));
        return;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "Updates the statistics\n"));

    switch (u1StatType)
    {
        case VXLAN_STAT_PKT_SENT:
            /* Update the Packet sent counter */
            pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktSent++;
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "Number of VXLAN packet sent -"
                             "%d\n", pVxlanVniVlanMapEntry->MibObject.
                             u4FsVxlanVniVlanMapPktSent));
            break;

        case VXLAN_STAT_PKT_RCVD:
            /* Update the Packet received counter */
            pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktRcvd++;
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "Number of VXLAN packet Rcvd -"
                             "%d\n", pVxlanVniVlanMapEntry->MibObject.
                             u4FsVxlanVniVlanMapPktRcvd));
            break;

        case VXLAN_STAT_PKT_DRPD:
            /* Update the Packet dropped counter */
            pVxlanVniVlanMapEntry->MibObject.u4FsVxlanVniVlanMapPktDrpd++;
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, "Number of VXLAN packet Drpd -"
                             "%d\n", pVxlanVniVlanMapEntry->MibObject.
                             u4FsVxlanVniVlanMapPktDrpd));
            break;

        default:
            break;
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:vxlanUtilUpdateStats: Exit\n"));
    return;
}

/* ********************************************************** *
 * Function   : VxlanUtilFindNextFreeReplicaIndex             *
 * Description: This function finds the next free Replica     *
 *              Index for the given VNI ID.                   *
 * Input      : u4FsVxlanNveVniNumber                         *
 * Output     : pu4FreeIndex                                  *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                   *
 * ********************************************************** */
UINT4
VxlanUtilFindNextFreeReplicaIndex (UINT4 u4FsVxlanNveVniNumber,
                                   UINT4 *pu4FreeIndex)
{
    UINT1               u1BitNdx = 0;
    UINT2               u2NofUINT4 = 0;
    UINT4               u4MaxU4 = 0;
    UINT4              *pBitmap = NULL;
    UINT4               u4ReplicaIndex = 0;

    VxlanUtilGetReplicaIndex (u4FsVxlanNveVniNumber, &u4ReplicaIndex);
    pBitmap = gVxlanGlobals.gau4VxlanReplicaBitMapIdx[u4ReplicaIndex];
    u4MaxU4 = MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE;

    for (u2NofUINT4 = 0; u2NofUINT4 < u4MaxU4; u2NofUINT4++)
    {
        if (VXLAN_UTIL_UINT4_MAX != pBitmap[u2NofUINT4])
        {
            for (u1BitNdx = 0; u1BitNdx < VXLAN_NUM_OF_BITS_IN_UINT4;
                 u1BitNdx++)
            {
                if (((u2NofUINT4 * VXLAN_NUM_OF_BITS_IN_UINT4) +
                     u1BitNdx) > VXLAN_MAX_NUM_REPLICA_ENTRY_PER_VNI)
                {
                    break;
                }
                if (!((pBitmap[u2NofUINT4] >> u1BitNdx) & 0x1))
                {
                    *pu4FreeIndex =
                        (UINT4) ((u2NofUINT4 *
                                  VXLAN_NUM_OF_BITS_IN_UINT4) + u1BitNdx);
                    return VXLAN_SUCCESS;
                }
            }
        }
    }
    return VXLAN_FAILURE;
}

/* ************************************************************** *
 * Function   : VxlanUtilSetReplicaIndex                          *
 * Description: This function sets the Replica index in the global*
 *              bitmap (used for reservation)                     *
 * Input      : u4FsVxlanNveVniNumber                             *
 *              u4ReplicaIndex                                    *
 *  Output     : NONE                                             *
 *  Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                      *
 ******************************************************************/
UINT4
VxlanUtilSetReplicaIndex (UINT4 u4ReplicaIndex, UINT4 u4InReplicaIndex)
{
    UINT4              *pBitmap = NULL;

    pBitmap = gVxlanGlobals.gau4VxlanReplicaBitMapIdx[u4ReplicaIndex];
    pBitmap[u4InReplicaIndex / VXLAN_NUM_OF_BITS_IN_UINT4] =
        (UINT4) ((UINT4) (pBitmap[(UINT4) (u4InReplicaIndex /
                                           VXLAN_NUM_OF_BITS_IN_UINT4)]) |
                 (UINT4) (1 << ((UINT4) (u4InReplicaIndex %
                                         VXLAN_NUM_OF_BITS_IN_UINT4))));
    return VXLAN_SUCCESS;
}

/* ************************************************************************* *
    Function        : VxlanUtilDelNveIntDatabases                            *
 *  Description     : Deletes the databases related to NVE interface         *
 *  Input           : u4IfIndex  - IfIndex                                   *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */

VOID
VxlanUtilDelNveIntDatabases (UINT4 u4IfIndex)
{
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanFsVxlanNveEntry *pVxlanNextNveEntry = NULL;
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry *pVxlanInReplicaNextEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanInReplicaEntry;
    tVxlanFsVxlanMCastEntry *pVxlanMCastEntry = NULL;
    tVxlanFsVxlanMCastEntry *pVxlanMCastNextEntry = NULL;
    tVxlanFsVxlanMCastEntry VxlanMCastEntry;
    UINT4               u4ReplicaIndex = 0;
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanDeleteRBTreeEntries: Entry\n"));

    /* Delete the VTEP entry */
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = (INT4) u4IfIndex;

    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    if (pVxlanVtepEntry != NULL)
    {
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable, pVxlanVtepEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANVTEPTABLE_POOLID,
                            (UINT1 *) pVxlanVtepEntry);
    }

    /* Delete the NVE entries */
    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));

    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = (INT4) u4IfIndex;

    pVxlanNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                       (tRBElem *) & VxlanNveEntry, NULL);

    while ((pVxlanNveEntry != NULL) &&
           ((pVxlanNveEntry->MibObject.i4FsVxlanNveIfIndex == (INT4) u4IfIndex)
            || (pVxlanNveEntry->u4OrgNveIfIndex == u4IfIndex)))
    {
        pVxlanNextNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                           pVxlanNveEntry, NULL);
        if (VxlanHwUpdateNveDatabase (pVxlanNveEntry, VXLAN_HW_DEL) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Nve entry from h/w\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable, pVxlanNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANNVETABLE_POOLID,
                            (UINT1 *) pVxlanNveEntry);
        pVxlanNveEntry = pVxlanNextNveEntry;
    }

    /* Delate the Ingree Replica Entries */
    MEMSET (&VxlanInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));

    VxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        (INT4) u4IfIndex;

    pVxlanInReplicaEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       (tRBElem *) & VxlanInReplicaEntry, NULL);

    while ((pVxlanInReplicaEntry != NULL) &&
           (pVxlanInReplicaEntry->MibObject.i4FsVxlanInReplicaNveIfIndex ==
            (INT4) u4IfIndex))
    {
        pVxlanInReplicaNextEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                           (tRBElem *) pVxlanInReplicaEntry, NULL);

        if (VxlanHwUpdateInReplicaDatabase (pVxlanInReplicaEntry, VXLAN_HW_DEL)
            == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Ingress Replica entry from h/w\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                   pVxlanInReplicaEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                            (UINT1 *) pVxlanInReplicaEntry);
        VxlanUtilGetReplicaIndex (pVxlanInReplicaEntry->MibObject.
                                  u4FsVxlanInReplicaVniNumber, &u4ReplicaIndex);

        VxlanUtilReleaseReplicaIndex (u4ReplicaIndex,
                                      (UINT4) pVxlanInReplicaEntry->MibObject.
                                      i4FsVxlanInReplicaIndex);

        pVxlanInReplicaEntry = pVxlanInReplicaNextEntry;
    }

    MEMSET (&VxlanMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    VxlanMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex = (INT4) u4IfIndex;

    pVxlanMCastEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                       (tRBElem *) & VxlanMCastEntry, NULL);

    while ((pVxlanMCastEntry != NULL) &&
           (pVxlanMCastEntry->MibObject.i4FsVxlanMCastNveIfIndex ==
            (INT4) u4IfIndex))
    {
        pVxlanMCastNextEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                           (tRBElem *) pVxlanMCastEntry, NULL);
        if (VxlanHwUpdateMcastDatabase (pVxlanMCastEntry, VXLAN_HW_DEL) ==
            VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete Multicast entry from h/w\r\n"));
        }
        VxlanUdpLeaveMulticast (pVxlanMCastEntry);
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanMCastTable,
                   pVxlanMCastEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANMCASTTABLE_POOLID,
                            (UINT1 *) pVxlanMCastEntry);
        pVxlanMCastEntry = pVxlanMCastNextEntry;
    }

}

/****************************************************************
 *Function   : EvpnUtilReleaseRtIndex                           *
 *Description: This function release the Replica index for the  *
 *             given u4FsVxlanNveVniNumber                      *
 *Input      : u4FsVxlanNveVniNumber                            *
 *             u4ReplicaIndex                                   *
 *Output     : NONE                                             *
 *Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                      *
 ****************************************************************/
UINT4
VxlanUtilReleaseReplicaIndex (UINT4 u4FsVxlanNveVniNumber, UINT4 u4ReplicaIndex)
{
    UINT4              *pBitmap = NULL;

    pBitmap = gVxlanGlobals.gau4VxlanReplicaBitMapIdx[u4FsVxlanNveVniNumber];
    pBitmap[u4ReplicaIndex / VXLAN_NUM_OF_BITS_IN_UINT4] =
        (UINT4) ((UINT4) (pBitmap[(UINT4) (u4ReplicaIndex /
                                           VXLAN_NUM_OF_BITS_IN_UINT4)]) &
                 ((UINT4) ~(1 << (UINT4) (u4ReplicaIndex %
                                          VXLAN_NUM_OF_BITS_IN_UINT4))));
    return VXLAN_SUCCESS;
}

/* ************************************************************************
 * Function   : VxlanGetReplicaEntry 
 * Description: This function gives the Replica entry from the given table index
 * Input      : u4FsVxlanNveIfIndex
 *              u4FsVxlanNveVniNumber
 *              pu4FsVxlanRemoteVtepAddressLen
 *              pu1FsVxlanRemoteVtepAddress
 * Output     : pVxlanReplicaEntry
 * Returns    : tVxlanFsVxlanInReplicaEntry
 ***************************************************************************/

tVxlanFsVxlanInReplicaEntry *
VxlanGetReplicaEntry (INT4 i4FsVxlanNveIfIndex, UINT4 u4FsVxlanNveVniNumber,
                      UINT4 *pu4FsVxlanRemoteVtepAddressLen,
                      UINT1 *pu1FsVxlanRemoteVtepAddress)
{
    tVxlanFsVxlanInReplicaEntry VxlanReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanReplicaEntry = NULL;

    MEMSET (&VxlanReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));

    VxlanReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        i4FsVxlanNveIfIndex;
    VxlanReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanNveVniNumber;

    pVxlanReplicaEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       &VxlanReplicaEntry, NULL);

    while (NULL != pVxlanReplicaEntry)
    {
        if ((pVxlanReplicaEntry->MibObject.
             i4FsVxlanInReplicaNveIfIndex == i4FsVxlanNveIfIndex) &&
            (pVxlanReplicaEntry->MibObject.
             u4FsVxlanInReplicaVniNumber == u4FsVxlanNveVniNumber))
        {
            if (MEMCMP (pVxlanReplicaEntry->
                        MibObject.au1FsVxlanInReplicaRemoteVtepAddress,
                        pu1FsVxlanRemoteVtepAddress,
                        *pu4FsVxlanRemoteVtepAddressLen) == 0)

            {
                return pVxlanReplicaEntry;
            }

        }

        pVxlanReplicaEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                           pVxlanReplicaEntry, NULL);
    }

    return NULL;
}

/* ********************************************************** *
 * Function   : VxlanUtilGetReplicaIndex                      *
 * Description: This function calculates the Replica index    *
 *              based on the given VNI number.                *
 * Input      : u4FsVxlanNveVniNumber                         *
 * Output     : pu4ReplicaIndex                               *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                   *
 * ********************************************************** */

INT4
VxlanUtilGetReplicaIndex (UINT4 u4FsVxlanNveVniNumber, UINT4 *pu4ReplicaIndex)
{
    UINT4               u4ReplicaIndex = 0;

    while (gVxlanGlobals.gau4VxlanReplicaIndex[u4ReplicaIndex] != VXLAN_FALSE)
    {
        if (u4ReplicaIndex >= MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:RT Index does not found\n"));
            return VXLAN_FAILURE;
        }
        if (gVxlanGlobals.gau4VxlanReplicaIndex[u4ReplicaIndex] ==
            u4FsVxlanNveVniNumber)
        {
            break;
        }
        u4ReplicaIndex++;
    }
    *pu4ReplicaIndex = u4ReplicaIndex;
    gVxlanGlobals.gau4VxlanReplicaIndex[u4ReplicaIndex] = u4FsVxlanNveVniNumber;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetRtIndex Exit\n"));
    return VXLAN_SUCCESS;
}

/* ***********************************************************************
 *Function   : VxlanDelIngressReplica
 *Description: This function deletes entry from the Replica Table
 *Input      : i4FsVxlanNveIfIndex,u4FsVxlanNveVniNumber
 *Output     : NONE
 *Returns    : NONE
 ************************************************************************/
VOID
VxlanDelIngressReplica (UINT4 u4FsVxlanNveIfIndex, UINT4 u4FsVxlanNveVniNumber)
{
    tVxlanFsVxlanInReplicaEntry *pVxlanGetInReplicaEntry = NULL;
    tVxlanFsVxlanInReplicaEntry VxlanGetInReplicaEntry;
    tVxlanFsVxlanInReplicaEntry *pVxlanGetNextInReplicaEntry = NULL;
    UINT4               u4ReplicaIndex = 0;
    MEMSET (&VxlanGetInReplicaEntry, 0, sizeof (tVxlanFsVxlanInReplicaEntry));

    VxlanGetInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
        (INT4) u4FsVxlanNveIfIndex;
    VxlanGetInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
        u4FsVxlanNveVniNumber;
    pVxlanGetInReplicaEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       &VxlanGetInReplicaEntry, NULL);

    while (NULL != pVxlanGetInReplicaEntry)
    {
        pVxlanGetNextInReplicaEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                           pVxlanGetInReplicaEntry, NULL);

        if ((pVxlanGetInReplicaEntry->MibObject.
             i4FsVxlanInReplicaNveIfIndex == (INT4) u4FsVxlanNveIfIndex) &&
            (pVxlanGetInReplicaEntry->MibObject.
             u4FsVxlanInReplicaVniNumber == u4FsVxlanNveVniNumber))
        {
            if (VxlanHwUpdateInReplicaDatabase (pVxlanGetInReplicaEntry,
                                                VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to delete Ingress-Replica entry in h/w\n"));
            }

            RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanInReplicaTable,
                       pVxlanGetInReplicaEntry);
            MemReleaseMemBlock (VXLAN_FSVXLANINREPLICATABLE_POOLID,
                                (UINT1 *) pVxlanGetInReplicaEntry);

            /* Release of Replica Index */
            VxlanUtilGetReplicaIndex (pVxlanGetInReplicaEntry->
                                      MibObject.u4FsVxlanInReplicaVniNumber,
                                      &u4ReplicaIndex);

            VxlanUtilReleaseReplicaIndex (u4ReplicaIndex,
                                          (UINT4) pVxlanGetInReplicaEntry->
                                          MibObject.i4FsVxlanInReplicaIndex);

        }
        pVxlanGetInReplicaEntry = pVxlanGetNextInReplicaEntry;
    }
    return;
}

/***************************************************************************
 * Function     : VxlanEvpnRegisterVlanInfo                                  *
 * Description  : This function is registered with VLAN module to get the    *
 *                MAC ADD/REMOVE information for EVPN module.                *
 * Input        : pVlanBasicInfo   - vlan information                        *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 ****************************************************************************/
VOID
VxlanEvpnRegisterVlanInfo (tVlanBasicInfo * pVlanBasicInfo)
{
#ifdef EVPN_VXLAN_WANTED
    UINT4               u4VrfId = 0;
    UINT4               u4VniId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1ESIString[EVPN_MAX_ESI_LEN];
    INT4                i4FsEvpnVxlanEnable = 0;
    INT4                i4EviId = 0;
    UINT1               au1VrfName[VXLAN_CLI_MAX_VRF_NAME_LEN];
    UINT1               u1IPAddressLen = 0;
    UINT1              *pu1Ipaddr = NULL;
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;
    tVxlanEvpnArpSupLocalMacEntry VxlanEvpnArpSupLocalMacEntry;
#endif
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;

    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanEvpnRegisterVlanInfo: Entry\n"));

    VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
        (INT4) pVlanBasicInfo->u2VlanId;
    pVxlanVniVlanMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                   (tRBElem *) & VxlanVniVlanMapEntry);

    if ((pVxlanVniVlanMapEntry != NULL) &&
        ((pVlanBasicInfo->u1Action == VLAN_CB_ADD_EGRESS_PORTS) ||
         (pVlanBasicInfo->u1Action == VLAN_CB_DELETE_EGRESS_PORTS)))
    {
        VxlanHwUpdateVniVlanMapDbBasedOnVlanPorts (pVxlanVniVlanMapEntry,
                                                   pVlanBasicInfo->u1Action,
                                                   pVlanBasicInfo->
                                                   LocalPortList);
        return;
    }
#ifdef EVPN_VXLAN_WANTED
    MEMSET (u1ESIString, 0, EVPN_MAX_ESI_LEN);
    MEMSET (au1VrfName, 0, VXLAN_CLI_MAX_VRF_NAME_LEN);
    MEMSET (&VxlanEvpnArpSupLocalMacEntry, 0,
            sizeof (tVxlanEvpnArpSupLocalMacEntry));

    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return;
    }

    CfaGetIfType (pVlanBasicInfo->u2Port, &u1IfType);

    if ((u1IfType == CFA_VXLAN_NVE)
        && (pVlanBasicInfo->u1EntryStatus == VLAN_FDB_LEARNT))
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "MAC is learnt via NVE interface. No need to give to BGP\n"));
        return;
    }

    /* Get the Vrf name from Vlan ID */
    if (VxlanUtilGetVrfFromVlan (pVlanBasicInfo->u2VlanId, (UINT1 *) au1VrfName)
        == VXLAN_SUCCESS)
    {
        if (VcmIsVrfExist (au1VrfName, &u4VrfId) == VXLAN_FALSE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
        }
    }
    switch (pVlanBasicInfo->u1Action)
    {
        case VLAN_CB_ADD_UNTAGGED_PORTS:
        case VLAN_CB_DELETE_UNTAGGED_PORTS:
            break;

        case VLAN_CB_UPDATE_MAC:

            if (VxlanUtilGetVniFromVlan (pVlanBasicInfo->u2VlanId, &u4VniId) !=
                VXLAN_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "VXLAN EVPN module - VNI Map to this VLAN %d is not found\r\n",
                            pVlanBasicInfo->u2VlanId));
            }
            else
            {
                if (EvpnUtilGetEviFromVni (u4VniId, &i4EviId) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "\r%% EVI Not Mapped for Vni %d\r\n", u4VniId));
                }

                if (EVPN_NOTIFY_MAC_UPDATE_CB != NULL)
                {
                    if (pVlanBasicInfo->u1EntryStatus == VLAN_FDB_MGMT)
                    {
                        pVlanBasicInfo->u1EntryStatus = EVPN_BGP4_STATIC_MAC;
                    }
                    EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                               pVlanBasicInfo->MacAddr, 0, NULL,
                                               pVlanBasicInfo->u1Action,
                                               pVlanBasicInfo->u1EntryStatus);
                }
            }
            break;

        case VLAN_CB_REMOVE_MAC:

            if (VxlanUtilGetVniFromVlan (pVlanBasicInfo->u2VlanId, &u4VniId) !=
                VXLAN_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "VXLAN EVPN module - VNI Map to this VLAN %d is not found\r\n",
                            pVlanBasicInfo->u2VlanId));
            }
            else
            {
                if (EvpnUtilGetEviFromVni (u4VniId, &i4EviId) == VXLAN_FAILURE)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "\r%% EVI Not Mapped for Vni %d\r\n", u4VniId));
                }
                MEMCPY (VxlanEvpnArpSupLocalMacEntry.SourceMac,
                        pVlanBasicInfo->MacAddr, sizeof (tMacAddr));

                pVxlanEvpnArpSupLocalMacEntry = RBTreeGet
                    (gVxlanGlobals.
                     VxlanEvpnArpSupLocalMacTable,
                     (tRBElem *) & VxlanEvpnArpSupLocalMacEntry);
                if (pVxlanEvpnArpSupLocalMacEntry != NULL)
                {
                    u1IPAddressLen =
                        pVxlanEvpnArpSupLocalMacEntry->u1AddressLen;
                    pu1Ipaddr = pVxlanEvpnArpSupLocalMacEntry->au1IpAddress;
                    RBTreeRem (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                               pVxlanEvpnArpSupLocalMacEntry);
                    MemReleaseMemBlock (VXLAN_EVPNARPSUPMACTABLE_POOLID,
                                        (UINT1 *)
                                        pVxlanEvpnArpSupLocalMacEntry);
                }

                if (EvpnUtilGetESIFromVniMapTable (i4EviId, u4VniId,
                                                   u1ESIString) ==
                    VXLAN_SUCCESS)
                {
                    if ((u1ESIString[0] == EVPN_ESI_TYPE_1) &&
                        (pVlanBasicInfo->u1OperStatus != EVPN_IF_OPER_DOWN))
                    {
                        EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                                   pVlanBasicInfo->MacAddr,
                                                   u1IPAddressLen, pu1Ipaddr,
                                                   pVlanBasicInfo->u1Action,
                                                   pVlanBasicInfo->
                                                   u1EntryStatus);
                    }
                    else if (u1ESIString[0] == EVPN_ESI_TYPE_0)
                    {
                        EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                                   pVlanBasicInfo->MacAddr,
                                                   u1IPAddressLen, pu1Ipaddr,
                                                   pVlanBasicInfo->u1Action,
                                                   pVlanBasicInfo->
                                                   u1EntryStatus);
                    }
                }
                else
                {
                    EVPN_NOTIFY_MAC_UPDATE_CB (u4VrfId, u4VniId, 0,
                                               pVlanBasicInfo->MacAddr,
                                               u1IPAddressLen, pu1Ipaddr,
                                               pVlanBasicInfo->u1Action,
                                               pVlanBasicInfo->u1EntryStatus);
                }
            }
            break;

        default:
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "VXLAN EVPN - VLAN module - Invalid indication from VLAN\r\n"));
            break;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnRegisterVlanInfo: Exit\n"));
    return;
#endif
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 *  Function    :  VxlanVniIsL2Vni
 *  Input       :  u4VniId 
 *  Description :  This routine Confirms if the given Vni number 
 *                 is used as L2Vni
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanVniIsL2Vni (UINT4 u4VniId)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    /* Check in EviVni Table */
    pVxlanFsEvpnVxlanEviVniMapEntry = RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable);
    while (pVxlanFsEvpnVxlanEviVniMapEntry != NULL)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber == u4VniId)
        {
            return (OSIX_SUCCESS);
        }
        pVxlanFsEvpnVxlanEviVniMapEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
             (tRBElem *) pVxlanFsEvpnVxlanEviVniMapEntry, NULL);
    }
    return (OSIX_FAILURE);
}

/****************************************************************************                                                 
 *  Function    :  VxlanVniIsL3Vni
 *  Input       :  u4VniId
 *  Description :  This routine Confirms if the given Vni number
 *                 is used as L3Vni
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanVniIsL3Vni (UINT4 u4VniId)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    /* Check in VRF-Vni Table */

    pVxlanFsEvpnVxlanVrfEntry = RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);

    while (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        if (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) == u4VniId)
        {
            return (OSIX_SUCCESS);
        }
        pVxlanFsEvpnVxlanVrfEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
             (tRBElem *) pVxlanFsEvpnVxlanVrfEntry, NULL);
    }
    return (OSIX_FAILURE);
}

/****************************************************************************
 *  Function    :  VxlanVniIsL2Vni
 *  Input       :  u4VniId 
 *  Description :  This routine Confirms if the given Vni number 
 *                 is used as L2Vni
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanVniIsL2Vni (UINT4 u4VniId)
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    /* Check in EviVni Table */
    pVxlanFsEvpnVxlanEviVniMapEntry = RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable);
    while (pVxlanFsEvpnVxlanEviVniMapEntry != NULL)
    {
        if (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber == u4VniId)
        {
            return (OSIX_SUCCESS);
        }
        pVxlanFsEvpnVxlanEviVniMapEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
             (tRBElem *) pVxlanFsEvpnVxlanEviVniMapEntry, NULL);
    }
    return (OSIX_FAILURE);
}

/****************************************************************************                                                 
 *  Function    :  VxlanVniIsL3Vni
 *  Input       :  u4VniId
 *  Description :  This routine Confirms if the given Vni number
 *                 is used as L3Vni
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanVniIsL3Vni (UINT4 u4VniId)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    /* Check in VRF-Vni Table */

    pVxlanFsEvpnVxlanVrfEntry = RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);

    while (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        if (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry) == u4VniId)
        {
            return (OSIX_SUCCESS);
        }
        pVxlanFsEvpnVxlanVrfEntry = RBTreeGetNext
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
             (tRBElem *) pVxlanFsEvpnVxlanVrfEntry, NULL);
    }
    return (OSIX_FAILURE);
}

/****************************************************************************
 *  Function    :  VxlanGetAllUtlFsEvpnVxlanEviVniMapTable
 *  Input       :  pVxlanGetFsEvpnVxlanEviVniMapEntry
 *                 pVxlandsFsEvpnVxlanEviVniMapEntry
 *  Description :  This routine Gets the
 *                 value of needed.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanGetAllUtlFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                         pVxlanGetFsEvpnVxlanEviVniMapEntry,
                                         tVxlanFsEvpnVxlanEviVniMapEntry *
                                         pVxlandsFsEvpnVxlanEviVniMapEntry)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanEviVniMapTable Entry\n"));
    UNUSED_PARAM (pVxlanGetFsEvpnVxlanEviVniMapEntry);
    UNUSED_PARAM (pVxlandsFsEvpnVxlanEviVniMapEntry);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanEviVniMapTable Exit\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  VxlanGetAllUtlFsEvpnVxlanBgpRTTable
 *  Input       :  pVxlanGetFsEvpnVxlanBgpRTEntry
 *                 pVxlandsFsEvpnVxlanBgpRTEntry
 *  Description :  This routine Gets the
 *                 value of needed.
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllUtlFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                     pVxlanGetFsEvpnVxlanBgpRTEntry,
                                     tVxlanFsEvpnVxlanBgpRTEntry *
                                     pVxlandsFsEvpnVxlanBgpRTEntry)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanBgpRTTable Entry\n"));
    UNUSED_PARAM (pVxlanGetFsEvpnVxlanBgpRTEntry);
    UNUSED_PARAM (pVxlandsFsEvpnVxlanBgpRTEntry);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanBgpRTTable Exit\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllUtlFsEvpnVxlanVrfTable
 Input       :  pVxlanGetFsEvpnVxlanVrfEntry
                pVxlandsFsEvpnVxlanVrfEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllUtlFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                   pVxlanGetFsEvpnVxlanVrfEntry,
                                   tVxlanFsEvpnVxlanVrfEntry *
                                   pVxlandsFsEvpnVxlanVrfEntry)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanVrfTable Entry\n"));
    UNUSED_PARAM (pVxlanGetFsEvpnVxlanVrfEntry);
    UNUSED_PARAM (pVxlandsFsEvpnVxlanVrfEntry);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanVrfTable Exit\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllUtlFsEvpnVxlanVrfRTTable
 Input       :  pVxlanGetFsEvpnVxlanVrfRTEntry
                pVxlandsFsEvpnVxlanVrfRTEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanGetAllUtlFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                     pVxlanGetFsEvpnVxlanVrfRTEntry,
                                     tVxlanFsEvpnVxlanVrfRTEntry *
                                     pVxlandsFsEvpnVxlanVrfRTEntry)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanVrfRTTable Entry\n"));
    UNUSED_PARAM (pVxlanGetFsEvpnVxlanVrfRTEntry);
    UNUSED_PARAM (pVxlandsFsEvpnVxlanVrfRTEntry);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanVrfRTTable Exit\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable
 Input       :  pVxlanGetFsEvpnVxlanMultihomedPeerTable
                pVxlandsFsEvpnVxlanMultihomedPeerTable
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanGetFsEvpnVxlanMultihomedPeerTable,
     tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlandsFsEvpnVxlanMultihomedPeerTable)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable Entry\n"));
    UNUSED_PARAM (pVxlanGetFsEvpnVxlanMultihomedPeerTable);
    UNUSED_PARAM (pVxlandsFsEvpnVxlanMultihomedPeerTable);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanGetAllUtlFsEvpnVxlanMultihomedPeerTable Exit\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  VxlanUtilUpdateFsEvpnVxlanEviVniMapTable
 * Input       :  pVxlanOldFsEvpnVxlanEviVniMapEntry
 *                pVxlanFsEvpnVxlanEviVniMapEntry
 *                pVxlanIsSetFsEvpnVxlanEviVniMapEntry
 * Descritpion :  This Routine checks set value
 *                with that of the value in database
 *                and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanUtilUpdateFsEvpnVxlanEviVniMapTable (tVxlanFsEvpnVxlanEviVniMapEntry *
                                          pVxlanOldFsEvpnVxlanEviVniMapEntry,
                                          tVxlanFsEvpnVxlanEviVniMapEntry *
                                          pVxlanFsEvpnVxlanEviVniMapEntry,
                                          tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                          pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanEviVniMapTable Entry\n"));
    switch (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
            i4FsEvpnVxlanEviVniMapRowStatus)
    {
        case ACTIVE:
            if (pVxlanOldFsEvpnVxlanEviVniMapEntry->MibObject.
                i4FsEvpnVxlanEviVniMapRowStatus != ACTIVE)
            {
                if (EvpnUtlHandleRowStatusActive
                    (pVxlanFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlHandleRowStatusActive Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
            if (pVxlanOldFsEvpnVxlanEviVniMapEntry == NULL)
            {
                if (EvpnUtlProcessTableCreation
                    (pVxlanFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlProcessTableCreation Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            else
            {
                if (EvpnUtlHandleRowStatusNotInService
                    (pVxlanFsEvpnVxlanEviVniMapEntry,
                     pVxlanIsSetFsEvpnVxlanEviVniMapEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlHandleRowStatusNotInService Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            break;

        case DESTROY:
            if (EvpnUtlProcessTableDeletion (pVxlanFsEvpnVxlanEviVniMapEntry,
                                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
                != VXLAN_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC:EvpnUtlProcessTableDeletion Failed\n"));
                return OSIX_FAILURE;
            }
            break;
        default:
            return OSIX_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanEviVniMapTable Exit\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  VxlanUtilUpdateFsEvpnVxlanBgpRTTable
 * Input       :  pVxlanOldFsEvpnVxlanBgpRTEntry
 *                pVxlanFsEvpnVxlanBgpRTEntry
 *                pVxlanIsSetFsEvpnVxlanBgpRTEntry
 * Descritpion :  This Routine checks set value
 *                with that of the value in database
 *                and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
VxlanUtilUpdateFsEvpnVxlanBgpRTTable (tVxlanFsEvpnVxlanBgpRTEntry *
                                      pVxlanOldFsEvpnVxlanBgpRTEntry,
                                      tVxlanFsEvpnVxlanBgpRTEntry *
                                      pVxlanFsEvpnVxlanBgpRTEntry,
                                      tVxlanIsSetFsEvpnVxlanBgpRTEntry *
                                      pVxlanIsSetFsEvpnVxlanBgpRTEntry)
{
    UINT1               u1RTParamType = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4RtIndex = 0;
    UINT4               u4VrfId = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanBgpRTTable Entry\n"));
    /* Get the Vrf ID from VNI */
    if (EvpnUtilGetVrfFromVni (pVxlanFsEvpnVxlanBgpRTEntry->
                               MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                               &u4VrfId) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%% Unable to get Vrf Index from Vni number\r\n"));
    }

    /* Handling of Row Status Change */
    if (pVxlanIsSetFsEvpnVxlanBgpRTEntry->bFsEvpnVxlanBgpRTRowStatus ==
        OSIX_TRUE)
    {
        switch (EVPN_P_BGP_RT_TYPE (pVxlanFsEvpnVxlanBgpRTEntry))
        {
            case EVPN_BGP_RT_IMPORT:
                u1RTParamType = EVPN_BGP4_IMPORT_RT;
                break;

            case EVPN_BGP_RT_EXPORT:
                u1RTParamType = EVPN_BGP4_EXPORT_RT;
                break;

            case EVPN_BGP_RT_BOTH:
                u1RTParamType = EVPN_BGP4_BOTH_RT;
                break;
            default:
                break;
        }

        u4ContextId = EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanBgpRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_L2VNI_RT);

        if ((EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) == ACTIVE)
            || (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
                CREATE_AND_WAIT))
        {
            /* Reserving the RT Index */
            EvpnUtilSetRtIndex (u4RtIndex,
                                pVxlanFsEvpnVxlanBgpRTEntry->MibObject.
                                u4FsEvpnVxlanBgpRTIndex, EVPN_L2VNI_RT);

            /* Notify with RT ADD notification to BGP */
            if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
                ACTIVE)
            {
                if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
                {
                    /* Since single VRF is used, EVI ID is passed as the default context of BGP */
                    (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                      EVPN_P_BGP_VNI
                                                      (pVxlanFsEvpnVxlanBgpRTEntry),
                                                      u1RTParamType,
                                                      EVPN_PARAM_RT_ADD,
                                                      EVPN_P_BGP_RT
                                                      (pVxlanFsEvpnVxlanBgpRTEntry),
                                                      EVPN_L2VNI_TYPE);
                }
                else
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "Cannot send the RT add notification to BGP.. since the callback is NULL\n"));
                }
            }
        }
        if ((EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
             NOT_IN_SERVICE)
            || (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
                NOT_READY))
        {
            /* Notify with RT DELETE notification to BGP */
            if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) ==
                ACTIVE)
            {
                if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
                {
                    /* Since single VRF is used, EVI ID is passed as the default context of BGP */
                    (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                      EVPN_P_BGP_VNI
                                                      (pVxlanFsEvpnVxlanBgpRTEntry),
                                                      u1RTParamType,
                                                      EVPN_PARAM_RT_DEL,
                                                      EVPN_P_BGP_RT
                                                      (pVxlanFsEvpnVxlanBgpRTEntry),
                                                      EVPN_L2VNI_TYPE);
                }
                else
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "Cannot send the RT delete notification to BGP.. since the callback is NULL\n"));
                }
            }
        }
        if (EVPN_P_BGP_RT_ROW_STATUS (pVxlanFsEvpnVxlanBgpRTEntry) == DESTROY)
        {
            EvpnUtilReleaseRtIndex (u4RtIndex,
                                    EVPN_P_BGP_RT_INDEX
                                    (pVxlanFsEvpnVxlanBgpRTEntry),
                                    EVPN_L2VNI_RT);
            if ((EVPN_P_BGP_RT_ROW_STATUS (pVxlanOldFsEvpnVxlanBgpRTEntry) ==
                 ACTIVE) && EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
            {
                /* Since single VRF is used, EVI ID is passed as the default context of BGP */
                (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                  EVPN_P_BGP_VNI
                                                  (pVxlanFsEvpnVxlanBgpRTEntry),
                                                  u1RTParamType,
                                                  EVPN_PARAM_RT_DEL,
                                                  EVPN_P_BGP_RT
                                                  (pVxlanFsEvpnVxlanBgpRTEntry),
                                                  EVPN_L2VNI_TYPE);
            }
        }
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanBgpRTTable Exit\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  VxlanUtilUpdateFsEvpnVxlanVrfTable
 * Input       :   pVxlanOldFsEvpnVxlanVrfEntry
                   pVxlanFsEvpnVxlanVrfEntry
                   pVxlanIsSetFsEvpnVxlanVrfEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanUtilUpdateFsEvpnVxlanVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                    pVxlanOldFsEvpnVxlanVrfEntry,
                                    tVxlanFsEvpnVxlanVrfEntry *
                                    pVxlanFsEvpnVxlanVrfEntry,
                                    tVxlanIsSetFsEvpnVxlanVrfEntry *
                                    pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UINT4               u4IRBIfIndex = 0;
    UINT2               u2VlanId = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanVrfTable Entry\n"));

    VxlanUtilGetVlanFromVni (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry),
                             &u2VlanId);
    u4IRBIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);
    switch (EVPN_P_VRF_ROW_STATUS (pVxlanFsEvpnVxlanVrfEntry))
    {
        case ACTIVE:
            if (EVPN_P_VRF_ROW_STATUS (pVxlanOldFsEvpnVxlanVrfEntry) != ACTIVE)
            {
                if (EvpnUtlHandleRowStatusActiveVrfTable
                    (pVxlanFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlHandleRowStatusActiveVrfTable Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            CfaInterfaceStatusChangeIndication ((UINT2) u4IRBIfIndex,
                                                CFA_IF_UP);
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
            if (pVxlanOldFsEvpnVxlanVrfEntry == NULL)
            {
                if (EvpnUtlProcessTableCreationVrfTable
                    (pVxlanFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlProcessTableCreationVrfTable Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            else
            {
                if (EvpnUtlHandleRowStatusNotInServiceVrfTable
                    (pVxlanFsEvpnVxlanVrfEntry,
                     pVxlanIsSetFsEvpnVxlanVrfEntry) != VXLAN_SUCCESS)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtlHandleRowStatusNotInServiceVrfTable Failed\n"));
                    return OSIX_FAILURE;
                }
            }
            break;
        case DESTROY:
            if (EvpnUtlProcessTableDeletionVrfTable (pVxlanFsEvpnVxlanVrfEntry,
                                                     pVxlanIsSetFsEvpnVxlanVrfEntry)
                != VXLAN_SUCCESS)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC:EvpnUtlProcessTableDeletionVrfTable Failed\n"));
                return OSIX_FAILURE;
            }
            CfaInterfaceStatusChangeIndication ((UINT2) u4IRBIfIndex,
                                                CFA_IF_DOWN);
            break;
        default:
            return OSIX_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanVrfTable Exit\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  VxlanUtilUpdateFsEvpnVxlanVrfRTTable
 * Input       :   pVxlanOldFsEvpnVxlanVrfRTEntry
                   pVxlanFsEvpnVxlanVrfRTEntry
                   pVxlanIsSetFsEvpnVxlanVrfRTEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
VxlanUtilUpdateFsEvpnVxlanVrfRTTable (tVxlanFsEvpnVxlanVrfRTEntry *
                                      pVxlanOldFsEvpnVxlanVrfRTEntry,
                                      tVxlanFsEvpnVxlanVrfRTEntry *
                                      pVxlanFsEvpnVxlanVrfRTEntry,
                                      tVxlanIsSetFsEvpnVxlanVrfRTEntry *
                                      pVxlanIsSetFsEvpnVxlanVrfRTEntry)
{
    UNUSED_PARAM (pVxlanOldFsEvpnVxlanVrfRTEntry);
    UINT4               u4ContextId = 0;
    UINT4               u4VrfId = 0;
    UINT4               u4RtIndex = 0;
    UINT1               u1RTParamType = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanVrfRTTable Entry\n"));
    /* Handling of Row Status Change */
    if (pVxlanIsSetFsEvpnVxlanVrfRTEntry->bFsEvpnVxlanVrfRTRowStatus ==
        OSIX_TRUE)
    {
        if (VcmIsVrfExist (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
                           &u4VrfId) == VXLAN_FALSE)
        {
            return OSIX_FAILURE;
        }
        switch (EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry))
        {
            case EVPN_BGP_RT_IMPORT:
                u1RTParamType = EVPN_BGP4_IMPORT_RT;
                break;

            case EVPN_BGP_RT_EXPORT:
                u1RTParamType = EVPN_BGP4_EXPORT_RT;
                break;

            case EVPN_BGP_RT_BOTH:
                u1RTParamType = EVPN_BGP4_BOTH_RT;
                break;
            default:
                break;
        }

        u4ContextId = EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_VRF_VNI_RT);

        if ((EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) == ACTIVE)
            || (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
                CREATE_AND_WAIT))
        {
            /* Reserving the RT Index */
            EvpnUtilSetRtIndex (u4RtIndex,
                                pVxlanFsEvpnVxlanVrfRTEntry->MibObject.
                                u4FsEvpnVxlanVrfRTIndex, EVPN_VRF_VNI_RT);
            /* Notify with RT ADD notification to BGP */
            if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
                ACTIVE)
            {
                if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
                {
                    /* Since single VRF is used, EVI ID is passed as the default context of BGP */
                    (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                      EVPN_P_VRF_RT_VNI
                                                      (pVxlanFsEvpnVxlanVrfRTEntry),
                                                      u1RTParamType,
                                                      EVPN_PARAM_RT_ADD,
                                                      EVPN_P_VRF_RT
                                                      (pVxlanFsEvpnVxlanVrfRTEntry),
                                                      EVPN_L3VNI_TYPE);
                    /* RT notify to Bgp for all the vlan-Vni associated with this Vrf */
                }
                else
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "Cannot send the RT add notification to BGP.. since the callback is NULL\n"));
                }
            }
        }
        if ((EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
             NOT_IN_SERVICE)
            || (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
                NOT_READY))
        {
            /* Notify with RT DELETE notification to BGP */
            if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) ==
                ACTIVE)
            {
                if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
                {
                    (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                      EVPN_P_VRF_RT_VNI
                                                      (pVxlanFsEvpnVxlanVrfRTEntry),
                                                      u1RTParamType,
                                                      EVPN_PARAM_RT_DEL,
                                                      EVPN_P_VRF_RT
                                                      (pVxlanFsEvpnVxlanVrfRTEntry),
                                                      EVPN_L3VNI_TYPE);
                }
                else
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "Cannot send the RT delete notification to BGP.. since the callback is NULL\n"));
                }
            }
        }

        if (EVPN_P_VRF_RT_ROW_STATUS (pVxlanFsEvpnVxlanVrfRTEntry) == DESTROY)
        {
            EvpnUtilReleaseRtIndex (u4RtIndex,
                                    EVPN_P_VRF_RT_INDEX
                                    (pVxlanFsEvpnVxlanVrfRTEntry),
                                    EVPN_VRF_VNI_RT);
            if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
            {
                (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                  EVPN_P_VRF_RT_VNI
                                                  (pVxlanFsEvpnVxlanVrfRTEntry),
                                                  u1RTParamType,
                                                  EVPN_PARAM_RT_DEL,
                                                  EVPN_P_VRF_RT
                                                  (pVxlanFsEvpnVxlanVrfRTEntry),
                                                  EVPN_L3VNI_TYPE);

            }
            else
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "Cannot send the RT delete notification to BGP.. since the callback is NULL\n"));
            }

        }
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanVrfRTTable Exit\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
 * Input       :   pVxlanOldFsEvpnVxlanMultihomedPeerTable
                   pVxlanFsEvpnVxlanMultihomedPeerTable
                   pVxlanIsSetFsEvpnVxlanMultihomedPeerTable
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable
    (tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanOldFsEvpnVxlanMultihomedPeerTable,
     tVxlanFsEvpnVxlanMultihomedPeerTable *
     pVxlanFsEvpnVxlanMultihomedPeerTable,
     tVxlanIsSetFsEvpnVxlanMultihomedPeerTable *
     pVxlanIsSetFsEvpnVxlanMultihomedPeerTable)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable Entry\n"));
    UNUSED_PARAM (pVxlanOldFsEvpnVxlanMultihomedPeerTable);
    UNUSED_PARAM (pVxlanFsEvpnVxlanMultihomedPeerTable);
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanMultihomedPeerTable);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanUtilUpdateFsEvpnVxlanMultihomedPeerTable Exit\n"));
    return OSIX_SUCCESS;
}

/* ************************************************************************ *
 *  Function Name   : EvpnVxlanUtilGetVniFromEvi                            *
 *  Description     : Gets VNI number from the EVI id                       *
 *  Input           : u4EviId - EVI id                                      *
 *  Output          : pu4VniId - VNI number                                 *
 *  Returns         : EVPN_FAILURE / EVPN_SUCCESS                           *
 ************************************************************************** */
INT4
EvpnVxlanUtilGetVniFromEvi (UINT4 u4EviId, UINT4 *pu4VniId)
{
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanUtilGetVniFromEvi Entry\n"));

    MEMSET (&EvpnEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        (INT4) u4EviId;

    pEvpnEviVniMapEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       (tRBElem *) & EvpnEviVniMapEntry, NULL);
    while (pEvpnEviVniMapEntry != NULL)
    {
        if (pEvpnEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex ==
            (INT4) u4EviId)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, " VNI entry found for EVI number -"
                        "%d\n", u4EviId));
            *pu4VniId =
                pEvpnEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber;
            return VXLAN_SUCCESS;
        }
        pEvpnEviVniMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           (tRBElem *) pEvpnEviVniMapEntry, NULL);
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, " VNI entry not found for EVI id -"
                "%d\n", u4EviId));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVxlanUtilGetVniFromEvi Exit\n"));

    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : EvpnDeleteRBTreeEntries                                *
 *  Description     : Deletes all the nodes from the EVPN RBTrees            *
 *  Input           : VOID                                                   *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */
VOID
EvpnDeleteRBTreeEntries ()
{
    tVxlanFsEvpnVxlanEviVniMapEntry *pEviVniMapEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry *pEvpnVxlanBgpRTEntry = NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMHPeerEntry = NULL;
    tVxlanFsVxlanEcmpNveEntry *pEvpnVxlanEcmpNveEntry = NULL;
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    UINT4               u4VrfId = 0;
    UINT1               u1RTParamType = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4RtIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnDeleteRBTreeEntries Entry\n"));

    /* Delete the Entries from RT table RBTreee */
    pEvpnVxlanBgpRTEntry = (tVxlanFsEvpnVxlanBgpRTEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable);
    while (pEvpnVxlanBgpRTEntry != NULL)
    {
        u4ContextId = 0;
        u4RtIndex = 0;
        u4ContextId = EVPN_P_BGP_VNI (pEvpnVxlanBgpRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_L2VNI_RT);
        EvpnUtilReleaseRtIndex (u4RtIndex,
                                EVPN_P_BGP_RT_INDEX (pEvpnVxlanBgpRTEntry),
                                EVPN_L2VNI_RT);

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                   pEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanBgpRTEntry);
        pEvpnVxlanBgpRTEntry =
            (tVxlanFsEvpnVxlanBgpRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsEvpnVxlanBgpRTTable);
    }

    /* Delete the Entries from EVI VNI map table RBTreee */
    pEviVniMapEntry = (tVxlanFsEvpnVxlanEviVniMapEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable);
    while (pEviVniMapEntry != NULL)
    {
        u4VrfId = 0;
        /* Get the Vrf ID from VNI */
        if (EvpnUtilGetVrfFromVni (pEviVniMapEntry->
                                   MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                                   &u4VrfId) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%% Unable to get Vrf Index from Vni number\r\n"));
        }

        if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
        {
            /* Send EVPN_OPER_DELETED trigger to BGP */
            (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                    EVPN_P_BGP_VNI
                                                    (pEviVniMapEntry),
                                                    EVPN_OPER_DELETED);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the oper-delete trigger to BGP\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   pEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pEviVniMapEntry);
        pEviVniMapEntry =
            (tVxlanFsEvpnVxlanEviVniMapEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                                VxlanGlbMib.
                                                                FsEvpnVxlanEviVniMapTable);
    }

    /* Delete the Entries from MH Peer table RBTreee */
    pEvpnVxlanMHPeerEntry =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst (gVxlanGlobals.
                                                                 VxlanGlbMib.
                                                                 FsEvpnVxlanMultihomedPeerTable);

    if (pEvpnVxlanMHPeerEntry != NULL)
    {
        VxlanHwUpdateMHPeerDatabase (0, 0, VXLAN_HW_DEL);
    }
    while (pEvpnVxlanMHPeerEntry != NULL)
    {
        TmrStopTimer (gVxlanGlobals.VxlanTimerList,
                      &(pEvpnVxlanMHPeerEntry->EvpnRouteTimer));
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   pEvpnVxlanMHPeerEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanMHPeerEntry);
        pEvpnVxlanMHPeerEntry =
            (tVxlanFsEvpnVxlanMultihomedPeerTable *)
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.
                            FsEvpnVxlanMultihomedPeerTable);
    }

    /* Delete the Entries from ECMP_NVE table RBTreee */
    pEvpnVxlanEcmpNveEntry = (tVxlanFsVxlanEcmpNveEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable);
    while (pEvpnVxlanEcmpNveEntry != NULL)
    {
        if (VxlanHwUpdateEcmpNveDatabase (pEvpnVxlanEcmpNveEntry, VXLAN_HW_DEL)
            == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_UTIL_TRC,
                        "Failed to delete EcmpNve entry from h/w\r\n"));
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                   pEvpnVxlanEcmpNveEntry);
        MemReleaseMemBlock (VXLAN_FSVXLANECMPNVETABLE_POOLID,
                            (UINT1 *) pEvpnVxlanEcmpNveEntry);
        pEvpnVxlanEcmpNveEntry =
            (tVxlanFsVxlanEcmpNveEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                          VxlanGlbMib.
                                                          FsVxlanEcmpNveTable);
    }

    pVxlanEvpnArpSupLocalMacEntry = RBTreeGetFirst (gVxlanGlobals.
                                                    VxlanEvpnArpSupLocalMacTable);
    if (pVxlanEvpnArpSupLocalMacEntry != NULL)
    {
        RBTreeRem (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                   pVxlanEvpnArpSupLocalMacEntry);
        MemReleaseMemBlock (VXLAN_EVPNARPSUPMACTABLE_POOLID,
                            (UINT1 *) pVxlanEvpnArpSupLocalMacEntry);
    }

    /* Delete the Entries in VRF RT Table */
    pVxlanFsEvpnVxlanVrfRTEntry = (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable);
    while (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
    {
        if ((VcmIsVrfExist
             (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
              &u4VrfId) == VCM_TRUE) && (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL))
        {
            switch (EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry))
            {
                case EVPN_BGP_RT_IMPORT:
                    u1RTParamType = EVPN_BGP4_IMPORT_RT;
                    break;

                case EVPN_BGP_RT_EXPORT:
                    u1RTParamType = EVPN_BGP4_EXPORT_RT;
                    break;

                case EVPN_BGP_RT_BOTH:
                    u1RTParamType = EVPN_BGP4_BOTH_RT;
                    break;
                default:
                    break;
            }

            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_RT_VNI
                                              (pVxlanFsEvpnVxlanVrfRTEntry),
                                              u1RTParamType, EVPN_PARAM_RT_DEL,
                                              EVPN_P_VRF_RT
                                              (pVxlanFsEvpnVxlanVrfRTEntry),
                                              EVPN_L3VNI_TYPE);
        }
        u4ContextId = 0;
        u4RtIndex = 0;
        u4ContextId = EVPN_P_VRF_RT_VNI (pVxlanFsEvpnVxlanVrfRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_VRF_VNI_RT);
        EvpnUtilReleaseRtIndex (u4RtIndex,
                                EVPN_P_VRF_RT_INDEX
                                (pVxlanFsEvpnVxlanVrfRTEntry), EVPN_VRF_VNI_RT);
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        pVxlanFsEvpnVxlanVrfRTEntry =
            (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsEvpnVxlanVrfRTTable);
    }

    /* Delete the VRF Table Entries */
    pVxlanFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
    while (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        u4VrfId = 0;
        /* Get the Vrf ID from VNI */
        if (EvpnUtilGetVrfFromVni (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry),
                                   &u4VrfId) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%% Unable to get Vrf Index from Vni number\r\n"));
        }

        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            /* Notify with RD Route Params to BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_VNI
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_DEL,
                                              EVPN_P_VRF_RD
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_L3VNI_TYPE);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD ADD trigger to BGP.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        pVxlanFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnDeleteRBTreeEntries Exit\n"));
}

/* ************************************************************************* *
 *  Function Name   : EvpnUtilIsSetVniMapEntry                               *
 *  Description     : Check for the Vlan-VNI map entry                       *
 *  Input           : u4VniIndex - VNI id                                    *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnUtilIsSetVniMapEntry (UINT4 u4VniIndex)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    INT4                i4FsVxlanVniVlanMapVlanId = EVPN_ZERO;
    INT4                i4NextFsVxlanVniVlanMapVlanId = EVPN_ZERO;
    INT4                i4RetVal = EVPN_ZERO;
    BOOL1               bVniEntryExists = VXLAN_FALSE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilIsSetVniMapEntry Entry\n"));
    MEMSET (&VxlanFsVxlanVniVlanMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    i4RetVal =
        nmhGetFirstIndexFsVxlanVniVlanMapTable (&i4FsVxlanVniVlanMapVlanId);
    while (SNMP_SUCCESS == i4RetVal)
    {
        VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            i4FsVxlanVniVlanMapVlanId;
        i4RetVal =
            VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry);
        if (VxlanFsVxlanVniVlanMapEntry.MibObject.
            u4FsVxlanVniVlanMapVniNumber == u4VniIndex)
        {
            if (bVniEntryExists == VXLAN_FALSE)
            {
                bVniEntryExists = VXLAN_TRUE;
            }
        }
        i4RetVal = nmhGetNextIndexFsVxlanVniVlanMapTable
            (i4FsVxlanVniVlanMapVlanId, &i4NextFsVxlanVniVlanMapVlanId);
        if (i4RetVal == SNMP_SUCCESS)
        {
            i4FsVxlanVniVlanMapVlanId = i4NextFsVxlanVniVlanMapVlanId;
        }
    }
    if (bVniEntryExists == VXLAN_TRUE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "VLAN-VNI Entry Found\n"));
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnUtilIsSetVniMapEntry Exit\n"));
        return VXLAN_SUCCESS;
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "VLAN-VNI Entry Does Not Found\n"));
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnUtilIsSetVniMapEntry Exit\n"));
        return VXLAN_FAILURE;
    }
}

/* ************************************************************************* *
 *  Function Name   : EvpnUtilUpdateStats                                    *
 *  Description     : Updates the Statistics                                 *
 *  Input           : u4VniIndex - VLAN id                                   *
 *                    i4EviIndex - EVPN id                                   *
 *                    u1StatType - Statistics type                           *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */
VOID
EvpnUtilUpdateStats (UINT4 u4VniIndex, INT4 i4EviIndex, UINT1 u1StatType)
{
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilUpdateStats: Entry\n"));

    MEMSET (&EvpnVxlanEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4VniIndex;
    EvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4EviIndex;

    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);

    if (pEvpnEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, " No VNI entry found for EVPN id -"
                    "%d\n", i4EviIndex));
        return;
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, "Updates the statistics\n"));

    switch (u1StatType)
    {
        case EVPN_STAT_PKT_SENT:
            /* Update the Packet sent counter */
            pEvpnEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapSentPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of EVPN packet sent -"
                        "%d\n", pEvpnEviVniMapEntry->MibObject.
                        u4FsEvpnVxlanEviVniMapSentPkts));
            break;

        case EVPN_STAT_PKT_RCVD:
            /* Update the Packet received counter */
            pEvpnEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapRcvdPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of EVPN packet Received -"
                        "%d\n", pEvpnEviVniMapEntry->MibObject.
                        u4FsEvpnVxlanEviVniMapRcvdPkts));
            break;

        case EVPN_STAT_PKT_DRPD:
            /* Update the Packet dropped counter */
            pEvpnEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapDroppedPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of EVPN packet Droppped -"
                        "%d\n", pEvpnEviVniMapEntry->MibObject.
                        u4FsEvpnVxlanEviVniMapDroppedPkts));
            break;

        default:
            break;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilUpdateStats: Exit\n"));
    return;
}

/* ************************************************************************* *
 *  Function Name   : VrfUtilUpdateStats                                     *
 *  Description     : Updates the Statistics                                 *
 *  Input           : u4VniIndex - VLAN id                                   *
 *                    i4EviIndex - EVPN id                                   *
 *                    u1StatType - Statistics type                           *
 *  Output          : None                                                   *
 *  Returns         : None                                                   *
 * ************************************************************************* */
VOID
VrfUtilUpdateStats (UINT4 u4VniNumber, UINT1 *pVrfName, UINT1 u1StatType)
{
    tVxlanFsEvpnVxlanVrfEntry EvpnVxlanVrfVniMapEntry;
    tVxlanFsEvpnVxlanVrfEntry *pEvpnVrfVniMapEntry = NULL;
    UINT4               u4VrfNameLength = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VrfUtilUpdateStats: Entry\n"));

    MEMSET (&EvpnVxlanVrfVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanVrfEntry));

    EvpnVxlanVrfVniMapEntry.MibObject.u4FsEvpnVxlanVrfVniNumber = u4VniNumber;
    MEMCPY (&(EvpnVxlanVrfVniMapEntry.MibObject.au1FsEvpnVxlanVrfName),
            pVrfName, EVPN_MAX_VRF_NAME_LEN);
    u4VrfNameLength = STRLEN (pVrfName);
    pEvpnVrfVniMapEntry =
        EvpnApiGetVrfTableEntry (pVrfName, u4VrfNameLength, u4VniNumber);
    /*RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
       (tRBElem *) &EvpnVxlanVrfVniMapEntry); */

    if (pEvpnVrfVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, " No VNI entry found for VRF -"
                    "%s\n", pVrfName));
        return;
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, "Updates the statistics\n"));

    switch (u1StatType)
    {
        case VRF_STAT_PKT_SENT:
            /* Update the Packet sent counter */
            pEvpnVrfVniMapEntry->MibObject.u4FsEvpnVxlanVrfVniMapSentPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of VRF-EVPN packet sent -"
                        "%d\n", pEvpnVrfVniMapEntry->MibObject.
                        u4FsEvpnVxlanVrfVniMapSentPkts));
            break;

        case VRF_STAT_PKT_RCVD:
            /* Update the Packet received counter */
            pEvpnVrfVniMapEntry->MibObject.u4FsEvpnVxlanVrfVniMapRcvdPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of VRF-EVPN packet Received -"
                        "%d\n", pEvpnVrfVniMapEntry->MibObject.
                        u4FsEvpnVxlanVrfVniMapRcvdPkts));
            break;

        case VRF_STAT_PKT_DRPD:
            /* Update the Packet dropped counter */
            pEvpnVrfVniMapEntry->MibObject.u4FsEvpnVxlanVrfVniMapDroppedPkts++;
            VXLAN_TRC ((VXLAN_EVPN_TRC, "Number of VRF-EVPN packet Droppped -"
                        "%d\n", pEvpnVrfVniMapEntry->MibObject.
                        u4FsEvpnVxlanVrfVniMapDroppedPkts));
            break;

        default:
            break;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VrfUtilUpdateStats: Exit\n"));
    return;
}

/* ************************************************************************* *
 *  Function Name   : EvpnUtilGetEviFromVni                                  *
 *  Description     : Gets EVI number from the VNI number                    *
 *  Input           : u4VniIndex - VNI id                                    *
 *  Output          : pi4EviIndex - EVI number                               *
 *  Returns         : VXLAN_FAILURE / VXLAN_SUCCESS                          *
 * ************************************************************************* */
INT4
EvpnUtilGetEviFromVni (UINT4 u4VniIndex, INT4 *pi4EviIndex)
{
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetEviFromVni Entry\n"));

    MEMSET (&EvpnEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;

    pEvpnEviVniMapEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       (tRBElem *) & EvpnEviVniMapEntry, NULL);
    while (pEvpnEviVniMapEntry != NULL)
    {
        if (pEvpnEviVniMapEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber ==
            u4VniIndex)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, " EVI entry found for VNI number -"
                        "%d\n", u4VniIndex));
            *pi4EviIndex =
                pEvpnEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex;
            return VXLAN_SUCCESS;
        }
        pEvpnEviVniMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           (tRBElem *) pEvpnEviVniMapEntry, NULL);
    }

    VXLAN_TRC ((VXLAN_EVPN_TRC, " EVI entry not found for VNI id -"
                "%d\n", u4VniIndex));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetEviFromVni Exit\n"));

    return VXLAN_FAILURE;
}

/* ********************************************************** *
 * Function   : EvpnUtilFindNextFreeRtIndex                   *
 * Description: This function finds the next free RT index    *
 *              for the given BGP context ID.                 *
 * Input      : u4ContextId                                   *
 * Output     : pu4FreeIndex                                  *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                   *
 * ********************************************************** */
UINT4
EvpnUtilFindNextFreeRtIndex (UINT4 u4ContextId, UINT4 *pu4FreeIndex,
                             INT4 i4RTType)
{
    UINT1               u1BitNdx = EVPN_ZERO;
    UINT2               u2NofUINT4 = EVPN_ZERO;
    UINT4               u4MaxU4 = EVPN_ZERO;
    UINT4              *pBitmap = NULL;
    UINT4               u4RtIndex = EVPN_ZERO;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilFindNextFreeRtIndex Entry\n"));
    EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, i4RTType);

    VXLAN_TRC ((VXLAN_EVPN_TRC,
                "FUNC:RT Index -%d found for the given Context\n", u4RtIndex));
    if (i4RTType == EVPN_L2VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnRtBitMapIdx[u4RtIndex];
        u4MaxU4 = MAX_VXLAN_EVPN_VNI;
    }
    else if (i4RTType == EVPN_VRF_VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnVrfRtBitMapIdx[u4RtIndex];
        u4MaxU4 = MAX_VXLAN_EVPN_VRF;
    }

    for (u2NofUINT4 = EVPN_ZERO; u2NofUINT4 < u4MaxU4; u2NofUINT4++)
    {
        if (EVPN_UTIL_UINT4_MAX != pBitmap[u2NofUINT4])
        {
            for (u1BitNdx = EVPN_ZERO; u1BitNdx < EVPN_NUM_OF_BITS_IN_UINT4;
                 u1BitNdx++)
            {
                if (((u2NofUINT4 * EVPN_NUM_OF_BITS_IN_UINT4) +
                     u1BitNdx) > EVPN_MAX_NUM_RT_PER_VRF)
                {
                    break;
                }
                if (!((pBitmap[u2NofUINT4] >> u1BitNdx) & 0x1))
                {
                    *pu4FreeIndex =
                        (UINT4) ((u2NofUINT4 *
                                  EVPN_NUM_OF_BITS_IN_UINT4) + u1BitNdx);
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:Free Index -%d found for the given Context\n",
                                *pu4FreeIndex));
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "FUNC:EvpnUtilFindNextFreeRtIndex Exit\n"));
                    return VXLAN_SUCCESS;
                }
            }
        }
    }
    VXLAN_TRC ((VXLAN_EVPN_TRC,
                "FUNC:Free Index does not found for the given Context\n"));
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilFindNextFreeRtIndex Exit\n"));
    return VXLAN_FAILURE;
}

/* ********************************************************** *
 * Function   : EvpnUtilGetRtIndex                      *
 * Description: This function calculates the RT index based   *
 *              on the given context ID.                      *
 * Input      : u4ContextId                                   *
 * Output     : pu4RtIndex                                    *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                   *
 * ********************************************************** */

INT4
EvpnUtilGetRtIndex (UINT4 u4ContextId, UINT4 *pu4RtIndex, INT4 i4RTType)
{
    UINT4               u4RtIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetRtIndex Entry\n"));
    if (i4RTType == EVPN_L2VNI_RT)
    {
        while (gVxlanGlobals.gau4EvpnRtIndex[u4RtIndex] != VXLAN_FALSE)
        {
            if (u4RtIndex >= MAX_VXLAN_EVPN_VNI)
            {
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:RT Index does not found\n"));
                return VXLAN_FAILURE;
            }
            if (gVxlanGlobals.gau4EvpnRtIndex[u4RtIndex] == u4ContextId)
            {
                break;
            }
            u4RtIndex++;
        }
        gVxlanGlobals.gau4EvpnRtIndex[u4RtIndex] = u4ContextId;
    }
    if (i4RTType == EVPN_VRF_VNI_RT)
    {
        while (gVxlanGlobals.gau4EvpnVrfRtIndex[u4RtIndex] != VXLAN_FALSE)
        {
            if (u4RtIndex >= MAX_VXLAN_EVPN_VRF)
            {
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:Vrf RT Index does not found\n"));
                return VXLAN_FAILURE;
            }
            if (gVxlanGlobals.gau4EvpnVrfRtIndex[u4RtIndex] == u4ContextId)
            {
                break;
            }
            u4RtIndex++;
        }
        gVxlanGlobals.gau4EvpnVrfRtIndex[u4RtIndex] = u4ContextId;
    }
    *pu4RtIndex = u4RtIndex;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetRtIndex Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************** *
 * Function   : EvpnUtilParseAndGenerateESI                       *
 * Description: This function takes random string as input        *
 *              and generate valid ESI Value                      *
 * Input      : pu1RandomString - Random strong (ASN:nn or IP:nn) *
 * Output     : pu1ESIVal - Generated ESI Value                   *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
INT4
EvpnUtilParseAndGenerateESI (UINT1 *pu1RandomString, UINT1 *pu1ESIVal)
{
    UINT1               au1InputString[EVPN_MAX_ESI_STR_LEN + 1];
    UINT4               u4MaxLoop = 0;
    UINT4               u4LoopIndex = 0;
    INT4                i4NumberOfDigits = -1;
    INT4                i4Index = 0;
    UINT2               u2ESIVal = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilParseAndGenerateESI Entry\n"));
    MEMSET (au1InputString, EVPN_ZERO, sizeof (au1InputString));

    if (NULL == pu1RandomString)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:Input String is NULL\n"));
        return VXLAN_FAILURE;
    }

    STRNCPY (au1InputString, pu1RandomString, (EVPN_MAX_ESI_STR_LEN));
    au1InputString[EVPN_MAX_ESI_STR_LEN] = '\0';
    u4MaxLoop = STRLEN (au1InputString);

    for (u4LoopIndex = EVPN_ZERO; u4LoopIndex < u4MaxLoop; u4LoopIndex++)
    {
        i4NumberOfDigits++;
        if (au1InputString[u4LoopIndex] == '.')
        {
            return VXLAN_FAILURE;
        }
        if (au1InputString[u4LoopIndex] != ':')
        {
            if (!(ISDIGIT (au1InputString[u4LoopIndex])))
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Input is not a digit\n"));
                return VXLAN_FAILURE;
            }
        }
        if (au1InputString[u4LoopIndex] == ':')
        {
            if (i4NumberOfDigits > 2)
            {
                return VXLAN_FAILURE;
            }
            i4NumberOfDigits = -1;

            u2ESIVal =
                (UINT2) strtoul ((const char *) pu1RandomString, NULL, 0);
            MEMCPY (&pu1ESIVal[i4Index], &u2ESIVal, sizeof (UINT2));
            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            i4Index++;
        }
    }
    /* Copying the Last bit */
    u2ESIVal = (UINT2) strtoul ((const char *) pu1RandomString, NULL, 0);
    MEMCPY (&pu1ESIVal[i4Index], &u2ESIVal, sizeof (UINT2));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilParseAndGenerateESI Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************** *
 * Function   : EvpnCliParseAndGenerateRdRt                       *
 * Description: This function takes random string as input        *
 *              and generate valid RD/RT Value                    *
 * Input      : pu1RandomString - Random strong (ASN:nn or IP:nn) *
 * Output     : pu1RdRt - Generated Rd Value                      *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
INT4
EvpnUtilParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt)
{
    UINT4               u4AsnValue = EVPN_ZERO;
    UINT4               u4nnValue = EVPN_ZERO;
    UINT4               u4LoopIndex = EVPN_ZERO;
    UINT4               u4IpIndex = EVPN_ZERO;
    UINT2               u2AsnValue = EVPN_ZERO;
    UINT2               u2nnValue = EVPN_ZERO;
    UINT1               au1InputString[EVPN_MAX_RD_RT_STRING_LEN + 1];
    UINT4               au4IPValue[EVPN_IPV4_ADDR_LENGTH];
    UINT1               u1IsColonFound = OSIX_FALSE;
    UINT1               u1IsDotFound = OSIX_FALSE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilParseAndGenerateRdRt Entry\n"));
    MEMSET (au1InputString, EVPN_ZERO, sizeof (au1InputString));
    MEMSET (au4IPValue, EVPN_ZERO, EVPN_IPV4_ADDR_LENGTH);

    if (NULL == pu1RandomString)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:Input String is NULL\n"));
        return VXLAN_FAILURE;
    }

    STRNCPY (au1InputString, pu1RandomString, EVPN_MAX_RD_RT_STRING_LEN);
    au1InputString[EVPN_MAX_RD_RT_STRING_LEN] = '\0';

    for (u4LoopIndex = EVPN_ZERO; u4LoopIndex < STRLEN (au1InputString);
         u4LoopIndex++)
    {
        if (au1InputString[u4LoopIndex] == '.')
        {
            if (OSIX_TRUE == u1IsColonFound)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC:Input string is wrong, '.' colon does not found\n"));
                return VXLAN_FAILURE;
            }

            pu1RdRt[0] = EVPN_RT_TYPE_1;
            au4IPValue[u4IpIndex] = (UINT4) ATOI (pu1RandomString);
            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsDotFound = OSIX_TRUE;
            u4IpIndex++;

            if (EVPN_IPV4_ADDR_LENGTH == u4IpIndex)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC:IP Index address length is wrong\n"));
                return VXLAN_FAILURE;
            }
            continue;
        }

        if (au1InputString[u4LoopIndex] == ':')
        {
            if (OSIX_TRUE == u1IsColonFound)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC:Input string is wrong, ':' colon does not found\n"));
                return VXLAN_FAILURE;
            }

            if (OSIX_FALSE == u1IsDotFound)
            {
                u4AsnValue = (UINT4) ATOI (pu1RandomString);
                if (EVPN_UINT2_MAX_VALUE < u4AsnValue)
                {
                    pu1RdRt[0] = EVPN_RT_TYPE_2;
                }
                else
                {
                    pu1RdRt[0] = EVPN_RT_TYPE_0;
                }
            }
            else
            {
                au4IPValue[u4IpIndex] = (UINT4) (ATOI (pu1RandomString));
                pu1RandomString = &au1InputString[u4LoopIndex + 1];
                u4IpIndex++;
                if (EVPN_IPV4_ADDR_LENGTH != u4IpIndex)
                {
                    if (2 == u4IpIndex)
                    {
                        pu1RdRt[0] = EVPN_RT_TYPE_2;
                    }
                    else
                    {
                        VXLAN_TRC ((VXLAN_EVPN_TRC,
                                    "FUNC:IP Index address length is wrong\n"));
                        return VXLAN_FAILURE;
                    }
                }
            }

            if (u4LoopIndex == (STRLEN (au1InputString) - 1))
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC: Loop Index does not exists\n"));
                return VXLAN_FAILURE;
            }

            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsColonFound = OSIX_TRUE;
            continue;
        }

        if (!(ISDIGIT (au1InputString[u4LoopIndex])))
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Input is not a digit\n"));
            return VXLAN_FAILURE;
        }
    }

    if (OSIX_TRUE != u1IsColonFound)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Colon is not Found\n"));
        return VXLAN_FAILURE;
    }

    u4nnValue = (UINT4) ATOI (pu1RandomString);

    if (EVPN_RT_TYPE_0 == pu1RdRt[0])
    {
        u2AsnValue = (UINT2) u4AsnValue;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4nnValue = OSIX_HTONL (u4nnValue);

        MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RdRt[4], &u4nnValue, sizeof (UINT4));
    }
    if (EVPN_RT_TYPE_1 == pu1RdRt[0])
    {
        if (EVPN_UINT2_MAX_VALUE < u4nnValue)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Given nnValue is wrorng\n"));
            return VXLAN_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u2nnValue = OSIX_HTONS (u2nnValue);

        if (EVPN_UINT1_MAX_VALUE < au4IPValue[0] ||
            EVPN_UINT1_MAX_VALUE < au4IPValue[1] ||
            EVPN_UINT1_MAX_VALUE < au4IPValue[2] ||
            EVPN_UINT1_MAX_VALUE < au4IPValue[3])
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Given IP Value is wrorng\n"));
            return VXLAN_FAILURE;
        }

        MEMCPY (&pu1RdRt[2], &au4IPValue[0], sizeof (UINT1));
        MEMCPY (&pu1RdRt[3], &au4IPValue[1], sizeof (UINT1));
        MEMCPY (&pu1RdRt[4], &au4IPValue[2], sizeof (UINT1));
        MEMCPY (&pu1RdRt[5], &au4IPValue[3], sizeof (UINT1));
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    if (EVPN_RT_TYPE_2 == pu1RdRt[0])
    {
        if (EVPN_UINT2_MAX_VALUE < u4nnValue)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC: Given nnValue is wrorng\n"));
            return VXLAN_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u4AsnValue = OSIX_HTONL (u4AsnValue);
        u2nnValue = OSIX_HTONS (u2nnValue);

        if (OSIX_TRUE == u1IsDotFound)
        {
            if (EVPN_UINT2_MAX_VALUE < au4IPValue[0] ||
                EVPN_UINT2_MAX_VALUE < au4IPValue[1])
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "FUNC: Given IP Value is wrorng\n"));
                return VXLAN_FAILURE;
            }

            MEMCPY (&u2AsnValue, &au4IPValue[0], sizeof (UINT2));
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));

            MEMCPY (&u2AsnValue, &au4IPValue[1], sizeof (UINT2));
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[4], &u2AsnValue, sizeof (UINT2));
        }
        else
        {
            MEMCPY (&pu1RdRt[2], &u4AsnValue, sizeof (UINT4));
        }
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilParseAndGenerateRdRt Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************** *
 * Function   : EvpnUtilGetESIFromVniMapTable                     *
 * Description: This function returns the ESI entry for the       *
 *              given EVI Index and VNI Index                     *
 * Input      : pu4EviIndex - EVI Index                           *
 *            : pu4VniIndex - VNI Index                           *
 * Output     : pu1ESIVal - ESI Value                             *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilGetESIFromVniMapTable (UINT4 u4EviIndex, UINT4 u4VniIndex,
                               UINT1 *pu1ESIVal)
{
    ULONG8              u8RdValue = EVPN_ZERO;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetESIFromVniMapTable: Entry\n"));

    MEMSET (&EvpnEviVniMapEntry, 0, sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = (INT4) u4EviIndex;

    EvpnEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnEviVniMapEntry);

    if (pEvpnEviVniMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No EVI entry found for VNI id -"
                         "%d\n", u4VniIndex));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " ESI entry found for VNI id -"
                     "%d\n", u4VniIndex));

    MEMCPY (pu1ESIVal, EVPN_P_BGP_ESI (pEvpnEviVniMapEntry),
            EVPN_P_BGP_ESI_LEN (pEvpnEviVniMapEntry));
    /* ESI Value Validation */
    MEMCPY (&u8RdValue, &pu1ESIVal, EVPN_P_BGP_ESI_LEN (pEvpnEviVniMapEntry));
    if (EVPN_ZERO == u8RdValue)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnUtilGetESIFromVniMapTable: ESI does not exist\n"));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetESIFromVniMapTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************** *
 * Function   : EvpnUtilGetRdFromVniMapTable                      *
 * Description: This function returns the RD entry for the        *
 *              given EVI Index and VNI Index                     *
 * Input      : pu4EviIndex - EVI Index                           *
 *            : pu4VniIndex - VNI Index                           *
 * Output     : pu1VniRD - Rd Value                               *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilGetRdFromVniMapTable (UINT4 u4EviIndex, UINT4 u4VniIndex,
                              UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto)
{
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetRdFromVniMapTable: Entry\n"));

    MEMSET (&EvpnEviVniMapEntry, 0, sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = (INT4) u4EviIndex;

    EvpnEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnEviVniMapEntry);

    if (pEvpnEviVniMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No RD entry found for VNI id -"
                         "%d\n", u4VniIndex));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " RD entry found for VNI id -"
                     "%d\n", u4VniIndex));

    MEMCPY (pu1VniRD, EVPN_P_BGP_RD (pEvpnEviVniMapEntry),
            EVPN_P_BGP_RD_LEN (pEvpnEviVniMapEntry));
    MEMCPY (pu1VniRD,
            pEvpnEviVniMapEntry->MibObject.au1FsEvpnVxlanEviVniMapBgpRD,
            pEvpnEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDLen);
    if (EVPN_P_BGP_RD_AUTO (pEvpnEviVniMapEntry) == OSIX_TRUE)
    {
        *pi4BgpRdAuto = OSIX_TRUE;
    }

    *pi4BgpRdAuto = OSIX_FALSE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetRdFromVniMapTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************** *
 * Function   : EvpnUtilCheckRdVniMapTable                        *
 * Description: This function returns the RD entry for the        *
 *              given EVI Index and VNI Index                     *
 * Input      : pu4EviIndex - EVI Index                           *
 *            : pu4VniIndex - VNI Index                           *
 *              pu1VniRD - RD                                     *
 *              pi4BgpRdAuto -RD Type                             *
 * Output     : pu1VniRD - Rd Value                               *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilCheckRdVniMapTable (UINT4 u4EviIndex, UINT4 u4VniIndex,
                            UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto)
{
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilCheckRdVniMapTable: Entry\n"));

    MEMSET (&EvpnEviVniMapEntry, 0, sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    EvpnEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = (INT4) u4EviIndex;

    EvpnEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnEviVniMapEntry);

    if (pEvpnEviVniMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No RD entry found for VNI id -"
                         "%d\n", u4VniIndex));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " RD entry found for VNI id -"
                     "%d\n", u4VniIndex));

    if (MEMCMP (pu1VniRD, EVPN_P_BGP_RD (pEvpnEviVniMapEntry),
                EVPN_P_BGP_RD_LEN (pEvpnEviVniMapEntry)) == 0 &&
        (pEvpnEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapBgpRDAuto ==
         *pi4BgpRdAuto))
    {
        return VXLAN_SUCCESS;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilCheckRdVniMapTable: Exit\n"));
    return VXLAN_FAILURE;
}

/* ************************************************************** *
 * Function   : EvpnUtilCheckRdVrfVniMapTable                     *
 * Description: This function returns the RD entry for the        *
 *              given EVI Index and VNI Index                     *
 * Input      : pu1VrfName - VRF Name                             *
 *            : pu4VniIndex - VNI Index                           *
 *              pu1VniRD - RD                                     *
 *              pi4BgpRdAuto -RD Type                             *
 * Output     : pu1VniRD - Rd Value                               *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilCheckRdVrfVniMapTable (UINT1 *pu1VrfName, UINT4 u4VniIndex,
                               UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto)
{
    tVxlanFsEvpnVxlanVrfEntry EvpnVrfVniMapEntry;
    tVxlanFsEvpnVxlanVrfEntry *pEvpnVrfVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilCheckRdVrfVniMapTable: Entry\n"));

    MEMSET (&EvpnVrfVniMapEntry, 0, sizeof (tVxlanMibFsEvpnVxlanVrfEntry));

    MEMCPY (EvpnVrfVniMapEntry.MibObject.
            au1FsEvpnVxlanVrfName, pu1VrfName, EVPN_MAX_VRF_NAME_LEN);
    EVPN_VRF_VNI (EvpnVrfVniMapEntry) = u4VniIndex;
    EVPN_VRF_NAME_LEN (EvpnVrfVniMapEntry) = STRLEN (pu1VrfName);
    pEvpnVrfVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) & EvpnVrfVniMapEntry);

    if (pEvpnVrfVniMapEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No RD entry found for VNI id -"
                         "%d\n", u4VniIndex));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " RD entry found for VNI id -"
                     "%d\n", u4VniIndex));

    if (MEMCMP (pu1VniRD, EVPN_P_VRF_RD (pEvpnVrfVniMapEntry),
                EVPN_P_VRF_RD_LEN (pEvpnVrfVniMapEntry)) == 0 &&
        (pEvpnVrfVniMapEntry->MibObject.i4FsEvpnVxlanVrfRDAuto ==
         *pi4BgpRdAuto))
    {

        return VXLAN_SUCCESS;
    }

    pVxlanEvpnArpSupLocalMacEntry = RBTreeGetFirst (gVxlanGlobals.
                                                    VxlanEvpnArpSupLocalMacTable);
    if (pVxlanEvpnArpSupLocalMacEntry != NULL)
    {
        RBTreeRem (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                   pVxlanEvpnArpSupLocalMacEntry);
        MemReleaseMemBlock (VXLAN_EVPNARPSUPMACTABLE_POOLID,
                            (UINT1 *) pVxlanEvpnArpSupLocalMacEntry);
    }

    /* Delete the Entries in VRF RT Table */
    pVxlanFsEvpnVxlanVrfRTEntry = (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable);
    while (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
    {
        if ((VcmIsVrfExist
             (EVPN_P_VRF_RT_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
              &u4VrfId) == VCM_TRUE) && (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL))
        {
            switch (EVPN_P_VRF_RT_TYPE (pVxlanFsEvpnVxlanVrfRTEntry))
            {
                case EVPN_BGP_RT_IMPORT:
                    u1RTParamType = EVPN_BGP4_IMPORT_RT;
                    break;

                case EVPN_BGP_RT_EXPORT:
                    u1RTParamType = EVPN_BGP4_EXPORT_RT;
                    break;

                case EVPN_BGP_RT_BOTH:
                    u1RTParamType = EVPN_BGP4_BOTH_RT;
                    break;
                default:
                    break;
            }

            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_RT_VNI
                                              (pVxlanFsEvpnVxlanVrfRTEntry),
                                              u1RTParamType, EVPN_PARAM_RT_DEL,
                                              EVPN_P_VRF_RT
                                              (pVxlanFsEvpnVxlanVrfRTEntry),
                                              EVPN_L3VNI_TYPE);
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   pVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfRTEntry);
        pVxlanFsEvpnVxlanVrfRTEntry =
            (tVxlanFsEvpnVxlanVrfRTEntry *) RBTreeGetFirst (gVxlanGlobals.
                                                            VxlanGlbMib.
                                                            FsEvpnVxlanVrfRTTable);
    }

    /* Delete the VRF Table Entries */
    pVxlanFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
    while (pVxlanFsEvpnVxlanVrfEntry != NULL)
    {
        u4VrfId = 0;
        /* Get the Vrf ID from VNI */
        if (EvpnUtilGetVrfFromVni (EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry),
                                   &u4VrfId) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%% Unable to get Vrf Index from Vni number\r\n"));
        }

        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            /* Notify with RD Route Params to BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_VNI
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_DEL,
                                              EVPN_P_VRF_RD
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_L3VNI_TYPE);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD ADD trigger to BGP.\r\n"));
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   pVxlanFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pVxlanFsEvpnVxlanVrfEntry);
        pVxlanFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilCheckRdVrfVniMapTable: Exit\n"));
    return VXLAN_FAILURE;
}

/* ************************************************************** *
 * Function   : EvpnUtilSetRtIndex                                *
 * Description: This function sets the RT index in the global     *
 *              bitmap (used for reservation)                     *
 * Input      : u4ContextId                                       *
 *              u4RtIndex                                         *
 * Output     : NONE                                              *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilSetRtIndex (UINT4 u4ContextId, UINT4 u4RtIndex, INT4 i4RTType)
{
    UINT4              *pBitmap = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilSetRtIndex: Entry\n"));
    if (i4RTType == EVPN_L2VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnRtBitMapIdx[u4ContextId];
    }
    else if (i4RTType == EVPN_VRF_VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnVrfRtBitMapIdx[u4ContextId];
    }
    if (NULL != pBitmap)
    {

        pBitmap[u4RtIndex / EVPN_NUM_OF_BITS_IN_UINT4] =
            (UINT4) ((UINT4) (pBitmap[(UINT4) (u4RtIndex /
                                               EVPN_NUM_OF_BITS_IN_UINT4)]) |
                     (UINT4) (1 << ((UINT4) (u4RtIndex %
                                             EVPN_NUM_OF_BITS_IN_UINT4))));
        return VXLAN_SUCCESS;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilSetRtIndex: Exit\n"));
    return VXLAN_FAILURE;
}

/* ************************************************************** *
 * Function   : EvpnUtilReleaseRtIndex                            *
 * Description: This function release the RT index for the given  *
 *              VRF context Id                                    *
 * Input      : u4ContextId                                       *
 *              u4RtIndex                                         *
 * Output     : NONE                                              *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilReleaseRtIndex (UINT4 u4ContextId, UINT4 u4RtIndex, INT4 i4RTType)
{
    UINT4              *pBitmap = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilReleaseRtIndex: Entry\n"));
    if (i4RTType == EVPN_L2VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnRtBitMapIdx[u4ContextId];
    }
    else if (i4RTType == EVPN_VRF_VNI_RT)
    {
        pBitmap = gVxlanGlobals.gau4EvpnVrfRtBitMapIdx[u4ContextId];
    }
    if (NULL != pBitmap)
    {

        pBitmap[u4RtIndex / EVPN_NUM_OF_BITS_IN_UINT4] =
            (UINT4) ((UINT4) (pBitmap[(UINT4) (u4RtIndex /
                                               EVPN_NUM_OF_BITS_IN_UINT4)]) &
                     ((UINT4) ~(1 << (UINT4) (u4RtIndex %
                                              EVPN_NUM_OF_BITS_IN_UINT4))));
        return VXLAN_SUCCESS;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilReleaseRtIndex: Exit\n"));
    return VXLAN_FAILURE;
}

/* *************************************************************
 * Function    : EvpnApiGetVrfTableEntry 
 * Description : This function returns the EVPN VRF entry for 
 *               the given VRF name and length
 * Input       : pu1EvpnVrfName
 *             : i4EvpnVrfNameLen
 * Output      : NONE
 * Returns     : tVxlanFsEvpnVxlanVrfEntry 
 * *************************************************************/
tVxlanFsEvpnVxlanVrfEntry *
EvpnApiGetVrfTableEntry (UINT1 *pu1EvpnVrfName, UINT2 u2EvpnVrfNameLen,
                         UINT4 u4Vnid)
{
    tVxlanFsEvpnVxlanVrfEntry *pVxlanSetFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry VxlanSetFsEvpnVxlanVrfEntry;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnApiGetVrfTableEntry: Entry\n"));
    if (pu1EvpnVrfName == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:Vrf Name is NULL\n"));
        return NULL;
    }

    MEMSET (&VxlanSetFsEvpnVxlanVrfEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMCPY (EVPN_VRF_NAME (VxlanSetFsEvpnVxlanVrfEntry), pu1EvpnVrfName,
            u2EvpnVrfNameLen);
    EVPN_VRF_NAME_LEN (VxlanSetFsEvpnVxlanVrfEntry) = u2EvpnVrfNameLen;
    EVPN_VRF_VNI (VxlanSetFsEvpnVxlanVrfEntry) = u4Vnid;
    pVxlanSetFsEvpnVxlanVrfEntry =
        VxlanGetFsEvpnVxlanVrfTable (&VxlanSetFsEvpnVxlanVrfEntry);

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnApiGetVrfTableEntry: Exit\n"));
    return pVxlanSetFsEvpnVxlanVrfEntry;
}

/* *************************************************************
 * Function    : EvpnUtlHandleRowStatusActive
 * Input       : pVxlanFsEvpnVxlanEviVniMapEntry 
 * Description : This function is called when the EVI-VNI row 
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlHandleRowStatusActive (tVxlanFsEvpnVxlanEviVniMapEntry *
                              pVxlanFsEvpnVxlanEviVniMapEntry,
                              tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                              pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
    UINT4               u4NveIfIndex = 0;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    UINT1               au1ESIValue[EVPN_MAX_ESI_LEN];
    UINT1              *pu1ESIValue = au1ESIValue;
    UINT4               u4VrfId = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActive: Entry\n"));

    MEMSET (&au1ESIValue, 0, EVPN_MAX_ESI_LEN);

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EVI VNI does not exists\n"));
        return VXLAN_FAILURE;
    }

    /* Get the Vrf ID from VNI */
    if (EvpnUtilGetVrfFromVni (pVxlanFsEvpnVxlanEviVniMapEntry->
                               MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                               &u4VrfId) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%% Unable to get Vrf Index from Vni number\r\n"));
    }
    if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
    {
        /* Notify with RD Route Params to BGP */
        if ((pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet == OSIX_FALSE) &&
            (EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) != 0))
        {
            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_BGP_VNI
                                              (pVxlanFsEvpnVxlanEviVniMapEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_ADD,
                                              EVPN_P_BGP_RD
                                              (pVxlanFsEvpnVxlanEviVniMapEntry),
                                              EVPN_L2VNI_TYPE);
            pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet = OSIX_TRUE;
            EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) =
                EVPN_MAX_RD_LEN;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD ADD trigger to BGP.\r\n"));
        }
        else if ((pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet == OSIX_TRUE) &&
                 (EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) == 0))
        {
            /* Notify with RD Route Params to BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_BGP_VNI
                                              (pVxlanFsEvpnVxlanEviVniMapEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_DEL,
                                              EVPN_P_BGP_RD
                                              (pVxlanFsEvpnVxlanEviVniMapEntry),
                                              EVPN_L2VNI_TYPE);
            pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet = OSIX_FALSE;
            EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) = EVPN_ZERO;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD Del trigger to BGP.\r\n"));
        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the RD ADD notification to BGP.. Callback is NULL.\r\n"));
    }

    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        if ((pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet == OSIX_TRUE) &&
            (EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) != 0))
        {
            /* Send EVPN_OPER_UP trigger to BGP once RD Configured */
            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                    EVPN_P_BGP_VNI
                                                    (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                    EVPN_OPER_UP);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the oper-up trigger to BGP.\r\n"));
        }
        else if ((pVxlanFsEvpnVxlanEviVniMapEntry->bRDSet == OSIX_FALSE) &&
                 (EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) == 0))
        {
            /* Send EVPN_OPER_DOWN trigger to BGP */
            (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                    EVPN_P_BGP_VNI
                                                    (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                    EVPN_OPER_DOWN);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the oper-down trigger to BGP.\r\n"));
        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the EVPN oper up notification to BGP.. Callback is NULL.\r\n"));
    }

    if (EVPN_NOTIFY_ESI_UPDATE_CB != NULL)
    {
        /* Get the NVE if index from the VNI number */
        if ((VxlanUtilGetNveIndexFromNveTable
             (EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry),
              &u4NveIfIndex)) == NULL)
        {
            /* If NVE database is not mapped with the VNI number
             * Get the Ingress Replica database from the VNI number */
            if ((VxlanUtilGetNveIndexFromInReplicaTable
                 (EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry),
                  &u4NveIfIndex)) == NULL)
            {
                /* If Ingress Replica database is not mapped with the VNI number
                 * Get the MCAST database from the VNI number */
                if ((VxlanUtilGetNveIndexFromMcastTable
                     (EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry),
                      &u4NveIfIndex)) == NULL)
                {
                    VXLAN_TRC ((VXLAN_EVPN_TRC,
                                "\r%%Not able to send the ESI notification to BGP.. Nve Index is 0.\r\n"));
                    return VXLAN_SUCCESS;
                }
            }
        }
        if (u4NveIfIndex != 0)
        {
            pVxlanVtepEntry = EvpnGetVtepTableFromNveIdx (u4NveIfIndex);
        }
        if (pVxlanVtepEntry == NULL)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Not able to send the ESI notification to BGP.. Nve Index is 0.\r\n"));
            return VXLAN_FAILURE;
        }
        /* Notify with ESI Route Params to BGP */
        if ((pVxlanFsEvpnVxlanEviVniMapEntry->bESISet == OSIX_FALSE) &&
            (EVPN_P_BGP_ESI_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) != 0))
        {

            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (u4VrfId,
                                            EVPN_P_BGP_VNI
                                            (pVxlanFsEvpnVxlanEviVniMapEntry),
                                            EVPN_P_BGP_ESI
                                            (pVxlanFsEvpnVxlanEviVniMapEntry),
                                            pVxlanVtepEntry->MibObject.
                                            au1FsVxlanVtepAddress,
                                            pVxlanVtepEntry->MibObject.
                                            i4FsVxlanVtepAddressLen,
                                            EVPN_PARAM_ESI_ADD);

            VxlanUtilAddESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                        i4FsVxlanVtepAddressLen,
                                        pVxlanVtepEntry->MibObject.
                                        au1FsVxlanVtepAddress,
                                        EVPN_P_BGP_ESI
                                        (pVxlanFsEvpnVxlanEviVniMapEntry));
            pVxlanFsEvpnVxlanEviVniMapEntry->bESISet = OSIX_TRUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the ESI ADD trigger to BGP.\r\n"));
        }
        else if ((pVxlanFsEvpnVxlanEviVniMapEntry->bESISet == OSIX_TRUE) &&
                 (EVPN_P_BGP_ESI_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) == 0))
        {
            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (u4VrfId,
                                            EVPN_P_BGP_VNI
                                            (pVxlanFsEvpnVxlanEviVniMapEntry),
                                            EVPN_P_BGP_ESI
                                            (pVxlanFsEvpnVxlanEviVniMapEntry),
                                            pVxlanVtepEntry->MibObject.
                                            au1FsVxlanVtepAddress,
                                            pVxlanVtepEntry->MibObject.
                                            i4FsVxlanVtepAddressLen,
                                            EVPN_PARAM_ESI_DEL);
            VxlanUtilDelESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                        i4FsVxlanVtepAddressType,
                                        pVxlanVtepEntry->MibObject.
                                        au1FsVxlanVtepAddress,
                                        EVPN_P_BGP_ESI
                                        (pVxlanFsEvpnVxlanEviVniMapEntry));
            pVxlanFsEvpnVxlanEviVniMapEntry->bESISet = OSIX_FALSE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the ESI DELETE trigger to BGP.\r\n"));
        }
        else if (EvpnBgpUtlGenerateESIValue
                 (EVPN_P_BGP_EVI (pVxlanFsEvpnVxlanEviVniMapEntry),
                  EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry),
                  pu1ESIValue) == VXLAN_SUCCESS)
        {
            MEMCPY (EVPN_P_BGP_ESI (pVxlanFsEvpnVxlanEviVniMapEntry),
                    &pu1ESIValue, EVPN_MAX_ESI_LEN);
            EVPN_P_BGP_ESI_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) =
                EVPN_MAX_ESI_LEN;
            if (EVPN_P_BGP_RD_LEN (pVxlanFsEvpnVxlanEviVniMapEntry) != 0)
            {
                (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                                EVPN_P_BGP_VNI
                                                (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                EVPN_P_BGP_ESI
                                                (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                pVxlanVtepEntry->MibObject.
                                                au1FsVxlanVtepAddress,
                                                pVxlanVtepEntry->MibObject.
                                                i4FsVxlanVtepAddressLen,
                                                EVPN_PARAM_ESI_ADD);

                VxlanUtilAddESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                            i4FsVxlanVtepAddressLen,
                                            pVxlanVtepEntry->MibObject.
                                            au1FsVxlanVtepAddress,
                                            EVPN_P_BGP_ESI
                                            (pVxlanFsEvpnVxlanEviVniMapEntry));
                pVxlanFsEvpnVxlanEviVniMapEntry->bESISet = OSIX_TRUE;
            }

        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the ESI UPDATE notification to BGP.. Callback is NULL.\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActive: Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlProcessTableCreation 
 * Input       : pVxlanFsEvpnVxlanEviVniMapEntry 
 * Description : This function is called when the EVI-VNI row 
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlProcessTableCreation (tVxlanFsEvpnVxlanEviVniMapEntry *
                             pVxlanFsEvpnVxlanEviVniMapEntry,
                             tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
    UINT4               u4VrfId = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableCreation: Entry\n"));
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EVI VNI does not exists\n"));
        return VXLAN_FAILURE;
    }

    /* Get the Vrf ID from VNI */
    if (EvpnUtilGetVrfFromVni (pVxlanFsEvpnVxlanEviVniMapEntry->
                               MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                               &u4VrfId) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%% Unable to get Vrf Index from Vni number\r\n"));
    }
    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        /* Send EVPN_OPER_CREATED trigger to BGP */
        /* Since single VRF is used, EVI ID is passed as the default context of BGP */
        (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                EVPN_P_BGP_VNI
                                                (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                EVPN_OPER_CREATED);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Send the oper created trigger to BGP.\r\n"));
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the EVPN oper created notification to BGP.. Callback is NULL.\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableCreation: Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlHandleRowStatusNotInService
 * Input       : pVxlanFsEvpnVxlanEviVniMapEntry 
 * Description : This function is called when the EVI-VNI row 
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlHandleRowStatusNotInService (tVxlanFsEvpnVxlanEviVniMapEntry *
                                    pVxlanFsEvpnVxlanEviVniMapEntry,
                                    tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                                    pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    UNUSED_PARAM (pVxlanFsEvpnVxlanEviVniMapEntry);
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
    UINT4               u4VrfId = 0;
    UINT4               u4VrfId = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusNotInService Entry\n"));
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusNotInService Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlProcessTableDeletion
 * Input       : pVxlanFsEvpnVxlanEviVniMapEntry 
 * Description : This function is called when the EVI-VNI row 
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlProcessTableDeletion (tVxlanFsEvpnVxlanEviVniMapEntry *
                             pVxlanFsEvpnVxlanEviVniMapEntry,
                             tVxlanIsSetFsEvpnVxlanEviVniMapEntry *
                             pVxlanIsSetFsEvpnVxlanEviVniMapEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanEviVniMapEntry);
    UINT4               u4VrfId = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableDeletion: Entry\n"));
    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:EVI VNI does not exists\n"));
        return VXLAN_FAILURE;
    }
    /* Get the Vrf ID from VNI */
    if (EvpnUtilGetVrfFromVni (pVxlanFsEvpnVxlanEviVniMapEntry->
                               MibObject.u4FsEvpnVxlanEviVniMapVniNumber,
                               &u4VrfId) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%% Unable to get Vrf Index from Vni number\r\n"));
    }

    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        /* Send EVPN_OPER_DELETED trigger to BGP */
        /* Since single VRF is used, EVI ID is passed as the default context of BGP */
        (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                EVPN_P_BGP_VNI
                                                (pVxlanFsEvpnVxlanEviVniMapEntry),
                                                EVPN_OPER_DELETED);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Send the oper-delete trigger to BGP\r\n"));
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Cannot send notification to BGP about EVPN_OPER_DELETED/EVPN_OPER_DOWN.. Callback is NULL\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableDeletion: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ***********************************************************************
 * Function   : EvpnGetRTEntryFromValue
 * Description: This function gives the RT entry from the given RT value
 * Input      : i4EviIndex
 *              u4VniIndex
 *              pu1RtValue
 * Output     : pEvpnBgpRtEntry
 * Returns    : tVxlanFsEvpnVxlanBgpRTEntry
 * ***********************************************************************/
tVxlanFsEvpnVxlanBgpRTEntry *
EvpnGetRTEntryFromValue (INT4 i4EviIndex, UINT4 u4VniIndex, UINT1 *pu1RtValue)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnGetRTEntryFromValue: Entry\n"));
    tVxlanFsEvpnVxlanBgpRTEntry EvpnBgpRtEntry;
    tVxlanFsEvpnVxlanBgpRTEntry *pEvpnBgpRtEntry = NULL;

    MEMSET (&EvpnBgpRtEntry, 0, sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    EvpnBgpRtEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    EvpnBgpRtEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;

    pEvpnBgpRtEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                       &EvpnBgpRtEntry, NULL);
    while (NULL != pEvpnBgpRtEntry)
    {
        if ((pEvpnBgpRtEntry->MibObject.
             i4FsEvpnVxlanEviVniMapEviIndex == i4EviIndex) &&
            (pEvpnBgpRtEntry->MibObject.
             u4FsEvpnVxlanEviVniMapVniNumber == u4VniIndex))
        {
            if (MEMCMP
                (pEvpnBgpRtEntry->MibObject.au1FsEvpnVxlanBgpRT, pu1RtValue,
                 EVPN_MAX_RT_LEN) == 0)
            {
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:RT Index value is - %d\r\n",
                                 EVPN_P_BGP_RT_INDEX (pEvpnBgpRtEntry)));
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:EvpnGetRTEntryFromValue: Exit\n"));
                return pEvpnBgpRtEntry;
            }
        }
        pEvpnBgpRtEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                           pEvpnBgpRtEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:RT Entry does not exists\r\n"));
    return NULL;

}

/* ************************************************************************
 * Function   : EvpnGetRTTableFromIndex
 * Description: This function gives the RT entry from the given table index
 * Input      : i4EviIndex
 *              u4VniIndex
 *              pu1RtValue
 * Output     : pEvpnBgpRtEntry
 * Returns    : tVxlanFsEvpnVxlanBgpRTEntry
 * ************************************************************************/
tVxlanFsEvpnVxlanBgpRTEntry *
EvpnGetRTTableFromEviVni (INT4 i4EviIndex, UINT4 u4VniIndex)
{
    tVxlanFsEvpnVxlanBgpRTEntry EvpnBgpRtEntry;
    tVxlanFsEvpnVxlanBgpRTEntry *pEvpnBgpRtEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnGetRTTableFromIndex Entry\n"));

    MEMSET (&EvpnBgpRtEntry, 0, sizeof (tVxlanFsEvpnVxlanBgpRTEntry));

    EvpnBgpRtEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    EvpnBgpRtEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;

    pEvpnBgpRtEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                       &EvpnBgpRtEntry, NULL);
    while (NULL != pEvpnBgpRtEntry)
    {
        if (EVPN_P_BGP_RT_AUTO (pEvpnBgpRtEntry) == OSIX_TRUE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:RT Index value is - %d\r\n",
                             EVPN_P_BGP_RT_INDEX (pEvpnBgpRtEntry)));
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnGetRTTableFromEviVni: Exit\n"));
            return pEvpnBgpRtEntry;
        }
        pEvpnBgpRtEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                           pEvpnBgpRtEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:RT Entry does not exists for EVI VNI\r\n"));
    return NULL;
}

/* ************************************************************************
 * Function   : EvpnGetVtepTableFromNveIdx
 * Description: This function gives the VTEP entry from the given NVE index
 * Input      : u4NveIfIndex
 * Output     : pVxlanVtepEntry
 * Returns    : tVxlanFsVxlanVtepEntry
 * ************************************************************************/
tVxlanFsVxlanVtepEntry *
EvpnGetVtepTableFromNveIdx (UINT4 u4NveIfIndex)
{
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanVtepEntry VxlanVtepEntry;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnGetVtepTableFromNveIdx Entry\n"));
    MEMSET (&VxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    VxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex = (INT4) u4NveIfIndex;
    pVxlanVtepEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVtepTable,
                   (tRBElem *) & VxlanVtepEntry);

    if (pVxlanVtepEntry != NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:Vtep Entry present with Nve Index- %d\r\n",
                         u4NveIfIndex));
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnGetVtepTableFromNveIdx: Exit\n"));
        return pVxlanVtepEntry;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:Vtep Entry does not exists for the Nve Index\r\n"));
    return NULL;
}

/* ***********************************************************************
 * Function   : VxlanDelEviVniMap
 * Description: This function deletes entry from the EVI_VNI Table
 * Input      : i4EviIndex
 * Output     : NONE
 * Returns    : NONE
 * ***********************************************************************/
VOID
VxlanDelEviVniMapEntry (INT4 i4FsEvpnIdx)
{

    tVxlanIsSetFsEvpnVxlanEviVniMapEntry VxlanIsSetFsEvpnVxlanEviVniMapEntry;
    INT4                i4RetVal = VXLAN_FAILURE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelEviVniMapEntry: Entry\n"));
    tVxlanFsEvpnVxlanEviVniMapEntry *pGetFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry *pGetNextFsEvpnVxlanEviVniMapEntry = NULL;

    pGetFsEvpnVxlanEviVniMapEntry = (tVxlanFsEvpnVxlanEviVniMapEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable);

    while (pGetFsEvpnVxlanEviVniMapEntry != NULL)
    {
        pGetNextFsEvpnVxlanEviVniMapEntry = (tVxlanFsEvpnVxlanEviVniMapEntry *)
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           (tRBElem *) pGetFsEvpnVxlanEviVniMapEntry, NULL);

        if (EVPN_P_BGP_EVI (pGetFsEvpnVxlanEviVniMapEntry) != i4FsEvpnIdx)
        {
            pGetFsEvpnVxlanEviVniMapEntry = pGetNextFsEvpnVxlanEviVniMapEntry;
            continue;
        }

        i4RetVal = EvpnUtlProcessTableDeletion (pGetFsEvpnVxlanEviVniMapEntry,
                                                &VxlanIsSetFsEvpnVxlanEviVniMapEntry);

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   pGetFsEvpnVxlanEviVniMapEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_POOLID,
                            (UINT1 *) pGetFsEvpnVxlanEviVniMapEntry);
        pGetFsEvpnVxlanEviVniMapEntry = pGetNextFsEvpnVxlanEviVniMapEntry;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelEviVniMapEntry: Exit\n"));
    UNUSED_PARAM (i4RetVal);
    return;
}

/* ***********************************************************************
 * Function   : VxlanDelBgpRT
 * Description: This function deletes entry from the BGP_RT Table
 * Input      : i4EviIndex, u4VniIdx
 * Output     : NONE
 * Returns    : NONE
 * ***********************************************************************/
VOID
VxlanDelBgpRTEntry (INT4 i4FsEvpnIdx, UINT4 u4VniIdx)
{
    UINT4               u4ContextId = 0;
    UINT4               u4RtIndex = 0;
    UINT1               u1RTParamType = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelBgpRTEntry: Entry\n"));
    tVxlanFsEvpnVxlanBgpRTEntry *pGetVxlanFsEvpnVxlanBgpRTEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry *pGetNextVxlanFsEvpnVxlanBgpRTEntry = NULL;

    pGetVxlanFsEvpnVxlanBgpRTEntry = (tVxlanFsEvpnVxlanBgpRTEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable);

    while (pGetVxlanFsEvpnVxlanBgpRTEntry != NULL)
    {
        pGetNextVxlanFsEvpnVxlanBgpRTEntry = (tVxlanFsEvpnVxlanBgpRTEntry *)
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                           (tRBElem *) pGetVxlanFsEvpnVxlanBgpRTEntry, NULL);

        if (EVPN_P_BGP_EVI (pGetVxlanFsEvpnVxlanBgpRTEntry) != i4FsEvpnIdx)
        {
            pGetVxlanFsEvpnVxlanBgpRTEntry = pGetNextVxlanFsEvpnVxlanBgpRTEntry;
            continue;
        }
        if ((u4VniIdx != 0) &&
            (EVPN_P_BGP_VNI (pGetVxlanFsEvpnVxlanBgpRTEntry) != u4VniIdx))
        {
            pGetVxlanFsEvpnVxlanBgpRTEntry = pGetNextVxlanFsEvpnVxlanBgpRTEntry;
            continue;
        }

        u4ContextId = EVPN_P_BGP_VNI (pGetVxlanFsEvpnVxlanBgpRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_L2VNI_RT);

        switch (EVPN_P_BGP_RT_TYPE (pGetVxlanFsEvpnVxlanBgpRTEntry))
        {
            case EVPN_BGP_RT_IMPORT:
                u1RTParamType = EVPN_BGP4_IMPORT_RT;
                break;

            case EVPN_BGP_RT_EXPORT:
                u1RTParamType = EVPN_BGP4_EXPORT_RT;
                break;

            case EVPN_BGP_RT_BOTH:
                u1RTParamType = EVPN_BGP4_BOTH_RT;
                break;
            default:
                break;
        }

        EvpnUtilReleaseRtIndex (u4RtIndex,
                                EVPN_P_BGP_RT_INDEX
                                (pGetVxlanFsEvpnVxlanBgpRTEntry),
                                EVPN_L2VNI_RT);
        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                              EVPN_P_BGP_VNI
                                              (pGetVxlanFsEvpnVxlanBgpRTEntry),
                                              u1RTParamType, EVPN_PARAM_RT_DEL,
                                              EVPN_P_BGP_RT
                                              (pGetVxlanFsEvpnVxlanBgpRTEntry),
                                              EVPN_L2VNI_TYPE);
        }
        else
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Cannot send the RT delete notification to BGP.. since the callback is NULL\n"));
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanBgpRTTable,
                   pGetVxlanFsEvpnVxlanBgpRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANBGPRTTABLE_POOLID,
                            (UINT1 *) pGetVxlanFsEvpnVxlanBgpRTEntry);
        pGetVxlanFsEvpnVxlanBgpRTEntry = pGetNextVxlanFsEvpnVxlanBgpRTEntry;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelBgpRTEntry: Exit\n"));
    return;
}

/*******************************************************************************/
/* Function Name : EvpnBgpGetDefaultRdValue                                    */
/* Description   : This function returns Default RD value for a given EVI_VNI  */
/* Input(s)      : u4VniIndex - VNI Instance                                   */
/*                 i4EviIndex - EVI Instance                                   */
/* Output(s)     : pu1RouteDistinguisher - RD value for given EVI_VNI          */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                                 */
/*******************************************************************************/
INT4
EvpnBgpUtlGetDefaultRdValue (INT4 i4EviIndex, UINT4 u4VniIndex,
                             UINT1 *pu1RouteDistinguisher)
{
    ULONG8              u8RdValue = EVPN_ZERO;
    UINT4               u4AsnValue = EVPN_ZERO;
    UINT4               u4VniId = EVPN_ZERO;
    UINT4               u4AsnNum = EVPN_ZERO;
    UINT2               u2AsnValue = EVPN_ZERO;
    UINT2               u2VniId = EVPN_ZERO;
    UINT1               u1AsnType = EVPN_ZERO;

    tVxlanFsEvpnVxlanEviVniMapEntry VxlanSetFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetDefaultRdValue: Entry\n"));

    MEMSET (&VxlanSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;
    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        VxlanGetFsEvpnVxlanEviVniMapTable (&VxlanSetFsEvpnVxlanEviVniMapEntry);
    if (NULL == pVxlanFsEvpnVxlanEviVniMapEntry)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnBgpGetDefaultRdValue: EVI_VNI Entry not found\n"));
        return VXLAN_FAILURE;
    }

    MEMCPY (&u8RdValue,
            EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry), EVPN_MAX_RD_LEN);
    if (EVPN_ZERO != u8RdValue)
    {
        MEMCPY (pu1RouteDistinguisher,
                EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry),
                EVPN_MAX_RD_LEN);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnBgpGetDefaultRdValue: Default RD is present\n"));
        return VXLAN_SUCCESS;
    }

    EvpnBgpGetASN (&u1AsnType, &u4AsnValue);
    MEMCPY (&u4VniId,
            &EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry), sizeof (UINT4));
    EvpnUtilGetRtIndex (u4VniId, &u4AsnNum, EVPN_L2VNI_RT);
    u4AsnNum++;

    if (EVPN_RD_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteDistinguisher[0] = EVPN_RD_TYPE_0;
        pu1RouteDistinguisher[1] = EVPN_RD_SUBTYPE;
        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4AsnNum = OSIX_HTONL (u4AsnNum);

        MEMCPY (&pu1RouteDistinguisher[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteDistinguisher[4], &u4AsnNum, sizeof (UINT4));
    }
    else
    {
        u2VniId = (UINT2) u4AsnNum;
        pu1RouteDistinguisher[0] = EVPN_RD_TYPE_2;
        pu1RouteDistinguisher[1] = EVPN_RD_SUBTYPE;

        u2VniId = OSIX_HTONS (u2VniId);
        u4AsnValue = OSIX_HTONL (u4AsnValue);

        MEMCPY (&pu1RouteDistinguisher[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteDistinguisher[6], &u2VniId, sizeof (UINT2));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetDefaultRdValue: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : EvpnBgpUtlGetDefaultRtValue                               */
/* Description   : This function returns Default Rt value for a given RT     */
/* Input(s)      : u4VniIndex - VNI Instance                                 */
/*                 i4EviIndex - EVI Instance                                 */
/*                 u4RtIndex -  RT Instance                                  */
/*                 i4RtType -  RT Type                                       */
/* Output(s)     : pu1RouteTarget - RT value for given RT Table              */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/
INT4
EvpnBgpUtlGetDefaultRtValue (INT4 i4EviIndex, UINT4 u4VniIndex, UINT4 u4RtIndex,
                             INT4 i4RtType, UINT1 *pu1RouteTarget)
{
    ULONG8              u8RtValue = EVPN_ZERO;
    UINT4               u4AsnValue = EVPN_ZERO;
    UINT4               u4VniId = EVPN_ZERO;
    UINT4               u4AsnNum = EVPN_ZERO;
    UINT2               u2AsnValue = EVPN_ZERO;
    UINT2               u2VniId = EVPN_ZERO;
    UINT1               u1AsnType = EVPN_ZERO;
    tVxlanFsEvpnVxlanEviVniMapEntry VxlanSetFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    tVxlanFsEvpnVxlanBgpRTEntry EvpnBgpRtEntry;
    tVxlanFsEvpnVxlanBgpRTEntry *pEvpnBgpRtEntry = NULL;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetDefaultRtValue: Entry\n"));

    MEMSET (&EvpnBgpRtEntry, 0, sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (&VxlanSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;
    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        VxlanGetFsEvpnVxlanEviVniMapTable (&VxlanSetFsEvpnVxlanEviVniMapEntry);
    if (NULL == pVxlanFsEvpnVxlanEviVniMapEntry)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnBgpGetDefaultRdValue: EVI_VNI Entry not found\n"));
        return VXLAN_FAILURE;
    }

    EvpnBgpRtEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;
    EvpnBgpRtEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;

    pEvpnBgpRtEntry = VxlanGetNextFsEvpnVxlanBgpRTTable (&EvpnBgpRtEntry);
    while (pEvpnBgpRtEntry != NULL)
    {
        if ((pEvpnBgpRtEntry->MibObject.u4FsEvpnVxlanEviVniMapVniNumber ==
             u4VniIndex)
            && (pEvpnBgpRtEntry->MibObject.i4FsEvpnVxlanEviVniMapEviIndex ==
                i4EviIndex)
            && (pEvpnBgpRtEntry->MibObject.i4FsEvpnVxlanBgpRTAuto == OSIX_TRUE))
        {
            MEMCPY (&u8RtValue,
                    EVPN_P_BGP_RT (pEvpnBgpRtEntry), EVPN_MAX_RT_LEN);
            if (EVPN_ZERO != u8RtValue)
            {
                MEMCPY (pu1RouteTarget,
                        EVPN_P_BGP_RT (pEvpnBgpRtEntry), EVPN_MAX_RT_LEN);
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            " FUNC:EvpnBgpGetDefaultRtValue: Default RT is present\n"));
                return VXLAN_SUCCESS;
            }
        }
        pEvpnBgpRtEntry = VxlanGetNextFsEvpnVxlanBgpRTTable (pEvpnBgpRtEntry);
    }
    EvpnBgpGetASN (&u1AsnType, &u4AsnValue);
    MEMCPY (&u4VniId,
            &EVPN_P_BGP_VNI (pVxlanFsEvpnVxlanEviVniMapEntry), sizeof (UINT4));
    EvpnUtilGetRtIndex (u4VniId, &u4AsnNum, EVPN_L2VNI_RT);
    u4AsnNum++;

    if (EVPN_RT_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteTarget[0] = EVPN_RT_TYPE_0;
        pu1RouteTarget[1] = EVPN_RT_SUBTYPE;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4AsnNum = OSIX_HTONL (u4AsnNum);

        MEMCPY (&pu1RouteTarget[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteTarget[4], &u4AsnNum, sizeof (UINT4));
    }
    else
    {
        u2VniId = (UINT2) u4AsnNum;
        pu1RouteTarget[0] = EVPN_RT_TYPE_2;
        pu1RouteTarget[1] = EVPN_RT_SUBTYPE;

        u2VniId = OSIX_HTONS (u2VniId);
        u4AsnValue = OSIX_HTONL (u4AsnValue);

        MEMCPY (&pu1RouteTarget[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteTarget[6], &u2VniId, sizeof (UINT2));
    }
    if (pEvpnBgpRtEntry != NULL)
    {
        MEMCPY (EVPN_P_BGP_RT (pEvpnBgpRtEntry),
                pu1RouteTarget, EVPN_MAX_RT_LEN);
    }

    UNUSED_PARAM (u4RtIndex);
    UNUSED_PARAM (i4RtType);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetDefaultRtValue: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : EvpnBgpGetASN                                              */
/* Description  : This function returns ASN value and type stored in         */
/*                local BGP DB.                                              */
/* Input        : NONE                                                       */
/* Output       : pu1AsnType    - ASN Type                                   */
/*                pu4AsnValue   - ASN Value                                  */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
EvpnBgpGetASN (UINT1 *pu1AsnType, UINT4 *pu4AsnValue)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetASN: Entry\n"));
    *pu4AsnValue = gu4BgpRdRtAsnValue;
    *pu1AsnType = gu1BgpRdRtAsnType;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpGetASN: Exit\n"));
}

/*****************************************************************************/
/* Function     : EvpnBgpUpdateASN                                           */
/* Description  : This function updates ASN value and type in local BGP DB.  */
/* Input        : u1AsnType    - ASN Type                                    */
/*                u4AsnValue   - ASN Value                                   */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
EvpnBgpUpdateASN (UINT1 u1AsnType, UINT4 u4AsnValue)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpUpdateASN: Entry\n"));
    if (1 == u1AsnType)
    {
        gu1BgpRdRtAsnType = 2;
    }
    else
    {
        gu1BgpRdRtAsnType = 0;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnBgpUpdateASN: Exit\n"));
    gu4BgpRdRtAsnValue = u4AsnValue;
}

/*****************************************************************************/
/* Function     : VxlanEvpnUtilValidateRDRT                                  */
/* Description  : This function validates RD or RT value configured for EVPN */
/* Input        : pau1RdRt                                                   */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_SUCCESS / VXLAN_FAILURE                              */
/*****************************************************************************/
INT4
VxlanEvpnUtilValidateRDRT (UINT1 *pau1RdRt)
{
    UINT4               u4AsnValue = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4IpAddress = 0;
    UINT2               u2AsnValue = 0;
    UINT2               u2VpnId = 0;
    UINT1               au1ValidateRdRt[EVPN_MAX_RD_LEN];

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:VxlanEvpnUtilValidateRDRT: Entry\n"));
    MEMSET (au1ValidateRdRt, 0, EVPN_MAX_RD_LEN);

    MEMCPY (au1ValidateRdRt, pau1RdRt, EVPN_MAX_RD_LEN);

    if (pau1RdRt[0] == EVPN_RD_TYPE_0)
    {
        MEMCPY (&u2AsnValue, &au1ValidateRdRt[2], sizeof (UINT2));
        MEMCPY (&u4VpnId, &au1ValidateRdRt[4], sizeof (UINT4));
        if ((u2AsnValue == 0) || (u4VpnId == 0))
        {
            return VXLAN_FAILURE;
        }
    }
    else if (pau1RdRt[0] == EVPN_RD_TYPE_2)
    {
        MEMCPY (&u4AsnValue, &au1ValidateRdRt[2], sizeof (UINT4));
        MEMCPY (&u2VpnId, &au1ValidateRdRt[6], sizeof (UINT2));
        if ((u4AsnValue == 0) || (u2VpnId == 0))
        {
            return VXLAN_FAILURE;
        }
    }
    else
    {
        MEMCPY (&u4IpAddress, &au1ValidateRdRt[2], sizeof (UINT4));
        MEMCPY (&u2VpnId, &au1ValidateRdRt[6], sizeof (UINT2));
        if ((u4IpAddress == 0) || (u2VpnId == 0))
        {
            return VXLAN_FAILURE;
        }
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnUtilValidateRDRT: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : VxlanUtilAddESInfoInMHPeer                                 */
/* Description  : This function updates ESI information to MHPeer Table      */
/* Input        : i4VtepAddressLen                                           */
/*                pu1VtepAddress                                             */
/*                pu1EviVniEsi                                               */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
VxlanUtilAddESInfoInMHPeer (INT4 i4VtepAddressLen, UINT1 *pu1VtepAddress,
                            UINT1 *pu1EviVniEsi)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMultihomedPeerTable = NULL;
    UINT4               u4Count = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilAddESInfoInMHPeer Entry\n"));
    /* Allocate memory for the new node */
    pEvpnVxlanMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *)
        MemAllocMemBlk (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID);
    if (pEvpnVxlanMultihomedPeerTable == NULL)
    {
        VXLAN_TRC ((VXLAN_UTIL_TRC,
                    "tVxlanFsEvpnVxlanMultihomedPeerTable: Fail to Allocate Memory.\r\n"));
        return;
    }

    pEvpnVxlanMultihomedPeerTable->MibObject.
        i4FsEvpnVxlanPeerIpAddressLen = i4VtepAddressLen;

    if (i4VtepAddressLen == VXLAN_IP4_ADDR_LEN)
    {
        MEMCPY (pEvpnVxlanMultihomedPeerTable->MibObject.
                au1FsEvpnVxlanPeerIpAddress, pu1VtepAddress,
                VXLAN_IP4_ADDR_LEN);
        pEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType = VXLAN_IPV4_UNICAST;
    }
    else
    {
        MEMCPY (pEvpnVxlanMultihomedPeerTable->MibObject.
                au1FsEvpnVxlanPeerIpAddress, pu1VtepAddress,
                VXLAN_IP6_ADDR_LEN);
        pEvpnVxlanMultihomedPeerTable->MibObject.
            i4FsEvpnVxlanPeerIpAddressType = VXLAN_IPV6_UNICAST;
    }
    MEMCPY (pEvpnVxlanMultihomedPeerTable->MibObject.
            au1FsEvpnVxlanMHEviVniESI, pu1EviVniEsi, EVPN_MAX_ESI_LEN);

    pEvpnVxlanMultihomedPeerTable->MibObject.i4FsEvpnVxlanMHEviVniESILen
        = EVPN_MAX_ESI_LEN;

    if (RBTreeAdd (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   (tRBElem *) pEvpnVxlanMultihomedPeerTable) != RB_SUCCESS)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "RB Tree addition failed\n"));
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanMultihomedPeerTable);
    }

    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                 &u4Count);
    if (u4Count > EVPN_ONE)
    {
        VxlanEvpnDFElection (pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanMHEviVniESI,
                             pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanPeerIpAddress);
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilAddESInfoInMHPeer Exit\n"));
}

/*****************************************************************************/
/* Function     : VxlanUtilDelESInfoInMHPeer                                 */
/* Description  : This function deletes ESI Entry in the MHPeer Table        */
/* Input        : i4VtepAddressLen                                           */
/*                pu1VtepAddress                                             */
/*                pu1EviVniEsi                                               */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
VxlanUtilDelESInfoInMHPeer (INT4 i4VtepAddressType, UINT1 *pu1VtepAddress,
                            UINT1 *pu1EviVniEsi)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMultihomedPeerTable = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    UINT4               u4VniIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilDelESInfoInMHPeer Entry\n"));
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    pEvpnVxlanMultihomedPeerTable =
        EvpnUtilGetMHPeerTable (i4VtepAddressType, pu1VtepAddress,
                                pu1EviVniEsi);
    if (pEvpnVxlanMultihomedPeerTable != NULL)
    {
        TmrStopTimer (gVxlanGlobals.VxlanTimerList,
                      &(pEvpnVxlanMultihomedPeerTable->EvpnRouteTimer));

        if (EvpnUtilGetVniFromEsiEntry (pu1EviVniEsi,
                                        &u4VniIndex) == VXLAN_SUCCESS)
        {
            VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
                u4VniIndex;

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) & VxlanVniVlanMapEntry, NULL);
            if (pVxlanVniVlanMapEntry != NULL)
            {
                VxlanHwUpdateMHPeerDatabase (pVxlanVniVlanMapEntry->MibObject.
                                             i4FsVxlanVniVlanDfElection,
                                             u4VniIndex, VXLAN_HW_DEL);
            }
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   pEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanMultihomedPeerTable);
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "RB Tree Deletion Successful\n"));
        pEvpnVxlanMultihomedPeerTable = NULL;
        pEvpnVxlanMultihomedPeerTable =
            (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable);

        if (pEvpnVxlanMultihomedPeerTable == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "MultihomedPeerTable is empty. Cant Do DF Election!\n"));
            return;
        }
        /* DF Re-election should happen in case of ES-Withdraw */
        VxlanEvpnDFElection (pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanMHEviVniESI,
                             pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanPeerIpAddress);
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilDelESInfoInMHPeer Exit\n"));
}

/*****************************************************************************/
/* Function     : VxlanUtilDelESInfoInMHPeer                                 */
/* Description  : This function deletes ESI Entry in the MHPeer Table        */
/* Input        : i4VtepAddressLen                                           */
/*                pu1VtepAddress                                             */
/*                pu1EviVniEsi                                               */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
VxlanUtilDelESInfoInMHPeer (INT4 i4VtepAddressType, UINT1 *pu1VtepAddress,
                            UINT1 *pu1EviVniEsi)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMultihomedPeerTable = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    UINT4               u4VniIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilDelESInfoInMHPeer Entry\n"));
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));

    pEvpnVxlanMultihomedPeerTable =
        EvpnUtilGetMHPeerTable (i4VtepAddressType, pu1VtepAddress,
                                pu1EviVniEsi);
    if (pEvpnVxlanMultihomedPeerTable != NULL)
    {
        TmrStopTimer (gVxlanGlobals.VxlanTimerList,
                      &(pEvpnVxlanMultihomedPeerTable->EvpnRouteTimer));

        if (EvpnUtilGetVniFromEsiEntry (pu1EviVniEsi,
                                        &u4VniIndex) == VXLAN_SUCCESS)
        {
            VxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
                u4VniIndex;

            pVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) & VxlanVniVlanMapEntry, NULL);
            if (pVxlanVniVlanMapEntry != NULL)
            {
                VxlanHwUpdateMHPeerDatabase (pVxlanVniVlanMapEntry->MibObject.
                                             i4FsVxlanVniVlanDfElection,
                                             u4VniIndex, VXLAN_HW_DEL);
            }
        }

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   pEvpnVxlanMultihomedPeerTable);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_POOLID,
                            (UINT1 *) pEvpnVxlanMultihomedPeerTable);
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "RB Tree Deletion Successful\n"));
        pEvpnVxlanMultihomedPeerTable = NULL;
        pEvpnVxlanMultihomedPeerTable =
            (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst
            (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable);

        if (pEvpnVxlanMultihomedPeerTable == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "MultihomedPeerTable is empty. Cant Do DF Election!\n"));
            return;
        }
        /* DF Re-election should happen in case of ES-Withdraw */
        VxlanEvpnDFElection (pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanMHEviVniESI,
                             pEvpnVxlanMultihomedPeerTable->MibObject.
                             au1FsEvpnVxlanPeerIpAddress);
    }
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilDelESInfoInMHPeer Exit\n"));
}

/*****************************************************************************/
/* Function     : EvpnUtilGetMHPeerTable                                     */
/* Description  : This function updates ESI information to MHPeer Table      */
/* Input        : pau1RdRt                                                   */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
tVxlanFsEvpnVxlanMultihomedPeerTable *
EvpnUtilGetMHPeerTable (INT4 i4VtepAddressType, UINT1 *pu1VtepAddress,
                        UINT1 *pu1EviVniEsi)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnVxlanMultihomedPeerTable = NULL;
    tVxlanFsEvpnVxlanMultihomedPeerTable EvpnVxlanMultihomedPeerTable;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnUtilGetMHPeerTable Entry\n"));

    MEMSET (&EvpnVxlanMultihomedPeerTable, 0,
            sizeof (tVxlanFsEvpnVxlanMultihomedPeerTable));

    EvpnVxlanMultihomedPeerTable.MibObject.
        i4FsEvpnVxlanPeerIpAddressType = i4VtepAddressType;

    if (i4VtepAddressType == VXLAN_IPV4_UNICAST)
    {
        MEMCPY (EvpnVxlanMultihomedPeerTable.MibObject.
                au1FsEvpnVxlanPeerIpAddress, pu1VtepAddress,
                VXLAN_IP4_ADDR_LEN);
        EvpnVxlanMultihomedPeerTable.MibObject.i4FsEvpnVxlanPeerIpAddressLen =
            VXLAN_IP4_ADDR_LEN;
    }
    else
    {
        MEMCPY (EvpnVxlanMultihomedPeerTable.MibObject.
                au1FsEvpnVxlanPeerIpAddress, pu1VtepAddress,
                VXLAN_IP6_ADDR_LEN);
        EvpnVxlanMultihomedPeerTable.MibObject.i4FsEvpnVxlanPeerIpAddressLen =
            VXLAN_IP6_ADDR_LEN;
    }
    MEMCPY (EvpnVxlanMultihomedPeerTable.MibObject.
            au1FsEvpnVxlanMHEviVniESI, pu1EviVniEsi, EVPN_MAX_ESI_LEN);
    EvpnVxlanMultihomedPeerTable.MibObject.
        i4FsEvpnVxlanMHEviVniESILen = EVPN_MAX_ESI_LEN;

    pEvpnVxlanMultihomedPeerTable =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                   (tRBElem *) & EvpnVxlanMultihomedPeerTable);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnUtilGetMHPeerTable  Exit\n"));

    return pEvpnVxlanMultihomedPeerTable;
}

/*****************************************************************************/
/* Function     : EvpnUtilADUpdateToBgp                                      */
/* Description  : This function updates ESI information to BGP after DF      */
/*                election                                                   */
/* Input        : pEvpnMultihomedPeerTable                                   */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilADUpdateToBgp (UINT1 *pau1FsEvpnVxlanMHEviVniESI)
{
    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnMultihomedPeerTable = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilADUpdateToBgp: Entry\n"));
    pEvpnMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst
        (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable);

    if (pEvpnMultihomedPeerTable == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "pEvpnMultihomedPeerTable is NULL\n"));
        return VXLAN_FAILURE;
    }

    while (pEvpnMultihomedPeerTable != NULL)
    {
        if (MEMCMP (pEvpnMultihomedPeerTable->MibObject.
                    au1FsEvpnVxlanMHEviVniESI, pau1FsEvpnVxlanMHEviVniESI,
                    EVPN_MAX_ESI_LEN) == 0)
        {
            VxlanEvpnDFElection (pEvpnMultihomedPeerTable->MibObject.
                                 au1FsEvpnVxlanMHEviVniESI,
                                 pEvpnMultihomedPeerTable->MibObject.
                                 au1FsEvpnVxlanPeerIpAddress);
            break;
        }
        pEvpnMultihomedPeerTable =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                           FsEvpnVxlanMultihomedPeerTable,
                           pEvpnMultihomedPeerTable, NULL);

    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilADUpdateToBgp: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : VxlanEvpnDFElection                                        */
/* Description  : This function updates ESI information to BGP after DF      */
/*                election                                                   */
/* Input        : pEvpnMultihomedPeerTable                                   */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
VxlanEvpnDFElection (UINT1 *pau1FsEvpnVxlanMHEviVniESI,
                     UINT1 *pau1FsEvpnVxlanPeerIpAddress)
{
    UINT4               u4VniIndex = 0;
    UINT4               u4NveIfIndex = 0;
    UINT2               u2VlanId = 0;
    INT4                i4EviIndex = 0;
    INT4                i4LBFlag = 0;
    UINT4               u4Count = 0;
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanVniVlanMapEntry;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanVniVlanMapEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnVxlanEviVniMapEntry = NULL;
    INT4                i4FsVxlanVniVlanDfElection = OSIX_FALSE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnDFElection: Entry\n"));
    MEMSET (&VxlanVniVlanMapEntry, 0, sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    if (EvpnUtilGetVniFromEsiEntry (pau1FsEvpnVxlanMHEviVniESI,
                                    &u4VniIndex) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No VNI entry found for VLAN id -"
                         "%d\n", u2VlanId));
        return VXLAN_FAILURE;
    }
    /* Get the Evi index from Vni */
    if (EvpnUtilGetEviFromVni (u4VniIndex, &i4EviIndex) == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%EVI_VNI Table is not configured.\r\n"));
    }
    /* Get the LoadBalance flag from Vni, Evi */
    EvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4EviIndex;
    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4VniIndex;
    pEvpnVxlanEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);
    i4LBFlag =
        pEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniLoadBalance;

    VxlanUtilGetVlanFromVni (u4VniIndex, &u2VlanId);

    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (u4VniIndex, &u4NveIfIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniIndex, &u4NveIfIndex))
            == NULL)
        {
            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable (u4VniIndex, &u4NveIfIndex))
                == NULL)
            {
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            "\r%%VxlanEvpnDFElection returns Fail because Nve Index is 0.\r\n"));
                return VXLAN_FAILURE;
            }
        }
    }
    if (u4NveIfIndex != 0)
    {
        pVxlanVtepEntry = EvpnGetVtepTableFromNveIdx (u4NveIfIndex);
        if (pVxlanVtepEntry == NULL)
        {
            return VXLAN_FAILURE;
        }
        if (MEMCMP (pVxlanVtepEntry->MibObject.au1FsVxlanVtepAddress,
                    pau1FsEvpnVxlanPeerIpAddress, VXLAN_IP4_ADDR_LEN) == 0)
        {
            i4FsVxlanVniVlanDfElection = OSIX_TRUE;
        }
        VxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            (INT4) u2VlanId;

        pVxlanVniVlanMapEntry =
            RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                       (tRBElem *) & VxlanVniVlanMapEntry);

        if (pVxlanVniVlanMapEntry == NULL)
        {
            VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No VNI entry found for VLAN id -"
                             "%d\n", u2VlanId));
            return VXLAN_FAILURE;
        }

        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " VNI entry found for VLAN id -"
                         "%d\n", u2VlanId));
        pVxlanVniVlanMapEntry->MibObject.i4FsVxlanVniVlanDfElection =
            i4FsVxlanVniVlanDfElection;
    }

    RBTreeCount (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanMultihomedPeerTable,
                 &u4Count);
    if (u4Count != EVPN_ONE)
    {
        /* Since single VRF is used, EVI ID is passed as the default context of BGP */
        (*(EVPN_NOTIFY_AD_ROUTE_CB)) (EVPN_PARAM_DEF_VRF_ID, u4VniIndex,
                                      pau1FsEvpnVxlanMHEviVniESI,
                                      (i4LBFlag == EVPN_CLI_LB_ENABLE) ?
                                      EVPN_PARAM_ALL_ACTIVE :
                                      EVPN_PARAM_SINGLE_ACTIVE);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Send the AD ROUTE trigger to BGP.\r\n"));
    }

    VxlanHwUpdateMHPeerDatabase ((UINT1) i4FsVxlanVniVlanDfElection, u4VniIndex,
                                 VXLAN_HW_ADD);
    /* Check for MAC presence */
    VlanApiGetMACUpdate (VLAN_VXLAN_EVPN_PROTO_ID, u2VlanId);

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnDFElection: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : EvpnUtilGetVniFromEsiEntry                                 */
/* Description  : This function updates ESI information to BGP after DF      */
/*                election                                                   */
/* Input        : pEvpnMultihomedPeerTable                                   */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilGetVniFromEsiEntry (UINT1 *pu1EviVniEsi, UINT4 *pu4VniIndex)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetVniFromEsiEntry: Entry\n"));
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pFsEvpnVxlanEviVniMapEntry = NULL;

    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    MEMCPY (EvpnVxlanEviVniMapEntry.MibObject.
            au1FsEvpnVxlanEviVniESI, pu1EviVniEsi, EVPN_MAX_ESI_LEN);

    pFsEvpnVxlanEviVniMapEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                       &EvpnVxlanEviVniMapEntry, NULL);
    while (NULL != pFsEvpnVxlanEviVniMapEntry)
    {
        if (MEMCMP (pFsEvpnVxlanEviVniMapEntry->MibObject.
                    au1FsEvpnVxlanEviVniESI, pu1EviVniEsi,
                    EVPN_MAX_ESI_LEN) == 0)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilGetVniFromEsiEntry: Exit\n"));
            *pu4VniIndex = pFsEvpnVxlanEviVniMapEntry->MibObject.
                u4FsEvpnVxlanEviVniMapVniNumber;
            return VXLAN_SUCCESS;
        }
        pFsEvpnVxlanEviVniMapEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                           pFsEvpnVxlanEviVniMapEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:ESI Entry does not exists\r\n"));
    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function     : EvpnUtilIsEsiPresent                                       */
/* Description  : This function updates ESI information to BGP after DF      */
/*                election                                                   */
/* Input        : pEvpnMultihomedPeerTable                                   */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilIsEsiPresent (UINT4 u4VniNumber, INT4 i4EviIndex)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilIsEsiPresent Entry\n"));
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pFsEvpnVxlanEviVniMapEntry = NULL;
    UINT1               au1EviVniESI[EVPN_MAX_ESI_LEN];

    MEMSET (&EvpnVxlanEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (&au1EviVniESI, EVPN_ZERO, sizeof (au1EviVniESI));

    EvpnVxlanEviVniMapEntry.MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber = u4VniNumber;
    EvpnVxlanEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;

    pFsEvpnVxlanEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);
    if (NULL == pFsEvpnVxlanEviVniMapEntry)
    {
        return VXLAN_FAILURE;
    }
    if (MEMCMP (pFsEvpnVxlanEviVniMapEntry->MibObject.
                au1FsEvpnVxlanEviVniESI, au1EviVniESI, EVPN_MAX_ESI_LEN) == 0)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilIsEsiPresent Exit\n"));
        return VXLAN_SUCCESS;
    }

    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function     : EvpnUtilMultihomedFwdPkt                                   */
/* Description  : This function validates singlehomed and multihomed case    */
/* Input        : u4VniNumber                                                */
/*                i4EviIndex                                                 */
/*                pPeerAddr                                                  */
/*                i4DfElection                                               */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilMultihomedFwdPkt (UINT4 u4VniNumber, INT4 i4EviIndex,
                          tIpAddr * pPeerAddr, UINT2 u2VlanId,
                          tMacAddr DestVmAddr, INT4 i4DfElection)
{
    UINT2               u2Port = 0;
    UINT1               u1Status = 0;
    UINT1               u1ESIString[EVPN_MAX_ESI_LEN];

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilMultihomedFwdPkt Entry\n"));
    MEMSET (u1ESIString, 0, EVPN_MAX_ESI_LEN);
    /* Return Failure if ESI is not present */
    if (EvpnUtilGetESIFromVniMapTable ((UINT4) i4EviIndex, u4VniNumber,
                                       u1ESIString) == VXLAN_FAILURE)
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnUtilMultihomedFwdPkt Exit for Single Home scenario\n"));
        /* Single home case */
        return VXLAN_SUCCESS;
    }
    /* Return Success for Unicast MAC */
    else if (VLAN_SUCCESS == VlanGetFdbEntryDetails (u2VlanId, DestVmAddr,
                                                     &u2Port, &u1Status))
    {
        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnUtilMultihomedFwdPkt Exit for Unicast Traffic\n"));
        /* For Unicast Traffic */
        return VXLAN_SUCCESS;
    }
    else
    {
        /* Multihomed case */
        if (EvpnUtilSplitHorizon (pPeerAddr, u1ESIString) == VXLAN_FAILURE)
        {

            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilMultihomedFwdPkt Exit for Multi Home scenario\n"));
            return VXLAN_FAILURE;
        }
        else if (i4DfElection == OSIX_TRUE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilMultihomedFwdPkt Exit because DF Already Elected\n"));
            return VXLAN_SUCCESS;
        }
    }

    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function     : EvpnUtilSplitHorizon                                       */
/* Description  : This function verify the split horizon funtionality        */
/* Input        : pPeerAddr, pu1ESIString                                    */
/* Output       : NONE                                                       */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilSplitHorizon (tIpAddr * pPeerAddr, UINT1 *pu1ESIString)
{

    tVxlanFsEvpnVxlanMultihomedPeerTable *pEvpnMultihomedPeerTable = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilSplitHorizon Entry\n"));
    pEvpnMultihomedPeerTable =
        (tVxlanFsEvpnVxlanMultihomedPeerTable *) RBTreeGetFirst (gVxlanGlobals.
                                                                 VxlanGlbMib.
                                                                 FsEvpnVxlanMultihomedPeerTable);

    while (NULL != pEvpnMultihomedPeerTable)
    {
        if ((MEMCMP (pEvpnMultihomedPeerTable->MibObject.
                     au1FsEvpnVxlanMHEviVniESI, pu1ESIString,
                     EVPN_MAX_ESI_LEN) == 0) &&
            (MEMCMP (pEvpnMultihomedPeerTable->MibObject.
                     au1FsEvpnVxlanPeerIpAddress, pPeerAddr,
                     VXLAN_IP4_ADDR_LEN) == 0))
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilSplitHorizon Exit with Failure\n"));
            return VXLAN_FAILURE;
        }

        pEvpnMultihomedPeerTable =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                           FsEvpnVxlanMultihomedPeerTable,
                           pEvpnMultihomedPeerTable, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilSplitHorizon Exit\n"));
    return VXLAN_SUCCESS;
}

/*******************************************************************************/
/* Function Name : EvpnBgpUtlGenerateESIValue                                  */
/* Description   : This function returns Default RD value for a given EVI_VNI  */
/* Input(s)      : u4VniIndex - VNI Instance                                   */
/*                 i4EviIndex - EVI Instance                                   */
/* Output(s)     : pu1ESIValue - generated ESI value for given EVI_VNI         */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                                 */
/*******************************************************************************/
INT4
EvpnBgpUtlGenerateESIValue (INT4 i4EviIndex, UINT4 u4VniIndex,
                            UINT1 *pu1ESIValue)
{
    ULONG8              u8RdValue = EVPN_ZERO;
    INT4                i4EvpnMCLAGSystemPriority = 0;
    UINT2               u2EvpnValue = EVPN_ZERO;
    tMacAddr            BaseMac;
    UINT2               u2VlanId = 0;

    tVxlanFsEvpnVxlanEviVniMapEntry VxlanSetFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnBgpUtlGenerateESIValue Entry\n"));

    MEMSET (&VxlanSetFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (&BaseMac, 0, sizeof (tMacAddr));

    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        i4FsEvpnVxlanEviVniMapEviIndex = i4EviIndex;
    VxlanSetFsEvpnVxlanEviVniMapEntry.MibObject.
        u4FsEvpnVxlanEviVniMapVniNumber = u4VniIndex;

    pVxlanFsEvpnVxlanEviVniMapEntry =
        VxlanGetFsEvpnVxlanEviVniMapTable (&VxlanSetFsEvpnVxlanEviVniMapEntry);
    if (NULL == pVxlanFsEvpnVxlanEviVniMapEntry)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnBgpUtlGenerateESIValue: EVI_VNI Entry not found\n"));
        return VXLAN_FAILURE;
    }

    MEMCPY (&u8RdValue, EVPN_P_BGP_ESI (pVxlanFsEvpnVxlanEviVniMapEntry), 8);    /* EVPN_MAX_ESI_LEN); */
    if (EVPN_ZERO != u8RdValue)
    {
        MEMCPY (&pu1ESIValue, EVPN_P_BGP_ESI (pVxlanFsEvpnVxlanEviVniMapEntry), 8);    /*EVPN_MAX_ESI_LEN); */
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnBgpUtlGenerateESIValue: ESI is already present\n"));
        return VXLAN_SUCCESS;
    }

    /* Get the Vlan Id From Vni */
    VxlanUtilGetVlanFromVni (u4VniIndex, &u2VlanId);

    /* Returns  the System Identifier and Priority for the port channel */
    if (VlanUtilEvpnGetMCLAGSystemID (u2VlanId, (tMacAddr *) & BaseMac,
                                      &i4EvpnMCLAGSystemPriority) ==
        VXLAN_SUCCESS)
    {
        u8RdValue = EVPN_ZERO;
        MEMCPY (&u8RdValue, &BaseMac, MAC_ADDR_LEN);
        if (EVPN_ZERO == u8RdValue)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        " FUNC:EvpnBgpUtlGenerateESIValue: MCLAGSystemID Entry not found\n"));
            return VXLAN_FAILURE;
        }

        /* ESI Type */
        pu1ESIValue[0] = EVPN_ESI_TYPE_1;
        /* LACP System MAC */
        MEMCPY (&pu1ESIValue[1], BaseMac, MAC_ADDR_LEN);
        /* Local Discriminator */
        u2EvpnValue = (UINT2) i4EvpnMCLAGSystemPriority;
        MEMCPY (&pu1ESIValue[7], &u2EvpnValue, sizeof (UINT2));

        VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                         "FUNC:EvpnBgpGetDefaultRdValue: Exit\n"));
        return VXLAN_SUCCESS;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnBgpGetDefaultRdValue returns failure\n"));
    return VXLAN_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetEcmpNveDatabase                            *
 *  Description     : Gets the ECMPNVE datatbase                             *
 *  Input           : u4VniId - VNI number                                   *
 *                    DestVmAddr - Destination VM mac address                *
 *                    u4IfIndex  - NVE if index vlaue                        *
 *                    i4VxlanRemoteVtepAddressType                           *
 *                    au1VxlanRemoteVtepAddress                              *
 *  Output          : None                                                   *
 *  Returns         : pVxlanEcmpNveEntry - ECMPNVE database                  *
 * ************************************************************************* */

tVxlanFsVxlanEcmpNveEntry *
VxlanUtilGetEcmpNveDatabase (UINT4 u4VniId, tMacAddr DestVmAddr,
                             UINT4 u4IfIndex, INT4 i4RemoteVtepAddressType,
                             UINT1 *pu1RemoteVtepAddress)
{
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetEcmpNveDatabase: Entry\n"));

    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4IfIndex;

    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4VniId;

    MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac, DestVmAddr,
            VXLAN_ETHERNET_ADDR_SIZE);
    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressType =
        i4RemoteVtepAddressType;
    MEMCPY (VxlanEcmpNveEntry.MibObject.au1FsVxlanEcmpNveRemoteVtepAddress,
            pu1RemoteVtepAddress,
            sizeof (VxlanEcmpNveEntry.MibObject.
                    au1FsVxlanEcmpNveRemoteVtepAddress));
    if (i4RemoteVtepAddressType == VXLAN_IPV4_UNICAST)
    {
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressLen =
            VXLAN_IP4_ADDR_LEN;
    }
    else
    {
        VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveRemoteVtepAddressLen =
            VXLAN_IP6_ADDR_LEN;
    }

    pVxlanEcmpNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                   (tRBElem *) & VxlanEcmpNveEntry);

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetEcmpNveDatabase: Exit\n"));

    return pVxlanEcmpNveEntry;
}

/* ************************************************************************* *
 *  Function Name   : VxlanUtilGetNveIndexFromEcmpNveTable                   *
 *  Description     : Gets the NVE if index from the ECMPNVE table           *
 *  Input           : u4VniNumber - VNI number                               *
 *  Output          : pu4NveIndex - Nve If Index                             *
 *  Returns         : None                                                   *
 * ************************************************************************* */

tVxlanFsVxlanEcmpNveEntry *
VxlanUtilGetNveIndexFromEcmpNveTable (UINT4 u4VniNumber, UINT4 *pu4NveIndex)
{
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromEcmpNveTable: Entry\n"));

    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber = u4VniNumber;

    pVxlanEcmpNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       (tRBElem *) & VxlanEcmpNveEntry, NULL);
    while (pVxlanEcmpNveEntry != NULL)
    {
        if (pVxlanEcmpNveEntry->MibObject.u4FsVxlanEcmpNveVniNumber ==
            u4VniNumber)
        {
            VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " NVE entry found for VNI number -"
                             "%d\n", u4VniNumber));
            *pu4NveIndex =
                (UINT4) pVxlanEcmpNveEntry->MibObject.i4FsVxlanEcmpNveIfIndex;
            return pVxlanEcmpNveEntry;
        }
        pVxlanEcmpNveEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                           (tRBElem *) pVxlanEcmpNveEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No ECMPNVE entry found for VNI number -"
                     "%d\n", u4VniNumber));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:VxlanUtilGetNveIndexFromEcmpNveTable: Exit\n"));

    return NULL;
}

/*****************************************************************************/
/* Function Name      : EvpnNveEntryTableHashFn                              */
/*                                                                           */
/* Description        : This function generates a hash index value based on  */
/*                      the MAC Address (Source / Destination MAC) given as  */
/*                      the input to this function.                          */
/*                                                                           */
/* Input(s)           : pMacAddress - MAC Address, based on which the hash   */
/*                                      index is calculated.                 */
/*                                                                           */
/* Output(s)          : pu4HashIndex - Hash Index value to be returned       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
EvpnNveEntryTableHashFn (tMacAddr * pMacAddress, UINT4 *pu4HashIndex,
                         UINT4 u4VniId, INT4 i4EviIndex)
{
    UINT2               u2Val;
    INT4                i4TotalPaths;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnVxlanEviVniMapEntry = NULL;

    MEMSET (&EvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnNveEntryTableHashFn: Entry\n"));
    EvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4EviIndex;
    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniId;
    pEvpnVxlanEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);
    i4TotalPaths =
        pEvpnVxlanEviVniMapEntry->MibObject.i4FsEvpnVxlanEviVniMapTotalPaths;
    if (i4TotalPaths == 0)
    {
        return VXLAN_FAILURE;
    }
    MEMCPY (&u2Val, ((UINT1 *) pMacAddress + 4), 2);
    u2Val = (UINT2) (OSIX_NTOHS (u2Val));
    *pu4HashIndex = (u2Val) % (UINT4) i4TotalPaths;
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnNveEntryTableHashFn: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : EvpnVxlanIsThisFirstEcmpEntry                              */
/* Description  : This function checks if this is the first Ecmp Entry       */
/*                to be added for this specific MAC or already a Ecmp        */
/*                Entry available with this MAC                              */
/* Input        : pEvpnRoute                                                 */
/*                u4NveIndex                                                 */
/* Output       : bFirstEntryFlag                                            */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
EvpnVxlanIsThisFirstEcmpEntry (tEvpnRoute * pEvpnRoute, UINT4 u4NveIndex,
                               BOOL1 * pbFirstEntryFlag)
{
    tVxlanFsVxlanEcmpNveEntry VxlanEcmpNveEntry;
    tVxlanFsVxlanEcmpNveEntry *pVxlanEcmpNveEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnVxlanIsThisFirstEcmpEntry: Entry\n"));
    MEMSET (&VxlanEcmpNveEntry, 0, sizeof (tVxlanFsVxlanEcmpNveEntry));

    VxlanEcmpNveEntry.MibObject.i4FsVxlanEcmpNveIfIndex = (INT4) u4NveIndex;
    VxlanEcmpNveEntry.MibObject.u4FsVxlanEcmpNveVniNumber =
        pEvpnRoute->u4VxlanVniNumber;
    MEMCPY (VxlanEcmpNveEntry.MibObject.FsVxlanEcmpNveDestVmMac,
            pEvpnRoute->VxlanDestVmMac, sizeof (tMacAddr));

    pVxlanEcmpNveEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanEcmpNveTable,
                       (tRBElem *) & VxlanEcmpNveEntry, NULL);
    if ((pVxlanEcmpNveEntry != NULL)
        &&
        (MEMCMP
         (pVxlanEcmpNveEntry->MibObject.au1FsVxlanEcmpMHEviVniESI,
          pEvpnRoute->au1FsEvpnMHEviVniESI, EVPN_MAX_ESI_LEN) == 0))
    {
        *pbFirstEntryFlag = VXLAN_FALSE;
    }
    else
    {
        *pbFirstEntryFlag = VXLAN_TRUE;
    }

    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC,
                     "FUNC:EvpnVxlanIsThisFirstEcmpEntry: Exit\n"));
    return;
}

/*****************************************************************************/
/* Function     : EvpnVxlanProcessEsiAndIfStateChange                        */
/* Description  : This functiion processes the ESI and Interface status      */
/*                change received from LA                                    */
/* Input        : u2VlanId - Vlan Id                                         */
/*                u1OperStatus - Interface Oper Status                       */
/* Output       : NONE                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
EvpnVxlanProcessEsiAndIfStateChange (UINT2 u2VlanId, tMacAddr SysId,
                                     UINT2 u2SysPriority, UINT1 u1OperStatus)
{
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pGetVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;
    UINT1               au1ESIValue[EVPN_MAX_ESI_LEN];
    UINT4               u4VniId = 0;
    INT4                i4EviId = 0;
    UINT4               u4NveIfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanProcessEsiAndIfStateChange: Entry\n"));
    MEMSET (au1ESIValue, 0, EVPN_MAX_ESI_LEN);
    MEMSET (&VxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (&EvpnVxlanEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniId) != VXLAN_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "VXLAN EVPN module - VNI Map to this VLAN %d is not found\r\n",
                    u2VlanId));
    }
    else
    {
        if (EvpnUtilGetEviFromVni (u4VniId, &i4EviId) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%% EVI Not Mapped for Vni %d\r\n", u4VniId));
        }
    }

    VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId = u2VlanId;
    pGetVxlanFsVxlanVniVlanMapEntry =
        VxlanGetFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry);

    /* Construct ESI */
    /* ESI Type */
    au1ESIValue[0] = EVPN_ESI_TYPE_1;
    /* LACP System MAC */
    MEMCPY (&au1ESIValue[1], SysId, MAC_ADDR_LEN);
    /* Local Discriminator */
    MEMCPY (&au1ESIValue[7], &u2SysPriority, sizeof (UINT2));

    /* Update the ESI Value to Evpn Vlan Vni Table */

    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniId;
    EvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex = i4EviId;

    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);

    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (u4VniId, &u4NveIfIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniId, &u4NveIfIndex)) ==
            NULL)
        {
            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable (u4VniId, &u4NveIfIndex)) ==
                NULL)
            {
                /* If MCAST database is also not mapped, dropped the VXLAN packet */
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "Nve index is not mapped for the VNI - %d"
                                 "in NVE, In-Replica and Multicast database\n",
                                 u4VniId));
            }
        }
    }

    pVxlanVtepEntry = EvpnGetVtepTableFromNveIdx (u4NveIfIndex);
    if (pVxlanVtepEntry != NULL)
    {
        if (EVPN_NOTIFY_ESI_UPDATE_CB != NULL)
        {
            if (u1OperStatus == EVPN_IF_OPER_UP)
            {
                if ((pGetVxlanFsVxlanVniVlanMapEntry != NULL) &&
                    (pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus !=
                     u1OperStatus))
                {
                    if (pEvpnEviVniMapEntry != NULL)
                    {
                        MEMCPY (pEvpnEviVniMapEntry->MibObject.
                                au1FsEvpnVxlanEviVniESI, au1ESIValue,
                                EVPN_MAX_ESI_LEN);
                        pEvpnEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniESILen = EVPN_MAX_ESI_LEN;
                    }
                    (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                                    u4VniId, au1ESIValue,
                                                    pVxlanVtepEntry->MibObject.
                                                    au1FsVxlanVtepAddress,
                                                    pVxlanVtepEntry->MibObject.
                                                    i4FsVxlanVtepAddressLen,
                                                    EVPN_PARAM_ESI_ADD);
                    pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus =
                        u1OperStatus;
                    VxlanUtilAddESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                                i4FsVxlanVtepAddressLen,
                                                pVxlanVtepEntry->MibObject.
                                                au1FsVxlanVtepAddress,
                                                au1ESIValue);

                }
            }
            else if (u1OperStatus == EVPN_IF_OPER_DOWN)
            {
                if ((pGetVxlanFsVxlanVniVlanMapEntry != NULL) &&
                    (pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus !=
                     u1OperStatus))
                {
                    (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                                    u4VniId, au1ESIValue,
                                                    pVxlanVtepEntry->MibObject.
                                                    au1FsVxlanVtepAddress,
                                                    pVxlanVtepEntry->MibObject.
                                                    i4FsVxlanVtepAddressLen,
                                                    EVPN_PARAM_ESI_DEL);
                    VxlanUtilDelESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                                i4FsVxlanVtepAddressLen,
                                                pVxlanVtepEntry->MibObject.
                                                au1FsVxlanVtepAddress,
                                                au1ESIValue);
                    pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus =
                        u1OperStatus;
                    if (pEvpnEviVniMapEntry != NULL)
                    {
                        MEMSET (pEvpnEviVniMapEntry->MibObject.
                                au1FsEvpnVxlanEviVniESI, EVPN_ZERO,
                                EVPN_MAX_ESI_LEN);
                        pEvpnEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniESILen = EVPN_ZERO;
                    }
                }
            }
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanProcessEsiAndIfStateChange: Exit\n"));

}

/*****************************************************************************/
/* Function     : EvpnVxlanProcessEsiAndIfStateChange                        */
/* Description  : This functiion processes the ESI and Interface status      */
/*                change received from LA                                    */
/* Input        : u2VlanId - Vlan Id                                         */
/*                u1OperStatus - Interface Oper Status                       */
/* Output       : NONE                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
EvpnVxlanProcessEsiAndIfStateChange (UINT2 u2VlanId, tMacAddr SysId,
                                     UINT2 u2SysPriority, UINT1 u1OperStatus)
{
    tVxlanFsVxlanVtepEntry *pVxlanVtepEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pGetVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry EvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pEvpnEviVniMapEntry = NULL;
    UINT1               au1ESIValue[EVPN_MAX_ESI_LEN];
    UINT4               u4VniId = 0;
    INT4                i4EviId = 0;
    UINT4               u4NveIfIndex = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanProcessEsiAndIfStateChange: Entry\n"));
    MEMSET (au1ESIValue, 0, EVPN_MAX_ESI_LEN);
    MEMSET (&VxlanFsVxlanVniVlanMapEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));
    MEMSET (&EvpnVxlanEviVniMapEntry, EVPN_ZERO,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

    if (VxlanUtilGetVniFromVlan (u2VlanId, &u4VniId) != VXLAN_SUCCESS)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "VXLAN EVPN module - VNI Map to this VLAN %d is not found\r\n",
                    u2VlanId));
    }
    else
    {
        if (EvpnUtilGetEviFromVni (u4VniId, &i4EviId) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%% EVI Not Mapped for Vni %d\r\n", u4VniId));
        }
    }

    VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId = u2VlanId;
    pGetVxlanFsVxlanVniVlanMapEntry =
        VxlanGetFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanMapEntry);

    /* Construct ESI */
    /* ESI Type */
    au1ESIValue[0] = EVPN_ESI_TYPE_1;
    /* LACP System MAC */
    MEMCPY (&au1ESIValue[1], SysId, MAC_ADDR_LEN);
    /* Local Discriminator */
    MEMCPY (&au1ESIValue[7], &u2SysPriority, sizeof (UINT2));

    /* Update the ESI Value to Evpn Vlan Vni Table */

    EvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber = u4VniId;
    EvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex = i4EviId;

    pEvpnEviVniMapEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanEviVniMapTable,
                   (tRBElem *) & EvpnVxlanEviVniMapEntry);

    /* Get the NVE if index from the VNI number */
    if ((VxlanUtilGetNveIndexFromNveTable (u4VniId, &u4NveIfIndex)) == NULL)
    {
        /* If NVE database is not mapped with the VNI number
         * Get the Ingress Replica database from the VNI number */
        if ((VxlanUtilGetNveIndexFromInReplicaTable (u4VniId, &u4NveIfIndex)) ==
            NULL)
        {
            /* If Ingress Replica database is not mapped with the VNI number
             * Get the MCAST database from the VNI number */
            if ((VxlanUtilGetNveIndexFromMcastTable (u4VniId, &u4NveIfIndex)) ==
                NULL)
            {
                /* If MCAST database is also not mapped, dropped the VXLAN packet */
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "Nve index is not mapped for the VNI - %d"
                                 "in NVE, In-Replica and Multicast database\n",
                                 u4VniId));
            }
        }
    }

    pVxlanVtepEntry = EvpnGetVtepTableFromNveIdx (u4NveIfIndex);
    if (pVxlanVtepEntry != NULL)
    {
        if (EVPN_NOTIFY_ESI_UPDATE_CB != NULL)
        {
            if (u1OperStatus == EVPN_IF_OPER_UP)
            {
                if ((pGetVxlanFsVxlanVniVlanMapEntry != NULL) &&
                    (pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus !=
                     u1OperStatus))
                {
                    if (pEvpnEviVniMapEntry != NULL)
                    {
                        MEMCPY (pEvpnEviVniMapEntry->MibObject.
                                au1FsEvpnVxlanEviVniESI, au1ESIValue,
                                EVPN_MAX_ESI_LEN);
                        pEvpnEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniESILen = EVPN_MAX_ESI_LEN;
                    }
                    (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                                    u4VniId, au1ESIValue,
                                                    pVxlanVtepEntry->MibObject.
                                                    au1FsVxlanVtepAddress,
                                                    pVxlanVtepEntry->MibObject.
                                                    i4FsVxlanVtepAddressLen,
                                                    EVPN_PARAM_ESI_ADD);
                    pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus =
                        u1OperStatus;
                    VxlanUtilAddESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                                i4FsVxlanVtepAddressLen,
                                                pVxlanVtepEntry->MibObject.
                                                au1FsVxlanVtepAddress,
                                                au1ESIValue);

                }
            }
            else if (u1OperStatus == EVPN_IF_OPER_DOWN)
            {
                if ((pGetVxlanFsVxlanVniVlanMapEntry != NULL) &&
                    (pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus !=
                     u1OperStatus))
                {
                    (*(EVPN_NOTIFY_ESI_UPDATE_CB)) (EVPN_PARAM_DEF_VRF_ID,
                                                    u4VniId, au1ESIValue,
                                                    pVxlanVtepEntry->MibObject.
                                                    au1FsVxlanVtepAddress,
                                                    pVxlanVtepEntry->MibObject.
                                                    i4FsVxlanVtepAddressLen,
                                                    EVPN_PARAM_ESI_DEL);
                    VxlanUtilDelESInfoInMHPeer (pVxlanVtepEntry->MibObject.
                                                i4FsVxlanVtepAddressLen,
                                                pVxlanVtepEntry->MibObject.
                                                au1FsVxlanVtepAddress,
                                                au1ESIValue);
                    pGetVxlanFsVxlanVniVlanMapEntry->MibObject.u1OperStatus =
                        u1OperStatus;
                    if (pEvpnEviVniMapEntry != NULL)
                    {
                        MEMSET (pEvpnEviVniMapEntry->MibObject.
                                au1FsEvpnVxlanEviVniESI, EVPN_ZERO,
                                EVPN_MAX_ESI_LEN);
                        pEvpnEviVniMapEntry->MibObject.
                            i4FsEvpnVxlanEviVniESILen = EVPN_ZERO;
                    }
                }
            }
        }
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVxlanProcessEsiAndIfStateChange: Exit\n"));

}

/* ************************************************************************* *
 *  Function Name   : EvpnUtilValidateNveTable                               *
 *  Description     : Validate NVE index and VNI from the NVE table          *
 *  Input           : u4VniNumber - VNI number                               *
 *                    u4NveIndex - Nve If Index                              *
 *  Output          : NONE                                                   *
 *  Returns         : VXLAN_SUCCESS/VXLAN_FAILURE                            *
 * ************************************************************************* */
INT4
EvpnUtilValidateNveTable (INT4 i4NveIndex, UINT4 u4VniNumber)
{
    tVxlanFsVxlanNveEntry VxlanNveEntry;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tMacAddr            ZeroVmMac;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilValidateNveTable Entry\n"));

    MEMSET (&VxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (&ZeroVmMac, 0, sizeof (tMacAddr));

    VxlanNveEntry.MibObject.i4FsVxlanNveIfIndex = i4NveIndex;
    VxlanNveEntry.MibObject.u4FsVxlanNveVniNumber = u4VniNumber;
    MEMCPY (VxlanNveEntry.MibObject.FsVxlanNveDestVmMac, ZeroVmMac,
            VXLAN_ETHERNET_ADDR_SIZE);

    pVxlanNveEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsVxlanNveTable,
                   (tRBElem *) & VxlanNveEntry);

    if (pVxlanNveEntry != NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " NVE entry found for VNI number -"
                         "%d\n", u4VniNumber));
        return VXLAN_SUCCESS;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, " No NVE entry found for VNI number -"
                     "%d\n", u4VniNumber));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilValidateNveTable Exit\n"));

    return VXLAN_FAILURE;
}

/* ************************************************************** *
 * Function   : EvpnUtilGetRdFromVrfTable                         *
 * Description: This function returns the RD entry for the        *
 *              given EVI Index and VNI Index                     *
 * Input      : pu1VxlanVrfName - Vrf Name                        *
 * Output     : pu1VniRD - Rd Value                               *
 * Returns    : VXLAN_SUCCESS/VXLAN_FAILURE                       *
 * ************************************************************** */
UINT4
EvpnUtilGetRdFromVrfTable (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex,
                           UINT1 *pu1VniRD, INT4 *pi4BgpRdAuto)
{
    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetRdFromVrfTable: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    MEMCPY (EVPN_VRF_NAME (VxlanFsEvpnVxlanVrfEntry),
            pu1VxlanVrfName, STRLEN (pu1VxlanVrfName));
    EVPN_VRF_NAME_LEN (VxlanFsEvpnVxlanVrfEntry) = STRLEN (pu1VxlanVrfName);

    EVPN_VRF_VNI (VxlanFsEvpnVxlanVrfEntry) = u4VniIndex;

    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) & VxlanFsEvpnVxlanVrfEntry);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC_FUNC ((VXLAN_FAIL_TRC, " No RD entry found for Vrf Name -"
                         "%s\n", pu1VxlanVrfName));
        return VXLAN_FAILURE;
    }

    VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC, " RD entry found for Vrf Name -"
                     "%s\n", pu1VxlanVrfName));

    MEMCPY (pu1VniRD, EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry),
            EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry));
    if (EVPN_P_VRF_RD_AUTO (pVxlanFsEvpnVxlanVrfEntry) == OSIX_TRUE)
    {
        *pi4BgpRdAuto = OSIX_TRUE;
    }

    *pi4BgpRdAuto = OSIX_FALSE;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetRdFromVrfTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ************************************************************************
 * Function   : EvpnGetiVrfRTTableFromVrf
 * Description: This function gives the VrfRT entry from the given table index
 * Input      : pu1VxlanVrfName
 * Output     : pEvpnVrfRtEntry
 * Returns    : tVxlanFsEvpnVxlanVrfRTEntry 
 * ************************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
EvpnGetVrfRTTableFromVrf (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex)
{
    tVxlanFsEvpnVxlanVrfRTEntry EvpnVrfRtEntry;
    tVxlanFsEvpnVxlanVrfRTEntry *pEvpnVrfRtEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnGetVrfRTTableFromVrf Entry\n"));

    MEMSET (&EvpnVrfRtEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    MEMCPY (EVPN_VRF_RT_NAME (EvpnVrfRtEntry), pu1VxlanVrfName,
            STRLEN (pu1VxlanVrfName));
    EVPN_VRF_RT_NAME_LEN (EvpnVrfRtEntry) = STRLEN (pu1VxlanVrfName);
    EVPN_VRF_RT_VNI (EvpnVrfRtEntry) = u4VniIndex;

    pEvpnVrfRtEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                       &EvpnVrfRtEntry, NULL);
    while (NULL != pEvpnVrfRtEntry)
    {
        if (EVPN_P_VRF_RT_AUTO (pEvpnVrfRtEntry) == OSIX_TRUE)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:RT Index value is - %d\r\n",
                             pEvpnVrfRtEntry->MibObject.
                             u4FsEvpnVxlanVrfRTIndex));
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnGetVrfRTTableFromVrf: Exit\n"));
            return pEvpnVrfRtEntry;
        }
        pEvpnVrfRtEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                           pEvpnVrfRtEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:RT Entry does not exists for EVI VNI\r\n"));
    return NULL;
}

/* ***********************************************************************
 * Function   : EvpnGetVrfRTEntryFromValue
 * Description: This function gives the RT entry from the given RT value
 * Input      : pu1VxlanVrfName
 *              pu1RtValue
 * Output     : pEvpnVrfRtEntry
 * Returns    : tVxlanFsEvpnVxlanVrfRTEntry
 * ***********************************************************************/
tVxlanFsEvpnVxlanVrfRTEntry *
EvpnGetVrfRTEntryFromValue (UINT1 *pu1VxlanVrfName, UINT1 *pu1RtValue)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnGetVrfRTEntryFromValue: Entry\n"));
    tVxlanFsEvpnVxlanVrfRTEntry *pEvpnVrfRtEntry = NULL;
    pEvpnVrfRtEntry =
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable);
    while (NULL != pEvpnVrfRtEntry)
    {
        if (MEMCMP (EVPN_P_VRF_RT_NAME (pEvpnVrfRtEntry),
                    pu1VxlanVrfName, STRLEN (pu1VxlanVrfName)) == 0)
        {
            if (MEMCMP (EVPN_P_VRF_RT (pEvpnVrfRtEntry), pu1RtValue,
                        EVPN_MAX_RT_LEN) == 0)
            {
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:RT Index value is - %d\r\n",
                                 pEvpnVrfRtEntry->MibObject.
                                 u4FsEvpnVxlanVrfRTIndex));
                VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                                 "FUNC:EvpnGetVrfRTEntryFromValue: Exit\n"));
                return pEvpnVrfRtEntry;
            }
        }
        pEvpnVrfRtEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                           pEvpnVrfRtEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:RT Entry does not exists\r\n"));
    return NULL;

}

/*******************************************************************************/
/* Function Name : EvpnVrfGetDefaultRdValue                                    */
/* Description   : This function returns Default RD value for a given Vrf      */
/* Input(s)      : pu1VxlanVrfName - Vrf Name                                   */
/* Output(s)     : pu1RouteDistinguisher - RD value for given EVI_VNI          */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                                 */
/*******************************************************************************/
INT4
EvpnVrfUtlGetDefaultRdValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT1 *pu1RouteDistinguisher)
{
    ULONG8              u8RdValue = EVPN_ZERO;
    UINT4               u4AsnValue = EVPN_ZERO;
    UINT4               u4VrfId = EVPN_ZERO;
    UINT4               u4AsnNum = EVPN_ZERO;
    UINT2               u2AsnValue = EVPN_ZERO;
    UINT2               u2VniId = EVPN_ZERO;
    UINT1               u1AsnType = EVPN_ZERO;

    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVrfGetDefaultRdValue: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMCPY (EVPN_VRF_NAME (VxlanFsEvpnVxlanVrfEntry),
            pu1VxlanVrfName, STRLEN (pu1VxlanVrfName));
    EVPN_VRF_NAME_LEN (VxlanFsEvpnVxlanVrfEntry) = STRLEN (pu1VxlanVrfName);
    EVPN_VRF_VNI (VxlanFsEvpnVxlanVrfEntry) = u4VniId;
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) & VxlanFsEvpnVxlanVrfEntry);

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnVrfGetDefaultRdValue: Vrf Entry not found\n"));
        return VXLAN_FAILURE;
    }

    MEMCPY (&u8RdValue,
            EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry), EVPN_MAX_RD_LEN);
    if (EVPN_ZERO != u8RdValue)
    {
        MEMCPY (pu1RouteDistinguisher,
                EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry), EVPN_MAX_RD_LEN);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnVrfGetDefaultRdValue: Default RD is present\n"));
        return VXLAN_SUCCESS;
    }

    EvpnBgpGetASN (&u1AsnType, &u4AsnValue);
    if (VcmIsVrfExist (pu1VxlanVrfName, &u4VrfId) == VXLAN_FALSE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
        return VXLAN_FAILURE;
    }
    EvpnUtilGetRtIndex (u4VrfId, &u4AsnNum, EVPN_VRF_VNI_RT);
    u4AsnNum++;

    if (EVPN_RD_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteDistinguisher[0] = EVPN_RD_TYPE_0;
        pu1RouteDistinguisher[1] = EVPN_RD_SUBTYPE;
        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4AsnNum = OSIX_HTONL (u4AsnNum);

        MEMCPY (&pu1RouteDistinguisher[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteDistinguisher[4], &u4AsnNum, sizeof (UINT4));
    }
    else
    {
        u2VniId = (UINT2) u4AsnNum;
        pu1RouteDistinguisher[0] = EVPN_RD_TYPE_2;
        pu1RouteDistinguisher[1] = EVPN_RD_SUBTYPE;

        u2VniId = OSIX_HTONS (u2VniId);
        u4AsnValue = OSIX_HTONL (u4AsnValue);

        MEMCPY (&pu1RouteDistinguisher[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteDistinguisher[6], &u2VniId, sizeof (UINT2));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnVrfGetDefaultRdValue: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : EvpnVrfUtlGetDefaultRtValue                               */
/* Description   : This function returns Default Rt value for a given RT     */
/* Input(s)      : pu1VxlanVrfName - Vrf Name                                */
/*                 u4RtIndex -  RT Instance                                  */
/*                 i4RtType -  RT Type                                       */
/* Output(s)     : pu1RouteTarget - RT value for given RT Table              */
/* Return(s)     : VXLAN_SUCCESS/VXLAN_FAILURE                               */
/*****************************************************************************/
INT4
EvpnVrfUtlGetDefaultRtValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT4 u4RtIndex, INT4 i4RtType,
                             UINT1 *pu1RouteTarget)
{
    ULONG8              u8RtValue = EVPN_ZERO;
    UINT4               u4AsnValue = EVPN_ZERO;
    UINT4               u4VrfId = EVPN_ZERO;
    UINT4               u4AsnNum = EVPN_ZERO;
    UINT2               u2AsnValue = EVPN_ZERO;
    UINT2               u2VniId = EVPN_ZERO;
    UINT1               u1AsnType = EVPN_ZERO;

    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry VxlanFsEvpnVxlanVrfRTEntry;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVrfUtlGetDefaultRtValue: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));
    MEMSET (&VxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    MEMCPY (EVPN_VRF_NAME (VxlanFsEvpnVxlanVrfEntry),
            pu1VxlanVrfName, STRLEN (pu1VxlanVrfName));
    EVPN_VRF_NAME_LEN (VxlanFsEvpnVxlanVrfEntry) = STRLEN (pu1VxlanVrfName);
    EVPN_VRF_VNI (VxlanFsEvpnVxlanVrfEntry) = u4VniId;
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGet (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   (tRBElem *) & VxlanFsEvpnVxlanVrfEntry);

    if (NULL == pVxlanFsEvpnVxlanVrfEntry)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnVrfUtlGetDefaultRdValue: Vrf Entry not found\n"));
        return VXLAN_FAILURE;
    }
    MEMCPY (EVPN_VRF_RT_NAME (VxlanFsEvpnVxlanVrfRTEntry),
            pu1VxlanVrfName, STRLEN (pu1VxlanVrfName));
    EVPN_VRF_RT_NAME_LEN (VxlanFsEvpnVxlanVrfRTEntry) =
        STRLEN (pu1VxlanVrfName);

    pVxlanFsEvpnVxlanVrfRTEntry = VxlanGetNextFsEvpnVxlanVrfRTTable
        (&VxlanFsEvpnVxlanVrfRTEntry);
    while (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
    {
        if ((pVxlanFsEvpnVxlanVrfRTEntry->MibObject.u4FsEvpnVxlanVrfVniNumber ==
             u4VniId)
            &&
            (MEMCMP
             (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.au1FsEvpnVxlanVrfName,
              pu1VxlanVrfName,
              EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfRTEntry)) == 0)
            && (pVxlanFsEvpnVxlanVrfRTEntry->MibObject.i4FsEvpnVxlanVrfRTAuto ==
                OSIX_TRUE))
        {
            MEMCPY (&u8RtValue,
                    EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
                    EVPN_MAX_RT_LEN);
            if (EVPN_ZERO != u8RtValue)
            {
                MEMCPY (pu1RouteTarget,
                        EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
                        EVPN_MAX_RT_LEN);
                VXLAN_TRC ((VXLAN_EVPN_TRC,
                            " FUNC:EvpnBgpGetDefaultRtValue: Default RT is present\n"));
                return VXLAN_SUCCESS;
            }
        }
        pVxlanFsEvpnVxlanVrfRTEntry =
            VxlanGetNextFsEvpnVxlanVrfRTTable (pVxlanFsEvpnVxlanVrfRTEntry);
    }

    EvpnBgpGetASN (&u1AsnType, &u4AsnValue);
    if (VcmIsVrfExist (pu1VxlanVrfName, &u4VrfId) == VXLAN_FALSE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    " FUNC:EvpnVrfGetDefaultRdValue: Failed to get Vrf Index\n"));
        return VXLAN_FAILURE;
    }
    EvpnUtilGetRtIndex (u4VrfId, &u4AsnNum, EVPN_VRF_VNI_RT);
    u4AsnNum++;

    if (EVPN_RT_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteTarget[0] = EVPN_RT_TYPE_0;
        pu1RouteTarget[1] = EVPN_RT_SUBTYPE;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4AsnNum = OSIX_HTONL (u4AsnNum);

        MEMCPY (&pu1RouteTarget[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteTarget[4], &u4AsnNum, sizeof (UINT4));
    }
    else
    {
        u2VniId = (UINT2) u4AsnNum;
        pu1RouteTarget[0] = EVPN_RT_TYPE_2;
        pu1RouteTarget[1] = EVPN_RT_SUBTYPE;

        u2VniId = OSIX_HTONS (u2VniId);
        u4AsnValue = OSIX_HTONL (u4AsnValue);

        MEMCPY (&pu1RouteTarget[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteTarget[6], &u2VniId, sizeof (UINT2));
    }
    if (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
    {
        MEMCPY (EVPN_P_VRF_RT (pVxlanFsEvpnVxlanVrfRTEntry),
                pu1RouteTarget, EVPN_MAX_RT_LEN);
    }

    UNUSED_PARAM (u4RtIndex);
    UNUSED_PARAM (i4RtType);

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnVrfUtlGetDefaultRtValue: Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlHandleRowStatusActiveVrfTable
 * Input       : pVxlanFsEvpnVxlanVrfEntry
 * Description : This function is called when the EVI-VNI row
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlHandleRowStatusActiveVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                      pVxlanFsEvpnVxlanVrfEntry,
                                      tVxlanIsSetFsEvpnVxlanVrfEntry *
                                      pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanVrfEntry);
    UINT4               u4VrfId = 0;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;
    tVxlanFsEvpnVxlanEviVniMapEntry VxlanFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanEviVniMapEntry *pVxlanFsEvpnVxlanEviVniMapEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActiveVrfTable: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:Vrf Entry does not exists\n"));
        return VXLAN_FAILURE;
    }
    if (VcmIsVrfExist (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry), &u4VrfId) ==
        VCM_FALSE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "\r%% VRF %s does not exist in "
                    "VCM\n", EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry)));
        return VXLAN_FAILURE;
    }

    /* RD Addition */
    if ((pVxlanFsEvpnVxlanVrfEntry->bRDSet == OSIX_FALSE) &&
        (EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) != 0))
    {
        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            /* Notify with RD Route Params to BGP */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_VNI
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_ADD,
                                              EVPN_P_VRF_RD
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_L3VNI_TYPE);
            pVxlanFsEvpnVxlanVrfEntry->bRDSet = OSIX_TRUE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD ADD trigger to BGP.\r\n"));
        }
        if (VxlanHwAddLoopBackPortToVlan
            (pVxlanFsEvpnVxlanVrfEntry, VXLAN_HW_ADD) == VXLAN_FAILURE)
        {
            VXLAN_TRC ((VXLAN_FAIL_TRC,
                        "Failed to Add VlanPort entry in h/w\n"));
        }
    }
    /* RD Delete */
    else if ((pVxlanFsEvpnVxlanVrfEntry->bRDSet == OSIX_TRUE) &&
             (EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) == 0))
    {
        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            /* Notify the L3Vni RD Delete to Bgp */
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_VNI
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_DEL,
                                              EVPN_P_VRF_RD
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_L3VNI_TYPE);
            pVxlanFsEvpnVxlanVrfEntry->bRDSet = OSIX_FALSE;
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD Del trigger to BGP.\r\n"));
            if (VxlanHwAddLoopBackPortToVlan
                (pVxlanFsEvpnVxlanVrfEntry, VXLAN_HW_DEL) == VXLAN_FAILURE)
            {
                VXLAN_TRC ((VXLAN_FAIL_TRC,
                            "Failed to Delete VlanPort entry in h/w\n"));
            }
        }
        /* Notify with L2Vni RD Route Params to BGP */
        pVxlanFsVxlanVniVlanMapEntry =
            RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable);
        while (pVxlanFsVxlanVniVlanMapEntry != NULL)
        {
            if (MEMCMP (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
                        pVxlanFsVxlanVniVlanMapEntry->MibObject.
                        au1FsVxlanVniVlanVrfName,
                        EVPN_P_VRF_NAME_LEN (pVxlanFsEvpnVxlanVrfEntry)) == 0)
            {
                VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
                    u4FsEvpnVxlanEviVniMapVniNumber =
                    pVxlanFsVxlanVniVlanMapEntry->MibObject.
                    u4FsVxlanVniVlanMapVniNumber;
                pVxlanFsEvpnVxlanEviVniMapEntry =
                    RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.
                                   FsEvpnVxlanEviVniMapTable,
                                   (tRBElem *) & VxlanFsEvpnVxlanEviVniMapEntry,
                                   NULL);
                if ((pVxlanFsEvpnVxlanEviVniMapEntry != NULL)
                    && (pVxlanFsEvpnVxlanEviVniMapEntry->MibObject.
                        u4FsEvpnVxlanEviVniMapVniNumber ==
                        pVxlanFsVxlanVniVlanMapEntry->MibObject.
                        u4FsVxlanVniVlanMapVniNumber))
                {
                    (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                                      pVxlanFsEvpnVxlanEviVniMapEntry->
                                                      MibObject.
                                                      u4FsEvpnVxlanEviVniMapVniNumber,
                                                      EVPN_P_PARAM_RD,
                                                      EVPN_PARAM_RD_ADD,
                                                      pVxlanFsEvpnVxlanEviVniMapEntry->
                                                      MibObject.
                                                      au1FsEvpnVxlanEviVniMapBgpRD,
                                                      EVPN_L2VNI_TYPE);
                }
            }
            pVxlanFsVxlanVniVlanMapEntry =
                RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsVxlanVniVlanMapTable,
                               (tRBElem *) pVxlanFsVxlanVniVlanMapEntry, NULL);
        }
    }
    /* RD Modification */
    else if ((pVxlanFsEvpnVxlanVrfEntry->bRDSet == OSIX_TRUE) &&
             (EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) != 0))
    {
        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_VNI
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_P_PARAM_RD,
                                              EVPN_PARAM_RD_ADD,
                                              EVPN_P_VRF_RD
                                              (pVxlanFsEvpnVxlanVrfEntry),
                                              EVPN_L3VNI_TYPE);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the RD ADD trigger to BGP.\r\n"));

        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the EVPN oper up notification to BGP.. Callback is NULL.\r\n"));
    }
    /* added to handle oper status during rd updation need to handle the same in Bgp4EvpnVrfUpHdlr */
    /*Bgp4EvpnVrfChangeHandler Bgp4EvpnVrfInit  if below code is required */
    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        if ((pVxlanFsEvpnVxlanVrfEntry->bRDSet == OSIX_TRUE) &&
            (EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) != 0))
        {
            /* Send EVPN_OPER_UP trigger to BGP once RD Configured */
            /* Since single VRF is used, EVI ID is passed as the default context of BGP */
            (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                    EVPN_P_VRF_VNI
                                                    (pVxlanFsEvpnVxlanVrfEntry),
                                                    EVPN_OPER_UP);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the oper-up trigger to BGP.\r\n"));
        }
        else if ((pVxlanFsEvpnVxlanVrfEntry->bRDSet == OSIX_FALSE) &&
                 (EVPN_P_VRF_RD_LEN (pVxlanFsEvpnVxlanVrfEntry) == 0))
        {
            /* Send EVPN_OPER_DOWN trigger to BGP */
            (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                    EVPN_P_VRF_VNI
                                                    (pVxlanFsEvpnVxlanVrfEntry),
                                                    EVPN_OPER_DOWN);
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "\r%%Send the oper-down trigger to BGP.\r\n"));
        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the EVPN oper up notification to BGP.. Callback is NULL.\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusActiveVrfTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlProcessTableCreationVrfTable
 * Input       : pVxlanFsEvpnVxlanVrfEntry
 * Description : This function is called when the EVI-VNI row
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlProcessTableCreationVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                     pVxlanFsEvpnVxlanVrfEntry,
                                     tVxlanIsSetFsEvpnVxlanVrfEntry *
                                     pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanVrfEntry);
    UINT4               u4VrfId = 0;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableCreationVrfTable: Entry\n"));
    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:VRF Entry does not exists\n"));
        return VXLAN_FAILURE;
    }
    if (VcmIsVrfExist (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry), &u4VrfId) ==
        VCM_FALSE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "\r%% VRF %s does not exist in "
                    "VCM\n", EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry)));
        return VXLAN_FAILURE;
    }
    if (EVPN_NOTIFY_L3VNI != NULL)
    {
        /* Update L3VNI to BGP, Vrf-L3Vni association indication */
        (*(EVPN_NOTIFY_L3VNI)) (u4VrfId,
                                EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry),
                                EVPN_L3VNI_ADD);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Send the L3Vni ADD trigger to BGP.\r\n"));
    }

    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        /* Send EVPN_OPER_CREATED trigger to BGP */
        (*(EVPN_NOTIFY_OPER_STATUS_CHANGE_CB)) (u4VrfId,
                                                EVPN_P_VRF_VNI
                                                (pVxlanFsEvpnVxlanVrfEntry),
                                                EVPN_OPER_CREATED);
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Send the oper created trigger to BGP.\r\n"));
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Not able to send the EVPN oper created notification to BGP.. Callback is NULL.\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableCreationVrfTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************                                                                                            
 * Function    : EvpnUtlHandleRowStatusNotInServiceVrfTable                                                                                         
 * Input       : pVxlanFsEvpnVxlanVrfEntry                                                                                                                  
 * Description : This function is called when the Vrf row
 *               status is changed to ACTIVE                                                                                                               
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlHandleRowStatusNotInServiceVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                            pVxlanFsEvpnVxlanVrfEntry,
                                            tVxlanIsSetFsEvpnVxlanVrfEntry *
                                            pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UNUSED_PARAM (pVxlanFsEvpnVxlanVrfEntry);
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanVrfEntry);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusNotInServiceVrfTable Entry\n"));
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlHandleRowStatusNotInServiceVrfTable Exit\n"));
    return VXLAN_SUCCESS;
}

/* *************************************************************
 * Function    : EvpnUtlProcessTableDeletionVrfTable
 * Input       : pVxlanFsEvpnVxlanVrfEntry
 * Description : This function is called when the EVI-VNI row
 *               status is changed to ACTIVE
 * Output      : NONE
 * Returns     : VXLAN_SUCCESS/VXLAN_FAILURE
 * *************************************************************/
UINT4
EvpnUtlProcessTableDeletionVrfTable (tVxlanFsEvpnVxlanVrfEntry *
                                     pVxlanFsEvpnVxlanVrfEntry,
                                     tVxlanIsSetFsEvpnVxlanVrfEntry *
                                     pVxlanIsSetFsEvpnVxlanVrfEntry)
{
    UNUSED_PARAM (pVxlanIsSetFsEvpnVxlanVrfEntry);
    UINT4               u4VrfId = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableDeletionVrfTable: Entry\n"));
    if (VcmIsVrfExist (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry), &u4VrfId) ==
        VCM_FALSE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "\r%% VRF %s does not exist in "
                    "VCM\n", EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry)));
        return VXLAN_FAILURE;
    }

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "FUNC:Vrf Entry does not exists\n"));
        return VXLAN_FAILURE;
    }
    if (EVPN_NOTIFY_OPER_STATUS_CHANGE_CB != NULL)
    {
        if (EVPN_NOTIFY_L3VNI != NULL)
        {
            (*(EVPN_NOTIFY_L3VNI)) (u4VrfId,
                                    EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry),
                                    EVPN_L3VNI_DEL);
        }
    }
    else
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "\r%%Cannot send notification to BGP about EVPN_OPER_DELETED/EVPN_OPER_DOWN.. Callback is NULL\r\n"));
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtlProcessTableDeletionVrfTable: Exit\n"));
    return VXLAN_SUCCESS;
}

/* ***********************************************************************
 * Function   : VxlanDelVrfRTEntry
 * Description: This function deletes entry from the VRF_RT Table
 * Input      : i4EviIndex, u4VniIdx
 * Output     : NONE
 * Returns    : NONE
 * ***********************************************************************/
VOID
VxlanDelVrfRTEntry (UINT1 *pu1VxlanVrfName, UINT4 u4VniIdx)
{
    UINT1               u1RTParamType = 0;
    UINT4               u4VrfId;
    UINT4               u4ContextId = 0;
    UINT4               u4RtIndex = 0;

    UNUSED_PARAM (u4VniIdx);
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelVrfRTEntry: Entry\n"));
    tVxlanFsEvpnVxlanVrfRTEntry *pGetVxlanFsEvpnVxlanVrfRTEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pGetNextVxlanFsEvpnVxlanVrfRTEntry = NULL;

    pGetVxlanFsEvpnVxlanVrfRTEntry = (tVxlanFsEvpnVxlanVrfRTEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable);
    while (pGetVxlanFsEvpnVxlanVrfRTEntry != NULL)
    {
        pGetNextVxlanFsEvpnVxlanVrfRTEntry = (tVxlanFsEvpnVxlanVrfRTEntry *)
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                           (tRBElem *) pGetVxlanFsEvpnVxlanVrfRTEntry, NULL);

        if (MEMCMP (EVPN_P_VRF_NAME (pGetVxlanFsEvpnVxlanVrfRTEntry),
                    pu1VxlanVrfName,
                    EVPN_P_VRF_NAME_LEN (pGetVxlanFsEvpnVxlanVrfRTEntry)) != 0)
        {
            pGetVxlanFsEvpnVxlanVrfRTEntry = pGetNextVxlanFsEvpnVxlanVrfRTEntry;
            continue;
        }
        if ((u4VniIdx != 0) &&
            (EVPN_P_VRF_VNI (pGetVxlanFsEvpnVxlanVrfRTEntry) != u4VniIdx))
        {
            pGetVxlanFsEvpnVxlanVrfRTEntry = pGetNextVxlanFsEvpnVxlanVrfRTEntry;
            continue;
        }
        u4ContextId = EVPN_P_VRF_RT_VNI (pGetVxlanFsEvpnVxlanVrfRTEntry);
        EvpnUtilGetRtIndex (u4ContextId, &u4RtIndex, EVPN_VRF_VNI_RT);

        switch (EVPN_P_VRF_RT_TYPE (pGetVxlanFsEvpnVxlanVrfRTEntry))
        {
            case EVPN_BGP_RT_IMPORT:
                u1RTParamType = EVPN_BGP4_IMPORT_RT;
                break;

            case EVPN_BGP_RT_EXPORT:
                u1RTParamType = EVPN_BGP4_EXPORT_RT;
                break;

            case EVPN_BGP_RT_BOTH:
                u1RTParamType = EVPN_BGP4_BOTH_RT;
                break;
            default:
                break;
        }
        if (VcmIsVrfExist
            (EVPN_P_VRF_RT_NAME (pGetVxlanFsEvpnVxlanVrfRTEntry),
             &u4VrfId) == VCM_FALSE)
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "\r%% VRF %s does not exist in "
                        "VCM\n",
                        EVPN_P_VRF_RT_NAME (pGetVxlanFsEvpnVxlanVrfRTEntry)));
            return;
        }
        if (EVPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
        {
            (*(EVPN_NOTIFY_ROUTE_PARAMS_CB)) (u4VrfId,
                                              EVPN_P_VRF_RT_VNI
                                              (pGetVxlanFsEvpnVxlanVrfRTEntry),
                                              u1RTParamType, EVPN_PARAM_RT_DEL,
                                              EVPN_P_VRF_RT
                                              (pGetVxlanFsEvpnVxlanVrfRTEntry),
                                              EVPN_L3VNI_TYPE);
        }
        else
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "Cannot send the RT delete notification to BGP.. since the callback is NULL\n"));
        }
        EvpnUtilReleaseRtIndex (u4RtIndex,
                                EVPN_P_VRF_RT_INDEX
                                (pGetVxlanFsEvpnVxlanVrfRTEntry),
                                EVPN_VRF_VNI_RT);

        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfRTTable,
                   pGetVxlanFsEvpnVxlanVrfRTEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFRTTABLE_POOLID,
                            (UINT1 *) pGetVxlanFsEvpnVxlanVrfRTEntry);
        pGetVxlanFsEvpnVxlanVrfRTEntry = pGetNextVxlanFsEvpnVxlanVrfRTEntry;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelVrfRTEntry: Exit\n"));
    return;
}

/* ***********************************************************************
 * Function   : VxlanDelVrfEntry
 * Description: This function deletes entry from the  VRF Table
 * Input      : i4EviIndex
 * Output     : NONE
 * Returns    : NONE
 * ***********************************************************************/
VOID
VxlanDelVrfEntry (UINT1 *pu1VxlanVrfName)
{
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelVrfEntry: Entry\n"));
    tVxlanFsEvpnVxlanVrfEntry *pGetFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pGetNextFsEvpnVxlanVrfEntry = NULL;

    pGetFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *)
        RBTreeGetFirst (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable);

    while (pGetFsEvpnVxlanVrfEntry != NULL)
    {
        pGetNextFsEvpnVxlanVrfEntry = (tVxlanFsEvpnVxlanVrfEntry *)
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           (tRBElem *) pGetFsEvpnVxlanVrfEntry, NULL);
        if (MEMCMP (EVPN_P_VRF_NAME (pGetFsEvpnVxlanVrfEntry), pu1VxlanVrfName,
                    EVPN_P_VRF_NAME_LEN (pGetFsEvpnVxlanVrfEntry)) != 0)
        {
            pGetFsEvpnVxlanVrfEntry = pGetNextFsEvpnVxlanVrfEntry;
            continue;
        }
        RBTreeRem (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                   pGetFsEvpnVxlanVrfEntry);
        MemReleaseMemBlock (VXLAN_FSEVPNVXLANVRFTABLE_POOLID,
                            (UINT1 *) pGetFsEvpnVxlanVrfEntry);
        pGetFsEvpnVxlanVrfEntry = pGetNextFsEvpnVxlanVrfEntry;
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanDelVrfEntry: Exit\n"));
    return;
}

/* ***********************************************************************
 * Function   : EvpnUtilGetVrfFromVni 
 * Description: This function gets the Vrf Id from Vlan
 * Input      : i4VniId
 * Output     : i4VrfId
 * Returns    : NONE
 * ***********************************************************************/
INT4
EvpnUtilGetVrfFromVni (UINT4 u4VniId, UINT4 *pu4VrfId)
{

    UINT2               u2VlanId = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4Return = 0;
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetVrfFromVni: Entry\n"));

    i4Return = VxlanUtilGetVlanFromVni (u4VniId, &u2VlanId);
    if (i4Return == VXLAN_FAILURE)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC,
                    "FUNC:Vni-Vlan map Entry does not exists\n"));
        return VXLAN_FAILURE;
    }
    u4IfIndex = CfaGetVlanInterfaceIndex (u2VlanId);

    if (u4IfIndex == CFA_INVALID_INDEX)
    {
        VXLAN_TRC_FUNC ((VXLAN_UTIL_TRC,
                         "Invalid L3 VLAN interface Index for the VLAN %d\n",
                         u2VlanId));
    }
    if (VcmGetIfMapVcId (u4IfIndex, pu4VrfId) == OSIX_FAILURE)
    {
        return VXLAN_FAILURE;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:EvpnUtilGetVrfFromVni: Exit\n"));
    return VXLAN_SUCCESS;
}

/*****************************************************************************/
/* Function     : EvpnUtilGetL3VniFromVrfEntry                               */
/* Description  : This function fetches L3VNI Id based on the VRF NAME       */
/* Input        : pu1VrfName                                                 */
/* Output       : pu4L3VniId                                                 */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilGetL3VniFromVrfEntry (UINT1 *pu1VrfName, UINT4 *pu4L3VniId)
{
    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetL3VniFromEsiEntry: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    MEMCPY (VxlanFsEvpnVxlanVrfEntry.MibObject.
            au1FsEvpnVxlanVrfName, pu1VrfName, EVPN_MAX_VRF_NAME_LEN);

    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                       &VxlanFsEvpnVxlanVrfEntry, NULL);
    while (NULL != pVxlanFsEvpnVxlanVrfEntry)
    {
        if ((pVxlanFsEvpnVxlanVrfEntry->MibObject.i4FsEvpnVxlanVrfNameLen ==
             (INT4) STRLEN (pu1VrfName)) &&
            (MEMCMP (pVxlanFsEvpnVxlanVrfEntry->MibObject.
                     au1FsEvpnVxlanVrfName, pu1VrfName,
                     STRLEN (pu1VrfName)) == 0))
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilGetL3VniFromEsiEntry: Exit\n"));
            *pu4L3VniId = pVxlanFsEvpnVxlanVrfEntry->MibObject.
                u4FsEvpnVxlanVrfVniNumber;
            return VXLAN_SUCCESS;
        }
        pVxlanFsEvpnVxlanVrfEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           pVxlanFsEvpnVxlanVrfEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:L3VNI Entry does not exists\r\n"));
    return VXLAN_FAILURE;
}

/*****************************************************************************/
/* Function     : EvpnUtilGetVrfNameFromVrfEntry                             */
/* Description  : This function fetches VRF NAME based on the L3 VNId        */
/* Input        : pu1VrfName                                                 */
/* Output       : pu4L3VniId                                                 */
/* Returns      : VXLAN_FAILURE/VXLAN_SUCCESS                                */
/*****************************************************************************/
INT4
EvpnUtilGetVrfNameFromVrfEntry (UINT4 u4L3VniId, UINT1 *pu1VrfName)
{
    tVxlanFsEvpnVxlanVrfEntry VxlanFsEvpnVxlanVrfEntry;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                     "FUNC:EvpnUtilGetVrfNameFromVrfEntry: Entry\n"));

    MEMSET (&VxlanFsEvpnVxlanVrfEntry, 0, sizeof (tVxlanFsEvpnVxlanVrfEntry));

    VxlanFsEvpnVxlanVrfEntry.MibObject.u4FsEvpnVxlanVrfVniNumber = u4L3VniId;
    pVxlanFsEvpnVxlanVrfEntry =
        RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                       &VxlanFsEvpnVxlanVrfEntry, NULL);
    while (NULL != pVxlanFsEvpnVxlanVrfEntry)
    {
        if (pVxlanFsEvpnVxlanVrfEntry->MibObject.u4FsEvpnVxlanVrfVniNumber ==
            u4L3VniId)
        {
            VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC,
                             "FUNC:EvpnUtilGetVrfNameFromVrfEntry: Exit\n"));
            MEMCPY (pu1VrfName, pVxlanFsEvpnVxlanVrfEntry->MibObject.
                    au1FsEvpnVxlanVrfName, EVPN_MAX_VRF_NAME_LEN);
            return VXLAN_SUCCESS;
        }
        pVxlanFsEvpnVxlanVrfEntry =
            RBTreeGetNext (gVxlanGlobals.VxlanGlbMib.FsEvpnVxlanVrfTable,
                           pVxlanFsEvpnVxlanVrfEntry, NULL);
    }

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VRF Entry does not exists\r\n"));
    return VXLAN_FAILURE;
}

/* ***************************************************************************
 * Function     : VxlanEvpnRegisterVlanInfo                                  *
 * Description  : This function is registered with ARP module to get the     *
 *                MAC/IP ADD/REMOVE information for EVPN module.             *
 * Input        : pArpBasicInfo   - ARP information                          *
 * Output       : None                                                       *
 *  Returns         : None                                                   *
 * ***************************************************************************/
VOID
VxlanEvpnRegisterArpInfo (tArpBasicInfo * pArpBasicInfo)
{
    UINT4               u4L3VniId = 0;
    UINT1               au1VrfName[EVPN_MAX_VRF_NAME_LEN];
    INT4                i4FsEvpnVxlanEnable = 0;
    UINT1               au1IpAddress[VXLAN_IP4_ADDR_LEN];
    tMacAddr            zeroMac;
    UINT4               u4VniId = 0;
    INT4                i4RetVal = 0;
    tVxlanFsVxlanNveEntry *pVxlanNveEntry = NULL;
    tVxlanEvpnArpSupLocalMacEntry *pVxlanEvpnArpSupLocalMacEntry = NULL;
    tVxlanEvpnArpSupLocalMacEntry VxlanEvpnArpSupLocalMacEntry;

    MEMSET (au1VrfName, 0, EVPN_MAX_VRF_NAME_LEN);
    MEMSET (au1IpAddress, 0, VXLAN_IP4_ADDR_LEN);
    MEMSET (zeroMac, 0, sizeof (tMacAddr));
    MEMSET (&VxlanEvpnArpSupLocalMacEntry, 0,
            sizeof (tVxlanEvpnArpSupLocalMacEntry));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnRegisterArpInfo: Entry\n"));
    VxlanGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_ENABLED)
    {
        VXLAN_TRC ((VXLAN_EVPN_TRC, "EVPN is not enabled\n"));
        return;
    }
    if (pArpBasicInfo->u4ContextId != 0)
    {
        if (VCM_FAILURE ==
            VcmGetAliasName (pArpBasicInfo->u4ContextId, au1VrfName))
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "VRF doesnt exist\n"));
            return;
        }
        if (VXLAN_FAILURE ==
            EvpnUtilGetL3VniFromVrfEntry (au1VrfName, &u4L3VniId))
        {
            VXLAN_TRC ((VXLAN_EVPN_TRC, "L3VNI is nt associated with VRF\n"));
            return;
        }
    }
    MEMCPY (au1IpAddress, &pArpBasicInfo->u4IpAddr, VXLAN_IP4_ADDR_LEN);
    i4RetVal = VxlanUtilGetVniFromVlan (pArpBasicInfo->u2VlanId, &u4VniId);
    UNUSED_PARAM (i4RetVal);
    switch (pArpBasicInfo->u1Action)
    {
        case ARP_CB_ADD_ENTRY:
            if ((EVPN_NOTIFY_MAC_UPDATE_CB != NULL) &&
                (MEMCMP (pArpBasicInfo->MacAddr, zeroMac, sizeof (tMacAddr)) !=
                 0))
            {
                EVPN_NOTIFY_MAC_UPDATE_CB (pArpBasicInfo->u4ContextId, u4VniId,
                                           u4L3VniId, pArpBasicInfo->MacAddr,
                                           VXLAN_IP4_ADDR_LEN, au1IpAddress,
                                           VXLAN_EVPN_UPDATE_MAC, 0);
            }
            break;
        case ARP_CB_REMOVE_ENTRY:
            if ((EVPN_NOTIFY_MAC_UPDATE_CB != NULL) &&
                (MEMCMP (pArpBasicInfo->MacAddr, zeroMac, sizeof (tMacAddr)) !=
                 0))
            {
                EVPN_NOTIFY_MAC_UPDATE_CB (pArpBasicInfo->u4ContextId, u4VniId,
                                           u4L3VniId, pArpBasicInfo->MacAddr,
                                           VXLAN_IP4_ADDR_LEN, au1IpAddress,
                                           VXLAN_EVPN_REMOVE_MAC, 0);
                pVxlanNveEntry = VxlanUtilGetNveIndexFromNveTable (u4VniId,
                                                                   (UINT4 *)
                                                                   &i4RetVal);
                if ((pVxlanNveEntry != NULL)
                    && (EVPN_CLI_ARP_SUPPRESS ==
                        pVxlanNveEntry->MibObject.i4FsVxlanSuppressArp))
                {
                    MEMCPY (VxlanEvpnArpSupLocalMacEntry.SourceMac,
                            pArpBasicInfo->MacAddr, sizeof (tMacAddr));
                    pVxlanEvpnArpSupLocalMacEntry = RBTreeGet (gVxlanGlobals.
                                                               VxlanEvpnArpSupLocalMacTable,
                                                               (tRBElem *) &
                                                               VxlanEvpnArpSupLocalMacEntry);
                    if (pVxlanEvpnArpSupLocalMacEntry != NULL)
                    {
                        RBTreeRem (gVxlanGlobals.VxlanEvpnArpSupLocalMacTable,
                                   pVxlanEvpnArpSupLocalMacEntry);
                        MemReleaseMemBlock (VXLAN_EVPNARPSUPMACTABLE_POOLID,
                                            (UINT1 *)
                                            pVxlanEvpnArpSupLocalMacEntry);
                    }

                }

            }
            break;

        default:
            VXLAN_TRC ((VXLAN_EVPN_TRC,
                        "VXLAN - ARP module - Invalid indication from ARP\r\n"));
            break;
    }
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanEvpnRegisterArpInfo: Exit\n"));
    return;
}

/*****************************************************************************/
/* Function     : VxlanUtilGetAnycastGwmac                                   */
/* Description  : This routine retrieves the Evpn Anycast gateway Mac Address*/
/* Input        : None                                                       */
/* Output       : pEvpnAnycastGwMac                                          */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
VxlanUtilGetAnycastGwmac (tMacAddr * pEvpnAnycastGwMac)
{
    tMacAddr            zeroMac;
    INT4                i4Ret = VXLAN_FAILURE;

    MEMSET (zeroMac, 0, sizeof (tMacAddr));

    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanUtilGetAnycastGwmac: Entry\n"));

    VxlanMainTaskLock ();
    VxlanGetFsEvpnAnycastGwMac (pEvpnAnycastGwMac);
    if (MEMCMP (pEvpnAnycastGwMac, zeroMac, sizeof (tMacAddr)) != 0)
    {
        i4Ret = VXLAN_SUCCESS;
    }
    VxlanMainTaskUnLock ();
    VXLAN_TRC_FUNC ((VXLAN_EVPN_TRC, "FUNC:VxlanUtilGetAnycastGwmac: Exit\n"));
    return i4Ret;
}
#endif /* EVPN_VXLAN_WANTED */
VOID
VxlanNvePortEntryRem (UINT1 *pau1Address, INT4 i4AddrLen)
{
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;

    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));

    MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, pau1Address, i4AddrLen);
    VxlanNvePortEntry.i4RemoteVtepAddressLen = i4AddrLen;
    pVxlanNvePortEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                                    (tRBElem *) & VxlanNvePortEntry);
    if (pVxlanNvePortEntry != NULL)
    {
        if (pVxlanNvePortEntry->u4RefCount > 1)
        {
            pVxlanNvePortEntry->u4RefCount--;
        }
        else
        {
            RBTreeRem (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                       (tRBElem *) pVxlanNvePortEntry);
            MemReleaseMemBlock (VXLAN_NVEPORTTABLE_POOLID,
                                (UINT1 *) pVxlanNvePortEntry);
        }
    }
}
tVxlanNvePortEntry *
VxlanGetNvePortEntry (UINT1 *pau1Address, INT4 i4AddrLen)
{
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;

    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));

    MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, pau1Address, i4AddrLen);
    VxlanNvePortEntry.i4RemoteVtepAddressLen = i4AddrLen;
    pVxlanNvePortEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                                    (tRBElem *) & VxlanNvePortEntry);
    return pVxlanNvePortEntry;
}

VOID
VxlanUpdateAccessPort (UINT4 u4IfIndex, UINT4 u4AccessPort,
                       UINT4 u4VniId, UINT4 u4VlanId)
{
    tVxlanVniVlanPortEntry VxlanVniVlanPortEntry;
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanMapEntry;
    tVxlanVniVlanPortEntry *pVxlanVniVlanPortEntry = NULL;
    tVxlanFsVxlanVniVlanMapEntry *pVxlanFsVxlanVniVlanMapEntry = NULL;

    VxlanFsVxlanVniVlanMapEntry.MibObject.u4FsVxlanVniVlanMapVniNumber =
        u4VniId;
    VxlanFsVxlanVniVlanMapEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
        (INT4) u4VlanId;
    VxlanVniVlanPortEntry.u4IfIndex = u4IfIndex;

    pVxlanFsVxlanVniVlanMapEntry =
        (tVxlanFsVxlanVniVlanMapEntry *) RBTreeGet (gVxlanGlobals.VxlanGlbMib.
                                                    FsVxlanVniVlanMapTable,
                                                    (tRBElem *) &
                                                    VxlanFsVxlanVniVlanMapEntry);
    if (pVxlanFsVxlanVniVlanMapEntry != NULL)
    {
        pVxlanVniVlanPortEntry =
            RBTreeGet (pVxlanFsVxlanVniVlanMapEntry->VxlanVniVlanPortTable,
                       (tRBElem *) & VxlanVniVlanPortEntry);
        if (pVxlanVniVlanPortEntry != NULL)
        {
            pVxlanVniVlanPortEntry->u4AccessPort = u4AccessPort;
        }
    }
}
VOID
VxlanUpdateNvePortEntry (UINT1 *pau1Address, INT4 i4AddessType, UINT4 u4NetPort,
                         UINT4 u4BUMPort)
{
    tVxlanNvePortEntry *pVxlanNvePortEntry = NULL;
    tVxlanNvePortEntry  VxlanNvePortEntry;

    MEMSET (&VxlanNvePortEntry, 0, sizeof (tVxlanNvePortEntry));

    if (i4AddessType == VXLAN_IPV4_UNICAST)
    {
        VxlanNvePortEntry.i4RemoteVtepAddressLen = VXLAN_IP4_ADDR_LEN;
        MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, pau1Address,
                VXLAN_IP4_ADDR_LEN);
    }
    else
    {
        VxlanNvePortEntry.i4RemoteVtepAddressLen = VXLAN_IP6_ADDR_LEN;
        MEMCPY (&VxlanNvePortEntry.au1RemoteVtepAddress, pau1Address,
                VXLAN_IP6_ADDR_LEN);
    }
    pVxlanNvePortEntry = RBTreeGet (gVxlanGlobals.VxlanGlbMib.VxlanNvePortTable,
                                    (tRBElem *) & VxlanNvePortEntry);
    if (pVxlanNvePortEntry != NULL)
    {
        if (u4BUMPort != 0)
        {
            pVxlanNvePortEntry->u4NetworkBUMPort = u4BUMPort;
            pVxlanNvePortEntry->u4NetworkPort = u4NetPort;
        }
        else
        {
            pVxlanNvePortEntry->u4NetworkPort = u4NetPort;
        }
    }
}

/*****************************************************************************/
/* Function     : VxlanPrintIpAddr                                           */
/* Description  : This routine takes the IP address in the decimal form and  */
/*                returns in the dotted notation.                            */
/* Input        : pu1Addr                                                    */
/* Output       : None                                                       */
/* Returns      : au1IpAddrBuffer                                            */
/*****************************************************************************/
UINT1              *
VxlanPrintIpAddr (UINT1 *pu1IpAddr)
{
    UINT1               au1IpAddrBuffer[VXLAN_CLI_MAX_IP_ADD_LEN];
    UINT1              *pu1AddrBuffer = NULL;
    pu1AddrBuffer = au1IpAddrBuffer;
    SPRINTF ((char *) au1IpAddrBuffer, "%d.%d.%d.%d",
             *(pu1IpAddr + 3), *(pu1IpAddr + 2), *(pu1IpAddr + 1),
             *(pu1IpAddr));
    return pu1AddrBuffer;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  vxlanutil.c                    */
/*-----------------------------------------------------------------------*/
