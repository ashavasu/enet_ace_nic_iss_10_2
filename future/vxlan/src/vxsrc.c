/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxsrc.c,v 1.26 2018/01/05 09:57:12 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*              for Vxlan module 
*********************************************************************/

#include "vxsrc.h"
#include "vxsrcdef.h"
#include "csr.h"
/****************************************************************************
 Function    : VxlanShowRunningConfig
 Description : This function displays the current configuration
                of VXLAN module
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, VxlanMainTaskLock, VxlanMainTaskUnLock);
#endif
    VXLAN_LOCK;

    CliPrintf (CliHandle, "!\r\n");
    VxlanShowRunningConfigScalar (CliHandle, u4Module);
    VxlanShowRunningConfigFsVxlanVtepTable (CliHandle, u4Module);
    VxlanShowRunningConfigFsVxlanNveTable (CliHandle, u4Module);
    VxlanShowRunningConfigFsVxlanInReplicaTable (CliHandle, u4Module);
    VxlanShowRunningConfigFsVxlanMCastTable (CliHandle, u4Module);
    VxlanShowRunningConfigFsVxlanVniVlanMapTable (CliHandle, u4Module);
#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    VXLAN_UNLOCK;
    return CLI_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/******************************************************************
 * Function    : EvpnShowRunningConfig
 * Description : This function displays the current configuration
 *               of EVPN module
 * Input       : CliHandle - Handle to the cli context
 *               u4Module - Specified module for configuration
 * Output      : None
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************/
INT4
EvpnShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, VxlanMainTaskLock, VxlanMainTaskUnLock);
#endif
    VXLAN_LOCK;
    CliPrintf (CliHandle, "!\r\n");
    EvpnShowRunningConfigScalar (CliHandle, u4Module);
    EvpnShowRunningConfigEviVniMapTable (CliHandle, u4Module);
    VxlanVrfShowRunningConfigVniMapTable (CliHandle, u4Module);
    EvpnVrfCliShowAnycastGwMac (CliHandle, u4Module);
#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    VXLAN_UNLOCK;
    return CLI_SUCCESS;
}

/******************************************************************
 * Function    : EvpnShowRunningConfigScalar
 * Description : This function displays the current configuration
 *               of the EVPN scalars
 * Input       : CliHandle - Handle to the cli context
 *               u4Module - Specified module for configuration
 * Output      : None
 * Returns     : CLI_SUCCESS always
 ******************************************************************/
INT4
EvpnShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    INT4                i4FsEvpnVxlanEnable = 0;

    UNUSED_PARAM (u4Module);
    nmhGetFsEvpnVxlanEnable (&i4FsEvpnVxlanEnable);
    if (i4FsEvpnVxlanEnable != EVPN_DEF_FSEVPNENABLE)
    {
        CliPrintf (CliHandle, "evpn overlay enable\r\n");
    }
    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : EvpnShowRunningConfigEviVniMapTable
 * Description : This function displays the current configuration
 *               of FsEvpnVxlanEviVniMapTable table
 * Input       : CliHandle - Handle to the cli context
 *               u4Module - Specified module for configuration
 * Output      : None
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
EvpnShowRunningConfigEviVniMapTable (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UNUSED_PARAM (u4Module);
    INT4                i4FsEvpnVxlanEviVniMapEviIndex = 0;
    INT4                i4NextFsEvpnVxlanEviVniMapEviIndex = 0;
    INT4                i4FsEvpnTempVxlanEviVniMapEviIndex = 0;
    INT4                i4FsEvpnVxlanBgpRTType = 0;
    INT4                i4NextFsEvpnVxlanBgpRTType = 0;
    INT4               *pi4NextFsEvpnVxlanBgpRTType = NULL;
    INT4                i4Loop = 0;
    UINT4               u4FsEvpnVxlanEviVniMapVniNumber = 0;
    UINT4               u4NextFsEvpnVxlanEviVniMapVniNumber = 0;
    UINT4               u4NextFsEvpnVxlanBgpRTIndex = 0;
    UINT4              *pu4NextFsEvpnVxlanBgpRTIndex = NULL;
    UINT4               u4FsEvpnVxlanBgpRTIndex = 0;
    UINT4               u4FsEvpnTempVxlanEviVniMapVniNumber = 0;
    UINT1               au1EvpnVxlanBgpRD[EVPN_MAX_RD_LEN];
    UINT1               au1ESIString[EVPN_MAX_ESI_LEN];

    tVxlanFsEvpnVxlanEviVniMapEntry VxlanFsEvpnVxlanEviVniMapEntry;
    tVxlanFsEvpnVxlanBgpRTEntry VxlanFsEvpnVxlanBgpRTEntry;

    MEMSET (au1EvpnVxlanBgpRD, 0, EVPN_MAX_RD_LEN);
    MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
            sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));
    MEMSET (&VxlanFsEvpnVxlanBgpRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
    MEMSET (au1ESIString, 0, EVPN_MAX_ESI_LEN);

    pu4NextFsEvpnVxlanBgpRTIndex = &u4NextFsEvpnVxlanBgpRTIndex;
    pi4NextFsEvpnVxlanBgpRTType = &i4NextFsEvpnVxlanBgpRTType;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsEvpnVxlanEviVniMapTable
        (&i4FsEvpnVxlanEviVniMapEviIndex, &u4FsEvpnVxlanEviVniMapVniNumber))
    {
        return CLI_SUCCESS;
    }

    VxlanFsEvpnVxlanEviVniMapEntry.MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
        i4FsEvpnVxlanEviVniMapEviIndex;
    VxlanFsEvpnVxlanEviVniMapEntry.MibObject.u4FsEvpnVxlanEviVniMapVniNumber =
        u4FsEvpnVxlanEviVniMapVniNumber;

    CliPrintf (CliHandle, "evpn %d\r\n", VxlanFsEvpnVxlanEviVniMapEntry.
               MibObject.i4FsEvpnVxlanEviVniMapEviIndex);
    while (1)
    {
        if (OSIX_SUCCESS !=
            VxlanGetAllFsEvpnVxlanEviVniMapTable
            (&VxlanFsEvpnVxlanEviVniMapEntry))
        {
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "map vni %d\r\n", VxlanFsEvpnVxlanEviVniMapEntry.
                   MibObject.u4FsEvpnVxlanEviVniMapVniNumber);

        /* To retrieve ESI value */
        if (VxlanFsEvpnVxlanEviVniMapEntry.
            MibObject.i4FsEvpnVxlanEviVniESILen == CLI_EVPN_MAX_ESI_LEN)
        {
            /* Displays the LB Status */
            if (VxlanFsEvpnVxlanEviVniMapEntry.
                MibObject.i4FsEvpnVxlanEviVniLoadBalance == EVPN_CLI_LB_ENABLE)
            {
                CliPrintf (CliHandle, "load-balancing enable \r\n");
            }

            CliPrintf (CliHandle, "\rethernet-segment-identifier ");
            for (i4Loop = 0; i4Loop < CLI_EVPN_MAX_ESI_LEN; i4Loop++)
            {
                /* Copy the received ESI value. */
                au1ESIString[i4Loop] =
                    (UINT1)
                    EVPN_BGP_ESI (VxlanFsEvpnVxlanEviVniMapEntry)[i4Loop];

                CliPrintf (CliHandle, "%.2x", au1ESIString[i4Loop]);
                if (i4Loop < (CLI_EVPN_MAX_ESI_LEN - 1))
                {
                    CliPrintf (CliHandle, ":");
                }
            }
            CliPrintf (CliHandle, "\r\n");
        }
        /* To retrieve RD value */
        if (0 !=
            MEMCMP (au1EvpnVxlanBgpRD,
                    EVPN_BGP_RD (VxlanFsEvpnVxlanEviVniMapEntry),
                    EVPN_MAX_RD_LEN))

        {
            /* Displays the RD configuration */
            EvpnCliShowBgpRD (CliHandle, &VxlanFsEvpnVxlanEviVniMapEntry);
        }

        /* To retrieve RT value */
        if (SNMP_SUCCESS ==
            nmhGetNextIndexFsEvpnVxlanBgpRTTable (0,
                                                  &i4NextFsEvpnVxlanEviVniMapEviIndex,
                                                  0,
                                                  &u4NextFsEvpnVxlanEviVniMapVniNumber,
                                                  0,
                                                  pu4NextFsEvpnVxlanBgpRTIndex,
                                                  0,
                                                  pi4NextFsEvpnVxlanBgpRTType))
        {
            do
            {
                u4FsEvpnTempVxlanEviVniMapVniNumber =
                    u4NextFsEvpnVxlanEviVniMapVniNumber;
                i4FsEvpnTempVxlanEviVniMapEviIndex =
                    i4NextFsEvpnVxlanEviVniMapEviIndex;
                u4FsEvpnVxlanBgpRTIndex = *pu4NextFsEvpnVxlanBgpRTIndex;
                i4FsEvpnVxlanBgpRTType = *pi4NextFsEvpnVxlanBgpRTType;

                /* RT not configure on VNI */
                if ((u4FsEvpnVxlanEviVniMapVniNumber ==
                     u4NextFsEvpnVxlanEviVniMapVniNumber)
                    && (i4FsEvpnVxlanEviVniMapEviIndex ==
                        i4NextFsEvpnVxlanEviVniMapEviIndex))
                {
                    MEMSET (&VxlanFsEvpnVxlanBgpRTEntry, 0,
                            sizeof (tVxlanFsEvpnVxlanBgpRTEntry));
                    VxlanFsEvpnVxlanBgpRTEntry.
                        MibObject.i4FsEvpnVxlanEviVniMapEviIndex =
                        i4FsEvpnVxlanEviVniMapEviIndex;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        u4FsEvpnVxlanEviVniMapVniNumber =
                        u4FsEvpnVxlanEviVniMapVniNumber;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        u4FsEvpnVxlanBgpRTIndex = u4FsEvpnVxlanBgpRTIndex;
                    VxlanFsEvpnVxlanBgpRTEntry.MibObject.
                        i4FsEvpnVxlanBgpRTType = i4FsEvpnVxlanBgpRTType;

                    if (OSIX_SUCCESS !=
                        VxlanGetAllFsEvpnVxlanBgpRTTable
                        (&VxlanFsEvpnVxlanBgpRTEntry))
                    {
                        break;
                    }

                    /* Displays the RT configuration */
                    EvpnCliShowBgpRT (CliHandle, &VxlanFsEvpnVxlanBgpRTEntry);
                }
            }
            while (nmhGetNextIndexFsEvpnVxlanBgpRTTable
                   (i4FsEvpnTempVxlanEviVniMapEviIndex,
                    &i4NextFsEvpnVxlanEviVniMapEviIndex,
                    u4FsEvpnTempVxlanEviVniMapVniNumber,
                    &u4NextFsEvpnVxlanEviVniMapVniNumber,
                    u4FsEvpnVxlanBgpRTIndex, pu4NextFsEvpnVxlanBgpRTIndex,
                    i4FsEvpnVxlanBgpRTType, pi4NextFsEvpnVxlanBgpRTType));
        }

        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "\r\n");

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsEvpnVxlanEviVniMapTable
            (i4FsEvpnVxlanEviVniMapEviIndex,
             &i4NextFsEvpnVxlanEviVniMapEviIndex,
             u4FsEvpnVxlanEviVniMapVniNumber,
             &u4NextFsEvpnVxlanEviVniMapVniNumber))
        {
            break;
        }
        if (i4NextFsEvpnVxlanEviVniMapEviIndex !=
            VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex)
        {
            CliPrintf (CliHandle, "!\r\n");
            CliPrintf (CliHandle, "evpn %d\r\n",
                       i4NextFsEvpnVxlanEviVniMapEviIndex);
        }
        i4FsEvpnVxlanEviVniMapEviIndex = i4NextFsEvpnVxlanEviVniMapEviIndex;
        u4FsEvpnVxlanEviVniMapVniNumber = u4NextFsEvpnVxlanEviVniMapVniNumber;

        MEMSET (&VxlanFsEvpnVxlanEviVniMapEntry, 0,
                sizeof (tVxlanFsEvpnVxlanEviVniMapEntry));

        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            i4FsEvpnVxlanEviVniMapEviIndex = i4NextFsEvpnVxlanEviVniMapEviIndex;
        VxlanFsEvpnVxlanEviVniMapEntry.MibObject.
            u4FsEvpnVxlanEviVniMapVniNumber =
            u4NextFsEvpnVxlanEviVniMapVniNumber;
    }
    CliPrintf (CliHandle, "!\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : EvpnCliShowBgpRD
 * Description : This function displays the RD configuration
 *               of FsEvpnVxlanEviVniMapTable table
 * Input       : CliHandle - Handle to the cli context
 *               *pVxlanFsEvpnVxlanEviVniMapEntry - Specified the Vni Table
 * Output      : None
 * Returns     : None
 ****************************************************************************/
VOID
EvpnCliShowBgpRD (tCliHandle CliHandle,
                  tVxlanFsEvpnVxlanEviVniMapEntry *
                  pVxlanFsEvpnVxlanEviVniMapEntry)
{
    UINT1               au1RDString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT2               u2ASN = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1RDString, 0, sizeof (au1RDString));

    if (pVxlanFsEvpnVxlanEviVniMapEntry == NULL)
    {
        return;
    }
    /* RD is of type  ASN:nn */
    if (EVPN_RD_TYPE_0 == EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[0])
    {
        MEMCPY (&u2ASN, &EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[2],
                sizeof (UINT2));
        MEMCPY (&u4AsignedNumber,
                &EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[4],
                sizeof (UINT4));
        u2ASN = OSIX_NTOHS (u2ASN);
        u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d:%u", u2ASN, u4AsignedNumber);
    }
    /* RD is of type  ip:nn */
    else if (EVPN_RD_TYPE_1 ==
             EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[0])
    {
        MEMCPY (&u2AsignedNumber,
                &EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[6],
                sizeof (UINT2));
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d.%d.%d.%d:%d",
                 EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[2],
                 EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[3],
                 EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[4],
                 EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[5],
                 u2AsignedNumber);
    }
    /* RD is of type  ASN.ASN:nn */
    else
    {
        MEMCPY (&u4ASN, &EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[2],
                sizeof (UINT4));
        MEMCPY (&u2AsignedNumber,
                &EVPN_P_BGP_RD (pVxlanFsEvpnVxlanEviVniMapEntry)[6],
                sizeof (UINT2));
        u4ASN = OSIX_NTOHL (u4ASN);
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d.%d:%hu",
                 (UINT4) ((u4ASN & 0xffff0000) >> 16),
                 (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
    }
    if (EVPN_P_BGP_RD_AUTO (pVxlanFsEvpnVxlanEviVniMapEntry) == OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\rrd auto-discover\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rrd manual %s\n", au1RDString);
    }
}

/****************************************************************************
 * Function    : EvpnCliShowBgpRT
 * Description : This function displays the RT configuration
 *               of FsEvpnVxlanBgpRTEntry table
 * Input       : CliHandle - Handle to the cli context
 *               *pVxlanMibFsEvpnVxlanBgpRTEntry - Specified the RT Table
 * Output      : None
 * Returns     : None
 ****************************************************************************/
VOID
EvpnCliShowBgpRT (tCliHandle CliHandle,
                  tVxlanFsEvpnVxlanBgpRTEntry * pVxlanMibFsEvpnVxlanBgpRTEntry)
{
    INT4                i4RtType = 0;
    UINT1               au1RTString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT2               u2ASN = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1RTString, 0, sizeof (au1RTString));
    if (pVxlanMibFsEvpnVxlanBgpRTEntry == NULL)
    {
        return;
    }
    i4RtType = EVPN_P_BGP_RT_TYPE (pVxlanMibFsEvpnVxlanBgpRTEntry);

    if (EVPN_P_BGP_RT_AUTO (pVxlanMibFsEvpnVxlanBgpRTEntry) == OSIX_TRUE)
    {
        if (EVPN_BGP_RT_IMPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target import auto-discover");
        }
        else if (EVPN_BGP_RT_EXPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target export auto-discover");
        }
        else
        {
            CliPrintf (CliHandle, "route-target both auto-discover");
        }
    }
    else
    {
        if (EVPN_BGP_RT_IMPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target import manual");
        }
        else if (EVPN_BGP_RT_EXPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target export manual");
        }
        else
        {
            CliPrintf (CliHandle, "route-target both manual");
        }
        /* RT is of type  ASN:nn */
        if (EVPN_RT_TYPE_0 == EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[0])
        {
            MEMCPY (&u2ASN, &EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[2],
                    sizeof (UINT2));
            MEMCPY (&u4AsignedNumber,
                    &EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            SPRINTF ((char *) au1RTString, "%d:%u", u2ASN, u4AsignedNumber);
        }
        /* RT is of type  ip:nn */
        else if (EVPN_RT_TYPE_1 ==
                 EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[0])
        {
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((char *) au1RTString, "%d.%d.%d.%d:%d",
                     EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[2],
                     EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[3],
                     EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[4],
                     EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[5],
                     u2AsignedNumber);
        }
        /* RT is of type  ASN.ASN:nn */
        else
        {
            MEMCPY (&u4ASN, &EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[2],
                    sizeof (UINT4));
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_BGP_RT (pVxlanMibFsEvpnVxlanBgpRTEntry)[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);

            SPRINTF ((char *) au1RTString, "%d.%d:%hu",
                     (UINT4) ((u4ASN & 0xffff0000) >> 16),
                     (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
        }
        CliPrintf (CliHandle, " %s\n", au1RTString);
    }

    CliPrintf (CliHandle, "\r\n");
}
#endif /* EVPN_VXLAN_WANTED */

/****************************************************************************
 Function    : VxlanShowRunningConfigScalar
 Description : This function displays the current configuration
                of the VXLAN scalars
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
VxlanShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    INT4                i4FsVxlanEnable = 0;
    UINT4               u4FsVxlanUdpPort = 0;

    UNUSED_PARAM (u4Module);
    nmhGetFsVxlanEnable (&i4FsVxlanEnable);
    if (i4FsVxlanEnable != VXLAN_DEF_FSVXLANENABLE)
    {
        CliPrintf (CliHandle, "set vxlan enable\r\n");
    }

    nmhGetFsVxlanUdpPort (&u4FsVxlanUdpPort);
    if (u4FsVxlanUdpPort != VXLAN_DEF_FSVXLANUDPPORT)
    {
        CliPrintf (CliHandle, "vxlan udp-port %d\r\n", u4FsVxlanUdpPort);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : VxlanShowRunningConfigFsVxlanVtepTable
 Description : This function displays the current configuration
                of FsVxlanVtepTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfigFsVxlanVtepTable (tCliHandle CliHandle, UINT4 u4Module,
                                        ...)
{
    tVxlanFsVxlanVtepEntry VxlanFsVxlanVtepEntry;
    INT4                i4FsVxlanVtepNveIfIndex = 0;
    INT4                i4NextFsVxlanVtepNveIfIndex = 0;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    UINT4               u4Port = 0;
    UINT4               u4L3IfIndex = 0;

    UNUSED_PARAM (u4Module);
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanFsVxlanVtepEntry, 0, sizeof (tVxlanFsVxlanVtepEntry));

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanVtepTable
        (&i4FsVxlanVtepNveIfIndex))
    {
        return CLI_SUCCESS;
    }

    VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
        i4FsVxlanVtepNveIfIndex;

    CfaCliConfGetIfName ((UINT4) i4FsVxlanVtepNveIfIndex, piIfName);
    CliPrintf (CliHandle, "interface %s\r\n", piIfName);

    while (1)
    {
        if (OSIX_SUCCESS !=
            VxlanGetAllFsVxlanVtepTable (&VxlanFsVxlanVtepEntry))
        {
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "source-interface ");
        if (VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepAddressType ==
            VXLAN_CLI_ADDRTYPE_IPV4)
        {
            MEMCPY (&(u4VtepAddr.u4Addr),
                    VxlanFsVxlanVtepEntry.MibObject.au1FsVxlanVtepAddress,
                    sizeof (UINT4));

            pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            if (NetIpv4IsLoopbackAddress (OSIX_HTONL (u4VtepAddr.u4Addr)) !=
                NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle, "vtep-ipv4 %s\r\n", pu1Address);
            }
            else
            {
                if (NETIPV4_SUCCESS == NetIpv4GetIfIndexFromAddr (OSIX_HTONL
                                                                  (u4VtepAddr.
                                                                   u4Addr),
                                                                  &u4Port))
                {
                    if (NETIPV4_SUCCESS ==
                        NetIpv4GetCfaIfIndexFromPort (u4Port, &u4L3IfIndex))
                    {
                        if (CFA_SUCCESS ==
                            CfaCliConfGetIfName (u4L3IfIndex, piIfName))
                        {
                            CliPrintf (CliHandle, "%s\r\n", piIfName);
                        }
                    }
                }
            }
        }
        else if (VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepAddressType ==
                 VXLAN_CLI_ADDRTYPE_IPV6)
        {
            MEMCPY (&Ip6Addr,
                    VxlanFsVxlanVtepEntry.MibObject.au1FsVxlanVtepAddress,
                    sizeof (tUtlIn6Addr));
            CliPrintf (CliHandle, "vtep-ipv6 %s\r\n", UtlInetNtoa6 (Ip6Addr));
        }
        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsVxlanVtepTable (i4FsVxlanVtepNveIfIndex,
                                             &i4NextFsVxlanVtepNveIfIndex))
        {
            break;
        }

        CliPrintf (CliHandle, "!\r\n");
        CfaCliConfGetIfName ((UINT4) i4NextFsVxlanVtepNveIfIndex, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);

        i4FsVxlanVtepNveIfIndex = i4NextFsVxlanVtepNveIfIndex;
        VxlanFsVxlanVtepEntry.MibObject.i4FsVxlanVtepNveIfIndex =
            i4NextFsVxlanVtepNveIfIndex;
    }
    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : VxlanShowRunningConfigFsVxlanNveTable 
 Description : This function displays the current configuration
                of FsVxlanNveTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfigFsVxlanNveTable (tCliHandle CliHandle, UINT4 u4Module,
                                       ...)
{
    tVxlanFsVxlanNveEntry VxlanFsVxlanNveEntry;
    INT4                i4FsVxlanNveIfIndex = 0;
    INT4                i4NextFsVxlanNveIfIndex = 0;
    INT4                i4TempNveIfIndex = 0;
    UINT4               u4FsVxlanNveVniNumber = 0;
    UINT4               u4NextFsVxlanNveVniNumber = 0;
    tMacAddr            u1FsVxlanNveDestVmMac;
    tMacAddr            u1NextFsVxlanNveDestVmMac;
    tMacAddr            zeroAddr;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;

    UNUSED_PARAM (u4Module);
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanFsVxlanNveEntry, 0, sizeof (tVxlanFsVxlanNveEntry));
    MEMSET (u1FsVxlanNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (u1NextFsVxlanNveDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    piIfName = (INT1 *) &au1IfName[0];

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanNveTable
        (&i4FsVxlanNveIfIndex, &u4FsVxlanNveVniNumber, &u1FsVxlanNveDestVmMac))
    {
        return CLI_SUCCESS;
    }
    while (1)
    {
        MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
        VxlanFsVxlanNveEntry.MibObject.i4FsVxlanNveIfIndex =
            i4FsVxlanNveIfIndex;
        VxlanFsVxlanNveEntry.MibObject.u4FsVxlanNveVniNumber =
            u4FsVxlanNveVniNumber;
        MEMCPY (&(VxlanFsVxlanNveEntry.MibObject.FsVxlanNveDestVmMac),
                &(u1FsVxlanNveDestVmMac), 6);

        if (OSIX_SUCCESS != VxlanGetAllFsVxlanNveTable (&VxlanFsVxlanNveEntry))
        {
            return CLI_SUCCESS;
        }

        if (VxlanFsVxlanNveEntry.MibObject.i4FsVxlanNveStorageType ==
            VXLAN_STRG_TYPE_NON_VOL)
        {
            if (i4TempNveIfIndex != i4FsVxlanNveIfIndex)
            {
                CfaCliConfGetIfName ((UINT4) i4FsVxlanNveIfIndex, piIfName);
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }

            i4TempNveIfIndex =
                VxlanFsVxlanNveEntry.MibObject.i4FsVxlanNveIfIndex;
            CliMacToStr (VxlanFsVxlanNveEntry.MibObject.FsVxlanNveDestVmMac,
                         au1MacStr);
            if (MEMCMP
                (VxlanFsVxlanNveEntry.MibObject.FsVxlanNveDestVmMac, zeroAddr,
                 VXLAN_ETHERNET_ADDR_SIZE) == 0)
            {
                if (VxlanFsVxlanNveEntry.MibObject.i4FsVxlanSuppressArp ==
                    EVPN_CLI_ARP_SUPPRESS)
                {
                    CliPrintf (CliHandle,
                               "member vni %d suppress-arp\r\n",
                               VxlanFsVxlanNveEntry.MibObject.
                               u4FsVxlanNveVniNumber);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "member vni %d ",
                               VxlanFsVxlanNveEntry.MibObject.
                               u4FsVxlanNveVniNumber);
                    if (VxlanFsVxlanNveEntry.MibObject.
                        i4FsVxlanNveRemoteVtepAddressType ==
                        VXLAN_CLI_ADDRTYPE_IPV4)
                    {
                        MEMCPY (&(u4VtepAddr.u4Addr),
                                VxlanFsVxlanNveEntry.MibObject.
                                au1FsVxlanNveRemoteVtepAddress, sizeof (UINT4));
                        pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
                        CliPrintf (CliHandle, "remote-vtep-ipv4 %s\r\n",
                                   pu1Address);
                    }
                    else if (VxlanFsVxlanNveEntry.MibObject.
                             i4FsVxlanNveRemoteVtepAddressType ==
                             VXLAN_CLI_ADDRTYPE_IPV6)
                    {
                        MEMCPY (&Ip6Addr,
                                VxlanFsVxlanNveEntry.MibObject.
                                au1FsVxlanNveRemoteVtepAddress,
                                sizeof (tUtlIn6Addr));
                        CliPrintf (CliHandle, "remote-vtep-ipv6 %s\r\n",
                                   UtlInetNtoa6 (Ip6Addr));
                    }
                }
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                au1MacStr[17] = '\0';
                CliPrintf (CliHandle,
                           "member vni %d static-vm-mac %s ",
                           VxlanFsVxlanNveEntry.MibObject.u4FsVxlanNveVniNumber,
                           au1MacStr);
                if (VxlanFsVxlanNveEntry.MibObject.
                    i4FsVxlanNveRemoteVtepAddressType ==
                    VXLAN_CLI_ADDRTYPE_IPV4)
                {
                    MEMCPY (&(u4VtepAddr.u4Addr),
                            VxlanFsVxlanNveEntry.MibObject.
                            au1FsVxlanNveRemoteVtepAddress, sizeof (UINT4));
                    pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
                    CliPrintf (CliHandle, "remote-vtep-ipv4 %s\r\n",
                               pu1Address);
                }
                else if (VxlanFsVxlanNveEntry.MibObject.
                         i4FsVxlanNveRemoteVtepAddressType ==
                         VXLAN_CLI_ADDRTYPE_IPV6)
                {
                    MEMCPY (&Ip6Addr,
                            VxlanFsVxlanNveEntry.MibObject.
                            au1FsVxlanNveRemoteVtepAddress,
                            sizeof (tUtlIn6Addr));
                    CliPrintf (CliHandle, "remote-vtep-ipv6 %s\r\n",
                               UtlInetNtoa6 (Ip6Addr));
                }
            }
        }
        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsVxlanNveTable (i4FsVxlanNveIfIndex,
                                            &i4NextFsVxlanNveIfIndex,
                                            u4FsVxlanNveVniNumber,
                                            &u4NextFsVxlanNveVniNumber,
                                            u1FsVxlanNveDestVmMac,
                                            &u1NextFsVxlanNveDestVmMac))
        {
            break;
        }

        i4FsVxlanNveIfIndex = i4NextFsVxlanNveIfIndex;
        if ((i4TempNveIfIndex != i4FsVxlanNveIfIndex) &&
            (i4TempNveIfIndex != 0))
        {
            CliPrintf (CliHandle, "!\r\n");
        }

        u4FsVxlanNveVniNumber = u4NextFsVxlanNveVniNumber;
        MEMCPY (&(u1FsVxlanNveDestVmMac), &(u1NextFsVxlanNveDestVmMac), 6);
    }
    if (i4TempNveIfIndex != 0)
    {
        CliPrintf (CliHandle, "!");
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : VxlanShowRunningConfigFsVxlanMCastTable 
 Description : This function displays the current configuration
                of FsVxlanMCastTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfigFsVxlanMCastTable (tCliHandle CliHandle, UINT4 u4Module,
                                         ...)
{
    tVxlanFsVxlanMCastEntry VxlanFsVxlaniMCastEntry;
    INT4                i4FsVxlanMCastNveIfIndex = 0;
    INT4                i4TempNveIfIndex = 0;
    INT4                i4NextFsVxlanMCastNveIfIndex = 0;
    UINT4               u4FsVxlanMCastVniNumber = 0;
    UINT4               u4NextFsVxlanMCastVniNumber = 0;
    tUtlInAddr          u4VtepAddr;
    CHR1               *pu1Address = NULL;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;

    UNUSED_PARAM (u4Module);

    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanFsVxlaniMCastEntry, 0, sizeof (tVxlanFsVxlanMCastEntry));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanMCastTable
        (&i4FsVxlanMCastNveIfIndex, &u4FsVxlanMCastVniNumber))
    {
        return CLI_SUCCESS;
    }
    VxlanFsVxlaniMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
        i4FsVxlanMCastNveIfIndex;
    VxlanFsVxlaniMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
        u4FsVxlanMCastVniNumber;

    while (1)
    {
        if (OSIX_SUCCESS !=
            VxlanGetAllFsVxlanMCastTable (&VxlanFsVxlaniMCastEntry))
        {
            return CLI_SUCCESS;
        }
        if (i4TempNveIfIndex != i4FsVxlanMCastNveIfIndex)
        {
            CfaCliConfGetIfName ((UINT4) i4FsVxlanMCastNveIfIndex, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        }

        i4TempNveIfIndex =
            VxlanFsVxlaniMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex;
        CliPrintf (CliHandle, "member vni %d ",
                   VxlanFsVxlaniMCastEntry.MibObject.u4FsVxlanMCastVniNumber);
        if (VxlanFsVxlaniMCastEntry.MibObject.i4FsVxlanMCastGroupAddressType ==
            VXLAN_CLI_ADDRTYPE_IPV4)
        {
            MEMCPY (&(u4VtepAddr.u4Addr),
                    VxlanFsVxlaniMCastEntry.MibObject.
                    au1FsVxlanMCastGroupAddress, sizeof (UINT4));
            pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            CliPrintf (CliHandle, "multicast-group-ipv4 %s\r\n", pu1Address);
        }
        else if (VxlanFsVxlaniMCastEntry.MibObject.
                 i4FsVxlanMCastGroupAddressType == VXLAN_CLI_ADDRTYPE_IPV6)
        {
            MEMCPY (&Ip6Addr,
                    VxlanFsVxlaniMCastEntry.MibObject.
                    au1FsVxlanMCastGroupAddress, sizeof (tUtlIn6Addr));
            CliPrintf (CliHandle,
                       "multicast-group-ipv6 %s\r\n", UtlInetNtoa6 (Ip6Addr));
        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsVxlanMCastTable
            (i4FsVxlanMCastNveIfIndex, &i4NextFsVxlanMCastNveIfIndex,
             u4FsVxlanMCastVniNumber, &u4NextFsVxlanMCastVniNumber))
        {
            break;
        }
        i4FsVxlanMCastNveIfIndex = i4NextFsVxlanMCastNveIfIndex;
        if (i4TempNveIfIndex != i4FsVxlanMCastNveIfIndex)
        {
            CliPrintf (CliHandle, "!\r\n");
        }
        u4FsVxlanMCastVniNumber = u4NextFsVxlanMCastVniNumber;
        VxlanFsVxlaniMCastEntry.MibObject.i4FsVxlanMCastNveIfIndex =
            i4NextFsVxlanMCastNveIfIndex;
        VxlanFsVxlaniMCastEntry.MibObject.u4FsVxlanMCastVniNumber =
            u4NextFsVxlanMCastVniNumber;

    }
    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :VxlanShowRunningConfigFsVxlanVniVlanMapTable 
 Description : This function displays the current configuration
                of FsVxlanMCastTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfigFsVxlanVniVlanMapTable (tCliHandle CliHandle,
                                              UINT4 u4Module, ...)
{
    tVxlanFsVxlanVniVlanMapEntry VxlanFsVxlanVniVlanEntry;
    INT4                i4FsVxlanVniVlanMapVlanId = 0;
    INT4                i4NextFsVxlanVniVlanMapVlanId = 0;

    UNUSED_PARAM (u4Module);

    MEMSET (&VxlanFsVxlanVniVlanEntry, 0,
            sizeof (tVxlanFsVxlanVniVlanMapEntry));

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanVniVlanMapTable
        (&i4FsVxlanVniVlanMapVlanId))
    {
        return CLI_SUCCESS;
    }
    VxlanFsVxlanVniVlanEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
        i4FsVxlanVniVlanMapVlanId;

    CliPrintf (CliHandle, "vlan %d\r\n", i4FsVxlanVniVlanMapVlanId);

    while (1)
    {
        if (OSIX_SUCCESS !=
            VxlanGetAllFsVxlanVniVlanMapTable (&VxlanFsVxlanVniVlanEntry))
        {
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle,
                   "member vni %d",
                   VxlanFsVxlanVniVlanEntry.MibObject.
                   u4FsVxlanVniVlanMapVniNumber);
        if (VxlanFsVxlanVniVlanEntry.MibObject.u1IsVlanTagged ==
            VXLAN_CLI_VLAN_TAGGED)
        {
            CliPrintf (CliHandle,
                       " tagged",
                       VxlanFsVxlanVniVlanEntry.MibObject.u1IsVlanTagged);
        }
        CliPrintf (CliHandle, "\r\n");
        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsVxlanVniVlanMapTable
            (i4FsVxlanVniVlanMapVlanId, &i4NextFsVxlanVniVlanMapVlanId))
        {
            break;
        }
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "vlan %d\r\n", i4NextFsVxlanVniVlanMapVlanId);

        i4FsVxlanVniVlanMapVlanId = i4NextFsVxlanVniVlanMapVlanId;
        VxlanFsVxlanVniVlanEntry.MibObject.i4FsVxlanVniVlanMapVlanId =
            i4NextFsVxlanVniVlanMapVlanId;

    }
    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : VxlanShowRunningConfigFsVxlanInReplicaTable
 Description : This function displays the current configuration
                of FsVxlanInReplicaTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
VxlanShowRunningConfigFsVxlanInReplicaTable (tCliHandle CliHandle,
                                             UINT4 u4Module, ...)
{
    tVxlanFsVxlanInReplicaEntry VxlanFsVxlanInReplicaEntry;
    INT4                i4FsVxlanInReplicaNveIfIndex = 0;
    INT4                i4NextFsVxlanInReplicaNveIfIndex = 0;
    INT4                i4TempNveIfIndex = 0;
    UINT4               u4FsVxlanInReplicaVniNumber = 0;
    INT4                i4FsVxlanInReplicaIndex = 0;
    UINT4               u4NextFsVxlanInReplicaVniNumber = 0;
    UINT4               u4FsVxlanInReplicaTempVniNumber = 0;
    INT4                i4NextFsVxlanInReplicaIndex = 0;
    tMacAddr            u1FsVxlanInReplicaDestVmMac;
    tMacAddr            u1NextFsVxlanInReplicaDestVmMac;
    tMacAddr            zeroAddr;
    tUtlInAddr          u4VtepAddr;
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    CHR1               *pu1Address = NULL;
    UNUSED_PARAM (u4Module);
    MEMSET (&u4VtepAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&VxlanFsVxlanInReplicaEntry, 0,
            sizeof (tVxlanFsVxlanInReplicaEntry));
    MEMSET (u1FsVxlanInReplicaDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (u1NextFsVxlanInReplicaDestVmMac, 0, sizeof (tMacAddr));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (zeroAddr, 0, VXLAN_ETHERNET_ADDR_SIZE);

    piIfName = (INT1 *) &au1IfName[0];

    if (SNMP_SUCCESS != nmhGetFirstIndexFsVxlanInReplicaTable
        (&i4FsVxlanInReplicaNveIfIndex, &u4FsVxlanInReplicaVniNumber,
         &i4FsVxlanInReplicaIndex))
    {
        return CLI_SUCCESS;
    }
    while (1)
    {
        VxlanFsVxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaNveIfIndex =
            i4FsVxlanInReplicaNveIfIndex;
        VxlanFsVxlanInReplicaEntry.MibObject.u4FsVxlanInReplicaVniNumber =
            u4FsVxlanInReplicaVniNumber;

        VxlanFsVxlanInReplicaEntry.MibObject.i4FsVxlanInReplicaIndex =
            i4FsVxlanInReplicaIndex;

        if (OSIX_SUCCESS != VxlanGetAllFsVxlanInReplicaTable
            (&VxlanFsVxlanInReplicaEntry))
        {
            return CLI_SUCCESS;
        }
        if (i4TempNveIfIndex != i4FsVxlanInReplicaNveIfIndex)
        {
            CfaCliConfGetIfName ((UINT4) i4FsVxlanInReplicaNveIfIndex,
                                 piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        }
        i4TempNveIfIndex = VxlanFsVxlanInReplicaEntry.
            MibObject.i4FsVxlanInReplicaNveIfIndex;
        if (u4FsVxlanInReplicaTempVniNumber != u4FsVxlanInReplicaVniNumber)
        {
            u4FsVxlanInReplicaTempVniNumber = u4FsVxlanInReplicaVniNumber;
            CliPrintf (CliHandle,
                       "member vni %d ingress-replication ",
                       VxlanFsVxlanInReplicaEntry.
                       MibObject.u4FsVxlanInReplicaVniNumber);

            if (VxlanFsVxlanInReplicaEntry.MibObject.
                i4FsVxlanInReplicaRemoteVtepAddressType ==
                VXLAN_CLI_ADDRTYPE_IPV4)
            {
                CliPrintf (CliHandle, "ipv4 ");
            }
            else if (VxlanFsVxlanInReplicaEntry.MibObject.
                     i4FsVxlanInReplicaRemoteVtepAddressType ==
                     VXLAN_CLI_ADDRTYPE_IPV6)

            {
                CliPrintf (CliHandle, "ipv6 ");
            }
        }

        if (VxlanFsVxlanInReplicaEntry.MibObject.
            i4FsVxlanInReplicaRemoteVtepAddressType == VXLAN_CLI_ADDRTYPE_IPV4)
        {

            MEMCPY (&(u4VtepAddr.u4Addr),
                    &(VxlanFsVxlanInReplicaEntry.MibObject.
                      au1FsVxlanInReplicaRemoteVtepAddress),
                    VXLAN_IP4_ADDR_LEN);
            pu1Address = (CHR1 *) CLI_INET_NTOA (u4VtepAddr);
            CliPrintf (CliHandle, "%s ", pu1Address);

        }

        else if (VxlanFsVxlanInReplicaEntry.MibObject.
                 i4FsVxlanInReplicaRemoteVtepAddressType ==
                 VXLAN_CLI_ADDRTYPE_IPV6)

        {
            MEMCPY (&Ip6Addr,
                    VxlanFsVxlanInReplicaEntry.MibObject.
                    au1FsVxlanInReplicaRemoteVtepAddress, sizeof (tUtlIn6Addr));
            CliPrintf (CliHandle, "%s ", UtlInetNtoa6 (Ip6Addr));

        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsVxlanInReplicaTable
            (i4FsVxlanInReplicaNveIfIndex,
             &i4NextFsVxlanInReplicaNveIfIndex,
             u4FsVxlanInReplicaVniNumber,
             &u4NextFsVxlanInReplicaVniNumber,
             i4FsVxlanInReplicaIndex, &i4NextFsVxlanInReplicaIndex))

        {
            break;
        }
        i4FsVxlanInReplicaNveIfIndex = i4NextFsVxlanInReplicaNveIfIndex;
        u4FsVxlanInReplicaVniNumber = u4NextFsVxlanInReplicaVniNumber;
        if (i4TempNveIfIndex != i4FsVxlanInReplicaNveIfIndex)
        {
            CliPrintf (CliHandle, "!\r\n");
        }

        if (u4FsVxlanInReplicaTempVniNumber != u4FsVxlanInReplicaVniNumber)
        {
            CliPrintf (CliHandle, "\r\n");
        }

        i4FsVxlanInReplicaIndex = i4NextFsVxlanInReplicaIndex;
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "!\r\n");

    return CLI_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/****************************************************************************
 * Function    : VrfShowRunningConfigVniMapTable
 * Description : This function displays the current configuration
 *               of FsEvpnVxlanEviVniMapTable table
 * Input       : CliHandle - Handle to the cli context
 *               u4Module - Specified module for configuration
 * Output      : None
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
VxlanVrfShowRunningConfigVniMapTable (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UNUSED_PARAM (u4Module);
    UINT1               au1EvpnVxlanVrfRD[EVPN_MAX_RD_LEN];
    UINT4              *pu4NextFsEvpnVxlanVrfRTIndex = NULL;
    INT4               *pi4NextFsEvpnVxlanVrfRTType = NULL;
    INT4                i4NextFsEvpnVxlanVrfRTType = 0;
    UINT4               u4NextFsEvpnVxlanVrfRTIndex = 0;
    UINT4               u4FsEvpnVxlanVrfVniMapVniNumber = 0;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfEntry = NULL;
    tVxlanFsEvpnVxlanVrfEntry *pVxlanFsEvpnVxlanVrfNextEntry = NULL;
    tVxlanFsEvpnVxlanVrfRTEntry *pVxlanFsEvpnVxlanVrfRTEntry;
    tVxlanFsEvpnVxlanVrfRTEntry VxlanFsEvpnVxlanVrfRTEntry;

    MEMSET (au1EvpnVxlanVrfRD, 0, EVPN_MAX_RD_LEN);
    MEMSET (&VxlanFsEvpnVxlanVrfRTEntry, 0,
            sizeof (tVxlanFsEvpnVxlanVrfRTEntry));

    pVxlanFsEvpnVxlanVrfEntry = VxlanGetFirstFsEvpnVxlanVrfTable ();
    pVxlanFsEvpnVxlanVrfRTEntry = VxlanGetFirstFsEvpnVxlanVrfRTTable ();

    if (pVxlanFsEvpnVxlanVrfEntry == NULL)
    {
        return CLI_SUCCESS;
    }
    pu4NextFsEvpnVxlanVrfRTIndex = &u4NextFsEvpnVxlanVrfRTIndex;
    pi4NextFsEvpnVxlanVrfRTType = &i4NextFsEvpnVxlanVrfRTType;
    u4FsEvpnVxlanVrfVniMapVniNumber =
        EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry);

    CliPrintf (CliHandle, "ip vrf  %s  vxlan\r\n",
               EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry));
    while (1)
    {
        if (OSIX_SUCCESS !=
            VxlanGetAllFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry))
        {
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "map vni %d\r\n",
                   EVPN_P_VRF_VNI (pVxlanFsEvpnVxlanVrfEntry));
        /* To retrieve RD value */
        if (0 !=
            MEMCMP (au1EvpnVxlanVrfRD,
                    EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfEntry), EVPN_MAX_RD_LEN))
        {
            /* Displays the RD configuration */
            EvpnVrfCliShowBgpRD (CliHandle, pVxlanFsEvpnVxlanVrfEntry);
        }
        /* To retrieve RT value */
        if (pVxlanFsEvpnVxlanVrfRTEntry != NULL)
        {

            do
            {
                if ((MEMCMP (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
                             EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfRTEntry),
                             EVPN_MAX_VRF_NAME_LEN) == 0))
                {
                    if (OSIX_SUCCESS !=
                        VxlanGetAllFsEvpnVxlanVrfRTTable
                        (pVxlanFsEvpnVxlanVrfRTEntry))
                    {
                        break;
                    }

                    EvpnVrfCliShowBgpRT (CliHandle,
                                         pVxlanFsEvpnVxlanVrfRTEntry);
                }

                pVxlanFsEvpnVxlanVrfRTEntry =
                    VxlanGetNextFsEvpnVxlanVrfRTTable
                    (pVxlanFsEvpnVxlanVrfRTEntry);
            }
            while (pVxlanFsEvpnVxlanVrfRTEntry != NULL);

        }

        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "\r\n");
        pVxlanFsEvpnVxlanVrfNextEntry =
            VxlanGetNextFsEvpnVxlanVrfTable (pVxlanFsEvpnVxlanVrfEntry);
        if (NULL == pVxlanFsEvpnVxlanVrfNextEntry)
        {
            break;
        }

        if (MEMCMP
            (EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfEntry),
             EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfNextEntry),
             EVPN_MAX_VRF_NAME_LEN) != 0)
        {
            CliPrintf (CliHandle, "!\r\n");
            CliPrintf (CliHandle, "ip vrf  %s vxlan\r\n",
                       EVPN_P_VRF_NAME (pVxlanFsEvpnVxlanVrfNextEntry));
        }
        pVxlanFsEvpnVxlanVrfEntry = pVxlanFsEvpnVxlanVrfNextEntry;
        pVxlanFsEvpnVxlanVrfRTEntry = VxlanGetFirstFsEvpnVxlanVrfRTTable ();
    }

    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;

}

/****************************************************************************
 * Function    : EvpnVrfShowBgpRD
 * Description : This function displays the RD configuration
 *               of FsEvpnVxlanEviVniMapTable table
 * Input       : CliHandle - Handle to the cli context
 *               *pVxlanFsEvpnVxlanVrfVniMapEntry - Specified the Vni Table
 * Output      : None
 * Returns     : None
 ****************************************************************************/
VOID
EvpnVrfCliShowBgpRD (tCliHandle CliHandle,
                     tVxlanFsEvpnVxlanVrfEntry *
                     pVxlanFsEvpnVxlanVrfVniMapEntry)
{
    UINT1               au1RDString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT2               u2ASN = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1RDString, 0, sizeof (au1RDString));

    if (pVxlanFsEvpnVxlanVrfVniMapEntry == NULL)
    {
        return;
    }
    /* RD is of type  ASN:nn */
    if (EVPN_RD_TYPE_0 == EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[0])
    {
        MEMCPY (&u2ASN, &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[2],
                sizeof (UINT2));
        MEMCPY (&u4AsignedNumber,
                &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[4],
                sizeof (UINT4));
        u2ASN = OSIX_NTOHS (u2ASN);
        u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d:%u", u2ASN, u4AsignedNumber);
    }
    /* RD is of type  ip:nn */
    else if (EVPN_RD_TYPE_1 ==
             EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[0])
    {
        MEMCPY (&u2AsignedNumber,
                &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[6],
                sizeof (UINT2));
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d.%d.%d.%d:%d",
                 EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[2],
                 EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[3],
                 EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[4],
                 EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[5],
                 u2AsignedNumber);
    }
    /* RD is of type  ASN.ASN:nn */
    else
    {
        MEMCPY (&u4ASN, &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[2],
                sizeof (UINT4));
        MEMCPY (&u2AsignedNumber,
                &EVPN_P_VRF_RD (pVxlanFsEvpnVxlanVrfVniMapEntry)[6],
                sizeof (UINT2));
        u4ASN = OSIX_NTOHL (u4ASN);
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%d.%d:%hu",
                 (UINT4) ((u4ASN & 0xffff0000) >> 16),
                 (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
    }
    if (EVPN_P_VRF_RD_AUTO (pVxlanFsEvpnVxlanVrfVniMapEntry) == OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\rrd auto-discover\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rrd manual %s\n", au1RDString);
    }
}

/****************************************************************************
 * Function    : EvpnVrfCliShowBgpRT
 * Description : This function displays the RT configuration
 *               of FsEvpnVxlanVrfRTEntry table
 * Input       : CliHandle - Handle to the cli context
 *               *pVxlanMibFsEvpnVxlanVrfRTEntry - Specified the RT Table
 * Output      : None
 * Returns     : None
 ****************************************************************************/
VOID
EvpnVrfCliShowBgpRT (tCliHandle CliHandle,
                     tVxlanFsEvpnVxlanVrfRTEntry *
                     pVxlanMibFsEvpnVxlanVrfRTEntry)
{
    INT4                i4RtType = 0;
    UINT1               au1RTString[EVPN_MAX_RD_RT_STRING_LEN];
    UINT2               u2ASN = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1RTString, 0, sizeof (au1RTString));
    if (pVxlanMibFsEvpnVxlanVrfRTEntry == NULL)
    {
        return;
    }
    i4RtType = EVPN_P_VRF_RT_TYPE (pVxlanMibFsEvpnVxlanVrfRTEntry);

    if (EVPN_P_VRF_RT_AUTO (pVxlanMibFsEvpnVxlanVrfRTEntry) == OSIX_TRUE)
    {
        if (EVPN_BGP_RT_IMPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target import auto-discover");
        }
        else if (EVPN_BGP_RT_EXPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target export auto-discover");
        }
        else
        {
            CliPrintf (CliHandle, "route-target both auto-discover");
        }
    }
    else
    {
        if (EVPN_BGP_RT_IMPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target import manual");
        }
        else if (EVPN_BGP_RT_EXPORT == i4RtType)
        {
            CliPrintf (CliHandle, "route-target export manual");
        }
        else
        {
            CliPrintf (CliHandle, "route-target both manual");
        }
        /* RT is of type  ASN:nn */
        if (EVPN_RT_TYPE_0 == EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[0])
        {
            MEMCPY (&u2ASN, &EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[2],
                    sizeof (UINT2));
            MEMCPY (&u4AsignedNumber,
                    &EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            SPRINTF ((char *) au1RTString, "%d:%u", u2ASN, u4AsignedNumber);
        }
        /* RT is of type  ip:nn */
        else if (EVPN_RT_TYPE_1 ==
                 EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[0])
        {
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((char *) au1RTString, "%d.%d.%d.%d:%d",
                     EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[2],
                     EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[3],
                     EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[4],
                     EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[5],
                     u2AsignedNumber);
        }
        /* RT is of type  ASN.ASN:nn */
        else
        {
            MEMCPY (&u4ASN, &EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[2],
                    sizeof (UINT4));
            MEMCPY (&u2AsignedNumber,
                    &EVPN_P_VRF_RT (pVxlanMibFsEvpnVxlanVrfRTEntry)[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((char *) au1RTString, "%d.%d:%hu",
                     (UINT4) ((u4ASN & 0xffff0000) >> 16),
                     (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);

        }
        CliPrintf (CliHandle, " %s\n", au1RTString);
    }

    CliPrintf (CliHandle, "\r\n");
}

/****************************************************************************
 * Function    : EvpnVrfCliShowAnycastGwMac
 * Description : This function displays the Anycast GatewayMAC  configuration
 *               of FsEvpnVxlanVrfRTEntry table
 * Input       : CliHandle - Handle to the cli context
 * Output      : None
 * Returns     : None
 ****************************************************************************/
VOID
EvpnVrfCliShowAnycastGwMac (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UNUSED_PARAM (u4Module);
    tMacAddr            au1RetValFsEvpnAnycastGwMac;
    tMacAddr            tZeroFsEvpnAnycastGwMac;
    UINT1               au1MacStr[VXLAN_MAC_STR_LEN];

    MEMSET (tZeroFsEvpnAnycastGwMac, 0, VXLAN_ETHERNET_ADDR_SIZE);
    MEMSET (au1RetValFsEvpnAnycastGwMac, 0, sizeof (tMacAddr));
    MEMSET (au1MacStr, 0, VXLAN_MAC_STR_LEN);
    nmhGetFsEvpnAnycastGwMac (&au1RetValFsEvpnAnycastGwMac);

    if ((0 != MEMCMP (au1RetValFsEvpnAnycastGwMac,
                      tZeroFsEvpnAnycastGwMac, VXLAN_ETHERNET_ADDR_SIZE)))
    {
        CliMacToStr (au1RetValFsEvpnAnycastGwMac, au1MacStr);
        au1MacStr[17] = '\0';
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "fabric forwarding anycast-gateway-mac %s\r\n",
                   au1MacStr);
    }
}
#endif
