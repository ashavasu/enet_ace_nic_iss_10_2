/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vxtask.c,v 1.2 2015/02/05 11:31:48 siva Exp $
 *
 * Description:This file contains procedures related to
 *             VXLAN - Task Initialization
 *******************************************************************/

#include "vxinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : VxlanTaskSpawnVxlanTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Vxlan Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if VXLAN Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
VxlanTaskSpawnVxlanTask (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "FUNC:VxlanTaskSpawnVxlanTask: Entry\n"));

    /* task initializations */
    if (VxlanMainTaskInit () == OSIX_FAILURE)
    {
        VXLAN_TRC ((VXLAN_MAIN_TRC, " !!!!! VXLAN TASK INIT FAILURE  !!!!! \n"));
        VxlanMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* spawn vxlan task */

    /*if (OsixCreateTask ((const UINT1 *) VXLAN_TASK_NAME, VXLAN_TASK_PRIORITY,
       OSIX_DEFAULT_STACK_SIZE,
       VxlanMainTask, NULL, OSIX_DEFAULT_TASK_MODE,
       (tOsixTaskId *) & (gVxlanGlobals.vxlanTaskId))
       == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } */

    if (OsixGetTaskId (SELF, (UINT1 *) VXLAN_TASK_NAME, &(gVxlanGlobals.vxlanTaskId))
                       == OSIX_FAILURE)
    {
        VXLAN_TRC((VXLAN_MAIN_TRC,"Error in getting task Id for VXLAN\n"));
        VxlanMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    VxlanTaskRegisterVxlanMibs();
    lrInitComplete (OSIX_SUCCESS);
    VxlanMainTask ();
    VXLAN_TRC_FUNC ((VXLAN_ENTRY_EXIT_TRC, "EXIT:VxlanTaskSpawnVxlanTask:Exit\n"));
    return;
}

PUBLIC INT4 
VxlanTaskRegisterVxlanMibs ()
{
    RegisterFSVXLA ();
    return VXLAN_SUCCESS;
}



/*------------------------------------------------------------------------*/
/*                        End of the file  vxlantask.c                     */
/*------------------------------------------------------------------------*/
