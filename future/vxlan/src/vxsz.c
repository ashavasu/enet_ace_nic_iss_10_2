#define _VXLANSZ_C
/********************************************************************
* Copyright (C) 2014  Aricent Inc . All Rights Reserved
*
* $Id: vxsz.c,v 1.2 2014/08/24 12:01:42 siva Exp $
*
*********************************************************************/

#include "vxinc.h"

extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
VxlanSizingMemCreateMemPools ()
{
    INT4                i4RetVal = 0;
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < VXLAN_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = (INT4)
            MemCreateMemPool (FsVXLANSizingParams[i4SizingId].u4StructSize,
                              FsVXLANSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(VXLANMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            VxlanSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
VxlanSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsVXLANSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, VXLANMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
VxlanSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < VXLAN_MAX_SIZING_ID; i4SizingId++)
    {
        if (VXLANMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (VXLANMemPoolIds[i4SizingId]);
            VXLANMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
