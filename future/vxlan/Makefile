.PHONY:  all clean tags tgz

include ../LR/make.h
include ../LR/make.rule
VXLAN = ${BASE_DIR}/vxlan

VXLANSRC=$(VXLAN)/src/
VXLANOBJ=$(VXLAN)/obj/
VXLANINC=$(VXLAN)/inc/
VXLANINCL= \
    -I$(BASE_DIR)/vxlan\
    -I$(BASE_DIR)/vxlan/inc\
    -I$(BASE_DIR)/vxlan/src\
    -I$(BASE_DIR)/vxlan/obj

VXLAN_HEADER_FILES= \
    $(VXLANINC)vxdefg.h \
    $(VXLANINC)vxmibcli.h \
    $(VXLANINC)vxsz.h \
    $(VXLANINC)vxglob.h \
    $(VXLANINC)vxsrcdef.h \
    $(VXLANINC)fsvxdb.h \
    $(VXLANINC)vxextn.h \
    $(VXLANINC)vxtdfsg.h \
    $(VXLANINC)vxlan.h \
    $(VXLANINC)vxprotg.h \
    $(VXLANINC)vxprot.h \
    $(VXLANINC)vxdefn.h \
    $(VXLANINC)vxwrg.h \
    $(VXLANINC)vxtdfs.h \
    $(VXLANINC)vxtrc.h \
    $(VXLANINC)vxred.h \
    $(VXLANINC)vxsrc.h \
    $(VXLANINC)vxinc.h \
    $(VXLANINC)vxlwg.h 

VXLAN_INCLUDES=$(COMMON_INCLUDE_DIRS) $(VXLANINCL)

VXLAN_COMPILATION_SWITCHES = -DDVXLAN_TRACE_WANTED $(VXLAN_EXP_FLAGS)

VXLAN_FLAGS =$(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)\
            $(VXLAN_COMPILATION_SWITCHES)

CC+=$(CC_FLAGS)
FINAL_VXLAN_OBJ=$(VXLANOBJ)FutureVxlan.o

VXLAN_OBJS= \
    $(VXLANOBJ)vxport.o \
    $(VXLANOBJ)vxutil.o \
    $(VXLANOBJ)vxdsg.o \
    $(VXLANOBJ)vxdefg.o \
    $(VXLANOBJ)vxque.o \
    $(VXLANOBJ)vxudp.o \
    $(VXLANOBJ)vxmain.o \
    $(VXLANOBJ)vxlwg.o \
    $(VXLANOBJ)vxlw.o \
    $(VXLANOBJ)vxsrc.o \
    $(VXLANOBJ)vxutlg.o \
    $(VXLANOBJ)vxdbg.o \
    $(VXLANOBJ)vxdb.o \
    $(VXLANOBJ)vxfwd.o \
    $(VXLANOBJ)vxred.o \
    $(VXLANOBJ)vxdef.o \
    $(VXLANOBJ)vxtask.o \
    $(VXLANOBJ)vxtrc.o \
    $(VXLANOBJ)vxsz.o \
    $(VXLANOBJ)vxweb.o \
    $(VXLANOBJ)vxmbsm.o \
    $(VXLANOBJ)vxnpwr.o

ifeq (${SNMP_2}, YES)
VXLAN_OBJS += \
    $(VXLANOBJ)vxwrg.o
endif

ifeq (${CLI}, YES)
VXLAN_OBJS += \
    $(VXLANOBJ)vxshcli.o \
    $(VXLANOBJ)vxclig.o
endif

all:  CHECK_OBJ_DIR	${VXLAN_OBJS}
	${LD} ${LD_FLAGS} -o $(FINAL_VXLAN_OBJ) $(VXLAN_OBJS)

CHECK_OBJ_DIR:
	mkdir -p obj


$(VXLANOBJ)vxport.o :	$(VXLANSRC)vxport.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxport.c  -o $@

$(VXLANOBJ)vxutil.o :	$(VXLANSRC)vxutil.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxutil.c  -o $@

$(VXLANOBJ)vxdsg.o :	$(VXLANSRC)vxdsg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxdsg.c  -o $@

$(VXLANOBJ)vxdefg.o :	$(VXLANSRC)vxdefg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxdefg.c  -o $@

$(VXLANOBJ)vxque.o :	$(VXLANSRC)vxque.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxque.c  -o $@

$(VXLANOBJ)vxudp.o :	$(VXLANSRC)vxudp.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxudp.c  -o $@

$(VXLANOBJ)vxmain.o :	$(VXLANSRC)vxmain.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxmain.c  -o $@

$(VXLANOBJ)vxlwg.o :	$(VXLANSRC)vxlwg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxlwg.c  -o $@

$(VXLANOBJ)vxlw.o :	$(VXLANSRC)vxlw.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxlw.c  -o $@

$(VXLANOBJ)vxshcli.o :	$(VXLANSRC)vxshcli.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxshcli.c  -o $@

$(VXLANOBJ)vxsrc.o :	$(VXLANSRC)vxsrc.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxsrc.c  -o $@

$(VXLANOBJ)vxutlg.o :	$(VXLANSRC)vxutlg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxutlg.c  -o $@

$(VXLANOBJ)vxwrg.o :	$(VXLANSRC)vxwrg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxwrg.c  -o $@

$(VXLANOBJ)vxdbg.o :	$(VXLANSRC)vxdbg.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxdbg.c  -o $@

$(VXLANOBJ)vxdb.o :	$(VXLANSRC)vxdb.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxdb.c  -o $@

$(VXLANOBJ)vxfwd.o :	$(VXLANSRC)vxfwd.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxfwd.c  -o $@

$(VXLANOBJ)vxred.o :	$(VXLANSRC)vxred.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxred.c  -o $@

$(VXLANOBJ)vxdef.o :	$(VXLANSRC)vxdef.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxdef.c  -o $@

$(VXLANOBJ)vxtask.o :	$(VXLANSRC)vxtask.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxtask.c  -o $@

$(VXLANOBJ)vxclig.o :	$(VXLANSRC)vxclig.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxclig.c  -o $@

$(VXLANOBJ)vxtrc.o :	$(VXLANSRC)vxtrc.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxtrc.c  -o $@

$(VXLANOBJ)vxsz.o :	$(VXLANSRC)vxsz.c   $(VXLAN_HEADER_FILES) 
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxsz.c  -o $@

$(VXLANOBJ)vxweb.o : $(VXLANSRC)vxweb.c   $(VXLAN_HEADER_FILES)
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxweb.c  -o $@

$(VXLANOBJ)vxmbsm.o : $(VXLANSRC)vxmbsm.c   $(VXLAN_HEADER_FILES)
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxmbsm.c  -o $@

$(VXLANOBJ)vxnpwr.o : $(VXLANSRC)vxnpwr.c   $(VXLAN_HEADER_FILES)
	$(CC) $(VXLAN_FLAGS)  $(VXLAN_INCLUDES)  $(VXLANSRC)vxnpwr.c  -o $@
clean:
	rm -f $(VXLANOBJ)*.o
tgz:
	make clean
	tar cvfz vxlan.tgz src/ inc/ Makefile make.h
tags:
	ctags -uR src/ inc/

