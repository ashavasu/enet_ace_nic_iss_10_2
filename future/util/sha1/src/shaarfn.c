/************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarfn.c,v 1.1 2015/04/28 12:53:05 siva Exp $
 *
 * Description: This file contains the source code for SHA1 module
 ************************************************************************/
#include "shaarglb.h"
#include "shaarinc.h"

/*****************************************************************************
 * Function           : Sha1ArTransform 
 *
 * Description        :This function perfomrs the SHA1 algorithm
 *
 * Input(s)           :pu4Arstate -pointer for Sha1 transformation of Current
 *                      State
 *                     pu1Arblock- poiner to string for encryption
 *                     
 * Output(s)          : 
 *
 * Returns            : NONE
 ***************************************************************************/
PRIVATE VOID
Sha1ArTransform (UINT4 *pu4Arstate, UINT1 *pu1Arblock)
{
    const UINT1        *pau1Buffer = NULL;
    UINT4               au4ArStateTab[SHA1_TOTAL_WORDS] = { CRY_INIT_VAL };
    UINT4               u4ArStateA = CRY_INIT_VAL;
    UINT4               u4ArStateB = CRY_INIT_VAL;
    UINT4               u4ArStateC = CRY_INIT_VAL;
    UINT4               u4ArStateD = CRY_INIT_VAL;
    UINT4               u4ArStateE = CRY_INIT_VAL;
    UINT4               u4ArStateF = CRY_INIT_VAL;
    UINT4               u4ArStateTemp = CRY_INIT_VAL;

    UINT4               u4ArCount = CRY_INIT_VAL;

    /* Initialising the state values */

    u4ArStateA = pu4Arstate[CRY_ARRAY_INDEX_ZERO];
    u4ArStateB = pu4Arstate[CRY_ARRAY_INDEX_ONE];
    u4ArStateC = pu4Arstate[CRY_ARRAY_INDEX_TWO];
    u4ArStateD = pu4Arstate[CRY_ARRAY_INDEX_THREE];
    u4ArStateE = pu4Arstate[CRY_ARRAY_INDEX_FOUR];

    /* Message Processing */
    for (u4ArCount = 0; u4ArCount < SHA1_WORD_SIZE; u4ArCount++)
    {
        pau1Buffer = pu1Arblock + SHA1_COUNT_SIZE_FOUR * u4ArCount;

        au4ArStateTab[u4ArCount] =
            ((UINT4) (pau1Buffer[CRY_ARRAY_INDEX_ZERO]) <<
             SHA1_THIRD_BYTE_OFFSET) |
            ((UINT4) (pau1Buffer[CRY_ARRAY_INDEX_ONE]) <<
             SHA1_SECOND_BYTE_OFFSET) |
            ((UINT4) (pau1Buffer[CRY_ARRAY_INDEX_TWO]) <<
             SHA1_FIRST_BYTE_OFFSET) | (pau1Buffer[CRY_ARRAY_INDEX_THREE]);
    }

    for (; u4ArCount < SHA1_TOTAL_WORDS; u4ArCount++)
    {
        au4ArStateTab[u4ArCount] =
            SHA1_CIRC_SHIFT_LEFT_OPER (au4ArStateTab, u4ArCount);
    }
    /* Message Processing from zero to twenty */
    for (u4ArCount = 0; u4ArCount < SHA1_WORDS_PART_ONE_SIZE; u4ArCount++)
    {
        u4ArStateF = (u4ArStateB & u4ArStateC) | (~u4ArStateB & u4ArStateD);
        u4ArStateTemp =
            SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateA, SHA1_CIRCULAR_SHIFT_FIVE) +
            u4ArStateF +
            u4ArStateE + SHA1_FIRST_SEQ_WORD + au4ArStateTab[u4ArCount];
        u4ArStateE = u4ArStateD;
        u4ArStateD = u4ArStateC;
        u4ArStateC = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateB,
                                               SHA1_CIRCULAR_SHIFT_THIRTY);
        u4ArStateB = u4ArStateA;
        u4ArStateA = u4ArStateTemp;
    }

    /* Message Processing from twenty to fourty */
    for (; u4ArCount < SHA1_WORDS_PART_TWO_SIZE; u4ArCount++)
    {
        u4ArStateF = u4ArStateB ^ u4ArStateC ^ u4ArStateD;
        u4ArStateTemp = SHA1_CIRCULAR_SHIFT_LEFT
            (u4ArStateA, SHA1_CIRCULAR_SHIFT_FIVE)
            + u4ArStateF + u4ArStateE + SHA1_SECOND_SEQ_WORD +
            au4ArStateTab[u4ArCount];
        u4ArStateE = u4ArStateD;
        u4ArStateD = u4ArStateC;
        u4ArStateC = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateB,
                                               SHA1_CIRCULAR_SHIFT_THIRTY);
        u4ArStateB = u4ArStateA;
        u4ArStateA = u4ArStateTemp;
    }

    /* Message Processing from fourty to sixty */
    for (; u4ArCount < SHA1_WORDS_PART_THREE_SIZE; u4ArCount++)
    {
        u4ArStateF = (u4ArStateB & u4ArStateC) | (u4ArStateB & u4ArStateD)
            | (u4ArStateC & u4ArStateD);
        u4ArStateTemp = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateA,
                                                  SHA1_CIRCULAR_SHIFT_FIVE) +
            u4ArStateF + u4ArStateE + SHA1_THIRD_SEQ_WORD +
            au4ArStateTab[u4ArCount];
        u4ArStateE = u4ArStateD;
        u4ArStateD = u4ArStateC;
        u4ArStateC = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateB,
                                               SHA1_CIRCULAR_SHIFT_THIRTY);
        u4ArStateB = u4ArStateA;
        u4ArStateA = u4ArStateTemp;
    }

    /* Message Processing from sixty to eighty */
    for (; u4ArCount < SHA1_TOTAL_WORDS; u4ArCount++)
    {
        u4ArStateF = u4ArStateB ^ u4ArStateC ^ u4ArStateD;
        u4ArStateTemp = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateA,
                                                  SHA1_CIRCULAR_SHIFT_FIVE) +
            u4ArStateF + u4ArStateE + SHA1_FOURTH_SEQ_WORD +
            au4ArStateTab[u4ArCount];
        u4ArStateE = u4ArStateD;
        u4ArStateD = u4ArStateC;
        u4ArStateC = SHA1_CIRCULAR_SHIFT_LEFT (u4ArStateB,
                                               SHA1_CIRCULAR_SHIFT_THIRTY);
        u4ArStateB = u4ArStateA;
        u4ArStateA = u4ArStateTemp;
    }

    /*Assigning the final calcualted states */
    pu4Arstate[CRY_ARRAY_INDEX_ZERO] = pu4Arstate[CRY_ARRAY_INDEX_ZERO]
        + u4ArStateA;
    pu4Arstate[CRY_ARRAY_INDEX_ONE] = pu4Arstate[CRY_ARRAY_INDEX_ONE] +
        u4ArStateB;
    pu4Arstate[CRY_ARRAY_INDEX_TWO] = pu4Arstate[CRY_ARRAY_INDEX_TWO] +
        u4ArStateC;
    pu4Arstate[CRY_ARRAY_INDEX_THREE] = pu4Arstate[CRY_ARRAY_INDEX_THREE] +
        u4ArStateD;
    pu4Arstate[CRY_ARRAY_INDEX_FOUR] = pu4Arstate[CRY_ARRAY_INDEX_FOUR] +
        u4ArStateE;
}

/******************************************************************************
 * Function           : Sha1ArStart 
 *
 * Description        :This function will initialize the SHA1Message
 *                       context in preparation for computing a new SHA1
 *                        message digest.
 *
 * Input(s)           :pu1ArSha1Context    Pointer to Sha1 Message context
 *                     
 *                     
 * Output(s)          : 
 *
 * Returns            : NONE
 *****************************************************************************/
PUBLIC VOID
Sha1ArStart (unArCryptoHash * pu1ArSha1Context)
{
    /* Initialising the Context for new Message Digest Creatioin */
    pu1ArSha1Context->tArSha1.u8ArNbytes = SHA1_INITIALIZE_0ULL;

    pu1ArSha1Context->tArSha1.au4ArState[CRY_ARRAY_INDEX_ZERO] =
        SHA1_H_WORD_ZERO;
    pu1ArSha1Context->tArSha1.au4ArState[CRY_ARRAY_INDEX_ONE] = SHA1_H_WORD_ONE;
    pu1ArSha1Context->tArSha1.au4ArState[CRY_ARRAY_INDEX_TWO] = SHA1_H_WORD_TWO;
    pu1ArSha1Context->tArSha1.au4ArState[CRY_ARRAY_INDEX_THREE] =
        SHA1_H_WORD_THREE;
    pu1ArSha1Context->tArSha1.au4ArState[CRY_ARRAY_INDEX_FOUR] =
        SHA1_H_WORD_FOURTH;
}

 /****************************************************************************
 * Function           : Sha1ArUpdate 
 *
 * Description        :This function will process the Sha1Algorithm  in 
 *                     64-byte blocks
 * 
 * Input(s)           :pu1ArSha1Context    Pointer to Sha1Message context
 *                     pu1ArPlainText        Pointer Message word to be encrypted
 *                     u4ArMessageLength    Length of message to be encrypted
 * Output(s)          : 
 *
 * Returns            : NONE
 **************************************************************************/
VOID
Sha1ArUpdate (unArCryptoHash * pu1ArSha1Context, const UINT1
              *pu1ArPlainText, UINT4 u4ArMessageLength)
{
    /* Processing  is done in 64-byte blocks */
    UINT1              *pu1ArString = NULL;
    AR_CRYPTO_UINT8     u8ArNum = SHA1_INITIALIZE_0ULL;

    pu1ArString = pu1ArSha1Context->tArSha1.au1ArBlock;

    /* Number of unprocessed bytes in the context */
    u8ArNum = pu1ArSha1Context->tArSha1.u8ArNbytes &
        SHA1_OFFSET_UNPROCESSED_BYTES;

    pu1ArSha1Context->tArSha1.u8ArNbytes =
        pu1ArSha1Context->tArSha1.u8ArNbytes + u4ArMessageLength;

    while (u4ArMessageLength)
    {
        pu1ArString[u8ArNum++] = *pu1ArPlainText++;
        u4ArMessageLength--;

        if (u8ArNum == SHA1_BLOCK_SIZE)
        {
            Sha1ArTransform (pu1ArSha1Context->tArSha1.au4ArState, pu1ArString);
            u8ArNum = CRY_INIT_VAL;
        }
    }
}

 /****************************************************************************
 * Function           : Sha1ArFinish 
 *
 * Description        :This function will process the padding required
 *                      for the computation of message digest.
 *
 * Input(s)           :pu1ArSha1Context    Pointer to Sha1Message context
 *                     *pu1ArMessageDigest    Pointer to the generated message
 *                     digest
 *                 
 * Output(s)          : 
 *
 * Returns            : NONE
 ***************************************************************************/
PUBLIC VOID
Sha1ArFinish (unArCryptoHash * pu1ArSha1Context, UINT1 *pu1ArMessageDigest)
{
    UINT4              *pu4Arstate = NULL;
    AR_CRYPTO_UINT8     u8Arnbits = CRY_INIT_VAL;
    AR_CRYPTO_UINT8     u8Arnum = SHA1_INITIALIZE_0ULL;
    UINT4               u4ArCount = CRY_INIT_VAL;
    UINT4               u4ArTempValueA = CRY_INIT_VAL;
    UINT1               au1ArLength[SHA1_OFFSET_EIGHT] = { CRY_INIT_VAL };

    pu4Arstate = pu1ArSha1Context->tArSha1.au4ArState;
    u8Arnbits = pu1ArSha1Context->tArSha1.u8ArNbytes << SHA1_OFFSET_THREE;
    u8Arnum = pu1ArSha1Context->tArSha1.u8ArNbytes &
        SHA1_OFFSET_UNPROCESSED_BYTES;

    /*Number of unprocessed bytes in the context */

    for (u4ArCount = 0; u4ArCount < SHA1_BYTE_SIZE; u4ArCount++)
    {
        au1ArLength[SHA1_BYTE_LAST_BIT_POSITION - u4ArCount] =
            (UINT1) (u8Arnbits & SHA1_OFFSET_FOR_PADDING);
        u8Arnbits >>= SHA1_BYTE_SIZE;
    }
    /* Padding the message digest */

    Sha1ArUpdate (pu1ArSha1Context, gaArSha1Pad, gaArShaNumPad[u8Arnum]);
    Sha1ArUpdate (pu1ArSha1Context, au1ArLength, SHA1_BYTE_SIZE);

    for (u4ArCount = CRY_INIT_VAL; u4ArCount <
         SHA1_MESSAGE_DIG_ITER_COUNT_MAX; u4ArCount++)
    {
        u4ArTempValueA = pu4Arstate[u4ArCount];

        SHA1_INCREMENT_VALUE (*pu1ArMessageDigest) =
            ((UINT1)
             ((u4ArTempValueA >> SHA1_OFFSET_TWENTYFOUR) &
              SHA1_OFFSET_FOR_PADDING));
        SHA1_INCREMENT_VALUE (*pu1ArMessageDigest) =
            ((UINT1)
             ((u4ArTempValueA >> SHA1_OFFSET_SIXTEEN) &
              SHA1_OFFSET_FOR_PADDING));
        SHA1_INCREMENT_VALUE (*pu1ArMessageDigest) =
            ((UINT1)
             ((u4ArTempValueA >> SHA1_OFFSET_EIGHT) & SHA1_OFFSET_FOR_PADDING));
        SHA1_INCREMENT_VALUE (*pu1ArMessageDigest) =
            ((UINT1) (u4ArTempValueA & SHA1_OFFSET_FOR_PADDING));
    }
}

/************************************************************************/
/*  Function Name   : Sha1ArAlgo                                        */
/*  Description     : The interface function to SHA1                    */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA digest value                  */
/*  Returns         : None                                              */
/************************************************************************/

VOID
Sha1ArAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unArCryptoHash      Tctx;

    Sha1ArStart (&Tctx);
    Sha1ArUpdate (&Tctx, pu1Buf, (UINT4) i4BufLen);
    Sha1ArFinish (&Tctx, pu1Digest);
    return;
}
