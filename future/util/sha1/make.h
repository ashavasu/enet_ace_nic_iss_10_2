######################################################################
# Copyright (C) 2008 Aricent Inc . All Rights Reserved                #
# ------------------------------------------                          #
# $Id: make.h,v 1.3 2015/04/28 12:51:05 siva Exp $                     #
#    DESCRIPTION            : Specifies the options and modules to be #
#                             included for building the SHA1 module    #
#######################################################################
include ../../LR/make.h
include ../../LR/make.rule

SHA1_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES} -UTRACE_WANTED

COMN_INCL_DIR = $(BASE_DIR)/inc
CRYPTO_SHA1_BASE_DIR = $(BASE_DIR)/util/sha1
CRYPTO_SHA1_SRC_DIR = $(CRYPTO_SHA1_BASE_DIR)/src
CRYPTO_SHA1_INC_DIR = $(CRYPTO_SHA1_BASE_DIR)/inc
CRYPTO_SHA1_OBJ_DIR = $(CRYPTO_SHA1_BASE_DIR)/obj
CRYPTO_INCLUDES = -I$(CRYPTO_SHA1_INC_DIR) -I$(COMN_INCL_DIR) -I ${COMMON_INCLUDE_DIRS}

