/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarglb.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the global defines required for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_GLB_H
#define _SHA1_GLB_H
/********************************************************************/
#include "shaarinc.h"

const UINT1 gaArSha1Pad[SHA1_BLOCK_SIZE] = {
        0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

 const UINT4 gaArShaNumPad[] = {
        56, 55, 54, 53, 52, 51, 50, 49, 
        48, 47, 46, 45, 44, 43, 42, 41, 
        40, 39, 38, 37, 36, 35, 34, 33, 
        32, 31, 30, 29, 28, 27, 26, 25, 
        24, 23, 22, 21, 20, 19, 18, 17, 
        16, 15, 14, 13, 12, 11, 10, 9, 
        8,  7,  6,  5,  4,  3,  2,  1, 
        64, 63, 62, 61, 60, 59, 58, 57
};
const tArHashfunction gArSha1Hashfunction = {
        SHA1_WORDS_PART_ONE_SIZE, 
        Sha1ArStart, 
        Sha1ArUpdate, 
        Sha1ArFinish
};

#endif


