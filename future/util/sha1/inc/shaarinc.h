/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarinc.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the header files required for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_INC_H
#define _SHA1_INC_H
/********************************************************************/
#include "cryardef.h"
#include "fsapsys.h"
#include "cryartdf.h"
#include "shaardef.h"
#include "shaarprt.h"
#include "shaarext.h"
#include "shaarmac.h"
#endif

