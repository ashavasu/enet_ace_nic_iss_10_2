/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarprt.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the function declarations for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_PROT_H
#define _SHA1_PROT_H
/********************************************************************/
 
PUBLIC VOID Sha1ArStart PROTO(( unArCryptoHash * ));
PUBLIC VOID Sha1ArUpdate PROTO(( unArCryptoHash *, const UINT1  *,  UINT4 ));
PUBLIC VOID Sha1ArFinish PROTO(( unArCryptoHash *,  UINT1 * ));
VOID Sha1ArAlgo PROTO((UINT1 *, INT4 , UINT1 *));

#endif
