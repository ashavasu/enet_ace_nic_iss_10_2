/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarmac.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the macros required for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_MACS_H
#define _SHA1_MACS_H
/********************************************************************/

#define SHA1_CIRCULAR_SHIFT_LEFT(x,n)           (((x) << (n)) | ((x) >> (32-(n)))) 
#define SHA1_CIRC_SHIFT_LEFT_OPER(arr,index)\
    SHA1_CIRCULAR_SHIFT_LEFT((arr[index-SHA1_OFFSET_THREE]^ \
                    arr[index-SHA1_OFFSET_EIGHT] \
                    ^arr[index-SHA1_OFFSET_FOURTEEN]\
                    ^arr[index-SHA1_OFFSET_SIXTEEN]), SHA1_OFFSET_ONE) 

#define SHA1_INCREMENT_VALUE(x) (x++)
#endif

