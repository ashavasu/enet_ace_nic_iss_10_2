/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaardef.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the defines required for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_DEFS_H
#define _SHA1_DEFS_H
/********************************************************************/

#define SHA1_FIRST_SEQ_WORD               0x5A827999
#define SHA1_SECOND_SEQ_WORD              0x6ED9EBA1 
#define SHA1_THIRD_SEQ_WORD               0x8F1BBCDC
#define SHA1_FOURTH_SEQ_WORD              0xCA62C1D6
#define SHA1_H_WORD_ZERO                  0x67452301
#define SHA1_H_WORD_ONE                   0xEFCDAB89
#define SHA1_H_WORD_TWO                   0x98BADCFE
#define SHA1_H_WORD_THREE                 0x10325476
#define SHA1_H_WORD_FOURTH                0xC3D2E1F0
#define SHA1_BLOCK_SIZE                   64
#define SHA1_SUCCESS                      1
#define SHA1_FAILURE                      0
#define SHA1_COUNT_SIZE_FOUR              4
#define SHA1_WORD_SIZE                    16
#define SHA1_THIRD_BYTE_OFFSET            24
#define SHA1_SECOND_BYTE_OFFSET           16
#define SHA1_FIRST_BYTE_OFFSET            8
#define SHA1_TOTAL_WORDS                  80
#define SHA1_WORDS_PART_ONE_SIZE          20
#define SHA1_WORDS_PART_TWO_SIZE          40
#define SHA1_WORDS_PART_THREE_SIZE        60
#define SHA1_OFFSET_ONE                   1
#define SHA1_OFFSET_THREE                 3
#define SHA1_OFFSET_EIGHT                 8
#define SHA1_OFFSET_FOURTEEN              14
#define SHA1_OFFSET_SIXTEEN               16
#define SHA1_OFFSET_TWENTYFOUR            24
#define SHA1_CIRCULAR_SHIFT_FIVE          5
#define SHA1_CIRCULAR_SHIFT_THIRTY        30
#define SHA1_OFFSET_UNPROCESSED_BYTES     0x3F
#define SHA1_BYTE_SIZE                    8
#define SHA1_BYTE_LAST_BIT_POSITION       7
#define SHA1_OFFSET_FOR_PADDING           0xFF
#define SHA1_MESSAGE_DIG_ITER_COUNT_MAX   5
#define SHA1_INITIALIZE_0ULL              0ULL
#endif

