/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: shaarext.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the externs for SHA1
 * module
 *********************************************************************/
#ifndef _SHA1_EXT_H
#define _SHA1_EXT_H
/********************************************************************/
extern const UINT1 gaArSha1Pad[SHA1_BLOCK_SIZE]; 
extern const UINT4 gaArShaNumPad[SHA1_BLOCK_SIZE]; 
extern const tArHashfunction gArSha1Hashfunction; 
#endif
