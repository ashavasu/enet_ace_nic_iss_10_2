/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * 
 * $Id: arHmac_api.c,v 1.1 2015/04/28 12:53:47 siva Exp $
 *
 * Description: This file contains the source code for hmac-sha2 module
 *********************************************************************/
#include "sha2.h"
#include "arHmac_defs.h"
#include "arHmac_api.h"
#include "fsutltrc.h"
#include "trace.h"

extern UINT4        gu4UtlTrcFlag;

/****************************************************************************
 * API Name: arHmac_Sha1 
 * Description: This API is invoked to do  HMAC Sha1 Operation 
 * Return : Nothing.
 *****************************************************************************/
VOID
arHmac_Sha1 (UINT1 *pu1Key, UINT4 u4KeyLen,
             UINT1 *pu1PktDataPtr, INT4 i4PktLength, UINT1 *pu1OutputDigest)
{
    tArHmac             HmacHashInfoContext;

    arHmac_start (&gArSha1Hashfunction, &HmacHashInfoContext, pu1Key, u4KeyLen);
    arHmac_update (&HmacHashInfoContext, pu1PktDataPtr, i4PktLength);
    arHmac_finish (&HmacHashInfoContext, pu1OutputDigest);
}

/****************************************************************************
 * API Name: arHmac_MD5 
 * Description: This API is invoked to do  HMAC MD5 Operation 
 * Return : Nothing.
 *****************************************************************************/
VOID
arHmac_MD5 (UINT1 *pu1PktDataPtr, INT4 i4PktLength,
            UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1OutputDigest)
{
    tArHmac             HmacHashInfoContext;

    arHmac_start (&gArMd5Hashfunction, &HmacHashInfoContext, pu1Key, u4KeyLen);
    arHmac_update (&HmacHashInfoContext, pu1PktDataPtr, i4PktLength);
    arHmac_finish (&HmacHashInfoContext, pu1OutputDigest);
}

/*******************************************************************************
 * Function           : arHmacSha2
 *
 * Description        : This funtion is invoked to do the HMAC Reset, Input and 
 *                      Result Operation
 *
 * Input(s)           : whichSha - which SHA algorithm to query
 *                      pu1Text - text to generate message digest. 
 *                      i4Textlen - text length for generating digest.
 *                      pu1Key - key for generating digest.
 *                      i4Keylen - key length for generating digest.
 *                      pu1Digest - Where the digest is returned
 *
 * Output(s)          : pu1Digest -it gives the  output as message digest.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/

PUBLIC INT4
arHmacSha2 (eShaVersion whichSha, CONST UINT1 *pu1Text, INT4 i4Textlen,
            CONST UINT1 *pu1Key, INT4 i4Keylen, UINT1 *pu1Digest)
{
    tHmacContext        HmacCtx;

    if ((arHmacReset (&HmacCtx, whichSha, pu1Key, i4Keylen)) == OSIX_FAILURE)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "Failed to init the Hmac context\n");
        return OSIX_FAILURE;
    }

    if ((arHmacInput (&HmacCtx, pu1Text, i4Textlen)) == OSIX_FAILURE)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "Hmac context input Failed\n");
        return OSIX_FAILURE;
    }

    if ((arHmacResult (&HmacCtx, pu1Digest)) == OSIX_FAILURE)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "Hmac context result Failed\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file arHmac_api.c                    */
/*-----------------------------------------------------------------------*/
