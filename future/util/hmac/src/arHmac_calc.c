/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: arHmac_calc.c,v 1.1 2015/04/28 12:53:47 siva Exp $
 *
 * Description: This file contains the API code for HMAC module
 *********************************************************************/

#include "sha2.h"
#include "arHmac_defs.h"
#include "arHmac_api.h"

/****************************************************************************
    HMAC Calculation

    H (K XOR opad, H (K XOR ipad, text))
    where,
        H : Hashing Algorithm (MD5 , SHA1 etc)
        K : Key
        opad : Outer pad , value 0x5c
        ipad : Inner pad , value 0x36
        text : the text on which MAC is to be calculated
*****************************************************************************/

/****************************************************************************
 * API Name: arHmac_start (K XOR ipad) 
 * 
 * Description: This API is invoked to start HMAC calculations.
 *              The strcuture pHmacHashInfo is populated, Key is validated 
 *              and the 64 byte hashed key XoRed with inner pad is updated 
 *              in the hash context.
 *
 * Return : Nothing.
 ******************************************************************************/
VOID
arHmac_start (IN tArHashfunction * pHashFunc,
              OUT tArHmac * pHmacHashInfo, IN UINT1 *pKey, IN UINT4 keyLen)
{
    UINT4               HashLen = 0, count = 0;
    UINT1               xorResult[ARHMAC_HASH_BLOCK_SIZE] = { 0, };
    unArCryptoHash      hashCtx;

#ifdef CRYPTO_DEBUG_LOG
    if ((ARHMAC_NULL == pHmacHashInfo) || (ARHMAC_NULL == pHashFunc))
    {
        /* without these information, HMAC calculation can not start
         * therefore return.
         */
        CRYPTO_DEBUG_PRINT
            ("\n arHmac_start : Hash Info or Hash Functions are NULL\n");
        return;
    }
#endif

    HashLen = ARHMAC_SIZEOF (pHmacHashInfo->au1ArKeyBlock);

    /* Initialize the Hash Info context */
    pHmacHashInfo->pArHashFunction = pHashFunc;
    pHmacHashInfo->u4ArDataLength = 0;
    ARHMAC_MEMSET (pHmacHashInfo->au1ArKeyBlock, 0, HashLen);

    /* If key length is more than 64 then generate a key of 64 length 
     * by applying hashing algorithm) */
    if (keyLen > HashLen)
    {
        pHmacHashInfo->pArHashFunction->ArCryptoHashStart (&hashCtx);
        pHmacHashInfo->pArHashFunction->ArCryptoHashUpdate
            (&hashCtx, pKey, keyLen);
        pHmacHashInfo->pArHashFunction->ArCryptoHashFinish
            (&hashCtx, pHmacHashInfo->au1ArKeyBlock);
    }
    else
    {
        ARHMAC_MEMCPY (pHmacHashInfo->au1ArKeyBlock, pKey, keyLen);
    }

    /* XOR each bit of Key with inner pad */
    for (count = 0; count < HashLen; count++)
    {
        xorResult[count] =
            pHmacHashInfo->au1ArKeyBlock[count] ^ ARHMAC_INNER_PAD;
    }

    /* Start HMAC Calculation , first initialize the context and then 
     * update the intermediary xorResult into the context */
    pHmacHashInfo->pArHashFunction->ArCryptoHashStart (&pHmacHashInfo->
                                                       ArContext);
    pHmacHashInfo->pArHashFunction->ArCryptoHashUpdate (&pHmacHashInfo->
                                                        ArContext, xorResult,
                                                        HashLen);

    return;

}

/****************************************************************************
 * API Name: arHmac_update (K XOR ipad, text) 
 * 
 * Description: This API is invoked to update the input text to hash context.
 *
 * Return : Nothing.
 ******************************************************************************/
VOID
arHmac_update (INOUT tArHmac * pHmacHashInfo,
               IN UINT1 *pBuffer, IN UINT4 buffLen)
{
#ifdef CRYPTO_DEBUG_LOG
    if ((ARHMAC_NULL == pHmacHashInfo)
        || (ARHMAC_NULL == pHmacHashInfo->pArHashFunction))
    {
        /* without these information, HMAC calculation can not proceed 
         * therefore return.
         */
        CRYPTO_DEBUG_PRINT
            ("\n arHmac_update : Hash Info or Hash Functions are NULL\n");
        return;
    }
#endif
    pHmacHashInfo->pArHashFunction->ArCryptoHashUpdate
        (&pHmacHashInfo->ArContext, pBuffer, buffLen);

    return;
}

/****************************************************************************
 * API Name: arHmac_finish (H (K XOR opad, H (K XOR ipad, text))) 
 * 
 * Description: This API is invoked to finalise HMAC calculations.
 *              The 64 byte Key is XoRed with outer pad and then hashed output 
 *              is generated.
 *
 * Return : Nothing.
 ******************************************************************************/

VOID
arHmac_finish (INOUT tArHmac * pHmacHashInfo, OUT UINT1 *pOutputBuff)
{
    UINT4               HashLen = 0, count = 0;
    UINT1               xorResult[ARHMAC_HASH_BLOCK_SIZE] = { 0, },
        innerOpt[ARHMAC_HASH_BLOCK_SIZE] =
    {
    0,};
    unArCryptoHash      hashCtx;

#ifdef CRYPTO_DEBUG_LOG
    if ((ARHMAC_NULL == pHmacHashInfo)
        || (ARHMAC_NULL == pHmacHashInfo->pArHashFunction))
    {
        /* without these information, HMAC calculation can not proceed 
         * therefore return.
         */
        CRYPTO_DEBUG_PRINT
            ("\n arHmac_finish : Hash Info or Hash Functions are NULL\n");
        return;
    }
    else if (ARHMAC_NULL == pOutputBuff)
    {
        CRYPTO_DEBUG_PRINT ("\n arHmac_finish : Output Buffer is NULL\n");
        return;
    }
#endif

    HashLen = ARHMAC_SIZEOF (pHmacHashInfo->au1ArKeyBlock);

    /* XOR each bit of Key with outer pad */
    for (count = 0; count < HashLen; count++)
    {
        xorResult[count] =
            pHmacHashInfo->au1ArKeyBlock[count] ^ ARHMAC_OUTER_PAD;
    }

    /* First generate the final hash output for inner block calculation */
    pHmacHashInfo->pArHashFunction->ArCryptoHashFinish
        (&pHmacHashInfo->ArContext, innerOpt);

    /* Start HMAC Calculation for Final Result */
    pHmacHashInfo->pArHashFunction->ArCryptoHashStart (&hashCtx);

    pHmacHashInfo->pArHashFunction->ArCryptoHashUpdate (&hashCtx, xorResult,
                                                        HashLen);
    pHmacHashInfo->pArHashFunction->ArCryptoHashUpdate (&hashCtx, innerOpt,
                                                        pHmacHashInfo->
                                                        pArHashFunction->
                                                        u4ArHashsize);

    pHmacHashInfo->pArHashFunction->ArCryptoHashFinish (&hashCtx, pOutputBuff);

    return;
}

/*******************************************************************************
 * Function           : arhmacReset
 *
 * Description        : This function will initialize the hmacContext in
 *                      preparation for computing a new HMAC message digest.
 *
 * Input(s)           : pHmacCtx -  The pHmacCtx use to reset
 *                      whichSha - One of SHA1, SHA224, SHA256, SHA384, SHA512
 *                      pu1key - The secret shared key.
 *                      i4keylen - The length of the secret shared key.
 * Output(s)          : pHmacCtx - that contains initialized context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
arHmacReset (tHmacContext * pHmacCtx, eShaVersion whichSha,
             CONST UINT1 *pu1key, INT4 i4keylen)
{
    INT4                i4Temp = 0, i4Blocksize = 0, i4Hashsize = 0;
    tUshaContext        UshaCtx;
    /* inner padding - key XORd with ipad */
    UINT1               au1Kipad[USHA_MAX_MESSAGE_BLOCK_SIZE];

    /* temporary buffer when keylen > i4Blocksize */
    UINT1               au1Tempkey[USHA_MAX_HASH_SIZE];

    MEMSET (au1Kipad, 0, USHA_MAX_MESSAGE_BLOCK_SIZE);
    MEMSET (au1Tempkey, 0, USHA_MAX_HASH_SIZE);

    if (pHmacCtx == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\narHmacReset: Input parameter pHmacCtx is NULL\n");
#endif
        return OSIX_FAILURE;
    }

    i4Blocksize = pHmacCtx->i4BlockSize = UshaBlockSize (whichSha);
    i4Hashsize = pHmacCtx->i4HashSize = UshaHashSize (whichSha);

    pHmacCtx->i4WhichSha = whichSha;

    /*
     * If key is longer than the hash i4Blocksize,
     * reset it to key = HASH(key).
     */
    if (i4keylen > i4Blocksize)
    {
        /* i4Err = UshaReset (&UshaCtx, whichSha); */
        if ((UshaReset (&UshaCtx, whichSha)) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        /* i4Err = UshaInput (&UshaCtx, pu1key, i4keylen); */
        if ((UshaInput (&UshaCtx, pu1key, (UINT4)i4keylen)) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        /* i4Err = UshaResult (&UshaCtx, au1Tempkey); */
        if ((UshaResult (&UshaCtx, au1Tempkey)) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        pu1key = au1Tempkey;
        i4keylen = i4Hashsize;

    }

    /* store key into the pads, XOR'd with ipad and opad values */
    for (i4Temp = 0; i4Temp < i4keylen; i4Temp++)
    {
        au1Kipad[i4Temp] = pu1key[i4Temp] ^ 0x36;
        pHmacCtx->au1Kopad[i4Temp] = pu1key[i4Temp] ^ 0x5c;
    }
    /* remaining pad bytes are '\0' XOR'd with ipad and opad values */
    for (; i4Temp < i4Blocksize; i4Temp++)
    {
        au1Kipad[i4Temp] = 0x36;
        pHmacCtx->au1Kopad[i4Temp] = 0x5c;
    }

    /* perform inner hash */
    /* init context for 1st pass */

    if ((UshaReset (&pHmacCtx->UshaContext, whichSha)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if ((UshaInput (&pHmacCtx->UshaContext, au1Kipad, (UINT4)i4Blocksize)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           : arHmacInput
 *
 * Description        : This function accepts an array of octets as the next
 *                      portion of the message.
 *
 * Input(s)           : pHmacCtx -  The pContext to update
 *                      pu1Text -  An array of characters representing the next 
                                   portion of the message.
 *                      i4Textlen - The length of the message in message array.
 * Output(s)          : pHmacCtx - that contains updated context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
arHmacInput (tHmacContext * pHmacCtx, CONST UINT1 *pu1Text, INT4 i4Textlen)
{
    if (pHmacCtx == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n arHmacInput:  Input parameter pHmacCtx is NULL\n");
#endif
        return OSIX_FAILURE;
    }
    /* then text of datagram */
    return UshaInput (&pHmacCtx->UshaContext, pu1Text, (UINT4)i4Textlen);
}

/*******************************************************************************
 * Function           : arHmacFinalBits
 *
 * Description        : This function will add in any final bits of the message.
 *
 * Input(s)           : pHmacCtx - The pContext to update
 *                      u1Bits - The final bits of the message, in the upper 
 *                                 portion of the byte. (Use 0b###00000 instead 
 *                                 0b00000### to input the three bits ###.) 
 *                      u4Bitcount - The length of the message in message array.
 * Output(s)          : pHmacCtx - that contains updated context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
arHmacFinalBits (tHmacContext * pHmacCtx, CONST UINT1 u1Bits, UINT4 u4Bitcount)
{
    if (pHmacCtx == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n arHmacFinalBits:  Input parameter pHmacCtx is NULL\n");
#endif
        return OSIX_FAILURE;
    }
    /* then final bits of datagram */
    return UshaFinalBits (&pHmacCtx->UshaContext, u1Bits, u4Bitcount);

}

/*******************************************************************************
 * Function           : arHmacResult
 *
 * Description        : This function will return the N-byte message digest into 
 *                      the pu1Digest array provided by the caller.
 *                      NOTE: The first octet of hash is stored in the 0th element,
 *                      the last octet of hash in the Nth element.
 *
 * Input(s)           : pHmacCtx - to calculate the hmac hash.
 *
 * Output(s)          : pu1Digest - Where the digest is returned. 
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
arHmacResult (tHmacContext * pHmacCtx, UINT1 *pu1Digest)
{
    if (pHmacCtx == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n arHmacResult:  Input parameter pHmacCtx is NULL\n");
#endif
        return OSIX_FAILURE;
    }

    /* finish up 1st pass */
    /* (Use digest here as a temporary buffer.) */

    if ((UshaResult (&pHmacCtx->UshaContext, pu1Digest)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* perform outer SHA */
    /* init context for 2nd pass */
    if ((UshaReset (&pHmacCtx->UshaContext, pHmacCtx->i4WhichSha)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* start with outer pad */
    if ((UshaInput (&pHmacCtx->UshaContext, pHmacCtx->au1Kopad,
                    (UINT4)pHmacCtx->i4BlockSize)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* then results of 1st hash */
    if ((UshaInput (&pHmacCtx->UshaContext, pu1Digest, (UINT4)pHmacCtx->i4HashSize))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* finish up 2nd pass */
    if ((UshaResult (&pHmacCtx->UshaContext, pu1Digest)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file arHmac_calc.c                    */
/*-----------------------------------------------------------------------*/
