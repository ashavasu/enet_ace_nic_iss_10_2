/******************************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved 
*
* $Id: arHmac_defs.h,v 1.1 2015/04/28 12:53:47 siva Exp $
*
* FILE NAME    : arHmac_defs.h 
*                                    
* DESCRIPTION  : Contains the literal and porting definitions 
*                                    
* Revision History :                            
*
*
*  DATE            NAME                 REFERENCE       REASON        
* ------         ------------           ---------     ----------
* 19-Dec-2008    Alok Tiwari            None          Initial
* 19-Dec-2008    Anuradha Gupta         LLD           Definitions
*                                    
*******************************************************************************/

#ifndef __ARHMAC_DEFS_H__
#define __ARHMAC_DEFS_H__

#include "fsapsys.h"

/* porting definitions */
#define ARHMAC_MEMSET                   memset
#define ARHMAC_MEMCPY                   memcpy
#define ARHMAC_SIZEOF                   sizeof
#define ARHMAC_NULL             NULL

#ifdef ALGO_DEBUG_PRINT
#define ARHMAC_PRINT            printf
#else
#define ARHMAC_PRINT
#endif

/* The Hmac inner pad value */    
#define ARHMAC_INNER_PAD                0x36

/* The Hmac outer pad value */    
#define ARHMAC_OUTER_PAD                0x5c

/* The Hmac hash block size */    
#define ARHMAC_HASH_BLOCK_SIZE  64

/* Prevent Name Mangling By C++ Compilers */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

typedef struct _HmacContext
{
    INT4 i4WhichSha;               /* which SHA is being used */
    INT4 i4HashSize;               /* hash size of SHA being used */
    INT4 i4BlockSize;              /* block size of SHA being used */
    tUshaContext UshaContext;     /* SHA context */
    UINT1 au1Kopad[USHA_MAX_MESSAGE_BLOCK_SIZE];
                        /* outer padding - key XORd with opad */
} tHmacContext;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ARHMAC_DEFS_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file  arHmac_defs.h                   */
/*-----------------------------------------------------------------------*/
