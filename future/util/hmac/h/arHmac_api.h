/******************************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved        
*
* $Id: arHmac_api.h,v 1.1 2015/04/28 12:53:47 siva Exp $ 
*
* FILE NAME    : arHmac_api.h                
*                                    
* DESCRIPTION  : Contains the definitions for the interfaces. 
*                                    
* Revision History :                            
*
 *
*  DATE            NAME                 REFERENCE       REASON        
* ------         ------------           ---------     ----------
* 19-Dec-2008    Alok Tiwari            None          Initial
* 19-Dec-2008    Anuradha Gupta         LLD           API Declarations
*                                    
*******************************************************************************/

#ifndef __ARHMAC_API_H__
#define __ARHMAC_API_H__


#include "osxstd.h"
#include "cryartdf.h"
#include "cryardef.h"
#include "shaarinc.h"
#include "arMD5_inc.h"
#include "sha2.h"
#include "arHmac_defs.h"

/* Prevent Name Mangling By C++ Compilers */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/****************************************************************************
    HMAC Calculation

    H (K XOR opad, H (K XOR ipad, text))
    where,
        H : Hashing Algorithm (MD5 , SHA1 etc)
        K : Key
        opad : Outer pad , value 0x5c
        ipad : Inner pad , value 0x36
        text : the text on which MAC is to be calculated
*****************************************************************************/

/****************************************************************************
 * API Name: arHmac_start 
 * 
 * Description: This API is invoked to start HMAC calculations.
 *              The strcuture pHmacHashInfo is populated, Key is validated 
 *              and the 64 byte hashed key XoRed with inner pad is updated 
 *              in the hash context.
 *
 * Return : Nothing.
 ******************************************************************************/
VOID arHmac_start( 
        IN  tArHashfunction *   pHashFunc, 
        OUT tArHmac *                   pHmacHashInfo, 
        IN  UINT1 *                     pKey, 
        IN  UINT4                       keyLen);

/****************************************************************************
 * API Name: arHmac_update 
 * 
 * Description: This API is invoked to update the input text to hash context.
 *
 * Return : Nothing.
 ******************************************************************************/
VOID arHmac_update(  
        INOUT tArHmac * pHmacHashInfo, 
        IN    UINT1 *   pBuffer,
        IN    UINT4     buffLen);

/****************************************************************************
 * API Name: arHmac_finish 
 * 
 * Description: This API is invoked to finalise HMAC calculations.
 *              The 64 byte Key is XoRed with outer pad and then hashed output 
 *              is generated.
 *
 * Return : Nothing.
 ******************************************************************************/
VOID arHmac_finish(  
        INOUT tArHmac * pHmacHashInfo,
        OUT   UINT1  *  pOutputBuff); 


/****************************************************************************
 * API Name: arHmac_Sha1 
 * 
 * Description: This API is invoked to finalise HMAC calculations.
 *              using Sha1 
 *              
 *
 * Return : Nothing.
 ******************************************************************************/
VOID arHmac_Sha1(  
       UINT1 *pu1Key ,
       UINT4 u4KeyLen,
       UINT1 *pu1PktDataPtr,
       INT4  i4PktLength,
       UINT1 *pu1OutputDigest)  ;

/****************************************************************************
 * API Name: arHmac_MD5 
 * 
 * Description: This API is invoked to finalise HMAC calculations.
 *              using MD5 
 *              
 *
 * Return : Nothing.
 ******************************************************************************/
VOID arHmac_MD5(  
       UINT1 *pu1PktDataPtr,
       INT4  i4PktLength,
       UINT1 *pu1Key ,
       UINT4 u4KeyLen,
       UINT1 *pu1Digest)  ;



/****************************************************************************
 * API Name: arHmacSha2
 *
 * Description: This API is invoked to Calculate the given digest for the 
                given pu1Text and pu1key, and return the resulting hash. 
                Octets in Message_Digest beyond finalise HMAC calculations.
 *              
 *
 *
 * Return : OSIX_SUCCESS/OSIX_FAILURE. 
 ******************************************************************************/
PUBLIC INT4 arHmacSha2 (eShaVersion whichSha, /* which SHA algorithm to use */
    CONST UINT1 *pu1Text,     /* pointer to data stream */
    INT4 i4Textlen,                  /* length of data stream */
    CONST UINT1 *pu1Key,      /* pointer to authentication key */
    INT4 i4keylen,                   /* length of authentication key */
    UINT1 *pu1Digest); /* caller digest to fill in */


/****************************************************************************
 * API Name: arHmacReset
 *
 * Description: This API is invoked to reset the tHmacContext HMAC calculations.
 *
 * Return : OSIX_SUCCESS/OSIX_FAILURE.
 ******************************************************************************/
PUBLIC INT4 arHmacReset(tHmacContext *pHmacCtx, eShaVersion whichSha,
                         CONST UINT1 *pu1key, INT4 i4keylen);


/****************************************************************************
 * API Name: arHmacInput
 *
 * Description: This API is invoked to Incorporate i4Textlen octets into the 
 *              hash.
 *
 * Return : OSIX_SUCCESS/OSIX_FAILURE.
 ******************************************************************************/
PUBLIC INT4 arHmacInput(tHmacContext *pctx, CONST UINT1 *pu1text,
                     INT4 i4Textlen);


/****************************************************************************
 * API Name: arHmacFinalBits 
 *
 * Description: This API is invoked to  Incorporate bitcount bits into the 
 *              hash.
 *
 * Return : OSIX_SUCCESS/OSIX_FAILURE.
 ******************************************************************************/
PUBLIC INT4 arHmacFinalBits (tHmacContext * pHmacCtx, CONST UINT1 u1Bits,
                   UINT4 u4Bitcount);


/****************************************************************************
 * API Name: arHmacResult
 *
 * Description: This API is invoked to do the final calculations on the hash 
 *              and copy the value into Message_Digest.  Octets in 
 *              Message_Digest beyond.
 *
 * Return :  OSIX_SUCCESS/OSIX_FAILURE.
 ******************************************************************************/
PUBLIC INT4 arHmacResult(tHmacContext *ctx, UINT1 *pu1digest);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __ARHMAC_API_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file  arHmac_api.h                   */
/*-----------------------------------------------------------------------*/
