/****************************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *  $Id: utilvlan.c,v 1.1 2015/04/28 12:51:02 siva Exp $
 *  
 *  Description:
 *  
 ***************************************************************************/

#include "lr.h"
#include "fsutil.h"
#include "l2iwf.h"
#include "fsutltrc.h"

extern UINT4        gu4UtlTrcFlag;

tMemPoolId          gVlanListSizePoolId;    /* Global Pool Id for Vlan List */
UINT4               gu4VlanListSizeUsedBlocks;    /* Number of Vlan List size
                                                   blocks allocated so far */
UINT4               gu4VlanListSizeTotalBlocks;    /* Number of blocks allocated 
                                                   in Vlan List Size mempool */
tMemPoolId          gVlanIdSizePoolId;    /* Global Pool Id for Vlan Id */
UINT4               gu4VlanIdSizeUsedBlocks;    /* Number of Vlan Id size
                                                   blocks allocated so far */
UINT4               gu4VlanIdSizeTotalBlocks;    /* Number of blocks allocated 
                                                   in Vlan Id Size mempool */
#define UTIL_VLAN_LIST_BLK_SIZE    sizeof(tVlanList)
#define UTIL_VLAN_ID_BLK_SIZE      (4094 * sizeof (UINT2))

/****************************************************************************
* Function     : UtilVlanInitVlanListSize   
*                                            
* Description  : To intialize the mempool required for VlanListSize (512).
*
* Input        : None
*
* Output       : None
*
* Returns      : None   
*
***************************************************************************/
VOID
UtilVlanInitVlanListSize (UINT4 u4Count)
{
    gu4VlanListSizeTotalBlocks = u4Count;
    gu4VlanListSizeUsedBlocks = 0;
    return;
}

/****************************************************************************
* Function     : UtilVlanAllocVlanListSize 
*                                            
* Description  : To allocate memory for a VlanListSize from the common mempool.
*
* Input        : u4Size - Size of the required VlanList.
*
* Output       : None
*
* Returns      : Pointer to the allocated Vlan List.
*
***************************************************************************/
UINT1              *
UtilVlanAllocVlanListSize (UINT4 u4Size)
{
    UINT1              *pu1VlanListSize = NULL;

    UNUSED_PARAM (u4Size);

    if (MemAllocateMemBlock (gVlanListSizePoolId, &pu1VlanListSize) ==
        MEM_FAILURE)
    {
        MOD_TRC_ARG2 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "Allocation failed = %s; Already allocated count = %d\n",
                      __FUNCTION__, gu4VlanListSizeUsedBlocks);
        return NULL;
    }
    MEMSET (pu1VlanListSize, 0, sizeof (tVlanListExt));
    /* Increment the number of VlanListSize blocks allocated. */
    gu4VlanListSizeUsedBlocks++;
    return pu1VlanListSize;
}

/****************************************************************************
* Function     : UtilVlanReleaseVlanListSize 
*                                            
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1VlanListSize - Pointer to the VlanList
*
* Output       : None
*               
* Returns      : None
*
***************************************************************************/
VOID
UtilVlanReleaseVlanListSize (UINT1 *pu1VlanListSize)
{
    UINT4               u4RetVal = MEM_FAILURE;

    if (pu1VlanListSize != NULL)
    {
        u4RetVal = MemReleaseMemBlock (gVlanListSizePoolId, (pu1VlanListSize));

        if (u4RetVal == MEM_SUCCESS)
        {
            /* Decrement the number of VlanList blocks allocated. */
            gu4VlanListSizeUsedBlocks--;
            pu1VlanListSize = NULL;
        }
    }
    return;
}

/****************************************************************************
* Function     : UtilVlanInitVlanIdSize   
*                                            
* Description  : To intialize the mempool required for VlanIdSize (4094).
*
* Input        : None
*
* Output       : None
*
* Returns      : None   
*
***************************************************************************/
VOID
UtilVlanInitVlanIdSize (UINT4 u4Count)
{
    gu4VlanIdSizeTotalBlocks = u4Count;
    gu4VlanIdSizeUsedBlocks = 0;
    return;
}

/****************************************************************************
* Function     : UtilVlanAllocVlanIdSize 
*                                            
* Description  : To allocate memory for a VlanIdSize from the common mempool.
*
* Input        : u4Size - Size of the required VlanId.
*
* Output       : None
*
* Returns      : Pointer to the allocated Vlan ID.
*
***************************************************************************/
UINT1              *
UtilVlanAllocVlanIdSize (UINT4 u4Size)
{
    UINT1              *pu1VlanIdSize = NULL;

    UNUSED_PARAM (u4Size);

    if (MemAllocateMemBlock (gVlanIdSizePoolId, &pu1VlanIdSize) == MEM_FAILURE)
    {

        MOD_TRC_ARG2 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "Allocation failed = %s; Already allocated count = %d\n",
                      __FUNCTION__, gu4VlanIdSizeUsedBlocks);
        return NULL;
    }
    MEMSET (pu1VlanIdSize, 0, sizeof (UINT2[VLAN_DEV_MAX_VLAN_ID_EXT]));
    /* Increment the number of VLANID blocks allocated. */
    gu4VlanIdSizeUsedBlocks++;
    return pu1VlanIdSize;
}

/****************************************************************************
* Function     : UtilVlanReleaseVlanIdSize 
*                                            
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1VlanIdSize - Pointer to the VlanId.
*
* Output       : None
*               
* Returns      : None
*
***************************************************************************/
VOID
UtilVlanReleaseVlanIdSize (UINT1 *pu1VlanIdSize)
{
    UINT4               u4RetVal = MEM_FAILURE;

    if (pu1VlanIdSize != NULL)
    {
        u4RetVal = MemReleaseMemBlock (gVlanIdSizePoolId, (pu1VlanIdSize));

        if (u4RetVal == MEM_SUCCESS)
        {
            /* Decrement the number of VLANID blocks allocated. */
            gu4VlanIdSizeUsedBlocks--;
            pu1VlanIdSize = NULL;
        }
    }
    return;
}
