/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: utilalgo.c,v 1.2 2017/09/12 13:27:06 siva Exp $
*
* Description: This file contains the API code for crypto modules.
*********************************************************************/
#ifndef _UTILALGO_C_
#define _UTILALGO_C_

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "utilalgo.h"
#include "aesarprt.h"
#include "desarprt.h"
#include "arHmac_api.h"
#include "arMD5_api.h"
#include "hsec.h"
#include "fsutltrc.h"
#include "msr.h"

extern UINT4        gu4UtlTrcFlag;

CHR1               *pUtilAlgoName[] = {
    "aes-cbc",
    "aes",
    "aes-cbc-mac96",
    "aes-cbc-mac128",
    "aes-ctr",
    "aes-cfb",
    "des-cbc",
    "triple-des-cbc",
    "hmac-sha1",
    "hmac-md5",
    "hmac-sha2",
    "md5",
    "sha1",
    "sha224",
    "sha256",
    "sha384",
    "sha512",
};

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilEncrypt                                      */
/*                                                                          */
/*    Description        : This function calls the appropriate algorithm to */
/*                         be encrypted based on the argument u1Algorithm.  */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm which is to be encyrpted.*/
/*                         punUtilAlgo - Union which contains datastructures*/
/*                                       of all Algorithms.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
UtilEncrypt (UINT1 u1Algorithm, unUtilAlgo * punUtilAlgo)
{
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    UINT4               u4Len = 0;

    UNUSED_PARAM (u4Len);
    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);

    if (UtilGetAlgoSupportInHw (u1Algorithm) == OSIX_SUCCESS)
    {
#ifdef HSEC_WANTED
        if (NpHwUtilFunction (u1Algorithm, punUtilAlgo) != FNP_SUCCESS)
        {
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Encrypt Failed for algorithm %s\n",
                          pUtilAlgoName[u1Algorithm]);
            return OSIX_FAILURE;
        }
        else
        {
            return OSIX_SUCCESS;
        }
#endif
    }
#ifdef EXT_CRYPTO_WANTED
    /* The CRYPTO Encryption is performed through external libraries call. */
    if (ExtWrUtilEncrypt (u1Algorithm, punUtilAlgo) != OSIX_SUCCESS)
    {
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            u4Len = STRLEN ("EXT Encrypt Algorithm FAILED");
            STRNCPY (au1AuditLogMsg, "EXT Encrypt Algorithm FAILED", u4Len);
            au1AuditLogMsg[u4Len] = '\0';
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
        }
        /* The external function may return OSIX_FAILURE in case of failing
         * to Encrypt in such as case OSIX_FAILURE is returned.*/
        MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "EXT Encrypt Failed for algorithm %s\n",
                      pUtilAlgoName[u1Algorithm]);
        u1RetVal = OSIX_FAILURE;
        return u1RetVal;
    }
    else
    {
        /* Encryption is successful in external Library. */
        return u1RetVal;
    }
#else
    switch (u1Algorithm)
    {
        case ISS_UTIL_ALGO_AES_CBC:

            if (AesArCbcEncrypt (punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                                 punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                                 (UINT2) punUtilAlgo->UtilAesAlgo.u4AesInKeyLen,
                                 punUtilAlgo->UtilAesAlgo.pu1AesInScheduleKey,
                                 punUtilAlgo->UtilAesAlgo.pu1AesInitVector)
                != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_AES:

            AesArEncrypt (punUtilAlgo->UtilAesAlgo.punAesArCryptoKey,
                          punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                          punUtilAlgo->UtilAesAlgo.pu1AesOutBuf);
            break;

        case ISS_UTIL_ALGO_AES_CTR_MODE:

            if (AesArCtrMode (punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                              punUtilAlgo->UtilAesAlgo.pu1AesOutBuf,
                              punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                              punUtilAlgo->UtilAesAlgo.punAesArCryptoKey,
                              punUtilAlgo->UtilAesAlgo.pu1AesInitVector,
                              punUtilAlgo->UtilAesAlgo.pu1AesEncryptBuf,
                              punUtilAlgo->UtilAesAlgo.pu4AesNum) !=
                AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_AES_CFB128:

            if (AesArCfb128 (punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                             punUtilAlgo->UtilAesAlgo.pu1AesOutBuf,
                             punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                             punUtilAlgo->UtilAesAlgo.punAesArCryptoKey,
                             punUtilAlgo->UtilAesAlgo.pu1AesInitVector,
                             (INT4 *) punUtilAlgo->UtilAesAlgo.pu4AesNum,
                             punUtilAlgo->UtilAesAlgo.i4AesEnc) != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_DES_CBC:

            if (DesArCbcEncrypt (punUtilAlgo->UtilDesAlgo.pu1DesInBuffer,
                                 punUtilAlgo->UtilDesAlgo.u4DesInBufSize,
                                 punUtilAlgo->UtilDesAlgo.punDesArCryptoKey,
                                 punUtilAlgo->UtilDesAlgo.pu1DesInitVect)
                != DES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_TDES_CBC:

            if (TDesArCbcEncrypt (punUtilAlgo->UtilDesAlgo.pu1DesInBuffer,
                                  punUtilAlgo->UtilDesAlgo.u4DesInBufSize,
                                  punUtilAlgo->UtilDesAlgo.punDesArCryptoKey,
                                  punUtilAlgo->UtilDesAlgo.pu1DesInitVect)
                != DES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        default:
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Invalid algorithm value (%d) passsed for "
                          "Encrypt Failed\n", u1Algorithm);
            u1RetVal = OSIX_FAILURE;
            break;
    }

    return u1RetVal;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilDecrypt                                      */
/*                                                                          */
/*    Description        : This function calls the appropriate algorithm to */
/*                         be decrypted based on the argument u1Algorithm.  */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm which is to be decyrpted.*/
/*                         punUtilAlgo - Union which contains datastructures*/
/*                                       of all Algorithms.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
UtilDecrypt (UINT1 u1Algorithm, unUtilAlgo * punUtilAlgo)
{
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    UINT4               u4Len = 0;

    UNUSED_PARAM (u4Len);
    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    if (UtilGetAlgoSupportInHw (u1Algorithm) == OSIX_SUCCESS)
    {
#ifdef HSEC_WANTED
        if (NpHwUtilFunction (u1Algorithm, punUtilAlgo) != FNP_SUCCESS)
        {
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Decrypt Failed for algorithm %s\n",
                          pUtilAlgoName[u1Algorithm]);
            u1RetVal = OSIX_FAILURE;
            return u1RetVal;
        }
        else
        {
            return u1RetVal;
        }
#endif
    }
#ifdef EXT_CRYPTO_WANTED
    /* The CRYPTO decryption is performed by external libraries. */
    if (ExtWrUtilDecrypt (u1Algorithm, punUtilAlgo) != OSIX_SUCCESS)
    {
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            u4Len = STRLEN ("EXT Decrypt Algorithm FAILED");
            STRNCPY (au1AuditLogMsg, "EXT Decrypt Algorithm FAILED", u4Len);
            au1AuditLogMsg[u4Len] = '\0';
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
        }
        /* If the external function may return OSIX_FAILURE in case of failing
         * to Decrypt in such as case we must return failure.*/
        MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "EXT Decrypt Failed for algorithm %s\n",
                      pUtilAlgoName[u1Algorithm]);
        return OSIX_FAILURE;
    }
    else
    {
        /* Decryption is successful in external Library. */
        return u1RetVal;
    }
#else
    switch (u1Algorithm)
    {
        case ISS_UTIL_ALGO_AES_CBC:

            if (AesArCbcDecrypt (punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                                 punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                                 (UINT2) punUtilAlgo->UtilAesAlgo.u4AesInKeyLen,
                                 punUtilAlgo->UtilAesAlgo.pu1AesInScheduleKey,
                                 punUtilAlgo->UtilAesAlgo.pu1AesInitVector)
                != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_AES:

            AesArDecrypt (punUtilAlgo->UtilAesAlgo.punAesArCryptoKey,
                          punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                          punUtilAlgo->UtilAesAlgo.pu1AesOutBuf);
            break;

        case ISS_UTIL_ALGO_AES_CFB128:

            if (AesArCfb128 (punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                             punUtilAlgo->UtilAesAlgo.pu1AesOutBuf,
                             punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                             punUtilAlgo->UtilAesAlgo.punAesArCryptoKey,
                             punUtilAlgo->UtilAesAlgo.pu1AesInitVector,
                             (INT4 *) punUtilAlgo->UtilAesAlgo.pu4AesNum,
                             punUtilAlgo->UtilAesAlgo.i4AesEnc) != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_DES_CBC:

            if (DesArCbcDecrypt (punUtilAlgo->UtilDesAlgo.pu1DesInBuffer,
                                 punUtilAlgo->UtilDesAlgo.u4DesInBufSize,
                                 punUtilAlgo->UtilDesAlgo.punDesArCryptoKey,
                                 punUtilAlgo->UtilDesAlgo.pu1DesInitVect)
                != DES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_TDES_CBC:

            if (TDesArCbcDecrypt (punUtilAlgo->UtilDesAlgo.pu1DesInBuffer,
                                  punUtilAlgo->UtilDesAlgo.u4DesInBufSize,
                                  punUtilAlgo->UtilDesAlgo.punDesArCryptoKey,
                                  punUtilAlgo->UtilDesAlgo.pu1DesInitVect)
                != DES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        default:
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Invalid algorithm value (%d) passsed "
                          "for Decrypt Failed\n", u1Algorithm);
            u1RetVal = OSIX_FAILURE;
            break;
    }

    return u1RetVal;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilMac                                          */
/*                                                                          */
/*    Description        : This function calls the appropriate algorithm to */
/*                         be authenticated based on the argument           */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm which is to be           */
/*                                       authenticated.                     */
/*                         punUtilAlgo - Union which contains datastructures*/
/*                                       of all Algorithms.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
UtilMac (UINT1 u1Algorithm, unUtilAlgo * punUtilAlgo)
{
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    UINT4               u4Len = 0;

    UNUSED_PARAM (u4Len);
    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    if (UtilGetAlgoSupportInHw (u1Algorithm) == OSIX_SUCCESS)
    {
#ifdef HSEC_WANTED
        if (NpHwUtilFunction (u1Algorithm, punUtilAlgo) != FNP_SUCCESS)
        {
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Auth Failed for algorithm %s\n",
                          pUtilAlgoName[u1Algorithm]);
            u1RetVal = OSIX_FAILURE;
            return u1RetVal;
        }
        else
        {
            return u1RetVal;
        }
#endif
    }
#ifdef EXT_CRYPTO_WANTED
    /* The CRYPTO Mac is performed by external libraries. */
    if (ExtWrUtilMac (u1Algorithm, punUtilAlgo) != OSIX_SUCCESS)
    {
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            u4Len = STRLEN ("EXT MAC Algorithm FAILED");
            STRNCPY (au1AuditLogMsg, "EXT MAC Algorithm FAILED", u4Len);
            au1AuditLogMsg[u4Len] = '\0';
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
        }
        /* If the external function may return OSIX_FAILURE in case of failing
         * to perform MAC operation in such as case we must return failure.*/
        MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "EXT MAC Failed for algorithm %s\n",
                      pUtilAlgoName[u1Algorithm]);
        return OSIX_FAILURE;
    }
    else
    {
        /* MAC opertaion is successful in external Library */
        return OSIX_SUCCESS;
    }
#else
    switch (u1Algorithm)
    {
        case ISS_UTIL_ALGO_AES_CBC_MAC96:

            if (AesArXcbcMac96 (punUtilAlgo->UtilAesAlgo.pu1AesInUserKey,
                                punUtilAlgo->UtilAesAlgo.u4AesInKeyLen,
                                punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                                (INT4) punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                                punUtilAlgo->UtilAesAlgo.pu1AesMac)
                != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_AES_CBC_MAC128:

            if (AesArXcbcMac128 (punUtilAlgo->UtilAesAlgo.pu1AesInUserKey,
                                 punUtilAlgo->UtilAesAlgo.u4AesInKeyLen,
                                 punUtilAlgo->UtilAesAlgo.pu1AesInBuf,
                                 (INT4) punUtilAlgo->UtilAesAlgo.u4AesInBufSize,
                                 punUtilAlgo->UtilAesAlgo.pu1AesMac)
                != AES_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        case ISS_UTIL_ALGO_MD5_ALGO:

            Md5Algo (punUtilAlgo->UtilMd5Algo.pu1Md5InBuf,
                     (INT4) punUtilAlgo->UtilMd5Algo.u4Md5InBufLen,
                     punUtilAlgo->UtilMd5Algo.pu1Md5OutDigest);
            break;

        case ISS_UTIL_ALGO_SHA1_ALGO:

            Sha1ArAlgo (punUtilAlgo->UtilSha1Algo.pu1Sha1Buf,
                        (INT4) punUtilAlgo->UtilSha1Algo.u4Sha1BufLen,
                        punUtilAlgo->UtilSha1Algo.pu1Sha1Digest);
            break;

        case ISS_UTIL_ALGO_SHA224_ALGO:

            Sha224ArAlgo (punUtilAlgo->UtilSha2Algo.pu1Sha2InBuf,
                          punUtilAlgo->UtilSha2Algo.i4Sha2BufLen,
                          punUtilAlgo->UtilSha2Algo.pu1Sha2MsgDigest);
            break;

        case ISS_UTIL_ALGO_SHA256_ALGO:

            Sha256ArAlgo (punUtilAlgo->UtilSha2Algo.pu1Sha2InBuf,
                          punUtilAlgo->UtilSha2Algo.i4Sha2BufLen,
                          punUtilAlgo->UtilSha2Algo.pu1Sha2MsgDigest);
            break;

        case ISS_UTIL_ALGO_SHA384_ALGO:

            Sha384ArAlgo (punUtilAlgo->UtilSha2Algo.pu1Sha2InBuf,
                          punUtilAlgo->UtilSha2Algo.i4Sha2BufLen,
                          punUtilAlgo->UtilSha2Algo.pu1Sha2MsgDigest);
            break;

        case ISS_UTIL_ALGO_SHA512_ALGO:

            Sha512ArAlgo (punUtilAlgo->UtilSha2Algo.pu1Sha2InBuf,
                          punUtilAlgo->UtilSha2Algo.i4Sha2BufLen,
                          punUtilAlgo->UtilSha2Algo.pu1Sha2MsgDigest);
            break;

        default:
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Invalid algorithm value (%d) passsed "
                          "for Authentication\n", u1Algorithm);
            u1RetVal = OSIX_FAILURE;
            break;
    }

#endif
    return u1RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilHash                                         */
/*                                                                          */
/*    Description        : This function calls the appropriate Hash         */
/*                         algorithm.                                       */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm which is to select       */
/*                                       appropriate Hash algorithm.        */
/*                         punUtilAlgo - Union which contains datastructures*/
/*                                       of all Algorithms.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
UtilHash (UINT1 u1Algorithm, unUtilAlgo * punUtilAlgo)
{
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    UINT4               u4Len = 0;

    UNUSED_PARAM (u4Len);
    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    if (UtilGetAlgoSupportInHw (u1Algorithm) == OSIX_SUCCESS)
    {
#ifdef HSEC_WANTED
        if (NpHwUtilFunction (u1Algorithm, punUtilAlgo) != FNP_SUCCESS)
        {
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Hash Failed for algorithm %s\n",
                          pUtilAlgoName[u1Algorithm]);
            u1RetVal = OSIX_FAILURE;
            return u1RetVal;
        }
        else
        {
            return u1RetVal;
        }
#endif
    }
#ifdef EXT_CRYPTO_WANTED
    /* The CRYPTO HASH is performed by external libraries. */
    if (ExtWrUtilHash (u1Algorithm, punUtilAlgo) != OSIX_SUCCESS)
    {
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            u4Len = STRLEN ("EXT HASH Algorithm FAILED");
            STRNCPY (au1AuditLogMsg, "EXT HASH Algorithm FAILED", u4Len);
            au1AuditLogMsg[u4Len] = '\0';
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
        }
        /* If the external function may return OSIX_FAILURE in case of failing
         * to HASH in such as case OSIX_FAILURE is returned.*/
        MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "EXT Hash Failed for algorithm %s\n",
                      pUtilAlgoName[u1Algorithm]);
        return OSIX_FAILURE;
    }
    else
    {
        /* Hash is successful in external Library. */
        return u1RetVal;
    }
#else
    switch (u1Algorithm)
    {
        case ISS_UTIL_ALGO_HMAC_SHA1:

            arHmac_Sha1 (punUtilAlgo->UtilHmacAlgo.pu1HmacKey,
                         punUtilAlgo->UtilHmacAlgo.u4HmacKeyLen,
                         punUtilAlgo->UtilHmacAlgo.pu1HmacPktDataPtr,
                         punUtilAlgo->UtilHmacAlgo.i4HmacPktLength,
                         punUtilAlgo->UtilHmacAlgo.pu1HmacOutDigest);
            break;

        case ISS_UTIL_ALGO_HMAC_MD5:

            arHmac_MD5 (punUtilAlgo->UtilHmacAlgo.pu1HmacPktDataPtr,
                        punUtilAlgo->UtilHmacAlgo.i4HmacPktLength,
                        punUtilAlgo->UtilHmacAlgo.pu1HmacKey,
                        punUtilAlgo->UtilHmacAlgo.u4HmacKeyLen,
                        punUtilAlgo->UtilHmacAlgo.pu1HmacOutDigest);
            break;

        case ISS_UTIL_ALGO_HMAC_SHA2:

            if (arHmacSha2 (punUtilAlgo->UtilHmacAlgo.HmacShaVersion,
                            punUtilAlgo->UtilHmacAlgo.pu1HmacPktDataPtr,
                            punUtilAlgo->UtilHmacAlgo.i4HmacPktLength,
                            punUtilAlgo->UtilHmacAlgo.pu1HmacKey,
                            (INT4) punUtilAlgo->UtilHmacAlgo.u4HmacKeyLen,
                            punUtilAlgo->UtilHmacAlgo.pu1HmacOutDigest)
                != OSIX_SUCCESS)
            {
                u1RetVal = OSIX_FAILURE;
            }
            break;

        default:
            MOD_TRC_ARG1 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                          "Invalid algorithm value (%d) passsed "
                          "for Hashing\n", u1Algorithm);
            u1RetVal = OSIX_FAILURE;
            break;
    }

    return u1RetVal;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilGetAlgoSupportInHw                           */
/*                                                                          */
/*    Description        : This function is to check whether the algorithm  */
/*                         is supported in hardware or not                  */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm to be checked for        */
/*                                       hardware support                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                        */
/****************************************************************************/
INT4
UtilGetAlgoSupportInHw (UINT1 u1AlgoIndex)
{
    if (gau1UtilHwSupportForAlgo[u1AlgoIndex] == OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilSetAlgoSupportInHw                           */
/*                                                                          */
/*    Description        : This function is to check whether the algorithm  */
/*                         is supported in hardware or not                  */
/*                                                                          */
/*    Input(s)           : u1Algorithm - Algorithm to be checked for        */
/*                                       hardware support                   */
/*                         u1HwSupport - To enable/disable the hardware     */
/*                                       support                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
UtilSetAlgoSupportInHw (UINT1 u1AlgoIndex, UINT1 u1HwSupport)
{
    gau1UtilHwSupportForAlgo[u1AlgoIndex] = u1HwSupport;
    return;
}

/*****************************************************************************/
/* Function Name      : UtlSetUtlTrcLevel                                    */
/*                                                                           */
/* Description        : This function is used to set the global trace level  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
UtlSetUtlTrcLevel (UINT4 u4UtlTrcLevel)
{
    gu4UtlTrcFlag = u4UtlTrcLevel;
    return OSIX_SUCCESS;
}

#endif
