/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: idxmgr.h,v 1.1 2015/04/28 12:51:02 siva Exp $
*
* Description: Routines for Index Manager Utility
*
*******************************************************************/

/* Index Manager List */
typedef struct _IndexManagerList
{
    UINT4          u4StartIndex;             /* Start Index of the Index List */
    UINT4          u4EndIndex;               /* End Index of the Index List */
    UINT4          u4TotalIndices;           /* Total Indices in the Index List */
    UINT4          u4FreeIndices;            /* Total number of Free indices in the List */
    UINT4          u4NextAvailableIndex;     /* Next Available Free Index in the List */
    UINT1          *pu1IndexBitList;         /* Index Bitmap List Pool */
    UINT1          u1Type;                   /* Type of getting the next available free index */
    UINT1          u1BoundReached;           /* Boundary Reached Flag*/
    UINT1          u1Reserved [2];
}tIndexManagerList;

/* Index Manager Semaphore */
tOsixSemId        gIndexManagerSemId;            

/* Global Index Manager List Pool */
tIndexManagerList gIndexManagerList[MAX_INDEX_MGR_LIST];

PRIVATE VOID IndexManagerUpdateFreeIndex PROTO ((tIndexManagerList *));
PRIVATE UINT1 IndexMgrGetFreeIndexId PROTO ((tIndexMgrId *));

