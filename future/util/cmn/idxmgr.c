/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: idxmgr.c,v 1.1 2015/04/28 12:51:02 siva Exp $
*
* Description: Routines for Index Manager Utility
*
*******************************************************************/

#include "lr.h"
#include "idxmgr.h"

/*****************************************************************************/
/* Function Name     : IndexManagerInit                                      */
/*                                                                           */
/* Description       : This function intialiases the Index Manager           */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerUtilInit ()
{
    MEMSET (gIndexManagerList, 0, sizeof (gIndexManagerList));

    if (OsixSemCrt ((UINT1 *) "IMSM", &gIndexManagerSemId) == OSIX_FAILURE)
    {
        return INDEX_FAILURE;
    }

    OsixSemGive (gIndexManagerSemId);

    return INDEX_SUCCESS;

}

/*****************************************************************************/
/* Function Name     : IndexManagerInitList                                  */
/*                                                                           */
/* Description       : This function intialiases a Index Manager Pool Record */
/*                                                                           */
/* Input Parameters  : u4StartIndex  - Start Index                           */
/*                     u4EndIndex    - End Index                             */
/*                     pu1BitmapList - Bitmap list used for maintaining      */
/*                                     the indices in the index manager      */
/*                     u1Type        - Type of Index Manager List            */
/*                                                                           */
/* Output Parameters : pIndexMgrId  - Index Manager Pool Record ID           */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerInitList (UINT4 u4StartIndex, UINT4 u4EndIndex,
                      UINT1 *pu1BitmapList, UINT1 u1Type,
                      tIndexMgrId * pIndexMgrId)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    tIndexMgrId         IndexMgrId = 0;
    UINT4               u4BitListSize = 0;

    if (pu1BitmapList == NULL)
    {
        return INDEX_FAILURE;
    }

    /* Get a Free Index Pool ID */
    if ((IndexMgrGetFreeIndexId (&IndexMgrId)) == INDEX_FAILURE)
    {
        return INDEX_FAILURE;
    }

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    *pIndexMgrId = IndexMgrId;

    /* Intialiasation of Index Manager Pool Record */
    pIndexManagerList->u4StartIndex = u4StartIndex;
    pIndexManagerList->u4EndIndex = u4EndIndex;
    pIndexManagerList->u4NextAvailableIndex = u4StartIndex;

    pIndexManagerList->pu1IndexBitList = pu1BitmapList;
    pIndexManagerList->u4TotalIndices = u4EndIndex - u4StartIndex + 1;
    pIndexManagerList->u4FreeIndices = u4EndIndex - u4StartIndex + 1;
    pIndexManagerList->u1Type = u1Type;
    pIndexManagerList->u1BoundReached = OSIX_FALSE;

    u4BitListSize = ((u4EndIndex - u4StartIndex) / BITS_PER_BYTE) + 1;

    MEMSET (pIndexManagerList->pu1IndexBitList, 0, u4BitListSize);
    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerDeInitList                                */
/*                                                                           */
/* Description       : This function De-Intialiases a Index Manager          */
/*                     Pool Record                                           */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerDeInitList (tIndexMgrId IndexMgrId)
{
    tIndexManagerList  *pIndexManagerList = NULL;

    if (IndexMgrId >= MAX_INDEX_MGR_LIST)
    {
        return INDEX_FAILURE;
    }

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    MEMSET (pIndexManagerList, 0, sizeof (tIndexManagerList));
    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerGetNextFreeIndex                          */
/*                                                                           */
/* Description       : This function returns a Free index from the given     */
/*                     Index manager Pool                                    */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                                                                           */
/* Output Parameters : pu4FreeIndex - Free index                             */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerGetNextFreeIndex (tIndexMgrId IndexMgrId, UINT4 *pu4FreeIndex)
{
    tIndexManagerList  *pIndexManagerList = NULL;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    OsixSemTake (gIndexManagerSemId);
    if (pIndexManagerList->u4FreeIndices == 0)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    OsixSemGive (gIndexManagerSemId);
    *pu4FreeIndex = pIndexManagerList->u4NextAvailableIndex;
    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerSetNextFreeIndex                          */
/*                                                                           */
/* Description       : This function returns and also sets a  Free index     */
/*                     in the Index manager Pool                             */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                                                                           */
/* Output Parameters : pu4FreeIndex - Free index                             */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerSetNextFreeIndex (tIndexMgrId IndexMgrId, UINT4 *pu4FreeIndex)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4FreeIndex = 0;
    UINT4               u4ArrSize = 0;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    OsixSemTake (gIndexManagerSemId);

    if (pIndexManagerList->u4FreeIndices == 0)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    u4ArrSize = ((pIndexManagerList->u4EndIndex -
                  pIndexManagerList->u4StartIndex) / BITS_PER_BYTE) + 1;
    u4FreeIndex = pIndexManagerList->u4NextAvailableIndex;

    OSIX_BITLIST_SET_BIT (pIndexManagerList->pu1IndexBitList,
                          (u4FreeIndex - pIndexManagerList->u4StartIndex + 1),
                          u4ArrSize);
    pIndexManagerList->u4FreeIndices -= 1;

    /* Updation of Next available Free Index */
    IndexManagerUpdateFreeIndex (pIndexManagerList);

    *pu4FreeIndex = u4FreeIndex;
    OsixSemGive (gIndexManagerSemId);

    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerCheckIndexIsFree                          */
/*                                                                           */
/* Description       : This function checks whether a particular index       */
/*                     is free or not                                        */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                     u4IndexToBeChecked - Input Index                      */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerCheckIndexIsFree (tIndexMgrId IndexMgrId, UINT4 u4IndexToBeChecked)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4ArrSize = 0;
    BOOL1               bIsSetFlag = OSIX_TRUE;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    OsixSemTake (gIndexManagerSemId);
    if ((u4IndexToBeChecked > pIndexManagerList->u4EndIndex) ||
        (u4IndexToBeChecked < pIndexManagerList->u4StartIndex))
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    u4ArrSize = ((pIndexManagerList->u4EndIndex -
                  pIndexManagerList->u4StartIndex) / BITS_PER_BYTE) + 1;
    OSIX_BITLIST_IS_BIT_SET (pIndexManagerList->pu1IndexBitList,
                             (u4IndexToBeChecked -
                              pIndexManagerList->u4StartIndex + 1), u4ArrSize,
                             bIsSetFlag);

    if (bIsSetFlag == OSIX_FALSE)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_SUCCESS;
    }

    OsixSemGive (gIndexManagerSemId);
    return INDEX_FAILURE;
}

/*****************************************************************************/
/* Function Name     : IndexManagerSetIndex                                  */
/*                                                                           */
/* Description       : This function is used to set a particular index       */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                     u4IndexToBeSet - Index to be Set.                     */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerSetIndex (tIndexMgrId IndexMgrId, UINT4 u4IndexToBeSet)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4ArrSize = 0;
    BOOL1               bIsSetFlag = OSIX_FALSE;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    OsixSemTake (gIndexManagerSemId);

    if (pIndexManagerList->u4FreeIndices == 0)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    if ((u4IndexToBeSet > pIndexManagerList->u4EndIndex) ||
        (u4IndexToBeSet < pIndexManagerList->u4StartIndex))
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    u4ArrSize = ((pIndexManagerList->u4EndIndex -
                  pIndexManagerList->u4StartIndex) / BITS_PER_BYTE) + 1;

    OSIX_BITLIST_IS_BIT_SET (pIndexManagerList->pu1IndexBitList,
                             (u4IndexToBeSet - pIndexManagerList->u4StartIndex +
                              1), u4ArrSize, bIsSetFlag);

    if (bIsSetFlag == OSIX_TRUE)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    OSIX_BITLIST_SET_BIT (pIndexManagerList->pu1IndexBitList,
                          (u4IndexToBeSet - pIndexManagerList->u4StartIndex +
                           1), u4ArrSize);
    pIndexManagerList->u4FreeIndices -= 1;

    /* In case of Incremental with holes allowed Index type, Bound reached Flag
     * is set once the End index is Set. After the Bound Flag is reached, the 
     * Index manager can give any index by filling the holes available in the
     * Index Pool */
    if ((pIndexManagerList->u1Type == INDEX_MGR_INCR_HOLES_TYPE) &&
        (pIndexManagerList->u1BoundReached == OSIX_FALSE) &&
        (pIndexManagerList->u4EndIndex == u4IndexToBeSet))
    {
        pIndexManagerList->u1BoundReached = OSIX_TRUE;
        IndexManagerUpdateFreeIndex (pIndexManagerList);
    }
    /* In case Incremental type with bound flag not set, we can directly 
     * set the next available index to one index greater than the
     * index to be set, to maintain the incremental indexing*/
    else if ((pIndexManagerList->u1Type != INDEX_MGR_NON_INCR_TYPE) &&
             (pIndexManagerList->u1BoundReached == OSIX_FALSE))
    {
        if (pIndexManagerList->u4NextAvailableIndex <= u4IndexToBeSet)
        {
            pIndexManagerList->u4NextAvailableIndex = u4IndexToBeSet + 1;
        }
    }
    /* In other cases if the next available index points to the index which 
     * is currently being set, then we update the next available index to 
     * utilise the holes in the Index Pool */
    else if (pIndexManagerList->u4NextAvailableIndex == u4IndexToBeSet)
    {
        IndexManagerUpdateFreeIndex (pIndexManagerList);
    }
    OsixSemGive (gIndexManagerSemId);
    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerFreeAllIndex                              */
/*                                                                           */
/* Description       : This function is used to free all allocated index     */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerFreeAllIndex (tIndexMgrId IndexMgrId)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4Index = 0;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    for (u4Index = pIndexManagerList->u4StartIndex;
         u4Index <= pIndexManagerList->u4EndIndex; u4Index++)
    {
        IndexManagerFreeIndex (IndexMgrId, u4Index);
    }

    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerFreeIndex                                 */
/*                                                                           */
/* Description       : This function is used to free a particular index      */
/*                     from the index pool                                   */
/*                                                                           */
/* Input Parameters  : IndexMgrId - Index Manager Record ID                  */
/*                     u4IndexToBeFreed - Index to be freed                  */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerFreeIndex (tIndexMgrId IndexMgrId, UINT4 u4IndexToBeFreed)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4ArrSize = 0;
    BOOL1               bIsSetFlag = OSIX_FALSE;

    pIndexManagerList = &gIndexManagerList[IndexMgrId];
    OsixSemTake (gIndexManagerSemId);

    if (pIndexManagerList->u1Type == INDEX_MGR_STRICT_INCR_TYPE)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }
    if ((u4IndexToBeFreed > pIndexManagerList->u4EndIndex) ||
        (u4IndexToBeFreed < pIndexManagerList->u4StartIndex))
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_FAILURE;
    }

    u4ArrSize = ((pIndexManagerList->u4EndIndex -
                  pIndexManagerList->u4StartIndex) / BITS_PER_BYTE) + 1;
    OSIX_BITLIST_IS_BIT_SET (pIndexManagerList->pu1IndexBitList,
                             (u4IndexToBeFreed -
                              pIndexManagerList->u4StartIndex + 1), u4ArrSize,
                             bIsSetFlag);

    if (bIsSetFlag == OSIX_FALSE)
    {
        OsixSemGive (gIndexManagerSemId);
        return INDEX_SUCCESS;
    }

    OSIX_BITLIST_RESET_BIT (pIndexManagerList->pu1IndexBitList,
                            (u4IndexToBeFreed -
                             pIndexManagerList->u4StartIndex + 1), u4ArrSize);

    pIndexManagerList->u4FreeIndices += 1;

    /* In case of non incremental order, next available index is made to 
     * point to the freed index if the freed index is lesser than the 
     * current next available index */
    if ((u4IndexToBeFreed < pIndexManagerList->u4NextAvailableIndex) &&
        ((pIndexManagerList->u1Type == INDEX_MGR_NON_INCR_TYPE) ||
         (pIndexManagerList->u1BoundReached == OSIX_TRUE)))
    {
        pIndexManagerList->u4NextAvailableIndex = u4IndexToBeFreed;
    }

    OsixSemGive (gIndexManagerSemId);
    return INDEX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerUpdateFreeIndex                           */
/*                                                                           */
/* Description       : This function is used to update the free index to     */
/*                     point to next available free index                    */
/*                                                                           */
/* Input Parameters  : pIndexManagerList - Index manager Pool List           */
/*                                                                           */
/* Output Parameters : None.                                                 */
/*                                                                           */
/* Return Value      : None.                                                 */
/*****************************************************************************/
PRIVATE VOID
IndexManagerUpdateFreeIndex (tIndexManagerList * pIndexManagerList)
{
    UINT4               u4Index = 0;
    UINT4               u4StartIndex = 0;
    UINT4               u4EndIndex = 0;
    UINT4               u4ArrSize = 0;
    BOOL1               bIsSetFlag = OSIX_TRUE;

    /* In case of strict incremental order, we directly update the
     * next available index to point to one greater than the current index */
    if (pIndexManagerList->u1Type == INDEX_MGR_STRICT_INCR_TYPE)
    {
        pIndexManagerList->u4NextAvailableIndex =
            pIndexManagerList->u4NextAvailableIndex + 1;
        return;
    }

    /* Searching of Free index starts from the next available index */
    u4StartIndex = pIndexManagerList->u4NextAvailableIndex -
        pIndexManagerList->u4StartIndex + 1;

    if (pIndexManagerList->u1Type == INDEX_MGR_INCR_HOLES_TYPE)
    {
        if ((pIndexManagerList->u1BoundReached == OSIX_FALSE) &&
            (pIndexManagerList->u4NextAvailableIndex !=
             pIndexManagerList->u4EndIndex))
        {
            pIndexManagerList->u4NextAvailableIndex =
                pIndexManagerList->u4NextAvailableIndex + 1;
            return;
        }
        else
        {
            pIndexManagerList->u1BoundReached = OSIX_TRUE;
            u4StartIndex = 1;
        }
    }

    u4EndIndex = pIndexManagerList->u4EndIndex -
        pIndexManagerList->u4StartIndex + 1;

    u4ArrSize = ((pIndexManagerList->u4EndIndex -
                  pIndexManagerList->u4StartIndex) / BITS_PER_BYTE) + 1;

    for (u4Index = u4StartIndex; u4Index <= u4EndIndex; u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (pIndexManagerList->pu1IndexBitList,
                                 u4Index, u4ArrSize, bIsSetFlag);

        if (bIsSetFlag == OSIX_FALSE)
        {
            pIndexManagerList->u4NextAvailableIndex = u4Index +
                pIndexManagerList->u4StartIndex - 1;
            return;
        }
    }

    /* If the list is full, the next available index to point to 
     * some invalid index not used in the pool */
    pIndexManagerList->u4NextAvailableIndex = pIndexManagerList->u4EndIndex + 1;
    return;
}

/*****************************************************************************/
/* Function Name     : IndexMgrGetFreeIndexId                                */
/*                                                                           */
/* Description       : This function returns a Free Index Pool Record ID     */
/*                                                                           */
/* Input Parameters  : None.                                                 */
/*                                                                           */
/* Output Parameters : pIndexMgrId - Index Pool Record ID                    */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
PRIVATE UINT1
IndexMgrGetFreeIndexId (tIndexMgrId * pIndexMgrId)
{
    tIndexManagerList  *pIndexManagerList = NULL;
    UINT4               u4Index;

    for (u4Index = 0; u4Index < MAX_INDEX_MGR_LIST; u4Index++)
    {
        pIndexManagerList = &gIndexManagerList[u4Index];
        if (pIndexManagerList->u4TotalIndices == 0)
        {
            *pIndexMgrId = u4Index;
            return INDEX_SUCCESS;
        }
    }
    return INDEX_FAILURE;

}
