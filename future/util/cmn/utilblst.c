/*
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *  $Id: utilblst.c,v 1.1 2015/04/28 12:51:02 siva Exp $
 *  
 *  Description:
 *  
 */

#include "lr.h"
#include "cfa.h"
#include "l2iwf.h"
#include "fsutil.h"
#include "fsbuddy.h"
#include "fsutltrc.h"

extern UINT4        gu4UtlTrcFlag;

tMemPoolId          gBitListPoolId;

UINT4               gu4BitListUsedBlocks;    /* Number of Bit List blocks 
                                               allocated so far. */
UINT4               gu4BitListTotalBlocks;    /* Number of blocks allocated in
                                               Bit List mempool. */

#define FS_UTIL_BITLIST_BLK_SIZE       sizeof(tPortList)

/****************************************************************************
* Function     : FsUtilBitListInit   
*                                            
* Description  : To intialize the mempool required for bitlist variable.
*
* Input        : None
*
* Output       : None
*
* Returns      : None   
*
***************************************************************************/
VOID
FsUtilBitListInit (UINT4 u4Count)
{
    gu4BitListTotalBlocks = u4Count;
    gu4BitListUsedBlocks = 0;
    return;
}

/****************************************************************************
* Function     : FsUtilAllocBitList 
*                                            
* Description  : To allocate memory for a bitlist from the common mempool.
*
* Input        : u4Size        - Size of the required portlist.
*
* Output       : None
*
* Returns      : Pointer to the allocated bit list.
*
***************************************************************************/
UINT1              *
FsUtilAllocBitList (UINT4 u4Size)
{
    UINT1              *pu1BitList = NULL;

    UNUSED_PARAM (u4Size);

    if (MemAllocateMemBlock (gBitListPoolId, &pu1BitList) == MEM_FAILURE)
    {
        MOD_TRC_ARG3 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "Allocation failed = %s; Failed pointer = %x Already allocated count = %d\n",
                      __FUNCTION__, pu1BitList, gu4BitListUsedBlocks);
        return NULL;
    }
    MEMSET (pu1BitList, 0, UTIL_BRG_PORT_LIST_SIZE);
    /* Increment the number of bitlist blocks allocated. */
    gu4BitListUsedBlocks++;
    return pu1BitList;
}

/****************************************************************************
* Function     : FsUtilReleaseBitList 
*                                            
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1BitList - Pointer to the bitlist
*
* Output       : None
*               
* Returns      : None
*
***************************************************************************/
VOID
FsUtilReleaseBitList (UINT1 *pu1BitList)
{
    if (MemReleaseMemBlock (gBitListPoolId, (pu1BitList)) == MEM_FAILURE)
    {
        MOD_TRC_ARG3 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "Release failed = %s; Release pointer = %x Release count = %d \n",
                      __FUNCTION__, pu1BitList, gu4BitListUsedBlocks);
    }

    /* Decrement the number of bitlist blocks allocated. */
    gu4BitListUsedBlocks--;
    pu1BitList = NULL;
}

/****************************************************************************
* Function     : FsUtilBitListIsAllZeros
*                                            
* Description  : This function will return OSIX_SUCCESS when the bitlist 
*                consists of all Zeros, OSIX_FAILURE otherwise.
*
* Input        : pu1BitList - Pointer to the bitlist
*                u4Size     - Size to be compared
*
* Output       : None
*               
* Returns      : OSIX_TRUE/OSIX_FALSE
*
***************************************************************************/
INT4
FsUtilBitListIsAllZeros (UINT1 *pu1BitList, UINT4 u4Size)
{
    UINT4               u4Index = 0;

    for (; u4Index < u4Size; u4Index++)
    {
        if (pu1BitList[u4Index] != 0)
        {
            return OSIX_FALSE;
        }
    }

    return OSIX_TRUE;
}
