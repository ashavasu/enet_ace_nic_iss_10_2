/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: utilsz.c,v 1.1 2015/04/28 12:51:02 siva Exp $
 * Description: This file contains all mempool creation/deletion 
 *              functions for UTIL Module.
****************************************************************************/
#define _UTILSZ_C
#include "utlshinc.h"

extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

#ifdef  _UTILSZ_C
INT4
UtilSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < UTIL_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsUTILSizingParams[i4SizingId].u4StructSize,
                              FsUTILSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(UTILMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            UtilSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
UtilSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsUTILSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, UTILMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
UtilSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < UTIL_MAX_SIZING_ID; i4SizingId++)
    {
        if (UTILMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (UTILMemPoolIds[i4SizingId]);
            UTILMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
