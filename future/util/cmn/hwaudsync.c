#ifndef _HWAUDSYNC_C_
#define _HWAUDSYNC_C_
#include "lr.h"
#include "cfa.h"
#include "fsvlan.h"
#include "rmgr.h"
#include "hwaud.h"

/* -------------------------------------------------------------
 *
 * Function: NpSyncContextIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncContextIdxSync (tNpSyncContextIdx ContextIdx, UINT4 u4AppId,
                      UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, ContextIdx.u4ContextId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncVlanIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncVlanIdxSync (tNpSyncVlanIdx VlanIdx, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (tVlanId);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, VlanIdx.u4ContextId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, VlanIdx.VlanId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncPortIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncPortIdxSync (tNpSyncPortIdx PortIdx, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortIdx.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortIdx.u4IfIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncPbbPortIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncPbbPortIdxSync (tNpSyncPbbPortIdx PbbPortIdx, UINT4 u4AppId,
                      UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PbbPortIdx.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PbbPortIdx.u4VipIfIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncIsidIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncIsidIdxSync (tNpSyncIsidIdx IsidIdx, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, IsidIdx.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, IsidIdx.u4CbpIfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, IsidIdx.u4BSid);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}
#endif
