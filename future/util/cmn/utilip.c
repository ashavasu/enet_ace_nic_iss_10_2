/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilip.c,v 1.9 2015/04/28 12:51:02 siva Exp $
 *
 * Description: 
 *
 */

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "arp.h"
#include "trace.h"
#include "snmputil.h"
#include "cli.h"

static UINT4        su4last = 0;
extern UINT1        gu1UtlDataLenPoolId;

/*****************************************************************************/
/*  Function Name   : UtlIpCSumLinBuf                                        */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pi1Buf       - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
UINT2
UtlIpCSumLinBuf (const INT1 *pi1Buf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((const UINT2 *) ((const VOID *) pi1Buf));
        u4Sum += u2Tmp;
        pi1Buf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = (UINT1) *pi1Buf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}

/*****************************************************************************/
/*  Function Name   : UtlIpCSumCruBuf                                        */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
UINT2
UtlIpCSumCruBuf (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4Size, UINT4 u4Offset)
{
#define TMP_ARRAY_SIZE   (16 * 2)    /* Should be a multiple of 2 */

    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1               u1Byte = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[TMP_ARRAY_SIZE];
    UINT4               u4TmpSize = 0;

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > TMP_ARRAY_SIZE)
        {
            u4TmpSize = TMP_ARRAY_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, au1TmpArray, u4Offset,
                                           u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > 0)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += sizeof (UINT2);
                u4TmpSize -= sizeof (UINT2);
            }

            u4Size -= TMP_ARRAY_SIZE;
            u4Offset += TMP_ARRAY_SIZE;
        }

        while (u4Size > 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp, u4Offset,
                                       sizeof (UINT2));
            u4Sum += u2Tmp;
            u4Size -= sizeof (UINT2);
            u4Offset += sizeof (UINT2);
        }

        if (u4Size == 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset, sizeof (UINT1));
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}

/******************************************************************************
 *  Function Name   : UtilConvMcastIP2Mac
 *  Description     : This function converts the IP multicast address to the 
 *                    euivalent multicast MAC address
 *  Input(s)        : None
 *  Output(s)       : None
 *  Returns         : Void
 ******************************************************************************/

PUBLIC VOID
UtilConvMcastIP2Mac (UINT4 u4GrpAddr, UINT1 *pu1McMacAddr)
{
    UINT4               u4Dword;
    UINT2               u2Word;
    u4Dword = 0x01005E00 | ((u4GrpAddr & 0x007f0000) >> 16);
    u2Word = (UINT2) (u4GrpAddr & 0x0000ffff);
    u4Dword = OSIX_NTOHL (u4Dword);
    u2Word = OSIX_NTOHS (u2Word);
    MEMCPY (pu1McMacAddr, (UINT1 *) &u4Dword, 4);
    MEMCPY (&pu1McMacAddr[4], (UINT1 *) &u2Word, 2);
}

VOID
UtilGetPortArrayFromPortList (UINT1 *pu1PortList, UINT2 u2ListSize,
                              UINT2 u2MaxPorts, UINT2 *pu2PortArray,
                              UINT2 *pu2Count)
{
    UINT2               u2UtilPort;
    BOOL1               bResult = OSIX_FALSE;

    *pu2Count = 0;
    for (u2UtilPort = 1; u2UtilPort <= u2MaxPorts; u2UtilPort++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1PortList, u2UtilPort, u2ListSize, bResult);

        if (bResult == OSIX_TRUE)
        {
            *(pu2PortArray++) = u2UtilPort;
            (*pu2Count)++;
        }
    }
}

/****************************************************************************
* Function     : UtilConvertMacToDotStr   
*                                            
* Description  : To print the Mac Address in a string format 
*
* Input        : pu1Mac - Mac Address     
*
* Output       : pu1DotStr -  String holding the Mac Address 
*               
*
* Returns      : None   
*
***************************************************************************/
VOID
UtilConvertMacToDotStr (UINT1 *pu1Mac, UINT1 *pu1DotStr)
{
    UINT1               u1Index = 0;
    CHR1               *pTemp = (CHR1 *) pu1DotStr;

    if (!(pu1Mac) || !(pu1DotStr))
        return;

    for (u1Index = 0; u1Index < MAC_ADDR_LEN; u1Index++)
    {
        SPRINTF ((CHR1 *) pTemp, "%02x", (pu1Mac)[u1Index]);
        pTemp += 2;
        *pTemp = ':';
        pTemp++;
    }
    pTemp--;
    *pTemp = '\0';
}

/****************************************************************************
* Function     : UtilConvertIpToStr
*
* Description  : To print the Ip Address in a string format
*
* Input        : u4Value - Ip Address
*
* Output       : pString -  String holding the Ip Address
*
*
* Returns      : None
*
***************************************************************************/
VOID
UtilConvertIpAddrToStr (CHR1 ** pString, UINT4 u4Value)
{
    tUtlInAddr          IpAddr;

    IpAddr.u4Addr = OSIX_NTOHL (u4Value);

    /* We need 15 bytes to store 255.255.255.255 */

    *pString = UtlInetNtoa (IpAddr);
    return;
}

/****************************************************************************/
/*                                                                           */
/*    Function Name      : UtilCalcCheckSum                                   */
/*                                                                           */
/*    Description        : Implementation of IP Checksumming. It calculates  */
/*                         checksum by reading the file line by line.        */
/*                                                                           */
/*    Input(s)           : pData points to the databuffer to be checksummed. */
/*                         u4len is the line length, pu4Sum points to the    */
/*                         checksum value calculated so far. pu2cSum will be */
/*                         pointing to the final checksum value and u4Type   */
/*                         is the type of the data to be checksummed.        */
/*                                                                           */
/*    Output(s)          : None.                                             */
/*                                                                           */
/*    Returns            : Returns the computed checksum.                    */
/*                                                                           */
/*****************************************************************************/
VOID
UtilCalcCheckSum (UINT1 *pData, UINT4 u4len, UINT4 *pu4Sum,
                  UINT2 *pu2cSum, UINT4 u4type)
{
    UINT4               u4Sum = 0;
    UINT4               u4Size = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pDataBuf = NULL;
    UINT1              *pBuf = NULL;

    pDataBuf = (UINT1 *) MemAllocMemBlk (gu1UtlDataLenPoolId);
    if (pDataBuf == NULL)
    {
        UtlTrcLog (1, 1, "UTIL", "Memory Allocation Failed");
        return;
    }
    MEMSET (pDataBuf, 0, MAX_DATA_LEN + 1);
    if (u4type != CKSUM_LASTDATA)
    {

        if (su4last == 0)
        {
            if ((u4len % 2) == 1)
            {
                pBuf = pData;
                u4Size = u4len - 1;
                su4last = 1;
            }
            else
            {
                pBuf = pData;
                u4Size = u4len;
                su4last = 0;
            }
        }
        else
        {
            pBuf = pDataBuf;
            sprintf ((char *) pDataBuf, "\n");
            pBuf += 1;

            if ((u4len % 2) == 1)
            {
                MEMCPY (pBuf, pData, MEM_MAX_BYTES (u4len, MAX_DATA_LEN));
                u4Size = u4len + 1;
                su4last = 0;
            }
            else
            {
                MEMCPY (pBuf, pData, MEM_MAX_BYTES ((u4len - 1), MAX_DATA_LEN));
                u4Size = u4len;
                su4last = 1;
            }
            pBuf = pDataBuf;
        }

        while (u4Size >= 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pBuf);
            MEMCPY (&u2Tmp, pBuf, sizeof (UINT2));
            *pu4Sum += u2Tmp;
            pBuf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
    }
    else
    {
        if (su4last == 1)
        {
            pBuf = pDataBuf;
            sprintf ((char *) pDataBuf, "\n");
            pBuf += 1;
            MEMCPY (pBuf, pData, MEM_MAX_BYTES (u4len, MAX_DATA_LEN));
            u4Size = u4len + 1;
            su4last = 0;
            pBuf = pDataBuf;
        }
        else
        {
            pBuf = pData;
            u4Size = u4len;
            su4last = 0;
        }
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pBuf);
            *pu4Sum += u2Tmp;
            pBuf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }

        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pBuf;
            *pu4Sum += u2Tmp;
        }
        u4Sum = *pu4Sum;
        u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
        u4Sum += (u4Sum >> 16);
        u2Tmp = (UINT2) (~((UINT2) u4Sum));
        *pu2cSum = u2Tmp;
    }
    MemReleaseMemBlock (gu1UtlDataLenPoolId, (UINT1 *) pDataBuf);

}

/****************************************************************************/
/*                                                                           */
/*    Function Name      : UtilCompCheckSum                                  */
/*                                                                           */
/*    Description        : Implementation of  Checksumming. It calculates    */
/*                         checksum by reading the file line by line.        */
/*                                                                           */
/*    Input(s)           : pData points to the databuffer to be checksummed. */
/*                         u4len is the line length, pu4Sum points to the    */
/*                         checksum value calculated so far. pu2cSum will be */
/*                         pointing to the final checksum value and u4Type   */
/*                         is the type of the data to be checksummed.        */
/*                                                                           */
/*    Output(s)          : None.                                             */
/*                                                                           */
/*    Returns            : Returns the computed checksum.                    */
/*                                                                           */
/*****************************************************************************/
VOID
UtilCompCheckSum (UINT1 *pData, UINT4 *pu4Sum,
                  UINT4 *pu4Last, UINT2 *pu2cSum, UINT4 u4len, UINT4 u4type)
{
    UINT4               u4Sum = 0;
    UINT4               u4Size = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pDataBuf = NULL;
    UINT1              *pBuf = NULL;

    pDataBuf = (UINT1 *) MemAllocMemBlk (gu1UtlDataLenPoolId);
    if (pDataBuf == NULL)
    {
        return;
    }
    MEMSET (pDataBuf, 0, MAX_DATA_LEN + 1);
    if (u4type != CKSUM_LASTDATA)
    {

        if (*pu4Last == 0)
        {
            if ((u4len % 2) == 1)
            {
                pBuf = pData;
                u4Size = u4len - 1;
                *pu4Last = 1;
            }
            else
            {
                pBuf = pData;
                u4Size = u4len;
                *pu4Last = 0;
            }
        }
        else
        {
            pBuf = pDataBuf;
            sprintf ((char *) pDataBuf, "\n");
            pBuf += 1;

            if ((u4len % 2) == 1)
            {
                MEMCPY (pBuf, pData, MEM_MAX_BYTES (u4len, MAX_DATA_LEN));
                u4Size = u4len + 1;
                *pu4Last = 0;
            }
            else
            {
                MEMCPY (pBuf, pData, MEM_MAX_BYTES ((u4len - 1), MAX_DATA_LEN));
                u4Size = u4len;
                *pu4Last = 1;
            }
            pBuf = pDataBuf;
        }

        while (u4Size >= 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pBuf);
            MEMCPY (&u2Tmp, pBuf, sizeof (UINT2));
            *pu4Sum += u2Tmp;
            pBuf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
    }
    else
    {
        if (*pu4Last == 1)
        {
            pBuf = pDataBuf;
            sprintf ((char *) pDataBuf, "\n");
            pBuf += 1;
            MEMCPY (pBuf, pData, MEM_MAX_BYTES (u4len, MAX_DATA_LEN));
            u4Size = u4len + 1;
            *pu4Last = 0;
            pBuf = pDataBuf;
        }
        else
        {
            pBuf = pData;
            u4Size = u4len;
            *pu4Last = 0;
        }
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pBuf);
            *pu4Sum += u2Tmp;
            pBuf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }

        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pBuf;
            *pu4Sum += u2Tmp;
        }
        u4Sum = *pu4Sum;
        u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
        u4Sum += (u4Sum >> 16);
        u2Tmp = (UINT2) (~((UINT2) u4Sum));
        *pu2cSum = u2Tmp;
    }
    MemReleaseMemBlock (gu1UtlDataLenPoolId, (UINT1 *) pDataBuf);

}

/****************************************************************************/
/*                                                                           */
/*    Function Name      : UtilSetChkSumLastStat                             */
/*                                                                           */
/*    Description        : Implementation of IP Checksumming. It calculates  */
/*                         checksum by reading the file line by line.        */
/*                                                                           */
/*    Input(s)           : pData points to the databuffer to be checksummed. */
/*                         u4len is the line length, pu4Sum points to the    */
/*                         checksum value calculated so far. pu2cSum will be */
/*                         pointing to the final checksum value and u4Type   */
/*                         is the type of the data to be checksummed.        */
/*                                                                           */
/*    Output(s)          : None.                                             */
/*                                                                           */
/*    Returns            : Returns the computed checksum.                    */
/*                                                                           */
/*****************************************************************************/
VOID
UtilSetChkSumLastStat (UINT4 u4InitVal)
{
    su4last = u4InitVal;
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : UtilDectoHexStr                                      *
 *                                                                         *
 *     Description   : This function converts Decimal to hex string        *
 *                                                                         *
 *                                                                         *
 *     Input(s)      : u4Value    : Decimal value to be converted          *
 *                                                                         *
 *     Output(s)     : pu1HexStr  : Pointer to the converted hex string    *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID UtilDectoHexStr(UINT4 u4Value, UINT1 *pu1HexStr)
{
	INT4           i4Quotient = 0;
	INT4           i4Index = 0, i4Temp = 0;
	UINT1           *pu1RevTemp1 = NULL, *pu1RevTemp2 = NULL;

	if (!(pu1HexStr))
	{
		return;
	}

	i4Quotient = u4Value;

	while(i4Quotient!=0){
		i4Temp = i4Quotient % 16;
		/* To convert integer into character*/
		if( i4Temp < 10)
		{
			i4Temp =i4Temp + 48; /*ASCII value addition for 0-9*/
		}
		else
		{
			i4Temp = i4Temp + 55; /*ASCII value addition for A-F*/
		}
		pu1HexStr[i4Index++]= i4Temp;
		i4Quotient = i4Quotient / 16;
	}
	pu1HexStr[i4Index] = '\0';

	if ( ! *pu1HexStr)
	{
		return ;
	}
	/* To do string reverse */
	for (pu1RevTemp1 = pu1HexStr, 
         pu1RevTemp2 = pu1HexStr + (STRLEN((CHR1 *)pu1HexStr) - 1 ); 
         pu1RevTemp2 > pu1RevTemp1; ++pu1RevTemp1, --pu1RevTemp2)
	{
		*pu1RevTemp1 ^= *pu1RevTemp2;
		*pu1RevTemp2 ^= *pu1RevTemp1;
		*pu1RevTemp1 ^= *pu1RevTemp2;
	}
	return;
}


/***************************************************************************
 *                                                                         *
 *     Function Name : UtilHexStrToDecimal                                  *
 *                                                                         *
 *     Description   : This function converts hexa to integer              *
 *                                                                         *
 *     Input(s)      : pu1Str     : Pointer to the HEX string              *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Decimal value of HEX string, if valid HEX string    *
 *                     else, HEX_FAILURE                                   *
 *                                                                         *
 ***************************************************************************/

	INT4
UtilHexStrToDecimal (UINT1 *pu1Str)
{
	INT4                i4Value = 0;
	UINT4               u4HexCount = 1;
	INT4                i4CurrValue = 0;
	UINT4               u4Count = 0;

	if (NULL != pu1Str)
	{
		u4Count = STRLEN (pu1Str);
	}

	if (!(pu1Str) || (*pu1Str != '0') ||
			(*(pu1Str + 1) != 'x') || (u4Count == 2))
	{
		return -1;
	}

	while (u4Count > 2)
	{
		i4CurrValue = UtilGetDecValue (*(pu1Str + u4Count - 1));

		if (i4CurrValue == HEX_FAILURE)
			return HEX_FAILURE;

		i4Value = (INT4) ((UINT4) i4Value + ((UINT4) i4CurrValue * u4HexCount));

		u4HexCount *= 16;

		u4Count--;
	}

	return (i4Value);
}
/***************************************************************************
 *                                                                         *
 *     Function Name : UtilGetDecValue                                      *
 *                                                                         *
 *     Description   : This function gets decimal value of hexa character  *
 *                                                                         *
 *     Input(s)      : u1Char    : Hex char                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Decimal value of the given HEX char                 *
 *                                                                         *
 ***************************************************************************/

	INT4
UtilGetDecValue (UINT1 u1Char)
{
	INT4                i4DecValue = 0;

	if ((u1Char >= '0') && (u1Char <= '9'))
		i4DecValue = u1Char - '0';
	else
	{
		switch (u1Char)
		{
			case 'a':
			case 'A':
				i4DecValue = 10;
				break;
			case 'b':
			case 'B':
				i4DecValue = 11;
				break;
			case 'c':
			case 'C':
				i4DecValue = 12;
				break;
			case 'd':
			case 'D':
				i4DecValue = 13;
				break;
			case 'e':
			case 'E':
				i4DecValue = 14;
				break;
			case 'f':
			case 'F':
				i4DecValue = 15;
				break;
			default:
				i4DecValue = -1;
		}
	}
	return i4DecValue;
}

/*****************************************************************************/
/* Function     : UtilSelectRouterId                                         */
/* Description  : This procedure selecting router-id of Highest Ip           */
/*                of interface address.                                      */
/* Input        : u4CxtId  :  Context Id                                     */
/* Output       : pu4RouterId        : IP address                            */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*****************************************************************************/
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
PUBLIC INT4
UtilSelectRouterId (UINT4 u4CxtId, UINT4 *pu4RouterId)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4LoopbackAddr = 0;
    UINT4               u4Addr = 0;
    UINT4               u4IfAddr = 0;
    UINT1               u1LoopbackFlag = FALSE;
    UINT1               u1CxtIdFalg = FALSE;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetFirstIfInfoInCxt (u4CxtId,
                                    &NetIpIfInfo) == NETIPV4_SUCCESS)
    {
        do
        {
            u4IfIndex = NetIpIfInfo.u4IfIndex;
            u4IfAddr = NetIpIfInfo.u4Addr;
            if (NetIpIfInfo.u4ContextId == u4CxtId)
            {
                u1CxtIdFalg = TRUE;
                if ((u4IfAddr != 0) && (NetIpIfInfo.u4IfType == CFA_LOOPBACK)
                    && (u4IfAddr > u4LoopbackAddr))
                {
                    /* highest priority is highest loopback
                     * address should be select as bgp router-id */
                    u4LoopbackAddr = u4IfAddr;
                    u1LoopbackFlag = TRUE;
                }
                else if ((u4IfAddr != 0) &&(u1LoopbackFlag == FALSE)
                         && (u4IfAddr > u4Addr))
                {
                    u4Addr = u4IfAddr;
                }
            }

            MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        }
        while (NetIpv4GetNextIfInfoInCxt (u4CxtId,
                                          u4IfIndex,
                                          &NetIpIfInfo) == NETIPV4_SUCCESS);
        if (u1CxtIdFalg == TRUE)
        {
                if(u1LoopbackFlag == TRUE)
                {
                        *pu4RouterId = u4LoopbackAddr;
                }
                else
                {
                        *pu4RouterId = u4Addr;
                }
        }
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}
#endif /*IP_WANTED || LNXIP4_WANTED*/ 
