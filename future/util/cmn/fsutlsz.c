#include "fsutlsz.h"

/******************************************************************************/
/* Function Name     : FsUtlSzCalculateModulePreAllocatedMemory               */
/* Description       : This procedure computes the pre allocated memory for   */
/*                     given module details.                                  */
/* Input Parameters  : tFsModSizingInfo - Module Sizing Info                  */
/* Output Parameters : NONE                                                   */
/* Return Value      : pre allocated memory                                   */
/******************************************************************************/
UINT4
FsUtlSzCalculateModulePreAllocatedMemory (tFsModSizingInfo * pModSizingInfo)
{
    UINT4               u4Index = 0;
    UINT4               u4TotSize = 0;

    tFsModSizingParams *pModSizingParams = pModSizingInfo->ModSizingParams;

    for (u4Index = 0; pModSizingParams[u4Index].u4StructSize != 0; u4Index++)
    {
        pModSizingParams[u4Index].u4MemPreAllocated =
            pModSizingParams[u4Index].u4StructSize *
            pModSizingParams[u4Index].u4PreAllocatedUnits;
        u4TotSize += pModSizingParams[u4Index].u4MemPreAllocated;
    }
    pModSizingInfo->u4ModMemPreAllocated = u4TotSize;
    return u4TotSize;
}
