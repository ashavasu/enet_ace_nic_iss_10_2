/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id : utilinit.c $
*
* Description: This file contains the init functions required for 
               enabling the hardware security algorithms 
*******************************************************************/

#ifndef __UTILINIT_C__
#define __UTILINIT_C__

#include "lr.h"
#include "utilalgo.h"
#include "hsec.h"

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtilInit                                           */
/*                                                                            */
/*    Description        : This function calls the appropriate init functions */
/*                         depends on the gi4SysOperMode.                     */
/*                         If the opermode is SEC_KERN_USER, it will invoke   */
/*                         the user space function to create character device */
/*                         If the opermode is SEC_KERN, then it will invoke   */
/*                         the kernel space function to register the device   */
/*                                                                            */
/*    Input(s)           : None                                               */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : None                                               */
/******************************************************************************/

VOID
UtilInit (VOID)
{
#ifdef HSEC_WANTED

    if (NpSecAlgoInit () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }
#endif
    lrInitComplete (OSIX_SUCCESS);
    return;
}

#endif
