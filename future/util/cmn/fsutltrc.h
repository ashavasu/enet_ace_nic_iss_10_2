/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutltrc.h,v 1.1 2015/04/28 12:51:02 siva Exp $
 *
 * Description: This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _UTIL_TRC_H_
#define _UTIL_TRC_H_


#ifdef TRACE_WANTED

#define  UTIL_TRC_FLAG  gu4UtlTrcFlag
#define  UTIL_NAME      "[UTIL] "

#define UTIL_TRC(TraceType, Str)                                              \
        MOD_TRC(UTIL_TRC_FLAG, TraceType, UTIL_NAME, Str)

#else  /* TRACE_WANTED */
#define UTIL_TRC(TraceType, Str)
#endif /* TRACE_WANTED */


#endif /* _UTIL_TRC_H_ */
/*---------------------------------------------------------------------------*/
/*                       End of the file  fsutltrc.h                         */
/*---------------------------------------------------------------------------*/
