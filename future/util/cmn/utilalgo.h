/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilalgo.h,v 1.2 2017/09/12 13:27:06 siva Exp $
 *
 * Description:
 *     This file contains the exported definitions and  macros of 
 *     util module.
 *
 *******************************************************************/

#ifndef __UTILALGO_H__
#define __UTILALGO_H__

#include "cryartdf.h"
#include "sha2.h"

#define ISS_UTIL_ENCRYPT          1
#define ISS_UTIL_DECRYPT          0
#define UTIL_MAX_ALGOS            17

enum {
ISS_UTIL_ALGO_AES_CBC,
ISS_UTIL_ALGO_AES,
ISS_UTIL_ALGO_AES_CBC_MAC96,
ISS_UTIL_ALGO_AES_CBC_MAC128,
ISS_UTIL_ALGO_AES_CTR_MODE,
ISS_UTIL_ALGO_AES_CFB128,
ISS_UTIL_ALGO_DES_CBC,
ISS_UTIL_ALGO_TDES_CBC,
ISS_UTIL_ALGO_HMAC_SHA1,
ISS_UTIL_ALGO_HMAC_MD5,
ISS_UTIL_ALGO_HMAC_SHA2,
ISS_UTIL_ALGO_MD5_ALGO,
ISS_UTIL_ALGO_SHA1_ALGO,
ISS_UTIL_ALGO_SHA224_ALGO,
ISS_UTIL_ALGO_SHA256_ALGO,
ISS_UTIL_ALGO_SHA384_ALGO,
ISS_UTIL_ALGO_SHA512_ALGO
};

#ifdef _UTILALGO_C_
UINT1        gau1UtilHwSupportForAlgo[UTIL_MAX_ALGOS];
#else
extern UINT1 gau1UtilHwSupportForAlgo[UTIL_MAX_ALGOS];
#endif

typedef struct _UtilAesAlgo
{ 
    unArCryptoKey *punAesArCryptoKey;
    UINT1         *pu1AesInBuf;
    UINT1         *pu1AesOutBuf;
    UINT1         *pu1AesEncryptBuf;
    UINT1         *pu1AesInScheduleKey;
    UINT1         *pu1AesInUserKey;
    UINT1         *pu1AesInitVector;
    UINT1         *pu1AesMac;
    UINT4         *pu4AesNum;
    UINT4          u4AesInBufSize;
    UINT4          u4AesInKeyLen;
    UINT4          u4AesInitVectLen;
    INT4           i4AesEnc;
} tUtilAesAlgo;

typedef struct _UtilDesAlgo 
{
    unArCryptoKey   *punDesArCryptoKey;
    UINT1           *pu1DesInUserKey;
    UINT1           *pu1DesInUserKey2;
    UINT1           *pu1DesInUserKey3;
    UINT1           *pu1DesInBuffer;
    UINT1           *pu1DesInitVect;
    UINT4            u4DesInBufSize;
    UINT4            u4DesInitVectLen;
    UINT4            u4DesInKeyLen;
    INT4             i4DesEnc;
} tUtilDesAlgo;

typedef struct _UtilHmacAlgo
{
    UINT1        *pu1HmacKey;
    UINT1        *pu1HmacPktDataPtr;
    UINT1        *pu1HmacOutDigest;
    UINT4         u4HmacKeyLen;
    UINT4         u4HmacOutDigestLen;
    INT4          i4HmacPktLength;
    eShaVersion   HmacShaVersion;
} tUtilHmacAlgo;

typedef struct _UtilMd5Algo
{
    UINT1                 *pu1Md5InBuf;
    UINT1                 *pu1Md5OutDigest;
    UINT4                  u4Md5InBufLen;
} tUtilMd5Algo;

typedef struct _UtilSha1Algo
{
    unArCryptoHash *pu1ArSha1Context;
    UINT4          *pu4Sha1Arstate;   
    UINT1          *pu1Sha1Buf;
    UINT1          *pu1Sha1Digest;
    UINT4           u4Sha1BufLen;
    UINT4           u4Sha1OutDigestLen;
} tUtilSha1Algo;

typedef struct _UtilSha2Algo
{
    UINT1         *pu1Sha2InBuf;
    UINT1         *pu1Sha2MsgDigest;
    INT4           i4Sha2BufLen;
    UINT4          u4Sha2OutDigestLen;
    enum SHAversion     Sha2Version;
} tUtilSha2Algo;

typedef union _UtilAlgo 
{
    tUtilAesAlgo    UtilAesAlgo;
    tUtilDesAlgo    UtilDesAlgo;
    tUtilHmacAlgo   UtilHmacAlgo;
    tUtilMd5Algo    UtilMd5Algo;
    tUtilSha1Algo   UtilSha1Algo;
    tUtilSha2Algo   UtilSha2Algo;
} unUtilAlgo;

INT4 UtilEncrypt PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
INT4 UtilDecrypt PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
INT4 UtilMac     PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
INT4 UtilHash    PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));

VOID UtilInit PROTO ((VOID));
VOID UtilSetAlgoSupportInHw PROTO ((UINT1 u1AlgoIndex, UINT1 u1HwSupport));
INT4 UtilGetAlgoSupportInHw PROTO ((UINT1 u1AlgoIndex));
INT1 UtlSetUtlTrcLevel PROTO ((UINT4 u4UtlTrcLevel));

#ifdef EXT_CRYPTO_WANTED
/*External Crypto functions for Safe Logic */
extern INT4 ExtWrUtilEncrypt PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
extern INT4 ExtWrUtilDecrypt PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
extern INT4 ExtWrUtilHash    PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
extern INT4 ExtWrUtilMac     PROTO ((UINT1 u1Algorithm, unUtilAlgo *punUtilAlgo));
#endif

#endif
