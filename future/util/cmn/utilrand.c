/********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 *  $Id: utilrand.c,v 1.1 2015/04/28 12:51:02 siva Exp $ *
 *                                                                  *
 *  Description: This file contains Random Number implementation of *
 *               FIPS approved AES PRNG based on ANSI X9.31 A.2.4.  *
 ********************************************************************/
#ifndef _UTILRAND_C_
#define _UTILRAND_C_

#include "utilrand.h"
#include "fsutil.h"

extern UINT4        UTIL_TRC_FLAG;

PRIVATE INT4        gi4PrevGenRandomNum;

/* AES FIPS PRNG implementation */

/* -------------------------------------------- */
tUtilPrngCtx        StatCtx;
PRIVATE UINT1       u1CleanseCtr = 0;

/***************************************************************************
 * Function           : UtilRandPrngReset 
 *
 * Description        : This function resets the PRNG Context
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
VOID
UtilRandPrngReset (tUtilPrngCtx * pPrngCtx)
{
    pPrngCtx->i4Seeded = 0;
    pPrngCtx->i4Keyed = 0;
    pPrngCtx->i4TestMode = 0;
    pPrngCtx->u8Counter = 0;
    pPrngCtx->i4PrngSecond = 0;
    pPrngCtx->i4PrngError = 0;
    pPrngCtx->i4Vpos = 0;
    UtilRandCleanse (pPrngCtx->au1Vect, UTIL_RAND_AES_BLOCK_LEN);
    UtilRandCleanse (&pPrngCtx->CryptoKey, sizeof (unArCryptoKey));
    return;
}

/***************************************************************************
 * Function           : UtilRandSetPrngKey
 *
 * Description        : This function sets the given key value in PRNG
 *                      context
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                      pu1Key - Pointer to the key
 *                      KeyLen - Length of the key
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandSetPrngKey (tUtilPrngCtx * pPrngCtx, UINT1 *pu1Key, size_t KeyLen)
{
    if ((KeyLen != UTIL_RAND_AES128_KEY_LEN) &&
        (KeyLen != UTIL_RAND_AES192_KEY_LEN) &&
        (KeyLen != UTIL_RAND_AES256_KEY_LEN))
    {
        /* error: invalid key size */
        return OSIX_FAILURE;
    }
    AesArSetEncryptKey (pu1Key, (UINT2) (KeyLen << 3), &pPrngCtx->CryptoKey);
    if (KeyLen == UTIL_RAND_AES128_KEY_LEN)
    {
        MEMCPY (pPrngCtx->au1TmpKey, pu1Key, UTIL_RAND_AES128_KEY_LEN);
        pPrngCtx->i4Keyed = 2;
    }
    else
    {
        pPrngCtx->i4Keyed = 1;
    }
    pPrngCtx->i4Seeded = 0;
    pPrngCtx->i4PrngSecond = 0;
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRandSetPrngSeed
 *
 * Description        : This function sets the given seed value in PRNG
 *                      context
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                      pu1Seed - Pointer to the seed
 *                      SeedLen - Length of the seed
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandSetPrngSeed (tUtilPrngCtx * pPrngCtx,
                     CONST UINT1 *pu1Seed, size_t SeedLen)
{
    INT4                i4Count = 0;

    if (!pPrngCtx->i4Keyed)
    {
        return OSIX_FAILURE;
    }
    /* In test mode seed is just supplied data */
    if (pPrngCtx->i4TestMode)
    {
        if (SeedLen != UTIL_RAND_AES_BLOCK_LEN)
        {
            return OSIX_FAILURE;
        }
        MEMCPY (pPrngCtx->au1Vect, pu1Seed, UTIL_RAND_AES_BLOCK_LEN);
        pPrngCtx->i4Seeded = 1;
        return OSIX_SUCCESS;
    }
    /* Outside test mode XOR supplied data with existing seed */
    for (i4Count = 0; i4Count < (INT4) SeedLen; i4Count++)
    {
        pPrngCtx->au1Vect[pPrngCtx->i4Vpos++] ^= pu1Seed[i4Count];
        if (pPrngCtx->i4Vpos >= UTIL_RAND_AES_BLOCK_LEN)
        {
            pPrngCtx->i4Vpos = 0;
            /* Special case if first seed and key length equals
             * block size check key and seed do not match.
             */
            if (pPrngCtx->i4Keyed == 2)
            {
                if (!MEMCMP (pPrngCtx->au1TmpKey, pPrngCtx->au1Vect,
                             UTIL_RAND_AES128_KEY_LEN))
                {
                    UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: key and seed must "
                              "not match\n");
                    return OSIX_FAILURE;
                }
                UtilRandCleanse (pPrngCtx->au1TmpKey, UTIL_RAND_AES128_KEY_LEN);
                pPrngCtx->i4Keyed = 1;
            }
            pPrngCtx->i4Seeded = 1;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRandSetTestMode
 *
 * Description        : This function sets the given context in test mode
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandSetTestMode (tUtilPrngCtx * pPrngCtx)
{
    if (pPrngCtx->i4Keyed)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: prng keyed\n");
        return OSIX_FAILURE;
    }
    pPrngCtx->i4TestMode = 1;
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRandSetDt
 *
 * Description        : This function sets the given DT in the static
 *                      PRNG context
 *
 * Input(s)           : pu1DT - Pointer to the DT
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandSetDt (UINT1 *pu1DT)
{
    if (!StatCtx.i4TestMode)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: context not in test mode\n");
        return OSIX_FAILURE;
    }
    MEMCPY (StatCtx.au1DT, pu1DT, UTIL_RAND_AES_BLOCK_LEN);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRandGetDt 
 *
 * Description        : This function fills the DT by getting the system
 *                      time in seconds and nano seconds
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
PRIVATE VOID
UtilRandGetDt (tUtilPrngCtx * pPrngCtx)
{
    tUtlSysPreciseTime  SysPreciseTime;
    UINT1              *pu1Buf = pPrngCtx->au1DT;
    FS_ULONG            u8Pid;

    UtlGetPreciseSysTime (&SysPreciseTime);
    pu1Buf[0] = (UINT1) (SysPreciseTime.u4Sec & 0xff);
    pu1Buf[1] = (UINT1) ((SysPreciseTime.u4Sec >> 8) & 0xff);
    pu1Buf[2] = (UINT1) ((SysPreciseTime.u4Sec >> 16) & 0xff);
    pu1Buf[3] = (UINT1) ((SysPreciseTime.u4Sec >> 24) & 0xff);
    pu1Buf[4] = (UINT1) (SysPreciseTime.u4NanoSec & 0xff);
    pu1Buf[5] = (UINT1) ((SysPreciseTime.u4NanoSec >> 8) & 0xff);
    pu1Buf[6] = (UINT1) ((SysPreciseTime.u4NanoSec >> 16) & 0xff);
    pu1Buf[7] = (UINT1) ((SysPreciseTime.u4NanoSec >> 24) & 0xff);
    pu1Buf[8] = (UINT1) (pPrngCtx->u8Counter & 0xff);
    pu1Buf[9] = (UINT1) ((pPrngCtx->u8Counter >> 8) & 0xff);
    pu1Buf[10] = (UINT1) ((pPrngCtx->u8Counter >> 16) & 0xff);
    pu1Buf[11] = (UINT1) ((pPrngCtx->u8Counter >> 24) & 0xff);

    pPrngCtx->u8Counter++;

    u8Pid = (FS_ULONG) getpid ();
    pu1Buf[12] = (UINT1) (u8Pid & 0xff);
    pu1Buf[13] = (UINT1) ((u8Pid >> 8) & 0xff);
    pu1Buf[14] = (UINT1) ((u8Pid >> 16) & 0xff);
    pu1Buf[15] = (UINT1) ((u8Pid >> 24) & 0xff);
    return;
}

/***************************************************************************
 * Function           : UtilRandSetPrngBytes 
 *
 * Description        : This function generates the random number by 
 *                      calling AES function and fills in the given 
 *                      out buffer
 *
 * Input(s)           : pPrngCtx - Pointer to the PRNG context structure
 *                      OutLen - Required output length
 *                     
 * Output(s)          : pu1Output - Output buffer
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandSetPrngBytes (tUtilPrngCtx * pPrngCtx, UINT1 *pu1Output, size_t OutLen)
{
    UINT1               au1Expect[UTIL_RAND_AES_BLOCK_LEN];
    UINT1               au1Inbuf[UTIL_RAND_AES_BLOCK_LEN];
    UINT1               au1Tmp[UTIL_RAND_AES_BLOCK_LEN];
    INT4                i4Count = 0;

    MEMSET (au1Expect, 0, UTIL_RAND_AES_BLOCK_LEN);
    MEMSET (au1Inbuf, 0, UTIL_RAND_AES_BLOCK_LEN);
    MEMSET (au1Tmp, 0, UTIL_RAND_AES_BLOCK_LEN);

    if (pPrngCtx->i4PrngError)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: prng error\n");
        return OSIX_FAILURE;
    }
    if (!pPrngCtx->i4Keyed)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: no key set\n");
        return OSIX_FAILURE;
    }
    if (!pPrngCtx->i4Seeded)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: not seeded\n");
        return OSIX_FAILURE;
    }
    for (;;)
    {
        if (!pPrngCtx->i4TestMode)
            UtilRandGetDt (pPrngCtx);
        AesArEncrypt (&pPrngCtx->CryptoKey, pPrngCtx->au1DT, au1Inbuf);
        for (i4Count = 0; i4Count < UTIL_RAND_AES_BLOCK_LEN; i4Count++)
            au1Tmp[i4Count] = au1Inbuf[i4Count] ^ pPrngCtx->au1Vect[i4Count];
        AesArEncrypt (&pPrngCtx->CryptoKey, au1Tmp, au1Expect);
        for (i4Count = 0; i4Count < UTIL_RAND_AES_BLOCK_LEN; i4Count++)
            au1Tmp[i4Count] = au1Expect[i4Count] ^ au1Inbuf[i4Count];
        AesArEncrypt (&pPrngCtx->CryptoKey, au1Tmp, pPrngCtx->au1Vect);
        /* Continuous PRNG test */
        if (pPrngCtx->i4PrngSecond)
        {
            if (!MEMCMP (au1Expect, pPrngCtx->au1Last, UTIL_RAND_AES_BLOCK_LEN))
            {
                UTIL_TRC (ALL_FAILURE_TRC, "RNG Err: prng strucked\n");
                return OSIX_FAILURE;
            }
        }
        MEMCPY (pPrngCtx->au1Last, au1Expect, UTIL_RAND_AES_BLOCK_LEN);
        if (!pPrngCtx->i4PrngSecond)
        {
            pPrngCtx->i4PrngSecond = 1;
            if (!pPrngCtx->i4TestMode)
                continue;
        }

        if (OutLen <= UTIL_RAND_AES_BLOCK_LEN)
        {
            MEMCPY (pu1Output, au1Expect, OutLen);
            break;
        }

        MEMCPY (pu1Output, au1Expect, UTIL_RAND_AES_BLOCK_LEN);
        pu1Output += UTIL_RAND_AES_BLOCK_LEN;
        OutLen -= UTIL_RAND_AES_BLOCK_LEN;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRandCleanse 
 *
 * Description        : This function clears or changes the values in the
 *                      given buffer which is used to store the keys
 *
 * Input(s)           : pPtr - Pointer to the buffer to be cleared
 *                      Length - Length of the buffer
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
VOID
UtilRandCleanse (VOID *pPtr, size_t Length)
{
    UINT1              *pu1TmpPtr = pPtr;
    size_t              Loop = Length;
    size_t              Counter = u1CleanseCtr;

    while (Loop--)
    {
        *(pu1TmpPtr++) = (UINT1) Counter;
        Counter += (17 + ((size_t) pu1TmpPtr & 0xF));
    }
    pu1TmpPtr = memchr (pPtr, (UINT1) Counter, Length);
    if (pu1TmpPtr)
    {
        Counter += (63 + (size_t) pu1TmpPtr);
    }
    u1CleanseCtr = (UINT1) Counter;
    return;
}

/***************************************************************************
 * Function           : UtilRandNumGen 
 *
 * Description        : This function sets the key and seed and call the
 *                      function to generate the random number
 *
 * Input(s)           : i4Count - Length of the random number
 *
 * Output(s)          : pu1Output - Output buffer
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
UtilRandNumGen (UINT1 *pu1Output, INT4 i4Count)
{
    tUtilPrngCtx        PrngCtx;
    UINT1               au1Aes128Key[UTIL_RAND_AES128_KEY_LEN];
    UINT1               au1Aes128tv[UTIL_RAND_AES128_KEY_LEN];

    MEMSET (au1Aes128Key, 0, UTIL_RAND_AES_BLOCK_LEN);
    MEMSET (au1Aes128tv, 0, UTIL_RAND_AES_BLOCK_LEN);
    MEMSET (&PrngCtx, 0, sizeof (tUtilPrngCtx));

    FsUtlGetUniqueRandom (au1Aes128Key, UTIL_RAND_AES128_KEY_LEN);
    if (UtilRandSetPrngKey (&PrngCtx, au1Aes128Key,
                            UTIL_RAND_AES128_KEY_LEN) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    FsUtlGetUniqueRandom (au1Aes128tv, UTIL_RAND_AES128_KEY_LEN);
    if ((UtilRandSetPrngSeed (&PrngCtx, au1Aes128tv,
                              UTIL_RAND_AES128_KEY_LEN)) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if ((UtilRandSetPrngBytes (&PrngCtx, pu1Output, (size_t) i4Count)) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : UtilRand 
 *
 * Description        : This function returns a pseudo-random number
 *
 * Input(s)           : None
 *                     
 * Output(s)          : None
 *
 * Returns            : Generated random number or zero in the
 *                      case of failure
 ***************************************************************************/
INT4
UtilRand ()
{
    UINT1               au1Buf[UTIL_RAND_NUM_LEN];
    UINT4               u4Value = 0;
    INT4                i4Count = 0;
    INT4                i4Rand = 0;

    i4Rand = rand ();
    if ((UtilRandNumGen (au1Buf, UTIL_RAND_NUM_LEN)) != OSIX_SUCCESS)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "RAND failed to generate random" "number\n");
        /* Failed to generate random number */
        return 0;
    }
    for (i4Count = 0; i4Count < UTIL_RAND_NUM_LEN; i4Count++)
    {
        u4Value = ((u4Value << 8) | au1Buf[i4Count]);
    }
    i4Rand = (INT4) (u4Value % OSIX_RAND_MAX);
    /* The generated random number should not match with the previously 
     * generated random number - Conditioanal Test(CBIT) as described
     * in FIPS PUB 140-2 */
    if (i4Rand == gi4PrevGenRandomNum)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "Util RNG failed in conditional "
                  "self-test !!!\n");
        return 0;
    }
    gi4PrevGenRandomNum = i4Rand;
    return (i4Rand);
}

#endif /* _UTILRAND_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  utilrand.c                     */
/*-----------------------------------------------------------------------*/
