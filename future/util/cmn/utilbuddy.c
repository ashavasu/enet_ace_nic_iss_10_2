/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilbuddy.c,v 1.1 2015/04/28 12:51:02 siva Exp $
 *
 * Description:
 *
 */

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "arp.h"
#include "fsutil.h"
/*****************************************************************************/
/* Function Name      : UtilCalculateBuddyMemPoolAdjust                      */
/*                                                                           */
/* Description        : This routine is called  to calculate adjustments     */
/*                      required to create Buddy Memory pool                 */
/*                                                                           */
/* Input(s)           : u4MinBlockSize - Minimum block size for Buddy memory */
/*                                       pool                                */
/*                      u4MaxBlkSize   - Maximum Block size for Buddy memory */
/*                                       pool                                */
/*                                                                           */
/* Output(s)          : pu4MinBlkAdjust - Pointer to Calculated Minimum      */
/*                                        Block Size value                   */
/*                      pu4MaxBlkAdjust - Pointer to Calculated Maximum      */
/*                                        Block Size value                   */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
VOID
UtilCalculateBuddyMemPoolAdjust (UINT4 u4MinBlockSize,
                                 UINT4 u4MaxBlockSize,
                                 UINT4 *pu4MinBlkAdjust, UINT4 *pu4MaxBlkAdjust)
{
    UINT4               u4MinInstReqd;
    UINT4               u4MaxInstReqd;
    UINT4               u4Adjust;

    *pu4MinBlkAdjust = 0;
    *pu4MaxBlkAdjust = 0;

    /* Check required for the case where parameter passed are not as expected */
    if (u4MinBlockSize < u4MaxBlockSize)
    {
        u4MinInstReqd = u4MinBlockSize;
        u4MaxInstReqd = u4MaxBlockSize;
    }
    else
    {
        u4MinInstReqd = u4MaxBlockSize;
        u4MaxInstReqd = u4MinBlockSize;
    }

    /* For ex - MaxBlock Size required = 1000                      *
     *          MinBlock Size required = 64                        *
     * This routine would return actual MaxBlkSize and MinBlkSize  *
     * as 1024 and 64 respectively                                 *
     * 64 > 4 and is also divisible by 4 hence MinBlkSize = 64     *
     * MaxBlkSize = 1000 + ( 64 -40 )                              *
     *     Here 40 is arrived by 1000mod64 i.e 40                  *
     * MaxBlkSize = 1024                                           *
     * i.e divisble by 64 and multiple of 4                        */

    /* The above calculation is valid for 32 bit processors only   *
     * The whole calculation needs to be done with 4 changed to 8  *
     * in case of 64 bit processors.
     * */

    /* Minimum Block Size should be atlease 4 */
    if (u4MinInstReqd <= BUDDY_MIN_BLK_SIZE)
    {
        *pu4MinBlkAdjust = BUDDY_MIN_BLK_SIZE;
        /* Maximum Block Size should be atlease 4 */
        if (u4MaxInstReqd <= BUDDY_MIN_BLK_SIZE)
        {
            *pu4MaxBlkAdjust = BUDDY_MIN_BLK_SIZE;
            return;
        }
    }
    else
    {
        /*Minimum Block size should be multiple of 4 */
        u4Adjust = u4MinInstReqd % BUDDY_MIN_BLK_SIZE;
        /*Adjust *pu4MinBlkAdjust to make it multiple of 4 */
        *pu4MinBlkAdjust = u4MinInstReqd - u4Adjust;
    }

    /*Maximum Block size should be multiple of Minimum Block Size */
    u4Adjust = u4MaxInstReqd % (*pu4MinBlkAdjust);

    /*Adjust *pu4MaxBlkAdjust to make it multiple of Minimum block size */
    *pu4MaxBlkAdjust = u4MaxInstReqd + (*pu4MinBlkAdjust - u4Adjust);

    return;
}
