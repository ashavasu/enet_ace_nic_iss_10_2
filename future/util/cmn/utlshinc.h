/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: utlshinc.h,v 1.1 2015/04/28 12:51:02 siva Exp $
 **
 ** Description:
 **     This file contains the exported definitions and  macros of
 **     util module.
 *******************************************************************/


#include "cfa.h"
#include "utilipvx.h"
#include "radius.h"
#include "tacacs.h"
#include "ip.h"
#include "l2iwf.h"

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#include "wssifcapdb.h"
#include "wlchdlr.h"
#include "wsswlan.h"
#include "wssifradiodb.h"
#include "wssmac.h"
#include "capwap.h"
#include "wssifstatdfs.h"
#endif
#if defined (WLC_WANTED)
#include "wssifwlcwlandb.h"
#endif

#if defined (WTP_WANTED)
#include "wssifwtpwlandb.h"
#endif

typedef struct UtlOifList
{
    UINT4  au4UtlOifList[IPIF_MAX_LOGICAL_IFACES+2];
} tUtlOifList;


#include "fsutil.h"
