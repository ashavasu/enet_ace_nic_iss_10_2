/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilrand.h,v 1.2 2017/06/13 13:18:01 siva Exp $ *
 *
 * Description: This file contains definitions required for random
 *              number generator
 *******************************************************************/
#ifndef _UTIL_RAND_H_
#define _UTIL_RAND_H_

#include "aesarinc.h"

#define UTIL_RAND_NUM_LEN         4
#define UTIL_RAND_AES_BLOCK_LEN   16
#define UTIL_RAND_AES128_KEY_LEN  16
#define UTIL_RAND_AES192_KEY_LEN  24
#define UTIL_RAND_AES256_KEY_LEN  32

typedef struct 
{
    INT4           i4Seeded;
    INT4           i4Keyed;
    INT4           i4TestMode;
    INT4           i4PrngSecond;
    INT4           i4PrngError;
    FS_ULONG       u8Counter;
    unArCryptoKey  CryptoKey;
    INT4           i4Vpos;
    /* Temporary storage for key if it equals seed length */
    UINT1          au1TmpKey[UTIL_RAND_AES_BLOCK_LEN];
    UINT1          au1Vect[UTIL_RAND_AES_BLOCK_LEN];
    UINT1          au1DT[UTIL_RAND_AES_BLOCK_LEN];
    UINT1          au1Last[UTIL_RAND_AES_BLOCK_LEN];
} tUtilPrngCtx;

VOID UtilRandPrngReset(tUtilPrngCtx *pPrngCtx);

INT4 UtilRandSetKey(UINT1 *pu1Key, size_t KeyLen);
INT4 UtilRandSeed(CONST VOID *pBuf, size_t Num);
INT4 UtilRandBytes(UINT1 *pu1Out, INT4 i4OutLen);
INT4 UtilRandSetPrngKey(tUtilPrngCtx *pPrngCtx,
                        UINT1 *pu1Key, size_t KeyLen);
INT4 UtilRandSetPrngSeed(tUtilPrngCtx *pPrngCtx,
                         CONST UINT1 *pu1Seed, size_t SeedLen);
INT4 UtilRandSetTestMode(tUtilPrngCtx *pPrngCtx);
INT4 UtilRandSetPrngBytes(tUtilPrngCtx *pPrngCtx,
                          UINT1 *pu1Output, size_t OutLen);

INT4 UtilRandTestMode(VOID);
VOID UtilRandReset(VOID);
INT4 UtilRandSetDt(UINT1 *pu1Dt);
VOID UtilRandCleanse(VOID *pPtr, size_t Length);

INT4 UtilRandNumGen (UINT1 *pu1Out, INT4 i4Count);

#ifndef __UTILRAND__
#define __UTILRAND__
INT4 UtilRand (VOID);
#endif

#endif
