/*
 *  Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutl.c,v 1.2 2015/06/15 06:54:15 siva Exp $
 *
 *  Description: Common utility functions used by all modules
 *
 */
#include "lr.h"
#include "fsutil.h"
#include "utilipvx.h"
#include "dns.h"
#include "arHmac_api.h"

#if 1
/* kishoreky */
PRIVATE UINT4       gu4UtlCrcTable[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,
    0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,
    0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
    0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,
    0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,
    0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,
    0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,
    0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,
    0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,
    0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,
    0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,
    0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,
    0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,
    0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,
    0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,
    0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,
    0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,
    0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

/* These are private macros used only for the CRC32 algorithm calcultion */
#define UTIL_UINT1_MAX  0xFF
#define UTIL_UINT4_MAX  0xFFFFFFFF
#define UTIL_SHIFT_8BITS 8
#define UTIL_NUM_OF_BITS_IN_CRC 32

PRIVATE UINT4 UtilReflectBits PROTO ((UINT4 u4Crc, INT4 i4BitNum));
#endif

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlReadLine                                    */
/*                                                                          */
/*    Description        : This function reads a line from the given file   */
/*                                                                          */
/*    Input(s)           : File descriptor                                  */
/*                                                                          */
/*    Output(s)          : Single line read from the file                   */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/ OSIX_FAILURE                       */
/*                                                                          */
/****************************************************************************/
INT1
FsUtlReadLine (INT4 i4Filefd, INT1 *pi1Buf)
{
    INT2                i2index = 0;
    INT2                i2ReadCount = 0;
    INT1                i1Char = 0;

    while ((i2ReadCount =
            (INT2) FileRead (i4Filefd, (CHR1 *) & i1Char, 1)) == 1)
    {
        i2index++;
        if ('\n' == i1Char)
        {
            *pi1Buf++ = i1Char;
            break;
        }
        if ('\r' == i1Char)
        {
            i1Char = ' ';
        }
        if (i2index >= FS_UTIL_MAX_LINE_LEN)
        {
            continue;
        }
        *pi1Buf++ = i1Char;
    }
    *pi1Buf = '\0';

    if (i2ReadCount == 0)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlEnhReadLine                                 */
/*                                                                          */
/*    Description        : This function reads atmost n-1 characters or a   */ 
/*                         line from the given file to a buffer of size n.  */
/*                         If n is lesser than a line, the rest of the line */
/*                         is read from file but not stored in the buffer   */
/*                         This function is an exact copy of FsUtlReadLine()*/
/*                         except it takes buffer length as third argument  */
/*                                                                          */
/*    Input(s)           : File descriptor i4FileFd                         */
/*                         Empty buffer pi1Buf                              */
/*                         Buffer length u4Len                              */
/*                                                                          */
/*    Output(s)          : Single line or (n-1) char from line              */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/ OSIX_FAILURE                       */
/*                                                                          */
/****************************************************************************/
INT1
FsUtlEnhReadLine (INT4 i4Filefd, INT1 *pi1Buf, UINT4 u4Len)
{
    INT2                i2index = 0;
    INT2                i2ReadCount = 0;
    INT1                i1Char = 0;

    while ((i2ReadCount =
            (INT2) FileRead (i4Filefd, (CHR1 *) & i1Char, 1)) == 1)
    {
        i2index++;
        if ('\n' == i1Char)
        {
            *pi1Buf++ = i1Char;
            break;
        }
        if ('\r' == i1Char)
        {
            i1Char = ' ';
        }
        if (i2index >= ((INT2) (u4Len-1)))
        {
            continue;
        }
        *pi1Buf++ = i1Char;
    }
    *pi1Buf = '\0';

    if (i2ReadCount == 0)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlResolveHostName                             */
/*                                                                          */
/*    Description        : This function resolves the IP Address for a      */
/*                         input Host name                                  */
/*                                                                          */
/*    Input(s)           : pu1HostName - Host Name                          */
/*                                                                          */
/*    Output(s)          : pu4IPAddr - Resolved IP Address                  */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/ OSIX_FAILURE                       */
/*                                                                          */
/****************************************************************************/
INT4
FsUtlResolveHostName (UINT1 *pu1HostName, UINT4 *pu4IPAddr)
{
    tUtlInAddr          UtlInAddr;
    INT4                i4RetVal = OSIX_FAILURE;
#ifdef DNS_WANTED
    tIPvXAddr           IPAddr;
    UINT4               u4Error = 0;

    MEMSET (&IPAddr, 0, sizeof (IPAddr));
#endif

    MEMSET (&UtlInAddr, 0, sizeof (UtlInAddr));

    if (UtlInetAton ((CONST CHR1 *) pu1HostName, &UtlInAddr) != 0)
    {
        *pu4IPAddr = UtlInAddr.u4Addr;
        i4RetVal = OSIX_SUCCESS;
    }
    else
    {
#ifdef DNS_WANTED
        if (DNSResolveIPFromName ((UINT1 *) pu1HostName, &IPAddr, &u4Error) ==
            DNS_SUCCESS)
        {
            MEMCPY (pu4IPAddr, IPAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            i4RetVal = OSIX_SUCCESS;
        }
#endif
    }
    return i4RetVal;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlIPvXResolveHostName                         */
/*                                                                          */
/*    Description        : This function resolves the IP Address for a      */
/*                         input Host name                                  */
/*                                                                          */
/*    Input(s)           : pu1HostName - Host Name                          */
/*                                                                          */
/*    Output(s)          : pu4IPAddr - Resolved IP Address                  */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/ OSIX_FAILURE                       */
/*                                                                          */
/****************************************************************************/
INT4
FsUtlIPvXResolveHostName (UINT1 *pu1QueryName, UINT1 u1IsAsync,
                          tDNSResolvedIpInfo * pResolvedIpInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;
#ifdef DNS_WANTED
    return (DNSApiResolverCacheAndAct (pu1QueryName, u1IsAsync,
                                       pResolvedIpInfo));

#endif
    UNUSED_PARAM (pu1QueryName);
    UNUSED_PARAM (u1IsAsync);
    UNUSED_PARAM (pResolvedIpInfo);
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlEncryptPasswd                               */
/*                                                                          */
/*    Description        : This function is invoked to encrypt the          */
/*                         incoming password                                */
/*                                                                          */
/*    Input(s)           : pi1Passwd - Pointer to Unencrypted Password      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
FsUtlEncryptPasswd (CHR1 * pi1Passwd)
{
    UINT4               u4PasswdLen = 0;
    UINT4               u4Index = 0;

    u4PasswdLen = STRLEN (pi1Passwd);

    for (u4Index = 0; u4Index < u4PasswdLen; u4Index++)
    {
        if (u4Index != (u4PasswdLen - 1))
        {
            pi1Passwd[u4Index] ^= pi1Passwd[u4Index + 1];
        }
        else
        {
            pi1Passwd[u4Index] ^= FS_UTIL_SPL_CHAR;
        }
        if (pi1Passwd[u4Index] == '\n')
        {
            pi1Passwd[u4Index] = FS_UTIL_SPL_CHAR;
        }
        if (pi1Passwd[u4Index] == '\0')
        {
            pi1Passwd[u4Index] = (FS_UTIL_SPL_CHAR + 1);
        }
    }
    pi1Passwd[u4Index] = '\0';
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsUtlDecryptPasswd                               */
/*                                                                          */
/*    Description        : This function is invoked to decrypt the          */
/*                         incoming password                                */
/*                                                                          */
/*    Input(s)           : pi1Passwd - Pointer to Encrypted Password        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
FsUtlDecryptPasswd (CHR1 * pi1Passwd)
{
    UINT4               u4PasswdLen = 0;
    UINT4               u4Index = 0;

    u4PasswdLen = STRLEN (pi1Passwd);

    for (u4Index = (u4PasswdLen - 1); u4Index < u4PasswdLen; u4Index--)
    {
        if (pi1Passwd[u4Index] == FS_UTIL_SPL_CHAR)
        {
            pi1Passwd[u4Index] = '\n';
        }
        if (pi1Passwd[u4Index] == (FS_UTIL_SPL_CHAR + 1))
        {
            pi1Passwd[u4Index] = '\0';
        }
        if (u4Index != (u4PasswdLen - 1))
        {
            pi1Passwd[u4Index] ^= pi1Passwd[u4Index + 1];
        }
        else
        {
            pi1Passwd[u4Index] ^= FS_UTIL_SPL_CHAR;
        }
    }
    pi1Passwd[u4PasswdLen] = '\0';
    return;
}

/*
 *  * ---------------------------------------------------------------
 *   Function Name : FsUtlGetRandom
 *   Description   : Function to generate seed for random number 
                     generation.
 *   Input(s)      : pu1Rand - pointer where the random number
 *                             has to be stored.
 *                   u4RandLen  - Required length of random number.
 *    Output(s)     :seed for Random number generation 
                     of required length.
 *     Return Values : None.
 * --------------------------------------------------------*/

VOID
FsUtlGetRandom (UINT1 *pu1Rand, UINT4 u4RandLen)
{
    UINT4               u4Temp;
    UINT1              *pu1Temp = pu1Rand;

    while (u4RandLen > 4)
    {
        u4Temp = (UINT4) rand ();
        MEMCPY (pu1Temp, &u4Temp, 4);
        pu1Temp += 4;
        u4RandLen -= 4;
    }

    if (u4RandLen > 0)
    {
        u4Temp = (UINT4) rand ();
        MEMCPY (pu1Temp, &u4Temp, u4RandLen);
    }
}

/*
 *  * ---------------------------------------------------------------
 *   Function Name : FsUtlGetUniqueRandom
 *   Description   : Function to generate random number compliant to 
 *                   FIPS to ensure more randomness and uniqueness.
 *   Input(s)      : pu1Rand - pointer where the random number
 *                             has to be stored.
 *                   u4RandLen  - Required length of random number.
 *   Output(s)     : Random number of required length.
 *   Return Values : None.
 * --------------------------------------------------------*/
VOID
FsUtlGetUniqueRandom (UINT1 *pu1Rand, UINT4 u4RandLen)
{
    UINT1              *pu1Temp = pu1Rand;
    UINT1               au1RandPlainText[RAND_SEED_LEN];
    UINT1               au1RandKeyHmac[RAND_SEED_LEN];
    UINT1               au1RandDigest[RAND_GEN_LEN];

    while (u4RandLen > 32)
    {
        MEMSET (au1RandPlainText, 0, RAND_SEED_LEN);
        MEMSET (au1RandKeyHmac, 0, RAND_SEED_LEN);
        MEMSET (au1RandDigest, 0, RAND_GEN_LEN);
        FsUtlGetRandom (au1RandPlainText, RAND_SEED_LEN);
        FsUtlGetRandom (au1RandKeyHmac, RAND_SEED_LEN);
        arHmacSha2 (2, au1RandPlainText, RAND_SEED_LEN, au1RandKeyHmac,
                    RAND_SEED_LEN, au1RandDigest);
        MEMCPY (pu1Temp, au1RandDigest, 32);
        pu1Temp += 32;
        u4RandLen -= 32;
    }
    if (u4RandLen > 0)
    {
        MEMSET (au1RandPlainText, 0, RAND_SEED_LEN);
        MEMSET (au1RandKeyHmac, 0, RAND_SEED_LEN);
        MEMSET (au1RandDigest, 0, RAND_GEN_LEN);
        FsUtlGetRandom (au1RandPlainText, RAND_SEED_LEN);
        FsUtlGetRandom (au1RandKeyHmac, RAND_SEED_LEN);
        arHmacSha2 (2, au1RandPlainText, RAND_SEED_LEN, au1RandKeyHmac,
                    RAND_SEED_LEN, au1RandDigest);
        MEMCPY (pu1Temp, au1RandDigest, u4RandLen);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilMemMove                                      */
/*                                                                          */
/*    Description        : An Implementation of memmove function.           */
/*                                                                          */
/*    Input(s)           : pDest - Destination buffer to which the output   */
/*                                 should be copied                         */
/*                         pSrc  - Source buffer to be copied               */
/*                         u4Len - Limit of bytes to be copied              */
/*                                                                          */
/*    Output(s)          : Copied buffer in pDst                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
VOID
UtilMemMove (VOID *pDst, CONST VOID *pSrc, UINT4 u4Len)
{
    /* If the destination address is greater than the source address, 
     * then copy back to front to avoid overwriting the data to be copied.
     */
    if ((pDst == NULL) || (pSrc == NULL))
    {
        return;
    }
    if ((FS_ULONG) pSrc < (FS_ULONG) pDst)
    {
        while (u4Len > 0)
        {
            ((CHR1 *) pDst)[u4Len - 1] = ((CONST CHR1 *) pSrc)[u4Len - 1];
            u4Len--;
        }
    }
    /* If the source address is greater than the destination address,
     * then there is no chance of overwriting the content to be copied. 
     * So memcpy can be used to copy the content
     */
    else
    {
        MEMCPY (pDst, pSrc, u4Len);
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilStrtoul                                      */
/*                                                                          */
/*    Description        : An Implementation of strtoul function.           */
/*                                                                          */
/*    Input(s)           : pc1Str     - Pointer to the string which needs to*/
/*                                      be converted into a unsigned long   */
/*                                      value                               */
/*                         i4Base     - Base value to be used for the       */
/*                                      conversion process                  */
/*                                                                          */
/*    Output(s)          : ppc1EndPtr - Starting pointer of illegal         */
/*                                      character from the string pc1Str    */
/*                                                                          */
/*    Returns            : Converted unsigned long value of string pc1Str   */
/*                                                                          */
/****************************************************************************/
FS_ULONG
UtilStrtoul (CHR1 * pc1Str, CHR1 ** ppc1EndPtr, INT4 i4Base)
{
    CHR1               *pc1TempStr = NULL;
    FS_ULONG            u1RetVal = 0;
    FS_ULONG            ulCutOff = 0;
    INT4                i4Char = 0;
    INT4                i4IsNeg = 0;
    INT4                i4IsOverFlow = 0;
    INT4                i4CutOffLimit = 0;

    if (pc1Str == NULL)
    {
        return u1RetVal;
    }

    pc1TempStr = pc1Str;

    /* Skip the white spaces if any at the beginning and pick up the 
     * leading +/- sign if any 
     */
    while (*pc1TempStr != '\0')
    {
        i4Char = *pc1TempStr++;
        if (ISSPACE (i4Char))
        {
            continue;
        }
        else
        {
            if (i4Char == '-')
            {
                i4IsNeg = 1;
                i4Char = *pc1TempStr++;
            }
            else if (i4Char == '+')
            {
                i4Char = *pc1TempStr++;
            }
            break;
        }
    }

    /* If base is 16, and if the initial part of the string contains 
     * "0x" or "0X", skip this 2 bytes and start processing from the 
     * actual data.
     *
     * If base is 0,
     * i)   Check whether the initial part of the string contains 
     *      "0x" or "0X". If so set the base as 16 and and skip 2 bytes for 
     *      start processing from the actual data.
     * ii)  Else check whether the first byte contains '0', if so set the 
     *      base as 8.
     * iii) Else assume the base as 10.
     */
    if ((i4Base == 16) || (i4Base == 0))
    {
        if ((i4Char == '0') && ((*pc1TempStr == 'x') || (*pc1TempStr == 'X')))
        {
            i4Base = 16;
            pc1TempStr++;
            i4Char = *pc1TempStr++;
        }
        else if ((i4Base == 0) && (i4Char == '0'))
        {
            i4Base = 8;
        }
        else if ((i4Base == 0) && (i4Char != '0'))
        {
            i4Base = 10;
        }
    }

    /*
     * Compute the CutOff value (i.e) largest legal value divided by the base
     * and the CutOffLimit value.
     */

    ulCutOff = FS_ULONG_MAX / (FS_ULONG) i4Base;
    i4CutOffLimit = (INT4) (FS_ULONG_MAX % (FS_ULONG) i4Base);

    do
    {
        if (ISALPHA (i4Char))
        {
            if (ISUPPER (i4Char))
            {
                i4Char = i4Char - ('A' - 10);
            }
            else
            {
                i4Char = i4Char - ('a' - 10);
            }
        }
        else if (ISDIGIT (i4Char))
        {
            i4Char = i4Char - '0';
        }
        else
        {
            break;
        }

        /* If the actual value of any of the character in the given string is 
         * greater than the base value, it is considered as the start of
         *  invalid string. So break the loop and return.
         */
        if (i4Char >= i4Base)
        {
            break;
        }

        /* If the already converted value is greater than the CutOff value 
         * and if the string is followed by a legal input character 
         * (i.e) value less than the base value, then it is considered as 
         * overflow.
         *
         * If the already converted value is equal to the CutOff value, 
         * it may be valid or not depends on the following case.
         *
         *  i)  If the string is followed by a legal input character 
         *      (i.e) value less than the base value, but it is greater than 
         *      the CutOffLimit, then it will be considered as overflow
         *
         *  ii) If the string is followed by a legal input character and it is 
         *      less than the CutOffLimit then it is valid.
         *
         * To indicate overflow set the IsOverFlow bit to 1.
         */

        if ((u1RetVal > ulCutOff) ||
            ((u1RetVal == ulCutOff) && (i4Char > i4CutOffLimit)) ||
            (i4IsOverFlow))
        {
            i4IsOverFlow = 1;
        }
        else
        {
            u1RetVal = (UINT1) (u1RetVal * (UINT4) i4Base);
            u1RetVal = (UINT1) (u1RetVal + (UINT4) i4Char);
        }
        i4Char = *pc1TempStr++;

    }
    while (i4Char != '\0');

    if (i4IsOverFlow)
    {
        u1RetVal = FS_ULONG_MAX;
    }
    if (i4IsNeg)
    {
        u1RetVal = -u1RetVal;
    }
    if (ppc1EndPtr != NULL)
    {
        *ppc1EndPtr = pc1TempStr - 1;
    }
    return u1RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtilStrcspn                                      */
/*                                                                          */
/*    Description        : An Implementation of strcspn function.           */
/*                                                                          */
/*    Input(s)           : pc1Str    - pointer to the string whose length   */
/*                                     has to be calculated which consists  */
/*                                     entirely of characters not in        */
/*                                     pc1Reject                            */
/*                         pc1Reject - pointer to the string which is used  */
/*                                     as reference to calculate the length */
/*                                     of the string pc1Str                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Number of characters in the initial segment of   */
/*                         pc1Str which are not in the string reject.       */
/*                                                                          */
/****************************************************************************/
INT4
UtilStrcspn (CHR1 * pc1Str, CHR1 * pc1Reject)
{
    CHR1               *pc1TempStr = NULL;
    CHR1               *pc1TempReject = NULL;
    INT4                i4Len = 0;

    if ((pc1Str != NULL) && (pc1Reject != NULL))
    {
        pc1TempStr = pc1Str;

        while (*pc1TempStr != '\0')
        {
            pc1TempReject = pc1Reject;

            while (*pc1TempReject != '\0')
            {
                /* Return immediately if any character from pc1Str matches a 
                 * character from pc1Reject. */
                if (*pc1TempReject == *pc1TempStr)
                {
                    return i4Len;
                }
                pc1TempReject++;
            }

            i4Len++;
            pc1TempStr++;
        }
    }
    return i4Len;
}

/*******************************************************************************
 * Function Name      : UtilReflectBits
 *
 * Description        : This routine is used to reflect the input bits used in
 *                      crc 32 calculation.
 *                        
 * Input(s)           : u4Crc - CRC bit to be reflected
 *                      i4BitNum - number of bits in crc
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : CRC after reflecting
 ******************************************************************************/
PRIVATE UINT4
UtilReflectBits (UINT4 u4Crc, INT4 i4BitNum)
{
    UINT4               u4bitshift = 1;
    UINT4               u4bitmask = 1;
    UINT4               u4Crcout = 0;

    for (u4bitshift = (UINT4) 1 << (i4BitNum - 1); u4bitshift; u4bitshift >>= 1)
    {
        if (u4Crc & u4bitshift)
        {
            u4Crcout |= u4bitmask;
        }
        u4bitmask <<= 1;
    }
    return (u4Crcout);
}

/*******************************************************************************
 * Function Name      : UtilCalculateCRC32
 *
 * Description        : This routine is used to calculate the Crc32 of the input 
 *                      data
 *                        
 * Input(s)           : pData - data on which crc calculation to be performed 
 *                      u4Len - Length of the data 
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC UINT4
UtilCalculateCRC32 (UINT1 *pData, UINT4 u4Len)
{
    UINT4               u4Crc = UTIL_UINT4_MAX;

    u4Crc = UtilReflectBits (u4Crc, UTIL_NUM_OF_BITS_IN_CRC);
    while (u4Len--)
    {
        u4Crc =
            (u4Crc >> UTIL_SHIFT_8BITS) ^
            gu4UtlCrcTable[(u4Crc & UTIL_UINT1_MAX) ^ *pData++];
    }

    u4Crc ^= UTIL_UINT4_MAX;
    u4Crc &= UTIL_UINT4_MAX;
    return u4Crc;
}

/*******************************************************************************
 * Function Name      : UtilStrToLower
 *
 * Description        : This routine is used to convert the given string into
 *                      lower case
 *
 * Input(s)           : pu1Str - input string
 *
 * Output(s)          : None
 *
 * Return Value(s)    : pu1Str - converted lower case string.
 ******************************************************************************/
PUBLIC UINT1
UtilStrToLower (UINT1 *pu1Str)
{
    UINT4               u4Len = 0;
    UINT1               u1temp[DNS_MAX_QUERY_LEN];
    MEMSET (u1temp, 0, DNS_MAX_QUERY_LEN);

    /*if the received host name has trailing characters, ignored here*/
    if (pu1Str[(STRLEN (pu1Str) -1)] == '.')
    {
        pu1Str[(STRLEN (pu1Str) -1)] = '\0';
    }

    while (pu1Str[u4Len])
    {
        u1temp[u4Len] = (UINT1) TOLOWER (pu1Str[u4Len]);
        u4Len++;
    }

    MEMCPY (pu1Str, u1temp, u4Len);

    return *pu1Str;
}
