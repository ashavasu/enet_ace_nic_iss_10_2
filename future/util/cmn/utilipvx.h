/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilipvx.h,v 1.8 2017/11/14 07:31:19 siva Exp $
 * 
 * Description: This file contains macros definitions for constants
 *              used in IPv6.
 *********************************************************************/
#ifndef _UTILIPVX_H
#define _UTILIPVX_H

#define IPVX_IPV4_ADDR_LEN   4
#define IPVX_IPV6_ADDR_LEN   16

#define IPVX_ADDR_FMLY_IPV4   1
#define IPVX_ADDR_FMLY_IPV6   2
#define IPVX_DNS_FAMILY       16

#define IPVX_IPV4_HOST_MASK  0xffffffff
#define IPVX_IPV4_MAX_MASK_LEN  32

#define IPVX_ZERO 0
#define IPVX_ONE 1

#define IPVX_NULL (0)
        
/*Currently IPVX_MAX_INET_ADDR_LEN is assigned to IPVX_IPV6_ADDR_LEN which is
 16,in future based on additional Addr Family support and its size , this macro
 needs to be changed accordingly*/
#define IPVX_MAX_INET_ADDR_LEN  IPVX_IPV6_ADDR_LEN


/*This is the comman Address structure to support different IP ADDR FAMILY types */
#ifndef PACK_REQUIRED
#pragma pack(1)
#endif

#ifndef MAC_ADDR_LEN
#define MAC_ADDR_LEN 6
#endif

typedef struct _IPvXAddr {
        UINT1           u1Afi;
        UINT1           u1AddrLen;      
        UINT2           u2Pad;
        UINT1           au1Addr[IPVX_MAX_INET_ADDR_LEN];
}tIPvXAddr;     

typedef struct _VrrpNwIntf
{
    /* IP Address to be given VRRP capability.
     * u1Afi in this structure denote the IP Address Family IPv4 or IPv6
     * u1AddrLen in this structure denote IP Address Length 4 or 6. */
    tIPvXAddr IpvXAddr; 

    /* Interface IP Address */
    tIPvXAddr IntfAddr;

    /* Virtual IP - This is the IP Addresses for which ARP requests are to
     * be responded. */
    tIPvXAddr VirtualIp;

    /* Check IP - IP to be checked. */
    tIPvXAddr CheckIp;

    /* IP Interface Index over which VRRP capability is required. */
    UINT4     u4IfIndex;

    /* Linux IP Virtual Port Number for this VRID */
    UINT4     u4LnxIpPortNum;

    /* IP Port Number */
    UINT4     u4Port;

    /* Virtual Router Identifier */
    UINT4     u4VrId;

    /* Virtual MAC Address. Last Octet denotes VRID */
    UINT1     au1MacAddr[MAC_ADDR_LEN];
    UINT4     u4VcNum;  /* VRF ID */

    /* VLAN Id value. For router port this will be zero. */
    UINT2     u2VlanId;

    /* Action required for doing VRRP Network Interface Action. 
     *    VRRP_NW_INTF_CREATE (1)           - To create VRRP capability for 
     *                                        Primary Address.
     *    VRRP_NW_INTF_SECONDARY_CREATE (2) - To create VRRP capability for
     *                                        Secondary Address.
     *    VRRP_NW_INTF_DELETE (3)           - To delete VRRP capability for
     *                                        Primary Address.
     *    VRRP_NW_INTF_SECONDARY_DELETE (4) - To delete VRRP capability for
     *                                        Secondary Address.
     *    VRRP_NW_INTF_MCAST_CREATE (5)     - To create solicited Node Multicast
     *                                        IP Address. Only applicable for
     *                                        IPv6.
     *    VRRP_NW_INTF_MCAST_DELETE (6)     - To delete solicited Node Multicast
     *                                        IP Address. Only applicable for
     *                                        IPv6.
     */
    UINT1     u1Action;

    /* Address Type */
    UINT1     u1AddrType;
 
    /* Is Owner Flag */
    BOOL1     b1IsIpvXOwner;

    /* Is Owner Present */
    BOOL1     b1IsOwnerPresent;

    /* Accept Mode */
    UINT1     u1AcceptMode;

    /* Secondary Index */
    UINT1     u1SecIndex;

    /* Boolean flag to indicate accept mode configuration is done */
    BOOL1     b1IsAcceptConf;

    /* Boolean flag to indicate if changing network action is required.
     * This flag is used in NetIp Get API for Linux IP alone to modify 
     * the Nw Action. */
    BOOL1     b1IsChgNwActionReqd;

    /* Boolean flag to indicate if comparing IP's are required. This flag
     * is used in NetIp Get API for Linux IP alone to compare IP's */
    BOOL1     b1IsCheckIpReqd;

    /* Flag to indicate if network action is changed or not.
     * In case of Linux IP, this is set to TRUE if network action change is
     * required.
     *
     * In case of Aricent IP, this is unused.
     */
    BOOL1     b1IsNwActionChgd;

    /* Padding */
    UINT1     au1Pad[3];
} tVrrpNwIntf;


#ifndef PACK_REQUIRED
#pragma pack ()
#endif


/* Copies the entire ADDR2 contents to ADDR1, both ADDR1 and ADDR2 are of type ttIPvXAddr */
#define IPVX_ADDR_COPY(pADDR1,pADDR2)\
        MEMCPY((pADDR1),(pADDR2),sizeof(tIPvXAddr))

#define IPVX_ADDR_CLEAR(pADDR)\
        MEMSET((pADDR),0,sizeof(tIPvXAddr))

#define IPVX_ADDR_INIT(ADDR1,u1AFi,pu1Addr)\
{\
    MEMSET(&(ADDR1),0,sizeof (tIPvXAddr) );\
    (ADDR1).u1Afi = (UINT1)(u1AFi);\
    if( (u1AFi) == IPVX_ADDR_FMLY_IPV4 ) {\
        (ADDR1).u1AddrLen = (UINT1)IPVX_IPV4_ADDR_LEN;\
        if((VOID *) (pu1Addr) != NULL)\
            MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV4_ADDR_LEN);\
        }\
    if( (u1AFi) == IPVX_ADDR_FMLY_IPV6) {\
        (ADDR1).u1AddrLen = (UINT1)IPVX_IPV6_ADDR_LEN;\
        if((VOID *) (pu1Addr) != NULL)\
            MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV6_ADDR_LEN);\
        }\
}

#define IPVX_ADDR_INIT_VAR(ADDR1,u1AFi,pu1Addr)\
{\
   MEMSET(&(ADDR1),0,sizeof (tIPvXAddr) );\
   (ADDR1).u1Afi = (UINT1)(u1AFi);\
   if( (u1AFi) == IPVX_ADDR_FMLY_IPV4 ) {\
     (ADDR1).u1AddrLen = (UINT1)IPVX_IPV4_ADDR_LEN;\
     MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV4_ADDR_LEN);\
     }\
   if( (u1AFi) == IPVX_ADDR_FMLY_IPV6) {\
     (ADDR1).u1AddrLen = (UINT1)IPVX_IPV6_ADDR_LEN;\
     MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV6_ADDR_LEN);\
     }\
}

#define IPVX_ADDR_INIT_FROMV4(ADDR1,u4Addr)\
{\
        MEMSET(&(ADDR1),0,sizeof(tIPvXAddr));\
        (ADDR1).u1Afi = IPVX_ADDR_FMLY_IPV4;\
        (ADDR1).u1AddrLen = IPVX_IPV4_ADDR_LEN;\
        MEMCPY((ADDR1).au1Addr, (VOID *)&u4Addr, IPVX_IPV4_ADDR_LEN);\
}
        
#define IPVX_ADDR_INIT_FROMV6(ADDR1,pIp6Addr)\
{\
    MEMSET(&(ADDR1),0,sizeof(tIPvXAddr));\
        (ADDR1).u1Afi = IPVX_ADDR_FMLY_IPV6;\
    (ADDR1).u1AddrLen = IPVX_IPV6_ADDR_LEN;\
    if((pIp6Addr) != NULL) MEMCPY((ADDR1).au1Addr,(pIp6Addr),IPVX_IPV6_ADDR_LEN);\
}

#define IPVX_ADDR_INIT_FROMV6_VAR(ADDR1,pIp6Addr)\
{\
    MEMSET(&(ADDR1),0,sizeof(tIPvXAddr));\
    (ADDR1).u1Afi = IPVX_ADDR_FMLY_IPV6;\
    (ADDR1).u1AddrLen = IPVX_IPV6_ADDR_LEN;\
    MEMCPY((ADDR1).au1Addr,(pIp6Addr),IPVX_IPV6_ADDR_LEN);\
}

#define IPVX_ADDR_INIT_IPV4(ADDR1,u1AFi,pu1Addr)\
{\
    MEMSET(&(ADDR1), 0, sizeof (tIPvXAddr));\
        (ADDR1).u1Afi=(u1AFi);\
        {\
          (ADDR1).u1AddrLen=IPVX_IPV4_ADDR_LEN;\
          if ((pu1Addr) != NULL)\
            MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV4_ADDR_LEN);\
        }\
}

#define IPVX_ADDR_INIT_IPV4_VAR(ADDR1,u1AFi,pu1Addr)\
{\
    MEMSET(&(ADDR1), 0, sizeof (tIPvXAddr));\
        (ADDR1).u1Afi=(u1AFi);\
        (ADDR1).u1AddrLen=IPVX_IPV4_ADDR_LEN;\
        MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV4_ADDR_LEN);\
}

#define IPVX_ADDR_INIT_IPV6(ADDR1,u1AFi,pu1Addr)\
{\
    MEMSET(&(ADDR1), 0, sizeof (tIPvXAddr));\
        (ADDR1).u1Afi=(u1AFi);\
        {\
          (ADDR1).u1AddrLen=IPVX_IPV6_ADDR_LEN;\
          if ((pu1Addr) != NULL)\
            MEMCPY((ADDR1).au1Addr,(pu1Addr),IPVX_IPV6_ADDR_LEN);\
        }\
}


/* Compares the au1Addr contents in ADDR1 and ADDR2 , both ADDR1 and ADDR2 are 
 * of type tIPvXAddr */
#define IPVX_ADDR_COMPARE(ADDR1,ADDR2)\
        MEMCMP(ADDR1.au1Addr,ADDR2.au1Addr,\
           MEM_MAX_BYTES((INT4)ADDR1.u1AddrLen,IPVX_MAX_INET_ADDR_LEN))

#define GET_IPV6_MASK(u1PrefixLen, pu4Mask)\
{ \
    UINT1     cnt1, cnt2, cnt3; \
\
    (*(pu4Mask + 0)) = 0x80000000; \
\
    for (cnt1 = 1, cnt2 = 1, cnt3 = 0; ((cnt1 < u1PrefixLen) && (cnt3 < 4)); \
         cnt1++, cnt2++) \
    { \
        (*(pu4Mask + cnt3)) |= ((*(pu4Mask + cnt3)) >> 1); \
        if (cnt2 == 31) \
        { \
            cnt2 = 0; \
            cnt3++; \
            cnt1++; \
\
            if ((cnt1 == u1PrefixLen)|| (cnt3 == 4)) \
                break; \
\
            (*(pu4Mask + cnt3)) = 0x80000000; \
        } \
    }\
}

/* convert IPv4 mask to mask length */
#define IPV4_MASK_TO_MASKLEN(masklen, mask)  \
{ \
    UINT4 u4TmpMask  = (mask); \
    UINT4 u4TmpMasklen = 32;      \
    for ( ; u4TmpMasklen > 0; u4TmpMasklen--, u4TmpMask >>= 1) { \
         if (u4TmpMask & 0x1) { \
             break; \
         } \
    }\
    (masklen) = (UINT1) u4TmpMasklen; \
}

/* convert mask length to IPv4 mask */
#define IPV4_MASKLEN_TO_MASK(mask,masklen) \
{\
    if (masklen == 0) { \
        (mask) = 0; \
    } \
    else { \
        (mask) = (0xFFFFFFFF << (32 - (masklen)));\
    } \
}

/* get address length by family */
#define IPVX_ADDR_FMLY_TO_LEN(len , t) \
{\
    if( (t) == IPVX_ADDR_FMLY_IPV4 )\
    {\
        (len) = IPVX_IPV4_ADDR_LEN;\
    }\
    else if( (t) == IPVX_ADDR_FMLY_IPV6 )\
    {\
        (len) = IPVX_IPV6_ADDR_LEN;\
    }\
    else\
    {\
        (len) = 0;\
    }\
}

/* Copy IPvx Address to Octet String */
#define IPVX_IPVX_ADDR_TO_OCT_STR(pIpvxAddr,pOctStr) \
{\
        MEMCPY((pOctStr)->pu1_OctetList,(pIpvxAddr)->au1Addr, \
                          (pIpvxAddr)->u1AddrLen); \
        (pOctStr)->i4_Length = (pIpvxAddr)->u1AddrLen; \
}

/* get address length by family */
#define IPVX_ADDR_LEN_FROM_FAMILY(t) \
{\
    ( ( (t) == IPVX_ADDR_FMLY_IPV4)? IPVX_IPV4_ADDR_LEN : \
          ( (t) == IPVX_ADDR_FMLY_IPV6) ? IPVX_IPV6_ADDR_LEN : \
                IPVX_IPV4_ADDR_LEN ); \
}

#endif /* _UTILIPVX_H */
