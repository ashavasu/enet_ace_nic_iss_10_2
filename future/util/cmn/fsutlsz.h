#ifndef _FSUTLSZ_H
#define _FSUTLSZ_H
#include "osxstd.h"

/* Memory Sizing structure */
typedef struct sizing_params {
    CHR1   StructName[128];
    CHR1   SizingParamId[128];
    UINT4  u4StructSize;
    UINT4  u4PreAllocatedUnits;
    UINT4  u4DefaultUnits;
    UINT4  u4MemPreAllocated;
} tFsModSizingParams;

typedef struct ModSizingInfo {
    CHR1                     ModName[64];
    tFsModSizingParams       *ModSizingParams;
    UINT4                    u4ModMemPreAllocated;
    UINT1                    au1Reserved[4]; /* This 4 byte padding is added 
                                                for 64 bit padding issue. 
                                                Always add pointers together
                                                as they occupy different 
                                                number of bytes in various 
                                                architecture. */
} tFsModSizingInfo;


UINT4 FsUtlSzCalculateModulePreAllocatedMemory (tFsModSizingInfo *pModSizingInfo);

#endif
