/********************************************************************/
/*  Copyright (C) 2006 Aricent Inc . All Rights Reserved            */
/*                                                                  */
/*  $Id: utlshmem.c,v 1.1 2015/04/28 12:51:02 siva Exp $            */
/*                                                                  */
/*  Description: This file contains the init and other functions    */
/*               common for memory allocation and freeing.          */
/*                                                                  */
/********************************************************************/

#ifndef __UTLSHMEM_C__
#define __UTLSHMEM_C__

#include "utlshinc.h"
UINT4               gu4TacMsgOutputPoolId;
UINT4               gu4TacMsgInputPoolId;
UINT4               gu4RadIfPoolId;
UINT4               gu4UtlOifPoolId;
UINT1               gu1UtlDataLenPoolId;
UINT4               gu4L2FramePoolId;
UINT4               gu4UtlVlanListPoolId;
UINT1               gWebAllocBufferPoolId;
UINT4               gu4RadKeyBlocksPoolId;
#if defined(WLC_WANTED) || defined (WTP_WANTED)
UINT4               gu4UtlWlcPoolId;
UINT4               gu4UtlWssWlanPoolId;
UINT4               gu4AntennaSelectionPoolId;
UINT4               gu4WlanDbPoolId;
UINT4               gu4UtilMacMsgPoolId;
UINT4               gu4capwapPktCountId;
UINT4               gu4ConfigStatusRspPoolId;
UINT4               gu4WssIfAuthDbPoolId;
UINT4               gu4AcInfoAfterDiscoveryPoolId;
UINT4               gu4JoinReqPoolId;
UINT4               gu4DiscRspPoolId;
UINT4               gu4DataReqPoolId;
UINT4               gu4JoinRspPoolId;
UINT4               gu4ConfigStatusReqPoolId;
#endif

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemInitPoolIds                                */
/*                                                                            */
/*    Description        : This function initializes the common memory pools  */
/*                         utilised by Radius,Tacacs and the other modules    */
/*                         which makes use of OIfList interdependently.       */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None                                               */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : None                                               */
/******************************************************************************/

VOID
UtlShMemInitPoolIds ()
{
    gu4TacMsgOutputPoolId = UTILMemPoolIds[MAX_TAC_MSG_OUTPUT_SIZING_ID];
    gu4TacMsgInputPoolId = UTILMemPoolIds[MAX_PNAC_TAC_MSG_INPUT_SIZING_ID];
    gu4RadIfPoolId = UTILMemPoolIds[MAX_RAD_INTERFACE_ENTRIES_SIZING_ID];
    gu4UtlOifPoolId = UTILMemPoolIds[MAX_CMN_OIF_BLOCKS_SIZING_ID];
    gu1UtlDataLenPoolId = (UINT1) UTILMemPoolIds[MAX_UTIL_DATA_LEN_SIZING_ID];
    gu4L2FramePoolId = UTILMemPoolIds[MAX_L2IWF_DUPL_BUF_SIZE_SIZING_ID];
    gu4UtlVlanListPoolId = UTILMemPoolIds[MAX_VLAN_LIST_COUNT_SIZING_ID];
    gWebAllocBufferPoolId =
        (UINT1) UTILMemPoolIds[MAX_WEB_BUFFER_LEN_SIZING_ID];
    gu4RadKeyBlocksPoolId = UTILMemPoolIds[MAX_UTIL_RAD_KEY_BLOCKS_SIZING_ID];
#if defined(WLC_WANTED) || defined (WTP_WANTED)
    gu4UtlWlcPoolId = UTILMemPoolIds[MAX_WLC_BUF_SIZE_SIZING_ID];
    gu4UtlWssWlanPoolId = UTILMemPoolIds[MAX_WSS_WLAN_BUF_SIZE_SIZING_ID];
    gu4AntennaSelectionPoolId =
        UTILMemPoolIds[MAX_UTIL_ANTENNA_SELECTION_SIZING_ID];
    gu4WlanDbPoolId = UTILMemPoolIds[MAX_WLAN_DB_BUF_SIZE_SIZING_ID];
    gu4UtilMacMsgPoolId =
        UTILMemPoolIds[MAX_WSS_MAC_MSG_STRUCT_BUF_SIZE_SIZING_ID];
    gu4capwapPktCountId = UTILMemPoolIds[MAX_CAPWAP_PKT_COUNT_SIZING_ID];
    gu4ConfigStatusRspPoolId =
        UTILMemPoolIds[MAX_CONFIG_STATUS_RSP_BUF_SIZE_SIZING_ID];
    gu4WssIfAuthDbPoolId = UTILMemPoolIds[MAX_WSSIF_AUTH_DB_BUF_SIZE_SIZING_ID];
    gu4AcInfoAfterDiscoveryPoolId =
        UTILMemPoolIds[MAX_AC_INFO_BUF_SIZE_SIZING_ID];
    gu4JoinReqPoolId = UTILMemPoolIds[MAX_JOIN_REQ_BUF_SIZE_SIZING_ID];
    gu4DiscRspPoolId = UTILMemPoolIds[MAX_DISC_RSP_BUF_SIZE_SIZING_ID];
    gu4DataReqPoolId = UTILMemPoolIds[MAX_DATA_REQ_BUF_SIZE_SIZING_ID];
    gu4JoinRspPoolId = UTILMemPoolIds[MAX_JOIN_RSP_BUF_SIZE_SIZING_ID];
    gu4ConfigStatusReqPoolId =
        UTILMemPoolIds[MAX_CONFIG_STATUS_REQ_BUF_SIZE_SIZING_ID];
#endif
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocTacMsgOutput                          */
/*                                                                            */
/*    Description        : This function allocates memory blocks for the      */
/*                         specified memory pool of TacMsgOutput.             */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to the structure tTacMsgOutput.            */
/******************************************************************************/

UINT1              *
UtlShMemAllocTacMsgOutput ()
{
    UINT1              *pBuf;
    pBuf = MemAllocMemBlk (gu4TacMsgOutputPoolId);
    return pBuf;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeTacMsgOutput                         */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     TacmessageOutput.                                      */
/*                                                                            */
/* Input(s)          : pTacMsgOutput - Pointer to the structure tTacMsgOutput */
/*                                                                            */
/* Outpu(s)          : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeTacMsgOutput (UINT1 *pTacMsgOutput)
{
    MemReleaseMemBlock (gu4TacMsgOutputPoolId, pTacMsgOutput);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocTacMsgInput                           */
/*                                                                            */
/*    Description        : This function allocates memory blocks for the      */
/*                         specified memory pool of TacMsgInput.              */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to the structure tTacMsgInput.             */
/******************************************************************************/

UINT1              *
UtlShMemAllocTacMsgInput ()
{
    UINT1              *pBuf;
    pBuf = MemAllocMemBlk (gu4TacMsgInputPoolId);
    return pBuf;

}

/******************************************************************************/
/* Function Name     : UtlShMemFreeTacMsgInput                          */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     TacmsgInput.                                           */
/*                                                                            */
/* Input(s)          : pTacMsgInput - Pointer to the structure tTacMsgOutput  */
/*                                                                            */
/* Outpu(s)          : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeTacMsgInput (UINT1 *pTacMsgInput)
{
    MemReleaseMemBlock (gu4TacMsgInputPoolId, pTacMsgInput);
    return;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeRadInterface                         */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     Radius Interface.                                      */
/*                                                                            */
/* Input(s)          : pIface - pointer to structure tRadInterface            */
/*                                                                            */
/* Outpu(s)          : None                                                   */
/*                                                                            */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/*                                                                            */
/******************************************************************************/

VOID
UtlShMemFreeRadInterface (UINT1 *pIface)
{
    MemReleaseMemBlock (gu4RadIfPoolId, pIface);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocRadInterface                         */
/*                                                                            */
/*    Description        : This function allocates memory blocks for the      */
/*                         specified memory pool of RadiusInterface.          */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to the structure tRadInterface.            */
/******************************************************************************/

UINT1              *
UtlShMemAllocRadInterface ()
{
    UINT1              *pBuf;
    pBuf = MemAllocMemBlk (gu4RadIfPoolId);
    return pBuf;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocOifList                               */
/*                                                                            */
/*    Description        : This function allocates memory blocks for the      */
/*                         OIfList pool for common utility.                   */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : UINT4 Pointer.                                     */
/******************************************************************************/

UINT4              *
UtlShMemAllocOifList ()
{
    UINT4              *pu4OifList;
    pu4OifList = (UINT4 *) MemAllocMemBlk (gu4UtlOifPoolId);
    return pu4OifList;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeOifList                                    */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     common utility.                                        */
/*                                                                            */
/* Input(s)          : pu4OifList - OIfList Pointer.                          */
/*                                                                            */
/* Output(s)         : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeOifList (UINT4 *pu4OifList)
{
    MemReleaseMemBlock (gu4UtlOifPoolId, (UINT1 *) pu4OifList);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocVlanList                              */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         VLAN List memory pool.                             */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size VLAN_LIST_SIZE           */
/******************************************************************************/

UINT1              *
UtlShMemAllocVlanList ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4UtlVlanListPoolId);
    if (pBuf != NULL)
    {
        MEMSET (pBuf, 0, sizeof (UINT1[VLAN_LIST_SIZE_EXT]));
    }
    return pBuf;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeVlanList                                   */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     Vlan List block                                        */
/*                                                                            */
/* Input(s)          : pVlanList - Pointer to Vlan List buffer                */
/*                                                                            */
/* Output(s)         : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeVlanList (UINT1 *pVlanList)
{
    MemReleaseMemBlock (gu4UtlVlanListPoolId, pVlanList);
    return;
}

/************************************************************************
 ** Function Name   : UtilWebAllocBufferSize
 ** Description     : function to allocate WEB Block
 ** Input           : None
 ** Output          : None
 ** Returns         : pointer to WEB block or null
 ***************************************************************************/
UINT1              *
UtilWebAllocBufferSize ()
{
    UINT1              *pu1BufferSize = NULL;
    pu1BufferSize = MemAllocMemBlk (gWebAllocBufferPoolId);
    return pu1BufferSize;
}

/****************************************************************************
* Function     : UtilWebReleaseBufferSize
*
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1BufferSize - Pointer to the BufferSize
*
* Output       : None
*
* Returns      : None
*
***************************************************************************/
VOID
UtilWebReleaseBufferSize (UINT1 *pu1BufferSize)
{
    MemReleaseMemBlock (gWebAllocBufferPoolId, pu1BufferSize);
    return;
}

/************************************************************************
 ** Function Name   : UtilRadKeyAllocBufferSize
 ** Description     : function to allocate Radius Key Block
 ** Input           : None
 ** Output          : None
 ** Returns         : pointer to Radius Key block or null
 ***************************************************************************/
UINT1              *
UtilRadKeyAllocBufferSize ()
{
    UINT1              *pu1BufferSize = NULL;
    pu1BufferSize = MemAllocMemBlk (gu4RadKeyBlocksPoolId);
    return pu1BufferSize;
}

/****************************************************************************
* Function     : UtilRadKeyReleaseBufferSize
*
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1BufferSize - Pointer to the BufferSize
*
* Output       : None
*
* Returns      : None
*
***************************************************************************/
VOID
UtilRadKeyReleaseBufferSize (UINT1 *pu1BufferSize)
{
    MemReleaseMemBlock (gu4RadKeyBlocksPoolId, pu1BufferSize);
    return;
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocWlcBuf                                */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         WLC  memory pool.                                  */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size VLAN_LIST_SIZE           */
/******************************************************************************/

UINT1              *
UtlShMemAllocWlcBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4UtlWlcPoolId);
    return pBuf;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeWlcBuf                                     */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     WLC memory block                                       */
/*                                                                            */
/* Input(s)          : pWlcBuf - Pointer to WLC buffer                        */
/*                                                                            */
/* Output(s)         : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeWlcBuf (UINT1 *pWlcBuf)
{
    MemReleaseMemBlock (gu4UtlWlcPoolId, pWlcBuf);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocWssWlanBuf                            */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         WLAN  memory pool.                                 */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tWssWlanMsgStruct        */
/******************************************************************************/

UINT1              *
UtlShMemAllocWssWlanBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4UtlWssWlanPoolId);
    return pBuf;
}

/******************************************************************************/
/* Function Name     : UtlShMemFreeWssWlanBuf                                 */
/*                                                                            */
/* Description       : This function is to release the memory allocated for   */
/*                     WLAN memory block                                      */
/*                                                                            */
/* Input(s)          : pWssWlanBuf - Pointer to WLAN buffer                   */
/*                                                                            */
/* Output(s)         : None                                                   */
/*                                                                            */
/* Return Values(s)  : None                                                   */
/******************************************************************************/

VOID
UtlShMemFreeWssWlanBuf (UINT1 *pWssWlanBuf)
{
    MemReleaseMemBlock (gu4UtlWssWlanPoolId, pWssWlanBuf);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocAntennaSelectionBuf                   */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         Antenna selection memory pool.                     */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size RADIOIF_ANTENNA_LIST_INDEX*/
/******************************************************************************/
UINT1              *
UtlShMemAllocAntennaSelectionBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4AntennaSelectionPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeAntennaSelectionBuf                          */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     Antenna selection block                                  */
/*                                                                              */
/* Input(s)          : pu1AntennaSelection - Pointer to Antenna selection buffer*/
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeAntennaSelectionBuf (UINT1 *pu1AntennaSelection)
{

    MemReleaseMemBlock (gu4AntennaSelectionPoolId, pu1AntennaSelection);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocWlanDbBuf                             */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         Wlandb memory pool.                                */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tWssWlanDb               */
/******************************************************************************/
UINT1              *
UtlShMemAllocWlanDbBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4WlanDbPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeWlanDbBuf                                    */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     wlandb block                                             */
/*                                                                              */
/* Input(s)          : pwssWlanDb - Pointer to wlandb buffer                    */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeWlanDbBuf (UINT1 *pwssWlanDb)
{

    MemReleaseMemBlock (gu4WlanDbPoolId, pwssWlanDb);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocMacMsgStructBuf                       */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         MacMsgStruct memory pool.                          */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tWssMacMsgStruct         */
/******************************************************************************/
UINT1              *
UtlShMemAllocMacMsgStructBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4UtilMacMsgPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeMacMsgStructBuf                              */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     MacMsgStruct block                                       */
/*                                                                              */
/* Input(s)          : pWssMacMsgRspStruct - Pointer to wlandb buffer                    */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeMacMsgStructBuf (UINT1 *pWssMacMsgRspStruct)
{

    MemReleaseMemBlock (gu4UtilMacMsgPoolId, pWssMacMsgRspStruct);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocConfigStatusRspBuf                    */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         ConfigStatusRsp memory pool.                       */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tConfigStatusRsp         */
/******************************************************************************/
UINT1              *
UtlShMemAllocConfigStatusRspBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4ConfigStatusRspPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeConfigStatusRspBuf                           */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     ConfigStatusRsp block                                    */
/*                                                                              */
/* Input(s)          : pConfigStatusRsp - Pointer to configStatusRsp buffer     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeConfigStatusRspBuf (UINT1 *pConfigStatusRsp)
{

    MemReleaseMemBlock (gu4ConfigStatusRspPoolId, pConfigStatusRsp);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocWssIfAuthDbBuf                        */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         WssIfAuthDb memory pool.                           */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tWssIfAuthDb             */
/******************************************************************************/
UINT1              *
UtlShMemAllocWssIfAuthDbBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4WssIfAuthDbPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeWssIfAuthDbBuf                               */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     WssIfAuthDb block                                        */
/*                                                                              */
/* Input(s)          : pWssIfAuthDb - Pointer to WssIfAuthDb buffer             */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeWssIfAuthDbBuf (UINT1 *pWssIfAuthDb)
{

    MemReleaseMemBlock (gu4WssIfAuthDbPoolId, pWssIfAuthDb);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocAcInfoBuf                             */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         AcInfoAfterDiscovery memory pool.                  */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tACInfoAfterDiscovery    */
/******************************************************************************/
UINT1              *
UtlShMemAllocAcInfoBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4AcInfoAfterDiscoveryPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeAcInfoBuf                               */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     AcInfoAfterDiscovery block                               */
/*                                                                              */
/* Input(s)          : pAcInfoAfterDisc - Pointer to AcInfoAfterDiscovery buffer*/
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeAcInfoBuf (UINT1 *pAcInfoAfterDisc)
{

    MemReleaseMemBlock (gu4AcInfoAfterDiscoveryPoolId, pAcInfoAfterDisc);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocJoinReqBuf                            */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         JoinReq memory pool.                               */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tJoinReq                 */
/******************************************************************************/
UINT1              *
UtlShMemAllocJoinReqBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4JoinReqPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeJoinReqBuf                                   */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     JoinReq block                                            */
/*                                                                              */
/* Input(s)          : pJoinReq - Pointer to JoinReq buffer                     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeJoinReqBuf (UINT1 *pJoinReq)
{

    MemReleaseMemBlock (gu4JoinReqPoolId, pJoinReq);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocDiscRspBuf                            */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         DiscRsp memory pool.                               */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tDiscRsp                 */
/******************************************************************************/
UINT1              *
UtlShMemAllocDiscRspBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4DiscRspPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeDiscRspBuf                                   */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     DiscRsp block                                            */
/*                                                                              */
/* Input(s)          : pDiscRsp - Pointer to DiscRsp buffer                     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeDiscRspBuf (UINT1 *pDiscRsp)
{

    MemReleaseMemBlock (gu4DiscRspPoolId, pDiscRsp);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocDataReqBuf                            */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         DataReq memory pool.                               */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tDataReq                 */
/******************************************************************************/
UINT1              *
UtlShMemAllocDataReqBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4DataReqPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeDataReqBuf                                   */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     DataReq block                                            */
/*                                                                              */
/* Input(s)          : pDataReq - Pointer to DataReq buffer                     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeDataReqBuf (UINT1 *pDataReq)
{

    MemReleaseMemBlock (gu4DataReqPoolId, pDataReq);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocCapwapPktLenBuf                   */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         Capwap Packet Len memory pool.                     */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size MAX_CAPWAP_PKT_LEN       */
/******************************************************************************/
UINT1              *
UtlShMemAllocCapwapPktBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4capwapPktCountId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeCapwapPktLenBuf                              */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     Capwap Pkt Len block                                     */
/*                                                                              */
/* Input(s)          : pu1Frame - Pointer to pkt len buffer                     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeCapwapPktBuf (UINT1 *pu1Frame)
{

    MemReleaseMemBlock (gu4capwapPktCountId, pu1Frame);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocJoinRspBuf                            */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         JoinRsp memory pool.                               */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tJoinRsp                 */
/******************************************************************************/
UINT1              *
UtlShMemAllocJoinRspBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4JoinRspPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeJoinRspBuf                                   */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     JoinRsp block                                            */
/*                                                                              */
/* Input(s)          : pJoinRsp - Pointer to JoinRsp buffer                     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeJoinRspBuf (UINT1 *pJoinRsp)
{

    MemReleaseMemBlock (gu4JoinRspPoolId, pJoinRsp);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemAllocConfigStatusReqBuf                    */
/*                                                                            */
/*    Description        : This function allocates memory block from the      */
/*                         ConfigStatusReq memory pool.                       */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None.                                              */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : Pointer to buffer of size tConfigStatusReq         */
/******************************************************************************/
UINT1              *
UtlShMemAllocConfigStatusReqBuf ()
{
    UINT1              *pBuf = NULL;
    pBuf = MemAllocMemBlk (gu4ConfigStatusReqPoolId);
    return pBuf;
}

/********************************************************************************/
/* Function Name     : UtlShMemFreeConfigStatusReqBuf                           */
/*                                                                              */
/* Description       : This function is to release the memory allocated for     */
/*                     ConfigStatusReq block                                    */
/*                                                                              */
/* Input(s)          : pConfigStatusReq - Pointer to ConfigStatusReq buffer     */
/*                                                                              */
/* Output(s)         : None                                                     */
/*                                                                              */
/* Return Values(s)  : None                                                     */
/********************************************************************************/

VOID
UtlShMemFreeConfigStatusReqBuf (UINT1 *pConfigStatusReq)
{

    MemReleaseMemBlock (gu4ConfigStatusReqPoolId, pConfigStatusReq);
    return;
}
#endif
#endif
