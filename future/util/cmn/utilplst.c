/*
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *  $Id: utilplst.c,v 1.1 2015/04/28 12:51:02 siva Exp $
 *  
 *  Description:
 *  
 */

#include "lr.h"
#include "l2iwf.h"
#include "fsutil.h"
#include "fsutltrc.h"

extern UINT4        gu4UtlTrcFlag;

tMemPoolId          gLocalPortListPoolId;    /* Local Port List Pool Id */
UINT4               gu4LocalPortListUsedBlocks;    /* Number of Local Port List 
                                                 * blocks  allocated so far.
                                                 */
UINT4               gu4LocalPortListTotalBlocks;    /* Number of blocks 
                                                     * allocated in Local
                                                     * Port List mempool. 
                                                     */
#define UTIL_LOCAL_PORTLIST_BLK_SIZE           sizeof(tLocalPortList)

/****************************************************************************
* Function     : UtilPlstInitLocalPortList   
*                                            
* Description  : To intialize the mempool required for Local Portlist variable.
*
* Input        : None
*
* Output       : None
*
* Returns      : None   
*
***************************************************************************/
VOID
UtilPlstInitLocalPortList (UINT4 u4Count)
{
    gu4LocalPortListTotalBlocks = u4Count;
    gu4LocalPortListUsedBlocks = 0;
    return;
}

/****************************************************************************
* Function     : UtilPlstAllocLocalPortList 
*                                            
* Description  : To allocate memory for a Local PortList from the common 
*                mempool.
*
* Input        : u4Size        - Size of the required Local Portlist.
*
* Output       : None
*
* Returns      : Pointer to the allocated Local Port List.
*
***************************************************************************/
UINT1              *
UtilPlstAllocLocalPortList (UINT4 u4Size)
{
    UINT1              *pu1LocalPortList = NULL;

    UNUSED_PARAM (u4Size);

    if (MemAllocateMemBlock (gLocalPortListPoolId, &pu1LocalPortList) ==
        MEM_FAILURE)
    {
        MOD_TRC_ARG2 (UTIL_TRC_FLAG, ALL_FAILURE_TRC, UTIL_NAME,
                      "Allocation failed = %s; Already allocated count = %d\n",
                      __FUNCTION__, gu4LocalPortListUsedBlocks);
        return NULL;
    }
    MEMSET (pu1LocalPortList, 0, UTIL_LOCAL_PORT_LIST_SIZE);
    gu4LocalPortListUsedBlocks++;
    return pu1LocalPortList;

}

/****************************************************************************
* Function     : UtilPlstReleaseLocalPortList 
*                                            
* Description  : To releases the memory to the common mempool.
*
* Input        : pu1LocalPortList - Pointer to the Local Portlist
*
* Output       : None
*               
* Returns      : None
*
***************************************************************************/
VOID
UtilPlstReleaseLocalPortList (UINT1 *pu1LocalPortList)
{
    UINT4               u4RetVal = MEM_FAILURE;

    if (pu1LocalPortList != NULL)
    {
        u4RetVal = MemReleaseMemBlock (gLocalPortListPoolId,
                                       (pu1LocalPortList));
        if (u4RetVal == MEM_SUCCESS)
        {
            /* Decrement the number of Local Portlist blocks allocated. */
            gu4LocalPortListUsedBlocks--;
            pu1LocalPortList = NULL;
        }
    }
    return;
}
