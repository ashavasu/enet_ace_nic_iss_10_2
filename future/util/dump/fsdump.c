/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsdump.c,v 1.8 2015/04/28 12:51:03 siva Exp $
 *
 * Description: packet dump routine for cru-buffer
 *
 *******************************************************************/

#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlsll.h"
#include "utldll.h"
#include "srmtmr.h"
#include "utlhash.h"
#include "utltrc.h"
#include "utleeh.h"
#include "utlmacro.h"

PUBLIC void DumpPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Length));
#ifdef __STDC__
PUBLIC void
DumpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Length)
#else
PUBLIC void
DumpPkt (pBuf, u4Length)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4Length;
#endif
{
#define MAX_DUMP_LEN        512

    UINT4               u4Count;
    UINT1               Linear[MAX_DUMP_LEN + 1];
    CHR1                au1Buf[10];

    if ((u4Count = CRU_BUF_Get_ChainValidByteCount (pBuf)) < u4Length)
        u4Length = u4Count;

    if (u4Length > MAX_DUMP_LEN)
        u4Length = MAX_DUMP_LEN;

    u4Count = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, Linear, 0, u4Length);
    while (u4Count < u4Length)
    {
        SPRINTF ((CHR1 *) au1Buf, "%02x ", Linear[u4Count]);
        UtlTrcPrint (au1Buf);
        u4Count++;
        if ((u4Count % 16) == 0)
        {
            UtlTrcPrint ("\n");
        }
    }
    UtlTrcPrint ("\n");
}
