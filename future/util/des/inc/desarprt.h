/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: desarprt.h,v 1.1 2015/04/28 12:53:03 siva Exp $
*
* Description:This file contains the prototypes for all the
*  procedures.
********************************************************************/
#ifndef _DES_PROT_H
#define _DES_PROT_H 
/********************************************************************/

PUBLIC VOID DesArSetup PROTO(( unArCryptoKey *, const UINT1 * ));
PUBLIC VOID DesArEncrypt PROTO(( const unArCryptoKey *, const UINT1 *, UINT1 * ));
PUBLIC VOID DesArDecrypt PROTO(( const unArCryptoKey *, const UINT1 *, UINT1 * ));

PUBLIC VOID TDesArSetup PROTO(( unArCryptoKey *, const UINT1 * ));
PUBLIC VOID TDesArEncrypt PROTO(( const unArCryptoKey *, const UINT1 *, UINT1 * ));
PUBLIC VOID TDesArDecrypt PROTO(( const unArCryptoKey *, const UINT1 *, UINT1 * ));
PUBLIC VOID DesArSetupSubKeys PROTO((AR_CRYPTO_UINT8 * pu8ArCryptoSubKey, const UINT1 *pu1ArKey));

    /*API*/
    /* Call the Des Setup routine for the Key Scheduling*/ 
PUBLIC UINT1 DesArKeyScheduler PROTO (( const UINT1 *, unArCryptoKey *));
PUBLIC UINT1 DesArCbcEncrypt PROTO ((UINT1 *, UINT4 , unArCryptoKey *, UINT1 *));
PUBLIC UINT1 DesArCbcDecrypt PROTO ((UINT1 *, UINT4 , unArCryptoKey *, UINT1 *));
PUBLIC UINT1 TDesArKeySchedule PROTO (( const UINT1 *,unArCryptoKey *));
PUBLIC UINT1 TDesArCbcEncrypt PROTO ((UINT1 *, UINT4 ,unArCryptoKey *, UINT1 *));
PUBLIC UINT1 TDesArCbcDecrypt PROTO ((UINT1 *, UINT4 ,unArCryptoKey *, UINT1 *));
PUBLIC UINT1 DesArSubKeys PROTO ((AR_CRYPTO_UINT8 * , const UINT1 *));
#endif
