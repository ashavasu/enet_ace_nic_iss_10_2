/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: desardef.h,v 1.1 2015/04/28 12:53:03 siva Exp $
 *
 * Description: This file contains the defines required for DES 
 * module
 *****************************************************************************/
#ifndef _DES_DEFS_H
#define _DES_DEFS_H
/********************************************************************/

#define DES_INIT_VAL                     0     /* Initialization value
                                                */
#define DES_TRUE_VAL                     1     /* True value */
#define DES_INIT_ULL                     0ULL  /* Initialization value of 
                                                * Unsigned Long Long 
                                                */
#define DES_SUBKEY_SIZE                  48    /* No of bits in a Des Sub Key
                                                */
#define DES_DATA_NO_OF_BYTES             8     /* No of bytes in a Des data*/
#define DES_KEY_NO_OF_BYTES              8     /* No of bytes in a Des Key*/
#define DES_KEY_SCHEDULE_ITERATION       16    /* No of iteration required for
                                                * Key Scheduling 
                                                */
#define DES_KEY_NO_OF_BITS               48    /* No of bits  in  Des Key*/
#define DES_NO_OF_BITS_FOR_S_BLOCK       6     /* No of bits  Required for the
                                                * use of S MAtrix
                                                */
#define DES_NO_OF_ITERATION_FOR_S_FN     8     /* No of S Selection Function 
                                                * Iteration
                                                */
#define DES_NO_OF_BITS_IN_DATA_L_BLOCK   32    /* No of bits in the first half
                                                * of he Permuted data block
                                                * (i.e L block)
                                                */
#define DES_NO_OF_BITS_IN_DATA_R_BLOCK   32    /* No of bits in the Second half
                                                * of he Permuted data block
                                                * (i.e R block)
                                                */
#define DES_NO_OF_BITS_IN_KEY_L_BLOCK    28    /* No of bits in the first half 
                                                * of he Permuted Key block
                                                * (i.e L block)
                                                */
#define DES_NO_OF_BITS_IN_KEY_R_BLOCK    28    /* No of bits in the Second
                                                * half of he Permuted Key 
                                                * block(i.e R block)
                                                */
#define DES_NO_OF_BYTE_IN_R_BLOCK        4     /* No of byte in R Block*/
#define DES_FOUR_BIT_MASK                0x0F  /* Mask the  4 LSB bits */
#define DES_SIX_BIT_MASK                 0x3F  /* Mask the  6 LSB bits */
#define DES_BYTE_MASK                    0xFF  /* Mask the  6 LSB bits */
#define DES_BLOCK_MASK                   0xFFFFFFFF     /* Mask the 4byte data
                                                        */
#define DES_ULL_MASK                     0xFFFFFFFFULL  /* Mask the unsigned 
                                                         * long long
                                                         */
#define DES_ULL_MASK_SEVEN_LSB_NIBBLES    0x0FFFFFFFULL  /* Mask the  unsigned
                                                         * long long but not
                                                         * the most signifigant
                                                         * Nibble
                                                         */
#define DES_DATA_LAST_BUT_ONE_BYTE       7      /* Last but one Byte number in 
                                                 * the Data lenth of 8 bytes 
                                                 */
#define DES_SUCCESS                      1
#define DES_FAILURE                      0
#define DES_MAX_ARRAY_SIZE               64
#define DES_MAX_E_ARRAY_SIZE             48
#define DES_MAX_P_ARRAY_SIZE             32
#define DES_MAX_PC1_ARRAY_SIZE           56
#define DES_MAX_PC2_ARRAY_SIZE           48
#define DES_MAX_NO_OF_S_MATRIX           8
#define DES_MAX_ELEMENT_S_MATRIX         64
#define DES_MULTIPLY_TWO                 2
#define DES_IV_SIZE                      8

#endif
