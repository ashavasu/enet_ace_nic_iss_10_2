/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: desarmac.h,v 1.1 2015/04/28 12:53:03 siva Exp $
*
* Description:This file contains the macro definition for the DES 
*             Module 
********************************************************************/
#ifndef _DES_MAC_H
#define _DES_MAC_H
/********************************************************************/

#define DES_PERMUTE(Size, BitPos)         (((AR_CRYPTO_UINT8) 1) << ((Size) - (BitPos)))
#define DES_ROTATE_LEFT(u4Datablock,ShiftN)\
        ( (0x0FFFFFFF & ((u4Datablock) << (ShiftN))) |\
         ((0x0FFFFFFF & (u4Datablock)) >> (28-(ShiftN))) )
                             /* Rotate the u4Datablock to ShiftN bits left
                              */
#define DES_NO_OF_ELEMENTS(array) (sizeof(array)/sizeof(array[0]))
                             /* Calculate the number of elements present
                              * in the Martix/Array x
                              */
#define DES_U8LEFT_SHIFT(u8Var,u1BitPos)\
     ((u8Var << u1BitPos))
                             /* u8Variable will be left shifted by 
                              * u1BitPositions 
                              */
#define DES_U4LEFT_SHIFT(u4Var,u1BitPos)\
     ((u4Var << u1BitPos))
                             /* u4Variable will be left shifted by
                              * u1BitPositions
                              */
#define DES_U8RIGHT_SHIFT(u8Var,u1BitPos)\
     ((u8Var >> u1BitPos))
                             /* u8Variable will be Right shifted by
                              * u1BitPositions 
                              */
#define DES_U4RIGHT_SHIFT(u4Var,u1BitPos)\
     ((u4Var >> u1BitPos))
                             /* u4Variable will be Right shifted by
                              * u1BitPositions
                              */
#endif
