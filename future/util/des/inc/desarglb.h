/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: desarglb.h,v 1.1 2015/04/28 12:53:03 siva Exp $
 *
 * Description: This file contains the global defines for DES
 * module
 ****************************************************************************/
#ifndef _DES_GLB_H
#define _DES_GLB_H
/****************************************************************************/
#include "desarinc.h"

const AR_CRYPTO_UINT8 gaArDesIP[DES_MAX_ARRAY_SIZE] = {
                DES_PERMUTE(64, 58), DES_PERMUTE(64, 50), DES_PERMUTE(64, 42), DES_PERMUTE(64, 34), DES_PERMUTE(64, 26), DES_PERMUTE(64, 18), DES_PERMUTE(64, 10), DES_PERMUTE(64, 2),
                DES_PERMUTE(64, 60), DES_PERMUTE(64, 52), DES_PERMUTE(64, 44), DES_PERMUTE(64, 36), DES_PERMUTE(64, 28), DES_PERMUTE(64, 20), DES_PERMUTE(64, 12), DES_PERMUTE(64, 4),
                DES_PERMUTE(64, 62), DES_PERMUTE(64, 54), DES_PERMUTE(64, 46), DES_PERMUTE(64, 38), DES_PERMUTE(64, 30), DES_PERMUTE(64, 22), DES_PERMUTE(64, 14), DES_PERMUTE(64, 6),
                DES_PERMUTE(64, 64), DES_PERMUTE(64, 56), DES_PERMUTE(64, 48), DES_PERMUTE(64, 40), DES_PERMUTE(64, 32), DES_PERMUTE(64, 24), DES_PERMUTE(64, 16), DES_PERMUTE(64, 8),
                DES_PERMUTE(64, 57), DES_PERMUTE(64, 49), DES_PERMUTE(64, 41), DES_PERMUTE(64, 33), DES_PERMUTE(64, 25), DES_PERMUTE(64, 17), DES_PERMUTE(64, 9),  DES_PERMUTE(64, 1),
                DES_PERMUTE(64, 59), DES_PERMUTE(64, 51), DES_PERMUTE(64, 43), DES_PERMUTE(64, 35), DES_PERMUTE(64, 27), DES_PERMUTE(64, 19), DES_PERMUTE(64, 11), DES_PERMUTE(64, 3),
                DES_PERMUTE(64, 61), DES_PERMUTE(64, 53), DES_PERMUTE(64, 45), DES_PERMUTE(64, 37), DES_PERMUTE(64, 29), DES_PERMUTE(64, 21), DES_PERMUTE(64, 13), DES_PERMUTE(64, 5),
                DES_PERMUTE(64, 63), DES_PERMUTE(64, 55), DES_PERMUTE(64, 47), DES_PERMUTE(64, 39), DES_PERMUTE(64, 31), DES_PERMUTE(64, 23), DES_PERMUTE(64, 15), DES_PERMUTE(64, 7)
        }; 
                /* The Initial Permutation Matrix required to perform the
                 * Initial Permutation on the 64 bit Data
                 */

const AR_CRYPTO_UINT8 gaArDesFP[DES_MAX_ARRAY_SIZE] = {
                DES_PERMUTE(64, 40), DES_PERMUTE(64, 8),  DES_PERMUTE(64, 48), DES_PERMUTE(64, 16), DES_PERMUTE(64, 56), DES_PERMUTE(64, 24), DES_PERMUTE(64, 64), DES_PERMUTE(64, 32),
                DES_PERMUTE(64, 39), DES_PERMUTE(64, 7),  DES_PERMUTE(64, 47), DES_PERMUTE(64, 15), DES_PERMUTE(64, 55), DES_PERMUTE(64, 23), DES_PERMUTE(64, 63), DES_PERMUTE(64, 31),
                DES_PERMUTE(64, 38), DES_PERMUTE(64, 6),  DES_PERMUTE(64, 46), DES_PERMUTE(64, 14), DES_PERMUTE(64, 54), DES_PERMUTE(64, 22), DES_PERMUTE(64, 62), DES_PERMUTE(64, 30),
                DES_PERMUTE(64, 37), DES_PERMUTE(64, 5),  DES_PERMUTE(64, 45), DES_PERMUTE(64, 13), DES_PERMUTE(64, 53), DES_PERMUTE(64, 21), DES_PERMUTE(64, 61), DES_PERMUTE(64, 29),
                DES_PERMUTE(64, 36), DES_PERMUTE(64, 4),  DES_PERMUTE(64, 44), DES_PERMUTE(64, 12), DES_PERMUTE(64, 52), DES_PERMUTE(64, 20), DES_PERMUTE(64, 60), DES_PERMUTE(64, 28),
                DES_PERMUTE(64, 35), DES_PERMUTE(64, 3),  DES_PERMUTE(64, 43), DES_PERMUTE(64, 11), DES_PERMUTE(64, 51), DES_PERMUTE(64, 19), DES_PERMUTE(64, 59), DES_PERMUTE(64, 27),
                DES_PERMUTE(64, 34), DES_PERMUTE(64, 2),  DES_PERMUTE(64, 42), DES_PERMUTE(64, 10), DES_PERMUTE(64, 50), DES_PERMUTE(64, 18), DES_PERMUTE(64, 58), DES_PERMUTE(64, 26),
                DES_PERMUTE(64, 33), DES_PERMUTE(64, 1),  DES_PERMUTE(64, 41), DES_PERMUTE(64, 9),  DES_PERMUTE(64, 49), DES_PERMUTE(64, 17), DES_PERMUTE(64, 57), DES_PERMUTE(64, 25)
        };
               /* The Inverse of initial Permutation required to perform the
                * Final permutation on the 64 bit information received through
                * complex key dependent computation
                */

const AR_CRYPTO_UINT8 gaArDesE[DES_MAX_E_ARRAY_SIZE] = {
                DES_PERMUTE(32, 32), DES_PERMUTE(32, 1),  DES_PERMUTE(32, 2),  DES_PERMUTE(32, 3),  DES_PERMUTE(32, 4),  DES_PERMUTE(32, 5),
                DES_PERMUTE(32, 4),  DES_PERMUTE(32, 5),  DES_PERMUTE(32, 6),  DES_PERMUTE(32, 7),  DES_PERMUTE(32, 8),  DES_PERMUTE(32, 9),
                DES_PERMUTE(32, 8),  DES_PERMUTE(32, 9),  DES_PERMUTE(32, 10), DES_PERMUTE(32, 11), DES_PERMUTE(32, 12), DES_PERMUTE(32, 13),
                DES_PERMUTE(32, 12), DES_PERMUTE(32, 13), DES_PERMUTE(32, 14), DES_PERMUTE(32, 15), DES_PERMUTE(32, 16), DES_PERMUTE(32, 17),
                DES_PERMUTE(32, 16), DES_PERMUTE(32, 17), DES_PERMUTE(32, 18), DES_PERMUTE(32, 19), DES_PERMUTE(32, 20), DES_PERMUTE(32, 21),
                DES_PERMUTE(32, 20), DES_PERMUTE(32, 21), DES_PERMUTE(32, 22), DES_PERMUTE(32, 23), DES_PERMUTE(32, 24), DES_PERMUTE(32, 25),
                DES_PERMUTE(32, 24), DES_PERMUTE(32, 25), DES_PERMUTE(32, 26), DES_PERMUTE(32, 27), DES_PERMUTE(32, 28), DES_PERMUTE(32, 29),
                DES_PERMUTE(32, 28), DES_PERMUTE(32, 29), DES_PERMUTE(32, 30), DES_PERMUTE(32, 31), DES_PERMUTE(32, 32), DES_PERMUTE(32, 1)
        };
               /* The Matrix used for expanding the 32 bit block data into
                * 48 bit data
                */

const AR_CRYPTO_UINT8 gaArDesP[DES_MAX_P_ARRAY_SIZE] = {
                DES_PERMUTE(32, 16), DES_PERMUTE(32, 7),  DES_PERMUTE(32, 20), DES_PERMUTE(32, 21),
                DES_PERMUTE(32, 29), DES_PERMUTE(32, 12), DES_PERMUTE(32, 28), DES_PERMUTE(32, 17),
                DES_PERMUTE(32, 1),  DES_PERMUTE(32, 15), DES_PERMUTE(32, 23), DES_PERMUTE(32, 26),
                DES_PERMUTE(32, 5),  DES_PERMUTE(32, 18), DES_PERMUTE(32, 31), DES_PERMUTE(32, 10),
                DES_PERMUTE(32, 2),  DES_PERMUTE(32, 8),  DES_PERMUTE(32, 24), DES_PERMUTE(32, 14),
                DES_PERMUTE(32, 32), DES_PERMUTE(32, 27), DES_PERMUTE(32, 3),  DES_PERMUTE(32, 9),
                DES_PERMUTE(32, 19), DES_PERMUTE(32, 13), DES_PERMUTE(32, 30), DES_PERMUTE(32, 6),
                DES_PERMUTE(32, 22), DES_PERMUTE(32, 11), DES_PERMUTE(32, 4),  DES_PERMUTE(32, 25)
        };
              /* Permutation Matrix used to obtain a 32 bit block as output on
               * providing a 32 bit input obtained by the usage of Selection
               * function S1, S2, S3<85>.and S8.
               */

const AR_CRYPTO_UINT8 gaArDesPC1[DES_MAX_PC1_ARRAY_SIZE] = {
                DES_PERMUTE(64, 57), DES_PERMUTE(64, 49), DES_PERMUTE(64, 41), DES_PERMUTE(64, 33), DES_PERMUTE(64, 25), DES_PERMUTE(64, 17), DES_PERMUTE(64, 9),
                DES_PERMUTE(64, 1),  DES_PERMUTE(64, 58), DES_PERMUTE(64, 50), DES_PERMUTE(64, 42), DES_PERMUTE(64, 34), DES_PERMUTE(64, 26), DES_PERMUTE(64, 18),
                DES_PERMUTE(64, 10), DES_PERMUTE(64, 2),  DES_PERMUTE(64, 59), DES_PERMUTE(64, 51), DES_PERMUTE(64, 43), DES_PERMUTE(64, 35), DES_PERMUTE(64, 27),
                DES_PERMUTE(64, 19), DES_PERMUTE(64, 11), DES_PERMUTE(64, 3),  DES_PERMUTE(64, 60), DES_PERMUTE(64, 52), DES_PERMUTE(64, 44), DES_PERMUTE(64, 36),
                DES_PERMUTE(64, 63), DES_PERMUTE(64, 55), DES_PERMUTE(64, 47), DES_PERMUTE(64, 39), DES_PERMUTE(64, 31), DES_PERMUTE(64, 23), DES_PERMUTE(64, 15),
                DES_PERMUTE(64, 7),  DES_PERMUTE(64, 62), DES_PERMUTE(64, 54), DES_PERMUTE(64, 46), DES_PERMUTE(64, 38), DES_PERMUTE(64, 30), DES_PERMUTE(64, 22),
                DES_PERMUTE(64, 14), DES_PERMUTE(64, 6),  DES_PERMUTE(64, 61), DES_PERMUTE(64, 53), DES_PERMUTE(64, 45), DES_PERMUTE(64, 37), DES_PERMUTE(64, 29),
                DES_PERMUTE(64, 21), DES_PERMUTE(64, 13), DES_PERMUTE(64, 5),  DES_PERMUTE(64, 28), DES_PERMUTE(64, 20), DES_PERMUTE(64, 12), DES_PERMUTE(64, 4)
        };
              /* The Permuted Choice 1 matrix required to perform the
               * Permutation on the given key.
               */

const AR_CRYPTO_UINT8 gaArDesPC2[DES_MAX_PC2_ARRAY_SIZE] = {
                DES_PERMUTE(56, 14), DES_PERMUTE(56, 17), DES_PERMUTE(56, 11), DES_PERMUTE(56, 24), DES_PERMUTE(56, 1),  DES_PERMUTE(56, 5),
                DES_PERMUTE(56, 3),  DES_PERMUTE(56, 28), DES_PERMUTE(56, 15), DES_PERMUTE(56, 6),  DES_PERMUTE(56, 21), DES_PERMUTE(56, 10),
                DES_PERMUTE(56, 23), DES_PERMUTE(56, 19), DES_PERMUTE(56, 12), DES_PERMUTE(56, 4),  DES_PERMUTE(56, 26), DES_PERMUTE(56, 8),
                DES_PERMUTE(56, 16), DES_PERMUTE(56, 7),  DES_PERMUTE(56, 27), DES_PERMUTE(56, 20), DES_PERMUTE(56, 13), DES_PERMUTE(56, 2),
                DES_PERMUTE(56, 41), DES_PERMUTE(56, 52), DES_PERMUTE(56, 31), DES_PERMUTE(56, 37), DES_PERMUTE(56, 47), DES_PERMUTE(56, 55),
                DES_PERMUTE(56, 30), DES_PERMUTE(56, 40), DES_PERMUTE(56, 51), DES_PERMUTE(56, 45), DES_PERMUTE(56, 33), DES_PERMUTE(56, 48),
                DES_PERMUTE(56, 44), DES_PERMUTE(56, 49), DES_PERMUTE(56, 39), DES_PERMUTE(56, 56), DES_PERMUTE(56, 34), DES_PERMUTE(56, 53),
                DES_PERMUTE(56, 46), DES_PERMUTE(56, 42), DES_PERMUTE(56, 50), DES_PERMUTE(56, 36), DES_PERMUTE(56, 29), DES_PERMUTE(56, 32)
        };
              /* The Permuted Choice 2 matrix required to perform the
               * Permutation on the Key to get the 48 bit Sub keys
               */

const UINT4 gaArDesLShift[DES_KEY_SCHEDULE_ITERATION] = 
    {
         1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
    };
             /* The Array used to know the number of left shifts needs to be
              * done on each iteration of Key Scheduling
              */

const UINT1 gaArDesSBlocks[DES_MAX_NO_OF_S_MATRIX][DES_MAX_ELEMENT_S_MATRIX] = {
            {
                14, 0, 4, 15, 13, 7, 1, 4,
                2, 14, 15, 2, 11, 13, 8, 1,
                3, 10, 10, 6, 6, 12, 12, 11,
                5, 9, 9, 5, 0, 3, 7, 8,
                4, 15, 1, 12, 14, 8, 8, 2,
                13, 4, 6, 9, 2, 1, 11, 7,
                15, 5, 12, 11, 9, 3, 7, 14,
                3, 10, 10, 0, 5, 6, 0, 13
            },
                      /* S1 */
            {
                15, 3, 1, 13, 8, 4, 14, 7,
                6, 15, 11, 2, 3, 8, 4, 14,
                9, 12, 7, 0, 2, 1, 13, 10,
                12, 6, 0, 9, 5, 11, 10, 5,
                0, 13, 14, 8, 7, 10, 11, 1,
                10, 3, 4, 15, 13, 4, 1, 2,
                5, 11, 8, 6, 12, 7, 6, 12,
                9, 0, 3, 5, 2, 14, 15, 9
            },
                      /* S2 */ 
            {
                10, 13, 0, 7, 9, 0, 14, 9,
                6, 3, 3, 4, 15, 6, 5, 10,
                1, 2, 13, 8, 12, 5, 7, 14,
                11, 12, 4, 11, 2, 15, 8, 1,
                13, 1, 6, 10, 4, 13, 9, 0,
                8, 6, 15, 9, 3, 8, 0, 7,
                11, 4, 1, 15, 2, 14, 12, 3,
                5, 11, 10, 5, 14, 2, 7, 12
            },
                     /* S3 */
            {
                7, 13, 13, 8, 14, 11, 3, 5,
                0, 6, 6, 15, 9, 0, 10, 3,
                1, 4, 2, 7, 8, 2, 5, 12,
                11, 1, 12, 10, 4, 14, 15, 9,
                10, 3, 6, 15, 9, 0, 0, 6,
                12, 10, 11, 1, 7, 13, 13, 8,
                15, 9, 1, 4, 3, 5, 14, 11,
                5, 12, 2, 7, 8, 2, 4, 14
            },
                    /* S4 */
            {
                2, 14, 12, 11, 4, 2, 1, 12,
                7, 4, 10, 7, 11, 13, 6, 1,
                8, 5, 5, 0, 3, 15, 15, 10, 13,
                3, 0, 9, 14, 8, 9, 6,
                4, 11, 2, 8, 1, 12, 11, 7,
                10, 1, 13, 14, 7, 2, 8, 13,
                15, 6, 9, 15, 12, 0, 5, 9,
                6, 10, 3, 4, 0, 5, 14, 3
            },
                     /* S5 */ 
            {
                12, 10, 1, 15, 10, 4, 15, 2,
                9, 7, 2, 12, 6, 9, 8, 5,
                0, 6, 13, 1, 3, 13, 4, 14,
                14, 0, 7, 11, 5, 3, 11, 8,
                9, 4, 14, 3, 15, 2, 5, 12,
                2, 9, 8, 5, 12, 15, 3, 10,
                7, 11, 0, 14, 4, 1, 10, 7,
                1, 6, 13, 0, 11, 8, 6, 13
            },
                  /* S6 */
            {
                4, 13, 11, 0, 2, 11, 14, 7,
                15, 4, 0, 9, 8, 1, 13, 10,
                3, 14, 12, 3, 9, 5, 7, 12,
                5, 2, 10, 15, 6, 8, 1, 6,
                1, 6, 4, 11, 11, 13, 13,
                8, 12, 1, 3, 4, 7, 10, 14, 7,
                10, 9, 15, 5, 6, 0, 8, 15,
                0, 14, 5, 2, 9, 3, 2, 12
           },
                 /* S7 */
           {
                13, 1, 2, 15, 8, 13, 4, 8,
                6, 10, 15, 3, 11, 7, 1, 4,
                10, 12, 9, 5, 3, 6, 14, 11,
                5, 0, 0, 14, 12, 9, 7, 2,
                7, 2, 11, 1, 4, 14, 1, 7,
                9, 4, 12, 10, 14, 8, 2, 13,
                0, 15, 6, 12, 10, 9, 13, 0,
                15, 3, 3, 5, 5, 6, 8, 11
          }
                 /* S8 */
};
                 /* The substitution matrix required for converting the 48 bit
                  * block information into 32 bit block information
                  */
const tArBlockcipher gArBlockcipherDes = {
        8,
        8,
        DesArSetup,
        DesArEncrypt,
        DesArDecrypt
};

const tArBlockcipher gArBlockcipherTDes = {
        8,
        24,
        TDesArSetup,
        TDesArEncrypt,
        TDesArDecrypt
};
#endif
