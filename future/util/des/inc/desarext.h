/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: desarext.h,v 1.1 2015/04/28 12:53:03 siva Exp $
 *
 * Description: This file contains the externs for DES
 * module
 *********************************************************************/
#ifndef _DES_EXT_H
#define _DES_EXT_H
/********************************************************************/

extern const AR_CRYPTO_UINT8 gaArDesIP[DES_MAX_ARRAY_SIZE];
extern const AR_CRYPTO_UINT8 gaArDesFP[DES_MAX_ARRAY_SIZE];
extern const AR_CRYPTO_UINT8 gaArDesE[DES_MAX_E_ARRAY_SIZE]; 
extern const AR_CRYPTO_UINT8 gaArDesP[DES_MAX_P_ARRAY_SIZE]; 
extern const AR_CRYPTO_UINT8 gaArDesPC1[DES_MAX_PC1_ARRAY_SIZE]; 
extern const AR_CRYPTO_UINT8 gaArDesPC2[DES_MAX_PC2_ARRAY_SIZE]; 
extern const UINT4 gaArDesLShift[DES_KEY_SCHEDULE_ITERATION]; 
extern const UINT1 gaArDesSBlocks[DES_MAX_NO_OF_S_MATRIX]\
                                [DES_MAX_ELEMENT_S_MATRIX];
extern const tArBlockcipher gArBlockcipherDes;
extern const tArBlockcipher gArBlockcipherTDes;
#endif
