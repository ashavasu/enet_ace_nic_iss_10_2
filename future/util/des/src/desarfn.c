/******************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved 
 *
 * $Id: desarfn.c,v 1.1 2015/04/28 12:53:03 siva Exp $ 
 *
 * Description: This file contains the routines required for the DES/TDES
                Encryption/Decryption 
 *****************************************************************************/
#include "desarglb.h"
#include "desarinc.h"

/****************************************************************************
 * Function Name      : DesArPermute
 *
 * Description        : This routine will permute the input as defined in the
 *                      Permutation table 
 *
 * Input(s)           : u8ArCryptoInput - Information/Data which needs to be
 *                                        permuted
 *                      u1ArCryptoNBits - No of elements in the permutation 
 *                                        table
 *                      pu8ArCryptoTable - Pointer to the Permutation table.
 * Output(s)          : Permuted information/Data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE             AR_CRYPTO_UINT8
DesArPermute (AR_CRYPTO_UINT8 u8ArCryptoInput, UINT4 u4ArCryptoNBits,
              const AR_CRYPTO_UINT8 * pu8ArCryptoTable)
{
    AR_CRYPTO_UINT8     u8ArPermutedOutput = DES_INIT_ULL;
    UINT4               u4ArBitCount = CRY_INIT_VAL;

    /*Perform the Permutation of the Input Inforamtion as
     *per the Permutation Table/Matrix
     */
    for (u4ArBitCount = CRY_INIT_VAL; u4ArBitCount < u4ArCryptoNBits;
         u4ArBitCount++)
    {
        u8ArPermutedOutput =
            ((DES_U8LEFT_SHIFT (u8ArPermutedOutput, 1)) |
             ((u8ArCryptoInput & pu8ArCryptoTable[u4ArBitCount]) ?
              DES_TRUE_VAL : CRY_INIT_VAL));
    }

    /*Return the Permuted Data/Information */
    return u8ArPermutedOutput;
}

/****************************************************************************
 * Function Name      : DesArEncryptCompute
 *
 * Description        : This routine will generate the 32 bit block by
 *                      computing the provided 32 bit R block and 48 bit 
 *                      sub keys 
 *
 * Input(s)           : u4ArRblock - R block
 *                      u8ArSubKeys - Subkey
 * Output(s)          : 32 bit Block
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE             AR_CRYPTO_UINT8
DesArEncryptCompute (UINT4 u4ArRblock, AR_CRYPTO_UINT8 u8ArSubKeys)
{
    AR_CRYPTO_UINT8     u8ArPermutedData = DES_INIT_ULL;
    UINT4               u4ArCount = CRY_INIT_VAL;
    UINT4               u4ArBitCount = CRY_INIT_VAL;
    UINT4               u4ArOutputofFnRK = CRY_INIT_VAL;
    UINT1               u1ArTemp = CRY_INIT_VAL;

    /*Perform the permutation to get 48 bit data block by 
     *providing the 32 bit data block
     *This permutation is done through DesE Matrix/Table
     */
    u8ArPermutedData =
        DesArPermute ((AR_CRYPTO_UINT8) u4ArRblock,
                      DES_NO_OF_ELEMENTS (gaArDesE), gaArDesE);

    /*Xor the Permuted 48 bit dat block with the 48 bit subKeys generated 
     *through key scheduler
     */
    u8ArPermutedData = u8ArPermutedData ^ u8ArSubKeys;

    /*Perform the substitution to get 32 bit data block by 
     *using the S1-S8 Tables
     */
    for (u4ArCount = CRY_INIT_VAL,
         u4ArBitCount = (DES_KEY_NO_OF_BITS - DES_NO_OF_BITS_FOR_S_BLOCK);
         u4ArCount < DES_NO_OF_ITERATION_FOR_S_FN; u4ArCount++,
         u4ArBitCount = (u4ArBitCount - DES_NO_OF_BITS_FOR_S_BLOCK))
    {
        u1ArTemp =
            gaArDesSBlocks[u4ArCount][(DES_U8RIGHT_SHIFT (u8ArPermutedData,
                                                          u4ArBitCount)) &
                                      DES_SIX_BIT_MASK];
        u4ArOutputofFnRK =
            ((DES_U4LEFT_SHIFT (u4ArOutputofFnRK, DES_NO_OF_BYTE_IN_R_BLOCK)) |
             (u1ArTemp & DES_FOUR_BIT_MASK));
    }

    /*Perform the Permutation opertion on the 32 bit data block
     * acquired  after doing the Substition operation by using 
     * the DesP permutation Matrix
     */
    u4ArOutputofFnRK = (UINT4)
        DesArPermute (u4ArOutputofFnRK, DES_NO_OF_ELEMENTS (gaArDesP),
                      gaArDesP);

    /*Return the 32 bit data block obtain after performing the 
     *Permutation operation
     */
    return u4ArOutputofFnRK;
}

/****************************************************************************
 * Function Name      : DesArEncryptRound
 *
 * Description        : This routine will do the Enciphering Computation by 
 *                     taking the L, R blocks and the Sub key. 
 *
 * Input(s)           : pu4ArLBlock - Pointer to the L Block
 *                      pu4ArRBock - Pointer to the R Block
 *                      u8ArSKey - Subkey
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
DesArEncryptRound (UINT4 *pu4ArLBlock, UINT4 *pu4ArRBlock,
                   AR_CRYPTO_UINT8 u8ArSKey)
{
    UINT4               u4ArTempLBlock = CRY_INIT_VAL;
    UINT4               u4ArTempRBlock = CRY_INIT_VAL;
    UINT4               u4ArFunc = CRY_INIT_VAL;

    u4ArFunc = (UINT4) DesArEncryptCompute ((*pu4ArRBlock), u8ArSKey);

    /*Perform the XORing of the LBlock data with 32 bit output of 
     *DesArEncryptCompute
     */
    u4ArTempRBlock = (*pu4ArLBlock) ^ u4ArFunc;

    /*Take the LBlock for the next round equalto Rblock of thepresent Round */
    u4ArTempLBlock = (*pu4ArRBlock);

    /*Copy the calculated LBlock and RBlocks into pu4ArLBlock and
     *pu4ArRBlock pointers
     */
    *pu4ArLBlock = u4ArTempLBlock;
    *pu4ArRBlock = u4ArTempRBlock;
}

/****************************************************************************
 * Function Name      : DesArEncryptData
 *
 * Description        : This routine will encrypt the input data by using the 
 *                      sub keys.
 *
 * Input(s)           : u8ArInput - An data which needs to be encrypted by 
 *                                using DES Algorithm
 *                     pu8ArSubkey - Pointer to an data which needs to be 
                                   encrypted by using DES Algorithm
 *
 * Output(s)          : Encrypted Data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE             AR_CRYPTO_UINT8
DesArEncryptData (AR_CRYPTO_UINT8 u8ArInput,
                  const AR_CRYPTO_UINT8 * pu8ArSubkey)
{
    AR_CRYPTO_UINT8     u8ArOutput = DES_INIT_ULL;
    AR_CRYPTO_UINT8     u8ArFinalOutput = DES_INIT_ULL;
    UINT4               u4ArLBlock = CRY_INIT_VAL;
    UINT4               u4ArRBlock = CRY_INIT_VAL;
    UINT4               u4ArTempBlock = CRY_INIT_VAL;

    /*Perform the Initial Permutation on the 64 bit Input Data */
    u8ArInput =
        DesArPermute (u8ArInput, DES_NO_OF_ELEMENTS (gaArDesIP), gaArDesIP);

    /*Split the block(permuted data) into two halves. 
     *The first 32 bits are called L block,
     *and the last 32 bits are called R block 
     */

    u4ArLBlock =
        (UINT4) ((DES_U8RIGHT_SHIFT (u8ArInput, DES_NO_OF_BITS_IN_DATA_L_BLOCK))
                 & DES_ULL_MASK);
    u4ArRBlock = (UINT4) ((u8ArInput) & DES_ULL_MASK);

    /*Perform the 16 Round/Iteration with the 16 subkeys and
     *the data block
     */

    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_ZERO]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_ONE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_TWO]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_THREE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_FOUR]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_FIVE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_SIX]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_SEVEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_EIGHT]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_NINE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_TEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_ELEVEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_TWELVE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_THIRTEEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_FOURTEEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubkey[CRY_ARRAY_INDEX_FIFTEEN]);

    /*Perform the Final Permutation on the Block RL( Note
     * that block R precedes block L this time )
     */

    /* Un-swapping the halves for the last round */
    u4ArTempBlock = u4ArLBlock;
    u4ArLBlock = u4ArRBlock;
    u4ArRBlock = u4ArTempBlock;

    /*combine the R and L data block */
    u8ArOutput =
        ((DES_U8LEFT_SHIFT
          (((AR_CRYPTO_UINT8) u4ArLBlock),
           DES_NO_OF_BITS_IN_DATA_L_BLOCK)) | ((AR_CRYPTO_UINT8) u4ArRBlock));

    /*perform the final permutation of LR block by using DesFP 
     *permutation matrix
     */
    u8ArFinalOutput =
        DesArPermute (u8ArOutput, DES_NO_OF_ELEMENTS (gaArDesFP), gaArDesFP);
    return u8ArFinalOutput;
}

/****************************************************************************
 * Function Name      : DesArDecryptData
 *
 * Description        : This routine will decrypt the input ciphered data by 
 *                     using the key and will give the enciphered data as 
 *                     output.
 * Input(s)           : u8ArInput - Data/Information needs to be decrypted
 *                      pu8ArSubKey - Sub Keys
 *
 * Output(s)          : Decrypted Data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE             AR_CRYPTO_UINT8
DesArDecryptData (AR_CRYPTO_UINT8 u8ArInput,
                  const AR_CRYPTO_UINT8 * pu8ArSubKey)
{
    AR_CRYPTO_UINT8     u8ArOutput = DES_INIT_ULL;
    AR_CRYPTO_UINT8     u8ArFinalOutput = DES_INIT_ULL;
    UINT4               u4ArLBlock = CRY_INIT_VAL;
    UINT4               u4ArRBlock = CRY_INIT_VAL;
    UINT4               u4ArTBlock = CRY_INIT_VAL;

    /*Perform the Permutation on the 64 bit Input Data as per
     *the Matrix DesIp
     */
    u8ArInput =
        DesArPermute (u8ArInput, DES_NO_OF_ELEMENTS (gaArDesIP), gaArDesIP);

    /*Split the block(permuted data) into two halves. 
     *The first 32 bits are called L block,
     *and the last 32 bits are called R block 
     */
    u4ArLBlock =
        (UINT4) ((DES_U8RIGHT_SHIFT (u8ArInput, DES_NO_OF_BITS_IN_DATA_L_BLOCK))
                 & DES_ULL_MASK);
    u4ArRBlock = (UINT4) ((u8ArInput) & DES_ULL_MASK);

    /*Perform the 16 Round/Iteration with the 16 subkeys to 
     *the data block
     */
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_FIFTEEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_FOURTEEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_THIRTEEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_TWELVE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_ELEVEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_TEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_NINE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_EIGHT]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_SEVEN]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_SIX]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_FIVE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_FOUR]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_THREE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_TWO]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_ONE]);
    DesArEncryptRound (&u4ArLBlock, &u4ArRBlock,
                       pu8ArSubKey[CRY_ARRAY_INDEX_ZERO]);

    /*Perform the Final Permutation on the Block RL( Note
     *that block R precedes block L this time )
     *Un-swapping the halves for the last round
     */

    u4ArTBlock = u4ArLBlock;
    u4ArLBlock = u4ArRBlock;
    u4ArRBlock = u4ArTBlock;

    /*combine the R and L data block */
    u8ArOutput =
        ((DES_U8LEFT_SHIFT
          (((AR_CRYPTO_UINT8) u4ArLBlock),
           DES_NO_OF_BITS_IN_DATA_L_BLOCK)) | ((AR_CRYPTO_UINT8) u4ArRBlock));

    /*perform the Permutation on output block by using DesFP 
     *permutation matrix
     */
    u8ArFinalOutput =
        DesArPermute (u8ArOutput, DES_NO_OF_ELEMENTS (gaArDesFP), gaArDesFP);
    return u8ArFinalOutput;

}

/****************************************************************************
 * Function Name      : DesArSetupSubKeys
 *
 * Description        : This routine takes the key and generates the 
 *                      16 sub keys according the Key Scheduler Calculation.
 *
 * Input(s)           : pu1ArArCryptokey - Pointer to the key through which 
 *                                      Data needs to get encrypt
 *
 * Output(s)          : pu8ArCryptoSubKey - Pointer to the array of Sub keys
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
DesArSetupSubKeys (AR_CRYPTO_UINT8 * pu8ArCryptoSubKey, const UINT1 *pu1ArKey)
{
    AR_CRYPTO_UINT8     u8ArTempKey = DES_INIT_ULL;
    AR_CRYPTO_UINT8     u8ArPermutedKey = DES_INIT_ULL;
    UINT4               u4ArLBlock = CRY_INIT_VAL;
    UINT4               u4ArRBlock = CRY_INIT_VAL;
    UINT4               u4ArTemp = CRY_INIT_VAL;

    for (u4ArTemp = CRY_INIT_VAL; u4ArTemp < DES_KEY_NO_OF_BYTES; u4ArTemp++)
    {
        u8ArTempKey = ((DES_U8LEFT_SHIFT (u8ArTempKey, DES_KEY_NO_OF_BYTES)) |
                       pu1ArKey[u4ArTemp]);
    }

    /*Perform the Permutation on the Input Key as per the Matrix DesPC1 */
    u8ArPermutedKey =
        DesArPermute (u8ArTempKey, DES_NO_OF_ELEMENTS (gaArDesPC1), gaArDesPC1);

    /*Split the block(permuted key) into two halves. 
     *The first 28 bits are called L block,
     *and the last 28 bits are called R block 
     */
    u4ArLBlock =
        (UINT4) ((DES_U8RIGHT_SHIFT
                  (u8ArPermutedKey,
                   DES_NO_OF_BITS_IN_KEY_L_BLOCK)) &
                 DES_ULL_MASK_SEVEN_LSB_NIBBLES);
    u4ArRBlock = (UINT4) ((u8ArPermutedKey) & DES_ULL_MASK_SEVEN_LSB_NIBBLES);

    /*Perform the Keys schedule operations:It consisits of performing 
     *the left shift on hte L and R blocks and then permuting the LR block
     *by using the DesPC2 matrix
     */

    for (u4ArTemp = CRY_INIT_VAL; u4ArTemp < DES_KEY_SCHEDULE_ITERATION;
         u4ArTemp++)
    {
        /*Perform the left shift operation on hte L and R blocks,
         *the number of Left shift positions are defined in lshift array
         */

        u4ArLBlock =
            DES_ROTATE_LEFT (u4ArLBlock,
                             gaArDesLShift[u4ArTemp]) & DES_BLOCK_MASK;
        u4ArRBlock =
            DES_ROTATE_LEFT (u4ArRBlock,
                             gaArDesLShift[u4ArTemp]) & DES_BLOCK_MASK;

        /* Combine the L and R blocks */
        u8ArPermutedKey =
            ((DES_U8LEFT_SHIFT
              ((AR_CRYPTO_UINT8) u4ArLBlock,
               DES_NO_OF_BITS_IN_KEY_L_BLOCK)) | ((AR_CRYPTO_UINT8)
                                                  u4ArRBlock));

        /*Perform the Permutation Choice 2 on the Combined LR block to get
         *the Sub key
         */
        pu8ArCryptoSubKey[u4ArTemp] =
            DesArPermute (u8ArPermutedKey, DES_NO_OF_ELEMENTS (gaArDesPC2),
                          gaArDesPC2);
    }
}

/****************************************************************************
 * Function Name      : DesArEncrypt
 *
 * Description        : This routine will encrypt the input data by using the 
 *                      key and will give the encrypted data as output by using
 *                      the DES Algorithm..
 *
 * Input(s)           : pu1ArArCryptokey - Pointer to the key through which 
 *                                      Data needs to get encrypt
 *                      pu1ArInput - Pointer to an data which needs to be 
 *                                 encrypted by using DES Algorithm
 *
 * Output(s)          : pu1ArOutput - Pointer to the encrypted data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
DesArEncrypt (const unArCryptoKey * punArCryptoSubKey,
              const UINT1 *pu1ArInput, UINT1 *pu1ArOutput)
{
    AR_CRYPTO_UINT8     u8ArInputData = DES_INIT_ULL;
    UINT4               u4ArTemp = CRY_INIT_VAL;

    /*Copy the Data that needs to encrypt into u8ArInputData variable */
    for (u4ArTemp = CRY_INIT_VAL, u8ArInputData = DES_INIT_ULL;
         u4ArTemp < DES_DATA_NO_OF_BYTES; u4ArTemp++)
    {
        u8ArInputData =
            ((DES_U8LEFT_SHIFT (u8ArInputData, DES_DATA_NO_OF_BYTES)) |
             pu1ArInput[u4ArTemp]);
    }
    /*Call the function DesArEncryptData to encrypt the Data */
    u8ArInputData =
        DesArEncryptData (u8ArInputData, punArCryptoSubKey->tArDes.au8ArSubkey);

    for (u4ArTemp = CRY_INIT_VAL; u4ArTemp < DES_DATA_NO_OF_BYTES; u4ArTemp++)
    {
        pu1ArOutput[DES_DATA_LAST_BUT_ONE_BYTE - u4ArTemp] =
            (UINT1) (u8ArInputData & DES_BYTE_MASK);
        u8ArInputData =
            (DES_U8RIGHT_SHIFT (u8ArInputData, DES_DATA_NO_OF_BYTES));
    }
}

/****************************************************************************
 * Function Name      : DesArDecrypt
 *
 * Description        : This routine will decrypt the input ciphered data by
                        using the key and will give the deciphered data as 
                        output.
 * Input(s)           : pu1ArArCryptokey - Pointer to the key through which 
 *                                      Data needs to get encrypt
 *                      pu1ArInput - Pointer to an data which needs to be 
 *                                 encrypted by using DES Algorithm
 *
 * Output(s)          : pu1ArOutput - Pointer to the encrypted data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
DesArDecrypt (const unArCryptoKey * punArCryptoSubKey,
              const UINT1 *pu1ArInput, UINT1 *pu1ArOutput)
{
    AR_CRYPTO_UINT8     u8ArInputData = DES_INIT_ULL;
    UINT4               u4ArTemp = CRY_INIT_VAL;

    /*Copy the Data that needs to decrypt into u8ArInputData variable */
    for (u4ArTemp = CRY_INIT_VAL; u4ArTemp < DES_DATA_NO_OF_BYTES; u4ArTemp++)
    {
        u8ArInputData =
            ((DES_U8LEFT_SHIFT (u8ArInputData, DES_DATA_NO_OF_BYTES)) |
             pu1ArInput[u4ArTemp]);
    }

    /*Call a function to decrypt the Data */
    u8ArInputData = DesArDecryptData (u8ArInputData,
                                      punArCryptoSubKey->tArDes.au8ArSubkey);

    for (u4ArTemp = CRY_INIT_VAL; u4ArTemp < DES_DATA_NO_OF_BYTES; u4ArTemp++)
    {
        pu1ArOutput[DES_DATA_LAST_BUT_ONE_BYTE - u4ArTemp] =
            (UINT1) (u8ArInputData & DES_BYTE_MASK);
        u8ArInputData = u8ArInputData >> DES_DATA_NO_OF_BYTES;
    }
}

/****************************************************************************
 * Function Name      : DesArSetup
 *
 * Description        : This routine is called to generate the 16 Sub keys
 *
 * Input(s)           : pu1ArKey - Input key through which Data needs to get 
 *                               encrypt
 *
 * Output(s)          : punArCryptoSubKey - Sub keys
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
DesArSetup (unArCryptoKey * punArCryptoSubKey, const UINT1 *pu1ArKey)
{
    /*Call a function to generate the 16 Sub keys */

    DesArSetupSubKeys (punArCryptoSubKey->tArDes.au8ArSubkey, pu1ArKey);
}

/****************************************************************************
 * Function Name      : TDesArSetup
 *
 * Description        : This routine is called to generate a set of three 
 *                      16 Sub keys.
 *
 * Input(s)           : pu1ArKey - Input key through which Data needs to get 
 *                               encrypt
 *
 * Output(s)          : punArCryptoSubKey - Sub keys
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
TDesArSetup (unArCryptoKey * punArCryptoSubKey, const UINT1 *pu1ArKey)
{
    /*Call functions to generate a set of 16 Sub keys */

    /*First Key will be the first 8 bytes of given input key */
    DesArSetupSubKeys (punArCryptoSubKey->tArTdes.ArKey0.au8ArSubkey, pu1ArKey);

    /*Second Key will be the second 8 bytes of given input key */
    DesArSetupSubKeys (punArCryptoSubKey->tArTdes.ArKey1.au8ArSubkey,
                       pu1ArKey + DES_KEY_NO_OF_BYTES);

    /*Third Key will be the last 8 bytes of given input key */
    DesArSetupSubKeys (punArCryptoSubKey->tArTdes.ArKey2.au8ArSubkey,
                       pu1ArKey + (DES_MULTIPLY_TWO * DES_KEY_NO_OF_BYTES));
}

/****************************************************************************
 * Function Name      : TDesArEncrypt
 *
 * Description        : This routine will encrypt the input data by using the
 *                      key and will give the encrypted data as output by using
 *                      the TDES Algorithm..
 * Input(s)           : pu1ArInput- Pointer to an data which needs to be 
 *                                 encrypted by using DES Algorithm
 *                      punArCryptoSubKey- Key with which the data need to be
 *                                      encrypted
 *
 * Output(s)          :pu1ArOutput- Pointer to the encrypted data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
TDesArEncrypt (const unArCryptoKey * punArCryptoSubKey,
               const UINT1 *pu1ArInput, UINT1 *pu1ArOutput)
{
    AR_CRYPTO_UINT8     u8ArData = DES_INIT_ULL;
    UINT4               u4ArCount = CRY_INIT_VAL;

    for (u4ArCount = CRY_INIT_VAL;
         u4ArCount < DES_DATA_NO_OF_BYTES; u4ArCount++)
    {
        u8ArData =
            DES_U8LEFT_SHIFT (u8ArData,
                              DES_DATA_NO_OF_BYTES) | pu1ArInput[u4ArCount];
    }
    /*Each TDES-Encryption Operation is
     *compounded Encryption-Decryption Encryption Operation
     */

    /*call a function for Encryption */
    u8ArData =
        DesArEncryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey0.au8ArSubkey);

    /*call a function for Decryption */
    u8ArData =
        DesArDecryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey1.au8ArSubkey);

    /*call a function for Encryption */
    u8ArData =
        DesArEncryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey2.au8ArSubkey);

    for (u4ArCount = CRY_INIT_VAL; u4ArCount < DES_DATA_NO_OF_BYTES;
         u4ArCount++)
    {
        pu1ArOutput[DES_DATA_LAST_BUT_ONE_BYTE - u4ArCount] =
            (UINT1) (u8ArData & DES_BYTE_MASK);
        u8ArData = u8ArData >> DES_DATA_NO_OF_BYTES;
    }
}

/****************************************************************************
 * Function Name      : TDesArDecrypt
 *
 * Description        : This routine will decrypt the input ciphered data by 
 *                     using the key and will give the deciphered data as 
 *                     output..
 * Input(s)           : pu1ArInput - Pointer to an data which needs to be 
 *                                 encrypted by using DES Algorithm
 *                      punArCryptoSubKey- Key with which the data need to be 
 *                                      encrypted
 *
 * Output(s)          :pu1ArOutput - Pointer to the encrypted data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
TDesArDecrypt (const unArCryptoKey * punArCryptoSubKey,
               const UINT1 *pu1ArInput, UINT1 *pu1ArOutput)
{
    AR_CRYPTO_UINT8     u8ArData = DES_INIT_ULL;
    UINT4               u4ArCount = CRY_INIT_VAL;

    for (u4ArCount = CRY_INIT_VAL;
         u4ArCount < DES_DATA_NO_OF_BYTES; u4ArCount++)
    {
        u8ArData =
            ((DES_U8LEFT_SHIFT (u8ArData, DES_DATA_NO_OF_BYTES)) |
             pu1ArInput[u4ArCount]);
    }

    /*Each TDES-Encryption Operation is
     *compounded Encryption-Decryption Encryption Operation
     */

    /*call a function for Decryption */
    u8ArData =
        DesArDecryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey2.au8ArSubkey);

    /*call a function for Encryption */
    u8ArData =
        DesArEncryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey1.au8ArSubkey);

    /*call a function for Decryption */
    u8ArData =
        DesArDecryptData (u8ArData,
                          punArCryptoSubKey->tArTdes.ArKey0.au8ArSubkey);

    for (u4ArCount = CRY_INIT_VAL; u4ArCount < DES_DATA_NO_OF_BYTES;
         u4ArCount++)
    {
        pu1ArOutput[DES_DATA_LAST_BUT_ONE_BYTE - u4ArCount] =
            (UINT1) (u8ArData & DES_BYTE_MASK);
        u8ArData = u8ArData >> DES_DATA_NO_OF_BYTES;
    }
}

/****************************************************************************
              End of File desarfn.c
****************************************************************************/
