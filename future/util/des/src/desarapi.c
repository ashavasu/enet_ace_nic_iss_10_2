/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: desarapi.c,v 1.1 2015/04/28 12:53:03 siva Exp $
 *
 * Description: This file contains the API code for DES module
 *********************************************************************/
#include "desarinc.h"
#include "lr.h"
#include "fsutltrc.h"
#include "trace.h"

extern UINT4        gu4UtlTrcFlag;

/*******************************************************************************
 * Function           : DesArKeyScheduler 
 *
 * Description        : This function is used to create the Key Schedule for DES
 *
 * Input(s)           : pu1ArUserKey - Input Key
 *                      u1ArKeyLength - Key Length
 * 
 * Output(s)          : punArCryptoKey - Key Schedule
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
DesArKeyScheduler (const UINT1 *pu1ArUserKey, unArCryptoKey * punArCryptoKey)
{
    if ((pu1ArUserKey == NULL) || (punArCryptoKey == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to DesArKeyScheduler\n");
#endif
        return DES_FAILURE;
    }

    /* Call the Des Setup routine for the Key Scheduling */
    DesArSetup (punArCryptoKey, pu1ArUserKey);
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : DesArCbcEncrypt 
 *
 * Description        :This Function is called by the Modules which Requires the
 *                     data to be Encrypted by CBC DES.
 *                     This function is used for encrypting the data by using 
 *                     the sub keys generated through the KeySchedulingfunction.
 *                     This function will give the encrypted data C mode
 *
 * Input(s)           : pu1ArBuffer - Pointer to the Data that needs to encrypt
 *                      u4ArSize - Length of the Data
 *                      punArCryptoKey - Pointer to the Sub keys
 *                      pu1ArInitVect - Pointer to the Initialization vector
 *
 * Output(s)          : pu1ArData - Encrypted Data
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
DesArCbcEncrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize,
                 unArCryptoKey * punArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArDesBlockcipher = NULL;
    UINT1              *pu1ArText = NULL;
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (punArCryptoKey == NULL) ||
        (pu1ArInitVect == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to DesArCbcEncrypt\n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "NULL Pointers passed to "
                  "DesArCbcEncrypt\n");
        /* Return Failure if Buffer  ir Kery or IV pointers are is Null */
        return DES_FAILURE;
    }

    /* Select the Des Blockcipher */
    pArDesBlockcipher = &gArBlockcipherDes;
    u4ArInitVectSize = pArDesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)
    {
        pu1ArText = pu1ArBuffer + u4ArOffset;
        for (u4ArLoopVar = CRY_INIT_VAL; u4ArLoopVar < u4ArInitVectSize;
             u4ArLoopVar++)
        {
            pu1ArText[u4ArLoopVar] =
                pu1ArInitVect[u4ArLoopVar] ^ pu1ArText[u4ArLoopVar];
        }
        pArDesBlockcipher->ArCryptoEncrypt (punArCryptoKey, pu1ArText,
                                            pu1ArText);
        u4ArSize = u4ArSize - (pArDesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, pu1ArText, u4ArInitVectSize);
        u4ArOffset = u4ArOffset + (pArDesBlockcipher->u4ArBlocksize);
    }
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : DesArCbcDecrypt 
 *
 * Description        :This Function is called by the Modules which Requires the
 *                     data to be Encrypted by CBC DES.
 *                     This function is used for encrypting the data by using
 *                     the sub keys generated through the KeySchedulingfunction.
 *                     This function will give the encrypted data C mode
 *
 * Input(s)           : pu1ArBuffer - Pointer to the Data that needs to decrypt
 *                      u4Ar444Size - Length of the Data
 *                      punArCryptoKey - Pointer to the Sub keys
 *                      pu1ArInitVect - Pointer to the Initialization vector
 * 
 * Output(s)          : pu1ArData - Decrypted Data
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
DesArCbcDecrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize,
                 unArCryptoKey * punArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArDesBlockcipher = NULL;
    UINT1              *pu1ArText = NULL;
    UINT1               au1ArTemp[DES_DATA_NO_OF_BYTES] = { CRY_INIT_VAL };
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (punArCryptoKey == NULL)
        || (pu1ArInitVect == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to DesArCbcDecrypt\n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "NULL Pointers passed to "
                  "DesArCbcDecrypt\n");
        return DES_FAILURE;
    }
    pArDesBlockcipher = &gArBlockcipherDes;
    u4ArInitVectSize = pArDesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)

    {
        pu1ArText = pu1ArBuffer + u4ArOffset;
        CRY_MEMCPY (au1ArTemp, pu1ArText, u4ArInitVectSize);
        pArDesBlockcipher->ArCryptoDecrypt (punArCryptoKey,
                                            pu1ArText, pu1ArText);
        for (u4ArLoopVar = CRY_INIT_VAL; u4ArLoopVar < u4ArInitVectSize;
             u4ArLoopVar++)

        {
            pu1ArText[u4ArLoopVar] =
                pu1ArInitVect[u4ArLoopVar] ^ pu1ArText[u4ArLoopVar];
        }
        u4ArSize = u4ArSize - (pArDesBlockcipher->u4ArBlocksize);
        u4ArOffset = u4ArOffset + (pArDesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, au1ArTemp, u4ArInitVectSize);
    }
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : TDesArKeySchedule 
 *
 * Description        : This function is used to create the KeySchedule for TDES
 *
 * Input(s)           : pu1ArUserKey - Input Key
 * 
 * Output(s)          : punArCryptoKey - Key Schedule
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
TDesArKeySchedule (const UINT1 *pu1ArUserKey, unArCryptoKey * punArCryptoKey)
{
    if ((pu1ArUserKey == NULL) || (punArCryptoKey == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to TDesArKeySchedule");
#endif
        return DES_FAILURE;
    }

    /* Call the TDes Setup routine for the Key Scheduling */
    TDesArSetup (punArCryptoKey, pu1ArUserKey);
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : TDesArCbcEncrypt 
 *
 * Description        :This Function is called by the Modules which Requires
 *                     the data to be Encrypted by TCBC DES.
 *                     This function is used for encrypting the data by using 
 *                     the 3 set of sub keys (used for encryption-decryption-
 *                     encryption des) generated through the Key Scheduling 
 *                     function.
 *
 * Input(s)           : pu1ArBuffer -Pointer to the Data that needs to encrypt
 *                      u4ArSize - Length of the Data
 *                      punArCryptoKey - Pointer to the Sub keys
 *                      pu1ArInitVect - Pointer to the Initialization vector
 * 
 * Output(s)          : pu1ArData - Encrypted Data
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
TDesArCbcEncrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize,
                  unArCryptoKey * punArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArTDesBlockcipher = NULL;
    UINT1              *pu1ArText = NULL;
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (punArCryptoKey == NULL)
        || (pu1ArInitVect == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to TDesArCbcEncrypt");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "NULL Pointers passed to "
                  "TDesArCbcEncrypt\n");
        /* Return Failure if Buffer  ir Kery or IV pointers are is Null */
        return DES_FAILURE;
    }

    /* Select the Des Blockcipher */
    pArTDesBlockcipher = &gArBlockcipherTDes;
    u4ArInitVectSize = pArTDesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)

    {
        pu1ArText = pu1ArBuffer + u4ArOffset;
        for (u4ArLoopVar = CRY_INIT_VAL; u4ArLoopVar < u4ArInitVectSize;
             u4ArLoopVar++)

        {
            pu1ArText[u4ArLoopVar] =
                pu1ArInitVect[u4ArLoopVar] ^ pu1ArText[u4ArLoopVar];
        }
        pArTDesBlockcipher->ArCryptoEncrypt (punArCryptoKey, pu1ArText,
                                             pu1ArText);
        u4ArSize = u4ArSize - (pArTDesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, pu1ArText, u4ArInitVectSize);
        u4ArOffset = u4ArOffset + (pArTDesBlockcipher->u4ArBlocksize);
    }
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : TDesArCbcDecrypt 
 *
 * Description        :This Function is called by the Modules which Requires
 *                     the data to be Encrypted by CBC DES.
 *                     This function is used for encrypting the data by using
 *                     the subkeys generated through the KeySchedulingfunction
 *                     This function will give the encrypted data C mode
 *
 * Input(s)           :pu1ArBuffer - Pointer to the Data that needs to decrypt
 *                     u4ArSize - Length of the Data
 *                     punArCryptoKey - Pointer to the Sub keys
 *                     pu1ArInitVect - Pointer to the Initialization vector
 * 
 * Output(s)          : pu1ArData - Decrypted Data
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
TDesArCbcDecrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize,
                  unArCryptoKey * punArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArTDesBlockcipher = NULL;
    UINT1              *pu1ArText = NULL;
    UINT1               au1ArTemp[DES_DATA_NO_OF_BYTES] = { CRY_INIT_VAL };
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (punArCryptoKey == NULL)
        || (pu1ArInitVect == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to TDesArCbcDecrypt");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "NULL Pointers passed to "
                  "TDesArCbcDecrypt\n");
        return DES_FAILURE;
    }
    pArTDesBlockcipher = &gArBlockcipherTDes;
    u4ArInitVectSize = pArTDesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)

    {
        pu1ArText = (pu1ArBuffer + u4ArOffset);
        CRY_MEMCPY (au1ArTemp, pu1ArText, u4ArInitVectSize);
        pArTDesBlockcipher->ArCryptoDecrypt (punArCryptoKey, pu1ArText,
                                             pu1ArText);
        for (u4ArLoopVar = CRY_INIT_VAL; u4ArLoopVar < u4ArInitVectSize;
             u4ArLoopVar++)

        {
            pu1ArText[u4ArLoopVar] =
                pu1ArInitVect[u4ArLoopVar] ^ pu1ArText[u4ArLoopVar];
        }
        u4ArSize = u4ArSize - (pArTDesBlockcipher->u4ArBlocksize);
        u4ArOffset = u4ArOffset + (pArTDesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, au1ArTemp, u4ArInitVectSize);

    }
    return DES_SUCCESS;
}

/*******************************************************************************
 * Function           : DesArSubKeys 
 *
 * Description        : This function is used to create the Key Schedule for DES
 *                      and TDES
 *
 * Input(s)           : punArCryptoKey - Input Key
 * 
 * Output(s)          : pu8ArCryptoSubKey - Key Schedule
 *
 * Returns            : DES_SUCCESS/DES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
DesArSubKeys (AR_CRYPTO_UINT8 * pu8ArCryptoSubKey, const UINT1 *punArCryptoKey)
{
    if ((pu8ArCryptoSubKey == NULL) || (punArCryptoKey == NULL))

    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL Pointers passed to DesArSubKeys\n");
#endif
        return DES_FAILURE;
    }
/*Sub keys will generated by using the given input key */
    DesArSetupSubKeys (pu8ArCryptoSubKey, punArCryptoKey);

    return DES_SUCCESS;
}

/****************************************************************************
              End of File desarapi.c
****************************************************************************/
