######################################################################
# Copyright (C) 2008 Aricent Inc . All Rights Reserved                #
# ------------------------------------------                          #
# $Id: make.h,v 1.3 2015/04/28 12:51:03 siva Exp $                     #
#    DESCRIPTION            : Specifies the options and modules to be #
#                             included for building the DES module    #
#######################################################################
include ../../LR/make.h
include ../../LR/make.rule
DES_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                ${SYSTEM_COMPILATION_SWITCHES}
COMN_INCL_DIR = ${BASE_DIR}/inc
CRYPTO_DES_BASE_DIR = ${BASE_DIR}/util/des
CRYPTO_DES_SRC_DIR = $(CRYPTO_DES_BASE_DIR)/src
CRYPTO_DES_INC_DIR = $(CRYPTO_DES_BASE_DIR)/inc
CRYPTO_DES_OBJ_DIR = $(CRYPTO_DES_BASE_DIR)/obj
CRYPTO_INCLUDES = -I$(CRYPTO_DES_INC_DIR) -I$(COMN_INCL_DIR) -I ${COMMON_INCLUDE_DIRS}
