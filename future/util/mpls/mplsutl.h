/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved 
 *
 * $Id: mplsutl.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: Prototypes of MPLS utilitiy functions.
 *******************************************************************/

#ifndef _MPLSUTL_H_
#define _MPLSUTL_H_
VOID
MplsUtlFillMplsHdr (tMplsHdr *pHdr, tCRU_BUF_CHAIN_HEADER *pOutBuf);
VOID
MplsUtlFillMplsAchHdr (tMplsAchHdr *pAchHdr, 
tCRU_BUF_CHAIN_HEADER *pOutBuf);
VOID
MplsUtlFillMplsAchTlvHdr (tMplsAchTlvHdr *pAchTlvHdr,
                           tCRU_BUF_CHAIN_HEADER *pOutBuf);
VOID
MplsUtlFillMplsAchTlv (tMplsAchTlv *pAchTlv,
                        tCRU_BUF_CHAIN_HEADER *pOutBuf);
#endif
