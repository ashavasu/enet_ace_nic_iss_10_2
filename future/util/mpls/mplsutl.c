/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved 
 *
 * $Id: mplsutl.c,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: Utility Routines for MPLS.
 *******************************************************************/
#include "lr.h"
#include "ipv6.h"
#include "mpls.h"
#include "mplsapi.h"
#include "mplsutl.h"

/* MPLS utility functions */
/*****************************************************************************/
/* Function     : MplsUtlFillMplsHdr                                        */
/*                                                                           */
/* Description  : Write the complete MPLS header in the given location       */
/*                                                                           */
/* Input        : pHdr - Pointer to the MPLS header                          */
/*                                                                           */
/* Output       : pOutBuf - Pointer to the buffer in                         */
/*                            which headers to be stored                     */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MplsUtlFillMplsHdr (tMplsHdr * pHdr, tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    UINT4               u4SrcVal = 0;
    UINT4               u4DstVal = 0;
    /*
     *  MPLS Header: 
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Label
     |                Label                  | Exp |S|       TTL     | Stack
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ Entry
     */
    /* 1. Check CRU buffer is linear or not
     * 2. Prepend the data in the CRU buff
     *
     * */

    if ((pHdr == NULL) || (pOutBuf == NULL))
    {
        return;
    }
    u4SrcVal = ((pHdr->u4Lbl << 12) & 0xfffff000) |
        ((pHdr->Exp << 9) & 0x00000e00) |
        ((pHdr->SI << 8) & 0x00000100) | (pHdr->Ttl & 0x000000ff);
    PTR_ASSIGN4 (&u4DstVal, u4SrcVal);
    CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4DstVal, sizeof (UINT4));
    return;
}

/*****************************************************************************/
/* Function     : MplsUtlFillMplsAchHdr                                     */
/*                                                                           */
/* Description  : Write the complete MPLS ACH header in the given location   */
/*                                                                           */
/* Input        : pAchHdr - Pointer to the ACH header                        */
/*                                                                           */
/* Output       : pOutBuf - Pointer to the buffer in                         */
/*                          which headers to be stored                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MplsUtlFillMplsAchHdr (tMplsAchHdr * pAchHdr, tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    UINT4               u4SrcVal = 0;
    UINT4               u4DstVal = 0;
    /*
     *  MPLS ACH Header: 
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |0 0 0 1|Version|   Reserved    |         Channel Type          |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */

    if ((pAchHdr == NULL) || (pOutBuf == NULL))
    {
        return;
    }

    u4SrcVal = ((pAchHdr->u4AchType << 28) & 0xf0000000) |
        ((pAchHdr->u1Version << 24) & 0x0f000000) |
        ((pAchHdr->u1Rsvd << 16) & 0x00ff0000) |
        (pAchHdr->u2ChannelType & 0x0000ffff);
    PTR_ASSIGN4 (&u4DstVal, u4SrcVal);
    CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4DstVal, sizeof (UINT4));
    return;
}

/*****************************************************************************/
/* Function     : MplsUtlFillMplsAchTlvHdr                                  */
/*                                                                           */
/* Description  : Write the complete MPLS ACH header in the given location   */
/*                                                                           */
/* Input        : pAchTlvHdr - Pointer to the ACH TLV header                 */
/*                                                                           */
/* Output       : pOutBuf - Pointer to the buffer in                         */
/*                          which headers to be stored                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MplsUtlFillMplsAchTlvHdr (tMplsAchTlvHdr * pAchTlvHdr,
                          tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    UINT4               u4SrcVal;
    UINT4               u4DstVal;
    /*
     *  MPLS ACH TLV Header: 
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |          Length               |            Reserved           |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */

    if ((pAchTlvHdr == NULL) || (pOutBuf == NULL))
    {
        return;
    }

#if 0

    u4SrcVal = (UINT4) ((((UINT4) pAchTlvHdr->u2TlvHdrLen << 16) & 0xffff0000) |
                        (pAchTlvHdr->u2Rsvd & 0x0000ffff));

#else

    u4SrcVal =
        (UINT4) ((((UINT4) (pAchTlvHdr->u2TlvHdrLen) << 16) & 0xffff0000) |
                 (pAchTlvHdr->u2Rsvd & 0x0000ffff));

#endif

    PTR_ASSIGN4 (&u4DstVal, u4SrcVal);
    CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4DstVal, sizeof (UINT4));
    return;
}

/*****************************************************************************/
/* Function     : MplsUtlFillMplsAchTlv                                     */
/*                                                                           */
/* Description  : Write the complete MPLS ACH TLV in the given location      */
/*                                                                           */
/* Input        : pAchTlv - Pointer to the ACH TLV                           */
/*                                                                           */
/* Output       : pOutBuf - Pointer to the buffer in                         */
/*                          which headers to be stored                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MplsUtlFillMplsAchTlv (tMplsAchTlv * pAchTlv, tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;
    UINT1               au1Pad[MPLS_MEG_ID_LENGTH] = { 0 };
    UINT1               u1IccLen = 0;
    UINT1               u1UmcLen = 0;
    UINT1               u1Pad = 0;

    MEMSET (&au1Pad, 0, MPLS_MEG_ID_LENGTH);
    /* As per MPLS-TP ACH TLV draft version 02

       Name       Type  Length    Description                  Reference
       (octets)
       Null        0      3       Null TLV                     This Draft
       SAv4        1      4       IPv4 Source Address          This Draft
       SAv6        2     16       IPv6 Source Address          This Draft
       ICC         3      6       ITU-T Carrier Code           This Draft
       Global_ID   4      4       Global Identifier            This Draft
       IF_ID       5      8       Network Interface ID         This Draft
       Auth        6     var      Authentication               This Draft

     */

    if ((pAchTlv == NULL) || (pOutBuf == NULL))
    {
        return;
    }

    switch (pAchTlv->u2TlvType)
    {
        case MPLS_ACH_NULL_TLV:
            /* 1 byte extra for padding */
            u4Val = 0;
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_SOURCE_V4_ADDR_TLV:
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.SrcIpAddr.u4_addr[0]);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_SOURCE_V6_ADDR_TLV:
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.
                                      SrcIpAddr.u1_addr[0], IP6_ADDR_SIZE);
            break;
        case MPLS_ACH_ICC_ID_TLV:
            /* 2 bytes extra for Padding */
            u2Val = 0;
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &u2Val, sizeof (UINT2));

            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.au1Icc[0],
                                      MPLS_ICC_LENGTH);
            break;
        case MPLS_ACH_GLOBAL_ID_TLV:
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.u4GlobalId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_IF_ID_TLV:
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.IfId.u4IfNum);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));

            u4Val = OSIX_HTONL (pAchTlv->unAchValue.IfId.u4NodeId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_AUTH_TLV:
            /*TODO AuthData is 2 bytes value here, to be changed in future */
            u2Val = OSIX_HTONS (pAchTlv->unAchValue.AuthTlv.u2AuthData);
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &u2Val, sizeof (UINT2));
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.
                                      AuthTlv.u1AuthLen, sizeof (UINT1));

            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.
                                      AuthTlv.u1AuthType, sizeof (UINT1));
            break;
        case MPLS_ACH_MIP_TLV:
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MipTlv.u4IfNum);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MipTlv.
                                GlobalNodeId.u4NodeId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MipTlv.
                                GlobalNodeId.u4GlobalId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_IP_LSP_MEP_TLV:
            u2Val = OSIX_HTONS (pAchTlv->unAchValue.MepTlv.
                                unMepTlv.IpLspMepTlv.u2LspNum);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u2Val,
                                      sizeof (UINT2));
            u2Val = OSIX_HTONS (pAchTlv->unAchValue.MepTlv.
                                unMepTlv.IpLspMepTlv.u2TnlNum);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u2Val,
                                      sizeof (UINT2));
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IpLspMepTlv.GlobalNodeId.u4NodeId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IpLspMepTlv.GlobalNodeId.u4GlobalId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            break;
        case MPLS_ACH_IP_PW_MEP_TLV:
            /* AC ID */
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IpPwMepTlv.u4AcId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            /* Node ID */
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IpPwMepTlv.u4NodeId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            /* Global ID */
            u4Val = OSIX_HTONL (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IpPwMepTlv.u4GlobalId);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u4Val,
                                      sizeof (UINT4));
            /* TODO AGI value to appended with padding 
             * if 4 byte alignment is required */
            /* AGI value */
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.MepTlv.
                                      unMepTlv.IpPwMepTlv.au1Agi,
                                      pAchTlv->unAchValue.MepTlv.unMepTlv.
                                      IpPwMepTlv.u1AgiLen);
            /* AGI length */
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.MepTlv.
                                      unMepTlv.IpPwMepTlv.u1AgiLen,
                                      sizeof (UINT1));
            /* AGI Type */
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.MepTlv.
                                      unMepTlv.IpPwMepTlv.u1AgiType,
                                      sizeof (UINT1));
            break;
        case MPLS_ACH_ICC_MEP_TLV:
            u1Val = 0;
            /* 1 byte padding */
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u1Val,
                                      sizeof (UINT1));
            /* MEP index */
            u2Val = OSIX_HTONS (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                IccMepTlv.u2MepIndex);
            CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u2Val,
                                      sizeof (UINT2));

            /* Calculate the length of ICC and UMC */
            u1UmcLen = (UINT1) STRLEN (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                       IccMepTlv.au1Umc);
            u1IccLen = (UINT1) STRLEN (pAchTlv->unAchValue.MepTlv.unMepTlv.
                                       IccMepTlv.au1Icc);

            /* The MEG ID is added to the packet which is the combination of 
             * ITU-T Carrier code (ICC) and Unique MEG Code (UMC). The MEG ID 
             * is 13 character consisting of ICC, immediately following UMC 
             * with trailing nulls.
             */
            if ((u1IccLen + u1UmcLen) < (MPLS_MEG_ID_LENGTH))
            {
                /* Calculate the Pad count if the ICC and UMC is lesser than
                 * 13 bytes and append the remaining as trailing NULLs */
                u1Pad = (UINT1) (MPLS_MEG_ID_LENGTH - (u1IccLen + u1UmcLen));
                CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &au1Pad[0], u1Pad);
            }

            /* Prepend the UMC */
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.MepTlv.
                                      unMepTlv.IccMepTlv.au1Umc[0], u1UmcLen);
            /* Prepend the ICC */
            CRU_BUF_Prepend_BufChain (pOutBuf,
                                      (UINT1 *) &pAchTlv->unAchValue.MepTlv.
                                      unMepTlv.IccMepTlv.au1Icc[0], u1IccLen);

            break;
        default:
            return;
            break;
    }
    /* Prepend the ACH type and ACH TLV header length */
    u2Val = OSIX_HTONS (pAchTlv->u2TlvLen);
    CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u2Val, sizeof (UINT2));
    u2Val = OSIX_HTONS (pAchTlv->u2TlvType);
    CRU_BUF_Prepend_BufChain (pOutBuf, (UINT1 *) &u2Val, sizeof (UINT2));
    return;
}
