/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triecidr.h,v 1.13 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains define / typedefs / macros related  to 
 *             RFC 2096 and  RFC 1519.                         
 *
 *******************************************************************/
#ifndef _TRIECIDR_H
#define _TRIECIDR_H

#define   NET_OFFSET      0
#define   MASK_OFFSET     4
#define   TOS_OFFSET      8
#define   NHOP_OFFSET    12

#endif          /*_TRIECIDR_H */
