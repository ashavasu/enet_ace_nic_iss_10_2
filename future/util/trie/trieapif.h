/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieapif.h,v 1.20 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains interface functions related            
 *             declarations, data structure definitions        
 *             and funtions prototype.                         
 *
 *******************************************************************/

#ifndef _TRIEAPIF_H
#define _TRIEAPIF_H

#include "triecidr.h"

#define TRIE_SUCCESS      0
#define TRIE_FAILURE    (-1)

/* configurable variables */
/* MAX_NUM_TRIE = (MAX_OSPF_CONTEXT * NO_OF_TRIE_INSTANCES_IN_OSPF) 
 *                 +(MAX_RIP_CONTEXT * NO_OF_TRIE_INSTANCES_IN_RIP)
 *                 + 10 = (255*3)+(255 * 1) +10 = 1030) */

/* change MAX_NUM_TRIE if any new trie instance
 * is created as part of MI support */ 

#define   MAX_KEY_SIZE    64
#define   MAX_NUM_KEYS     4

#define MAX_PATHS        64

/* variables used by TrieConfigParams structure */
#define   NUM_1ST_KEY        TRIE_MAX_RTS

#define   KEY1_SIZE                   8
#define   KEY2_SIZE                  16
#define   KEY3_SIZE                  32
#define   KEY4_SIZE                  64

#define   NUM_2ND_KEY                 0
#define   NUM_3RD_KEY                 0
#define   NUM_4TH_KEY                 0
#define   INVALID_NEXTHOP    0xffffffff
#define   TRIE_SEM_COUNT              1

#define   TRIE_SEM_NAME      (const UINT1 *)"TRIE"
#define   TRIE_SEM_FLAGS     OSIX_DEFAULT_SEM_MODE
#define   TRIE_SEM_NODE_ID   SELF


/* typedef declarations */

typedef union _Key {
    UINT1  *pKey;
    UINT4  u4Key;
} tKey;

typedef struct _ScanOutParams {
    void   *pAppSpecInfo;
    tKey   *pKey;
    UINT4  u4NumEntries;
} tScanOutParams;

typedef struct _InputParams {
    void   *pRoot;
    INT1   i1AppId;
    UINT1  u1Reserved1;
    UINT1  u1Reserved2;
    UINT1  u1Reserved3;
    tKey   Key;
} tInputParams;

typedef struct _OutputParams {
    void   *pAppSpecInfo;
    tKey   Key;
    UINT4  au4NoOfMultipath[MAX_APPLICATIONS];
    VOID  *pObject1;
    VOID  *pObject2;
    UINT1  u1LeafFlag;
    UINT1  u1Reserved;
    UINT2  u2Reserved;
} tOutputParams;

typedef tScanOutParams tDeleteOutParams;

typedef UINT4 *tSemId;

typedef struct _RadixNode
{
    struct _RadixNode *pParent;
    tKey        Mask;
    UINT1       u1NodeType;
    UINT1       u1Reserved1;
    UINT1       u1ByteToTest;
    UINT1       u1BitToTest;
    void        *pLeft;
    void        *pRight;
} tRadixNode;

typedef struct _RadixNodeHead
{
    tRadixNode      *pRadixNodeTop;
    UINT4           u4Type;
    UINT2           u2AppMask;
    UINT1           u1KeySize;
    UINT1           u1Reserved;
} tRadixNodeHead;

typedef struct _LeafNode
{
    tRadixNode  *pParent;
    tKey        Mask;
    UINT1       u1NodeType;
    UINT1       u1NumApp;
    UINT2       u2AppMask;
    void        *pAppSpecInfo;
    tKey        Key;
    void        *pListNode;
    UINT4       au4NoOfMultipath[MAX_APPLICATIONS];  
    UINT1       u1CurApp;
    UINT1       u1ReservedByte;
    UINT2       u2ReservedWord;
} tLeafNode;
                
/* For every route added in trie, increment the multipath value
 * by MAX_PATHS. Currently multipaths for any route can be 64
 * which means 2^6. So, 6 bits to be masked for every retrieval
 * operation. c0 means 6 bits will be masked and since UINT4 can
 * take 4 bytes, the mask is 0xffffffc0 */

#define   TRIE_INIT_ROUTES_BITMASK(u4NoOfMultipath)   u4NoOfMultipath = 0
#define TRIE_INIT_EQUAL_COST_ROUTES(u4NoOfMultipath) \
        u4NoOfMultipath = u4NoOfMultipath & 0xffffff80;
#define TRIE_INC_TOTAL_NUM_OF_ROUTES(u4NoOfMultipath) \
        u4NoOfMultipath += 128
#define TRIE_INC_EQUAL_COST_ROUTES(u4NoOfMultipath) \
        u4NoOfMultipath += 1
#define TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE(u4NoOfMultipath) \
        { \
        u4NoOfMultipath = u4NoOfMultipath & 0xffffff80; \
        u4NoOfMultipath += 1; \
        }

#define TRIE_DEC_TOTAL_NUM_OF_ROUTES(u4NoOfMultipath) \
        {\
         if(u4NoOfMultipath >= 128)\
         {\
         u4NoOfMultipath -= 128;\
         }\
        } 

#define TRIE_DEC_NUM_EQUAL_COST_ROUTES(u4NoOfMultipath) \
        {\
         if(u4NoOfMultipath > 0)\
         {\
           u4NoOfMultipath -= 1;\
         }\
        }

#define TRIE_COPY_TOTAL_NUM_OF_ROUTES(u1BitMask, u4NumOfMultipath) \
        u4NumOfMultipath = (UINT1)(u1BitMask >> 7)

#define TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES(u1BitMask, u4NumOfMultipath) \
        u4NumOfMultipath = (u1BitMask & 0x7f )

typedef struct _TrieApp
{
    void      *pNextApp;
    void      *pNextAlternatepath;  
    UINT4     u4Net;
    UINT4     u4Mask;
    UINT4     u4Tos;
    UINT4     u4NxtHop;
    UINT4     u4RtIfIndx;
    UINT2     u2RtType;
    UINT2     u2RtProto;
    UINT4     u4RtAge;
    UINT4     u4RtNxtHopAs;
    UINT4     u4RtMetr1;
    UINT4     u4RowStatus;
} tTrieApp;
              
typedef struct _TrieConfigParams
{
    UINT4     u4NumRadixNodes;
    UINT4     u4NumLeafNodes;
    UINT4     au4NumKey[MAX_KEY_SIZE + 1];
} tTrieConfigParams;

#define  COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT(pOutputParams,pDeleteOutParams,u4TmpNumEntries) \
   while (((tTrieApp *)pOutputParams->pAppSpecInfo) != NULL) {\
         ((tTrieApp **) pDeleteOutParams->pAppSpecInfo)[u4TmpNumEntries++] = pOutputParams->pAppSpecInfo;\
            pOutputParams->pAppSpecInfo = ((tTrieApp *)pOutputParams->pAppSpecInfo)->pNextAlternatepath;\
               }

INT4 TrieMemCreateMemPool ARG_LIST ((UINT4 u4ObjSize, UINT4 u4ObjCount));

void TrieMemDeleteMemPool ARG_LIST ((INT4 i4QueueId));


INT4 TrieGetLeafInfo ARG_LIST ((tInputParams *, tOutputParams *));

INT4 TrieUpdate ARG_LIST ((tInputParams *, void *, UINT4, tOutputParams *));

/* Exported Functions */

INT4 TrieInit     ARG_LIST ((void));   
void TrieShutDown ARG_LIST ((void));   

INT4 TrieCreate (tCreateParams * pCreateParams, void *pRoot);

INT4 TrieDelete ARG_LIST ((tInputParams * pInputParams,
                           void (*AppSpecDelFunc) (tDeleteOutParams *),
                           tDeleteOutParams * pDeleteOutParams));
INT4 TrieAddEntry    ARG_LIST((tInputParams  *pInputParams,   
                               void          *pAppSpecInfo,   
                               tOutputParams *pOutputParams));   
INT4 TrieDeleteEntry ARG_LIST((tInputParams  *pInputParams,   
                               tOutputParams *pOutputParams,   
                               UINT4          u4NxtHop));   

INT4 TrieSearchEntry ARG_LIST((tInputParams  *pInputParams,   
                               tOutputParams *pOutputParams));   

INT4 TrieLookupEntry ARG_LIST((tInputParams  *pInputParams,   
                               tOutputParams *pOutputParams));   

INT4 TrieScan ARG_LIST((tInputParams   *pInputParams,   
                        INT4           (*AppSpecScanFunc)(tScanOutParams *),   
                        tScanOutParams *pScanOutParams));   

INT4 TrieGetNextEntry ARG_LIST((tInputParams  *pInputParams,   
                                tOutputParams *pOutputParams)); 

INT4 TrieScanGetNextEntry ARG_LIST((tInputParams  *pInputParams,   
                                tOutputParams *pOutputParams)); 
#endif /* _TRIEAPIF_H */

