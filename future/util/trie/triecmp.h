/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triecmp.h,v 1.13 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains macros related to key of Trie 
 *             library.
 *
 *******************************************************************/
#ifndef _TRIECMP_H
#define _TRIECMP_H

/* this header file has some macros which are not following every  
 * coding guidelines recommended in the document 001cg001.doc.
 * these macros are little less readable but they are not modified as
 * modifying them also reduces the efficieny of these macros.
 * As these macros are in the critical path, they should be  
 * written in the optimal manner. 
 */

#define DIFFBYTE(Key1, Key2, u1Size, u4Byte, u1Diff)\
 if ((u1Size) == U4_SIZE) {DIFF4BYTE(Key1.u4Key, Key2.u4Key, u4Byte, u1Diff);}\
 else               {DIFFNBYTE(Key1.pKey, Key2.pKey, u1Size, u4Byte, u1Diff);}

#define DIFF4BYTE(u4Key1, u4Key2, u4Byte, u1Diff) {u4Byte = (u4Key1 ^ u4Key2);\
    u4Byte =  (u1Diff  = (UINT1)(u4Byte >> 24)) ? 0 :\
              ((u1Diff = (UINT1)(u4Byte >> 16)) ? 1 :\
              ((u1Diff = (UINT1)(u4Byte >> 8))  ? 2 :\
              ((u1Diff = (UINT1)(u4Byte))       ? 3 : 4)));}

#define DIFFNBYTE(pKey1, pKey2, u1Size, u4Byte, u1Diff)\
   for (u4Byte = 0; (u4Byte < (UINT4) u1Size) &&\
                     !(u1Diff = (pKey1[u4Byte] ^ pKey2[u4Byte])); u4Byte++);

#define DIFFBIT(u1Bit, u1Diff) for(u1Bit = 0; !(u1Diff >> (7-u1Bit)); u1Bit++)

#define KEYCMP(Key1, Key2, u1Size)  (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key < (Key2).u4Key) ? -1 : 1) :\
         memcmp ((Key1).pKey, (Key2).pKey, (u1Size)))

#define KEYEQUAL(Key1, Key2, u1Size) (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key == (Key2).u4Key) ? 0 : 1) :\
         (memcmp ((Key1).pKey, (Key2).pKey, (u1Size))))

#define GETBYTE(Key, u1ByteNum, u1Size) (UINT1) (((u1Size) == U4_SIZE) ?\
 ((Key).u4Key >> BYTE_LENGTH*(U4_SIZE - 1 - u1ByteNum)) :\
 ((Key).pKey [u1ByteNum]))

#endif /* __TRIECMP_H__ */
