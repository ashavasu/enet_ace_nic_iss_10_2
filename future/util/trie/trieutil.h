/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieutil.h,v 1.16 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains internal functions related constants   
 *             macros, function prototypes.                  
 *
 *******************************************************************/
#ifndef _TRIEUTIL_H
#define _TRIEUTIL_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "trieapif.h"
#include "triecmp.h"

/* constant declaration */
#define   RADIX_NODE     0
#define   LEAF_NODE      1
#define   U4_SIZE        4

/* error numbers */
#define   LEAF_ALLOC_FAIL          0
#define   RADIX_ALLOC_FAIL         1
#define   KEY_ALLOC_FAIL           2
#define   MAX_KEY_SIZE_OVERFLOW    3
#define   MAX_APP_OVERFLOW         4
#define   MAX_INSTANCE_OVERFLOW    5
#define   SEM_CREATE_FAIL          6
#define   ALREADY_USED_APP_ID      7
#define   MEM_ALLOC_FAIL           8

/* extern declaration */
extern INT4 gi4RadixPool;
extern INT4 gi4LeafPool;
extern INT4 gai4KeyPoolIdx[MAX_KEY_SIZE + 1];

extern tTrieConfigParams gConfigParams;
extern tRadixNodeHead    gaTrieFamily[MAX_NUM_TRIE];

/* macro declaration */
#define   BIT_TEST(a, b)                  (a) & (b)
#define   SET_BIT(u1AppId, u2AppMask)     (u2AppMask) =(UINT2)((u2AppMask)| 0x0001 << (u1AppId))
#define   RESET_BIT(i1AppId, u2AppMask)   (u2AppMask) =(UINT2)((u2AppMask)& ~(0x0001 << (i1AppId)))
#define   CHECK_BIT(i4AppId, u2AppMask)   ((u2AppMask) & (0x0001 << (i4AppId)))

#define KEYCOPY(Dst, Src, u1Size) do {             \
    if ((u1Size) > U4_SIZE)                        \
        MEMCPY ((Dst).pKey, (Src).pKey, (u1Size)); \
    else                                           \
       (Dst).u4Key = (Src).u4Key;                  \
    } while (0)


/* function prototypes */
void  TrieInitMemManager ARG_LIST ((void));
void  TrieMemFree ARG_LIST ((INT4 i4QueueId, UINT1 * pu1BuffAddr));
VOID *TrieMemAlloc ARG_LIST ((INT4 i4QueueId));
tRadixNodeHead *TrieSelectInstance ARG_LIST ((UINT4 u4Type, UINT1 u1KeySize));
INT4 TrieTraverse ARG_LIST ((UINT1 u1KeySize,
                             tRadixNode * pRoot, tKey Key, void *pNode));

INT4 TrieAddLeaf ARG_LIST ((UINT1 u1KeySize,
                            tInputParams * pInputParams,
                            tTrieApp * pAppSpecInfo,
                            tRadixNode * pParentNode,
                            tOutputParams * pOutputParams));

INT4 TrieAddRadix ARG_LIST ((UINT1 u1KeySize,
                             UINT1 u1BitToTest,
                             UINT1 u1ByteToTest,
                             tInputParams * pInputParams,
                             tTrieApp * pAppSpecInfo,
                             tRadixNode * pParentNode,
                             tOutputParams * pOutputParams));

INT4 TrieDeleteAppAndUpdateLeaf ARG_LIST ((UINT1 u1KeySize,
                                           tInputParams * pInputParams,
                                           tLeafNode * pLeafNode,
                                           tOutputParams * pOutputParams,
                                           UINT4 NxtHop));

void TrieDeleteRadixLeaf ARG_LIST ((UINT1 u1KeySize,
                                    tLeafNode * pLeafNode,
                                    tOutputParams * pOutputParams,
                                    UINT1 u1AppId));

void TrieAssignMask ARG_LIST ((UINT1 u1KeySize, void *pNode));

INT4 TrieFindBestMatch ARG_LIST ((UINT1 u1KeySize,
                                  tInputParams * pInputParams,
                                  void *pNode, tOutputParams * pOutputParams));

INT4 TrieFindBestMatch4 ARG_LIST ((UINT1 u1KeySize,
                                   tInputParams * pInputParams,
                                   void *pNode, tOutputParams * pOutputParams));

void *TrieAllocateNode ARG_LIST ((UINT1 u1KeySize, UINT1 u1NodeType));

void TrieErrorReport ARG_LIST ((UINT1 u1NumError));

void TrieInitVariables ARG_LIST ((void));

INT4 TrieGetLinkListIndex (INT1 i1AppId,
                           UINT2 u2AppMask, UINT1 * pu1LinkListIndex);

UINT4 TrieGetBitDifference (UINT1 u1NumBytes,
                            UINT1 u1MaskFlag,
                            UINT1 * pu1Key1, UINT1 * pu1Key2);

INT4 TrieScanAll ARG_LIST ((tInputParams * pInputParams,
                           INT4 (*AppSpecScanFunc) (tScanOutParams *),
                           tScanOutParams * pScanOutParams));


#endif /* _TRIEUTIL_H */
