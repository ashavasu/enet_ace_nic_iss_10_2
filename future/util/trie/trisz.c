/*$Id: trisz.c,v 1.1 2015/04/28 12:51:06 siva Exp $*/
#define _TRISZ_C
#include "trieinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
TriSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRI_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsTRISizingParams[i4SizingId].u4StructSize,
                                     FsTRISizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(TRIMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            TriSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TriSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTRISizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TRIMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TriSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRI_MAX_SIZING_ID; i4SizingId++)
    {
        if (TRIMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TRIMemPoolIds[i4SizingId]);
            TRIMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
