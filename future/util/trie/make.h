# Copyright (C) 2006 Aricent Inc . All Rights Reserved

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
              ${CONFIG_OPNS} ${DUMP_OPNS} \
              ${PROT_TASK_OPNS} \
              ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES = $(COMMON_INCLUDE_DIRS)
#############################################################################

