/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triemem.c,v 1.13 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains functions related to memory            
 *             management of Trie library.          
 *
 *******************************************************************/
#include "trieutil.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
#define NUM_POOLS         100
typedef struct _MemPoolNode
{
    UINT1              *pNext;
}
tMemPoolNode;
typedef struct _MemPool
{
    UINT1              *pu1Base;
    UINT1              *pu1Head;
    INT4                i4NxtIdx;
}
tMemPool;
static tMemPool     aMemPool[NUM_POOLS];
static INT4         i4FreeIdx;

/**************************************************************************/

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieInitMemManager ()                                    */
    /*                                                                         */
    /*  Description   This function initialises memory module of Trie          */
    /*                library.                                                 */
    /*                                                                         */
    /*                                                                         */
    /*  Call          This function should be called exactly once              */
    /*  condition     before using any other functions related to memory       */
    /*                management.                                              */
    /*                                                                         */
    /*  Input(s)      None.                                                    */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        TrieInit ().                                             */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void
TrieInitMemManager ()
#else /*  */
void
TrieInitMemManager ()
#endif                            /*  */
{
    INT4                i4Idx;
    for (i4Idx = 0; i4Idx < NUM_POOLS; i4Idx++)

    {
        aMemPool[i4Idx].i4NxtIdx = i4Idx + 1;
    }
    i4FreeIdx = 0;
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieMemCreateMemPool ()                                  */
    /*                                                                         */
    /*  Description   This function creates a buffer pool of size u4ObjSize    */
    /*                and u4ObjCount. It also returns an index to the          */
    /*                created buffer pool if successful.                       */
    /*                                                                         */
    /*  Call          This function is called to create memory pool.           */
    /*                                                                         */
    /*  Input(s)      u4ObjSize -        Size of the object.                   */
    /*                u4ObjCount -       Number of the object.                 */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*                                                                         */
    /*  Return        An index for the memory pool if                          */
    /*                successful.                                              */
    /*                TRIE_FAILURE - A negative number is returned.                 */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
INT4
TrieMemCreateMemPool (UINT4 u4ObjSize, UINT4 u4ObjCount)
#else /*  */
INT4
TrieMemCreateMemPool (u4ObjSize, u4ObjCount)
     UINT4               u4ObjSize;
     UINT4               u4ObjCount;

#endif /*  */
{
    UINT4               u4RetVal;
    tMemPoolId          i4PoolId;
    u4RetVal = MemCreateMemPool (u4ObjSize, (u4ObjSize * u4ObjCount),
                                 MEM_DEFAULT_MEMORY_TYPE, &i4PoolId);
    if (u4RetVal == (UINT4) MEM_FAILURE)
    {
        return (-1);
    }
    return (INT4)i4PoolId;
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieMemDeleteMemPool ()                                  */
    /*                                                                         */
    /*  Description   This function frees the memory occupied by the memory    */
    /*                pool. The index of the memory pool is given by           */
    /*                i4PoolId.                                                */
    /*                                                                         */
    /*                                                                         */
    /*  Input(s)      i4PoolId - Memory pool identifier.                       */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*                                                                         */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS - If successful.                                 */
    /*                TRIE_FAILURE - Otherwise.                                     */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void
TrieMemDeleteMemPool (INT4 i4PoolId)
#else /*  */
void
TrieMemDeleteMemPool (i4PoolId)
     INT4                i4PoolId;

#endif /*  */
{
    MemDeleteMemPool ((UINT4) i4PoolId);
    i4PoolId = 0;
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieiMemAlloc ()                                             */
    /*                                                                         */
    /*  Description   This function returns a pointer to a buffer maintained   */
    /*                in the queue identified by i4PoolId.                     */
    /*                                                                         */
    /*                                                                         */
    /*  Input(s)      i4PoolId -      Pool identifier.                         */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*                                                                         */
    /*  Return        Pointer to the created object, if successful.            */
    /*                NULL - Otherwise.                                        */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
VOID               *
TrieMemAlloc (INT4 i4PoolId)
#else /*  */
VOID               *
TrieMemAlloc (i4PoolId)
     INT4                i4PoolId;

#endif /*  */
{
    UINT1              *pu1Obj = NULL;

    pu1Obj = MemAllocMemBlk ((UINT4)i4PoolId);
    return ((VOID *) pu1Obj);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieMemFree ()                                              */
    /*                                                                         */
    /*  Description   This function frees the memory pointed by pu1BuffAddr.   */
    /*                If it is successful, it has a return value TRIE_SUCCESS. */
    /*                                                                         */
    /*  Input(s)      i4PoolId -    Pool identifier.                           */
    /*                pu1BuffAddr - Pointer to buffer address.                 */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*                                                                         */
    /*  Return        None.                                                    */
 /***************************************************************************/

#ifdef __STDC__
void
TrieMemFree (INT4 i4PoolId, UINT1 *pu1Obj)
#else /*  */
void
TrieMemFree (i4PoolId, pu1Obj)
     INT4                i4PoolId;
     UINT1              *pu1Obj;

#endif /*  */
{
    MemReleaseMemBlock ((UINT4)i4PoolId, pu1Obj);
}
