/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieutil.c,v 1.20 2015/10/23 07:38:40 siva Exp $
 *
 * Description: Contains internal functions of Trie Library. 
 *
 *******************************************************************/
#include "trieinc.h"

 /***************************************************************************/
    /*                                                                         */
    /*  Function      INT4 TrieTraverse ()                                     */
    /*                                                                         */
    /*  Description   This function traverse an instance of Trie pointed to    */
    /*                by the parameter pRoot. When it reaches the leaf node    */
    /*                or NULL, it returns TRIE_SUCCESS or TRIE_FAILURE respectively.     */
    /*                In case of TRIE_FAILURE, it also returns the pointer to the   */
    /*                parent node. In case of TRIE_SUCCESS, it returns the pointer  */
    /*                to the leaf node.                                        */
    /*                                                                         */
    /*  Call          When the functions want to search for the key.           */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize      Size of the key.                          */
    /*                pRoot -        Pointer to the tRadixNode structure.      */
    /*                pKey -         Pointer to the key.                       */
    /*                                                                         */
    /*  Output(s)     pNode -        Pointer to the  chosen node.              */
    /*                                                                         */
    /*  Access        TrieAddEntry (), TrieDeleteEntry (),TrieLookupEntry ()   */
    /*  privileges    TrieSearchEntry ().                                      */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS - If the leaf node is found.                     */
    /*                TRIE_FAILURE - Otherwise.                                     */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
INT4
TrieTraverse (UINT1 u1KeySize, tRadixNode * pRoot, tKey Key, void *pNode)
#else /*  */
INT4
TrieTraverse (u1KeySize, pRoot, Key, pNode)
     UINT1               u1KeySize;
     tRadixNode         *pRoot;
     tKey                Key;
     void               *pNode;

#endif /*  */
{
    UINT1               u1ByteVal;
    tRadixNode         *pCurrNode;
    tRadixNode         *pTmpParent;
    pCurrNode = pTmpParent = pRoot;
    while ((pCurrNode != NULL) && (pCurrNode->u1NodeType == RADIX_NODE))

    {
        pTmpParent = pCurrNode;
        u1ByteVal = (UINT1) GETBYTE (Key, pCurrNode->u1ByteToTest, u1KeySize);
        pCurrNode = (BIT_TEST (u1ByteVal, pCurrNode->u1BitToTest)) ?
            (tRadixNode *) pCurrNode->pRight : (tRadixNode *) pCurrNode->pLeft;
    }

    /* fails in finding the nearest leaf node */
    if (pCurrNode == NULL)

    {
        *((tRadixNode **) pNode) = pTmpParent;
        return (TRIE_FAILURE);
    }
    *((tLeafNode **) pNode) = (tLeafNode *) pCurrNode;
    return (TRIE_SUCCESS);
}

/****************************************************************************/
    /*                                                                         */
    /*  Function      INT4 TrieAddLeaf (                                       */
    /*                UINT1            u1KeySize,                              */
    /*                tInputParams     *pInputParams,                          */
    /*                tTrieApp         *pAppSpecInfo,                          */
    /*                tRadixNode       *pParentNode)                           */
    /*                                                                         */
    /*  Description   This function creates the leaf node. It copies from      */
    /*                tInputParams to relevant fields of the leaf node. It     */
    /*                also assigns the parent pointer of the leaf node to      */
    /*                the pParentNode.                                         */
    /*                                                                         */
    /*  Call          When a leaf node is to be added.                         */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize       Size of the key.                         */
    /*                pInputParams -  Pointer to the tInputParams structure.   */
    /*                pAppSpecInfo -  Pointer to the application specific      */
    /*                                information.                             */
    /*                pParentNode -   Pointer to the parent node.              */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        TrieAddEntry (), TrieAddRadix ()                         */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS - If successful creation of the leaf node.       */
    /*                TRIE_FAILURE - Otherwise.                                     */
    /*                                                                         */
 /***************************************************************************/
#ifdef __STDC__
INT4
TrieAddLeaf (UINT1 u1KeySize, tInputParams * pInputParams,
             tTrieApp * pAppSpecInfo, tRadixNode * pParentNode,
             tOutputParams * pOutputParams)
#else /*  */
INT4
TrieAddLeaf (u1KeySize, pInputParams, pAppSpecInfo, pParentNode, pOutputParams)
     UINT1               u1KeySize;
     tInputParams       *pInputParams;
     tTrieApp           *pAppSpecInfo;
     tRadixNode         *pParentNode;
     tOutputParams      *pOutputParams;

#endif /*  */
{
    UINT1               u1ByteVal;
    UINT1               u1AppId;
    UINT1               u1Index;
    tLeafNode          *pLeafNode;
    u1AppId = (UINT1) pInputParams->i1AppId;
    pLeafNode = (tLeafNode *) TrieAllocateNode (u1KeySize, LEAF_NODE);
    if (pLeafNode == NULL)

    {
        return (TRIE_FAILURE);
    }
    ((tTrieApp *) pAppSpecInfo)->pNextApp = NULL;
    ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = NULL;
    pLeafNode->pAppSpecInfo = pAppSpecInfo;
    pLeafNode->u1NumApp = 1;
    pLeafNode->u2AppMask = 0;
    pLeafNode->u1CurApp = u1AppId;
    pOutputParams->pObject1 = pLeafNode;
    pOutputParams->pObject2 = NULL;

   /******** V.N. Starts *******/
    /* Intialize the number of paths available for each protocol for this leaf */
    for (u1Index = 0; u1Index < MAX_APPLICATIONS; u1Index++)

    {
        TRIE_INIT_ROUTES_BITMASK (pLeafNode->au4NoOfMultipath[u1Index]);
    }
    TRIE_INC_TOTAL_NUM_OF_ROUTES (pLeafNode->au4NoOfMultipath[u1AppId]);
    if (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE)

    {
        TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE (pLeafNode->au4NoOfMultipath
                                               [u1AppId]);
    }

   /******** V.N. ENDS *******/
    pLeafNode->Mask.u4Key = pAppSpecInfo->u4Mask;
    SET_BIT (u1AppId, pLeafNode->u2AppMask);
    KEYCOPY (pLeafNode->Key, pInputParams->Key, u1KeySize);

    /* pointer updation */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u1KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))

    {
        pParentNode->pRight = (void *) pLeafNode;
    }

    else

    {
        pParentNode->pLeft = (void *) pLeafNode;
    }
    pLeafNode->pParent = pParentNode;
    return (TRIE_SUCCESS);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieAddRadix ()                                          */
    /*                                                                         */
    /*  Description   This function creates the radix node. It modifies the    */
    /*                pointers to reflect this change. It also creates a       */
    /*                leaf node by calling another function. If it fails due   */
    /*                to resource problems, it returns TRIE_FAILURE.                */
    /*                                                                         */
    /*  Call          When a radix node needs to be added.                     */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize       Size of the key.                         */
    /*                u1BitToTest -   Bit mask of the selected byte.           */
    /*                u1ByteToTest -  Selected Byte.                           */
    /*                pInputParams -  Pointer to the tInputParams structure.   */
    /*                pAppSpecInfo -  Pointer to the application specific      */
    /*                                information.                             */
    /*                pParentNode -   Pointer to the parent node.              */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*                                                                         */
    /*  Access        TrieAddEntry ().                                         */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS - If successful creation of the radix node.      */
    /*                TRIE_FAILURE - Otherwise.                                     */
    /*                                                                         */
 /***************************************************************************/
#ifdef __STDC__
INT4
TrieAddRadix (UINT1 u1KeySize, UINT1 u1BitToTest, UINT1 u1ByteToTest,
              tInputParams * pInputParams, tTrieApp * pAppSpecInfo,
              tRadixNode * pParentNode, tOutputParams * pOutputParams)
#else /*  */
INT4
TrieAddRadix (u1KeySize, u1BitToTest, u1ByteToTest, pInputParams, pAppSpecInfo,
              pParentNode, pOutputParams)
     UINT1               u1KeySize;
     UINT1               u1BitToTest;
     UINT1               u1ByteToTest;
     tInputParams       *pInputParams;
     tTrieApp           *pAppSpecInfo;
     tRadixNode         *pParentNode;
     tOutputParams      *pOutputParams;

#endif /*  */
{
    UINT1               u1ByteVal;
    INT4                i4Cmp;
    tRadixNode         *pRadixNode;
    void               *pTmpNode;
    pRadixNode = (tRadixNode *) TrieAllocateNode (u1KeySize, RADIX_NODE);
    if (pRadixNode == NULL)

    {
        return TRIE_FAILURE;
    }
    pRadixNode->u1ByteToTest = u1ByteToTest;
    pRadixNode->u1BitToTest = u1BitToTest;

    /* add the leaf node */
    if ((TrieAddLeaf
         (u1KeySize, pInputParams, pAppSpecInfo, pRadixNode,
          pOutputParams)) == TRIE_FAILURE)

    {
        TrieMemFree (gi4RadixPool, (UINT1 *) pRadixNode);
        return (TRIE_FAILURE);
    }
    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u1KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))

    {
        pTmpNode = pParentNode->pRight;
        pParentNode->pRight = pRadixNode;
    }

    else

    {
        pTmpNode = pParentNode->pLeft;
        pParentNode->pLeft = pRadixNode;
    }
    pRadixNode->pParent = pParentNode;

    /* update the pointers in the child of new radix node */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pRadixNode->u1ByteToTest, u1KeySize);
    if (BIT_TEST (u1ByteVal, pRadixNode->u1BitToTest))

    {
        pRadixNode->pLeft = pTmpNode;
    }

    else

    {
        pRadixNode->pRight = pTmpNode;
    }
    i4Cmp = KEYCMP (((tRadixNode *) pRadixNode->pLeft)->Mask,
                    ((tRadixNode *) pRadixNode->pRight)->Mask, U4_SIZE);
    if (i4Cmp < 0)

    {
        pRadixNode->Mask = ((tRadixNode *) pRadixNode->pLeft)->Mask;
    }

    else

    {
        pRadixNode->Mask = ((tRadixNode *) pRadixNode->pRight)->Mask;
    }
    ((tRadixNode *) pTmpNode)->pParent = pRadixNode;

    /* update the mask 
     * Here TrieAssignMask () is supposed to be called with u1KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieAssignMask (U4_SIZE, pRadixNode->pParent);
    return (TRIE_SUCCESS);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieDeleteAppAndUpdateLeaf ()                            */
    /*                                                                         */
    /*  Description   This function deletes the application specific           */
    /*                information. It updates the leaf node to reflect this    */
    /*                deletion. It also refreshes the mask of the upper        */
    /*                level nodes by calling another function.                 */
    /*                                                                         */
    /*  Call          When application specific information                    */
    /*  condition     needs to be deleted from the leaf                        */
    /*                node.                                                    */
    /*                                                                         */
    /*  Input(s)      u1AppId -        Application identifier.                 */
    /*                u1KeySize -      Size of the key.                        */
    /*                pLeafNode -      Pointer to the tLeafNode to be          */
    /*                                 deleted.                                */
    /*                                                                         */
    /*  Output(s)     pOutputParams -  Pointer to the tOutputParams            */
    /*                                 structure.                              */
    /*                                                                         */
    /*  Access        TrieDeleteEntry (), TrieDelete ().                       */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
INT4
TrieDeleteAppAndUpdateLeaf (UINT1 u1KeySize, tInputParams * pInputParams,
                            tLeafNode * pLeafNode,
                            tOutputParams * pOutputParams, UINT4 NxtHop)
#else /*  */
INT4
TrieDeleteAppAndUpdateLeaf (u1KeySize, pInputParams, pLeafNode, pOutputParams,
                            NxtHop)
     UINT1               u1KeySize;
     tInputParams       *pInputParams;
     tLeafNode          *pLeafNode;
     tOutputParams      *pOutputParams;
     UINT4               NxtHop;

#endif /*  */
{
    INT4                i4Idx;
    UINT4               u4BestMetric;
    tTrieApp          **pCurrApp;
    tTrieApp           *pPrevPath;
    tTrieApp           *pTmpApp;
    UINT4               u4NumOfMultipath;
    UINT1               u1AppMask;
    UINT1               u1AppId;
    UINT1               u1RtType;
    u1AppId = (UINT1) pInputParams->i1AppId;

    /* reaching to the entry of interest */
    pCurrApp = (tTrieApp **) (VOID *) &(pLeafNode->pAppSpecInfo);
    for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)

    {
        pCurrApp = CHECK_BIT (i4Idx, pLeafNode->u2AppMask) ?
            (tTrieApp **) (VOID *) &((*pCurrApp)->pNextApp) : pCurrApp;
    }
    if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

    {
        KEYCOPY (pOutputParams->Key, pLeafNode->Key, u1KeySize);
    }

    /* Request for deleting all the routes for a particular application */
    if (NxtHop == INVALID_NEXTHOP)

    {
        pOutputParams->pAppSpecInfo = (void *) (*pCurrApp);
        TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeafNode->au4NoOfMultipath[u1AppId],
                                       pOutputParams->
                                       au4NoOfMultipath[u1AppId]);
        (*pCurrApp) = (tTrieApp *) (*pCurrApp)->pNextApp;

        /* update the leaf node information */
        pLeafNode->u1NumApp--;
        TRIE_INIT_ROUTES_BITMASK (pLeafNode->au4NoOfMultipath[u1AppId]);
        RESET_BIT ((INT1) u1AppId, pLeafNode->u2AppMask);
        if ((u1AppId == pLeafNode->u1CurApp) && (pLeafNode->pListNode != NULL))

        {
            if ((u1AppMask = (UINT1) (pLeafNode->u2AppMask)) != 0)

            {

                do

                {
                    pLeafNode->u1CurApp++;
                }
                while ((pLeafNode->u1CurApp & u1AppMask) != 0);
                pOutputParams->pObject1 = pLeafNode->pListNode;
                pOutputParams->pObject2 = NULL;
            }
        }
        return TRIE_SUCCESS;
    }

    else

    {

        /* Request for deleting a single route with the given next hop */
        pPrevPath = (tTrieApp *) (*pCurrApp);
        pTmpApp = (tTrieApp *) (*pCurrApp);
        if (pTmpApp == NULL)
        {
            return TRIE_FAILURE;
        }
        u4BestMetric = pTmpApp->u4RtMetr1;
        if (pTmpApp->u4NxtHop == NxtHop)

        {
            TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeafNode->
                                           au4NoOfMultipath[u1AppId],
                                           u4NumOfMultipath);
            if (u4NumOfMultipath == 1)

            {                    /* Only one routing entry available */
                if (((u1AppId == (OSPF_RT_ID - 1)) ||
                     (u1AppId == (OSPF_CAL_RT_ID - 1))))

                {
                    u1RtType = pInputParams->u1Reserved1;
                    if (u1RtType != ((UINT1) pTmpApp->u2RtType))

                    {

                        /* Only one routing entry available
                         * whose route type doesn't match 
                         * the incoming route type */
                        return TRIE_FAILURE;
                    }
                }
                pOutputParams->pAppSpecInfo = (void *) (*pCurrApp);
                (*pCurrApp) = (tTrieApp *) (*pCurrApp)->pNextApp;

                /* update the leaf node information */
                pLeafNode->u1NumApp--;
                RESET_BIT ((INT1) u1AppId, pLeafNode->u2AppMask);
                if ((u1AppId == pLeafNode->u1CurApp) &&
                    (pLeafNode->pListNode != NULL))

                {
                    if ((u1AppMask = (UINT1) (pLeafNode->u2AppMask)) != 0)

                    {

                        do

                        {
                            pLeafNode->u1CurApp++;
                        }
                        while ((pLeafNode->u1CurApp & u1AppMask) != 0);
                        pOutputParams->pObject1 = pLeafNode->pListNode;
                        pOutputParams->pObject2 = NULL;
                    }
                }
                if (u1AppId < MAX_APPLICATIONS)
                {
                    TRIE_INIT_EQUAL_COST_ROUTES (pLeafNode->au4NoOfMultipath
                                                 [u1AppId]);
                }
            }

            else

            {

                /* If the searching entry with NEXT_HOP and METRIC 
                 * happens to be in the start of the List , now we 
                 * will search for route type in the available list */
                if (((u1AppId == (OSPF_RT_ID - 1)) ||
                     (u1AppId == (OSPF_CAL_RT_ID - 1))))

                {
                    u1RtType = pInputParams->u1Reserved1;
                    if (u1RtType == ((UINT1) pTmpApp->u2RtType))

                    {

                        /* The Searching RtType is the 
                         * first entry in the List 
                         * Updating pLeafNode->pNextApp with 
                         * pCurrApp->pNextAlternatepath
                         * */
                        pOutputParams->pAppSpecInfo = (void *) (*pCurrApp);
                        (*pCurrApp) =
                            ((tTrieApp *) * pCurrApp)->pNextAlternatepath;
                        (*pCurrApp)->pNextApp = pPrevPath->pNextApp;
                        TRIE_DEC_NUM_EQUAL_COST_ROUTES
                            (pLeafNode->au4NoOfMultipath[u1AppId]);
                    }

                    else

                    {

                        /*   The Searching RtType is NOT the 
                         *   first entry in the List    */
                        pPrevPath = pTmpApp;
                        pTmpApp = pTmpApp->pNextAlternatepath;
                        while (pTmpApp != NULL)

                        {
                            if (u1RtType == ((UINT1) pTmpApp->u2RtType))

                            {
                                pOutputParams->pAppSpecInfo = (void *) pTmpApp;
                                pPrevPath->pNextAlternatepath =
                                    pTmpApp->pNextAlternatepath;
                                TRIE_DEC_NUM_EQUAL_COST_ROUTES (pLeafNode->
                                                                au4NoOfMultipath
                                                                [u1AppId]);
                                break;
                            }
                            pPrevPath = pTmpApp;
                            pTmpApp = pTmpApp->pNextAlternatepath;
                        }
                        if (pTmpApp == NULL)

                        {
                            return TRIE_FAILURE;
                        }
                    }

                    /* Decrement total num of routes */
                    TRIE_DEC_TOTAL_NUM_OF_ROUTES (pLeafNode->
                                                  au4NoOfMultipath[u1AppId]);
                    return TRIE_SUCCESS;
                }

                else

                {

                    /* Member to be deleted is first. Number of entries more than one */
                    pOutputParams->pAppSpecInfo = (void *) (*pCurrApp);
                    (*pCurrApp) = ((tTrieApp *) * pCurrApp)->pNextAlternatepath;
                    if ((*pCurrApp) == NULL)
                    {
                        return TRIE_FAILURE;
                    }
                    (*pCurrApp)->pNextApp = pPrevPath->pNextApp;
                    pTmpApp = (*pCurrApp);
                    if ((u1AppId == pLeafNode->u1CurApp) &&
                        (pLeafNode->pListNode != NULL))

                    {
                        pOutputParams->pObject1 = pLeafNode->pListNode;
                        pOutputParams->pObject2 = NULL;
                    }

                    /* The following operation is done to update the number of equal cost
                     * routes */
                    if (pTmpApp == NULL)
                    {
                        return TRIE_FAILURE;
                    }
                    if (pTmpApp->u4RtMetr1 == u4BestMetric)

                    {
                        if (u1AppId < MAX_APPLICATIONS)
                        {
                            TRIE_DEC_NUM_EQUAL_COST_ROUTES (pLeafNode->
                                                            au4NoOfMultipath
                                                            [u1AppId]);
                        }
                    }

                    else

                    {
                        if (u1AppId < MAX_APPLICATIONS)
                        {
                            TRIE_INIT_EQUAL_COST_ROUTES (pLeafNode->
                                                         au4NoOfMultipath
                                                         [u1AppId]);
                        }
                        u4BestMetric = pTmpApp->u4RtMetr1;
                        while (pTmpApp != NULL)

                        {
                            if (pTmpApp->u4RtMetr1 > u4BestMetric)
                                break;
                            if (pTmpApp->u4RowStatus == ACTIVE)
                            {
                                if (u1AppId < MAX_APPLICATIONS)
                                {
                                    TRIE_INC_EQUAL_COST_ROUTES
                                        (pLeafNode->au4NoOfMultipath[u1AppId]);
                                }
                            }
                            pTmpApp = pTmpApp->pNextAlternatepath;
                        }
                    }
                }                /* Else End for Checking OSPF APP ID */
            }                    /* Else End */
        }

        else

        {

            /*   The Searching NEXT_HOP is NOT the 
             *   first entry in the List    */
            if (((u1AppId == (OSPF_RT_ID - 1)) ||
                 (u1AppId == (OSPF_CAL_RT_ID - 1))))

            {
                u1RtType = pInputParams->u1Reserved1;
                pPrevPath = pTmpApp;
                pTmpApp = pTmpApp->pNextAlternatepath;
                while (pTmpApp != NULL)

                {
                    if ((pTmpApp->u4NxtHop == NxtHop) &&
                        (u1RtType == ((UINT1) pTmpApp->u2RtType)))

                    {
                        pOutputParams->pAppSpecInfo = (void *) pTmpApp;
                        pPrevPath->pNextAlternatepath =
                            pTmpApp->pNextAlternatepath;
                        TRIE_DEC_NUM_EQUAL_COST_ROUTES (pLeafNode->
                                                        au4NoOfMultipath
                                                        [u1AppId]);
                        break;
                    }
                    pPrevPath = pTmpApp;
                    pTmpApp = pTmpApp->pNextAlternatepath;
                }
                if (pTmpApp == NULL)
                {
                    return TRIE_FAILURE;
                }

                /* Decrement total num of routes */
                TRIE_DEC_TOTAL_NUM_OF_ROUTES (pLeafNode->
                                              au4NoOfMultipath[u1AppId]);
                return TRIE_SUCCESS;
            }

            else

            {                    /* else for RIP and other modules */
                while (pTmpApp != NULL)

                {
                    if (pTmpApp->u4NxtHop == NxtHop)

                    {
                        pOutputParams->pAppSpecInfo = (void *) pTmpApp;
                        pPrevPath->pNextAlternatepath =
                            pTmpApp->pNextAlternatepath;
                        if ((pTmpApp->u4RtMetr1 == u4BestMetric) &&
                            (u1AppId < MAX_APPLICATIONS))
                        {
                            TRIE_DEC_NUM_EQUAL_COST_ROUTES
                                (pLeafNode->au4NoOfMultipath[u1AppId]);
                        }
                        break;
                    }
                    pPrevPath = pTmpApp;
                    pTmpApp = pTmpApp->pNextAlternatepath;
                }
                if (pTmpApp == NULL)

                {
                    return TRIE_FAILURE;
                }
            }                    /* else for RIP and other modules */
        }                        /* The Searching NEXT_HOP is NOT the first entry */
        if (u1AppId < MAX_APPLICATIONS)
        {
            TRIE_DEC_TOTAL_NUM_OF_ROUTES (pLeafNode->au4NoOfMultipath[u1AppId]);
        }
    }                            /* Else CHecking For INVALID NEXTHOP */
    return TRIE_SUCCESS;
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieDeleteRadixLeaf ()                                   */
    /*                                                                         */
    /*  Description   This function deletes the leaf node and the associated   */
    /*                radix node if any, pointed to by the parameters. it      */
    /*                copies the deleted leaf node information in the          */
    /*                relevant fields of tOutputParams structure.              */
    /*                                                                         */
    /*  Call          When the only residing application in the leaf node      */
    /*  condition     is also deleted.                                         */
    /*                                                                         */
    /*  Input(s)      u1KeySize -      Size of the key.                        */
    /*                pLeafNode -      Pointer to the tLeafNode to be          */
    /*                                 deleted.                                */
    /*                                                                         */
    /*  Output(s)     pOutputParams -  Pointer to the tOutputParams            */
    /*                                 structure.                              */
    /*                                                                         */
    /*  Access        TrieDeleteEntry (), TrieDelete ().                       */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/
#ifdef __STDC__
void
TrieDeleteRadixLeaf (UINT1 u1KeySize, tLeafNode * pLeafNode,
                     tOutputParams * pOutputParams, UINT1 u1AppId)
#else /*  */
void
TrieDeleteRadixLeaf (u1KeySize, pLeafNode, pOutputParams, u1AppId)
     UINT1               u1KeySize;
     tLeafNode          *pLeafNode;
     tOutputParams      *pOutputParams;
     UINT1               u1AppId;

#endif /*  */
{
    tRadixNode         *pTmpParent;
    tRadixNode         *pPredNode;
    tRadixNode         *pOtherTree;
    pOutputParams->pAppSpecInfo = pLeafNode->pAppSpecInfo;

/*   pOutputParams->au4NoOfMultipath[u1AppId] =  
                      pLeafNode->au4NoOfMultipath[u1AppId]; */
    if (u1AppId < MAX_APPLICATIONS)
    {
        TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeafNode->au4NoOfMultipath[u1AppId],
                                       pOutputParams->
                                       au4NoOfMultipath[u1AppId]);
    }
    if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

    {
        KEYCOPY (pOutputParams->Key, pLeafNode->Key, u1KeySize);
    }
    pTmpParent = pLeafNode->pParent;
    if (pTmpParent->pRight == pLeafNode)

    {
        pTmpParent->pRight = NULL;
        pOtherTree = pTmpParent->pLeft;
    }

    else

    {
        pTmpParent->pLeft = NULL;
        pOtherTree = pTmpParent->pRight;
    }

    /* free the memory of the key, if allocated */
    if ((u1KeySize > U4_SIZE) && (u1KeySize < MAX_KEY_SIZE + 1))

    {
        TrieMemFree (gai4KeyPoolIdx[u1KeySize], (UINT1 *) pLeafNode->Key.pKey);
    }

    /* free the leaf node */
    TrieMemFree (gi4LeafPool, (UINT1 *) pLeafNode);

    /* free the parent node except root */
    pPredNode = pTmpParent->pParent;
    if (pPredNode != NULL)

    {

        /* update the pointers */
        if (pPredNode->pRight == pTmpParent)

        {
            pPredNode->pRight = pOtherTree;
        }

        else

        {
            pPredNode->pLeft = pOtherTree;
        }
        pOtherTree->pParent = pPredNode;

        /* free the parent radix node */
        TrieMemFree (gi4RadixPool, (UINT1 *) pTmpParent);
        pTmpParent = pPredNode;
    }

    /* update the mask 
     * Here TrieAssignMask () is supposed to be called with u1KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieAssignMask (U4_SIZE, (void *) pTmpParent);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieAssignMask ()                                        */
    /*                                                                         */
    /*  Description   This function assigns mask irrespective of the node      */
    /*                type. In every assignment of the mask, it selects the    */
    /*                mask with the least number of leading ones.              */
    /*                                                                         */
    /*  Call          When mask needs to be reassigned.                        */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize -  Size of the key.                            */
    /*                pNode -      Pointer to the node whose mask needs to     */
    /*                             be reassigned.                              */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        TrieAddEntry (), TrieDeleteAppAndUpdateLeaf (),          */
    /*  privileges    TrieDeleteRadixLeaf ().                                  */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void
TrieAssignMask (UINT1 u1KeySize, void *pNode)
#else /*  */
void
TrieAssignMask (u1KeySize, pNode)
     UINT1               u1KeySize;
     void               *pNode;

#endif /*  */
{
    INT4                i4Cmp;
    INT4                i4Eql;
    tKey                PrvMask;
    tRadixNode         *pRadix;
    pRadix = (tRadixNode *) pNode;
    while (pRadix != NULL)

    {
        PrvMask = pRadix->Mask;
        if ((pRadix->pRight != NULL) && (pRadix->pLeft != NULL))

        {
            i4Cmp = KEYCMP (((tRadixNode *) pRadix->pRight)->Mask,
                            ((tRadixNode *) pRadix->pLeft)->Mask, u1KeySize);
            if (i4Cmp < 0)

            {
                pRadix->Mask = ((tRadixNode *) pRadix->pRight)->Mask;
            }

            else

            {
                pRadix->Mask = ((tRadixNode *) pRadix->pLeft)->Mask;
            }
        }

        else

        {
            if ((pRadix->pRight == NULL) && (pRadix->pLeft == NULL))

            {
                break;
            }
            if (pRadix->pRight == NULL)

            {
                pRadix->Mask = ((tRadixNode *) pRadix->pLeft)->Mask;
            }

            else

            {
                pRadix->Mask = ((tRadixNode *) pRadix->pRight)->Mask;
            }
        }
        i4Eql = KEYEQUAL (pRadix->Mask, PrvMask, u1KeySize);
        if (i4Eql == 0)

        {
            break;
        }
        pRadix = pRadix->pParent;
    }
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieFindBestMatch ()                                     */
    /*                                                                         */
    /*  Description   This functions starts to find the best match with the    */
    /*                given node and in the worst case, goes to the root. If   */
    /*                this operation is successful, it returns TRIE_SUCCESS. In     */
    /*                case of successful return, it also copies relevant       */
    /*                fields in the tOutputParams structure.                   */
    /*                                                                         */
    /*  Call          When best match needs to be found.                       */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize -       Size of the key.                       */
    /*                pInputParams -    Pointer to the tInputParams            */
    /*                                  structure.                             */
    /*                pNode -           Pointer to the  node from where        */
    /*                                  search  for the best match begins.     */
    /*                                                                         */
    /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
    /*                                  structure.                             */
    /*                                                                         */
    /*  Access        TrieLookupEntry ().                                      */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS  - If it is able to  find  the best match.       */
    /*                TRIE_FAILURE -  Otherwise.                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
INT4
TrieFindBestMatch (UINT1 u1KeySize, tInputParams * pInputParams, void *pNode,
                   tOutputParams * pOutputParams)
#else /*  */
INT4
TrieFindBestMatch (u1KeySize, pInputParams, pNode, pOutputParams)
     UINT1               u1KeySize;
     tInputParams       *pInputParams;
     void               *pNode;
     tOutputParams      *pOutputParams;

#endif /*  */
{
    INT1                i1AppId;
    UINT2               u2CurrAppBit;
    INT4                i4Idx;
    UINT4               u4TmpNumBytes;
    UINT4               u4Counter;
    UINT4               u4DummyMask;
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tKey                LeafKey;
    UINT1              *pTmpKey;
    UINT1              *pau1Mask;
    tRadixNode         *pCurrNode;
    tLeafNode          *pLeaf;
    void               *pCurrApp;
    UINT1               au1Key[MAX_KEY_SIZE];
    UINT1               au1PrvKey[MAX_KEY_SIZE];
    InKey = pInputParams->Key;
    i1AppId = pInputParams->i1AppId;
    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)

    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
        LeafKey.pKey = &(au1PrvKey[0]);

        /* a temporary solution once mask size is more than 
         * 4 byte shouls not be problem
         */
        u4DummyMask = OSIX_HTONL (pLeaf->Mask.u4Key);
        pau1Mask = (UINT1 *) &(u4DummyMask);

        /* here assumption is IP address is of 4 byte (U4_SIZE) 
         * this logic can be made more efficient by diretly ANDING with
         * 4 byte mask only. Here it is byte by byte.
         */
        for (u4TmpNumBytes = 0; u4TmpNumBytes < U4_SIZE; u4TmpNumBytes++)

        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
            LeafKey.pKey[u4TmpNumBytes] =
                (pLeaf->Key).pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }
        if (memcmp (LeafKey.pKey, CurrKey.pKey, U4_SIZE) == 0)

        {

            /* If application id is generic appid, than return
             * every application specific entry of the leaf node in its 
             * proper place.
             */
            pCurrApp = pLeaf->pAppSpecInfo;
            if (i1AppId < 0)

            {
                u2CurrAppBit = 1;
                for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
                     u4Counter++)

                {
                    if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                    {
                        if (pCurrApp == NULL)
                        {
                            return (TRIE_SUCCESS);
                        }

                        ((tTrieApp **) pOutputParams->
                         pAppSpecInfo)[u4Counter] = ((tTrieApp *) pCurrApp);
                        pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;

/*                  pOutputParams->au4NoOfMultipath[u4Counter] =
                           pLeaf->au4NoOfMultipath[u4Counter]; */
                        TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                            (pLeaf->au4NoOfMultipath[u4Counter],
                             pOutputParams->au4NoOfMultipath[u4Counter]);
                    }
                    u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                }
                if (pOutputParams->Key.pKey != NULL)

                {
                    MEMCPY (pOutputParams->Key.pKey, pLeaf->Key.pKey,
                            u1KeySize);
                }
                return (TRIE_SUCCESS);
            }

            /* Check if application entry exists in the leaf node */
            if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

            {
                for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                {
                    pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                        ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                }

                /* copy the entry in output parameters */
                pOutputParams->pAppSpecInfo = pCurrApp;

/*            pOutputParams->au4NoOfMultipath[i4Idx] = 
                             pLeaf->au4NoOfMultipath[i4Idx]; */
                TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES (pLeaf->
                                                    au4NoOfMultipath
                                                    [i4Idx],
                                                    pOutputParams->
                                                    au4NoOfMultipath[i4Idx]);
                if (pOutputParams->Key.pKey != NULL)

                {
                    MEMCPY (pOutputParams->Key.pKey, pLeaf->Key.pKey,
                            u1KeySize);
                }
                return (TRIE_SUCCESS);
            }
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }

    else

    {
        pCurrNode = (tRadixNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
    }

    if (pCurrNode == NULL)
    {
        return (TRIE_FAILURE);
    }
    PrvKey.pKey = &(au1PrvKey[0]);
    u4DummyMask = OSIX_HTONL (pCurrNode->Mask.u4Key);
    pau1Mask = (UINT1 *) &(u4DummyMask);
    au1Key[0] = (UINT1) (~(au1PrvKey[0] & pau1Mask[0]));
    while (pCurrNode != NULL)

    {
        u4DummyMask = OSIX_HTONL (pCurrNode->Mask.u4Key);
        pau1Mask = (UINT1 *) &(u4DummyMask);

        /* change the arrays to reflect correct value */
        pTmpKey = CurrKey.pKey;
        CurrKey.pKey = PrvKey.pKey;
        PrvKey.pKey = pTmpKey;
        for (u4TmpNumBytes = 0; u4TmpNumBytes < U4_SIZE; u4TmpNumBytes++)

        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        /* ideally upper limit should be 2*U4_SIZE, but here both are same
         * (u1KeySize and 2*U4_SIZE)
         */
        for (u4TmpNumBytes = U4_SIZE; u4TmpNumBytes < u1KeySize;
             u4TmpNumBytes++)

        {
            CurrKey.pKey[u4TmpNumBytes] = pau1Mask[u4TmpNumBytes - U4_SIZE];
        }

        /* to prevent reduntant lookup */
        if (memcmp (CurrKey.pKey, PrvKey.pKey, u1KeySize) != 0)

        {
            if (TrieTraverse
                (u1KeySize, pCurrNode, CurrKey,
                 (void *) &(pLeaf)) == TRIE_SUCCESS)

            {
                u4DummyMask = OSIX_HTONL (pLeaf->Mask.u4Key);
                pau1Mask = (UINT1 *) &(u4DummyMask);
                for (u4TmpNumBytes = 0; u4TmpNumBytes < U4_SIZE;
                     u4TmpNumBytes++)

                {
                    PrvKey.pKey[u4TmpNumBytes] =
                        pLeaf->Key.
                        pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                    CurrKey.pKey[u4TmpNumBytes] =
                        InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                }

                /* if keys match */
                if (memcmp (CurrKey.pKey, PrvKey.pKey, U4_SIZE) == 0)

                {

                    /* If application id is generic appid, than return
                     * every application specific entry of the leaf node in its 
                     * proper place.
                     */
                    pCurrApp = pLeaf->pAppSpecInfo;
                    if (i1AppId < 0)

                    {
                        u2CurrAppBit = 1;
                        for (u4Counter = 0;
                             u4Counter < BYTE_LENGTH * sizeof (UINT2);
                             u4Counter++)

                        {
                            if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                            {
                                    if (pCurrApp == NULL)
                                    {
                                            return (TRIE_SUCCESS);
                                    }


                                ((tTrieApp **) pOutputParams->pAppSpecInfo)
                                    [u4Counter] = ((tTrieApp *) pCurrApp);
                                pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;

                                /* pOutputParams->au4NoOfMultipath[u4Counter] =
                                   pLeaf->au4NoOfMultipath[u4Counter]; */
                                TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                                    (pLeaf->au4NoOfMultipath[u4Counter],
                                     pOutputParams->
                                     au4NoOfMultipath[u4Counter]);
                            }
                            u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                        }
                        if (pOutputParams->Key.pKey != NULL)

                        {
                            MEMCPY (pOutputParams->Key.pKey, pLeaf->Key.pKey,
                                    u1KeySize);
                        }
                        return (TRIE_SUCCESS);
                    }

                    /* Check if application entry exists in the leaf node */
                    if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

                    {
                        for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                        {
                            pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                                ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                        }

                        /* copy the entry in output parameters */
                        pOutputParams->pAppSpecInfo = pCurrApp;

                        /* pOutputParams->au4NoOfMultipath[i4Idx] = 
                           pLeaf->au4NoOfMultipath[i4Idx]; */
                        TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                            (pLeaf->au4NoOfMultipath[i4Idx],
                             pOutputParams->au4NoOfMultipath[i4Idx]);
                        if (pOutputParams->Key.pKey != NULL)

                        {
                            MEMCPY (pOutputParams->Key.pKey, pLeaf->Key.pKey,
                                    u1KeySize);
                        }
                        return (TRIE_SUCCESS);
                    }
                }
            }

            else

            {
                return (TRIE_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (TRIE_FAILURE);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieFindBestMatch4 ()                                    */
    /*                                                                         */
    /*  Description   This functions starts to find the best match with the    */
    /*                given node and in the worst case, goes to the root. If   */
    /*                this operation is successful, it returns TRIE_SUCCESS. In     */
    /*                case of successful return, it also copies relevant       */
    /*                fields in the tOutputParams structure. This function     */
    /*                handles only 4 byte key.                                 */
    /*                                                                         */
    /*  Call          When best match needs to be found.                       */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1KeySize -       Size of the key.                       */
    /*                pInputParams -    Pointer to the tInputParams            */
    /*                                  structure.                             */
    /*                pNode -           Pointer to the  node from where        */
    /*                                  search  for the best match begins.     */
    /*                                                                         */
    /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
    /*                                  structure.                             */
    /*                                                                         */
    /*  Access        TrieLookupEntry ().                                      */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        TRIE_SUCCESS  - If it is able to  find  the best match.       */
    /*                TRIE_FAILURE -  Otherwise.                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
INT4
TrieFindBestMatch4 (UINT1 u1KeySize, tInputParams * pInputParams, void *pNode,
                    tOutputParams * pOutputParams)
#else /*  */
INT4
TrieFindBestMatch4 (u1KeySize, pInputParams, pNode, pOutputParams)
     UINT1               u1KeySize;
     tInputParams       *pInputParams;
     void               *pNode;
     tOutputParams      *pOutputParams;

#endif /*  */
{
    INT1                i1AppId;
    UINT2               u2CurrAppBit;
    INT4                i4Idx;
    UINT4               u4Counter;
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tRadixNode         *pCurrNode;
    tLeafNode          *pLeaf;
    void               *pCurrApp;
    InKey = pInputParams->Key;
    i1AppId = pInputParams->i1AppId;
    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)

    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.u4Key = InKey.u4Key & (pLeaf->Mask).u4Key;
        if (CurrKey.u4Key == ((pLeaf->Key).u4Key & (pLeaf->Mask).u4Key))

        {

            /* If application id is generic appid, than return
             * every application specific entry of the leaf node in its 
             * proper place.
             */
            pCurrApp = pLeaf->pAppSpecInfo;
            if (i1AppId < 0)

            {
                u2CurrAppBit = 1;
                for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
                     u4Counter++)

                {
                    if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                    {
                        ((tTrieApp **) pOutputParams->
                         pAppSpecInfo)[u4Counter] = ((tTrieApp *) pCurrApp);
                        pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;

                        /* pOutputParams->au4NoOfMultipath[u4Counter] =
                           pLeaf->au4NoOfMultipath[u4Counter]; */
                        TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                            (pLeaf->au4NoOfMultipath[u4Counter],
                             pOutputParams->au4NoOfMultipath[u4Counter]);
                    }
                    u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                }
                pOutputParams->Key.u4Key = (pLeaf->Key).u4Key;
                return (TRIE_SUCCESS);
            }

            /* Check if application entry exists in the leaf node */
            if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

            {
                for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                {
                    pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                        ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                }
                if (((InKey.u4Key) & (((tTrieApp *) pCurrApp)->u4Mask)) ==
                    ((pLeaf->Key.u4Key) & (((tTrieApp *) pCurrApp)->u4Mask)))

                {

                    /* copy the entry in output parameters */
                    pOutputParams->pAppSpecInfo = pCurrApp;

                    /* pOutputParams->au4NoOfMultipath[i4Idx] = 
                       pLeaf->au4NoOfMultipath[i4Idx]; */
                    TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES (pLeaf->
                                                        au4NoOfMultipath
                                                        [i4Idx],
                                                        pOutputParams->
                                                        au4NoOfMultipath
                                                        [i4Idx]);
                    pOutputParams->Key.u4Key = (pLeaf->Key).u4Key;
                    return (TRIE_SUCCESS);
                }
            }
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }

    else

    {
        pCurrNode = (tRadixNode *) pNode;
    }
    if (pCurrNode == NULL)
    {
        return (TRIE_FAILURE);
    }
    CurrKey.u4Key = ~(InKey.u4Key & (pCurrNode->Mask).u4Key);
    while (pCurrNode != NULL)

    {

        /* change the arrays to reflect correct value */
        PrvKey.u4Key = CurrKey.u4Key;
        CurrKey.u4Key = InKey.u4Key & (pCurrNode->Mask).u4Key;
        if (CurrKey.u4Key != PrvKey.u4Key)

        {
            if (TrieTraverse
                (u1KeySize, pCurrNode, CurrKey,
                 (void *) &(pLeaf)) == TRIE_SUCCESS)

            {
                if (CurrKey.u4Key == (pLeaf->Key.u4Key & pLeaf->Mask.u4Key))

                {

                    /* If application id is generic appid, than return
                     * every application specific entry of the leaf node in its 
                     * proper place.
                     */
                    pCurrApp = pLeaf->pAppSpecInfo;
                    if (i1AppId < 0)

                    {
                        u2CurrAppBit = 1;
                        for (u4Counter = 0;
                             u4Counter < BYTE_LENGTH * sizeof (UINT2);
                             u4Counter++)

                        {
                            if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                            {
                                ((tTrieApp **) pOutputParams->pAppSpecInfo)
                                    [u4Counter] = ((tTrieApp *) pCurrApp);
                                pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;

/*                        pOutputParams->au4NoOfMultipath[u4Counter] =
                             pLeaf->au4NoOfMultipath[u4Counter]; */
                                TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                                    (pLeaf->au4NoOfMultipath[u4Counter],
                                     pOutputParams->
                                     au4NoOfMultipath[u4Counter]);
                            }
                            u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                        }
                        pOutputParams->Key.u4Key = pLeaf->Key.u4Key;
                        return (TRIE_SUCCESS);
                    }

                    /* Check if application entry exists in the leaf node */
                    if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

                    {
                        for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                        {
                            pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                                ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                        }
                        if (((InKey.u4Key) &
                             (((tTrieApp *) pCurrApp)->u4Mask)) ==
                            ((pLeaf->Key.
                              u4Key) & (((tTrieApp *) pCurrApp)->u4Mask)))

                        {

                            /* copy the entry in output parameters */
                            pOutputParams->pAppSpecInfo = pCurrApp;

                            /*pOutputParams->au4NoOfMultipath[i4Idx] = 
                               pLeaf->au4NoOfMultipath[i4Idx]; */
                            TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES
                                (pLeaf->au4NoOfMultipath[i4Idx],
                                 pOutputParams->au4NoOfMultipath[i4Idx]);
                            pOutputParams->Key.u4Key = pLeaf->Key.u4Key;
                            return (TRIE_SUCCESS);
                        }
                    }
                }
            }

            else

            {
                return (TRIE_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (TRIE_FAILURE);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieAllocateNode ()                                      */
    /*                                                                         */
    /*  Description   This function allocates the memory for the node as       */
    /*                requested by the u1NodeType parameter. It also           */
    /*                initialises the fields of the structure. If it fails     */
    /*                in allocating memory, it returns TRIE_FAILURE. In case of     */
    /*                the successful return, it passes a pointer to the        */
    /*                structure created.                                       */
    /*                                                                         */
    /*  Call          When the function wants to create either the leaf node   */
    /*  condition     or the radix node.                                       */
    /*                                                                         */
    /*  Input(s)      u1KeySize -     Size of the key.                         */
    /*                u1NodeType -    Indicates the type of the node.          */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        TrieAddLeaf (), TrieAddRadix ().                         */
    /*  privileges    TrieSelectInstance ().                                   */
    /*                                                                         */
    /*  Return        Pointer to the created node - If it is successful.       */
    /*                TRIE_FAILURE -  Otherwise.                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void               *
TrieAllocateNode (UINT1 u1KeySize, UINT1 u1NodeType)
#else /*  */
void               *
TrieAllocateNode (u1KeySize, u1NodeType)
     UINT1               u1KeySize;
     UINT1               u1NodeType;

#endif /*  */
{
    tLeafNode          *pLeaf;
    tRadixNode         *pRadix;
    if (u1NodeType == RADIX_NODE)

    {
        pRadix = (tRadixNode *) TrieMemAlloc (gi4RadixPool);
        if (pRadix == NULL)

        {
            TrieErrorReport (RADIX_ALLOC_FAIL);
            return ((void *) NULL);
        }
        pRadix->u1NodeType = RADIX_NODE;
        return ((void *) pRadix);
    }
    pLeaf = (tLeafNode *) TrieMemAlloc (gi4LeafPool);
    if (pLeaf == NULL)

    {
        TrieErrorReport (LEAF_ALLOC_FAIL);
        return ((void *) NULL);
    }
    if (u1KeySize > U4_SIZE)

    {
        pLeaf->Key.pKey = NULL;
        if (u1KeySize < MAX_KEY_SIZE + 1)
        {
            pLeaf->Key.pKey = TrieMemAlloc (gai4KeyPoolIdx[u1KeySize]);
        }
        if (pLeaf->Key.pKey == NULL)

        {
            TrieMemFree (gi4LeafPool, (UINT1 *) pLeaf);
            TrieErrorReport (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }
    }
    pLeaf->u1NodeType = LEAF_NODE;
    return ((void *) pLeaf);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      void TrieErrorReport ()                                  */
    /*                                                                         */
    /*  Description   This function reports the error encountered by the       */
    /*                different functions. As per the u1NumError field, it     */
    /*                sends error message to the standard error device.        */
    /*                                                                         */
    /*  Call          When calling function encounters errors.                 */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      u1NumError -     Error number                            */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        Higher level functions of the Trie library.              */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void
TrieErrorReport (UINT1 u1NumError)
#else /*  */
void
TrieErrorReport (u1NumError)
     UINT1               u1NumError;

#endif /*  */
{
    UINT1               u1StrLength;
    char                buf[100];
    const char         *aErr[] =
        { "Memory allocation failure for the leaf node.",
        "Memory allocation failure for the radix node.",
        "Memory allocation failure for the key.",
        "Key size can not be accommodated.",
        "Application id can not be accommodated.",
        "Number of instances are more than the maximum value.",
        "Semaphore creation failure.",
        "Application id is already taken before.",
        "Memory allocation failed."
    };
    u1StrLength = (UINT1) (STRLEN (aErr[u1NumError]));
    if (u1StrLength > 99)
    {
        u1StrLength = 99;
    }
    SNPRINTF (buf, u1StrLength, "%s\n", aErr[u1NumError]);
    UtlTrcPrint (buf);
}

 /***************************************************************************/
    /*                                                                         */
    /*  Function      TrieInitVariables ()                                     */
    /*                                                                         */
    /*  Description   This function initialises configurable variables.        */
    /*                                                                         */
    /*  Call          When configurable variables needs to be initialised.     */
    /*  condition                                                              */
    /*                                                                         */
    /*  Input(s)      None.                                                    */
    /*                                                                         */
    /*  Output(s)     None.                                                    */
    /*                                                                         */
    /*  Access        TrieInit ().                                             */
    /*  privileges                                                             */
    /*                                                                         */
    /*  Return        None.                                                    */
    /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__
void
TrieInitVariables (void)
#else /*  */
void
TrieInitVariables ()
#endif                            /*  */
{

#ifdef IP_WANTED
    tIpSystemSize       IpSystemSize;
#endif

#ifdef RIP_WANTED
    tRipSystemSize      RipSystemSize;
#endif

#ifdef OSPF_WANTED
    tOspfSystemSize     OspfSystemSize;
#endif

#ifdef BGP_WANTED
    tBgpSystemSize      BgpSystemsize;
#endif

#ifdef MPLS_WANTED
    tMplsSystemSize     MplsSystemSize;
#endif

    gConfigParams.u4NumRadixNodes = NUM_1ST_KEY;

#ifdef IP_WANTED
    UNUSED_PARAM (IpSystemSize);
#endif
#ifdef RIP_WANTED
    UNUSED_PARAM (RipSystemSize);
#endif
#ifdef OSPF_WANTED
    UNUSED_PARAM (OspfSystemSize);
#endif
#ifdef BGP_WANTED
    UNUSED_PARAM (BgpSystemsize);
#endif
#ifdef MPLS_WANTED
    UNUSED_PARAM (MplsSystemSize);
#endif

    gConfigParams.u4NumLeafNodes = gConfigParams.u4NumRadixNodes;
    memset (&gConfigParams.au4NumKey[0], 0, MAX_KEY_SIZE * sizeof (UINT4));
    gConfigParams.au4NumKey[KEY1_SIZE] = gConfigParams.u4NumRadixNodes;
    gConfigParams.au4NumKey[KEY2_SIZE] = NUM_2ND_KEY;
    gConfigParams.au4NumKey[KEY3_SIZE] = NUM_3RD_KEY;
    gConfigParams.au4NumKey[KEY4_SIZE] = NUM_4TH_KEY;
}
