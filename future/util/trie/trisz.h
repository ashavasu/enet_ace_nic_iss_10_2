/*$Id: trisz.h,v 1.1 2015/04/28 12:51:06 siva Exp $*/
enum {
    MAX_TRIE_NUM_LEAF_NODES_SIZING_ID,
    MAX_TRIE_NUM_RADIX_NODES_SIZING_ID,
    MAX_TRIE_NUM_TRIE_KEYS_SIZING_ID,
    TRI_MAX_SIZING_ID
};


#ifdef  _TRISZ_C
tMemPoolId TRIMemPoolIds[ TRI_MAX_SIZING_ID];
INT4  TriSizingMemCreateMemPools(VOID);
VOID  TriSizingMemDeleteMemPools(VOID);
INT4  TriSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TRISZ_C  */
extern tMemPoolId TRIMemPoolIds[ ];
extern INT4  TriSizingMemCreateMemPools(VOID);
extern VOID  TriSizingMemDeleteMemPools(VOID);
#endif /*  _TRISZ_C  */


#ifdef  _TRISZ_C
tFsModSizingParams FsTRISizingParams [] = {
{ "tLeafNode", "MAX_TRIE_NUM_LEAF_NODES", sizeof(tLeafNode),MAX_TRIE_NUM_LEAF_NODES, MAX_TRIE_NUM_LEAF_NODES,0 },
{ "tRadixNode", "MAX_TRIE_NUM_RADIX_NODES", sizeof(tRadixNode),MAX_TRIE_NUM_RADIX_NODES, MAX_TRIE_NUM_RADIX_NODES,0 },
{ "UINT1[8]", "MAX_TRIE_NUM_TRIE_KEYS", sizeof(UINT1[8]),MAX_TRIE_NUM_TRIE_KEYS, MAX_TRIE_NUM_TRIE_KEYS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TRISZ_C  */
extern tFsModSizingParams FsTRISizingParams [];
#endif /*  _TRISZ_C  */


