/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieapif.c,v 1.22 2015/04/28 12:51:06 siva Exp $
 *
 * Description:Contains interface functions of trie library
 *
 *******************************************************************/
#include "trieinc.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
static tOsixSemId   gSem;
INT4                gi4RadixPool;
INT4                gi4LeafPool;
INT4                gai4KeyPoolIdx[MAX_KEY_SIZE + 1];
tRadixNodeHead      gaTrieFamily[MAX_NUM_TRIE];
tTrieConfigParams   gConfigParams;

/* Stack Data structure used for inorder traversal */
/* used in TrieGetSucc */
typedef struct StackNode
{
    tTMO_SLL_NODE       Node;
    void               *pPtr;
}
tStackNode;

#define TRIE_SEM_ID  gSem

/**************************************************************************/

/***************************************************************************/
/*                                                                         */
/*  Function      TrieInit ()                                              */
/*                                                                         */
/*  Description   This function initialises an array. Each member of this  */
/*                array is a tRadixNodeHead structure and represents       */
/*                different instance of Trie. It also initialises integer  */
/*                fields of all members. It creates global semaphore.      */
/*                This ensures that even in presence of more applications  */
/*                TrieCreate () is always called in  mutual exclusion.     */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family. */
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieInit (void)
{

    MEMSET ((tRadixNodeHead *) gaTrieFamily, 0,
            MAX_NUM_TRIE * sizeof (tRadixNodeHead));

    if (OsixCreateSem (TRIE_SEM_NAME, TRIE_SEM_COUNT, TRIE_SEM_FLAGS,
                       &TRIE_SEM_ID) != OSIX_SUCCESS)

    {
        TrieErrorReport (SEM_CREATE_FAIL);
        return (TRIE_FAILURE);
    }
    TrieInitVariables ();
    MEMSET ((INT4 *) gai4KeyPoolIdx, (-1), MAX_KEY_SIZE * sizeof (INT4));

    /*Mempools Creation */
    if (TriSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        TrieErrorReport (MEM_ALLOC_FAIL);
        OsixSemDel (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    gi4RadixPool = (INT4) TRIMemPoolIds[MAX_TRIE_NUM_RADIX_NODES_SIZING_ID];
    gi4LeafPool = (INT4) TRIMemPoolIds[MAX_TRIE_NUM_LEAF_NODES_SIZING_ID];
    gai4KeyPoolIdx[KEY1_SIZE] =
        (INT4) TRIMemPoolIds[MAX_TRIE_NUM_TRIE_KEYS_SIZING_ID];
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieShutDown ()                                          */
/*                                                                         */
/*  Description   This function frees all the resources  occupied by all   */
/*                the instances of Trie.                                   */
/*                                                                         */
/*  Call          When all the resources are to be                         */
/*  condition     freed.                                                   */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
void
TrieShutDown (void)
{
    /*Deleting Mempools */
    TriSizingMemDeleteMemPools ();
    OsixSemDel (TRIE_SEM_ID);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieCreate ()                                            */
/*                                                                         */
/*  Description   This function is used to register the application as     */
/*                well as to create a Trie instance. It returns pRoot      */
/*                parameter  with the root of the selected Trie instance.  */
/*                It returns unique identifier for each application as     */
/*                application identity number.                             */
/*                                                                         */
/*  Call          Should be called once before using                       */
/*  condition     any other Trie library function.                         */
/*                                                                         */
/*  Input(s)      pCreateParams - Pointer to the tCreateParams             */
/*                                structure.                               */
/*                                                                         */
/*  Output(s)     pRoot -         Pointer to the root of the instance of   */
/*                                the Trie (the tRadixNodeHead             */
/*                                structure), if successful.               */
/*                                                                         */
/*  Access        Applications who want to use the Trie library.           */
/*  privileges                                                             */
/*                                                                         */
/*  Return        Valid unique positive application identifier,            */
/*                if  successful.                                          */
/*                TRIE_FAILURE - Otherwise. (A negative number represents  */
/*                              TRIE_FAILURE.)                             */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieCreate (tCreateParams * pCreateParams, void *pRoot)
{
    UINT1               u1AppId;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance;

    OsixSemTake (TRIE_SEM_ID);
    if (pCreateParams->u1KeySize > MAX_KEY_SIZE)
    {
        TrieErrorReport (MAX_KEY_SIZE_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    u1AppId = pCreateParams->u1AppId;
    if ((u1AppId > BYTE_LENGTH * sizeof (UINT2)) || (u1AppId == 0))

    {
        TrieErrorReport (MAX_APP_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)

    {
        if ((gaTrieFamily[u4Instance].u4Type == pCreateParams->u4Type) &&
            (gaTrieFamily[u4Instance].u1KeySize == pCreateParams->u1KeySize))

        {
            if (CHECK_BIT ((u1AppId - 1), gaTrieFamily[u4Instance].u2AppMask))

            {
                TrieErrorReport (ALREADY_USED_APP_ID);
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }
            SET_BIT ((u1AppId - 1), gaTrieFamily[u4Instance].u2AppMask);
            *((tRadixNodeHead **) pRoot) = &gaTrieFamily[u4Instance];
            OsixSemGive (TRIE_SEM_ID);
            return ((INT4) (u1AppId - 1));
        }
    }
    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)

    {
        if (gaTrieFamily[u4Instance].u1KeySize == 0)

        {
            break;
        }
    }
    if (u4Instance == MAX_NUM_TRIE)

    {
        TrieErrorReport (MAX_INSTANCE_OVERFLOW);
        *((tRadixNodeHead **) pRoot) = NULL;
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);

    }
    gaTrieFamily[u4Instance].pRadixNodeTop =
        TrieAllocateNode (pCreateParams->u1KeySize, RADIX_NODE);
    if (gaTrieFamily[u4Instance].pRadixNodeTop == NULL)

    {
        *((tRadixNodeHead **) pRoot) = NULL;
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    gaTrieFamily[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
    gaTrieFamily[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
    gaTrieFamily[u4Instance].pRadixNodeTop->pLeft = NULL;
    gaTrieFamily[u4Instance].pRadixNodeTop->pRight = NULL;
    gaTrieFamily[u4Instance].pRadixNodeTop->pParent = NULL;
    gaTrieFamily[u4Instance].u4Type = pCreateParams->u4Type;
    gaTrieFamily[u4Instance].u1KeySize = pCreateParams->u1KeySize;
    pHead = &gaTrieFamily[u4Instance];

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))

    {
        TrieErrorReport (ALREADY_USED_APP_ID);
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    *((tRadixNodeHead **) pRoot) = pHead;
    OsixSemGive (TRIE_SEM_ID);
    return ((INT4) (u1AppId - 1));
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDelete ()                                            */
/*                                                                         */
/*  Description   This function may delete internal structures of Trie     */
/*                after giving application specific information to         */
/*                AppSpecDelFunc () for deletion. *AppSpecDelFunc () is    */
/*                called after traversing the fixed number of application  */
/*                specific entries.                                        */
/*                                                                         */
/*  Call          When there is urgent need for shutdown operation.        */
/*  condition     (A typical condition is when resources are               */
/*                blocked and freeing the resources might help.)           */
/*                                                                         */
/*  Input(s)      pInputParams -         Pointer to the                    */
/*                                       tInputParams structure.           */
/*                (AppSpecDelFunc ()) -  A function to delete the          */
/*                                       application specific              */
/*                                       information.                      */
/*                pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Output(s)     pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
INT4
TrieDelete (tInputParams * pInputParams,
            void (*AppSpecDelFunc) (tDeleteOutParams *),
            tDeleteOutParams * pDeleteOutParams)
{
    UINT1               u1AppId;
    UINT4               u4Index;
    UINT4               u4NumMultipath;
    UINT1               u1KeySize;
    UINT4               u4StopFlag;
    UINT4               u4TmpNumEntries;
    UINT4               u4OrgNumEntries;
    tOutputParams       OutParams;
    tKey                NullUnion;
    void               *pCurrNode;
    tRadixNode         *pPrevNode;
    tRadixNodeHead     *pHead;
    tLeafNode          *pDeleteLeaf;
    tLeafNode          *pLeaf;
    tOutputParams      *pOutputParams;

    u4OrgNumEntries = pDeleteOutParams->u4NumEntries;
    OsixSemTake (TRIE_SEM_ID);
    pHead = (tRadixNodeHead *) pInputParams->pRoot;
    pDeleteLeaf = NULL;
    NullUnion.pKey = NULL;
    u1KeySize = pHead->u1KeySize;
    u4StopFlag = 0;
    u4TmpNumEntries = 0;
    pPrevNode = (tRadixNode *) pHead->pRadixNodeTop;
    pCurrNode = (void *) pPrevNode;
    u1AppId = (UINT1) pInputParams->i1AppId;
    pOutputParams = &(OutParams);
    if (pPrevNode == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    if (pPrevNode->pLeft == NULL)

    {
        if (pPrevNode->pRight == NULL)

        {
            u4StopFlag = 1;
        }

        else

        {
            pCurrNode = pPrevNode->pRight;
        }
    }
    while (!(u4StopFlag))

    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))

        {

            /* the node marked for deletion is deleted */
            if (pDeleteLeaf != NULL)

            {
                if (u1KeySize <= U4_SIZE)

                {
                    TrieDeleteRadixLeaf (u1KeySize, pDeleteLeaf, pOutputParams,
                                         u1AppId);
                    if (pDeleteOutParams->pKey != NULL)
                    {
                        ((tKey *) pDeleteOutParams->pKey)[u4TmpNumEntries] =
                            pOutputParams->Key;
                    }
                }
                else

                {
                    pOutputParams->Key = (pDeleteOutParams->pKey != NULL) ?
                        ((tKey *) pDeleteOutParams->
                         pKey)[u4TmpNumEntries] : NullUnion;
                    TrieDeleteRadixLeaf (u1KeySize, pDeleteLeaf, pOutputParams,
                                         u1AppId);
                }
                u4NumMultipath = pOutputParams->au4NoOfMultipath[u1AppId];
                if (u4NumMultipath > 1)

                {
                    for (u4Index = 1; u4Index < u4NumMultipath; u4Index++)

                    {
                        if (pDeleteOutParams->pKey != NULL)
                        {
                            KEYCOPY (((tKey *) pDeleteOutParams->pKey)
                                     [u4TmpNumEntries + u4Index],
                                     pOutputParams->Key, u1KeySize);
                        }
                    }
                }
                COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT (pOutputParams,
                                                       pDeleteOutParams,
                                                       u4TmpNumEntries);
                pDeleteLeaf = NULL;
                if (u4TmpNumEntries == pDeleteOutParams->u4NumEntries)

                {
                    (AppSpecDelFunc) (pDeleteOutParams);
                    u4TmpNumEntries = 0;
                }
                pPrevNode = ((tLeafNode *) pCurrNode)->pParent;
            }
            pLeaf = (tLeafNode *) pCurrNode;

            /* application is present. delete it or mark it for deletion */
            if (CHECK_BIT ((INT4) pInputParams->i1AppId, pLeaf->u2AppMask))

            {
                TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeaf->
                                               au4NoOfMultipath[u1AppId],
                                               u4NumMultipath);
                if (u4TmpNumEntries + u4NumMultipath <=
                    pDeleteOutParams->u4NumEntries)

                {
                    if (pLeaf->u1NumApp > 1)

                    {
                        if (u1KeySize <= U4_SIZE)

                        {
                            TrieDeleteAppAndUpdateLeaf (u1KeySize,
                                                        pInputParams,
                                                        pLeaf,
                                                        pOutputParams,
                                                        INVALID_NEXTHOP);
                            if (pDeleteOutParams->pKey != NULL)
                            {
                                ((tKey *) pDeleteOutParams->
                                 pKey)[u4TmpNumEntries] = pOutputParams->Key;
                            }
                        }

                        else

                        {
                            pOutputParams->Key =
                                (pDeleteOutParams->pKey != NULL) ?
                                ((tKey *) pDeleteOutParams->
                                 pKey)[u4TmpNumEntries] : NullUnion;

                            TrieDeleteAppAndUpdateLeaf (u1KeySize,
                                                        pInputParams,
                                                        pLeaf,
                                                        pOutputParams,
                                                        INVALID_NEXTHOP);
                        }

                        if (u4NumMultipath > 1)

                        {
                            for (u4Index = 1; u4Index < u4NumMultipath;
                                 u4Index++)

                            {
                                if (pDeleteOutParams->pKey != NULL)
                                {
                                    KEYCOPY (((tKey *) pDeleteOutParams->pKey)
                                             [u4TmpNumEntries + u4Index],
                                             pOutputParams->Key, u1KeySize);
                                }
                            }
                        }
                        COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT (pOutputParams,
                                                               pDeleteOutParams,
                                                               u4TmpNumEntries);
                        if (u4TmpNumEntries == pDeleteOutParams->u4NumEntries)

                        {
                            (AppSpecDelFunc) (pDeleteOutParams);
                            u4TmpNumEntries = 0;
                        }
                    }

                    else

                    {
                        pDeleteLeaf = pLeaf;
                    }
                }

                else

                {
                    pDeleteOutParams->u4NumEntries = u4TmpNumEntries;
                    (AppSpecDelFunc) (pDeleteOutParams);
                    u4TmpNumEntries = 0;
                    pDeleteOutParams->u4NumEntries = u4OrgNumEntries;
                    continue;
                }
            }

            if (pPrevNode == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }
            /* pointer updation to reach the next leaf node */
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }

            else

            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))

                {
                    pCurrNode = (void *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)

                {
                    break;
                }

                else

                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }

        /*  the node is either the null node or the radix node */
        else

        {
            if (pCurrNode == NULL)

            {
                break;
            }

            /* radix node */
            pPrevNode = (tRadixNode *) pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
    }
    if (pDeleteLeaf != NULL)

    {
        if (u1KeySize <= U4_SIZE)

        {
            TrieDeleteRadixLeaf (u1KeySize, pDeleteLeaf, pOutputParams,
                                 u1AppId);
            if (pDeleteOutParams->pKey != NULL)
            {
                ((tKey *) pDeleteOutParams->pKey)[u4TmpNumEntries] =
                    pOutputParams->Key;
            }
        }

        else

        {
            pOutputParams->Key = (pDeleteOutParams->pKey != NULL) ?
                ((tKey *) pDeleteOutParams->pKey)[u4TmpNumEntries] : NullUnion;
            TrieDeleteRadixLeaf (u1KeySize, pDeleteLeaf, pOutputParams,
                                 u1AppId);
        }
        u4NumMultipath = pOutputParams->au4NoOfMultipath[u1AppId];
        if (u4NumMultipath > 1)

        {
            for (u4Index = 1; u4Index < u4NumMultipath; u4Index++)

            {
                if (pDeleteOutParams->pKey != NULL)
                {
                    KEYCOPY (((tKey *) pDeleteOutParams->pKey)
                             [u4TmpNumEntries + u4Index], pOutputParams->Key,
                             u1KeySize);
                }
            }
        }
        COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT (pOutputParams,
                                               pDeleteOutParams,
                                               u4TmpNumEntries);
    }

    /* remaining entries are given to the AppSpecDelFunc () for deletion */
    if (u4TmpNumEntries != 0)

    {
        pDeleteOutParams->u4NumEntries = u4TmpNumEntries;
        (AppSpecDelFunc) (pDeleteOutParams);
    }
    RESET_BIT (pInputParams->i1AppId, pHead->u2AppMask);

    /* no entries are there in this instance */
    if (pHead->u2AppMask == 0)

    {
        TrieMemFree (gi4RadixPool, (UINT1 *) pHead->pRadixNodeTop);
        pHead->u1KeySize = 0;
        pHead->u4Type = 0;
    }
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieAddEntry ()                                          */
/*                                                                         */
/*  Description   This function adds the pAppSpecInfo in an instance of    */
/*                Trie. If the application specific information is         */
/*                already present in the leaf node, it is replaced with    */
/*                the new information and the old information is           */
/*                returned using tOutputParams. If this operation is       */
/*                aborted due to resource problems, it has the return      */
/*                value TRIE_FAILURE.                                      */
/*                                                                         */
/*  Call          Whenever an application wants to add the entry or        */
/*  condition     update already existing entry.                           */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                pAppSpecInfo -    Pointer to the application specific    */
/*                                  information.                           */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If  adding  operation   is              */
/*                            successful.                                  */
/*                TRIE_FAILURE -   If it fails due to any                  */
/*                            reasons.                                     */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieAddEntry (tInputParams * pInputParams,
              void *pAppSpecInfo, tOutputParams * pOutputParams)
{
    UINT1               u1AppId;
    UINT1               u1KeySize;
    UINT1               u1DiffBit;
    UINT1               u1BitToTest;
    UINT4               u4NumOfMultiPath;
    UINT1               u1DiffByteVal = 0;
    UINT4               u4Metric;
    INT4                i4Idx;
    UINT4               u4DiffByte;
    tKey                InKey;
    tRadixNode         *pRadix;
    tRadixNode         *pParentNode;
    tTrieApp           *pCurrApp;
    tTrieApp           *pTmpApp;
    tTrieApp          **pPrevApp;
    tLeafNode          *pLeaf;
    tTrieApp           *pPrevPath = NULL;
    UINT4               u4NextHop = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1EcmpRoute = OSIX_FALSE;

    OsixSemTake (TRIE_SEM_ID);
    pRadix = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    InKey = pInputParams->Key;
    u1AppId = (UINT1) pInputParams->i1AppId;
    pLeaf = NULL;

    /* When TrieTravese fails to find the nearest leaf node
     * this function tries to add a leaf.
     */
    if (TrieTraverse (u1KeySize, pRadix, InKey, (void *) &(pLeaf)) ==
        TRIE_FAILURE)

    {
        if ((TrieAddLeaf (u1KeySize, pInputParams, pAppSpecInfo,
                          ((tRadixNode *) pLeaf),
                          pOutputParams)) == TRIE_FAILURE)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }

        /* instead of calling TrieAssignMask () with u1KeySize
         * It is called here with size of the mask.
         */

        /* pLeaf represents parent of the leaf node here */
        TrieAssignMask (U4_SIZE, pLeaf);
        pOutputParams->pAppSpecInfo = (void *) NULL;
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_SUCCESS);
    }

    /* TrieTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    DIFFBYTE (pLeaf->Key, InKey, u1KeySize, u4DiffByte, u1DiffByteVal);
    if (u4DiffByte == u1KeySize)

    {

        /* reach to the specific entry */
        pCurrApp = (tTrieApp *) (pLeaf->pAppSpecInfo);
        pPrevApp = (tTrieApp **) (VOID *) &(pLeaf->pAppSpecInfo);
        for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)

        {
            if (CHECK_BIT (i4Idx, pLeaf->u2AppMask))

            {
                pPrevApp = (tTrieApp **) (VOID *) &(pCurrApp->pNextApp);
                pCurrApp = (tTrieApp *) (pCurrApp->pNextApp);
            }
        }

        /* Check whether the application is present or not */
        if (CHECK_BIT ((INT4) pInputParams->i1AppId, pLeaf->u2AppMask))

        {

            /* Application is present. Either add or replace the entry */
            pTmpApp = (tTrieApp *) (pCurrApp);

            if (pTmpApp == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }

            /* Check whether the number of entries is less than maximum entres */
            TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeaf->
                                           au4NoOfMultipath[u1AppId],
                                           u4NumOfMultiPath);

            if (u4NumOfMultiPath < MAX_PATHS)
            {
                u4Metric = ((tTrieApp *) pAppSpecInfo)->u4RtMetr1;
                u4NextHop = ((tRtInfo *) pAppSpecInfo)->u4NextHop;
                u4IfIndex = ((tRtInfo *) pAppSpecInfo)->u4RtIfIndx;

                /* Incoming Route's metric is lesser than the first entry */
                if (pTmpApp->u4RtMetr1 > u4Metric)

                {
                    if (pLeaf->pListNode != NULL)

                    {
                        pOutputParams->pObject2 = pLeaf->pListNode;
                        pOutputParams->pObject1 = NULL;
                    }

                    else

                    {
                        pOutputParams->pObject1 = pLeaf;
                        pOutputParams->pObject2 = NULL;
                    }
                    pLeaf->u1CurApp = u1AppId;
                    ((tTrieApp *) pAppSpecInfo)->pNextApp = pTmpApp->pNextApp;
                    ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = pTmpApp;
                    *pPrevApp = (tTrieApp *) pAppSpecInfo;
                    pTmpApp->pNextApp = NULL;
                    if (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE)

                    {
                        TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE
                            (pLeaf->au4NoOfMultipath[u1AppId]);
                    }
                }

                else

                {
                    if ((u4Metric == pTmpApp->u4RtMetr1) &&
                        (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE))

                    {
                        if (!((u1AppId == (OSPF_RT_ID - 1)) ||
                              (u1AppId == (OSPF_CAL_RT_ID - 1))))

                        {
                            if ((u4NextHop ==
                                 ((tRtInfo *) pTmpApp)->
                                 u4NextHop) && (u4IfIndex ==
                                                ((tRtInfo *) pTmpApp)->
                                                u4RtIfIndx))

                            {
                                OsixSemGive (TRIE_SEM_ID);
                                return TRIE_FAILURE;
                            }
                        }
                        if (u1AppId < MAX_APPLICATIONS)
                        {
                            TRIE_INC_EQUAL_COST_ROUTES (pLeaf->au4NoOfMultipath
                                                        [u1AppId]);
                        }
                        u1EcmpRoute = OSIX_TRUE;
                    }

                    while ((pTmpApp != NULL)
                           && (pTmpApp->u4RtMetr1 <= u4Metric))

                    {
                        /*
                         * Avoid adding duplicate nodes
                         */
                        if (((tTrieApp *) pAppSpecInfo)->u4NxtHop ==
                            pTmpApp->u4NxtHop)
                        {
                            if (u1EcmpRoute == OSIX_TRUE)
                            {
                                if (u1AppId < MAX_APPLICATIONS)
                                {
                                    TRIE_DEC_NUM_EQUAL_COST_ROUTES (pLeaf->
                                                                    au4NoOfMultipath
                                                                    [u1AppId]);
                                }
                            }
                            OsixSemGive (TRIE_SEM_ID);
                            return (TRIE_SUCCESS);
                        }

                        pPrevPath = pTmpApp;
                        pTmpApp = pTmpApp->pNextAlternatepath;
                    }
                    ((tTrieApp *) pAppSpecInfo)->pNextApp = NULL;
                    ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = pTmpApp;
                    if (pPrevPath == NULL)
                    {
                        OsixSemGive (TRIE_SEM_ID);
                        return (TRIE_FAILURE);
                    }
                    pPrevPath->pNextAlternatepath = (tTrieApp *) pAppSpecInfo;
                }
                TRIE_INC_TOTAL_NUM_OF_ROUTES (pLeaf->au4NoOfMultipath[u1AppId]);
            }

            else

            {

                /* Oops!! Can not add any more Routes - Overflow !!! */
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }
        }

        else

        {
            pLeaf->u1NumApp++;
            SET_BIT (u1AppId, pLeaf->u2AppMask);
            TRIE_INC_TOTAL_NUM_OF_ROUTES (pLeaf->au4NoOfMultipath[u1AppId]);

            /* Database has changed so unlink the ListNode and
             * append it at the end.
             */
            if (pLeaf->pListNode != NULL)

            {
                pOutputParams->pObject2 = pLeaf->pListNode;
                pOutputParams->pObject1 = NULL;
            }

            else

            {
                pOutputParams->pObject1 = pLeaf;
                pOutputParams->pObject2 = NULL;
            }
            pLeaf->u1CurApp = u1AppId;

            /* reach to the specific entry and insert new entry */
            ((tTrieApp *) pAppSpecInfo)->pNextApp = pCurrApp;
            ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = NULL;
            *pPrevApp = (tTrieApp *) pAppSpecInfo;
            if (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE)

            {
                TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE (pLeaf->
                                                       au4NoOfMultipath
                                                       [u1AppId]);
            }
        }
    }

    else

    {
        DIFFBIT (u1DiffBit, u1DiffByteVal);
        u1BitToTest = (UINT1) (0x80 >> u1DiffBit);

        /*u1DiffBit   += BYTE_LENGTH*u4DiffByte; */
        pParentNode = pLeaf->pParent;
        while ((u4DiffByte < pParentNode->u1ByteToTest) ||
               ((u4DiffByte == pParentNode->u1ByteToTest) &&
                (u1BitToTest > pParentNode->u1BitToTest)))

        {
            pParentNode = pParentNode->pParent;
        }
        if (TrieAddRadix
            (u1KeySize, u1BitToTest, (UINT1) u4DiffByte, pInputParams,
             pAppSpecInfo, pParentNode, pOutputParams) == TRIE_FAILURE)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }
    }

    /* Make output parameters null to represent fresh addition */
    pOutputParams->pAppSpecInfo = (void *) NULL;
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDeleteEntry()                                        */
/*                                                                         */
/*  Description   Delete operation is always preceded by an exact search   */
/*                internally. After finding the required information, the  */
/*                application specific information and the key is          */
/*                returned to the application using the tOutputParams      */
/*                structure to delete actually. If search operation is     */
/*                unsuccessful, this function returns with TRIE_FAILURE. This */
/*                function deletes unnecessary radix/leaf node, if any.    */
/*                                                                         */
/*  Call          Whenever an application wants to remove an entry from    */
/*  condition     this instance of Trie.                                   */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If  deleting  operation  is successful. */
/*                TRIE_FAILURE  -  If  it  fails  due  to  any reasons.    */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieDeleteEntry (tInputParams * pInputParams,
                 tOutputParams * pOutputParams, UINT4 u4NxtHop)
{
    UINT1               u1KeySize;
    UINT1               u1AppId;
    UINT4               u4NumOfMultipath;
    INT4                i4Eql;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    INT4                i4RetVal = TRIE_SUCCESS;
    INT1                u1RtType;

    /* Following is valid only for RIP */
    pOutputParams->u1LeafFlag = TRUE;
    OsixSemTake (TRIE_SEM_ID);
    pRadix = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    InKey = pInputParams->Key;
    if (TrieTraverse (u1KeySize, pRadix, InKey, (void *) &(pLeaf)) ==
        TRIE_FAILURE)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    u1AppId = (UINT1) pInputParams->i1AppId;

    /*TrieTraverse returned a leaf node. */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u1KeySize);
    if (i4Eql != 0)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    /* Check if application entry exists in the leaf node */
    if (!(CHECK_BIT ((INT4) pInputParams->i1AppId, pLeaf->u2AppMask)))

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    /* if only one application exists, delete the leaf node */
    if (pLeaf->u1NumApp > 1)

    {
        i4RetVal =
            TrieDeleteAppAndUpdateLeaf (u1KeySize, pInputParams, pLeaf,
                                        pOutputParams, u4NxtHop);
    }

    else

    {

        /* The following Check is done with the assumption that TrieDeleteEnry 
           is called with valid nexthop only after a TrieSearchEntry.So
           when passed NxtHop is valid and the number of routes in leaf for
           that application is one, it must be the entry we are targetting!!! */
        TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeaf->au4NoOfMultipath[u1AppId],
                                       u4NumOfMultipath);
        if ((u4NxtHop == INVALID_NEXTHOP) || (u4NumOfMultipath == 1))

        {
            if (((u1AppId == (OSPF_RT_ID - 1)) ||
                 (u1AppId == (OSPF_CAL_RT_ID - 1))))

            {
                u1RtType = (INT1) pInputParams->u1Reserved1;
                if ((u4NxtHop == ((tTrieApp *)
                                  (pLeaf->pAppSpecInfo))->u4NxtHop))

                {
                    if ((u1RtType != ((UINT1) ((tTrieApp *)
                                               (pLeaf->pAppSpecInfo))->
                                      u2RtType)))

                    {

                        /* Only one routing entry available
                         * whose route type doesn't match 
                         * the incoming route type */
                        OsixSemGive (TRIE_SEM_ID);
                        return TRIE_FAILURE;
                    }

                    /* Only one routing entry available
                     * whose route type and NEXTHOP matches
                     * so delete the routing entry */
                    TrieDeleteRadixLeaf (u1KeySize, pLeaf, pOutputParams,
                                         u1AppId);
                    OsixSemGive (TRIE_SEM_ID);
                    return i4RetVal;
                }
                if ((u4NxtHop != INVALID_NEXTHOP) && (u4NxtHop != 0))

                {

                    /* No Route Entry with this NEXT_HOP exists
                     * so returning FAILURE */
                    OsixSemGive (TRIE_SEM_ID);
                    return TRIE_FAILURE;
                }
            }
            if (pLeaf->pListNode != NULL)

            {
                pOutputParams->pObject1 = NULL;
                pOutputParams->pObject2 = pLeaf->pListNode;
                pOutputParams->u1LeafFlag = FALSE;
            }
            TrieDeleteRadixLeaf (u1KeySize, pLeaf, pOutputParams, u1AppId);
        }

        else

        {
            i4RetVal =
                TrieDeleteAppAndUpdateLeaf (u1KeySize, pInputParams, pLeaf,
                                            pOutputParams, u4NxtHop);
        }
    }
    OsixSemGive (TRIE_SEM_ID);

    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*  Function      INT4 TrieSearchEntry ()                                  */
/*                                                                         */
/*  Description   This function searches for the specified key given by    */
/*                the tInputParams in an instance of Trie. If the key is   */
/*                found, this function copies the application specific     */
/*                information and key of the searched entry to the         */
/*                tOutputParams. If the  matching key is not found,        */
/*                it returns TRIE_FAILURE. This is an exact match operation.*/
/*                The result of this operation should not be used to       */
/*                delete entries present in the Trie instance.             */
/*                TrieDeleteEntry () must be called for deleting entries.  */
/*                                                                         */
/*  Call          whenever an application needs to search for              */
/*  condition     information associated with the given key only.          */
/*                                                                         */
/*  Input(s)      pInputParams -  Pointer to the tInputParams structure.   */
/*                                                                         */
/*  Output(s)     pOutputParams - Pointer to the tOutputParams structure.  */
/*                                                                         */
/*  Access        The applications using the Trie library.                 */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If the key is found.                      */
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieSearchEntry (tInputParams * pInputParams, tOutputParams * pOutputParams)
{
    INT1                i1AppId;
    UINT1               u1KeySize;
    UINT2               u2CurrAppBit;
    INT4                i4Eql;
    INT4                i4Idx;
    UINT4               u4Counter;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    void               *pCurrApp;

    OsixSemTake (TRIE_SEM_ID);
    if(pInputParams->pRoot == NULL) 
    { 
        OsixSemGive (TRIE_SEM_ID); 
        return (TRIE_FAILURE); 
    } 

    pRadix = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    InKey = pInputParams->Key;
    i1AppId = pInputParams->i1AppId;
    if (TrieTraverse (u1KeySize, pRadix, InKey, (void *) &(pLeaf)) ==
        TRIE_FAILURE)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    /*TrieTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u1KeySize);
    if (i4Eql != 0)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    /* If application id is generic appid, than return
     * every application specific entry of the leaf node in its proper place.
     */
    pCurrApp = pLeaf->pAppSpecInfo;
    if (pCurrApp == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    if (i1AppId < 0)

    {
        u2CurrAppBit = 01;
        for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
             u4Counter++)

        {
            if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

            {
                ((tTrieApp **) pOutputParams->pAppSpecInfo)[u4Counter] =
                    ((tTrieApp *) pCurrApp);
                pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;
            }
            if (pCurrApp == NULL)
            {
                break;
            }
            u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
        }
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_SUCCESS);
    }

    /* Check if application entry exists in the leaf node */
    if (!(CHECK_BIT (i1AppId, pLeaf->u2AppMask)))

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

    {
        pCurrApp = (CHECK_BIT (i4Idx, pLeaf->u2AppMask)) ?
            ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
    }

    /* copy the entry in output parameters */
    pOutputParams->pAppSpecInfo = pCurrApp;
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLookupEntry()                                        */
/*                                                                         */
/*  Description   This function first searches for an exact match in an    */
/*                instance of Trie. However, if it is not successful in    */
/*                finding exact match, it looks for the best match. If it  */
/*                fails in finding the best match also, it returns         */
/*                TRIE_FAILURE. The result of this operation should not be   */
/*                used to delete entries which are present in Trie. If     */
/*                the return value is TRIE_SUCCESS, tOutputParams will     */
/*                contain related information of exact/best match including */
/*                the key.                                                 */
/*                                                                         */
/*  Call          This function is mostly appropriate to routing           */
/*  condition     tables. Here, a key of the tInputParams represents the   */
/*                IP address. The application, by calling this function,   */
/*                requests best match.                                     */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If it finds the exact/best match.       */
/*                TRIE_FAILURE  -  If it fails in finding  the             */
/*                            best match also.                             */
/*                                                                         */
/***************************************************************************/
INT4
TrieLookupEntry (tInputParams * pInputParams, tOutputParams * pOutputParams)
{
    INT1                i1AppId;
    UINT1               u1KeySize;
    UINT1               u4Counter;
    UINT2               u2CurrAppBit;
    INT4                i4Eql;
    INT4                i4Idx;
    INT4                i4Return;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    void               *pCurrApp;
    UINT4              *pLeafMultiPath;
    UINT4              *pOutparamsMultiPath;

    OsixSemTake (TRIE_SEM_ID);
    pRadix = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    InKey = pInputParams->Key;
    i1AppId = pInputParams->i1AppId;
    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    if (TrieTraverse (u1KeySize, pRadix, InKey, (void *) &(pLeaf)) ==
        TRIE_FAILURE)

    {
        if (u1KeySize > U4_SIZE)

        {
            i4Return =
                TrieFindBestMatch (u1KeySize, pInputParams, (void *) pLeaf,
                                   pOutputParams);
        }

        else

        {
            i4Return =
                TrieFindBestMatch4 (u1KeySize, pInputParams, (void *) pLeaf,
                                    pOutputParams);
        }
        if (i4Return == TRIE_FAILURE)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }

        else

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_SUCCESS);
        }
    }

    /*TrieTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u1KeySize);
    if (i4Eql == 0)

    {

        /* If application id is generic appid, than return
         * every application specific entry of the leaf node in its proper place.
         */
        pCurrApp = pLeaf->pAppSpecInfo;
        if (i1AppId < 0)

        {
            u2CurrAppBit = 01;
            for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
                 u4Counter++)

            {
                if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                {
                    ((tTrieApp **) (pOutputParams->pAppSpecInfo))[u4Counter]
                        = ((tTrieApp *) pCurrApp);
                    if (pCurrApp == NULL)
                    {
                        OsixSemGive (TRIE_SEM_ID);
                        return (TRIE_FAILURE);
                    }

                    pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;
                    pLeafMultiPath = &(pLeaf->au4NoOfMultipath[u4Counter]);
                    pOutparamsMultiPath =
                        &(pOutputParams->au4NoOfMultipath[u4Counter]);
                    TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES (*pLeafMultiPath,
                                                        *pOutparamsMultiPath);
                }
                u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
            }
            if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

            {
                KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
            }
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_SUCCESS);
        }

        /* Check if application entry exists in the leaf node */
        if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

        {
            for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

            {
                pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                    ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
            }

            /* copy the entry in output parameters */
            pOutputParams->pAppSpecInfo = pCurrApp;

/*         pOutputParams->au4NoOfMultipath[i4Idx] = 
                             pLeaf->au4NoOfMultipath[i4Idx]; */
            TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES (pLeaf->
                                                au4NoOfMultipath[i4Idx],
                                                pOutputParams->
                                                au4NoOfMultipath[i4Idx]);
            if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

            {
                KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
            }
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_SUCCESS);
        }
    }
    if (u1KeySize > U4_SIZE)

    {
        i4Return =
            TrieFindBestMatch (u1KeySize, pInputParams, (void *) pLeaf,
                               pOutputParams);
    }

    else

    {
        i4Return =
            TrieFindBestMatch4 (u1KeySize, pInputParams, (void *) pLeaf,
                                pOutputParams);
    }
    if (i4Return == TRIE_SUCCESS)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_SUCCESS);
    }

    else

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieScan ()                                              */
/*                                                                         */
/*  Description   This function scans entire instance of Trie for a        */
/*                given application. AppSpecScanFunc () is called after    */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns TRIE_FAILURE, this*/
/*                function also returns TRIE_FAILURE.                      */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     Trie has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS -  If this operation  is  done successfully. */
/*                TRIE_FAILURE -  If this operation is aborted due to the  */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieScan (tInputParams * pInputParams,
          INT4 (*AppSpecScanFunc) (tScanOutParams *),
          tScanOutParams * pScanOutParams)
{
    UINT1               u1AppId;
    UINT1               u1KeySize;
    INT4                i4Idx;
    UINT4               u4TmpNumEntries;
    void               *pCurrNode;
    void               *pCurrApp;
    tRadixNode         *pPrevNode;
    tLeafNode          *pLeaf;

    OsixSemTake (TRIE_SEM_ID);
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    u4TmpNumEntries = 0;
    pPrevNode = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    pCurrNode = (void *) pPrevNode;
    u1AppId = (UINT1) pInputParams->i1AppId;
    if (u1AppId == 0xff)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TrieScanAll (pInputParams, AppSpecScanFunc, pScanOutParams));
    }
    if (pPrevNode == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    if (pPrevNode->pLeft == NULL)

    {
        if (pPrevNode->pRight == NULL)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_SUCCESS);
        }

        else

        {
            pCurrNode = pPrevNode->pRight;
        }
    }
    while (1)

    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))

        {
            pLeaf = (tLeafNode *) pCurrNode;
            pCurrApp = pLeaf->pAppSpecInfo;
            for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)

            {
                pCurrApp = CHECK_BIT (i4Idx, pLeaf->u2AppMask) ?
                    ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
            }
            if (CHECK_BIT (i4Idx, pLeaf->u2AppMask))

            {

                /* the application specific entry is present */
                while ((pCurrApp) != NULL)

                {
                    ((tTrieApp **) pScanOutParams->
                     pAppSpecInfo)[u4TmpNumEntries] = ((tTrieApp *) pCurrApp);

                    if (pScanOutParams->pKey != NULL)
                    {
                        if ((u1KeySize == U4_SIZE) ||
                            ((tKey *) pScanOutParams->pKey)[u4TmpNumEntries].
                            pKey != NULL)
                        {
                            KEYCOPY (((tKey *) pScanOutParams->pKey)
                                     [u4TmpNumEntries], pLeaf->Key, u1KeySize);
                        }
                    }

                    u4TmpNumEntries++;
                    if (u4TmpNumEntries == pScanOutParams->u4NumEntries)

                    {
                        if ((AppSpecScanFunc) (pScanOutParams) == TRIE_FAILURE)

                        {
                            OsixSemGive (TRIE_SEM_ID);
                            return (TRIE_FAILURE);
                        }
                        u4TmpNumEntries = 0;
                    }
                    pCurrApp = ((tTrieApp *) pCurrApp)->pNextAlternatepath;
                }
            }
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else

            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))

                {
                    pCurrNode = (void *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)

                {
                    break;
                }

                else

                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }

        else

        {
            if (pCurrNode == NULL)

            {
                break;
            }

            /* radix node */
            while (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE)

            {
                pPrevNode = (tRadixNode *) pCurrNode;
                pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
            }
        }
    }
    if (u4TmpNumEntries != 0)

    {
        pScanOutParams->u4NumEntries = u4TmpNumEntries;
        (AppSpecScanFunc) (pScanOutParams);
    }
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetNextEntry                                         */
/*                                                                         */
/*  Description   This function finds the next key and the associated      */
/*                application specfic entry  using a given key and         */
/*                application id. The input key needs  to be present       */
/*                inside the trie structure. If input id is a generic id,  */
/*                it returns every application specific information along  */
/*                with the key.                                            */
/*                                                                         */
/*  Call         When next entry in the routing table is needed.           */
/*  condition    typically for SNMP.                                       */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the Trie library.                 */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the next entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetNextEntry (tInputParams * pInputParams, tOutputParams * pOutputParams)
{
    INT1                i1AppId;
    UINT1               u1KeySize;
    UINT2               u2CurrAppBit;
    INT4                i4Cmp;
    tKey                InKey;
    void               *pCurrNode;
    tRadixNode         *pPrevNode;
    tLeafNode          *pLeaf;

    OsixSemTake (TRIE_SEM_ID);
    if(pInputParams->pRoot == NULL) 
    { 
        OsixSemGive (TRIE_SEM_ID); 
        return (TRIE_FAILURE); 
    } 

    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    pPrevNode = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    pCurrNode = (void *) pPrevNode;
    i1AppId = pInputParams->i1AppId;
    InKey = pInputParams->Key;
    if ((pPrevNode->pRight == NULL) && (pPrevNode->pLeft == NULL))

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    if (TrieTraverse (u1KeySize, pPrevNode, InKey,
                      (void *) &(pLeaf)) == TRIE_FAILURE)

    {
        pCurrNode = NULL;
        pPrevNode = (tRadixNode *) pLeaf;
    }

    else

    {
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u1KeySize);
        if (i4Cmp < 0)

        {
            void               *pCurrApp;
            pCurrApp = pLeaf->pAppSpecInfo;
            if (pCurrApp == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }

            if (i1AppId < 0)

            {
                UINT4               u4Counter;
                u2CurrAppBit = 01;
                for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
                     u4Counter++)

                {
                    if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                    {
                        ((tTrieApp **) pOutputParams->
                         pAppSpecInfo)[u4Counter] = ((tTrieApp *) pCurrApp);
                        pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;
                        if (pCurrApp == NULL)
                        {
                            break;
                        }
                    }
                    u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                }
                if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

                {
                    KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
                }
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_SUCCESS);
            }
            if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

            {
                INT4                i4Idx;
                for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                {
                    pCurrApp = (CHECK_BIT (i4Idx, pLeaf->u2AppMask)) ?
                        ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                }

                /* copy the entry in output parameters */
                pOutputParams->pAppSpecInfo = pCurrApp;
                if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

                {
                    KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
                }
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_SUCCESS);
            }
        }
        pPrevNode = pLeaf->pParent;
        pCurrNode = pLeaf;
    }

    if (pPrevNode == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    while (1)

    {

        /* here logic is same as that used in TrieScan */
        if (pPrevNode->pLeft == pCurrNode)

        {
            pCurrNode = pPrevNode->pRight;
        }

        else

        {
            while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))

            {
                pCurrNode = (void *) pPrevNode;
                pPrevNode = pPrevNode->pParent;
            }
            if (pPrevNode == NULL)

            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }

            else

            {
                pCurrNode = pPrevNode->pRight;
            }
        }

        /* find out the next leaf node or NULL */
        while ((pCurrNode != NULL) &&
               (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE))

        {
            pPrevNode = pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
        if (pCurrNode == NULL)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }
        pLeaf = (tLeafNode *) pCurrNode;
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u1KeySize);
        if (i4Cmp < 0)

        {
            void               *pCurrApp;
            pCurrApp = pLeaf->pAppSpecInfo;
            if (pCurrApp == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }

            /* global task has enquired. so  fill all the entries */
            if (i1AppId < 0)

            {
                UINT4               u4Counter;
                u2CurrAppBit = 01;
                for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2);
                     u4Counter++)

                {
                    if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

                    {
                        ((tTrieApp **) pOutputParams->
                         pAppSpecInfo)[u4Counter] = ((tTrieApp *) pCurrApp);
                        pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;
                        if (pCurrApp == NULL)
                        {
                            break;
                        }
                    }
                    u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
                }
                if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

                {
                    KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
                }
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_SUCCESS);
            }

            /* application specific entry is present in the leaf node */
            if (CHECK_BIT (i1AppId, pLeaf->u2AppMask))

            {
                INT4                i4Idx;
                for (i4Idx = 0; i4Idx < i1AppId; i4Idx++)

                {
                    pCurrApp = (CHECK_BIT (i4Idx, pLeaf->u2AppMask)) ?
                        ((tTrieApp *) pCurrApp)->pNextApp : pCurrApp;
                }

                /* copy the entry in output parameters */
                pOutputParams->pAppSpecInfo = pCurrApp;
                if ((u1KeySize == U4_SIZE) || (pOutputParams->Key.pKey != NULL))

                {
                    KEYCOPY (pOutputParams->Key, pLeaf->Key, u1KeySize);
                }
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_SUCCESS);
            }
        }
    }
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieUpdate                                               */
/*                                                                         */
/*  Description   This function updates the alternate path list.           */
/*                Whenever there is a change in Metric or Row Status, this */
/*                function should be called. This sorts based on the new   */
/*                metric. This function helps to avoid the row status check */
/*                in Forwarding path.                                      */
/*                                                                         */
/*  Call         Whenever there is a change in Metric for the existing     */
/*  condition    route.                                                    */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the Trie library.                 */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the next entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieUpdate (tInputParams * pInputParams,
            void *pAppSpecInfo,
            UINT4 u4NewMetric, tOutputParams * pOutputParams)
{
    UINT1               u1AppId;
    UINT1               u1KeySize;
    UINT4               u4NumOfMultipath;
    UINT4               u4BestMetric;
    INT4                i4Idx;
    tKey                InKey;
    tRadixNode         *pRadix;
    tTrieApp           *pCurrApp;
    tTrieApp           *pTmpApp;
    tTrieApp           *pBackupApp;
    tTrieApp          **pPrevApp;
    tLeafNode          *pLeaf;
    tTrieApp           *pPrevPath = NULL;

    /* The following is valid only for RIP */
    pOutputParams->u1LeafFlag = TRUE;
    OsixSemTake (TRIE_SEM_ID);
    pRadix = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    InKey = pInputParams->Key;
    u1AppId = (UINT1) pInputParams->i1AppId;
    pLeaf = NULL;
    if (TrieTraverse (u1KeySize, pRadix, InKey, (void *) &(pLeaf)) ==
        TRIE_FAILURE)

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }

    /* TrieTraverse returned a leaf node. */

    /* Check whether the application is present or not */
    if (CHECK_BIT ((INT4) pInputParams->i1AppId, pLeaf->u2AppMask))

    {

        /* reach to the specific entry */
        pCurrApp = (tTrieApp *) (pLeaf->pAppSpecInfo);
        pPrevApp = (tTrieApp **) (VOID *) &(pLeaf->pAppSpecInfo);
        for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)

        {
            if (CHECK_BIT (i4Idx, pLeaf->u2AppMask))

            {
                pPrevApp = (tTrieApp **) (VOID *) &(pCurrApp->pNextApp);
                pCurrApp = (tTrieApp *) (pCurrApp->pNextApp);
            }
        }
        pTmpApp = pCurrApp;
        if (pTmpApp == NULL)
        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }
        TRIE_COPY_TOTAL_NUM_OF_ROUTES (pLeaf->au4NoOfMultipath[u1AppId],
                                       u4NumOfMultipath);

        /* We have only one member. Need not sort it. */
        if (u4NumOfMultipath == 1)

        {
            if (pTmpApp == pAppSpecInfo)

            {
                if (((tTrieApp *) pAppSpecInfo)->u4RowStatus != ACTIVE)

                {
                    TRIE_INIT_EQUAL_COST_ROUTES (pLeaf->au4NoOfMultipath
                                                 [u1AppId]);
                }
                if (u1AppId == pLeaf->u1CurApp)

                {

                    /* unlink and append the node */
                    pOutputParams->pObject2 = pLeaf->pListNode;
                    pOutputParams->pObject1 = NULL;
                }
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_SUCCESS);
            }
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }
        pBackupApp = pTmpApp;

        if (pBackupApp == NULL)
        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_FAILURE);
        }

        if (pTmpApp == pAppSpecInfo)

        {
            if (u1AppId == pLeaf->u1CurApp)

            {

                /* unlink and append the node */
                pOutputParams->pObject2 = pLeaf->pListNode;
                pOutputParams->pObject1 = NULL;
            }
            pBackupApp = pTmpApp->pNextAlternatepath;
            if (pBackupApp == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }
            ((tTrieApp *) pTmpApp->pNextAlternatepath)->pNextApp =
                pTmpApp->pNextApp;
            (*pPrevApp) = ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath;
        }

        else

        {
            do
            {
                pPrevPath = pTmpApp;
                pTmpApp = pTmpApp->pNextAlternatepath;
                if (pTmpApp == (tTrieApp *) pAppSpecInfo)

                {
                    pPrevPath->pNextAlternatepath = pTmpApp->pNextAlternatepath;
                    break;
                }
            }
            while (pTmpApp != NULL);
        }

        if (pBackupApp->u4RtMetr1 > u4NewMetric)

        {
            if (u1AppId == pLeaf->u1CurApp)

            {

                /* unlink and append the node */
                pOutputParams->pObject2 = pLeaf->pListNode;
                pOutputParams->pObject1 = NULL;
            }
            ((tTrieApp *) pAppSpecInfo)->pNextApp = pBackupApp->pNextApp;
            ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = pBackupApp;
            (*pPrevApp) = (tTrieApp *) pAppSpecInfo;
            pBackupApp->pNextApp = NULL;
            if (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE)

            {
                TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE (pLeaf->
                                                       au4NoOfMultipath
                                                       [u1AppId]);
            }
        }

        else

        {
            u4BestMetric = pBackupApp->u4RtMetr1;
            if ((u4BestMetric == u4NewMetric) &&
                (((tTrieApp *) pAppSpecInfo)->u4RowStatus == ACTIVE))

            {
                TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE (pLeaf->
                                                       au4NoOfMultipath
                                                       [u1AppId]);
            }

            else
                TRIE_INIT_EQUAL_COST_ROUTES (pLeaf->au4NoOfMultipath[u1AppId]);
            while ((pBackupApp != NULL) &&
                   (pBackupApp->u4RtMetr1 <= u4NewMetric))

            {
                if (u4BestMetric == pBackupApp->u4RtMetr1)
                    TRIE_INC_EQUAL_COST_ROUTES (pLeaf->au4NoOfMultipath
                                                [u1AppId]);
                pPrevPath = pBackupApp;
                pBackupApp = pBackupApp->pNextAlternatepath;
            }
            ((tTrieApp *) pAppSpecInfo)->pNextApp = NULL;
            ((tTrieApp *) pAppSpecInfo)->pNextAlternatepath = pBackupApp;
            if (pPrevPath == NULL)
            {
                OsixSemGive (TRIE_SEM_ID);
                return (TRIE_FAILURE);
            }
            pPrevPath->pNextAlternatepath = (tTrieApp *) pAppSpecInfo;
        }
    }

    else

    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

INT4
TrieGetLeafInfo (tInputParams * pInParms, tOutputParams * pOutParms)
{
    tLeafNode          *pLeaf = (tLeafNode *) pInParms->pRoot;
    void               *pCurrApp = NULL;
    UINT4               u4Counter;
    UINT2               u2CurrAppBit;

    if (pLeaf == NULL)

    {
        return TRIE_FAILURE;
    }
    OsixSemTake (TRIE_SEM_ID);
    pCurrApp = pLeaf->pAppSpecInfo;
    u2CurrAppBit = 01;
    for (u4Counter = 0; u4Counter < BYTE_LENGTH * sizeof (UINT2); u4Counter++)

    {
        if (BIT_TEST (pLeaf->u2AppMask, u2CurrAppBit))

        {
            ((tTrieApp **) pOutParms->pAppSpecInfo)[u4Counter] =
                ((tTrieApp *) pCurrApp);
            pCurrApp = ((tTrieApp *) pCurrApp)->pNextApp;
        }
        u2CurrAppBit = (UINT2) (u2CurrAppBit << 1);
    }
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieScanAll ()                                           */
/*                                                                         */
/*  Description   This function scans entire instance of Trie for all      */
/*                applications. AppSpecScanFunc () is called after         */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns TRIE_FAILURE, this  */
/*                function also returns TRIE_FAILURE.                        */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     Trie has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS -  If this operation  is  done successfully.  */
/*                TRIE_FAILURE -  If this operation is aborted due to the    */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieScanAll (tInputParams * pInputParams,
             INT4 (*AppSpecScanFunc) (tScanOutParams *),
             tScanOutParams * pScanOutParams)
{
    UINT1               u1AppId;
    UINT1               u1KeySize;
    INT4                i4Idx;
    UINT4               u4TmpNumEntries;
    void               *pCurrNode;
    void               *pCurrApp;
    tRadixNode         *pPrevNode;
    tLeafNode          *pLeaf;
    void               *pTemp;

    OsixSemTake (TRIE_SEM_ID);
    u1KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u1KeySize;
    u4TmpNumEntries = 0;
    pPrevNode = ((tRadixNodeHead *) pInputParams->pRoot)->pRadixNodeTop;
    if (pPrevNode == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_SUCCESS);
    }
    pCurrNode = (void *) pPrevNode;
    u1AppId = (UINT1) pInputParams->i1AppId;
    if (pPrevNode->pLeft == NULL)

    {
        if (pPrevNode->pRight == NULL)

        {
            OsixSemGive (TRIE_SEM_ID);
            return (TRIE_SUCCESS);
        }

        else

        {
            pCurrNode = pPrevNode->pRight;
        }
    }
    while (1)

    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))

        {
            pLeaf = (tLeafNode *) pCurrNode;
            pCurrApp = pLeaf->pAppSpecInfo;
            for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)

            {
                if (CHECK_BIT (i4Idx, pLeaf->u2AppMask))

                {
                    pTemp = pCurrApp;

                    /* the application specific entry is present */
                    while ((pCurrApp) != NULL)

                    {
                        ((tTrieApp **) pScanOutParams->
                         pAppSpecInfo)[u4TmpNumEntries] =
                        ((tTrieApp *) pCurrApp);

                        if (pScanOutParams->pKey != NULL)
                        {
                            if ((u1KeySize == U4_SIZE) ||
                                ((tKey *) pScanOutParams->
                                 pKey)[u4TmpNumEntries].pKey != NULL)
                            {
                                KEYCOPY (((tKey *) pScanOutParams->pKey)
                                         [u4TmpNumEntries], pLeaf->Key,
                                         u1KeySize);
                            }
                        }

                        u4TmpNumEntries++;
                        if (u4TmpNumEntries == pScanOutParams->u4NumEntries)

                        {
                            if ((AppSpecScanFunc) (pScanOutParams) ==
                                TRIE_FAILURE)

                            {
                                OsixSemGive (TRIE_SEM_ID);
                                return (TRIE_FAILURE);
                            }
                            u4TmpNumEntries = 0;
                        }
                        pCurrApp = ((tTrieApp *) pCurrApp)->pNextAlternatepath;
                    }
                    pCurrApp = ((tTrieApp *) pTemp)->pNextApp;
                }
            }
            if (pPrevNode->pLeft == pCurrNode)

            {
                pCurrNode = pPrevNode->pRight;
            }

            else

            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))

                {
                    pCurrNode = (void *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)

                {
                    break;
                }

                else

                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }

        else

        {
            if (pCurrNode == NULL)

            {
                break;
            }

            /* radix node */
            while (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE)

            {
                pPrevNode = (tRadixNode *) pCurrNode;
                pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
            }
        }
    }
    if (u4TmpNumEntries != 0)

    {
        pScanOutParams->u4NumEntries = u4TmpNumEntries;
        (AppSpecScanFunc) (pScanOutParams);
    }
    OsixSemGive (TRIE_SEM_ID);
    return (TRIE_SUCCESS);
}
