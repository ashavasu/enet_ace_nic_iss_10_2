/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dbutil.h,v 1.1 2015/04/28 12:53:02 siva Exp $
 * 
 * Description: This file contains data structure, definitions for constants
 *              used for protocol database implementation.
 *********************************************************************/

#ifndef __DBUTIL_H__
#define __DBUTIL_H__

#include "rmgr.h"
/* Every data structure containing dynamic information, a DbNode need to
 * be embedded. Whenever dynamic information of a structure changes the
 * corresponding embedded DbNode should get added into ModuleDbTbl and when
 * this information needs to be synced up, a utility API should be called to
 * sync all the changed data and flush this data from the ModuleDbTbl.
 * ModuleDbTbl will be required for every module that needs to sync dynamic
 * information.
 */

typedef tTMO_DLL tDbTbl;

#define DB_MAX_BUF_SIZE 1500

#define DbNodeFind TMO_DLL_Find
#define DbNodeCount TMO_DLL_Count
#define DbNodeAdd TMO_DLL_Add
#define DbNodeDel TMO_DLL_Delete
#define DbNodeScan TMO_DLL_Scan
#define DbNodeDeleteAll TMO_DLL_Clear
#define DbNodeNext TMO_DLL_Next
#define DbNodeFirst TMO_DLL_First

typedef struct _sDbTblNode{
    tTMO_DLL_NODE     TblNode;
    UINT4             u4DataDescId;                  
                          /* This is descriptor id to hold module specific data
                           * structure enum. Module ID and this Descriptor can 
                           * be used to uniquely identify the data structure.*/

}tDbTblNode;

/* This structure holds the Offset and Size of an element that needs byte order
 * conversions (Host-To-Network / Network-To-Host) while sending/receiving it.
 */
typedef struct _sDbOffsetTemplate{
    INT2              i2Offset;
                          /* This field represents the offset of the element in
                           * the structure (from tDbNode in the structure). The
                           * offset and the structure member Size can be used to
                           * identify data type of the element in the Module 
                           * structure.*/
    UINT2             u2Size;
                          /* This field represent the size of the data structure
                           * elements. For example: sizeof (u4IfIndex) is 4 
                           * byte.*/
}tDbOffsetTemplate;

typedef struct _sDbDataDescInfo{
    tDbOffsetTemplate *pDbDataOffsetTbl;
                          /* This is a pointer to array of structures.  This 
                           * array contains (offset, size) tuple for the 
                           * elements to identify data structure members that 
                           * requires byte order conversion. This will be used 
                           * for appropriate conversion of the structure member
                           * before sending out to standby.*/
    UINT4             u4DbDataSize;
                          /* This field represent the total size of the data 
                           * structure elements from Db Node.*/
    INT4              (*pDbDataHandleFunction) (VOID *pData, VOID *pRetVal);
                          /* Function pointer that can be used to operate on
                           * module data.*/
}tDbDataDescInfo;

typedef struct _sDbDescrParams{
    tDbDataDescInfo  *pDbDataDescList;
                          /* This field will contain the list of Module 
                           * descriptors data.*/
    UINT4             u4ModuleId;
                          /* Module Id of the module (it should be same as 
                           * AppId defined in RM).*/
}tDbDescrParams;

typedef struct _sDbTblDescriptor{
    tDbTbl            DbTbl;
                          /* This field will have head pointer of the Module 
                           * dynamic data nodes DLL.*/
    tDbDescrParams    DbTblDescrParams;
}tDbTblDescriptor;

#define DB_UTIL_PKT_TYPE_FIELD_SIZE   1
#define DB_UTIL_PKT_LENGTH_FIELD_SIZE 2

PUBLIC VOID DbUtilAddTblNode (tDbTblDescriptor *pModuleDesc, tDbTblNode *pDbNode);
PUBLIC VOID DbUtilDelTblNode (tDbTblDescriptor *pModuleDesc, tDbTblNode *pDbNode);

#define DB_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define DB_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define DB_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define DB_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

PUBLIC INT4 DbUtilSyncModuleNodes (tDbTblDescriptor *pModuleDesc);
PUBLIC VOID DbUtilNodeInit (tDbTblNode *pDbNode, UINT4 u4DataDescId);
PUBLIC VOID DbUtilTblInit (tDbTblDescriptor *pModuleDesc, 
                       tDbDescrParams *pModuleParams);
PUBLIC INT4 DbUtilSyncModuleBuffers (tDbTblDescriptor * pModuleDesc,
                                     tMemPoolId PoolId);

#endif /*__DBUTIL_H__*/
