
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dbutil.c,v 1.2 2015/05/23 10:49:08 siva Exp $
 *
 * Declaration of private functions used in this file */
/*******************************************************************/
#include "lr.h"
#include "dbutil.h"
#ifdef L2RED_WANTED
#include "rmgr.h"
PRIVATE INT4        DbUtilFormSyncMsg (tDbTblNode * pDbNode,
                                       tDbDataDescInfo * pDataDescInfo,
                                       tRmMsg * pBuf, UINT4 *pu4Offset);
PRIVATE INT4        DbUtilSendMsgToRm (tRmMsg * pBuf, UINT4 u4Offset,
                                       UINT4 u4ModuleId);

#endif

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilTblInit                                          */
/*                                                                           */
/* DESCRIPTION      : This routing initializes the module descriptor table   */
/*                    with module descriptor parameters.                     */
/*                                                                           */
/* INPUT            : pModuleDesc - pointer to Module Descriptor table.      */
/*                    pModuleParams - Module specific parameter that needs   */
/*                                    to be updated in the descriptor table. */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbUtilTblInit (tDbTblDescriptor * pModuleDesc, tDbDescrParams * pModuleParams)
{
    TMO_DLL_Init (&pModuleDesc->DbTbl);

    pModuleDesc->DbTblDescrParams.pDbDataDescList
        = pModuleParams->pDbDataDescList;

    pModuleDesc->DbTblDescrParams.u4ModuleId = pModuleParams->u4ModuleId;

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilNodeInit                                         */
/*                                                                           */
/* DESCRIPTION      : This routing initializes the module dbnode of dynamic  */
/*                    info.                                                  */
/*                                                                           */
/* INPUT            : pDbNode - database node of dynamic info that needs to  */
/*                              be initialized.                              */
/*                    u4DataDescId - Module data structure identifier that   */
/*                                   will be used to uniquely identify data  */
/*                                   structure to which a node in the db     */
/*                                   table belong to.                        */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbUtilNodeInit (tDbTblNode * pDbNode, UINT4 u4DataDescId)
{
    TMO_DLL_Init_Node (&pDbNode->TblNode);
    pDbNode->u4DataDescId = u4DataDescId;

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilAddTblNode                                       */
/*                                                                           */
/* DESCRIPTION      : This routing is used to add a node into db table of the*/
/*                    module.                                                */
/*                                                                           */
/* INPUT            : pModuleDesc - pointer to module descriptor table.      */
/*                    pDbNode - database node of dyanamic info that needs to */
/*                              be added.                                    */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbUtilAddTblNode (tDbTblDescriptor * pModuleDesc, tDbTblNode * pDbNode)
{
    if ((pModuleDesc == NULL) || (pDbNode == NULL))
    {
        return;
    }

    /* If Db Node is already added, do not add it again */
    if (DbNodeFind (&pModuleDesc->DbTbl, &pDbNode->TblNode) == 0)
    {
        DbNodeAdd (&pModuleDesc->DbTbl, &pDbNode->TblNode);
    }

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilDelTblNode                                       */
/*                                                                           */
/* DESCRIPTION      : This routine is used to delete a node from db table    */
/*                    of the module.                                         */
/*                                                                           */
/* INPUT            : pModuleDesc - pointer to module descriptor table.      */
/*                    pDbNode - database node of dynamic info that needs to */
/*                              be deleted.                                  */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbUtilDelTblNode (tDbTblDescriptor * pModuleDesc, tDbTblNode * pDbNode)
{
    if ((pModuleDesc == NULL) || (pDbNode == NULL))
    {
        return;
    }
    DbNodeDel (&pModuleDesc->DbTbl, &pDbNode->TblNode);

    return;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* FUNCTION NAME    : DbUtilSyncModuleNodes                                  */
/*                                                                           */
/* DESCRIPTION      : This routing is used initiate the sync process for     */
/*                    module dynamic info.                                   */
/*                                                                           */
/* INPUT            : pModuleDesc - pointer to module descriptor table.      */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
DbUtilSyncModuleNodes (tDbTblDescriptor * pModuleDesc)
{
    tDbDataDescInfo    *pDataDescInfo = NULL;
    tDbTblNode         *pDbNode = NULL;
    tDbTblNode         *pTempDbNode = NULL;
    tRmMsg             *pBuf = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    INT4                i4OffsetValue = 0;

    if (pModuleDesc == NULL)
    {
        return OSIX_FAILURE;
    }

    ProtoEvt.u4AppId = pModuleDesc->DbTblDescrParams.u4ModuleId;

    UTL_DLL_OFFSET_SCAN (&(pModuleDesc->DbTbl), pDbNode, pTempDbNode,tDbTblNode *)
    {
	pDataDescInfo
	    =
	    &pModuleDesc->DbTblDescrParams.pDbDataDescList[pDbNode->
	    u4DataDescId];

	if (pBuf == NULL)
	{
	    pBuf = RM_ALLOC_TX_BUF (DB_MAX_BUF_SIZE);

	    if (pBuf == NULL)
	    {
		ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
		RmApiHandleProtocolEvent (&ProtoEvt);
		DbUtilDelTblNode (pModuleDesc, pDbNode);
		continue;
	    }
	}

	DbUtilFormSyncMsg (pDbNode, pDataDescInfo, pBuf, &u4Offset);
	i4OffsetValue = (INT4)(DB_MAX_BUF_SIZE - u4Offset);
	if (i4OffsetValue < (INT4) (pDataDescInfo->u4DbDataSize + 
		    sizeof (pDbNode->u4DataDescId)))
	{
	    /* No more data can be accommodated in the Buffer,
	     * Send the filled data to RM and get new buffer.
	     */
	    if (DbUtilSendMsgToRm (pBuf, u4Offset,
			pModuleDesc->DbTblDescrParams.u4ModuleId)
		    != OSIX_SUCCESS)
	    {
		RM_FREE (pBuf);
		if (DbNodeCount (&pModuleDesc->DbTbl) != 0)
		{
		    DbNodeDeleteAll (&pModuleDesc->DbTbl);
		}
		return OSIX_FAILURE;
	    }

	    u4Offset = 0;

	    pBuf = RM_ALLOC_TX_BUF (DB_MAX_BUF_SIZE);

	    if (pBuf == NULL)
	    {
		ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
		RmApiHandleProtocolEvent (&ProtoEvt);
		if (DbNodeCount (&pModuleDesc->DbTbl) != 0)
		{
		    DbNodeDeleteAll (&pModuleDesc->DbTbl);
		}
		return OSIX_FAILURE;
	    }
	}

    }

    /* Send all remaining data to RM */

    if (pBuf != NULL)
    {
	if (DbUtilSendMsgToRm (pBuf, u4Offset,
		    pModuleDesc->DbTblDescrParams.u4ModuleId)
		!= OSIX_SUCCESS)
	{
	    RM_FREE (pBuf);
	}
    }

    if (DbNodeCount (&pModuleDesc->DbTbl) != 0)
    {
        DbNodeDeleteAll (&pModuleDesc->DbTbl);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilFormSyncMsg                                      */
/*                                                                           */
/* DESCRIPTION      : This routing is used form and fill RM messages for     */
/*                    module dynamic info.                                   */
/*                                                                           */
/* INPUT            : pDbNode - database node that needs to be added into    */
/*                              rm messages.                                 */
/*                    pDataDescInfo - pointer to module desriptor info such  */
/*                                    as:                                    */
/*                                    - Dynamic info size                    */
/*                                    - Offset table required for HTON cover-*/
/*                                      sion.                                */
/*                    pBuf - Rm buffer that will be used for framing rm msg. */
/*                                                                           */
/* OUTPUT           : pu4Offset - pointer to offset value.                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
DbUtilFormSyncMsg (tDbTblNode * pDbNode,
                   tDbDataDescInfo * pDataDescInfo,
                   tRmMsg * pBuf, UINT4 *pu4Offset)
{
    UINT4               u4PrevOffSet = 0;
    UINT1              *pNodeBuf = NULL;
    UINT2               u2DataSize = 0;
    UINT1               u1OffsetCount = 0;
    UINT1               u1NextOffset = 0;

    UINT2               u2Info;
    UINT4               u4Info;

    u4PrevOffSet = *pu4Offset;

    /* Copy the data structure identifier. */
    DB_RM_PUT_1_BYTE (pBuf, pu4Offset, (UINT1) pDbNode->u4DataDescId);
    DB_RM_PUT_2_BYTE (pBuf, pu4Offset, ((UINT2) pDataDescInfo->u4DbDataSize +
                                        DB_UTIL_PKT_TYPE_FIELD_SIZE +
                                        DB_UTIL_PKT_LENGTH_FIELD_SIZE));

    /* Get the location of data that has to be copied into RM buffer. */
    pNodeBuf = ((UINT1 *) pDbNode) + sizeof (tDbTblNode);

    if (pDataDescInfo->pDbDataOffsetTbl == NULL)
    {
        /* If no UINT2 or UINT4 data is present, copy complete data into
         * RM buffer.
         */
        DB_RM_PUT_N_BYTE (pBuf, pNodeBuf, pu4Offset,
                          pDataDescInfo->u4DbDataSize);
    }
    else
    {
        if (pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].i2Offset != 0)
        {
            /* If UINT2 or UINT4 data are not the first element after DbNode in 
             * the data structure.              
             */

            u2DataSize =
                (UINT2) pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].i2Offset;

            DB_RM_PUT_N_BYTE (pBuf, pNodeBuf, pu4Offset, u2DataSize);
            pNodeBuf += u2DataSize;
        }

        while (pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].i2Offset != -1)
        {
            if (pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].u2Size == 2)
            {
                MEMCPY (&u2Info, pNodeBuf, sizeof (UINT2));
                DB_RM_PUT_2_BYTE (pBuf, pu4Offset, u2Info);

                /* After copying 2 Byte, 
                 * next offset should be incremented by 2 */
                u1NextOffset = 2;

                /* update pointer for data location that needs to be copied */
                pNodeBuf += u1NextOffset;

            }
            else if (pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].u2Size == 4)
            {
                MEMCPY (&u4Info, pNodeBuf, sizeof (UINT4));
                DB_RM_PUT_4_BYTE (pBuf, pu4Offset, u4Info);

                /* After copying 4 Byte, 
                 * next offset should be incremented by 4 */
                u1NextOffset = 4;

                /* update pointer for data location that needs to be copied */
                pNodeBuf += u1NextOffset;
            }

            u1OffsetCount++;

            /* If UINT2 or UINT4 data are not continous, copy data present 
             * between previous UINT2 or UINT4 values.
             */

            if ((pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].i2Offset != -1)
                && ((pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].i2Offset -
                     pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount -
                                                     1].i2Offset) !=
                    u1NextOffset))
            {
                u2DataSize =
                    (UINT2) (pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount].
                             i2Offset -
                             pDataDescInfo->pDbDataOffsetTbl[u1OffsetCount -
                                                             1].i2Offset);

                DB_RM_PUT_N_BYTE (pBuf, pNodeBuf, pu4Offset, u2DataSize);

                pNodeBuf += u2DataSize;
            }
        }

        if (((*pu4Offset - u4PrevOffSet) - DB_UTIL_PKT_TYPE_FIELD_SIZE -
             DB_UTIL_PKT_LENGTH_FIELD_SIZE) < pDataDescInfo->u4DbDataSize)
        {
            /* copy remaining data */
            u2DataSize = (UINT2) (pDataDescInfo->u4DbDataSize -
                                  (UINT4) (pDataDescInfo->
                                           pDbDataOffsetTbl[u1OffsetCount -
                                                            1].i2Offset +
                                           u1NextOffset));

            DB_RM_PUT_N_BYTE (pBuf, pNodeBuf, pu4Offset, u2DataSize);
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilSendMsgToRm                                      */
/*                                                                           */
/* DESCRIPTION      : This routing is used send messages to RM module.       */
/*                                                                           */
/* INPUT            : pBuf - RM buffer that needs to be send to RM.          */
/*                    u4ModuleId - RM Application Id.                        */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
DbUtilSendMsgToRm (tRmMsg * pBuf, UINT4 u4Offset, UINT4 u4ModuleId)
{
    /* Send all remaining data to RM */
    if (RmEnqMsgToRmFromAppl (pBuf, (UINT2) u4Offset,
                              u4ModuleId, u4ModuleId) != RM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : DbUtilSyncModuleBuffers                                */
/*                                                                           */
/* DESCRIPTION      : This function is used initiate the sync process for    */
/*                    module dynamic info. when the protocol dynamic         */
/*                    information to be synced in not present in a single DS,*/
/*                    then the module dynamic information is written to a    */
/*                    buffer and added to the DBList. This function scans the*/
/*                    DBList and enqueue the buffer to RM.                   */
/*                                                                           */
/* INPUT            : pModuleDesc - pointer to module descriptor table.      */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DbUtilSyncModuleBuffers (tDbTblDescriptor * pModuleDesc, tMemPoolId PoolId)
{
    tDbTblNode         *pDbNode = NULL;
    tDbTblNode         *pDbNextNode = NULL;
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = 0;
    VOID               *pu1Addr = NULL;

    if (pModuleDesc == NULL)
    {
        return OSIX_FAILURE;
    }

    pDbNode = (tDbTblNode *) DbNodeFirst (&pModuleDesc->DbTbl);
    while (pDbNode != NULL)
    {
        pu1Addr = (VOID *) pDbNode + sizeof (tDbTblNode) + sizeof (UINT4);
        pBuf = *(tRmMsg **) pu1Addr;
        MEMCPY (&u4Offset, (((UINT1 *) pDbNode) + sizeof (tDbTblNode)),
                sizeof (UINT4));
        if (DbUtilSendMsgToRm (pBuf, u4Offset,
                               pModuleDesc->DbTblDescrParams.u4ModuleId)
            == OSIX_FAILURE)
        {
            /* When msg cannot be sent to RM. Do not remove the node. 
             * The msg can be sent in the next invocation when RM queue is free */
            return OSIX_FAILURE;
        }
        pDbNextNode = (tDbTblNode *) DbNodeNext (&pModuleDesc->DbTbl,
                                                 (tTMO_DLL_NODE *) pDbNode);
        DbNodeDel (&pModuleDesc->DbTbl, (tTMO_DLL_NODE *) pDbNode);
        if (PoolId != 0)
        {
            MemReleaseMemBlock (PoolId, (UINT1 *) pDbNode);
        }
        pDbNode = pDbNextNode;
    }
    return OSIX_SUCCESS;
}

#endif /*L2RED_WANTED */
