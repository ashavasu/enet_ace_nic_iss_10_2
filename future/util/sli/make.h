
# $Id: make.h,v 1.3 2015/04/28 12:51:05 siva Exp $                     #
# Top level make include files
# ----------------------------

include ../../LR/make.h
include ../../LR/make.rule

SLI_BASE_DIR  = $(BASE_DIR)/util/sli

# Compilation switches
# --------------------
SLI_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES}

# Directories
# -----------
SLI_BASE_DIR  = $(BASE_DIR)/util/sli


# Include directories
# -------------------

SLI_INCLUDE_DIRS = -I${SLI_BASE_DIR}

SLI_FINAL_INCLUDE_DIRS = ${SLI_INCLUDE_DIRS}\
                         ${COMMON_INCLUDE_DIRS}


# Project dependencies
# --------------------

SLI_DEPENDENCIES = $(COMMON_DEPENDENCIES)\
                   $(SLI_FINAL_INCLUDE_FILES) \
                   $(SLI_BASE_DIR)/Makefile \
                   $(SLI_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
