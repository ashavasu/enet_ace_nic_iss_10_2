/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sliwrap.c,v 1.6 2015/04/28 12:51:05 siva Exp $
 *
 * Description: This file contains the Wrapper functions that
 *              routes the socket call to the respective rtos
 *              based on the switch defined.
 *
 *******************************************************************/
#define IS_SLI_WRAPPER_MODULE
#include "lr.h"
#include "fssocket.h"
#if defined (BSDCOMP_SLI_WANTED) || defined (SLI_WANTED)

/**************************************************************************/
/*  Function Name   : SocketWrapper                                       */
/*  Description     : This function creates an endpoint for communication */
/**************************************************************************/
INT4
SocketWrapper (INT4 i4Family, INT4 i4SockType, INT4 i4Protocol)
{
#ifdef SLI_WANTED
    return (SliSocket (i4Family, i4SockType, i4Protocol));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (socket ((int) i4Family, (int) i4SockType, (int) i4Protocol));
#endif
}

/***************************************************************************/
/*  Function Name   : FcntlWrapper                                         */
/*  Description     : Can be used to set a socket to be non-blocking       */
/***************************************************************************/
INT4
FcntlWrapper (INT4 i4Sd, INT4 i4Cmd, INT4 i4Arg)
{
#ifdef SLI_WANTED
    return (SliFcntl (i4Sd, i4Cmd, i4Arg));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (fcntl ((int) i4Sd, (int) i4Cmd, (long) i4Arg));
#endif
}

/***************************************************************************/
/*  Function Name   : BindWrapper                                          */
/*  Description     : This function binds the caller's address specified   */
/*                    to the specified socket.                             */
/***************************************************************************/
INT4
BindWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_myaddr, INT4 i4Addrlen)
{
#ifdef SLI_WANTED
    return (SliBind (i4SockDesc, cpsa_myaddr, i4Addrlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (bind
            ((int) i4SockDesc, (struct sockaddr *) cpsa_myaddr,
             (int) i4Addrlen));
#endif
}

/**************************************************************************/
/*  Function Name   : ConnectWrapper                                      */
/*  Description     : This function connects the caller to the specified  */
/*                    server address.                                     */
/**************************************************************************/
INT4
ConnectWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_serv_addr,
                INT4 i4Addrlen)
{
#ifdef SLI_WANTED
    return (SliConnect (i4SockDesc, cpsa_serv_addr, i4Addrlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (connect
            ((int) i4SockDesc, (struct sockaddr *) cpsa_serv_addr,
             (int) i4Addrlen));
#endif
}

/*************************************************************************/
/*  Function Name   : ListenWrapper                                      */
/*  Description     : This function sets the max number of connection    */
/*                    requests that can be outstanding for a connection  */
/*                    oriented server.                                   */
/*************************************************************************/
#ifdef TCP_WANTED
#ifdef SLI_WANTED
INT4
ListenWrapper (INT4 i4SockDesc, INT4 i4Backlog)
{
    return (SliListen (i4SockDesc, i4Backlog));
}
#endif
#endif

#ifdef BSDCOMP_SLI_WANTED
INT4
ListenWrapper (INT4 i4SockDesc, INT4 i4Backlog)
{

    return (listen ((int) i4SockDesc, (int) i4Backlog));
}
#endif
/*************************************************************************/
/*  Function Name   : AcceptWrapper                                      */
/*  Description     : This function waits for a connection request to    */
/*                    arrive and, after the request arrives, fills up    */
/*                    the return addrs info of the peer.                 */
/**************************************************************************/
#ifdef SLI_WANTED
#ifdef TCP_WANTED
INT4
AcceptWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_peer_addr,
               INT4 *pi4Addrlen)
{
    return (SliAccept (i4SockDesc, cpsa_peer_addr, pi4Addrlen));
}
#endif
#endif

#ifdef BSDCOMP_SLI_WANTED
INT4
AcceptWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_peer_addr,
               INT4 *pi4Addrlen)
{

    return (accept
            ((int) i4SockDesc, (struct sockaddr *) cpsa_peer_addr,
             (socklen_t *) pi4Addrlen));
}
#endif
/************************************************************************/
/*  Function Name   : ReadWrapper                                       */
/*  Description     : This function reads the requested number of bytes */
/*                    from the socket specified.                        */
/************************************************************************/
INT4
ReadWrapper (INT4 i4SockDesc, VOID *cpi1ReadBuf, INT4 i4NumToRead)
{
#ifdef SLI_WANTED
    return (SliRead (i4SockDesc, cpi1ReadBuf, i4NumToRead));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (read ((int) i4SockDesc, (void *) cpi1ReadBuf, (int) i4NumToRead));
#endif
}

/*************************************************************************/
/*  Function Name   : WriteWrapper                                       */
/*  Description     : This function writes the requested number of bytes */
/*                    to the socket specified.                           */
/*************************************************************************/
INT4
WriteWrapper (INT4 i4SockDesc, CONST VOID *cpi1WriteBuf, INT4 i4NumToWrite)
{
#ifdef SLI_WANTED
    return (SliWrite (i4SockDesc, cpi1WriteBuf, i4NumToWrite));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (write ((int) i4SockDesc, (const void *) cpi1WriteBuf,
                   (int) i4NumToWrite));
#endif
}

/************************************************************************/
/*  Function Name   : RecvWrapper                                       */
/*  Description     : This function is used to read as well as          */
/*                    control the reception of data.                    */
/************************************************************************/
INT4
RecvWrapper (INT4 i4SockDesc, VOID *cpi1ReadBuf, INT4 i4NumToRead,
             UINT4 u4Flags)
{
#ifdef SLI_WANTED
    return (SliRecv (i4SockDesc, cpi1ReadBuf, i4NumToRead, u4Flags));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (recv
            ((int) i4SockDesc, (void *) cpi1ReadBuf, (int) i4NumToRead,
             (unsigned int) u4Flags));
#endif
}

/*************************************************************************/
/*  Function Name   : SendWrapper                                        */
/*  Description     : This function can be used by the caller to send    */
/*                    data as well as to have control over the           */
/*                    transmission.                                      */
/*************************************************************************/
INT4
SendWrapper (INT4 i4SockDesc, CONST VOID *cpi1WriteBuf, INT4 i4NumToWrite,
             UINT4 u4Flags)
{
#ifdef SLI_WANTED
    return (SliSend (i4SockDesc, cpi1WriteBuf, i4NumToWrite, u4Flags));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (send
            ((int) i4SockDesc, (const void *) cpi1WriteBuf, (int) i4NumToWrite,
             (unsigned int) u4Flags));
#endif
}

/**************************************************************************/
/*  Function Name   : SelectWrapper                                       */
/*  Description     : This function can be used to give the SLI a set of  */
/*                    descriptors for read, write and exception pending   */
/*                    events with the option of a timeout.If time = 0,    */
/*                    the status of all the descriptors is returned.      */
/**************************************************************************/
#ifndef SLI_WANTED
INT4
SelectWrapper (INT4 i4Maxfdpl, fd_set * readfds, fd_set * writefds,
               fd_set * exceptfds, struct timeval *timeout)
#else
INT4
SelectWrapper (INT4 i4Maxfdpl, SliFdSet * readfds, SliFdSet * writefds,
               SliFdSet * exceptfds, struct timeval *timeout)
#endif
{
#ifdef SLI_WANTED
    return (SliSelect (i4Maxfdpl, readfds, writefds, exceptfds, timeout));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (select
            ((int) i4Maxfdpl, (fd_set *) readfds, (fd_set *) writefds,
             (fd_set *) exceptfds, (struct timeval *) timeout));
#endif
}

/**************************************************************************/
/*   Function Name   : RecvfromWrapper                                    */
/*   Description     : This function receives a dataram and fills up      */
/*                     source address in pPeerAddress and data in pi1Buf. */
/**************************************************************************/
INT4
RecvfromWrapper (INT4 i4SockDesc, VOID *pi1Buf, INT4 i4BufLen, UINT4 u4Flags,
                 struct sockaddr *pPeerAddr, INT4 *pi4AddrLen)
{
#ifdef SLI_WANTED
    return (SliRecvfrom
            (i4SockDesc, pi1Buf, i4BufLen, u4Flags, pPeerAddr, pi4AddrLen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (recvfrom
            ((int) i4SockDesc, (void *) pi1Buf, (int) i4BufLen,
             (unsigned int) u4Flags, (struct sockaddr *) pPeerAddr,
             (socklen_t *) pi4AddrLen));
#endif
}

/*************************************************************************/
/*  Function Name   : SendtoWrapper                                      */
/*  Description     : This function sends a datagram to the destination  */
/*                    specified by pPeerAddr.                            */
/*************************************************************************/
INT4
SendtoWrapper (INT4 i4SockDesc, CONST VOID *pi1Buf, INT4 i4BufLen,
               UINT4 u4Flags, struct sockaddr *pPeerAddr, INT4 i4AddrLen)
{
#ifdef SLI_WANTED
    return (SliSendto
            (i4SockDesc, pi1Buf, i4BufLen, u4Flags, pPeerAddr, i4AddrLen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (sendto
            ((int) i4SockDesc, (const void *) pi1Buf, (int) i4BufLen,
             (unsigned int) u4Flags, (struct sockaddr *) pPeerAddr,
             (socklen_t) i4AddrLen));
#endif
}

/**************************************************************************/
/*  Function Name   : CloseWrapper                                        */
/*  Description     : This function disconnects the caller from           */
/*                    the connection established before and destroys the  */
/*                                  corresponding socket.                 */
/**************************************************************************/
INT4
CloseWrapper (INT4 i4SockDesc)
{
#ifdef SLI_WANTED
    return (SliClose (i4SockDesc));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (close ((int) i4SockDesc));
#endif
}

/**************************************************************************/
/*  Function Name   : GetsocknameWrapper                                  */
/*  Description     : This function gets the local address of the         */
/*                    given socket.                                       */
/**************************************************************************/
INT4
GetsocknameWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_myaddr,
                    INT4 *pi4Addrlen)
{
#ifdef SLI_WANTED
    return (SliGetsockname (i4SockDesc, cpsa_myaddr, pi4Addrlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (getsockname
            ((int) i4SockDesc, (struct sockaddr *) cpsa_myaddr,
             (unsigned int *) pi4Addrlen));
#endif
}

/**************************************************************************/
/*  Function Name   : GetpeernameWrapper                                  */
/*  Description     : This function gets the peer address of a given      */
/*                    socket.                                             */
/**************************************************************************/
INT4
GetpeernameWrapper (INT4 i4SockDesc, struct sockaddr *cpsa_peeraddr,
                    INT4 *pi4Addrlen)
{
#ifdef SLI_WANTED
    return (SliGetpeername (i4SockDesc, cpsa_peeraddr, pi4Addrlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (getpeername
            ((int) i4SockDesc, (struct sockaddr *) cpsa_peeraddr,
             (socklen_t *) pi4Addrlen));
#endif
}

/**************************************************************************/
/*  Function Name  : GetsockoptWrapper                                    */
/*  Description    : This function gets the value of the specified option */
/**************************************************************************/
INT4
GetsockoptWrapper (INT4 i4SockDesc, INT4 i4Level, INT4 i4Optname,
                   VOID *Cpi1Optval, INT4 *pi4Optlen)
{
#ifdef SLI_WANTED
    return (SliGetsockopt
            (i4SockDesc, i4Level, i4Optname, Cpi1Optval, pi4Optlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (getsockopt
            ((int) i4SockDesc, (int) i4Level, (int) i4Optname,
             (void *) Cpi1Optval, (socklen_t *) pi4Optlen));
#endif
}

/**************************************************************************/
/*  Function Name   : SetsockoptWrapper                                   */
/*  Description     : This function sets the socket options specified     */
/**************************************************************************/
INT4
SetsockoptWrapper (INT4 i4SockDesc, INT4 i4Level, INT4 i4Optname,
                   VOID *Cpi1Optval, INT4 i4Optlen)
{
#ifdef SLI_WANTED
    return (SliSetsockopt
            (i4SockDesc, i4Level, i4Optname, Cpi1Optval, i4Optlen));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (setsockopt
            ((int) i4SockDesc, (int) i4Level, (int) i4Optname,
             (void *) Cpi1Optval, (socklen_t) i4Optlen));
#endif
}

/***************************************************************************/
/*  Function Name   : SendmsgWrapper                                       */
/*  Description     : Used to send Ancillary data about socket.            */
/***************************************************************************/
INT4
SendmsgWrapper (INT4 i4Sd, const struct msghdr *pMsg, UINT4 u4Flags)
{
#ifdef SLI_WANTED
    return (SliSendmsg (i4Sd, pMsg, u4Flags));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (sendmsg
            ((int) i4Sd, (const struct msghdr *) pMsg, (unsigned int) u4Flags));
#endif
}

/**************************************************************************/
/*  Function Name   : RecvmsgWrapper                                      */
/*  Description     : Used to receive Ancillary data about socket.        */
/**************************************************************************/
INT4
RecvmsgWrapper (INT4 i4Sd, struct msghdr *pMsg, UINT4 u4Flags)
{
#ifdef SLI_WANTED
    return (SliRecvmsg (i4Sd, pMsg, u4Flags));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (recvmsg
            ((int) i4Sd, (struct msghdr *) pMsg, (unsigned int) u4Flags));
#endif
}

/**************************************************************************/
/*  Function Name   : InetPtonWrapper                                     */
/*  Description     : Create a network address structure                  */
/**************************************************************************/
INT4
InetPtonWrapper (INT4 i4Family, const char *pu1Src, VOID *pDst)
{
#ifdef SLI_WANTED
    return (SliInetPton (i4Family, (const UINT1 *) pu1Src, pDst));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (inet_pton ((int) i4Family, pu1Src, pDst));
#endif
}

/**************************************************************************/
/*  Function Name   : InetNtopWrapper                                     */
/*  Description     : convert a network format address to presentation    */
/*                    format                                              */
/**************************************************************************/
const UINT1        *
InetNtopWrapper (INT4 i4Family, const void *pu1Src, char *pu1Dst, INT4 i4Size)
{
#ifdef SLI_WANTED
    return (SliInetNtop (i4Family, pu1Src, (UINT1 *) pu1Dst, i4Size));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return ((const UINT1 *)
            inet_ntop ((int) i4Family, pu1Src, pu1Dst, (int) i4Size));
#endif
}

/**************************************************************************/
/*  Function Name   : ShutdownWrapper                                     */
/*  Description     : This function disconnects a part of a full-duplex   */
/*                     connection established                             */
/**************************************************************************/
INT4
ShutdownWrapper (INT4 i4SockDesc, INT4 i4How)
{
#ifdef SLI_WANTED
    return (SliShutdown (i4SockDesc, i4How));
#endif
#ifdef BSDCOMP_SLI_WANTED
    return (shutdown ((int) i4SockDesc, (int) i4How));
#endif
}
#endif /* defined (BSDCOMP_SLI_WANTED) || defined (SLI_WANTED) */
