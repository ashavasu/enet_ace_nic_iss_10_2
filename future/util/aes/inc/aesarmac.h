/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesarmac.h,v 1.1 2015/04/28 12:51:01 siva Exp $
 *
 * Description: This file contains the macros required for AES 
 * module
 *********************************************************************/
#ifndef _AES_MACS_H
#define _AES_MACS_H
/********************************************************************/
#define AES_INC_VAL(val) val++

#endif
