/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesarprt.h,v 1.1 2015/04/28 12:51:01 siva Exp $
 *
 * Description: This file contains the function declarations for AES 
 * module
 *********************************************************************/
#ifndef _AES_PROT_H
#define _AES_PROT_H
/********************************************************************/

PUBLIC VOID AesArScheduleCore PROTO((const UINT1 *, UINT1 *, UINT4));
PUBLIC VOID AesArMixColumns PROTO((const UINT1 *, UINT1 *));
PUBLIC VOID AesArInvMixColumns PROTO((const UINT1 *, UINT1 *));
/*PUBLIC VOID AesAr128Setup PROTO((unArCryptoKey *,const UINT1 *));*/
void AesAr128Setup PROTO((unArCryptoKey *,const UINT1 *));
PUBLIC VOID AesAr192Setup PROTO((unArCryptoKey *,const UINT1 *));
PUBLIC VOID AesAr256Setup PROTO((unArCryptoKey *,const UINT1 *));

/* API */                                    
PUBLIC UINT1 AesArSetEncryptKey PROTO((UINT1 *, UINT2 ,unArCryptoKey *));
PUBLIC UINT1 AesArSetDecryptKey PROTO((UINT1 *, UINT2 ,unArCryptoKey *));
PUBLIC UINT1 AesArCbcEncrypt PROTO((UINT1 *, UINT4 , UINT2 ,
                                    UINT1 *, UINT1 *));
PUBLIC UINT1 AesArCbcDecrypt PROTO((UINT1 *, UINT4 , UINT2 ,
                                    UINT1 *, UINT1 *));
PUBLIC VOID AesArEncrypt PROTO((const unArCryptoKey*,
                                const UINT1*,UINT1*));
PUBLIC VOID AesArDecrypt PROTO((const unArCryptoKey*,
                                const UINT1*,UINT1*));

/* This API is invoked by the Modules which Requires a digest 
   to be generated for a given Data through AES-XCBC-MAC-96 mode (RFC 3566) */
PUBLIC UINT1 AesArXcbcMac96 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength, 
		UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac);

/* This API is invoked by the Modules which Requires a digest 
 * to be generated for a given Data through AES-XCBC-PRF-128 mode (RFC 4344) */
PUBLIC UINT1 AesArXcbcPrf128 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength, 
		UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac);

/* This API is invoked by the Modules which Requires a digest 
   to be generated for a given Data through AES-XCBC-MAC-128 mode  */
PUBLIC UINT1 AesArXcbcMac128 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength, 
		UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac);
/*API to encrypt using the AES counter mode*/
PUBLIC INT1 AesArCtrMode(CONST UINT1 *, UINT1 *,FS_ULONG , 
                            CONST unArCryptoKey *,UINT1 *,UINT1 *,UINT4 *);
/*Function to increment the counter and handle overflow if occured*/
PUBLIC INT1 AesArIncrCounter(UINT1 *);
/* This API is invoked by the Modules which Requires a encrypted/decrypted data 
 *    to be generated for a given Data through AES-CFB-128 mode  */
PUBLIC INT4 AesArCfb128(CONST UINT1 *pu1InText, UINT1 *pu1OutText,
        CONST FS_ULONG u4InLen, CONST unArCryptoKey *pKey,
         UINT1 *pu1Ivec, INT4 *pi4Num, CONST INT4 i4Enc);
#endif
