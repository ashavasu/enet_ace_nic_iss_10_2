/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesardef.h,v 1.1 2015/04/28 12:51:01 siva Exp $
 *
 * Description: This file contains the defines required for AES 
 * module
 *********************************************************************/
#ifndef _AES_DEFS_H
#define _AES_DEFS_H
/********************************************************************/

#define AesAr128Encrypt                 AesArEncrypt
#define AesAr128Decrypt                 AesArDecrypt
#define AesAr192Encrypt                 AesArEncrypt
#define AesAr192Decrypt                 AesArDecrypt
#define AesAr256Encrypt                 AesArEncrypt
#define AesAr256Decrypt                 AesArDecrypt
#define AES_COLUMNS                     4
#define AES_128_ENCR_ROUNDS             10
#define AES_192_ENCR_ROUNDS             12
#define AES_256_ENCR_ROUNDS             14
#define AES_128_KEY_BYTES               16
#define AES_192_KEY_BYTES               24
#define AES_256_KEY_BYTES               32
#define AES_KEY_OFFSET_12               12
#define AES_KEY_OFFSET_16               16
#define AES_KEY_OFFSET_4                4
#define AES_KEY_OFFSET_20               20
#define AES_KEY_OFFSET_24               24
#define AES_KEY_OFFSET_28               28
#define AES_KEY_OFFSET_32               32
#define AES_128_KEY_EXPANSION_SIZE      176
#define AES_192_KEY_EXPANSION_SIZE      208
#define AES_256_KEY_EXPANSION_SIZE      240
#define AES_STATE_SECOND_COL_CONS       4
#define AES_STATE_THIRD_COL_CONS        8
#define AES_STATE_FOURTH_COL_CONS       12
#define AES_128_KEY_LENGTH              128
#define AES_192_KEY_LENGTH              192
#define AES_256_KEY_LENGTH              256
#define AES_BLOCK_SIZE                  16
#define AES_SUCCESS                     1
#define AES_FAILURE                     0
#define AES_MAX_MATRIX_SIZE             256
#define AES_STATE_SIZE                  16
#define AES_INIT_VECT_SIZE              16
#define AES_MAX_UINT4                   0xFFFFFFFF
/*Contains the number of blocks in 128bit key*/
#define AES_BLOCKS_IN_128_KEY		8
#define AES_XCBC_12_KEY_BYTES		12
#define AES_128_IN_HEX			0x80
#endif
