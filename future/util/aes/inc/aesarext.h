/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesarext.h,v 1.1 2015/04/28 12:51:01 siva Exp $
 *
 * Description: This file contains the externs for AES 
 * module
 *********************************************************************/
#ifndef _AES_EXT_H
#define _AES_EXT_H
/********************************************************************/
extern const UINT1 gaArSf[AES_MAX_MATRIX_SIZE]; 
extern const UINT1 gaArSb[AES_MAX_MATRIX_SIZE]; 
extern const UINT1 gaArRc[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArHf[AES_STATE_SIZE];
extern const UINT1 gaArHb[AES_STATE_SIZE]; 
extern const UINT1 gaArM_2[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArM_3[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArM_9[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArM_11[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArM_13[AES_MAX_MATRIX_SIZE];
extern const UINT1 gaArM_14[AES_MAX_MATRIX_SIZE];
extern const tArBlockcipher gArBlockcipherAes128; 
extern const tArBlockcipher gArBlockcipherAes192; 
extern const tArBlockcipher gArBlockcipherAes256; 
#endif
