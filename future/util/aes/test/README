Copyright (C) 2011 Aricent Inc . All Rights Reserved
====================================================

Introduction
============
	This directory contains UT cases for verifying 
	- AES-XCBC-MAC and AES-XCBC-PRF algorithms (AesArXcbcMac96,AesArXcbcMac128 
  	  and AesArXcbcPrf128 modules in aes/src/aesarapi.c) 
    - AES-CTR algorithm 
	  (AesArCtrMode,AesArCtrInc,AesArFormCtrBlock modules in 
	  aes/src/aesarapi.c,aes/src/aesarfn.c).

Contents
========

    aesxcbc.c -	file which contains the test vectors for  
			    verifying AES-XCBC-MAC and AES-XCBC-PRF algorithm.

    aesctr.c - file which contains the test vectors for verifying 
	           AES-CTR algorithm.
    aescfb.c - file which contains the test vectors for verifying AES-CFB (128bit) 
               algorithm.


How to use this?
================
1. To test AES-XCBC-MAC and AES-XCBC-PRF algorithms:

a) 	Do the following changes in util/aes/Makefile as specified in the diff given below 

RCS file: /home/projects/isbase/code/future/util/aes/Makefile,v
retrieving revision 1.4
diff -u -p -b -B -r1.4 Makefile
--- Makefile    6 Mar 2009 13:29:34 -0000       1.4
+++ Makefile    13 Jan 2011 13:00:07 -0000
@@ -10,7 +10,8 @@ include make.h

 OBJ =  \
        $(CRYPTO_AES_OBJ_DIR)/aesarfn.o\
-    $(CRYPTO_AES_OBJ_DIR)/aesarapi.o
+    $(CRYPTO_AES_OBJ_DIR)/aesarapi.o \
+    $(CRYPTO_AES_OBJ_DIR)/aesxcbc.o

 AES_FINAL_OBJ = $(CRYPTO_AES_OBJ_DIR)/aes.o

@@ -56,6 +57,10 @@ $(CRYPTO_AES_OBJ_DIR)/aesarapi.o : \
        $(DEPENDENCIES)
        $(CC) $(CC_FLAGS) ${AES_FINAL_COMPILATION_SWITCHES} $(CRYPTO_INCLUDES) -c -o $@ $(CRYPTO_AES_SRC_DIR)/aesarapi.c

+$(CRYPTO_AES_OBJ_DIR)/aesxcbc.o : \
+       $(CRYPTO_AES_BASE_DIR)/test/aesxcbc.c \
+       $(DEPENDENCIES)
+       $(CC) $(CC_FLAGS) ${AES_FINAL_COMPILATION_SWITCHES} $(CRYPTO_INCLUDES) -c -o $@ $(CRYPTO_AES_BASE_DIR)/test/aesxcbc.c
 ## Cleanup ###################################################################
 clean:
        rm -f $(AES_FINAL_OBJ)

b) Compile and build the exe

c) Invoke the function testvectors() defined in aesxcbc.c for testing AES-XCBC-MAC and AES-XCBC-PRF algorithms.


2. To test AES-CTR algorithm:

a)	Do the following changes in util/aes/Makefile as specified in the diff given below 

RCS file: /home/projects/isbase/code/future/util/aes/Makefile,v
retrieving revision 1.4
diff -r1.4 Makefile
13c13,14
<     $(CRYPTO_AES_OBJ_DIR)/aesarapi.o
---
>     $(CRYPTO_AES_OBJ_DIR)/aesarapi.o\
>     $(CRYPTO_AES_OBJ_DIR)/aesctr.o
58a60,63
> $(CRYPTO_AES_OBJ_DIR)/aesctr.o : \
>   $(CRYPTO_AES_TEST_DIR)/aesctr.c \
>   $(DEPENDENCIES)
>   $(CC) $(CC_FLAGS) ${AES_FINAL_COMPILATION_SWITCHES} $(CRYPTO_INCLUDES) -c -o $@ $(CRYPTO_AES_BASE_DIR)/test/aesctr.c
~

b) Compile and build the exe

c) Invoke the functions aes_ctr_testcases() and aes_ctr_inc_testcases() defined in aesctr.c for testing AES-CTR mode algorithm.

3. To test AES-CFB algorithm:

a)      Do the following changes in util/aes/Makefile as specified in the diff given below

===================================================================
RCS file: /home/projects/isbase/code/future/util/aes/Makefile,v
retrieving revision 1.4
diff -u -p -r1.4 Makefile
--- Makefile    6 Mar 2009 13:29:34 -0000       1.4
+++ Makefile    18 Feb 2011 08:57:54 -0000
@@ -10,7 +10,8 @@ include make.h

 OBJ =  \
        $(CRYPTO_AES_OBJ_DIR)/aesarfn.o\
-    $(CRYPTO_AES_OBJ_DIR)/aesarapi.o
+    $(CRYPTO_AES_OBJ_DIR)/aesarapi.o\
+$(CRYPTO_AES_OBJ_DIR)/aescfb.o

 AES_FINAL_OBJ = $(CRYPTO_AES_OBJ_DIR)/aes.o

@@ -56,6 +57,11 @@ $(CRYPTO_AES_OBJ_DIR)/aesarapi.o : \
        $(DEPENDENCIES)
        $(CC) $(CC_FLAGS) ${AES_FINAL_COMPILATION_SWITCHES} $(CRYPTO_INCLUDES) -c -o $@ $(CRYPTO_AES_SRC_DIR)/aesarapi.c

+$(CRYPTO_AES_OBJ_DIR)/aescfb.o : \
+       $(CRYPTO_AES_BASE_DIR)/test/aescfb.c \
+       $(DEPENDENCIES)
+       $(CC) $(CC_CFLAGS) ${AES_FINAL_COMPILATION_SWITCHES} $(CRYPTO_INCLUDES) -c -o $@ $(CRYPTO_AES_BASE_DIR)/test/aescfb.c
+
 ## Cleanup ###################################################################
 clean:
        rm -f $(AES_FINAL_OBJ)


b) Compile and build the exe

c) Invoke the functions aes_cfb_testcases() defined in aescfb.c for testing AES-CFB (128 bit) mode algorithm.

