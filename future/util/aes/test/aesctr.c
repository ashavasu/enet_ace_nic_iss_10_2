/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesctr.c,v 1.1 2015/04/28 12:53:02 siva Exp $
 *
 * Description: This file contains the code for testcases for AES-CTR 
 *              module
 *********************************************************************/
#include "aesarinc.h"

/*Function that contains testcases for AES-CTR mode of various AES key lengths and block size*/
void aes_ctr_testcases(void);
/*Function tha contains testcases for counter increment and overflow in all dwords*/
void aes_ctr_inc_testcases(void);

/*******************************************************************************
 * Function           : aes_ctr_testcases
 *
 * Description        : Function that contains testcases for AES-CTR mode of 
 *                      various AES key lengths and block size
 *
 * Input(s)           : NONE
 *                     
 * Output(s)          : Results of test vectors in RFC 3686
 *
 * Returns            : NONE
 ******************************************************************************/

void aes_ctr_testcases(void)
{

/*test case 1 : Encrypting 16 octets using AES-CTR with 128-bit key*/
   unArCryptoKey ArKeySchedule1;
   unsigned char au1ArTemp1Subkey[16] = {0xAE, 0x68, 0x52, 0xF8, 0x12, 0x10, 0x67, 0xCC, 0x4B, 0xF7, 0xA5, 0x76, 0x55, 0x77,0xF3, 0x9E };
   const unsigned char in1[16]= { 0x53,0x69, 0x6E, 0x67, 0x6C,0x65, 0x20, 0x62, 0x6C, 0x6F, 0x63, 0x6B, 0x20, 0x6D, 0x73, 0x67};
   const unsigned char nonce1[4] = {0x00, 0x00,0x00, 0x30};
   const unsigned char iv1[8] = {0x00, 0x00 ,0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
   const unsigned char counter1[4]= { 0x00, 0x00,0x00, 0x01};
   unsigned char ivec1[AES_BLOCK_SIZE];
   memset(ivec1,'\0',16);
   memcpy(ivec1,nonce1,4);
   memcpy(ivec1+4,iv1,8);
   memcpy(ivec1+12,counter1,4);
   unsigned char out1[16];
   memset(out1,'\0',16);
   unsigned int num1 = 0;
   unsigned char result1[16] = {0xE4,0x09 ,0x5D, 0x4F, 0xB7, 0xA7, 0xB3, 0x79, 0x2D, 0x61, 0x75, 0xA3, 0x26, 0x13, 0x11, 0xB8 };
   unsigned char ecount_buf1[AES_BLOCK_SIZE];
   memset(ecount_buf1,'\0',16);
   AesArSetEncryptKey(au1ArTemp1Subkey,128,&ArKeySchedule1);
   AesArCtrMode(in1,out1,16,&ArKeySchedule1,ivec1,ecount_buf1,&num1);
   if (memcmp(result1, out1, 16)!=0)
        {
                printf("\n testcase:1 failure\n");
        }
        else
        {
                printf("\n testcase:1 success\n");
        }

/*test case 2 : Encrypting 32 octets using AES-CTR with 128-bit key*/
     unArCryptoKey ArKeySchedule2;
     unsigned char au1ArTemp2Subkey[] = { 0x7E, 0x24, 0x06, 0x78, 0x17, 0xFA, 0xE0, 0xD7, 0x43, 0xD6, 0xCE, 0x1F, 0x32, 0x53, 0x91, 0x63};
     const unsigned char in2[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
     const unsigned char nonce2[4]= { 0x00, 0x6C, 0xB6, 0xDB};
     const unsigned char iv2[8] ={0xC0, 0x54, 0x3B, 0x59, 0xDA, 0x48, 0xD9, 0x0B};
     const unsigned char counter2[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec2[AES_BLOCK_SIZE];
     memset(ivec2,'\0',16);
     memcpy(ivec2,nonce2,4);
     memcpy(ivec2+4,iv2,8);
     memcpy(ivec2+12,counter2,4);
     unsigned char out2[32];
     unsigned int num2 = 0;
     unsigned char result2[] ={0x51, 0x04, 0xA1, 0x06, 0x16, 0x8A, 0x72, 0xD9, 0x79, 0x0D, 0x41, 0xEE, 0x8E, 0xDA, 0xD3, 0x88,0xEB, 0x2E, 0x1E, 0xFC, 0x46, 0xDA ,0x57, 0xC8, 0xFC, 0xE6, 0x30 ,0xDF, 0x91, 0x41, 0xBE, 0x28}; 
     unsigned char ecount_buf2[AES_BLOCK_SIZE];
     memset(ecount_buf2,'\0',16);
     memset(out2,'\0',32);
     AesArSetEncryptKey(au1ArTemp2Subkey,128,&ArKeySchedule2);
     AesArCtrMode(in2,out2,32,&ArKeySchedule2,ivec2,ecount_buf2,&num2);
     if (memcmp(result2, out2, 32)!=0)
        {
                printf("\n testcase:2 failure\n");
        }
        else
        {
                printf("\n testcase:2 success\n");
        }
/*test case 3 : Encrypting 36 octets using AES-CTR with 128-bit key*/
     unArCryptoKey ArKeySchedule3;
     unsigned char au1ArTemp3Subkey[] ={ 0x76, 0x91, 0xBE, 0x03, 0x5E, 0x50, 0x20, 0xA8, 0xAC, 0x6E, 0x61, 0x85, 0x29, 0xF9, 0xA0, 0xDC};
     const unsigned char in3[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22 ,0x23};
     const unsigned char nonce3[4]= {0x00, 0xE0, 0x01 ,0x7B};
     const unsigned char iv3[8] ={0x27, 0x77 ,0x7F, 0x3F, 0x4A, 0x17 ,0x86, 0xF0};
     const unsigned char counter3[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec3[AES_BLOCK_SIZE];
     memset(ivec3,'\0',16);
     memcpy(ivec3,nonce3,4);
     memcpy(ivec3+4,iv3,8);
     memcpy(ivec3+12,counter3,4);
     unsigned char out3[48];
     unsigned int num3 = 0;
     unsigned char result3[] ={0xC1, 0xCF, 0x48, 0xA8, 0x9F, 0x2F, 0xFD, 0xD9, 0xCF, 0x46, 0x52, 0xE9, 0xEF, 0xDB, 0x72, 0xD7,0x45, 0x40, 0xA4, 0x2B, 0xDE, 0x6D, 0x78, 0x36, 0xD5, 0x9A, 0x5C, 0xEA, 0xAE, 0xF3, 0x10, 0x53,0x25, 0xB2, 0x07, 0x2F};
     unsigned char ecount_buf3[AES_BLOCK_SIZE];
     memset(ecount_buf3,'\0',16);
     memset(out3,'\0',48);
     AesArSetEncryptKey(au1ArTemp3Subkey,128,&ArKeySchedule3);
     AesArCtrMode(in3,out3,48,&ArKeySchedule3,ivec3,ecount_buf3,&num3);
     if (memcmp(result3, out3, 36)!=0)
        {
                printf("\n testcase:3 failure\n");
        }
        else
        {
                printf("\n testcase:3 success\n");
        }


/*test case 4 : Encrypting 16 octets using AES-CTR with 192-bit key*/
     unArCryptoKey ArKeySchedule4;
     unsigned char au1ArTemp4Subkey[] ={0x16, 0xAF, 0x5B, 0x14, 0x5F, 0xC9, 0xF5, 0x79, 0xC1, 0x75, 0xF9, 0x3E, 0x3B, 0xFB, 0x0E, 0xED,0x86, 0x3D, 0x06, 0xCC, 0xFD, 0xB7, 0x85, 0x15};
     const unsigned char in4[]={0x53, 0x69, 0x6E, 0x67, 0x6C, 0x65, 0x20, 0x62, 0x6C, 0x6F, 0x63, 0x6B, 0x20, 0x6D, 0x73, 0x67};
     const unsigned char nonce4[4]= {0x00, 0x00, 0x00, 0x48};
     const unsigned char iv4[8] ={0x36, 0x73, 0x3C, 0x14, 0x7D, 0x6D, 0x93, 0xCB};
     const unsigned char counter4[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec4[AES_BLOCK_SIZE];
     memset(ivec4,'\0',16);
     memcpy(ivec4,nonce4,4);
     memcpy(ivec4+4,iv4,8);
     memcpy(ivec4+12,counter4,4);
     unsigned char out4[16];
     unsigned int num4 = 0;
     unsigned char result4[] ={0x4B, 0x55, 0x38, 0x4F, 0xE2, 0x59, 0xC9, 0xC8, 0x4E, 0x79, 0x35, 0xA0, 0x03, 0xCB,0xE9, 0x28};
     unsigned char ecount_buf4[AES_BLOCK_SIZE];
     memset(ecount_buf4,'\0',16);
     memset(out4,'\0',16);
     AesArSetEncryptKey(au1ArTemp4Subkey,192,&ArKeySchedule4);
     AesArCtrMode(in4,out4,16,&ArKeySchedule4,ivec4,ecount_buf4,&num4);
     if (memcmp(result4, out4, 16)!=0)
        {
                printf("\n testcase:4 failure\n");
        }
        else
        {
                printf("\n testcase:4 success\n");
        }

/*test case 5 : Encrypting 32 octets using AES-CTR with 192-bit key*/
     unArCryptoKey ArKeySchedule5;
     unsigned char au1ArTemp5Subkey[] ={0x7C, 0x5C, 0xB2, 0x40, 0x1B, 0x3D, 0xC3, 0x3C, 0x19, 0xE7, 0x34, 0x08, 0x19, 0xE0, 0xF6, 0x9C, 0x67, 0x8C, 0x3D, 0xB8, 0xE6, 0xF6, 0xA9, 0x1A};
     const unsigned char in5[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
     const unsigned char nonce5[4]= {0x00, 0x96, 0xB0, 0x3B};
     const unsigned char iv5[8] ={0x02, 0x0C, 0x6E, 0xAD, 0xC2, 0xCB, 0x50, 0x0D};
     const unsigned char counter5[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec5[AES_BLOCK_SIZE];
     memset(ivec5,'\0',16);
     memcpy(ivec5,nonce5,4);
     memcpy(ivec5+4,iv5,8);
     memcpy(ivec5+12,counter5,4);
     unsigned char out5[32];
     unsigned int num5 = 0;
     unsigned char result5[] ={0x45, 0x32, 0x43, 0xFC, 0x60, 0x9B, 0x23, 0x32, 0x7E, 0xDF, 0xAA, 0xFA, 0x71, 0x31, 0xCD, 0x9F, 0x84, 0x90, 0x70, 0x1C, 0x5A, 0xD4, 0xA7, 0x9C, 0xFC, 0x1F, 0xE0, 0xFF, 0x42, 0xF4, 0xFB, 0x00};
     unsigned char ecount_buf5[AES_BLOCK_SIZE];
     memset(ecount_buf5,'\0',16);
     memset(out5,'\0',32);
     AesArSetEncryptKey(au1ArTemp5Subkey,192,&ArKeySchedule5);
     AesArCtrMode(in5,out5,32,&ArKeySchedule5,ivec5,ecount_buf5,&num5);
     if (memcmp(result5, out5, 32)!=0)
        {
                printf("\n testcase:5 failure\n");
        }
        else
        {
                printf("\n testcase:5 success\n");
        }


/*test case 6 : Encrypting 36 octets using AES-CTR with 192-bit key*/
     unArCryptoKey ArKeySchedule6;
     unsigned char au1ArTemp6Subkey[] ={0x02, 0xBF, 0x39, 0x1E, 0xE8, 0xEC, 0xB1, 0x59, 0xB9, 0x59, 0x61, 0x7B, 0x09, 0x65, 0x27, 0x9B, 0xF5, 0x9B, 0x60, 0xA7, 0x86, 0xD3, 0xE0, 0xFE};
     const unsigned char in6[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22 ,0x23}; 
     const unsigned char nonce6[4]= {0x00, 0x07,0xBD, 0xFD};
     const unsigned char iv6[8] ={0x5C, 0xBD, 0x60, 0x27, 0x8D, 0xCC, 0x09, 0x12};
     const unsigned char counter6[4]= {0x00, 0x00, 0x00, 0x01}; 
     unsigned char ivec6[AES_BLOCK_SIZE];
     memset(ivec6,'\0',16);
     memcpy(ivec6,nonce6,4);
     memcpy(ivec6+4,iv6,8);
     memcpy(ivec6+12,counter6,4);
     unsigned char out6[48];
     unsigned int num6 = 0;
     unsigned char result6[] ={0x96, 0x89, 0x3F, 0xC5, 0x5E, 0x5C, 0x72, 0x2F, 0x54, 0x0B, 0x7D, 0xD1, 0xDD, 0xF7, 0xE7, 0x58, 0xD2, 0x88, 0xBC, 0x95, 0xC6, 0x91, 0x65, 0x88, 0x45, 0x36, 0xC8, 0x11, 0x66, 0x2F, 0x21, 0x88, 0xAB, 0xEE, 0x09, 0x35};
     unsigned char ecount_buf6[AES_BLOCK_SIZE];
     memset(ecount_buf6,'\0',16);
     memset(out6,'\0',48);
     AesArSetEncryptKey(au1ArTemp6Subkey,192,&ArKeySchedule6);
     AesArCtrMode(in6,out6,48,&ArKeySchedule6,ivec6,ecount_buf6,&num6);
     if (memcmp(result6, out6, 36)!=0)
        {
                printf("\n testcase:6 failure\n");
        }
        else
        {
                printf("\n testcase:6 success\n");
        }

/*test case 7 : Encrypting 16 octets using AES-CTR with 256-bit key*/
     unArCryptoKey ArKeySchedule7;
     unsigned char au1ArTemp7Subkey[] ={0x77, 0x6B, 0xEF, 0xF2, 0x85, 0x1D, 0xB0, 0x6F, 0x4C, 0x8A, 0x05, 0x42, 0xC8, 0x69, 0x6F, 0x6C, 0x6A, 0x81, 0xAF, 0x1E, 0xEC, 0x96, 0xB4, 0xD3, 0x7F, 0xC1, 0xD6, 0x89, 0xE6, 0xC1, 0xC1, 0x04 };
     const unsigned char in7[]={0x53, 0x69, 0x6E, 0x67, 0x6C, 0x65, 0x20, 0x62, 0x6C, 0x6F, 0x63, 0x6B, 0x20, 0x6D, 0x73, 0x67};
     const unsigned char nonce7[4]= {0x00, 0x00, 0x00, 0x60};
     const unsigned char iv7[8] ={0xDB, 0x56, 0x72, 0xC9, 0x7A, 0xA8, 0xF0, 0xB2};
     const unsigned char counter7[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec7[AES_BLOCK_SIZE];
     memset(ivec7,'\0',16);
     memcpy(ivec7,nonce7,4);
     memcpy(ivec7+4,iv7,8);
     memcpy(ivec7+12,counter7,4);
     unsigned char out7[16];
     unsigned int num7 = 0;
     unsigned char result7[] ={0x14, 0x5A, 0xD0, 0x1D, 0xBF, 0x82, 0x4E, 0xC7, 0x56, 0x08, 0x63, 0xDC, 0x71, 0xE3, 0xE0, 0xC0};
     unsigned char ecount_buf7[AES_BLOCK_SIZE];
     memset(ecount_buf7,'\0',16);
     memset(out7,'\0',16);
     AesArSetEncryptKey(au1ArTemp7Subkey,256,&ArKeySchedule7);
     AesArCtrMode(in7,out7,16,&ArKeySchedule7,ivec7,ecount_buf7,&num7);
     if (memcmp(result7, out7, 16)!=0)
        {
                printf("\n testcase:7 failure\n");
        }
        else
        {
                printf("\n testcase:7 success\n");
        }

/*test case 8 : Encrypting 32 octets using AES-CTR with 256-bit key*/
     unArCryptoKey ArKeySchedule8;
     unsigned char au1ArTemp8Subkey[] ={0xF6, 0xD6, 0x6D, 0x6B, 0xD5, 0x2D, 0x59, 0xBB, 0x07, 0x96, 0x36, 0x58, 0x79, 0xEF, 0xF8, 0x86, 0xC6, 0x6D, 0xD5, 0x1A, 0x5B ,0x6A, 0x99, 0x74, 0x4B, 0x50, 0x59, 0x0C, 0x87, 0xA2, 0x38, 0x84};
     const unsigned char in8[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
     const unsigned char nonce8[4]= {0x00, 0xFA, 0xAC, 0x24};
     const unsigned char iv8[8] ={0xC1, 0x58, 0x5E, 0xF1, 0x5A, 0x43, 0xD8, 0x75};
     const unsigned char counter8[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec8[AES_BLOCK_SIZE];
     memset(ivec8,'\0',16);
     memcpy(ivec8,nonce8,4);
     memcpy(ivec8+4,iv8,8);
     memcpy(ivec8+12,counter8,4);
     unsigned char out8[32];
     unsigned int num8 = 0;
     unsigned char result8[] ={0xF0, 0x5E, 0x23, 0x1B, 0x38, 0x94, 0x61, 0x2C, 0x49, 0xEE, 0x00, 0x0B, 0x80, 0x4E, 0xB2, 0xA9, 0xB8, 0x30, 0x6B, 0x50, 0x8F, 0x83, 0x9D, 0x6A, 0x55, 0x30, 0x83, 0x1D, 0x93, 0x44, 0xAF, 0x1C};
     unsigned char ecount_buf8[AES_BLOCK_SIZE];
     memset(ecount_buf8,'\0',16);
     memset(out8,'\0',32);
     AesArSetEncryptKey(au1ArTemp8Subkey,256,&ArKeySchedule8);
     AesArCtrMode(in8,out8,32,&ArKeySchedule8,ivec8,ecount_buf8,&num8);
     if (memcmp(result8, out8, 32)!=0)
        {
                printf("\n testcase:8 failure\n");
        }
        else
        {
                printf("\n testcase:8 success\n");
        }


/*test case 9 : Encrypting 36 octets using AES-CTR with 256-bit key*/
     unArCryptoKey ArKeySchedule9;
     unsigned char au1ArTemp9Subkey[] ={0xFF, 0x7A, 0x61, 0x7C, 0xE6, 0x91, 0x48, 0xE4, 0xF1, 0x72, 0x6E, 0x2F, 0x43, 0x58, 0x1D, 0xE2, 0xAA, 0x62, 0xD9, 0xF8, 0x05, 0x53, 0x2E, 0xDF, 0xF1, 0xEE, 0xD6, 0x87, 0xFB, 0x54, 0x15, 0x3D};
     const unsigned char in9[]={0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A ,0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22 ,0x23};
     const unsigned char nonce9[4]= {0x00, 0x1C, 0xC5, 0xB7};
     const unsigned char iv9[8] ={0x51, 0xA5, 0x1D, 0x70, 0xA1, 0xC1, 0x11, 0x48};
     const unsigned char counter9[4]= {0x00, 0x00, 0x00, 0x01};
     unsigned char ivec9[AES_BLOCK_SIZE];
     memset(ivec9,'\0',16);
     memcpy(ivec9,nonce9,4);
     memcpy(ivec9+4,iv9,8);
     memcpy(ivec9+12,counter9,4);
     unsigned char out9[48];
     unsigned int num9 = 0;
     unsigned char result9[] ={0xEB, 0x6C, 0x52, 0x82, 0x1D, 0x0B, 0xBB, 0xF7, 0xCE, 0x75, 0x94, 0x46, 0x2A, 0xCA, 0x4F, 0xAA, 0xB4, 0x07, 0xDF, 0x86, 0x65, 0x69, 0xFD, 0x07, 0xF4, 0x8C, 0xC0, 0xB5, 0x83, 0xD6, 0x07, 0x1F, 0x1E, 0xC0, 0xE6, 0xB8};
     unsigned char ecount_buf9[AES_BLOCK_SIZE];
     memset(ecount_buf9,'\0',16);
     memset(out9,'\0',48);
     AesArSetEncryptKey(au1ArTemp9Subkey,256,&ArKeySchedule9);
     AesArCtrMode(in9,out9,48,&ArKeySchedule9,ivec9,ecount_buf9,&num9);
     if (memcmp(result9, out9, 36)!=0)
        {
                printf("\n testcase:9 failure\n\n");
        }
        else
        {
                printf("\n testcase:9 success\n\n");
        }

return ;
}
/*******************************************************************************
 * Function           : aes_ctr_inc_testcases
 *
 * Description        : Function tha contains testcases for counter increment 
 *                      and overflow in all dwords
 *
 * Input(s)           : NONE
 *                     
 * Output(s)          : Results increments and over flow handled counter values
 *
 * Returns            : NONE
 ******************************************************************************/

void aes_ctr_inc_testcases(void)
{
 /*test case 1 : to test bottom dword overflow*/
 unsigned char counter[16]= {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF};
 const unsigned char result[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00};
        AesArIncrCounter(counter);
        if (memcmp(result,counter, 16)!=0)
        {
                printf("\n testcase_ctr:1 failure\n\n");
        }
        else
        {
                printf("\n testcase_ctr:1 success\n\n");
        }
 /*test case 2 : to test last two bottom dword overflow*/	
 unsigned char counter1[16]= {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
 const unsigned char result1[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
        AesArIncrCounter(counter1);
        if (memcmp(result1,counter1, 16)!=0)
        {
                printf("\n testcase_ctr:2 failure\n\n");
        }
        else
        {
                printf("\n testcase_ctr:2 success\n\n");
        }
  /*test case 3 : to test last three bottom dword overflow*/	
 unsigned char counter2[16]= {0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
 const unsigned char result2[16] = {0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
        AesArIncrCounter(counter2);
        if (memcmp(result2,counter2, 16)!=0)
        {
                printf("\n testcase_ctr:3 failure\n\n");
        }
        else
        {
                printf("\n testcase_ctr:3 success\n\n");
        }
  /*test case 4 : to test entire counter overflow*/	
 unsigned char counter4[16]= {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
 const unsigned char result4[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
        AesArIncrCounter(counter4);
        if (memcmp(result4,counter4, 16)!=0)
        {
                printf("\n testcase_ctr:4 failure\n\n");
        }
        else
        {
                printf("\n testcase_ctr:4 success\n\n");
        }
}
