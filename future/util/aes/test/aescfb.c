
/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aescfb.c,v 1.1 2015/04/28 12:53:02 siva Exp $
 *
 * Description: This file contains the code for testcases for AES-CFB 
 *              module
 *********************************************************************/
#include "aesarinc.h"

/*Function that contains testcases for AES-CFB mode of various AES key lengths and block size*/

/*******************************************************************************
 * Function           : aes_cfb_testcases 
 *
 * Description        : Function that contains testcases for AES-CFB mode of 
 *                      various AES plain text inputs 
 *
 * Input(s)           : NONE
 *                     
 * Output(s)          : Results of test vectors
 *
 * Returns            : NONE
 ******************************************************************************/

unArCryptoKey       ArKeySchedule1;
unsigned char       au1ArTemp1Subkey[16] =
    { 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88,
0x09, 0xCF, 0x4F, 0x3C };
PUBLIC VOID
aes_cfb_testcases (void)
{

/*test case 1 : Encrypting 16 octets using AES-CFB with 128-bit key*/
    printf ("\n testcase:1 \n");
    const unsigned char in1[16] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E,
0x11, 0x73, 0x93, 0x17, 0x2A };
    const unsigned char iv1[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       out1[16];
    memset (out1, '\0', 16);
    unsigned char       ivec1[AES_BLOCK_SIZE];
    memset (ivec1, '\0', 16);
    memcpy (ivec1, iv1, 16);
    unsigned int        num1 = 1;

    unsigned char       result1[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0xF8, 0xE8, 0x3C, 0xFB, 0x4A
    };
    int                 ecount_buf1 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in1, out1, 16, &ArKeySchedule1, ivec1, &ecount_buf1,
                         num1);

    printf ("ecpected output:\n\t ");
    for (num1 = 0; num1 < 16; num1++)
    {

        printf (" %x ", result1[num1]);
    }

    printf ("\n\n ");

    printf ("ciper output:\n\t ");
    for (num1 = 0; num1 < 16; num1++)
    {
            printf (" %x ", out1[num1]);
    }

    if (memcmp (result1, out1, 16) != 0)
    {
        printf ("\n testcase:1 failure\n");
    }
    else
    {
        printf ("\n testcase:1 success\n");
    }

/*test case 2 : Encrypting 16 octets using AES-CFB with 128-bit key*/
    printf ("\n testcase:2 ");
    const unsigned char in2[16] =
        { 0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C, 0x9E, 0xB7, 0x6F,
0xAC, 0x45, 0xAF, 0x8E, 0x51 };
    const unsigned char iv2[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20, 0x33, 0x34, 0x49,
0xF8, 0xE8, 0x3C, 0xFB, 0x4A };
    unsigned char       out2[16];
    memset (out1, '\0', 16);
    unsigned char       ivec2[AES_BLOCK_SIZE];
    memset (ivec2, '\0', 16);
    memcpy (ivec2, iv2, 16);
    unsigned int        num2 = 1;
    unsigned char       result2[16] =
        { 0xC8, 0xA6, 0x45, 0x37, 0xA0, 0xB3, 0xA9, 0x3F, 0xCD, 0xE3, 0xCD,
0xAD, 0x9F, 0x1C, 0xE5, 0x8B };
    int                 ecount_buf2 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in2, out2, 16, &ArKeySchedule1, ivec2, &ecount_buf2,
                         num2);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num2 = 0; num2 < 16; num2++)
    {
        printf (" %x ", result2[num2]);
    }

    printf ("\n ");

    printf ("ciper output:\n\t ");
    for (num2 = 0; num2 < 16; num2++)
    {
        printf (" %x ", out2[num2]);
    }

    if (memcmp (result2, out2, 16) != 0)
    {
        printf ("\n testcase:2 failure\n");
    }
    else
    {
        printf ("\n testcase:2 success\n");
    }

    /*test case 3 : Encrypting 16 octets using AES-CFB with 128-bit key */
   printf ("\n testcase:3 ");
    const unsigned char in3[16] =
        { 0x30, 0xC8, 0x1C, 0x46, 0xA3, 0x5C, 0xE4, 0x11, 0xE5, 0xFB, 0xC1,
0x19, 0x1A, 0x0A, 0x52, 0xEF };
    const unsigned char iv3[16] =
        { 0xC8, 0xA6, 0x45, 0x37, 0xA0, 0xB3, 0xA9, 0x3F, 0xCD, 0xE3, 0xCD,
0xAD, 0x9F, 0x1C, 0xE5, 0x8B };
    unsigned char       out3[16];
    memset (out3, '\0', 16);
    unsigned char       ivec3[AES_BLOCK_SIZE];
    memset (ivec3, '\0', 16);
    memcpy (ivec3, iv3, 16);
    unsigned int        num3 = 1;
    unsigned char       result3[16] =
        { 0x26, 0x75, 0x1F, 0x67, 0xA3, 0xCB, 0xB1, 0x40, 0xB1, 0x80, 0x8C,
0xF1, 0x87, 0xA4, 0xF4, 0xDF };
    int                 ecount_buf3 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in3, out3, 16, &ArKeySchedule1, ivec3, &ecount_buf3,
                         num3);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num3 = 0; num3 < 16; num3++)
    {
        printf (" %x ", result3[num3]);
    }

    printf ( "\n ");

    printf ( "ciper output:\n\t ");
    for (num3 = 0; num3 < 16; num3++)
    {
        printf ( " %x ", out3[num3]);
    }

    if (memcmp (result3, out3, 16) != 0)
    {
        printf ("\n testcase:3 failure\n");
    }
    else
    {
        printf ("\n testcase:3 success\n");
    }

    /*test case 4 : Encrypting 16 octets using AES-CFB with 128-bit key */
    printf ("\n testcase:4 ");
    const unsigned char in4[16] =
        { 0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17, 0xAD, 0x2B, 0x41,
0x7B, 0xE6, 0x6C, 0x37, 0x10 };
    const unsigned char iv4[16] =
        { 0x26, 0x75, 0x1F, 0x67, 0xA3, 0xCB, 0xB1, 0x40, 0xB1, 0x80, 0x8C,
0xF1, 0x87, 0xA4, 0xF4, 0xDF };
    unsigned char       out4[16];
    memset (out4, '\0', 16);
    unsigned char       ivec4[AES_BLOCK_SIZE];
    memset (ivec4, '\0', 16);
    memcpy (ivec4, iv4, 16);
    unsigned int        num4 = 4;
    unsigned char       result4[16] =
        { 0xC0, 0x4B, 0x05, 0x35, 0x7C, 0x5D, 0x1C, 0x0E, 0xEA, 0xC4, 0xC6,
0x6F, 0x9F, 0xF7, 0xF2, 0xE6 };
    int                 ecount_buf4 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in4, out4, 16, &ArKeySchedule1, ivec4, &ecount_buf4,
                         num4);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num4 = 0; num4 < 16; num4++)
    {
        printf (" %x ", result4[num4]);
    }

    printf ("\n ");

    printf ("ciper output:\n\t ");
    for (num4 = 0; num4 < 16; num4++)
    {
        printf (" %x ", out4[num4]);
    }

    if (memcmp (result4, out4, 16) != 0)
    {
        printf ("\n testcase:4 failure\n");
    }
    else
    {
        printf ("\n testcase:4 success\n");
    }

    /*test case 5 : Decrypting 16 octets using AES-CFB with 128-bit key */
    printf ("\n testcase:5 ");
    const unsigned char iv5[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       in5[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0xF8, 0xE8, 0x3C, 0xFB, 0x4A
    };
    const unsigned char result5[16] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E,
0x11, 0x73, 0x93, 0x17, 0x2A };
    unsigned char       out5[16];
    memset (out5, '\0', 16);
    unsigned char       ivec5[AES_BLOCK_SIZE];
    memset (ivec5, '\0', 16);
    memcpy (ivec5, iv5, 16);
    unsigned int        num5 = 0;

    int                 ecount_buf5 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in5, out5, 16, &ArKeySchedule1, ivec5, &ecount_buf5,
                         num5);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num5 = 0; num5 < 16; num5++)
    {
        printf (" %x ", out5[num5]);
    }

    printf ("\n");

    printf ("ciper output:\n\t ");

    for (num5 = 0; num5 < 16; num5++)
    {
        printf (" %x ", result5[num5]);
    }
    if (memcmp (result5, out5, 16) != 0)
    {
        printf ("\n testcase:5 failure\n");
    }
    else
    {
        printf ("\n testcase:5 success\n");
    }

    /*test case 6 : Decrypting 16 octets using AES-CFB with 128-bit key */
    printf ("\n testcase:6 ");
    const unsigned char iv6[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20, 0x33, 0x34, 0x49,
0xF8, 0xE8, 0x3C, 0xFB, 0x4A };
    unsigned char       in6[16] =
        { 0xC8, 0xA6, 0x45, 0x37, 0xA0, 0xB3, 0xA9, 0x3F, 0xCD, 0xE3, 0xCD,
0xAD, 0x9F, 0x1C, 0xE5, 0x8B };
    unsigned char       out6[16];
    memset (out6, '\0', 16);
    unsigned char       ivec6[AES_BLOCK_SIZE];
    memset (ivec6, '\0', 16);
    memcpy (ivec6, iv6, 16);
    unsigned int        num6 = 0;
    const unsigned char result6[16] =
        { 0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C, 0x9E, 0xB7, 0x6F,
0xAC, 0x45, 0xAF, 0x8E, 0x51 };
    int                 ecount_buf6 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in6, out6, 16, &ArKeySchedule1, ivec6, &ecount_buf6,
                         num6);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num6 = 0; num6 < 16; num6++)
    {
        printf (" %x ", out6[num6]);
    }

    printf ("\n ");

    printf ("ciper output:\n\t ");
    for (num6 = 0; num6 < 16; num6++)
    {
        printf (" %x ", result6[num6]);
    }
    if (memcmp (result6, out6, 16) != 0)
    {
        printf ("\n testcase:6 failure\n");
    }
    else
    {
        printf ("\n testcase:6 success\n");
    }

    /*test case 7 : Decrypting 16 octets using AES-CFB with 128-bit key */
    printf ("\n testcase:7 ");
    const unsigned char iv7[16] =
        { 0xC8, 0xA6, 0x45, 0x37, 0xA0, 0xB3, 0xA9, 0x3F, 0xCD, 0xE3, 0xCD,
0xAD, 0x9F, 0x1C, 0xE5, 0x8B };
    unsigned char       in7[16] =
        { 0x26, 0x75, 0x1F, 0x67, 0xA3, 0xCB, 0xB1, 0x40, 0xB1, 0x80, 0x8C,
0xF1, 0x87, 0xA4, 0xF4, 0xDF };
    unsigned char       out7[16];
    memset (out7, '\0', 16);
    unsigned char       ivec7[AES_BLOCK_SIZE];
    memset (ivec7, '\0', 16);
    memcpy (ivec7, iv7, 16);
    unsigned int        num7 = 1;
    const unsigned char result7[16] =
        { 0x30, 0xC8, 0x1C, 0x46, 0xA3, 0x5C, 0xE4, 0x11, 0xE5, 0xFB, 0xC1,
0x19, 0x1A, 0x0A, 0x52, 0xEF };
    int                 ecount_buf7 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in7, out7, 16, &ArKeySchedule1, ivec7, &ecount_buf7,
                         num7);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num7 = 0; num7 < 16; num7++)
    {
        printf (" %x ", out7[num7]);
    }

    printf ("\n");

    printf ("ciper output:\n\t ");
    for (num7 = 0; num7 < 16; num7++)
    {

        printf (" %x ", result7[num7]);
    }

    if (memcmp (result7, out7, 16) != 0)
    {
        printf ("\n testcase:7 failure\n");
    }
    else
    {
        printf ("\n testcase:7 success\n");
    }

    /*test case 8 : Decrypting 16 octets using AES-CFB with 128-bit key */
    printf ("\n testcase:8 ");
    const unsigned char iv8[16] =
        { 0x26, 0x75, 0x1F, 0x67, 0xA3, 0xCB, 0xB1, 0x40, 0xB1, 0x80, 0x8C,
0xF1, 0x87, 0xA4, 0xF4, 0xDF };
    unsigned char       in8[16] =
        { 0xC0, 0x4B, 0x05, 0x35, 0x7C, 0x5D, 0x1C, 0x0E, 0xEA, 0xC4, 0xC6,
0x6F, 0x9F, 0xF7, 0xF2, 0xE6 };
    unsigned char       out8[16];
    memset (out8, '\0', 16);
    unsigned char       ivec8[AES_BLOCK_SIZE];
    memset (ivec8, '\0', 16);
    memcpy (ivec8, iv8, 16);
    unsigned int        num8 = 0;
    const unsigned char result8[16] =
        { 0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17, 0xAD, 0x2B, 0x41,
0x7B, 0xE6, 0x6C, 0x37, 0x10 };
    int                 ecount_buf8 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in8, out8, 16, &ArKeySchedule1, ivec8, &ecount_buf8,
                         num8);

    printf ("\n");
    printf ("ecpected output:\n\t ");
    for (num8 = 0; num8 < 16; num8++)
    {
        printf (" %x ", out8[num8]);
    }

    printf ("\n ");

    printf ("ciper output:\n\t ");

    for (num8 = 0; num8 < 16; num8++)
    {
        printf (" %x ", result8[num8]);
    }
    if (memcmp (result8, out8, 16) != 0)
    {
        printf ("\n testcase:8 failure\n");
    }
    else
    {
        printf ("\n testcase:8 success\n");
    }
     
     /* test case:9 encrypting 32 octets using AES-CFB with 128-bit key */
     printf ("\n testcase:9 \n");
    const unsigned char in9[32] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17, 0x2A, 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17 };
    const unsigned char iv9[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       out9[32];
    memset (out9, '\0', 32);
    unsigned char       ivec9[AES_BLOCK_SIZE];
    memset (ivec9, '\0', 16);
    memcpy (ivec9, iv9, 16);
    unsigned int        num9 = 1;

    unsigned char       result9[32] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0xF8, 0xE8, 0x3C, 0xFB, 0x4A, 0xD, 0x4A, 0x71, 0x82, 0x90, 0xF0, 0x9A, 0x35, 0xBA, 0x69, 0xDC, 0x10, 0xA9, 0x20, 0x7C, 0xDA
    };
    int                 ecount_buf9 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in9, out9, 32, &ArKeySchedule1, ivec9, &ecount_buf9,
                         num1);

    printf ("ecpected output:\n\t ");
    for (num9 = 0; num9 < 32; num9++)
    {

        printf (" %x ", result9[num9]);
    }

    printf ("\n\n ");

    printf ("ciper output:\n\t ");
    for (num9 = 0; num9 < 32; num9++)
    {
        printf (" %x ", out9[num9]);
    }

    if (memcmp (result9, out9, 32) != 0)
    {
        printf ("\n testcase:1 failure\n");
    }
    else
    {
        printf ("\n testcase:1 success\n");
    }


    /* test case:10 encrypting 11 octets using AES-CFB with 128-bit key */
     printf ("\n testcase:10 \n");
    const unsigned char in10[11] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E };
    const unsigned char iv10[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       out10[16];
    memset (out10, '\0', 16);
    unsigned char       ivec10[AES_BLOCK_SIZE];
    memset (ivec10, '\0', 16);
    memcpy (ivec10, iv10, 16);
    unsigned int        num10 = 1;

    unsigned char       result10[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    int                 ecount_buf10 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in10, out10, 11, &ArKeySchedule1, ivec10, &ecount_buf10,
                         num10);

    printf ("ecpected output:\n\t ");
    for (num10 = 0; num10 < 11; num10++)
    {

        printf (" %x ", result10[num10]);
    }

    printf ("\n\n ");

    printf ("ciper output:\n\t ");
    for (num10 = 0; num10 < 16; num10++)
    {
        printf (" %x ", out10[num10]);
    }

    if (memcmp (result10, out10, 16) != 0)
    {
        printf ("\n testcase:1 failure\n");
    }
    else
    {
        printf ("\n testcase:1 success\n");
    }
    
     printf ("\n testcase:11 for decryption  \n");
    const unsigned char in11[32] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E,
0x11, 0x73, 0x93, 0x17, 0x2A, 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17 };
    const unsigned char iv11[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       out11[16];
    memset (out11, '\0', 16);
    unsigned char       ivec11[AES_BLOCK_SIZE];
    memset (ivec11, '\0', 16);
    memcpy (ivec11, iv11, 16);
    unsigned int        num11 = 0;
    int j;

    unsigned char       result11[32] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0xF8, 0xE8, 0x3C, 0xFB, 0x4A, 0x51, 0x16, 0xC5, 0x56, 0x23, 0x3A, 0xA9, 0xF6, 0x41, 0xA3, 0xB4, 0xE2, 0x57, 0xF5, 0xF8, 0x97
    };
    int                 ecount_buf11 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))
    {

        AesArCfb128 (in11, out11, 32, &ArKeySchedule1, ivec11, &ecount_buf11,
                         num11);
       
    }

    printf ("ecpected output:\n\t ");
    for (num11 = 0; num11 < 32; num11++)
    {

        printf (" %x ", result11[num11]);
    }

    printf ("\n\n ");

    printf ("ciper output:\n\t ");
    for (num11 = 0; num11 < 32; num11++)
    {
        printf (" %x ", out11[num11]);
    }

    if (memcmp (result11, out11, 32) != 0)
    {
        printf ("\n testcase:11 failure\n");
    }
    else
    {
        printf ("\n testcase:11 success\n");
    }

     printf ("\n testcase:12 \n");
    const unsigned char in12[11] =
        { 0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9f, 0x96, 0xE9, 0x3D, 0x7E };
    const unsigned char iv12[16] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
    unsigned char       out12[16];
    memset (out12, '\0', 16);
    unsigned char       ivec12[AES_BLOCK_SIZE];
    memset (ivec12, '\0', 16);
    memcpy (ivec12, iv12, 16);
    unsigned int        num12 = 0;

    unsigned char       result12[16] =
        { 0x3B, 0x3F, 0xD9, 0x2E, 0xB7, 0x2D, 0xAD, 0x20,
        0x33, 0x34, 0x49, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    int                 ecount_buf12 = 0;
    if (AesArSetEncryptKey (au1ArTemp1Subkey, 128, &ArKeySchedule1))

        AesArCfb128 (in12, out12, 11, &ArKeySchedule1, ivec12, &ecount_buf12,
                         num12);
        
    printf ("ecpected output:\n\t ");
    for (num12 = 0; num12 < 16; num12++)
    {
        printf (" %x ", result12[num12]);
    }

    printf ("\n\n ");

    printf ("ciper output:\n\t ");
    for (num12 = 0; num12 < 16; num12++)
    {
        printf (" %x ", out12[num12]);
    }

    if (memcmp (result12, out12, 16) != 0)
    {
        printf ("\n testcase:12 failure\n");
    }
    else
    {
        printf ("\n testcase:12 success\n");
    }


    return;
}
