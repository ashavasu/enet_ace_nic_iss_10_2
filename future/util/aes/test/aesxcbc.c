/********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 *  $Id: aesxcbc.c,v 1.1 2015/04/28 12:53:02 siva Exp $      *
 * 				                                                    *
 *  Description: This file contains the test vectors for verifying  *
 *               AES-XCBC-MAC and AES-XCBC-PRF algorithm.           *
 ********************************************************************/

#include "aesarinc.h"

PUBLIC VOID testvectors PROTO ((VOID));

PUBLIC VOID testvectors()
{
		unsigned char digest96[16];
		unsigned char digest128[16]; 		
		/* Test Case #1   : AES-XCBC-MAC-96 with 0-byte input
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : <empty string>
		 * AES-XCBC-MAC   : 75f0251d528ac01c4573dfd584d79f29
		 * AES-XCBC-MAC-96: 75f0251d528ac01c4573dfd5
		 * 		 		 		 		 		 */
		unsigned char key1[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain1[] = {
		};
		unsigned char mac1[] = {
				0x75,0xf0,0x25,0x1d,0x52,0x8a,0xc0,0x1c,
				0x45,0x73,0xdf,0xd5,0x84,0xd7,0x9f,0x29
		};
		AesArXcbcMac96(key1,16,plain1,0,digest96);
		AesArXcbcMac128(key1,16,plain1,0,digest128);


		if ((memcmp(digest96, mac1, 12)!=0) && (memcmp(digest128, mac1, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-1\n");
		}

		/*
		 * Test Case #2   : AES-XCBC-MAC-96 with 3-byte input
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : 000102
		 * AES-XCBC-MAC   : 5b376580ae2f19afe7219ceef172756f
		 * AES-XCBC-MAC-96: 5b376580ae2f19afe7219cee
		 * 		 		 		 		 		 		 */
		unsigned char key2[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain2[] = {
				0x00,0x01,0x02
		};
		unsigned char mac2[] = {
				0x5b,0x37,0x65,0x80,0xae,0x2f,0x19,0xaf,
				0xe7,0x21,0x9c,0xee,0xf1,0x72,0x75,0x6f
		};
		AesArXcbcMac96(key2,16,plain2,3,digest96);
		AesArXcbcMac128(key2,16,plain2,3,digest128);

		if ((memcmp(digest96, mac2, 12)!=0) && (memcmp(digest128, mac2, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-2\n");
		}


		/* Test Case #3   : AES-XCBC-MAC-96 with 16-byte input
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : 000102030405060708090a0b0c0d0e0f
		 * AES-XCBC-MAC   : d2a246fa349b68a79998a4394ff7a263
		 * AES-XCBC-MAC-96: d2a246fa349b68a79998a439
		 * 		 		 		 		 		 */
		unsigned char key3[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain3[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char mac3[] = {
				0xd2,0xa2,0x46,0xfa,0x34,0x9b,0x68,0xa7,
				0x99,0x98,0xa4,0x39,0x4f,0xf7,0xa2,0x63
		};
		AesArXcbcMac96(key3,16,plain3,16,digest96);
		AesArXcbcMac128(key3,16,plain3,16,digest128);

		if ((memcmp(digest96, mac3, 12)!=0) && (memcmp(digest128, mac3, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-3\n");
		}

		/*  Test Case #4   : AES-XCBC-MAC-96 with 20-byte input
		 * 	Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * 	Message (M)    : 000102030405060708090a0b0c0d0e0f10111213
		 * 	AES-XCBC-MAC   : 47f51b4564966215b8985c63055ed308
		 * 	AES-XCBC-MAC-96: 47f51b4564966215b8985c63
		 * 		 		 		 		 		 */
		unsigned char key4[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain4[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
				0x10,0x11,0x12,0x13
		};
		unsigned char mac4[] = {
				0x47,0xf5,0x1b,0x45,0x64,0x96,0x62,0x15,
				0xb8,0x98,0x5c,0x63,0x05,0x5e,0xd3,0x08
		};
		AesArXcbcMac96(key4,16,plain4,20,digest96);
		AesArXcbcMac128(key4,16,plain4,20,digest128);

		if ((memcmp(digest96, mac4, 12)!=0) && (memcmp(digest128, mac4, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-4\n");
		}
		/* Test Case #5   : AES-XCBC-MAC-96 with 32-byte input
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : 000102030405060708090a0b0c0d0e0f10111213141516171819
		 * 	                1a1b1c1d1e1f
		 * AES-XCBC-MAC   : f54f0ec8d2b9f3d36807734bd5283fd4
		 * AES-XCBC-MAC-96: f54f0ec8d2b9f3d36807734b
		 * 		 		 		 		 		 		 */
		unsigned char key5[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain5[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
				0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,
				0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f
		};
		unsigned char mac5[] = {
				0xf5,0x4f,0x0e,0xc8,0xd2,0xb9,0xf3,0xd3,
				0x68,0x07,0x73,0x4b,0xd5,0x28,0x3f,0xd4
		};
		AesArXcbcMac96(key5,16,plain5,32,digest96);
		AesArXcbcMac128(key5,16,plain5,32,digest128);

		if ((memcmp(digest96, mac5, 12)!=0) && (memcmp(digest128, mac5, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-5\n");
		}
		/* Test Case #6  : AES-XCBC-MAC-96 with 32-byte input using append mode
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : 000102030405060708090a0b0c0d0e0f10111213141516171819
		 * 	                1a1b1c1d1e1f2021
		 * AES-XCBC-MAC : becbb3bccdb518a30677d5481fb6b4d8
		 * AES-XCBC-MAC-96: becbb3bccdb518a30677d548
		 * 		 		 		 		 		 		 *
		 * 		 		 		 		 		 		 		 */
		unsigned char key6[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain6[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
				0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,
				0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
				0x20,0x21
		};
		unsigned char mac6[] = {
				0xbe,0xcb,0xb3,0xbc,0xcd,0xb5,0x18,0xa3,
				0x06,0x77,0xd5,0x48,0x1f,0xb6,0xb4,0xd8
		};
		AesArXcbcMac96(key6,16,plain6,34,digest96);
		AesArXcbcMac128(key6,16,plain6,34,digest128);

		if ((memcmp(digest96, mac6, 12)!=0) && (memcmp(digest128, mac6, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-6\n");
		}

		/* Test Case #7   : AES-XCBC-MAC-96 with 600-byte input
		 * Key (K)        : 000102030405060708090a0b0c0d0e0f
		 * Message (M)    : 00000000000000000000 ... 00000000000000000000
		 *                  [1000 bytes]
		 * AES-XCBC-MAC   : f0dafee895db30253761103b5d84528f
		 * AES-XCBC-MAC-96: f0dafee895db30253761103b
		 * 		 		 		 		 		 		 */
		unsigned char key7[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain7[1000];
		memset(plain7, 0, 1000);
		unsigned char mac7[] = {
				0xf0,0xda,0xfe,0xe8,0x95,0xdb,0x30,0x25,
				0x37,0x61,0x10,0x3b,0x5d,0x84,0x52,0x8f
		};
		AesArXcbcMac96(key7,16,plain7,1000,digest96);
		AesArXcbcMac128(key7,16,plain7,1000,digest128);

		if ((memcmp(digest96, mac7, 12)!=0) && (memcmp(digest128, mac7, 16)!=0))
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 3566- TEST CASE-7\n");
		}


		/* Test Case AES-XCBC-PRF-128 with 20-byte input
		 * Key        : 000102030405060708090a0b0c0d0e0f
		 * Message    : 000102030405060708090a0b0c0d0e0f10111213
		 * PRF Output : 47f51b4564966215b8985c63055ed308
		 *                     */
		unsigned char key9[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
		};
		unsigned char plain9[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
				0x10,0x11,0x12,0x13
		};
		unsigned char mac9[] = {
				0x47,0xf5,0x1b,0x45,0x64,0x96,0x62,0x15,
				0xb8,0x98,0x5c,0x63,0x05,0x5e,0xd3,0x08
		};



		AesArXcbcPrf128(key9,16,plain9,20,digest128);

		if (memcmp(digest128, mac9, 16)!=0)
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 4344- TEST CASE-2\n");
		}


		/* variable key test, RFC4434
		 *
		 * Test Case AES-XCBC-PRF-128 with 20-byte input
		 * Key        : 00010203040506070809
		 * Message    : 000102030405060708090a0b0c0d0e0f10111213
		 * PRF Output : 0fa087af7d866e7653434e602fdde835
		 *                         */
		unsigned char key8[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09
		};
		unsigned char plain8[] = {
				0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
				0x10,0x11,0x12,0x13
		};
		unsigned char mac8[] = {
				0x0f,0xa0,0x87,0xaf,0x7d,0x86,0x6e,0x76,
				0x53,0x43,0x4e,0x60,0x2f,0xdd,0xe8,0x35
		};


		AesArXcbcPrf128(key8,10,plain8,20,digest128);

		if (memcmp(digest128, mac8, 16)!=0)
		{
				printf("\nfailure\n");
		}
		else
		{
				printf("\nSUCCESS-RFC 4344- TEST CASE-1\n");
		}





}
