######################################################################
# Copyright (C) 2008 Aricent Inc . All Rights Reserved                #
# ------------------------------------------                          #
# $Id: make.h,v 1.2 2015/04/28 12:51:41 siva Exp $                     #
#    DESCRIPTION            : Specifies the options and modules to be #
#                             included for building the AES module    #
#######################################################################
include ../../LR/make.h
include ../../LR/make.rule

AES_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES} -DTRACE_WANTED

COMN_INCL_DIR = $(BASE_DIR)/inc
CRYPTO_AES_BASE_DIR = $(BASE_DIR)/util/aes
CRYPTO_AES_SRC_DIR = $(CRYPTO_AES_BASE_DIR)/src
CRYPTO_AES_INC_DIR = $(CRYPTO_AES_BASE_DIR)/inc
CRYPTO_AES_OBJ_DIR = $(CRYPTO_AES_BASE_DIR)/obj
CRYPTO_INCLUDES = -I$(CRYPTO_AES_INC_DIR) -I$(COMN_INCL_DIR) ${COMMON_INCLUDE_DIRS}

