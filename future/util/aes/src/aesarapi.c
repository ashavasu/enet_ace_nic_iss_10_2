/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesarapi.c,v 1.1 2015/04/28 12:53:02 siva Exp $
 *
 * Description: This file contains the API code for AES module
 *********************************************************************/
#include "aesarinc.h"

UINT4               gu4UtlTrcFlag = 0;

/*******************************************************************************
 * Function           : AesArSetEncryptKey 
 *
 * Description        : This function is used to create the Key Schedule for
 *                      Encryption.
 *
 * Input(s)           : pu1ArUserKey - Input Key
 *                      u2ArKeyLength - Key Length
 * 
 * Output(s)          : punArCryptoKey - Key Schedul
 *
 * Returns            : AES_SUCCESS/AES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
AesArSetEncryptKey (UINT1 *pu1ArUserKey, UINT2 u2ArKeyLength,
                    unArCryptoKey * punArCryptoKey)
{
    if ((pu1ArUserKey == NULL) || (punArCryptoKey == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "NULL Pointers passed to"
                  "AesArSetEncryptKey\n");
        return AES_FAILURE;
    }
    /* The key length varies for all the 3 modes of AES.
     * Check for the Key Length and call approprite function
     */
    switch (u2ArKeyLength)
    {
        case AES_128_KEY_LENGTH:
            AesAr128Setup (punArCryptoKey, pu1ArUserKey);
            break;
        case AES_192_KEY_LENGTH:
            AesAr192Setup (punArCryptoKey, pu1ArUserKey);
            break;
        case AES_256_KEY_LENGTH:
            AesAr256Setup (punArCryptoKey, pu1ArUserKey);
            break;
        default:
            UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length Passed in"
                      "AesArSetEncryptKey\n");
            return AES_FAILURE;
    }
    return AES_SUCCESS;
}

/*******************************************************************************
 * Function           : AesArSetDecryptKey 
 *
 * Description        : This function is used to create the Key Schedule for
 *                      Decryption.
 *
 * Input(s)           : pu1ArUserKey - Input Key
 *                      u2ArKeyLength - Key Length
 * 
 * Output(s)          : punArCryptoKey - Key Schedul
 *
 * Returns            : AES_SUCCESS/AES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
AesArSetDecryptKey (UINT1 *pu1ArUserKey, UINT2 u2ArKeyLength,
                    unArCryptoKey * punArCryptoKey)
{
    if ((pu1ArUserKey == NULL) || (punArCryptoKey == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL Pointers passed to"
                  "AesArSetDecryptKey\n");
        return AES_FAILURE;
    }
    /* The key length varies for all the 3 modes of AES.
     * Check for the Key Length and call approprite function
     */
    switch (u2ArKeyLength)
    {
        case AES_128_KEY_LENGTH:
            AesAr128Setup (punArCryptoKey, pu1ArUserKey);
            break;
        case AES_192_KEY_LENGTH:
            AesAr192Setup (punArCryptoKey, pu1ArUserKey);
            break;
        case AES_256_KEY_LENGTH:
            AesAr256Setup (punArCryptoKey, pu1ArUserKey);
            break;
        default:
            UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length Passed in"
                      "AesArSetDecryptKey\n");
            return AES_FAILURE;
    }
    return AES_SUCCESS;
}

/*******************************************************************************
 * Function           : AesArCbcEncrypt 
 *
 * Description        :This Function is called by the Modules which Requires the
 *                     data to be Encrypted by CBC
 *                      
 *
 * Input(s)           :pu1ArBuffer : Refers to the Incoming Data to be Encrypted
 *                     u4ArSize : Refers to the Size of the Data
 *                     u2ArInKeyLen : Length of the InputKey
 *                     pu1ArCryptoKey : Key used for encrypting the data
 *                     pu1ArInitVect : IV To Be used
 * 
 * Output(s)          : pu1ArBuffer : Encrypted Data
 *
 * Returns            : AES_SUCCESS/AES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
AesArCbcEncrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize, UINT2 u2ArInKeyLen,
                 UINT1 *pu1ArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArAesBlockcipher = NULL;
    unArCryptoKey       unArAesCryptoKey;
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT1              *pu1ArText = NULL;
    UINT1               u1ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (pu1ArCryptoKey == NULL) ||
        (pu1ArInitVect == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL Pointers passed to"
                  "AesArCbcEncrypt\n");
        return AES_FAILURE;
    }
    /* Depending on the key length select the type of Blockcipher */
    switch (u2ArInKeyLen)
    {
        case AES_128_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes128;
            break;
        case AES_192_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes192;
            break;
        case AES_256_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes256;
            break;
        default:
            UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length  Passed in"
                      "AesArCbcEncrypt\n");
            return AES_FAILURE;
    }
    pArAesBlockcipher->ArCryptoSetup (&unArAesCryptoKey, pu1ArCryptoKey);
    u4ArInitVectSize = pArAesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)
    {
        pu1ArText = pu1ArBuffer + u4ArOffset;

        for (u1ArLoopVar = CRY_INIT_VAL; u1ArLoopVar < u4ArInitVectSize;
             u1ArLoopVar++)
        {
            pu1ArText[u1ArLoopVar] =
                pu1ArInitVect[u1ArLoopVar] ^ pu1ArText[u1ArLoopVar];
        }
        pArAesBlockcipher->ArCryptoEncrypt (&unArAesCryptoKey, pu1ArText,
                                            pu1ArText);
        u4ArSize = u4ArSize - (pArAesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, pu1ArText, u4ArInitVectSize);
        u4ArOffset = u4ArOffset + (UINT1) (pArAesBlockcipher->u4ArBlocksize);
    }
    return AES_SUCCESS;
}

/*******************************************************************************
 * Function           : AesArCbcDecrypt 
 *
 * Description        :This Function is called by the Modules which Requires the
 *                     data to be Decrypted by CBC
 *                      
 *
 * Input(s)           :pu1ArBuffer : Refers to the Incoming Data to be Decrypted
 *                     u4ArSize : Refers to the Size of the Data
 *                     u1InKeyLen : Length of the InputKey
 *                     pu1ArCryptoKey : Key used for decrypting the data
 *                     pu1ArInitVect : IV To Be used
 * 
 * Output(s)          :pu1ArBuffer: Decrypted Data
 *
 * Returns            : AES_SUCCESS/AES_FAILURE
 ******************************************************************************/
PUBLIC UINT1
AesArCbcDecrypt (UINT1 *pu1ArBuffer, UINT4 u4ArSize, UINT2 u2ArInKeyLen,
                 UINT1 *pu1ArCryptoKey, UINT1 *pu1ArInitVect)
{
    const tArBlockcipher *pArAesBlockcipher = NULL;
    unArCryptoKey       unArAesCryptoKey;
    UINT4               u4ArInitVectSize = CRY_INIT_VAL;
    UINT1              *pu1ArText = NULL;
    UINT1               au1ArTemp[AES_BLOCK_SIZE] = { CRY_INIT_VAL };
    UINT4               u4ArOffset = CRY_INIT_VAL;
    UINT1               u1ArLoopVar = CRY_INIT_VAL;
    if ((pu1ArBuffer == NULL) || (pu1ArCryptoKey == NULL) ||
        (pu1ArInitVect == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL Pointers passed to"
                  "AesArCbcDecrypt\n");
        return AES_FAILURE;
    }

    switch (u2ArInKeyLen)
    {
        case AES_128_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes128;
            break;
        case AES_192_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes192;
            break;
        case AES_256_KEY_LENGTH:
            pArAesBlockcipher = &gArBlockcipherAes256;
            break;
        default:
            UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length Passed in"
                      "AesArCbcDecrypt\n");
            return AES_FAILURE;
    }
    pArAesBlockcipher->ArCryptoSetup (&unArAesCryptoKey, pu1ArCryptoKey);
    u4ArInitVectSize = pArAesBlockcipher->u4ArBlocksize;
    while (u4ArSize != CRY_INIT_VAL)
    {
        pu1ArText = pu1ArBuffer + u4ArOffset;
        CRY_MEMCPY (au1ArTemp, pu1ArText, u4ArInitVectSize);

        pArAesBlockcipher->ArCryptoDecrypt (&unArAesCryptoKey,
                                            pu1ArText, pu1ArText);
        for (u1ArLoopVar = CRY_INIT_VAL; u1ArLoopVar < u4ArInitVectSize;
             u1ArLoopVar++)
        {
            pu1ArText[u1ArLoopVar] =
                pu1ArInitVect[u1ArLoopVar] ^ pu1ArText[u1ArLoopVar];
        }

        if (u4ArSize < u4ArInitVectSize)
        {
            u4ArInitVectSize = u4ArSize;
            u4ArSize = CRY_INIT_VAL;
        }
        else
        {
            u4ArSize = u4ArSize - (pArAesBlockcipher->u4ArBlocksize);
        }

        u4ArOffset = u4ArOffset + (UINT1) (pArAesBlockcipher->u4ArBlocksize);
        CRY_MEMCPY (pu1ArInitVect, au1ArTemp, u4ArInitVectSize);

    }
    return AES_SUCCESS;
}

/*******************************************************************************
 * Function           : AesArEncrypt
 *
 * Description        : This function is used to encrypt the data using AES mode
 *                      of encryption.
 *
 * Input(s)           : punArCryptoKey : Pointer to the structure identifying the
 *                      key type
 *                      pu1ArPlainText : Pointer to the input data which needs
 *                      encryption
 *                     
 * Output(s)          : pu1ArEncryptedText : Pointer to the encrypted data
 *
 * Returns            : NONE 
 ******************************************************************************/
PUBLIC VOID
AesArEncrypt (const unArCryptoKey * punArKeySchedule,
              const UINT1 *pu1ArPlainText, UINT1 *pu1ArEncryptedText)
{
    const UINT1        *pu1ArKeySchedule = NULL;
    UINT4               u4ArStateVar = CRY_INIT_VAL;
    UINT4               u4ArRoundVar = CRY_INIT_VAL;
    UINT1               au1ArStateMatrix1[AES_STATE_SIZE] = { CRY_INIT_VAL };
    UINT1               au1ArStateMatrix2[AES_STATE_SIZE] = { CRY_INIT_VAL };

    pu1ArKeySchedule = punArKeySchedule->tArAes.au1ArSubkey;

    /* Perform Add Round Key Operation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix1[u4ArStateVar] =
            pu1ArPlainText[u4ArStateVar] ^ pu1ArKeySchedule[u4ArStateVar];
    }
    /* Fetch the next key schedule */
    pu1ArKeySchedule = pu1ArKeySchedule + AES_STATE_SIZE;

    /* Perform the Encryption steps for one less than total number of rounds 
     * as per the Key Length
     */
    for (u4ArRoundVar = 0; u4ArRoundVar <
         (punArKeySchedule->tArAes.u4ArRounds - 1); u4ArRoundVar++)
    {
        /* Perform Sub Bytes Transformation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix1[u4ArStateVar] =
                gaArSf[au1ArStateMatrix1[u4ArStateVar]];
        }

        /* Perform Shift Rows Transformation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix2[u4ArStateVar] =
                au1ArStateMatrix1[gaArHf[u4ArStateVar]];
        }

        /* Perform Mix Columns Transformation */
        AesArMixColumns (au1ArStateMatrix2, au1ArStateMatrix1);
        AesArMixColumns (au1ArStateMatrix2 + AES_STATE_SECOND_COL_CONS,
                         au1ArStateMatrix1 + AES_STATE_SECOND_COL_CONS);
        AesArMixColumns (au1ArStateMatrix2 + AES_STATE_THIRD_COL_CONS,
                         au1ArStateMatrix1 + AES_STATE_THIRD_COL_CONS);
        AesArMixColumns (au1ArStateMatrix2 + AES_STATE_FOURTH_COL_CONS,
                         au1ArStateMatrix1 + AES_STATE_FOURTH_COL_CONS);

        /* Perform Add Round Key Operation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix1[u4ArStateVar] ^= pu1ArKeySchedule[u4ArStateVar];
        }

        pu1ArKeySchedule = pu1ArKeySchedule + AES_STATE_SIZE;
    }
    /* Final Round */
    /* Perform Sub Bytes Transformation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix1[u4ArStateVar] =
            gaArSf[au1ArStateMatrix1[u4ArStateVar]];
    }

    /* Perform Shift Rows Transformation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix2[u4ArStateVar] =
            au1ArStateMatrix1[gaArHf[u4ArStateVar]];
    }

    /* Perform Add Round Key Operation to get the output */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        pu1ArEncryptedText[u4ArStateVar] =
            au1ArStateMatrix2[u4ArStateVar] ^ pu1ArKeySchedule[u4ArStateVar];
    }
}

/*******************************************************************************
 * Function           : AesArDecrypt
 *
 * Description        : This function is used to decrypt the data using AES mode
 *                      of decryption
 *
 * Input(s)           :punArCryptoKey : Pointer to the structure identifying the
 *                     key type
 *                     pu1ArEncryptedTex:Pointer to the input data which needs
 *                     decryption
 *                     
 * Output(s)          : pu1ArPlainText : Pointer to the decrypted data.
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesArDecrypt (const unArCryptoKey * punArKeySchedule,
              const UINT1 *pu1ArEncryptedText, UINT1 *pu1ArPlainText)
{
    const UINT1        *pu1ArKeySchedule = NULL;
    UINT4               u4ArStateVar = CRY_INIT_VAL;
    UINT4               u4ArRoundVar = CRY_INIT_VAL;
    UINT1               au1ArStateMatrix1[AES_STATE_SIZE] = { CRY_INIT_VAL };
    UINT1               au1ArStateMatrix2[AES_STATE_SIZE] = { CRY_INIT_VAL };

    /* For Decryption, the key schedule is used in the reverse manner as used
     * while encryption
     */
    pu1ArKeySchedule = (punArKeySchedule->tArAes.au1ArSubkey +
                        (AES_STATE_SIZE * (UINT1)
                         punArKeySchedule->tArAes.u4ArRounds));

    /* Perform Add Round Key Operation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix1[u4ArStateVar] =
            pu1ArEncryptedText[u4ArStateVar] ^ pu1ArKeySchedule[u4ArStateVar];
    }
    /* Fetch the next key schedule */
    pu1ArKeySchedule = pu1ArKeySchedule - AES_STATE_SIZE;

    /* Perform the Decryption steps for one less than total number of rounds 
     * as per the Key Length
     */
    for (u4ArRoundVar = 0; u4ArRoundVar <
         (punArKeySchedule->tArAes.u4ArRounds - 1); u4ArRoundVar++)
    {
        /* Perform Inverse Shift rows transformation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix2[u4ArStateVar] =
                au1ArStateMatrix1[gaArHb[u4ArStateVar]];
        }

        /* Perform Inverse Sub Bytes Transformation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix2[u4ArStateVar] =
                gaArSb[au1ArStateMatrix2[u4ArStateVar]];
        }

        /* Perform Add Round Key Operation */
        for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
        {
            au1ArStateMatrix2[u4ArStateVar] ^= pu1ArKeySchedule[u4ArStateVar];
        }
        /* Fetch the next key schedule */
        pu1ArKeySchedule = pu1ArKeySchedule - AES_STATE_SIZE;

        /* Perform Inverse Mix Columns Transformation */
        AesArInvMixColumns (au1ArStateMatrix2, au1ArStateMatrix1);
        AesArInvMixColumns (au1ArStateMatrix2 + AES_STATE_SECOND_COL_CONS,
                            au1ArStateMatrix1 + AES_STATE_SECOND_COL_CONS);
        AesArInvMixColumns (au1ArStateMatrix2 + AES_STATE_THIRD_COL_CONS,
                            au1ArStateMatrix1 + AES_STATE_THIRD_COL_CONS);
        AesArInvMixColumns (au1ArStateMatrix2 + AES_STATE_FOURTH_COL_CONS,
                            au1ArStateMatrix1 + AES_STATE_FOURTH_COL_CONS);
    }

    /* Final Round */
    /* Perform Inverse Shift rows transformation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix2[u4ArStateVar] =
            au1ArStateMatrix1[gaArHb[u4ArStateVar]];
    }

    /* Perform Inverse Sub Bytes Transformation */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        au1ArStateMatrix2[u4ArStateVar] =
            gaArSb[au1ArStateMatrix2[u4ArStateVar]];
    }

    /* Perform Add Round Key Operation  and get the output */
    for (u4ArStateVar = 0; u4ArStateVar < AES_STATE_SIZE; u4ArStateVar++)
    {
        pu1ArPlainText[u4ArStateVar] =
            au1ArStateMatrix2[u4ArStateVar] ^ pu1ArKeySchedule[u4ArStateVar];
    }
}

/******************************************************************************
 * Function           : AesArXcbcMac96
 *
 * Description        : This API is invoked by the Modules which Requires a 
 *                      digest to be generated for a given Data through 
 *                      AES-XCBC-MAC-96 mode.
 *                     
 * Input(s)           : pu1ArUserKey :     Key used for producing message 
 *                              authentication code(Mac).
 *                      u4ArKeyLength:     Length of the InputKey in bytes
 *                      pu1ArBuffer  :     Incoming Data for which Mac is to be
 *                                              generated
 *                      i4ArBufferLen:     Size of the Data in Bytes.
 *                      pu1ArMac     :     Pointer in which generated message 
 *                                 authentication code will be stored.                
 * 
 * Output(s)          : pu1ArMac     :     Pointer to the generated Message 
 *                     Authentication Code.
 *
 * Returns            : AES_SUCCESS/AES_FAILURE                      
 *****************************************************************************/

PUBLIC UINT1
AesArXcbcMac96 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength,
                UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac)
{
    UINT1               u1ArReturnValue = 0;
    UINT1               au1ArBuffer[AES_128_KEY_BYTES] = { CRY_INIT_VAL };

    u1ArReturnValue = AesArXcbcMac128 (pu1ArUserKey, u4ArKeyLength,
                                       pu1ArBuffer, i4ArBufferLen, au1ArBuffer);
    if (u1ArReturnValue == AES_FAILURE)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\nAesArXcbcMac128 Failure\n");
        return AES_FAILURE;
    }
    /*The authenticator value is the leftmost 96 bits of the 
       128-bit value produced */
    CRY_MEMCPY (pu1ArMac, &au1ArBuffer[0], AES_XCBC_12_KEY_BYTES);
    return AES_SUCCESS;
}

/*******************************************************************************
 * Function           : AesArXcbcMac128
 *
 * Description        : This API is invoked by the Modules which Requires a 
 *             digest to be generated for a given Data through 
 *             AES-XCBC-MAC-128 mode
 *                     
 * Input(s)           :    pu1ArUserKey : Key used for producing 
 *                     message authentication code(Mac).
 *                    u4ArKeyLength: Length of the InputKey in bytes
 *                   pu1ArBuffer  : Incoming Data for which Mac is to be
 *                                              generated
 *                      i4ArBufferLen: Size of the Data in bytes.
 *                      pu1ArMac     : Pointer in which generated message 
 *                                    authentication code will be stored.                
 * 
 * Output(s)          : pu1ArMac     : Pointer to the generated 
 *                        Message Authentication Code.
 *
 * Returns            : AES_SUCCESS/AES_FAILURE                      
 *****************************************************************************/

PUBLIC UINT1
AesArXcbcMac128 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength,
                 UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac)
{
    unArCryptoKey       unArAesKey, unArAesKey1;
    UINT1               au1ArKey1[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1               au1ArKey2[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1               au1ArKey3[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1               au1Ebuf1[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1               au1Ebuf2[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1              *pu1ArEprev = NULL, *pu1ArEcurr = NULL;
    INT4                i4ArBlocks = 0;
    INT4                i4ArLeft = 0;
    INT4                i4ArCounter = 0, i4ArPosition = 0;
    UINT1              *pu1ArData = NULL;

    if ((pu1ArUserKey == NULL) || (pu1ArBuffer == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL Pointers passed to "
                  "AesArXcbcMac128\n");
        return AES_FAILURE;
    }
    /*If the key is not 128 bits , report failure */
    if (u4ArKeyLength != AES_128_KEY_BYTES)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length Passed in "
                  " AesArXcbcMac128\n");
        return AES_FAILURE;
    }

    i4ArBlocks = i4ArBufferLen / AES_128_KEY_BYTES;
    i4ArLeft = i4ArBufferLen % AES_128_KEY_BYTES;
    if (i4ArLeft == 0)
    {
        i4ArBlocks--;
    }

    MEMSET (&unArAesKey, 0, sizeof (unArCryptoKey));
    MEMSET (&unArAesKey1, 0, sizeof (unArCryptoKey));

    /*Derive 3 128-bit keys (au1ArKey1, au1ArKey2 and au1ArKey3) from 128-bit
       key K */
    /* au1ArKey1 = 0x01010101010101010101010101010101 
     * au1ArKey2 = 0x02020202020202020202020202020202
     * au1ArKey3 = 0x03030303030303030303030303030303*/
    MEMSET (au1ArKey1, 0x01, AES_128_KEY_BYTES);
    MEMSET (au1ArKey2, 0x02, AES_128_KEY_BYTES);
    MEMSET (au1ArKey3, 0x03, AES_128_KEY_BYTES);

    /*create the Key Schedule for Encryption. */
    AesArSetEncryptKey (pu1ArUserKey,
                        (UINT2) (u4ArKeyLength * AES_BLOCKS_IN_128_KEY),
                        &unArAesKey);
    AesArEncrypt (&unArAesKey, &au1ArKey1[0], &au1ArKey1[0]);
    AesArEncrypt (&unArAesKey, &au1ArKey2[0], &au1ArKey2[0]);
    AesArEncrypt (&unArAesKey, &au1ArKey3[0], &au1ArKey3[0]);
    AesArSetEncryptKey (&au1ArKey1[0],
                        AES_128_KEY_BYTES * AES_BLOCKS_IN_128_KEY,
                        &unArAesKey1);

    pu1ArEprev = &au1Ebuf1[0];
    pu1ArEcurr = &au1Ebuf2[0];
    MEMSET (pu1ArEprev, 0x00, AES_128_KEY_BYTES);

    if (i4ArBufferLen == 0)        /*Message is the empty string */
    {
        pu1ArEcurr[0] = (UINT1) AES_128_IN_HEX ^ au1ArKey3[0];
        for (i4ArPosition = 1; i4ArPosition < AES_128_KEY_BYTES; i4ArPosition++)
        {
            pu1ArEcurr[i4ArPosition] = au1ArKey3[i4ArPosition];
        }

        AesArEncrypt (&unArAesKey1, au1Ebuf2, au1Ebuf1);

    }
    else
    {
        pu1ArData = pu1ArBuffer;
        /*XOR M[i] with E[i-1], then encrypt the result with Key unArAesKey1,
           yielding E[i] */
        for (i4ArCounter = 0; i4ArCounter < i4ArBlocks; i4ArCounter++)
        {
            for (i4ArPosition = 0; i4ArPosition < AES_128_KEY_BYTES;
                 i4ArPosition++)
            {
                pu1ArEcurr[i4ArPosition] =
                    pu1ArEprev[i4ArPosition] ^ pu1ArData[i4ArPosition];
            }
            pu1ArData += AES_128_KEY_BYTES;
            AesArEncrypt (&unArAesKey1, pu1ArEcurr, pu1ArEprev);
        }

        /*XOR pu1ArData with pu1ArEprev and Key au1ArKey2, then 
         * encrypt the result with Key unArAesKey1, yielding pu1ArEcur*/
        if (i4ArLeft == 0)
        {
            for (i4ArPosition = 0; i4ArPosition < AES_128_KEY_BYTES;
                 i4ArPosition++)
            {
                pu1ArEcurr[i4ArPosition] =
                    pu1ArEprev[i4ArPosition] ^
                    pu1ArData[i4ArPosition] ^ au1ArKey2[i4ArPosition];
            }
            AesArEncrypt (&unArAesKey1, pu1ArEcurr, pu1ArEprev);
        }
        else
        {
            /*Pad pu1ArData with a single "1" bit, followed by the number of
               "0" bits (possibly none) required to increase pu1ArDatas
               blocksize to 128 bits. */

            for (i4ArPosition = 0; i4ArPosition < i4ArLeft; i4ArPosition++)
            {
                pu1ArEcurr[i4ArPosition] = pu1ArData[i4ArPosition];
            }
            pu1ArEcurr[i4ArLeft] = (UINT1) AES_128_IN_HEX;
            for (i4ArPosition = i4ArLeft + 1;
                 i4ArPosition < AES_128_KEY_BYTES; i4ArPosition++)
            {
                pu1ArEcurr[i4ArPosition] = (UINT1) 0x00;
            }

            /*XOR pu1ArData with pu1ArEprev and Key au1ArKey3,then encrypt result
               with Key unArAesKey1, yielding pu1ArEcur */

            for (i4ArPosition = 0; i4ArPosition < AES_128_KEY_BYTES;
                 i4ArPosition++)
            {
                pu1ArEcurr[i4ArPosition] =
                    pu1ArEprev[i4ArPosition] ^
                    pu1ArEcurr[i4ArPosition] ^ au1ArKey3[i4ArPosition];
            }
            AesArEncrypt (&unArAesKey1, pu1ArEcurr, pu1ArEprev);
        }
    }
    CRY_MEMCPY (pu1ArMac, pu1ArEprev, AES_128_KEY_BYTES);
    return AES_SUCCESS;
}

/******************************************************************************
 * Function           : AesArXcbcPrf128
 *
 * Description        : This API is invoked by the Modules which Requires a 
 *                      digest to be generated for a given Data through 
 *                      AesXcbcPrf128 mode
 *                     
 * Input(s)           : pu1ArUserKey : Key used for producing message 
 *                     authentication code(Mac).
 *             u4ArKeyLength: Length of the InputKey in bytes
 *             pu1ArBuffer  :Incoming Data for which Mac is to be
 *                                          generated
 *                      i4ArBufferLen : Size of the Data in bytes
 *                      pu1ArMac      : Pointer in which generated 
 *                                     message digest will be stored.                
 * 
 * Output(s)          : pu1ArMac     : Pointer to the generated Message digest
 *
 * Returns            : AES_SUCCESS/AES_FAILURE                      
 *****************************************************************************/

PUBLIC UINT1
AesArXcbcPrf128 (UINT1 *pu1ArUserKey, UINT4 u4ArKeyLength,
                 UINT1 *pu1ArBuffer, INT4 i4ArBufferLen, UINT1 *pu1ArMac)
{
    UINT1               au1newkey[AES_128_KEY_BYTES] = { CRY_INIT_VAL };
    UINT1               u1ArReturnValue = 0;

    if ((pu1ArUserKey == NULL) || (pu1ArBuffer == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL Pointers passed to "
                  "AesArXcbcPrf128\n");
        return AES_FAILURE;
    }

    /*If the key has fewer than 128 bits, lengthen it to exactly 128
     * bits by padding it on the right with zero bits.*/
    if ((INT4) u4ArKeyLength < AES_128_KEY_BYTES)
    {
        MEMSET (au1newkey, 0x00, AES_128_KEY_BYTES);
        CRY_MEMCPY (au1newkey, pu1ArUserKey, u4ArKeyLength);
        u1ArReturnValue = AesArXcbcMac128 (au1newkey, AES_128_KEY_BYTES,
                                           pu1ArBuffer, i4ArBufferLen,
                                           pu1ArMac);
        if (u1ArReturnValue == AES_FAILURE)
        {
            UTIL_TRC (ALL_FAILURE_TRC, "\nAesArXcbcMac128 Failure\n");
            return AES_FAILURE;
        }
    }
    /*If the key is exactly 128 bits long, use it as-is. */
    else if ((INT4) u4ArKeyLength == AES_128_KEY_BYTES)
    {
        u1ArReturnValue = AesArXcbcMac128 (pu1ArUserKey, AES_128_KEY_BYTES,
                                           pu1ArBuffer, i4ArBufferLen,
                                           pu1ArMac);
        if (u1ArReturnValue == AES_FAILURE)
        {
            UTIL_TRC (ALL_FAILURE_TRC, "\nAesArXcbcMac128 Failure\n");
            return AES_FAILURE;
        }
    }
    /*If the key is longer than 128 bits , report failure */
    else
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n Incorrect Key Length Passed"
                  "in AesArXcbcPrf128\n");
        return AES_FAILURE;

    }

    return AES_SUCCESS;
}

/**************************************************************************
 *Function           : AesArCtrMode
 *
 *Description        : This function is used to encrypt and decrypt the data 
 *                     using 
 *                     AES-CTR mode.    
 *Input(s)           : pu1ArInBuf        : pointer to data that is to be 
 *                                          encrypted/decrypted 
 *                     pu1ArOutBuf       : pointer to data that is excrypted
 *                                         /decrypted
 *                     ulArInBufTextLen  : value to store the plain text length
 *                     unArCryptoKey     : Pointer to the structure identifying
 *                     the key type
 *                     pu1ArCounterBlock : array containing the counter block
 *                     Couter block (16): (num) indicates number of bytes
 *                MSB   _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  LSB
 *                    |                                |
 *                    |nonce(4)|    IV(8)      | ctr(4)|
 *                    | _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _| 
 *                     
 *                     pu1ArEncryptBuf   : array containing the encrypted 
 *                                         counter block with AES
 *                     pu4ArNum          : Num to track the block size                    
 *                             
 *Output(s)          : AES-CTR mode encypted/decrypted data in pu1ArOutBuf
 *             
 *Returns            : AES_FAILURE/AES_SUCCESS
 *****************************************************************************/

PUBLIC INT1
AesArCtrMode (CONST UINT1 *pu1ArInBuf, UINT1 *pu1ArOutBuf,
              FS_ULONG ulArInBufTextLen, CONST unArCryptoKey * punArKeySchedule,
              UINT1 *pu1ArCounterBlock, UINT1 *pu1ArEncryptBuf, UINT4 *pu4ArNum)
{

    UINT4               u4ArTempNum = 0;
    FS_ULONG            ulArTempLen = ulArInBufTextLen;

    if ((pu1ArInBuf == NULL) || (pu1ArOutBuf == NULL) ||
        (punArKeySchedule == NULL) || (pu1ArCounterBlock == NULL) ||
        (pu1ArEncryptBuf == NULL) || (pu4ArNum == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n NULL passed for pointer in"
                  ":AesArCtrMode \n");
        return AES_FAILURE;
    }

    if (ulArInBufTextLen <= 0)
    {
        UTIL_TRC (ALL_FAILURE_TRC, "\n Wrong value passed for ulArInBufTextLen"
                  " in AesArCtrMode\n");
        return AES_FAILURE;
    }

    u4ArTempNum = *pu4ArNum;

    while (ulArTempLen--)
    {
        if (u4ArTempNum == 0)
        {
            /* performing AES encryption on counter block */
            AesArEncrypt (punArKeySchedule, pu1ArCounterBlock, pu1ArEncryptBuf);
            /*incrementing the counter for the next block */
            AesArIncrCounter (pu1ArCounterBlock);
        }
        /*AES CTR mode encryption with plain text and AES 
         *      Encrypted counter block*/
        *(pu1ArOutBuf++) = *(pu1ArInBuf++) ^ pu1ArEncryptBuf[u4ArTempNum];
        /* tracking the 128 bit block size */
        u4ArTempNum = (u4ArTempNum + 1) % AES_BLOCK_SIZE;
    }

    *pu4ArNum = u4ArTempNum;
    return AES_SUCCESS;
}

/**************************************************************************
 *Function           : AesArCfb128
 *
 *Description        : This function is used to encrypt and decrypt the data
 *                     using AES-CFB mode.
 *
 *Input(s)           : pu1InText         : Pointer to pu1InText that is to be
 *                                         encrypted/decrypted.
 *                     pu1OutText        : Pointer to data that is encrypted
 *                                         /decrypted chiper output.
 *                     u4InLen           : Length of the Input data in bytes
 *                     pkey              : Pointer to pkey that contains the 
 *                                         the key. 
 *                     pu1Ivec           : Pointer to the data that contains 
 *                                         the initialization vector
 *                     pi4Num            : Pointer to the pi4Num used to 
 *                                         track the block size.
 *                     i4Enc             : Value to specify the mode of 
 *                                         operation like encryption/decryption.
 *                                         for encryption value should be a not 
 *                                         null and for decryption value should
 *                                         be a null
 * 
 *Output(s)          : AES-CFB mode encypted/decrypted data in pu1OutText
 *
 *Returns            : AES_FAILURE/AES_SUCCESS
 *****************************************************************************/

PUBLIC INT4
AesArCfb128 (CONST UINT1 *pu1InText, UINT1 *pu1OutText,
             CONST FS_ULONG u4InLen, CONST unArCryptoKey * pKey,
             UINT1 *pu1Ivec, INT4 *pi4Num, CONST INT4 i4Enc)
{

    UINT4               u4Count = 0;
    FS_ULONG            u4InLenTemp = u4InLen;
    UINT1               u1Tempchar = 0;

    if ((pu1InText == NULL) || (pu1OutText == NULL) ||
        (pKey == NULL) || (pu1Ivec == NULL) || (pi4Num == NULL))
    {
        UTIL_TRC (ALL_FAILURE_TRC, "NULL passed for pointer in:AesArCfb128 \n");
        return AES_FAILURE;
    }

    u4Count = (UINT4) *pi4Num;

    if (i4Enc)
    {
        while (u4InLenTemp--)
        {
            if (u4Count == 0)
            {
                /* performing AES encryption on initialization vector 
                 * and key 
                 */
                AesArEncrypt (pKey, pu1Ivec, pu1Ivec);
            }

            /*AES CFB mode XOR with plain text and AES Encrypted data */
            pu1Ivec[u4Count] = *(pu1OutText++) =
                *(pu1InText++) ^ pu1Ivec[u4Count];
            /* tracking the 128 bit block size */
            u4Count = (u4Count + 1) % AES_BLOCK_SIZE;
        }
    }
    else
    {
        while (u4InLenTemp--)
        {
            if (u4Count == 0)
            {
                /* performing AES decryption on initialization vector
                 * and key
                 */
                AesArEncrypt (pKey, pu1Ivec, pu1Ivec);
            }
            u1Tempchar = *(pu1InText);

            /*AES CFB mode XOR with plain text and AES Encrypted data */
            *(pu1OutText++) = *(pu1InText++) ^ pu1Ivec[u4Count];
            pu1Ivec[u4Count] = u1Tempchar;
            /* tracking the 128 bit block size */
            u4Count = (u4Count + 1) % AES_BLOCK_SIZE;
        }
    }

    *pi4Num = (INT4) u4Count;
    return AES_SUCCESS;
}
