/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: aesarfn.c,v 1.1 2015/04/28 12:53:02 siva Exp $
 *
 * Description: This file contains the source code for AES module
 *********************************************************************/
#include "aesarinc.h"
#include "aesarglb.h"

/*******************************************************************************
 * Function           : AesArScheduleCore 
 *
 * Description        :This function is used to perform the initial operations
 *                     (Rotate, Substitute and xor with round constant) for 
 *                     preparing the key schedule 
 *
 * Input(s)           : pu1ArInputKey : Input key
 *                      u4ArRoundVal : Value of the round. This is required to
 *                      apply the round constant.
 *                     
 * Output(s)          : pu1ArOutputKey : Output key
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesArScheduleCore (const UINT1 *pu1ArInputKey, UINT1 *pu1ArOutputKey,
                   UINT4 u4ArRoundVal)
{
    pu1ArOutputKey[CRY_ARRAY_INDEX_ZERO] =
        gaArSf[pu1ArInputKey[CRY_ARRAY_INDEX_ONE]] ^ gaArRc[u4ArRoundVal];
    pu1ArOutputKey[CRY_ARRAY_INDEX_ONE] =
        gaArSf[pu1ArInputKey[CRY_ARRAY_INDEX_TWO]];
    pu1ArOutputKey[CRY_ARRAY_INDEX_TWO] =
        gaArSf[pu1ArInputKey[CRY_ARRAY_INDEX_THREE]];
    pu1ArOutputKey[CRY_ARRAY_INDEX_THREE] =
        gaArSf[pu1ArInputKey[CRY_ARRAY_INDEX_ZERO]];
}

/*******************************************************************************
 * Function           : AesArMixColumns 
 *
 * Description        : This function performs the operation of mixing columns
 *                      while encryption of data.
 *
 * Input(s)           : pu1ArInputColumn : Input data on which mix columns
 *                      operation is desired
 *                     
 * Output(s)          : pu1ArMixedOutput : Mixed column data output
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesArMixColumns (const UINT1 *pu1ArInputColumn, UINT1 *pu1ArMixedOutput)
{
    UINT4               u4ArVal0 = CRY_INIT_VAL;
    UINT4               u4ArVal1 = CRY_INIT_VAL;
    UINT4               u4ArVal2 = CRY_INIT_VAL;
    UINT4               u4ArVal3 = CRY_INIT_VAL;
    /* This function performs a matrix multiplication of the input column with
     * the matrix denoted by the polynomial :
     * {03}x^3 + {01}x^2 + {01}x + {02}
     */
    u4ArVal0 = pu1ArInputColumn[CRY_ARRAY_INDEX_ZERO];
    u4ArVal1 = pu1ArInputColumn[CRY_ARRAY_INDEX_ONE];
    u4ArVal2 = pu1ArInputColumn[CRY_ARRAY_INDEX_TWO];
    u4ArVal3 = pu1ArInputColumn[CRY_ARRAY_INDEX_THREE];

    pu1ArMixedOutput[CRY_ARRAY_INDEX_ZERO] =
        (UINT1) (gaArM_2[u4ArVal0] ^ gaArM_3[u4ArVal1] ^ u4ArVal2 ^ u4ArVal3);
    pu1ArMixedOutput[CRY_ARRAY_INDEX_ONE] =
        (UINT1) (u4ArVal0 ^ gaArM_2[u4ArVal1] ^ gaArM_3[u4ArVal2] ^ u4ArVal3);
    pu1ArMixedOutput[CRY_ARRAY_INDEX_TWO] =
        (UINT1) (u4ArVal0 ^ u4ArVal1 ^ gaArM_2[u4ArVal2] ^ gaArM_3[u4ArVal3]);
    pu1ArMixedOutput[CRY_ARRAY_INDEX_THREE] =
        (UINT1) (gaArM_3[u4ArVal0] ^ u4ArVal1 ^ u4ArVal2 ^ gaArM_2[u4ArVal3]);
}

/*******************************************************************************
 * Function           : AesArInvMixColumns 
 *
 * Description        : This function performs the operation of mixing columns
 *                      while decryption of data.
 *
 * Input(s)           : pu1ArInputColumn : Input data on which mix columns
 *                      operation is desired
 *                     
 * Output(s)          : pu1ArInvMixedOutput : Mixed column data output
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesArInvMixColumns (const UINT1 *pu1ArInputColumn, UINT1 *pu1ArInvMixedOutput)
{
    UINT4               u4ArVal0 = CRY_INIT_VAL;
    UINT4               u4ArVal1 = CRY_INIT_VAL;
    UINT4               u4ArVal2 = CRY_INIT_VAL;
    UINT4               u4ArVal3 = CRY_INIT_VAL;

    /* This function performs a matrix multiplication of the input column with
     * the matrix denoted by the polynomial :
     * {0b}x^3 + {0d}x^2 + {09}x + {0e}
     */
    u4ArVal0 = pu1ArInputColumn[CRY_ARRAY_INDEX_ZERO];
    u4ArVal1 = pu1ArInputColumn[CRY_ARRAY_INDEX_ONE];
    u4ArVal2 = pu1ArInputColumn[CRY_ARRAY_INDEX_TWO];
    u4ArVal3 = pu1ArInputColumn[CRY_ARRAY_INDEX_THREE];

    pu1ArInvMixedOutput[CRY_ARRAY_INDEX_ZERO] =
        gaArM_14[u4ArVal0] ^ gaArM_11[u4ArVal1] ^ gaArM_13[u4ArVal2] ^
        gaArM_9[u4ArVal3];

    pu1ArInvMixedOutput[CRY_ARRAY_INDEX_ONE] =
        gaArM_9[u4ArVal0] ^ gaArM_14[u4ArVal1] ^ gaArM_11[u4ArVal2] ^
        gaArM_13[u4ArVal3];

    pu1ArInvMixedOutput[CRY_ARRAY_INDEX_TWO] =
        gaArM_13[u4ArVal0] ^ gaArM_9[u4ArVal1] ^ gaArM_14[u4ArVal2] ^
        gaArM_11[u4ArVal3];

    pu1ArInvMixedOutput[CRY_ARRAY_INDEX_THREE] =
        gaArM_11[u4ArVal0] ^ gaArM_13[u4ArVal1] ^ gaArM_9[u4ArVal2] ^
        gaArM_14[u4ArVal3];
}

/*******************************************************************************
 * Function           : AesAr128Setup 
 *
 * Description        : This function is used for key expansion for key length
 *                      of 128 bits. This generates a key schedule for the 
 *                      input key.
 *
 * Input(s)           : pu1ArCryptoKey : Pointer to the key for which key schedule
 *                      is to be prepared.
 *                     
 * Output(s)          : punArKeySchedule : Pointer to the structure identifying
 *                      the key type
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesAr128Setup (unArCryptoKey * punArKeySchedule, const UINT1 *pu1ArCryptoKey)
{
    UINT1              *pu1ArInterKeySc1 = NULL;
    UINT1              *pu1ArInterKeySc2 = NULL;
    UINT1              *pu1ArInterKeySc3 = NULL;
    UINT4               u4ArRoundVal = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    UINT1               au1ArKeyColumn[AES_COLUMNS] = { CRY_INIT_VAL };

    /* A total of 10 rounds are required for AES 128bit Encryption */
    punArKeySchedule->tArAes.u4ArRounds = AES_128_ENCR_ROUNDS;
    pu1ArInterKeySc1 = punArKeySchedule->tArAes.au1ArSubkey;

    /* Copy the key */
    CRY_MEMCPY (pu1ArInterKeySc1, pu1ArCryptoKey, AES_128_KEY_BYTES);

    /* Initialize the intermediate keys */
    pu1ArInterKeySc2 = pu1ArInterKeySc1;
    pu1ArInterKeySc3 = pu1ArInterKeySc1 + AES_KEY_OFFSET_12;
    pu1ArInterKeySc1 = pu1ArInterKeySc1 + AES_KEY_OFFSET_16;

    /* Initialise the value of round constant. */
    u4ArRoundVal = 1;

    /* Prepare a 44 word Key Expansion routine */
    while (pu1ArInterKeySc1 - punArKeySchedule->tArAes.au1ArSubkey <
           AES_128_KEY_EXPANSION_SIZE)
    {
        /* Perform Rotation, Substitution and XOR operation on the Key */
        AesArScheduleCore (pu1ArInterKeySc3, au1ArKeyColumn, u4ArRoundVal);
        AES_INC_VAL (u4ArRoundVal);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ZERO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ONE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_TWO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_THREE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;

        for (u4ArLoopVar = 0; u4ArLoopVar < (AES_COLUMNS - 1); u4ArLoopVar++)
        {
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;
        }
    }
}

/*******************************************************************************
 * Function           : AesAr192Setup 
 *
 * Description        : This function is used for key expansion for key length
 *                      of 192 bits. This generates a key schedule for the 
 *                      input key
 *
 * Input(s)           : pu1ArCryptoKey : Pointer to the key for which key schedule
 *                      is to be prepared
 *                     
 * Output(s)          : punArKeySchedule : Pointer to the structure identifying
 *                      the key type
 *
 * Returns            : None 
 ******************************************************************************/
PUBLIC VOID
AesAr192Setup (unArCryptoKey * punArKeySchedule, const UINT1 *pu1ArCryptoKey)
{
    UINT4               u4ArRoundVal = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    UINT1              *pu1ArInterKeySc1 = NULL;
    UINT1              *pu1ArInterKeySc2 = NULL;
    UINT1              *pu1ArInterKeySc3 = NULL;
    UINT1               au1ArKeyColumn[AES_COLUMNS] = { CRY_INIT_VAL };

    /* A total of 12 rounds are required for AES 128bit Encryption */
    punArKeySchedule->tArAes.u4ArRounds = AES_192_ENCR_ROUNDS;
    pu1ArInterKeySc1 = punArKeySchedule->tArAes.au1ArSubkey;

    /* Copy the key */
    CRY_MEMCPY (pu1ArInterKeySc1, pu1ArCryptoKey, AES_192_KEY_BYTES);

    /* Initialize the intermediate keys */
    pu1ArInterKeySc2 = pu1ArInterKeySc1;
    pu1ArInterKeySc3 = pu1ArInterKeySc1 + AES_KEY_OFFSET_20;
    pu1ArInterKeySc1 = pu1ArInterKeySc1 + AES_KEY_OFFSET_24;

    /* Initialise the value of round constant. */
    u4ArRoundVal = 1;
    /* Prepare a 52 word Key Expansion routine */
    for (;;)
    {
        /* Perform Rotation, Substitution and XOR operation on the Key */
        AesArScheduleCore (pu1ArInterKeySc3, au1ArKeyColumn, u4ArRoundVal);
        AES_INC_VAL (u4ArRoundVal);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ZERO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ONE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_TWO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_THREE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;

        for (u4ArLoopVar = 0; u4ArLoopVar < (AES_COLUMNS - 1); u4ArLoopVar++)
        {
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;
        }

        if ((pu1ArInterKeySc1 - punArKeySchedule->tArAes.au1ArSubkey) ==
            AES_192_KEY_EXPANSION_SIZE)
        {
            break;
        }

        for (u4ArLoopVar = 0; u4ArLoopVar < ((AES_COLUMNS - 1) - 1);
             u4ArLoopVar++)
        {
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);
            pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;
        }
    }
}

/*******************************************************************************
 * Function           : AesAr256Setup 
 *
 * Description        : This function is used for key expansion for key length
 *                      of 256 bits. This generates a key schedule for the 
 *                      input key.
 *
 * Input(s)           : pu1ArCryptoKey : Pointer to the key for which key schedule
 *                      is to be prepared.
 *                     
 * Output(s)          : punArKeySchedule : Pointer to the structure identifying
 *                      the key type
 *
 * Returns            : NONE
 ******************************************************************************/
PUBLIC VOID
AesAr256Setup (unArCryptoKey * punArKeySchedule, const UINT1 *pu1ArCryptoKey)
{
    UINT1              *pu1ArInterKeySc1 = NULL;
    UINT1              *pu1ArInterKeySc2 = NULL;
    UINT1              *pu1ArInterKeySc3 = NULL;
    UINT4               u4ArRoundVal = CRY_INIT_VAL;
    UINT4               u4ArLoopVar = CRY_INIT_VAL;
    UINT1               au1ArKeyColumn[AES_COLUMNS] = { CRY_INIT_VAL };

    /* A total of 14 rounds are required for AES 128bit Encryption */
    punArKeySchedule->tArAes.u4ArRounds = AES_256_ENCR_ROUNDS;
    pu1ArInterKeySc1 = punArKeySchedule->tArAes.au1ArSubkey;

    /* Copy the key */
    CRY_MEMCPY (pu1ArInterKeySc1, pu1ArCryptoKey, AES_256_KEY_BYTES);
    pu1ArInterKeySc2 = pu1ArInterKeySc1;
    pu1ArInterKeySc3 = pu1ArInterKeySc1 + AES_KEY_OFFSET_28;
    pu1ArInterKeySc1 = pu1ArInterKeySc1 + AES_KEY_OFFSET_32;

    /* Initialise the value of round constant */
    u4ArRoundVal = 1;

    /* Prepare a 60 word Key Expansion routine */
    for (;;)
    {
        /* Perform Rotation, Substitution and XOR operation on the Key */
        AesArScheduleCore (pu1ArInterKeySc3, au1ArKeyColumn, u4ArRoundVal);
        u4ArRoundVal++;

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ZERO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_ONE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_TWO] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            au1ArKeyColumn[CRY_ARRAY_INDEX_THREE] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;

        for (u4ArLoopVar = 0; u4ArLoopVar < (AES_COLUMNS - 1); u4ArLoopVar++)
        {
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;
        }

        if ((pu1ArInterKeySc1 - punArKeySchedule->tArAes.au1ArSubkey) ==
            AES_256_KEY_EXPANSION_SIZE)
        {
            break;
        }

        AES_INC_VAL (*pu1ArInterKeySc1) =
            gaArSf[pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO]] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            gaArSf[pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE]] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        AES_INC_VAL (*pu1ArInterKeySc1) =
            gaArSf[pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO]] ^
            AES_INC_VAL (*pu1ArInterKeySc2);
        AES_INC_VAL (*pu1ArInterKeySc1) =
            gaArSf[pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE]] ^
            AES_INC_VAL (*pu1ArInterKeySc2);

        pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;

        for (u4ArLoopVar = 0; u4ArLoopVar < (AES_COLUMNS - 1); u4ArLoopVar++)
        {
            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ZERO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_ONE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_TWO] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            AES_INC_VAL (*pu1ArInterKeySc1) =
                pu1ArInterKeySc3[CRY_ARRAY_INDEX_THREE] ^
                AES_INC_VAL (*pu1ArInterKeySc2);

            pu1ArInterKeySc3 = pu1ArInterKeySc3 + AES_KEY_OFFSET_4;
        }
    }
}

/*******************************************************************************
 * Function           : AesArIncrCounter
 *
 * Description        : This function is used for incrementing the counter value 
 *                      and handle the overflow
 *
 * Input(s)           : pu1ArCounter : pointer to counter 
 *                     
 * Output(s)          : incremented counter value
 *
 * Returns            : AES_SUCCESS/AES_FAILURE
 ******************************************************************************/
INT1
AesArIncrCounter (UINT1 *pu1ArCounter)
{
#ifdef CRYPTO_DEBUG_LOG
    CRYPTO_DEBUG_PRINT ("\n Entry : AesArCtrInc\n");
#endif
    UINT4               u4ArTempCounter = 0;
    if (pu1ArCounter == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n NULL passed for Counter"
                            "in increment in AesArCtrInc\n");
#endif
        return AES_FAILURE;
    }

    /* Grab bottom dword of counter and increment */
    PTR_FETCH4 (u4ArTempCounter, pu1ArCounter + 12);
    u4ArTempCounter++;
    u4ArTempCounter &= AES_MAX_UINT4;
    PTR_ASSIGN4 (pu1ArCounter + 12, u4ArTempCounter);

    /* if no overflow, return */
    if (u4ArTempCounter)
    {
        return AES_SUCCESS;
    }
    /* Grab 1st dword of counter and increment */
    PTR_FETCH4 (u4ArTempCounter, pu1ArCounter + 8);
    u4ArTempCounter++;
    u4ArTempCounter &= AES_MAX_UINT4;
    PTR_ASSIGN4 (pu1ArCounter + 8, u4ArTempCounter);

    /* if no overflow, return */
    if (u4ArTempCounter)
    {
        return AES_SUCCESS;
    }
    /* Grab 2nd dword of counter and increment */
    PTR_FETCH4 (u4ArTempCounter, pu1ArCounter + 4);
    u4ArTempCounter++;
    u4ArTempCounter &= AES_MAX_UINT4;
    PTR_ASSIGN4 (pu1ArCounter + 4, u4ArTempCounter);

    /* if no overflow, return */
    if (u4ArTempCounter)
    {
        return AES_SUCCESS;
    }
    /* Grab top dword of counter and increment */
    PTR_FETCH4 (u4ArTempCounter, pu1ArCounter + 0);
    u4ArTempCounter++;
    u4ArTempCounter &= AES_MAX_UINT4;
    PTR_ASSIGN4 (pu1ArCounter + 0, u4ArTempCounter);

#ifdef CRYPTO_DEBUG_LOG
    CRYPTO_DEBUG_PRINT ("\n Exit : AesArCtrInc\n");
#endif
    return AES_SUCCESS;
}
