/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
* $Id: aes.h,v 1.10 2017/04/21 13:51:16 siva Exp $ 
 *
* Description:This files contains the macros and definitions used by the
*             AES algorithm.
 *
*******************************************************************/

#include "osxstd.h"
#include "utlmacro.h"
   

#define   AES_OK  (0)
#define   AES_NOT_OK (-1)
#define   AES_IV_SIZE  16
    
/* Functions exported by AES Algo */

VOID AesSetKey(UINT4 au4InKey[], UINT1 u1InKeyLen,UINT4 au4EncryptKey[64],
        UINT4 au4DecryptKey[64]);

INT1
AesEncrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
        UINT4 au4EncryptKey[64]);


INT1
AesDecrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
     UINT4 au4EncryptKey[64],UINT4 au4DecryptKey[64]);


INT1
AesCbcEncrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen, 
        UINT4 au4EncryptKey[64],UINT1 *pu1InitVect);


INT1
AesCbcDecrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
               UINT4 au4EncryptKey[64], UINT4 au4DecryptKey[64],
             UINT1 *pu1InitVect);
