/********************************************************************
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
*  $Id: aes.c,v 1.6 2017/04/21 13:51:19 siva Exp $
 *
*  Description: Code for AES Encryption.
 *
*******************************************************************/


#include "aes.h"

/* Basic macros for speeding up generic operations  */
#define ROTR(x,n)   (((x) >> ((INT4)(n))) | ((x) << (32 - (INT4)(n))))
#define ROTL(x,n)   (((x) << ((INT4)(n))) | ((x) >> (32 - (INT4)(n))))

/* Extract BYTE from a 32 bit quantity (little endian notation)     */
#define BYTE(x,n)   ((UINT1)((x) >> (8 * n)))

/* Look Up Tables for AES */
UINT1               au1PowTable[256];
UINT1               au1LogTable[256];
UINT1               au1SBox[256];
UINT1               au1InvSBox[256];
UINT4               au4RcoTable[10];
UINT4               au4FtTable[4][256];
UINT4               au4ItTable[4][256];
UINT4               au4FLTable[4][256];
UINT4               au4ILTable[4][256];
UINT4               u4TabGenFlag = 0;

#define FF_MULT(a,b)    (a && b ? au1PowTable[(au1LogTable[a] + au1LogTable[b]) % 255] : 0)

#define F_ROUND(bo, bi, n, k)                          \
    bo[n] =  au4FtTable[0][BYTE(bi[n],0)] ^             \
             au4FtTable[1][BYTE(bi[(n + 1) & 3],1)] ^   \
             au4FtTable[2][BYTE(bi[(n + 2) & 3],2)] ^   \
             au4FtTable[3][BYTE(bi[(n + 3) & 3],3)] ^ *(k + n)

#define I_ROUND(bo, bi, n, k)                          \
    bo[n] =  au4ItTable[0][BYTE(bi[n],0)] ^             \
             au4ItTable[1][BYTE(bi[(n + 3) & 3],1)] ^   \
             au4ItTable[2][BYTE(bi[(n + 2) & 3],2)] ^   \
             au4ItTable[3][BYTE(bi[(n + 1) & 3],3)] ^ *(k + n)

#define LS_BOX(x)                \
    ( au4FLTable[0][BYTE(x, 0)] ^    \
      au4FLTable[1][BYTE(x, 1)] ^    \
      au4FLTable[2][BYTE(x, 2)] ^    \
      au4FLTable[3][BYTE(x, 3)] )

#define F_L_ROUND(bo, bi, n, k)                          \
    bo[n] =  au4FLTable[0][BYTE(bi[n],0)] ^             \
             au4FLTable[1][BYTE(bi[(n + 1) & 3],1)] ^   \
             au4FLTable[2][BYTE(bi[(n + 2) & 3],2)] ^   \
             au4FLTable[3][BYTE(bi[(n + 3) & 3],3)] ^ *(k + n)

#define I_L_ROUND(bo, bi, n, k)                          \
    bo[n] =  au4ILTable[0][BYTE(bi[n],0)] ^             \
             au4ILTable[1][BYTE(bi[(n + 3) & 3],1)] ^   \
             au4ILTable[2][BYTE(bi[(n + 2) & 3],2)] ^   \
             au4ILTable[3][BYTE(bi[(n + 1) & 3],3)] ^ *(k + n)

#define STAR_X(x) (((x) & 0x7f7f7f7f) << 1) ^ ((((x) & 0x80808080) >> 7) * 0x1b)

#define INVERSE_MIX_COL(y,x)       \
    u   = STAR_X(x);        \
    v   = STAR_X(u);        \
    w   = STAR_X(v);        \
    t   = w ^ (x);          \
   (y)  = u ^ v ^ w;        \
   (y) ^= ROTR(u ^ t,  8) ^ \
          ROTR(v ^ t, 16) ^ \
          ROTR(t,24)

/* initialise the key schedule from the user supplied key   */
#define INIT_4_KS_ARRAY(i)                                \
{                                               \
    t = ROTR(t, 8);                         \
    t = LS_BOX(t) ^ au4RcoTable[i];             \
        t ^= au4EncryptKey[4 * i];            \
    au4EncryptKey[4 * i + 4] = t;        \
        t ^= au4EncryptKey[4 * i + 1];        \
    au4EncryptKey[4 * i + 5] = t;        \
        t ^= au4EncryptKey[4 * i + 2];        \
    au4EncryptKey[4 * i + 6] = t;        \
        t ^= au4EncryptKey[4 * i + 3];        \
    au4EncryptKey[4 * i + 7] = t;        \
}

#define INIT_6_KS_ARRAY(i)                                    \
{   t = LS_BOX(ROTR(t,  8)) ^ au4RcoTable[i];           \
    t ^= au4EncryptKey[6 * i];     au4EncryptKey[6 * i + 6] = t;    \
    t ^= au4EncryptKey[6 * i + 1]; au4EncryptKey[6 * i + 7] = t;    \
    t ^= au4EncryptKey[6 * i + 2]; au4EncryptKey[6 * i + 8] = t;    \
    t ^= au4EncryptKey[6 * i + 3]; au4EncryptKey[6 * i + 9] = t;    \
    t ^= au4EncryptKey[6 * i + 4]; au4EncryptKey[6 * i + 10] = t;   \
    t ^= au4EncryptKey[6 * i + 5]; au4EncryptKey[6 * i + 11] = t;   \
}

#define INIT_8_KS_ARRAY(i)                                    \
{   t = LS_BOX(ROTR(t,  8)) ^ au4RcoTable[i];           \
    t ^= au4EncryptKey[8 * i];     au4EncryptKey[8 * i + 8] = t;    \
    t ^= au4EncryptKey[8 * i + 1]; au4EncryptKey[8 * i + 9] = t;    \
    t ^= au4EncryptKey[8 * i + 2]; au4EncryptKey[8 * i + 10] = t;   \
    t ^= au4EncryptKey[8 * i + 3]; au4EncryptKey[8 * i + 11] = t;   \
    t  = au4EncryptKey[8 * i + 4] ^ LS_BOX(t);              \
    au4EncryptKey[8 * i + 12] = t;                          \
    t ^= au4EncryptKey[8 * i + 5]; au4EncryptKey[8 * i + 13] = t;   \
    t ^= au4EncryptKey[8 * i + 6]; au4EncryptKey[8 * i + 14] = t;   \
    t ^= au4EncryptKey[8 * i + 7]; au4EncryptKey[8 * i + 15] = t;   \
}

#define ENCR_N_ROUND(bo, bi, k) \
    F_ROUND(bo, bi, 0, k);     \
    F_ROUND(bo, bi, 1, k);     \
    F_ROUND(bo, bi, 2, k);     \
    F_ROUND(bo, bi, 3, k);     \
    k += 4

#define ENCR_L_ROUND(bo, bi, k) \
    F_L_ROUND(bo, bi, 0, k);     \
    F_L_ROUND(bo, bi, 1, k);     \
    F_L_ROUND(bo, bi, 2, k);     \
    F_L_ROUND(bo, bi, 3, k)

#define DECR_N_ROUND(bo, bi, k) \
    I_ROUND(bo, bi, 0, k);     \
    I_ROUND(bo, bi, 1, k);     \
    I_ROUND(bo, bi, 2, k);     \
    I_ROUND(bo, bi, 3, k);     \
    k -= 4

#define DECR_L_ROUND(bo, bi, k) \
    I_L_ROUND(bo, bi, 0, k);     \
    I_L_ROUND(bo, bi, 1, k);     \
    I_L_ROUND(bo, bi, 2, k);     \
    I_L_ROUND(bo, bi, 3, k)

VOID                AesGenerateTables (VOID);
VOID                AesEncr (UINT4 au4InBlk[4], UINT1 u1InKeyLen,
                             UINT4 au4EncryptKey[64]);
VOID                AesDecr (UINT4 au4InBlk[4], UINT1 u1InKeyLen,
                             UINT4 au4EncryptKey[64], UINT4 au4DecryptKey[64]);

/************************************************************************/
/*  Function Name   : AesGenerateTables                                 */
/*  Description     : This function is used to generate the look up     */
/*                  : tables for AES Algorithm                          */
/*                  :                                                   */
/*  Input(s)        :                                                   */
/*                  :                                                   */
/*  Output(s)       :Initializes the lookup table                       */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/

VOID
AesGenerateTables (VOID)
{
    UINT4               i, t;
    UINT1               p, q;

    /* log and power tables for GF(2**8) finite field with  */
    /* 0x11b as modular polynomial - the simplest prmitive  */
    /* root is 0x11, used here to generate the tables       */

    for (i = 0, p = 1; i < 256; ++i)
    {
        au1PowTable[i] = (UINT1) p;
        au1LogTable[p] = (UINT1) i;
        p = (UINT1) (p ^ (p << 1) ^ (p & 0x80 ? 0x01b : 0));
    }

    au1LogTable[1] = 0;
    p = 1;

    for (i = 0; i < 10; ++i)
    {
        au4RcoTable[i] = p;
        p = (UINT1) ((p << 1) ^ (p & 0x80 ? 0x1b : 0));
    }

    /* note that the affine BYTE transformation matrix in   */
    /* rijndael specification is in big endian format with  */
    /* bit 0 as the most significant bit. In the remainder  */
    /* of the specification the bits are numbered from the  */
    /* least significant end of a BYTE.                     */

    for (i = 0; i < 256; ++i)
    {
        p = (i ? au1PowTable[255 - au1LogTable[i]] : 0);
        q = p;
        q = (UINT1) ((q >> 7) | (q << 1));
        p ^= q;
        q = (UINT1) ((q >> 7) | (q << 1));
        p ^= q;
        q = (UINT1) ((q >> 7) | (q << 1));
        p ^= q;
        q = (UINT1) ((q >> 7) | (q << 1));
        p ^= q ^ 0x63;
        au1SBox[i] = (UINT1) p;
        au1InvSBox[p] = (UINT1) i;
    }

    for (i = 0; i < 256; ++i)
    {
        p = au1SBox[i];
        t = p;
        au4FLTable[0][i] = t;
        au4FLTable[1][i] = ROTL (t, 8);
        au4FLTable[2][i] = ROTL (t, 16);
        au4FLTable[3][i] = ROTL (t, 24);
        t = ((UINT4) FF_MULT (2, p)) |
            ((UINT4) p << 8) |
            ((UINT4) p << 16) | ((UINT4) FF_MULT (3, p) << 24);

        au4FtTable[0][i] = t;
        au4FtTable[1][i] = ROTL (t, 8);
        au4FtTable[2][i] = ROTL (t, 16);
        au4FtTable[3][i] = ROTL (t, 24);

        p = au1InvSBox[i];
        t = p;
        au4ILTable[0][i] = t;
        au4ILTable[1][i] = ROTL (t, 8);
        au4ILTable[2][i] = ROTL (t, 16);
        au4ILTable[3][i] = ROTL (t, 24);
        t = ((UINT4) FF_MULT (14, p)) |
            ((UINT4) FF_MULT (9, p) << 8) |
            ((UINT4) FF_MULT (13, p) << 16) | ((UINT4) FF_MULT (11, p) << 24);

        au4ItTable[0][i] = t;
        au4ItTable[1][i] = ROTL (t, 8);
        au4ItTable[2][i] = ROTL (t, 16);
        au4ItTable[3][i] = ROTL (t, 24);
    }

    u4TabGenFlag = 1;

}

/************************************************************************/
/*  Function Name   : AesSetKey                                         */
/*  Description     :This Function is used to Initialize key schedule   */
/*                  :array                                              */
/*                  :                                                   */
/*  Input(s)        :i1Key:Refers to the Input key with which data is   */
/*                  :to be encrypted                                    */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :Key schedule array is initialized                  */
/*  Returns         :Returns the Array                                  */
/*                  :                                                   */
/************************************************************************/

VOID
AesSetKey (UINT4 au4InKey[], UINT1 u1InKeyLen, UINT4 au4EncryptKey[64],
           UINT4 au4DecryptKey[64])
{

    UINT4               i, t, u, v, w;
    UINT4               u4KeyLen = 0;

    if (!u4TabGenFlag)
    {
        AesGenerateTables ();
    }

    u4KeyLen = (u1InKeyLen) / 4;

    au4EncryptKey[0] = au4InKey[0];
    au4EncryptKey[1] = au4InKey[1];
    au4EncryptKey[2] = au4InKey[2];
    au4EncryptKey[3] = au4InKey[3];

    switch (u4KeyLen)
    {

        case 4:
            t = au4EncryptKey[3];
            for (i = 0; i < 10; ++i)
            {
                INIT_4_KS_ARRAY (i);
            }
            break;

        case 6:
            au4EncryptKey[4] = au4InKey[4];
            t = au4EncryptKey[5] = au4InKey[5];
            for (i = 0; i < 8; ++i)
            {
                INIT_6_KS_ARRAY (i);
            }
            break;

        case 8:
            au4EncryptKey[4] = au4InKey[4];
            au4EncryptKey[5] = au4InKey[5];
            au4EncryptKey[6] = au4InKey[6];
            t = au4EncryptKey[7] = au4InKey[7];
            for (i = 0; i < 7; ++i)
            {
                INIT_8_KS_ARRAY (i);
            }
            break;
    }

    au4DecryptKey[0] = au4EncryptKey[0];
    au4DecryptKey[1] = au4EncryptKey[1];
    au4DecryptKey[2] = au4EncryptKey[2];
    au4DecryptKey[3] = au4EncryptKey[3];

    for (i = 4; i < 4 * u4KeyLen + 24; ++i)
    {
        INVERSE_MIX_COL (au4DecryptKey[i], au4EncryptKey[i]);
    }

}

/************************************************************************/
/*  Function Name   :AesEncr                                            */
/*  Description     :This function encrypts the Blocks of 64-bit Data   */
/*                  :                                                   */
/*  Input(s)        :pi1Block:Refers to the incoming data               */
/*                  :pau1IfKeyBlock:Refers to the outgoing data         */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :Encrypted Data                                     */
/*  Returns         :                                                   */
/************************************************************************/

VOID
AesEncr (UINT4 au4InBlk[4], UINT1 u1InKeyLen, UINT4 au4EncryptKey[64])
{

    UINT4               au4Block0[4], au4Block1[4], *pu4Key;
    UINT4               u4KeyLen = u1InKeyLen / 4;

    au4Block0[0] = au4InBlk[0] ^ au4EncryptKey[0];
    au4Block0[1] = au4InBlk[1] ^ au4EncryptKey[1];
    au4Block0[2] = au4InBlk[2] ^ au4EncryptKey[2];
    au4Block0[3] = au4InBlk[3] ^ au4EncryptKey[3];

    pu4Key = au4EncryptKey + 4;

    if (u4KeyLen > 6)
    {
        ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
        ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    }

    if (u4KeyLen > 4)
    {
        ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
        ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    }

    ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
    ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
    ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
    ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
    ENCR_N_ROUND (au4Block0, au4Block1, pu4Key);
    ENCR_N_ROUND (au4Block1, au4Block0, pu4Key);
    ENCR_L_ROUND (au4Block0, au4Block1, pu4Key);

    au4InBlk[0] = au4Block0[0];
    au4InBlk[1] = au4Block0[1];
    au4InBlk[2] = au4Block0[2];
    au4InBlk[3] = au4Block0[3];
}

/************************************************************************/
/*  Function Name   : AesDecr                                           */
/*  Description     :This Function Decrypts the 64-bit Block of         */
/*                  :Encrypted data                                     */
/*                  :                                                   */
/*  Input(s)        :pi1Block:Refers to the Block of data to be         */
/*                  :Decrypted                                          */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :Decrypted Data                                     */
/*  Returns         :                                                   */
/************************************************************************/

VOID
AesDecr (UINT4 au4InBlk[4], UINT1 u1InKeyLen, UINT4 au4EncryptKey[64],
         UINT4 au4DecryptKey[64])
{

    UINT4               au4Block0[4], au4Block1[4], *pu4Key;
    UINT4               u4KeyLen = u1InKeyLen / 4;

    au4Block0[0] = au4InBlk[0] ^ au4EncryptKey[4 * u4KeyLen + 24];
    au4Block0[1] = au4InBlk[1] ^ au4EncryptKey[4 * u4KeyLen + 25];
    au4Block0[2] = au4InBlk[2] ^ au4EncryptKey[4 * u4KeyLen + 26];
    au4Block0[3] = au4InBlk[3] ^ au4EncryptKey[4 * u4KeyLen + 27];

    pu4Key = au4DecryptKey + 4 * (u4KeyLen + 5);

    if (u4KeyLen > 6)
    {
        DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
        DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    }

    if (u4KeyLen > 4)
    {
        DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
        DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    }

    DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
    DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
    DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
    DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
    DECR_N_ROUND (au4Block0, au4Block1, pu4Key);
    DECR_N_ROUND (au4Block1, au4Block0, pu4Key);
    DECR_L_ROUND (au4Block0, au4Block1, pu4Key);

    au4InBlk[0] = au4Block0[0];
    au4InBlk[1] = au4Block0[1];
    au4InBlk[2] = au4Block0[2];
    au4InBlk[3] = au4Block0[3];
}

/************************************************************************/
/*  Function Name   :AesEncrypt                                         */
/*  Description     :This Function is called by the Modules which       */
/*                  :Requires the data to be Encrypted                  */
/*                  :                                                   */
/*  Input(s)        :pu4Buffer:Refers to the Incoming Data to be        */
/*                  :Encrypted                                          */
/*                  :u2Size : Refers to the Size of the Data            */
/*                  :u1InKeyLen : Length of the InputKey                */
/*                  :au4EncryptKey : Key used for encrypting the data   */
/*                  :                                                   */
/*  Output(s)       :The Encrypted data                                 */
/*  Returns         :Returns AES_OK or AES_NOT_OK                       */
/*                  :                                                   */
/************************************************************************/
INT1
AesEncrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
            UINT4 au4EncryptKey[64])
{
    UINT2               u2Count1 = 0;
    UINT2               u2Count2 = (UINT2) (u2Size / 4);

    if (pu4Buffer == NULL)
    {
        return AES_NOT_OK;
    }

    while (u2Count2 != 0)
    {

        AesEncr ((pu4Buffer + u2Count1), u1InKeyLen, au4EncryptKey);
        u2Count1 = (UINT2) (u2Count1 + 4);
        u2Count2 = (UINT2) (u2Count2 - 4);

    }

    return AES_OK;
}

/************************************************************************/
/*  Function Name   :AesDecrypt                                         */
/*  Description     :This Function is called by the Modules which       */
/*                  :Requires the data to be Decrypted                  */
/*                  :                                                   */
/*  Input(s)        :pu4Buffer:Refers to the Incoming Data to be        */
/*                  :Decrypted                                          */
/*                  :u2Size : Refers to the Size of the Data            */
/*                  :u1InKeyLen : Refers to the Length of the Input Key */
/*                  :au4EncryptKey:Refers to the encrypn key            */
/*                  :au4DecryptKey: Refres to the decrypt key           */
/*                  :                                                   */
/*  Output(s)       :The Decrypted data                                 */
/*                  :                                                   */
/*  Returns         :Returns AES_OK or AES_NOT_OK                       */
/*                  :                                                   */
/************************************************************************/
INT1
AesDecrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
            UINT4 au4EncryptKey[64], UINT4 au4DecryptKey[64])
{
    UINT2               u2Count1 = 0;
    UINT2               u2Count2 = (UINT2) (u2Size / 4);

    if (pu4Buffer == NULL)
    {
        return AES_NOT_OK;
    }

    while (u2Count2 != 0)
    {

        AesDecr ((pu4Buffer + u2Count1), u1InKeyLen, au4EncryptKey,
                 au4DecryptKey);
        u2Count1 = (UINT2) (u2Count1 + 4);
        u2Count2 = (UINT2) (u2Count2 - 4);

    }
    return AES_OK;
}

/************************************************************************/
/*  Function Name   :AesCbcEncrypt                                      */
/*  Description     :This Function is called by the Modules which       */
/*                  :Requires the data to be Encrypted                  */
/*                  :                                                   */
/*  Input(s)        :pu4Buffer:Refers to the Incoming Data to be        */
/*                  :Encrypted                                          */
/*                  :u2Size : Refers to the Size of the Data            */
/*                  :u1InKeyLen : Length of the InputKey                */
/*                  :au4EncryptKey : Key used for encrypting the data   */
/*                  :pu1InitVect :- IV To Be used                       */
/*                  :                                                   */
/*  Output(s)       :The Encrypted data                                 */
/*  Returns         :Returns AES_OK or AES_NOT_OK                       */
/*                  :                                                   */
/************************************************************************/
INT1
AesCbcEncrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
               UINT4 au4EncryptKey[64], UINT1 *pu1InitVect)
{
    UINT2               u2ReadOffset = 0;
    UINT2               u2BufSize = (UINT2) (u2Size / 4);
    UINT1               u1IVCount = 0;
    UINT1              *pu1Buf = NULL;

    if ((pu4Buffer == NULL) || (pu1InitVect == NULL))
    {
        return AES_NOT_OK;
    }
    while (u2BufSize != 0)
    {
        pu1Buf = (UINT1 *) (pu4Buffer + u2ReadOffset);
        for (u1IVCount = 0; u1IVCount < AES_IV_SIZE; u1IVCount++)
        {
            pu1Buf[u1IVCount] ^= pu1InitVect[u1IVCount];
        }
        AesEncr ((pu4Buffer + u2ReadOffset), u1InKeyLen, au4EncryptKey);
        u2BufSize = (UINT2) (u2BufSize - 4);
        MEMCPY (pu1InitVect, (pu4Buffer + u2ReadOffset), AES_IV_SIZE);
        u2ReadOffset += 4;
    }
    return AES_OK;
}

/************************************************************************/
/*  Function Name   :AesCbcDecrypt                                      */
/*  Description     :This Function is called by the Modules which       */
/*                  :Requires the data to be Decrypted                  */
/*                  :                                                   */
/*  Input(s)        :pu4Buffer:Refers to the Incoming Data to be        */
/*                  :Decrypted                                          */
/*                  :u2Size : Refers to the Size of the Data            */
/*                  :u1InKeyLen : Refers to the Length of the Input Key */
/*                  :au4EncryptKey:Refers to the encrypn key            */
/*                  :au4DecryptKey: Refres to the decrypt key           */
/*                  :pu1InitVect:Refers to InitVect                     */
/*                  :                                                   */
/*  Output(s)       :The Decrypted data                                 */
/*                  :                                                   */
/*  Returns         :Returns AES_OK or AES_NOT_OK                       */
/*                  :                                                   */
/************************************************************************/
INT1
AesCbcDecrypt (UINT4 *pu4Buffer, UINT2 u2Size, UINT1 u1InKeyLen,
               UINT4 au4EncryptKey[64], UINT4 au4DecryptKey[64],
               UINT1 *pu1InitVect)
{
    UINT2               u2ReadOffset = 0;
    UINT2               u2BufSize = (UINT2) (u2Size / 4);
    UINT1               au1Temp[AES_IV_SIZE];
    UINT1              *pu1Buf = NULL;
    UINT1               u1IVCount = 0;

    if ((pu4Buffer == NULL) || (pu1InitVect == NULL))
    {
        return AES_NOT_OK;
    }
    while (u2BufSize != 0)
    {
        MEMCPY (au1Temp, (pu4Buffer + u2ReadOffset), AES_IV_SIZE);
        AesDecr ((pu4Buffer + u2ReadOffset), u1InKeyLen, au4EncryptKey,
                 au4DecryptKey);
        pu1Buf = (UINT1 *) (pu4Buffer + u2ReadOffset);
        for (u1IVCount = 0; u1IVCount < AES_IV_SIZE; u1IVCount++)
        {
            pu1Buf[u1IVCount] ^= pu1InitVect[u1IVCount];
        }
        u2ReadOffset += 4;
        u2BufSize -= 4;
        MEMCPY (pu1InitVect, au1Temp, AES_IV_SIZE);
    }
    return AES_OK;
}
