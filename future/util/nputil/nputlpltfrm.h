/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: nputlpltfrm.h,v 1.1 2013/03/14 14:06:03 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             and prototypes for functions used in custnputil.c file
 *             <Part of dual node stacking model>
 *
 *******************************************************************/
#ifndef __CUST_NP_UTIL_H__
#define __CUST_NP_UTIL_H__
#include "nputil.h"
#include "nputlremnp.h"
#include "npstackutl.h"
INT4 CustNpUtilMaskNpPorts PROTO ((tFsHwNp *pFsHwNp, tRemoteHwNp *pRemoteHwNp,
                                   UINT1   *pu1NpCallStatus));
#endif

