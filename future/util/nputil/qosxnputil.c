/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: qosxnputil.c,v 1.10 2016/06/22 10:20:04 siva Exp $
 *
 * Description:This file contains the single NP QOSX function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __QOSXNPUTIL_C__
#define __QOSXNPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskQosxNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskQosxNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxRemoteNpModInfo *pQosxRemoteNpModInfo = NULL;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    pQosxNpModInfo = &(pFsHwNp->QosxNpModInfo);
    pQosxRemoteNpModInfo = &(pRemoteHwNp->QosxRemoteNpModInfo);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_QOS_HW_CONFIG_PFC:
        {
            tQosxNpWrFsQosHwConfigPfc *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpFsQosHwConfigPfc;
            if (NpUtilIsPortChannel (pEntry->pQosPfcHwEntry->u4Ifindex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort
                (pEntry->pQosPfcHwEntry->u4Ifindex, &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case QO_S_HW_DELETE_CLASS_MAP_ENTRY:
        {
            tQosxNpWrQoSHwDeleteClassMapEntry *pEntry = NULL;
            tQosxRemoteNpWrQoSHwDeleteClassMapEntry *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwDeleteClassMapEntry;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwDeleteClassMapEntry;
            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pEntry->pClsMapEntry->
                                               pL2FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pEntry->pClsMapEntry->
                                               pL3FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    if (NpUtilIsPortChannel
                        (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if (NpUtilIsRemotePort
                        (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex,
                         &HwPortInfo) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                    }
                }
            }
            break;
        }
        case QO_S_HW_GET_CO_S_Q_STATS:
        {
            tQosxNpWrQoSHwGetCoSQStats *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetCoSQStats;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case QO_S_HW_GET_ALG_DROP_STATS:
        case QO_S_HW_GET_COUNT_ACT_STATS:
        case QO_S_HW_GET_METER_STATS:
        case QO_S_HW_GET_RANDOM_DROP_STATS:
        case QO_S_HW_INIT:
        case QO_S_HW_METER_CREATE:
        case QO_S_HW_METER_DELETE:
        case QO_S_HW_SET_CPU_RATE_LIMIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSHwMapClassToPolicy *pEntry = NULL;
            tQosxRemoteNpWrQoSHwMapClassToPolicy *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToPolicy;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToPolicy;
            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pEntry->pClsMapEntry->
                                               pL2FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pEntry->pClsMapEntry->
                                               pL3FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    if (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex != 0)
                    {
                        if (NpUtilIsPortChannel
                            (pEntry->pClsMapEntry->pPriorityMapPtr->
                             u4IfIndex) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus =
                                NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                        }
                        else if (NpUtilIsRemotePort
                            (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex,
                             &HwPortInfo) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                        else
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                        }
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                }
            }
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxNpWrQoSHwMapClassToQueue *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueue;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxNpWrQoSHwMapClassToQueueId *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueueId;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case QO_S_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSHwQueueCreate *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueCreate;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_QUEUE_DELETE:
        {
            tQosxNpWrQoSHwQueueDelete *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueDelete;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSHwSchedulerAdd *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerAdd;
            if (NpUtilIsPortChannel (pEntry->pSchedEntry->i4QosIfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort
                (pEntry->pSchedEntry->i4QosIfIndex, &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SCHEDULER_DELETE:
        {
            tQosxNpWrQoSHwSchedulerDelete *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerDelete;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSHwSchedulerHierarchyMap *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerHierarchyMap;
            if (NpUtilIsPortChannel (pEntry->i4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSHwSchedulerUpdateParams *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerUpdateParams;
            if (NpUtilIsPortChannel (pEntry->pSchedEntry->i4QosIfIndex) == 
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->pSchedEntry->i4QosIfIndex,
                                    &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSHwSetDefUserPriority *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetDefUserPriority;
            if (NpUtilIsPortChannel (pEntry->i4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4Port, &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSHwSetPbitPreferenceOverDscp *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetPbitPreferenceOverDscp;
            if (NpUtilIsPortChannel (pEntry->i4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->i4Port, &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        } 
        case QO_S_HW_UNMAP_CLASS_FROM_POLICY:
        {
            tQosxNpWrQoSHwUnmapClassFromPolicy *pEntry = NULL;
            tQosxRemoteNpWrQoSHwUnmapClassFromPolicy *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwUnmapClassFromPolicy;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUnmapClassFromPolicy;
            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pEntry->pClsMapEntry->
                                               pL2FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pEntry->pClsMapEntry->
                                               pL3FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    if (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex != 0)
                    {
                        if (NpUtilIsPortChannel
                            (pEntry->pClsMapEntry->pPriorityMapPtr->
                             u4IfIndex) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus =
                                NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                        }
                        else if (NpUtilIsRemotePort
                            (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex,
                             &HwPortInfo) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                        else
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                        }
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                }
            }
            if (pEntry->pPlyMapEntry != NULL)
            {
                if (pEntry->pPlyMapEntry->i4IfIndex != 0)
                {
                    if (NpUtilIsPortChannel (pEntry->pPlyMapEntry->i4IfIndex) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if (NpUtilIsRemotePort
                        (pEntry->pPlyMapEntry->i4IfIndex,
                         &HwPortInfo) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                    }
                }
                else
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
            } 
            break;
        } 
        case QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxNpWrQoSHwUpdatePolicyMapForClass *pEntry = NULL;
            tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwUpdatePolicyMapForClass;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUpdatePolicyMapForClass;
            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pEntry->pClsMapEntry->
                                               pL2FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pEntry->pClsMapEntry->
                                               pL3FilterPtr,
                                               &pRemEntry->ClsMapEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    if (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex != 0)
                    {
                        if (NpUtilIsPortChannel
                            (pEntry->pClsMapEntry->pPriorityMapPtr->
                             u4IfIndex) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus =
                                NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                        }
                        else if (NpUtilIsRemotePort
                            (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex,
                             &HwPortInfo) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                        else
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                        }
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                }
            }
            if (pEntry->pPlyMapEntry != NULL)
            {
                if (pEntry->pPlyMapEntry->i4IfIndex != 0)
                {
                    if (NpUtilIsPortChannel (pEntry->pPlyMapEntry->i4IfIndex) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if (NpUtilIsRemotePort
                        (pEntry->pPlyMapEntry->i4IfIndex,
                         &HwPortInfo) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                    }
                }
                else
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
            }  
            break;
        }
        case QO_S_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSHwMapClasstoPriMap *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClasstoPriMap;
            if (pEntry->pQosClassToPriMapEntry->i4IfIndex != 0)
            {
                if (NpUtilIsPortChannel
                    (pEntry->pQosClassToPriMapEntry->i4IfIndex) == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
                else if (NpUtilIsRemotePort
                    (pEntry->pQosClassToPriMapEntry->i4IfIndex,
                     &HwPortInfo) == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            break;
        }
        case QOS_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosHwMapClassToIntPriority *pEntry = NULL;
            tQosxRemoteNpWrQosHwMapClassToIntPriority *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQosHwMapClassToIntPriority;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQosHwMapClassToIntPriority;
            if (pEntry->pQosClassToIntPriEntry != NULL)
            {
                if (pEntry->pQosClassToIntPriEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pEntry->pQosClassToIntPriEntry->
                                               pL2FilterPtr,
                                               &pRemEntry->
                                               QosClassToIntPriEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pEntry->pQosClassToIntPriEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pEntry->pQosClassToIntPriEntry->
                                               pL3FilterPtr,
                                               &pRemEntry->
                                               QosClassToIntPriEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
            }
            break;
        }
        case FS_QOS_HW_GET_PFC_STATS:
        {
            tQosxNpWrFsQosHwGetPfcStats *pEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpFsQosHwGetPfcStats;

            if (NpUtilIsPortChannel (pEntry->pQosPfcHwStats->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->pQosPfcHwStats->u4IfIndex, &HwPortInfo) ==
                    FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
#ifdef MBSM_WANTED 
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE:
        {
            pRemoteHwNp->u4Opcode = QO_S_HW_MAP_CLASS_TO_QUEUE;

            tQosxNpWrQoSMbsmHwMapClassToQueue *pEntry = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueue *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueue;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueue;

            if (NpUtilIsPortChannel (pEntry->i4IfIndex) ==
                FNP_SUCCESS)
			{
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
				break;
            }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                pRemEntry->i4IfIndex = pEntry->i4IfIndex;
                pRemEntry->i4ClsOrPriType = pEntry->i4ClsOrPriType;
                pRemEntry->u4ClsOrPri = pEntry->u4ClsOrPri;
                pRemEntry->u4QId = pEntry->u4QId;
                pRemEntry->u1Flag = pEntry->u1Flag;
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            break;
        } 
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            pRemoteHwNp->u4Opcode = QO_S_HW_MAP_CLASS_TO_QUEUE_ID;

            tQosxNpWrQoSMbsmHwMapClassToQueueId *pEntry = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueueId *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueueId;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueueId;

            if (pEntry->i4IfIndex != 0)
	    {
	        if (NpUtilIsPortChannel (pEntry->i4IfIndex) ==
	            FNP_SUCCESS)
		{
		    *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
		    break;
		}
            	else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) !=
                    FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    break;
                }
            }
            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemEntry->ClsMapEntry.L2FilterPtr,
                                                      pEntry->pClsMapEntry->pL2FilterPtr);
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemEntry->ClsMapEntry.L3FilterPtr,
                                                      pEntry->pClsMapEntry->pL3FilterPtr);
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemEntry->ClsMapEntry.PriorityMapPtr),
                            (pEntry->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemEntry->ClsMapEntry.u4QoSMFClass =
                    pEntry->pClsMapEntry->u4QoSMFClass;
                pRemEntry->ClsMapEntry.u1QoSMFCStatus =
                    pEntry->pClsMapEntry->u1QoSMFCStatus;
                pRemEntry->ClsMapEntry.u1PreColor =
                    pEntry->pClsMapEntry->u1PreColor;
            }

            pRemEntry->i4IfIndex = pEntry->i4IfIndex;
            pRemEntry->i4ClsOrPriType = pEntry->i4ClsOrPriType;
            pRemEntry->u4ClsOrPri = pEntry->u4ClsOrPri;
            pRemEntry->u4QId = pEntry->u4QId;
            pRemEntry->u1Flag = pEntry->u1Flag;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        } 
        case QO_S_MBSM_HW_QUEUE_CREATE:
        {
            pRemoteHwNp->u4Opcode = QO_S_HW_QUEUE_CREATE;

            tQosxNpWrQoSMbsmHwQueueCreate *pEntry = NULL;
            tQosxRemoteNpWrQoSHwQueueCreate *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwQueueCreate;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueCreate;

	    if (NpUtilIsPortChannel (pEntry->i4IfIndex) ==
		    FNP_SUCCESS)
	    {
		*pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
		break;
	    }
            else if (NpUtilIsRemotePort (pEntry->i4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                break;
            }
            pRemEntry->i4IfIndex = pEntry->i4IfIndex;
            pRemEntry->u4QId = pEntry->u4QId;
	    if (pEntry->pQEntry != NULL)
	    {
		if (pEntry->pQEntry->pShapePtr != NULL)
		{
		    MEMCPY (&(pRemEntry->QEntry.ShapePtr),
			    (pEntry->pQEntry->pShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}

		if (pEntry->pQEntry->pSchedPtr != NULL)
		{
		    if (pEntry->pQEntry->pSchedPtr->pShapePtr != NULL)
		    {
			MEMCPY (&(pRemEntry->QEntry.SchedPtr.ShapePtr),
				(pEntry->pQEntry->pSchedPtr->pShapePtr),
				sizeof (tQoSShapeCfgEntry));
		    }
		    pRemEntry->QEntry.SchedPtr.i4QosIfIndex =
			pEntry->pQEntry->pSchedPtr->i4QosIfIndex;
		    pRemEntry->QEntry.SchedPtr.i4QosSchedulerId =
			pEntry->pQEntry->pSchedPtr->i4QosSchedulerId;
		    pRemEntry->QEntry.SchedPtr.u4QosSchedHwId =
			pEntry->pQEntry->pSchedPtr->u4QosSchedHwId;
		    pRemEntry->QEntry.SchedPtr.u4QosSchedChildren =
			pEntry->pQEntry->pSchedPtr->u4QosSchedChildren;
		    pRemEntry->QEntry.SchedPtr.u1QosSchedAlgo =
			pEntry->pQEntry->pSchedPtr->u1QosSchedAlgo;
		    pRemEntry->QEntry.SchedPtr.u1QosSchedulerStatus =
			pEntry->pQEntry->pSchedPtr->u1QosSchedulerStatus;
		    pRemEntry->QEntry.SchedPtr.u1HL =
			pEntry->pQEntry->pSchedPtr->u1HL;
		    pRemEntry->QEntry.SchedPtr.u1Flag =
			pEntry->pQEntry->pSchedPtr->u1Flag;
		}

		pRemEntry->QEntry.i4QosQId = pEntry->pQEntry->i4QosQId;
		pRemEntry->QEntry.i4QosSchedulerId =
		    pEntry->pQEntry->i4QosSchedulerId;
		pRemEntry->QEntry.i4QosQueueHwId = pEntry->pQEntry->i4QosQueueHwId;
		pRemEntry->QEntry.u4QueueType = pEntry->pQEntry->u4QueueType;
		pRemEntry->QEntry.u2QosQWeight = pEntry->pQEntry->u2QosQWeight;
		pRemEntry->QEntry.u1QosQPriority = pEntry->pQEntry->u1QosQPriority;
		pRemEntry->QEntry.u1QosQStatus = pEntry->pQEntry->u1QosQStatus;
	    }
            if (pEntry->pQTypeEntry != NULL)
            {
                MEMCPY (&(pRemEntry->QTypeEntry), (pEntry->pQTypeEntry),
                        sizeof (tQoSQtypeEntry));
            }
            if (pEntry->papRDCfgEntry != NULL)
            {
                MEMCPY (&(pRemEntry->ApRDCfgEntry), (pEntry->papRDCfgEntry),
                        sizeof (tQoSREDCfgEntry *));
            }
            pRemEntry->i2HL = pEntry->i2HL;
            break;
        } 
        case QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            pRemoteHwNp->u4Opcode = QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS;

            tQosxNpWrQoSMbsmHwUpdatePolicyMapForClass *pEntry = NULL;
            tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwUpdatePolicyMapForClass;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUpdatePolicyMapForClass;            

            if (pEntry->pClsMapEntry != NULL)
            {
                if (pEntry->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemEntry->ClsMapEntry.L2FilterPtr,
                                                      pEntry->pClsMapEntry->pL2FilterPtr);
                    NpUtilIssHwUpdateL2Filter (pEntry->pClsMapEntry->pL2FilterPtr,
                                               &pRemEntry->ClsMapEntry.L2FilterPtr,0,
                                               &HwPortInfo, pu1NpCallStatus);
                    if (*pu1NpCallStatus != NPUTIL_INVOKE_REMOTE_NP)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        break;
                    }
                }
                if (pEntry->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemEntry->ClsMapEntry.L3FilterPtr,
                                                      pEntry->pClsMapEntry->pL3FilterPtr);
                    NpUtilIssHwUpdateL3Filter (pEntry->pClsMapEntry->pL3FilterPtr,
                                               &pRemEntry->ClsMapEntry.L3FilterPtr,0,
                                               &HwPortInfo, pu1NpCallStatus);
                    if (*pu1NpCallStatus != NPUTIL_INVOKE_REMOTE_NP)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        break;
                    }
                }
                if (pEntry->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemEntry->ClsMapEntry.PriorityMapPtr), (pEntry->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                    if (NpUtilIsPortChannel
                        (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
						break;
                    }
                    else if (NpUtilIsRemotePort
                        (pEntry->pClsMapEntry->pPriorityMapPtr->u4IfIndex,
                         &HwPortInfo) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        break;
                    }
                }
                pRemEntry->ClsMapEntry.u4QoSMFClass = pEntry->pClsMapEntry->u4QoSMFClass;
                pRemEntry->ClsMapEntry.u1QoSMFCStatus = pEntry->pClsMapEntry->u1QoSMFCStatus;
                pRemEntry->ClsMapEntry.u1PreColor = pEntry->pClsMapEntry->u1PreColor;
            }
            if (pEntry->pPlyMapEntry != NULL)
            {
            MEMCPY (&(pRemEntry->PlyMapEntry), (pEntry->pPlyMapEntry),
                    sizeof (tQoSPolicyMapEntry));
                if (pEntry->pPlyMapEntry->i4IfIndex != 0)
                {
                    if (NpUtilIsPortChannel
                        (pEntry->pPlyMapEntry->i4IfIndex) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
						break;
                    }
                    else if (NpUtilIsRemotePort
                        (pEntry->pPlyMapEntry->i4IfIndex,
                         &HwPortInfo) == FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        break;
                    }
                }
                else
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            if (pEntry->pInProActEntry != NULL)
            {
            MEMCPY (&(pRemEntry->InProActEntry), (pEntry->pInProActEntry),
                    sizeof (tQoSInProfileActionEntry));
            }
            if (pEntry->pOutProActEntry != NULL)
            {
            MEMCPY (&(pRemEntry->OutProActEntry),
                    (pEntry->pOutProActEntry),
                    sizeof (tQoSOutProfileActionEntry));
            }
            if (pEntry->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemEntry->MeterEntry), (pEntry->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSMbsmHwSchedulerAdd *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerAdd *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerAdd;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerAdd;

            if (pLocalArgs->pSchedEntry != NULL)
            {
                if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->SchedEntry.ShapePtr),
                            (pLocalArgs->pSchedEntry->pShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                }
                pRemoteArgs->SchedEntry.i4QosIfIndex =
                    pLocalArgs->pSchedEntry->i4QosIfIndex;
                pRemoteArgs->SchedEntry.i4QosSchedulerId =
                    pLocalArgs->pSchedEntry->i4QosSchedulerId;
                pRemoteArgs->SchedEntry.u4QosSchedHwId =
                    pLocalArgs->pSchedEntry->u4QosSchedHwId;
                pRemoteArgs->SchedEntry.u4QosSchedChildren =
                    pLocalArgs->pSchedEntry->u4QosSchedChildren;
                pRemoteArgs->SchedEntry.u1QosSchedAlgo =
                    pLocalArgs->pSchedEntry->u1QosSchedAlgo;
                pRemoteArgs->SchedEntry.u1QosSchedulerStatus =
                    pLocalArgs->pSchedEntry->u1QosSchedulerStatus;
                pRemoteArgs->SchedEntry.u1HL = pLocalArgs->pSchedEntry->u1HL;
                pRemoteArgs->SchedEntry.u1Flag =
                    pLocalArgs->pSchedEntry->u1Flag;
            }

            pRemoteHwNp->u4Opcode = QO_S_HW_SCHEDULER_ADD;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        } 
        case FS_QOS_MBSM_HW_CONFIG_PFC:
        {
            pRemoteHwNp->u4Opcode = FS_QOS_HW_CONFIG_PFC;

            tQosxNpWrFsQosMbsmHwConfigPfc *pEntry = NULL;
            tQosxRemoteNpWrFsQosHwConfigPfc *pRemEntry = NULL;

            pEntry = &pQosxNpModInfo->QosxNpFsQosMbsmHwConfigPfc;
            pRemEntry = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwConfigPfc;

            if (NpUtilIsPortChannel
                (pEntry->pQosPfcHwEntry->u4Ifindex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
				break;
            }
            else if (NpUtilIsRemotePort
                (pEntry->pQosPfcHwEntry->u4Ifindex, &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                break;
            }
            MEMCPY (&(pRemEntry->QosPfcHwEntry), (pEntry->pQosPfcHwEntry),
                    sizeof (tQosPfcHwEntry));
            break;
        }
        case QO_S_MBSM_HW_METER_CREATE:
        {
            tQosxNpWrQoSMbsmHwMeterCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMeterCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwMeterCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterCreate;
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }

            pRemoteHwNp->u4Opcode = QO_S_HW_METER_CREATE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

        case QO_S_MBSM_HW_INIT:
        {
            pRemoteHwNp->u4Opcode = QO_S_HW_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }

        case QO_S_MBSM_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxNpWrQoSMbsmHwSetCpuRateLimit *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetCpuRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSetCpuRateLimit;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetCpuRateLimit;
            pRemoteArgs->i4CpuQueueId = pLocalArgs->i4CpuQueueId;
            pRemoteArgs->u4MinRate = pLocalArgs->u4MinRate;
            pRemoteArgs->u4MaxRate = pLocalArgs->u4MaxRate;
            pRemoteHwNp->u4Opcode = QO_S_HW_SET_CPU_RATE_LIMIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSMbsmHwMapClassToPolicy *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToPolicy;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToPolicy;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->PlyMapEntry), (pLocalArgs->pPlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pInProActEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->InProActEntry),
                        (pLocalArgs->pInProActEntry),
                        sizeof (tQoSInProfileActionEntry));
            }
            if (pLocalArgs->pOutProActEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->OutProActEntry),
                        (pLocalArgs->pOutProActEntry),
                        sizeof (tQoSOutProfileActionEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL2Filter (pLocalArgs->pClsMapEntry->
                                               pL2FilterPtr,
                                               &pRemoteArgs->ClsMapEntry.
                                               L2FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                    NpUtilIssHwUpdateL3Filter (pLocalArgs->pClsMapEntry->
                                               pL3FilterPtr,
                                               &pRemoteArgs->ClsMapEntry.
                                               L3FilterPtr,0, &HwPortInfo,
                                               pu1NpCallStatus);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    if (pLocalArgs->pClsMapEntry->pPriorityMapPtr->u4IfIndex !=
                        0)
                    {
                        if (NpUtilIsPortChannel
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr->
                             u4IfIndex) == FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        }
                        else if (NpUtilIsRemotePort
                                 (pLocalArgs->pClsMapEntry->pPriorityMapPtr->
                                  u4IfIndex, &HwPortInfo) != FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        }
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    }
                }
            }
            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            if (*pu1NpCallStatus != NPUTIL_NO_NP_INVOKE)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            pRemoteHwNp->u4Opcode = QO_S_HW_MAP_CLASS_TO_POLICY;
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSMbsmHwSchedulerHierarchyMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerHierarchyMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerHierarchyMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerHierarchyMap;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4SchedId = pLocalArgs->u4SchedId;
            pRemoteArgs->u2Sweight = pLocalArgs->u2Sweight;
            pRemoteArgs->u1Spriority = pLocalArgs->u1Spriority;
            pRemoteArgs->u4NextSchedId = pLocalArgs->u4NextSchedId;
            pRemoteArgs->u4NextQId = pLocalArgs->u4NextQId;
            pRemoteArgs->i2HL = pLocalArgs->i2HL;
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            pRemoteHwNp->u4Opcode = QO_S_HW_SCHEDULER_HIERARCHY_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSMbsmHwSchedulerUpdateParams *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerUpdateParams *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerUpdateParams;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerUpdateParams;
            if (pLocalArgs->pSchedEntry != NULL)
            {
                if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->SchedEntry.ShapePtr),
                            (pLocalArgs->pSchedEntry->pShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                }
            }
            pRemoteArgs->SchedEntry.i4QosIfIndex =
                pLocalArgs->pSchedEntry->i4QosIfIndex;
            pRemoteArgs->SchedEntry.i4QosSchedulerId =
                pLocalArgs->pSchedEntry->i4QosSchedulerId;
            pRemoteArgs->SchedEntry.u4QosSchedHwId =
                pLocalArgs->pSchedEntry->u4QosSchedHwId;
            pRemoteArgs->SchedEntry.u4QosSchedChildren =
                pLocalArgs->pSchedEntry->u4QosSchedChildren;
            pRemoteArgs->SchedEntry.u1QosSchedAlgo =
                pLocalArgs->pSchedEntry->u1QosSchedAlgo;
            pRemoteArgs->SchedEntry.u1QosSchedulerStatus =
                pLocalArgs->pSchedEntry->u1QosSchedulerStatus;
            pRemoteArgs->SchedEntry.u1HL = pLocalArgs->pSchedEntry->u1HL;
            pRemoteArgs->SchedEntry.u1Flag = pLocalArgs->pSchedEntry->u1Flag;

            pRemoteHwNp->u4Opcode = QO_S_HW_SCHEDULER_UPDATE_PARAMS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSMbsmHwSetDefUserPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSetDefUserPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetDefUserPriority;
            pRemoteArgs->i4Port = pLocalArgs->i4Port;
            pRemoteArgs->i4DefPriority = pLocalArgs->i4DefPriority;
            pRemoteHwNp->u4Opcode = QO_S_HW_SET_DEF_USER_PRIORITY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSMbsmHwSetPbitPreferenceOverDscp *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp *pRemoteArgs = NULL;
            pLocalArgs =
                &pQosxNpModInfo->QosxNpQoSMbsmHwSetPbitPreferenceOverDscp;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->
                QosxRemoteNpQoSHwSetPbitPreferenceOverDscp;
            pRemoteArgs->i4Port = pLocalArgs->i4Port;
            pRemoteArgs->i4PbitPref = pLocalArgs->i4PbitPref;
            pRemoteHwNp->u4Opcode = QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSMbsmHwMapClasstoPriMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClasstoPriMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClasstoPriMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClasstoPriMap;
            if (pLocalArgs->pQosClassToPriMapEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosClassToPriMapEntry),
                        (pLocalArgs->pQosClassToPriMapEntry),
                        sizeof (tQosClassToPriMapEntry));
            }
            pRemoteHwNp->u4Opcode = QO_S_HW_MAP_CLASSTO_PRI_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosMbsmHwMapClassToIntPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQosHwMapClassToIntPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQosMbsmHwMapClassToIntPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQosHwMapClassToIntPriority;
            if (pLocalArgs->pQosClassToIntPriEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosClassToIntPriEntry),
                        (pLocalArgs->pQosClassToIntPriEntry),
                        sizeof (tQosClassToIntPriEntry));
            }
            pRemoteHwNp->u4Opcode = QOS_HW_MAP_CLASS_TO_INT_PRIORITY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

#endif /* MBSM_WANTED */
        default:
            break;
    }
#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS) ||
        (pFsHwNp->u4Opcode > FS_QOS_MBSM_HW_CONFIG_PFC))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_QOSX_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilQosxConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilQosxConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxRemoteNpModInfo *pQosxRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pQosxNpModInfo = &(pFsHwNp->QosxNpModInfo);
    pQosxRemoteNpModInfo = &(pRemoteHwNp->QosxRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length = pRemoteHwNp->i4Length + sizeof (tQosxRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_QOS_HW_CONFIG_PFC:
        {
            tQosxNpWrFsQosHwConfigPfc *pLocalArgs = NULL;
            tQosxRemoteNpWrFsQosHwConfigPfc *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpFsQosHwConfigPfc;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwConfigPfc;
            if (pLocalArgs->pQosPfcHwEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosPfcHwEntry),
                        (pLocalArgs->pQosPfcHwEntry), sizeof (tQosPfcHwEntry));
            }
            break;
        }
        case QO_S_HW_DELETE_CLASS_MAP_ENTRY:
        {
            tQosxNpWrQoSHwDeleteClassMapEntry *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwDeleteClassMapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwDeleteClassMapEntry;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwDeleteClassMapEntry;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }
            break;
        }
        case QO_S_HW_GET_ALG_DROP_STATS:
        {
            tQosxNpWrQoSHwGetAlgDropStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetAlgDropStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetAlgDropStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetAlgDropStats;
            pRemoteArgs->u4DiffServId = pLocalArgs->u4DiffServId;
            pRemoteArgs->u4StatsType = pLocalArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValDiffServCounter),
                        (pLocalArgs->pu8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_CO_S_Q_STATS:
        {
            tQosxNpWrQoSHwGetCoSQStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetCoSQStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetCoSQStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCoSQStats;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4QId = pLocalArgs->u4QId;
            pRemoteArgs->u4StatsType = pLocalArgs->u4StatsType;

            if (pLocalArgs->pu8CoSQStatsCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8CoSQStatsCounter),
                        (pLocalArgs->pu8CoSQStatsCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_COUNT_ACT_STATS:
        {
            tQosxNpWrQoSHwGetCountActStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetCountActStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetCountActStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCountActStats;
            pRemoteArgs->u4DiffServId = pLocalArgs->u4DiffServId;
            pRemoteArgs->u4StatsType = pLocalArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValDiffServCounter),
                        (pLocalArgs->pu8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_METER_STATS:
        {
            tQosxNpWrQoSHwGetMeterStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetMeterStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetMeterStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetMeterStats;
            pRemoteArgs->u4MeterId = pLocalArgs->u4MeterId;
            pRemoteArgs->u4StatsType = pLocalArgs->u4StatsType;
            if (pLocalArgs->pu8MeterStatsCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8MeterStatsCounter),
                        (pLocalArgs->pu8MeterStatsCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_RANDOM_DROP_STATS:
        {
            tQosxNpWrQoSHwGetRandomDropStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetRandomDropStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetRandomDropStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetRandomDropStats;
            pRemoteArgs->u4DiffServId = pLocalArgs->u4DiffServId;
            pRemoteArgs->u4StatsType = pLocalArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValDiffServCounter),
                        (pLocalArgs->pu8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_INIT:
        {
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSHwMapClassToPolicy *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToPolicy;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToPolicy;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
            MEMCPY (&(pRemoteArgs->PlyMapEntry), (pLocalArgs->pPlyMapEntry),
                    sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pInProActEntry != NULL)
            {
            MEMCPY (&(pRemoteArgs->InProActEntry), (pLocalArgs->pInProActEntry),
                    sizeof (tQoSInProfileActionEntry));
            }
            if (pLocalArgs->pOutProActEntry != NULL)
            {
            MEMCPY (&(pRemoteArgs->OutProActEntry),
                    (pLocalArgs->pOutProActEntry),
                    sizeof (tQoSOutProfileActionEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
            MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                    sizeof (tQoSMeterEntry));
            }
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxNpWrQoSHwMapClassToQueue *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueue *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueue;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueue;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4ClsOrPriType = pLocalArgs->i4ClsOrPriType;
            pRemoteArgs->u4ClsOrPri = pLocalArgs->u4ClsOrPri;
            pRemoteArgs->u4QId = pLocalArgs->u4QId;
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxNpWrQoSHwMapClassToQueueId *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueueId *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueueId;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueueId;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4ClsOrPriType = pLocalArgs->i4ClsOrPriType;
            pRemoteArgs->u4ClsOrPri = pLocalArgs->u4ClsOrPri;
            pRemoteArgs->u4QId = pLocalArgs->u4QId;
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_METER_CREATE:
        {
            tQosxNpWrQoSHwMeterCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMeterCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMeterCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterCreate;
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            break;
        }
        case QO_S_HW_METER_DELETE:
        {
            tQosxNpWrQoSHwMeterDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMeterDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMeterDelete;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterDelete;
            pRemoteArgs->i4MeterId = pLocalArgs->i4MeterId;
            break;
        }
        case QO_S_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSHwQueueCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwQueueCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwQueueCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueCreate;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4QId = pLocalArgs->u4QId;
	    if (pLocalArgs->pQEntry != NULL)
	    {
		if (pLocalArgs->pQEntry->pShapePtr != NULL)
		{
		    MEMCPY (&(pRemoteArgs->QEntry.ShapePtr),
			    (pLocalArgs->pQEntry->pShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}

		if (pLocalArgs->pQEntry->pSchedPtr != NULL)
		{
		    if (pLocalArgs->pQEntry->pSchedPtr->pShapePtr != NULL)
		    {
			MEMCPY (&(pRemoteArgs->QEntry.SchedPtr.ShapePtr),
				(pLocalArgs->pQEntry->pSchedPtr->pShapePtr),
				sizeof (tQoSShapeCfgEntry));
		    }
		    pRemoteArgs->QEntry.SchedPtr.i4QosIfIndex =
			pLocalArgs->pQEntry->pSchedPtr->i4QosIfIndex;
		    pRemoteArgs->QEntry.SchedPtr.i4QosSchedulerId =
			pLocalArgs->pQEntry->pSchedPtr->i4QosSchedulerId;
		    pRemoteArgs->QEntry.SchedPtr.u4QosSchedHwId =
			pLocalArgs->pQEntry->pSchedPtr->u4QosSchedHwId;
		    pRemoteArgs->QEntry.SchedPtr.u4QosSchedChildren =
			pLocalArgs->pQEntry->pSchedPtr->u4QosSchedChildren;
		    pRemoteArgs->QEntry.SchedPtr.u1QosSchedAlgo =
			pLocalArgs->pQEntry->pSchedPtr->u1QosSchedAlgo;
		    pRemoteArgs->QEntry.SchedPtr.u1QosSchedulerStatus =
			pLocalArgs->pQEntry->pSchedPtr->u1QosSchedulerStatus;
		    pRemoteArgs->QEntry.SchedPtr.u1HL =
			pLocalArgs->pQEntry->pSchedPtr->u1HL;
		    pRemoteArgs->QEntry.SchedPtr.u1Flag =
			pLocalArgs->pQEntry->pSchedPtr->u1Flag;
		}
		pRemoteArgs->QEntry.i4QosQId = pLocalArgs->pQEntry->i4QosQId;
		pRemoteArgs->QEntry.i4QosSchedulerId =
		    pLocalArgs->pQEntry->i4QosSchedulerId;
		pRemoteArgs->QEntry.i4QosQueueHwId =
		    pLocalArgs->pQEntry->i4QosQueueHwId;
		pRemoteArgs->QEntry.u4QueueType = pLocalArgs->pQEntry->u4QueueType;
		pRemoteArgs->QEntry.u2QosQWeight =
		    pLocalArgs->pQEntry->u2QosQWeight;
		pRemoteArgs->QEntry.u1QosQPriority =
		    pLocalArgs->pQEntry->u1QosQPriority;
		pRemoteArgs->QEntry.u1QosQStatus =
		    pLocalArgs->pQEntry->u1QosQStatus;
	    }

            if (pLocalArgs->pQTypeEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QTypeEntry), (pLocalArgs->pQTypeEntry),
                        sizeof (tQoSQtypeEntry));
            }
            if (pLocalArgs->papRDCfgEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->ApRDCfgEntry),
                        (pLocalArgs->papRDCfgEntry),
                        sizeof (tQoSREDCfgEntry *));
            }
            pRemoteArgs->i2HL = pLocalArgs->i2HL;
            break;
        }
        case QO_S_HW_QUEUE_DELETE:
        {
            tQosxNpWrQoSHwQueueDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwQueueDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwQueueDelete;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueDelete;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4Id = pLocalArgs->u4Id;
            break;
        }
        case QO_S_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSHwSchedulerAdd *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerAdd *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerAdd;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerAdd;
	    if (pLocalArgs->pSchedEntry != NULL)
	    {
		if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
		{
		    MEMCPY (&(pRemoteArgs->SchedEntry.ShapePtr),
			    (pLocalArgs->pSchedEntry->pShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}
		pRemoteArgs->SchedEntry.i4QosIfIndex =
		    pLocalArgs->pSchedEntry->i4QosIfIndex;
		pRemoteArgs->SchedEntry.i4QosSchedulerId =
		    pLocalArgs->pSchedEntry->i4QosSchedulerId;
		pRemoteArgs->SchedEntry.u4QosSchedHwId =
		    pLocalArgs->pSchedEntry->u4QosSchedHwId;
		pRemoteArgs->SchedEntry.u4QosSchedChildren =
		    pLocalArgs->pSchedEntry->u4QosSchedChildren;
		pRemoteArgs->SchedEntry.u1QosSchedAlgo =
		    pLocalArgs->pSchedEntry->u1QosSchedAlgo;
		pRemoteArgs->SchedEntry.u1QosSchedulerStatus =
		    pLocalArgs->pSchedEntry->u1QosSchedulerStatus;
		pRemoteArgs->SchedEntry.u1HL = pLocalArgs->pSchedEntry->u1HL;
		pRemoteArgs->SchedEntry.u1Flag = pLocalArgs->pSchedEntry->u1Flag;
	    }

            break;
        }
        case QO_S_HW_SCHEDULER_DELETE:
        {
            tQosxNpWrQoSHwSchedulerDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerDelete;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerDelete;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4SchedId = pLocalArgs->u4SchedId;
            break;
        }
        case QO_S_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSHwSchedulerHierarchyMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerHierarchyMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerHierarchyMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerHierarchyMap;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->u4SchedId = pLocalArgs->u4SchedId;
            pRemoteArgs->u2Sweight = pLocalArgs->u2Sweight;
            pRemoteArgs->u1Spriority = pLocalArgs->u1Spriority;
            pRemoteArgs->u4NextSchedId = pLocalArgs->u4NextSchedId;
            pRemoteArgs->u4NextQId = pLocalArgs->u4NextQId;
            pRemoteArgs->i2HL = pLocalArgs->i2HL;
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSHwSchedulerUpdateParams *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerUpdateParams *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerUpdateParams;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerUpdateParams;

            if (pLocalArgs->pSchedEntry != NULL)
            {
                if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->SchedEntry.ShapePtr),
                            (pLocalArgs->pSchedEntry->pShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                }
            pRemoteArgs->SchedEntry.i4QosIfIndex =
                pLocalArgs->pSchedEntry->i4QosIfIndex;
            pRemoteArgs->SchedEntry.i4QosSchedulerId =
                pLocalArgs->pSchedEntry->i4QosSchedulerId;
            pRemoteArgs->SchedEntry.u4QosSchedHwId =
                pLocalArgs->pSchedEntry->u4QosSchedHwId;
            pRemoteArgs->SchedEntry.u4QosSchedChildren =
                pLocalArgs->pSchedEntry->u4QosSchedChildren;
            pRemoteArgs->SchedEntry.u1QosSchedAlgo =
                pLocalArgs->pSchedEntry->u1QosSchedAlgo;
            pRemoteArgs->SchedEntry.u1QosSchedulerStatus =
                pLocalArgs->pSchedEntry->u1QosSchedulerStatus;
            pRemoteArgs->SchedEntry.u1HL = pLocalArgs->pSchedEntry->u1HL;
            pRemoteArgs->SchedEntry.u1Flag = pLocalArgs->pSchedEntry->u1Flag;
            }

            break;
        }
        case QO_S_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxNpWrQoSHwSetCpuRateLimit *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetCpuRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetCpuRateLimit;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetCpuRateLimit;
            pRemoteArgs->i4CpuQueueId = pLocalArgs->i4CpuQueueId;
            pRemoteArgs->u4MinRate = pLocalArgs->u4MinRate;
            pRemoteArgs->u4MaxRate = pLocalArgs->u4MaxRate;
            break;
        }
        case QO_S_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSHwSetDefUserPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetDefUserPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetDefUserPriority;
            pRemoteArgs->i4Port = pLocalArgs->i4Port;
            pRemoteArgs->i4DefPriority = pLocalArgs->i4DefPriority;
            break;
        }
        case QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSHwSetPbitPreferenceOverDscp *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetPbitPreferenceOverDscp;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->
                QosxRemoteNpQoSHwSetPbitPreferenceOverDscp;
            pRemoteArgs->i4Port = pLocalArgs->i4Port;
            pRemoteArgs->i4PbitPref = pLocalArgs->i4PbitPref;
            break;
        }
        case QO_S_HW_UNMAP_CLASS_FROM_POLICY:
        {
            tQosxNpWrQoSHwUnmapClassFromPolicy *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwUnmapClassFromPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwUnmapClassFromPolicy;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUnmapClassFromPolicy;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->PlyMapEntry), (pLocalArgs->pPlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxNpWrQoSHwUpdatePolicyMapForClass *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwUpdatePolicyMapForClass;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUpdatePolicyMapForClass;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL2Filter(&pRemoteArgs->ClsMapEntry.L2FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyLocalToRemoteL3Filter(&pRemoteArgs->ClsMapEntry.L3FilterPtr,
                                                      pLocalArgs->pClsMapEntry->pL3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY (&(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            (pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pRemoteArgs->ClsMapEntry.u4QoSMFClass =
                    pLocalArgs->pClsMapEntry->u4QoSMFClass;
                pRemoteArgs->ClsMapEntry.u1QoSMFCStatus =
                    pLocalArgs->pClsMapEntry->u1QoSMFCStatus;
                pRemoteArgs->ClsMapEntry.u1PreColor =
                    pLocalArgs->pClsMapEntry->u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->PlyMapEntry), (pLocalArgs->pPlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pInProActEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->InProActEntry),
                        (pLocalArgs->pInProActEntry),
                        sizeof (tQoSInProfileActionEntry));
            }
            if (pLocalArgs->pOutProActEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->OutProActEntry),
                        (pLocalArgs->pOutProActEntry),
                        sizeof (tQoSOutProfileActionEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->MeterEntry), (pLocalArgs->pMeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSHwMapClasstoPriMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClasstoPriMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClasstoPriMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClasstoPriMap;
            if (pLocalArgs->pQosClassToPriMapEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosClassToPriMapEntry),
                        pLocalArgs->pQosClassToPriMapEntry,
                        sizeof (tQosClassToPriMapEntry));
            }
            break;
        }
        case QOS_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosHwMapClassToIntPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQosHwMapClassToIntPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQosHwMapClassToIntPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQosHwMapClassToIntPriority;
            if (pLocalArgs->pQosClassToIntPriEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosClassToIntPriEntry),
                        pLocalArgs->pQosClassToIntPriEntry,
                        sizeof (tQosClassToIntPriEntry));
            }
            break;
        }

        case QO_S_HW_SET_VLAN_QUEUING_STATUS:
        {
            tQosxNpWrQoSHwSetVlanQueuingStatus *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetVlanQueuingStatus *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetVlanQueuingStatus;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetVlanQueuingStatus;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            if (pLocalArgs->pQEntrySubscriber != NULL)
            {
                MEMCPY (&(pRemoteArgs->QEntrySubscriber),
                        (pLocalArgs->pQEntrySubscriber), sizeof (tQoSQEntry));
            }
            break;
        }
        case FS_QOS_HW_GET_PFC_STATS:
        {
            tQosxNpWrFsQosHwGetPfcStats *pLocalArgs = NULL;
            tQosxRemoteNpWrFsQosHwGetPfcStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpFsQosHwGetPfcStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwGetPfcStats;
            if (pLocalArgs->pQosPfcHwStats != NULL)
            {
                MEMCPY (&(pRemoteArgs->QosPfcHwStats),
                        (pLocalArgs->pQosPfcHwStats), sizeof (tQosPfcHwStats));
            }
            break;
        }

#ifdef MBSM_WANTED 
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE:
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID:
        case QO_S_MBSM_HW_QUEUE_CREATE:
        case QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        case FS_QOS_MBSM_HW_CONFIG_PFC:
        case QO_S_MBSM_HW_SCHEDULER_ADD:
        case QO_S_MBSM_HW_METER_CREATE:
        case QO_S_MBSM_HW_INIT:
        case QO_S_MBSM_HW_SET_CPU_RATE_LIMIT:
        case QO_S_MBSM_HW_MAP_CLASS_TO_POLICY:
        case QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP:
        case QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS:
        case QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        case QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP:
        case QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY:
        case QO_S_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            break;
        }
#endif /* MBSM_WANTED */ 
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    return (u1RetVal);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilQosxConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilQosxConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxRemoteNpModInfo *pQosxRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pQosxNpModInfo = &(pFsHwNp->QosxNpModInfo);
    pQosxRemoteNpModInfo = &(pRemoteHwNp->QosxRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_QOS_HW_CONFIG_PFC:
        {
            tQosxNpWrFsQosHwConfigPfc *pLocalArgs = NULL;
            tQosxRemoteNpWrFsQosHwConfigPfc *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpFsQosHwConfigPfc;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwConfigPfc;
            if (pLocalArgs->pQosPfcHwEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pQosPfcHwEntry),
                        &(pRemoteArgs->QosPfcHwEntry), sizeof (tQosPfcHwEntry));
            }
            break;
        }
        case QO_S_HW_DELETE_CLASS_MAP_ENTRY:
        {
            tQosxNpWrQoSHwDeleteClassMapEntry *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwDeleteClassMapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwDeleteClassMapEntry;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwDeleteClassMapEntry;
            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL2Filter(pLocalArgs->pClsMapEntry->pL2FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL3Filter(pLocalArgs->pClsMapEntry->pL3FilterPtr,                     
                             &pRemoteArgs->ClsMapEntry.L3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            &(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pLocalArgs->pClsMapEntry->u4QoSMFClass =
                    pRemoteArgs->ClsMapEntry.u4QoSMFClass;
                pLocalArgs->pClsMapEntry->u1QoSMFCStatus =
                    pRemoteArgs->ClsMapEntry.u1QoSMFCStatus;
                pLocalArgs->pClsMapEntry->u1PreColor =
                    pRemoteArgs->ClsMapEntry.u1PreColor;
            }
            break;
        }
        case QO_S_HW_GET_ALG_DROP_STATS:
        {
            tQosxNpWrQoSHwGetAlgDropStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetAlgDropStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetAlgDropStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetAlgDropStats;
            pLocalArgs->u4DiffServId = pRemoteArgs->u4DiffServId;
            pLocalArgs->u4StatsType = pRemoteArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValDiffServCounter),
                        &(pRemoteArgs->U8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_CO_S_Q_STATS:
        {
            tQosxNpWrQoSHwGetCoSQStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetCoSQStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetCoSQStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCoSQStats;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4QId = pRemoteArgs->u4QId;
            pLocalArgs->u4StatsType = pRemoteArgs->u4StatsType;
            if (pLocalArgs->pu8CoSQStatsCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu8CoSQStatsCounter),
                        &(pRemoteArgs->U8CoSQStatsCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_COUNT_ACT_STATS:
        {
            tQosxNpWrQoSHwGetCountActStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetCountActStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetCountActStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCountActStats;
            pLocalArgs->u4DiffServId = pRemoteArgs->u4DiffServId;
            pLocalArgs->u4StatsType = pRemoteArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValDiffServCounter),
                        &(pRemoteArgs->U8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_METER_STATS:
        {
            tQosxNpWrQoSHwGetMeterStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetMeterStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetMeterStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetMeterStats;
            pLocalArgs->u4MeterId = pRemoteArgs->u4MeterId;
            pLocalArgs->u4StatsType = pRemoteArgs->u4StatsType;
            if (pLocalArgs->pu8MeterStatsCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu8MeterStatsCounter),
                        &(pRemoteArgs->U8MeterStatsCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_GET_RANDOM_DROP_STATS:
        {
            tQosxNpWrQoSHwGetRandomDropStats *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwGetRandomDropStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwGetRandomDropStats;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetRandomDropStats;
            pLocalArgs->u4DiffServId = pRemoteArgs->u4DiffServId;
            pLocalArgs->u4StatsType = pRemoteArgs->u4StatsType;
            if (pLocalArgs->pu8RetValDiffServCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValDiffServCounter),
                        &(pRemoteArgs->U8RetValDiffServCounter),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case QO_S_HW_INIT:
        {
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSHwMapClassToPolicy *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToPolicy;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToPolicy;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL2Filter(pLocalArgs->pClsMapEntry->pL2FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL3Filter(pLocalArgs->pClsMapEntry->pL3FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            &(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pLocalArgs->pClsMapEntry->u4QoSMFClass =
                    pRemoteArgs->ClsMapEntry.u4QoSMFClass;
                pLocalArgs->pClsMapEntry->u1QoSMFCStatus =
                    pRemoteArgs->ClsMapEntry.u1QoSMFCStatus;
                pLocalArgs->pClsMapEntry->u1PreColor =
                    pRemoteArgs->ClsMapEntry.u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pPlyMapEntry), &(pRemoteArgs->PlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pInProActEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pInProActEntry),
                        &(pRemoteArgs->InProActEntry),
                        sizeof (tQoSInProfileActionEntry));
            }
            if (pLocalArgs->pOutProActEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pOutProActEntry),
                        &(pRemoteArgs->OutProActEntry),
                        sizeof (tQoSOutProfileActionEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pMeterEntry), &(pRemoteArgs->MeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxNpWrQoSHwMapClassToQueue *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueue *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueue;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueue;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4ClsOrPriType = pRemoteArgs->i4ClsOrPriType;
            pLocalArgs->u4ClsOrPri = pRemoteArgs->u4ClsOrPri;
            pLocalArgs->u4QId = pRemoteArgs->u4QId;
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxNpWrQoSHwMapClassToQueueId *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClassToQueueId *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueueId;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueueId;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL2Filter(pLocalArgs->pClsMapEntry->pL2FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L2FilterPtr);

                    if (pLocalArgs->pClsMapEntry->pL2FilterPtr->pIssFilterShadowInfo != NULL)
                    {
                        MEMCPY(pLocalArgs->pClsMapEntry->pL2FilterPtr->pIssFilterShadowInfo,
                               &(pRemoteArgs->ClsMapEntry.L2FilterPtr.IssFilterShadowInfo),
                                sizeof (tIssFilterShadowTable));
                    }
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL3Filter(pLocalArgs->pClsMapEntry->pL3FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L3FilterPtr);

                    if (pLocalArgs->pClsMapEntry->pL3FilterPtr->pIssFilterShadowInfo != NULL)
                    {
                        MEMCPY(pLocalArgs->pClsMapEntry->pL3FilterPtr->pIssFilterShadowInfo,
                               &(pRemoteArgs->ClsMapEntry.L3FilterPtr.IssFilterShadowInfo),
                                sizeof (tIssFilterShadowTable));
                    }

                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            &(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pLocalArgs->pClsMapEntry->u4QoSMFClass =
                    pRemoteArgs->ClsMapEntry.u4QoSMFClass;
                pLocalArgs->pClsMapEntry->u1QoSMFCStatus =
                    pRemoteArgs->ClsMapEntry.u1QoSMFCStatus;
                pLocalArgs->pClsMapEntry->u1PreColor =
                    pRemoteArgs->ClsMapEntry.u1PreColor;
            }

            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4ClsOrPriType = pRemoteArgs->i4ClsOrPriType;
            pLocalArgs->u4ClsOrPri = pRemoteArgs->u4ClsOrPri;
            pLocalArgs->u4QId = pRemoteArgs->u4QId;
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_METER_CREATE:
        {
            tQosxNpWrQoSHwMeterCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMeterCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMeterCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterCreate;
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pMeterEntry), &(pRemoteArgs->MeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            break;
        }
        case QO_S_HW_METER_DELETE:
        {
            tQosxNpWrQoSHwMeterDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMeterDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMeterDelete;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterDelete;
            pLocalArgs->i4MeterId = pRemoteArgs->i4MeterId;
            break;
        }
        case QO_S_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSHwQueueCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwQueueCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwQueueCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueCreate;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4QId = pRemoteArgs->u4QId;
	    if (pLocalArgs->pQEntry != NULL)
	    {
		if (pLocalArgs->pQEntry->pShapePtr != NULL)
		{
		    MEMCPY ((pLocalArgs->pQEntry->pShapePtr),
			    &(pRemoteArgs->QEntry.ShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}

		if (pLocalArgs->pQEntry->pShapePtr != NULL)
		{
		    MEMCPY ((pLocalArgs->pQEntry->pSchedPtr->pShapePtr),
			    &(pRemoteArgs->QEntry.SchedPtr.ShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		    pLocalArgs->pQEntry->pSchedPtr->i4QosIfIndex =
			pRemoteArgs->QEntry.SchedPtr.i4QosIfIndex;
		    pLocalArgs->pQEntry->pSchedPtr->i4QosSchedulerId =
			pRemoteArgs->QEntry.SchedPtr.i4QosSchedulerId;
		    pLocalArgs->pQEntry->pSchedPtr->u4QosSchedHwId =
			pRemoteArgs->QEntry.SchedPtr.u4QosSchedHwId;
		    pLocalArgs->pQEntry->pSchedPtr->u4QosSchedChildren =
			pRemoteArgs->QEntry.SchedPtr.u4QosSchedChildren;
		    pLocalArgs->pQEntry->pSchedPtr->u1QosSchedAlgo =
			pRemoteArgs->QEntry.SchedPtr.u1QosSchedAlgo;
		    pLocalArgs->pQEntry->pSchedPtr->u1QosSchedulerStatus =
			pRemoteArgs->QEntry.SchedPtr.u1QosSchedulerStatus;
		    pLocalArgs->pQEntry->pSchedPtr->u1HL =
			pRemoteArgs->QEntry.SchedPtr.u1HL;
		    pLocalArgs->pQEntry->pSchedPtr->u1Flag =
			pRemoteArgs->QEntry.SchedPtr.u1Flag;
		}

		pLocalArgs->pQEntry->i4QosQId = pRemoteArgs->QEntry.i4QosQId;
		pLocalArgs->pQEntry->i4QosSchedulerId =
		    pRemoteArgs->QEntry.i4QosSchedulerId;
		pLocalArgs->pQEntry->i4QosQueueHwId =
		    pRemoteArgs->QEntry.i4QosQueueHwId;
		pLocalArgs->pQEntry->u4QueueType = pRemoteArgs->QEntry.u4QueueType;
		pLocalArgs->pQEntry->u2QosQWeight =
		    pRemoteArgs->QEntry.u2QosQWeight;
		pLocalArgs->pQEntry->u1QosQPriority =
		    pRemoteArgs->QEntry.u1QosQPriority;
		pLocalArgs->pQEntry->u1QosQStatus =
		    pRemoteArgs->QEntry.u1QosQStatus;
	    }
	
            if (pLocalArgs->pQTypeEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pQTypeEntry), &(pRemoteArgs->QTypeEntry),
                        sizeof (tQoSQtypeEntry));
            }
            if (pLocalArgs->papRDCfgEntry != NULL)
            {
                MEMCPY ((pLocalArgs->papRDCfgEntry),
                        &(pRemoteArgs->ApRDCfgEntry),
                        sizeof (tQoSREDCfgEntry *));
            }
            pLocalArgs->i2HL = pRemoteArgs->i2HL;
            break;
        }
        case QO_S_HW_QUEUE_DELETE:
        {
            tQosxNpWrQoSHwQueueDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwQueueDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwQueueDelete;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueDelete;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4Id = pRemoteArgs->u4Id;
            break;
        }
        case QO_S_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSHwSchedulerAdd *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerAdd *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerAdd;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerAdd;
	    if (pLocalArgs->pSchedEntry != NULL)
	    {
		if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
		{
		    MEMCPY ((pLocalArgs->pSchedEntry->pShapePtr),
			    &(pRemoteArgs->SchedEntry.ShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}
		pLocalArgs->pSchedEntry->i4QosIfIndex =
		    pRemoteArgs->SchedEntry.i4QosIfIndex;
		pLocalArgs->pSchedEntry->i4QosSchedulerId =
		    pRemoteArgs->SchedEntry.i4QosSchedulerId;
		pLocalArgs->pSchedEntry->u4QosSchedHwId =
		    pRemoteArgs->SchedEntry.u4QosSchedHwId;
		pLocalArgs->pSchedEntry->u4QosSchedChildren =
		    pRemoteArgs->SchedEntry.u4QosSchedChildren;
		pLocalArgs->pSchedEntry->u1QosSchedAlgo =
		    pRemoteArgs->SchedEntry.u1QosSchedAlgo;
		pLocalArgs->pSchedEntry->u1QosSchedulerStatus =
		    pRemoteArgs->SchedEntry.u1QosSchedulerStatus;
		pLocalArgs->pSchedEntry->u1HL = pRemoteArgs->SchedEntry.u1HL;
		pLocalArgs->pSchedEntry->u1Flag = pRemoteArgs->SchedEntry.u1Flag;
	    }

            break;
        }
        case QO_S_HW_SCHEDULER_DELETE:
        {
            tQosxNpWrQoSHwSchedulerDelete *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerDelete *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerDelete;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerDelete;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4SchedId = pRemoteArgs->u4SchedId;
            break;
        }
        case QO_S_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSHwSchedulerHierarchyMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerHierarchyMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerHierarchyMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerHierarchyMap;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4SchedId = pRemoteArgs->u4SchedId;
            pLocalArgs->u2Sweight = pRemoteArgs->u2Sweight;
            pLocalArgs->u1Spriority = pRemoteArgs->u1Spriority;
            pLocalArgs->u4NextSchedId = pRemoteArgs->u4NextSchedId;
            pLocalArgs->u4NextQId = pRemoteArgs->u4NextQId;
            pLocalArgs->i2HL = pRemoteArgs->i2HL;
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSHwSchedulerUpdateParams *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerUpdateParams *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSchedulerUpdateParams;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerUpdateParams;

	    if (pLocalArgs->pSchedEntry != NULL)
	    {
		if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
		{
		    MEMCPY ((pLocalArgs->pSchedEntry->pShapePtr),
			    &(pRemoteArgs->SchedEntry.ShapePtr),
			    sizeof (tQoSShapeCfgEntry));
		}
		pLocalArgs->pSchedEntry->i4QosIfIndex =
		    pRemoteArgs->SchedEntry.i4QosIfIndex;
		pLocalArgs->pSchedEntry->i4QosSchedulerId =
		    pRemoteArgs->SchedEntry.i4QosSchedulerId;
		pLocalArgs->pSchedEntry->u4QosSchedHwId =
		    pRemoteArgs->SchedEntry.u4QosSchedHwId;
		pLocalArgs->pSchedEntry->u4QosSchedChildren =
		    pRemoteArgs->SchedEntry.u4QosSchedChildren;
		pLocalArgs->pSchedEntry->u1QosSchedAlgo =
		    pRemoteArgs->SchedEntry.u1QosSchedAlgo;
		pLocalArgs->pSchedEntry->u1QosSchedulerStatus =
		    pRemoteArgs->SchedEntry.u1QosSchedulerStatus;
		pLocalArgs->pSchedEntry->u1HL = pRemoteArgs->SchedEntry.u1HL;
		pLocalArgs->pSchedEntry->u1Flag = pRemoteArgs->SchedEntry.u1Flag;
	    }

            break;
        }
        case QO_S_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxNpWrQoSHwSetCpuRateLimit *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetCpuRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetCpuRateLimit;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetCpuRateLimit;
            pLocalArgs->i4CpuQueueId = pRemoteArgs->i4CpuQueueId;
            pLocalArgs->u4MinRate = pRemoteArgs->u4MinRate;
            pLocalArgs->u4MaxRate = pRemoteArgs->u4MaxRate;
            break;
        }
        case QO_S_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSHwSetDefUserPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetDefUserPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetDefUserPriority;
            pLocalArgs->i4Port = pRemoteArgs->i4Port;
            pLocalArgs->i4DefPriority = pRemoteArgs->i4DefPriority;
            break;
        }
        case QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSHwSetPbitPreferenceOverDscp *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetPbitPreferenceOverDscp;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->
                QosxRemoteNpQoSHwSetPbitPreferenceOverDscp;
            pLocalArgs->i4Port = pRemoteArgs->i4Port;
            pLocalArgs->i4PbitPref = pRemoteArgs->i4PbitPref;
            break;
        }
        case QO_S_HW_UNMAP_CLASS_FROM_POLICY:
        {
            tQosxNpWrQoSHwUnmapClassFromPolicy *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwUnmapClassFromPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwUnmapClassFromPolicy;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUnmapClassFromPolicy;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL2Filter(pLocalArgs->pClsMapEntry->pL2FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL3Filter(pLocalArgs->pClsMapEntry->pL3FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            &(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pLocalArgs->pClsMapEntry->u4QoSMFClass =
                    pRemoteArgs->ClsMapEntry.u4QoSMFClass;
                pLocalArgs->pClsMapEntry->u1QoSMFCStatus =
                    pRemoteArgs->ClsMapEntry.u1QoSMFCStatus;
                pLocalArgs->pClsMapEntry->u1PreColor =
                    pRemoteArgs->ClsMapEntry.u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pPlyMapEntry), &(pRemoteArgs->PlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pMeterEntry), &(pRemoteArgs->MeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxNpWrQoSHwUpdatePolicyMapForClass *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwUpdatePolicyMapForClass;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUpdatePolicyMapForClass;

            if (pLocalArgs->pClsMapEntry != NULL)
            {
                if (pLocalArgs->pClsMapEntry->pL2FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL2Filter(pLocalArgs->pClsMapEntry->pL2FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L2FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pL3FilterPtr != NULL)
                {
                     IssCopyRemoteToLocalL3Filter(pLocalArgs->pClsMapEntry->pL3FilterPtr,
                             &pRemoteArgs->ClsMapEntry.L3FilterPtr);
                }
                if (pLocalArgs->pClsMapEntry->pPriorityMapPtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pClsMapEntry->pPriorityMapPtr),
                            &(pRemoteArgs->ClsMapEntry.PriorityMapPtr),
                            sizeof (tQoSPriorityMapEntry));
                }
                pLocalArgs->pClsMapEntry->u4QoSMFClass =
                    pRemoteArgs->ClsMapEntry.u4QoSMFClass;
                pLocalArgs->pClsMapEntry->u1QoSMFCStatus =
                    pRemoteArgs->ClsMapEntry.u1QoSMFCStatus;
                pLocalArgs->pClsMapEntry->u1PreColor =
                    pRemoteArgs->ClsMapEntry.u1PreColor;
            }

            if (pLocalArgs->pPlyMapEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pPlyMapEntry), &(pRemoteArgs->PlyMapEntry),
                        sizeof (tQoSPolicyMapEntry));
            }
            if (pLocalArgs->pInProActEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pInProActEntry),
                        &(pRemoteArgs->InProActEntry),
                        sizeof (tQoSInProfileActionEntry));
            }
            if (pLocalArgs->pOutProActEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pOutProActEntry),
                        &(pRemoteArgs->OutProActEntry),
                        sizeof (tQoSOutProfileActionEntry));
            }
            if (pLocalArgs->pMeterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pMeterEntry), &(pRemoteArgs->MeterEntry),
                        sizeof (tQoSMeterEntry));
            }
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case QO_S_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSHwMapClasstoPriMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClasstoPriMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwMapClasstoPriMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClasstoPriMap;
            if (pLocalArgs->pQosClassToPriMapEntry != NULL)
            {
                MEMCPY (pLocalArgs->pQosClassToPriMapEntry,
                        &(pRemoteArgs->QosClassToPriMapEntry),
                        sizeof (tQosClassToPriMapEntry));
            }
            break;
        }
        case QOS_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosHwMapClassToIntPriority *pLocalArgs = NULL;
            tQosxRemoteNpWrQosHwMapClassToIntPriority *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQosHwMapClassToIntPriority;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQosHwMapClassToIntPriority;
            if (pLocalArgs->pQosClassToIntPriEntry != NULL)
            {
                MEMCPY (pLocalArgs->pQosClassToIntPriEntry,
                        &(pRemoteArgs->QosClassToIntPriEntry),
                        sizeof (tQosClassToIntPriEntry));
            }
            break;
        }

        case QO_S_HW_SET_VLAN_QUEUING_STATUS:
        {
            tQosxNpWrQoSHwSetVlanQueuingStatus *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSetVlanQueuingStatus *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSHwSetVlanQueuingStatus;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetVlanQueuingStatus;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            if (pLocalArgs->pQEntrySubscriber != NULL)
            {
                MEMCPY ((pLocalArgs->pQEntrySubscriber),
                        &(pRemoteArgs->QEntrySubscriber), sizeof (tQoSQEntry));
            }
            break;
        }
        case FS_QOS_HW_GET_PFC_STATS:
        {
            tQosxNpWrFsQosHwGetPfcStats *pLocalArgs = NULL;
            tQosxRemoteNpWrFsQosHwGetPfcStats *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpFsQosHwGetPfcStats;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwGetPfcStats;
            if (pLocalArgs->pQosPfcHwStats != NULL)
            {
                MEMCPY ((pLocalArgs->pQosPfcHwStats),
                        &(pRemoteArgs->QosPfcHwStats), sizeof (tQosPfcHwStats));
            }
            break;
        }
#ifdef MBSM_WANTED

        case QO_S_MBSM_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSMbsmHwQueueCreate *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwQueueCreate *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwQueueCreate;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueCreate;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->u4QId = pRemoteArgs->u4QId;
            if (pLocalArgs->pQEntry != NULL)
            {
                if (pLocalArgs->pQEntry->pShapePtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pQEntry->pShapePtr),
                            &(pRemoteArgs->QEntry.ShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                }

                if (pLocalArgs->pQEntry->pShapePtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pQEntry->pSchedPtr->pShapePtr),
                            &(pRemoteArgs->QEntry.SchedPtr.ShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                    pLocalArgs->pQEntry->pSchedPtr->i4QosIfIndex =
                        pRemoteArgs->QEntry.SchedPtr.i4QosIfIndex;
                    pLocalArgs->pQEntry->pSchedPtr->i4QosSchedulerId =
                        pRemoteArgs->QEntry.SchedPtr.i4QosSchedulerId;
                    pLocalArgs->pQEntry->pSchedPtr->u4QosSchedHwId =
                        pRemoteArgs->QEntry.SchedPtr.u4QosSchedHwId;
                    pLocalArgs->pQEntry->pSchedPtr->u4QosSchedChildren =
                        pRemoteArgs->QEntry.SchedPtr.u4QosSchedChildren;
                    pLocalArgs->pQEntry->pSchedPtr->u1QosSchedAlgo =
                        pRemoteArgs->QEntry.SchedPtr.u1QosSchedAlgo;
                    pLocalArgs->pQEntry->pSchedPtr->u1QosSchedulerStatus =
                        pRemoteArgs->QEntry.SchedPtr.u1QosSchedulerStatus;
                    pLocalArgs->pQEntry->pSchedPtr->u1HL =
                        pRemoteArgs->QEntry.SchedPtr.u1HL;
                    pLocalArgs->pQEntry->pSchedPtr->u1Flag =
                        pRemoteArgs->QEntry.SchedPtr.u1Flag;
                }

                pLocalArgs->pQEntry->i4QosQId = pRemoteArgs->QEntry.i4QosQId;
                pLocalArgs->pQEntry->i4QosSchedulerId =
                    pRemoteArgs->QEntry.i4QosSchedulerId;
                pLocalArgs->pQEntry->i4QosQueueHwId =
                    pRemoteArgs->QEntry.i4QosQueueHwId;
                pLocalArgs->pQEntry->u4QueueType =
                    pRemoteArgs->QEntry.u4QueueType;
                pLocalArgs->pQEntry->u2QosQWeight =
                    pRemoteArgs->QEntry.u2QosQWeight;
                pLocalArgs->pQEntry->u1QosQPriority =
                    pRemoteArgs->QEntry.u1QosQPriority;
                pLocalArgs->pQEntry->u1QosQStatus =
                    pRemoteArgs->QEntry.u1QosQStatus;
            }

            if (pLocalArgs->pQTypeEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pQTypeEntry), &(pRemoteArgs->QTypeEntry),
                        sizeof (tQoSQtypeEntry));
            }
            if (pLocalArgs->papRDCfgEntry != NULL)
            {
                MEMCPY ((pLocalArgs->papRDCfgEntry),
                        &(pRemoteArgs->ApRDCfgEntry),
                        sizeof (tQoSREDCfgEntry *));
            }
            pLocalArgs->i2HL = pRemoteArgs->i2HL;
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE:
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID:
        case QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        case FS_QOS_MBSM_HW_CONFIG_PFC:
        case QO_S_MBSM_HW_METER_CREATE:
        case QO_S_MBSM_HW_INIT:
        case QO_S_MBSM_HW_SET_CPU_RATE_LIMIT:
        case QO_S_MBSM_HW_MAP_CLASS_TO_POLICY:
        case QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP:
        case QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        case QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY:
        case QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS:
        case QO_S_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSMbsmHwSchedulerAdd *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwSchedulerAdd *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerAdd;
            pRemoteArgs = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerAdd;
            if (pLocalArgs->pSchedEntry != NULL)
            {
                if (pLocalArgs->pSchedEntry->pShapePtr != NULL)
                {
                    MEMCPY ((pLocalArgs->pSchedEntry->pShapePtr),
                            &(pRemoteArgs->SchedEntry.ShapePtr),
                            sizeof (tQoSShapeCfgEntry));
                }
                pLocalArgs->pSchedEntry->i4QosIfIndex =
                    pRemoteArgs->SchedEntry.i4QosIfIndex;
                pLocalArgs->pSchedEntry->i4QosSchedulerId =
                    pRemoteArgs->SchedEntry.i4QosSchedulerId;
                pLocalArgs->pSchedEntry->u4QosSchedHwId =
                    pRemoteArgs->SchedEntry.u4QosSchedHwId;
                pLocalArgs->pSchedEntry->u4QosSchedChildren =
                    pRemoteArgs->SchedEntry.u4QosSchedChildren;
                pLocalArgs->pSchedEntry->u1QosSchedAlgo =
                    pRemoteArgs->SchedEntry.u1QosSchedAlgo;
                pLocalArgs->pSchedEntry->u1QosSchedulerStatus =
                    pRemoteArgs->SchedEntry.u1QosSchedulerStatus;
                pLocalArgs->pSchedEntry->u1HL = pRemoteArgs->SchedEntry.u1HL;
                pLocalArgs->pSchedEntry->u1Flag =
                    pRemoteArgs->SchedEntry.u1Flag;
            }
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSMbsmHwMapClasstoPriMap *pLocalArgs = NULL;
            tQosxRemoteNpWrQoSHwMapClasstoPriMap *pRemoteArgs = NULL;
            pLocalArgs = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClasstoPriMap;
            pRemoteArgs =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClasstoPriMap;
            if (pLocalArgs->pQosClassToPriMapEntry != NULL)
            {
                MEMCPY (pLocalArgs->pQosClassToPriMapEntry,
                        &(pRemoteArgs->QosClassToPriMapEntry),
                        sizeof (tQosClassToPriMapEntry));
            }
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilQosxRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRstpNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilQosxRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tQosxRemoteNpModInfo *pQosxRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pQosxRemoteNpModInfo = &(pRemoteHwNpInput->QosxRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_QOS_HW_CONFIG_PFC:
        {
            tQosxRemoteNpWrFsQosHwConfigPfc *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwConfigPfc;
            u1RetVal = FsQosHwConfigPfc (&(pInput->QosPfcHwEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_DELETE_CLASS_MAP_ENTRY:
        {
            tQosxRemoteNpWrQoSHwDeleteClassMapEntry *pInput = NULL;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;

            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwDeleteClassMapEntry;

            tQoSClassMapEntry ClsMapEntry;

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);

            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry), 
                                         &(pInput->ClsMapEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->ClsMapEntry.L3FilterPtr));

            MEMSET (&ClsMapEntry, 0, sizeof (tQoSClassMapEntry));
            ClsMapEntry.pL2FilterPtr = &IssL2FilterEntry;
            ClsMapEntry.pL3FilterPtr = &IssL3FilterEntry;
            ClsMapEntry.pPriorityMapPtr = &pInput->ClsMapEntry.PriorityMapPtr;
            ClsMapEntry.u4QoSMFClass = pInput->ClsMapEntry.u4QoSMFClass;
            ClsMapEntry.u1QoSMFCStatus = pInput->ClsMapEntry.u1QoSMFCStatus;
            ClsMapEntry.u1PreColor = pInput->ClsMapEntry.u1PreColor;

            u1RetVal = QoSHwDeleteClassMapEntry (&(ClsMapEntry));
            pInput->ClsMapEntry.u4QoSMFClass = ClsMapEntry.u4QoSMFClass;
            pInput->ClsMapEntry.u1QoSMFCStatus = ClsMapEntry.u1QoSMFCStatus;
            pInput->ClsMapEntry.u1PreColor = ClsMapEntry.u1PreColor;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_GET_ALG_DROP_STATS:
        {
            tQosxRemoteNpWrQoSHwGetAlgDropStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetAlgDropStats;
            u1RetVal =
                QoSHwGetAlgDropStats (pInput->u4DiffServId, pInput->u4StatsType,
                                      &(pInput->U8RetValDiffServCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_GET_CO_S_Q_STATS:
        {
            tQosxRemoteNpWrQoSHwGetCoSQStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCoSQStats;
            u1RetVal =
                QoSHwGetCoSQStats (pInput->i4IfIndex, pInput->u4QId,
                                   pInput->u4StatsType,
                                   &(pInput->U8CoSQStatsCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_GET_COUNT_ACT_STATS:
        {
            tQosxRemoteNpWrQoSHwGetCountActStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetCountActStats;
            u1RetVal =
                QoSHwGetCountActStats (pInput->u4DiffServId,
                                       pInput->u4StatsType,
                                       &(pInput->U8RetValDiffServCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_GET_METER_STATS:
        {
            tQosxRemoteNpWrQoSHwGetMeterStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetMeterStats;
            u1RetVal =
                QoSHwGetMeterStats (pInput->u4MeterId, pInput->u4StatsType,
                                    &(pInput->U8MeterStatsCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_GET_RANDOM_DROP_STATS:
        {
            tQosxRemoteNpWrQoSHwGetRandomDropStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwGetRandomDropStats;
            u1RetVal =
                QoSHwGetRandomDropStats (pInput->u4DiffServId,
                                         pInput->u4StatsType,
                                         &(pInput->U8RetValDiffServCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_INIT:
        {
            u1RetVal = QoSHwInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxRemoteNpWrQoSHwMapClassToPolicy *pInput = NULL;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToPolicy;

            tQoSClassMapEntry ClsMapEntry;
            MEMSET (&ClsMapEntry, 0, sizeof (tQoSClassMapEntry));

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);

            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry), 
                                         &(pInput->ClsMapEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->ClsMapEntry.L3FilterPtr));

            ClsMapEntry.pL2FilterPtr = &IssL2FilterEntry;
            ClsMapEntry.pL3FilterPtr = &IssL3FilterEntry;
            ClsMapEntry.pPriorityMapPtr = &pInput->ClsMapEntry.PriorityMapPtr;
            ClsMapEntry.u4QoSMFClass = pInput->ClsMapEntry.u4QoSMFClass;
            ClsMapEntry.u1QoSMFCStatus = pInput->ClsMapEntry.u1QoSMFCStatus;
            ClsMapEntry.u1PreColor = pInput->ClsMapEntry.u1PreColor;

            u1RetVal =
                QoSHwMapClassToPolicy (&(ClsMapEntry),
                                       &(pInput->PlyMapEntry),
                                       &(pInput->InProActEntry),
                                       &(pInput->OutProActEntry),
                                       &(pInput->MeterEntry), pInput->u1Flag);

            pInput->ClsMapEntry.u4QoSMFClass = ClsMapEntry.u4QoSMFClass;
            pInput->ClsMapEntry.u1QoSMFCStatus = ClsMapEntry.u1QoSMFCStatus;
            pInput->ClsMapEntry.u1PreColor = ClsMapEntry.u1PreColor;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxRemoteNpWrQoSHwMapClassToQueue *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueue;
            u1RetVal =
                QoSHwMapClassToQueue (pInput->i4IfIndex, pInput->i4ClsOrPriType,
                                      pInput->u4ClsOrPri, pInput->u4QId,
                                      pInput->u1Flag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxRemoteNpWrQoSHwMapClassToQueueId *pInput = NULL;
            tQoSClassMapEntry ClsMapEntry;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;

            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClassToQueueId;

            MEMSET (&ClsMapEntry, 0, sizeof (tQoSClassMapEntry));


            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);

            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry), 
                                         &(pInput->ClsMapEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry), 
                                         &(pInput->ClsMapEntry.L3FilterPtr));
            ClsMapEntry.pL2FilterPtr = &IssL2FilterEntry;
            ClsMapEntry.pL3FilterPtr = &IssL3FilterEntry;
            ClsMapEntry.pPriorityMapPtr = &pInput->ClsMapEntry.PriorityMapPtr;
            ClsMapEntry.u4QoSMFClass = pInput->ClsMapEntry.u4QoSMFClass;
            ClsMapEntry.u1QoSMFCStatus = pInput->ClsMapEntry.u1QoSMFCStatus;
            ClsMapEntry.u1PreColor = pInput->ClsMapEntry.u1PreColor;

            u1RetVal =
                QoSHwMapClassToQueueId (&(ClsMapEntry),
                                        pInput->i4IfIndex,
                                        pInput->i4ClsOrPriType,
                                        pInput->u4ClsOrPri, pInput->u4QId,
                                        pInput->u1Flag);

            pInput->ClsMapEntry.u4QoSMFClass = ClsMapEntry.u4QoSMFClass;
            pInput->ClsMapEntry.u1QoSMFCStatus = ClsMapEntry.u1QoSMFCStatus;
            pInput->ClsMapEntry.u1PreColor = ClsMapEntry.u1PreColor;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_METER_CREATE:
        {
            tQosxRemoteNpWrQoSHwMeterCreate *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterCreate;
            u1RetVal = QoSHwMeterCreate (&(pInput->MeterEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_METER_DELETE:
        {
            tQosxRemoteNpWrQoSHwMeterDelete *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMeterDelete;
            u1RetVal = QoSHwMeterDelete (pInput->i4MeterId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_QUEUE_CREATE:
        {
            tQosxRemoteNpWrQoSHwQueueCreate *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueCreate;

            tQoSQEntry QEntry;
            tQoSSchedulerEntry SchedPtr;

            QEntry.pShapePtr = &pInput->QEntry.ShapePtr;
            SchedPtr.pShapePtr = &pInput->QEntry.SchedPtr.ShapePtr;
            SchedPtr.i4QosIfIndex = pInput->QEntry.SchedPtr.i4QosIfIndex;
            SchedPtr.i4QosSchedulerId = pInput->QEntry.SchedPtr.i4QosSchedulerId;
            SchedPtr.u4QosSchedHwId = pInput->QEntry.SchedPtr.u4QosSchedHwId;
            SchedPtr.u4QosSchedChildren = pInput->QEntry.SchedPtr.u4QosSchedChildren;
            SchedPtr.u1QosSchedAlgo = pInput->QEntry.SchedPtr.u1QosSchedAlgo;
            SchedPtr.u1QosSchedulerStatus = pInput->QEntry.SchedPtr.u1QosSchedulerStatus;
            SchedPtr.u1HL = pInput->QEntry.SchedPtr.u1HL;
            SchedPtr.u1Flag = pInput->QEntry.SchedPtr.u1Flag;

            QEntry.pSchedPtr = &SchedPtr;
            QEntry.i4QosQId = pInput->QEntry.i4QosQId;
            QEntry.i4QosSchedulerId = pInput->QEntry.i4QosSchedulerId;
            QEntry.i4QosQueueHwId = pInput->QEntry.i4QosQueueHwId;
            QEntry.u4QueueType = pInput->QEntry.u4QueueType;
            QEntry.u2QosQWeight = pInput->QEntry.u2QosQWeight;
            QEntry.u1QosQPriority = pInput->QEntry.u1QosQPriority;
            QEntry.u1QosQStatus = pInput->QEntry.u1QosQStatus;
            
            u1RetVal =
                QoSHwQueueCreate (pInput->i4IfIndex, pInput->u4QId,
                                  &(QEntry), &(pInput->QTypeEntry),
                                  &(pInput->ApRDCfgEntry), pInput->i2HL);


            pInput->QEntry.SchedPtr.i4QosIfIndex = SchedPtr.i4QosIfIndex;
            pInput->QEntry.SchedPtr.i4QosSchedulerId = SchedPtr.i4QosSchedulerId;
            pInput->QEntry.SchedPtr.u4QosSchedHwId = SchedPtr.u4QosSchedHwId;
            pInput->QEntry.SchedPtr.u4QosSchedChildren = SchedPtr.u4QosSchedChildren;
            pInput->QEntry.SchedPtr.u1QosSchedAlgo = SchedPtr.u1QosSchedAlgo;
            pInput->QEntry.SchedPtr.u1QosSchedulerStatus = SchedPtr.u1QosSchedulerStatus;
            pInput->QEntry.SchedPtr.u1HL = SchedPtr.u1HL;
            pInput->QEntry.SchedPtr.u1Flag = SchedPtr.u1Flag;

            pInput->QEntry.i4QosQId = QEntry.i4QosQId;
            pInput->QEntry.i4QosSchedulerId = QEntry.i4QosSchedulerId;
            pInput->QEntry.i4QosQueueHwId = QEntry.i4QosQueueHwId;
            pInput->QEntry.u4QueueType = QEntry.u4QueueType;
            pInput->QEntry.u2QosQWeight = QEntry.u2QosQWeight;
            pInput->QEntry.u1QosQPriority = QEntry.u1QosQPriority;
            pInput->QEntry.u1QosQStatus = QEntry.u1QosQStatus;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_QUEUE_DELETE:
        {
            tQosxRemoteNpWrQoSHwQueueDelete *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwQueueDelete;
            u1RetVal = QoSHwQueueDelete (pInput->i4IfIndex, pInput->u4Id);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SCHEDULER_ADD:
        {
            tQosxRemoteNpWrQoSHwSchedulerAdd *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerAdd;

            tQoSSchedulerEntry SchedEntry;

            SchedEntry.pShapePtr = &pInput->SchedEntry.ShapePtr;
            SchedEntry.i4QosIfIndex = pInput->SchedEntry.i4QosIfIndex;
            SchedEntry.i4QosSchedulerId = pInput->SchedEntry.i4QosSchedulerId;
            SchedEntry.u4QosSchedHwId = pInput->SchedEntry.u4QosSchedHwId;
            SchedEntry.u4QosSchedChildren = pInput->SchedEntry.u4QosSchedChildren;
            SchedEntry.u1QosSchedAlgo = pInput->SchedEntry.u1QosSchedAlgo;
            SchedEntry.u1QosSchedulerStatus = pInput->SchedEntry.u1QosSchedulerStatus;
            SchedEntry.u1HL = pInput->SchedEntry.u1HL;
            SchedEntry.u1Flag = pInput->SchedEntry.u1Flag;

            u1RetVal = QoSHwSchedulerAdd (&(SchedEntry));

            pInput->SchedEntry.i4QosSchedulerId = SchedEntry.i4QosSchedulerId;
            pInput->SchedEntry.u4QosSchedHwId = SchedEntry.u4QosSchedHwId;
            pInput->SchedEntry.u4QosSchedChildren = SchedEntry.u4QosSchedChildren;
            pInput->SchedEntry.u1QosSchedAlgo = SchedEntry.u1QosSchedAlgo;
            pInput->SchedEntry.u1QosSchedulerStatus = SchedEntry.u1QosSchedulerStatus;
            pInput->SchedEntry.u1HL = SchedEntry.u1HL;
            pInput->SchedEntry.u1Flag = SchedEntry.u1Flag;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SCHEDULER_DELETE:
        {
            tQosxRemoteNpWrQoSHwSchedulerDelete *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerDelete;
            u1RetVal =
                QoSHwSchedulerDelete (pInput->i4IfIndex, pInput->u4SchedId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxRemoteNpWrQoSHwSchedulerHierarchyMap *pInput = NULL;
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerHierarchyMap;
            u1RetVal =
                QoSHwSchedulerHierarchyMap (pInput->i4IfIndex,
                                            pInput->u4SchedId,
                                            pInput->u2Sweight,
                                            pInput->u1Spriority,
                                            pInput->u4NextSchedId,
                                            pInput->u4NextQId, pInput->i2HL,
                                            pInput->u1Flag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxRemoteNpWrQoSHwSchedulerUpdateParams *pInput = NULL;
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSchedulerUpdateParams;

            tQoSSchedulerEntry SchedEntry;

            SchedEntry.pShapePtr = &pInput->SchedEntry.ShapePtr;
            SchedEntry.i4QosIfIndex = pInput->SchedEntry.i4QosIfIndex;
            SchedEntry.i4QosSchedulerId = pInput->SchedEntry.i4QosSchedulerId;
            SchedEntry.u4QosSchedHwId = pInput->SchedEntry.u4QosSchedHwId;
            SchedEntry.u4QosSchedChildren = pInput->SchedEntry.u4QosSchedChildren;
            SchedEntry.u1QosSchedAlgo = pInput->SchedEntry.u1QosSchedAlgo;
            SchedEntry.u1QosSchedulerStatus = pInput->SchedEntry.u1QosSchedulerStatus;
            SchedEntry.u1HL = pInput->SchedEntry.u1HL;
            SchedEntry.u1Flag = pInput->SchedEntry.u1Flag;

            u1RetVal = QoSHwSchedulerUpdateParams (&(SchedEntry));

            pInput->SchedEntry.i4QosSchedulerId = SchedEntry.i4QosSchedulerId;
            pInput->SchedEntry.u4QosSchedHwId = SchedEntry.u4QosSchedHwId;
            pInput->SchedEntry.u4QosSchedChildren = SchedEntry.u4QosSchedChildren;
            pInput->SchedEntry.u1QosSchedAlgo = SchedEntry.u1QosSchedAlgo;
            pInput->SchedEntry.u1QosSchedulerStatus = SchedEntry.u1QosSchedulerStatus;
            pInput->SchedEntry.u1HL = SchedEntry.u1HL;
            pInput->SchedEntry.u1Flag = SchedEntry.u1Flag;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxRemoteNpWrQoSHwSetCpuRateLimit *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetCpuRateLimit;
            u1RetVal =
                QoSHwSetCpuRateLimit (pInput->i4CpuQueueId, pInput->u4MinRate,
                                      pInput->u4MaxRate);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxRemoteNpWrQoSHwSetDefUserPriority *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetDefUserPriority;
            u1RetVal =
                QoSHwSetDefUserPriority (pInput->i4Port, pInput->i4DefPriority);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp *pInput = NULL;
            pInput =
                &pQosxRemoteNpModInfo->
                QosxRemoteNpQoSHwSetPbitPreferenceOverDscp;
            u1RetVal =
                QoSHwSetPbitPreferenceOverDscp (pInput->i4Port,
                                                pInput->i4PbitPref);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_UNMAP_CLASS_FROM_POLICY:
        {
            tQosxRemoteNpWrQoSHwUnmapClassFromPolicy *pInput = NULL;
            tQoSClassMapEntry ClsMapEntry;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;
            
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUnmapClassFromPolicy;

            MEMSET (&ClsMapEntry, 0, sizeof (tQoSClassMapEntry));

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);
            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry),
                                         &(pInput->ClsMapEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->ClsMapEntry.L3FilterPtr));

            ClsMapEntry.pL2FilterPtr = &IssL2FilterEntry;
            ClsMapEntry.pL3FilterPtr = &IssL3FilterEntry;
            ClsMapEntry.pPriorityMapPtr = &pInput->ClsMapEntry.PriorityMapPtr;
            ClsMapEntry.u4QoSMFClass = pInput->ClsMapEntry.u4QoSMFClass;
            ClsMapEntry.u1QoSMFCStatus = pInput->ClsMapEntry.u1QoSMFCStatus;
            ClsMapEntry.u1PreColor = pInput->ClsMapEntry.u1PreColor;

            u1RetVal =
                QoSHwUnmapClassFromPolicy (&(ClsMapEntry),
                                           &(pInput->PlyMapEntry),
                                           &(pInput->MeterEntry),
                                           pInput->u1Flag);

            pInput->ClsMapEntry.u4QoSMFClass = ClsMapEntry.u4QoSMFClass;
            pInput->ClsMapEntry.u1QoSMFCStatus = ClsMapEntry.u1QoSMFCStatus;
            pInput->ClsMapEntry.u1PreColor = ClsMapEntry.u1PreColor;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass *pInput = NULL;
            tQoSClassMapEntry ClsMapEntry;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;
            
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwUpdatePolicyMapForClass;

            MEMSET (&ClsMapEntry, 0, sizeof (tQoSClassMapEntry));

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);
            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry),
                                         &(pInput->ClsMapEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->ClsMapEntry.L3FilterPtr));

            ClsMapEntry.pL2FilterPtr = &IssL2FilterEntry;
            ClsMapEntry.pL3FilterPtr = &IssL3FilterEntry;
            ClsMapEntry.pPriorityMapPtr = &pInput->ClsMapEntry.PriorityMapPtr;
            ClsMapEntry.u4QoSMFClass = pInput->ClsMapEntry.u4QoSMFClass;
            ClsMapEntry.u1QoSMFCStatus = pInput->ClsMapEntry.u1QoSMFCStatus;
            ClsMapEntry.u1PreColor = pInput->ClsMapEntry.u1PreColor;

            u1RetVal =
                QoSHwUpdatePolicyMapForClass (&(ClsMapEntry),
                                              &(pInput->PlyMapEntry),
                                              &(pInput->InProActEntry),
                                              &(pInput->OutProActEntry),
                                              &(pInput->MeterEntry),
                                              pInput->u1Flag);

            pInput->ClsMapEntry.u4QoSMFClass = ClsMapEntry.u4QoSMFClass;
            pInput->ClsMapEntry.u1QoSMFCStatus = ClsMapEntry.u1QoSMFCStatus;
            pInput->ClsMapEntry.u1PreColor = ClsMapEntry.u1PreColor;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QO_S_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxRemoteNpWrQoSHwMapClasstoPriMap *pInput = NULL;
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwMapClasstoPriMap;

            u1RetVal = QoSHwMapClasstoPriMap (&(pInput->QosClassToPriMapEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case QOS_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxRemoteNpWrQosHwMapClassToIntPriority *pInput = NULL;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQosHwMapClassToIntPriority;

            tQosClassToIntPriEntry QosClassToIntPriEntry;

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL2FilterEntry, 0, sizeof(tIssL2FilterEntry));
            MEMSET (&IssL3FilterEntry, 0, sizeof(tIssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo);
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);
            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry),
                                         &(pInput->QosClassToIntPriEntry.L2FilterPtr));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->QosClassToIntPriEntry.L3FilterPtr));

            QosClassToIntPriEntry.pL2FilterPtr = &IssL2FilterEntry;
            QosClassToIntPriEntry.pL3FilterPtr = &IssL3FilterEntry;
            QosClassToIntPriEntry.u1IntPriority = pInput->QosClassToIntPriEntry.u1IntPriority;
            QosClassToIntPriEntry.u1Flag = pInput->QosClassToIntPriEntry.u1Flag;

            u1RetVal =
                QosHwMapClassToIntPriority (&(QosClassToIntPriEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case QO_S_HW_SET_VLAN_QUEUING_STATUS:
        {
            tQosxRemoteNpWrQoSHwSetVlanQueuingStatus *pInput = NULL;
            pInput =
                &pQosxRemoteNpModInfo->QosxRemoteNpQoSHwSetVlanQueuingStatus;
            u1RetVal =
                QoSHwSetVlanQueuingStatus (pInput->i4IfIndex,
                                           &(pInput->QEntrySubscriber));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_QOS_HW_GET_PFC_STATS:
        {
            tQosxRemoteNpWrFsQosHwGetPfcStats *pInput = NULL;
            pInput = &pQosxRemoteNpModInfo->QosxRemoteNpFsQosHwGetPfcStats;
            u1RetVal = FsQosHwGetPfcStats (&(pInput->QosPfcHwStats));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __QOSXNPUTIL_C__ */

