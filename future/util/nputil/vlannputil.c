/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: vlannputil.c,v 1.38 2017/11/15 12:59:54 siva Exp $
 *
 * Description:This file contains the NP utility functions of VLAN
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __VLANUTIL_C__
#define __VLANUTIL_C__

#include "npstackutl.h"
#include "nputlpltfrm.h"
#include "fsb.h"
/***************************************************************************/
/*  Function Name       : NpUtilMaskVlanNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS / FNP_FAILURE                  */
/***************************************************************************/
INT4
NpUtilMaskVlanNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    INT4                i4Index = 0;
    tHwPortArray       *pLocalHwPorts = NULL;
    tRemoteHwPortArray *pRemoteHwPorts = NULL;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
    BOOL1               bRemPortCheck = FNP_FALSE;
    BOOL1               bIsCentProtOpCode = FNP_FALSE;

    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    /* Get the stacking port of the Local and Remote unit */
    CfaNpGetHwPortInfo (&LocalHwPortInfo);

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, i4Index);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
    pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_VLAN_HW_INIT:
        case FS_MI_VLAN_HW_DE_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetAllGroupsPorts *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetAllGroupsPorts;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetAllGroupsPorts;
            pLocalHwPorts = pEntry->pHwAllGroupPorts;
            pRemoteHwPorts = &(pRemEntry->HwAllGroupPorts);
            NpUtilMaskAndAddStackingPort (pLocalHwPorts, pRemoteHwPorts,
                                          &LocalHwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount);
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetAllGroupsPorts *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetAllGroupsPorts *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetAllGroupsPorts;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwResetAllGroupsPorts;
            pLocalHwPorts = pEntry->pHwResetAllGroupPorts;
            pRemoteHwPorts = &(pRemEntry->HwResetAllGroupPorts);
            NpUtilMaskAndAddStackingPort (pLocalHwPorts, pRemoteHwPorts,
                                          &LocalHwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount);
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetUnRegGroupsPorts *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetUnRegGroupsPorts;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwResetUnRegGroupsPorts;
            pLocalHwPorts = pEntry->pHwResetUnRegGroupPorts;
            pRemoteHwPorts = &(pRemEntry->HwResetUnRegGroupPorts);
            NpUtilMaskAndAddStackingPort (pLocalHwPorts, pRemoteHwPorts,
                                          &LocalHwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount);
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetUnRegGroupsPorts;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts;
            pLocalHwPorts = pEntry->pHwUnRegPorts;
            pRemoteHwPorts = &(pRemEntry->HwUnRegPorts);
            NpUtilMaskAndAddStackingPort (pLocalHwPorts, pRemoteHwPorts,
                                          &LocalHwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount);
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            break;
        }
        case FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_FDB_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case FS_NP_HW_GET_PORT_FROM_FDB:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifndef SW_LEARNING
        case FS_MI_VLAN_HW_GET_FDB_COUNT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY:
        {
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY:
        {
            break;
        }
#endif /* SW_LEARNING */
        case FS_MI_VLAN_HW_ADD_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddMcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddMcastEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddMcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddMcastEntry;
            pLocalHwPorts = pEntry->pHwMcastPorts;
            pRemoteHwPorts = &(pRemEntry->HwMcastPorts);
            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                /* Adding the stacking port to the Local and Remote
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array alone */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else
            {
                /* Adding the stacking port to the Local
                 * Port Array alone */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStMcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStMcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddStMcastEntry;
            pLocalHwPorts = pEntry->pHwMcastPorts;
            pRemoteHwPorts = &(pRemEntry->HwMcastPorts);
            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                /* Adding the stacking port to the Local and Remote
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else
            {
                /* Adding the stacking port to the Local
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMcastPort *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastPort *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastPort;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastPort;
            /*Port has to be added in the native unit where the port is present 
               Stacking port should be added in the non-native unit */
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &LocalHwPortInfo)
                     == FNP_SUCCESS)
            {
                pEntry->u4IfIndex = u4LocalStackPort;
            }
            else
            {
                pRemEntry->u4IfIndex = u4RemoteStackPort;
            }
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_RESET_MCAST_PORT:
        {
            /*Port has to be removed in the native unit where the port is present 
               Stacking port in the non-native unit should not be removed since  
               some other ports in the native unit may be member of the group */
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &LocalHwPortInfo)
                     == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_DEL_MCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddVlanEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddVlanEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanEntry;
            pLocalHwPorts = pEntry->pHwEgressPorts;
            pRemoteHwPorts = &(pRemEntry->HwEgressPorts);
#ifdef FSB_WANTED
            if (FsbIsFCoEVlan (pEntry->u4ContextId, pEntry->VlanId) == FSB_TRUE)
            {
                NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                       &LocalHwPortInfo, &u4LocalPortCount,
                                       &u4RemotePortCount);

            }
            else
#endif
            {
                NpUtilMaskAndAddStackingPort (pLocalHwPorts, pRemoteHwPorts,
                                              &LocalHwPortInfo,
                                              &u4LocalPortCount,
                                              &u4RemotePortCount);
            }
            CfaNpGetHwPortInfo (&LocalHwPortInfo);
            pLocalHwPorts = pEntry->pHwUnTagPorts;
            pRemoteHwPorts = &(pRemEntry->HwUnTagPorts);
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_HW_SET_PORT_PVID:
        case FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE:
        case FS_MI_VLAN_HW_SET_PORT_ING_FILTERING:
        case FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT:
        case FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        case FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT:
        case FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY:
        case FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES:
        case FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY:
        case FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP:
        case FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &LocalHwPortInfo)
                     == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef L2RED_WANTED
        case FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID:
        {
            break;
        }
        case FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID:
        {
            break;
        }
        case FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL:
        {
            break;
        }
        case FS_MI_VLAN_HW_SCAN_MULTICAST_TBL:
        {
            break;
        }
        case FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SCAN_UNICAST_TBL:
        {
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR:
        {
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwGetVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanProtocolMap;
            if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;

        }
#endif /* L2RED_WANTED */
        case FS_MI_VLAN_HW_VLAN_ENABLE:
        case FS_MI_VLAN_HW_VLAN_DISABLE:
        case FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE:
        case FS_MI_VLAN_HW_GMRP_ENABLE:
        case FS_MI_VLAN_HW_GMRP_DISABLE:
        case FS_MI_NP_DELETE_ALL_FDB_ENTRIES:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwAddVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanProtocolMap;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }

            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwDelVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanProtocolMap;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS:
        {
            tVlanNpWrFsMiVlanHwGetPortStats *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats;
            if (NpUtilIsPortChannel (pEntry->u4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4Port, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS64:
        {
            tVlanNpWrFsMiVlanHwGetPortStats64 *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats64;
            if (NpUtilIsPortChannel (pEntry->u4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4Port, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_STATS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_STATS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &LocalHwPortInfo)
                     == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_FILTER:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS:
        {
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS:
        {
            break;
        }
        case FS_MI_VLAN_HW_CREATE_FDB_ID:
        case FS_MI_VLAN_HW_DELETE_FDB_ID:
        case FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB:
        case FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_FDB_ID:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_SET_SHORT_AGEOUT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_RESET_SHORT_AGEOUT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_INFO:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_MCAST_ENTRY:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_SET_AGING_TIME:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_HW_SET_BRG_MODE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortMacVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortMacVlanEntry;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortMacVlanEntry;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortSubnetVlanEntry;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortSubnetVlanEntry;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_NP_HW_RUN_MAC_AGEING:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_LIMIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwMacLearningStatus *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwMacLearningStatus *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningStatus;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningStatus;
            pLocalHwPorts = pEntry->pHwEgressPorts;
            pRemoteHwPorts = &(pRemEntry->HwEgressPorts);
            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                /* Adding the stacking port to the Local and Remote
                 * Port Array */
                pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                    u4LocalStackPort;
                pLocalHwPorts->i4Length++;
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else
            {
                /* Adding the stacking port to the Local
                 * Port Array */
                pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                    u4LocalStackPort;
                pLocalHwPorts->i4Length++;
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetFidPortLearningStatus *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetFidPortLearningStatus;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus;
            if (pEntry->u4Port != 0)
            {
                if (NpUtilIsPortChannel (pEntry->u4Port) == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
                else if (NpUtilIsRemotePort (pEntry->u4Port, &LocalHwPortInfo)
                         == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            else
            {
                pLocalHwPorts = &(pEntry->HwPortList);
                pRemoteHwPorts = &(pRemEntry->HwPortList);
                /* Filter the Local ports and Remote ports to the Local
                 * and Remote Port Array respectively */
                NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                       &LocalHwPortInfo, &u4LocalPortCount,
                                       &u4RemotePortCount);
                if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
                {
                    /* Adding the stacking port to the Local and Remote
                     * Port Array */
                    if (pLocalHwPorts->pu4PortArray != NULL)
                    {
                        pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                            u4LocalStackPort;
                        pLocalHwPorts->i4Length++;
                    }
                    pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                        u4RemoteStackPort;
                    pRemoteHwPorts->i4Length++;
                }
                else if (u4LocalPortCount != 0)
                {
                    /* Adding the stacking port to the Remote
                     * Port Array */
                    pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                        u4RemoteStackPort;
                    pRemoteHwPorts->i4Length++;
                }
                else
                {
                    /* Adding the stacking port to the Local
                     * Port Array */
                    if (pLocalHwPorts->pu4PortArray != NULL)
                    {
                        pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                            u4LocalStackPort;
                        pLocalHwPorts->i4Length++;
                    }
                }
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortProtectedStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProtectedStatus;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntryEx;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntryEx;
            pLocalHwPorts = pEntry->pHwAllowedToGoPorts;
            pRemoteHwPorts = &(pRemEntry->HwAllowedToGoPorts);
            /* Filter the Local ports and Remote ports to the Local
             * and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                /* Adding the stacking port to the Local and Remote
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else
            {
                /* Adding the stacking port to the Local
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_VLAN_HW_FORWARD_PKT_ON_PORTS:
        {
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningStatus;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4IfIndex, &LocalHwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_VLAN_HW_GET_MAC_LEARNING_MODE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_VLAN_MBSM_HW_INIT:
        {
            tVlanNpWrFsMiVlanMbsmHwInit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwInit;
            pRemoteArgs = &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_BRG_MODE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetBrgMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBrgMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBrgMode;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetBrgMode;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4BridgeMode = pLocalArgs->u4BridgeMode;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_BRG_MODE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortProperty *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProperty *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProperty;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetPortProperty;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->VlanPortProperty),
                    &(pLocalArgs->VlanPortProperty),
                    sizeof (tHwVlanPortProperty));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_PROPERTY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanMbsmHwSetDefaultVlanId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetDefaultVlanId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefaultVlanId;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetDefaultVlanId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT:
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT:
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwDelStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelStaticUcastEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStaticUcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwDelStaticUcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4RcvPort;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddMcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pLocalHwPorts = pLocalArgs->pHwMcastPorts;
            pRemoteHwPorts = &(pRemoteArgs->HwMcastPorts);

            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);

            /* There is a possibility for null port list getting programmed
             * in the remote node for this Multicast group. Hence adding the 
             * stacking port to the Remote Port Array */
            pRemoteHwPorts->au4PortArray[u4RemotePortCount] = u4RemoteStackPort;
            pRemoteHwPorts->i4Length++;

            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_MCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetMcastPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_MCAST_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwResetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetMcastPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwResetMcastPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_RESET_MCAST_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddVlanEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwEgressPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwEgressPorts.au4PortArray[0]),
                        (pLocalArgs->pHwEgressPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwEgressPorts->i4Length));
            }
            pRemoteArgs->HwEgressPorts.i4Length =
                pLocalArgs->pHwEgressPorts->i4Length;
            if (pLocalArgs->pHwUnTagPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwUnTagPorts.au4PortArray[0]),
                        (pLocalArgs->pHwUnTagPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwUnTagPorts->i4Length));
            }
            pRemoteArgs->HwUnTagPorts.i4Length =
                pLocalArgs->pHwUnTagPorts->i4Length;
            /* Mask and add the remote stacking port 
             * in the remote hw port array
             */
            pRemoteHwPorts = &(pRemoteArgs->HwEgressPorts);
            NpUtilMaskAndAddStkPortInRemPorts (pRemoteHwPorts,
                                               &LocalHwPortInfo,
                                               &u4RemotePortCount);
            pRemoteHwPorts = &(pRemoteArgs->HwUnTagPorts);
            NpUtilMaskRemoteHwPortArray (pRemoteHwPorts,
                                         &LocalHwPortInfo, &u4RemotePortCount);
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_VLAN_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetVlanMemberPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetVlanMemberPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->u1IsTagged = pLocalArgs->u1IsTagged;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwResetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetVlanMemberPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwResetVlanMemberPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PVID:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortPvid *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPvid *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPvid;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetPortPvid;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_PVID;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortAccFrameType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortAccFrameType;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortAccFrameType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->u1AccFrameType = pLocalArgs->u1AccFrameType;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortIngFiltering *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngFiltering;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngFiltering;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->u1IngFilterEnable = pLocalArgs->u1IngFilterEnable;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_ING_FILTERING;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetDefUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefUserPriority;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetDefUserPriority;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->i4DefPriority = pLocalArgs->i4DefPriority;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT:
        {
            tVlanNpWrFsMiVlanMbsmHwTraffClassMapInit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwTraffClassMapInit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwTraffClassMapInit;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwTraffClassMapInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1Priority = pLocalArgs->u1Priority;
            pRemoteArgs->i4CosqValue = pLocalArgs->i4CosqValue;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanNpWrFsMiVlanMbsmHwSetTraffClassMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTraffClassMap;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetTraffClassMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->i4UserPriority = pLocalArgs->i4UserPriority;
            pRemoteArgs->i4TraffClass = pLocalArgs->i4TraffClass;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStMcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddStMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->McastAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->McastAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->i4RcvPort = pLocalArgs->i4RcvPort;

            pLocalHwPorts = pLocalArgs->pHwMcastPorts;
            pRemoteHwPorts = &(pRemoteArgs->HwMcastPorts);

            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }

            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwDelStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStMcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwDelStMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->i4RcvPort = pLocalArgs->u4RcvPort;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortNumTrafClasses *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortNumTrafClasses;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortNumTrafClasses;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->i4NumTraffClass = pLocalArgs->i4NumTraffClass;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetRegenUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetRegenUserPriority;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetRegenUserPriority;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->i4UserPriority = pLocalArgs->i4UserPriority;
            pRemoteArgs->i4RegenPriority = pLocalArgs->i4RegenPriority;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetSubnetBasedStatusOnPort *pLocalArgs =
                NULL;
            tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->u1SubnetBasedVlanEnable =
                pLocalArgs->u1VlanSubnetEnable;
            pRemoteHwNp->u4Opcode =
                FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwEnableProtoVlanOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEnableProtoVlanOnPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwEnableProtoVlanOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->u1VlanProtoEnable = pLocalArgs->u1VlanProtoEnable;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetAllGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetAllGroupsPorts;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetAllGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwAllGroupPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllGroupPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllGroupPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllGroupPorts->i4Length));
            }
            pRemoteArgs->HwAllGroupPorts.i4Length =
                pLocalArgs->pHwAllGroupPorts->i4Length;

            /* Mask and add the remote stacking port 
             * in the remote hw port array
             */
            pRemoteHwPorts = &(pRemoteArgs->HwAllGroupPorts);
            NpUtilMaskAndAddStkPortInRemPorts (pRemoteHwPorts,
                                               &LocalHwPortInfo,
                                               &u4RemotePortCount);
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetUnRegGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetUnRegGroupsPorts;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwUnRegPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwUnRegPorts.au4PortArray[0]),
                        (pLocalArgs->pHwUnRegPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwUnRegPorts->i4Length));
            }
            pRemoteArgs->HwUnRegPorts.i4Length =
                pLocalArgs->pHwUnRegPorts->i4Length;
            /* Mask and add the remote stacking port 
             * in the remote hw port array
             */
            pRemoteHwPorts = &(pRemoteArgs->HwUnRegPorts);
            NpUtilMaskAndAddStkPortInRemPorts (pRemoteHwPorts,
                                               &LocalHwPortInfo,
                                               &u4RemotePortCount);
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER:
        {
            tVlanNpWrFsMiVlanMbsmHwSetTunnelFilter *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTunnelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTunnelFilter;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetTunnelFilter;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->i4BridgeMode = pLocalArgs->i4BridgeMode;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_TUNNEL_FILTER;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddPortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddPortSubnetVlanEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortSubnetVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4Port;
            pRemoteArgs->SubnetAddr = pLocalArgs->u4SubnetAddr;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->bARPOption = pLocalArgs->u1ArpOption;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwPortPktReflectStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmHwPortPktReflectStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortPktReflectStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwPortPktReflectStatus;
            MEMCPY (&(pRemoteArgs->PortReflectEntry),
                    (pLocalArgs->pPortReflectEntry),
                    sizeof (tFsNpVlanPortReflectEntry));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_MBSM_SET_AGING_TIME:
        {
            tVlanNpWrFsMiBrgMbsmSetAgingTime *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiBrgSetAgingTime *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiBrgMbsmSetAgingTime;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiBrgSetAgingTime;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->i4AgingTime = pLocalArgs->i4AgingTime;
            pRemoteHwNp->u4Opcode = FS_MI_BRG_SET_AGING_TIME;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetFidPortLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetFidPortLearningStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            pRemoteArgs->u1Action = pLocalArgs->u1Action;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort *pLocalArgs =
                NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->
                VlanNpFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->ProtocolId), &(pLocalArgs->ProtocolId),
                    sizeof (tVlanHwTunnelFilters));
            pRemoteArgs->u4TunnelStatus = pLocalArgs->u4TunnelStatus;
            pRemoteHwNp->u4Opcode =
                FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortProtectedStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProtectedStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortProtectedStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4ProtectedStatus = pLocalArgs->u4ProtectedStatus;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwSwitchMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSwitchMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSwitchMacLearningLimit;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSwitchMacLearningLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwPortMacLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMacLearningLimit;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u2FdbId = pLocalArgs->u2FdbId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_MAC_LEARNING_LIMIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntryEx *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntryEx;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntryEx;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4FdbId;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4RcvPort;
            if (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllowedToGoPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllowedToGoPorts->i4Length));
            }
            pRemoteArgs->HwAllowedToGoPorts.i4Length =
                pLocalArgs->pHwAllowedToGoPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            if (pLocalArgs->ConnectionId != NULL)
            {
                MEMCPY (&(pRemoteArgs->ConnectionId),
                        (pLocalArgs->ConnectionId), sizeof (tMacAddr));
            }
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_INDEX:
        {
            tVlanNpWrFsMiVlanHwSetMcastIndex *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastIndex;
            if (pLocalArgs->HwMcastIndexInfo.u4Opcode ==
                VLAN_OPCODE_GET_MCAST_INDEX)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            else if (pLocalArgs->HwMcastIndexInfo.u4Opcode ==
                     VLAN_OPCODE_SET_MCAST_INDEX)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortIngressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngressEtherType;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngressEtherType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2EtherType = pLocalArgs->u2EtherType;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortEgressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEgressEtherType;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEgressEtherType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2EtherType = pLocalArgs->u2EtherType;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetProviderBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetProviderBridgePortType;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProviderBridgePortType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4PortType = pLocalArgs->u4PortType;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortSVlanTranslationStatus *pLocalArgs =
                NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->
                VlanNpFsMiVlanMbsmHwSetPortSVlanTranslationStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanTranslationStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            pRemoteHwNp->u4Opcode =
                FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanTranslationEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddSVlanTranslationEntry;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2LocalSVlan = pLocalArgs->u2LocalSVlan;
            pRemoteArgs->u2RelaySVlan = pLocalArgs->u2RelaySVlan;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortEtherTypeSwapStatus *pLocalArgs =
                NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            pRemoteHwNp->u4Opcode =
                FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddEtherTypeSwapEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddEtherTypeSwapEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddEtherTypeSwapEntry;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2LocalEtherType = pLocalArgs->u2LocalEtherType;
            pRemoteArgs->u2RelayEtherType = pLocalArgs->u2RelayEtherType;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanMbsmHwAddSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanMap;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwAddSVlanMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&pRemoteArgs->VlanSVlanMap, &pLocalArgs->VlanSVlanMap,
                    sizeof (tVlanSVlanMap));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_S_VLAN_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortSVlanClassifyMethod *pLocalArgs =
                NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortSVlanClassifyMethod;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanClassifyMethod;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1TableType = pLocalArgs->u1TableType;
            pRemoteHwNp->u4Opcode =
                FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwPortMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningLimit;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningLimit;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortCustomerVlan *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortCustomerVlan;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortCustomerVlan;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->CVlanId = pLocalArgs->CVlanId;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwCreateProviderEdgePort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwCreateProviderEdgePort;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwCreateProviderEdgePort;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->SVlanId = pLocalArgs->SVlanId;
            MEMCPY (&pRemoteArgs->PepConfig, &pLocalArgs->SVlanId,
                    sizeof (tHwVlanPbPepInfo));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPcpEncodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpEncodTbl;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpEncodTbl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&pRemoteArgs->NpPbVlanPcpInfo, &pLocalArgs->NpPbVlanPcpInfo,
                    sizeof (tHwVlanPbPcpInfo));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPcpDecodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpDecodTbl;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpDecodTbl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&pRemoteArgs->NpPbVlanPcpInfo, &pLocalArgs->NpPbVlanPcpInfo,
                    sizeof (tHwVlanPbPcpInfo));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PCP_DECOD_TBL;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortUseDei *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortUseDei *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortUseDei;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetPortUseDei;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1UseDei = pLocalArgs->u1UseDei;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_USE_DEI;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortReqDropEncoding *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortReqDropEncoding;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortReqDropEncoding;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1ReqDrpEncoding = pLocalArgs->u1ReqDrpEncoding;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortPcpSelection *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPcpSelection;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortPcpSelection;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2PcpSelection = pLocalArgs->u2PcpSelection;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetServicePriRegenEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetServicePriRegenEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwSetServicePriRegenEntry;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->i4RecvPriority = pLocalArgs->i4RecvPriority;
            pRemoteArgs->i4RegenPriority = pLocalArgs->i4RegenPriority;
            pRemoteArgs->SVlanId = pLocalArgs->SVlanId;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwMulticastMacTableLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMulticastMacTableLimit;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwMulticastMacTableLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* PB_WANTED */
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
            /* Masking Logic differs for each Platform */
            /* Invoke a Platform specific custom function */
        {
            i4RetVal = CustNpUtilMaskNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEvbConfigSChIface;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwEvbConfigSChIface;
            MEMCPY (&(pRemoteArgs->VlanEvbHwConfigInfo),
                    &(pLocalArgs->VlanEvbHwConfigInfo),
                    sizeof (tVlanHwPortInfo));

            if ((pLocalArgs->VlanEvbHwConfigInfo.u1OpCode ==
                 VLAN_EVB_HW_SCH_IF_CREATE)
                || (pLocalArgs->VlanEvbHwConfigInfo.u1OpCode ==
                    VLAN_EVB_HW_SCH_IF_DELETE))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if ((pLocalArgs->VlanEvbHwConfigInfo.u1OpCode ==
                      VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD)
                     || (pLocalArgs->VlanEvbHwConfigInfo.u1OpCode ==
                         VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK))
            {
                if (NpUtilIsRemotePort
                    (pLocalArgs->VlanEvbHwConfigInfo.u4UapIfIndex,
                     &LocalHwPortInfo) == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }

            break;
        }

        case FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE:

        {
            tVlanNpWrFsMiVlanMbsmHwSetBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBridgePortType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBridgePortType;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetBridgePortType;
            MEMCPY (&(pRemoteArgs->VlanHwPortInfo),
                    &(pLocalArgs->VlanHwPortInfo), sizeof (tVlanHwPortInfo));
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE;
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }

#endif /* MBSM_WANTED */
        case FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE:
        case FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE:
        case FS_MI_VLAN_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanHwSetPortProperty *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProperty;

            if (pFsHwNp->u4IfIndex != 0)
            {
                bRemPortCheck = FNP_TRUE;
            }

            else
            {
                if ((pFsHwNp->u4IfIndex == 0)
                    && (pEntry->VlanPortProperty.VlanId != 0))
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
            }
            break;
        }
#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        case FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        case FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY:
        case FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        case FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        case FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY:
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN:
        case FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN:
        case FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT:
        case FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT:
        case FS_MI_VLAN_HW_SET_PEP_PVID:
        case FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE:
        case FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY:
        case FS_MI_VLAN_HW_SET_PEP_ING_FILTERING:
        case FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL:
        case FS_MI_VLAN_HW_SET_PCP_DECOD_TBL:
        case FS_MI_VLAN_HW_SET_PORT_USE_DEI:
        case FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING:
        case FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION:
        case FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwAddSVlanMap *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanMap;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = (UINT4) pLocalArgs->VlanSVlanMap.u2Port;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwDeleteSVlanMap *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteSVlanMap;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = (UINT4) pLocalArgs->VlanSVlanMap.u2Port;
            break;
        }
        case FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagPep *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagPep;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = (UINT4) pLocalArgs->VlanSVlanMap.u2Port;
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagCep *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagCep;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = (UINT4) pLocalArgs->VlanSVlanMap.u2Port;
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanMap *pLocalArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanMap;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = (UINT4) pLocalArgs->VlanSVlanMap.u2Port;
            break;
        }
#endif /* PB_WANTED */
        case FS_MI_VLAN_HW_SET_LOOPBACK_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_CVLAN_STAT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;

        }
        case FS_MI_VLAN_HW_GET_CVLAN_STAT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;

        }
        case FS_MI_VLAN_HW_CLEAR_CVLAN_STAT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;

        }
#endif
        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanHwEvbConfigSChIface *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface *pRemEntry = NULL;
            tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwEvbConfigSChIface;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwEvbConfigSChIface;
            pVlanEvbHwConfigInfo = &(pEntry->VlanEvbHwConfigInfo);
            /*S-channel interface has to be created on both local and remote unit. 
             * Oper status will be changed only on the unit where the UAP port 
             * over which SBP is instantiated  is present  */

            if ((pEntry->VlanEvbHwConfigInfo.u1OpCode ==
                 VLAN_EVB_HW_SCH_IF_CREATE)
                || (pEntry->VlanEvbHwConfigInfo.u1OpCode ==
                    VLAN_EVB_HW_SCH_IF_DELETE))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if ((pEntry->VlanEvbHwConfigInfo.u1OpCode ==
                      VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD)
                     || (pEntry->VlanEvbHwConfigInfo.u1OpCode ==
                         VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK))
            {
                if (NpUtilIsRemotePort
                    (pEntry->VlanEvbHwConfigInfo.u4UapIfIndex,
                     &LocalHwPortInfo) == FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            break;
        }

        case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetBridgePortType *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBridgePortType *pRemEntry = NULL;
            tVlanHwPortInfo    *pVlanHwPortInfo = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBridgePortType;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwSetBridgePortType;
            pVlanHwPortInfo = &pEntry->VlanHwPortInfo;
            if (NpUtilIsRemotePort
                (pEntry->VlanHwPortInfo.u4IfIndex,
                 &LocalHwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }

        default:
            break;
    }
    if (bRemPortCheck == FNP_TRUE)
    {
        if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
        else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &LocalHwPortInfo)
                 == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
    }
    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * VLAN protocol will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if ((u4StackingModel == ISS_DISS_STACKING_MODEL) &&
        (bIsCentProtOpCode != FNP_TRUE))
    {
        if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            /* don't do anything */
        }
    }
#ifdef MBSM_WANTED
    if (pFsHwNp->u4Opcode < FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE)
    {
        if (MbsmIsNpBulkSyncInProgress (NP_VLAN_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return i4RetVal;
}

/***************************************************************************/
/*  Function Name       : NpUtilVlanMergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*               and Remote NP call execution                              */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp            */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value         */
/*                    pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilVlanMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp,
                                    INT4 *pi4LocalNpRetVal,
                                    INT4 *pi4RemoteNpRetVal)
{
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    tHwPortArray       *pLocalHwPorts = NULL;
    tRemoteHwPortArray *pRemoteHwPorts = NULL;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
    INT4                i4Arrayidx = 0;
    INT4                i4LastArrayidx = 0;

    UNUSED_PARAM (u4RemoteStackPort);

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    /* Get the Stacking Port of the Local and Remote Unit */
    CfaNpGetHwPortInfo (&LocalHwPortInfo);
    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);

    pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
    pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_VLAN_HW_INIT:
        case FS_MI_VLAN_HW_DE_INIT:
        case FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS:
        case FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS:
        case FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS:
        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_HW_ADD_MCAST_ENTRY:
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        case FS_MI_VLAN_HW_DEL_MCAST_ENTRY:
        case FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY:
        case FS_MI_VLAN_HW_ADD_VLAN_ENTRY:
        case FS_MI_VLAN_HW_DEL_VLAN_ENTRY:
        case FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID:
        case FS_MI_VLAN_HW_VLAN_ENABLE:
        case FS_MI_VLAN_HW_VLAN_DISABLE:
        case FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE:
        case FS_MI_VLAN_HW_GMRP_ENABLE:
        case FS_MI_VLAN_HW_GMRP_DISABLE:
        case FS_MI_NP_DELETE_ALL_FDB_ENTRIES:
        case FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP:
        case FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP:
        case FS_MI_VLAN_HW_SET_TUNNEL_FILTER:
        case FS_MI_VLAN_HW_CREATE_FDB_ID:
        case FS_MI_VLAN_HW_DELETE_FDB_ID:
        case FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB:
        case FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB:
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        case FS_MI_VLAN_HW_FLUSH_PORT:
        case FS_MI_VLAN_HW_FLUSH_FDB_ID:
        case FS_MI_VLAN_HW_SET_SHORT_AGEOUT:
        case FS_MI_VLAN_HW_RESET_SHORT_AGEOUT:
        case FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT:
        case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_SET_AGING_TIME:
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_HW_SET_BRG_MODE:
        case FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE:
        {
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_STATS:
        {
            tVlanNpWrFsMiVlanHwGetVlanStats *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetVlanStats *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanStats;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanStats;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            (*pEntry->pu4VlanStatsValue) =
                (*pEntry->pu4VlanStatsValue) + pRemEntry->u4VlanStatsValue;
            break;
        }
        case FS_NP_HW_GET_PORT_FROM_FDB:
        {
            tVlanNpWrFsNpHwGetPortFromFdb *pEntry = NULL;
            tVlanRemoteNpWrFsNpHwGetPortFromFdb *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsNpHwGetPortFromFdb;
            pRemEntry = &pRemoteVlanNpModInfo->VlanRemoteNpFsNpHwGetPortFromFdb;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            if (0 == *(pEntry->pu4Port))
            {
                (*pEntry->pu4Port) = pRemEntry->u4Port;
            }
            break;
        }
#ifndef SW_LEARNING
        case FS_MI_VLAN_HW_GET_FDB_COUNT:
        {
            tVlanNpWrFsMiVlanHwGetFdbCount *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetFdbCount *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbCount;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetFdbCount;
            /* Merge the Output of the Remote Structure with 
             * the Local Structure */
            (*pEntry->pu4Count) = (*pEntry->pu4Count) + pRemEntry->u4Count;
            break;
        }
#endif /* SW_LEARNING */
#ifdef L2RED_WANTED
        case FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStMcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStMcastEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStMcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetStMcastEntry;
            pLocalHwPorts = pEntry->pHwMcastPorts;
            pRemoteHwPorts = &pRemEntry->HwMcastPorts;

            if ((*pi4LocalNpRetVal == FNP_FAILURE) &&
                (*pi4RemoteNpRetVal == FNP_FAILURE))
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
            }
            else
            {
                /* Merge the Output of the Remote Structure with 
                 * the Local Structure */
                i4LastArrayidx = pLocalHwPorts->i4Length;
                for (i4Arrayidx = 0; i4Arrayidx < pRemoteHwPorts->i4Length;
                     i4Arrayidx++)
                {
                    pLocalHwPorts->pu4PortArray[i4LastArrayidx] =
                        pRemoteHwPorts->au4PortArray[i4Arrayidx];
                    i4LastArrayidx++;
                }
                pLocalHwPorts->i4Length = i4LastArrayidx;
                *pi4LocalNpRetVal = FNP_SUCCESS;
            }
            NpUtilRemoveStackPort (pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntry;
            pLocalHwPorts = pEntry->pAllowedToGoPorts;
            pRemoteHwPorts = &pRemEntry->AllowedToGoPorts;

            if ((*pi4LocalNpRetVal == FNP_FAILURE) &&
                (*pi4RemoteNpRetVal == FNP_FAILURE))
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
            }
            else
            {
                /* Merge the Output of the Remote Structure with
                 * the Local Structure */
                i4LastArrayidx = pLocalHwPorts->i4Length;
                for (i4Arrayidx = 0; i4Arrayidx < pRemoteHwPorts->i4Length;
                     i4Arrayidx++)
                {
                    pLocalHwPorts->pu4PortArray[i4LastArrayidx] =
                        pRemoteHwPorts->au4PortArray[i4Arrayidx];
                    i4LastArrayidx++;
                }
                pLocalHwPorts->i4Length = i4LastArrayidx;
                *pi4LocalNpRetVal = FNP_SUCCESS;
            }

            NpUtilRemoveStackPort (pEntry->pAllowedToGoPorts);
            break;
        }
#endif /* L2RED_WANTED */
        case FS_MI_VLAN_HW_GET_VLAN_INFO:
        {
            tVlanNpWrFsMiVlanHwGetVlanInfo *pVlanEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetVlanInfo *pRemoteVlanEntry = NULL;
            tHwVlanPortArray   *pEntry = NULL;
            tRemoteHwVlanPortArray *pRemEntry = NULL;
            INT4                i4Index = 0;
            tHwPortArray       *pLocalHwPortsArray[4] =
                { NULL, NULL, NULL, NULL };
            tRemoteHwPortArray *pRemoteHwPortsArray[4] =
                { NULL, NULL, NULL, NULL };
            pVlanEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanInfo;
            pRemoteVlanEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanInfo;
            pEntry = pVlanEntry->pHwEntry;
            pRemEntry = &pRemoteVlanEntry->HwEntry;

            if ((*pi4LocalNpRetVal == FNP_FAILURE) &&
                (*pi4RemoteNpRetVal == FNP_FAILURE))
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
            }
            else
            {
                pLocalHwPortsArray[0] = pEntry->pHwMemberPortArray;
                pRemoteHwPortsArray[0] = &pRemEntry->HwMemberPortArray;
                pLocalHwPortsArray[1] = pEntry->pHwUntagPortArray;
                pRemoteHwPortsArray[1] = &pRemEntry->HwUntagPortArray;
                pLocalHwPortsArray[2] = pEntry->pHwFwdAllPortArray;
                pRemoteHwPortsArray[2] = &pRemEntry->HwFwdAllPortArray;
                pLocalHwPortsArray[3] = pEntry->pHwFwdUnregPortArray;
                pRemoteHwPortsArray[3] = &pRemEntry->HwFwdUnregPortArray;

                for (i4Index = 0; i4Index < 4; i4Index++)
                {
                    i4LastArrayidx = pLocalHwPortsArray[i4Index]->i4Length;
                    if (i4LastArrayidx == 1)
                    {
                        /* Copy the Output of the Remote Structure to
                         * the Local Structure */
                        if (pLocalHwPortsArray[i4Index]->pu4PortArray[0] ==
                            u4LocalStackPort)
                        {
                            MEMCPY (&pLocalHwPortsArray[i4Index]->
                                    pu4PortArray[0],
                                    &pRemoteHwPortsArray[i4Index]->
                                    au4PortArray[0],
                                    sizeof (UINT4) *
                                    (pRemoteHwPortsArray[i4Index]->i4Length));
                            pLocalHwPortsArray[i4Index]->i4Length =
                                pRemoteHwPortsArray[i4Index]->i4Length;
                        }
                        else
                        {
                            /* Merge the Output of the Remote Structure with
                             * the Local Structure */
                            for (i4Arrayidx = 0;
                                 i4Arrayidx <
                                 pRemoteHwPortsArray[i4Index]->i4Length;
                                 i4Arrayidx++)
                            {
                                if (pLocalHwPortsArray[i4Index]->pu4PortArray !=
                                    NULL)
                                {
                                    pLocalHwPortsArray[i4Index]->
                                        pu4PortArray[i4LastArrayidx] =
                                        pRemoteHwPortsArray[i4Index]->
                                        au4PortArray[i4Arrayidx];
                                    i4LastArrayidx++;
                                }
                            }
                            pLocalHwPortsArray[i4Index]->i4Length =
                                i4LastArrayidx;
                        }
                    }
                    else
                    {
                        /* Merge the Output of the Remote Structure with
                         * the Local Structure */
                        for (i4Arrayidx = 0;
                             i4Arrayidx <
                             pRemoteHwPortsArray[i4Index]->i4Length;
                             i4Arrayidx++)
                        {
                            if (pLocalHwPortsArray[i4Index]->pu4PortArray !=
                                NULL)
                            {
                                pLocalHwPortsArray[i4Index]->
                                    pu4PortArray[i4LastArrayidx] =
                                    pRemoteHwPortsArray[i4Index]->
                                    au4PortArray[i4Arrayidx];
                                i4LastArrayidx++;
                            }
                        }
                        pLocalHwPortsArray[i4Index]->i4Length = i4LastArrayidx;
                    }
                }
                *pi4LocalNpRetVal = FNP_SUCCESS;
            }
            NpUtilRemoveStackPort (pEntry->pHwMemberPortArray);
            NpUtilRemoveStackPort (pEntry->pHwUntagPortArray);
            NpUtilRemoveStackPort (pEntry->pHwFwdAllPortArray);
            NpUtilRemoveStackPort (pEntry->pHwFwdUnregPortArray);
            break;
        }
        case FS_MI_VLAN_HW_GET_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetMcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetMcastEntry *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetMcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetMcastEntry;
            pLocalHwPorts = pEntry->pHwMcastPorts;
            pRemoteHwPorts = &pRemEntry->HwMcastPorts;

            if ((*pi4LocalNpRetVal == FNP_FAILURE) &&
                (*pi4RemoteNpRetVal == FNP_FAILURE))
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
            }
            else
            {
                /* Merge the Output of the Remote Structure with
                 * the Local Structure */
                i4LastArrayidx = pLocalHwPorts->i4Length;
                for (i4Arrayidx = 0; i4Arrayidx < pRemoteHwPorts->i4Length;
                     i4Arrayidx++)
                {
                    pLocalHwPorts->pu4PortArray[i4LastArrayidx] =
                        pRemoteHwPorts->au4PortArray[i4Arrayidx];
                    i4LastArrayidx++;
                }
                pLocalHwPorts->i4Length = i4LastArrayidx;
                *pi4LocalNpRetVal = FNP_SUCCESS;
            }
            NpUtilRemoveStackPort (pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntryEx *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntryEx;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntryEx;
            pLocalHwPorts = pEntry->pAllowedToGoPorts;
            pRemoteHwPorts = &pRemEntry->AllowedToGoPorts;
            if ((*pi4LocalNpRetVal == FNP_FAILURE) &&
                (*pi4RemoteNpRetVal == FNP_FAILURE))
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
            }
            else
            {
                /* Merge the Output of the Remote Structure with
                 * the Local Structure */
                i4LastArrayidx = pLocalHwPorts->i4Length;
                for (i4Arrayidx = 0; i4Arrayidx < pRemoteHwPorts->i4Length;
                     i4Arrayidx++)
                {
                    pLocalHwPorts->pu4PortArray[i4LastArrayidx] =
                        pRemoteHwPorts->au4PortArray[i4Arrayidx];
                    i4LastArrayidx++;
                }
                pLocalHwPorts->i4Length = i4LastArrayidx;
                *pi4LocalNpRetVal = FNP_SUCCESS;
            }
            NpUtilRemoveStackPort (pEntry->pAllowedToGoPorts);
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_GET_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwGetCVlanStat *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetCVlanStat *pRemEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetCVlanStat;
            pRemEntry =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanHwGetCVlanStat;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            (pEntry->pu4VlanStatsValue) =
                (*pEntry->pu4VlanStatsValue) + pRemEntry->pu4VlanStatsValue;
            break;
        }
#endif
        default:
            break;
    }
    return FNP_SUCCESS;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVlanConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVlanConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanRemoteNpModInfo *pVlanRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
    pVlanRemoteNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tVlanRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_VLAN_HW_INIT:
        {
            tVlanNpWrFsMiVlanHwInit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwInit;
            pRemoteArgs = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_DE_INIT:
        {
            tVlanNpWrFsMiVlanHwDeInit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDeInit;
            pRemoteArgs = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetAllGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetAllGroupsPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetAllGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwAllGroupPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllGroupPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllGroupPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllGroupPorts->i4Length));
            }
            pRemoteArgs->HwAllGroupPorts.i4Length =
                pLocalArgs->pHwAllGroupPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetAllGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetAllGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetAllGroupsPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetAllGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwResetAllGroupPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwResetAllGroupPorts.au4PortArray[0]),
                        (pLocalArgs->pHwResetAllGroupPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwResetAllGroupPorts->i4Length));
            }
            pRemoteArgs->HwResetAllGroupPorts.i4Length =
                pLocalArgs->pHwResetAllGroupPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetUnRegGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetUnRegGroupsPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetUnRegGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwResetUnRegGroupPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwResetUnRegGroupPorts.au4PortArray[0]),
                        (pLocalArgs->pHwResetUnRegGroupPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwResetUnRegGroupPorts->i4Length));
            }
            pRemoteArgs->HwResetUnRegGroupPorts.i4Length =
                pLocalArgs->pHwResetUnRegGroupPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetUnRegGroupsPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwUnRegPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwUnRegPorts.au4PortArray[0]),
                        (pLocalArgs->pHwUnRegPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwUnRegPorts->i4Length));
            }
            pRemoteArgs->HwUnRegPorts.i4Length =
                pLocalArgs->pHwUnRegPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllowedToGoPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllowedToGoPorts->i4Length));
            }
            pRemoteArgs->HwAllowedToGoPorts.i4Length =
                pLocalArgs->pHwAllowedToGoPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelStaticUcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStaticUcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelStaticUcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            break;
        }
        case FS_MI_VLAN_HW_GET_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetFdbEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetFdbEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetFdbEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4FdbId = pLocalArgs->u4FdbId;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            if (pLocalArgs->pEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->Entry), (pLocalArgs->pEntry),
                        sizeof (tHwUnicastMacEntry));
            }
            break;
        }
        case FS_NP_HW_GET_PORT_FROM_FDB:
        {
            tVlanNpWrFsNpHwGetPortFromFdb *pLocalArgs = NULL;
            tVlanRemoteNpWrFsNpHwGetPortFromFdb *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsNpHwGetPortFromFdb;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsNpHwGetPortFromFdb;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->i1pHwAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->pHwAddr), (pLocalArgs->i1pHwAddr),
                        sizeof (tMacAddr));
            }
            if (pLocalArgs->pu4Port != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Port), (pLocalArgs->pu4Port),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortPktReflectStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortPktReflectStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortPktReflectStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortPktReflectStatus;
            if (pLocalArgs->pPortReflectEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortReflectEntry),
                        (pLocalArgs->pPortReflectEntry),
                        sizeof (tFsNpVlanPortReflectEntry));
            }
            break;
        }

#ifndef SW_LEARNING
        case FS_MI_VLAN_HW_GET_FDB_COUNT:
        {
            tVlanNpWrFsMiVlanHwGetFdbCount *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetFdbCount *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbCount;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetFdbCount;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4FdbId = pLocalArgs->u4FdbId;
            if (pLocalArgs->pu4Count != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Count), (pLocalArgs->pu4Count),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetFirstTpFdbEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetFirstTpFdbEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFirstTpFdbEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetFirstTpFdbEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu4FdbId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FdbId), (pLocalArgs->pu4FdbId),
                        sizeof (UINT4));
            }
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetNextTpFdbEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetNextTpFdbEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextTpFdbEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetNextTpFdbEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4FdbId = pLocalArgs->u4FdbId;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            if (pLocalArgs->pu4NextContextId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4NextContextId),
                        (pLocalArgs->pu4NextContextId), sizeof (UINT4));
            }
            if (pLocalArgs->pu4NextFdbId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4NextFdbId), (pLocalArgs->pu4NextFdbId),
                        sizeof (UINT4));
            }
            if (pLocalArgs->NextMacAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMacAddr), (pLocalArgs->NextMacAddr),
                        sizeof (tMacAddr));
            }
            break;
        }
#endif /* SW_LEARNING */
        case FS_MI_VLAN_HW_ADD_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            if (pLocalArgs->pHwMcastPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwMcastPorts.au4PortArray[0]),
                        (pLocalArgs->pHwMcastPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwMcastPorts->i4Length));
            }
            pRemoteArgs->HwMcastPorts.i4Length =
                pLocalArgs->pHwMcastPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddStMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->i4RcvPort = pLocalArgs->i4RcvPort;
            if (pLocalArgs->pHwMcastPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwMcastPorts.au4PortArray[0]),
                        (pLocalArgs->pHwMcastPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwMcastPorts->i4Length));
            }
            pRemoteArgs->HwMcastPorts.i4Length =
                pLocalArgs->pHwMcastPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_RESET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwResetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetMcastPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetMcastPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_DEL_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            break;
        }
        case FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelStMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->i4RcvPort = pLocalArgs->i4RcvPort;
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwEgressPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwEgressPorts.au4PortArray[0]),
                        (pLocalArgs->pHwEgressPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwEgressPorts->i4Length));
            }
            pRemoteArgs->HwEgressPorts.i4Length =
                pLocalArgs->pHwEgressPorts->i4Length;
            if (pLocalArgs->pHwUnTagPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwUnTagPorts.au4PortArray[0]),
                        (pLocalArgs->pHwUnTagPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->pHwUnTagPorts->i4Length));
            }
            pRemoteArgs->HwUnTagPorts.i4Length =
                pLocalArgs->pHwUnTagPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwSetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanMemberPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetVlanMemberPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1IsTagged = pLocalArgs->u1IsTagged;
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwResetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanMemberPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetVlanMemberPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPortPvid *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPvid *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPvid;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortPvid;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanHwSetDefaultVlanId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetDefaultVlanId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefaultVlanId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetDefaultVlanId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
#ifdef L2RED_WANTED
        case FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanHwSyncDefaultVlanId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSyncDefaultVlanId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSyncDefaultVlanId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSyncDefaultVlanId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->SwVlanId), &(pLocalArgs->SwVlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanRedHwUpdateDBForDefaultVlanId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanRedHwUpdateDBForDefaultVlanId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL:
        {
            tVlanNpWrFsMiVlanHwScanProtocolVlanTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwScanProtocolVlanTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwScanProtocolVlanTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwScanProtocolVlanTbl;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwGetVlanProtocolMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetVlanProtocolMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanProtocolMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanProtocolMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4GroupId = pLocalArgs->u4GroupId;
            if (pLocalArgs->pProtoTemplate != NULL)
            {
                MEMCPY (&(pRemoteArgs->ProtoTemplate),
                        (pLocalArgs->pProtoTemplate),
                        sizeof (tVlanProtoTemplate));
            }
            if (pLocalArgs->pVlanId != NULL)
            {
                MEMCPY (&(pRemoteArgs->VlanId), (pLocalArgs->pVlanId),
                        sizeof (tVlanId));
            }
            break;
        }
        case FS_MI_VLAN_HW_SCAN_MULTICAST_TBL:
        {
            tVlanNpWrFsMiVlanHwScanMulticastTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwScanMulticastTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwScanMulticastTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwScanMulticastTbl;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetStMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4RcvPort = pLocalArgs->u4RcvPort;
            MEMCPY (&(pRemoteArgs->HwMcastPorts.au4PortArray[0]),
                    (pLocalArgs->pHwMcastPorts->pu4PortArray),
                    sizeof (UINT4) * (pLocalArgs->pHwMcastPorts->i4Length));
            pRemoteArgs->HwMcastPorts.i4Length =
                pLocalArgs->pHwMcastPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_SCAN_UNICAST_TBL:
        {
            tVlanNpWrFsMiVlanHwScanUnicastTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwScanUnicastTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwScanUnicastTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwScanUnicastTbl;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            MEMCPY (&(pRemoteArgs->AllowedToGoPorts.au4PortArray[0]),
                    (pLocalArgs->pAllowedToGoPorts->pu4PortArray),
                    sizeof (UINT4) * (pLocalArgs->pAllowedToGoPorts->i4Length));
            pRemoteArgs->AllowedToGoPorts.i4Length =
                pLocalArgs->pAllowedToGoPorts->i4Length;
            if (pLocalArgs->pu1Status != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1Status), (pLocalArgs->pu1Status),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR:
        {
            tVlanNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwGetSyncedTnlProtocolMacAddr;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetSyncedTnlProtocolMacAddr;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2Protocol = pLocalArgs->u2Protocol;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            break;
        }
#endif /* L2RED_WANTED */
        case FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortAccFrameType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortAccFrameType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortAccFrameType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1AccFrameType = pLocalArgs->u1AccFrameType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPortIngFiltering *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngFiltering;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngFiltering;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1IngFilterEnable = pLocalArgs->u1IngFilterEnable;
            break;
        }
        case FS_MI_VLAN_HW_VLAN_ENABLE:
        {
            tVlanNpWrFsMiVlanHwVlanEnable *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwVlanEnable *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanEnable;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwVlanEnable;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_VLAN_DISABLE:
        {
            tVlanNpWrFsMiVlanHwVlanDisable *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwVlanDisable *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanDisable;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwVlanDisable;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetVlanLearningType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetVlanLearningType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLearningType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetVlanLearningType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1LearningType = pLocalArgs->u1LearningType;
            break;
        }
        case FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMacBasedStatusOnPort *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetMacBasedStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetMacBasedStatusOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1MacBasedVlanEnable =
                pLocalArgs->u1MacBasedVlanEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1SubnetBasedVlanEnable =
                pLocalArgs->u1SubnetBasedVlanEnable;
            break;
        }
        case FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwEnableProtoVlanOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwEnableProtoVlanOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1VlanProtoEnable = pLocalArgs->u1VlanProtoEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetDefUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetDefUserPriority;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4DefPriority = pLocalArgs->i4DefPriority;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanNpWrFsMiVlanHwSetPortNumTrafClasses *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortNumTrafClasses;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortNumTrafClasses;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4NumTraffClass = pLocalArgs->i4NumTraffClass;
            break;
        }
        case FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetRegenUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetRegenUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetRegenUserPriority;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4UserPriority = pLocalArgs->i4UserPriority;
            pRemoteArgs->i4RegenPriority = pLocalArgs->i4RegenPriority;
            break;
        }
        case FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanNpWrFsMiVlanHwSetTraffClassMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTraffClassMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetTraffClassMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4UserPriority = pLocalArgs->i4UserPriority;
            pRemoteArgs->i4TraffClass = pLocalArgs->i4TraffClass;
            break;
        }
        case FS_MI_VLAN_HW_GMRP_ENABLE:
        {
            tVlanNpWrFsMiVlanHwGmrpEnable *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGmrpEnable *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpEnable;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGmrpEnable;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_GMRP_DISABLE:
        {
            tVlanNpWrFsMiVlanHwGmrpDisable *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGmrpDisable *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpDisable;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGmrpDisable;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_NP_DELETE_ALL_FDB_ENTRIES:
        {
            tVlanNpWrFsMiNpDeleteAllFdbEntries *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiNpDeleteAllFdbEntries *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiNpDeleteAllFdbEntries;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiNpDeleteAllFdbEntries;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwAddVlanProtocolMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddVlanProtocolMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanProtocolMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanProtocolMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4GroupId = pLocalArgs->u4GroupId;
            if (pLocalArgs->pProtoTemplate != NULL)
            {
                MEMCPY (&(pRemoteArgs->ProtoTemplate),
                        (pLocalArgs->pProtoTemplate),
                        sizeof (tVlanProtoTemplate));
            }
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwDelVlanProtocolMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelVlanProtocolMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanProtocolMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelVlanProtocolMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4GroupId = pLocalArgs->u4GroupId;
            if (pLocalArgs->pProtoTemplate != NULL)
            {
                MEMCPY (&(pRemoteArgs->ProtoTemplate),
                        (pLocalArgs->pProtoTemplate),
                        sizeof (tVlanProtoTemplate));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS:
        {
            tVlanNpWrFsMiVlanHwGetPortStats *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetPortStats *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1StatsType = pLocalArgs->u1StatsType;
            if (pLocalArgs->pu4PortStatsValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4PortStatsValue),
                        (pLocalArgs->pu4PortStatsValue), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS64:
        {
            tVlanNpWrFsMiVlanHwGetPortStats64 *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetPortStats64 *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats64;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats64;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1StatsType = pLocalArgs->u1StatsType;
            if (pLocalArgs->pValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->Value), (pLocalArgs->pValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_STATS:
        {
            tVlanNpWrFsMiVlanHwGetVlanStats *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetVlanStats *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanStats;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanStats;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1StatsType = pLocalArgs->u1StatsType;
            if (pLocalArgs->pu4VlanStatsValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4VlanStatsValue),
                        (pLocalArgs->pu4VlanStatsValue), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_STATS:
        {
            tVlanNpWrFsMiVlanHwResetVlanStats *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetVlanStats *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanStats;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetVlanStats;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE:
        {
            tVlanNpWrFsMiVlanHwSetPortTunnelMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortTunnelMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortTunnelMode;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortTunnelMode;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4Mode = pLocalArgs->u4Mode;
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_FILTER:
        {
            tVlanNpWrFsMiVlanHwSetTunnelFilter *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTunnelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelFilter;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetTunnelFilter;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->i4BridgeMode = pLocalArgs->i4BridgeMode;
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS:
        {
            tVlanNpWrFsMiVlanHwCheckTagAtEgress *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCheckTagAtEgress *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtEgress;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwCheckTagAtEgress;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu1TagSet != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1TagSet), (pLocalArgs->pu1TagSet),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS:
        {
            tVlanNpWrFsMiVlanHwCheckTagAtIngress *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCheckTagAtIngress *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtIngress;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwCheckTagAtIngress;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu1TagSet != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1TagSet), (pLocalArgs->pu1TagSet),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_MI_VLAN_HW_CREATE_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwCreateFdbId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCreateFdbId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwCreateFdbId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwCreateFdbId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwDeleteFdbId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeleteFdbId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteFdbId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeleteFdbId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            break;
        }
        case FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB:
        {
            tVlanNpWrFsMiVlanHwAssociateVlanFdb *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAssociateVlanFdb *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAssociateVlanFdb;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAssociateVlanFdb;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB:
        {
            tVlanNpWrFsMiVlanHwDisassociateVlanFdb *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDisassociateVlanFdb *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDisassociateVlanFdb;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDisassociateVlanFdb;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            pRemoteArgs->i4OptimizeFlag = pLocalArgs->i4OptimizeFlag;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbList *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbList *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbList;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbList;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pVlanFlushInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->VlanFlushInfo),
                        (pLocalArgs->pVlanFlushInfo), sizeof (tVlanFlushInfo));
            }
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            tVlanNpWrFsMiVlanHwFlushPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4OptimizeFlag = pLocalArgs->i4OptimizeFlag;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwFlushFdbId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushFdbId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushFdbId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushFdbId;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            break;
        }
        case FS_MI_VLAN_HW_SET_SHORT_AGEOUT:
        {
            tVlanNpWrFsMiVlanHwSetShortAgeout *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetShortAgeout *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetShortAgeout;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetShortAgeout;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            pRemoteArgs->i4AgingTime = pLocalArgs->i4AgingTime;
            break;
        }
        case FS_MI_VLAN_HW_RESET_SHORT_AGEOUT:
        {
            tVlanNpWrFsMiVlanHwResetShortAgeout *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetShortAgeout *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetShortAgeout;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetShortAgeout;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            pRemoteArgs->i4LongAgeout = pLocalArgs->i4LongAgeout;
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_INFO:
        {
            tVlanNpWrFsMiVlanHwGetVlanInfo *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetVlanInfo *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanInfo;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanInfo;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&
                    (pRemoteArgs->HwEntry.HwMemberPortArray.
                     au4PortArray[0]),
                    (pLocalArgs->pHwEntry->pHwMemberPortArray->
                     pu4PortArray),
                    sizeof (UINT4) *
                    (pLocalArgs->pHwEntry->pHwMemberPortArray->i4Length));
            pRemoteArgs->HwEntry.HwMemberPortArray.i4Length =
                pLocalArgs->pHwEntry->pHwMemberPortArray->i4Length;
            MEMCPY (&
                    (pRemoteArgs->HwEntry.HwUntagPortArray.au4PortArray[0]),
                    (pLocalArgs->pHwEntry->pHwUntagPortArray->pu4PortArray),
                    sizeof (UINT4) *
                    (pLocalArgs->pHwEntry->pHwUntagPortArray->i4Length));
            pRemoteArgs->HwEntry.HwUntagPortArray.i4Length =
                pLocalArgs->pHwEntry->pHwUntagPortArray->i4Length;
            MEMCPY (&
                    (pRemoteArgs->HwEntry.HwFwdAllPortArray.
                     au4PortArray[0]),
                    (pLocalArgs->pHwEntry->pHwFwdAllPortArray->
                     pu4PortArray),
                    sizeof (UINT4) *
                    (pLocalArgs->pHwEntry->pHwFwdAllPortArray->i4Length));
            pRemoteArgs->HwEntry.HwFwdAllPortArray.i4Length =
                pLocalArgs->pHwEntry->pHwFwdAllPortArray->i4Length;
            MEMCPY (&
                    (pRemoteArgs->HwEntry.HwFwdUnregPortArray.
                     au4PortArray[0]),
                    (pLocalArgs->pHwEntry->pHwFwdUnregPortArray->
                     pu4PortArray),
                    sizeof (UINT4) *
                    (pLocalArgs->pHwEntry->pHwFwdUnregPortArray->i4Length));
            pRemoteArgs->HwEntry.HwFwdUnregPortArray.i4Length =
                pLocalArgs->pHwEntry->pHwFwdUnregPortArray->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_GET_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetMcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            MEMCPY (&(pRemoteArgs->HwMcastPorts.au4PortArray[0]),
                    (pLocalArgs->pHwMcastPorts->pu4PortArray),
                    sizeof (UINT4) * (pLocalArgs->pHwMcastPorts->i4Length));
            pRemoteArgs->HwMcastPorts.i4Length =
                pLocalArgs->pHwMcastPorts->i4Length;
            break;
        }
        case FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT:
        {
            tVlanNpWrFsMiVlanHwTraffClassMapInit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwTraffClassMapInit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwTraffClassMapInit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwTraffClassMapInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1Priority = pLocalArgs->u1Priority;
            pRemoteArgs->i4CosqValue = pLocalArgs->i4CosqValue;
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_SET_AGING_TIME:
        {
            tVlanNpWrFsMiBrgSetAgingTime *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiBrgSetAgingTime *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiBrgSetAgingTime;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiBrgSetAgingTime;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->i4AgingTime = pLocalArgs->i4AgingTime;
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_HW_SET_BRG_MODE:
        {
            tVlanNpWrFsMiVlanHwSetBrgMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBrgMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBrgMode;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBrgMode;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4BridgeMode = pLocalArgs->u4BridgeMode;
            break;
        }
        case FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE:
        {
            tVlanNpWrFsMiVlanHwSetBaseBridgeMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBaseBridgeMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBaseBridgeMode;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBaseBridgeMode;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4Mode = pLocalArgs->u4Mode;
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortMacVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddPortMacVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortMacVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortMacVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->bSuppressOption = pLocalArgs->bSuppressOption;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeletePortMacVlanEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortMacVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortMacVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortSubnetVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->SubnetAddr = pLocalArgs->SubnetAddr;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->bARPOption = pLocalArgs->bARPOption;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortSubnetVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->SubnetAddr = pLocalArgs->SubnetAddr;
            break;
        }
        case FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4SubnetAddr = pLocalArgs->u4SubnetAddr;
            pRemoteArgs->u4SubnetMask = pLocalArgs->u4SubnetMask;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->bARPOption = pLocalArgs->bARPOption;
            pRemoteArgs->u1Action = pLocalArgs->u1Action;
            break;
        }
        case FS_MI_VLAN_NP_HW_RUN_MAC_AGEING:
        {
            tVlanNpWrFsMiVlanNpHwRunMacAgeing *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanNpHwRunMacAgeing *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanNpHwRunMacAgeing;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanNpHwRunMacAgeing;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u2FdbId = pLocalArgs->u2FdbId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwSwitchMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSwitchMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSwitchMacLearningLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSwitchMacLearningLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwMacLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMacLearningStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u2FdbId = pLocalArgs->u2FdbId;
            MEMCPY (&(pRemoteArgs->HwEgressPorts.au4PortArray[0]),
                    (pLocalArgs->pHwEgressPorts->pu4PortArray),
                    sizeof (UINT4) * (pLocalArgs->pHwEgressPorts->i4Length));
            pRemoteArgs->HwEgressPorts.i4Length =
                pLocalArgs->pHwEgressPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetFidPortLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetFidPortLearningStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            if (pLocalArgs->HwPortList.pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwPortList.au4PortArray[0]),
                        (pLocalArgs->HwPortList.pu4PortArray),
                        sizeof (UINT4) * (pLocalArgs->HwPortList.i4Length));
            }
            pRemoteArgs->HwPortList.i4Length = pLocalArgs->HwPortList.i4Length;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            pRemoteArgs->u1Action = pLocalArgs->u1Action;
            break;
        }
        case FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->ProtocolId), &(pLocalArgs->ProtocolId),
                    sizeof (tVlanHwTunnelFilters));
            pRemoteArgs->u4TunnelStatus = pLocalArgs->u4TunnelStatus;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortProtectedStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProtectedStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortProtectedStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4ProtectedStatus = pLocalArgs->i4ProtectedStatus;
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntryEx;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntryEx;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllowedToGoPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllowedToGoPorts->i4Length));
            }
            pRemoteArgs->HwAllowedToGoPorts.i4Length =
                pLocalArgs->pHwAllowedToGoPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            if (pLocalArgs->ConnectionId != NULL)
            {
                MEMCPY (&(pRemoteArgs->ConnectionId),
                        (pLocalArgs->ConnectionId), sizeof (tMacAddr));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntryEx *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntryEx;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntryEx;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4Fid;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            break;
        }
        case FS_VLAN_HW_FORWARD_PKT_ON_PORTS:
        {
            tVlanNpWrFsVlanHwForwardPktOnPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsVlanHwForwardPktOnPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsVlanHwForwardPktOnPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsVlanHwForwardPktOnPorts;
            if (pLocalArgs->pu1Packet != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1Packet), (pLocalArgs->pu1Packet),
                        sizeof (UINT1));
            }
            pRemoteArgs->u2PacketLen = pLocalArgs->u2PacketLen;
            if (pLocalArgs->pVlanFwdInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->VlanFwdInfo.VlanTag),
                        &(pLocalArgs->pVlanFwdInfo->VlanTag),
                        sizeof (tVlanTag));
                if (pLocalArgs->pVlanFwdInfo->TagPorts.pu4PortArray != NULL)
                {
                    MEMCPY (&
                            (pRemoteArgs->VlanFwdInfo.TagPorts.au4PortArray[0]),
                            (pLocalArgs->pVlanFwdInfo->TagPorts.pu4PortArray),
                            sizeof (UINT4) *
                            (pLocalArgs->pVlanFwdInfo->TagPorts.i4Length));
                }
                pRemoteArgs->VlanFwdInfo.TagPorts.i4Length =
                    pLocalArgs->pVlanFwdInfo->TagPorts.i4Length;
                if (pLocalArgs->pVlanFwdInfo->UnTagPorts.pu4PortArray != NULL)
                {
                    MEMCPY (&
                            (pRemoteArgs->VlanFwdInfo.UnTagPorts.
                             au4PortArray[0]),
                            (pLocalArgs->pVlanFwdInfo->UnTagPorts.pu4PortArray),
                            sizeof (UINT4) *
                            (pLocalArgs->pVlanFwdInfo->UnTagPorts.i4Length));
                }
                pRemoteArgs->VlanFwdInfo.UnTagPorts.i4Length =
                    pLocalArgs->pVlanFwdInfo->UnTagPorts.i4Length;
                pRemoteArgs->VlanFwdInfo.u4ContextId =
                    pLocalArgs->pVlanFwdInfo->u4ContextId;
            }
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_VLAN_HW_GET_MAC_LEARNING_MODE:
        {
            tVlanNpWrFsVlanHwGetMacLearningMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsVlanHwGetMacLearningMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsVlanHwGetMacLearningMode;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsVlanHwGetMacLearningMode;
            if (pLocalArgs->pu4LearningMode != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4LearningMode),
                        (pLocalArgs->pu4LearningMode), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_INDEX:
        {
            tVlanNpWrFsMiVlanHwSetMcastIndex *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastIndex *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastIndex;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastIndex;
            MEMCPY (&(pRemoteArgs->HwMcastIndexInfo),
                    &(pLocalArgs->HwMcastIndexInfo),
                    sizeof (tHwMcastIndexInfo));
            if (pLocalArgs->pu4McastIndex != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4McastIndex),
                        (pLocalArgs->pu4McastIndex), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortIngressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngressEtherType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngressEtherType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2EtherType = pLocalArgs->u2EtherType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortEgressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEgressEtherType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEgressEtherType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2EtherType = pLocalArgs->u2EtherType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanHwSetPortProperty *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProperty *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProperty;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortProperty;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->VlanPortProperty),
                    &(pLocalArgs->VlanPortProperty),
                    sizeof (tHwVlanPortProperty));
            break;
        }
        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanHwEvbConfigSChIface *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwEvbConfigSChIface;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwEvbConfigSChIface;
            MEMCPY (&pRemoteArgs->VlanEvbHwConfigInfo,
                    &pLocalArgs->VlanEvbHwConfigInfo,
                    sizeof (tVlanEvbHwConfigInfo));
            break;
        }
        case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBridgePortType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBridgePortType;

            MEMCPY (&pRemoteArgs->VlanHwPortInfo,
                    &pLocalArgs->VlanHwPortInfo, sizeof (tVlanHwPortInfo));
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetProviderBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProviderBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProviderBridgePortType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4PortType = pLocalArgs->u4PortType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanTranslationStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanTranslationStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddSVlanTranslationEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2LocalSVlan = pLocalArgs->u2LocalSVlan;
            pRemoteArgs->u2RelaySVlan = pLocalArgs->u2RelaySVlan;
            break;
        }
        case FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDelSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelSVlanTranslationEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2LocalSVlan = pLocalArgs->u2LocalSVlan;
            pRemoteArgs->u2RelaySVlan = pLocalArgs->u2RelaySVlan;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddEtherTypeSwapEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddEtherTypeSwapEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2LocalEtherType = pLocalArgs->u2LocalEtherType;
            pRemoteArgs->u2RelayEtherType = pLocalArgs->u2RelayEtherType;
            break;
        }
        case FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelEtherTypeSwapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelEtherTypeSwapEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelEtherTypeSwapEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2LocalEtherType = pLocalArgs->u2LocalEtherType;
            pRemoteArgs->u2RelayEtherType = pLocalArgs->u2RelayEtherType;
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwAddSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddSVlanMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanSVlanMap), &(pLocalArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_DELETE_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwDeleteSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeleteSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeleteSVlanMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanSVlanMap), &(pLocalArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanClassifyMethod;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanClassifyMethod;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1TableType = pLocalArgs->u1TableType;
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanNpWrFsMiVlanHwMulticastMacTableLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwMulticastMacTableLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwMulticastMacTableLimit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4MacLimit = pLocalArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwSetPortCustomerVlan *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortCustomerVlan;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortCustomerVlan;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->CVlanId), &(pLocalArgs->CVlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwResetPortCustomerVlan *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetPortCustomerVlan *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetPortCustomerVlan;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetPortCustomerVlan;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwCreateProviderEdgePort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwCreateProviderEdgePort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwCreateProviderEdgePort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->PepConfig), &(pLocalArgs->PepConfig),
                    sizeof (tHwVlanPbPepInfo));
            break;
        }
        case FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwDelProviderEdgePort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelProviderEdgePort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelProviderEdgePort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelProviderEdgePort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPepPvid *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepPvid *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepPvid;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepPvid;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->Pvid), &(pLocalArgs->Pvid),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPepAccFrameType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepAccFrameType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepAccFrameType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepAccFrameType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1AccepFrameType = pLocalArgs->u1AccepFrameType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetPepDefUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepDefUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPepDefUserPriority;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            pRemoteArgs->i4DefUsrPri = pLocalArgs->i4DefUsrPri;
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPepIngFiltering *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepIngFiltering *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepIngFiltering;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepIngFiltering;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1IngFilterEnable = pLocalArgs->u1IngFilterEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagPep *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagPep *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagPep;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagPep;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanSVlanMap), &(pLocalArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagCep *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagCep *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagCep;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagCep;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanSVlanMap), &(pLocalArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpEncodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpEncodTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpEncodTbl;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->NpPbVlanPcpInfo),
                    &(pLocalArgs->NpPbVlanPcpInfo), sizeof (tHwVlanPbPcpInfo));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_DECOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpDecodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpDecodTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpDecodTbl;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->NpPbVlanPcpInfo),
                    &(pLocalArgs->NpPbVlanPcpInfo), sizeof (tHwVlanPbPcpInfo));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_USE_DEI:
        {
            tVlanNpWrFsMiVlanHwSetPortUseDei *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortUseDei *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortUseDei;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortUseDei;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1UseDei = pLocalArgs->u1UseDei;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanNpWrFsMiVlanHwSetPortReqDropEncoding *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortReqDropEncoding;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortReqDropEncoding;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1ReqDrpEncoding = pLocalArgs->u1ReqDrpEncoding;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanNpWrFsMiVlanHwSetPortPcpSelection *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPcpSelection;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortPcpSelection;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2PcpSelection = pLocalArgs->u2PcpSelection;
            break;
        }
        case FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwSetServicePriRegenEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetServicePriRegenEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetServicePriRegenEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            pRemoteArgs->i4RecvPriority = pLocalArgs->i4RecvPriority;
            pRemoteArgs->i4RegenPriority = pLocalArgs->i4RegenPriority;
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS:
        {
            tVlanNpWrFsMiVlanHwSetTunnelMacAddress *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTunnelMacAddress *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelMacAddress;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetTunnelMacAddress;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u2Protocol = pLocalArgs->u2Protocol;
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetNextSVlanMap;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanSVlanMap), &(pLocalArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            if (pLocalArgs->pRetVlanSVlanMap != NULL)
            {
                MEMCPY (&(pRemoteArgs->RetVlanSVlanMap),
                        (pLocalArgs->pRetVlanSVlanMap), sizeof (tVlanSVlanMap));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetNextSVlanTranslationEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->u2LocalSVlan), &(pLocalArgs->u2LocalSVlan),
                    sizeof (tVlanId));
            if (pLocalArgs->pVidTransEntryInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->VidTransEntryInfo),
                        (pLocalArgs->pVidTransEntryInfo),
                        sizeof (tVidTransEntryInfo));
            }
            break;
        }
#endif /* PB_WANTED */
#ifdef MBSM_WANTED
#ifdef PB_WANTED
        case FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        case FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN:
        case FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL:
        case FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION:
        case FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        case FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT:
#endif /* PB_WANTED */
        case FS_MI_VLAN_MBSM_HW_INIT:
        case FS_MI_VLAN_MBSM_HW_SET_BRG_MODE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY:
        case FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID:
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PVID:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING:
        case FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY:
        case FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT:
        case FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP:
        case FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES:
        case FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY:
        case FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS:
        case FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS:
        case FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER:
        case FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_MBSM_SET_AGING_TIME:
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS:
        case FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS:
        case FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS:
        case FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX:
        case FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS:
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
        {
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmHwSetBridgePortType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwSetBridgePortType;
            MEMCPY (&pRemoteArgs->VlanHwPortInfo, &pLocalArgs->VlanHwPortInfo,
                    sizeof (tVlanHwPortInfo));
            break;
        }
        case FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmHwEvbConfigSChIface *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEvbConfigSChIface;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwEvbConfigSChIface;
            MEMCPY (&pRemoteArgs->VlanEvbHwConfigInfo,
                    &pLocalArgs->VlanEvbHwConfigInfo,
                    sizeof (tVlanEvbHwConfigInfo));
            break;
        }
#endif /* MBSM_WANTED */
        case FS_MI_VLAN_HW_SET_LOOPBACK_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetVlanLoopbackStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetVlanLoopbackStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLoopbackStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetVlanLoopbackStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->VlanId = pLocalArgs->VlanId;
            pRemoteArgs->i4LoopbackStatus = pLocalArgs->i4LoopbackStatus;
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwSetCVlanStat *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetCVlanStat *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCVlanStat;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCVlanStat;
            MEMCPY (&(pRemoteArgs->VlanStat), &(pLocalArgs->VlanStat),
                    sizeof (tHwVlanCVlanStat));
            break;
        }
        case FS_MI_VLAN_HW_GET_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwGetCVlanStat *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetCVlanStat *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetCVlanStat;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetCVlanStat;
            pRemoteArgs->u2CVlanId = pLocalArgs->u2CVlanId;
            pRemoteArgs->u2Port = pLocalArgs->u2Port;
            pRemoteArgs->u1StatsType = pLocalArgs->u1StatsType;
            pRemoteArgs->pu4VlanStatsValue = pLocalArgs->pu4VlanStatsValue;
            break;
        }

        case FS_MI_VLAN_HW_CLEAR_CVLAN_STAT:
        {

            tVlanNpWrFsMiVlanHwClearCVlanStat *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwClearCVlanStat *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwClearCVlanStat;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwClearCVlanStat;
            pRemoteArgs->u2CVlanId = pLocalArgs->u2CVlanId;
            pRemoteArgs->u2Port = pLocalArgs->u2Port;
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVlanConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVlanConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanRemoteNpModInfo *pVlanRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
    pVlanRemoteNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetUnRegGroupsPorts;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            if (pLocalArgs->pHwUnRegPorts->pu4PortArray != NULL)
            {
                MEMCPY ((pLocalArgs->pHwUnRegPorts->pu4PortArray),
                        &(pRemoteArgs->HwUnRegPorts.au4PortArray[0]),
                        sizeof (UINT4) * (pRemoteArgs->HwUnRegPorts.i4Length));
            }
            pLocalArgs->pHwUnRegPorts->i4Length =
                pRemoteArgs->HwUnRegPorts.i4Length;
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4Fid = pRemoteArgs->u4Fid;
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray != NULL)
            {
                MEMCPY ((pLocalArgs->pHwAllowedToGoPorts->pu4PortArray),
                        &(pRemoteArgs->HwAllowedToGoPorts.au4PortArray[0]),
                        sizeof (UINT4) *
                        (pRemoteArgs->HwAllowedToGoPorts.i4Length));
            }
            pLocalArgs->pHwAllowedToGoPorts->i4Length =
                pRemoteArgs->HwAllowedToGoPorts.i4Length;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStMcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStMcastEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddStMcastEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            pLocalArgs->i4RcvPort = pRemoteArgs->i4RcvPort;
            if (pLocalArgs->pHwMcastPorts->pu4PortArray != NULL)
            {
                MEMCPY ((pLocalArgs->pHwMcastPorts->pu4PortArray),
                        &(pRemoteArgs->HwMcastPorts.au4PortArray[0]),
                        sizeof (UINT4) * (pRemoteArgs->HwMcastPorts.i4Length));
            }
            pLocalArgs->pHwMcastPorts->i4Length =
                pRemoteArgs->HwMcastPorts.i4Length;
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_RESET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwResetMcastPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetMcastPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetMcastPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetMcastPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwSetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanMemberPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetVlanMemberPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1IsTagged = pRemoteArgs->u1IsTagged;
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwResetVlanMemberPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanMemberPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetVlanMemberPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPortPvid *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPvid *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPvid;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortPvid;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortAccFrameType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortAccFrameType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortAccFrameType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1AccFrameType = pRemoteArgs->u1AccFrameType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPortIngFiltering *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngFiltering;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngFiltering;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1IngFilterEnable = pRemoteArgs->u1IngFilterEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMacBasedStatusOnPort *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetMacBasedStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetMacBasedStatusOnPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1MacBasedVlanEnable =
                pRemoteArgs->u1MacBasedVlanEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1SubnetBasedVlanEnable =
                pRemoteArgs->u1SubnetBasedVlanEnable;
            break;
        }
        case FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwEnableProtoVlanOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwEnableProtoVlanOnPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1VlanProtoEnable = pRemoteArgs->u1VlanProtoEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetDefUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetDefUserPriority;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4DefPriority = pRemoteArgs->i4DefPriority;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanNpWrFsMiVlanHwSetPortNumTrafClasses *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortNumTrafClasses;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortNumTrafClasses;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4NumTraffClass = pRemoteArgs->i4NumTraffClass;
            break;
        }
        case FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetRegenUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetRegenUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetRegenUserPriority;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4UserPriority = pRemoteArgs->i4UserPriority;
            pLocalArgs->i4RegenPriority = pRemoteArgs->i4RegenPriority;
            break;
        }
        case FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanNpWrFsMiVlanHwSetTraffClassMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTraffClassMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetTraffClassMap;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4UserPriority = pRemoteArgs->i4UserPriority;
            pLocalArgs->i4TraffClass = pRemoteArgs->i4TraffClass;
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS:
        {
            tVlanNpWrFsMiVlanHwGetPortStats *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetPortStats *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u1StatsType = pRemoteArgs->u1StatsType;
            if (pLocalArgs->pu4PortStatsValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4PortStatsValue),
                        &(pRemoteArgs->u4PortStatsValue), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS64:
        {
            tVlanNpWrFsMiVlanHwGetPortStats64 *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetPortStats64 *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats64;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats64;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u1StatsType = pRemoteArgs->u1StatsType;
            if (pLocalArgs->pValue != NULL)
            {
                MEMCPY ((pLocalArgs->pValue), &(pRemoteArgs->Value),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE:
        {
            tVlanNpWrFsMiVlanHwSetPortTunnelMode *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortTunnelMode *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortTunnelMode;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortTunnelMode;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u4Mode = pRemoteArgs->u4Mode;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbId *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbId *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbId;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbId;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            pLocalArgs->u4Fid = pRemoteArgs->u4Fid;
            pLocalArgs->i4OptimizeFlag = pRemoteArgs->i4OptimizeFlag;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbList *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbList *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbList;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbList;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            if (pLocalArgs->pVlanFlushInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pVlanFlushInfo),
                        &(pRemoteArgs->VlanFlushInfo), sizeof (tVlanFlushInfo));
            }
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            tVlanNpWrFsMiVlanHwFlushPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwFlushPort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4OptimizeFlag = pRemoteArgs->i4OptimizeFlag;
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortMacVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddPortMacVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortMacVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortMacVlanEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->bSuppressOption = pRemoteArgs->bSuppressOption;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeletePortMacVlanEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortMacVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortMacVlanEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY ((pLocalArgs->MacAddr), &(pRemoteArgs->MacAddr),
                    sizeof (tMacAddr));
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortSubnetVlanEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->SubnetAddr = pRemoteArgs->SubnetAddr;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->bARPOption = pRemoteArgs->bARPOption;
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortSubnetVlanEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->SubnetAddr = pRemoteArgs->SubnetAddr;
            break;
        }
        case FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u4SubnetAddr = pRemoteArgs->u4SubnetAddr;
            pLocalArgs->u4SubnetMask = pRemoteArgs->u4SubnetMask;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->bARPOption = pRemoteArgs->bARPOption;
            pLocalArgs->u1Action = pRemoteArgs->u1Action;
            break;
        }
        case FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetFidPortLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetFidPortLearningStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4Fid = pRemoteArgs->u4Fid;
            if (pLocalArgs->HwPortList.pu4PortArray != NULL)
            {
                MEMCPY ((pLocalArgs->HwPortList.pu4PortArray),
                        &(pRemoteArgs->HwPortList.au4PortArray[0]),
                        sizeof (UINT4) * (pRemoteArgs->HwPortList.i4Length));
            }
            pLocalArgs->HwPortList.i4Length = pRemoteArgs->HwPortList.i4Length;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            pLocalArgs->u1Action = pRemoteArgs->u1Action;
            break;
        }
        case FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->ProtocolId), &(pRemoteArgs->ProtocolId),
                    sizeof (tVlanHwTunnelFilters));
            pLocalArgs->u4TunnelStatus = pRemoteArgs->u4TunnelStatus;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortProtectedStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProtectedStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortProtectedStatus;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4ProtectedStatus = pRemoteArgs->i4ProtectedStatus;
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningStatus;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_INDEX:
        {
            tVlanNpWrFsMiVlanHwSetMcastIndex *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetMcastIndex *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastIndex;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastIndex;
            MEMCPY (&(pLocalArgs->HwMcastIndexInfo),
                    &(pRemoteArgs->HwMcastIndexInfo),
                    sizeof (tHwMcastIndexInfo));
            if (pLocalArgs->pu4McastIndex != NULL)
            {
                MEMCPY ((pLocalArgs->pu4McastIndex),
                        &(pRemoteArgs->u4McastIndex), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortIngressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngressEtherType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngressEtherType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2EtherType = pRemoteArgs->u2EtherType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortEgressEtherType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEgressEtherType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEgressEtherType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2EtherType = pRemoteArgs->u2EtherType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanHwSetPortProperty *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortProperty *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProperty;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortProperty;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->VlanPortProperty),
                    &(pRemoteArgs->VlanPortProperty),
                    sizeof (tHwVlanPortProperty));
            break;
        }

        case FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortPktReflectStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortPktReflectStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortPktReflectStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortPktReflectStatus;
            if (pLocalArgs->pPortReflectEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pPortReflectEntry),
                        &(pRemoteArgs->PortReflectEntry),
                        sizeof (tFsNpVlanPortReflectEntry));
            }
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetProviderBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProviderBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProviderBridgePortType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u4PortType = pRemoteArgs->u4PortType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanTranslationStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanTranslationStatus;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddSVlanTranslationEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2LocalSVlan = pRemoteArgs->u2LocalSVlan;
            pLocalArgs->u2RelaySVlan = pRemoteArgs->u2RelaySVlan;
            break;
        }
        case FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwDelSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelSVlanTranslationEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2LocalSVlan = pRemoteArgs->u2LocalSVlan;
            pLocalArgs->u2RelaySVlan = pRemoteArgs->u2RelaySVlan;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddEtherTypeSwapEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddEtherTypeSwapEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2LocalEtherType = pRemoteArgs->u2LocalEtherType;
            pLocalArgs->u2RelayEtherType = pRemoteArgs->u2RelayEtherType;
            break;
        }
        case FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelEtherTypeSwapEntry *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelEtherTypeSwapEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelEtherTypeSwapEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2LocalEtherType = pRemoteArgs->u2LocalEtherType;
            pLocalArgs->u2RelayEtherType = pRemoteArgs->u2RelayEtherType;
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwAddSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddSVlanMap;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanSVlanMap), &(pRemoteArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_DELETE_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwDeleteSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDeleteSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeleteSVlanMap;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanSVlanMap), &(pRemoteArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanClassifyMethod;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanClassifyMethod;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1TableType = pRemoteArgs->u1TableType;
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningLimit;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u4MacLimit = pRemoteArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanNpWrFsMiVlanHwMulticastMacTableLimit *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwMulticastMacTableLimit;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwMulticastMacTableLimit;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4MacLimit = pRemoteArgs->u4MacLimit;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwSetPortCustomerVlan *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortCustomerVlan;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortCustomerVlan;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->CVlanId), &(pRemoteArgs->CVlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwResetPortCustomerVlan *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwResetPortCustomerVlan *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwResetPortCustomerVlan;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetPortCustomerVlan;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwCreateProviderEdgePort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwCreateProviderEdgePort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwCreateProviderEdgePort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pLocalArgs->PepConfig), &(pRemoteArgs->PepConfig),
                    sizeof (tHwVlanPbPepInfo));
            break;
        }
        case FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwDelProviderEdgePort *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwDelProviderEdgePort *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwDelProviderEdgePort;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelProviderEdgePort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPepPvid *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepPvid *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepPvid;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepPvid;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pLocalArgs->Pvid), &(pRemoteArgs->Pvid),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPepAccFrameType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepAccFrameType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepAccFrameType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepAccFrameType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            pLocalArgs->u1AccepFrameType = pRemoteArgs->u1AccepFrameType;
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetPepDefUserPriority *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepDefUserPriority *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepDefUserPriority;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPepDefUserPriority;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            pLocalArgs->i4DefUsrPri = pRemoteArgs->i4DefUsrPri;
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPepIngFiltering *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPepIngFiltering *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepIngFiltering;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepIngFiltering;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            pLocalArgs->u1IngFilterEnable = pRemoteArgs->u1IngFilterEnable;
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagPep *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagPep *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagPep;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagPep;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanSVlanMap), &(pRemoteArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagCep *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagCep *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagCep;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagCep;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanSVlanMap), &(pRemoteArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpEncodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpEncodTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpEncodTbl;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->NpPbVlanPcpInfo),
                    &(pRemoteArgs->NpPbVlanPcpInfo), sizeof (tHwVlanPbPcpInfo));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_DECOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpDecodTbl *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpDecodTbl;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpDecodTbl;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->NpPbVlanPcpInfo),
                    &(pRemoteArgs->NpPbVlanPcpInfo), sizeof (tHwVlanPbPcpInfo));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_USE_DEI:
        {
            tVlanNpWrFsMiVlanHwSetPortUseDei *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortUseDei *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortUseDei;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortUseDei;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1UseDei = pRemoteArgs->u1UseDei;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanNpWrFsMiVlanHwSetPortReqDropEncoding *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding *pRemoteArgs = NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortReqDropEncoding;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortReqDropEncoding;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1ReqDrpEncoding = pRemoteArgs->u1ReqDrpEncoding;
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanNpWrFsMiVlanHwSetPortPcpSelection *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPcpSelection;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortPcpSelection;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2PcpSelection = pRemoteArgs->u2PcpSelection;
            break;
        }
        case FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwSetServicePriRegenEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetServicePriRegenEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetServicePriRegenEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            pLocalArgs->i4RecvPriority = pRemoteArgs->i4RecvPriority;
            pLocalArgs->i4RegenPriority = pRemoteArgs->i4RegenPriority;
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanMap *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanMap *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanMap;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetNextSVlanMap;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanSVlanMap), &(pRemoteArgs->VlanSVlanMap),
                    sizeof (tVlanSVlanMap));
            if (pLocalArgs->pRetVlanSVlanMap != NULL)
            {
                MEMCPY ((pLocalArgs->pRetVlanSVlanMap),
                        &(pRemoteArgs->RetVlanSVlanMap),
                        sizeof (tVlanSVlanMap));
            }
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanTranslationEntry;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetNextSVlanTranslationEntry;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->u2LocalSVlan), &(pRemoteArgs->u2LocalSVlan),
                    sizeof (tVlanId));
            if (pLocalArgs->pVidTransEntryInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pVidTransEntryInfo),
                        &(pRemoteArgs->VidTransEntryInfo),
                        sizeof (tVidTransEntryInfo));
            }
            break;
        }
#endif /* PB_WANTED */

        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanHwEvbConfigSChIface *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwEvbConfigSChIface;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwEvbConfigSChIface;
            MEMCPY (&pLocalArgs->VlanEvbHwConfigInfo,
                    &pRemoteArgs->VlanEvbHwConfigInfo,
                    sizeof (tVlanEvbHwConfigInfo));
            break;
        }

        case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwSetBridgePortType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBridgePortType;

            MEMCPY (&pLocalArgs->VlanHwPortInfo, &pRemoteArgs->VlanHwPortInfo,
                    sizeof (tVlanHwPortInfo));
            break;
        }

#ifdef MBSM_WANTED
#ifdef PB_WANTED
        case FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        case FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN:
        case FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL:
        case FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION:
        case FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        case FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT:
#endif /* PB_WANTED */
        case FS_MI_VLAN_MBSM_HW_INIT:
        case FS_MI_VLAN_MBSM_HW_SET_BRG_MODE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY:
        case FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID:
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT:
        case FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PVID:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING:
        case FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY:
        case FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT:
        case FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP:
        case FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES:
        case FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY:
        case FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS:
        case FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS:
        case FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER:
        case FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_MBSM_SET_AGING_TIME:
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS:
        case FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS:
        case FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS:
        case FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT:
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX:
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
            break;

        case FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetBridgePortType *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmHwSetBridgePortType *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBridgePortType;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwSetBridgePortType;

            MEMCPY (&pLocalArgs->VlanHwPortInfo, &pRemoteArgs->VlanHwPortInfo,
                    sizeof (tVlanHwPortInfo));
            break;
        }
        case FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmHwEvbConfigSChIface *pRemoteArgs = NULL;
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEvbConfigSChIface;
            pRemoteArgs =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwEvbConfigSChIface;
            MEMCPY (&pLocalArgs->VlanEvbHwConfigInfo,
                    &pRemoteArgs->VlanEvbHwConfigInfo,
                    sizeof (tVlanEvbHwConfigInfo));
            break;
        }
#endif /* MBSM_WANTED */
        case FS_MI_VLAN_HW_SET_LOOPBACK_STATUS:
        {
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS:
        {
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVlanRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tVlanNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVlanRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVlanRemoteNpModInfo *pVlanRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pVlanRemoteNpModInfo = &(pRemoteHwNpInput->VlanRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_VLAN_HW_INIT:
        {
            tVlanRemoteNpWrFsMiVlanHwInit *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwInit;
            u1RetVal = FsMiVlanHwInit (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DE_INIT:
        {
            tVlanRemoteNpWrFsMiVlanHwDeInit *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeInit;
            u1RetVal = FsMiVlanHwDeInit (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts *pInput = NULL;
            tHwPortArray        HwAllGroupPorts;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetAllGroupsPorts;
            HwAllGroupPorts.pu4PortArray =
                &(pInput->HwAllGroupPorts.au4PortArray[0]);
            HwAllGroupPorts.i4Length = pInput->HwAllGroupPorts.i4Length;
            u1RetVal =
                FsMiVlanHwSetAllGroupsPorts (pInput->u4ContextId,
                                             pInput->VlanId,
                                             &(HwAllGroupPorts));
            pInput->HwAllGroupPorts.i4Length = HwAllGroupPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS:
        {
            tVlanRemoteNpWrFsMiVlanHwResetAllGroupsPorts *pInput = NULL;
            tHwPortArray        HwResetAllGroupPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetAllGroupsPorts;
            HwResetAllGroupPorts.pu4PortArray =
                &(pInput->HwResetAllGroupPorts.au4PortArray[0]);
            HwResetAllGroupPorts.i4Length =
                pInput->HwResetAllGroupPorts.i4Length;
            u1RetVal =
                FsMiVlanHwResetAllGroupsPorts (pInput->u4ContextId,
                                               pInput->VlanId,
                                               &(HwResetAllGroupPorts));
            pInput->HwResetAllGroupPorts.i4Length =
                HwResetAllGroupPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS:
        {
            tVlanRemoteNpWrFsMiVlanHwResetUnRegGroupsPorts *pInput = NULL;
            tHwPortArray        HwResetUnRegGroupPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetUnRegGroupsPorts;
            HwResetUnRegGroupPorts.pu4PortArray =
                &(pInput->HwResetUnRegGroupPorts.au4PortArray[0]);
            HwResetUnRegGroupPorts.i4Length =
                pInput->HwResetUnRegGroupPorts.i4Length;
            u1RetVal =
                FsMiVlanHwResetUnRegGroupsPorts (pInput->u4ContextId,
                                                 pInput->VlanId,
                                                 &(HwResetUnRegGroupPorts));
            pInput->HwResetUnRegGroupPorts.i4Length =
                HwResetUnRegGroupPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwPortPktReflectStatus *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortPktReflectStatus;
            u1RetVal =
                FsMiVlanHwPortPktReflectStatus (&(pInput->PortReflectEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts *pInput = NULL;
            tHwPortArray        HwUnRegPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts;
            HwUnRegPorts.pu4PortArray = &(pInput->HwUnRegPorts.au4PortArray[0]);
            HwUnRegPorts.i4Length = pInput->HwUnRegPorts.i4Length;
            u1RetVal =
                FsMiVlanHwSetUnRegGroupsPorts (pInput->u4ContextId,
                                               pInput->VlanId, &(HwUnRegPorts));
            pInput->HwUnRegPorts.i4Length = HwUnRegPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pInput = NULL;
            tHwPortArray        HwAllowedToGoPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            HwAllowedToGoPorts.pu4PortArray =
                &(pInput->HwAllowedToGoPorts.au4PortArray[0]);
            HwAllowedToGoPorts.i4Length = pInput->HwAllowedToGoPorts.i4Length;
            u1RetVal =
                FsMiVlanHwAddStaticUcastEntry (pInput->u4ContextId,
                                               pInput->u4Fid, pInput->MacAddr,
                                               pInput->u4Port,
                                               &(HwAllowedToGoPorts),
                                               pInput->u1Status);
            pInput->HwAllowedToGoPorts.i4Length = HwAllowedToGoPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelStaticUcastEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelStaticUcastEntry;
            u1RetVal =
                FsMiVlanHwDelStaticUcastEntry (pInput->u4ContextId,
                                               pInput->u4Fid, pInput->MacAddr,
                                               pInput->u4Port);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_HW_GET_PORT_FROM_FDB:
        {
            tVlanRemoteNpWrFsNpHwGetPortFromFdb *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsNpHwGetPortFromFdb;
            u1RetVal =
                FsMiVlanHwGetPortFromFdb (pInput->u2VlanId, (pInput->pHwAddr),
                                          &(pInput->u4Port));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

#ifndef SW_LEARNING
        case FS_MI_VLAN_HW_GET_FDB_COUNT:
        {
            tVlanRemoteNpWrFsMiVlanHwGetFdbCount *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetFdbCount;
            u1RetVal =
                FsMiVlanHwGetFdbCount (pInput->u4ContextId, pInput->u4FdbId,
                                       &(pInput->u4Count));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* SW_LEARNING */
        case FS_MI_VLAN_HW_ADD_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddMcastEntry *pInput = NULL;
            tHwPortArray        HwMcastPorts;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddMcastEntry;
            HwMcastPorts.pu4PortArray = &(pInput->HwMcastPorts.au4PortArray[0]);
            HwMcastPorts.i4Length = pInput->HwMcastPorts.i4Length;
            u1RetVal =
                FsMiVlanHwAddMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                         pInput->MacAddr, &(HwMcastPorts));
            pInput->HwMcastPorts.i4Length = HwMcastPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry *pInput = NULL;
            tHwPortArray        HwMcastPorts;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddStMcastEntry;
            HwMcastPorts.pu4PortArray = &(pInput->HwMcastPorts.au4PortArray[0]);
            HwMcastPorts.i4Length = pInput->HwMcastPorts.i4Length;
            u1RetVal =
                FsMiVlanHwAddStMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                           pInput->MacAddr, pInput->i4RcvPort,
                                           &(HwMcastPorts));
            pInput->HwMcastPorts.i4Length = HwMcastPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetMcastPort *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastPort;
            u1RetVal =
                FsMiVlanHwSetMcastPort (pInput->u4ContextId, pInput->VlanId,
                                        pInput->MacAddr, pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_MCAST_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwResetMcastPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetMcastPort;
            u1RetVal =
                FsMiVlanHwResetMcastPort (pInput->u4ContextId, pInput->VlanId,
                                          pInput->MacAddr, pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelMcastEntry *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelMcastEntry;
            u1RetVal =
                FsMiVlanHwDelMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                         pInput->MacAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelStMcastEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelStMcastEntry;
            u1RetVal =
                FsMiVlanHwDelStMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                           pInput->MacAddr, pInput->i4RcvPort);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddVlanEntry *pInput = NULL;
            tHwPortArray        HwEgressPorts;
            tHwPortArray        HwUnTagPorts;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanEntry;
            HwEgressPorts.pu4PortArray =
                &(pInput->HwEgressPorts.au4PortArray[0]);
            HwEgressPorts.i4Length = pInput->HwEgressPorts.i4Length;
            HwUnTagPorts.pu4PortArray = &(pInput->HwUnTagPorts.au4PortArray[0]);
            HwUnTagPorts.i4Length = pInput->HwUnTagPorts.i4Length;
            u1RetVal =
                FsMiVlanHwAddVlanEntry (pInput->u4ContextId, pInput->VlanId,
                                        &(HwEgressPorts), &(HwUnTagPorts));
            pInput->HwEgressPorts.i4Length = HwEgressPorts.i4Length;
            pInput->HwUnTagPorts.i4Length = HwUnTagPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelVlanEntry *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelVlanEntry;
            u1RetVal =
                FsMiVlanHwDelVlanEntry (pInput->u4ContextId, pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetVlanMemberPort;
            u1RetVal =
                FsMiVlanHwSetVlanMemberPort (pInput->u4ContextId,
                                             pInput->VlanId, pInput->u4IfIndex,
                                             pInput->u1IsTagged);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetVlanMemberPort;
            u1RetVal =
                FsMiVlanHwResetVlanMemberPort (pInput->u4ContextId,
                                               pInput->VlanId,
                                               pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PVID:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortPvid *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortPvid;
            u1RetVal =
                FsMiVlanHwSetPortPvid (pInput->u4ContextId, pInput->u4IfIndex,
                                       pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID:
        {
            tVlanRemoteNpWrFsMiVlanHwSetDefaultVlanId *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetDefaultVlanId;
            u1RetVal =
                FsMiVlanHwSetDefaultVlanId (pInput->u4ContextId,
                                            pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef L2RED_WANTED
        case FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwGetStMcastEntry *pInput = NULL;
            tHwPortArray        HwMcastPorts;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetStMcastEntry;
            HwMcastPorts.pu4PortArray = &(pInput->HwMcastPorts.au4PortArray[0]);
            HwMcastPorts.i4Length = pInput->HwMcastPorts.i4Length;
            u1RetVal =
                FsMiVlanHwGetStMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                           pInput->MacAddr, pInput->u4RcvPort,
                                           &(HwMcastPorts));
            pInput->HwMcastPorts.i4Length = HwMcastPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntry *pInput = NULL;
            tHwPortArray        AllowedToGoPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntry;
            AllowedToGoPorts.pu4PortArray =
                &(pInput->AllowedToGoPorts.au4PortArray[0]);
            AllowedToGoPorts.i4Length = pInput->AllowedToGoPorts.i4Length;
            u1RetVal =
                FsMiVlanHwGetStaticUcastEntry (pInput->u4ContextId,
                                               pInput->u4Fid, pInput->MacAddr,
                                               pInput->u4Port,
                                               &(AllowedToGoPorts),
                                               &(pInput->u1Status));
            pInput->AllowedToGoPorts.i4Length = AllowedToGoPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* L2RED_WANTED */
        case FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortAccFrameType;
            u1RetVal =
                FsMiVlanHwSetPortAccFrameType (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->u1AccFrameType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ING_FILTERING:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngFiltering;
            u1RetVal =
                FsMiVlanHwSetPortIngFiltering (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->u1IngFilterEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_VLAN_ENABLE:
        {
            tVlanRemoteNpWrFsMiVlanHwVlanEnable *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwVlanEnable;
            u1RetVal = FsMiVlanHwVlanEnable (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_VLAN_DISABLE:
        {
            tVlanRemoteNpWrFsMiVlanHwVlanDisable *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwVlanDisable;
            u1RetVal = FsMiVlanHwVlanDisable (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetVlanLearningType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetVlanLearningType;
            u1RetVal =
                FsMiVlanHwSetVlanLearningType (pInput->u4ContextId,
                                               pInput->u1LearningType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetMacBasedStatusOnPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetMacBasedStatusOnPort;
            u1RetVal =
                FsMiVlanHwSetMacBasedStatusOnPort (pInput->u4ContextId,
                                                   pInput->u4IfIndex,
                                                   pInput->
                                                   u1MacBasedVlanEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            u1RetVal =
                FsMiVlanHwSetSubnetBasedStatusOnPort (pInput->u4ContextId,
                                                      pInput->u4IfIndex,
                                                      pInput->
                                                      u1SubnetBasedVlanEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwEnableProtoVlanOnPort;
            u1RetVal =
                FsMiVlanHwEnableProtoVlanOnPort (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->u1VlanProtoEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetDefUserPriority;
            u1RetVal =
                FsMiVlanHwSetDefUserPriority (pInput->u4ContextId,
                                              pInput->u4IfIndex,
                                              pInput->i4DefPriority);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortNumTrafClasses;
            u1RetVal =
                FsMiVlanHwSetPortNumTrafClasses (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->i4NumTraffClass);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetRegenUserPriority;
            u1RetVal =
                FsMiVlanHwSetRegenUserPriority (pInput->u4ContextId,
                                                pInput->u4IfIndex,
                                                pInput->i4UserPriority,
                                                pInput->i4RegenPriority);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetTraffClassMap;
            u1RetVal =
                FsMiVlanHwSetTraffClassMap (pInput->u4ContextId,
                                            pInput->u4IfIndex,
                                            pInput->i4UserPriority,
                                            pInput->i4TraffClass);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GMRP_ENABLE:
        {
            tVlanRemoteNpWrFsMiVlanHwGmrpEnable *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGmrpEnable;
            u1RetVal = FsMiVlanHwGmrpEnable (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GMRP_DISABLE:
        {
            tVlanRemoteNpWrFsMiVlanHwGmrpDisable *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGmrpDisable;
            u1RetVal = FsMiVlanHwGmrpDisable (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_DELETE_ALL_FDB_ENTRIES:
        {
            tVlanRemoteNpWrFsMiNpDeleteAllFdbEntries *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiNpDeleteAllFdbEntries;
            u1RetVal = FsMiNpDeleteAllFdbEntries (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwAddVlanProtocolMap *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddVlanProtocolMap;
            u1RetVal =
                FsMiVlanHwAddVlanProtocolMap (pInput->u4ContextId,
                                              pInput->u4IfIndex,
                                              pInput->u4GroupId,
                                              &(pInput->ProtoTemplate),
                                              pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwDelVlanProtocolMap *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDelVlanProtocolMap;
            u1RetVal =
                FsMiVlanHwDelVlanProtocolMap (pInput->u4ContextId,
                                              pInput->u4IfIndex,
                                              pInput->u4GroupId,
                                              &(pInput->ProtoTemplate));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS:
        {
            tVlanRemoteNpWrFsMiVlanHwGetPortStats *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats;
            u1RetVal =
                FsMiVlanHwGetPortStats (pInput->u4ContextId, pInput->u4Port,
                                        pInput->VlanId, pInput->u1StatsType,
                                        &(pInput->u4PortStatsValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS64:
        {
            tVlanRemoteNpWrFsMiVlanHwGetPortStats64 *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetPortStats64;
            u1RetVal =
                FsMiVlanHwGetPortStats64 (pInput->u4ContextId, pInput->u4Port,
                                          pInput->VlanId, pInput->u1StatsType,
                                          &(pInput->Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_STATS:
        {
            tVlanRemoteNpWrFsMiVlanHwGetVlanStats *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanStats;
            u1RetVal =
                FsMiVlanHwGetVlanStats (pInput->u4ContextId, pInput->VlanId,
                                        pInput->u1StatsType,
                                        &(pInput->u4VlanStatsValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_STATS:
        {
            tVlanRemoteNpWrFsMiVlanHwResetVlanStats *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetVlanStats;
            u1RetVal =
                FsMiVlanHwResetVlanStats (pInput->u4ContextId, pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortTunnelMode *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortTunnelMode;
            u1RetVal =
                FsMiVlanHwSetPortTunnelMode (pInput->u4ContextId,
                                             pInput->u4IfIndex, pInput->u4Mode);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_FILTER:
        {
            tVlanRemoteNpWrFsMiVlanHwSetTunnelFilter *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetTunnelFilter;
            u1RetVal =
                FsMiVlanHwSetTunnelFilter (pInput->u4ContextId,
                                           pInput->i4BridgeMode);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        {
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbId *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbId;
            u1RetVal =
                FsMiVlanHwFlushPortFdbId (pInput->u4ContextId, pInput->u4Port,
                                          pInput->u4Fid,
                                          pInput->i4OptimizeFlag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        {
            tVlanRemoteNpWrFsMiVlanHwFlushPortFdbList *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPortFdbList;
            u1RetVal =
                FsMiVlanHwFlushPortFdbList (pInput->u4ContextId,
                                            &(pInput->VlanFlushInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwFlushPort *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushPort;
            u1RetVal =
                FsMiVlanHwFlushPort (pInput->u4ContextId, pInput->u4IfIndex,
                                     pInput->i4OptimizeFlag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_FDB_ID:
        {
            tVlanRemoteNpWrFsMiVlanHwFlushFdbId *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwFlushFdbId;
            u1RetVal =
                FsMiVlanHwFlushFdbId (pInput->u4ContextId, pInput->u4Fid);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_SHORT_AGEOUT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetShortAgeout *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetShortAgeout;
            u1RetVal =
                FsMiVlanHwSetShortAgeout (pInput->u4ContextId, pInput->u4Port,
                                          pInput->i4AgingTime);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_SHORT_AGEOUT:
        {
            tVlanRemoteNpWrFsMiVlanHwResetShortAgeout *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwResetShortAgeout;
            u1RetVal =
                FsMiVlanHwResetShortAgeout (pInput->u4ContextId, pInput->u4Port,
                                            pInput->i4LongAgeout);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_INFO:
        {
            tVlanRemoteNpWrFsMiVlanHwGetVlanInfo *pInput = NULL;
            tHwVlanPortArray    HwEntry;
            tHwPortArray        PortArray1;
            tHwPortArray        PortArray2;
            tHwPortArray        PortArray3;
            tHwPortArray        PortArray4;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetVlanInfo;
            PortArray1.pu4PortArray =
                &(pInput->HwEntry.HwMemberPortArray.au4PortArray[0]);
            PortArray1.i4Length = (pInput->HwEntry.HwMemberPortArray.i4Length);
            HwEntry.pHwMemberPortArray = &(PortArray1);
            PortArray2.pu4PortArray =
                &(pInput->HwEntry.HwUntagPortArray.au4PortArray[0]);
            PortArray2.i4Length = (pInput->HwEntry.HwUntagPortArray.i4Length);
            HwEntry.pHwUntagPortArray = &(PortArray2);
            PortArray3.pu4PortArray =
                &(pInput->HwEntry.HwFwdAllPortArray.au4PortArray[0]);
            PortArray3.i4Length = (pInput->HwEntry.HwFwdAllPortArray.i4Length);
            HwEntry.pHwFwdAllPortArray = &(PortArray3);
            PortArray4.pu4PortArray =
                &(pInput->HwEntry.HwFwdUnregPortArray.au4PortArray[0]);
            PortArray4.i4Length =
                (pInput->HwEntry.HwFwdUnregPortArray.i4Length);
            HwEntry.pHwFwdUnregPortArray = &(PortArray4);
            u1RetVal =
                FsMiVlanHwGetVlanInfo (pInput->u4ContextId, pInput->VlanId,
                                       &(HwEntry));
            pInput->HwEntry.HwMemberPortArray.i4Length =
                HwEntry.pHwMemberPortArray->i4Length;
            pInput->HwEntry.HwUntagPortArray.i4Length =
                HwEntry.pHwUntagPortArray->i4Length;
            pInput->HwEntry.HwFwdAllPortArray.i4Length =
                HwEntry.pHwFwdAllPortArray->i4Length;
            pInput->HwEntry.HwFwdUnregPortArray.i4Length =
                HwEntry.pHwFwdUnregPortArray->i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_MCAST_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwGetMcastEntry *pInput = NULL;
            tHwPortArray        HwMcastPorts;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetMcastEntry;
            HwMcastPorts.pu4PortArray = &(pInput->HwMcastPorts.au4PortArray[0]);
            HwMcastPorts.i4Length = pInput->HwMcastPorts.i4Length;
            u1RetVal =
                FsMiVlanHwGetMcastEntry (pInput->u4ContextId, pInput->VlanId,
                                         pInput->MacAddr, &(HwMcastPorts));
            pInput->HwMcastPorts.i4Length = HwMcastPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT:
        {
            tVlanRemoteNpWrFsMiVlanHwTraffClassMapInit *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwTraffClassMapInit;
            u1RetVal =
                FsMiVlanHwTraffClassMapInit (pInput->u4ContextId,
                                             pInput->u1Priority,
                                             pInput->i4CosqValue);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_SET_AGING_TIME:
        {
            tVlanRemoteNpWrFsMiBrgSetAgingTime *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiBrgSetAgingTime;
            u1RetVal =
                FsMiBrgSetAgingTime (pInput->u4ContextId, pInput->i4AgingTime);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_HW_SET_BRG_MODE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetBrgMode *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBrgMode;
            u1RetVal =
                FsMiVlanHwSetBrgMode (pInput->u4ContextId,
                                      pInput->u4BridgeMode);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetBaseBridgeMode *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBaseBridgeMode;
            u1RetVal =
                FsMiVlanHwSetBaseBridgeMode (pInput->u4IfIndex, pInput->u4Mode);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddPortMacVlanEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortMacVlanEntry;
            u1RetVal =
                FsMiVlanHwAddPortMacVlanEntry (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->MacAddr, pInput->VlanId,
                                               pInput->bSuppressOption);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDeletePortMacVlanEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortMacVlanEntry;
            u1RetVal =
                FsMiVlanHwDeletePortMacVlanEntry (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->MacAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddPortSubnetVlanEntry;
            u1RetVal =
                FsMiVlanHwAddPortSubnetVlanEntry (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->SubnetAddr,
                                                  pInput->VlanId,
                                                  pInput->bARPOption);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDeletePortSubnetVlanEntry;
            u1RetVal =
                FsMiVlanHwDeletePortSubnetVlanEntry (pInput->u4ContextId,
                                                     pInput->u4IfIndex,
                                                     pInput->SubnetAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            u1RetVal =
                FsMiVlanHwUpdatePortSubnetVlanEntry (pInput->u4ContextId,
                                                     pInput->u4IfIndex,
                                                     pInput->u4SubnetAddr,
                                                     pInput->u4SubnetMask,
                                                     pInput->VlanId,
                                                     pInput->bARPOption,
                                                     pInput->u1Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_NP_HW_RUN_MAC_AGEING:
        {
            tVlanRemoteNpWrFsMiVlanNpHwRunMacAgeing *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanNpHwRunMacAgeing;
            u1RetVal = FsMiVlanNpHwRunMacAgeing (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_LIMIT:
        {
            tVlanRemoteNpWrFsMiVlanHwMacLearningLimit *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningLimit;
            u1RetVal =
                FsMiVlanHwMacLearningLimit (pInput->u4ContextId, pInput->VlanId,
                                            pInput->u2FdbId,
                                            pInput->u4MacLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            tVlanRemoteNpWrFsMiVlanHwSwitchMacLearningLimit *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSwitchMacLearningLimit;
            u1RetVal =
                FsMiVlanHwSwitchMacLearningLimit (pInput->u4ContextId,
                                                  pInput->u4MacLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwMacLearningStatus *pInput = NULL;
            tHwPortArray        HwEgressPorts;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwMacLearningStatus;
            HwEgressPorts.pu4PortArray =
                &(pInput->HwEgressPorts.au4PortArray[0]);
            HwEgressPorts.i4Length = pInput->HwEgressPorts.i4Length;
            u1RetVal =
                FsMiVlanHwMacLearningStatus (pInput->u4ContextId,
                                             pInput->VlanId, pInput->u2FdbId,
                                             &(HwEgressPorts),
                                             pInput->u1Status);
            pInput->HwEgressPorts.i4Length = HwEgressPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus *pInput = NULL;
            tHwPortArray        HwPortList;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus;
            HwPortList.pu4PortArray = &(pInput->HwPortList.au4PortArray[0]);
            HwPortList.i4Length = pInput->HwPortList.i4Length;
            u1RetVal =
                FsMiVlanHwSetFidPortLearningStatus (pInput->u4ContextId,
                                                    pInput->u4Fid,
                                                    HwPortList,
                                                    pInput->u4Port,
                                                    pInput->u1Action);
            pInput->HwPortList.i4Length = HwPortList.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pInput =
                NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            u1RetVal =
                FsMiVlanHwSetProtocolTunnelStatusOnPort (pInput->u4ContextId,
                                                         pInput->u4IfIndex,
                                                         pInput->ProtocolId,
                                                         pInput->
                                                         u4TunnelStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortProtectedStatus;
            u1RetVal =
                FsMiVlanHwSetPortProtectedStatus (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->i4ProtectedStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx *pInput = NULL;
            tHwPortArray        HwAllowedToGoPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntryEx;
            HwAllowedToGoPorts.pu4PortArray =
                &(pInput->HwAllowedToGoPorts.au4PortArray[0]);
            HwAllowedToGoPorts.i4Length = pInput->HwAllowedToGoPorts.i4Length;
            u1RetVal =
                FsMiVlanHwAddStaticUcastEntryEx (pInput->u4ContextId,
                                                 pInput->u4Fid, pInput->MacAddr,
                                                 pInput->u4Port,
                                                 &(HwAllowedToGoPorts),
                                                 pInput->u1Status,
                                                 pInput->ConnectionId);
            pInput->HwAllowedToGoPorts.i4Length = HwAllowedToGoPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX:
        {
            tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntryEx *pInput = NULL;
            tHwPortArray        AllowedToGoPorts;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetStaticUcastEntryEx;
            AllowedToGoPorts.pu4PortArray =
                &(pInput->AllowedToGoPorts.au4PortArray[0]);
            AllowedToGoPorts.i4Length = pInput->AllowedToGoPorts.i4Length;
            u1RetVal =
                FsMiVlanHwGetStaticUcastEntryEx (pInput->u4ContextId,
                                                 pInput->u4Fid, pInput->MacAddr,
                                                 pInput->u4Port,
                                                 &(AllowedToGoPorts),
                                                 &(pInput->u1Status),
                                                 pInput->ConnectionId);
            pInput->AllowedToGoPorts.i4Length = AllowedToGoPorts.i4Length;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VLAN_HW_FORWARD_PKT_ON_PORTS:
        {
            tVlanRemoteNpWrFsVlanHwForwardPktOnPorts *pInput = NULL;
            tHwVlanFwdInfo      VlanFwdInfo;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsVlanHwForwardPktOnPorts;
            MEMCPY (&(VlanFwdInfo.VlanTag), &(pInput->VlanFwdInfo.VlanTag),
                    sizeof (tVlanTag));
            VlanFwdInfo.TagPorts.pu4PortArray =
                &(pInput->VlanFwdInfo.TagPorts.au4PortArray[0]),
                VlanFwdInfo.TagPorts.i4Length =
                pInput->VlanFwdInfo.TagPorts.i4Length;
            VlanFwdInfo.UnTagPorts.pu4PortArray =
                &(pInput->VlanFwdInfo.UnTagPorts.au4PortArray[0]);
            VlanFwdInfo.UnTagPorts.i4Length =
                pInput->VlanFwdInfo.UnTagPorts.i4Length;
            VlanFwdInfo.u4ContextId = pInput->VlanFwdInfo.u4ContextId;
            u1RetVal =
                FsVlanHwForwardPktOnPorts (&(pInput->u1Packet),
                                           pInput->u2PacketLen, &(VlanFwdInfo));
            MEMCPY (&(pInput->VlanFwdInfo.VlanTag), &(VlanFwdInfo.VlanTag),
                    sizeof (tVlanTag));
            pInput->VlanFwdInfo.TagPorts.i4Length =
                VlanFwdInfo.TagPorts.i4Length;
            pInput->VlanFwdInfo.UnTagPorts.i4Length =
                VlanFwdInfo.UnTagPorts.i4Length;
            pInput->VlanFwdInfo.u4ContextId = VlanFwdInfo.u4ContextId;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningStatus;
            u1RetVal =
                FsMiVlanHwPortMacLearningStatus (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_INDEX:
        {
            tVlanRemoteNpWrFsMiVlanHwSetMcastIndex *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetMcastIndex;
            u1RetVal = FsMiVlanHwSetMcastIndex (pInput->HwMcastIndexInfo,
                                                &(pInput->u4McastIndex));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortIngressEtherType;
            u1RetVal =
                FsMiVlanHwSetPortIngressEtherType (pInput->u4ContextId,
                                                   pInput->u4IfIndex,
                                                   pInput->u2EtherType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEgressEtherType;
            u1RetVal =
                FsMiVlanHwSetPortEgressEtherType (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->u2EtherType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROPERTY:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortProperty *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortProperty;
            u1RetVal =
                FsMiVlanHwSetPortProperty (pInput->u4ContextId,
                                           pInput->u4IfIndex,
                                           pInput->VlanPortProperty);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetProviderBridgePortType;
            u1RetVal =
                FsMiVlanHwSetProviderBridgePortType (pInput->u4ContextId,
                                                     pInput->u4IfIndex,
                                                     pInput->u4PortType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pInput =
                NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanTranslationStatus;
            u1RetVal =
                FsMiVlanHwSetPortSVlanTranslationStatus (pInput->u4ContextId,
                                                         pInput->u4IfIndex,
                                                         pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddSVlanTranslationEntry;
            u1RetVal =
                FsMiVlanHwAddSVlanTranslationEntry (pInput->u4ContextId,
                                                    pInput->u4IfIndex,
                                                    pInput->u2LocalSVlan,
                                                    pInput->u2RelaySVlan);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelSVlanTranslationEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelSVlanTranslationEntry;
            u1RetVal =
                FsMiVlanHwDelSVlanTranslationEntry (pInput->u4ContextId,
                                                    pInput->u4IfIndex,
                                                    pInput->u2LocalSVlan,
                                                    pInput->u2RelaySVlan);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            u1RetVal =
                FsMiVlanHwSetPortEtherTypeSwapStatus (pInput->u4ContextId,
                                                      pInput->u4IfIndex,
                                                      pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwAddEtherTypeSwapEntry;
            u1RetVal =
                FsMiVlanHwAddEtherTypeSwapEntry (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->u2LocalEtherType,
                                                 pInput->u2RelayEtherType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwDelEtherTypeSwapEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelEtherTypeSwapEntry;
            u1RetVal =
                FsMiVlanHwDelEtherTypeSwapEntry (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->u2LocalEtherType,
                                                 pInput->u2RelayEtherType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwAddSVlanMap *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwAddSVlanMap;
            u1RetVal =
                FsMiVlanHwAddSVlanMap (pInput->u4ContextId,
                                       pInput->VlanSVlanMap);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DELETE_S_VLAN_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwDeleteSVlanMap *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwDeleteSVlanMap;
            u1RetVal =
                FsMiVlanHwDeleteSVlanMap (pInput->u4ContextId,
                                          pInput->VlanSVlanMap);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortSVlanClassifyMethod;
            u1RetVal =
                FsMiVlanHwSetPortSVlanClassifyMethod (pInput->u4ContextId,
                                                      pInput->u4IfIndex,
                                                      pInput->u1TableType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwPortMacLearningLimit;
            u1RetVal =
                FsMiVlanHwPortMacLearningLimit (pInput->u4ContextId,
                                                pInput->u4IfIndex,
                                                pInput->u4MacLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwMulticastMacTableLimit;
            u1RetVal =
                FsMiVlanHwMulticastMacTableLimit (pInput->u4ContextId,
                                                  pInput->u4MacLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortCustomerVlan;
            u1RetVal =
                FsMiVlanHwSetPortCustomerVlan (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->CVlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN:
        {
            tVlanRemoteNpWrFsMiVlanHwResetPortCustomerVlan *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwResetPortCustomerVlan;
            u1RetVal =
                FsMiVlanHwResetPortCustomerVlan (pInput->u4ContextId,
                                                 pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwCreateProviderEdgePort;
            u1RetVal =
                FsMiVlanHwCreateProviderEdgePort (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->SVlanId,
                                                  pInput->PepConfig);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT:
        {
            tVlanRemoteNpWrFsMiVlanHwDelProviderEdgePort *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwDelProviderEdgePort;
            u1RetVal =
                FsMiVlanHwDelProviderEdgePort (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->SVlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_PVID:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPepPvid *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepPvid;
            u1RetVal =
                FsMiVlanHwSetPepPvid (pInput->u4ContextId, pInput->u4IfIndex,
                                      pInput->SVlanId, pInput->Pvid);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPepAccFrameType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepAccFrameType;
            u1RetVal =
                FsMiVlanHwSetPepAccFrameType (pInput->u4ContextId,
                                              pInput->u4IfIndex,
                                              pInput->SVlanId,
                                              pInput->u1AccepFrameType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPepDefUserPriority *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPepDefUserPriority;
            u1RetVal =
                FsMiVlanHwSetPepDefUserPriority (pInput->u4ContextId,
                                                 pInput->u4IfIndex,
                                                 pInput->SVlanId,
                                                 pInput->i4DefUsrPri);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ING_FILTERING:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPepIngFiltering *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPepIngFiltering;
            u1RetVal =
                FsMiVlanHwSetPepIngFiltering (pInput->u4ContextId,
                                              pInput->u4IfIndex,
                                              pInput->SVlanId,
                                              pInput->u1IngFilterEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP:
        {
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagPep *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagPep;
            u1RetVal =
                FsMiVlanHwSetCvidUntagPep (pInput->u4ContextId,
                                           pInput->VlanSVlanMap);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP:
        {
            tVlanRemoteNpWrFsMiVlanHwSetCvidUntagCep *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCvidUntagCep;
            u1RetVal =
                FsMiVlanHwSetCvidUntagCep (pInput->u4ContextId,
                                           pInput->VlanSVlanMap);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpEncodTbl;
            u1RetVal =
                FsMiVlanHwSetPcpEncodTbl (pInput->u4ContextId,
                                          pInput->u4IfIndex,
                                          pInput->NpPbVlanPcpInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_DECOD_TBL:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPcpDecodTbl;
            u1RetVal =
                FsMiVlanHwSetPcpDecodTbl (pInput->u4ContextId,
                                          pInput->u4IfIndex,
                                          pInput->NpPbVlanPcpInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_USE_DEI:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortUseDei *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetPortUseDei;
            u1RetVal =
                FsMiVlanHwSetPortUseDei (pInput->u4ContextId, pInput->u4IfIndex,
                                         pInput->u1UseDei);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortReqDropEncoding;
            u1RetVal =
                FsMiVlanHwSetPortReqDropEncoding (pInput->u4ContextId,
                                                  pInput->u4IfIndex,
                                                  pInput->u1ReqDrpEncoding);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetPortPcpSelection;
            u1RetVal =
                FsMiVlanHwSetPortPcpSelection (pInput->u4ContextId,
                                               pInput->u4IfIndex,
                                               pInput->u2PcpSelection);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetServicePriRegenEntry;
            u1RetVal =
                FsMiVlanHwSetServicePriRegenEntry (pInput->u4ContextId,
                                                   pInput->u4IfIndex,
                                                   pInput->SVlanId,
                                                   pInput->i4RecvPriority,
                                                   pInput->i4RegenPriority);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS:
        {
            tVlanRemoteNpWrFsMiVlanHwSetTunnelMacAddress *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwSetTunnelMacAddress;
            u1RetVal =
                FsMiVlanHwSetTunnelMacAddress (pInput->u4ContextId,
                                               pInput->MacAddr,
                                               pInput->u2Protocol);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP:
        {
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanMap *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetNextSVlanMap;
            u1RetVal =
                FsMiVlanHwGetNextSVlanMap (pInput->u4ContextId,
                                           pInput->VlanSVlanMap,
                                           &(pInput->RetVlanSVlanMap));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanRemoteNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pInput =
                NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanHwGetNextSVlanTranslationEntry;
            u1RetVal =
                FsMiVlanHwGetNextSVlanTranslationEntry (pInput->u4ContextId,
                                                        pInput->u4IfIndex,
                                                        pInput->u2LocalSVlan,
                                                        &(pInput->
                                                          VidTransEntryInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* PB_WANTED */
#ifdef MBSM_WANTED
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
        {
            tVlanRemoteNpWrFsMiVlanMbsmSyncFDBInfo *pInput = NULL;
            tHwPortArray        HwPortArray;
            UINT4               u4Index = 0;
            UINT4               au4PortArray[1];
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanMbsmSyncFDBInfo;
            for (u4Index = 0;
                 u4Index < (UINT4) pInput->FDBInfoArray.u4FdbEntryCount;
                 u4Index++)
            {
                MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));
                MEMSET (&au4PortArray, 0, sizeof (au4PortArray));
                HwPortArray.pu4PortArray = &au4PortArray[0];
                HwPortArray.i4Length = 1;
                MEMCPY (HwPortArray.pu4PortArray,
                        &(pInput->FDBInfoArray.aFDBInfoArray[u4Index].u4Port),
                        sizeof (UINT4));
                /* MAC  Entry is programmed in Hardware as Dynamic Entry */
                /* Last Argument to the Npapi indicates it is a Dynamic Entry */
                u1RetVal =
                    FsMiVlanHwAddStaticUcastEntry (pInput->FDBInfoArray.
                                                   aFDBInfoArray[u4Index].
                                                   u4ContextId,
                                                   pInput->FDBInfoArray.
                                                   aFDBInfoArray[u4Index].
                                                   u4FdbId,
                                                   pInput->FDBInfoArray.
                                                   aFDBInfoArray[u4Index].
                                                   MacAddr, 0, &HwPortArray,
                                                   VLAN_DELETE_ON_TIMEOUT);
                if (u1RetVal == FNP_FAILURE)
                {
                    break;
                }
#ifdef SW_LEARNING
                /* The Mac Entry is updated the vlan FDB table */
                if (VlanAddFDBInfo
                    (&(pInput->FDBInfoArray.aFDBInfoArray[u4Index])) !=
                    VLAN_SUCCESS)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }
#endif /* SW_LEARNING */
            }
            break;
        }
        case FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanRemoteNpWrFsMiVlanMbsmHwEvbConfigSChIface *pEvbInput = NULL;
            pEvbInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwEvbConfigSChIface;
            u1RetVal =
                FsMiVlanHwEvbConfigSChIface (&(pEvbInput->VlanEvbHwConfigInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanMbsmHwSetBridgePortType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->
                VlanRemoteNpFsMiVlanMbsmHwSetBridgePortType;
            u1RetVal = FsMiVlanHwSetBridgePortType (&(pInput->VlanHwPortInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* MBSM_WANTED  */

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_CVLAN_STAT:
        {
            tVlanRemoteNpWrFsMiVlanHwSetCVlanStat *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetCVlanStat;
            u1RetVal = FsMiVlanHwSetCVlanStat (pInput->VlanStat);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_GET_CVLAN_STAT:
        {
            tVlanRemoteNpWrFsMiVlanHwGetCVlanStat *pInput = NULL;
            pInput = &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwGetCVlanStat;
            u1RetVal =
                FsMiVlanHwGetCVlanStat (pInput->u2Port, pInput->u2CVlanId,
                                        pInput->u1StatsType,
                                        (pInput->pu4VlanStatsValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_VLAN_HW_CLEAR_CVLAN_STAT:
        {
            tVlanRemoteNpWrFsMiVlanHwClearCVlanStat *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwClearCVlanStat;
            u1RetVal =
                FsMiVlanHwClearCVlanStat (pInput->u2Port, pInput->u2CVlanId);

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif
        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwEvbConfigSChIface;
            u1RetVal =
                FsMiVlanHwEvbConfigSChIface (&(pInput->VlanEvbHwConfigInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanRemoteNpWrFsMiVlanHwSetBridgePortType *pInput = NULL;
            pInput =
                &pVlanRemoteNpModInfo->VlanRemoteNpFsMiVlanHwSetBridgePortType;
            u1RetVal = FsMiVlanHwSetBridgePortType (&(pInput->VlanHwPortInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __VLANUTIL_C__ */
