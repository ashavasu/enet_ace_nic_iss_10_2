
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnputil.c,v 1.7 2015/07/18 06:36:48 siva Exp $
 *
 * Description:This file contains the single NP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __ECFM_NPUTIL_C__
#define __ECFM_NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEcfmConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEcfmConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmRemoteNpModInfo *pEcfmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pEcfmNpModInfo = &(pFsHwNp->EcfmNpModInfo);
    pEcfmRemoteNpModInfo = &(pRemoteHwNp->EcfmRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tEcfmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdLbrCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmClearRcvdLbrCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdLbrCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdLbrCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdTstCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmClearRcvdTstCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdTstCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdTstCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_GET_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdLbrCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmGetRcvdLbrCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdLbrCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdLbrCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pu4LbrIn != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4LbrIn), (pLocalArgs->pu4LbrIn),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_GET_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdTstCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmGetRcvdTstCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdTstCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdTstCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pu4TstIn != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4TstIn), (pLocalArgs->pu4TstIn),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_HW_CALL_NP_API:
        {
            tEcfmNpWrFsMiEcfmHwCallNpApi *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwCallNpApi *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwCallNpApi;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwCallNpApi;
            pRemoteArgs->u1Type = pLocalArgs->u1Type;

            MEMCPY (&(pRemoteArgs->EcfmHwInfo.EcfmHwMaParams),
                    &(pLocalArgs->pEcfmHwInfo->EcfmHwMaParams),
                    sizeof (tEcfmHwMaParams));
            MEMCPY (&(pRemoteArgs->EcfmHwInfo.EcfmHwMepParams),
                    &(pLocalArgs->pEcfmHwInfo->EcfmHwMepParams),
                    sizeof (tEcfmHwMepParams));
            MEMCPY (&(pRemoteArgs->EcfmHwInfo.EcfmHwRMepParams),
                    &(pLocalArgs->pEcfmHwInfo->EcfmHwRMepParams),
                    sizeof (tEcfmHwRMepParams));
            switch (pLocalArgs->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                case ECFM_NP_STOP_CCM_TX:
                {
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        PortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.PortList.au4PortArray[0]),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.PortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.PortList.i4Length));
                    }
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.
                        i4Length =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        PortList.i4Length;
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.UntagPortList.
                                 au4PortArray[0]),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.UntagPortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.UntagPortList.i4Length));
                    }
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u4ContextId =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u4ContextId;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u4IfIndex;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2TxFilterId =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2TxFilterId;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2PduLength =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2PduLength;
                    if ((pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                         pu1Pdu != NULL)
                        && (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                            u2PduLength < ECFM_MAX_PDU_SIZE))
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.u1Pdu[0]),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.pu1Pdu),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.u2PduLength));
                    }
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        PortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcRxParams.PortList.au4PortArray[0]),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.PortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.PortList.i4Length));
                    }
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.
                        i4Length =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        PortList.i4Length;
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcRxParams.UntagPortList.
                                 au4PortArray[0]),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.UntagPortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.UntagPortList.i4Length));
                    }
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length;
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        pEcfmCcRxInfo != NULL)
                    {
                        MEMCPY (&
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcRxParams.EcfmCcRxInfo),
                                (pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.pEcfmCcRxInfo),
                                sizeof (tEcfmCcOffRxInfo));
                    }
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u4ContextId =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u4ContextId;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u4IfIndex;
                    pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u2RxFilterId =
                        pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u2RxFilterId;
                    break;
                }
            }

            pRemoteArgs->EcfmHwInfo.u1EcfmOffStatus =
                pLocalArgs->pEcfmHwInfo->u1EcfmOffStatus;

            break;
        }
        case FS_MI_ECFM_HW_DE_INIT:
        {
            tEcfmNpWrFsMiEcfmHwDeInit *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwDeInit;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwDeInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_GET_CAPABILITY:
        {
            tEcfmNpWrFsMiEcfmHwGetCapability *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCapability *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCapability;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCapability;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu4HwCapability != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4HwCapability),
                        (pLocalArgs->pu4HwCapability), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmRxStatistics *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmRxStatistics;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmRxStatistics;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2RxFilterId = pLocalArgs->u2RxFilterId;
            if (pLocalArgs->pEcfmCcOffMepRxStats != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmCcOffMepRxStats),
                        (pLocalArgs->pEcfmCcOffMepRxStats),
                        sizeof (tEcfmCcOffMepRxStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmTxStatistics *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmTxStatistics;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmTxStatistics;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2TxFilterId = pLocalArgs->u2TxFilterId;
            if (pLocalArgs->pEcfmCcOffMepTxStats != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmCcOffMepTxStats),
                        (pLocalArgs->pEcfmCcOffMepTxStats),
                        sizeof (tEcfmCcOffMepTxStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_PORT_CC_STATS:
        {
            tEcfmNpWrFsMiEcfmHwGetPortCcStats *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetPortCcStats *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetPortCcStats;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetPortCcStats;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pEcfmCcOffPortStats != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmCcOffPortStats),
                        (pLocalArgs->pEcfmCcOffPortStats),
                        sizeof (tEcfmCcOffPortStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE:
        {
            tEcfmNpWrFsMiEcfmHwHandleIntQFailure *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwHandleIntQFailure *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwHandleIntQFailure;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwHandleIntQFailure;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pEcfmCcOffRxHandleInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmCcOffRxHandleInfo),
                        (pLocalArgs->pEcfmCcOffRxHandleInfo),
                        sizeof (tEcfmCcOffRxHandleInfo));
            }
            if (pLocalArgs->pu2RxHandle != NULL)
            {
                MEMCPY (&(pRemoteArgs->u2RxHandle), (pLocalArgs->pu2RxHandle),
                        sizeof (UINT2));
            }
            if (pLocalArgs->pb1More != NULL)
            {
                MEMCPY (&(pRemoteArgs->b1More), (pLocalArgs->pb1More),
                        sizeof (BOOL1));
            }
            break;
        }
        case FS_MI_ECFM_HW_INIT:
        {
            tEcfmNpWrFsMiEcfmHwInit *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwInit;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_REGISTER:
        {
            tEcfmNpWrFsMiEcfmHwRegister *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwRegister *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwRegister;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwRegister;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE:
        {
            tEcfmNpWrFsMiEcfmHwSetVlanEtherType *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwSetVlanEtherType *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwSetVlanEtherType;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwSetVlanEtherType;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2EtherTypeValue = pLocalArgs->u2EtherTypeValue;
            pRemoteArgs->u1EtherType = pLocalArgs->u1EtherType;
            break;
        }
        case FS_MI_ECFM_START_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartLbmTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartLbmTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLbmTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLbmTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pEcfmConfigLbmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmConfigLbmInfo),
                        (pLocalArgs->pEcfmConfigLbmInfo),
                        sizeof (tEcfmConfigLbmInfo));
            }
            break;
        }
        case FS_MI_ECFM_START_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartTstTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartTstTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartTstTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartTstTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pEcfmConfigTstInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmConfigTstInfo),
                        (pLocalArgs->pEcfmConfigTstInfo),
                        sizeof (tEcfmConfigTstInfo));
            }
            break;
        }
        case FS_MI_ECFM_STOP_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopLbmTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopLbmTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLbmTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLbmTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_STOP_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopTstTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopTstTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopTstTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopTstTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->EcfmMepInfoParams),
                        (pLocalArgs->pEcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_TRANSMIT1_DM:
        {
            tEcfmNpWrFsMiEcfmTransmit1Dm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmit1Dm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmit1Dm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmit1Dm;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmPdu != NULL)
                && (pLocalArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY (&(pRemoteArgs->u1DmPdu[0]), (pLocalArgs->pu1DmPdu),
                        (pLocalArgs->u2PduLength));
            }
            pRemoteArgs->u2PduLength = pLocalArgs->u2PduLength;
            MEMCPY (&(pRemoteArgs->VlanTag), &(pLocalArgs->VlanTag),
                    sizeof (tVlanTag));
            pRemoteArgs->u1Direction = pLocalArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMM:
        {
            tEcfmNpWrFsMiEcfmTransmitDmm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitDmm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmm;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmmPdu != NULL)
                && (pLocalArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY (&(pRemoteArgs->u1DmmPdu[0]), (pLocalArgs->pu1DmmPdu),
                        (pLocalArgs->u2PduLength));
            }
            pRemoteArgs->u2PduLength = pLocalArgs->u2PduLength;
            MEMCPY (&(pRemoteArgs->VlanTag), &(pLocalArgs->VlanTag),
                    sizeof (tVlanTag));
            pRemoteArgs->u1Direction = pLocalArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMR:
        {
            tEcfmNpWrFsMiEcfmTransmitDmr *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitDmr *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmr;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmr;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmrPdu != NULL)
                && (pLocalArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY (&(pRemoteArgs->u1DmrPdu[0]), (pLocalArgs->pu1DmrPdu),
                        (pLocalArgs->u2PduLength));
            }
            pRemoteArgs->u2PduLength = pLocalArgs->u2PduLength;
            MEMCPY (&(pRemoteArgs->VlanTag), &(pLocalArgs->VlanTag),
                    sizeof (tVlanTag));
            pRemoteArgs->u1Direction = pLocalArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMM:
        {
            tEcfmNpWrFsMiEcfmTransmitLmm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitLmm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmm;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if ((pLocalArgs->pu1LmmPdu != NULL)
                && (pLocalArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY (&(pRemoteArgs->u1LmmPdu[0]), (pLocalArgs->pu1LmmPdu),
                        (pLocalArgs->u2PduLength));
            }
            pRemoteArgs->u2PduLength = pLocalArgs->u2PduLength;
            MEMCPY (&(pRemoteArgs->VlanTag), &(pLocalArgs->VlanTag),
                    sizeof (tVlanTagInfo));
            pRemoteArgs->u1Direction = pLocalArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMR:
        {
            tEcfmNpWrFsMiEcfmTransmitLmr *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitLmr *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmr;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmr;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if ((pLocalArgs->pu1LmrPdu != NULL)
                && (pLocalArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY (&(pRemoteArgs->u1LmrPdu[0]), (pLocalArgs->pu1LmrPdu),
                        (pLocalArgs->u2PduLength));
            }
            pRemoteArgs->u2PduLength = pLocalArgs->u2PduLength;
            if (pLocalArgs->pVlanTag != NULL)
            {
                MEMCPY (&(pRemoteArgs->VlanTag), (pLocalArgs->pVlanTag),
                        sizeof (tVlanTagInfo));
            }
            pRemoteArgs->u1Direction = pLocalArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_START_LM:
        {
            tEcfmNpWrFsMiEcfmStartLm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartLm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLm;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLm;
            if (pLocalArgs->pHwLmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwLmInfo), (pLocalArgs->pHwLmInfo),
                        sizeof (tEcfmHwLmParams));
            }
            break;
        }
        case FS_MI_ECFM_STOP_LM:
        {
            tEcfmNpWrFsMiEcfmStopLm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopLm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLm;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLm;
            if (pLocalArgs->pHwLmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwLmInfo), (pLocalArgs->pHwLmInfo),
                        sizeof (tEcfmHwLmParams));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ECFM_MBSM_HW_CALL_NP_API:
        case FS_MI_ECFM_MBSM_NP_INIT_HW:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEcfmConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEcfmConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
    tEcfmRemoteNpModInfo *pEcfmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pEcfmNpModInfo = &(pFsHwNp->EcfmNpModInfo);
    pEcfmRemoteNpModInfo = &(pRemoteHwNp->EcfmRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdLbrCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmClearRcvdLbrCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdLbrCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdLbrCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdTstCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmClearRcvdTstCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdTstCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdTstCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_GET_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdLbrCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmGetRcvdLbrCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdLbrCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdLbrCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pu4LbrIn != NULL)
            {
                MEMCPY ((pLocalArgs->pu4LbrIn), &(pRemoteArgs->u4LbrIn),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_GET_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdTstCounter *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmGetRcvdTstCounter *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdTstCounter;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdTstCounter;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pu4TstIn != NULL)
            {
                MEMCPY ((pLocalArgs->pu4TstIn), &(pRemoteArgs->u4TstIn),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_HW_CALL_NP_API:
        {
            tEcfmNpWrFsMiEcfmHwCallNpApi *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwCallNpApi *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwCallNpApi;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwCallNpApi;
            pLocalArgs->u1Type = pRemoteArgs->u1Type;
            MEMCPY (&(pLocalArgs->pEcfmHwInfo->EcfmHwMaParams),
                    &(pRemoteArgs->EcfmHwInfo.EcfmHwMaParams),
                    sizeof (tEcfmHwMaParams));
            MEMCPY (&(pLocalArgs->pEcfmHwInfo->EcfmHwMepParams),
                    &(pRemoteArgs->EcfmHwInfo.EcfmHwMepParams),
                    sizeof (tEcfmHwMepParams));
            MEMCPY (&(pLocalArgs->pEcfmHwInfo->EcfmHwRMepParams),
                    &(pRemoteArgs->EcfmHwInfo.EcfmHwRMepParams),
                    sizeof (tEcfmHwRMepParams));

            switch (pRemoteArgs->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                case ECFM_NP_STOP_CCM_TX:
                {
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        PortList.pu4PortArray != NULL)
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.PortList.pu4PortArray),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcTxParams.PortList.au4PortArray[0]),
                                sizeof (UINT4) *
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.PortList.i4Length));
                    }

                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.
                        i4Length =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        PortList.i4Length;

                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.UntagPortList.pu4PortArray),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcTxParams.UntagPortList.
                                  au4PortArray[0]),
                                sizeof (UINT4) *
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.UntagPortList.i4Length));
                    }
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u4ContextId =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u4ContextId;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u4IfIndex =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u4IfIndex;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2TxFilterId =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2TxFilterId;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2PduLength =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2PduLength;
                    if ((pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                         pu1Pdu != NULL)
                        && (pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                            u2PduLength < ECFM_MAX_PDU_SIZE))
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcTxParams.pu1Pdu),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcTxParams.u1Pdu[0]),
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcTxParams.u2PduLength));
                    }
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        PortList.pu4PortArray != NULL)
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.PortList.pu4PortArray),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcRxParams.PortList.au4PortArray[0]),
                                sizeof (UINT4) *
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcRxParams.PortList.i4Length));
                    }
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.
                        i4Length =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        PortList.i4Length;
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.UntagPortList.pu4PortArray),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcRxParams.UntagPortList.
                                  au4PortArray[0]),
                                sizeof (UINT4) *
                                (pRemoteArgs->EcfmHwInfo.unParam.
                                 EcfmHwCcRxParams.UntagPortList.i4Length));
                    }
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length;
                    if (pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        pEcfmCcRxInfo != NULL)
                    {
                        MEMCPY ((pLocalArgs->pEcfmHwInfo->unParam.
                                 EcfmHwCcRxParams.pEcfmCcRxInfo),
                                &(pRemoteArgs->EcfmHwInfo.unParam.
                                  EcfmHwCcRxParams.EcfmCcRxInfo),
                                sizeof (tEcfmCcOffRxInfo));
                    }
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u4ContextId =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u4ContextId;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u4IfIndex =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u4IfIndex;
                    pLocalArgs->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u2RxFilterId =
                        pRemoteArgs->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u2RxFilterId;
                    break;
                }
            }

            pLocalArgs->pEcfmHwInfo->u1EcfmOffStatus =
                pRemoteArgs->EcfmHwInfo.u1EcfmOffStatus;

            break;
        }
        case FS_MI_ECFM_HW_DE_INIT:
        {
            tEcfmNpWrFsMiEcfmHwDeInit *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwDeInit;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwDeInit;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_GET_CAPABILITY:
        {
            tEcfmNpWrFsMiEcfmHwGetCapability *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCapability *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCapability;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCapability;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            if (pLocalArgs->pu4HwCapability != NULL)
            {
                MEMCPY ((pLocalArgs->pu4HwCapability),
                        &(pRemoteArgs->u4HwCapability), sizeof (UINT4));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmRxStatistics *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmRxStatistics;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmRxStatistics;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u2RxFilterId = pRemoteArgs->u2RxFilterId;
            if (pLocalArgs->pEcfmCcOffMepRxStats != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmCcOffMepRxStats),
                        &(pRemoteArgs->EcfmCcOffMepRxStats),
                        sizeof (tEcfmCcOffMepRxStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS:
        {
            tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmTxStatistics *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetCcmTxStatistics;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmTxStatistics;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u2TxFilterId = pRemoteArgs->u2TxFilterId;
            if (pLocalArgs->pEcfmCcOffMepTxStats != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmCcOffMepTxStats),
                        &(pRemoteArgs->EcfmCcOffMepTxStats),
                        sizeof (tEcfmCcOffMepTxStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_GET_PORT_CC_STATS:
        {
            tEcfmNpWrFsMiEcfmHwGetPortCcStats *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwGetPortCcStats *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwGetPortCcStats;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetPortCcStats;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pEcfmCcOffPortStats != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmCcOffPortStats),
                        &(pRemoteArgs->EcfmCcOffPortStats),
                        sizeof (tEcfmCcOffPortStats));
            }
            break;
        }
        case FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE:
        {
            tEcfmNpWrFsMiEcfmHwHandleIntQFailure *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwHandleIntQFailure *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwHandleIntQFailure;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwHandleIntQFailure;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            if (pLocalArgs->pEcfmCcOffRxHandleInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmCcOffRxHandleInfo),
                        &(pRemoteArgs->EcfmCcOffRxHandleInfo),
                        sizeof (tEcfmCcOffRxHandleInfo));
            }
            if (pLocalArgs->pu2RxHandle != NULL)
            {
                MEMCPY ((pLocalArgs->pu2RxHandle), &(pRemoteArgs->u2RxHandle),
                        sizeof (UINT2));
            }
            if (pLocalArgs->pb1More != NULL)
            {
                MEMCPY ((pLocalArgs->pb1More), &(pRemoteArgs->b1More),
                        sizeof (BOOL1));
            }
            break;
        }
        case FS_MI_ECFM_HW_INIT:
        {
            tEcfmNpWrFsMiEcfmHwInit *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwInit;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwInit;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_REGISTER:
        {
            tEcfmNpWrFsMiEcfmHwRegister *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwRegister *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwRegister;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwRegister;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
        case FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE:
        {
            tEcfmNpWrFsMiEcfmHwSetVlanEtherType *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwSetVlanEtherType *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwSetVlanEtherType;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwSetVlanEtherType;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2EtherTypeValue = pRemoteArgs->u2EtherTypeValue;
            pLocalArgs->u1EtherType = pRemoteArgs->u1EtherType;
            break;
        }
        case FS_MI_ECFM_START_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartLbmTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartLbmTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLbmTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLbmTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pEcfmConfigLbmInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmConfigLbmInfo),
                        &(pRemoteArgs->EcfmConfigLbmInfo),
                        sizeof (tEcfmConfigLbmInfo));
            }
            break;
        }
        case FS_MI_ECFM_START_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartTstTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartTstTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartTstTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartTstTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            if (pLocalArgs->pEcfmConfigTstInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmConfigTstInfo),
                        &(pRemoteArgs->EcfmConfigTstInfo),
                        sizeof (tEcfmConfigTstInfo));
            }
            break;
        }
        case FS_MI_ECFM_STOP_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopLbmTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopLbmTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLbmTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLbmTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_STOP_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopTstTransaction *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopTstTransaction *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopTstTransaction;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopTstTransaction;
            if (pLocalArgs->pEcfmMepInfoParams != NULL)
            {
                MEMCPY ((pLocalArgs->pEcfmMepInfoParams),
                        &(pRemoteArgs->EcfmMepInfoParams),
                        sizeof (tEcfmMepInfoParams));
            }
            break;
        }
        case FS_MI_ECFM_TRANSMIT1_DM:
        {
            tEcfmNpWrFsMiEcfmTransmit1Dm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmit1Dm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmit1Dm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmit1Dm;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmPdu != NULL)
                && (pRemoteArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY ((pLocalArgs->pu1DmPdu), &(pRemoteArgs->u1DmPdu[0]),
                        (pRemoteArgs->u2PduLength));
            }
            pLocalArgs->u2PduLength = pRemoteArgs->u2PduLength;
            MEMCPY (&(pLocalArgs->VlanTag), &(pRemoteArgs->VlanTag),
                    sizeof (tVlanTag));
            pLocalArgs->u1Direction = pRemoteArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMM:
        {
            tEcfmNpWrFsMiEcfmTransmitDmm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitDmm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmm;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmmPdu != NULL)
                && (pRemoteArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY ((pLocalArgs->pu1DmmPdu), &(pRemoteArgs->u1DmmPdu[0]),
                        (pRemoteArgs->u2PduLength));
            }
            pLocalArgs->u2PduLength = pRemoteArgs->u2PduLength;
            MEMCPY (&(pLocalArgs->VlanTag), &(pRemoteArgs->VlanTag),
                    sizeof (tVlanTag));
            pLocalArgs->u1Direction = pRemoteArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMR:
        {
            tEcfmNpWrFsMiEcfmTransmitDmr *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitDmr *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitDmr;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmr;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if ((pLocalArgs->pu1DmrPdu != NULL)
                && (pRemoteArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY ((pLocalArgs->pu1DmrPdu), &(pRemoteArgs->u1DmrPdu[0]),
                        (pRemoteArgs->u2PduLength));
            }
            pLocalArgs->u2PduLength = pRemoteArgs->u2PduLength;
            MEMCPY (&(pLocalArgs->VlanTag), &(pRemoteArgs->VlanTag),
                    sizeof (tVlanTag));
            pLocalArgs->u1Direction = pRemoteArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMM:
        {
            tEcfmNpWrFsMiEcfmTransmitLmm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitLmm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmm;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmm;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if ((pLocalArgs->pu1LmmPdu != NULL)
                && (pRemoteArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY ((pLocalArgs->pu1LmmPdu), &(pRemoteArgs->u1LmmPdu[0]),
                        (pRemoteArgs->u2PduLength));
            }
            pLocalArgs->u2PduLength = pRemoteArgs->u2PduLength;
            MEMCPY (&(pLocalArgs->VlanTag), &(pRemoteArgs->VlanTag),
                    sizeof (tVlanTagInfo));
            pLocalArgs->u1Direction = pRemoteArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMR:
        {
            tEcfmNpWrFsMiEcfmTransmitLmr *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmTransmitLmr *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmTransmitLmr;
            pRemoteArgs =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmr;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if ((pLocalArgs->pu1LmrPdu != NULL)
                && (pRemoteArgs->u2PduLength < ECFM_MAX_PDU_SIZE))
            {
                MEMCPY ((pLocalArgs->pu1LmrPdu), &(pRemoteArgs->u1LmrPdu[0]),
                        (pRemoteArgs->u2PduLength));
            }
            pLocalArgs->u2PduLength = pRemoteArgs->u2PduLength;
            if (pLocalArgs->pVlanTag != NULL)
            {
                MEMCPY ((pLocalArgs->pVlanTag), &(pRemoteArgs->VlanTag),
                        sizeof (tVlanTagInfo));
            }
            pLocalArgs->u1Direction = pRemoteArgs->u1Direction;
            break;
        }
        case FS_MI_ECFM_START_LM:
        {
            tEcfmNpWrFsMiEcfmStartLm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStartLm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLm;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLm;
            if (pLocalArgs->pHwLmInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pHwLmInfo), &(pRemoteArgs->HwLmInfo),
                        sizeof (tEcfmHwLmParams));
            }
            break;
        }
        case FS_MI_ECFM_STOP_LM:
        {
            tEcfmNpWrFsMiEcfmStopLm *pLocalArgs = NULL;
            tEcfmRemoteNpWrFsMiEcfmStopLm *pRemoteArgs = NULL;
            pLocalArgs = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLm;
            pRemoteArgs = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLm;
            if (pLocalArgs->pHwLmInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pHwLmInfo), &(pRemoteArgs->HwLmInfo),
                        sizeof (tEcfmHwLmParams));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ECFM_MBSM_HW_CALL_NP_API:
        case FS_MI_ECFM_MBSM_NP_INIT_HW:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEcfmRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEcfmNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEcfmRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEcfmRemoteNpModInfo *pEcfmRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pEcfmRemoteNpModInfo = &(pRemoteHwNpInput->EcfmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER:
        {
            tEcfmRemoteNpWrFsMiEcfmClearRcvdLbrCounter *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdLbrCounter;
            u1RetVal =
                FsMiEcfmClearRcvdLbrCounter (&(pInput->EcfmMepInfoParams));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER:
        {
            tEcfmRemoteNpWrFsMiEcfmClearRcvdTstCounter *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmClearRcvdTstCounter;
            u1RetVal =
                FsMiEcfmClearRcvdTstCounter (&(pInput->EcfmMepInfoParams));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_GET_RCVD_LBR_COUNTER:
        {
            tEcfmRemoteNpWrFsMiEcfmGetRcvdLbrCounter *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdLbrCounter;
            u1RetVal =
                FsMiEcfmGetRcvdLbrCounter (&(pInput->EcfmMepInfoParams),
                                           &(pInput->u4LbrIn));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_GET_RCVD_TST_COUNTER:
        {
            tEcfmRemoteNpWrFsMiEcfmGetRcvdTstCounter *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmGetRcvdTstCounter;
            u1RetVal =
                FsMiEcfmGetRcvdTstCounter (&(pInput->EcfmMepInfoParams),
                                           &(pInput->u4TstIn));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_CALL_NP_API:
        {
            tEcfmRemoteNpWrFsMiEcfmHwCallNpApi *pInput = NULL;
            tEcfmHwParams       EcfmHwInfo;
            tEcfmCcOffRxInfo    EcfmCcRxInfo;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwCallNpApi;

            MEMCPY (&(EcfmHwInfo.EcfmHwMaParams),
                    &(pInput->EcfmHwInfo.EcfmHwMaParams),
                    sizeof (tEcfmHwMaParams));
            MEMCPY (&(EcfmHwInfo.EcfmHwMepParams),
                    &(pInput->EcfmHwInfo.EcfmHwMepParams),
                    sizeof (tEcfmHwMepParams));
            MEMCPY (&(EcfmHwInfo.EcfmHwRMepParams),
                    &(pInput->EcfmHwInfo.EcfmHwRMepParams),
                    sizeof (tEcfmHwRMepParams));
            switch (pInput->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                case ECFM_NP_STOP_CCM_TX:
                {
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.pu4PortArray =
                        &(pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.
                          au4PortArray[0]);
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.i4Length =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.
                        i4Length;
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.
                        pu4PortArray =
                        &(pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                          UntagPortList.au4PortArray[0]);
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.i4Length =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length;
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId;
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex;
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2TxFilterId;
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.pu1Pdu =
                        &(pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u1Pdu[0]);
                    EcfmHwInfo.unParam.EcfmHwCcTxParams.u2PduLength =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u2PduLength;
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.pu4PortArray =
                        &(pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.
                          au4PortArray[0]);
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.i4Length =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.
                        i4Length;
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.
                        pu4PortArray =
                        &(pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                          UntagPortList.au4PortArray[0]);
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.i4Length =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length;

                    EcfmCcRxInfo.u4StartSeqNo =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        EcfmCcRxInfo.u4StartSeqNo;
                    EcfmCcRxInfo.u4CcmRxTimeout =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        EcfmCcRxInfo.u4CcmRxTimeout;
                    EcfmCcRxInfo.u4VlanIdIsid =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        EcfmCcRxInfo.u4VlanIdIsid;
                    EcfmCcRxInfo.u2RMepId =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        EcfmCcRxInfo.u2RMepId;
                    EcfmCcRxInfo.u1MdLevel =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        EcfmCcRxInfo.u1MdLevel;
                    MEMCPY (&(EcfmCcRxInfo.au1MAID),
                            &(pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                              EcfmCcRxInfo.au1MAID),
                            sizeof (pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                                    EcfmCcRxInfo.au1MAID));

                    EcfmHwInfo.unParam.EcfmHwCcRxParams.pEcfmCcRxInfo =
                        &(EcfmCcRxInfo);
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId;
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex;
                    EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId =
                        pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u2RxFilterId;
                    break;
                }
            }
            EcfmHwInfo.u1EcfmOffStatus = pInput->EcfmHwInfo.u1EcfmOffStatus;

            u1RetVal = FsMiEcfmHwCallNpApi (pInput->u1Type, &(EcfmHwInfo));

            switch (pInput->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                case ECFM_NP_STOP_CCM_TX:
                {
                    pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.
                        i4Length =
                        EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.i4Length;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.
                        i4Length =
                        EcfmHwInfo.unParam.EcfmHwCcTxParams.UntagPortList.
                        i4Length;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId =
                        EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId =
                        EcfmHwInfo.unParam.EcfmHwCcTxParams.u2TxFilterId;
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.
                        i4Length =
                        EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.i4Length;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.
                        i4Length =
                        EcfmHwInfo.unParam.EcfmHwCcRxParams.UntagPortList.
                        i4Length;

                    pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId =
                        EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex =
                        EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex;
                    pInput->EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId =
                        EcfmHwInfo.unParam.EcfmHwCcRxParams.u2RxFilterId;
                    break;
                }
            }
            pInput->EcfmHwInfo.u1EcfmOffStatus = EcfmHwInfo.u1EcfmOffStatus;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_DE_INIT:
        {
            tEcfmRemoteNpWrFsMiEcfmHwDeInit *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwDeInit;
            u1RetVal = FsMiEcfmHwDeInit (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_GET_CAPABILITY:
        {
            tEcfmRemoteNpWrFsMiEcfmHwGetCapability *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCapability;
            u1RetVal =
                FsMiEcfmHwGetCapability (pInput->u4ContextId,
                                         &(pInput->u4HwCapability));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS:
        {
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmRxStatistics *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmRxStatistics;
            u1RetVal =
                FsMiEcfmHwGetCcmRxStatistics (pInput->u4ContextId,
                                              pInput->u2RxFilterId,
                                              &(pInput->EcfmCcOffMepRxStats));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS:
        {
            tEcfmRemoteNpWrFsMiEcfmHwGetCcmTxStatistics *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetCcmTxStatistics;
            u1RetVal =
                FsMiEcfmHwGetCcmTxStatistics (pInput->u4ContextId,
                                              pInput->u2TxFilterId,
                                              &(pInput->EcfmCcOffMepTxStats));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_GET_PORT_CC_STATS:
        {
            tEcfmRemoteNpWrFsMiEcfmHwGetPortCcStats *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwGetPortCcStats;
            u1RetVal =
                FsMiEcfmHwGetPortCcStats (pInput->u4ContextId,
                                          pInput->u4IfIndex,
                                          &(pInput->EcfmCcOffPortStats));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE:
        {
            tEcfmRemoteNpWrFsMiEcfmHwHandleIntQFailure *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwHandleIntQFailure;
            u1RetVal =
                FsMiEcfmHwHandleIntQFailure (pInput->u4ContextId,
                                             &(pInput->EcfmCcOffRxHandleInfo),
                                             &(pInput->u2RxHandle),
                                             &(pInput->b1More));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_INIT:
        {
            tEcfmRemoteNpWrFsMiEcfmHwInit *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwInit;
            u1RetVal = FsMiEcfmHwInit (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_REGISTER:
        {
            tEcfmRemoteNpWrFsMiEcfmHwRegister *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwRegister;
            u1RetVal = FsMiEcfmHwRegister (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE:
        {
            tEcfmRemoteNpWrFsMiEcfmHwSetVlanEtherType *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwSetVlanEtherType;
            u1RetVal =
                FsMiEcfmHwSetVlanEtherType (pInput->u4ContextId,
                                            pInput->u4IfIndex,
                                            pInput->u2EtherTypeValue,
                                            pInput->u1EtherType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_START_LBM_TRANSACTION:
        {
            tEcfmRemoteNpWrFsMiEcfmStartLbmTransaction *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLbmTransaction;
            u1RetVal =
                FsMiEcfmStartLbmTransaction (&(pInput->EcfmMepInfoParams),
                                             &(pInput->EcfmConfigLbmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_START_TST_TRANSACTION:
        {
            tEcfmRemoteNpWrFsMiEcfmStartTstTransaction *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartTstTransaction;
            u1RetVal =
                FsMiEcfmStartTstTransaction (&(pInput->EcfmMepInfoParams),
                                             &(pInput->EcfmConfigTstInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_STOP_LBM_TRANSACTION:
        {
            tEcfmRemoteNpWrFsMiEcfmStopLbmTransaction *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLbmTransaction;
            u1RetVal =
                FsMiEcfmStopLbmTransaction (&(pInput->EcfmMepInfoParams));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_STOP_TST_TRANSACTION:
        {
            tEcfmRemoteNpWrFsMiEcfmStopTstTransaction *pInput = NULL;
            pInput =
                &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopTstTransaction;
            u1RetVal =
                FsMiEcfmStopTstTransaction (&(pInput->EcfmMepInfoParams));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_TRANSMIT1_DM:
        {
            tEcfmRemoteNpWrFsMiEcfmTransmit1Dm *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmit1Dm;
            u1RetVal =
                FsMiEcfmTransmit1Dm (pInput->u4ContextId, pInput->u4IfIndex,
                                     &(pInput->u1DmPdu[0]), pInput->u2PduLength,
                                     pInput->VlanTag, pInput->u1Direction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMM:
        {
            tEcfmRemoteNpWrFsMiEcfmTransmitDmm *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmm;
            u1RetVal =
                FsMiEcfmTransmitDmm (pInput->u4ContextId, pInput->u4IfIndex,
                                     &(pInput->u1DmmPdu[0]),
                                     pInput->u2PduLength, pInput->VlanTag,
                                     pInput->u1Direction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_TRANSMIT_DMR:
        {
            tEcfmRemoteNpWrFsMiEcfmTransmitDmr *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitDmr;
            u1RetVal =
                FsMiEcfmTransmitDmr (pInput->u4ContextId, pInput->u4IfIndex,
                                     &(pInput->u1DmrPdu[0]),
                                     pInput->u2PduLength, pInput->VlanTag,
                                     pInput->u1Direction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMM:
        {
            tEcfmRemoteNpWrFsMiEcfmTransmitLmm *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmm;
            u1RetVal =
                FsMiEcfmTransmitLmm (pInput->u4ContextId, pInput->u4IfIndex,
                                     &(pInput->u1LmmPdu[0]),
                                     pInput->u2PduLength, pInput->VlanTag,
                                     pInput->u1Direction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_TRANSMIT_LMR:
        {
            tEcfmRemoteNpWrFsMiEcfmTransmitLmr *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmTransmitLmr;
            u1RetVal =
                FsMiEcfmTransmitLmr (pInput->u4ContextId, pInput->u4IfIndex,
                                     &(pInput->u1LmrPdu[0]),
                                     pInput->u2PduLength, &(pInput->VlanTag),
                                     pInput->u1Direction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_START_LM:
        {
            tEcfmRemoteNpWrFsMiEcfmStartLm *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStartLm;
            u1RetVal = FsMiEcfmStartLm (&(pInput->HwLmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_ECFM_STOP_LM:
        {
            tEcfmRemoteNpWrFsMiEcfmStopLm *pInput = NULL;
            pInput = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmStopLm;
            u1RetVal = FsMiEcfmStopLm (&(pInput->HwLmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************/
/*  Function Name       : NpUtilMaskEcfmNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskEcfmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tEcfmNpModInfo     *pEcfmNpModInfo = NULL;
#ifdef MBSM_WANTED
    tEcfmRemoteNpModInfo *pEcfmRemoteNpModInfo = NULL;
#endif

    pEcfmNpModInfo = &pFsHwNp->EcfmNpModInfo;
#ifdef MBSM_WANTED
    pEcfmRemoteNpModInfo = &pRemoteHwNp->EcfmRemoteNpModInfo;
#endif

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdLbrCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdLbrCounter;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmClearRcvdTstCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmClearRcvdTstCounter;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_GET_RCVD_LBR_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdLbrCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdLbrCounter;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_GET_RCVD_TST_COUNTER:
        {
            tEcfmNpWrFsMiEcfmGetRcvdTstCounter *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmGetRcvdTstCounter;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_HW_CALL_NP_API:
        {
            tEcfmNpWrFsMiEcfmHwCallNpApi *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmHwCallNpApi;
            switch (pEntry->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                {
                    NpUtilPortCheck (pEntry->pEcfmHwInfo->unParam.
                                     EcfmHwCcTxParams.u4IfIndex, &HwPortInfo,
                                     pu1NpCallStatus);
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    NpUtilPortCheck (pEntry->pEcfmHwInfo->unParam.
                                     EcfmHwCcRxParams.u4IfIndex, &HwPortInfo,
                                     pu1NpCallStatus);
                    break;
                }
                case ECFM_NP_STOP_CCM_TX:
                case ECFM_NP_MA_CREATION:
                case ECFM_NP_MA_DELETION:
                case ECFM_NP_OFFLOAD_STATE:
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                default:
                    break;
            }

            break;
        }
        case FS_MI_ECFM_HW_DE_INIT:
        case FS_MI_ECFM_HW_INIT:
        case FS_MI_ECFM_HW_REGISTER:
        case FS_MI_ECFM_HW_GET_CAPABILITY:
        case FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS:
        case FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS:
        case FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_ECFM_HW_GET_PORT_CC_STATS:
        case FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE:
        case FS_MI_ECFM_TRANSMIT1_DM:
        case FS_MI_ECFM_TRANSMIT_DMM:
        case FS_MI_ECFM_TRANSMIT_DMR:
        case FS_MI_ECFM_TRANSMIT_LMM:
        case FS_MI_ECFM_TRANSMIT_LMR:
        {
            NpUtilPortCheck (pFsHwNp->u4IfIndex, &HwPortInfo, pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_START_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartLbmTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLbmTransaction;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_START_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStartTstTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartTstTransaction;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_STOP_LBM_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopLbmTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLbmTransaction;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_STOP_TST_TRANSACTION:
        {
            tEcfmNpWrFsMiEcfmStopTstTransaction *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopTstTransaction;
            NpUtilPortCheck (pEntry->pEcfmMepInfoParams->u4IfIndex, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_START_LM:
        {
            tEcfmNpWrFsMiEcfmStartLm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStartLm;
            NpUtilPortCheck (pEntry->pHwLmInfo->u4PortId, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
        case FS_MI_ECFM_STOP_LM:
        {
            tEcfmNpWrFsMiEcfmStopLm *pEntry = NULL;
            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmStopLm;
            NpUtilPortCheck (pEntry->pHwLmInfo->u4PortId, &HwPortInfo,
                             pu1NpCallStatus);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ECFM_MBSM_HW_CALL_NP_API:
        {
            pRemoteHwNp->u4Opcode = FS_MI_ECFM_HW_CALL_NP_API;
            tEcfmNpWrFsMiEcfmMbsmHwCallNpApi *pEntry = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwCallNpApi *pRemEntry = NULL;

            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmHwCallNpApi;
            pRemEntry = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwCallNpApi;

            pRemEntry->u1Type = pEntry->u1Type;

            MEMCPY (&(pRemEntry->EcfmHwInfo.EcfmHwMaParams),
                    &(pEntry->pEcfmHwInfo->EcfmHwMaParams),
                    sizeof (tEcfmHwMaParams));
            MEMCPY (&(pRemEntry->EcfmHwInfo.EcfmHwMepParams),
                    &(pEntry->pEcfmHwInfo->EcfmHwMepParams),
                    sizeof (tEcfmHwMepParams));
            MEMCPY (&(pRemEntry->EcfmHwInfo.EcfmHwRMepParams),
                    &(pEntry->pEcfmHwInfo->EcfmHwRMepParams),
                    sizeof (tEcfmHwRMepParams));

            switch (pEntry->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                case ECFM_NP_STOP_CCM_TX:
                {
                    if (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.
                        pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                                 PortList.au4PortArray[0]),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 PortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 PortList.i4Length));
                    }
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.PortList.
                        i4Length =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.PortList.
                        i4Length;
                    if (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                                 UntagPortList.au4PortArray[0]),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 UntagPortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 UntagPortList.i4Length));
                    }
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        UntagPortList.i4Length;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4ContextId =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u4ContextId;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.u4IfIndex =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.u4IfIndex;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                        u2TxFilterId =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2TxFilterId;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.u2PduLength =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                        u2PduLength;
                    if ((pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.pu1Pdu !=
                         NULL)
                        && (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                            u2PduLength < ECFM_MAX_PDU_SIZE))
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcTxParams.
                                 u1Pdu[0]),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 pu1Pdu),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                                 u2PduLength));
                    }
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    if (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.
                        pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                                 PortList.au4PortArray[0]),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                                 PortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                                 PortList.i4Length));
                    }
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.PortList.
                        i4Length =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.PortList.
                        i4Length;
                    if (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.pu4PortArray != NULL)
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                                 UntagPortList.au4PortArray[0]),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                                 UntagPortList.pu4PortArray),
                                sizeof (UINT4) *
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                                 UntagPortList.i4Length));
                    }
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        UntagPortList.i4Length;
                    if (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        pEcfmCcRxInfo != NULL)
                    {
                        MEMCPY (&
                                (pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                                 EcfmCcRxInfo),
                                (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                                 pEcfmCcRxInfo), sizeof (tEcfmCcOffRxInfo));
                    }
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4ContextId =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u4ContextId;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.u4IfIndex =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.u4IfIndex;
                    pRemEntry->EcfmHwInfo.unParam.EcfmHwCcRxParams.
                        u2RxFilterId =
                        pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                        u2RxFilterId;
                    break;
                }
            }
            pRemEntry->EcfmHwInfo.u1EcfmOffStatus =
                pEntry->pEcfmHwInfo->u1EcfmOffStatus;

            switch (pEntry->u1Type)
            {
                case ECFM_NP_START_CCM_TX_ON_PORTLIST:
                case ECFM_NP_START_CCM_TX:
                {
                    if ((NpUtilIsPortChannel
                         (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                          u4IfIndex) == FNP_SUCCESS)
                        ||
                        (NpUtilIsRemotePort
                         (pEntry->pEcfmHwInfo->unParam.EcfmHwCcTxParams.
                          u4IfIndex, &HwPortInfo) == FNP_SUCCESS))
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    break;
                }
                case ECFM_NP_START_CCM_RX_ON_PORTLIST:
                case ECFM_NP_START_CCM_RX:
                case ECFM_NP_STOP_CCM_RX:
                {
                    if ((NpUtilIsPortChannel
                         (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                          u4IfIndex) == FNP_SUCCESS)
                        ||
                        (NpUtilIsRemotePort
                         (pEntry->pEcfmHwInfo->unParam.EcfmHwCcRxParams.
                          u4IfIndex, &HwPortInfo) == FNP_SUCCESS))
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    break;
                }
                case ECFM_NP_STOP_CCM_TX:
                case ECFM_NP_MA_CREATION:
                case ECFM_NP_MA_DELETION:
                case ECFM_NP_OFFLOAD_STATE:
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                default:
                    break;
            }
            break;
        }
        case FS_MI_ECFM_MBSM_NP_INIT_HW:
        {
            pRemoteHwNp->u4Opcode = FS_MI_ECFM_HW_INIT;

            tEcfmNpWrFsMiEcfmMbsmNpInitHw *pEntry = NULL;
            tEcfmRemoteNpWrFsMiEcfmHwInit *pRemEntry = NULL;

            pEntry = &pEcfmNpModInfo->EcfmNpFsMiEcfmMbsmNpInitHw;
            pRemEntry = &pEcfmRemoteNpModInfo->EcfmRemoteNpFsMiEcfmHwInit;

            pRemEntry->u4ContextId = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MI_ECFM_MBSM_HW_CALL_NP_API) ||
        (pFsHwNp->u4Opcode > FS_MI_ECFM_MBSM_NP_INIT_HW))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_ECFM_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

VOID
NpUtilPortCheck (UINT4 u4IfIndex, tHwPortInfo * pPortCheckHwPortInfo,
                 UINT1 *pu1NpCallStatus)
{
    if (NpUtilIsPortChannel (u4IfIndex) == FNP_SUCCESS)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (NpUtilIsRemotePort (u4IfIndex, pPortCheckHwPortInfo) ==
             FNP_SUCCESS)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
}
#endif /*__ECFM_NPUTIL_C__*/
