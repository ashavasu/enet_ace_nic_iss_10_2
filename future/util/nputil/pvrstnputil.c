/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstnputil.c,v 1.4 2014/01/29 13:14:26 siva Exp $
 *
 * Description:This file contains the single NP PVRSTP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __PVRSTUTIL_C__
#define __PVRSTUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskPvrstNpPorts                           */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef PVRST_WANTED
VOID
NpUtilMaskPvrstNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE:
        case FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE:
        case FS_MI_PVRST_NP_INIT_HW:
        case FS_MI_PVRST_NP_DE_INIT_HW:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
        case FS_MI_PVRST_NP_GET_VLAN_PORT_STATE:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        default:
            break;
    }
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPvrstConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPvrstConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstRemoteNpModInfo *pPvrstRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pPvrstNpModInfo = &(pFsHwNp->PvrstNpModInfo);
    pPvrstRemoteNpModInfo = &(pRemoteHwNp->PvrstRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tPvrstRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE:
        {
            tPvrstNpWrFsMiPvrstNpCreateVlanSpanningTree *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpCreateVlanSpanningTree *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pPvrstNpModInfo->PvrstNpFsMiPvrstNpCreateVlanSpanningTree;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpCreateVlanSpanningTree;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE:
        {
            tPvrstNpWrFsMiPvrstNpDeleteVlanSpanningTree *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpDeleteVlanSpanningTree *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeleteVlanSpanningTree;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpDeleteVlanSpanningTree;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            break;
        }
        case FS_MI_PVRST_NP_INIT_HW:
        {
            tPvrstNpWrFsMiPvrstNpInitHw *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpInitHw;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->PvrstRemoteNpFsMiPvrstNpInitHw;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_PVRST_NP_DE_INIT_HW:
        {
            tPvrstNpWrFsMiPvrstNpDeInitHw *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpDeInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpDeInitHw;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->PvrstRemoteNpFsMiPvrstNpDeInitHw;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpSetVlanPortState *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpSetVlanPortState *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpSetVlanPortState;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpSetVlanPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u1PortState = pLocalArgs->u1PortState;
            break;
        }
        case FS_MI_PVRST_NP_GET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpGetVlanPortState *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpGetVlanPortState *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpGetVlanPortState;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpGetVlanPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->u1Status), (pLocalArgs->pu1Status),
                    sizeof (UINT1));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPvrstConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPvrstConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPvrstNpModInfo    *pPvrstNpModInfo = NULL;
    tPvrstRemoteNpModInfo *pPvrstRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pPvrstNpModInfo = &(pFsHwNp->PvrstNpModInfo);
    pPvrstRemoteNpModInfo = &(pRemoteHwNp->PvrstRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpSetVlanPortState *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpSetVlanPortState *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpSetVlanPortState;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpSetVlanPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u1PortState = pRemoteArgs->u1PortState;
            break;
        }
        case FS_MI_PVRST_NP_GET_VLAN_PORT_STATE:
        {
            tPvrstNpWrFsMiPvrstNpGetVlanPortState *pLocalArgs = NULL;
            tPvrstRemoteNpWrFsMiPvrstNpGetVlanPortState *pRemoteArgs = NULL;
            pLocalArgs = &pPvrstNpModInfo->PvrstNpFsMiPvrstNpGetVlanPortState;
            pRemoteArgs =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpGetVlanPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY ((pLocalArgs->pu1Status), &(pRemoteArgs->u1Status),
                    sizeof (UINT1));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPvrstRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tPvrstNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPvrstRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                               tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPvrstRemoteNpModInfo *pPvrstRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pPvrstRemoteNpModInfo = &(pRemoteHwNpInput->PvrstRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE:
        {
            tPvrstRemoteNpWrFsMiPvrstNpCreateVlanSpanningTree *pInput = NULL;
            pInput =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpCreateVlanSpanningTree;
            u1RetVal =
                FsMiPvrstNpCreateVlanSpanningTree (pInput->u4ContextId,
                                                   pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE:
        {
            tPvrstRemoteNpWrFsMiPvrstNpDeleteVlanSpanningTree *pInput = NULL;
            pInput =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpDeleteVlanSpanningTree;
            u1RetVal =
                FsMiPvrstNpDeleteVlanSpanningTree (pInput->u4ContextId,
                                                   pInput->VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PVRST_NP_INIT_HW:
        {
            tPvrstRemoteNpWrFsMiPvrstNpInitHw *pInput = NULL;
            pInput = &pPvrstRemoteNpModInfo->PvrstRemoteNpFsMiPvrstNpInitHw;
            u1RetVal = FsMiPvrstNpInitHw (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PVRST_NP_DE_INIT_HW:
        {
            tPvrstRemoteNpWrFsMiPvrstNpDeInitHw *pInput = NULL;
            pInput = &pPvrstRemoteNpModInfo->PvrstRemoteNpFsMiPvrstNpDeInitHw;
            FsMiPvrstNpDeInitHw (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
        {
            tPvrstRemoteNpWrFsMiPvrstNpSetVlanPortState *pInput = NULL;
            pInput =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpSetVlanPortState;
            u1RetVal =
                FsMiPvrstNpSetVlanPortState (pInput->u4ContextId,
                                             pInput->u4IfIndex, pInput->VlanId,
                                             pInput->u1PortState);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PVRST_NP_GET_VLAN_PORT_STATE:
        {
            tPvrstRemoteNpWrFsMiPvrstNpGetVlanPortState *pInput = NULL;
            pInput =
                &pPvrstRemoteNpModInfo->
                PvrstRemoteNpFsMiPvrstNpGetVlanPortState;
            u1RetVal =
                FsMiPvrstNpGetVlanPortState (pInput->u4ContextId,
                                             pInput->VlanId, pInput->u4IfIndex,
                                             &(pInput->u1Status));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* PVRST_WANTED */
#endif /* __PVRSTUTIL_C__ */
