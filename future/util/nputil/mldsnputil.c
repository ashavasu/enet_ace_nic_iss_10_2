/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnputil.c,v 1.4 2014/10/08 11:01:24 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __MLDSNPUTIL_C__
#define __MLDSNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskMldsNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef MLDS_WANTED
VOID
NpUtilMaskMldsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsRemoteNpModInfo *pMldsRemoteNpModInfo = NULL;
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    pMldsNpModInfo = &(pFsHwNp->MldsNpModInfo);
    pMldsRemoteNpModInfo = &(pRemoteHwNp->MldsRemoteNpModInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING:
        case FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING:
        case FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING:
        case FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING:
        case FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES:
        {
            tPortList          *pPortList = NULL;
            tPortList          *pUntagPortList = NULL;
            tPortList          *pRemotePortList = NULL;
            tPortList          *pRemoteUntagPortList = NULL;
            UINT4               u4LocalPortCount = 0;
            UINT4               u4RemotePortCount = 0;
            tMldsNpWrFsMiNpUpdateIp6mcFwdEntries *pEntry = NULL;
            tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries *pRemEntry = NULL;

            pEntry = &pMldsNpModInfo->MldsNpFsMiNpUpdateIp6mcFwdEntries;
            pRemEntry =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiNpUpdateIp6mcFwdEntries;

            pPortList = (tPortList *) pEntry->PortList;
            pRemotePortList = &pRemEntry->PortList;

            NpUtilMaskHwPortList ((UINT1 *) pPortList,
                                  (UINT1 *) pRemotePortList,
                                  &HwPortInfo,
                                  &u4LocalPortCount,
                                  &u4RemotePortCount, BRG_PORT_LIST_SIZE);

            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (u4LocalPortCount != 0)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }

            pUntagPortList = (tPortList *) pEntry->UntagPortList;
            pRemoteUntagPortList = &pRemEntry->UntagPortList;

            NpUtilMaskHwPortList ((UINT1 *) pUntagPortList,
                                  (UINT1 *) pRemoteUntagPortList,
                                  &HwPortInfo,
                                  &u4LocalPortCount,
                                  &u4RemotePortCount, BRG_PORT_LIST_SIZE);

            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (u4LocalPortCount != 0)
            {
                if (*pu1NpCallStatus != NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                }
            }
            else
            {
                if (*pu1NpCallStatus != NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }

            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING;
            tMldsNpWrFsMiMldsMbsmHwEnableIpMldSnooping *pEntry = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping *pRemEntry = NULL;

            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableIpMldSnooping;
            pRemEntry =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwEnableIpMldSnooping;

            pRemEntry->u4Instance = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING;
            tMldsNpWrFsMiMldsMbsmHwEnableMldSnooping *pEntry = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping *pRemEntry = NULL;

            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableMldSnooping;
            pRemEntry =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwEnableMldSnooping;

            pRemEntry->u4Instance = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
#endif /* MBSM_WANTED */

        default:
            break;
    }
#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING) ||
        ((pFsHwNp->u4Opcode > FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_MLDS_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldsConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldsConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsRemoteNpModInfo *pMldsRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMldsNpModInfo = &(pFsHwNp->MldsNpModInfo);
    pMldsRemoteNpModInfo = &(pRemoteHwNp->MldsRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tMldsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableIpMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwDisableIpMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableIpMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwDisableIpMldSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwDisableMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwDisableMldSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableIpMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableIpMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwEnableIpMldSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwEnableMldSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pMldsNpModInfo->MldsNpFsMiNpGetIp6FwdEntryHitBitStatus;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiNpGetIp6FwdEntryHitBitStatus;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tSnoopVlanId));
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pSrcAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcAddr), (pLocalArgs->pSrcAddr),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES:
        {
            tMldsNpWrFsMiNpUpdateIp6mcFwdEntries *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiNpUpdateIp6mcFwdEntries;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiNpUpdateIp6mcFwdEntries;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tSnoopVlanId));
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pSrcAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcAddr), (pLocalArgs->pSrcAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                        sizeof (tPortList));
            }
            if (pLocalArgs->UntagPortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->UntagPortList),
                        (pLocalArgs->UntagPortList), sizeof (tPortList));
            }
            pRemoteArgs->u1EventType = pLocalArgs->u1EventType;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING:
        case FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldsConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldsConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsRemoteNpModInfo *pMldsRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pMldsNpModInfo = &(pFsHwNp->MldsNpModInfo);
    pMldsRemoteNpModInfo = &(pRemoteHwNp->MldsRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableIpMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwDisableIpMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableIpMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwDisableIpMldSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwDisableMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwDisableMldSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableIpMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableIpMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwEnableIpMldSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableMldSnooping *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableMldSnooping;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwEnableMldSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pRemoteArgs = NULL;
            pLocalArgs =
                &pMldsNpModInfo->MldsNpFsMiNpGetIp6FwdEntryHitBitStatus;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiNpGetIp6FwdEntryHitBitStatus;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tSnoopVlanId));
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pGrpAddr), &(pRemoteArgs->grpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pSrcAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pSrcAddr), &(pRemoteArgs->srcAddr),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES:
        {
            tMldsNpWrFsMiNpUpdateIp6mcFwdEntries *pLocalArgs = NULL;
            tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pMldsNpModInfo->MldsNpFsMiNpUpdateIp6mcFwdEntries;
            pRemoteArgs =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiNpUpdateIp6mcFwdEntries;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tSnoopVlanId));
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pGrpAddr), &(pRemoteArgs->grpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pSrcAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pSrcAddr), &(pRemoteArgs->srcAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY ((pLocalArgs->PortList), &(pRemoteArgs->PortList),
                        sizeof (tPortList));
            }
            if (pLocalArgs->UntagPortList != NULL)
            {
                MEMCPY ((pLocalArgs->UntagPortList),
                        &(pRemoteArgs->UntagPortList), sizeof (tPortList));
            }
            pLocalArgs->u1EventType = pRemoteArgs->u1EventType;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING:
        case FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldsRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMldsNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldsRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldsRemoteNpModInfo *pMldsRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pMldsRemoteNpModInfo = &(pRemoteHwNpInput->MldsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING:
        {
            tMldsRemoteNpWrFsMiMldsHwDisableIpMldSnooping *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwDisableIpMldSnooping;
            u1RetVal = FsMiMldsHwDisableIpMldSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING:
        {
            tMldsRemoteNpWrFsMiMldsHwDisableMldSnooping *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwDisableMldSnooping;
            u1RetVal = FsMiMldsHwDisableMldSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING:
        {
            tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiMldsHwEnableIpMldSnooping;
            u1RetVal = FsMiMldsHwEnableIpMldSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING:
        {
            tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiMldsHwEnableMldSnooping;
            u1RetVal = FsMiMldsHwEnableMldSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tMldsRemoteNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->
                MldsRemoteNpFsMiNpGetIp6FwdEntryHitBitStatus;
            u1RetVal =
                FsMiNpGetIp6FwdEntryHitBitStatus (pInput->u4Instance,
                                                  pInput->VlanId,
                                                  &(pInput->grpAddr),
                                                  &(pInput->srcAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES:
        {
            tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries *pInput = NULL;
            pInput =
                &pMldsRemoteNpModInfo->MldsRemoteNpFsMiNpUpdateIp6mcFwdEntries;
            u1RetVal =
                FsMiNpUpdateIp6mcFwdEntries (pInput->u4Instance, pInput->VlanId,
                                             &(pInput->grpAddr),
                                             &(pInput->srcAddr),
                                             pInput->PortList,
                                             pInput->UntagPortList,
                                             pInput->u1EventType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* MLDS_WANTED */

#endif /* __MLDSNPUTIL_C__ */
