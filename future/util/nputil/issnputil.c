
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: issnputil.c,v 1.22 2016/07/02 09:42:19 siva Exp $
 *
 * Description:This file contains the single NP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __ISS_NPUTIL_C__
#define __ISS_NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

extern VOID NpSetTrace PROTO ((UINT1 u1TraceModule, UINT1 u1TraceLevel));
extern VOID NpGetTrace PROTO ((UINT1 u1TraceModule, UINT1 *pu1TraceLevel));
extern VOID NpSetTraceLevel PROTO ((UINT2 u2TraceLevel));
extern VOID NpGetTraceLevel PROTO ((UINT2 *pu2TraceLevel));


/***************************************************************************/
/*  Function Name       : IssCopyLocalToRemoteL2Filter                     */
/*                                                                         */
/*  Description         : This function to copy Local to Remote value L2   */
/*                                                                         */
/*  Input(s)            : pLocalArgs Param of type tIssL2FilterEntry       */
/*                                                                         */
/*  Output(s)           : Copy Local to Remote                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
IssCopyLocalToRemoteL2Filter(tRemoteIssL2FilterEntry *pRemoteArgs, tIssL2FilterEntry *pLocalArgs)
{

    MEMCPY (&(pRemoteArgs->IssNextNode),
            &(pLocalArgs->IssNextNode),
            sizeof(tIssSllNode));
    MEMCPY (&(pRemoteArgs->PriorityNode),
            &(pLocalArgs->PriorityNode),
            sizeof(tIssPriorityFilterEntry));
    MEMCPY (&(pRemoteArgs->IssSortedNextNode),
            &(pLocalArgs->IssSortedNextNode),
            sizeof(tIssSllNode));

    pRemoteArgs->i4IssL2FilterNo = pLocalArgs->i4IssL2FilterNo;
    pRemoteArgs->i4IssL2NextFilterNo = pLocalArgs->i4IssL2NextFilterNo;
    pRemoteArgs->i4IssL2NextFilterType = pLocalArgs->i4IssL2NextFilterType;

    if (pLocalArgs->pIssFilterShadowInfo != NULL)
    {
        MEMCPY (&(pRemoteArgs->IssFilterShadowInfo),
                (pLocalArgs->pIssFilterShadowInfo),
                sizeof(tIssFilterShadowTable) );
    }

    pRemoteArgs->i4IssL2FilterPriority = pLocalArgs->i4IssL2FilterPriority;
    pRemoteArgs->i4IssL2FilterEncapType = pLocalArgs->i4IssL2FilterEncapType;
    pRemoteArgs->i4IssL2FilterStatsEnabledStatus = pLocalArgs->i4IssL2FilterStatsEnabledStatus;

    pRemoteArgs->i4IssClearL2FilterStats = pLocalArgs->i4IssClearL2FilterStats;
    pRemoteArgs->u4IssL2FilterProtocolType = pLocalArgs->u4IssL2FilterProtocolType;
    pRemoteArgs->u4IssL2FilterCustomerVlanId = pLocalArgs->u4IssL2FilterCustomerVlanId;
    pRemoteArgs->u4IssL2FilterMatchCount = pLocalArgs->u4IssL2FilterMatchCount;
    pRemoteArgs->u4StatsTransitFlag = pLocalArgs->u4StatsTransitFlag;
    pRemoteArgs->u4IssL2RedirectId = pLocalArgs->u4IssL2RedirectId;
    pRemoteArgs->u4RefCount = pLocalArgs->u4RefCount;

    MEMCPY (&(pRemoteArgs->IssL2FilterAction),
            &(pLocalArgs->IssL2FilterAction),
            sizeof(tIssFilterAction));

    MEMCPY (&(pRemoteArgs->RedirectIfGrp),
            &(pLocalArgs->RedirectIfGrp),
            sizeof(tIssRedirectIfGrp));


    MEMCPY (&(pRemoteArgs->IssL2FilterDstMacAddr),
            &(pLocalArgs->IssL2FilterDstMacAddr),
            sizeof(tMacAddr));


    MEMCPY (&(pRemoteArgs->IssL2FilterSrcMacAddr),
            &(pLocalArgs->IssL2FilterSrcMacAddr),
            sizeof(tMacAddr));

    MEMCPY (&(pRemoteArgs->IssL2FilterInPortList),
            &(pLocalArgs->IssL2FilterInPortList),
            sizeof(tIssPortList));

    MEMCPY (&(pRemoteArgs->IssL2FilterOutPortList),
            &(pLocalArgs->IssL2FilterOutPortList),
            sizeof(tIssPortList));

    MEMCPY (&(pRemoteArgs->IssL2FilterInPortChannelList),
            &(pLocalArgs->IssL2FilterInPortChannelList),
            sizeof(tIssPortChannelList));

    MEMCPY (&(pRemoteArgs->IssL2FilterOutPortChannelList),
            &(pLocalArgs->IssL2FilterOutPortChannelList),
            sizeof(tIssPortChannelList));

    MEMCPY (&(pRemoteArgs->FilterRestore),
            &(pLocalArgs->FilterRestore),
            sizeof(tIssFilterRestore));

    pRemoteArgs->u2InnerEtherType = pLocalArgs->u2InnerEtherType;
    pRemoteArgs->u2OuterEtherType = pLocalArgs->u2OuterEtherType;
    pRemoteArgs->u2IssL2SubActionId = pLocalArgs->u2IssL2SubActionId;
    pRemoteArgs->i1IssL2FilterCVlanPriority = pLocalArgs->i1IssL2FilterCVlanPriority;
    pRemoteArgs->i1IssL2FilterSVlanPriority = pLocalArgs->i1IssL2FilterSVlanPriority;
    pRemoteArgs->u1IssL2FilterTagType = pLocalArgs->u1IssL2FilterTagType;
    pRemoteArgs->u1FilterDirection = pLocalArgs->u1FilterDirection;
    pRemoteArgs->u1IssL2FilterStatus = pLocalArgs->u1IssL2FilterStatus;
    pRemoteArgs->u1IssL2HwStatus = pLocalArgs->u1IssL2HwStatus;
    pRemoteArgs->u1IssL2SubAction = pLocalArgs->u1IssL2SubAction;
    pRemoteArgs->u1PriorityFlag = pLocalArgs->u1PriorityFlag;
    pRemoteArgs->u1IssL2FilterCreationMode = pLocalArgs->u1IssL2FilterCreationMode;
    pRemoteArgs->i1DropPrecedence = pLocalArgs->i1DropPrecedence;
    pRemoteArgs->i1CfiDei = pLocalArgs->i1CfiDei;
    pRemoteArgs->u1IssL2FilterUserPriority = pLocalArgs->u1IssL2FilterUserPriority;
    pRemoteArgs->u1IssL2FilterTrapSent = pLocalArgs->u1IssL2FilterTrapSent;
}

/***************************************************************************/
/*  Function Name       : IssCopyLocalToRemoteL3Filter                     */
/*                                                                         */
/*  Description         : This function to copy Local to Remote value L3   */
/*                                                                         */
/*  Input(s)            : pLocalArgs Param of type tIssL3FilterEntry       */
/*                                                                         */
/*  Output(s)           : Copy Local  to Remote                            */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
IssCopyLocalToRemoteL3Filter(tRemoteIssL3FilterEntry *pRemoteArgs, tIssL3FilterEntry *pLocalArgs)
{


    MEMCPY (&(pRemoteArgs->IssNextNode),
            &(pLocalArgs->IssNextNode),
            sizeof(tIssSllNode));

    MEMCPY (&(pRemoteArgs->PriorityNode),
            &(pLocalArgs->PriorityNode),
            sizeof(tIssPriorityFilterEntry));

    MEMCPY (&(pRemoteArgs->IssSortedNextNode),
            &(pLocalArgs->IssSortedNextNode),
            sizeof(tIssSllNode));

    pRemoteArgs->i4IssL3FilterNo = pLocalArgs->i4IssL3FilterNo;     

    pRemoteArgs->i4IssL3FilterPriority = pLocalArgs->i4IssL3FilterNo;

    MEMCPY (&(pRemoteArgs->IssL3FilterProtocol),
            &(pLocalArgs->IssL3FilterProtocol),        
            sizeof(tIssFilterProtocol));

    if(pLocalArgs->pIssFilterShadowInfo != NULL )
    {
        MEMCPY (&(pRemoteArgs->IssFilterShadowInfo),
                (pLocalArgs->pIssFilterShadowInfo),
                sizeof(tIssFilterShadowTable) );
    }


    pRemoteArgs->i4IssL3FilterMessageType = pLocalArgs->i4IssL3FilterMessageType;
    pRemoteArgs->i4IssL3FilterMessageCode = pLocalArgs->i4IssL3FilterMessageCode;

    pRemoteArgs->u4CVlanId = pLocalArgs->u4CVlanId;
    pRemoteArgs->u4SVlanId =pLocalArgs->u4SVlanId;
    pRemoteArgs->u4IssL3FilterDstIpAddr = pLocalArgs->u4IssL3FilterDstIpAddr;
    pRemoteArgs->u4IssL3FilterSrcIpAddr = pLocalArgs->u4IssL3FilterSrcIpAddr;
    pRemoteArgs->u4IssL3FilterDstIpAddrMask = pLocalArgs->u4IssL3FilterDstIpAddrMask;
    pRemoteArgs->u4IssL3FilterSrcIpAddrMask = pLocalArgs->u4IssL3FilterSrcIpAddrMask;
    pRemoteArgs->u4IssL3FilterMinDstProtPort = pLocalArgs->u4IssL3FilterMinDstProtPort;
    pRemoteArgs->u4IssL3FilterMaxDstProtPort = pLocalArgs->u4IssL3FilterMaxDstProtPort;
    pRemoteArgs->u4IssL3FilterMinSrcProtPort = pLocalArgs->u4IssL3FilterMinSrcProtPort;
    pRemoteArgs->u4IssL3FilterMaxSrcProtPort = pLocalArgs->u4IssL3FilterMaxSrcProtPort;
    MEMCPY (&(pRemoteArgs->IssL3FilterAckBit),
            &(pLocalArgs->IssL3FilterAckBit),
            sizeof(tIssAckBit));

    MEMCPY ( &(pRemoteArgs->IssL3FilterRstBit),
            &(pLocalArgs->IssL3FilterRstBit),
            sizeof(tIssRstBit));

    MEMCPY (&(pRemoteArgs->IssL3FilterTos),
            &(pLocalArgs->IssL3FilterTos),
            sizeof(tIssTos));

    pRemoteArgs->i4IssL3FilterDscp = pLocalArgs->i4IssL3FilterDscp;


    MEMCPY (&(pRemoteArgs->IssL3FilterDirection),
            &(pLocalArgs->IssL3FilterDirection),
            sizeof(tIssDirection));


    MEMCPY (&(pRemoteArgs->IssL3FilterAction),
            &(pLocalArgs->IssL3FilterAction),
            sizeof(tIssFilterAction));

    MEMCPY (&(pRemoteArgs->RedirectIfGrp),
            &(pLocalArgs->RedirectIfGrp),
            sizeof(tIssRedirectIfGrp));

    pRemoteArgs->u4StatsTransitFlag = pLocalArgs->u4StatsTransitFlag;
    pRemoteArgs->u4RefCount = pLocalArgs->u4RefCount;

    MEMCPY (&(pRemoteArgs->IssL3FilterInPortList),
            &(pLocalArgs->IssL3FilterInPortList),
            sizeof(tIssPortList));


    MEMCPY (&(pRemoteArgs->IssL3FilterOutPortList),
            &(pLocalArgs->IssL3FilterOutPortList),
            sizeof(tIssPortList));

    MEMCPY (&(pRemoteArgs->IssL3FilterInPortChannelList),
            &(pLocalArgs->IssL3FilterInPortChannelList),
            sizeof(tIssPortChannelList));


    MEMCPY (&(pRemoteArgs->IssL3FilterOutPortChannelList),
            &(pLocalArgs->IssL3FilterOutPortChannelList),
            sizeof(tIssPortChannelList));
    MEMCPY (&(pRemoteArgs->FilterRestore),
            &(pLocalArgs->FilterRestore),
            sizeof(tIssFilterRestore));

    pRemoteArgs->u4IssL3FilterMultiFieldClfrDstPrefixLength = pLocalArgs->u4IssL3FilterMultiFieldClfrDstPrefixLength;
    pRemoteArgs->u4IssL3FilterMultiFieldClfrSrcPrefixLength = pLocalArgs->u4IssL3FilterMultiFieldClfrSrcPrefixLength;
    pRemoteArgs->u4IssL3MultiFieldClfrFlowId = pLocalArgs->u4IssL3MultiFieldClfrFlowId;
    pRemoteArgs->u4IssL3RedirectId = pLocalArgs->u4IssL3RedirectId;
    pRemoteArgs->i4IssL3MultiFieldClfrAddrType = pLocalArgs->i4IssL3MultiFieldClfrAddrType;
    pRemoteArgs->i4StorageType = pLocalArgs->i4StorageType;
    pRemoteArgs->i4IssL3FilterStatsEnabledStatus = pLocalArgs->i4IssL3FilterStatsEnabledStatus;
    pRemoteArgs->i4IssClearL3FilterStats = pLocalArgs->i4IssClearL3FilterStats;

    MEMCPY ( &(pRemoteArgs->ipv6SrcIpAddress),
            &(pLocalArgs->ipv6SrcIpAddress),
            sizeof(tIp6Addr));

    MEMCPY ( &(pRemoteArgs->ipv6DstIpAddress),
            &(pLocalArgs->ipv6DstIpAddress),
            sizeof(tIp6Addr));

    pRemoteArgs->u2IssL3SubActionId = pLocalArgs->u2IssL3SubActionId;
    pRemoteArgs->i1CVlanPriority = pLocalArgs->i1CVlanPriority;
    pRemoteArgs->i1SVlanPriority = pLocalArgs->i1SVlanPriority;
    pRemoteArgs->u1IssL3FilterTagType = pLocalArgs->u1IssL3FilterTagType;
    pRemoteArgs->u1IssL3FilterStatus = pLocalArgs->u1IssL3FilterStatus;
    pRemoteArgs->u1IssL3HwStatus = pLocalArgs->u1IssL3HwStatus;
    pRemoteArgs->u1IssL3SubAction = pLocalArgs->u1IssL3SubAction;
    pRemoteArgs->u1PriorityFlag = pLocalArgs->u1PriorityFlag;
    pRemoteArgs->u1IssL3FilterCreationMode = pLocalArgs->u1IssL3FilterCreationMode;
    pRemoteArgs->u1IssL3FilterTrapSent = pLocalArgs->u1IssL3FilterTrapSent;
}

/***************************************************************************/
/*  Function Name       : IssCopyRemoteToLocalL2Filter                     */
/*                                                                         */
/*  Description         : This function to copy Remote to Local value L2   */
/*                                                                         */
/*  Input(s)            : pRemoteArgs Param of type tRemoteIssL2FilterEntry*/
/*                                                                         */
/*  Output(s)           : Copy Remote to local                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
IssCopyRemoteToLocalL2Filter(tIssL2FilterEntry *pLocalArgs, tRemoteIssL2FilterEntry *pRemoteArgs)
{

    MEMCPY (&(pLocalArgs->IssNextNode),
            &(pRemoteArgs->IssNextNode),
            sizeof(tIssSllNode));
    MEMCPY (&(pLocalArgs->PriorityNode),
            &(pRemoteArgs->PriorityNode),
            sizeof(tIssPriorityFilterEntry));
    MEMCPY (&(pLocalArgs->IssSortedNextNode),
            &(pRemoteArgs->IssSortedNextNode),
            sizeof(tIssSllNode));

    pLocalArgs->i4IssL2FilterNo = pRemoteArgs->i4IssL2FilterNo;
    pLocalArgs->i4IssL2NextFilterNo = pRemoteArgs->i4IssL2NextFilterNo; 
    pLocalArgs->i4IssL2NextFilterType = pRemoteArgs->i4IssL2NextFilterType; 

    if (pLocalArgs->pIssFilterShadowInfo != NULL)
    {
        MEMCPY ((pLocalArgs->pIssFilterShadowInfo),
                &(pRemoteArgs->IssFilterShadowInfo),
                sizeof(tIssFilterShadowTable) );
    }

    pLocalArgs->i4IssL2FilterPriority = pRemoteArgs->i4IssL2FilterPriority;


    pLocalArgs->i4IssL2FilterEncapType = pRemoteArgs->i4IssL2FilterEncapType;
    pLocalArgs->i4IssL2FilterStatsEnabledStatus  = pRemoteArgs->i4IssL2FilterStatsEnabledStatus;

    pLocalArgs->i4IssClearL2FilterStats =pRemoteArgs->i4IssClearL2FilterStats;
    pLocalArgs->u4IssL2FilterProtocolType = pRemoteArgs->u4IssL2FilterProtocolType; 
    pLocalArgs->u4IssL2FilterCustomerVlanId = pRemoteArgs->u4IssL2FilterCustomerVlanId;
    pLocalArgs->u4IssL2FilterServiceVlanId = pRemoteArgs->u4IssL2FilterServiceVlanId;
    pLocalArgs->u4IssL2FilterMatchCount = pRemoteArgs->u4IssL2FilterMatchCount;
    pLocalArgs->u4StatsTransitFlag = pRemoteArgs->u4StatsTransitFlag;
    pLocalArgs->u4IssL2RedirectId = pRemoteArgs->u4IssL2RedirectId;
    pLocalArgs->u4RefCount = pRemoteArgs->u4RefCount;

    MEMCPY ( &(pLocalArgs->IssL2FilterAction),
            &(pRemoteArgs->IssL2FilterAction),
            sizeof(tIssFilterAction));

    MEMCPY ( &(pLocalArgs->RedirectIfGrp),
            &(pRemoteArgs->RedirectIfGrp),
            sizeof(tIssRedirectIfGrp));


    MEMCPY ( &(pLocalArgs->IssL2FilterDstMacAddr),
            &(pRemoteArgs->IssL2FilterDstMacAddr),
            sizeof(tMacAddr));


    MEMCPY ( &(pLocalArgs->IssL2FilterSrcMacAddr),
            &(pRemoteArgs->IssL2FilterSrcMacAddr),
            sizeof(tMacAddr));

    MEMCPY ( &(pLocalArgs->IssL2FilterInPortList),
            &(pRemoteArgs->IssL2FilterInPortList),
            sizeof(tIssPortList));

    MEMCPY ( &(pLocalArgs->IssL2FilterOutPortList),
            &(pRemoteArgs->IssL2FilterOutPortList),
            sizeof(tIssPortList));

    MEMCPY (&(pLocalArgs->IssL2FilterInPortChannelList),
            &(pRemoteArgs->IssL2FilterInPortChannelList),
            sizeof(tIssPortChannelList));

    MEMCPY ( &(pLocalArgs->IssL2FilterOutPortChannelList),
            &(pRemoteArgs->IssL2FilterOutPortChannelList),
            sizeof(tIssPortChannelList));

    MEMCPY ( &(pLocalArgs->FilterRestore),
            &(pRemoteArgs->FilterRestore),
            sizeof(tIssFilterRestore));

    pLocalArgs->u2InnerEtherType = pRemoteArgs->u2InnerEtherType;
    pLocalArgs->u2OuterEtherType = pRemoteArgs->u2OuterEtherType;
    pLocalArgs->u2IssL2SubActionId = pRemoteArgs->u2IssL2SubActionId;
    pLocalArgs->i1IssL2FilterCVlanPriority = pRemoteArgs->i1IssL2FilterCVlanPriority;

    pLocalArgs->i1IssL2FilterSVlanPriority = pRemoteArgs->i1IssL2FilterSVlanPriority;
    pLocalArgs->u1IssL2FilterTagType = pRemoteArgs->u1IssL2FilterTagType;
    pLocalArgs->u1FilterDirection = pRemoteArgs->u1FilterDirection;
    pLocalArgs->u1IssL2FilterStatus = pRemoteArgs->u1IssL2FilterStatus;
    pLocalArgs->u1IssL2HwStatus = pRemoteArgs->u1IssL2HwStatus;
    pLocalArgs->u1IssL2SubAction	= pRemoteArgs->u1IssL2SubAction;
    pLocalArgs->u1PriorityFlag = pRemoteArgs->u1PriorityFlag;
    pLocalArgs->u1IssL2FilterCreationMode = pRemoteArgs->u1IssL2FilterCreationMode;
    pLocalArgs->i1DropPrecedence = pRemoteArgs->i1DropPrecedence;
    pLocalArgs->i1CfiDei = pRemoteArgs->i1CfiDei;
    pLocalArgs->u1IssL2FilterUserPriority = pRemoteArgs->u1IssL2FilterUserPriority ;
    pLocalArgs->u1IssL2FilterTrapSent = pRemoteArgs->u1IssL2FilterTrapSent;
}

/***************************************************************************/
/*  Function Name       : IssCopyRemoteToLocalL3Filter                     */
/*                                                                         */
/*  Description         : This function to copy Remote to Local value L3   */
/*                                                                         */
/*  Input(s)            : pRemoteArgs Param of type tRemoteIssL3FilterEntry*/
/*                                                                         */
/*  Output(s)           : Copy Remote to local                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
IssCopyRemoteToLocalL3Filter(tIssL3FilterEntry *pLocalArgs, tRemoteIssL3FilterEntry *pRemoteArgs)
{

    MEMCPY (&(pLocalArgs->IssNextNode),
            &(pRemoteArgs->IssNextNode),
            sizeof(tIssSllNode));
    MEMCPY (&(pLocalArgs->PriorityNode),
            &(pRemoteArgs->PriorityNode),
            sizeof(tIssPriorityFilterEntry));

    MEMCPY (&(pLocalArgs->IssSortedNextNode),
            &(pRemoteArgs->IssSortedNextNode),
            sizeof(tIssSllNode));


    pLocalArgs->i4IssL3FilterNo = pRemoteArgs->i4IssL3FilterNo;
    pLocalArgs->i4IssL3FilterNo = pRemoteArgs->i4IssL3FilterPriority;

    MEMCPY (&(pLocalArgs->IssL3FilterProtocol),
            &(pRemoteArgs->IssL3FilterProtocol),
            sizeof(tIssFilterProtocol));


    if( pLocalArgs->pIssFilterShadowInfo != NULL )
    {
        MEMCPY ( (pLocalArgs->pIssFilterShadowInfo),
                &(pRemoteArgs->IssFilterShadowInfo),
                sizeof(tIssFilterShadowTable) );
    }

    pLocalArgs->i4IssL3FilterMessageType = pRemoteArgs->i4IssL3FilterMessageType;
    pLocalArgs->i4IssL3FilterMessageCode = pRemoteArgs->i4IssL3FilterMessageCode;


    pLocalArgs->u4CVlanId  = pRemoteArgs->u4CVlanId;
    pLocalArgs->u4SVlanId = pRemoteArgs->u4SVlanId;
    pLocalArgs->u4IssL3FilterDstIpAddr = pRemoteArgs->u4IssL3FilterDstIpAddr;
    pLocalArgs->u4IssL3FilterSrcIpAddr = pRemoteArgs->u4IssL3FilterSrcIpAddr;
    pLocalArgs->u4IssL3FilterDstIpAddrMask = pRemoteArgs->u4IssL3FilterDstIpAddrMask;
    pLocalArgs->u4IssL3FilterSrcIpAddrMask = pRemoteArgs->u4IssL3FilterSrcIpAddrMask;
    pLocalArgs->u4IssL3FilterMinDstProtPort = pRemoteArgs->u4IssL3FilterMinDstProtPort;
    pLocalArgs->u4IssL3FilterMaxDstProtPort = pRemoteArgs->u4IssL3FilterMaxDstProtPort;
    pLocalArgs->u4IssL3FilterMinSrcProtPort = pRemoteArgs->u4IssL3FilterMinSrcProtPort;
    pLocalArgs->u4IssL3FilterMaxSrcProtPort = pRemoteArgs->u4IssL3FilterMaxSrcProtPort;
    MEMCPY (&(pLocalArgs->IssL3FilterAckBit),
            &(pRemoteArgs->IssL3FilterAckBit),
            sizeof(tIssAckBit));


    MEMCPY (&(pLocalArgs->IssL3FilterRstBit),
            &(pRemoteArgs->IssL3FilterRstBit),
            sizeof(tIssRstBit));

    MEMCPY (&(pLocalArgs->IssL3FilterTos),
            &(pRemoteArgs->IssL3FilterTos),
            sizeof(tIssTos));

    pLocalArgs->i4IssL3FilterDscp = pRemoteArgs->i4IssL3FilterDscp;


    MEMCPY (&(pLocalArgs->IssL3FilterDirection),
            &(pRemoteArgs->IssL3FilterDirection),
            sizeof(tIssDirection));


    MEMCPY (&(pLocalArgs->IssL3FilterAction),
            &(pRemoteArgs->IssL3FilterAction),
            sizeof(tIssFilterAction));

    MEMCPY (&(pLocalArgs->RedirectIfGrp),
            &(pRemoteArgs->RedirectIfGrp),
            sizeof(tIssRedirectIfGrp));

    pLocalArgs->u4IssL3FilterMatchCount = pRemoteArgs->u4IssL3FilterMatchCount;
    pLocalArgs->u4StatsTransitFlag = pRemoteArgs->u4StatsTransitFlag;
    pLocalArgs->u4RefCount = pRemoteArgs->u4RefCount;

    MEMCPY (&(pLocalArgs->IssL3FilterInPortList),
            &(pRemoteArgs->IssL3FilterInPortList),
            sizeof(tIssPortList));


    MEMCPY (&(pLocalArgs->IssL3FilterOutPortList),
            &(pRemoteArgs->IssL3FilterOutPortList),
            sizeof(tIssPortList));

    MEMCPY (&(pLocalArgs->IssL3FilterInPortChannelList),
            &(pRemoteArgs->IssL3FilterInPortChannelList),
            sizeof(tIssPortChannelList));

    MEMCPY (&(pLocalArgs->IssL3FilterOutPortChannelList),
            &(pRemoteArgs->IssL3FilterOutPortChannelList),
            sizeof(tIssPortChannelList));
    MEMCPY (&(pLocalArgs->FilterRestore),
            &(pRemoteArgs->FilterRestore),
            sizeof(tIssFilterRestore));

    pLocalArgs->u4IssL3FilterMultiFieldClfrDstPrefixLength = pRemoteArgs->u4IssL3FilterMultiFieldClfrDstPrefixLength;
    pLocalArgs->u4IssL3FilterMultiFieldClfrSrcPrefixLength = pRemoteArgs->u4IssL3FilterMultiFieldClfrSrcPrefixLength;
    pLocalArgs->u4IssL3MultiFieldClfrFlowId = pRemoteArgs->u4IssL3MultiFieldClfrFlowId;
    pLocalArgs->u4IssL3RedirectId = pRemoteArgs->u4IssL3RedirectId;
    pLocalArgs->i4IssL3MultiFieldClfrAddrType = pRemoteArgs->i4IssL3MultiFieldClfrAddrType;
    pLocalArgs->i4StorageType = pRemoteArgs->i4StorageType;
    pLocalArgs->i4IssL3FilterStatsEnabledStatus = pRemoteArgs->i4IssL3FilterStatsEnabledStatus;
    pLocalArgs->i4IssClearL3FilterStats = pRemoteArgs->i4IssClearL3FilterStats;

    MEMCPY (&(pLocalArgs->ipv6SrcIpAddress),
            &(pRemoteArgs->ipv6SrcIpAddress),
            sizeof(tIp6Addr));

    MEMCPY (&(pLocalArgs->ipv6DstIpAddress),
            &(pRemoteArgs->ipv6DstIpAddress),
            sizeof(tIp6Addr));

    pLocalArgs->u2IssL3SubActionId = pRemoteArgs->u2IssL3SubActionId;
    pLocalArgs->i1CVlanPriority = pRemoteArgs->i1CVlanPriority;
    pLocalArgs->i1SVlanPriority = pRemoteArgs->i1SVlanPriority;
    pLocalArgs->u1IssL3FilterTagType = pRemoteArgs->u1IssL3FilterTagType;
    pLocalArgs->u1IssL3FilterStatus = pRemoteArgs->u1IssL3FilterStatus;
    pLocalArgs->u1IssL3HwStatus = pRemoteArgs->u1IssL3HwStatus;
    pLocalArgs->u1IssL3SubAction = pRemoteArgs->u1IssL3SubAction;
    pLocalArgs->u1PriorityFlag = pRemoteArgs->u1PriorityFlag;
    pLocalArgs->u1IssL3FilterCreationMode = pRemoteArgs->u1IssL3FilterCreationMode;
    pLocalArgs->u1IssL3FilterTrapSent = pRemoteArgs->u1IssL3FilterTrapSent;

}

/***************************************************************************/
/*  Function Name       : NpUtilMaskIsssysNpPorts                          */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIsssysNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                         UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysRemoteNpModInfo *pRemoteIsssysNpModInfo = NULL;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    UINT2               u2NumPorts = 0;
    BOOL1               bRemPortCheck = FNP_FALSE;
    INT4                i4Port;
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    pIsssysNpModInfo = &(pFsHwNp->IsssysNpModInfo);
    pRemoteIsssysNpModInfo = &(pRemoteHwNp->IsssysRemoteNpModInfo);


    /* Get the stacking port of the Local and Remote unit */
    CfaNpGetHwPortInfo (&LocalHwPortInfo);

    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);


    u4StackingModel = ISS_GET_STACKING_MODEL ();
    switch (pFsHwNp->u4Opcode)
    {
        case ISS_HW_SET_PORT_EGRESS_STATUS:
        {
            bRemPortCheck = FNP_FALSE;
            break;
        }
        case ISS_HW_SET_PORT_STATS_COLLECTION:
        {
            bRemPortCheck = FNP_FALSE;
            break;
        }
        case ISS_HW_SET_PORT_MODE:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_DUPLEX:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_SPEED:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_RENEGOTIATE:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ADDR:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ACTION:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_MIRRORING_STATUS:
        case ISS_HW_SET_MIRROR_TO_PORT:
        case ISS_HW_SET_INGRESS_MIRRORING:
        case ISS_HW_SET_EGRESS_MIRRORING:
        case ISS_HW_SET_MIRRORING:
        case ISS_HW_MIRROR_ADD_REMOVE_PORT:
        case ISS_HW_CPU_MIRRORING:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case ISS_HW_SET_RATE_LIMITING_VALUE:
        {
            tIsssysNpWrIssHwSetRateLimitingValue *pEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetRateLimitingValue;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else
            {
                bRemPortCheck = FNP_TRUE;
            }
            break;
        }
        case ISS_HW_SET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwSetPortEgressPktRate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressPktRate;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else
            {
                bRemPortCheck = FNP_TRUE;
            }
            break;
        }
        case ISS_HW_GET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwGetPortEgressPktRate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortEgressPktRate;
            if (NpUtilIsPortChannel (pEntry->u4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else
            {
                bRemPortCheck = FNP_TRUE;
            }
            break;
        }
        case ISS_HW_RESTART_SYSTEM:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            *pi4RpcCallStatus = OSIX_NO_WAIT;
            break;
        }
        case ISS_HW_INIT_FILTER:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case ISS_HW_GET_PORT_DUPLEX:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_GET_PORT_SPEED:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_GET_PORT_FLOW_CONTROL:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssHwUpdateL2Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL2Filter;
            pRemEntry =
                &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;

            NpUtilIssHwUpdateL2Filter (pEntry->pIssL2FilterEntry,
                                       &pRemEntry->IssL2FilterEntry,
                                       pEntry->i4Value,
                                       &HwPortInfo, pu1NpCallStatus);
            break;
        }
        case ISS_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssHwUpdateL3Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL3Filter;
            pRemEntry =
                &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

            NpUtilIssHwUpdateL3Filter (pEntry->pIssL3FilterEntry,
                                       &pRemEntry->IssL3FilterEntry,
                                       pEntry->i4Value,
                                       &HwPortInfo, pu1NpCallStatus);
            break;
        }
        case ISS_HW_UPDATE_L4_S_FILTER:
        {
            tIsssysNpWrIssHwUpdateL4SFilter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL4SFilter;
            if (NpUtilIsRemotePort
                (pEntry->pIssL4SFilterEntry->u4L4SCopyToPort,
                 &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            break;
        }
        case ISS_HW_SEND_BUFFER_TO_LINUX:
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            break;
        }
        case ISS_HW_GET_FAN_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case ISS_HW_INIT_MIRR_DATA_BASE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case ISS_HW_SET_MAC_LEARNING_RATE_LIMIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case ISS_HW_GET_LEARNED_MAC_ADDR_COUNT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case ISS_HW_UPDATE_USER_DEFINED_FILTER:
        {
            tIsssysNpWrIssHwUpdateUserDefinedFilter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter *pRemEntry = NULL;
            UINT1               u1L2NpCallStatus;
            UINT1               u1L3NpCallStatus;
            UINT1               u1UDNpCallStatus;

            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;
            pRemEntry =
                &pRemoteIsssysNpModInfo->
                IsssysRemoteNpIssHwUpdateUserDefinedFilter;

            if (pEntry->pL2AccessFilterEntry != NULL)
            {
                NpUtilIssHwUpdateL2Filter (pEntry->pL2AccessFilterEntry,
                                           &pRemEntry->L2AccessFilterEntry,
                                           ISS_L2FILTER_STAT_DISABLE,
                                           &HwPortInfo, &u1L2NpCallStatus);
            }
            if (pEntry->pL3AccessFilterEntry != NULL)
            {
                NpUtilIssHwUpdateL3Filter (pEntry->pL3AccessFilterEntry,
                                           &pRemEntry->L3AccessFilterEntry,
                                           ISS_L3FILTER_STAT_DISABLE,
                                           &HwPortInfo, &u1L3NpCallStatus);
            }
            if (pEntry->pAccessFilterEntry != NULL)
            {
                NpUtilIssHwUpdateUDFilter (pEntry->pAccessFilterEntry,
                                           &pRemEntry->AccessFilterEntry,
                                           pEntry->i4FilterAction,
                                           &HwPortInfo, &u1UDNpCallStatus);
            }
            if ((u1L2NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) &&
                (u1L3NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) &&
                (u1UDNpCallStatus == NPUTIL_INVOKE_LOCAL_NP))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            else if ((u1L2NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) &&
                     (u1L3NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) &&
                     (u1UDNpCallStatus == NPUTIL_INVOKE_REMOTE_NP))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            break;
        }
        case ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_LEARNING_MODE:
        {
            tIsssysNpWrIssHwSetLearningMode *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;
            if (NpUtilIsPortChannel (pEntry->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else
            {
                bRemPortCheck = FNP_TRUE;
            }
            break;
        }
        case ISS_HW_SET_PORT_MDI_OR_MDIX_CAP:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_GET_PORT_MDI_OR_MDIX_CAP:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL_RATE:
        {
            bRemPortCheck = FNP_TRUE;
            break;
        }
        case ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysNpWrIssHwConfigPortIsolationEntry *pEntry = NULL;
            tIsssysRemoteNpWrIssHwConfigPortIsolationEntry *pRemEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwConfigPortIsolationEntry;
            pRemEntry = &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwConfigPortIsolationEntry;
            u2NumPorts = pEntry->pPortIsolation->u2NumEgressPorts;
            if (NpUtilIsPortChannel (pEntry->pPortIsolation->u4IngressPort) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                pEntry->pPortIsolation->au4EgressPorts[u2NumPorts] = u4LocalStackPort;
                pEntry->pPortIsolation->u2NumEgressPorts++;
                pRemEntry->PortIsolation.au4EgressPorts[u2NumPorts] = u4RemoteStackPort;
                pRemEntry->PortIsolation.u2NumEgressPorts++;
            }
            else if (NpUtilIsRemotePort (pEntry->pPortIsolation->u4IngressPort,
                                         &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                for(i4Port = 0;i4Port < u2NumPorts;i4Port++)
                {
                    if (NpUtilIsPortChannel (pEntry->pPortIsolation->au4EgressPorts[i4Port]))
                    {
                        pRemEntry->PortIsolation.au4EgressPorts[u2NumPorts] = u4RemoteStackPort;
                        pRemEntry->PortIsolation.u2NumEgressPorts++;

                    }
                    else if (NpUtilIsRemotePort (pEntry->pPortIsolation->au4EgressPorts[i4Port],
                                                 &HwPortInfo) != FNP_SUCCESS)
                    {
                        pRemEntry->PortIsolation.au4EgressPorts[i4Port] = u4RemoteStackPort; 

                    }

                }
            }
            else
            {
                for(i4Port = 0;i4Port < u2NumPorts;i4Port++)
                {
                    if (NpUtilIsPortChannel (pEntry->pPortIsolation->au4EgressPorts[i4Port]) == FNP_SUCCESS)
                    {
                        pEntry->pPortIsolation->au4EgressPorts[u2NumPorts] = u4LocalStackPort;
                        pEntry->pPortIsolation->u2NumEgressPorts++;
                        
                    }
                    else if (NpUtilIsRemotePort (pEntry->pPortIsolation->au4EgressPorts[i4Port],
                                                 &HwPortInfo) == FNP_SUCCESS)
                    {
                        pEntry->pPortIsolation->au4EgressPorts[i4Port] = u4LocalStackPort; 
                        
                    }
                }
            }

            break;
        }
        case NP_SET_TRACE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case NP_GET_TRACE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case ISS_NP_HW_GET_CAPABILITIES:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#ifdef NPAPI_WANTED        
        case NP_SET_TRACE_LEVEL:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case NP_GET_TRACE_LEVEL:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#endif
#ifdef MBSM_WANTED
        case ISS_MBSM_HW_SET_PORT_EGRESS_STATUS:
        {
            tIsssysNpWrIssMbsmHwSetPortEgressStatus *pEntry = NULL;
            tIsssysRemoteNpWrIssHwSetPortEgressStatus *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortEgressStatus;
            pRemEntry =
                &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwSetPortEgressStatus;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            pRemEntry->u1EgressEnable = pEntry->u1Status;
            pRemoteHwNp->u4Opcode = ISS_HW_SET_PORT_EGRESS_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case ISS_MBSM_HW_SET_PORT_STATS_COLLECTION:
        {
            tIsssysNpWrIssMbsmHwSetPortStatsCollection *pEntry = NULL;
            tIsssysRemoteNpWrIssHwSetPortStatsCollection *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortStatsCollection;
            pRemEntry =
                &pRemoteIsssysNpModInfo->
                IsssysRemoteNpIssHwSetPortStatsCollection;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            pRemEntry->u1StatsEnable = pEntry->u1Status;
            pRemoteHwNp->u4Opcode = ISS_HW_SET_PORT_STATS_COLLECTION;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS:
        {
            tIsssysNpWrIssMbsmHwSetPortMirroringStatus *pEntry = NULL;
            tIsssysRemoteNpWrIssHwSetPortMirroringStatus *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortMirroringStatus;
            pRemEntry =
                &pRemoteIsssysNpModInfo->
                IsssysRemoteNpIssHwSetPortMirroringStatus;
            pRemEntry->i4MirrorStatus = pEntry->i4MirrorStatus;
            pRemoteHwNp->u4Opcode = ISS_HW_SET_PORT_MIRRORING_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
	case ISS_MBSM_HW_SET_MIRROR_TO_PORT:
	{
		tIsssysNpWrIssMbsmHwSetMirrorToPort *pEntry = NULL;
		tIsssysRemoteNpWrIssHwSetMirrorToPort *pRemEntry = NULL;

		pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetMirrorToPort;
		pRemEntry =
			&pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwSetMirrorToPort;
		pRemEntry->u4IfIndex = pEntry->u4MirrorPort;
		pRemoteHwNp->u4Opcode = ISS_HW_SET_MIRROR_TO_PORT;
		*pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
		break;
	}
    case ISS_MBSM_HW_CPU_MIRRORING:
        {
            tIsssysNpWrIssMbsmHwSetCpuMirroring *pEntry = NULL;
            tIsssysRemoteNpWrIssHwSetCpuMirroring *pRemEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetCpuMirroring;
            pRemEntry =
                &pRemoteIsssysNpModInfo->IsssysRemoteNpWrIssHwSetCpuMirroring;
            pRemEntry->u4IssCpuMirrorToPort = pEntry->u4IssCpuMirrorToPort;
            pRemEntry->i4IssCpuMirrorType = pEntry->i4IssCpuMirrorType;
            pRemoteHwNp->u4Opcode = ISS_HW_CPU_MIRRORING;
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }


    case ISS_MBSM_HW_UPDATE_L2_FILTER:
    {
        tIsssysNpWrIssMbsmHwUpdateL2Filter *pEntry = NULL;
        tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemEntry = NULL;

        pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL2Filter;
        pRemEntry =
            &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;

        if(pEntry->pIssL2FilterEntry != NULL )
        {
            IssCopyLocalToRemoteL2Filter(&pRemEntry->IssL2FilterEntry, 
                                         pEntry->pIssL2FilterEntry);
        }
        pRemEntry->i4Value = pEntry->i4Value;
        pRemoteHwNp->u4Opcode = ISS_HW_UPDATE_L2_FILTER;
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        break;
    }

        case ISS_MBSM_HW_SET_MIRRORING:
        {
            tIsssysNpWrIssMbsmHwSetMirroring *pEntry = NULL;
            tIsssysRemoteNpWrIssHwSetMirroring *pRemEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetMirroring;
            pRemEntry =
                &pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwSetMirroring;
            if (pEntry->pMirrorInfo != NULL)
            {
                MEMCPY (&(pRemEntry->MirrorInfo), (pEntry->pMirrorInfo),
                        sizeof (tIssHwMirrorInfo));
            }
            pRemoteHwNp->u4Opcode = ISS_HW_SET_MIRRORING;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
	case ISS_MBSM_HW_UPDATE_L3_FILTER:
	{
		tIsssysNpWrIssMbsmHwUpdateL3Filter *pEntry = NULL;
		tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemEntry = NULL;

		pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL3Filter;
		pRemEntry =
			&pRemoteIsssysNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

		if (pEntry->pIssL3FilterEntry != NULL)
		{
            IssCopyLocalToRemoteL3Filter(&(pRemEntry->IssL3FilterEntry), 
                                         pEntry->pIssL3FilterEntry);
        }
		pRemEntry->i4Value = pEntry->i4Value;
		pRemoteHwNp->u4Opcode = ISS_HW_UPDATE_L3_FILTER;
		*pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
		break;
	}
        case ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysNpWrIssPIMbsmHwConfigPortIsolationEntry *pEntry = NULL;
            tIsssysRemoteNpWrIssHwConfigPortIsolationEntry *pRemEntry = NULL;

            pEntry =
                &pIsssysNpModInfo->IsssysNpIssPIMbsmHwConfigPortIsolationEntry;
            pRemEntry =
                &pRemoteIsssysNpModInfo->
                IsssysRemoteNpIssHwConfigPortIsolationEntry;
            MEMCPY (&(pRemEntry->PortIsolation), (pEntry->pHwUpdPortIsolation),
                    sizeof (tIssHwUpdtPortIsolation));
            pRemoteHwNp->u4Opcode = ISS_HW_CONFIG_PORT_ISOLATION_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif
        default:
            break;
    }
    if (bRemPortCheck == FNP_TRUE)
    {
        if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
    }

    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * ISS module will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if (u4StackingModel == ISS_DISS_STACKING_MODEL)
    {
        if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            /* don't do anything */
        }
    }

    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilMergeLocalRemoteIsssysNpOutput             */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*               and Remote NP call execution                */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*              pRemoteHwNp Param of type tRemoteHwNp           */
/*              pi4LocalNpRetVal - Lcoal Np Return Value       */
/*              pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilMergeLocalRemoteIsssysNpOutput (tFsHwNp * pFsHwNp,
                                      tRemoteHwNp * pRemoteHwNp,
                                      INT4 *pi4LocalNpRetVal,
                                      INT4 *pi4RemoteNpRetVal)
{
    tHwPortInfo             HwPortInfo;
    tIsssysNpModInfo        *pIssSysNpModInfo = NULL;
    tIsssysRemoteNpModInfo  *pRemoteIssSysNpModInfo = NULL;
    
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    UNUSED_PARAM (pi4LocalNpRetVal);
    UNUSED_PARAM (pi4RemoteNpRetVal);

    pIssSysNpModInfo = &(pFsHwNp->IsssysNpModInfo);
    pRemoteIssSysNpModInfo = &(pRemoteHwNp->IsssysRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case ISS_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssHwUpdateL2Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemEntry = NULL;
            pEntry = &pIssSysNpModInfo->IsssysNpIssHwUpdateL2Filter;
            pRemEntry = &pRemoteIssSysNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            if (pEntry->i4Value == ISS_L2FILTER_STAT_GET)
            {
                pEntry->pIssL2FilterEntry->u4IssL2FilterMatchCount =
                    pEntry->pIssL2FilterEntry->u4IssL2FilterMatchCount +
                    pRemEntry->IssL2FilterEntry.u4IssL2FilterMatchCount;
            }
            break;
        }
        case ISS_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssHwUpdateL3Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemEntry = NULL;
            pEntry = &pIssSysNpModInfo->IsssysNpIssHwUpdateL3Filter;
            pRemEntry = &pRemoteIssSysNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            if (pEntry->i4Value == ISS_L3FILTER_STAT_GET)
            {
                pEntry->pIssL3FilterEntry->u4IssL3FilterMatchCount =
                    pEntry->pIssL3FilterEntry->u4IssL3FilterMatchCount +
                    pRemEntry->IssL3FilterEntry.u4IssL3FilterMatchCount;
            }


            break;
        }
        case ISS_HW_UPDATE_USER_DEFINED_FILTER:
        {
            tIsssysNpWrIssHwUpdateUserDefinedFilter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter *pRemEntry = NULL;
            pEntry = &pIssSysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;
            pRemEntry = &pRemoteIssSysNpModInfo->IsssysRemoteNpIssHwUpdateUserDefinedFilter;
            /* Merge the Output of the Remote Structure with
             * the Local Structure */
            if (pEntry->i4FilterAction == ISS_USERDEFINED_L2L3FILTER_STAT_GET)
            {
                pEntry->pAccessFilterEntry->u4IssUdbFilterMatchCount=
                    pEntry->pAccessFilterEntry->u4IssUdbFilterMatchCount +
                    pRemEntry->AccessFilterEntry.u4IssUdbFilterMatchCount;
            }
            break;
        }
        default:
            break;
    }

    return FNP_SUCCESS;
}


/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIsssysConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIsssysConvertLocalToRemoteNp (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysRemoteNpModInfo *pIsssysRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIsssysNpModInfo = &(pFsHwNp->IsssysNpModInfo);
    pIsssysRemoteNpModInfo = &(pRemoteHwNp->IsssysRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIsssysRemoteNpModInfo);

    switch (u4Opcode)
    {
        case ISS_HW_SET_PORT_EGRESS_STATUS:
        {
            tIsssysNpWrIssHwSetPortEgressStatus *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortEgressStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressStatus;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortEgressStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1EgressEnable = pLocalArgs->u1EgressEnable;
            break;
        }
        case ISS_HW_SET_PORT_STATS_COLLECTION:
        {
            tIsssysNpWrIssHwSetPortStatsCollection *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortStatsCollection *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortStatsCollection;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortStatsCollection;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1StatsEnable = pLocalArgs->u1StatsEnable;
            break;
        }
        case ISS_HW_SET_PORT_MODE:
        {
            tIsssysNpWrIssHwSetPortMode *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortMode *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMode;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMode;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_DUPLEX:
        {
            tIsssysNpWrIssHwSetPortDuplex *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortDuplex *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortDuplex;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortDuplex;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_SPEED:
        {
            tIsssysNpWrIssHwSetPortSpeed *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortSpeed *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortSpeed;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortSpeed;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL:
        {
            tIsssysNpWrIssHwSetPortFlowControl *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortFlowControl *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControl;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortFlowControl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1FlowCtrlEnable = pLocalArgs->u1FlowCtrlEnable;
            break;
        }
        case ISS_HW_SET_PORT_RENEGOTIATE:
        {
            tIsssysNpWrIssHwSetPortRenegotiate *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortRenegotiate *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortRenegotiate;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortRenegotiate;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ADDR:
        {
            tIsssysNpWrIssHwSetPortMaxMacAddr *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortMaxMacAddr *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAddr;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAddr;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ACTION:
        {
            tIsssysNpWrIssHwSetPortMaxMacAction *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortMaxMacAction *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAction;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAction;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SET_PORT_MIRRORING_STATUS:
        {
            tIsssysNpWrIssHwSetPortMirroringStatus *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortMirroringStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMirroringStatus;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortMirroringStatus;
            pRemoteArgs->i4MirrorStatus = pLocalArgs->i4MirrorStatus;
            break;
        }
        case ISS_HW_SET_MIRROR_TO_PORT:
        {
            tIsssysNpWrIssHwSetMirrorToPort *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetMirrorToPort *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetMirrorToPort;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirrorToPort;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case ISS_HW_CPU_MIRRORING:
        {
            tIsssysNpWrIssHwSetCpuMirroring *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetCpuMirroring *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpWrIssHwSetCpuMirroring;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpWrIssHwSetCpuMirroring;
            pRemoteArgs->u4IssCpuMirrorToPort = pLocalArgs->u4IssCpuMirrorToPort;
            pRemoteArgs->i4IssCpuMirrorType = pLocalArgs->i4IssCpuMirrorType;
            break;
        }

        case ISS_HW_SET_INGRESS_MIRRORING:
        {
            tIsssysNpWrIssHwSetIngressMirroring *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetIngressMirroring *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetIngressMirroring;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetIngressMirroring;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IngMirroring = pLocalArgs->i4IngMirroring;
            break;
        }
        case ISS_HW_SET_EGRESS_MIRRORING:
        {
            tIsssysNpWrIssHwSetEgressMirroring *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetEgressMirroring *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetEgressMirroring;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetEgressMirroring;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4EgrMirroring = pLocalArgs->i4EgrMirroring;
            break;
        }
        case ISS_HW_SET_RATE_LIMITING_VALUE:
        {
            tIsssysNpWrIssHwSetRateLimitingValue *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetRateLimitingValue *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetRateLimitingValue;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetRateLimitingValue;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1PacketType = pLocalArgs->u1PacketType;
            pRemoteArgs->i4RateLimitVal = pLocalArgs->i4RateLimitVal;
            break;
        }
        case ISS_HW_SET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwSetPortEgressPktRate *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortEgressPktRate *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressPktRate;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortEgressPktRate;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4PktRate = pLocalArgs->i4PktRate;
            pRemoteArgs->i4BurstRate = pLocalArgs->i4BurstRate;
            break;
        }
        case ISS_HW_GET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwGetPortEgressPktRate *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortEgressPktRate *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortEgressPktRate;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortEgressPktRate;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pi4PortPktRate != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortPktRate),
                        (pLocalArgs->pi4PortPktRate), sizeof (INT4));
            }
            if (pLocalArgs->pi4PortBurstRate != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortBurstRate),
                        (pLocalArgs->pi4PortBurstRate), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_RESTART_SYSTEM:
        {
            break;
        }
        case ISS_HW_INIT_FILTER:
        {
            break;
        }
        case ISS_HW_GET_PORT_DUPLEX:
        {
            tIsssysNpWrIssHwGetPortDuplex *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortDuplex *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortDuplex;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortDuplex;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4PortDuplexStatus != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortDuplexStatus),
                        (pLocalArgs->pi4PortDuplexStatus), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_GET_PORT_SPEED:
        {
            tIsssysNpWrIssHwGetPortSpeed *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortSpeed *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortSpeed;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortSpeed;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4PortSpeed != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortSpeed), (pLocalArgs->pi4PortSpeed),
                        sizeof (INT4));
            }
            break;
        }
        case ISS_HW_GET_PORT_FLOW_CONTROL:
        {
            tIsssysNpWrIssHwGetPortFlowControl *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortFlowControl *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortFlowControl;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortFlowControl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4PortFlowControl != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortFlowControl),
                        (pLocalArgs->pi4PortFlowControl), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysNpWrIssHwSetPortHOLBlockPrevention *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortHOLBlockPrevention *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwSetPortHOLBlockPrevention;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortHOLBlockPrevention;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysNpWrIssHwGetPortHOLBlockPrevention *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortHOLBlockPrevention *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwGetPortHOLBlockPrevention;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortHOLBlockPrevention;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4Value), (pLocalArgs->pi4Value),
                        sizeof (INT4));
            }
            break;
        }
        case ISS_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssHwUpdateL2Filter *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL2Filter;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;

            if (pLocalArgs->pIssL2FilterEntry != NULL)
            {
                IssCopyLocalToRemoteL2Filter(&pRemoteArgs->IssL2FilterEntry, 
                                             pLocalArgs->pIssL2FilterEntry);
            }

            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssHwUpdateL3Filter *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL3Filter;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

            if (pLocalArgs->pIssL3FilterEntry != NULL)
            {
                IssCopyLocalToRemoteL3Filter(&pRemoteArgs->IssL3FilterEntry,
                                             pLocalArgs->pIssL3FilterEntry);
            }
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_UPDATE_L4_S_FILTER:
        {
            tIsssysNpWrIssHwUpdateL4SFilter *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwUpdateL4SFilter *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL4SFilter;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL4SFilter;
            if (pLocalArgs->pIssL4SFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->IssL4SFilterEntry),
                        (pLocalArgs->pIssL4SFilterEntry),
                        sizeof (tIssL4SFilterEntry));
            }
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_SEND_BUFFER_TO_LINUX:
        {
            tIsssysNpWrIssHwSendBufferToLinux *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSendBufferToLinux *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSendBufferToLinux;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSendBufferToLinux;
            if (pLocalArgs->pBuffer != NULL)
            {
                MEMCPY (&(pRemoteArgs->buffer), (pLocalArgs->pBuffer),
                        sizeof (UINT1));
            }
            pRemoteArgs->u4Len = pLocalArgs->u4Len;
            pRemoteArgs->i4ImageType = pLocalArgs->i4ImageType;
            break;
        }
        case ISS_HW_GET_FAN_STATUS:
        {
            tIsssysNpWrIssHwGetFanStatus *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetFanStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetFanStatus;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetFanStatus;
            pRemoteArgs->u4FanIndex = pLocalArgs->u4FanIndex;
            if (pLocalArgs->pu4FanStatus != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FanStatus), (pLocalArgs->pu4FanStatus),
                        sizeof (UINT4));
            }
            break;
        }
        case ISS_HW_INIT_MIRR_DATA_BASE:
        case ISS_NP_HW_GET_CAPABILITIES:
        {
            break;
        }
        case ISS_HW_SET_MIRRORING:
        {
            tIsssysNpWrIssHwSetMirroring *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetMirroring *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetMirroring;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirroring;
            if (pLocalArgs->pMirrorInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MirrorInfo), (pLocalArgs->pMirrorInfo),
                        sizeof (tIssHwMirrorInfo));
            }
            break;
        }
        case ISS_HW_MIRROR_ADD_REMOVE_PORT:
        {
            tIsssysNpWrIssHwMirrorAddRemovePort *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwMirrorAddRemovePort *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwMirrorAddRemovePort;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwMirrorAddRemovePort;
            pRemoteArgs->u4SrcIndex = pLocalArgs->u4SrcIndex;
            pRemoteArgs->u4DestIndex = pLocalArgs->u4DestIndex;
            pRemoteArgs->u1Mode = pLocalArgs->u1Mode;
            pRemoteArgs->u1MirrCfg = pLocalArgs->u1MirrCfg;
            break;
        }
        case ISS_HW_SET_MAC_LEARNING_RATE_LIMIT:
        {
            tIsssysNpWrIssHwSetMacLearningRateLimit *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetMacLearningRateLimit *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwSetMacLearningRateLimit;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetMacLearningRateLimit;
            pRemoteArgs->i4IssMacLearnLimitVal =
                pLocalArgs->i4IssMacLearnLimitVal;
            break;
        }
        case ISS_HW_GET_LEARNED_MAC_ADDR_COUNT:
        {
            tIsssysNpWrIssHwGetLearnedMacAddrCount *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetLearnedMacAddrCount *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetLearnedMacAddrCount;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetLearnedMacAddrCount;
            if (pLocalArgs->pi4LearnedMacAddrCount != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4LearnedMacAddrCount),
                        (pLocalArgs->pi4LearnedMacAddrCount), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_UPDATE_USER_DEFINED_FILTER:
        {
            tIsssysNpWrIssHwUpdateUserDefinedFilter *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwUpdateUserDefinedFilter;
            if (pLocalArgs->pAccessFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->AccessFilterEntry),
                        (pLocalArgs->pAccessFilterEntry),
                        sizeof (tIssUDBFilterEntry));
            }
            if (pLocalArgs->pL2AccessFilterEntry != NULL)
            {
                IssCopyLocalToRemoteL2Filter(&pRemoteArgs->L2AccessFilterEntry,
                        pLocalArgs->pL2AccessFilterEntry);
            }
            if (pLocalArgs->pL3AccessFilterEntry != NULL)
            {
                IssCopyLocalToRemoteL3Filter(&pRemoteArgs->L3AccessFilterEntry,
                        pLocalArgs->pL3AccessFilterEntry);
            }
            pRemoteArgs->i4FilterAction = pLocalArgs->i4FilterAction;
            break;
        }
        case ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortAutoNegAdvtCapBits *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwSetPortAutoNegAdvtCapBits;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortAutoNegAdvtCapBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4MauCapBits != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4MauCapBits),
                        (pLocalArgs->pi4MauCapBits), sizeof (INT4));
            }
            if (pLocalArgs->pi4IssCapBits != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4IssCapBits),
                        (pLocalArgs->pi4IssCapBits), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortAutoNegAdvtCapBits *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwGetPortAutoNegAdvtCapBits;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortAutoNegAdvtCapBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case ISS_HW_SET_LEARNING_MODE:
        {
            tIsssysNpWrIssHwSetLearningMode *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetLearningMode *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetLearningMode;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Status = pLocalArgs->i4Status;
            break;
        }
        case ISS_HW_SET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysNpWrIssHwSetPortMdiOrMdixCap *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortMdiOrMdixCap *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMdiOrMdixCap;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMdiOrMdixCap;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4Value = pLocalArgs->i4Value;
            break;
        }
        case ISS_HW_GET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysNpWrIssHwGetPortMdiOrMdixCap *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwGetPortMdiOrMdixCap *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortMdiOrMdixCap;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortMdiOrMdixCap;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pi4PortStatus != NULL)
            {
                MEMCPY (&(pRemoteArgs->i4PortStatus),
                        (pLocalArgs->pi4PortStatus), sizeof (INT4));
            }
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL_RATE:
        {
            tIsssysNpWrIssHwSetPortFlowControlRate *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetPortFlowControlRate *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControlRate;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortFlowControlRate;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4PortMaxRate = pLocalArgs->i4PortMaxRate;
            pRemoteArgs->i4PortMinRate = pLocalArgs->i4PortMinRate;
            break;
        }
        case ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysNpWrIssHwConfigPortIsolationEntry *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwConfigPortIsolationEntry *pRemoteArgs = NULL;
            pLocalArgs =
                &pIsssysNpModInfo->IsssysNpIssHwConfigPortIsolationEntry;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwConfigPortIsolationEntry;
            if (pLocalArgs->pPortIsolation != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortIsolation),
                        (pLocalArgs->pPortIsolation),
                        sizeof (tIssHwUpdtPortIsolation));
            }
            break;
        }
        case NP_SET_TRACE:
        {
            tIsssysNpWrNpSetTrace *pLocalArgs = NULL;
            tIsssysRemoteNpWrNpSetTrace *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpNpSetTrace;
            pRemoteArgs = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTrace;
            pRemoteArgs->u1TraceModule = pLocalArgs->u1TraceModule;
            pRemoteArgs->u1TraceLevel = pLocalArgs->u1TraceLevel;
            break;
        }
        case NP_GET_TRACE:
        {
            tIsssysNpWrNpGetTrace *pLocalArgs = NULL;
            tIsssysRemoteNpWrNpGetTrace *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpNpGetTrace;
            pRemoteArgs = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTrace;
            if ((&(pLocalArgs->u1TraceModule) != NULL)
                && (pLocalArgs->pu1TraceLevel != NULL))
            {
                MEMCPY (&(pRemoteArgs->u1TraceLevel),
                        (pLocalArgs->pu1TraceLevel), sizeof (UINT1));
            }
            break;
        }
#ifdef NPAPI_WANTED
        case NP_SET_TRACE_LEVEL:
        {
            tIsssysNpWrNpSetTraceLevel *pLocalArgs = NULL;
            tIsssysRemoteNpWrNpSetTraceLevel *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpNpSetTraceLevel;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTraceLevel;
            pRemoteArgs->u2TraceLevel = pLocalArgs->u2TraceLevel;
            break;
        }
        case NP_GET_TRACE_LEVEL:
        {
            tIsssysNpWrNpGetTraceLevel *pLocalArgs = NULL;
            tIsssysRemoteNpWrNpGetTraceLevel *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpNpGetTraceLevel;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTraceLevel;
            if (pLocalArgs->pu2TraceLevel != NULL)
            {
                MEMCPY (&(pRemoteArgs->u2TraceLevel),
                        (pLocalArgs->pu2TraceLevel), sizeof (UINT2));
            }
            break;
        }
#endif
#ifdef MBSM_WANTED
        case ISS_MBSM_HW_CPU_MIRRORING:
        case ISS_MBSM_HW_SET_PORT_EGRESS_STATUS:
        case ISS_MBSM_HW_SET_PORT_STATS_COLLECTION:
        case ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS:
        case ISS_MBSM_HW_SET_MIRROR_TO_PORT:
        case ISS_MBSM_HW_UPDATE_L2_FILTER:
        case ISS_MBSM_HW_UPDATE_L3_FILTER:
        case ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY:
        case ISS_MBSM_HW_SET_MIRRORING:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIsssysConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIsssysConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp,
        tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysRemoteNpModInfo *pIsssysRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pIsssysNpModInfo = &(pFsHwNp->IsssysNpModInfo);
    pIsssysRemoteNpModInfo = &(pRemoteHwNp->IsssysRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case ISS_HW_SET_PORT_EGRESS_STATUS:
            {
                tIsssysNpWrIssHwSetPortEgressStatus *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortEgressStatus *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressStatus;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortEgressStatus;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->u1EgressEnable = pRemoteArgs->u1EgressEnable;
                break;
            }
        case ISS_HW_SET_PORT_STATS_COLLECTION:
            {
                tIsssysNpWrIssHwSetPortStatsCollection *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortStatsCollection *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortStatsCollection;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwSetPortStatsCollection;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->u1StatsEnable = pRemoteArgs->u1StatsEnable;
                break;
            }
        case ISS_HW_SET_PORT_MODE:
            {
                tIsssysNpWrIssHwSetPortMode *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortMode *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMode;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMode;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_DUPLEX:
            {
                tIsssysNpWrIssHwSetPortDuplex *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortDuplex *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortDuplex;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortDuplex;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_SPEED:
            {
                tIsssysNpWrIssHwSetPortSpeed *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortSpeed *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortSpeed;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortSpeed;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_FLOW_CONTROL:
            {
                tIsssysNpWrIssHwSetPortFlowControl *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortFlowControl *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControl;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortFlowControl;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->u1FlowCtrlEnable = pRemoteArgs->u1FlowCtrlEnable;
                break;
            }
        case ISS_HW_SET_PORT_RENEGOTIATE:
            {
                tIsssysNpWrIssHwSetPortRenegotiate *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortRenegotiate *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortRenegotiate;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortRenegotiate;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_MAX_MAC_ADDR:
            {
                tIsssysNpWrIssHwSetPortMaxMacAddr *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortMaxMacAddr *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAddr;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAddr;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_MAX_MAC_ACTION:
            {
                tIsssysNpWrIssHwSetPortMaxMacAction *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortMaxMacAction *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAction;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAction;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_SET_PORT_MIRRORING_STATUS:
            {
                tIsssysNpWrIssHwSetPortMirroringStatus *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortMirroringStatus *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMirroringStatus;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwSetPortMirroringStatus;
                pLocalArgs->i4MirrorStatus = pRemoteArgs->i4MirrorStatus;
                break;
            }
        case ISS_HW_SET_MIRROR_TO_PORT:
            {
                tIsssysNpWrIssHwSetMirrorToPort *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetMirrorToPort *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetMirrorToPort;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirrorToPort;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                break;
            }
        case ISS_HW_CPU_MIRRORING:
        {
            tIsssysNpWrIssHwSetCpuMirroring *pLocalArgs = NULL;
            tIsssysRemoteNpWrIssHwSetCpuMirroring *pRemoteArgs = NULL;
            pLocalArgs = &pIsssysNpModInfo->IsssysNpWrIssHwSetCpuMirroring;
            pRemoteArgs =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpWrIssHwSetCpuMirroring;
            pLocalArgs->u4IssCpuMirrorToPort = pRemoteArgs->u4IssCpuMirrorToPort;
            pLocalArgs->i4IssCpuMirrorType = pRemoteArgs->i4IssCpuMirrorType;
            break;
        }


        case ISS_HW_SET_INGRESS_MIRRORING:
            {
                tIsssysNpWrIssHwSetIngressMirroring *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetIngressMirroring *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetIngressMirroring;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetIngressMirroring;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4IngMirroring = pRemoteArgs->i4IngMirroring;
                break;
            }
        case ISS_HW_SET_EGRESS_MIRRORING:
            {
                tIsssysNpWrIssHwSetEgressMirroring *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetEgressMirroring *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetEgressMirroring;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetEgressMirroring;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4EgrMirroring = pRemoteArgs->i4EgrMirroring;
                break;
            }
        case ISS_HW_SET_RATE_LIMITING_VALUE:
            {
                tIsssysNpWrIssHwSetRateLimitingValue *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetRateLimitingValue *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetRateLimitingValue;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwSetRateLimitingValue;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->u1PacketType = pRemoteArgs->u1PacketType;
                pLocalArgs->i4RateLimitVal = pRemoteArgs->i4RateLimitVal;
                break;
            }
        case ISS_HW_SET_PORT_EGRESS_PKT_RATE:
            {
                tIsssysNpWrIssHwSetPortEgressPktRate *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortEgressPktRate *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressPktRate;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwSetPortEgressPktRate;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4PktRate = pRemoteArgs->i4PktRate;
                pLocalArgs->i4BurstRate = pRemoteArgs->i4BurstRate;
                break;
            }
        case ISS_HW_GET_PORT_EGRESS_PKT_RATE:
            {
                tIsssysNpWrIssHwGetPortEgressPktRate *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwGetPortEgressPktRate *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortEgressPktRate;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwGetPortEgressPktRate;
                pLocalArgs->u4Port = pRemoteArgs->u4Port;
                if (pLocalArgs->pi4PortPktRate != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4PortPktRate),
                            &(pRemoteArgs->i4PortPktRate), sizeof (INT4));
                }
                if (pLocalArgs->pi4PortBurstRate != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4PortBurstRate),
                            &(pRemoteArgs->i4PortBurstRate), sizeof (INT4));
                }
                break;
            }
        case ISS_HW_RESTART_SYSTEM:
            {
                break;
            }
        case ISS_HW_INIT_FILTER:
            {
                break;
            }
        case ISS_HW_GET_PORT_DUPLEX:
            {
                tIsssysNpWrIssHwGetPortDuplex *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwGetPortDuplex *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortDuplex;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortDuplex;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                if (pLocalArgs->pi4PortDuplexStatus != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4PortDuplexStatus),
                            &(pRemoteArgs->i4PortDuplexStatus), sizeof (INT4));
                }
                break;
            }
        case ISS_HW_GET_PORT_SPEED:
            {
                tIsssysNpWrIssHwGetPortSpeed *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwGetPortSpeed *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortSpeed;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortSpeed;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                if (pLocalArgs->pi4PortSpeed != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4PortSpeed), &(pRemoteArgs->i4PortSpeed),
                            sizeof (INT4));
                }
                break;
            }
        case ISS_HW_GET_PORT_FLOW_CONTROL:
            {
                tIsssysNpWrIssHwGetPortFlowControl *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwGetPortFlowControl *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortFlowControl;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortFlowControl;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                if (pLocalArgs->pi4PortFlowControl != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4PortFlowControl),
                            &(pRemoteArgs->i4PortFlowControl), sizeof (INT4));
                }
                break;
            }
        case ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION:
            {
                tIsssysNpWrIssHwSetPortHOLBlockPrevention *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwSetPortHOLBlockPrevention *pRemoteArgs = NULL;
                pLocalArgs =
                    &pIsssysNpModInfo->IsssysNpIssHwSetPortHOLBlockPrevention;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwSetPortHOLBlockPrevention;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION:
            {
                tIsssysNpWrIssHwGetPortHOLBlockPrevention *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwGetPortHOLBlockPrevention *pRemoteArgs = NULL;
                pLocalArgs =
                    &pIsssysNpModInfo->IsssysNpIssHwGetPortHOLBlockPrevention;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->
                    IsssysRemoteNpIssHwGetPortHOLBlockPrevention;
                pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
                if (pLocalArgs->pi4Value != NULL)
                {
                    MEMCPY ((pLocalArgs->pi4Value), &(pRemoteArgs->i4Value),
                            sizeof (INT4));
                }
                break;
            }
        case ISS_HW_UPDATE_L2_FILTER:
            {
                tIsssysNpWrIssHwUpdateL2Filter *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL2Filter;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;
                if (pLocalArgs->pIssL2FilterEntry != NULL)
                {
                    IssCopyRemoteToLocalL2Filter(pLocalArgs->pIssL2FilterEntry,
                                                 &pRemoteArgs->IssL2FilterEntry);
                }
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
        case ISS_HW_UPDATE_L3_FILTER:
            {
                tIsssysNpWrIssHwUpdateL3Filter *pLocalArgs = NULL;
                tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemoteArgs = NULL;
                pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL3Filter;
                pRemoteArgs =
                    &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

                if (pLocalArgs->pIssL3FilterEntry != NULL)
                {
                    IssCopyRemoteToLocalL3Filter(pLocalArgs->pIssL3FilterEntry,
                                                 &(pRemoteArgs->IssL3FilterEntry));
                }
                pLocalArgs->i4Value = pRemoteArgs->i4Value;
                break;
            }
    case ISS_HW_UPDATE_L4_S_FILTER:
    {
        tIsssysNpWrIssHwUpdateL4SFilter *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwUpdateL4SFilter *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwUpdateL4SFilter;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL4SFilter;
        if (pLocalArgs->pIssL4SFilterEntry != NULL)
        {
            MEMCPY ((pLocalArgs->pIssL4SFilterEntry),
                    &(pRemoteArgs->IssL4SFilterEntry),
                    sizeof (tIssL4SFilterEntry));
        }
        pLocalArgs->i4Value = pRemoteArgs->i4Value;
        break;
    }
    case ISS_HW_SEND_BUFFER_TO_LINUX:
    {
        tIsssysNpWrIssHwSendBufferToLinux *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSendBufferToLinux *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSendBufferToLinux;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSendBufferToLinux;
        if (pLocalArgs->pBuffer != NULL)
        {
            MEMCPY ((pLocalArgs->pBuffer), &(pRemoteArgs->buffer),
                    sizeof (UINT1));
        }
        pLocalArgs->u4Len = pRemoteArgs->u4Len;
        pLocalArgs->i4ImageType = pRemoteArgs->i4ImageType;
        break;
    }
    case ISS_HW_GET_FAN_STATUS:
    {
        tIsssysNpWrIssHwGetFanStatus *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwGetFanStatus *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetFanStatus;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetFanStatus;
        pLocalArgs->u4FanIndex = pRemoteArgs->u4FanIndex;
        if (pLocalArgs->pu4FanStatus != NULL)
        {
            MEMCPY ((pLocalArgs->pu4FanStatus), &(pRemoteArgs->u4FanStatus),
                    sizeof (UINT4));
        }
        break;
    }
    case ISS_HW_INIT_MIRR_DATA_BASE:
    {
        break;
    }
    case ISS_HW_SET_MIRRORING:
    {
        tIsssysNpWrIssHwSetMirroring *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetMirroring *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetMirroring;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirroring;
        if (pLocalArgs->pMirrorInfo != NULL)
        {
            MEMCPY ((pLocalArgs->pMirrorInfo), &(pRemoteArgs->MirrorInfo),
                    sizeof (tIssHwMirrorInfo));
        }
        break;
    }
    case ISS_HW_MIRROR_ADD_REMOVE_PORT:
    {
        tIsssysNpWrIssHwMirrorAddRemovePort *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwMirrorAddRemovePort *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwMirrorAddRemovePort;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwMirrorAddRemovePort;
        pLocalArgs->u4SrcIndex = pRemoteArgs->u4SrcIndex;
        pLocalArgs->u4DestIndex = pRemoteArgs->u4DestIndex;
        pLocalArgs->u1Mode = pRemoteArgs->u1Mode;
        pLocalArgs->u1MirrCfg = pRemoteArgs->u1MirrCfg;
        break;
    }
    case ISS_HW_SET_MAC_LEARNING_RATE_LIMIT:
    {
        tIsssysNpWrIssHwSetMacLearningRateLimit *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetMacLearningRateLimit *pRemoteArgs = NULL;
        pLocalArgs =
            &pIsssysNpModInfo->IsssysNpIssHwSetMacLearningRateLimit;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwSetMacLearningRateLimit;
        pLocalArgs->i4IssMacLearnLimitVal =
            pRemoteArgs->i4IssMacLearnLimitVal;
        break;
    }
    case ISS_HW_GET_LEARNED_MAC_ADDR_COUNT:
    {
        tIsssysNpWrIssHwGetLearnedMacAddrCount *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwGetLearnedMacAddrCount *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetLearnedMacAddrCount;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwGetLearnedMacAddrCount;
        if (pLocalArgs->pi4LearnedMacAddrCount != NULL)
        {
            MEMCPY ((pLocalArgs->pi4LearnedMacAddrCount),
                    &(pRemoteArgs->i4LearnedMacAddrCount), sizeof (INT4));
        }
        break;
    }
    case ISS_HW_UPDATE_USER_DEFINED_FILTER:
    {
        tIsssysNpWrIssHwUpdateUserDefinedFilter *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter *pRemoteArgs = NULL;
        pLocalArgs =
            &pIsssysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwUpdateUserDefinedFilter;
        if (pLocalArgs->pAccessFilterEntry != NULL)
        {
            MEMCPY ((pLocalArgs->pAccessFilterEntry),
                    &(pRemoteArgs->AccessFilterEntry),
                    sizeof (tIssUDBFilterEntry));
        }
        if (pLocalArgs->pL2AccessFilterEntry != NULL)
        {
                IssCopyRemoteToLocalL2Filter(pLocalArgs->pL2AccessFilterEntry,
                        &pRemoteArgs->L2AccessFilterEntry);
        }
        if (pLocalArgs->pL3AccessFilterEntry != NULL)
        {
                IssCopyRemoteToLocalL3Filter((pLocalArgs->pL3AccessFilterEntry),
                        &(pRemoteArgs->L3AccessFilterEntry));
        }
        pLocalArgs->i4FilterAction = pRemoteArgs->i4FilterAction;
        break;
    }
    case ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS:
    {
        tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetPortAutoNegAdvtCapBits *pRemoteArgs = NULL;
        pLocalArgs =
            &pIsssysNpModInfo->IsssysNpIssHwSetPortAutoNegAdvtCapBits;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwSetPortAutoNegAdvtCapBits;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        if (pLocalArgs->pi4MauCapBits != NULL)
        {
            MEMCPY ((pLocalArgs->pi4MauCapBits),
                    &(pRemoteArgs->i4MauCapBits), sizeof (INT4));
        }
        if (pLocalArgs->pi4IssCapBits != NULL)
        {
            MEMCPY ((pLocalArgs->pi4IssCapBits),
                    &(pRemoteArgs->i4IssCapBits), sizeof (INT4));
        }
        break;
    }
    case ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS:
    {
        tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwGetPortAutoNegAdvtCapBits *pRemoteArgs = NULL;
        pLocalArgs =
            &pIsssysNpModInfo->IsssysNpIssHwGetPortAutoNegAdvtCapBits;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwGetPortAutoNegAdvtCapBits;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        if (pLocalArgs->pElement != NULL)
        {
            MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                    sizeof (INT4));
        }
        break;
    }
    case ISS_HW_SET_LEARNING_MODE:
    {
        tIsssysNpWrIssHwSetLearningMode *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetLearningMode *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetLearningMode;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        pLocalArgs->i4Status = pRemoteArgs->i4Status;
        break;
    }
    case ISS_HW_SET_PORT_MDI_OR_MDIX_CAP:
    {
        tIsssysNpWrIssHwSetPortMdiOrMdixCap *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetPortMdiOrMdixCap *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortMdiOrMdixCap;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMdiOrMdixCap;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        pLocalArgs->i4Value = pRemoteArgs->i4Value;
        break;
    }
    case ISS_HW_GET_PORT_MDI_OR_MDIX_CAP:
    {
        tIsssysNpWrIssHwGetPortMdiOrMdixCap *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwGetPortMdiOrMdixCap *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwGetPortMdiOrMdixCap;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortMdiOrMdixCap;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        if (pLocalArgs->pi4PortStatus != NULL)
        {
            MEMCPY ((pLocalArgs->pi4PortStatus),
                    &(pRemoteArgs->i4PortStatus), sizeof (INT4));
        }
        break;
    }
    case ISS_HW_SET_PORT_FLOW_CONTROL_RATE:
    {
        tIsssysNpWrIssHwSetPortFlowControlRate *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwSetPortFlowControlRate *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControlRate;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwSetPortFlowControlRate;
        pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
        pLocalArgs->i4PortMaxRate = pRemoteArgs->i4PortMaxRate;
        pLocalArgs->i4PortMinRate = pRemoteArgs->i4PortMinRate;
        break;
    }
    case ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
    {
        tIsssysNpWrIssHwConfigPortIsolationEntry *pLocalArgs = NULL;
        tIsssysRemoteNpWrIssHwConfigPortIsolationEntry *pRemoteArgs = NULL;
        pLocalArgs =
            &pIsssysNpModInfo->IsssysNpIssHwConfigPortIsolationEntry;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->
            IsssysRemoteNpIssHwConfigPortIsolationEntry;
        if (pLocalArgs->pPortIsolation != NULL)
        {
            MEMCPY ((pLocalArgs->pPortIsolation),
                    &(pRemoteArgs->PortIsolation),
                    sizeof (tIssHwUpdtPortIsolation));
        }
        break;
    }
    case NP_SET_TRACE:
    {
        tIsssysNpWrNpSetTrace *pLocalArgs = NULL;
        tIsssysRemoteNpWrNpSetTrace *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpNpSetTrace;
        pRemoteArgs = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTrace;
        pLocalArgs->u1TraceModule = pRemoteArgs->u1TraceModule;
        pLocalArgs->u1TraceLevel = pRemoteArgs->u1TraceLevel;
        break;
    }
    case NP_GET_TRACE:
    {
        tIsssysNpWrNpGetTrace *pLocalArgs = NULL;
        tIsssysRemoteNpWrNpGetTrace *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpNpGetTrace;
        pRemoteArgs = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTrace;
        if ((&(pLocalArgs->u1TraceModule) != NULL)
                && (pLocalArgs->pu1TraceLevel != NULL))
        {
            MEMCPY ((pLocalArgs->pu1TraceLevel),
                    &(pRemoteArgs->u1TraceLevel), sizeof (UINT1));
        }
        break;
    }
#ifdef NPAPI_WANTED
    case NP_SET_TRACE_LEVEL:
    {
        tIsssysNpWrNpSetTraceLevel *pLocalArgs = NULL;
        tIsssysRemoteNpWrNpSetTraceLevel *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpNpSetTraceLevel;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTraceLevel;
        pLocalArgs->u2TraceLevel = pRemoteArgs->u2TraceLevel;
        break;
    }
    case NP_GET_TRACE_LEVEL:
    {
        tIsssysNpWrNpGetTraceLevel *pLocalArgs = NULL;
        tIsssysRemoteNpWrNpGetTraceLevel *pRemoteArgs = NULL;
        pLocalArgs = &pIsssysNpModInfo->IsssysNpNpGetTraceLevel;
        pRemoteArgs =
            &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTraceLevel;
        if (pLocalArgs->pu2TraceLevel != NULL)
        {
            MEMCPY ((pLocalArgs->pu2TraceLevel),
                    &(pRemoteArgs->u2TraceLevel), sizeof (UINT2));
        }
        break;
    }
#endif
#ifdef MBSM_WANTED
        case ISS_MBSM_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssMbsmHwUpdateL2Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL2Filter *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL2Filter;
            pRemEntry = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;
            if(pEntry->pIssL2FilterEntry != NULL)
            {
                IssCopyRemoteToLocalL2Filter(pEntry->pIssL2FilterEntry ,&pRemEntry->IssL2FilterEntry);
            }
            pEntry->i4Value = pEntry->i4Value;
            break;
        }
        case ISS_MBSM_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssMbsmHwUpdateL3Filter *pEntry = NULL;
            tIsssysRemoteNpWrIssHwUpdateL3Filter *pRemEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL3Filter;
            pRemEntry =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

            if(pEntry->pIssL3FilterEntry != NULL)
            {
                IssCopyRemoteToLocalL3Filter(pEntry->pIssL3FilterEntry ,&pRemEntry->IssL3FilterEntry);
            }
            pEntry->i4Value = pEntry->i4Value;
            break;
        }
    case ISS_MBSM_HW_CPU_MIRRORING:
    case ISS_MBSM_HW_SET_PORT_EGRESS_STATUS:
    case ISS_MBSM_HW_SET_PORT_STATS_COLLECTION:
    case ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS:
    case ISS_MBSM_HW_SET_MIRROR_TO_PORT:
    case ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY:
    case ISS_MBSM_HW_SET_MIRRORING:
    {
        break;
    }
#endif /* MBSM_WANTED */
    default:
    u1RetVal = FNP_FAILURE;
    break;

}                            /* switch */

return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIsssysRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIsssysNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIsssysRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                                tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIsssysRemoteNpModInfo *pIsssysRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }


    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIsssysRemoteNpModInfo = &(pRemoteHwNpInput->IsssysRemoteNpModInfo);

    switch (u4Opcode)
    {
        case ISS_HW_SET_PORT_EGRESS_STATUS:
        {
            tIsssysRemoteNpWrIssHwSetPortEgressStatus *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortEgressStatus;
            u1RetVal =
                IssHwSetPortEgressStatus (pInput->u4IfIndex,
                                          pInput->u1EgressEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_STATS_COLLECTION:
        {
            tIsssysRemoteNpWrIssHwSetPortStatsCollection *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortStatsCollection;
            u1RetVal =
                IssHwSetPortStatsCollection (pInput->u4IfIndex,
                                             pInput->u1StatsEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_MODE:
        {
            tIsssysRemoteNpWrIssHwSetPortMode *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMode;
            u1RetVal = IssHwSetPortMode (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_DUPLEX:
        {
            tIsssysRemoteNpWrIssHwSetPortDuplex *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortDuplex;
            u1RetVal = IssHwSetPortDuplex (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_SPEED:
        {
            tIsssysRemoteNpWrIssHwSetPortSpeed *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortSpeed;
            u1RetVal = IssHwSetPortSpeed (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL:
        {
            tIsssysRemoteNpWrIssHwSetPortFlowControl *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortFlowControl;
            u1RetVal =
                IssHwSetPortFlowControl (pInput->u4IfIndex,
                                         pInput->u1FlowCtrlEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_RENEGOTIATE:
        {
            tIsssysRemoteNpWrIssHwSetPortRenegotiate *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortRenegotiate;
            u1RetVal =
                IssHwSetPortRenegotiate (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ADDR:
        {
            tIsssysRemoteNpWrIssHwSetPortMaxMacAddr *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAddr;
            u1RetVal =
                IssHwSetPortMaxMacAddr (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ACTION:
        {
            tIsssysRemoteNpWrIssHwSetPortMaxMacAction *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMaxMacAction;
            u1RetVal =
                IssHwSetPortMaxMacAction (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_MIRRORING_STATUS:
        {
            tIsssysRemoteNpWrIssHwSetPortMirroringStatus *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortMirroringStatus;
            u1RetVal = IssHwSetPortMirroringStatus (pInput->i4MirrorStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_MIRROR_TO_PORT:
        {
            tIsssysRemoteNpWrIssHwSetMirrorToPort *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirrorToPort;
            u1RetVal = IssHwSetMirrorToPort (pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_CPU_MIRRORING:
        {
            tIsssysRemoteNpWrIssHwSetCpuMirroring *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpWrIssHwSetCpuMirroring;
            u1RetVal = (UINT1)IssHwSetCpuMirroring (pInput->i4IssCpuMirrorType,
                                         pInput->u4IssCpuMirrorToPort);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case ISS_HW_SET_INGRESS_MIRRORING:
        {
            tIsssysRemoteNpWrIssHwSetIngressMirroring *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetIngressMirroring;
            u1RetVal =
                IssHwSetIngressMirroring (pInput->u4IfIndex,
                                          pInput->i4IngMirroring);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_EGRESS_MIRRORING:
        {
            tIsssysRemoteNpWrIssHwSetEgressMirroring *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetEgressMirroring;
            u1RetVal =
                IssHwSetEgressMirroring (pInput->u4IfIndex,
                                         pInput->i4EgrMirroring);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_RATE_LIMITING_VALUE:
        {
            tIsssysRemoteNpWrIssHwSetRateLimitingValue *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetRateLimitingValue;
            u1RetVal =
                IssHwSetRateLimitingValue (pInput->u4IfIndex,
                                           pInput->u1PacketType,
                                           pInput->i4RateLimitVal);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysRemoteNpWrIssHwSetPortEgressPktRate *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortEgressPktRate;
            u1RetVal =
                IssHwSetPortEgressPktRate (pInput->u4IfIndex, pInput->i4PktRate,
                                           pInput->i4BurstRate);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysRemoteNpWrIssHwGetPortEgressPktRate *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortEgressPktRate;
            u1RetVal =
                IssHwGetPortEgressPktRate (pInput->u4Port,
                                           &(pInput->i4PortPktRate),
                                           &(pInput->i4PortBurstRate));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_RESTART_SYSTEM:
        {
            IssHwRestartSystem ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_INIT_FILTER:
        {
            u1RetVal = IssHwInitFilter ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_DUPLEX:
        {
            tIsssysRemoteNpWrIssHwGetPortDuplex *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortDuplex;
            u1RetVal =
                IssHwGetPortDuplex (pInput->u4IfIndex,
                                    &(pInput->i4PortDuplexStatus));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_SPEED:
        {
            tIsssysRemoteNpWrIssHwGetPortSpeed *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortSpeed;
            u1RetVal =
                IssHwGetPortSpeed (pInput->u4IfIndex, &(pInput->i4PortSpeed));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_FLOW_CONTROL:
        {
            tIsssysRemoteNpWrIssHwGetPortFlowControl *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortFlowControl;
            u1RetVal =
                IssHwGetPortFlowControl (pInput->u4IfIndex,
                                         &(pInput->i4PortFlowControl));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysRemoteNpWrIssHwSetPortHOLBlockPrevention *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortHOLBlockPrevention;
            u1RetVal =
                IssHwSetPortHOLBlockPrevention (pInput->u4IfIndex,
                                                pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysRemoteNpWrIssHwGetPortHOLBlockPrevention *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortHOLBlockPrevention;
            u1RetVal =
                IssHwGetPortHOLBlockPrevention (pInput->u4IfIndex,
                                                &(pInput->i4Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_UPDATE_L2_FILTER:
        {
            tIsssysRemoteNpWrIssHwUpdateL2Filter *pInput = NULL;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssFilterShadowTable IssFilterShadowInfo;

            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL2Filter;

            MEMSET (&(IssL2FilterEntry) ,0 ,sizeof(tIssL2FilterEntry));
            MEMSET (&(IssFilterShadowInfo), 0, sizeof(tIssFilterShadowTable));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssFilterShadowInfo);

            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry),
                                         &(pInput->IssL2FilterEntry)); 

            u1RetVal =
                IssHwUpdateL2Filter (&IssL2FilterEntry,
                                     pInput->i4Value);

            pInput->IssL2FilterEntry.u4IssL2FilterMatchCount = IssL2FilterEntry.u4IssL2FilterMatchCount;
            MEMCPY (&(pInput->IssL2FilterEntry.IssFilterShadowInfo),
                        IssL2FilterEntry.pIssFilterShadowInfo,
                         sizeof(tIssFilterShadowTable));

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));

            break;
        }
        case ISS_HW_UPDATE_L3_FILTER:
        {
            tIsssysRemoteNpWrIssHwUpdateL3Filter *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL3Filter;

            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssFilterShadowInfo;

            MEMSET (&(IssL3FilterEntry) ,0 ,sizeof(IssL3FilterEntry));
            MEMSET (&(IssFilterShadowInfo), 0, sizeof(tIssFilterShadowTable));

            IssL3FilterEntry.pIssFilterShadowInfo = &(IssFilterShadowInfo);

            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry),
                                         &(pInput->IssL3FilterEntry));
            
            u1RetVal =
                IssHwUpdateL3Filter (&(IssL3FilterEntry),
                        pInput->i4Value);
            pInput->IssL3FilterEntry.u4IssL3FilterMatchCount = IssL3FilterEntry.u4IssL3FilterMatchCount;
            MEMCPY(&(pInput->IssL3FilterEntry.IssFilterShadowInfo),
                       IssL3FilterEntry.pIssFilterShadowInfo,
                       sizeof(tIssFilterShadowTable));

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_UPDATE_L4_S_FILTER:
        {
            tIsssysRemoteNpWrIssHwUpdateL4SFilter *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwUpdateL4SFilter;
            u1RetVal =
                IssHwUpdateL4SFilter (&(pInput->IssL4SFilterEntry),
                                      pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SEND_BUFFER_TO_LINUX:
        {
            tIsssysRemoteNpWrIssHwSendBufferToLinux *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSendBufferToLinux;
            u1RetVal =
                IssHwSendBufferToLinux (&(pInput->buffer), pInput->u4Len,
                                        pInput->i4ImageType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_FAN_STATUS:
        {
            tIsssysRemoteNpWrIssHwGetFanStatus *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetFanStatus;
            u1RetVal =
                IssHwGetFanStatus (pInput->u4FanIndex, &(pInput->u4FanStatus));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_INIT_MIRR_DATA_BASE:
        {
            u1RetVal = IssHwInitMirrDataBase ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_MIRRORING:
        {
            tIsssysRemoteNpWrIssHwSetMirroring *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetMirroring;
            u1RetVal = IssHwSetMirroring (&(pInput->MirrorInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_MIRROR_ADD_REMOVE_PORT:
        {
            tIsssysRemoteNpWrIssHwMirrorAddRemovePort *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwMirrorAddRemovePort;
            u1RetVal =
                IssHwMirrorAddRemovePort (pInput->u4SrcIndex,
                                          pInput->u4DestIndex, pInput->u1Mode,
                                          pInput->u1MirrCfg);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_MAC_LEARNING_RATE_LIMIT:
        {
            tIsssysRemoteNpWrIssHwSetMacLearningRateLimit *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetMacLearningRateLimit;
            u1RetVal =
                IssHwSetMacLearningRateLimit (pInput->i4IssMacLearnLimitVal);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_LEARNED_MAC_ADDR_COUNT:
        {
            tIsssysRemoteNpWrIssHwGetLearnedMacAddrCount *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetLearnedMacAddrCount;
            u1RetVal =
                IssHwGetLearnedMacAddrCount (&(pInput->i4LearnedMacAddrCount));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_UPDATE_USER_DEFINED_FILTER:
        {
            tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter *pInput = NULL;
            tIssL2FilterEntry IssL2FilterEntry;
            tIssL3FilterEntry IssL3FilterEntry;
            tIssFilterShadowTable IssL2FilterShadowInfo;
            tIssFilterShadowTable IssL3FilterShadowInfo;

            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwUpdateUserDefinedFilter;

            MEMSET (&IssL2FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET (&IssL3FilterShadowInfo, 0, sizeof(tIssFilterShadowTable));
            MEMSET(&(IssL2FilterEntry), 0, sizeof(IssL2FilterEntry));
            MEMSET(&(IssL3FilterEntry), 0, sizeof(IssL3FilterEntry));

            IssL2FilterEntry.pIssFilterShadowInfo = &(IssL2FilterShadowInfo); 
            IssL3FilterEntry.pIssFilterShadowInfo = &(IssL3FilterShadowInfo);

            IssCopyRemoteToLocalL2Filter(&(IssL2FilterEntry), 
                                         &(pInput->L2AccessFilterEntry));
            IssCopyRemoteToLocalL3Filter(&(IssL3FilterEntry), 
                                         &(pInput->L3AccessFilterEntry));
            u1RetVal =
                IssHwUpdateUserDefinedFilter (&(pInput->AccessFilterEntry),
                                              &(IssL2FilterEntry),
                                              &(IssL3FilterEntry),
                                              pInput->i4FilterAction);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysRemoteNpWrIssHwSetPortAutoNegAdvtCapBits *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortAutoNegAdvtCapBits;
            u1RetVal =
                IssHwSetPortAutoNegAdvtCapBits (pInput->u4IfIndex,
                                                &(pInput->i4MauCapBits),
                                                &(pInput->i4IssCapBits));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysRemoteNpWrIssHwGetPortAutoNegAdvtCapBits *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwGetPortAutoNegAdvtCapBits;
            u1RetVal =
                IssHwGetPortAutoNegAdvtCapBits (pInput->u4IfIndex,
                                                &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_LEARNING_MODE:
        {
            tIsssysRemoteNpWrIssHwSetLearningMode *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetLearningMode;
            u1RetVal =
                IssHwSetLearningMode (pInput->u4IfIndex, pInput->i4Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysRemoteNpWrIssHwSetPortMdiOrMdixCap *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwSetPortMdiOrMdixCap;
            u1RetVal =
                IssHwSetPortMdiOrMdixCap (pInput->u4IfIndex, pInput->i4Value);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_GET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysRemoteNpWrIssHwGetPortMdiOrMdixCap *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->IsssysRemoteNpIssHwGetPortMdiOrMdixCap;
            u1RetVal =
                IssHwGetPortMdiOrMdixCap (pInput->u4IfIndex,
                                          &(pInput->i4PortStatus));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL_RATE:
        {
            tIsssysRemoteNpWrIssHwSetPortFlowControlRate *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwSetPortFlowControlRate;
            u1RetVal =
                IssHwSetPortFlowControlRate (pInput->u4IfIndex,
                                             pInput->i4PortMaxRate,
                                             pInput->i4PortMinRate);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysRemoteNpWrIssHwConfigPortIsolationEntry *pInput = NULL;
            pInput =
                &pIsssysRemoteNpModInfo->
                IsssysRemoteNpIssHwConfigPortIsolationEntry;
            u1RetVal = IssHwConfigPortIsolationEntry (&(pInput->PortIsolation));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case NP_SET_TRACE:
        {
            tIsssysRemoteNpWrNpSetTrace *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTrace;
            NpSetTrace (pInput->u1TraceModule, pInput->u1TraceLevel);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case NP_GET_TRACE:
        {
            tIsssysRemoteNpWrNpGetTrace *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTrace;
            NpGetTrace (pInput->u1TraceModule, &(pInput->u1TraceLevel));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef NPAPI_WANTED
        case NP_SET_TRACE_LEVEL:
        {
            tIsssysRemoteNpWrNpSetTraceLevel *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpSetTraceLevel;
            NpSetTraceLevel (pInput->u2TraceLevel);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case NP_GET_TRACE_LEVEL:
        {
            tIsssysRemoteNpWrNpGetTraceLevel *pInput = NULL;
            pInput = &pIsssysRemoteNpModInfo->IsssysRemoteNpNpGetTraceLevel;
            NpGetTraceLevel (&(pInput->u2TraceLevel));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __ISS_NPUTIL_C__ */
