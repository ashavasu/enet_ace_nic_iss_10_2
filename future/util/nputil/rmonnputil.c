/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonnputil.c,v 1.3 2014/02/27 12:35:27 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *              required for the remote NP Hardware Programming
 *              (Dual Unit Stacking Environment)
 *
 *******************************************************************/

#ifndef __RMONNPUTIL_C__
#define __RMONNPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskRmonNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef RMON_WANTED
VOID
NpUtilMaskRmonNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;

    pRmonNpModInfo = &(pFsHwNp->RmonNpModInfo);

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_RMON_HW_GET_ETH_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwGetEthStatsTable *pEntry = NULL;

            pEntry = &pRmonNpModInfo->RmonNpFsRmonHwGetEthStatsTable;
            if (NpUtilIsRemotePort (pEntry->u4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_RMON_HW_SET_ETHER_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwSetEtherStatsTable *pEntry = NULL;

            pEntry = &pRmonNpModInfo->RmonNpFsRmonHwSetEtherStatsTable;
            if (NpUtilIsRemotePort (pEntry->u4IfIndex, &HwPortInfo) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        default:
            break;
    }
    return;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;
    tRmonRemoteNpModInfo *pRmonRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pRmonNpModInfo = &(pFsHwNp->RmonNpModInfo);
    pRmonRemoteNpModInfo = &(pRemoteHwNp->RmonRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tRmonRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_RMON_HW_GET_ETH_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwGetEthStatsTable *pLocalArgs = NULL;
            tRmonRemoteNpWrFsRmonHwGetEthStatsTable *pRemoteArgs = NULL;
            pLocalArgs = &pRmonNpModInfo->RmonNpFsRmonHwGetEthStatsTable;
            pRemoteArgs =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwGetEthStatsTable;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->EthStatsEntry),
                    (pLocalArgs->pEthStatsEntry), sizeof (tRmonEtherStatsNode));
            break;
        }
        case FS_RMON_HW_SET_ETHER_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwSetEtherStatsTable *pLocalArgs = NULL;
            tRmonRemoteNpWrFsRmonHwSetEtherStatsTable *pRemoteArgs = NULL;
            pLocalArgs = &pRmonNpModInfo->RmonNpFsRmonHwSetEtherStatsTable;
            pRemoteArgs =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwSetEtherStatsTable;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1EtherStatsEnable = pLocalArgs->u1EtherStatsEnable;
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;
    tRmonRemoteNpModInfo *pRmonRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pRmonNpModInfo = &(pFsHwNp->RmonNpModInfo);
    pRmonRemoteNpModInfo = &(pRemoteHwNp->RmonRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_RMON_HW_GET_ETH_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwGetEthStatsTable *pLocalArgs = NULL;
            tRmonRemoteNpWrFsRmonHwGetEthStatsTable *pRemoteArgs = NULL;
            pLocalArgs = &pRmonNpModInfo->RmonNpFsRmonHwGetEthStatsTable;
            pRemoteArgs =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwGetEthStatsTable;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY ((pLocalArgs->pEthStatsEntry),
                    &(pRemoteArgs->EthStatsEntry),
                    sizeof (tRmonEtherStatsNode));
            break;
        }
        case FS_RMON_HW_SET_ETHER_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwSetEtherStatsTable *pLocalArgs = NULL;
            tRmonRemoteNpWrFsRmonHwSetEtherStatsTable *pRemoteArgs = NULL;
            pLocalArgs = &pRmonNpModInfo->RmonNpFsRmonHwSetEtherStatsTable;
            pRemoteArgs =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwSetEtherStatsTable;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1EtherStatsEnable = pRemoteArgs->u1EtherStatsEnable;
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonRemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tRmonNpModInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonRemoteNpModInfo *pRmonRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pRmonRemoteNpModInfo = &(pRemoteHwNpInput->RmonRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_RMON_HW_GET_ETH_STATS_TABLE:
        {
            tRmonRemoteNpWrFsRmonHwGetEthStatsTable *pInput = NULL;
            pInput =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwGetEthStatsTable;
            u1RetVal =
                FsRmonHwGetEthStatsTable (pInput->u4IfIndex,
                                          &(pInput->EthStatsEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMON_HW_SET_ETHER_STATS_TABLE:
        {
            tRmonRemoteNpWrFsRmonHwSetEtherStatsTable *pInput = NULL;
            pInput =
                &pRmonRemoteNpModInfo->RmonRemoteNpFsRmonHwSetEtherStatsTable;
            u1RetVal =
                FsRmonHwSetEtherStatsTable (pInput->u4IfIndex,
                                            pInput->u1EtherStatsEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* RMON_WANTED */
#endif /* __RMONNPUTIL_C__ */
