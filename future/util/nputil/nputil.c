/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: nputil.c,v 1.53 2018/02/02 09:47:36 siva Exp $
 *
 * Description:This file contains the single NP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __NPUTIL_C__
#define __NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"
#include "cfanp.h"
#include "custnp.h"
#include "msr.h"

static UINT4        gSequenceNum = 0;    /* Sequence Num to validate the Send/Rcv calls bet Act-Stby */
static UINT4        gu4BulkNpCount = 0;    /* Count of bulk NP calls that can be sent from active to Act-Stby with OSIX_NO_WAIT */
static UINT4        gu4StandbyRxSequenceNum = 0;    /* Sequence Num Last processed by the standby unit */
static UINT4        gu4StandbyRetStatus = 0;    /* NPAPI Execution status in standby unit */
static UINT1        gRetStatus = 0;    /* Return Status of the overall NP Client Programming */
static UINT1        gbSemRelease = OSIX_FALSE;
UINT1               gu1NpUtilIPCSyncStatus = OSIX_TRUE;
extern INT4         gi4MibResStatus;
/***************************************************************************/
/*  Function Name       : NpUtilHwProgram                                  */
/*                                                                         */
/*  Description         : This function executes the NP based on the       */
/*                       stacking model                                    */
/*                       if stacking model is ISS_CTRL_PLANE_STACKING_MODEL*/
/*                       then NP can be executed in Both Local And Remote  */
/*                       units based on the ports in the NP call           */
/*                       For all the other stacking models                 */
/*                       NP will be executed only in Local Unit            */
/*  Input(s)            : FsHwNpParam of type tfsHwNp                      */
/*                                                                         */
/*  Output(s)           : None                                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/***************************************************************************/

PUBLIC UINT1
NpUtilHwProgram (tFsHwNp * pFsHwNp)
{
    INT4                i4StackingModel = ISS_STACKING_MODEL_NONE;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgram : ENTRY \n");

    if (pFsHwNp == NULL)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilHwProgram: FsHwNp is Null \n");
        return FNP_FAILURE;
    }
    i4StackingModel = IssGetStackingModel ();

    if ((!(i4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)) &&
        (!(i4StackingModel == ISS_DISS_STACKING_MODEL)))
    {
        /* For Stacking model ISS_STACKING_MODEL_NONE and
         * ISS_DATA_PLANE_STACKING_MODEL no IPC is used for 
         * invoking the NP Calls in the remote unit
         * NP Calls will be invoked only in the local unit
         */
        NPUTIL_DBG (NPUTIL_DBG_MSG,
                    "Data plane stacking model is chosen and "
                    "NP calls are invoked only in the local unit \n");
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgram : EXIT \n");
        return (NpUtilHwProgramLocalUnit (pFsHwNp));
    }
    else
    {
        /* For Stacking model ISS_CTRL_PLANE_STACKING_MODEL execution of NP will be
         * done on LOCAL or REMOTE Or LocalAndRemote Units
         */
        if (NPUTIL_IS_NP_PROGRAMMING_ALLOWED () == NPUTIL_TRUE)
        {
            NPUTIL_DBG (NPUTIL_DBG_MSG,
                        "Control plane stacking model is chosen and NP "
                        "calls are executed in local or remote or LocalAndRemote units\n");
            NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgram : EXIT \n");
            return (NpUtilHwProgramLocalAndRemoteUnit (pFsHwNp));
        }
        else
        {
            /* RM NP calls has to be allowed in standby to program its node status */
            /* Since Reloading Stand-by cannot be achieved through IPC ,
               it is achieved through MIB object Sync-up .Hence NPAPI invocation
               is allowed for Restart NPAPI in stand-by system */
            if ((pFsHwNp->NpModuleId == NP_RM_MOD) ||
                ((pFsHwNp->NpModuleId == NP_ISSSYS_MOD) &&
                 (pFsHwNp->u4Opcode == ISS_HW_RESTART_SYSTEM)))
            {
                return (NpUtilHwProgramLocalAndRemoteUnit (pFsHwNp));
            }
#if defined (MBSM_WANTED) && defined (CFA_WANTED)
            else if ((pFsHwNp->NpModuleId == NP_CFA_MOD) &&
                     ((pFsHwNp->u4Opcode == FS_NP_CFA_MBSM_ARP_MODIFY_FILTER)
                      || (pFsHwNp->u4Opcode ==
                          FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER)))
            {
                return (NpUtilHwProgramLocalUnit (pFsHwNp));
            }
#endif

            else
            {
                NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgram : EXIT \n");
                return FNP_SUCCESS;
            }
        }
    }

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgram : EXIT \n");
}

/***************************************************************************/
/*  Function Name       : NpUtilHwProgramLocalUnit                         */
/*                                                                         */
/*  Description         : This function executes the NP Calls in the Local */
/*                        unit associated with this system by calling the  */
/*                        NP wrappers based on the Module ID               */
/*                                                                         */
/*  Input(s)            : FsHwNpParam of type tfsHwNp                      */
/*                                                                         */
/*  Output(s)           : None                                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/***************************************************************************/
PUBLIC UINT1
NpUtilHwProgramLocalUnit (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_FAILURE;
    UINT4               u4ModuleId;

    u4ModuleId = pFsHwNp->NpModuleId;
    NPUTIL_DBG2 (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgramLocalUnit: ENTRY "
                 "with module ID %d with Opcode %d\n", u4ModuleId,
                 pFsHwNp->u4Opcode);

    switch (u4ModuleId)
    {
        case NP_VLAN_MOD:
        {
#ifdef VLAN_WANTED
            u1RetVal = VlanNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_MSTP_MOD:
        {
#ifdef MSTP_WANTED
            u1RetVal = MstpNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_RSTP_MOD:
        {
#ifdef RSTP_WANTED
            u1RetVal = RstpNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_PVRST_MOD:
        {
#ifdef PVRST_WANTED
            u1RetVal = PvrstNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_LA_MOD:
        {
#ifdef LA_WANTED
            u1RetVal = LaNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_PNAC_MOD:
        {
#ifdef PNAC_WANTED
            u1RetVal = PnacNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_VCM_MOD:
        {
#ifdef VCM_WANTED
            u1RetVal = VcmNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_RMON_MOD:
        {
#ifdef RMON_WANTED
            u1RetVal = RmonNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_RMONv2_MOD:
        {
#ifdef RMON2_WANTED
            u1RetVal = Rmonv2NpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IGS_MOD:
        {
#ifdef IGS_WANTED
            u1RetVal = IgsNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_MLDS_MOD:
        {
#ifdef MLDS_WANTED
            u1RetVal = MldsNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_DSMON_MOD:
        {
#ifdef DSMON_WANTED
            u1RetVal = DsmonNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_CFA_MOD:
        {
#ifdef CFA_WANTED
            u1RetVal = CfaNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_EOAM_MOD:
        {
#ifdef EOAM_WANTED
            u1RetVal = EoamNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_EOAMFM_MOD:
        {
#ifdef EOAM_FM_WANTED
            u1RetVal = EoamfmNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_ISSSYS_MOD:
        {
#ifdef ISS_WANTED
            u1RetVal = IsssysNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_ELPS_MOD:
        {
#ifdef ELPS_WANTED
            u1RetVal = ElpsNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_ECFM_MOD:
        {
#ifdef ECFM_WANTED
            u1RetVal = EcfmNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_ERPS_MOD:
        {
#ifdef ERPS_WANTED
            u1RetVal = ErpsNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_QOSX_MOD:
        {
#ifdef QOSX_WANTED
            u1RetVal = QosxNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_RM_MOD:
        {
#ifdef RM_WANTED
            u1RetVal = RmNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_MBSM_MOD:
        {
#ifdef MBSM_WANTED
            u1RetVal = MbsmNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_SYNCE_MOD:
        {
#ifdef SYNCE_WANTED
            u1RetVal = SynceNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IP_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            u1RetVal = IpNpWrHwProgram (pFsHwNp);
#endif
            break;
        }

#ifdef WLC_WANTED
        case NP_CAPWAP_MODULE:
        {
#ifdef CAPWAP_WANTED
            u1RetVal = CapwapNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_WLAN_MODULE:
        {
#ifdef WSSWLAN_WANTED
            u1RetVal = WlanNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_WLAN_CLIENT_MODULE:
        {
#ifdef WSSSTA_WANTED
            u1RetVal = WlanClientNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
#endif
#ifdef WTP_WANTED
        case NP_RADIO_MODULE:
        {
#ifdef RADIOIF_WANTED
            u1RetVal = RadioNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
#endif

        case NP_OFC_MOD:
        {
#ifdef OPENFLOW_WANTED
            u1RetVal = OfcNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            u1RetVal = Ipv6NpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_MPLS_MOD:
        {
#ifdef MPLS_WANTED
            u1RetVal = MplsNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IGMP_MOD:
        {
#ifdef IGMP_WANTED
            u1RetVal = IgmpNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            u1RetVal = IpmcNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_MLD_MOD:
        {
#ifdef MLD_WANTED
            u1RetVal = MldNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED)
            u1RetVal = Ip6mcNpWrHwProgram (pFsHwNp);
#endif
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED
            u1RetVal = FsbNpWrHwProgram (pFsHwNp);
#endif /* FSB_WANTED */
            break;
        }
        default:
            break;
    }
    NPUTIL_DBG1 (NPUTIL_DBG_MSG,
                 "NpUtilHwProgramLocalUnit: EXIT with return value %d \n",
                 u1RetVal);
    return u1RetVal;
}

/***************************************************************************/
/*  Function Name       : NpUtilHwProgramLocalAndRemoteUnit                 */
/*                                                                         */
/*  Description         : This function validates whether any Local and    */
/*                        remote ports are present and takes the following */
/*                        action,                                          */
/*                        if only Local Ports present - NP is executed     */
/*                           only in Local unit                            */
/*                        if only remote Ports present - NP is executed    */
/*                           only in remote unit                           */
/*                        if both Local and remote ports are present - NP  */
/*                           is executed in both Local and remote units    */
/*                                                                         */
/*  Input(s)            : pFsHwNp                                          */
/*                                                                         */
/*  Output(s)           : pRemoteHwNp                                      */
/*                      : pu1NpCallStatus                                  */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/***************************************************************************/

PUBLIC UINT1
NpUtilHwProgramLocalAndRemoteUnit (tFsHwNp * pFsHwNp)
{
    UINT4               u4RpcCallStatus = OSIX_WAIT;
    tRemoteHwNp        *pRemoteHwNp = NULL;
    UINT1               u1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    UINT1               u1IPCSyncStatus = OSIX_TRUE;
    INT4                i4LocalNpRetVal = FNP_SUCCESS;
    INT4                i4RemoteNpRetVal = FNP_SUCCESS;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilHwProgramLocalAndRemoteUnit: ENTRY \n");
    /* When Control reaches here, the Stacking model is 
     * ISS_CTRL_PLANE_STACKING_MODEL and IPC is used for 
     * invoking the NP Calls in the remote unit
     * Allocate Memory for the pRemoteHwNp structure 
     */
    NPUTIL_DBG2 (NPUTIL_CTRL_PATH_TRC, "NpUtilHwProgramLocalAndRemoteUnit "
                 "with module ID %d with Opcode %d\n", pFsHwNp->NpModuleId,
                 pFsHwNp->u4Opcode);

    /* Acquire the Remote NP Lock
     */
    NpUtilRemoteNpLock ();

    pRemoteHwNp = NpUtilAllocRemoteHwNpMem ();
    if (pRemoteHwNp == NULL)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilHwProgramLocalAndRemoteUnit: RemoteHwNp is Null \n");
        NpUtilRemoteNpUnLock ();
        return FNP_FAILURE;
    }

    /* Convert any pointers present in the structure and populate the 
     * RemoteHwNp structure
     */
    if (NpUtilConvertLocalArgsToRemoteArgs (pFsHwNp, pRemoteHwNp) ==
        FNP_FAILURE)
    {
        NpUtilRelRemoteHwNpMem (pRemoteHwNp);
        NpUtilRemoteNpUnLock ();
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "Population of RemoteHwNp structure failed \n");
        return FNP_FAILURE;
    }
    /* Split the ports to Active Node ports and Remote Node Ports
     * pFsHwNp -- will have only active ports
     * pRemoteHwNp -- will have only standby ports
     * If Only Remote Node Ports are present then pu1NpCallStatus 
     * will be set as NPUTIL_INVOKE_REMOTE_NP
     * if Active Node Ports are present then pu1NpCallStatus
     * will be set as NPUTIL_INVOKE_LOCAL_NP
     * if both Active and Standby Node Ports are present then pu1NpCallStatus
     * will be set as NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP
     */
    if (NpUtilCheckRemoteIPCStatus (pFsHwNp, pRemoteHwNp, &u1NpCallStatus,
                                    (INT4 *) &u4RpcCallStatus) == FNP_FAILURE)
    {
        NpUtilRelRemoteHwNpMem (pRemoteHwNp);
        NpUtilRemoteNpUnLock ();
        NPUTIL_DBG2 (NPUTIL_DBG_ERR, "NpUtilCheckRemoteIPCStatus failed."
                     " NpCallStatus value is %d. RpcCallStatus value is %u \n",
                     u1NpCallStatus, u4RpcCallStatus);
        return FNP_FAILURE;
    }
    if (gi4MibResStatus == MIB_RESTORE_IN_PROGRESS)
    {
        if (u1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilHwProgramLocalAndRemoteUnit: NpUtilCall to remote "
                        "is not allowed as restoration is in progress \n");
            u1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (u1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilHwProgramLocalAndRemoteUnit: NpUtilCall to remote "
                        "is not allowed & only performed on local "
                        "as restoration is in progress \n");
            u1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
    }

    if (u1NpCallStatus == NPUTIL_NO_NP_INVOKE)
    {
        NpUtilRelRemoteHwNpMem (pRemoteHwNp);
        NpUtilRemoteNpUnLock ();
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilHwProgramLocalAndRemoteUnit: NpUtilCall status is no NP invoke\n");
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                    "NpUtilHwProgramLocalAndRemoteUnit: EXIT\n");
        return ((UINT1) i4LocalNpRetVal);
    }

    /* Check whether IPC call Invocation is allowed for NPAPI
       execution in Stand-by . */

    /* Currently it is disabled when Active node is reloaded */
    NpUtilGetIPCSyncStatus (&u1IPCSyncStatus);
    if (u1IPCSyncStatus == OSIX_FALSE)
    {
        /* When IPC Sync up is disabled , only Local invocation is allowed */
        if (u1NpCallStatus != NPUTIL_INVOKE_LOCAL_NP)
        {
            NpUtilRelRemoteHwNpMem (pRemoteHwNp);
            NpUtilRemoteNpUnLock ();
            return i4LocalNpRetVal;
        }
    }
    if ((u1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
        (u1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
    {
        i4LocalNpRetVal = NpUtilHwProgramLocalUnit (pFsHwNp);
    }
    if ((u1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
        (u1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
    {
        /* NpUtilRemoteClntHwProgram constructs the IPC message and sends out to remote
         * Output from Remote Unit will be copied back to the pRemoteHwNp
         */

        u4RpcCallStatus =
            NpUtilIPCWaitStatus (pFsHwNp->NpModuleId, pFsHwNp->u4Opcode);
        if (CfaGetPeerNodeCount () != FNP_ZERO)
        {
            i4RemoteNpRetVal =
                NpUtilRemoteClntHwProgram (pRemoteHwNp, u4RpcCallStatus);
        }
        else
        {
            if (u1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                NpUtilRelRemoteHwNpMem (pRemoteHwNp);
                NpUtilRemoteNpUnLock ();
                NPUTIL_DBG (NPUTIL_DBG_MSG,
                            "Call Status is invoke both local and remote NP\n");
                NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                            "NpUtilHwProgramLocalAndRemoteUnit: EXIT\n");
                return (UINT1) i4LocalNpRetVal;
            }
            NpUtilRelRemoteHwNpMem (pRemoteHwNp);
            NpUtilRemoteNpUnLock ();
            NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                        "NpUtilHwProgramLocalAndRemoteUnit: EXIT\n");
            return (UINT1) i4RemoteNpRetVal;

        }
    }

    if ((u1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        && (i4RemoteNpRetVal == FNP_SUCCESS))
    {
        /* NPUTIL_INVOKE_REMOTE_NP -- The NP is executed at remote unit,copy the 
         *                            output from the remote(pRemoteHwNp) to pFsHwNp 
         */
        NpUtilConvertRemoteArgsToLocalArgs (pRemoteHwNp, pFsHwNp);
    }

    if ((u1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP) &&
        (i4RemoteNpRetVal == FNP_SUCCESS))
    {
        /* NPUTIL_INVOKE_REMOTE_NP -- The NP is executed at both Local and remote units, 
         *                            Merge the output of Local and Remote Units to pFsHwNp
         */
        NpUtilMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp, &i4LocalNpRetVal,
                                        &i4RemoteNpRetVal);
    }

    if ((i4RemoteNpRetVal == FNP_FAILURE) || (i4LocalNpRetVal == FNP_FAILURE))
    {
        NpUtilRelRemoteHwNpMem (pRemoteHwNp);
        NpUtilRemoteNpUnLock ();
        NPUTIL_DBG2 (NPUTIL_DBG_ERR,
                     "Local and Remote Np return value failed. "
                     "RemoteNpRetVal is %d, LocalNpRetVal is %d \n",
                     i4RemoteNpRetVal, i4LocalNpRetVal);
        return FNP_FAILURE;
    }

    if (i4LocalNpRetVal == FNP_NOT_SUPPORTED)
    {
        NpUtilRelRemoteHwNpMem (pRemoteHwNp);
        NpUtilRemoteNpUnLock ();
        NPUTIL_DBG2 (NPUTIL_DBG_ERR,
                     "Local and Remote Np return value not supported. "
                     "RemoteNpRetVal is %d, LocalNpRetVal is %d \n",
                     i4RemoteNpRetVal, i4LocalNpRetVal);
        return FNP_NOT_SUPPORTED;
    }

    NpUtilRelRemoteHwNpMem (pRemoteHwNp);
    NpUtilRemoteNpUnLock ();
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilHwProgramLocalAndRemoteUnit: EXIT\n");
    return FNP_SUCCESS;
}

/***************************************************************************/
/*  Function Name       : NpUtilMergeLocalRemoteNPOutput                   */
/*                                                                         */
/*  Description         :  The NP is executed at both Local and            */
/*                  remote units, Merge the output of Local         */
/*                  and Remote Units to pFsHwNp                     */
/*                                                                         */
/*  Input(s)            : pFsHwNp, pRemoteHwNp,                            */
/*                pi4LocalNpRetVal,pi4RemoteNpRetVal              */
/*                                                                         */
/*  Output(s)           : pFsHwNp, pi4LocalNpRetVal,pi4RemoteNpRetVal      */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/***************************************************************************/

UINT1
NpUtilMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                                INT4 *pi4LocalNpRetVal, INT4 *pi4RemoteNpRetVal)
{

    UINT1               u1RetVal = FNP_FAILURE;
    NPUTIL_DBG1 (NPUTIL_CTRL_PATH_TRC,
                 "NpUtilMergeLocalRemoteNPOutput: ENTRY with module id %d\n",
                 pFsHwNp->NpModuleId);

    switch (pFsHwNp->NpModuleId)
    {
        case NP_VLAN_MOD:
#ifdef VLAN_WANTED
            u1RetVal =
                NpUtilVlanMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                    pi4LocalNpRetVal,
                                                    pi4RemoteNpRetVal);
#endif
            break;
        case NP_ISSSYS_MOD:
        {
#ifdef ISS_WANTED
            u1RetVal = NpUtilMergeLocalRemoteIsssysNpOutput (pFsHwNp,
                                                             pRemoteHwNp,
                                                             pi4LocalNpRetVal,
                                                             pi4RemoteNpRetVal);
#endif
            break;
        }
        case NP_MSTP_MOD:
        case NP_RSTP_MOD:
        case NP_PVRST_MOD:
        case NP_LA_MOD:
        case NP_PNAC_MOD:
        case NP_VCM_MOD:
        case NP_RMON_MOD:
        case NP_RMONv2_MOD:
        case NP_MLDS_MOD:
        case NP_DSMON_MOD:
        case NP_EOAM_MOD:
        case NP_EOAMFM_MOD:
        case NP_ELPS_MOD:
        case NP_ECFM_MOD:
        case NP_ERPS_MOD:
        case NP_QOSX_MOD:
        case NP_RM_MOD:
        case NP_MBSM_MOD:
        case NP_SYNCE_MOD:
        case NP_OFC_MOD:
        case NP_MAX_MOD:
            break;
        case NP_CFA_MOD:
        {
#ifdef CFA_WANTED
            u1RetVal =
                NpUtilCfaMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                   pi4LocalNpRetVal,
                                                   pi4RemoteNpRetVal);
#endif /* CFA_WANTED */
            break;
        }
        case NP_MPLS_MOD:
        {
#ifdef MPLS_WANTED
            u1RetVal =
                NpUtilMplsMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                    pi4LocalNpRetVal,
                                                    pi4RemoteNpRetVal);
#endif /* (MPLS_WANTED) */
            break;
        }
        case NP_IP_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            u1RetVal =
                NpUtilIpMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                  pi4LocalNpRetVal,
                                                  pi4RemoteNpRetVal);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
            break;
        }
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            u1RetVal =
                NpUtilIpv6MergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                    pi4LocalNpRetVal,
                                                    pi4RemoteNpRetVal);
#endif /* (IP6_WANTED) || (LNXIP6_WANTED) */
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            u1RetVal =
                NpUtilIpmcMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                    pi4LocalNpRetVal,
                                                    pi4RemoteNpRetVal);
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED */
            break;
        }
        case NP_IGS_MOD:
        {
#ifdef IGS_WANTED

            u1RetVal =
                NpUtilIgsMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                   pi4LocalNpRetVal,
                                                   pi4RemoteNpRetVal);
#endif
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED)
            u1RetVal =
                NpUtilIp6mcMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                     pi4LocalNpRetVal,
                                                     pi4RemoteNpRetVal);
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED
            u1RetVal = NpUtilFsbMergeLocalRemoteNPOutput (pFsHwNp, pRemoteHwNp,
                                                          pi4LocalNpRetVal,
                                                          pi4RemoteNpRetVal);
#endif /* FSB_WANTED */
            break;
        }
        default:
            UNUSED_PARAM (pRemoteHwNp);
            UNUSED_PARAM (pi4LocalNpRetVal);
            UNUSED_PARAM (pi4RemoteNpRetVal);
            break;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMergeLocalRemoteNPOutput: EXIT\n");
    return (u1RetVal);
}

/***************************************************************************/
/*  Function Name       : NpUtilCheckRemoteIPCStatus                       */
/*                                                                         */
/*  Description         : This function validates spilts port information  */
/*                        in pFsHwNp to active and standby ports           */
/*                        active ports updated back into the pFsHwNp       */
/*                        standby ports are updated back into pRemoteHwNp  */
/*                        if the NP Call needs to be executed in           */
/*                        asynchronous mode then if the NP Call needs to   */
/*                        then pi4RpcCallStatus is set as OSIX_NOWAIT      */
/*                                                                         */
/*  Input(s)            : pFsHwNp                                          */
/*                                                                         */
/*  Output(s)           : pRemoteHwNp                                      */
/*                      : pu1NpCallStatus                                  */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/***************************************************************************/

INT4
NpUtilCheckRemoteIPCStatus (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                            UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{

    INT4                i4RetVal = FNP_SUCCESS;
    NPUTIL_DBG1 (NPUTIL_CTRL_PATH_TRC,
                 "NpUtilCheckRemoteIPCStatus: ENTRY with module ID %d\n",
                 pFsHwNp->NpModuleId);

    /* Do not invoke the NP calls for which implementation support is not there
     * in the NPAPI layer. 
     */
    if (OSIX_FAILURE ==
        NpUtilIsNpSupported (pFsHwNp->NpModuleId, pFsHwNp->u4Opcode))
    {
        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        return FNP_SUCCESS;
    }

    switch (pFsHwNp->NpModuleId)
    {
            /* Set Calls with u4IfIndex as input */
        case NP_CFA_MOD:
#ifdef CFA_WANTED
            NpUtilMaskCfaNpPorts (pFsHwNp, pRemoteHwNp,
                                  pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_RSTP_MOD:
#ifdef RSTP_WANTED
            NpUtilMaskRstpNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_MSTP_MOD:
#ifdef MSTP_WANTED
            NpUtilMaskMstpNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_PVRST_MOD:
#ifdef PVRST_WANTED
            NpUtilMaskPvrstNpPorts (pFsHwNp, pRemoteHwNp,
                                    pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_VLAN_MOD:
#ifdef VLAN_WANTED
            i4RetVal = NpUtilMaskVlanNpPorts (pFsHwNp, pRemoteHwNp,
                                              pu1NpCallStatus,
                                              pi4RpcCallStatus);
#endif
            break;
        case NP_VCM_MOD:
#ifdef VCM_WANTED
            NpUtilMaskVcmNpPorts (pFsHwNp, pRemoteHwNp,
                                  pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_PNAC_MOD:
#ifdef PNAC_WANTED
            NpUtilMaskPnacNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_EOAM_MOD:
#ifdef EOAM_WANTED
            NpUtilMaskEoamNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_EOAMFM_MOD:
#ifdef EOAM_FM_WANTED
            NpUtilMaskEoamFmNpPorts (pFsHwNp, pRemoteHwNp,
                                     pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_IGS_MOD:
#ifdef IGS_WANTED
            NpUtilMaskIgsNpPorts (pFsHwNp, pRemoteHwNp,
                                  pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_MLDS_MOD:
#ifdef MLDS_WANTED
            NpUtilMaskMldsNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
        case NP_ISSSYS_MOD:
#ifdef ISS_WANTED
            NpUtilMaskIsssysNpPorts (pFsHwNp, pRemoteHwNp,
                                     pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_LA_MOD:
#ifdef LA_WANTED
            NpUtilMaskLaNpPorts (pFsHwNp, pRemoteHwNp,
                                 pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_QOSX_MOD:
#ifdef QOSX_WANTED
            NpUtilMaskQosxNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_ELPS_MOD:
#ifdef ELPS_WANTED
            NpUtilMaskElpsNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_RM_MOD:
#ifdef RM_WANTED
            NpUtilMaskRmNpPorts (pFsHwNp, pRemoteHwNp,
                                 pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_ERPS_MOD:
#ifdef ERPS_WANTED
            NpUtilMaskErpsNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_MBSM_MOD:
#ifdef MBSM_WANTED
            NpUtilMaskMbsmNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_RMON_MOD:
#ifdef RMON_WANTED
            NpUtilMaskRmonNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_RMONv2_MOD:
#ifdef RMON2_WANTED
            NpUtilMaskRmonv2NpPorts (pFsHwNp, pRemoteHwNp,
                                     pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_DSMON_MOD:
#ifdef DSMON_WANTED
            NpUtilMaskDsmonNpPorts (pFsHwNp, pRemoteHwNp,
                                    pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_SYNCE_MOD:
#ifdef SYNCE_WANTED
            NpUtilMaskSynceNpPorts (pFsHwNp, pRemoteHwNp,
                                    pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_IP_MOD:
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            NpUtilMaskIpNpPorts (pFsHwNp, pRemoteHwNp,
                                 pu1NpCallStatus, pi4RpcCallStatus);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
            break;
        case NP_ECFM_MOD:
#ifdef ECFM_WANTED
            NpUtilMaskEcfmNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            NpUtilMaskIpv6NpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif /* (IP6_WANTED) || (LNXIP6_WANTED) */
            break;
        }
        case NP_MPLS_MOD:
#ifdef MPLS_WANTED
            NpUtilMaskMplsNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif
            break;
        case NP_IGMP_MOD:
        {
#if defined (IGMP_WANTED)
            NpUtilMaskIgmpNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif /* (IGMP_WANTED) */
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            NpUtilMaskIpmcNpPorts (pFsHwNp, pRemoteHwNp,
                                   pu1NpCallStatus, pi4RpcCallStatus);
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED */
            break;
        }
        case NP_MLD_MOD:
        {
#if defined (MLD_WANTED)
            NpUtilMaskMldNpPorts (pFsHwNp, pRemoteHwNp,
                                  pu1NpCallStatus, pi4RpcCallStatus);
#endif /* (MLD_WANTED) */
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED)
            NpUtilMaskIp6mcNpPorts (pFsHwNp, pRemoteHwNp,
                                    pu1NpCallStatus, pi4RpcCallStatus);
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED
            NpUtilMaskFsbNpPorts (pFsHwNp, pRemoteHwNp,
                                  pu1NpCallStatus, pi4RpcCallStatus);
#endif /* FSB_WANTED */
            break;
        }
        default:
            UNUSED_PARAM (pRemoteHwNp);
            UNUSED_PARAM (pi4RpcCallStatus);
            break;
    }
    if (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL)
    {
        if (CfaGetRetrieveNodeState () == RM_STANDBY)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
        }
        else if (CfaGetRetrieveNodeState () == RM_INIT)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
    }
#ifdef RM_WANTED
    else if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        if (RmRetrieveNodeState () != RM_ACTIVE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }

    }
#endif

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilCheckRemoteIPCStatus: EXIT\n");
    return i4RetVal;
}

/*****************************************************************************
 *    Function Name            : NpUtilProcessWrapperFn
 *    Description              : Processes the IPC Message received from Active
 *                               CPU and Invoke the Common HW NPAPI function.
 *                               (Client HW Programming)
 *                                       
 *    Inputs                     HwInfo - Hardware Message Info .
 *
 *    Output(s)                : None 
 *    Globals Referred         : gRemoteSvciInputHwNp, gRemoteSvcOutputHwNp
 *    Globals Modified         : gRemoteSvcOutputHwNp
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
NpUtilProcessWrapperFn (tHwInfo * pHwRemoteInfo)
{
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               SequenceNum = 0;
    tRemoteHwNp        *pRemoteHwNpInput = NULL;
    tRemoteHwNp        *pRemoteHwNpOutput = NULL;
    tHwInfo             OutHwInfo;    /* Output Msg to be sent */
    BOOL1               bRemoteOutputMemAllocated = FNP_FALSE;
    MEMSET (&OutHwInfo, 0, sizeof (tHwInfo));

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilProcessWrapperFn: ENTRY\n");

    /* Allocate the global memory / mem pool */
    pRemoteHwNpInput = NpRemoteClntHwNpInMemAlloc ();
    if (pRemoteHwNpInput == NULL)
    {
        /* Mem Alloc Failure */
        NPUTIL_DBG (NPUTIL_DBG_MEM,
                    "Memory Allocation failed for RemoteClientInputHWNP:"
                    "RemoteHwNpOutput is Null \n");
        return FNP_FAILURE;
    }

    pRemoteHwNpInput->i4Length = (INT4) pHwRemoteInfo->u4PktSize;
    MEMCPY (pRemoteHwNpInput,
            pHwRemoteInfo->uHwMsgInfo.HwRemClntMsgInfo.pu1Data,
            pHwRemoteInfo->u4PktSize);
    if ((pRemoteHwNpInput->NpModuleId >= NP_MAX_MOD)
        || (pRemoteHwNpInput->u4Opcode >= NP_MAX_OPCODES))
    {
        NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);
        return FNP_FAILURE;
    }
    /* The parameter u4Status of uHwMsgInfo.HwRemClntMsgInfo is overloaded
     * to indicate whether ack is required for this NP call or not.
     */
    if (pHwRemoteInfo->uHwMsgInfo.HwRemClntMsgInfo.u4Status == FNP_SUCCESS)
    {
        /* Execute the NPAPI Call *//* Allocate the global memory / mem pool */
        pRemoteHwNpOutput = NpRemoteClntHwNpOutMemAlloc ();
        if (pRemoteHwNpOutput == NULL)
        {
            /* Mem Alloc Failure */
            NPUTIL_DBG (NPUTIL_DBG_MEM,
                        "Memory Allocation failed for RemoteClientOutputHWNP"
                        "RemoteHwNpOutput is Null \n");
            NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);
            return FNP_FAILURE;
        }

        /* Generic NP Util function to execute the Stby NP API calls.   *
         * NP Wrapper execution call received from Active Slot in a     *
         * Dual Unit Stacking environment                               */
        i4RetVal =
            NpUtilRemoteSvcHwProgram (pRemoteHwNpInput, pRemoteHwNpOutput);
        if (i4RetVal == FNP_FAILURE)
        {
            /* Send log failure message */

            NPUTIL_DBG2 (NPUTIL_DBG_ERR,
                         "Remote NP Programming failed for ModId %d OpCode %d\r\n",
                         pRemoteHwNpInput->NpModuleId,
                         pRemoteHwNpInput->u4Opcode);
            NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);
            NpRemoteClntHwNpOutMemRel (pRemoteHwNpOutput);
            return FNP_FAILURE;
        }
        NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);
        NpRemoteClntHwNpOutMemRel (pRemoteHwNpOutput);

        return FNP_SUCCESS;
    }

    /* Copy the sequence number of the Active NP wrapper call */
    SequenceNum = pHwRemoteInfo->uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum;
    /* Since Retransmission logic is introduced, It is possible to 
     * get a remote NPAPI call more than once, hence first check if the last NPAPI call
     * sequence number, NPAPI Module and NP Call matches the current call then 
     * skip the NPAPI execution, and resend the last call output 
     */
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "Sequence number for the "
                 "Active NP wrapper call is %d \n", SequenceNum);

    if ((SequenceNum == gu4StandbyRxSequenceNum) &&
        (pRemoteHwNpInput->NpModuleId == gRemoteSvcOutputHwNp.NpModuleId) &&
        (pRemoteHwNpInput->u4Opcode == gRemoteSvcOutputHwNp.u4Opcode))
    {
        pRemoteHwNpOutput = &gRemoteSvcOutputHwNp;
    }
    else
    {
        /* Execute the NPAPI Call */
        /* Allocate the global memory / mem pool */
        pRemoteHwNpOutput = NpRemoteClntHwNpOutMemAlloc ();
        if (pRemoteHwNpOutput == NULL)
        {
            /* Mem Alloc Failure */
            NPUTIL_DBG (NPUTIL_DBG_MEM,
                        "Memory Allocation failed for RemoteClientOutputHWNP"
                        "RemoteHwNpOutput is Null \n");
            NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);
            return FNP_FAILURE;
        }
        bRemoteOutputMemAllocated = FNP_TRUE;

        /* Generic NP Util function to execute the Stby NP API calls.   *
         * NP Wrapper execution call received from Active Slot in a     *
         * Dual Unit Stacking environment                               */
        gu4StandbyRetStatus =
            NpUtilRemoteSvcHwProgram (pRemoteHwNpInput, pRemoteHwNpOutput);
        gu4StandbyRxSequenceNum = SequenceNum;
    }

    /* Send the Output from Stby to the Active */

    OutHwInfo.u4MessageType = NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT;
    OutHwInfo.u4IfIndex = 0;    /* IfIndex not required for Higig Messaging */
    OutHwInfo.u4PktSize = pHwRemoteInfo->u4PktSize;    /* sizeof(tRemoteHwNp); */
    OutHwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4Status = gu4StandbyRetStatus;
    OutHwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum = SequenceNum;
    /* reply back the same sequence to Active for Validation */
    OutHwInfo.uHwMsgInfo.HwRemClntMsgInfo.pu1Data = (UINT1 *) pRemoteHwNpOutput;

    i4RetVal = FsCfaHwSendIPCMsg (&OutHwInfo);

    /* Release the allocated global memory */
    NpRemoteClntHwNpInMemRel (pRemoteHwNpInput);

    if (bRemoteOutputMemAllocated == FNP_TRUE)
    {
        /* Deallocate memory if it has been allocated from mem pool */
        NpRemoteClntHwNpOutMemRel (pRemoteHwNpOutput);
    }

    if (i4RetVal != FNP_SUCCESS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "FsCfaHwSendIPCMsg failed \n");
        /* ERROR */
        return FNP_FAILURE;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilProcessWrapperFn: EXIT\n");
    return i4RetVal;
}

/****************************************************************************/
/*    Function Name      : NpUtilProcessActiveNpIPCMsg                      */
/*                                                                          */
/*    Description        : This function process the packet from Standby,   */
/*                         validates the return val and seq. number.        */
/*                                                                          */
/*    Input(s)           : pu1DataBuf - pointer to the buffer.              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
NpUtilProcessActiveNpIPCMsg (tHwInfo * pHwInfo)
{
    UINT1              *pu1Buf = pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.pu1Data;
    UINT4               u4SeqNumber =
        pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum;
    INT4                i4RetVal =
        (INT4) pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4Status;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilProcessActiveNpIPCMsg: ENTRY\n");

    /* SUCCESSFUL Msg from STBY and Sequence Num of the NPAPI calls to be validated */
    if ((i4RetVal != FNP_FAILURE) && (u4SeqNumber == gSequenceNum))
    {
        gRetStatus = FNP_SUCCESS;

        /* On arrival of successful update from STBY, copy the data to the global structure
         * so that the same can be updated to the caller as part of GetCalls */
        MEMCPY (&gGetCallUpdateHwNp, pu1Buf, pHwInfo->u4PktSize);
        if (TmrStop (gRemoteNpTimerListId, &gRemoteNpTmrBlk) != TMR_SUCCESS)
        {
            NPUTIL_DBG (NPUTIL_DBG_TMR,
                        "NpUtilProcessActiveNpIPCMsg: TmrStop failed \n");
            return FNP_FAILURE;
        }
        NpRemoteExecSemUnLock ();
        NPUTIL_DBG (NPUTIL_DBG_MSG, "Successfully copied the global data. \n");
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                    "NpUtilProcessActiveNpIPCMsg: EXIT\n");
        return FNP_SUCCESS;
    }
    else if (u4SeqNumber < gSequenceNum)
    {
        /* Sequence number mismatch happened because of delay response */
        NPUTIL_DBG2 (NPUTIL_DBG_ERR, "Sequence number %d mismatches with "
                     "globalSequence number %d due "
                     "to delay response\n", u4SeqNumber, gSequenceNum);
        return FNP_FAILURE;
    }
    else
    {
        /* Update the overall status of the NP Remote Programming */
        gRetStatus = FNP_FAILURE;

        /* When the status fails, don't update anything to the caller
         * Update the caller with default values or no data as '0'   */
        MEMSET (&gGetCallUpdateHwNp, 0, sizeof (tRemoteHwNp));
        if (gbSemRelease == OSIX_FALSE)
        {
            gbSemRelease = OSIX_TRUE;
            if (TmrStop (gRemoteNpTimerListId, &gRemoteNpTmrBlk) != TMR_SUCCESS)
            {
                NPUTIL_DBG (NPUTIL_DBG_TMR,
                            "NpUtilProcessActiveNpIPCMsg: TmrStop failed \n");
                return FNP_FAILURE;
            }
            /* SEM release  */
            NpRemoteExecSemUnLock ();
        }
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NP Remote Programming status failed \n");
        return FNP_FAILURE;
    }
}

/*****************************************************************************
 *    Function Name      : NpUtilRemoteClntHwProgram()
 *    Description        : Utility function which can be used to send/execute 
 *                         the NP wrapper calls to the Standby Node of the
 *                         Dual Unit Stacking Model .
 *    Input(s)           : tRemoteHwNp - Remote Client Programming info
 *                         u4RpcWait - Wait for the IPC/RPC Status call
 *
 *    Output(s)                : None
 *    Globals Referred         : gRemoteExecSemId - Semaphore
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
UINT1
NpUtilRemoteClntHwProgram (tRemoteHwNp * pRemoteHwNp, UINT4 u4RpcWait)
{
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    UINT4               u4RetVal = FNP_FAILURE;
    UINT4               u4SlotId = FNP_ZERO;
    tHwInfo             HwInfo;
    tRemoteHwNp        *pGetCallHwNp = NULL;
    UINT4               u4PktSize = 0;

    MEMSET (&HwInfo, 0, sizeof (tHwInfo));

    u4StackingModel = (UINT4) ISS_GET_STACKING_MODEL ();
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteClntHwProgram: ENTRY\n");

    /* Message type to send the msg to STBY  */
    HwInfo.u4MessageType = NPWRAP_CALL_FROM_ACT_TO_STBY;
    HwInfo.u4IfIndex = 0;        /* doubt: IfIndex is 0 for Higig Transport */
    HwInfo.u4PktSize = (UINT4) pRemoteHwNp->i4Length;    /* PKT size differs based on each module */
    HwInfo.u4DestSlotId = u4SlotId;
    NPUTIL_DBG2 (NPUTIL_DBG_MSG,
                 "NpUtilRemoteClntHwProgram: Message Type is %d.Interface index is %d.\n",
                 HwInfo.u4MessageType, HwInfo.u4IfIndex);
    /* Sequence Number for Validation */
    HwInfo.uHwMsgInfo.HwRemClntMsgInfo.pu1Data = (UINT1 *) pRemoteHwNp;
    /* Actual data to be sent */
    HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4Status = FNP_FAILURE;
    /* Status of Remote NP Programming */

    /* Remote execution of NP wrapper calls applicable
     * for Dual Unit Stacking model  */
    /* Allocate a global structure for Get Calls */
    pGetCallHwNp = NpGetCallHwNpMemAlloc ();
    /* Copy the Incoming data to a Global Structure and it can be 
     * updated once the STBY msg arrives   */

    MEMCPY (pGetCallHwNp, pRemoteHwNp, pRemoteHwNp->i4Length);

    /* Osix No wait.
     * For the calls that do not require any acknowledgement from remote node,
     * send the IPC message directly without acquiring the RemoteExecSemLock.
     * To ensure the remote node does not send an acknowledgement, set the 
     * acknowledgement required bit in the tHwInfo structure.
     */
    if (OSIX_NO_WAIT == u4RpcWait)
    {
        if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
            (u4StackingModel == ISS_DISS_STACKING_MODEL))
        {
            if (CfaGetRetrieveNodeState () == RM_ACTIVE)
            {
                if (gu4BulkNpCount != NP_UTIL_BULK_NP_COUNT)
                {

                    /* The parameter u4Status of uHwMsgInfo.HwRemClntMsgInfo is overloaded
                     * to indicate whether ack is required for this NP call or not.
                     */
                    HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4Status = FNP_SUCCESS;
                    HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum = 0;
                    u4RetVal = (UINT4) FsCfaHwSendIPCMsg (&HwInfo);
                    gu4BulkNpCount++;
                    return u4RetVal;
                }
                else
                {
                    gu4BulkNpCount = 0;
                    u4RpcWait = OSIX_WAIT;
                }
            }
        }
    }

    if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (u4StackingModel == ISS_DISS_STACKING_MODEL))
    {
        /* If Port is part of Peer Unit,Send the pkt to Peer CPU
         * through IPC Mechanism */
        u4PktSize = HwInfo.u4PktSize;
        if (CfaGetRetrieveNodeState () == RM_ACTIVE)
        {
            gSequenceNum++;

            if (gSequenceNum == 0)
            {
                /* Wrapper around case hence do one more increment */
                gSequenceNum++;
            }

            HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum = gSequenceNum;
            NPUTIL_DBG2 (NPUTIL_DBG_MSG,
                         "NpUtilRemoteClntHwProgram: Packet size is %d. Sequence number is %d.\n",
                         HwInfo.u4PktSize, gSequenceNum);
            /* Acquire the Remote NP Execution Lock */
            if (u4RpcWait == OSIX_WAIT)
            {
                NpRemoteExecSemLock ();
                gRetStatus = FNP_FAILURE;
            }

            u4RetVal = (UINT4) FsCfaHwSendIPCMsg (&HwInfo);

            /* Implement TIMER Mechanism here */
            if (u4RetVal != FNP_SUCCESS)
            {
                if (u4RpcWait == OSIX_WAIT)
                {
                    NpRemoteExecSemUnLock ();
                }
                NPUTIL_DBG (NPUTIL_DBG_TMR,
                            "NpUtilRemoteClntHwProgram: FsCfaHwSendIPCMsg failed \n");
                return FNP_FAILURE;
            }

            /* Wait for the Peer Response before returning the function  */
            if (u4RpcWait == OSIX_WAIT)
            {
                /* Start the Timer */
                if (gRemoteNpTimerListId == 0)
                {
                    if (TmrCreateTimerList
                        (NULL, 0, NpUtilRemoteNpTimerCallBack,
                         &gRemoteNpTimerListId) == TMR_FAILURE)
                    {
                        NPUTIL_DBG (NPUTIL_DBG_TMR,
                                    "NpUtilRemoteClntHwProgram: TmrCreateTimerList failed \n");
                        return FNP_FAILURE;
                    }
                }
                /* Initialize the retransmission count */
                gu4RetransmissionCount = NP_UTIL_MAX_IPC_RETRANSMISSION;
                if (TmrStart
                    (gRemoteNpTimerListId, &gRemoteNpTmrBlk, 1, 2,
                     NP_UTIL_IPC_TIMEOUT) != TMR_SUCCESS)
                {
                    NPUTIL_DBG (NPUTIL_DBG_TMR,
                                "NpUtilRemoteClntHwProgram: TmrStart failed \n");
                    return FNP_FAILURE;
                }
                gbSemRelease = OSIX_FALSE;
                NpRemoteExecSemLock ();
            }

            /* Copy back the global output to the caller for GetCalls Implementation */
            MEMCPY (pRemoteHwNp, pGetCallHwNp, u4PktSize);

            NpRemoteExecSemUnLock ();
            /* Release the GLOBAL Memory availed */
            NpGetCallHwNpMemRel (pGetCallHwNp);

            /* Before returning to the caller check for overall status of *
             * NP Remote Programming in the 'gRetStatus' Flag             */
            if (gRetStatus != FNP_SUCCESS)
            {
                u4RetVal = FNP_FAILURE;
            }
            return (UINT1) u4RetVal;
        }
        else
        {

            /* As Remote NP Wrapper call execution of Packet Tx occurs 
             * only in Master state, ignore all other states. */
            NPUTIL_DBG (NPUTIL_DBG_MSG,
                        "ignore all other  states as Remote NP wrapper call"
                        "execution of packet tx occurs only in Active state\n");
            NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                        "NpUtilRemoteClntHwProgram: EXIT\n");
            return FNP_SUCCESS;
        }
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteClntHwProgram: EXIT\n");
    return FNP_SUCCESS;
}

VOID
NpUtilRemoteNpTimerCallBack (tTimerListId TimerListId)
{
    tHwInfo             HwInfo;
    UINT4               i4RetVal = TMR_SUCCESS;
    UINT4               i4NpRetVal = FNP_SUCCESS;
    UNUSED_PARAM (TimerListId);
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpTimerCallBack: ENTRY\n");

    if (gu4RetransmissionCount == 0)
    {
        /* Reached the Max Retransmission count for the NPAPI IPC call
         * still no reply from the remote unit, hence return failure.
         * if needed rechange teh max retransmission count value NP_UTIL_MAX_IPC_RETRANSMISSION
         */
        if (gbSemRelease == OSIX_FALSE)
        {
            gbSemRelease = OSIX_TRUE;
            NpRemoteExecSemUnLock ();
            NPUTIL_DBG (NPUTIL_DBG_MSG,
                        "MAX retransmission count reached."
                        " No reply from remote unit. So return failure\n");
            NPUTIL_DBG (NPUTIL_DBG_ERR, "Retransmission failed \n");
            return;
        }
    }
    else
    {
        /* Decrement the retransmission count */
        gu4RetransmissionCount--;
        NPUTIL_DBG (NPUTIL_DBG_MSG, "Decrement the retransmission count\n");

        /* Message type to send the msg to STBY  */
        HwInfo.u4MessageType = NPWRAP_CALL_FROM_ACT_TO_STBY;
        HwInfo.u4IfIndex = 0;    /* doubt: IfIndex is 0 for Higig Transport */
        HwInfo.u4PktSize = (UINT4) gGetCallUpdateHwNp.i4Length;    /* PKT size differs based on each module */
        /* Sequence Number for Validation */
        HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum = gSequenceNum;
        NPUTIL_DBG2 (NPUTIL_DBG_MSG,
                     "Message type to send the msg to STBY is %d."
                     " Interface index is %d\n",
                     HwInfo.u4MessageType, HwInfo.u4IfIndex);

        /* Current NPAPI details are stored in gGetCallUpdateHwNp 
         * use this and send it again to remote unit
         */

        HwInfo.uHwMsgInfo.HwRemClntMsgInfo.pu1Data =
            (UINT1 *) &gGetCallUpdateHwNp;
        /* Actual data to be sent */
        HwInfo.uHwMsgInfo.HwRemClntMsgInfo.u4Status = FNP_FAILURE;
        /* Status of Remote NP Programming */
        i4NpRetVal = (UINT4) FsCfaHwSendIPCMsg (&HwInfo);
        i4RetVal = TmrStart
            (gRemoteNpTimerListId, &gRemoteNpTmrBlk, 1, 2, NP_UTIL_IPC_TIMEOUT);
        if ((i4NpRetVal != FNP_SUCCESS) || (i4RetVal != TMR_SUCCESS))
        {
            /* Resend failed */
            if (gbSemRelease == OSIX_FALSE)
            {
                gbSemRelease = OSIX_TRUE;
                NpRemoteExecSemUnLock ();
            }
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilRemoteNpTimerCallBack : Resend failed \n");
            return;
        }
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpTimerCallBack: EXIT\n");
    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilSetIPCSyncStatus                           */
/*                                                                         */
/*  Description         : This function sets the global variable which     */
/*                        indicates whether IPC  call for NPAPI Invocation */
/*                        should be sent to Stand-by or Not .              */
/*                                                                         */
/*  Input(s)            : u1IPCSyncStatus - Indicates whether IPC  call    */
/*                                          for NPAPI Invocation should be */
/*                                          sent to Stand-by or Not .      */
/*                                                                         */
/*  Output(s)           : None                                             */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS                                       */
/***************************************************************************/
INT4
NpUtilSetIPCSyncStatus (UINT1 u1IPCSyncStatus)
{
    gu1NpUtilIPCSyncStatus = u1IPCSyncStatus;
    return FNP_SUCCESS;
}

/***************************************************************************/
/*  Function Name       : NpUtilGetIPCSyncStatus                           */
/*                                                                         */
/*  Description         : This function returns the global variable which  */
/*                        indicates whether IPC  call for NPAPI Invocation */
/*                        should be sent to Stand-by or Not .              */

/*  Input(s)           :  None                                              */
/*                                                                          */
/*  Output(s)            :pu1IPCSyncStatus - Indicates whether IPC  call    */
/*                                           for NPAPI Invocation should be */
/*                                           sent to Stand-by or Not .      */
/*                                                                         */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling    : None.                                       */
/*                                                                         */
/*  Use of Recursion        : None.                                        */
/*                                                                         */
/*  Returns            : FNP_SUCCESS                                       */
/***************************************************************************/
INT4
NpUtilGetIPCSyncStatus (UINT1 *pu1IPCSyncStatus)
{
    *pu1IPCSyncStatus = gu1NpUtilIPCSyncStatus;
    return FNP_SUCCESS;
}
#endif /* __NPUTIL_C__ */
