/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mcnputil.c,v 1.4 2016/06/14 12:28:15 siva Exp $
 *
 * Description:This file contains the NP MC utility functions of IP
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __IP6MC_NPUTIL_C__
#define __IP6MC_NPUTIL_C__

#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskIp6mcNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIp6mcNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
#ifdef MBSM_WANTED
    tIp6mcNpModInfo     *pIp6mcNpModInfo = NULL;
    tIp6mcRemoteNpModInfo *pIp6mcRemoteNpModInfo = NULL;

    pIp6mcNpModInfo = &pFsHwNp->Ip6mcNpModInfo;
    pIp6mcRemoteNpModInfo = &pRemoteHwNp->Ip6mcRemoteNpModInfo;
#endif
    UNUSED_PARAM (pi4RpcCallStatus);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;


    switch (pFsHwNp->u4Opcode)
    {
#ifdef PIMV6_WANTED
	case FS_NP_IPV6_MC_INIT:
	case FS_NP_IPV6_MC_DE_INIT:
	case FS_PIMV6_NP_INIT_HW:
	case FS_PIMV6_NP_DE_INIT_HW:
	case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
	case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
	case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
	case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
	case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
	case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
	case FS_NP_IPV6_MC_GET_HIT_STATUS:
	case FS_NP_IPV6_MC_ADD_CPU_PORT:
	case FS_NP_IPV6_MC_DEL_CPU_PORT:
	case FS_NP_IPV6_GET_M_ROUTE_STATS:
	case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
	case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
	case FS_NP_IPV6_GET_M_IFACE_STATS:
	case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
	case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD: 
	case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
	{
	    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
	    break;
	}
#ifdef MBSM_WANTED
	case FS_NP_IPV6_MBSM_MC_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_MC_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
	case FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6MbsmMcAddRouteEntry *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McAddRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6MbsmMcAddRouteEntry;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McAddRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            pRemoteArgs->u1CallerId = pLocalArgs->u1CallerId;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf),
                        (pLocalArgs->pDownStreamIf), 
                        (sizeof (tMc6DownStreamIf) * pLocalArgs->u2NoOfDownStreamIf));
            }
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_MC_ADD_ROUTE_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_PIMV6_MBSM_NP_INIT_HW:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_PIMV6_NP_INIT_HW;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#else
        UNUSED_PARAM (pRemoteHwNp);
#endif
#else
        UNUSED_PARAM (pRemoteHwNp);
#endif
	default:
	    break;	
    }

#ifdef PIMV6_WANTED
#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_NP_IPV6_MBSM_MC_INIT) ||
        ((pFsHwNp->u4Opcode > FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_IP6MC_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
#endif
    return;
}


/***************************************************************************/
/*  Function Name       : NpUtilIp6mcMergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                        */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp                */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value          */
/*                    pi4RemoteNpRetVal - Remote Np Return Value           */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilIp6mcMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp,
                                    INT4 *pi4LocalNpRetVal,
                                    INT4 *pi4RemoteNpRetVal)
{
    tIp6mcNpModInfo     *pIp6mcNpModInfo = NULL;
    tIp6mcRemoteNpModInfo *pIp6mcRemoteNpModInfo = NULL;
    UINT1               u1RetVal = FNP_SUCCESS;

    pIp6mcNpModInfo = &pFsHwNp->Ip6mcNpModInfo;
    pIp6mcRemoteNpModInfo = &pRemoteHwNp->Ip6mcRemoteNpModInfo;

    switch (pFsHwNp->u4Opcode)
    {
#ifdef PIMV6_WANTED
	case FS_NP_IPV6_MC_INIT:
	case FS_NP_IPV6_MC_DE_INIT:
	case FS_PIMV6_NP_INIT_HW:
	case FS_PIMV6_NP_DE_INIT_HW:
	case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
	case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
	case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
	case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
	case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
	case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
	case FS_NP_IPV6_MC_ADD_CPU_PORT:
	case FS_NP_IPV6_MC_DEL_CPU_PORT:
	case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD: 
	case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
	{
	    break;
	}
	case FS_NP_IPV6_MC_GET_HIT_STATUS:
	{
            tIp6mcNpWrFsNpIpv6McGetHitStatus *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McGetHitStatus;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McGetHitStatus;
	    (*pLocalArgs->pu4HitStatus) = 
		(pRemoteArgs->u4HitStatus) | (*pLocalArgs->pu4HitStatus);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV6_GET_M_ROUTE_STATS:
	{
            tIp6mcNpWrFsNpIpv6GetMRouteStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
	{
            tIp6mcNpWrFsNpIpv6GetMRouteHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteHCStats;
	    (pLocalArgs->pu8RetValue->msn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.msn) + (pLocalArgs->pu8RetValue->msn));
	    (pLocalArgs->pu8RetValue->lsn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.lsn) + (pLocalArgs->pu8RetValue->lsn));
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
	{
            tIp6mcNpWrFsNpIpv6GetMNextHopStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMNextHopStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMNextHopStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV6_GET_M_IFACE_STATS:
	{
            tIp6mcNpWrFsNpIpv6GetMIfaceStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
	{
            tIp6mcNpWrFsNpIpv6GetMIfaceHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceHCStats;
	    (pLocalArgs->pu8RetValue->msn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.msn) + (pLocalArgs->pu8RetValue->msn));
	    (pLocalArgs->pu8RetValue->lsn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.lsn) + (pLocalArgs->pu8RetValue->lsn));
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
#endif
        default:
            break;
    }
#ifndef PIMV6_WANTED
    UNUSED_PARAM (pi4LocalNpRetVal);
    UNUSED_PARAM (pi4RemoteNpRetVal);
#endif
    return u1RetVal;
}


/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIp6mcConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIp6mcConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcRemoteNpModInfo *pIp6mcRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIp6mcNpModInfo = &(pFsHwNp->Ip6mcNpModInfo);
    pIp6mcRemoteNpModInfo = &(pRemoteHwNp->Ip6mcRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIp6mcRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef PIMV6_WANTED
        case FS_NP_IPV6_MC_INIT:
        {
            break;
        }
        case FS_NP_IPV6_MC_DE_INIT:
        {
            break;
        }
        case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McAddRouteEntry *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McAddRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddRouteEntry;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McAddRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            pRemoteArgs->u1CallerId = pLocalArgs->u1CallerId;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf), (pLocalArgs->pDownStreamIf),
                        (pLocalArgs->u2NoOfDownStreamIf * sizeof (tMc6DownStreamIf)));
            }
            break;
        }
        case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McDelRouteEntry *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McDelRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelRouteEntry;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McDelRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf),
                        (pLocalArgs->pDownStreamIf), sizeof (tMc6DownStreamIf));
            }
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
        {
            tIp6mcNpWrFsNpIpv6McClearAllRoutes *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McClearAllRoutes *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearAllRoutes;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McClearAllRoutes;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McUpdateOifVlanEntry *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McUpdateOifVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateOifVlanEntry;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6McUpdateOifVlanEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            MEMCPY (&(pRemoteArgs->downStreamIf), &(pLocalArgs->downStreamIf),
                    sizeof (tMc6DownStreamIf));
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McUpdateIifVlanEntry *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McUpdateIifVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateIifVlanEntry;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6McUpdateIifVlanEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            MEMCPY (&(pRemoteArgs->downStreamIf), &(pLocalArgs->downStreamIf),
                    sizeof (tMc6DownStreamIf));
            break;
        }
        case FS_NP_IPV6_MC_GET_HIT_STATUS:
        {
            tIp6mcNpWrFsNpIpv6McGetHitStatus *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McGetHitStatus;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McGetHitStatus;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4Iif = pLocalArgs->u4Iif;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->pu4HitStatus != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4HitStatus), (pLocalArgs->pu4HitStatus),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_MC_ADD_CPU_PORT:
        {
            tIp6mcNpWrFsNpIpv6McAddCpuPort *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McAddCpuPort *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddCpuPort;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McAddCpuPort;
            pRemoteArgs->u1GenRtrId = pLocalArgs->u1GenRtrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf),
                        (pLocalArgs->pDownStreamIf), sizeof (tMc6DownStreamIf));
            }
            break;
        }
        case FS_NP_IPV6_MC_DEL_CPU_PORT:
        {
            tIp6mcNpWrFsNpIpv6McDelCpuPort *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McDelCpuPort *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelCpuPort;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McDelCpuPort;
            pRemoteArgs->u1GenRtrId = pLocalArgs->u1GenRtrId;
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMc6RtEntry));
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1GrpAddr), (pLocalArgs->pu1GrpAddr),
                        sizeof (tIp6Addr));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SrcAddr), (pLocalArgs->pu1SrcAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteHCStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1GrpAddr), (pLocalArgs->pu1GrpAddr),
                        sizeof (tIp6Addr));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SrcAddr), (pLocalArgs->pu1SrcAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValue), (pLocalArgs->pu8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMNextHopStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMNextHopStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMNextHopStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1GrpAddr), (pLocalArgs->pu1GrpAddr),
                        sizeof (tIp6Addr));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SrcAddr), (pLocalArgs->pu1SrcAddr),
                        sizeof (tIp6Addr));
            }
            pRemoteArgs->i4OutIfIndex = pLocalArgs->i4OutIfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceHCStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValue), (pLocalArgs->pu8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIp6mcNpWrFsNpIpv6SetMIfaceTtlTreshold *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6SetMIfaceTtlTreshold *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceTtlTreshold;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6SetMIfaceTtlTreshold;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4TtlTreshold = pLocalArgs->i4TtlTreshold;
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
        {
            tIp6mcNpWrFsNpIpv6SetMIfaceRateLimit *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6SetMIfaceRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceRateLimit;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6SetMIfaceRateLimit;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4RateLimit = pLocalArgs->i4RateLimit;
            break;
        }
        case FS_PIMV6_NP_INIT_HW:
        {
            break;
        }
        case FS_PIMV6_NP_DE_INIT_HW:
        {
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
        {
            tIp6mcNpWrFsNpIpv6McClearHitBit *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McClearHitBit *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearHitBit;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McClearHitBit;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->srcIpAddr), (pLocalArgs->pSrcIpAddr),
                        sizeof (tIp6Addr));
            }
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->grpAddr), (pLocalArgs->pGrpAddr),
                        sizeof (tIp6Addr));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_IPV6_MBSM_MC_INIT:
        case FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY:
        case FS_PIMV6_MBSM_NP_INIT_HW:
	{
	    break;
	}
#endif 
#endif /* PIMV6_WANTED */
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIp6mcConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIp6mcConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcRemoteNpModInfo *pIp6mcRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pIp6mcNpModInfo = &(pFsHwNp->Ip6mcNpModInfo);
    pIp6mcRemoteNpModInfo = &(pRemoteHwNp->Ip6mcRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef PIMV6_WANTED
        case FS_NP_IPV6_MC_INIT:
        case FS_NP_IPV6_MC_DE_INIT:
        case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
        case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
        case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
        case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
        case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
        case FS_NP_IPV6_MC_ADD_CPU_PORT:
        case FS_NP_IPV6_MC_DEL_CPU_PORT:
        case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD:
        case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
        case FS_PIMV6_NP_INIT_HW:
        case FS_PIMV6_NP_DE_INIT_HW:
        case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
        {
            break;
        }
        case FS_NP_IPV6_MC_GET_HIT_STATUS:
        {
            tIp6mcNpWrFsNpIpv6McGetHitStatus *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McGetHitStatus;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McGetHitStatus;
            if (pLocalArgs->pSrcIpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pSrcIpAddr), &(pRemoteArgs->srcIpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pGrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pGrpAddr), &(pRemoteArgs->grpAddr),
                        sizeof (UINT1));
            }
            pLocalArgs->u4Iif = pRemoteArgs->u4Iif;
            pLocalArgs->u2VlanId = pRemoteArgs->u2VlanId;
            if (pLocalArgs->pu4HitStatus != NULL)
            {
                MEMCPY ((pLocalArgs->pu4HitStatus), &(pRemoteArgs->u4HitStatus),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1GrpAddr), &(pRemoteArgs->u1GrpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SrcAddr), &(pRemoteArgs->u1SrcAddr),
                        sizeof (UINT1));
            }
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteHCStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1GrpAddr), &(pRemoteArgs->u1GrpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SrcAddr), &(pRemoteArgs->u1SrcAddr),
                        sizeof (UINT1));
            }
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValue), &(pRemoteArgs->U8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMNextHopStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMNextHopStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMNextHopStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            if (pLocalArgs->pu1GrpAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1GrpAddr), &(pRemoteArgs->u1GrpAddr),
                        sizeof (UINT1));
            }
            if (pLocalArgs->pu1SrcAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SrcAddr), &(pRemoteArgs->u1SrcAddr),
                        sizeof (UINT1));
            }
            pLocalArgs->i4OutIfIndex = pRemoteArgs->i4OutIfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceHCStats *pLocalArgs = NULL;
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceHCStats;
            pRemoteArgs =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceHCStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValue), &(pRemoteArgs->U8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_IPV6_MBSM_MC_INIT:
        case FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY:
        case FS_PIMV6_MBSM_NP_INIT_HW:
	{
	    break;
	}
#endif 
#endif /* PIMV6_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIp6mcRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIp6mcNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIp6mcRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                               tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIp6mcRemoteNpModInfo *pIp6mcRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIp6mcRemoteNpModInfo = &(pRemoteHwNpInput->Ip6mcRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef PIMV6_WANTED
        case FS_NP_IPV6_MC_INIT:
        {
            u1RetVal = FsNpIpv6McInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_DE_INIT:
        {
            u1RetVal = FsNpIpv6McDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
        {
            tIp6mcRemoteNpWrFsNpIpv6McAddRouteEntry *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McAddRouteEntry;
            u1RetVal =
                FsNpIpv6McAddRouteEntry (pInput->u4VrId, (UINT1 *) &(pInput->grpAddr),
                                         pInput->u4GrpPrefix,
                                         (UINT1 *)&(pInput->srcIpAddr),
                                         pInput->u4SrcIpPrefix,
                                         pInput->u1CallerId, pInput->rtEntry,
                                         pInput->u2NoOfDownStreamIf,
                                         (tMc6DownStreamIf *)&(pInput->DownStreamIf));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
        {
            tIp6mcRemoteNpWrFsNpIpv6McDelRouteEntry *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McDelRouteEntry;
            u1RetVal =
                FsNpIpv6McDelRouteEntry (pInput->u4VrId, (UINT1 *)&(pInput->grpAddr),
                                         pInput->u4GrpPrefix,
                                         (UINT1 *)&(pInput->srcIpAddr),
                                         pInput->u4SrcIpPrefix, pInput->rtEntry,
                                         pInput->u2NoOfDownStreamIf,
                                         &(pInput->DownStreamIf));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
        {
            tIp6mcRemoteNpWrFsNpIpv6McClearAllRoutes *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McClearAllRoutes;
            FsNpIpv6McClearAllRoutes (pInput->u4VrId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIp6mcRemoteNpWrFsNpIpv6McUpdateOifVlanEntry *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6McUpdateOifVlanEntry;
            FsNpIpv6McUpdateOifVlanEntry (pInput->u4VrId, (UINT1 *)&(pInput->grpAddr),
                                          pInput->u4GrpPrefix,
                                          (UINT1 *)&(pInput->srcIpAddr),
                                          pInput->u4SrcIpPrefix,
                                          pInput->rtEntry,
                                          pInput->downStreamIf);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIp6mcRemoteNpWrFsNpIpv6McUpdateIifVlanEntry *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6McUpdateIifVlanEntry;
            FsNpIpv6McUpdateIifVlanEntry (pInput->u4VrId, (UINT1 *)&(pInput->grpAddr),
                                          pInput->u4GrpPrefix,
                                          (UINT1 *)&(pInput->srcIpAddr),
                                          pInput->u4SrcIpPrefix,
                                          pInput->rtEntry,
                                          pInput->downStreamIf);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_GET_HIT_STATUS:
        {
            tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McGetHitStatus;
            FsNpIpv6McGetHitStatus ((UINT1 *)&(pInput->srcIpAddr), 
				    (UINT1 *)&(pInput->grpAddr),
                                    pInput->u4Iif, pInput->u2VlanId,
                                    &(pInput->u4HitStatus));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_ADD_CPU_PORT:
        {
            tIp6mcRemoteNpWrFsNpIpv6McAddCpuPort *pInput = NULL;
            pInput = &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McAddCpuPort;
            u1RetVal =
                FsNpIpv6McAddCpuPort (pInput->u1GenRtrId, (UINT1 *)&(pInput->grpAddr),
                                      pInput->u4GrpPrefix, (UINT1 *)&(pInput->srcIpAddr),
                                      pInput->u4SrcIpPrefix, pInput->rtEntry,
                                      pInput->u2NoOfDownStreamIf,
                                      &(pInput->DownStreamIf));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_DEL_CPU_PORT:
        {
            tIp6mcRemoteNpWrFsNpIpv6McDelCpuPort *pInput = NULL;
            pInput = &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McDelCpuPort;
            u1RetVal =
                FsNpIpv6McDelCpuPort (pInput->u1GenRtrId, (UINT1 *)&(pInput->grpAddr),
                                      pInput->u4GrpPrefix, (UINT1 *)&(pInput->srcIpAddr),
                                      pInput->u4SrcIpPrefix, pInput->rtEntry);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_STATS:
        {
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteStats;
            u1RetVal =
                FsNpIpv6GetMRouteStats (pInput->u4VrId, (UINT1 *)&(pInput->u1GrpAddr),
                                        (UINT1 *)&(pInput->u1SrcAddr),
                                        pInput->i4StatType,
                                        &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
        {
            tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMRouteHCStats;
            u1RetVal =
                FsNpIpv6GetMRouteHCStats (pInput->u4VrId, (UINT1 *)&(pInput->u1GrpAddr),
                                          (UINT1 *)&(pInput->u1SrcAddr),
                                          pInput->i4StatType,
                                          &(pInput->U8RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
        {
            tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMNextHopStats;
            u1RetVal =
                FsNpIpv6GetMNextHopStats (pInput->u4VrId, (UINT1 *)&(pInput->u1GrpAddr),
                                          (UINT1 *)&(pInput->u1SrcAddr),
                                          pInput->i4OutIfIndex,
                                          pInput->i4StatType,
                                          &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_STATS:
        {
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceStats;
            u1RetVal =
                FsNpIpv6GetMIfaceStats (pInput->u4VrId, pInput->i4IfIndex,
                                        pInput->i4StatType,
                                        &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
        {
            tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6GetMIfaceHCStats;
            u1RetVal =
                FsNpIpv6GetMIfaceHCStats (pInput->u4VrId, pInput->i4IfIndex,
                                          pInput->i4StatType,
                                          &(pInput->U8RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIp6mcRemoteNpWrFsNpIpv6SetMIfaceTtlTreshold *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->
                Ip6mcRemoteNpFsNpIpv6SetMIfaceTtlTreshold;
            u1RetVal =
                FsNpIpv6SetMIfaceTtlTreshold (pInput->u4VrId, pInput->i4IfIndex,
                                              pInput->i4TtlTreshold);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
        {
            tIp6mcRemoteNpWrFsNpIpv6SetMIfaceRateLimit *pInput = NULL;
            pInput =
                &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6SetMIfaceRateLimit;
            u1RetVal =
                FsNpIpv6SetMIfaceRateLimit (pInput->u4VrId, pInput->i4IfIndex,
                                            pInput->i4RateLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_PIMV6_NP_INIT_HW:
        {
            u1RetVal = FsPimv6NpInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_PIMV6_NP_DE_INIT_HW:
        {
            u1RetVal = FsPimv6NpDeInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
        {
            tIp6mcRemoteNpWrFsNpIpv6McClearHitBit *pInput = NULL;
            pInput = &pIp6mcRemoteNpModInfo->Ip6mcRemoteNpFsNpIpv6McClearHitBit;
            u1RetVal =
                FsNpIpv6McClearHitBit (pInput->u2VlanId, (UINT1 *)&(pInput->srcIpAddr),
                                       (UINT1 *)&(pInput->grpAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* PIMV6_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
