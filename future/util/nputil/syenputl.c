/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syenputl.c,v 1.3 2013/11/14 11:39:42 siva Exp $
 *
 * Description:This file contains the NP utilities functionalities
 *             required for the remote NP Hardware Programming
 *
 *******************************************************************/

#include "npstackutl.h"

/***************************************************************************
*  Function Name       : NpUtilMaskSynceNpPorts
*
*  Description         : This function mask/split the Active and Standby
*                        ports as per the tFsHwNp information and update
*                        the NpCallStatus accordingly.
*
*  Input(s)            : FsHwNp Param of type tfsHwNp
*
*  Output(s)           : NpCallStauts of type UINT1
*
*  Global Variables Referred : None
*
*  Global Variables Modified : None.
*
*  Exceptions or Operating
*  System Error Handling     : None.
*
*  Use of Recursion          : None.
*
*  Returns                   : NONE(VOID)
***************************************************************************/
VOID
NpUtilMaskSynceNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    UNUSED_PARAM(pFsHwNp);
    UNUSED_PARAM(pRemoteHwNp);
    UNUSED_PARAM(pu1NpCallStatus);
    UNUSED_PARAM(pi4RpcCallStatus);

    return;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilSynceConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilSynceConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1                u1RetVal = FNP_SUCCESS;
    UINT4                u4Opcode = 0;
    tSynceNpModInfo         * pSynceNpModInfo = NULL;
    tSynceRemoteNpModInfo   * pSynceRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pSynceNpModInfo = &(pFsHwNp->SynceNpModInfo);
    pSynceRemoteNpModInfo = &(pRemoteHwNp->SynceRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =  pRemoteHwNp->i4Length + sizeof(tSynceRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_HW_CONFIG_SYNCE_INFO:
        {
            tSynceNpWrFsNpHwConfigSynceInfo * pLocalArgs = NULL;
            tSynceRemoteNpWrFsNpHwConfigSynceInfo * pRemoteArgs = NULL;
            pLocalArgs = &pSynceNpModInfo->SynceNpFsNpHwConfigSynceInfo;
        pRemoteArgs = &pSynceRemoteNpModInfo->SynceRemoteNpFsNpHwConfigSynceInfo;
   if(pLocalArgs->pSynceHwSynceInfo != NULL)
   {
       MEMCPY(&(pRemoteArgs->SynceHwSynceInfo), (pLocalArgs->pSynceHwSynceInfo), sizeof (tSynceHwInfo ));
   }
            break;
        }
    default:
        u1RetVal = FNP_FAILURE;
        break;

    } /* switch*/

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilSynceConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilSynceConvertRemoteToLocalNp (tRemoteHwNp *pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tSynceNpModInfo        * pSynceNpModInfo = NULL;
    tSynceRemoteNpModInfo  * pSynceRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pSynceNpModInfo = &(pFsHwNp->SynceNpModInfo);
    pSynceRemoteNpModInfo = &(pRemoteHwNp->SynceRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_NP_HW_CONFIG_SYNCE_INFO:
        {
            tSynceNpWrFsNpHwConfigSynceInfo * pLocalArgs = NULL;
            tSynceRemoteNpWrFsNpHwConfigSynceInfo * pRemoteArgs = NULL;
            pLocalArgs = &pSynceNpModInfo->SynceNpFsNpHwConfigSynceInfo;
        pRemoteArgs = &pSynceRemoteNpModInfo->SynceRemoteNpFsNpHwConfigSynceInfo;
   if(pLocalArgs->pSynceHwSynceInfo != NULL)
   {
       MEMCPY((pLocalArgs->pSynceHwSynceInfo), &(pRemoteArgs->SynceHwSynceInfo), sizeof (tSynceHwInfo ));
   }
            break;
        }
    default:
        u1RetVal = FNP_FAILURE;
        break;

    } /* switch*/

    return (u1RetVal);
}


/***************************************************************************
 *
 *    Function Name       : NpUtilSynceRemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tSynceNpModInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilSynceRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1         u1RetVal = FNP_SUCCESS;
    UINT4         u4Opcode = 0;
    tSynceRemoteNpModInfo      *pSynceRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pSynceRemoteNpModInfo = &(pRemoteHwNpInput->SynceRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_HW_CONFIG_SYNCE_INFO:
        {
            tSynceRemoteNpWrFsNpHwConfigSynceInfo * pInput = NULL;
        pInput = &pSynceRemoteNpModInfo->SynceRemoteNpFsNpHwConfigSynceInfo;
    u1RetVal = FsNpHwConfigSynceInfo (&(pInput->SynceHwSynceInfo));
             MEMCPY(pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
    default:
        u1RetVal = FNP_FAILURE;
        break;

    } /* switch*/

    return (u1RetVal);
}
