/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: mstnputil.c,v 1.8 2018/02/13 09:11:18 siva Exp $
 *
 * Description:This file contains the single NP RSTP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __MSTUTIL_C__
#define __MSTUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskMstpNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef MSTP_WANTED
VOID
NpUtilMaskMstpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
#ifdef MBSM_WANTED
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpRemoteNpModInfo *pMstpRemoteNpModInfo = NULL;
#endif

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

#ifdef MBSM_WANTED
    pMstpNpModInfo = &(pFsHwNp->MstpNpModInfo);
    pMstpRemoteNpModInfo = &(pRemoteHwNp->MstpRemoteNpModInfo);
#endif

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaNpGetHwPortInfo (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_MSTP_NP_CREATE_INSTANCE:
        case FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING:
        case FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING:
        case FS_MI_MSTP_NP_DELETE_INSTANCE:
        case FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING:
        case FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING:
        case FS_MI_MSTP_NP_INIT_HW:
        case FS_MI_MSTP_NP_DE_INIT_HW:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        case FS_MI_MST_NP_GET_PORT_STATE:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MSTP_NP_CREATE_INSTANCE;

            tMstpNpWrFsMiMstpMbsmNpCreateInstance *pEntry = NULL;
            tMstpRemoteNpWrFsMiMstpNpCreateInstance *pRemEntry = NULL;

            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpCreateInstance;
            pRemEntry =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpCreateInstance;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u2InstId = pEntry->u2InstId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING;

            tMstpNpWrFsMiMstpMbsmNpAddVlanInstMapping *pEntry = NULL;
            tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping *pRemEntry = NULL;

            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpAddVlanInstMapping;
            pRemEntry =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpAddVlanInstMapping;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            MEMCPY (&(pRemEntry->VlanId), &(pEntry->VlanId), sizeof (tVlanId));
            pRemEntry->u2InstId = pEntry->u2InstId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE;

            tMstpNpWrFsMiMstpMbsmNpSetInstancePortState *pEntry = NULL;
            tMstpRemoteNpWrFsMiMstpNpSetInstancePortState *pRemEntry = NULL;

            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpSetInstancePortState;
            pRemEntry =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpSetInstancePortState;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            pRemEntry->u2InstanceId = pEntry->u2InstanceId;
            pRemEntry->u1PortState = pEntry->u1PortState;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_MSTP_MBSM_NP_INIT_HW:
        {
            pRemoteHwNp->u4Opcode = FS_MI_MSTP_NP_INIT_HW;

            tMstpNpWrFsMiMstpMbsmNpInitHw *pEntry = NULL;
            tMstpRemoteNpWrFsMiMstpNpInitHw *pRemEntry = NULL;

            pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpInitHw;
            pRemEntry = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpInitHw;

            pRemEntry->u4ContextId = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE) ||
        ((pFsHwNp->u4Opcode > FS_MI_MSTP_MBSM_NP_INIT_HW)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_MSTP_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMstpConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMstpConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpRemoteNpModInfo *pMstpRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMstpNpModInfo = &(pFsHwNp->MstpNpModInfo);
    pMstpRemoteNpModInfo = &(pRemoteHwNp->MstpRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tMstpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_MSTP_NP_CREATE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpCreateInstance *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpCreateInstance *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpCreateInstance;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpCreateInstance;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpAddVlanInstMapping;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanListInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpAddVlanListInstMapping *pRemoteArgs = NULL;
            pLocalArgs =
                &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanListInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpAddVlanListInstMapping;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu1VlanList != NULL)
            {
                MEMCPY (&(pRemoteArgs->au1VlanList[0]),
                        (pLocalArgs->pu1VlanList),
                        sizeof (UINT1) * (AST_VLAN_LIST_SIZE));
            }
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            pRemoteArgs->u2NumVlans = pLocalArgs->u2NumVlans;
            if (pLocalArgs->pu2LastVlan != NULL)
            {
                MEMCPY (&(pRemoteArgs->u2LastVlan), (pLocalArgs->pu2LastVlan),
                        sizeof (UINT2));
            }
            break;
        }
        case FS_MI_MSTP_NP_DELETE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpDeleteInstance *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDeleteInstance *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDeleteInstance;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeleteInstance;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDelVlanInstMapping *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDelVlanInstMapping;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanListInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDelVlanListInstMapping *pRemoteArgs = NULL;
            pLocalArgs =
                &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanListInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpDelVlanListInstMapping;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            if (pLocalArgs->pu1VlanList != NULL)
            {
                MEMCPY (&(pRemoteArgs->au1VlanList[0]),
                        (pLocalArgs->pu1VlanList),
                        sizeof (UINT1) * (AST_VLAN_LIST_SIZE));
            }
            pRemoteArgs->u2InstId = pLocalArgs->u2InstId;
            pRemoteArgs->u2NumVlans = pLocalArgs->u2NumVlans;
            if (pLocalArgs->pu2LastVlan != NULL)
            {
                MEMCPY (&(pRemoteArgs->u2LastVlan), (pLocalArgs->pu2LastVlan),
                        sizeof (UINT2));
            }
            break;
        }
        case FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        {
            tMstpNpWrFsMiMstpNpSetInstancePortState *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpSetInstancePortState *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpSetInstancePortState;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpSetInstancePortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2InstanceId = pLocalArgs->u2InstanceId;
            pRemoteArgs->u1PortState = pLocalArgs->u1PortState;
            break;
        }
        case FS_MI_MSTP_NP_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpInitHw *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpInitHw;
            pRemoteArgs = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpInitHw;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_MSTP_NP_DE_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpDeInitHw *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDeInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDeInitHw;
            pRemoteArgs = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeInitHw;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_MST_NP_GET_PORT_STATE:
        {
            tMstpNpWrFsMiMstNpGetPortState *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstNpGetPortState;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstNpGetPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u2InstanceId = pLocalArgs->u2InstanceId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu1Status != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1Status), (pLocalArgs->pu1Status),
                        sizeof (UINT1));
            }
            break;
        }
#ifdef  MBSM_WANTED
        case FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE:
        case FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING:
        case FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE:
        case FS_MI_MSTP_MBSM_NP_INIT_HW:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMstpConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMstpConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpRemoteNpModInfo *pMstpRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pMstpNpModInfo = &(pFsHwNp->MstpNpModInfo);
    pMstpRemoteNpModInfo = &(pRemoteHwNp->MstpRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_MSTP_NP_CREATE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpCreateInstance *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpCreateInstance *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpCreateInstance;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpCreateInstance;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpAddVlanInstMapping;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpAddVlanListInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpAddVlanListInstMapping *pRemoteArgs = NULL;
            pLocalArgs =
                &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanListInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpAddVlanListInstMapping;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            if (pLocalArgs->pu1VlanList != NULL)
            {
                MEMCPY ((pLocalArgs->pu1VlanList),
                        &(pRemoteArgs->au1VlanList[0]),
                        sizeof (UINT1) * (AST_VLAN_LIST_SIZE));
            }
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            pLocalArgs->u2NumVlans = pRemoteArgs->u2NumVlans;
            if (pLocalArgs->pu2LastVlan != NULL)
            {
                MEMCPY ((pLocalArgs->pu2LastVlan), &(pRemoteArgs->u2LastVlan),
                        sizeof (UINT2));
            }
            break;
        }
        case FS_MI_MSTP_NP_DELETE_INSTANCE:
        {
            tMstpNpWrFsMiMstpNpDeleteInstance *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDeleteInstance *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDeleteInstance;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeleteInstance;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDelVlanInstMapping *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDelVlanInstMapping;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING:
        {
            tMstpNpWrFsMiMstpNpDelVlanListInstMapping *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDelVlanListInstMapping *pRemoteArgs = NULL;
            pLocalArgs =
                &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanListInstMapping;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpDelVlanListInstMapping;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            if (pLocalArgs->pu1VlanList != NULL)
            {
                MEMCPY ((pLocalArgs->pu1VlanList),
                        &(pRemoteArgs->au1VlanList[0]),
                        sizeof (UINT1) * (AST_VLAN_LIST_SIZE));
            }
            pLocalArgs->u2InstId = pRemoteArgs->u2InstId;
            pLocalArgs->u2NumVlans = pRemoteArgs->u2NumVlans;
            if (pLocalArgs->pu2LastVlan != NULL)
            {
                MEMCPY ((pLocalArgs->pu2LastVlan), &(pRemoteArgs->u2LastVlan),
                        sizeof (UINT2));
            }
            break;
        }
        case FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        {
            tMstpNpWrFsMiMstpNpSetInstancePortState *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpSetInstancePortState *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpSetInstancePortState;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpSetInstancePortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u2InstanceId = pRemoteArgs->u2InstanceId;
            pLocalArgs->u1PortState = pRemoteArgs->u1PortState;
            break;
        }
        case FS_MI_MSTP_NP_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpInitHw *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpInitHw;
            pRemoteArgs = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpInitHw;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
        case FS_MI_MSTP_NP_DE_INIT_HW:
        {
            tMstpNpWrFsMiMstpNpDeInitHw *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstpNpDeInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstpNpDeInitHw;
            pRemoteArgs = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeInitHw;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
        case FS_MI_MST_NP_GET_PORT_STATE:
        {
            tMstpNpWrFsMiMstNpGetPortState *pLocalArgs = NULL;
            tMstpRemoteNpWrFsMiMstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pMstpNpModInfo->MstpNpFsMiMstNpGetPortState;
            pRemoteArgs =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstNpGetPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u2InstanceId = pRemoteArgs->u2InstanceId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu1Status != NULL)
            {
                MEMCPY ((pLocalArgs->pu1Status), &(pRemoteArgs->u1Status),
                        sizeof (UINT1));
            }
            break;
        }
#ifdef  MBSM_WANTED
        case FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE:
        case FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING:
        case FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE:
        case FS_MI_MSTP_MBSM_NP_INIT_HW:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMstpRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMstpNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMstpRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMstpRemoteNpModInfo *pMstpRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pMstpRemoteNpModInfo = &(pRemoteHwNpInput->MstpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_MSTP_NP_CREATE_INSTANCE:
        {
            tMstpRemoteNpWrFsMiMstpNpCreateInstance *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpCreateInstance;
            u1RetVal =
                FsMiMstpNpCreateInstance (pInput->u4ContextId,
                                          pInput->u2InstId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING:
        {
            tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpAddVlanInstMapping;
            u1RetVal =
                FsMiMstpNpAddVlanInstMapping (pInput->u4ContextId,
                                              pInput->VlanId, pInput->u2InstId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING:
        {
            tMstpRemoteNpWrFsMiMstpNpAddVlanListInstMapping *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpAddVlanListInstMapping;
            u1RetVal =
                FsMiMstpNpAddVlanListInstMapping (pInput->u4ContextId,
                                                  &(pInput->au1VlanList[0]),
                                                  pInput->u2InstId,
                                                  pInput->u2NumVlans,
                                                  &(pInput->u2LastVlan));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_DELETE_INSTANCE:
        {
            tMstpRemoteNpWrFsMiMstpNpDeleteInstance *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeleteInstance;
            u1RetVal =
                FsMiMstpNpDeleteInstance (pInput->u4ContextId,
                                          pInput->u2InstId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING:
        {
            tMstpRemoteNpWrFsMiMstpNpDelVlanInstMapping *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDelVlanInstMapping;
            u1RetVal =
                FsMiMstpNpDelVlanInstMapping (pInput->u4ContextId,
                                              pInput->VlanId, pInput->u2InstId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING:
        {
            tMstpRemoteNpWrFsMiMstpNpDelVlanListInstMapping *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpDelVlanListInstMapping;
            u1RetVal =
                FsMiMstpNpDelVlanListInstMapping (pInput->u4ContextId,
                                                  &(pInput->au1VlanList[0]),
                                                  pInput->u2InstId,
                                                  pInput->u2NumVlans,
                                                  &(pInput->u2LastVlan));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        {
            tMstpRemoteNpWrFsMiMstpNpSetInstancePortState *pInput = NULL;
            pInput =
                &pMstpRemoteNpModInfo->
                MstpRemoteNpFsMiMstpNpSetInstancePortState;
            u1RetVal =
                FsMiMstpNpSetInstancePortState (pInput->u4ContextId,
                                                pInput->u4IfIndex,
                                                pInput->u2InstanceId,
                                                pInput->u1PortState);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_INIT_HW:
        {
            tMstpRemoteNpWrFsMiMstpNpInitHw *pInput = NULL;
            pInput = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpInitHw;
            FsMiMstpNpInitHw (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MSTP_NP_DE_INIT_HW:
        {
            tMstpRemoteNpWrFsMiMstpNpDeInitHw *pInput = NULL;
            pInput = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstpNpDeInitHw;
            FsMiMstpNpDeInitHw (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_MST_NP_GET_PORT_STATE:
        {
            tMstpRemoteNpWrFsMiMstNpGetPortState *pInput = NULL;
            pInput = &pMstpRemoteNpModInfo->MstpRemoteNpFsMiMstNpGetPortState;
            u1RetVal =
                FsMiMstNpGetPortState (pInput->u4ContextId,
                                       pInput->u2InstanceId, pInput->u4IfIndex,
                                       &(pInput->u1Status));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* MSTP_WANTED */
#endif /* __MSTUTIL_C__ */
