/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rstnputil.c,v 1.5 2014/10/08 11:01:24 siva Exp $
 *
 * Description:This file contains the single NP RSTP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __RSTUTIL_C__
#define __RSTUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskRstpNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef RSTP_WANTED
VOID
NpUtilMaskRstpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
#ifdef MBSM_WANTED
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpRemoteNpModInfo *pRstpRemoteNpModInfo = NULL;
#endif
    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

#ifdef MBSM_WANTED
    pRstpNpModInfo = &(pFsHwNp->RstpNpModInfo);
    pRstpRemoteNpModInfo = &(pRemoteHwNp->RstpRemoteNpModInfo);
#endif

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_STP_NP_HW_INIT:
        case FS_MI_RSTP_NP_INIT_HW:
#ifdef PBB_WANTED
        case FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS:
#endif
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_RSTP_NP_SET_PORT_STATE:
        case FS_MI_RST_NP_GET_PORT_STATE:
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_NP_SET_PORT_STATE:
        case FS_MI_PB_RST_NP_GET_PORT_STATE:
#endif
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_RSTP_MBSM_NP_SET_PORT_STATE:
        {
            pRemoteHwNp->u4Opcode = FS_MI_RSTP_NP_SET_PORT_STATE;

            tRstpNpWrFsMiRstpMbsmNpSetPortState *pEntry = NULL;
            tRstpRemoteNpWrFsMiRstpNpSetPortState *pRemEntry = NULL;

            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpSetPortState;
            pRemEntry =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpSetPortState;

            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u1Status = pEntry->u1Status;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_RSTP_MBSM_NP_INIT_HW:
        {
            pRemoteHwNp->u4Opcode = FS_MI_RSTP_NP_INIT_HW;

            tRstpNpWrFsMiRstpMbsmNpInitHw *pEntry = NULL;
            tRstpRemoteNpWrFsMiRstpNpInitHw *pRemEntry = NULL;

            pEntry = &pRstpNpModInfo->RstpNpFsMiRstpMbsmNpInitHw;
            pRemEntry = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpInitHw;

            pRemEntry->u4ContextId = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */
#ifdef PB_WANTED
#ifdef MBSM_WANTED
        case FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE:
        {
            pRemoteHwNp->u4Opcode = FS_MI_PB_RSTP_NP_SET_PORT_STATE;

            tRstpNpWrFsMiPbRstpMbsmNpSetPortState *pEntry = NULL;
            tRstpRemoteNpWrFsMiPbRstpNpSetPortState *pRemEntry = NULL;

            pEntry = &pRstpNpModInfo->RstpNpFsMiPbRstpMbsmNpSetPortState;
            pRemEntry =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstpNpSetPortState;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            MEMCPY (&(pRemEntry->SVid), &(pEntry->Svid), sizeof (tVlanId));
            pRemEntry->u1Status = pEntry->u1Status;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */
#endif /* PB_WANTED */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MI_RSTP_MBSM_NP_SET_PORT_STATE) ||
        ((pFsHwNp->u4Opcode > FS_MI_RSTP_MBSM_NP_INIT_HW)))
    {
#ifdef PB_WANTED
        if (FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE != pFsHwNp->u4Opcode)
#endif
            if (MbsmIsNpBulkSyncInProgress (NP_RSTP_MOD) == OSIX_TRUE)
            {
                if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                }
                else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                }

            }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRstpConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRstpConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpRemoteNpModInfo *pRstpRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pRstpNpModInfo = &(pFsHwNp->RstpNpModInfo);
    pRstpRemoteNpModInfo = &(pRemoteHwNp->RstpRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tRstpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_STP_NP_HW_INIT:
        {
            tRstpNpWrFsMiStpNpHwInit *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiStpNpHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiStpNpHwInit;
            pRemoteArgs = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiStpNpHwInit;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_RSTP_NP_INIT_HW:
        {
            tRstpNpWrFsMiRstpNpInitHw *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiRstpNpInitHw *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiRstpNpInitHw;
            pRemoteArgs = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpInitHw;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_MI_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiRstpNpSetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiRstpNpSetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiRstpNpSetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpSetPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiRstNpGetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiRstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiRstNpGetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstNpGetPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->u1Status), (pLocalArgs->pu1Status),
                    sizeof (UINT1));
            break;
        }
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstpNpSetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiPbRstpNpSetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiPbRstpNpSetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstpNpSetPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVid), &(pLocalArgs->SVid),
                    sizeof (tVlanId));
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_MI_PB_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstNpGetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiPbRstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiPbRstNpGetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstNpGetPortState;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->SVlanId), &(pLocalArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY (&(pRemoteArgs->u1Status), (pLocalArgs->pu1Status),
                    sizeof (UINT1));
            break;
        }
#endif /* PB_WANTED */
#ifdef PBB_WANTED
        case FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS:
        {
            tRstpNpWrFsMiPbbRstHwServiceInstancePtToPtStatus *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiPbbRstHwServiceInstancePtToPtStatus *pRemoteArgs
                = NULL;
            pLocalArgs =
                &pRstpNpModInfo->RstpNpFsMiPbbRstHwServiceInstancePtToPtStatus;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->
                RstpRemoteNpFsMiPbbRstHwServiceInstancePtToPtStatus;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4VipIndex = pLocalArgs->u4VipIndex;
            pRemoteArgs->u4Isid = pLocalArgs->u4Isid;
            pRemoteArgs->u1PointToPointStatus =
                pLocalArgs->u1PointToPointStatus;
            break;
        }
#endif /* PBB_WANTED */
#ifdef MBSM_WANTED
        case FS_MI_RSTP_MBSM_NP_SET_PORT_STATE:
        case FS_MI_RSTP_MBSM_NP_INIT_HW:
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE:
#endif /*PB_WANTED */
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRstpConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRstpConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpRemoteNpModInfo *pRstpRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pRstpNpModInfo = &(pFsHwNp->RstpNpModInfo);
    pRstpRemoteNpModInfo = &(pRemoteHwNp->RstpRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiRstpNpSetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiRstpNpSetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiRstpNpSetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpSetPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiRstNpGetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiRstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiRstNpGetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstNpGetPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu1Status != NULL)
            {
                MEMCPY ((pLocalArgs->pu1Status), &(pRemoteArgs->u1Status),
                        sizeof (UINT1));
            }
            break;
        }
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_NP_SET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstpNpSetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiPbRstpNpSetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiPbRstpNpSetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstpNpSetPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVid), &(pRemoteArgs->SVid),
                    sizeof (tVlanId));
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_MI_PB_RST_NP_GET_PORT_STATE:
        {
            tRstpNpWrFsMiPbRstNpGetPortState *pLocalArgs = NULL;
            tRstpRemoteNpWrFsMiPbRstNpGetPortState *pRemoteArgs = NULL;
            pLocalArgs = &pRstpNpModInfo->RstpNpFsMiPbRstNpGetPortState;
            pRemoteArgs =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstNpGetPortState;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->SVlanId), &(pRemoteArgs->SVlanId),
                    sizeof (tVlanId));
            MEMCPY ((pLocalArgs->pu1Status), &(pRemoteArgs->u1Status),
                    sizeof (UINT1));
            break;
        }
#endif /* PB_WANTED */
#ifdef MBSM_WANTED
        case FS_MI_RSTP_MBSM_NP_SET_PORT_STATE:
        case FS_MI_RSTP_MBSM_NP_INIT_HW:
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE:
#endif /*PB_WANTED */
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRstpRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRstpNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRstpRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRstpRemoteNpModInfo *pRstpRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pRstpRemoteNpModInfo = &(pRemoteHwNpInput->RstpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_STP_NP_HW_INIT:
        {
            tRstpRemoteNpWrFsMiStpNpHwInit *pInput = NULL;
            pInput = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiStpNpHwInit;
            u1RetVal = FsMiStpNpHwInit (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_RSTP_NP_INIT_HW:
        {
            tRstpRemoteNpWrFsMiRstpNpInitHw *pInput = NULL;
            pInput = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpInitHw;
            FsMiRstpNpInitHw (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_RSTP_NP_SET_PORT_STATE:
        {
            tRstpRemoteNpWrFsMiRstpNpSetPortState *pInput = NULL;
            pInput = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstpNpSetPortState;
            u1RetVal =
                FsMiRstpNpSetPortState (pInput->u4ContextId, pInput->u4IfIndex,
                                        pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_RST_NP_GET_PORT_STATE:
        {
            tRstpRemoteNpWrFsMiRstNpGetPortState *pInput = NULL;
            pInput = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiRstNpGetPortState;
            u1RetVal =
                FsMiRstNpGetPortState (pInput->u4ContextId, pInput->u4IfIndex,
                                       &(pInput->u1Status));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef PB_WANTED
        case FS_MI_PB_RSTP_NP_SET_PORT_STATE:
        {
            tRstpRemoteNpWrFsMiPbRstpNpSetPortState *pInput = NULL;
            pInput =
                &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstpNpSetPortState;
            u1RetVal =
                FsMiPbRstpNpSetPortState (pInput->u4ContextId,
                                          pInput->u4IfIndex, pInput->SVid,
                                          pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_PB_RST_NP_GET_PORT_STATE:
        {
            tRstpRemoteNpWrFsMiPbRstNpGetPortState *pInput = NULL;
            pInput = &pRstpRemoteNpModInfo->RstpRemoteNpFsMiPbRstNpGetPortState;
            u1RetVal =
                FsMiPbRstNpGetPortState (pInput->u4ContextId, pInput->u4IfIndex,
                                         pInput->SVlanId, &(pInput->u1Status));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* PB_WANTED */
#ifdef PBB_WANTED
        case FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS:
        {
            tRstpRemoteNpWrFsMiPbbRstHwServiceInstancePtToPtStatus *pInput =
                NULL;
            pInput =
                &pRstpRemoteNpModInfo->
                RstpRemoteNpFsMiPbbRstHwServiceInstancePtToPtStatus;
            u1RetVal =
                FsMiPbbRstHwServiceInstancePtToPtStatus (pInput->u4ContextId,
                                                         pInput->u4VipIndex,
                                                         pInput->u4Isid,
                                                         pInput->
                                                         u1PointToPointStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* PBB_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* RSTP_WANTED */
#endif /* __RSTUTIL_C__ */
