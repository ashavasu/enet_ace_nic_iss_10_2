
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsmnputil.c,v 1.1 2013/03/19 12:29:56 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __MBSMNPUTIL_C__
#define __MBSMNPUTIL_C__

#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskMbsmNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskMbsmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tMbsmNpModInfo       *pMbsmNpModInfo = NULL;
    tMbsmRemoteNpModInfo *pMbsmRemoteNpModInfo = NULL;

    pMbsmNpModInfo = &pFsHwNp->MbsmNpModInfo;
    pMbsmRemoteNpModInfo = &pRemoteHwNp->MbsmRemoteNpModInfo;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
#ifdef L2RED_WANTED
        case MBSM_NP_PROC_RM_NODE_TRANSITION:
	case MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE:
        case MBSM_NP_GET_SLOT_INFO:
#endif /* L2RED_WANTED */
        case MBSM_NP_INIT_HW_TOPO_DISC:
	case MBSM_NP_CLEAR_HW_TBL:
	case MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING:
	case MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING:
	case MBS_NP_SET_LOAD_SHARING_STATUS:
	case MBSM_NP_GET_CARD_TYPE_TABLE:
	case MBSM_NP_HANDLE_NODE_TRANSITION:
	case MBSM_NP_TX_ON_STACK_INTERFACE:
	case MBSM_NP_GET_STACK_MAC:
	     {
		*pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
		break;
	     }
        default:
            break;
    }
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMbsmConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMbsmConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmRemoteNpModInfo *pMbsmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMbsmNpModInfo = &(pFsHwNp->MbsmNpModInfo);
    pMbsmRemoteNpModInfo = &(pRemoteHwNp->MbsmRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tMbsmRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case MBSM_NP_PROC_RM_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpProcRmNodeTransition *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpProcRmNodeTransition *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpProcRmNodeTransition;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpProcRmNodeTransition;
            pRemoteArgs->i4Event = pLocalArgs->i4Event;
            pRemoteArgs->u1PrevState = pLocalArgs->u1PrevState;
            pRemoteArgs->u1State = pLocalArgs->u1State;
            break;
        }
        case MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE:
        {
            tMbsmNpWrMbsmInitNpInfoOnStandbyToActive *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmInitNpInfoOnStandbyToActive *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmInitNpInfoOnStandbyToActive;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmInitNpInfoOnStandbyToActive;
            if (pLocalArgs->pSlotInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->SlotInfo), (pLocalArgs->pSlotInfo),
                        sizeof (tMbsmSlotInfo));
            }
            break;
        }
        case MBSM_NP_GET_SLOT_INFO:
        {
            tMbsmNpWrMbsmNpGetSlotInfo *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetSlotInfo *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetSlotInfo;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetSlotInfo;
            pRemoteArgs->i4SlotId = pLocalArgs->i4SlotId;
            if (pLocalArgs->pHwMsg != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwMsg), (pLocalArgs->pHwMsg),
                        sizeof (tMbsmHwMsg));
            }
            break;
        }
#endif /* L2RED_WANTED */
        case MBSM_NP_INIT_HW_TOPO_DISC:
        {
            tMbsmNpWrMbsmNpInitHwTopoDisc *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpInitHwTopoDisc *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpInitHwTopoDisc;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpInitHwTopoDisc;
            pRemoteArgs->i4SlotId = pLocalArgs->i4SlotId;
            pRemoteArgs->i1NodeState = pLocalArgs->i1NodeState;
            break;
        }
        case MBSM_NP_CLEAR_HW_TBL:
        {
            tMbsmNpWrMbsmNpClearHwTbl *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpClearHwTbl *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpClearHwTbl;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpClearHwTbl;
            pRemoteArgs->i4SlotId = pLocalArgs->i4SlotId;
            break;
        }
        case MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpProcCardInsertForLoadSharing *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpProcCardInsertForLoadSharing *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pMbsmNpModInfo->MbsmNpMbsmNpProcCardInsertForLoadSharing;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpProcCardInsertForLoadSharing;
            pRemoteArgs->i4MsgType = pLocalArgs->i4MsgType;
            break;
        }
        case MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpUpdtHwTblForLoadSharing *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpUpdtHwTblForLoadSharing;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpUpdtHwTblForLoadSharing;
            pRemoteArgs->u1Flag = pLocalArgs->u1Flag;
            break;
        }
        case MBS_NP_SET_LOAD_SHARING_STATUS:
        {
            tMbsmNpWrMbsNpSetLoadSharingStatus *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsNpSetLoadSharingStatus *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsNpSetLoadSharingStatus;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsNpSetLoadSharingStatus;
            pRemoteArgs->i4LoadSharingFlag = pLocalArgs->i4LoadSharingFlag;
            break;
        }
        case MBSM_NP_GET_CARD_TYPE_TABLE:
        {
            tMbsmNpWrMbsmNpGetCardTypeTable *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetCardTypeTable *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetCardTypeTable;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetCardTypeTable;
            pRemoteArgs->i4LoopIdx = pLocalArgs->i4LoopIdx;
            if (pLocalArgs->pMbsmCardInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MbsmCardInfo),
                        (pLocalArgs->pMbsmCardInfo), sizeof (tMbsmCardInfo));
            }
            break;
        }
        case MBSM_NP_HANDLE_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpHandleNodeTransition *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpHandleNodeTransition *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpHandleNodeTransition;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpHandleNodeTransition;
            pRemoteArgs->i4Event = pLocalArgs->i4Event;
            pRemoteArgs->u1PrevState = pLocalArgs->u1PrevState;
            pRemoteArgs->u1State = pLocalArgs->u1State;
            break;
        }
        case MBSM_NP_TX_ON_STACK_INTERFACE:
        {
	    /* No Need of Local-Remote Conversion.
 	     * This call never be executed in Remote Unit */
            break;
        }
        case MBSM_NP_GET_STACK_MAC:
        {
            tMbsmNpWrMbsmNpGetStackMac *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetStackMac *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetStackMac;
            pRemoteArgs->u4SlotId = pLocalArgs->u4SlotId;
            if (pLocalArgs->pu1MacAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1MacAddr), (pLocalArgs->pu1MacAddr),
                        sizeof (UINT1) * MAC_LEN);
            }
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMbsmConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMbsmConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmRemoteNpModInfo *pMbsmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pMbsmNpModInfo = &(pFsHwNp->MbsmNpModInfo);
    pMbsmRemoteNpModInfo = &(pRemoteHwNp->MbsmRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case MBSM_NP_PROC_RM_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpProcRmNodeTransition *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpProcRmNodeTransition *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpProcRmNodeTransition;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpProcRmNodeTransition;
            pLocalArgs->i4Event = pRemoteArgs->i4Event;
            pLocalArgs->u1PrevState = pRemoteArgs->u1PrevState;
            pLocalArgs->u1State = pRemoteArgs->u1State;
            break;
        }
        case MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE:
        {
            tMbsmNpWrMbsmInitNpInfoOnStandbyToActive *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmInitNpInfoOnStandbyToActive *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmInitNpInfoOnStandbyToActive;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmInitNpInfoOnStandbyToActive;
            if (pLocalArgs->pSlotInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pSlotInfo), &(pRemoteArgs->SlotInfo),
                        sizeof (tMbsmSlotInfo));
            }
            break;
        }
        case MBSM_NP_GET_SLOT_INFO:
        {
            tMbsmNpWrMbsmNpGetSlotInfo *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetSlotInfo *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetSlotInfo;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetSlotInfo;
            pLocalArgs->i4SlotId = pRemoteArgs->i4SlotId;
            if (pLocalArgs->pHwMsg != NULL)
            {
                MEMCPY ((pLocalArgs->pHwMsg), &(pRemoteArgs->HwMsg),
                        sizeof (tMbsmHwMsg));
            }
            break;
        }
#endif /* L2RED_WANTED */
        case MBSM_NP_INIT_HW_TOPO_DISC:
        {
            tMbsmNpWrMbsmNpInitHwTopoDisc *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpInitHwTopoDisc *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpInitHwTopoDisc;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpInitHwTopoDisc;
            pLocalArgs->i4SlotId = pRemoteArgs->i4SlotId;
            pLocalArgs->i1NodeState = pRemoteArgs->i1NodeState;
            break;
        }
        case MBSM_NP_CLEAR_HW_TBL:
        {
            tMbsmNpWrMbsmNpClearHwTbl *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpClearHwTbl *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpClearHwTbl;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpClearHwTbl;
            pLocalArgs->i4SlotId = pRemoteArgs->i4SlotId;
            break;
        }
        case MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpProcCardInsertForLoadSharing *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpProcCardInsertForLoadSharing *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pMbsmNpModInfo->MbsmNpMbsmNpProcCardInsertForLoadSharing;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpProcCardInsertForLoadSharing;
            pLocalArgs->i4MsgType = pRemoteArgs->i4MsgType;
            break;
        }
        case MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpUpdtHwTblForLoadSharing *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpUpdtHwTblForLoadSharing;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpUpdtHwTblForLoadSharing;
            pLocalArgs->u1Flag = pRemoteArgs->u1Flag;
            break;
        }
        case MBS_NP_SET_LOAD_SHARING_STATUS:
        {
            tMbsmNpWrMbsNpSetLoadSharingStatus *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsNpSetLoadSharingStatus *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsNpSetLoadSharingStatus;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsNpSetLoadSharingStatus;
            pLocalArgs->i4LoadSharingFlag = pRemoteArgs->i4LoadSharingFlag;
            break;
        }
        case MBSM_NP_GET_CARD_TYPE_TABLE:
        {
            tMbsmNpWrMbsmNpGetCardTypeTable *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetCardTypeTable *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetCardTypeTable;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetCardTypeTable;
            pLocalArgs->i4LoopIdx = pRemoteArgs->i4LoopIdx;
            if (pLocalArgs->pMbsmCardInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pMbsmCardInfo),
                        &(pRemoteArgs->MbsmCardInfo), sizeof (tMbsmCardInfo));
            }
            break;
        }
        case MBSM_NP_HANDLE_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpHandleNodeTransition *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpHandleNodeTransition *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpHandleNodeTransition;
            pRemoteArgs =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpHandleNodeTransition;
            pLocalArgs->i4Event = pRemoteArgs->i4Event;
            pLocalArgs->u1PrevState = pRemoteArgs->u1PrevState;
            pLocalArgs->u1State = pRemoteArgs->u1State;
            break;
        }
        case MBSM_NP_TX_ON_STACK_INTERFACE:
        {
	    /* No Need of Remote-Local Conversion.
 	     * This call never be executed in Remote Unit */
            break;
        }
        case MBSM_NP_GET_STACK_MAC:
        {
            tMbsmNpWrMbsmNpGetStackMac *pLocalArgs = NULL;
            tMbsmRemoteNpWrMbsmNpGetStackMac *pRemoteArgs = NULL;
            pLocalArgs = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;
            pRemoteArgs = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetStackMac;
            pLocalArgs->u4SlotId = pRemoteArgs->u4SlotId;
            if (pLocalArgs->pu1MacAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1MacAddr), &(pRemoteArgs->u1MacAddr),
                        sizeof (UINT1) * MAC_LEN);
            }
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMbsmRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMbsmNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMbsmRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMbsmRemoteNpModInfo *pMbsmRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pMbsmRemoteNpModInfo = &(pRemoteHwNpInput->MbsmRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case MBSM_NP_PROC_RM_NODE_TRANSITION:
        {
            tMbsmRemoteNpWrMbsmNpProcRmNodeTransition *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpProcRmNodeTransition;
            u1RetVal =
                MbsmNpProcRmNodeTransition (pInput->i4Event,
                                            pInput->u1PrevState,
                                            pInput->u1State);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE:
        {
            tMbsmRemoteNpWrMbsmInitNpInfoOnStandbyToActive *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmInitNpInfoOnStandbyToActive;
            u1RetVal = MbsmInitNpInfoOnStandbyToActive (&(pInput->SlotInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_GET_SLOT_INFO:
        {
            tMbsmRemoteNpWrMbsmNpGetSlotInfo *pInput = NULL;
            pInput = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetSlotInfo;
            u1RetVal = MbsmNpGetSlotInfo (pInput->i4SlotId, &(pInput->HwMsg));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* L2RED_WANTED */
        case MBSM_NP_INIT_HW_TOPO_DISC:
        {
            tMbsmRemoteNpWrMbsmNpInitHwTopoDisc *pInput = NULL;
            pInput = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpInitHwTopoDisc;
            u1RetVal =
                MbsmNpInitHwTopoDisc (pInput->i4SlotId, pInput->i1NodeState);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_CLEAR_HW_TBL:
        {
            tMbsmRemoteNpWrMbsmNpClearHwTbl *pInput = NULL;
            pInput = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpClearHwTbl;
            u1RetVal = MbsmNpClearHwTbl (pInput->i4SlotId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING:
        {
            tMbsmRemoteNpWrMbsmNpProcCardInsertForLoadSharing *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpProcCardInsertForLoadSharing;
            u1RetVal = MbsmNpProcCardInsertForLoadSharing (pInput->i4MsgType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING:
        {
            tMbsmRemoteNpWrMbsmNpUpdtHwTblForLoadSharing *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->
                MbsmRemoteNpMbsmNpUpdtHwTblForLoadSharing;
            u1RetVal = MbsmNpUpdtHwTblForLoadSharing (pInput->u1Flag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBS_NP_SET_LOAD_SHARING_STATUS:
        {
            tMbsmRemoteNpWrMbsNpSetLoadSharingStatus *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsNpSetLoadSharingStatus;
            u1RetVal = MbsNpSetLoadSharingStatus (pInput->i4LoadSharingFlag);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_GET_CARD_TYPE_TABLE:
        {
            tMbsmRemoteNpWrMbsmNpGetCardTypeTable *pInput = NULL;
            pInput = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetCardTypeTable;
            u1RetVal =
                MbsmNpGetCardTypeTable (pInput->i4LoopIdx,
                                        &(pInput->MbsmCardInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_HANDLE_NODE_TRANSITION:
        {
            tMbsmRemoteNpWrMbsmNpHandleNodeTransition *pInput = NULL;
            pInput =
                &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpHandleNodeTransition;
            u1RetVal =
                MbsmNpHandleNodeTransition (pInput->i4Event,
                                            pInput->u1PrevState,
                                            pInput->u1State);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case MBSM_NP_TX_ON_STACK_INTERFACE:
        {
	    /* This call should not be executed in Remote Unit.
 	     * Hence Return Failure*/
            u1RetVal = FNP_FAILURE;
            break;
        }
        case MBSM_NP_GET_STACK_MAC:
        {
            tMbsmRemoteNpWrMbsmNpGetStackMac *pInput = NULL;
            pInput = &pMbsmRemoteNpModInfo->MbsmRemoteNpMbsmNpGetStackMac;
            MbsmNpGetStackMac (pInput->u4SlotId, (pInput->u1MacAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __MBSMNPUTIL_C__ */
