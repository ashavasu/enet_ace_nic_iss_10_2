/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnputil.c,v 1.6 2016/06/14 12:28:15 siva Exp $
 *
 * Description:This file contains the NP MC utility functions of IP
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __IPMC_NPUTIL_C__
#define __IPMC_NPUTIL_C__

#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskIpmcNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIpmcNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
#ifdef MBSM_WANTED
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcRemoteNpModInfo *pIpmcRemoteNpModInfo = NULL;

    pIpmcNpModInfo = &pFsHwNp->IpmcNpModInfo;
    pIpmcRemoteNpModInfo = &pRemoteHwNp->IpmcRemoteNpModInfo;
#endif
    UNUSED_PARAM (pi4RpcCallStatus);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;


    switch (pFsHwNp->u4Opcode)
    {
	case FS_NP_IPV4_MC_INIT:
	case FS_NP_IPV4_MC_DE_INIT:
#ifdef PIM_WANTED
	case FS_PIM_NP_INIT_HW:
	case FS_PIM_NP_DE_INIT_HW:
	case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
#endif
#ifdef DVMRP_WANTED
	case FS_DVMRP_NP_INIT_HW:
	case FS_DVMRP_NP_DE_INIT_HW:
#endif
#ifdef IGMPPRXY_WANTED
	case FS_NP_IGMP_PROXY_INIT:
	case FS_NP_IGMP_PROXY_DE_INIT:
#endif
	case FS_NP_IPV4_VLAN_MC_INIT:
	case FS_NP_IPV4_VLAN_MC_DE_INIT:
	case FS_NP_IPV4_RPORT_MC_INIT:
	case FS_NP_IPV4_RPORT_MC_DE_INIT:
	case FS_NP_IPV4_MC_ADD_CPU_PORT:
	case FS_NP_IPV4_MC_DEL_CPU_PORT:
	case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
	case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
	case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
	case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
	case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
	case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
	case FS_NP_IPV4_MC_RPF_D_F_INFO: 
	case FS_NP_IPV4_MC_GET_HIT_STATUS:
	case FS_NP_IPV4_GET_M_ROUTE_STATS:
	case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
	case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
	case FS_NP_IPV4_GET_M_IFACE_STATS: 
	case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
	{
	    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
	    break;
	}
	case FS_NP_IPV_X_HW_GET_MCAST_ENTRY:
	{
	    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
	    break;
	}
#ifdef MBSM_WANTED
	case FS_NP_IPV4_MBSM_MC_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_MC_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
	case FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4MbsmMcAddRouteEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McAddRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4MbsmMcAddRouteEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McAddRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            pRemoteArgs->u1CallerId = pLocalArgs->u1CallerId;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf),
                        (pLocalArgs->pDownStreamIf), 
                        (sizeof (tMcDownStreamIf) * pLocalArgs->u2NoOfDownStreamIf));
            }
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_MC_ADD_ROUTE_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_MBSM_NP_INIT_HW:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_PIM_NP_INIT_HW;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif
#ifdef DVMRP_WANTED
        case FS_DVMRP_MBSM_NP_INIT_HW:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_DVMRP_NP_INIT_HW;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif
#else

    UNUSED_PARAM (pRemoteHwNp);
#endif
	default:
	    break;	
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_NP_IPV4_MBSM_MC_INIT) ||
        ((pFsHwNp->u4Opcode > FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_IPMC_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return;
}
/*******************************************************************************
   * Function    : NpDbgPrintTime ()
   * Description : This routine prints the TimeStamp
   * Input(s)    : u4Flag - Current value of the trace flag
   *               u4Value - Value against which the flag is to be compared.
   * Output(s)   : None
   * Globals     : Not referred or modified
   * Returns     : VOID
   ******************************************************************************/

PUBLIC VOID
NpDbgPrintTime (UINT4 u4Flag)
{
#ifdef FS_NPAPI
    time_t              T;
    struct tm          *pTime = NULL;
    UINT4 u4AscTimeLen =0;
    UINT1 au1str[FS_NP_DBG_TIME_LEN];

    time (&T);
    pTime = localtime (&T);
                if (pTime != NULL)
                {
                    u4AscTimeLen = STRLEN (asctime (pTime));
                    STRNCPY (au1str, asctime (pTime), u4AscTimeLen - 1);
                    au1str[u4AscTimeLen - 1] = '\0';
                }

    if (!(u4Flag))
    {
        return;
    }
    else if (au1str != NULL )

    {
        PRINTF ("%s ", au1str);
    }
#endif
    return;

}

/***************************************************************************/
/*  Function Name       : NpUtilIpmcMergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                        */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp                */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value          */
/*                    pi4RemoteNpRetVal - Remote Np Return Value           */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilIpmcMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp,
                                    INT4 *pi4LocalNpRetVal,
                                    INT4 *pi4RemoteNpRetVal)
{
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcRemoteNpModInfo *pIpmcRemoteNpModInfo = NULL;
    UINT1               u1RetVal = FNP_SUCCESS;

    pIpmcNpModInfo = &pFsHwNp->IpmcNpModInfo;
    pIpmcRemoteNpModInfo = &pRemoteHwNp->IpmcRemoteNpModInfo;

    switch (pFsHwNp->u4Opcode)
    {
	case FS_NP_IPV4_MC_INIT:
	case FS_NP_IPV4_MC_DE_INIT:
#ifdef PIM_WANTED
	case FS_PIM_NP_INIT_HW:
	case FS_PIM_NP_DE_INIT_HW:
	case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
#endif
#ifdef DVMRP_WANTED
	case FS_DVMRP_NP_INIT_HW:
	case FS_DVMRP_NP_DE_INIT_HW:
#endif
#ifdef IGMPPRXY_WANTED
	case FS_NP_IGMP_PROXY_INIT:
	case FS_NP_IGMP_PROXY_DE_INIT:
#endif
	case FS_NP_IPV4_VLAN_MC_INIT:
	case FS_NP_IPV4_VLAN_MC_DE_INIT:
	case FS_NP_IPV4_RPORT_MC_INIT:
	case FS_NP_IPV4_RPORT_MC_DE_INIT:
	case FS_NP_IPV4_MC_ADD_CPU_PORT:
	case FS_NP_IPV4_MC_DEL_CPU_PORT:
	case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
	case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
	case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
	case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
	case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
	case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
	case FS_NP_IPV4_MC_RPF_D_F_INFO: 
	{
	    break;
	}
	case FS_NP_IPV4_MC_GET_HIT_STATUS:
	{
            tIpmcNpWrFsNpIpv4McGetHitStatus *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McGetHitStatus;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McGetHitStatus;
	    (*pLocalArgs->pu4HitStatus) = 
		(pRemoteArgs->u4HitStatus) | (*pLocalArgs->pu4HitStatus);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV4_GET_M_ROUTE_STATS:
	{
            tIpmcNpWrFsNpIpv4GetMRouteStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
	{
            tIpmcNpWrFsNpIpv4GetMRouteHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteHCStats;
	    (pLocalArgs->pu8RetValue->msn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.msn) + (pLocalArgs->pu8RetValue->msn));
	    (pLocalArgs->pu8RetValue->lsn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.lsn) + (pLocalArgs->pu8RetValue->lsn));
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
	{
            tIpmcNpWrFsNpIpv4GetMNextHopStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMNextHopStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMNextHopStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV4_GET_M_IFACE_STATS: 
	{
            tIpmcNpWrFsNpIpv4GetMIfaceStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceStats;
	    (*pLocalArgs->pu4RetValue) = 
		(pRemoteArgs->u4RetValue) + (*pLocalArgs->pu4RetValue);
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
	case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
	{
            tIpmcNpWrFsNpIpv4GetMIfaceHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceHCStats;
	    (pLocalArgs->pu8RetValue->msn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.msn) + (pLocalArgs->pu8RetValue->msn));
	    (pLocalArgs->pu8RetValue->lsn) = 
		(UINT4) ((pRemoteArgs->U8RetValue.lsn) + (pLocalArgs->pu8RetValue->lsn));
	    *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
        default:
            break;
    }
    return u1RetVal;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpmcConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpmcConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcRemoteNpModInfo *pIpmcRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIpmcNpModInfo = &(pFsHwNp->IpmcNpModInfo);
    pIpmcRemoteNpModInfo = &(pRemoteHwNp->IpmcRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIpmcRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef FS_NPAPI
#if defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
        case FS_NP_IPV4_MC_INIT:
        {
            break;
        }
        case FS_NP_IPV4_MC_DE_INIT:
        {
            break;
        }
        case FS_NP_IPV4_VLAN_MC_INIT:
        {
            tIpmcNpWrFsNpIpv4VlanMcInit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4VlanMcInit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcInit;
            pRemoteArgs = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4VlanMcInit;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            break;
        }
        case FS_NP_IPV4_VLAN_MC_DE_INIT:
        {
            tIpmcNpWrFsNpIpv4VlanMcDeInit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4VlanMcDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcDeInit;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4VlanMcDeInit;
            pRemoteArgs->u4Index = pLocalArgs->u4Index;
            break;
        }
        case FS_NP_IPV4_RPORT_MC_INIT:
        {
            tIpmcNpWrFsNpIpv4RportMcInit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4RportMcInit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcInit;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4RportMcInit;
            MEMCPY (&(pRemoteArgs->PortInfo), &(pLocalArgs->PortInfo),
                    sizeof (tRPortInfo));
            break;
        }
        case FS_NP_IPV4_RPORT_MC_DE_INIT:
        {
            tIpmcNpWrFsNpIpv4RportMcDeInit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4RportMcDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcDeInit;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4RportMcDeInit;
            MEMCPY (&(pRemoteArgs->PortInfo), &(pLocalArgs->PortInfo),
                    sizeof (tRPortInfo));
            break;
        }
        case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McAddRouteEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McAddRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddRouteEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McAddRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            pRemoteArgs->u1CallerId = pLocalArgs->u1CallerId;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf), (pLocalArgs->pDownStreamIf),
                        (sizeof (tMcDownStreamIf) * pLocalArgs->u2NoOfDownStreamIf));
            }
            break;
        }
        case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McDelRouteEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McDelRouteEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelRouteEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McDelRouteEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4GrpPrefix = pLocalArgs->u4GrpPrefix;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            pRemoteArgs->u4SrcIpPrefix = pLocalArgs->u4SrcIpPrefix;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            pRemoteArgs->u2NoOfDownStreamIf = pLocalArgs->u2NoOfDownStreamIf;
            if (pLocalArgs->pDownStreamIf != NULL)
            {
                MEMCPY (&(pRemoteArgs->DownStreamIf),
                        (pLocalArgs->pDownStreamIf), sizeof (tMcDownStreamIf));
            }
            break;
        }
        case FS_NP_IPV4_MC_CLEAR_ALL_ROUTES:
        {
            tIpmcNpWrFsNpIpv4McClearAllRoutes *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McClearAllRoutes *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearAllRoutes;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McClearAllRoutes;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            break;
        }
        case FS_NP_IPV4_MC_ADD_CPU_PORT:
        {
            tIpmcNpWrFsNpIpv4McAddCpuPort *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McAddCpuPort *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddCpuPort;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McAddCpuPort;
            pRemoteArgs->u2GenRtrId = pLocalArgs->u2GenRtrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            break;
        }
        case FS_NP_IPV4_MC_DEL_CPU_PORT:
        {
            tIpmcNpWrFsNpIpv4McDelCpuPort *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McDelCpuPort *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelCpuPort;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McDelCpuPort;
            pRemoteArgs->u2GenRtrId = pLocalArgs->u2GenRtrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            break;
        }
        case FS_NP_IPV4_MC_GET_HIT_STATUS:
        {
            tIpmcNpWrFsNpIpv4McGetHitStatus *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McGetHitStatus;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McGetHitStatus;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4Iif = pLocalArgs->u4Iif;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->pu4HitStatus != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4HitStatus), (pLocalArgs->pu4HitStatus),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McUpdateOifVlanEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McUpdateOifVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateOifVlanEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McUpdateOifVlanEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            MEMCPY (&(pRemoteArgs->downStreamIf), &(pLocalArgs->downStreamIf),
                    sizeof (tMcDownStreamIf));
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McUpdateIifVlanEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McUpdateIifVlanEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateIifVlanEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McUpdateIifVlanEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcIpAddr = pLocalArgs->u4SrcIpAddr;
            MEMCPY (&(pRemoteArgs->rtEntry), &(pLocalArgs->rtEntry),
                    sizeof (tMcRtEntry));
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteHCStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValue), (pLocalArgs->pu8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMNextHopStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMNextHopStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMNextHopStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            pRemoteArgs->i4OutIfIndex = pLocalArgs->i4OutIfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4RetValue), (pLocalArgs->pu4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceHCStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8RetValue), (pLocalArgs->pu8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIpmcNpWrFsNpIpv4SetMIfaceTtlTreshold *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4SetMIfaceTtlTreshold *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceTtlTreshold;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4SetMIfaceTtlTreshold;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4TtlTreshold = pLocalArgs->i4TtlTreshold;
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
        {
            tIpmcNpWrFsNpIpv4SetMIfaceRateLimit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4SetMIfaceRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceRateLimit;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4SetMIfaceRateLimit;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4RateLimit = pLocalArgs->i4RateLimit;
            break;
        }
        case FS_NP_IPV_X_HW_GET_MCAST_ENTRY:
        {
            tIpmcNpWrFsNpIpvXHwGetMcastEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpvXHwGetMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpvXHwGetMcastEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpvXHwGetMcastEntry;
            if (pLocalArgs->pNpL3McastEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->NpL3McastEntry),
                        (pLocalArgs->pNpL3McastEntry),
                        sizeof (tNpL3McastEntry));
            }
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_NP_INIT_HW:
        {
            break;
        }
        case FS_PIM_NP_DE_INIT_HW:
        {
            break;
        }
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_NP_INIT_HW:
        {
            break;
        }
        case FS_DVMRP_NP_DE_INIT_HW:
        {
            break;
        }
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
        case FS_NP_IGMP_PROXY_INIT:
        {
            break;
        }
        case FS_NP_IGMP_PROXY_DE_INIT:
        {
            break;
        }
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
        case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
        {
            tIpmcNpWrFsNpIpv4McClearHitBit *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McClearHitBit *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearHitBit;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McClearHitBit;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            break;
        }
#endif
        case FS_NP_IPV4_MC_RPF_D_F_INFO:
        {
            tIpmcNpWrFsNpIpv4McRpfDFInfo *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McRpfDFInfo *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McRpfDFInfo;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McRpfDFInfo;
            if (pLocalArgs->pMcRpfDFInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->McRpfDFInfo), (pLocalArgs->pMcRpfDFInfo),
                        sizeof (tMcRpfDFInfo));
            }
            break;
        }
#endif
#endif
#ifdef MBSM_WANTED
        case FS_NP_IPV4_MBSM_MC_INIT:
        case FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY:
#ifdef PIM_WANTED
        case FS_PIM_MBSM_NP_INIT_HW:
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_MBSM_NP_INIT_HW:
#endif /* DVMRP_WANTED */
	{
	    break;
	}
#endif
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpmcConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpmcConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcRemoteNpModInfo *pIpmcRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pIpmcNpModInfo = &(pFsHwNp->IpmcNpModInfo);
    pIpmcRemoteNpModInfo = &(pRemoteHwNp->IpmcRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef FS_NPAPI
#if defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
        case FS_NP_IPV4_MC_INIT:
        case FS_NP_IPV4_MC_DE_INIT:
        case FS_NP_IPV4_VLAN_MC_INIT:
        case FS_NP_IPV4_VLAN_MC_DE_INIT:
        case FS_NP_IPV4_RPORT_MC_INIT:
        case FS_NP_IPV4_RPORT_MC_DE_INIT:
        case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
        case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
        case FS_NP_IPV4_MC_CLEAR_ALL_ROUTES:
        case FS_NP_IPV4_MC_ADD_CPU_PORT:
        case FS_NP_IPV4_MC_DEL_CPU_PORT:
        case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
        case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
        case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
        case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
        {
            break;
        }

        case FS_NP_IPV4_MC_GET_HIT_STATUS:
        {
            tIpmcNpWrFsNpIpv4McGetHitStatus *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4McGetHitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4McGetHitStatus;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McGetHitStatus;
            pLocalArgs->u4SrcIpAddr = pRemoteArgs->u4SrcIpAddr;
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4Iif = pRemoteArgs->u4Iif;
            pLocalArgs->u2VlanId = pRemoteArgs->u2VlanId;
            if (pLocalArgs->pu4HitStatus != NULL)
            {
                MEMCPY ((pLocalArgs->pu4HitStatus), &(pRemoteArgs->u4HitStatus),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4SrcAddr = pRemoteArgs->u4SrcAddr;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteHCStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4SrcAddr = pRemoteArgs->u4SrcAddr;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValue), &(pRemoteArgs->U8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMNextHopStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMNextHopStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMNextHopStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4SrcAddr = pRemoteArgs->u4SrcAddr;
            pLocalArgs->i4OutIfIndex = pRemoteArgs->i4OutIfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu4RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu4RetValue), &(pRemoteArgs->u4RetValue),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceHCStats *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceHCStats;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceHCStats;
            pLocalArgs->u4VrId = pRemoteArgs->u4VrId;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4StatType = pRemoteArgs->i4StatType;
            if (pLocalArgs->pu8RetValue != NULL)
            {
                MEMCPY ((pLocalArgs->pu8RetValue), &(pRemoteArgs->U8RetValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_NP_IPV_X_HW_GET_MCAST_ENTRY:
        {
            tIpmcNpWrFsNpIpvXHwGetMcastEntry *pLocalArgs = NULL;
            tIpmcRemoteNpWrFsNpIpvXHwGetMcastEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpmcNpModInfo->IpmcNpFsNpIpvXHwGetMcastEntry;
            pRemoteArgs =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpvXHwGetMcastEntry;
            if (pLocalArgs->pNpL3McastEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pNpL3McastEntry),
                        &(pRemoteArgs->NpL3McastEntry),
                        sizeof (tNpL3McastEntry));
            }
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_NP_INIT_HW:
        case FS_PIM_NP_DE_INIT_HW:
        {
            break;
        }
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_NP_INIT_HW:
        case FS_DVMRP_NP_DE_INIT_HW:
        {
            break;
        }
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
        case FS_NP_IGMP_PROXY_INIT:
        case FS_NP_IGMP_PROXY_DE_INIT:
        {
            break;
        }
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
        case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
        {
            break;
        }
#endif
        case FS_NP_IPV4_MC_RPF_D_F_INFO:
        {
            break;
        }
#endif
#endif
#ifdef MBSM_WANTED
        case FS_NP_IPV4_MBSM_MC_INIT:
        case FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY:
#ifdef PIM_WANTED
        case FS_PIM_MBSM_NP_INIT_HW:
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_MBSM_NP_INIT_HW:
#endif /* DVMRP_WANTED */
#endif
	{
	    break;
	}
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpmcRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpmcNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpmcRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpmcRemoteNpModInfo *pIpmcRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIpmcRemoteNpModInfo = &(pRemoteHwNpInput->IpmcRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef FS_NPAPI
#if defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
        case FS_NP_IPV4_MC_INIT:
        {
            u1RetVal = FsNpIpv4McInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_DE_INIT:
        {
            u1RetVal = FsNpIpv4McDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VLAN_MC_INIT:
        {
            tIpmcRemoteNpWrFsNpIpv4VlanMcInit *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4VlanMcInit;
            u1RetVal = FsNpIpv4VlanMcInit (pInput->u4VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VLAN_MC_DE_INIT:
        {
            tIpmcRemoteNpWrFsNpIpv4VlanMcDeInit *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4VlanMcDeInit;
            u1RetVal = FsNpIpv4VlanMcDeInit (pInput->u4Index);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_RPORT_MC_INIT:
        {
            tIpmcRemoteNpWrFsNpIpv4RportMcInit *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4RportMcInit;
            u1RetVal = FsNpIpv4RportMcInit (pInput->PortInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_RPORT_MC_DE_INIT:
        {
            tIpmcRemoteNpWrFsNpIpv4RportMcDeInit *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4RportMcDeInit;
            u1RetVal = FsNpIpv4RportMcDeInit (pInput->PortInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
        {
            tIpmcRemoteNpWrFsNpIpv4McAddRouteEntry *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McAddRouteEntry;
            u1RetVal =
                FsNpIpv4McAddRouteEntry (pInput->u4VrId, pInput->u4GrpAddr,
                                         pInput->u4GrpPrefix,
                                         pInput->u4SrcIpAddr,
                                         pInput->u4SrcIpPrefix,
                                         pInput->u1CallerId, pInput->rtEntry,
                                         pInput->u2NoOfDownStreamIf,
                                         (tMcDownStreamIf *)&(pInput->DownStreamIf));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
        {
            tIpmcRemoteNpWrFsNpIpv4McDelRouteEntry *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McDelRouteEntry;
            u1RetVal =
                FsNpIpv4McDelRouteEntry (pInput->u4VrId, pInput->u4GrpAddr,
                                         pInput->u4GrpPrefix,
                                         pInput->u4SrcIpAddr,
                                         pInput->u4SrcIpPrefix, pInput->rtEntry,
                                         pInput->u2NoOfDownStreamIf,
                                         &(pInput->DownStreamIf));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_CLEAR_ALL_ROUTES:
        {
            tIpmcRemoteNpWrFsNpIpv4McClearAllRoutes *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McClearAllRoutes;
            FsNpIpv4McClearAllRoutes (pInput->u4VrId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_ADD_CPU_PORT:
        {
            tIpmcRemoteNpWrFsNpIpv4McAddCpuPort *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McAddCpuPort;
            u1RetVal =
                FsNpIpv4McAddCpuPort (pInput->u2GenRtrId, pInput->u4GrpAddr,
                                      pInput->u4SrcAddr, pInput->rtEntry);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_DEL_CPU_PORT:
        {
            tIpmcRemoteNpWrFsNpIpv4McDelCpuPort *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McDelCpuPort;
            u1RetVal =
                FsNpIpv4McDelCpuPort (pInput->u2GenRtrId, pInput->u4GrpAddr,
                                      pInput->u4SrcAddr, pInput->rtEntry);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_GET_HIT_STATUS:
        {
            tIpmcRemoteNpWrFsNpIpv4McGetHitStatus *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McGetHitStatus;
            FsNpIpv4McGetHitStatus (pInput->u4SrcIpAddr, pInput->u4GrpAddr,
                                    pInput->u4Iif, pInput->u2VlanId,
                                    &(pInput->u4HitStatus));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIpmcRemoteNpWrFsNpIpv4McUpdateOifVlanEntry *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McUpdateOifVlanEntry;
            FsNpIpv4McUpdateOifVlanEntry (pInput->u4VrId, pInput->u4GrpAddr,
                                          pInput->u4SrcIpAddr, pInput->rtEntry,
                                          pInput->downStreamIf);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIpmcRemoteNpWrFsNpIpv4McUpdateIifVlanEntry *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McUpdateIifVlanEntry;
            FsNpIpv4McUpdateIifVlanEntry (pInput->u4VrId, pInput->u4GrpAddr,
                                          pInput->u4SrcIpAddr, pInput->rtEntry);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_STATS:
        {
            tIpmcRemoteNpWrFsNpIpv4GetMRouteStats *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteStats;
            u1RetVal =
                FsNpIpv4GetMRouteStats (pInput->u4VrId, pInput->u4GrpAddr,
                                        pInput->u4SrcAddr, pInput->i4StatType,
                                        &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
        {
            tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMRouteHCStats;
            u1RetVal =
                FsNpIpv4GetMRouteHCStats (pInput->u4VrId, pInput->u4GrpAddr,
                                          pInput->u4SrcAddr, pInput->i4StatType,
                                          &(pInput->U8RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
        {
            tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMNextHopStats;
            u1RetVal =
                FsNpIpv4GetMNextHopStats (pInput->u4VrId, pInput->u4GrpAddr,
                                          pInput->u4SrcAddr,
                                          pInput->i4OutIfIndex,
                                          pInput->i4StatType,
                                          &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_STATS:
        {
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceStats;
            u1RetVal =
                FsNpIpv4GetMIfaceStats (pInput->u4VrId, pInput->i4IfIndex,
                                        pInput->i4StatType,
                                        &(pInput->u4RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
        {
            tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4GetMIfaceHCStats;
            u1RetVal =
                FsNpIpv4GetMIfaceHCStats (pInput->u4VrId, pInput->i4IfIndex,
                                          pInput->i4StatType,
                                          &(pInput->U8RetValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIpmcRemoteNpWrFsNpIpv4SetMIfaceTtlTreshold *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4SetMIfaceTtlTreshold;
            u1RetVal =
                FsNpIpv4SetMIfaceTtlTreshold (pInput->u4VrId, pInput->i4IfIndex,
                                              pInput->i4TtlTreshold);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
        {
            tIpmcRemoteNpWrFsNpIpv4SetMIfaceRateLimit *pInput = NULL;
            pInput =
                &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4SetMIfaceRateLimit;
            u1RetVal =
                FsNpIpv4SetMIfaceRateLimit (pInput->u4VrId, pInput->i4IfIndex,
                                            pInput->i4RateLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV_X_HW_GET_MCAST_ENTRY:
        {
            tIpmcRemoteNpWrFsNpIpvXHwGetMcastEntry *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpvXHwGetMcastEntry;
            u1RetVal = FsNpIpvXHwGetMcastEntry (&(pInput->NpL3McastEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_NP_INIT_HW:
        {
            u1RetVal = FsPimNpInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_PIM_NP_DE_INIT_HW:
        {
            u1RetVal = FsPimNpDeInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_NP_INIT_HW:
        {
            u1RetVal = FsDvmrpNpInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DVMRP_NP_DE_INIT_HW:
        {
            u1RetVal = FsDvmrpNpDeInitHw ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
        case FS_NP_IGMP_PROXY_INIT:
        {
            u1RetVal = FsNpIgmpProxyInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IGMP_PROXY_DE_INIT:
        {
            u1RetVal = FsNpIgmpProxyDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
        case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
        {
            tIpmcRemoteNpWrFsNpIpv4McClearHitBit *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McClearHitBit;
            u1RetVal =
                FsNpIpv4McClearHitBit (pInput->u2VlanId, pInput->u4GrpAddr,
                                       pInput->u4SrcAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif
        case FS_NP_IPV4_MC_RPF_D_F_INFO:
        {
            tIpmcRemoteNpWrFsNpIpv4McRpfDFInfo *pInput = NULL;
            pInput = &pIpmcRemoteNpModInfo->IpmcRemoteNpFsNpIpv4McRpfDFInfo;
            u1RetVal = FsNpIpv4McRpfDFInfo (&(pInput->McRpfDFInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
