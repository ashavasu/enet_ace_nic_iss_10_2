
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnputil.c,v 1.2 2015/03/25 06:10:32 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __IGMPNPUTIL_C__
#define __IGMPNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskIgmpNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIgmpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_IGMP_HW_ENABLE_IGMP:
        case FS_IGMP_HW_DISABLE_IGMP:
        case FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_IGMP_MBSM_HW_ENABLE_IGMP:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_IGMP_HW_ENABLE_IGMP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#else
    UNUSED_PARAM (pRemoteHwNp);
#endif /* MBSM_WANTED  */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_IGMP_MBSM_HW_ENABLE_IGMP) ||
        ((pFsHwNp->u4Opcode > FS_IGMP_MBSM_HW_ENABLE_IGMP)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_IGMP_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgmpConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgmpConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgmpNpModInfo     *pIgmpNpModInfo = NULL;
    tIgmpRemoteNpModInfo *pIgmpRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIgmpNpModInfo = &(pFsHwNp->IgmpNpModInfo);
    pIgmpRemoteNpModInfo = &(pRemoteHwNp->IgmpRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIgmpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_IGMP_HW_ENABLE_IGMP:
        {
            break;
        }
        case FS_IGMP_HW_DISABLE_IGMP:
        {
            break;
        }
        case FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE:
        {
            tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate *pLocalArgs = NULL;
            tIgmpRemoteNpWrFsNpIpv4SetIgmpIfaceJoinRate *pRemoteArgs = NULL;
            pLocalArgs = &pIgmpNpModInfo->IgmpNpFsNpIpv4SetIgmpIfaceJoinRate;
            pRemoteArgs =
                &pIgmpRemoteNpModInfo->IgmpRemoteNpFsNpIpv4SetIgmpIfaceJoinRate;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4RateLimit = pLocalArgs->i4RateLimit;
            break;
        }
#ifdef MBSM_WANTED
        case FS_IGMP_MBSM_HW_ENABLE_IGMP:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgmpConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgmpConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgmpNpModInfo     *pIgmpNpModInfo = NULL;
    tIgmpRemoteNpModInfo *pIgmpRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pIgmpNpModInfo = &(pFsHwNp->IgmpNpModInfo);
    pIgmpRemoteNpModInfo = &(pRemoteHwNp->IgmpRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_IGMP_HW_ENABLE_IGMP:
        {
            break;
        }
        case FS_IGMP_HW_DISABLE_IGMP:
        {
            break;
        }
        case FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE:
        {
            tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate *pLocalArgs = NULL;
            tIgmpRemoteNpWrFsNpIpv4SetIgmpIfaceJoinRate *pRemoteArgs = NULL;
            pLocalArgs = &pIgmpNpModInfo->IgmpNpFsNpIpv4SetIgmpIfaceJoinRate;
            pRemoteArgs =
                &pIgmpRemoteNpModInfo->IgmpRemoteNpFsNpIpv4SetIgmpIfaceJoinRate;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4RateLimit = pRemoteArgs->i4RateLimit;
            break;
        }
#ifdef MBSM_WANTED
        case FS_IGMP_MBSM_HW_ENABLE_IGMP:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgmpRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIgmpNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgmpRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgmpRemoteNpModInfo *pIgmpRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIgmpRemoteNpModInfo = &(pRemoteHwNpInput->IgmpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_IGMP_HW_ENABLE_IGMP:
        {
            u1RetVal = FsIgmpHwEnableIgmp ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_IGMP_HW_DISABLE_IGMP:
        {
            u1RetVal = FsIgmpHwDisableIgmp ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE:
        {
            tIgmpRemoteNpWrFsNpIpv4SetIgmpIfaceJoinRate *pInput = NULL;
            pInput =
                &pIgmpRemoteNpModInfo->IgmpRemoteNpFsNpIpv4SetIgmpIfaceJoinRate;
            u1RetVal =
                FsNpIpv4SetIgmpIfaceJoinRate (pInput->i4IfIndex,
                                              pInput->i4RateLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
