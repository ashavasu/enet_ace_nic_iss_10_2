
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnputil.c,v 1.20 2016/10/03 10:34:46 siva Exp $
 *
 * Description:This file contains the NP utility functions of IP
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __IP_NPUTIL_C__
#define __IP_NPUTIL_C__

#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskIpNpPorts                              */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                     UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpRemoteNpModInfo *pIpRemoteNpModInfo = NULL;
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
#ifdef MBSM_WANTED
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfType = 0;
#endif
    BOOL1               bIsCentProtOpCode = FNP_FALSE;

    UNUSED_PARAM (pi4RpcCallStatus);

    pIpNpModInfo = &(pFsHwNp->IpNpModInfo);
    pIpRemoteNpModInfo = &(pRemoteHwNp->IpRemoteNpModInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    /* Get the stacking port of the Local and Remote unit */
    CfaNpGetHwPortInfo (&LocalHwPortInfo);
    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);    /* DISS TBD */
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);
    switch (pFsHwNp->u4Opcode)
    {
        case FS_NP_IP_INIT:
        case FS_NP_OSPF_INIT:
        case FS_NP_OSPF_DE_INIT:
        case FS_NP_DHCP_SRV_INIT:
        case FS_NP_DHCP_SRV_DE_INIT:
        case FS_NP_DHCP_RLY_INIT:
        case FS_NP_DHCP_RLY_DE_INIT:
        case FS_NP_RIP_INIT:
        case FS_NP_RIP_DE_INIT:
        case FS_NP_IPV4_CREATE_IP_INTERFACE:
        case FS_NP_IPV4_MODIFY_IP_INTERFACE:
        case FS_NP_IPV4_DELETE_IP_INTERFACE:
        case FS_NP_IPV4_DELETE_L3SUB_INTERFACE:
        case FS_NP_IPV4_DELETE_SEC_IP_INTERFACE:
        case FS_NP_IPV4_SET_FORWARDING_STATUS:
        case FS_NP_IPV4_VRF_CLEAR_ARP_TABLE:
        case FS_NP_IPV4_VRF_ARP_ADD:
        case FS_NP_IPV4_VRF_ARP_MODIFY:
        case FS_NP_IPV4_VRF_ARP_DEL:
        case FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY:
        case FS_NP_IPV4_VRF_ARP_GET:
        case FS_NP_IPV4_VRF_ARP_GET_NEXT:
        case FS_NP_L3_IPV4_VRF_ARP_ADD:
        case FS_NP_L3_IPV4_VRF_ARP_MODIFY:
        case FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO:
        case FS_NP_CFA_VRF_SET_DLF_STATUS:
#ifdef VRRP_WANTED
        case FS_NP_IPV4_VRRP_INTF_CREATE_WR:
        case FS_NP_IPV4_VRRP_INTF_DELETE_WR:
        case FS_NP_IPV4_GET_VRRP_INTERFACE:
        case FS_NP_IPV4_VRRP_INSTALL_FILTER:
        case FS_NP_IPV4_VRRP_REMOVE_FILTER:
        case FS_NP_VRRP_HW_PROGRAM:
#endif /* VRRP_WANTED */
#ifdef ISIS_WANTED
        case FS_NP_ISIS_HW_PROGRAM:
#endif /* ISIS_WANTED */
        case FS_NP_IPV4_GET_STATS:
        case FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        case FS_NP_IPV4_UC_GET_ROUTE:
        case FS_NP_IPV4_L3_IP_INTERFACE:
        case FS_NP_IPV4_INTF_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
#ifndef VRF_WANTED
        case FS_NP_IPV4_GET_SRC_MOVED_IP_ADDR:
#else
        case FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR:
#endif
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case FS_NP_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpIpv4UcAddRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcAddRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcAddRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcAddRoute;
#ifndef NPSIM_WANTED
            if (pLocalArgs->pRouteEntry->u4IfIndex
#else
            if (pLocalArgs->routeEntry.u4IfIndex
#endif
                == u4LocalStackPort)
            {
                pRemoteArgs->routeEntry.u4IfIndex = u4RemoteStackPort;
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_NP_IPV4_UC_DEL_ROUTE:
        {
            tIpNpWrFsNpIpv4UcDelRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcDelRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcDelRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcDelRoute;
            if (pLocalArgs->routeEntry.u4IfIndex == u4LocalStackPort)
            {
                pRemoteArgs->routeEntry.u4IfIndex = u4RemoteStackPort;
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IP_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_IP_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#ifdef ISIS_WANTED
        case FS_NP_MBSM_ISIS_HW_PROGRAM:
        {
                tIpNpWrFsNpMbsmIsisHwProgram    *pLocalArgs = NULL;
                tIpRemoteNpWrFsNpIsisHwProgram  *pRemoteArgs = NULL;
                pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIsisHwProgram;
                pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIsisHwProgram;
                pRemoteArgs->u1Status = pLocalArgs->u1Status;
                pRemoteHwNp->u4Opcode = FS_NP_ISIS_HW_PROGRAM;
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                break;
        }
#endif        
        case FS_NP_MBSM_OSPF_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_OSPF_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_DHCP_SRV_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_DHCP_SRV_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_DHCP_RLY_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_DHCP_RLY_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4CreateIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4CreateIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateIpInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_CREATE_IP_INTERFACE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_IPV4_CREATE_L3SUB_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4CreateL3SubInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4CreateL3SubInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateL3SubInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateL3SubInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_CREATE_L3SUB_INTERFACE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

        case FS_NP_MBSM_IPV4_VRF_ARP_ADDITION:
        {
            tIpNpWrFsNpMbsmIpv4VrfArpAddition *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpAdd *pRemoteArgs = NULL;
            tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd *pRemoteL3Args = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAddition;

            /* check whether the index is router port */
            MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
            CfaGetIfaceType (pLocalArgs->u4IfIndex, &u1IfType);
            CfaGetIfInfo (pLocalArgs->u4IfIndex, &CfaIfInfo);

            if ((u1IfType == CFA_ENET) &&
                CfaIfInfo.u1BridgedIface != CFA_ENABLED)
            {
                pRemoteL3Args =
                    &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpAdd;
                pRemoteL3Args->u4VrfId = pLocalArgs->u4VrId;
                pRemoteL3Args->u4IfIndex = pLocalArgs->u4IfIndex;
                pRemoteL3Args->u4IpAddr = pLocalArgs->u4IpAddr;
                MEMCPY (&(pRemoteL3Args->macAddr), (pLocalArgs->pMacAddr),
                        sizeof (UINT1) * MAC_ADDR_LEN);
                MEMCPY (&(pRemoteL3Args->au1IfName), (pLocalArgs->pu1IfName),
                        sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
                pRemoteL3Args->i1State = pLocalArgs->i1State;
                MEMCPY (&(pRemoteL3Args->bu1TblFull), (pLocalArgs->pbu1TblFull),
                        sizeof (UINT1));

                pRemoteHwNp->u4Opcode = FS_NP_L3_IPV4_VRF_ARP_ADD;
            }
            else
            {
                pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpAdd;
                pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
                MEMCPY (&(pRemoteArgs->u2VlanId), &(pLocalArgs->u2VlanId),
                        sizeof (tNpVlanId));
                pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
                MEMCPY (&(pRemoteArgs->macAddr), (pLocalArgs->pMacAddr),
                        sizeof (UINT1) * MAC_ADDR_LEN);
                MEMCPY (&(pRemoteArgs->u4TblFull), (pLocalArgs->pbu1TblFull),
                        sizeof (UINT1));
                pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
                MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                        sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
                pRemoteArgs->i1State = pLocalArgs->i1State;

                pRemoteHwNp->u4Opcode = FS_NP_IPV4_VRF_ARP_ADD;
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpMbsmIpv4UcAddRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcAddRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIpv4UcAddRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcAddRoute;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpDestAddr = pLocalArgs->u4IpDestAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
#ifndef NPSIM_WANTED
            MEMCPY (&(pRemoteArgs->routeEntry), (pLocalArgs->pRouteEntry),
                    sizeof (tFsNpNextHopInfo));
#else
            MEMCPY (&(pRemoteArgs->routeEntry), &(pLocalArgs->routeEntry),
                    sizeof (tFsNpNextHopInfo));
#endif
            MEMCPY (&(pRemoteArgs->bu1TblFull), (pLocalArgs->pbu1TblFull),
                    sizeof (UINT1));
#ifndef NPSIM_WANTED
            if (pLocalArgs->pRouteEntry->u4IfIndex 
#else
            if (pLocalArgs->routeEntry.u4IfIndex 
#endif
                == u4LocalStackPort)
            {
                pRemoteArgs->routeEntry.u4IfIndex = u4RemoteStackPort;
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_UC_ADD_ROUTE;
            break;
        }
        case FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4MapVlansToIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpMbsmIpv4MapVlansToIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4MapVlansToIpInterface;
            MEMCPY (&(pRemoteArgs->PvlanMappingInfo),
                    (pLocalArgs->pPvlanMappingInfo),
                    sizeof (tNpIpVlanMappingInfo));
            pRemoteHwNp->u4Opcode = FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }
    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * PNAC protocol will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if ((u4StackingModel == ISS_DISS_STACKING_MODEL) &&
        (bIsCentProtOpCode != FNP_TRUE))
    {
        if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            /* don't do anything */
        }
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_NP_MBSM_IP_INIT) ||
        ((pFsHwNp->u4Opcode > FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS)))
    {
        /*
         * If a nexthop is resolved during MBSM processing, don't skip the delete
         * route indication, otherwise the egress info will not be updated in h/w
         * with resolved nexthop
         */
        if ((pFsHwNp->u4Opcode != FS_NP_IPV4_UC_DEL_ROUTE))
        {
            if (MbsmIsNpBulkSyncInProgress (NP_IP_MOD) == OSIX_TRUE)
            {
                if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                }
                else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                }

            }
        }
    }
#endif

    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilIpMergeLocalRemoteNpOutput                 */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                    */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp            */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value         */
/*                    pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilIpMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                  tRemoteHwNp * pRemoteHwNp,
                                  INT4 *pi4LocalNpRetVal,
                                  INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpRemoteNpModInfo *pIpRemoteNpModInfo = NULL;

    pIpNpModInfo = &(pFsHwNp->IpNpModInfo);
    pIpRemoteNpModInfo = &(pRemoteHwNp->IpRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_NP_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpIpv4VrfArpAdd *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpAdd *pRemoteArgs = NULL;
            UINT4               u4TblFull = FNP_FALSE;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpAdd;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpAdd;
            if ((*(pLocalArgs->pu4TblFull)) == FNP_TRUE
                || pRemoteArgs->u4TblFull == FNP_TRUE)
            {
                u4TblFull = FNP_TRUE;
            }
            MEMCPY ((pLocalArgs->pu4TblFull), &(u4TblFull), sizeof (UINT4));
            break;
        }
        case FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY:
        {
            tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfCheckHitOnArpEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfCheckHitOnArpEntry;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfCheckHitOnArpEntry;
            if (pLocalArgs->u1HitBitStatus == FNP_TRUE
                || pRemoteArgs->u1HitBitStatus == FNP_TRUE)
            {
                pLocalArgs->u1HitBitStatus = FNP_TRUE;
            }
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpL3Ipv4VrfArpAdd *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd *pRemoteArgs = NULL;
            UINT1               bu1TblFull = FNP_FALSE;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpAdd;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpAdd;
            if ((*(pLocalArgs->pbu1TblFull)) == FNP_TRUE
                || pRemoteArgs->bu1TblFull == FNP_TRUE)
            {
                bu1TblFull = FNP_TRUE;
            }
            MEMCPY ((pLocalArgs->pbu1TblFull), &(bu1TblFull), sizeof (UINT1));
            break;
        }
        case FS_NP_IPV4_GET_STATS:
        {
            tIpNpWrFsNpIpv4GetStats *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4GetStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4GetStats;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4GetStats;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            (*pLocalArgs->pu4RetVal) =
                (*pLocalArgs->pu4RetVal) + (pRemoteArgs->u4RetVal);
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
        case FS_NP_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpIpv4UcAddRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcAddRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcAddRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcAddRoute;
#ifndef NPSIM_WANTED
            pLocalArgs->pRouteEntry->u4HwIntfId[1] = pRemoteArgs->routeEntry.u4HwIntfId[0];
            pRemoteArgs->routeEntry.u4HwIntfId[1] = pLocalArgs->pRouteEntry->u4HwIntfId[0];  
#else
            pLocalArgs->routeEntry.u4HwIntfId[1] = pRemoteArgs->routeEntry.u4HwIntfId[0];
            pRemoteArgs->routeEntry.u4HwIntfId[1] = pLocalArgs->routeEntry.u4HwIntfId[0];  
#endif
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;

            break;
        }

        default:
            break;
    }
    return u1RetVal;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpRemoteNpModInfo *pIpRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pIpNpModInfo = &(pFsHwNp->IpNpModInfo);
    pIpRemoteNpModInfo = &(pRemoteHwNp->IpRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length = pRemoteHwNp->i4Length + sizeof (tIpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_IP_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_OSPF_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_OSPF_DE_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_DHCP_SRV_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_DHCP_SRV_DE_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_DHCP_RLY_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_DHCP_RLY_DE_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_RIP_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_RIP_DE_INIT:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_IPV4_CREATE_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4CreateIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4CreateIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4CreateIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateIpInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            break;
        }
        case FS_NP_IPV4_CREATE_L3SUB_INTERFACE:
        {
            tIpNpWrFsNpIpv4CreateL3SubInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4CreateL3SubInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4CreateL3SubInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateL3SubInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            break;
        }

        case FS_NP_IPV4_MODIFY_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4ModifyIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4ModifyIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4ModifyIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4ModifyIpInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            break;
        }
        case FS_NP_IPV4_DELETE_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4DeleteIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4DeleteIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteIpInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            break;
        }
        case FS_NP_IPV4_DELETE_L3SUB_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteL3SubInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4DeleteL3SubInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4DeleteL3SubInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteL3SubInterface;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }

        case FS_NP_IPV4_DELETE_SEC_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteSecIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4DeleteSecIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4DeleteSecIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteSecIpInterface;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            break;
        }
        case FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS:
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS:
#endif
        {
            tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UpdateIpInterfaceStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UpdateIpInterfaceStatus;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->u4CfaIfIndex = pLocalArgs->u4CfaIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            pRemoteArgs->u4Status = pLocalArgs->u4Status;
            break;
        }
        case FS_NP_IPV4_SET_FORWARDING_STATUS:
        {
            tIpNpWrFsNpIpv4SetForwardingStatus *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4SetForwardingStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4SetForwardingStatus;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4SetForwardingStatus;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_NP_IPV4_UC_DEL_ROUTE:
        {
            tIpNpWrFsNpIpv4UcDelRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcDelRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcDelRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcDelRoute;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpDestAddr = pLocalArgs->u4IpDestAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            MEMCPY (&(pRemoteArgs->routeEntry), &(pLocalArgs->routeEntry),
                    sizeof (tFsNpNextHopInfo));
            MEMCPY (&(pRemoteArgs->i4FreeDefIpB4Del),
                    (pLocalArgs->pi4FreeDefIpB4Del), sizeof (INT4));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpIpv4VrfArpAdd *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpAdd;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpAdd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u2VlanId), &(pLocalArgs->u2VlanId),
                    sizeof (tNpVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->macAddr), (pLocalArgs->pMacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->i1State = pLocalArgs->i1State;
            MEMCPY (&(pRemoteArgs->u4TblFull), (pLocalArgs->pu4TblFull),
                    sizeof (UINT4));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_MODIFY:
        {
            tIpNpWrFsNpIpv4VrfArpModify *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpModify *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpModify;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpModify;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u2VlanId), &(pLocalArgs->u2VlanId),
                    sizeof (tNpVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4PhyIfIndex = pLocalArgs->u4PhyIfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->macAddr), (pLocalArgs->pMacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->i1State = pLocalArgs->i1State;
            break;
        }
        case FS_NP_IPV4_VRF_ARP_DEL:
        {
            tIpNpWrFsNpIpv4VrfArpDel *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpDel *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpDel;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpDel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->i1State = pLocalArgs->i1State;
            break;
        }
        case FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY:
        {
            tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfCheckHitOnArpEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfCheckHitOnArpEntry;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfCheckHitOnArpEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpAddress = pLocalArgs->u4IpAddress;
            pRemoteArgs->u1NextHopFlag = pLocalArgs->u1NextHopFlag;
            pRemoteArgs->u1HitBitStatus = pLocalArgs->u1HitBitStatus;
            break;
        }
        case FS_NP_IPV4_VRF_CLEAR_ARP_TABLE:
        {
            tIpNpWrFsNpIpv4VrfClearArpTable *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfClearArpTable *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfClearArpTable;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfClearArpTable;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            break;
        }
        case FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR:
        {
            tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfGetSrcMovedIpAddr *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfGetSrcMovedIpAddr;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfGetSrcMovedIpAddr;
            MEMCPY (&(pRemoteArgs->u4VrId), (pLocalArgs->pu4VrId),
                    sizeof (UINT4));
            MEMCPY (&(pRemoteArgs->u4IpAddress), (pLocalArgs->pu4IpAddress),
                    sizeof (UINT4));
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpL3Ipv4VrfArpAdd *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpAdd;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpAdd;
            pRemoteArgs->u4VrfId = pLocalArgs->u4VrfId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->macAddr), (pLocalArgs->pMacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->i1State = pLocalArgs->i1State;
            MEMCPY (&(pRemoteArgs->bu1TblFull), (pLocalArgs->pbu1TblFull),
                    sizeof (UINT1));
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_MODIFY:
        {
            tIpNpWrFsNpL3Ipv4VrfArpModify *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpL3Ipv4VrfArpModify *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpModify;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpModify;
            pRemoteArgs->u4VrfId = pLocalArgs->u4VrfId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->macAddr), (pLocalArgs->pMacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            MEMCPY (&(pRemoteArgs->au1IfName), (pLocalArgs->pu1IfName),
                    sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH);
            pRemoteArgs->i1State = pLocalArgs->i1State;
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET:
        {
            tIpNpWrFsNpIpv4VrfArpGet *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpGet *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGet;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpGet;
            MEMCPY (&(pRemoteArgs->ArpNpInParam), &(pLocalArgs->ArpNpInParam),
                    sizeof (tNpArpInput));
            MEMCPY (&(pRemoteArgs->ArpNpOutParam),
                    (pLocalArgs->pArpNpOutParam), sizeof (tNpArpOutput));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET_NEXT:
        {
            tIpNpWrFsNpIpv4VrfArpGetNext *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrfArpGetNext *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGetNext;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpGetNext;
            MEMCPY (&(pRemoteArgs->ArpNpInParam), &(pLocalArgs->ArpNpInParam),
                    sizeof (tNpArpInput));
            MEMCPY (&(pRemoteArgs->ArpNpOutParam),
                    (pLocalArgs->pArpNpOutParam), sizeof (tNpArpOutput));
            break;
        }
        case FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpIpv4UcAddRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcAddRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcAddRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcAddRoute;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpDestAddr = pLocalArgs->u4IpDestAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
#ifndef NPSIM_WANTED
            MEMCPY (&(pRemoteArgs->routeEntry), (pLocalArgs->pRouteEntry),
                    sizeof (tFsNpNextHopInfo));
#else
            MEMCPY (&(pRemoteArgs->routeEntry), &(pLocalArgs->routeEntry),
                    sizeof (tFsNpNextHopInfo));
#endif
            MEMCPY (&(pRemoteArgs->bu1TblFull), (pLocalArgs->pbu1TblFull),
                    sizeof (UINT1));
            break;
        }
        case FS_NP_CFA_VRF_SET_DLF_STATUS:
        {
            tIpNpWrFsNpCfaVrfSetDlfStatus *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpCfaVrfSetDlfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpCfaVrfSetDlfStatus;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpCfaVrfSetDlfStatus;
            pRemoteArgs->u4VrfId = pLocalArgs->u4VrfId;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
#ifdef VRRP_WANTED
        case FS_NP_IPV4_VRRP_INTF_CREATE_WR:
        {
            tIpNpWrFsNpIpv4VrrpIntfCreateWr *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrrpIntfCreateWr *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfCreateWr;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrrpIntfCreateWr;
            MEMCPY (&(pRemoteArgs->u2VlanId), &(pLocalArgs->u2VlanId),
                    sizeof (tNpVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            break;
        }
        case FS_NP_IPV4_VRRP_INTF_DELETE_WR:
        {
            tIpNpWrFsNpIpv4VrrpIntfDeleteWr *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4VrrpIntfDeleteWr *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfDeleteWr;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrrpIntfDeleteWr;
            MEMCPY (&(pRemoteArgs->u2VlanId), &(pLocalArgs->u2VlanId),
                    sizeof (tNpVlanId));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4IpAddr = pLocalArgs->u4IpAddr;
            MEMCPY (&(pRemoteArgs->au1MacAddr), (pLocalArgs->au1MacAddr),
                    sizeof (UINT1) * MAC_ADDR_LEN);
            break;
        }
        case FS_NP_IPV4_GET_VRRP_INTERFACE:
        {
            tIpNpWrFsNpIpv4GetVrrpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4GetVrrpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4GetVrrpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4GetVrrpInterface;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4VrId = pLocalArgs->i4VrId;
            break;
        }
        case FS_NP_IPV4_VRRP_INSTALL_FILTER:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_IPV4_VRRP_REMOVE_FILTER:
        {
            /* No Arguments */
            break;
        }
        case FS_NP_VRRP_HW_PROGRAM:
        {
            tIpNpWrFsNpVrrpHwProgram *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpVrrpHwProgram *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpVrrpHwProgram;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpVrrpHwProgram;
            pRemoteArgs->u1NpAction = pLocalArgs->u1NpAction;
            pRemoteArgs->pVrrpNwIntf = pLocalArgs->pVrrpNwIntf;
            break;
        }

#endif /* VRRP_WANTED */
#ifdef ISIS_WANTED
        case FS_NP_ISIS_HW_PROGRAM:
        {
            tIpNpWrFsNpIsisHwProgram *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIsisHwProgram *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIsisHwProgram;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIsisHwProgram;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
#endif /* ISIS_WANTED */
        case FS_NP_IPV4_GET_STATS:
        {
            tIpNpWrFsNpIpv4GetStats *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4GetStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4GetStats;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4GetStats;
            pRemoteArgs->i4StatType = pLocalArgs->i4StatType;
            MEMCPY (&(pRemoteArgs->u4RetVal), (pLocalArgs->pu4RetVal),
                    sizeof (UINT4));
            break;
        }
        case FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4MapVlansToIpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4MapVlansToIpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4MapVlansToIpInterface;
            pRemoteArgs =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4MapVlansToIpInterface;
            MEMCPY (&(pRemoteArgs->PvlanMappingInfo),
                    (pLocalArgs->pPvlanMappingInfo),
                    sizeof (tNpIpVlanMappingInfo));
            break;
        }
        case FS_NP_IPV4_INTF_STATUS:
        {
            tIpNpWrFsNpIpv4IntfStatus *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4IntfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4IntfStatus;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4IntfStatus;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfStatus = pLocalArgs->u4IfStatus;
            MEMCPY (&(pRemoteArgs->IpIntInfo), (pLocalArgs->pIpIntInfo),
                    sizeof (tFsNpIp4IntInfo));
            break;
        }
        case FS_NP_IPV4_UC_GET_ROUTE:
        {
            tIpNpWrFsNpIpv4UcGetRoute *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4UcGetRoute *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4UcGetRoute;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcGetRoute;
            MEMCPY (&(pRemoteArgs->RtmNpInParam), &(pLocalArgs->RtmNpInParam),
                    sizeof (tNpRtmInput));
            MEMCPY (&(pRemoteArgs->RtmNpOutParam),
                    (pLocalArgs->pRtmNpOutParam), sizeof (tNpRtmOutput));
            break;
        }
        case FS_NP_IPV4_L3_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4L3IpInterface *pLocalArgs = NULL;
            tIpRemoteNpWrFsNpIpv4L3IpInterface *pRemoteArgs = NULL;
            pLocalArgs = &pIpNpModInfo->IpNpFsNpIpv4L3IpInterface;
            pRemoteArgs = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4L3IpInterface;
            MEMCPY (&(pRemoteArgs->L3Action), &(pLocalArgs->L3Action),
                    sizeof (tL3Action));
            MEMCPY (&(pRemoteArgs->FsNpL3IfInfo),
                    (pLocalArgs->pFsNpL3IfInfo), sizeof (tFsNpL3IfInfo));
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IP_INIT:
        case FS_NP_MBSM_ISIS_HW_PROGRAM:
        case FS_NP_MBSM_OSPF_INIT:
        case FS_NP_MBSM_DHCP_SRV_INIT:
        case FS_NP_MBSM_DHCP_RLY_INIT:
        case FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE:
        case FS_NP_MBSM_IPV4_ARP_ADDITION:
        case FS_NP_MBSM_IPV4_VRF_ARP_ADDITION:
        case FS_NP_MBSM_IPV4_UC_ADD_ROUTE:
        case FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;

    u4Opcode = pRemoteHwNp->u4Opcode;

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IP_INIT:
        case FS_NP_MBSM_ISIS_HW_PROGRAM:
        case FS_NP_MBSM_OSPF_INIT:
        case FS_NP_MBSM_DHCP_SRV_INIT:
        case FS_NP_MBSM_DHCP_RLY_INIT:
        case FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE:
        case FS_NP_MBSM_IPV4_ARP_ADDITION:
        case FS_NP_MBSM_IPV4_VRF_ARP_ADDITION:
        case FS_NP_MBSM_IPV4_UC_ADD_ROUTE:
        case FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        case FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                            tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpRemoteNpModInfo *pIpRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIpRemoteNpModInfo = &(pRemoteHwNpInput->IpRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_IP_INIT:
        {
            FsNpIpInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_OSPF_INIT:
        {
            u1RetVal = FsNpOspfInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_OSPF_DE_INIT:
        {
            FsNpOspfDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_DHCP_SRV_INIT:
        {
            u1RetVal = FsNpDhcpSrvInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_DHCP_SRV_DE_INIT:
        {
            FsNpDhcpSrvDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_DHCP_RLY_INIT:
        {
            u1RetVal = FsNpDhcpRlyInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_DHCP_RLY_DE_INIT:
        {
            FsNpDhcpRlyDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_RIP_INIT:
        {
            u1RetVal = FsNpRipInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_RIP_DE_INIT:
        {
            FsNpRipDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_CREATE_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4CreateIpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateIpInterface;
            u1RetVal =
                FsNpIpv4CreateIpInterface (pInput->u4VrId, (pInput->au1IfName),
                                           pInput->u4CfaIfIndex,
                                           pInput->u4IpAddr,
                                           pInput->u4IpSubNetMask,
                                           pInput->u2VlanId,
                                           (pInput->au1MacAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_CREATE_L3SUB_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4CreateL3SubInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4CreateL3SubInterface;
            u1RetVal =
                (UINT1)(FsNpIpv4CreateL3SubInterface (pInput->u4VrId, (pInput->au1IfName),
                                           pInput->u4CfaIfIndex,
                                           pInput->u4IpAddr,
                                           pInput->u4IpSubNetMask,
                                           pInput->u2VlanId,
                                           (pInput->au1MacAddr),
                                            (pInput->u4ParentIndex)));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MODIFY_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4ModifyIpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4ModifyIpInterface;
            u1RetVal =
                FsNpIpv4ModifyIpInterface (pInput->u4VrId, (pInput->au1IfName),
                                           pInput->u4CfaIfIndex,
                                           pInput->u4IpAddr,
                                           pInput->u4IpSubNetMask,
                                           pInput->u2VlanId,
                                           (pInput->au1MacAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_DELETE_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4DeleteIpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteIpInterface;
            u1RetVal =
                FsNpIpv4DeleteIpInterface (pInput->u4VrId, (pInput->au1IfName),
                                           pInput->u4IfIndex, pInput->u2VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_DELETE_L3SUB_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4DeleteL3SubInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteL3SubInterface;
            u1RetVal =
                (UINT1)(FsNpIpv4DeleteL3SubInterface (pInput->u4IfIndex));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case FS_NP_IPV4_DELETE_SEC_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4DeleteSecIpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4DeleteSecIpInterface;
            u1RetVal =
                FsNpIpv4DeleteSecIpInterface (pInput->u4VrId, (pInput->au1IfName),
                                           pInput->u4IfIndex, pInput->u4IpAddr, 
                                           pInput->u2VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS:
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS:
#endif
        {
            tIpRemoteNpWrFsNpIpv4UpdateIpInterfaceStatus *pInput = NULL;
            pInput =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UpdateIpInterfaceStatus;
            u1RetVal =
                FsNpIpv4UpdateIpInterfaceStatus (pInput->u4VrId,
                                                 (pInput->au1IfName),
                                                 pInput->u4CfaIfIndex,
                                                 pInput->u4IpAddr,
                                                 pInput->u4IpSubNetMask,
                                                 pInput->u2VlanId,
                                                 (pInput->au1MacAddr),
                                                 pInput->u4Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_SET_FORWARDING_STATUS:
        {
            tIpRemoteNpWrFsNpIpv4SetForwardingStatus *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4SetForwardingStatus;
            u1RetVal =
                FsNpIpv4SetForwardingStatus (pInput->u4VrId, pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_UC_DEL_ROUTE:
        {
            tIpRemoteNpWrFsNpIpv4UcDelRoute *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcDelRoute;
            u1RetVal =
                FsNpIpv4UcDelRoute (pInput->u4VrId, pInput->u4IpDestAddr,
                                    pInput->u4IpSubNetMask, pInput->routeEntry,
                                    (int *) &(pInput->i4FreeDefIpB4Del));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_ADD:
        {
            tIpRemoteNpWrFsNpIpv4VrfArpAdd *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpAdd;
            u1RetVal =
                FsNpIpv4VrfArpAdd (pInput->u4VrId, pInput->u2VlanId,
                                   pInput->u4IfIndex, pInput->u4IpAddr,
                                   (pInput->macAddr), (pInput->au1IfName),
                                   pInput->i1State, &(pInput->u4TblFull));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_MODIFY:
        {
            tIpRemoteNpWrFsNpIpv4VrfArpModify *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpModify;
            u1RetVal =
                FsNpIpv4VrfArpModify (pInput->u4VrId, pInput->u2VlanId,
                                      pInput->u4IfIndex, pInput->u4PhyIfIndex,
                                      pInput->u4IpAddr,
                                      (pInput->macAddr), (pInput->au1IfName),
                                      pInput->i1State);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_DEL:
        {
            tIpRemoteNpWrFsNpIpv4VrfArpDel *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpDel;
            u1RetVal =
                FsNpIpv4VrfArpDel (pInput->u4VrId, pInput->u4IpAddr,
                                   (pInput->au1IfName), pInput->i1State);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY:
        {
            tIpRemoteNpWrFsNpIpv4VrfCheckHitOnArpEntry *pInput = NULL;
            pInput =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfCheckHitOnArpEntry;
            pInput->u1HitBitStatus =
                FsNpIpv4VrfCheckHitOnArpEntry (pInput->u4VrId,
                                               pInput->u4IpAddress,
                                               pInput->u1NextHopFlag);
            u1RetVal = FNP_SUCCESS;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_CLEAR_ARP_TABLE:
        {
            tIpRemoteNpWrFsNpIpv4VrfClearArpTable *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfClearArpTable;
            u1RetVal = FsNpIpv4VrfClearArpTable (pInput->u4VrId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_ADD:
        {
            tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpAdd;
            u1RetVal =
                FsNpL3Ipv4VrfArpAdd (pInput->u4VrfId, pInput->u4IfIndex,
                                     pInput->u4IpAddr, (pInput->macAddr),
                                     (pInput->au1IfName), pInput->i1State,
                                     &(pInput->bu1TblFull));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_MODIFY:
        {
            tIpRemoteNpWrFsNpL3Ipv4VrfArpModify *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpL3Ipv4VrfArpModify;
            u1RetVal =
                FsNpL3Ipv4VrfArpModify (pInput->u4VrfId, pInput->u4IfIndex,
                                        pInput->u4IpAddr, (pInput->macAddr),
                                        (pInput->au1IfName), pInput->i1State);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET:
        {
            tIpRemoteNpWrFsNpIpv4VrfArpGet *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpGet;
            u1RetVal =
                FsNpIpv4VrfArpGet (pInput->ArpNpInParam,
                                   &(pInput->ArpNpOutParam));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET_NEXT:
        {
            tIpRemoteNpWrFsNpIpv4VrfArpGetNext *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrfArpGetNext;
            u1RetVal =
                FsNpIpv4VrfArpGetNext (pInput->ArpNpInParam,
                                       &(pInput->ArpNpOutParam));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO:
        {
            FsNpIpv4SyncVlanAndL3Info ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_UC_ADD_ROUTE:
        {
            tIpRemoteNpWrFsNpIpv4UcAddRoute *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcAddRoute;
            u1RetVal =
                FsNpIpv4UcAddRoute (pInput->u4VrId, pInput->u4IpDestAddr,
                                    pInput->u4IpSubNetMask,
#ifndef NPSIM_WANTED
                                    &(pInput->routeEntry),
#else
                                    pInput->routeEntry,
#endif
                                    &(pInput->bu1TblFull));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_CFA_VRF_SET_DLF_STATUS:
        {
            tIpRemoteNpWrFsNpCfaVrfSetDlfStatus *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpCfaVrfSetDlfStatus;
            u1RetVal =
                FsNpCfaVrfSetDlfStatus (pInput->u4VrfId, pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef VRRP_WANTED
        case FS_NP_IPV4_VRRP_INTF_CREATE_WR:
        {
            tIpRemoteNpWrFsNpIpv4VrrpIntfCreateWr *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrrpIntfCreateWr;
            u1RetVal =
                FsNpIpv4VrrpIntfCreateWr (pInput->u2VlanId, pInput->u4IfIndex,
                                          pInput->u4IpAddr,
                                          (pInput->au1MacAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRRP_INTF_DELETE_WR:
        {
            tIpRemoteNpWrFsNpIpv4VrrpIntfDeleteWr *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4VrrpIntfDeleteWr;
            u1RetVal =
                FsNpIpv4VrrpIntfDeleteWr (pInput->u2VlanId, pInput->u4IfIndex,
                                          pInput->u4IpAddr,
                                          (pInput->au1MacAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_GET_VRRP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4GetVrrpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4GetVrrpInterface;
            u1RetVal =
                FsNpIpv4GetVrrpInterface (pInput->i4IfIndex, pInput->i4VrId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRRP_INSTALL_FILTER:
        {
            u1RetVal = FsNpIpv4VrrpInstallFilter ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_VRRP_REMOVE_FILTER:
        {
            u1RetVal = FsNpIpv4VrrpRemoveFilter ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_VRRP_HW_PROGRAM:
        {
            tIpRemoteNpWrFsNpVrrpHwProgram *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpVrrpHwProgram;
            u1RetVal =
                FsNpVrrpHwProgram (pInput->u1NpAction, pInput->pVrrpNwIntf);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

#endif /* VRRP_WANTED */
#ifdef ISIS_WANTED
        case FS_NP_ISIS_HW_PROGRAM:
        {
            tIpRemoteNpWrFsNpIsisHwProgram *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIsisHwProgram;
            u1RetVal = FsNpIsisHwProgram (pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /*ISIS_WANTED */
        case FS_NP_IPV4_GET_STATS:
        {
            tIpRemoteNpWrFsNpIpv4GetStats *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4GetStats;
            u1RetVal =
                FsNpIpv4GetStats (pInput->i4StatType, &(pInput->u4RetVal));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4MapVlansToIpInterface *pInput = NULL;
            pInput =
                &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4MapVlansToIpInterface;
            u1RetVal =
                FsNpIpv4MapVlansToIpInterface (&(pInput->PvlanMappingInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_INTF_STATUS:
        {
            tIpRemoteNpWrFsNpIpv4IntfStatus *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4IntfStatus;
            u1RetVal =
                FsNpIpv4IntfStatus (pInput->u4VrId, pInput->u4IfStatus,
                                    &(pInput->IpIntInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_UC_GET_ROUTE:
        {
            tIpRemoteNpWrFsNpIpv4UcGetRoute *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4UcGetRoute;
            u1RetVal =
                FsNpIpv4UcGetRoute (pInput->RtmNpInParam,
                                    &(pInput->RtmNpOutParam));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV4_L3_IP_INTERFACE:
        {
            tIpRemoteNpWrFsNpIpv4L3IpInterface *pInput = NULL;
            pInput = &pIpRemoteNpModInfo->IpRemoteNpFsNpIpv4L3IpInterface;
            u1RetVal =
                FsNpIpv4L3IpInterface (pInput->L3Action,
                                       &(pInput->FsNpL3IfInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __IP_NPUTIL_C__ */
