/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnputil.c,v 1.2 2015/03/25 06:10:32 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __MLDNPUTIL_C__
#define __MLDNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskMldNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskMldNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MLD_HW_ENABLE_MLD:
        case FS_MLD_HW_DISABLE_MLD:
        case FS_MLD_HW_DESTROY_M_L_D_FILTER:
        case FS_MLD_HW_DESTROY_M_L_D_F_P:
        case FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MLD_MBSM_HW_ENABLE_MLD:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_MLD_HW_ENABLE_MLD;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#else
        UNUSED_PARAM (pRemoteHwNp);
#endif /* MBSM_WANTED  */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MLD_MBSM_HW_ENABLE_MLD) ||
        ((pFsHwNp->u4Opcode > FS_MLD_MBSM_HW_ENABLE_MLD)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_MLD_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return;
}


/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldNpModInfo      *pMldNpModInfo = NULL;
    tMldRemoteNpModInfo *pMldRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMldNpModInfo = &(pFsHwNp->MldNpModInfo);
    pMldRemoteNpModInfo = &(pRemoteHwNp->MldRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tMldRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MLD_HW_ENABLE_MLD:
        {
            break;
        }
        case FS_MLD_HW_DISABLE_MLD:
        {
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_FILTER:
        {
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_F_P:
        {
            break;
        }
        case FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE:
        {
            tMldNpWrFsNpIpv6SetMldIfaceJoinRate *pLocalArgs = NULL;
            tMldRemoteNpWrFsNpIpv6SetMldIfaceJoinRate *pRemoteArgs = NULL;
            pLocalArgs = &pMldNpModInfo->MldNpFsNpIpv6SetMldIfaceJoinRate;
            pRemoteArgs =
                &pMldRemoteNpModInfo->MldRemoteNpFsNpIpv6SetMldIfaceJoinRate;
            pRemoteArgs->i4IfIndex = pLocalArgs->i4IfIndex;
            pRemoteArgs->i4RateLimit = pLocalArgs->i4RateLimit;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MLD_MBSM_HW_ENABLE_MLD:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldNpModInfo      *pMldNpModInfo = NULL;
    tMldRemoteNpModInfo *pMldRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pMldNpModInfo = &(pFsHwNp->MldNpModInfo);
    pMldRemoteNpModInfo = &(pRemoteHwNp->MldRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MLD_HW_ENABLE_MLD:
        {
            break;
        }
        case FS_MLD_HW_DISABLE_MLD:
        {
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_FILTER:
        {
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_F_P:
        {
            break;
        }
        case FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE:
        {
            tMldNpWrFsNpIpv6SetMldIfaceJoinRate *pLocalArgs = NULL;
            tMldRemoteNpWrFsNpIpv6SetMldIfaceJoinRate *pRemoteArgs = NULL;
            pLocalArgs = &pMldNpModInfo->MldNpFsNpIpv6SetMldIfaceJoinRate;
            pRemoteArgs =
                &pMldRemoteNpModInfo->MldRemoteNpFsNpIpv6SetMldIfaceJoinRate;
            pLocalArgs->i4IfIndex = pRemoteArgs->i4IfIndex;
            pLocalArgs->i4RateLimit = pRemoteArgs->i4RateLimit;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MLD_MBSM_HW_ENABLE_MLD:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMldRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMldNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMldRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                             tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldRemoteNpModInfo *pMldRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pMldRemoteNpModInfo = &(pRemoteHwNpInput->MldRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MLD_HW_ENABLE_MLD:
        {
            u1RetVal = FsMldHwEnableMld ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MLD_HW_DISABLE_MLD:
        {
            u1RetVal = FsMldHwDisableMld ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_FILTER:
        {
            u1RetVal = FsMldHwDestroyMLDFilter ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_F_P:
        {
            u1RetVal = FsMldHwDestroyMLDFP ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE:
        {
            tMldRemoteNpWrFsNpIpv6SetMldIfaceJoinRate *pInput = NULL;
            pInput =
                &pMldRemoteNpModInfo->MldRemoteNpFsNpIpv6SetMldIfaceJoinRate;
            u1RetVal =
                FsNpIpv6SetMldIfaceJoinRate (pInput->i4IfIndex,
                                             pInput->i4RateLimit);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
