/*************************************************************************
 * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 *
 * $Id: fsbnputil.c,v 1.7 2017/11/15 12:59:54 siva Exp $
 *
 * Description: This file contains the NP utility functions
 *              responsible for Hardware Programming 
 *
 **************************************************************************/
#ifndef _FSBNPUTIL_C_
#define _FSBNPUTIL_C_

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskFsbNpPorts                             */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskFsbNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbRemoteNpModInfo *pRemoteFsbNpModInfo = NULL;
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    UINT4               u4PhyIfIndex = 0;

    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    pFsbNpModInfo = &(pFsHwNp->FsbNpModInfo);
    pRemoteFsbNpModInfo = &(pRemoteHwNp->FsbRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_NP_FSB_HW_INIT:
        {
            /* Default VLAN filter should be programmed in both
             * Local and Remote node */
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_FSB_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbHwCreateFilter *pEntry = NULL;
            tFsbRemoteNpWrFsFsbHwCreateFilter *pRemEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateFilter;
            pRemEntry = &pRemoteFsbNpModInfo->FsbRemoteNpFsFsbHwCreateFilter;

            FsbGetPinnedPortForFCoEVlan (pEntry->pFsbHwFilterEntry->u4ContextId,
                                         pEntry->pFsbHwFilterEntry->u2VlanId,
                                         &u4PhyIfIndex);
            if (u4PhyIfIndex != 0)
            {
                if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                    FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            else
            {
                if (((pEntry->pFsbHwFilterEntry->u4AggIndex == 0)
                     && (pEntry->pFsbHwFilterEntry->u2ClassId == 0)))
                {
                    /* The following function , segregates the local and
                     * remote PortList to make the appropriate NPAPI call */
                    NpUtilMaskHwPortList ((UINT1 *) pEntry->pFsbHwFilterEntry->
                                          PortList,
                                          (UINT1 *) (pRemEntry->
                                                     FsbHwFilterEntry.PortList),
                                          &HwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount,
                                          ISS_PORT_LIST_SIZE);

                    /* In case of FSB, PortList will either be in Remote or Local 
                     * The below case will not be hit in our scenario.
                     * Added as safety check  */
                    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
                    {
                        FsbGetValidPhyIfIndexforVlan (pEntry->
                                                      pFsbHwFilterEntry->
                                                      u2VlanId, &u4PhyIfIndex);

                        if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                            FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                    }
                    else if (u4RemotePortCount != 0)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else if ((u4LocalPortCount == 0)
                             && (u4RemotePortCount == 0))
                    {
                        FsbGetValidPhyIfIndexforVlan (pEntry->
                                                      pFsbHwFilterEntry->
                                                      u2VlanId, &u4PhyIfIndex);

                        if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                            FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                    }
                }
                else if (pEntry->pFsbHwFilterEntry->u4AggIndex != 0)
                {
                    FsbGetValidPhyIfIndexforVlan (pEntry->pFsbHwFilterEntry->
                                                  u2VlanId, &u4PhyIfIndex);
                    if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                }
                else if (pEntry->pFsbHwFilterEntry->u2ClassId != 0)
                {
                    FsbGetValidPhyIfIndexforVlan (pEntry->pFsbHwFilterEntry->
                                                  u2VlanId, &u4PhyIfIndex);

                    if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                }
            }
            break;
        }
        case FS_FSB_HW_DELETE_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteFilter *pEntry = NULL;
            tFsbRemoteNpWrFsFsbHwDeleteFilter *pRemEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteFilter;
            pRemEntry = &pRemoteFsbNpModInfo->FsbRemoteNpFsFsbHwDeleteFilter;

            FsbGetPinnedPortForFCoEVlan (pEntry->pFsbHwFilterEntry->u4ContextId,
                                         pEntry->pFsbHwFilterEntry->u2VlanId,
                                         &u4PhyIfIndex);
            if (u4PhyIfIndex != 0)
            {
                if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                    FNP_SUCCESS)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            else
            {

                if (((pEntry->pFsbHwFilterEntry->u4AggIndex == 0)
                     && (pEntry->pFsbHwFilterEntry->u2ClassId == 0)))
                {
                    /* The following function , segregates the local and
                     * remote PortList to make the appropriate NPAPI call */
                    NpUtilMaskHwPortList ((UINT1 *) pEntry->pFsbHwFilterEntry->
                                          PortList,
                                          (UINT1 *) (&pRemEntry->
                                                     FsbHwFilterEntry.PortList),
                                          &HwPortInfo, &u4LocalPortCount,
                                          &u4RemotePortCount,
                                          ISS_PORT_LIST_SIZE);

                    /* In case of FSB, PortList will either be in Remote or Local 
                     * The below case will not be hit in our scenario.
                     * Added as safety check  */
                    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
                    {
                        FsbGetValidPhyIfIndexforVlan (pEntry->
                                                      pFsbHwFilterEntry->
                                                      u2VlanId, &u4PhyIfIndex);

                        if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                            FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                    }
                    else if (u4RemotePortCount != 0)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else if ((u4LocalPortCount == 0)
                             && (u4RemotePortCount == 0))
                    {
                        FsbGetValidPhyIfIndexforVlan (pEntry->
                                                      pFsbHwFilterEntry->
                                                      u2VlanId, &u4PhyIfIndex);

                        if (u4PhyIfIndex == 0)
                        {
                            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                        }

                        if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                            FNP_SUCCESS)
                        {
                            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                        }
                    }
                }
                else if (pEntry->pFsbHwFilterEntry->u4AggIndex != 0)
                {
                    FsbGetValidPhyIfIndexforVlan (pEntry->pFsbHwFilterEntry->
                                                  u2VlanId, &u4PhyIfIndex);
                    if (u4PhyIfIndex == 0)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    }
                    if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                }
                else if (pEntry->pFsbHwFilterEntry->u2ClassId != 0)
                {
                    FsbGetValidPhyIfIndexforVlan (pEntry->pFsbHwFilterEntry->
                                                  u2VlanId, &u4PhyIfIndex);

                    if (u4PhyIfIndex == 0)
                    {
                        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
                    }
                    if (NpUtilIsRemotePort (u4PhyIfIndex, &HwPortInfo) ==
                        FNP_SUCCESS)
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                }
            }
            NPUTIL_DBG1 (NPUTIL_DBG_MSG,
                         "NpUtilMaskFsbNpPorts: NP call status: %d \n",
                         *pu1NpCallStatus);
            break;
        }
        case FS_MI_NP_FSB_HW_DE_INIT:
        {
            tFsbNpWrFsMiNpFsbHwDeInit *pEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwDeInit;

            /* Default VLAN filter should be removed in both
             * Local and Remote node */
            if ((pEntry->FsbLocRemFilterId.u4LocalFilterId != 0) &&
                (pEntry->FsbLocRemFilterId.u4RemoteFilterId != 0))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (pEntry->FsbLocRemFilterId.u4RemoteFilterId != 0)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwCreateSChannelFilter *pEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateSChannelFilter;

            if (NpUtilIsRemotePort
                (pEntry->pFsbHwSChannelFilterEntry->u4IfIndex,
                 &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteSChannelFilter *pEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteSChannelFilter;

            if (NpUtilIsRemotePort
                (pEntry->pFsbHwSChannelFilterEntry->u4IfIndex,
                 &HwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }

            break;
        }
        case FS_FSB_HW_SET_FCOE_PARAMS:
        {
            /* Addition/Removal of stacking ports from FCoE vlan should be
             * programmed in both local and remote*/
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        {
            /* Default VLAN filter should be programmed only in the Remote node,
             * on MBSM Init, since Default VLAN Filters will already be 
             * programmed on Local */
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_FSB_MBSM_HW_CREATE_FILTER:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }
    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilFsbMergeLocalRemoteNPOutput                */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                        and Remote NP call execution                     */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                        pRemoteHwNp Param of type tRemoteHwNp            */
/*                        pi4LocalNpRetVal - Lcoal Np Return Value         */
/*                        pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilFsbMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                   tRemoteHwNp * pRemoteHwNp,
                                   INT4 *pi4LocalNpRetVal,
                                   INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbRemoteNpModInfo *pFsbRemoteNpModInfo = NULL;

    pFsbNpModInfo = &(pFsHwNp->FsbNpModInfo);
    pFsbRemoteNpModInfo = &(pRemoteHwNp->FsbRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
            /* Merging logic is not requied in the below cases,
             * as, Hw programming will be done either in remote or in
             * Local for FS_FSB_HW_CREATE_FILTER case. 
             * For cases FS_MI_NP_FSB_HW_DE_INIT and FS_FSB_HW_DELETE_FILTER, 
             * filters will be deleted and no parameter will be returned
             * In FS_MI_NP_FSB_MBSM_HW_INIT and FS_FSB_MBSM_HW_CREATE_FILTER cases,
             * the Hw programming will be done in remote, so merging logic is not
             * required */
        case FS_FSB_HW_CREATE_FILTER:
        case FS_FSB_HW_DELETE_FILTER:
        case FS_MI_NP_FSB_HW_DE_INIT:
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        case FS_FSB_HW_SET_FCOE_PARAMS:
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        case FS_FSB_MBSM_HW_CREATE_FILTER:
#endif /* MBSM_WANTED */
        {
            break;
        }
        case FS_MI_NP_FSB_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbHwInit *pEntry = NULL;
            tFsbRemoteNpWrFsMiNpFsbHwInit *pRemEntry = NULL;

            pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwInit;
            pRemEntry = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbHwInit;

            /* The Remote Node returns the Filter Id after programming and
             * it is returned back to Local node. Therefore, assigning 
             * the returned entry in local structure as remote filter id */
            pEntry->pFsbLocRemFilterId->u4RemoteFilterId =
                pRemEntry->FsbLocRemFilterId.u4LocalFilterId;

            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;

            break;
        }
        default:
            break;

    }
    return u1RetVal;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilFsbConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilFsbConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbRemoteNpModInfo *pFsbRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilFsbConvertLocalToRemoteNp: "
                    "pFsHwNp or pRemoteHwNp is NULL \n");
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pFsbNpModInfo = &(pFsHwNp->FsbNpModInfo);
    pFsbRemoteNpModInfo = &(pRemoteHwNp->FsbRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tFsbRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_NP_FSB_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbHwInit *pLocalArgs = NULL;
            tFsbRemoteNpWrFsMiNpFsbHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsMiNpFsbHwInit;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbHwInit;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pFsbLocRemFilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbLocRemFilterId),
                        (pLocalArgs->pFsbLocRemFilterId),
                        sizeof (tFsbLocRemFilterId));
            }
            break;
        }
        case FS_FSB_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbHwCreateFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwCreateFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwCreateFilter;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FilterId), (pLocalArgs->pu4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_FSB_HW_DELETE_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwDeleteFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwDeleteFilter;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            pRemoteArgs->u4FilterId = pLocalArgs->u4FilterId;
            break;
        }
        case FS_MI_NP_FSB_HW_DE_INIT:
        {
            tFsbNpWrFsMiNpFsbHwDeInit *pLocalArgs = NULL;
            tFsbRemoteNpWrFsMiNpFsbHwDeInit *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsMiNpFsbHwDeInit;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbHwDeInit;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            /* Swapping the Local and Remote Filter Id, since the
             * below structure will be used to program the Remote Node */
            pRemoteArgs->FsbLocRemFilterId.u4LocalFilterId =
                pLocalArgs->FsbLocRemFilterId.u4RemoteFilterId;
            pRemoteArgs->FsbLocRemFilterId.u4RemoteFilterId =
                pLocalArgs->FsbLocRemFilterId.u4LocalFilterId;
            break;
        }
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwCreateSChannelFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwCreateSChannelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwCreateSChannelFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateSChannelFilter;
            if (pLocalArgs->pFsbHwSChannelFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwSChannelFilterEntry),
                        (pLocalArgs->pFsbHwSChannelFilterEntry),
                        sizeof (tFsbHwSChannelFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FilterId), (pLocalArgs->pu4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteSChannelFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwDeleteSChannelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwDeleteSChannelFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteSChannelFilter;
            if (pLocalArgs->pFsbHwSChannelFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwSChannelFilterEntry),
                        (pLocalArgs->pFsbHwSChannelFilterEntry),
                        sizeof (tFsbHwSChannelFilterEntry));
            }
            pRemoteArgs->u4FilterId = pLocalArgs->u4FilterId;
            break;
        }
        case FS_FSB_HW_SET_FCOE_PARAMS:
        {
            tFsbNpWrFsFsbHwSetFCoEParams *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwSetFCoEParams *pRemoteArgs = NULL;

            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwSetFCoEParams;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwSetFCoEParams;
            if (pLocalArgs->pFsbHwVlanEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwVlanEntry),
                        (pLocalArgs->pFsbHwVlanEntry),
                        sizeof (tFsbHwVlanEntry));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbMbsmHwInit *pLocalArgs = NULL;
            tFsbRemoteNpWrFsMiNpFsbMbsmHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsMiNpFsbMbsmHwInit;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbMbsmHwInit;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pFsbLocRemFilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbLocRemFilterId),
                        (pLocalArgs->pFsbLocRemFilterId),
                        sizeof (tFsbLocRemFilterId));
            }
            break;
        }
        case FS_FSB_MBSM_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbMbsmHwCreateFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbMbsmHwCreateFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbMbsmHwCreateFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbMbsmHwCreateFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY (&(pRemoteArgs->FsbHwFilterEntry),
                        (pLocalArgs->pFsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FilterId), (pLocalArgs->pu4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */
    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilFsbConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilFsbConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbRemoteNpModInfo *pFsbRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilFsbConvertLocalToRemoteNp: "
                    "pFsHwNp or pRemoteHwNp is NULL \n");
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pFsbNpModInfo = &(pFsHwNp->FsbNpModInfo);
    pFsbRemoteNpModInfo = &(pRemoteHwNp->FsbRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
            /* Coversion of Remote arguments to Local arguments needs to be 
             * done only when REMOTE_ONLY call is invoked.
             * In case of FS_MI_NP_FSB_HW_INIT and FS_MI_NP_FSB_HW_DE_INIT, the
             * npcall will be both LOCAL_AND_REMOTE. Hence, for these cases,
             * the conversion need npot be handled */
        case FS_MI_NP_FSB_HW_INIT:
        case FS_MI_NP_FSB_HW_DE_INIT:
        {
            break;
        }
        case FS_FSB_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbHwCreateFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwCreateFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwCreateFilter;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwFilterEntry),
                        &(pRemoteArgs->FsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY ((pLocalArgs->pu4FilterId), &(pRemoteArgs->u4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_FSB_HW_DELETE_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwDeleteFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwDeleteFilter;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwFilterEntry),
                        &(pRemoteArgs->FsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            pLocalArgs->u4FilterId = pRemoteArgs->u4FilterId;
            break;
        }
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwCreateSChannelFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwCreateSChannelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwCreateSChannelFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateSChannelFilter;
            if (pLocalArgs->pFsbHwSChannelFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwSChannelFilterEntry),
                        &(pRemoteArgs->FsbHwSChannelFilterEntry),
                        sizeof (tFsbHwSChannelFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY ((pLocalArgs->pu4FilterId), &(pRemoteArgs->u4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteSChannelFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwDeleteSChannelFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwDeleteSChannelFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteSChannelFilter;
            if (pLocalArgs->pFsbHwSChannelFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwSChannelFilterEntry),
                        &(pRemoteArgs->FsbHwSChannelFilterEntry),
                        sizeof (tFsbHwSChannelFilterEntry));
            }
            pLocalArgs->u4FilterId = pRemoteArgs->u4FilterId;
            break;
        }
        case FS_FSB_HW_SET_FCOE_PARAMS:
        {
            tFsbNpWrFsFsbHwSetFCoEParams *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbHwSetFCoEParams *pRemoteArgs = NULL;

            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbHwSetFCoEParams;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwSetFCoEParams;

            if (pLocalArgs->pFsbHwVlanEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwVlanEntry),
                        &(pRemoteArgs->FsbHwVlanEntry),
                        sizeof (tFsbHwVlanEntry));
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbMbsmHwInit *pLocalArgs = NULL;
            tFsbRemoteNpWrFsMiNpFsbMbsmHwInit *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsMiNpFsbMbsmHwInit;
            pRemoteArgs = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbMbsmHwInit;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwFilterEntry),
                        &(pRemoteArgs->FsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            /* The Remote Node returns the Filter Id after programming and
             * it is returned back to Local node. Therefore, assigning
             * the returned entry in local structure as remote filter id */
            pLocalArgs->pFsbLocRemFilterId->u4RemoteFilterId =
                pRemoteArgs->FsbLocRemFilterId.u4LocalFilterId;

            break;
        }
        case FS_FSB_MBSM_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbMbsmHwCreateFilter *pLocalArgs = NULL;
            tFsbRemoteNpWrFsFsbMbsmHwCreateFilter *pRemoteArgs = NULL;
            pLocalArgs = &pFsbNpModInfo->FsbNpFsFsbMbsmHwCreateFilter;
            pRemoteArgs =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbMbsmHwCreateFilter;
            if (pLocalArgs->pFsbHwFilterEntry != NULL)
            {
                MEMCPY ((pLocalArgs->pFsbHwFilterEntry),
                        &(pRemoteArgs->FsbHwFilterEntry),
                        sizeof (tFsbHwFilterEntry));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY ((pLocalArgs->pu4FilterId), &(pRemoteArgs->u4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilFsbRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tFsbNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilFsbRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                             tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tFsbRemoteNpModInfo *pFsbRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilFsbConvertLocalToRemoteNp: "
                    "pRemoteHwNpInput or pRemoteHwNpOutput is NULL \n");
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pFsbRemoteNpModInfo = &(pRemoteHwNpInput->FsbRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_NP_FSB_HW_INIT:
        {
            tFsbRemoteNpWrFsMiNpFsbHwInit *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbHwInit;
            u1RetVal =
                (UINT1) FsMiNpFsbHwInit (&(pInput->FsbHwFilterEntry),
                                         &(pInput->FsbLocRemFilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_HW_CREATE_FILTER:
        {
            tFsbRemoteNpWrFsFsbHwCreateFilter *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateFilter;
            u1RetVal =
                (UINT1) FsFsbHwCreateFilter (&(pInput->FsbHwFilterEntry),
                                             &(pInput->u4FilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_HW_DELETE_FILTER:
        {
            tFsbRemoteNpWrFsFsbHwDeleteFilter *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteFilter;
            u1RetVal =
                (UINT1) FsFsbHwDeleteFilter (&(pInput->FsbHwFilterEntry),
                                             pInput->u4FilterId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_FSB_HW_DE_INIT:
        {
            tFsbRemoteNpWrFsMiNpFsbHwDeInit *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbHwDeInit;
            u1RetVal =
                (UINT1) FsMiNpFsbHwDeInit (&(pInput->FsbHwFilterEntry),
                                           pInput->FsbLocRemFilterId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        {
            tFsbRemoteNpWrFsFsbHwCreateSChannelFilter *pInput = NULL;
            pInput =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwCreateSChannelFilter;
            u1RetVal =
                (UINT1) FsFsbHwCreateSChannelFilter (&
                                                     (pInput->
                                                      FsbHwSChannelFilterEntry),
                                                     &(pInput->u4FilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        {
            tFsbRemoteNpWrFsFsbHwDeleteSChannelFilter *pInput = NULL;
            pInput =
                &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwDeleteSChannelFilter;
            u1RetVal =
                (UINT1) FsFsbHwDeleteSChannelFilter (&
                                                     (pInput->
                                                      FsbHwSChannelFilterEntry),
                                                     pInput->u4FilterId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_HW_SET_FCOE_PARAMS:
        {
            tFsbRemoteNpWrFsFsbHwSetFCoEParams *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbHwSetFCoEParams;
            u1RetVal = (UINT1) FsFsbHwSetFCoEParams (&(pInput->FsbHwVlanEntry));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        {
            tFsbRemoteNpWrFsMiNpFsbMbsmHwInit *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsMiNpFsbMbsmHwInit;
            u1RetVal =
                (UINT1) FsMiNpFsbMbsmHwInit (&(pInput->FsbHwFilterEntry),
                                             &(pInput->FsbLocRemFilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_FSB_MBSM_HW_CREATE_FILTER:
        {
            tFsbRemoteNpWrFsFsbMbsmHwCreateFilter *pInput = NULL;
            pInput = &pFsbRemoteNpModInfo->FsbRemoteNpFsFsbMbsmHwCreateFilter;
            u1RetVal =
                (UINT1) FsFsbMbsmHwCreateFilter (&(pInput->FsbHwFilterEntry),
                                                 &(pInput->u4FilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* _FSBNPUTIL_C_ */
