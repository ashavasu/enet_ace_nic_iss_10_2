/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: dsmonnputil.c,v 1.2 2013/11/14 11:54:12 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *            (Dual Unit Stacking Environment)
 *
 *******************************************************************/

#ifndef __DSMONNPUTIL_C__
#define __DSMONNPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskDsmonNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef DSMON_WANTED
VOID
NpUtilMaskDsmonNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonRemoteNpModInfo *pRemoteDsmonNpModInfo = NULL;
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    BOOL1               bRemPortCheck = FNP_FALSE;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    pDsmonNpModInfo = &(pFsHwNp->DsmonNpModInfo);
    pRemoteDsmonNpModInfo = &(pRemoteHwNp->DsmonRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_DSMON_ADD_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONAddFlowStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONAddFlowStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        case FS_DSMON_COLLECT_STATS:
        {
            tDsmonNpWrFsDSMONCollectStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONCollectStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        case FS_DSMON_ENABLE_PROBE:
        {
            tDsmonNpWrFsDSMONEnableProbe *pEntry = NULL;
            tDsmonRemoteNpWrFsDSMONEnableProbe *pRemEntry = NULL;

            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONEnableProbe;
            pRemEntry = &pRemoteDsmonNpModInfo->DsmonRemoteNpFsDSMONEnableProbe;
            if (pEntry->u2VlanId == 0)
            {
                bRemPortCheck = FNP_TRUE;
                pFsHwNp->u4IfIndex = pEntry->u4InterfaceIdx;
            }
            else
            {
                NpUtilMaskHwPortList (pEntry->PortList,
                                      (UINT1 *) (&pRemEntry->PortList),
                                      &HwPortInfo, &u4LocalPortCount,
                                      &u4RemotePortCount, ISS_PORT_LIST_SIZE);
                if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
                else if (u4RemotePortCount != 0)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            break;
        }
        case FS_DSMON_DISABLE_PROBE:
        {
            tDsmonNpWrFsDSMONDisableProbe *pEntry = NULL;
            tDsmonRemoteNpWrFsDSMONDisableProbe *pRemEntry = NULL;

            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONDisableProbe;
            pRemEntry =
                &pRemoteDsmonNpModInfo->DsmonRemoteNpFsDSMONDisableProbe;
            if (pEntry->u2VlanId == 0)
            {
                bRemPortCheck = FNP_TRUE;
                pFsHwNp->u4IfIndex = pEntry->u4InterfaceIdx;
            }
            else
            {
                NpUtilMaskHwPortList (pEntry->PortList,
                                      (UINT1 *) (&pRemEntry->PortList),
                                      &HwPortInfo, &u4LocalPortCount,
                                      &u4RemotePortCount, ISS_PORT_LIST_SIZE);
                if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                }
                else if (u4RemotePortCount != 0)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }
            break;
        }
        case FS_DSMON_HW_SET_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_DSMON_HW_GET_STATUS:
        {
            break;
        }
        case FS_DSMON_REMOVE_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONRemoveFlowStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONRemoveFlowStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        default:
            break;
    }
    if (bRemPortCheck == FNP_TRUE)
    {
        if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
        else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                 FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
    }
    return;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilDsmonConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilDsmonConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonRemoteNpModInfo *pDsmonRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pDsmonNpModInfo = &(pFsHwNp->DsmonNpModInfo);
    pDsmonRemoteNpModInfo = &(pRemoteHwNp->DsmonRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tDsmonRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_DSMON_ADD_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONAddFlowStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONAddFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONAddFlowStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONAddFlowStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        case FS_DSMON_COLLECT_STATS:
        {
            tDsmonNpWrFsDSMONCollectStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONCollectStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONCollectStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONCollectStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            if (pLocalArgs->pStatsCnt != NULL)
            {
                MEMCPY (&(pRemoteArgs->StatsCnt), (pLocalArgs->pStatsCnt),
                        sizeof (tRmon2Stats));
            }
            break;
        }
        case FS_DSMON_ENABLE_PROBE:
        {
            tDsmonNpWrFsDSMONEnableProbe *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONEnableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONEnableProbe;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONEnableProbe;
            pRemoteArgs->u4InterfaceIdx = pLocalArgs->u4InterfaceIdx;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                        sizeof (tPortList));
            }
            break;
        }
        case FS_DSMON_DISABLE_PROBE:
        {
            tDsmonNpWrFsDSMONDisableProbe *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONDisableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONDisableProbe;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONDisableProbe;
            pRemoteArgs->u4InterfaceIdx = pLocalArgs->u4InterfaceIdx;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                        sizeof (tPortList));
            }
            break;
        }
        case FS_DSMON_HW_SET_STATUS:
        {
            tDsmonNpWrFsDSMONHwSetStatus *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONHwSetStatus *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONHwSetStatus;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONHwSetStatus;
            pRemoteArgs->u4DSMONStatus = pLocalArgs->u4DSMONStatus;
            break;
        }
        case FS_DSMON_HW_GET_STATUS:
        {
            break;
        }
        case FS_DSMON_REMOVE_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONRemoveFlowStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONRemoveFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONRemoveFlowStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONRemoveFlowStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilDsmonConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilDsmonConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonRemoteNpModInfo *pDsmonRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pDsmonNpModInfo = &(pFsHwNp->DsmonNpModInfo);
    pDsmonRemoteNpModInfo = &(pRemoteHwNp->DsmonRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_DSMON_ADD_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONAddFlowStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONAddFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONAddFlowStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONAddFlowStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        case FS_DSMON_COLLECT_STATS:
        {
            tDsmonNpWrFsDSMONCollectStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONCollectStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONCollectStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONCollectStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            if (pLocalArgs->pStatsCnt != NULL)
            {
                MEMCPY ((pLocalArgs->pStatsCnt), &(pRemoteArgs->StatsCnt),
                        sizeof (tRmon2Stats));
            }
            break;
        }
        case FS_DSMON_ENABLE_PROBE:
        {
            tDsmonNpWrFsDSMONEnableProbe *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONEnableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONEnableProbe;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONEnableProbe;
            pLocalArgs->u4InterfaceIdx = pRemoteArgs->u4InterfaceIdx;
            pLocalArgs->u2VlanId = pRemoteArgs->u2VlanId;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY ((pLocalArgs->PortList), &(pRemoteArgs->PortList),
                        sizeof (tPortList));
            }
            break;
        }
        case FS_DSMON_DISABLE_PROBE:
        {
            tDsmonNpWrFsDSMONDisableProbe *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONDisableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONDisableProbe;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONDisableProbe;
            pLocalArgs->u4InterfaceIdx = pRemoteArgs->u4InterfaceIdx;
            pLocalArgs->u2VlanId = pRemoteArgs->u2VlanId;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY ((pLocalArgs->PortList), &(pRemoteArgs->PortList),
                        sizeof (tPortList));
            }
            break;
        }
        case FS_DSMON_HW_SET_STATUS:
        {
            tDsmonNpWrFsDSMONHwSetStatus *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONHwSetStatus *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONHwSetStatus;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONHwSetStatus;
            pLocalArgs->u4DSMONStatus = pRemoteArgs->u4DSMONStatus;
            break;
        }
        case FS_DSMON_HW_GET_STATUS:
        {
            break;
        }
        case FS_DSMON_REMOVE_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONRemoveFlowStats *pLocalArgs = NULL;
            tDsmonRemoteNpWrFsDSMONRemoveFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pDsmonNpModInfo->DsmonNpFsDSMONRemoveFlowStats;
            pRemoteArgs =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONRemoveFlowStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilDsmonRemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tDsmonNpModInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilDsmonRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                               tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tDsmonRemoteNpModInfo *pDsmonRemoteNpModInfo = NULL;

    if ((NULL == pRemoteHwNpInput) || (NULL == pRemoteHwNpOutput))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pDsmonRemoteNpModInfo = &(pRemoteHwNpInput->DsmonRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_DSMON_ADD_FLOW_STATS:
        {
            tDsmonRemoteNpWrFsDSMONAddFlowStats *pInput = NULL;
            pInput = &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONAddFlowStats;
            u1RetVal = FsDSMONAddFlowStats (pInput->FlowStatsTuple);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_COLLECT_STATS:
        {
            tDsmonRemoteNpWrFsDSMONCollectStats *pInput = NULL;
            pInput = &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONCollectStats;
            u1RetVal =
                FsDSMONCollectStats (pInput->FlowStatsTuple,
                                     &(pInput->StatsCnt));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_ENABLE_PROBE:
        {
            tDsmonRemoteNpWrFsDSMONEnableProbe *pInput = NULL;
            pInput = &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONEnableProbe;
            u1RetVal =
                FsDSMONEnableProbe (pInput->u4InterfaceIdx, pInput->u2VlanId,
                                    pInput->PortList);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_DISABLE_PROBE:
        {
            tDsmonRemoteNpWrFsDSMONDisableProbe *pInput = NULL;
            pInput = &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONDisableProbe;
            u1RetVal =
                FsDSMONDisableProbe (pInput->u4InterfaceIdx, pInput->u2VlanId,
                                     pInput->PortList);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_HW_SET_STATUS:
        {
            tDsmonRemoteNpWrFsDSMONHwSetStatus *pInput = NULL;
            pInput = &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONHwSetStatus;
            u1RetVal = FsDSMONHwSetStatus (pInput->u4DSMONStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_HW_GET_STATUS:
        {
            u1RetVal = FsDSMONHwGetStatus ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_DSMON_REMOVE_FLOW_STATS:
        {
            tDsmonRemoteNpWrFsDSMONRemoveFlowStats *pInput = NULL;
            pInput =
                &pDsmonRemoteNpModInfo->DsmonRemoteNpFsDSMONRemoveFlowStats;
            u1RetVal = FsDSMONRemoveFlowStats (pInput->FlowStatsTuple);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* DSMON_WANTED */
#endif /* __DSMONNPUTIL_C__ */
