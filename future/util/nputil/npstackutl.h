/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: npstackutl.h,v 1.26 2016/06/30 10:10:21 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __NPSTACKUTL_H__
#define __NPSTACKUTL_H__

#include "nputlremnp.h"
#include "nputil.h"
/* Header file to be included to fetch the NP support availability
 * based on the TARGET_ASIC chosen in the build.
 * Currently BCM specific support file is included..
 * A new header file can be added and included if the support
 * is required for other platforms.
 */
#ifdef BCMX_WANTED
#include "npbcmsup.h"
#else
#include "npgensup.h"
#endif


#define FNP_RESET 0
#define FNP_SET 1
#define STANDBY_NOT_PRESENT 0
#define NP_UTIL_BULK_NP_COUNT 10
#define NP_UTIL_IPC_TIMEOUT 600
#define NP_UTIL_MAX_IPC_RETRANSMISSION 1
#define NP_UTIL_INVALID_INDEX 0xffffffff

#define NPUTIL_DBG_FLAG 0x00000000

#define NPUTIL_DBG_ALL 0xffffffff
#define NPUTIL_CTRL_PATH_TRC 0x00000001
#define NPUTIL_DBG_ERR 0x00000002
#define NPUTIL_DBG_MSG 0x00000004
#define NPUTIL_DBG_MEM 0x00000008
#define NPUTIL_DBG_TMR 0x00000010

#ifdef TRACE_WANTED

#define NPUTIL_DBG(u4Value, pu1Format)     \
           if((u4Value) == (u4Value & NPUTIL_DBG_FLAG)) \
               UtlTrcLog  (NPUTIL_DBG_FLAG, u4Value, "NPUTIL", pu1Format)
#define NPUTIL_DBG1(u4Value, pu1Format, Arg1)     \
           if((u4Value) == (u4Value & NPUTIL_DBG_FLAG)) \
               UtlTrcLog  (NPUTIL_DBG_FLAG, u4Value, "NPUTIL", pu1Format, Arg1)
#define NPUTIL_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
           if((u4Value) == (u4Value & NPUTIL_DBG_FLAG)) \
               UtlTrcLog  (NPUTIL_DBG_FLAG, u4Value, "NPUTIL", pu1Format, Arg1, Arg2)

#else

#define NPUTIL_DBG(u4Value, pu1Format) UNUSED_PARAM (u4Value)
#define NPUTIL_DBG1(u4Value, pu1Format, Arg1) UNUSED_PARAM (u4Value)
#define NPUTIL_DBG2(u4Value, pu1Format, Arg1, Arg2) UNUSED_PARAM (u4Value)
#endif


UINT1               NpUtilConvertPorts (tFsHwNp * pFsHwNp);
VOID                NpUtilRelRemoteHwNpMem (tRemoteHwNp * pRemoteHwNp);
tRemoteHwNp        *NpUtilAllocRemoteHwNpMem (VOID);
PUBLIC UINT4         NpUtilRemoteSemCreate (VOID);
PUBLIC VOID         NpUtilRemoteNpUnLock (VOID);
PUBLIC INT4         NpUtilRemoteNpLock (VOID);
UINT1 
NpUtilRemoteClntHwProgram (tRemoteHwNp *pRemoteHwNp, UINT4 u4RpcWaitStatus);
INT4
NpUtilCheckRemoteIPCStatus (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                            UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1
NpUtilMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 * pi4LocalNpRetVal, INT4 * pi4RemoteNpRetVal);
VOID
NpUtilRemoveStackPort (tHwPortArray * pHwPorts);
UINT1
NpUtilIsRemotePort (UINT4 u4IfIndex, tHwPortInfo * pHwPorts);
UINT1   
NpUtilIsPortChannel (UINT4 u4IfIndex);
VOID
NpUtilMaskHwPortList (UINT1 * pu1LocalUnitHwPortList,
                      UINT1 * pu1RemoteUnitHwPortList,
                      tHwPortInfo * pLocalUnitHwPortRange, 
        UINT4 * pu4LocalPortCount,
                      UINT4 * pu4RemotePortCount, UINT4 u4ListSize);
VOID 
NpUtilStackHwPortList (UINT1 * pu1LocalUnitHwPortList,
                       UINT1 * pu1RemoteUnitHwPortList,
                       tHwPortInfo * pLocalUnitHwPortRange, 
         UINT4 * pu4LocalPortCount,
                       UINT4 * pu4RemotePortCount, UINT4 u4ListSize);
VOID 
NpUtilMaskHwPortArray(tHwPortArray * pHwPorts,
                              tRemoteHwPortArray * pRemoteHwPorts,
                              tHwPortInfo * pLocalUnitHwPortRange,
                              UINT4 *pu4LocalPortCount,
                              UINT4 *pu4RemotePortCount);
VOID
NpUtilMaskAndAddStackingPort (tHwPortArray * pHwPorts, 
                                     tRemoteHwPortArray * pRemoteHwPorts,
                                     tHwPortInfo * pLocalUnitHwPortRange, 
                                     UINT4 *pu4LocalPortCount, 
                                     UINT4 * pu4RemotePortCount);
VOID
NpUtilMaskAndAddStkPortInRemPorts (tRemoteHwPortArray * pRemoteHwPorts,
                              tHwPortInfo * pHwPortInfo,
                              UINT4 *pu4RemotePortCount);
VOID
NpUtilMaskRemoteHwPortArray (tRemoteHwPortArray * pRemoteHwPorts,
                      tHwPortInfo * pHwPortInfo, UINT4 *pu4RemotePortCount);
VOID 
NpUtilGetMirrorPortsCount (tIssHwSourceId * pSourceList, UINT4 *pu4DestList,
                           UINT4 *pu4SrcList, UINT4 *pu4SrcPortNum,
                           UINT4 *pu4DestPortNum);
#ifdef ISS_WANTED
VOID 
NpUtilIssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry,
                           tRemoteIssL2FilterEntry * pRemoteIssL2FilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange, 
      UINT1 *pu1NpCallStatus);
VOID
NpUtilIssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry,
                           tRemoteIssL3FilterEntry * pRemoteIssL3FilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange, 
      UINT1 *pu1NpCallStatus);
#endif /* ISS_WANTED */
VOID
NpUtilIssHwUpdateUDFilter (tIssUDBFilterEntry *pIssUDBFilterEntry,
                           tIssUDBFilterEntry *pRemoteIssUDBFilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange, 
      UINT1 *pu1NpCallStatus);
#ifdef IGS_WANTED
VOID 
NpUtilIgsHwUpdateIpmcEntry (tIgsHwIpFwdInfo  *pIgsHwIpFwdInfo,
                                 tIgsHwIpFwdInfo  *pRemoteIgsHwIpFwdInfo,
                                 tHwPortInfo * pHwPortInfo,
                                 UINT1 *pu1NpCallStatus);
#endif /* IGS_WANTED */
#ifdef PVRST_WANTED
VOID
NpUtilMaskPvrstNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* PVRST_WANTED */
#ifdef MSTP_WANTED 
VOID
NpUtilMaskMstpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* MSTP_WANTED */
#ifdef RSTP_WANTED
VOID
NpUtilMaskRstpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* RSTP_WANTED */
#ifdef CFA_WANTED
VOID
NpUtilMaskCfaNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1
NpUtilCfaMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 * pi4LocalNpRetVal, INT4 * pi4RemoteNpRetVal);

#endif /* CFA_WANTED */
#ifdef VCM_WANTED
VOID
NpUtilMaskVcmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* VCM_WANTED */
#ifdef PNAC_WANTED
VOID
NpUtilMaskPnacNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* PNAC_WANTED */
#ifdef EOAM_WANTED 
VOID
NpUtilMaskEoamNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* EOAM_WANTED */
#ifdef EOAM_FM_WANTED
VOID
NpUtilMaskEoamFmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* EOAM_FM_WANTED */
#ifdef IGS_WANTED
VOID
NpUtilMaskIgsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* IGS_WANTED */
#ifdef MLDS_WANTED 
VOID
NpUtilMaskMldsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* MLDS_WANTED */
#ifdef ISS_WANTED
VOID
NpUtilMaskIsssysNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                         UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1
NpUtilMergeLocalRemoteIsssysNpOutput (tFsHwNp * pFsHwNp, 
                                      tRemoteHwNp * pRemoteHwNp, INT4 * pi4LocalNpRetVal, INT4 * pi4RemoteNpRetVal);

VOID
IssCopyLocalToRemoteL2Filter(tRemoteIssL2FilterEntry *pRemoteArgs, 
                             tIssL2FilterEntry *pLocalArgs);

VOID
IssCopyLocalToRemoteL3Filter(tRemoteIssL3FilterEntry *pRemoteArgs, 
                             tIssL3FilterEntry *pLocalArgs);

VOID
IssCopyRemoteToLocalL2Filter(tIssL2FilterEntry *pLocalArgs, 
                             tRemoteIssL2FilterEntry *pRemoteArgs);

VOID
IssCopyRemoteToLocalL3Filter(tIssL3FilterEntry *pLocalArgs, 
                             tRemoteIssL3FilterEntry *pRemoteArgs);

#endif /* ISS_WANTED */
#ifdef VLAN_WANTED 
INT4
NpUtilMaskVlanNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1
NpUtilVlanMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 * pi4LocalNpRetVal, INT4 * pi4RemoteNpRetVal);

#endif /* VLAN_WANTED */
#ifdef LA_WANTED 
VOID
NpUtilMaskLaNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                     UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* LA_WANTED */
#ifdef RM_WANTED
VOID
NpUtilMaskRmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* RM_WANTED */
#ifdef MBSM_WANTED
VOID
NpUtilMaskMbsmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* MBSM_WANTED */

#ifdef QOSX_WANTED
VOID
NpUtilMaskQosxNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* QOSX_WANTED */
#ifdef ELPS_WANTED
VOID
NpUtilMaskElpsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
VOID
NpUtilElpsPortDisjoint (tElpsHwPgSwitchInfo * pElpsHwPgSwitchInfo,
                   tElpsHwPgSwitchInfo * pRemElpsHwPgSwitchInfo,
                   tHwPortInfo * pLocalHwPortInfo,
                   UINT1 *pu1NpCallStatus);
#ifdef MBSM_WANTED
VOID
NpUtilElpsMbsmPortDisjoint (tElpsHwPgSwitchInfo * pRemElpsMbsmHwPgSwitchInfo,
                   tHwPortInfo * pLocalHwPortInfo,
                   UINT1 *pu1NpCallStatus);
#endif /* MBSM_WANTED */
VOID
NpUtilIsPortOrService (tElpsHwPgSwitchInfo * pElpsPortOrServiceHwPgSwitchInfo,
                         tElpsHwPgSwitchInfo * pRemElpsPortOrServiceHwPgSwitchInfo,
                         tHwPortInfo * pHwPortInfo,
                           UINT1 *pu1NpCallStatus);

#endif /* ELPS_WANTED */
#ifdef ERPS_WANTED
VOID
NpUtilMaskErpsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif
#ifdef RMON_WANTED
VOID
NpUtilMaskRmonNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* RMON_WANTED */
#ifdef RMON2_WANTED
VOID
NpUtilMaskRmonv2NpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                         UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* RMON2_WANTED */
#ifdef DSMON_WANTED
VOID
NpUtilMaskDsmonNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* DSMON_WANTED */
#ifdef SYNCE_WANTED
VOID
NpUtilMaskSynceNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* SYNCE_WANTED */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
VOID
NpUtilMaskIpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1
NpUtilIpMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 * pi4LocalNpRetVal, INT4 * pi4RemoteNpRetVal);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
#ifdef ECFM_WANTED 
VOID
NpUtilMaskEcfmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
VOID
NpUtilPortCheck (UINT4 u4IfIndex, tHwPortInfo * pPortCheckHwPortInfo,
   UINT1 *pu1NpCallStatus);
#endif /* ECFM_WANTED */
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
VOID NpUtilMaskIpv6NpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1 NpUtilIpv6MergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal,INT4 *pi4RemoteNpRetVal);
#endif /* IP6_WANTED || LNXIP6_WANTED */

#ifdef MPLS_WANTED
VOID
NpUtilMaskMplsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                 UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1 NpUtilMplsMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal,INT4 *pi4RemoteNpRetVal);
#endif /*MPLS_WANTED*/
#if defined (IGMP_WANTED) 
VOID
NpUtilMaskIgmpNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* (IGMP_WANTED) */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
VOID NpUtilMaskIpmcNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1 NpUtilIpmcMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal,INT4 *pi4RemoteNpRetVal);
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED*/ 
#if defined (MLD_WANTED) 
VOID
NpUtilMaskMldNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
#endif /* (MLD_WANTED) */
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
VOID NpUtilMaskIp6mcNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                              UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1 NpUtilIp6mcMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal,INT4 *pi4RemoteNpRetVal);
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */ 
#ifdef FSB_WANTED
VOID NpUtilMaskFsbNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
UINT1 NpUtilFsbMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal, INT4 *pi4RemoteNpRetVal);
#endif  /* FSB_WANTED */

#ifdef _NPSTACKUTL_C_
tOsixSemId          gRemoteNpSemId = 0;
tRemoteHwNp         gRemoteHwNp;
tRemoteHwPortArray  gLocalHwPortArray;
tRemoteHwPortArray  gLocalPortArray;
tRemoteHwPortArray  gRemotePortArray;
INT4                gRemoteStackingPort = FNP_RESET;
tHwPortInfo         gRemoteHwPortInfo;
tRemoteHwNp         gRemoteStackingHwNp;
tOsixSemId          gRemoteExecSemId = 0;
tRemoteHwNp         gRemoteSvcInputHwNp;
tRemoteHwNp         gRemoteSvcOutputHwNp;
tRemoteHwNp         gGetCallUpdateHwNp;
UINT4               gu4IpcReplyReceived = FNP_TRUE;
UINT4               gu4RetransmissionCount = NP_UTIL_MAX_IPC_RETRANSMISSION; 
tTimerListId        gRemoteNpTimerListId = 0;
tTmrBlk             gRemoteNpTmrBlk;
#else
extern tOsixSemId gRemoteNpSemId;
extern tRemoteHwNp gRemoteHwNp;
extern tRemoteHwPortArray  gLocalHwPortArray;
extern tRemoteHwPortArray  gLocalPortArray;
extern tRemoteHwPortArray  gRemotePortArray;
extern INT4                gRemoteStackingPort;
extern tHwPortInfo         gRemoteHwPortInfo;
extern tRemoteHwNp         gRemoteStackingHwNp;
extern tOsixSemId          gRemoteExecSemId;
extern tRemoteHwNp         gRemoteSvcInputHwNp;
extern tRemoteHwNp         gRemoteSvcOutputHwNp;
extern tRemoteHwNp         gGetCallUpdateHwNp;
extern UINT4               gu4RetransmissionCount;
extern UINT4               gu4IpcReplyReceived;
extern tTimerListId        gRemoteNpTimerListId;
extern tTmrBlk             gRemoteNpTmrBlk;
#endif /* _NPSTACKUTL_C_ */

tRemoteHwNp* NpRemoteClntHwNpInMemAlloc (VOID);
VOID NpRemoteClntHwNpInMemRel(tRemoteHwNp *);
tRemoteHwNp* NpRemoteClntHwNpOutMemAlloc (VOID);
VOID NpRemoteClntHwNpOutMemRel(tRemoteHwNp *);
tRemoteHwNp * NpGetCallHwNpMemAlloc (VOID);
VOID NpGetCallHwNpMemRel (tRemoteHwNp * pRemoteHwNp);

PUBLIC UINT4 NpRemoteExecSemCreate (VOID);
PUBLIC VOID NpRemoteExecSemUnLock (VOID);
PUBLIC INT4 NpRemoteExecSemLock (VOID);

VOID NpUtilRemoteNpTimerCallBack (tTimerListId TimerListId);
UINT1 NpUtilGetRemotePort (tRemoteHwNp * pRemoteHwNp, UINT4 *pu4Port, 
                        UINT1 *pu1Flag, tPortList *pPortList);
VOID NpUtilGetRemotePortList (UINT1 *pTagPortList, UINT1 *pUnTagPortList,
                           UINT4 u4ListSize, UINT1 *pPortList);

UINT1
NpUtilIsNpSupported (UINT4 u4ModuleId, UINT4 u4OpCode);
UINT4
NpUtilIPCWaitStatus (UINT4 u4ModuleId, UINT4 u4OpCode);

#endif /* __NPSTACKUTL_H__ */
