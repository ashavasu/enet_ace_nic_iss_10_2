/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamfmnputil.c,v 1.2 2013/11/14 11:54:12 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __EOAMFMNPUTIL_C__
#define __EOAMFMNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskEoamFmNpPorts                          */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef EOAM_FM_WANTED
VOID
NpUtilMaskEoamFmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                         UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FM_NP_REGISTER_FOR_FAILURE_INDICATIONS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case NP_FM_FAILURE_INDICATION_CALLBACK_FUNC:
        {
            if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex,
                                    &HwPortInfo) == FNP_SUCCESS)
            {
                /* This is for remote port
                 * set the NP Call status NPUTIL_INVOKE_REMOTE_NP
                 * for executing this call at the remote unit
                 */
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        default:
            break;
    }
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamfmConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamfmConvertLocalToRemoteNp (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamfmNpModInfo   *pEoamfmNpModInfo = NULL;
    tEoamfmRemoteNpModInfo *pEoamfmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pEoamfmNpModInfo = &(pFsHwNp->EoamfmNpModInfo);
    pEoamfmRemoteNpModInfo = &(pRemoteHwNp->EoamfmRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tEoamfmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FM_NP_REGISTER_FOR_FAILURE_INDICATIONS:
        {
            break;
        }
        case NP_FM_FAILURE_INDICATION_CALLBACK_FUNC:
        {
            tEoamfmNpWrNpFmFailureIndicationCallbackFunc *pLocalArgs = NULL;
            tEoamfmRemoteNpWrNpFmFailureIndicationCallbackFunc *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pEoamfmNpModInfo->EoamfmNpNpFmFailureIndicationCallbackFunc;
            pRemoteArgs =
                &pEoamfmRemoteNpModInfo->
                EoamfmRemoteNpNpFmFailureIndicationCallbackFunc;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1EventType = pLocalArgs->u1EventType;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamfmConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamfmConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp,
                                    tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamfmNpModInfo   *pEoamfmNpModInfo = NULL;
    tEoamfmRemoteNpModInfo *pEoamfmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pEoamfmNpModInfo = &(pFsHwNp->EoamfmNpModInfo);
    pEoamfmRemoteNpModInfo = &(pRemoteHwNp->EoamfmRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FM_NP_REGISTER_FOR_FAILURE_INDICATIONS:
        {
            break;
        }
        case NP_FM_FAILURE_INDICATION_CALLBACK_FUNC:
        {
            tEoamfmNpWrNpFmFailureIndicationCallbackFunc *pLocalArgs = NULL;
            tEoamfmRemoteNpWrNpFmFailureIndicationCallbackFunc *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pEoamfmNpModInfo->EoamfmNpNpFmFailureIndicationCallbackFunc;
            pRemoteArgs =
                &pEoamfmRemoteNpModInfo->
                EoamfmRemoteNpNpFmFailureIndicationCallbackFunc;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1EventType = pRemoteArgs->u1EventType;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamfmRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEoamfmNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamfmRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                                tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamfmRemoteNpModInfo *pEoamfmRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pEoamfmRemoteNpModInfo = &(pRemoteHwNpInput->EoamfmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FM_NP_REGISTER_FOR_FAILURE_INDICATIONS:
        {
            u1RetVal = FmNpRegisterForFailureIndications ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case NP_FM_FAILURE_INDICATION_CALLBACK_FUNC:
        {
            tEoamfmRemoteNpWrNpFmFailureIndicationCallbackFunc *pInput = NULL;
            pInput =
                &pEoamfmRemoteNpModInfo->
                EoamfmRemoteNpNpFmFailureIndicationCallbackFunc;
            u1RetVal =
                NpFmFailureIndicationCallbackFunc (pInput->u4IfIndex,
                                                   pInput->u1EventType,
                                                   pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* EOAM_FM_WANTED */

#endif /* __EOAMFMNPUTIL_C__ */
