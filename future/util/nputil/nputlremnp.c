/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: nputlremnp.c,v 1.15 2015/09/09 11:00:01 siva Exp $
 *
 * Description: This file contains functions for Local to Remote 
 *            structure conversion and vice-versa for HA+ stacking
 *
 *******************************************************************/

#ifndef __NPUTLREMNP_C__
#define __NPUTLREMNP_C__

#include "nputlremnp.h"

/***************************************************************************
 *
 *    Function Name       : NpUtilConvertLocalArgsToRemoteArgs
 *
 *    Description         :
 *
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilConvertLocalArgsToRemoteArgs (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    pRemoteHwNp->i4Length = sizeof (UINT4) + sizeof (INT4) + sizeof (tNpModule);

    switch (pFsHwNp->NpModuleId)
    {
        case NP_VLAN_MOD:
        {
#ifdef VLAN_WANTED
            u1RetVal = NpUtilVlanConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_CFA_MOD:
        {
#ifdef CFA_WANTED
            u1RetVal = NpUtilCfaConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_RSTP_MOD:
        {
#ifdef RSTP_WANTED
            u1RetVal = NpUtilRstpConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_MSTP_MOD:
        {
#ifdef MSTP_WANTED
            u1RetVal = NpUtilMstpConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_PVRST_MOD:
        {
#ifdef PVRST_WANTED
            u1RetVal = NpUtilPvrstConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_VCM_MOD:
        {
#ifdef VCM_WANTED
            u1RetVal = NpUtilVcmConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_PNAC_MOD:
        {
#ifdef PNAC_WANTED
            u1RetVal = NpUtilPnacConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IGS_MOD:
        {
#ifdef IGS_WANTED
            u1RetVal = NpUtilIgsConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_MLDS_MOD:
        {
#ifdef MLDS_WANTED
            u1RetVal = NpUtilMldsConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_ISSSYS_MOD:
        {
#ifdef ISS_WANTED
            u1RetVal =
                NpUtilIsssysConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_EOAM_MOD:
        {
#ifdef EOAM_WANTED
            u1RetVal = NpUtilEoamConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_EOAMFM_MOD:
        {
#ifdef EOAM_FM_WANTED
            u1RetVal =
                NpUtilEoamfmConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_LA_MOD:
        {
#ifdef LA_WANTED
            u1RetVal = NpUtilLaConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_RM_MOD:
        {
#ifdef RM_WANTED
            u1RetVal = NpUtilRmConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_MBSM_MOD:
        {
#ifdef MBSM_WANTED
            u1RetVal = NpUtilMbsmConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_QOSX_MOD:
        {
#ifdef QOSX_WANTED
            u1RetVal = NpUtilQosxConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_ELPS_MOD:
        {
#ifdef ELPS_WANTED
            u1RetVal = NpUtilElpsConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_RMON_MOD:
        {
#ifdef RMON_WANTED
            u1RetVal = NpUtilRmonConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_RMONv2_MOD:
        {
#ifdef RMON2_WANTED
            u1RetVal =
                NpUtilRmonv2ConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_ERPS_MOD:
        {
#ifdef ERPS_WANTED
            u1RetVal = NpUtilErpsConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_DSMON_MOD:
        {
#ifdef DSMON_WANTED
            u1RetVal = NpUtilDsmonConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_SYNCE_MOD:
        {
#ifdef SYNCE_WANTED
            u1RetVal = NpUtilSynceConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IP_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            u1RetVal = NpUtilIpConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
            break;
        }
        case NP_ECFM_MOD:
        {
#ifdef ECFM_WANTED
            u1RetVal = NpUtilEcfmConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            u1RetVal = NpUtilIpv6ConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_MPLS_MOD:
        {
#ifdef MPLS_WANTED
            u1RetVal = NpUtilMplsConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IGMP_MOD:
        {
#ifdef IGMP_WANTED
            u1RetVal = NpUtilIgmpConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            u1RetVal = NpUtilIpmcConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_MLD_MOD:
        {
#ifdef MLD_WANTED
            u1RetVal = NpUtilMldConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
            u1RetVal = NpUtilIp6mcConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED
            u1RetVal = NpUtilFsbConvertLocalToRemoteNp (pFsHwNp, pRemoteHwNp);
#endif  /* FSB_WANTED */
            break;
        }
        default:
            break;
    }

    return u1RetVal;

}

/***************************************************************************
 *
 *    Fdunction Name       : NpUtilConvertRemoteArgsToLocalArgs
 *
 *    Description         :
 *
 *
 *    Input(s)            : pRemoteHwNp  - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilConvertRemoteArgsToLocalArgs (tRemoteHwNp * pRemoteHwNp,
                                    tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;

    switch (pRemoteHwNp->NpModuleId)
    {
        case NP_VLAN_MOD:
        {
#ifdef VLAN_WANTED
            u1RetVal = NpUtilVlanConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_CFA_MOD:
        {
#ifdef CFA_WANTED
            u1RetVal = NpUtilCfaConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_RSTP_MOD:
        {
#ifdef RSTP_WANTED
            u1RetVal = NpUtilRstpConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_MSTP_MOD:
        {
#ifdef MSTP_WANTED
            u1RetVal = NpUtilMstpConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_PVRST_MOD:
        {
#ifdef PVRST_WANTED
            u1RetVal = NpUtilPvrstConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_VCM_MOD:
        {
#ifdef VCM_WANTED
            u1RetVal = NpUtilVcmConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_PNAC_MOD:
        {
#ifdef PNAC_WANTED
            u1RetVal = NpUtilPnacConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IGS_MOD:
        {
#ifdef IGS_WANTED
            u1RetVal = NpUtilIgsConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_MLDS_MOD:
        {
#ifdef MLDS_WANTED
            u1RetVal = NpUtilMldsConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_ISSSYS_MOD:
        {
#ifdef ISS_WANTED
            u1RetVal =
                NpUtilIsssysConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_EOAM_MOD:
        {
#ifdef EOAM_WANTED
            u1RetVal = NpUtilEoamConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_EOAMFM_MOD:
        {
#ifdef EOAM_FM_WANTED
            u1RetVal =
                NpUtilEoamfmConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_LA_MOD:
        {
#ifdef LA_WANTED
            u1RetVal = NpUtilLaConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_RM_MOD:
        {
#ifdef RM_WANTED
            u1RetVal = NpUtilRmConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_MBSM_MOD:
        {
#ifdef MBSM_WANTED
            u1RetVal = NpUtilMbsmConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }

        case NP_QOSX_MOD:
        {
#ifdef QOSX_WANTED
            u1RetVal = NpUtilQosxConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_ELPS_MOD:
        {
#ifdef ELPS_WANTED
            u1RetVal = NpUtilElpsConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_ERPS_MOD:
        {
#ifdef ERPS_WANTED
            u1RetVal = NpUtilErpsConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_RMON_MOD:
        {
#ifdef RMON_WANTED
            u1RetVal = NpUtilRmonConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_RMONv2_MOD:
        {
#ifdef RMON2_WANTED
            u1RetVal =
                NpUtilRmonv2ConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_DSMON_MOD:
        {
#ifdef DSMON_WANTED
            u1RetVal = NpUtilDsmonConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_SYNCE_MOD:
        {
#ifdef SYNCE_WANTED
            u1RetVal = NpUtilSynceConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IP_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            u1RetVal = NpUtilIpConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
            break;
        }
        case NP_ECFM_MOD:
        {
#ifdef ECFM_WANTED
            u1RetVal = NpUtilEcfmConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            u1RetVal = NpUtilIpv6ConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_MPLS_MOD:
        {
#ifdef MPLS_WANTED
            u1RetVal = NpUtilMplsConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IGMP_MOD:
        {
#ifdef IGMP_WANTED
            u1RetVal = NpUtilIgmpConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            u1RetVal = NpUtilIpmcConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_MLD_MOD:
        {
#ifdef MLD_WANTED
            u1RetVal = NpUtilMldConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
            u1RetVal = NpUtilIp6mcConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED
            u1RetVal = NpUtilFsbConvertRemoteToLocalNp (pRemoteHwNp, pFsHwNp);
#endif  /* FSB_WANTED */
            break;
        }
        default:
            break;
    }

    return u1RetVal;

}

/***************************************************************************
 *
 *    Function Name       : NpUtilRemoteSvcHwProgram
 *
 *    Description         :
 *
 *
 *    Input(s)            : pRemoteHwNpInput  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNpOutput - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                          tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;

    if ((pRemoteHwNpInput == NULL) || (pRemoteHwNpOutput == NULL))
    {
        return FNP_FAILURE;
    }
    switch (pRemoteHwNpInput->NpModuleId)
    {
        case NP_VLAN_MOD:
        {
#ifdef VLAN_WANTED
            u1RetVal =
                NpUtilVlanRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_CFA_MOD:
        {
#ifdef CFA_WANTED
            u1RetVal =
                NpUtilCfaRemoteSvcHwProgram (pRemoteHwNpInput,
                                             pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_RSTP_MOD:
        {
#ifdef RSTP_WANTED
            u1RetVal =
                NpUtilRstpRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_MSTP_MOD:
        {
#ifdef MSTP_WANTED
            u1RetVal =
                NpUtilMstpRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_PVRST_MOD:
        {
#ifdef PVRST_WANTED
            u1RetVal =
                NpUtilPvrstRemoteSvcHwProgram (pRemoteHwNpInput,
                                               pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_VCM_MOD:
        {
#ifdef VCM_WANTED
            u1RetVal =
                NpUtilVcmRemoteSvcHwProgram (pRemoteHwNpInput,
                                             pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_PNAC_MOD:
        {
#ifdef PNAC_WANTED
            u1RetVal =
                NpUtilPnacRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IGS_MOD:
        {
#ifdef IGS_WANTED
            u1RetVal =
                NpUtilIgsRemoteSvcHwProgram (pRemoteHwNpInput,
                                             pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_MLDS_MOD:
        {
#ifdef MLDS_WANTED
            u1RetVal =
                NpUtilMldsRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_ISSSYS_MOD:
        {
#ifdef ISS_WANTED
            u1RetVal =
                NpUtilIsssysRemoteSvcHwProgram (pRemoteHwNpInput,
                                                pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_EOAM_MOD:
        {
#ifdef EOAM_WANTED
            u1RetVal =
                NpUtilEoamRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_EOAMFM_MOD:
        {
#ifdef EOAM_FM_WANTED
            u1RetVal =
                NpUtilEoamfmRemoteSvcHwProgram (pRemoteHwNpInput,
                                                pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_LA_MOD:
        {
#ifdef LA_WANTED
            u1RetVal =
                NpUtilLaRemoteSvcHwProgram (pRemoteHwNpInput,
                                            pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_RM_MOD:
        {
#ifdef RM_WANTED
            u1RetVal =
                NpUtilRmRemoteSvcHwProgram (pRemoteHwNpInput,
                                            pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_MBSM_MOD:
        {
#ifdef MBSM_WANTED
            u1RetVal =
                NpUtilMbsmRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_QOSX_MOD:
        {
#ifdef QOSX_WANTED
            u1RetVal =
                NpUtilQosxRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_ELPS_MOD:
        {
#ifdef ELPS_WANTED
            u1RetVal =
                NpUtilElpsRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_ERPS_MOD:
        {
#ifdef ERPS_WANTED
            u1RetVal =
                NpUtilErpsRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_RMON_MOD:
        {
#ifdef RMON_WANTED
            u1RetVal =
                NpUtilRmonRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_RMONv2_MOD:
        {
#ifdef RMON2_WANTED
            u1RetVal =
                NpUtilRmonv2RemoteSvcHwProgram (pRemoteHwNpInput,
                                                pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_DSMON_MOD:
        {
#ifdef DSMON_WANTED
            u1RetVal =
                NpUtilDsmonRemoteSvcHwProgram (pRemoteHwNpInput,
                                               pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_SYNCE_MOD:
        {
#ifdef SYNCE_WANTED
            u1RetVal =
                NpUtilSynceRemoteSvcHwProgram (pRemoteHwNpInput,
                                               pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IP_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            u1RetVal =
                NpUtilIpRemoteSvcHwProgram (pRemoteHwNpInput,
                                            pRemoteHwNpOutput);
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
            break;
        }
        case NP_ECFM_MOD:
        {
#ifdef ECFM_WANTED
            u1RetVal =
                NpUtilEcfmRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IPV6_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            u1RetVal =
                NpUtilIpv6RemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_MPLS_MOD:
        {
#ifdef MPLS_WANTED
            u1RetVal =
                NpUtilMplsRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IGMP_MOD:
        {
#ifdef IGMP_WANTED
            u1RetVal =
                NpUtilIgmpRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IPMC_MOD:
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
            u1RetVal =
                NpUtilIpmcRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_MLD_MOD:
        {
#ifdef MLD_WANTED
            u1RetVal =
                NpUtilMldRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_IP6MC_MOD:
        {
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
            u1RetVal =
                NpUtilIp6mcRemoteSvcHwProgram (pRemoteHwNpInput,
                                              pRemoteHwNpOutput);
#endif
            break;
        }
        case NP_FSB_MOD:
        {
#ifdef FSB_WANTED 
        u1RetVal = NpUtilFsbRemoteSvcHwProgram (pRemoteHwNpInput,
                                                pRemoteHwNpOutput);
#endif
            break;
        }
        default:
            break;
    }
    return u1RetVal;

}

#endif /* __NPUTLREMNP_C__ */
