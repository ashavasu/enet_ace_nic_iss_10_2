/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: cfanputil.c,v 1.18 2018/02/02 09:47:36 siva Exp $
 *
 * Description:This file contains the NP utility functions of CFA
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __CFAUTIL_C__
#define __CFAUTIL_C__

#include "npstackutl.h"
/***************************************************************************/
/*  Function Name       : NpUtilMaskCfaNpPorts                             */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskCfaNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaRemoteNpModInfo *pCfaRemoteNpModInfo = NULL;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;

    pCfaNpModInfo = &pFsHwNp->CfaNpModInfo;
    pCfaRemoteNpModInfo = &pRemoteHwNp->CfaRemoteNpModInfo;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    switch (pFsHwNp->u4Opcode)
    {
        case CFA_NP_GET_LINK_STATUS:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                /* This NPAPI is invoked for L3 Interfaces */
                /* If-Index can be a L3 Interface */
                /* In such a case NP should  be invoked only in Local Unit */
                if (pFsHwNp->u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
                else
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                }
            }
            break;
        }
        case FS_CFA_HW_GET_IF_FLOW_CONTROL:
        case FS_CFA_HW_GET_MTU:
        case FS_CFA_HW_SET_MAC_ADDR:
        case FS_CFA_HW_SET_MTU:
        case FS_CFA_HW_L3_SET_MTU:
        case FS_ETHER_HW_GET_CNTRL:
        case FS_ETHER_HW_GET_COLL_FREQ:
        case FS_ETHER_HW_GET_PAUSE_FRAMES:
        case FS_ETHER_HW_GET_PAUSE_MODE:
        case FS_ETHER_HW_GET_STATS:
        case FS_ETHER_HW_SET_PAUSE_ADMIN_MODE:
        case FS_HW_GET_ETHERNET_TYPE:
        case FS_HW_GET_STAT:
        case FS_HW_GET_STAT64:
        case FS_HW_SET_CUST_IF_PARAMS:
        case FS_HW_UPDATE_ADMIN_STATUS_CHANGE:
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG:
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS:
        case FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS:
        case FS_MAU_HW_GET_AUTO_NEG_CAP_BITS:
        case FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS:
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT:
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD:
        case FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING:
        case FS_MAU_HW_GET_AUTO_NEG_RESTART:
        case FS_MAU_HW_GET_AUTO_NEG_SUPPORTED:
        case FS_MAU_HW_GET_FALSE_CARRIERS:
        case FS_MAU_HW_GET_JABBER_STATE:
        case FS_MAU_HW_GET_JABBERING_STATE_ENTERS:
        case FS_MAU_HW_GET_JACK_TYPE:
        case FS_MAU_HW_GET_MAU_TYPE:
        case FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS:
        case FS_MAU_HW_GET_MEDIA_AVAILABLE:
        case FS_MAU_HW_GET_TYPE_LIST_BITS:
        case FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS:
        case FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS:
        case FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT:
        case FS_MAU_HW_SET_AUTO_NEG_RESTART:
        case FS_MAU_HW_SET_MAU_STATUS:
        case FS_MAU_HW_GET_STATUS:
        case FS_MAU_HW_SET_DEFAULT_TYPE:
        case FS_MAU_HW_GET_DEFAULT_TYPE:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_CFA_HW_CLEAR_STATS:
        {
            tCfaNpWrFsCfaHwClearStats *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwClearStats;

            if (NpUtilIsPortChannel (pEntry->u4Port) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pEntry->u4Port, &HwPortInfo) ==
                     FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaNpWrFsHwGetVlanIntfStats *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaRemoteNpFsHwGetVlanIntfStats;

            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case CFA_REGISTER_WITH_NP_DRV:
        case FS_NP_CFA_SET_DLF_STATUS:
        case CFA_NP_SET_STACKING_MODEL:
        case FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH:
#ifdef IP6_WANTED
        case FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH:
#endif
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_CFA_HW_SET_WAN_TYE:
        {
            tCfaNpWrFsCfaHwSetWanTye *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetWanTye;

            if (NpUtilIsPortChannel (pEntry->pCfaWanInfo->u4IfIndex) ==
                FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if ((NpUtilIsRemotePort (pEntry->pCfaWanInfo->u4IfIndex,
                                          &HwPortInfo)) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_CFA_HW_MAC_LOOKUP:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case CFA_NP_SET_HW_PORT_INFO:
        {
            break;
        }
        case CFA_NP_GET_HW_PORT_INFO:
        {
            break;
        }
        case FS_CFA_HW_DELETE_PKT_FILTER:
        {
            break;
        }
        case FS_CFA_HW_CREATE_PKT_FILTER:
        {
            break;
        }
        case CFA_NP_GET_HW_INFO:
        {
            tCfaNpWrCfaNpGetHwInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpGetHwInfo;

            if ((NpUtilIsRemotePort (pEntry->pHwIdInfo->u4IfIndex,
                                     &HwPortInfo)) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case CFA_NP_REMOTE_SET_HW_INFO:
        {
            tCfaNpWrCfaNpRemoteSetHwInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpRemoteSetHwInfo;

            /* In this case, since we need to store Remote port's
             * inforamtion locally, if the u4IfIndex is remote port,
             * pu1NpCallStatus should be invoked as NPUTIL_INVOKE_LOCAL_NP
             * and vice-versa. */
            if ((NpUtilIsRemotePort (pEntry->pHwIdInfo->u4IfIndex,
                                     &HwPortInfo)) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }

#ifdef MBSM_WANTED
        case CFA_MBSM_REGISTER_WITH_NP_DRV:
        {
            pRemoteHwNp->u4Opcode = CFA_REGISTER_WITH_NP_DRV;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_CFA_MBSM_SET_MAC_ADDR:
        {
            pRemoteHwNp->u4Opcode = FS_CFA_HW_SET_MAC_ADDR;
            tCfaNpWrFsCfaMbsmSetMacAddr *pEntry = NULL;
            tCfaRemoteNpWrFsCfaHwSetMacAddr *pRemEntry = NULL;

            pEntry = &pCfaNpModInfo->CfaNpFsCfaMbsmSetMacAddr;
            pRemEntry = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMacAddr;

            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            if ((pEntry->PortMac) != NULL)
            {
                MEMCPY (&(pRemEntry->PortMac), (pEntry->PortMac),
                        sizeof (tMacAddr));
            }

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_HW_MBSM_SET_CUST_IF_PARAMS:
        {
            pRemoteHwNp->u4Opcode = FS_HW_SET_CUST_IF_PARAMS;
            tCfaNpWrFsHwMbsmSetCustIfParams *pEntry = NULL;
            tCfaRemoteNpWrFsHwSetCustIfParams *pRemEntry = NULL;

            pEntry = &pCfaNpModInfo->CfaNpFsHwMbsmSetCustIfParams;
            pRemEntry = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwSetCustIfParams;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;

            MEMCPY (&(pRemEntry->HwCustParamType), &(pEntry->HwCustParamType),
                    sizeof (tHwCustIfParamType));
            pRemEntry->CustIfParamVal.TLV.u4Type =
                pEntry->CustIfParamVal.TLV.u4Type;
            pRemEntry->CustIfParamVal.TLV.u4Length =
                pEntry->CustIfParamVal.TLV.u4Length;
            if ((pEntry->CustIfParamVal.TLV.pu1Value) != NULL)
            {
                MEMCPY (&(pRemEntry->CustIfParamVal.TLV.u1Value),
                        (pEntry->CustIfParamVal.TLV.pu1Value), sizeof (UINT1));
            }
            MEMCPY (&(pRemEntry->CustIfParamVal.OpaqueAttrbs),
                    &(pEntry->CustIfParamVal.OpaqueAttrbs),
                    sizeof (tHwCustIfOpqAttrs));
            MEMCPY (&(pRemEntry->EntryAction), &(pEntry->EntryAction),
                    sizeof (tNpEntryAction));

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case CFA_MBSM_NP_SLOT_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
        case CFA_MBSM_NP_SLOT_DE_INIT:
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            break;
        }
        case CFA_MBSM_UPDATE_PORT_MAC_ADDRESS:
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            break;
        }
        case FS_NP_CFA_MBSM_ARP_MODIFY_FILTER:
        case FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }

#else
            UNUSED_PARAM (pCfaRemoteNpModInfo);
#endif /* MBSM_WANTED */
        default:
            break;
    }
    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * CFA module will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL */
    if (u4StackingModel == ISS_DISS_STACKING_MODEL)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }
    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilCfaMergeLocalRemoteNPOutput                 */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                        and Remote NP call execution                    */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                        pRemoteHwNp Param of type tRemoteHwNp            */
/*                        pi4LocalNpRetVal - Lcoal Np Return Value         */
/*                        pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilCfaMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                   tRemoteHwNp * pRemoteHwNp,
                                   INT4 *pi4LocalNpRetVal,
                                   INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaRemoteNpModInfo *pCfaRemoteNpModInfo = NULL;

    pCfaNpModInfo = &(pFsHwNp->CfaNpModInfo);
    pCfaRemoteNpModInfo = &(pRemoteHwNp->CfaRemoteNpModInfo);

    if ((pCfaNpModInfo == NULL) || (pCfaRemoteNpModInfo == NULL))
    {
        return FNP_FAILURE;
    }

    switch (pFsHwNp->u4Opcode)
    {
        case FS_HW_GET_STAT:
        {
            tCfaNpWrFsHwGetStat *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat;
            /* merging is required only for port channel case */
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                *(pLocalArgs->pu4Value) = *(pLocalArgs->pu4Value) +
                    pRemoteArgs->u4Value;
                *pi4LocalNpRetVal = FNP_SUCCESS;
                *pi4RemoteNpRetVal = FNP_SUCCESS;
            }
            break;
        }
        case FS_HW_GET_STAT64:
        {
            tCfaNpWrFsHwGetStat64 *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat64 *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat64;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat64;
            /* merging is required only for port channel case */
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                (pLocalArgs->pValue->msn) = (pLocalArgs->pValue->msn) +
                    (pRemoteArgs->Value.msn);
                (pLocalArgs->pValue->lsn) = (pLocalArgs->pValue->lsn) +
                    (pRemoteArgs->Value.lsn);
                *pi4LocalNpRetVal = FNP_SUCCESS;
                *pi4RemoteNpRetVal = FNP_SUCCESS;
            }
            break;
        }
        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaNpWrFsHwGetVlanIntfStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetVlanIntfStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetVlanIntfStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetVlanIntfStats;
            if ((pRemoteArgs->i1StatType == NP_STAT_IF_IN_OCTETS) ||
                (pRemoteArgs->i1StatType == NP_STAT_IF_IN_PKTS) ||
                (pRemoteArgs->i1StatType == NP_STAT_IF_IN_DISCARDS))
            {
                *(pLocalArgs->pu4Value) = *(pLocalArgs->pu4Value) +
                    pRemoteArgs->u4Value;
                *pi4LocalNpRetVal = FNP_SUCCESS;
                *pi4RemoteNpRetVal = FNP_SUCCESS;
            }
            break;
        }
        default:
            break;
    }
    return u1RetVal;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilCfaConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilCfaConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaRemoteNpModInfo *pCfaRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pCfaNpModInfo = &(pFsHwNp->CfaNpModInfo);
    pCfaRemoteNpModInfo = &(pRemoteHwNp->CfaRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tCfaRemoteNpModInfo);

    switch (u4Opcode)
    {
        case CFA_NP_GET_LINK_STATUS:
        {
            tCfaNpWrCfaNpGetLinkStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetLinkStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetLinkStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetLinkStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1PortLinkStatus = pLocalArgs->u1PortLinkStatus;
            break;
        }
        case CFA_REGISTER_WITH_NP_DRV:
        {
            break;
        }
        case FS_CFA_HW_CLEAR_STATS:
        {
            tCfaNpWrFsCfaHwClearStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwClearStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwClearStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwClearStats;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            break;
        }
        case FS_CFA_HW_GET_IF_FLOW_CONTROL:
        {
            tCfaNpWrFsCfaHwGetIfFlowControl *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwGetIfFlowControl *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwGetIfFlowControl;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetIfFlowControl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu4PortFlowControl != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4PortFlowControl),
                        (pLocalArgs->pu4PortFlowControl), sizeof (UINT4));
            }
            break;
        }
        case FS_CFA_HW_GET_MTU:
        {
            tCfaNpWrFsCfaHwGetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwGetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwGetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetMtu;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu4MtuSize != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4MtuSize), (pLocalArgs->pu4MtuSize),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_CFA_HW_SET_MAC_ADDR:
        {
            tCfaNpWrFsCfaHwSetMacAddr *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetMacAddr *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetMacAddr;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMacAddr;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->PortMac != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortMac), (pLocalArgs->PortMac),
                        sizeof (tMacAddr));
            }
            break;
        }
        case FS_CFA_HW_SET_MTU:
        {
            tCfaNpWrFsCfaHwSetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMtu;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4MtuSize = pLocalArgs->u4MtuSize;
            break;
        }
        case FS_CFA_HW_L3_SET_MTU:

        {
            tCfaNpWrFsCfaHwL3SetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwL3SetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwL3SetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwL3SetMtu;
            pRemoteArgs->L3Mtu.u4VrfId = pLocalArgs->L3Mtu.u4VrfId;
            pRemoteArgs->L3Mtu.u4Mtu = pLocalArgs->L3Mtu.u4Mtu;
            pRemoteArgs->L3Mtu.u4IfIndex = pLocalArgs->L3Mtu.u4IfIndex;
            break;
        }

        case FS_CFA_HW_SET_WAN_TYE:
        {
            tCfaNpWrFsCfaHwSetWanTye *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetWanTye *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetWanTye;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetWanTye;
            if (pLocalArgs->pCfaWanInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->CfaWanInfo), (pLocalArgs->pCfaWanInfo),
                        sizeof (tIfWanInfo));
            }
            break;
        }
        case FS_ETHER_HW_GET_CNTRL:
        {
            tCfaNpWrFsEtherHwGetCntrl *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetCntrl *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetCntrl;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCntrl;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            pRemoteArgs->i4Code = pLocalArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_COLL_FREQ:
        {
            tCfaNpWrFsEtherHwGetCollFreq *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetCollFreq *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetCollFreq;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCollFreq;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4dot3CollCount = pLocalArgs->i4dot3CollCount;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_FRAMES:
        {
            tCfaNpWrFsEtherHwGetPauseFrames *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetPauseFrames *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseFrames;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseFrames;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (UINT4));
            }
            pRemoteArgs->i4Code = pLocalArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_MODE:
        {
            tCfaNpWrFsEtherHwGetPauseMode *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetPauseMode *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseMode;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseMode;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            pRemoteArgs->i4Code = pLocalArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_STATS:
        {
            tCfaNpWrFsEtherHwGetStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetStats;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pEthStats != NULL)
            {
                MEMCPY (&(pRemoteArgs->EthStats), (pLocalArgs->pEthStats),
                        sizeof (tEthStats));
            }
            break;
        }
        case FS_ETHER_HW_SET_PAUSE_ADMIN_MODE:
        {
            tCfaNpWrFsEtherHwSetPauseAdminMode *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwSetPauseAdminMode *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwSetPauseAdminMode;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwSetPauseAdminMode;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_HW_GET_ETHERNET_TYPE:
        {
            tCfaNpWrFsHwGetEthernetType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetEthernetType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetEthernetType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetEthernetType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu1EtherType != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1EtherType), (pLocalArgs->pu1EtherType),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_HW_GET_STAT:
        {
            tCfaNpWrFsHwGetStat *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i1StatType = pLocalArgs->i1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Value), (pLocalArgs->pu4Value),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_HW_GET_STAT64:
        {
            tCfaNpWrFsHwGetStat64 *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat64 *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat64;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat64;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i1StatType = pLocalArgs->i1StatType;
            if (pLocalArgs->pValue != NULL)
            {
                MEMCPY (&(pRemoteArgs->Value), (pLocalArgs->pValue),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_HW_SET_CUST_IF_PARAMS:
        {
            tCfaNpWrFsHwSetCustIfParams *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwSetCustIfParams *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwSetCustIfParams;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwSetCustIfParams;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->HwCustParamType),
                    &(pLocalArgs->HwCustParamType),
                    sizeof (tHwCustIfParamType));
            pRemoteArgs->CustIfParamVal.TLV.u4Type =
                pLocalArgs->CustIfParamVal.TLV.u4Type;
            pRemoteArgs->CustIfParamVal.TLV.u4Length =
                pLocalArgs->CustIfParamVal.TLV.u4Length;
            if (pLocalArgs->CustIfParamVal.TLV.pu1Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->CustIfParamVal.TLV.u1Value),
                        (pLocalArgs->CustIfParamVal.TLV.pu1Value),
                        sizeof (UINT1));
            }
            MEMCPY (&(pRemoteArgs->CustIfParamVal.OpaqueAttrbs),
                    &(pLocalArgs->CustIfParamVal.OpaqueAttrbs),
                    sizeof (tHwCustIfOpqAttrs));
            MEMCPY (&(pRemoteArgs->EntryAction), &(pLocalArgs->EntryAction),
                    sizeof (tNpEntryAction));
            break;
        }
        case FS_HW_UPDATE_ADMIN_STATUS_CHANGE:
        {
            tCfaNpWrFsHwUpdateAdminStatusChange *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwUpdateAdminStatusChange *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwUpdateAdminStatusChange;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsHwUpdateAdminStatusChange;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1AdminEnable = pLocalArgs->u1AdminEnable;
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminConfig *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminConfig *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminConfig;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminConfig;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminStatus;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapAdvtBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapAdvtBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapAdvtBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapAdvtBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapRcvdBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapRcvdBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapRcvdBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapRcvdBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltAdvt *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltAdvt *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltAdvt;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltAdvt;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltRcvd *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltRcvd *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltRcvd;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltRcvd;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING:
        {
            tCfaNpWrFsMauHwGetAutoNegRemoteSignaling *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemoteSignaling *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemoteSignaling;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsMauHwGetAutoNegRemoteSignaling;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwGetAutoNegRestart *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRestart *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRestart;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRestart;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_SUPPORTED:
        {
            tCfaNpWrFsMauHwGetAutoNegSupported *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegSupported *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegSupported;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegSupported;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_FALSE_CARRIERS:
        {
            tCfaNpWrFsMauHwGetFalseCarriers *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetFalseCarriers *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetFalseCarriers;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetFalseCarriers;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JABBER_STATE:
        {
            tCfaNpWrFsMauHwGetJabberState *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJabberState *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJabberState;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberState;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JABBERING_STATE_ENTERS:
        {
            tCfaNpWrFsMauHwGetJabberingStateEnters *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJabberingStateEnters *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJabberingStateEnters;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberingStateEnters;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JACK_TYPE:
        {
            tCfaNpWrFsMauHwGetJackType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJackType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJackType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJackType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            pRemoteArgs->i4ifJackIndex = pLocalArgs->i4ifJackIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MAU_TYPE:
        {
            tCfaNpWrFsMauHwGetMauType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMauType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMauType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMauType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pu4Val != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Val), (pLocalArgs->pu4Val),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS:
        {
            tCfaNpWrFsMauHwGetMediaAvailStateExits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMediaAvailStateExits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailStateExits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailStateExits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAILABLE:
        {
            tCfaNpWrFsMauHwGetMediaAvailable *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMediaAvailable *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailable;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailable;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_TYPE_LIST_BITS:
        {
            tCfaNpWrFsMauHwGetTypeListBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetTypeListBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetTypeListBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetTypeListBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwSetAutoNegAdminStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegAdminStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegAdminStatus;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegAdminStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwSetAutoNegCapAdvtBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegCapAdvtBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegCapAdvtBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegCapAdvtBits;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwSetAutoNegRemFltAdvt *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegRemFltAdvt *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRemFltAdvt;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRemFltAdvt;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwSetAutoNegRestart *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegRestart *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRestart;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRestart;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_MAU_STATUS:
        {
            tCfaNpWrFsMauHwSetMauStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetMauStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetMauStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetMauStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_STATUS:
        {
            tCfaNpWrFsMauHwGetStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY (&(pRemoteArgs->element), (pLocalArgs->pElement),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwSetDefaultType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetDefaultType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetDefaultType;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetDefaultType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            pRemoteArgs->u4Val = pLocalArgs->u4Val;
            break;
        }
        case FS_MAU_HW_GET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwGetDefaultType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetDefaultType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetDefaultType;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetDefaultType;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i4IfMauId = pLocalArgs->i4IfMauId;
            if (pLocalArgs->pu4Val != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Val), (pLocalArgs->pu4Val),
                        sizeof (UINT4));
            }
            break;
        }
        case NP_CFA_FRONT_PANEL_PORTS:
        {
            tCfaNpWrNpCfaFrontPanelPorts *pLocalArgs = NULL;
            tCfaRemoteNpWrNpCfaFrontPanelPorts *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpNpCfaFrontPanelPorts;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpNpCfaFrontPanelPorts;
            pRemoteArgs->u4MaxFrontPanelPorts =
                pLocalArgs->u4MaxFrontPanelPorts;
            break;
        }
        case FS_CFA_HW_MAC_LOOKUP:
        {
            tCfaNpWrFsCfaHwMacLookup *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwMacLookup *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwMacLookup;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwMacLookup;
            if (pLocalArgs->pu1MacAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1MacAddr), (pLocalArgs->pu1MacAddr),
                        sizeof (UINT1) * MAC_ADDR_LEN);
            }
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanIfaceVlanId));
            if (pLocalArgs->pu2Port != NULL)
            {
                MEMCPY (&(pRemoteArgs->u2Port), (pLocalArgs->pu2Port),
                        sizeof (UINT2));
            }
            break;
        }
        case CFA_NP_SET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpSetHwPortInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpSetHwPortInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpSetHwPortInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetHwPortInfo;
            MEMCPY (&(pRemoteArgs->HwPortInfo), &(pLocalArgs->HwPortInfo),
                    sizeof (tHwPortInfo));
            break;
        }
        case CFA_NP_GET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpGetHwPortInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetHwPortInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetHwPortInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwPortInfo;
            if (pLocalArgs->pHwPortInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwPortInfo), (pLocalArgs->pHwPortInfo),
                        sizeof (tHwPortInfo));
            }
            break;
        }
        case FS_CFA_HW_DELETE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwDeletePktFilter *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwDeletePktFilter *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwDeletePktFilter;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwDeletePktFilter;
            pRemoteArgs->u4FilterId = pLocalArgs->u4FilterId;
            break;
        }
        case FS_CFA_HW_CREATE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwCreatePktFilter *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwCreatePktFilter *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwCreatePktFilter;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwCreatePktFilter;
            if (pLocalArgs->pFilterInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->FilterInfo), (pLocalArgs->pFilterInfo),
                        sizeof (tHwCfaFilterInfo));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4FilterId), (pLocalArgs->pu4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_CFA_SET_DLF_STATUS:
        {
            tCfaNpWrFsNpCfaSetDlfStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsNpCfaSetDlfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsNpCfaSetDlfStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsNpCfaSetDlfStatus;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case CFA_NP_SET_STACKING_MODEL:
        {
            tCfaNpWrCfaNpSetStackingModel *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpSetStackingModel *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpSetStackingModel;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetStackingModel;
            pRemoteArgs->u4StackingModel = pLocalArgs->u4StackingModel;
            break;
        }
        case CFA_NP_GET_HW_INFO:
        {
            tCfaNpWrCfaNpGetHwInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetHwInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetHwInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwInfo;
            if (pLocalArgs->pHwIdInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwIdInfo), (pLocalArgs->pHwIdInfo),
                        sizeof (tHwIdInfo));
            }
            break;
        }
        case CFA_NP_REMOTE_SET_HW_INFO:
        {
            tCfaNpWrCfaNpRemoteSetHwInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpRemoteSetHwInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpRemoteSetHwInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpRemoteSetHwInfo;
            MEMCPY (&(pRemoteArgs->HwIdInfo), pLocalArgs->pHwIdInfo,
                    sizeof (tHwIdInfo));
            break;
        }
        case FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            pRemoteArgs->u4IpNet = pLocalArgs->u4IpNet;
            pRemoteArgs->u4IpMask = pLocalArgs->u4IpMask;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
#ifdef IP6_WANTED
        case FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            pRemoteArgs->Ip6Addr = pLocalArgs->Ip6Addr;
            pRemoteArgs->u4Prefix = pLocalArgs->u4Prefix;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
#endif

#ifdef MBSM_WANTED
        case CFA_MBSM_NP_SLOT_DE_INIT:
        case CFA_MBSM_NP_SLOT_INIT:
        case CFA_MBSM_REGISTER_WITH_NP_DRV:
        case CFA_MBSM_UPDATE_PORT_MAC_ADDRESS:
        case FS_CFA_MBSM_SET_MAC_ADDR:
        case FS_HW_MBSM_SET_CUST_IF_PARAMS:
        case FS_NP_CFA_MBSM_ARP_MODIFY_FILTER:
        case FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER:

        {
            break;
        }
#endif /* MBSM_WANTED */
        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaNpWrFsHwGetVlanIntfStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetVlanIntfStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetVlanIntfStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetVlanIntfStats;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->i1StatType = pLocalArgs->i1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Value), (pLocalArgs->pu4Value),
                        sizeof (UINT4));
            }
            break;
        }

        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilCfaConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilCfaConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaRemoteNpModInfo *pCfaRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pCfaNpModInfo = &(pFsHwNp->CfaNpModInfo);
    pCfaRemoteNpModInfo = &(pRemoteHwNp->CfaRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case CFA_NP_GET_LINK_STATUS:
        {
            tCfaNpWrCfaNpGetLinkStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetLinkStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetLinkStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetLinkStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1PortLinkStatus = pRemoteArgs->u1PortLinkStatus;
            break;
        }
        case CFA_REGISTER_WITH_NP_DRV:
        {
            break;
        }
        case FS_CFA_HW_CLEAR_STATS:
        {
            tCfaNpWrFsCfaHwClearStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwClearStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwClearStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwClearStats;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            break;
        }
        case FS_CFA_HW_GET_IF_FLOW_CONTROL:
        {
            tCfaNpWrFsCfaHwGetIfFlowControl *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwGetIfFlowControl *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwGetIfFlowControl;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetIfFlowControl;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu4PortFlowControl != NULL)
            {
                MEMCPY ((pLocalArgs->pu4PortFlowControl),
                        &(pRemoteArgs->u4PortFlowControl), sizeof (UINT4));
            }
            break;
        }
        case FS_CFA_HW_GET_MTU:
        {
            tCfaNpWrFsCfaHwGetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwGetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwGetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetMtu;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu4MtuSize != NULL)
            {
                MEMCPY ((pLocalArgs->pu4MtuSize), &(pRemoteArgs->u4MtuSize),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_CFA_HW_SET_MAC_ADDR:
        {
            tCfaNpWrFsCfaHwSetMacAddr *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetMacAddr *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetMacAddr;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMacAddr;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->PortMac != NULL)
            {
                MEMCPY ((pLocalArgs->PortMac), &(pRemoteArgs->PortMac),
                        sizeof (tMacAddr));
            }
            break;
        }
        case FS_CFA_HW_SET_MTU:
        {
            tCfaNpWrFsCfaHwSetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMtu;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u4MtuSize = pRemoteArgs->u4MtuSize;
            break;
        }

        case FS_CFA_HW_L3_SET_MTU:
        {
            tCfaNpWrFsCfaHwL3SetMtu *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwL3SetMtu *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwL3SetMtu;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwL3SetMtu;
            pLocalArgs->L3Mtu.u4IfIndex = pRemoteArgs->L3Mtu.u4IfIndex;
            pLocalArgs->L3Mtu.u4Mtu = pRemoteArgs->L3Mtu.u4Mtu;
            pLocalArgs->L3Mtu.u4VrfId = pRemoteArgs->L3Mtu.u4VrfId;
            break;
        }
        case FS_CFA_HW_SET_WAN_TYE:
        {
            tCfaNpWrFsCfaHwSetWanTye *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwSetWanTye *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwSetWanTye;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetWanTye;
            if (pLocalArgs->pCfaWanInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pCfaWanInfo), &(pRemoteArgs->CfaWanInfo),
                        sizeof (tIfWanInfo));
            }
            break;
        }
        case FS_ETHER_HW_GET_CNTRL:
        {
            tCfaNpWrFsEtherHwGetCntrl *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetCntrl *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetCntrl;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCntrl;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            pLocalArgs->i4Code = pRemoteArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_COLL_FREQ:
        {
            tCfaNpWrFsEtherHwGetCollFreq *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetCollFreq *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetCollFreq;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCollFreq;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4dot3CollCount = pRemoteArgs->i4dot3CollCount;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_FRAMES:
        {
            tCfaNpWrFsEtherHwGetPauseFrames *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetPauseFrames *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseFrames;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseFrames;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (UINT4));
            }
            pLocalArgs->i4Code = pRemoteArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_MODE:
        {
            tCfaNpWrFsEtherHwGetPauseMode *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetPauseMode *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseMode;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseMode;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            pLocalArgs->i4Code = pRemoteArgs->i4Code;
            break;
        }
        case FS_ETHER_HW_GET_STATS:
        {
            tCfaNpWrFsEtherHwGetStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwGetStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwGetStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetStats;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pEthStats != NULL)
            {
                MEMCPY ((pLocalArgs->pEthStats), &(pRemoteArgs->EthStats),
                        sizeof (tEthStats));
            }
            break;
        }
        case FS_ETHER_HW_SET_PAUSE_ADMIN_MODE:
        {
            tCfaNpWrFsEtherHwSetPauseAdminMode *pLocalArgs = NULL;
            tCfaRemoteNpWrFsEtherHwSetPauseAdminMode *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsEtherHwSetPauseAdminMode;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwSetPauseAdminMode;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_HW_GET_ETHERNET_TYPE:
        {
            tCfaNpWrFsHwGetEthernetType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetEthernetType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetEthernetType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetEthernetType;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu1EtherType != NULL)
            {
                MEMCPY ((pLocalArgs->pu1EtherType), &(pRemoteArgs->u1EtherType),
                        sizeof (UINT1));
            }
            break;
        }
        case FS_HW_GET_STAT:
        {
            tCfaNpWrFsHwGetStat *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i1StatType = pRemoteArgs->i1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY ((pLocalArgs->pu4Value), &(pRemoteArgs->u4Value),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_HW_GET_STAT64:
        {
            tCfaNpWrFsHwGetStat64 *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetStat64 *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetStat64;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat64;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i1StatType = pRemoteArgs->i1StatType;
            if (pLocalArgs->pValue != NULL)
            {
                MEMCPY ((pLocalArgs->pValue), &(pRemoteArgs->Value),
                        sizeof (tSNMP_COUNTER64_TYPE));
            }
            break;
        }
        case FS_HW_SET_CUST_IF_PARAMS:
        {
            tCfaNpWrFsHwSetCustIfParams *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwSetCustIfParams *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwSetCustIfParams;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwSetCustIfParams;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->HwCustParamType),
                    &(pRemoteArgs->HwCustParamType),
                    sizeof (tHwCustIfParamType));
            pLocalArgs->CustIfParamVal.TLV.u4Type =
                pRemoteArgs->CustIfParamVal.TLV.u4Type;
            pLocalArgs->CustIfParamVal.TLV.u4Length =
                pRemoteArgs->CustIfParamVal.TLV.u4Length;
            if (pLocalArgs->CustIfParamVal.TLV.pu1Value != NULL)
            {
                MEMCPY ((pLocalArgs->CustIfParamVal.TLV.pu1Value),
                        &(pRemoteArgs->CustIfParamVal.TLV.u1Value),
                        sizeof (UINT1));
            }
            MEMCPY (&(pLocalArgs->CustIfParamVal.OpaqueAttrbs),
                    &(pRemoteArgs->CustIfParamVal.OpaqueAttrbs),
                    sizeof (tHwCustIfOpqAttrs));
            MEMCPY (&(pLocalArgs->EntryAction), &(pRemoteArgs->EntryAction),
                    sizeof (tNpEntryAction));
            break;
        }
        case FS_HW_UPDATE_ADMIN_STATUS_CHANGE:
        {
            tCfaNpWrFsHwUpdateAdminStatusChange *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwUpdateAdminStatusChange *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwUpdateAdminStatusChange;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsHwUpdateAdminStatusChange;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1AdminEnable = pRemoteArgs->u1AdminEnable;
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminConfig *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminConfig *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminConfig;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminConfig;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminStatus;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapAdvtBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapAdvtBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapAdvtBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapAdvtBits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapBits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapRcvdBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegCapRcvdBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapRcvdBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapRcvdBits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltAdvt *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltAdvt *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltAdvt;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltAdvt;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltRcvd *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltRcvd *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltRcvd;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltRcvd;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING:
        {
            tCfaNpWrFsMauHwGetAutoNegRemoteSignaling *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRemoteSignaling *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemoteSignaling;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsMauHwGetAutoNegRemoteSignaling;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwGetAutoNegRestart *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegRestart *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRestart;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRestart;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_SUPPORTED:
        {
            tCfaNpWrFsMauHwGetAutoNegSupported *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetAutoNegSupported *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegSupported;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegSupported;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_FALSE_CARRIERS:
        {
            tCfaNpWrFsMauHwGetFalseCarriers *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetFalseCarriers *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetFalseCarriers;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetFalseCarriers;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JABBER_STATE:
        {
            tCfaNpWrFsMauHwGetJabberState *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJabberState *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJabberState;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberState;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JABBERING_STATE_ENTERS:
        {
            tCfaNpWrFsMauHwGetJabberingStateEnters *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJabberingStateEnters *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJabberingStateEnters;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberingStateEnters;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_JACK_TYPE:
        {
            tCfaNpWrFsMauHwGetJackType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetJackType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetJackType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJackType;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            pLocalArgs->i4ifJackIndex = pRemoteArgs->i4ifJackIndex;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MAU_TYPE:
        {
            tCfaNpWrFsMauHwGetMauType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMauType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMauType;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMauType;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pu4Val != NULL)
            {
                MEMCPY ((pLocalArgs->pu4Val), &(pRemoteArgs->u4Val),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS:
        {
            tCfaNpWrFsMauHwGetMediaAvailStateExits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMediaAvailStateExits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailStateExits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailStateExits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAILABLE:
        {
            tCfaNpWrFsMauHwGetMediaAvailable *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetMediaAvailable *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailable;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailable;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_TYPE_LIST_BITS:
        {
            tCfaNpWrFsMauHwGetTypeListBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetTypeListBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetTypeListBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetTypeListBits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwSetAutoNegAdminStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegAdminStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegAdminStatus;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegAdminStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwSetAutoNegCapAdvtBits *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegCapAdvtBits *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegCapAdvtBits;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegCapAdvtBits;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwSetAutoNegRemFltAdvt *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegRemFltAdvt *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRemFltAdvt;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRemFltAdvt;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwSetAutoNegRestart *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetAutoNegRestart *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRestart;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRestart;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_MAU_STATUS:
        {
            tCfaNpWrFsMauHwSetMauStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetMauStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetMauStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetMauStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_GET_STATUS:
        {
            tCfaNpWrFsMauHwGetStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pElement != NULL)
            {
                MEMCPY ((pLocalArgs->pElement), &(pRemoteArgs->element),
                        sizeof (INT4));
            }
            break;
        }
        case FS_MAU_HW_SET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwSetDefaultType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwSetDefaultType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwSetDefaultType;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetDefaultType;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            pLocalArgs->u4Val = pRemoteArgs->u4Val;
            break;
        }
        case FS_MAU_HW_GET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwGetDefaultType *pLocalArgs = NULL;
            tCfaRemoteNpWrFsMauHwGetDefaultType *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsMauHwGetDefaultType;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetDefaultType;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i4IfMauId = pRemoteArgs->i4IfMauId;
            if (pLocalArgs->pu4Val != NULL)
            {
                MEMCPY ((pLocalArgs->pu4Val), &(pRemoteArgs->u4Val),
                        sizeof (UINT4));
            }
            break;
        }
        case NP_CFA_FRONT_PANEL_PORTS:
        {
            tCfaNpWrNpCfaFrontPanelPorts *pLocalArgs = NULL;
            tCfaRemoteNpWrNpCfaFrontPanelPorts *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpNpCfaFrontPanelPorts;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpNpCfaFrontPanelPorts;
            pLocalArgs->u4MaxFrontPanelPorts =
                pRemoteArgs->u4MaxFrontPanelPorts;
            break;
        }
        case FS_CFA_HW_MAC_LOOKUP:
        {
            tCfaNpWrFsCfaHwMacLookup *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwMacLookup *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwMacLookup;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwMacLookup;
            if (pLocalArgs->pu1MacAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1MacAddr), &(pRemoteArgs->u1MacAddr),
                        sizeof (UINT1) * MAC_ADDR_LEN);
            }
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanIfaceVlanId));
            if (pLocalArgs->pu2Port != NULL)
            {
                MEMCPY ((pLocalArgs->pu2Port), &(pRemoteArgs->u2Port),
                        sizeof (UINT2));
            }
            break;
        }
        case CFA_NP_SET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpSetHwPortInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpSetHwPortInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpSetHwPortInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetHwPortInfo;
            MEMCPY (&(pLocalArgs->HwPortInfo), &(pRemoteArgs->HwPortInfo),
                    sizeof (tHwPortInfo));
            break;
        }
        case CFA_NP_GET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpGetHwPortInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetHwPortInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetHwPortInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwPortInfo;
            if (pLocalArgs->pHwPortInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pHwPortInfo), &(pRemoteArgs->HwPortInfo),
                        sizeof (tHwPortInfo));
            }
            break;
        }
        case FS_CFA_HW_DELETE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwDeletePktFilter *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwDeletePktFilter *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwDeletePktFilter;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwDeletePktFilter;
            pLocalArgs->u4FilterId = pRemoteArgs->u4FilterId;
            break;
        }
        case FS_CFA_HW_CREATE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwCreatePktFilter *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwCreatePktFilter *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwCreatePktFilter;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwCreatePktFilter;
            if (pLocalArgs->pFilterInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pFilterInfo), &(pRemoteArgs->FilterInfo),
                        sizeof (tHwCfaFilterInfo));
            }
            if (pLocalArgs->pu4FilterId != NULL)
            {
                MEMCPY ((pLocalArgs->pu4FilterId), &(pRemoteArgs->u4FilterId),
                        sizeof (UINT4));
            }
            break;
        }
        case FS_NP_CFA_SET_DLF_STATUS:
        {
            tCfaNpWrFsNpCfaSetDlfStatus *pLocalArgs = NULL;
            tCfaRemoteNpWrFsNpCfaSetDlfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsNpCfaSetDlfStatus;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsNpCfaSetDlfStatus;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case CFA_NP_SET_STACKING_MODEL:
        {
            tCfaNpWrCfaNpSetStackingModel *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpSetStackingModel *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpSetStackingModel;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetStackingModel;
            pLocalArgs->u4StackingModel = pRemoteArgs->u4StackingModel;
            break;
        }
        case CFA_NP_GET_HW_INFO:
        {
            tCfaNpWrCfaNpGetHwInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpGetHwInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpGetHwInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwInfo;
            if (pLocalArgs->pHwIdInfo != NULL)
            {
                MEMCPY (pLocalArgs->pHwIdInfo, &(pRemoteArgs->HwIdInfo),
                        sizeof (tHwIdInfo));
            }
            break;
        }
        case CFA_NP_REMOTE_SET_HW_INFO:
        {
            tCfaNpWrCfaNpRemoteSetHwInfo *pLocalArgs = NULL;
            tCfaRemoteNpWrCfaNpRemoteSetHwInfo *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpCfaNpRemoteSetHwInfo;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpRemoteSetHwInfo;
            MEMCPY (pLocalArgs->pHwIdInfo, &(pRemoteArgs->HwIdInfo),
                    sizeof (tHwIdInfo));
            break;
        }
        case FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            pLocalArgs->u4IpNet = pRemoteArgs->u4IpNet;
            pLocalArgs->u4IpMask = pRemoteArgs->u4IpMask;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
#ifdef IP6_WANTED
        case FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pLocalArgs = NULL;
            tCfaRemoteNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            pRemoteArgs =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            pLocalArgs->Ip6Addr = pRemoteArgs->Ip6Addr;
            pLocalArgs->u4Prefix = pRemoteArgs->u4Prefix;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            break;
        }
#endif

#ifdef MBSM_WANTED
        case CFA_MBSM_NP_SLOT_DE_INIT:
        case CFA_MBSM_NP_SLOT_INIT:
        case CFA_MBSM_REGISTER_WITH_NP_DRV:
        case CFA_MBSM_UPDATE_PORT_MAC_ADDRESS:
        case FS_CFA_MBSM_SET_MAC_ADDR:
        case FS_HW_MBSM_SET_CUST_IF_PARAMS:
        case FS_NP_CFA_MBSM_ARP_MODIFY_FILTER:
        case FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER:

        {
            break;
        }
#endif /* MBSM_WANTED */
        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaNpWrFsHwGetVlanIntfStats *pLocalArgs = NULL;
            tCfaRemoteNpWrFsHwGetVlanIntfStats *pRemoteArgs = NULL;
            pLocalArgs = &pCfaNpModInfo->CfaNpFsHwGetVlanIntfStats;
            pRemoteArgs = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetVlanIntfStats;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->i1StatType = pRemoteArgs->i1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY ((pLocalArgs->pu4Value), &(pRemoteArgs->u4Value),
                        sizeof (UINT4));
            }
            break;
        }

        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilCfaRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tCfaNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilCfaRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                             tRemoteHwNp * pRemoteHwNpOutput)
{
    INT4                i4RetVal = FNP_SUCCESS;
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tCfaRemoteNpModInfo *pCfaRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pCfaRemoteNpModInfo = &(pRemoteHwNpInput->CfaRemoteNpModInfo);

    switch (u4Opcode)
    {
        case CFA_NP_GET_LINK_STATUS:
        {
            tCfaRemoteNpWrCfaNpGetLinkStatus *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetLinkStatus;
            pInput->u1PortLinkStatus = CfaNpGetLinkStatus (pInput->u4IfIndex);
            i4RetVal = FNP_SUCCESS;
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_REGISTER_WITH_NP_DRV:
        {
            i4RetVal = CfaRegisterWithNpDrv ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_CLEAR_STATS:
        {
            tCfaRemoteNpWrFsCfaHwClearStats *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwClearStats;
            i4RetVal = FsCfaHwClearStats (pInput->u4Port);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_GET_IF_FLOW_CONTROL:
        {
            tCfaRemoteNpWrFsCfaHwGetIfFlowControl *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetIfFlowControl;
            i4RetVal =
                FsCfaHwGetIfFlowControl (pInput->u4IfIndex,
                                         &(pInput->u4PortFlowControl));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_GET_MTU:
        {
            tCfaRemoteNpWrFsCfaHwGetMtu *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwGetMtu;
            i4RetVal =
                FsCfaHwGetMtu ((UINT2) pInput->u4IfIndex, &(pInput->u4MtuSize));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_SET_MAC_ADDR:
        {
            tCfaRemoteNpWrFsCfaHwSetMacAddr *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMacAddr;
            i4RetVal = FsCfaHwSetMacAddr (pInput->u4IfIndex, pInput->PortMac);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_SET_MTU:
        {
            tCfaRemoteNpWrFsCfaHwSetMtu *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetMtu;
            i4RetVal = FsCfaHwSetMtu (pInput->u4IfIndex, pInput->u4MtuSize);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_L3_SET_MTU:
        {
            tCfaRemoteNpWrFsCfaHwL3SetMtu *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwL3SetMtu;
            i4RetVal = FsCfaHwL3SetMtu (pInput->L3Mtu);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_SET_WAN_TYE:
        {
            tCfaRemoteNpWrFsCfaHwSetWanTye *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwSetWanTye;
            i4RetVal = FsCfaHwSetWanTye (&(pInput->CfaWanInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_GET_CNTRL:
        {
            tCfaRemoteNpWrFsEtherHwGetCntrl *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCntrl;
            i4RetVal =
                FsEtherHwGetCntrl (pInput->u4IfIndex, &(pInput->element),
                                   pInput->i4Code);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_GET_COLL_FREQ:
        {
            tCfaRemoteNpWrFsEtherHwGetCollFreq *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetCollFreq;
            i4RetVal =
                FsEtherHwGetCollFreq (pInput->u4IfIndex,
                                      pInput->i4dot3CollCount,
                                      &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_FRAMES:
        {
            tCfaRemoteNpWrFsEtherHwGetPauseFrames *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseFrames;
            i4RetVal =
                FsEtherHwGetPauseFrames (pInput->u4IfIndex, &(pInput->element),
                                         pInput->i4Code);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_MODE:
        {
            tCfaRemoteNpWrFsEtherHwGetPauseMode *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetPauseMode;
            i4RetVal =
                FsEtherHwGetPauseMode (pInput->u4IfIndex, &(pInput->element),
                                       pInput->i4Code);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_GET_STATS:
        {
            tCfaRemoteNpWrFsEtherHwGetStats *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwGetStats;
            i4RetVal =
                FsEtherHwGetStats (pInput->u4IfIndex, &(pInput->EthStats));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_ETHER_HW_SET_PAUSE_ADMIN_MODE:
        {
            tCfaRemoteNpWrFsEtherHwSetPauseAdminMode *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsEtherHwSetPauseAdminMode;
            i4RetVal =
                FsEtherHwSetPauseAdminMode (pInput->u4IfIndex,
                                            &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_GET_ETHERNET_TYPE:
        {
            tCfaRemoteNpWrFsHwGetEthernetType *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetEthernetType;
            i4RetVal =
                FsHwGetEthernetType (pInput->u4IfIndex, &(pInput->u1EtherType));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_GET_STAT:
        {
            tCfaRemoteNpWrFsHwGetStat *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat;
            i4RetVal =
                FsHwGetStat (pInput->u4IfIndex, pInput->i1StatType,
                             &(pInput->u4Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_GET_STAT64:
        {
            tCfaRemoteNpWrFsHwGetStat64 *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetStat64;
            i4RetVal =
                FsHwGetStat64 (pInput->u4IfIndex, pInput->i1StatType,
                               &(pInput->Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_SET_CUST_IF_PARAMS:
        {
            tCfaRemoteNpWrFsHwSetCustIfParams *pInput = NULL;
            tHwCustIfParamVal   CustIfParamVal;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwSetCustIfParams;

            CustIfParamVal.TLV.u4Type = pInput->CustIfParamVal.TLV.u4Type;
            CustIfParamVal.TLV.u4Length = pInput->CustIfParamVal.TLV.u4Length;
            CustIfParamVal.TLV.pu1Value = &(pInput->CustIfParamVal.TLV.u1Value);

            MEMCPY (&(CustIfParamVal.OpaqueAttrbs),
                    &(pInput->CustIfParamVal.OpaqueAttrbs),
                    sizeof (tHwCustIfOpqAttrs));

            i4RetVal =
                FsHwSetCustIfParams (pInput->u4IfIndex, pInput->HwCustParamType,
                                     CustIfParamVal, pInput->EntryAction);

            pInput->CustIfParamVal.TLV.u4Type = CustIfParamVal.TLV.u4Type;
            pInput->CustIfParamVal.TLV.u4Length = CustIfParamVal.TLV.u4Length;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_UPDATE_ADMIN_STATUS_CHANGE:
        {
            tCfaRemoteNpWrFsHwUpdateAdminStatusChange *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsHwUpdateAdminStatusChange;
            i4RetVal =
                FsHwUpdateAdminStatusChange (pInput->u4IfIndex,
                                             pInput->u1AdminEnable);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminConfig *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminConfig;
            i4RetVal =
                FsMauHwGetAutoNegAdminConfig (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegAdminStatus *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegAdminStatus;
            i4RetVal =
                FsMauHwGetAutoNegAdminStatus (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegCapAdvtBits *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapAdvtBits;
            i4RetVal =
                FsMauHwGetAutoNegCapAdvtBits (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_BITS:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegCapBits *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapBits;
            i4RetVal =
                FsMauHwGetAutoNegCapBits (pInput->u4IfIndex, pInput->i4IfMauId,
                                          &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegCapRcvdBits *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegCapRcvdBits;
            i4RetVal =
                FsMauHwGetAutoNegCapRcvdBits (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltAdvt *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltAdvt;
            i4RetVal =
                FsMauHwGetAutoNegRemFltAdvt (pInput->u4IfIndex,
                                             pInput->i4IfMauId,
                                             &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegRemFltRcvd *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRemFltRcvd;
            i4RetVal =
                FsMauHwGetAutoNegRemFltRcvd (pInput->u4IfIndex,
                                             pInput->i4IfMauId,
                                             &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegRemoteSignaling *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsMauHwGetAutoNegRemoteSignaling;
            i4RetVal =
                FsMauHwGetAutoNegRemoteSignaling (pInput->u4IfIndex,
                                                  pInput->i4IfMauId,
                                                  &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_RESTART:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegRestart *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegRestart;
            i4RetVal =
                FsMauHwGetAutoNegRestart (pInput->u4IfIndex, pInput->i4IfMauId,
                                          &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_SUPPORTED:
        {
            tCfaRemoteNpWrFsMauHwGetAutoNegSupported *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetAutoNegSupported;
            i4RetVal =
                FsMauHwGetAutoNegSupported (pInput->u4IfIndex,
                                            pInput->i4IfMauId,
                                            &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_FALSE_CARRIERS:
        {
            tCfaRemoteNpWrFsMauHwGetFalseCarriers *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetFalseCarriers;
            i4RetVal =
                FsMauHwGetFalseCarriers (pInput->u4IfIndex, pInput->i4IfMauId,
                                         &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_JABBER_STATE:
        {
            tCfaRemoteNpWrFsMauHwGetJabberState *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberState;
            i4RetVal =
                FsMauHwGetJabberState (pInput->u4IfIndex, pInput->i4IfMauId,
                                       &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_JABBERING_STATE_ENTERS:
        {
            tCfaRemoteNpWrFsMauHwGetJabberingStateEnters *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJabberingStateEnters;
            i4RetVal =
                FsMauHwGetJabberingStateEnters (pInput->u4IfIndex,
                                                pInput->i4IfMauId,
                                                &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_JACK_TYPE:
        {
            tCfaRemoteNpWrFsMauHwGetJackType *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetJackType;
            i4RetVal =
                FsMauHwGetJackType (pInput->u4IfIndex, pInput->i4IfMauId,
                                    pInput->i4ifJackIndex, &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_MAU_TYPE:
        {
            tCfaRemoteNpWrFsMauHwGetMauType *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMauType;
            i4RetVal =
                FsMauHwGetMauType (pInput->u4IfIndex, pInput->i4IfMauId,
                                   &(pInput->u4Val));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS:
        {
            tCfaRemoteNpWrFsMauHwGetMediaAvailStateExits *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailStateExits;
            i4RetVal =
                FsMauHwGetMediaAvailStateExits (pInput->u4IfIndex,
                                                pInput->i4IfMauId,
                                                &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAILABLE:
        {
            tCfaRemoteNpWrFsMauHwGetMediaAvailable *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetMediaAvailable;
            i4RetVal =
                FsMauHwGetMediaAvailable (pInput->u4IfIndex, pInput->i4IfMauId,
                                          &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_TYPE_LIST_BITS:
        {
            tCfaRemoteNpWrFsMauHwGetTypeListBits *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetTypeListBits;
            i4RetVal =
                FsMauHwGetTypeListBits (pInput->u4IfIndex, pInput->i4IfMauId,
                                        &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaRemoteNpWrFsMauHwSetAutoNegAdminStatus *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegAdminStatus;
            i4RetVal =
                FsMauHwSetAutoNegAdminStatus (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaRemoteNpWrFsMauHwSetAutoNegCapAdvtBits *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegCapAdvtBits;
            i4RetVal =
                FsMauHwSetAutoNegCapAdvtBits (pInput->u4IfIndex,
                                              pInput->i4IfMauId,
                                              &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaRemoteNpWrFsMauHwSetAutoNegRemFltAdvt *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRemFltAdvt;
            i4RetVal =
                FsMauHwSetAutoNegRemFltAdvt (pInput->u4IfIndex,
                                             pInput->i4IfMauId,
                                             &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_RESTART:
        {
            tCfaRemoteNpWrFsMauHwSetAutoNegRestart *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetAutoNegRestart;
            i4RetVal =
                FsMauHwSetAutoNegRestart (pInput->u4IfIndex, pInput->i4IfMauId,
                                          &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_MAU_STATUS:
        {
            tCfaRemoteNpWrFsMauHwSetMauStatus *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetMauStatus;
            i4RetVal =
                FsMauHwSetMauStatus (pInput->u4IfIndex, pInput->i4IfMauId,
                                     &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_STATUS:
        {
            tCfaRemoteNpWrFsMauHwGetStatus *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetStatus;
            i4RetVal =
                FsMauHwGetStatus (pInput->u4IfIndex, pInput->i4IfMauId,
                                  &(pInput->element));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_SET_DEFAULT_TYPE:
        {
            tCfaRemoteNpWrFsMauHwSetDefaultType *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwSetDefaultType;
            i4RetVal =
                FsMauHwSetDefaultType (pInput->u4IfIndex, pInput->i4IfMauId,
                                       pInput->u4Val);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MAU_HW_GET_DEFAULT_TYPE:
        {
            tCfaRemoteNpWrFsMauHwGetDefaultType *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsMauHwGetDefaultType;
            i4RetVal =
                FsMauHwGetDefaultType (pInput->u4IfIndex, pInput->i4IfMauId,
                                       &(pInput->u4Val));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case NP_CFA_FRONT_PANEL_PORTS:
        {
            tCfaRemoteNpWrNpCfaFrontPanelPorts *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpNpCfaFrontPanelPorts;
            i4RetVal = NpCfaFrontPanelPorts (pInput->u4MaxFrontPanelPorts);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_MAC_LOOKUP:
        {
            tCfaRemoteNpWrFsCfaHwMacLookup *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwMacLookup;
            i4RetVal =
                FsCfaHwMacLookup (&(pInput->u1MacAddr[0]), pInput->VlanId,
                                  &(pInput->u2Port));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_SET_HW_PORT_INFO:
        {
            tCfaRemoteNpWrCfaNpSetHwPortInfo *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetHwPortInfo;
            CfaNpSetHwPortInfo (pInput->HwPortInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_GET_HW_PORT_INFO:
        {
            tCfaRemoteNpWrCfaNpGetHwPortInfo *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwPortInfo;
            CfaGetLocalUnitPortInformation (&(pInput->HwPortInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_DELETE_PKT_FILTER:
        {
            tCfaRemoteNpWrFsCfaHwDeletePktFilter *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwDeletePktFilter;
            i4RetVal = FsCfaHwDeletePktFilter (pInput->u4FilterId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_CFA_HW_CREATE_PKT_FILTER:
        {
            tCfaRemoteNpWrFsCfaHwCreatePktFilter *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsCfaHwCreatePktFilter;
            i4RetVal =
                FsCfaHwCreatePktFilter (&(pInput->FilterInfo),
                                        &(pInput->u4FilterId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_CFA_SET_DLF_STATUS:
        {
            tCfaRemoteNpWrFsNpCfaSetDlfStatus *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsNpCfaSetDlfStatus;
            i4RetVal = FsNpCfaSetDlfStatus (pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_SET_STACKING_MODEL:
        {
            tCfaRemoteNpWrCfaNpSetStackingModel *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpSetStackingModel;
            i4RetVal = CfaNpSetStackingModel (pInput->u4StackingModel);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_GET_HW_INFO:
        {
            tCfaRemoteNpWrCfaNpGetHwInfo *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwInfo;
            CfaNpGetHwInfo (&(pInput->HwIdInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_REMOTE_SET_HW_INFO:
        {
            tCfaRemoteNpWrCfaNpRemoteSetHwInfo *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpRemoteSetHwInfo;
            CfaNpRemoteSetHwInfo (&(pInput->HwIdInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case CFA_NP_GET_HW_STACKING_INDEX:
        {
            tCfaRemoteNpWrCfaNpGetHwPortInfo *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpCfaNpGetHwPortInfo;
            /* Update data structure with received remote unit stack port information */
            MEMCPY (&(gRemoteHwPortInfo), &(pInput->HwPortInfo),
                    sizeof (tHwPortInfo));
            gRemoteStackingPort = FNP_SET;
            CfaGetLocalUnitPortInformation (&(pInput->HwPortInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaRemoteNpWrFsHwGetVlanIntfStats *pInput = NULL;
            pInput = &pCfaRemoteNpModInfo->CfaRemoteNpFsHwGetVlanIntfStats;
            i4RetVal =
                FsHwGetVlanIntfStats (pInput->u4IfIndex, pInput->i1StatType,
                                      &(pInput->u4Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH:
        {
            tCfaRemoteNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            i4RetVal =
                FsCfaHwRemoveIpNetRcvdDlfInHash (pInput->u4ContextId,
                                                 pInput->u4IpNet,
                                                 pInput->u4IpMask);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef IP6_WANTED
        case FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH:
        {
            tCfaRemoteNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pInput = NULL;
            pInput =
                &pCfaRemoteNpModInfo->
                CfaRemoteNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            i4RetVal =
                FsCfaHwRemoveIp6NetRcvdDlfInHash (pInput->u4ContextId,
                                                  pInput->Ip6Addr,
                                                  pInput->u4Prefix);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif

        default:
            i4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    u1RetVal = (UINT1) i4RetVal;
    return (u1RetVal);
}
#endif /* __CFAUTIL_C__ */
