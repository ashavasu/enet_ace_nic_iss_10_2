/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rmn2nputil.c,v 1.5 2015/09/13 10:36:22 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *
 *******************************************************************/

#ifndef __RMON2NPUTIL_C__
#define __RMON2NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskRmonNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef RMON2_WANTED
VOID
NpUtilMaskRmonv2NpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                         UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    BOOL1               bRemPortCheck = FNP_FALSE;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    pRmonv2NpModInfo = &(pFsHwNp->Rmonv2NpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_RMONV2_ADD_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2AddFlowStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2AddFlowStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        case FS_RMONV2_COLLECT_STATS:
        {
            tRmonv2NpWrFsRMONv2CollectStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2CollectStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        case FS_RMONV2_DISABLE_PROBE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_DISABLE_PROTOCOL:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_ENABLE_PROBE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_ENABLE_PROTOCOL:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_HW_SET_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_NP_DE_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_NP_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_RMONV2_REMOVE_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2RemoveFlowStats *pEntry = NULL;
            pEntry = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2RemoveFlowStats;
            bRemPortCheck = FNP_TRUE;
            pFsHwNp->u4IfIndex = pEntry->FlowStatsTuple.u4IfIndex;
            break;
        }
        case FS_RMONV2_GET_PORT_ID:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;

        }

        default:
            break;
    }
    if (bRemPortCheck == FNP_TRUE)
    {
        if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
        else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex, &HwPortInfo) ==
                 FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
    }
    return;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonv2ConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonv2ConvertLocalToRemoteNp (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2RemoteNpModInfo *pRmonv2RemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pRmonv2NpModInfo = &(pFsHwNp->Rmonv2NpModInfo);
    pRmonv2RemoteNpModInfo = &(pRemoteHwNp->Rmonv2RemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tRmonv2RemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_RMONV2_ADD_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2AddFlowStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2AddFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2AddFlowStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2AddFlowStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        case FS_RMONV2_COLLECT_STATS:
        {
            tRmonv2NpWrFsRMONv2CollectStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2CollectStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2CollectStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2CollectStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            MEMCPY (&(pRemoteArgs->StatsCnt), (pLocalArgs->pStatsCnt),
                    sizeof (tRmon2Stats));
            break;
        }
        case FS_RMONV2_DISABLE_PROBE:
        {
            tRmonv2NpWrFsRMONv2DisableProbe *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2DisableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProbe;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2DisableProbe;
            pRemoteArgs->u4InterfaceIdx = pLocalArgs->u4InterfaceIdx;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                    sizeof (tPortList));
            break;
        }
        case FS_RMONV2_DISABLE_PROTOCOL:
        {
            tRmonv2NpWrFsRMONv2DisableProtocol *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2DisableProtocol *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2DisableProtocol;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2DisableProtocol;
            pRemoteArgs->u4ProtocolLclIdx = pLocalArgs->u4ProtocolLclIdx;
            break;
        }
        case FS_RMONV2_ENABLE_PROBE:
        {
            tRmonv2NpWrFsRMONv2EnableProbe *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2EnableProbe *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProbe;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2EnableProbe;
            pRemoteArgs->u4InterfaceIdx = pLocalArgs->u4InterfaceIdx;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                    sizeof (tPortList));
            break;
        }
        case FS_RMONV2_ENABLE_PROTOCOL:
        {
            tRmonv2NpWrFsRMONv2EnableProtocol *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2EnableProtocol *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2EnableProtocol;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2EnableProtocol;
            MEMCPY (&(pRemoteArgs->Rmon2ProtoIdfr),
                    &(pLocalArgs->Rmon2ProtoIdfr), sizeof (tRmon2ProtocolIdfr));
            break;
        }
        case FS_RMONV2_HW_SET_STATUS:
        {
            tRmonv2NpWrFsRMONv2HwSetStatus *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2HwSetStatus *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2HwSetStatus;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2HwSetStatus;
            pRemoteArgs->u4RMONv2Status = pLocalArgs->u4RMONv2Status;
            break;
        }
        case FS_RMONV2_NP_DE_INIT:
        {
            break;
        }
        case FS_RMONV2_NP_INIT:
        {
            break;
        }
        case FS_RMONV2_REMOVE_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2RemoveFlowStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2RemoveFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2RemoveFlowStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2RemoveFlowStats;
            MEMCPY (&(pRemoteArgs->FlowStatsTuple),
                    &(pLocalArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        case FS_RMONV2_GET_PORT_ID:
        {
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonv2ConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonv2ConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp,
                                    tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonv2NpModInfo   *pRmonv2NpModInfo = NULL;
    tRmonv2RemoteNpModInfo *pRmonv2RemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pRmonv2NpModInfo = &(pFsHwNp->Rmonv2NpModInfo);
    pRmonv2RemoteNpModInfo = &(pRemoteHwNp->Rmonv2RemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_RMONV2_ADD_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2AddFlowStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2AddFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2AddFlowStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2AddFlowStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        case FS_RMONV2_COLLECT_STATS:
        {
            tRmonv2NpWrFsRMONv2CollectStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2CollectStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2CollectStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2CollectStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            MEMCPY ((pLocalArgs->pStatsCnt), &(pRemoteArgs->StatsCnt),
                    sizeof (tRmon2Stats));
            break;
        }
        case FS_RMONV2_REMOVE_FLOW_STATS:
        {
            tRmonv2NpWrFsRMONv2RemoveFlowStats *pLocalArgs = NULL;
            tRmonv2RemoteNpWrFsRMONv2RemoveFlowStats *pRemoteArgs = NULL;
            pLocalArgs = &pRmonv2NpModInfo->Rmonv2NpFsRMONv2RemoveFlowStats;
            pRemoteArgs =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2RemoveFlowStats;
            MEMCPY (&(pLocalArgs->FlowStatsTuple),
                    &(pRemoteArgs->FlowStatsTuple), sizeof (tPktHeader));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilRmonv2RemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tRmonv2NpModInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmonv2RemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                                tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonv2RemoteNpModInfo *pRmonv2RemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pRmonv2RemoteNpModInfo = &(pRemoteHwNpInput->Rmonv2RemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_RMONV2_ADD_FLOW_STATS:
        {
            tRmonv2RemoteNpWrFsRMONv2AddFlowStats *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2AddFlowStats;
            u1RetVal = FsRMONv2AddFlowStats (pInput->FlowStatsTuple);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_COLLECT_STATS:
        {
            tRmonv2RemoteNpWrFsRMONv2CollectStats *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2CollectStats;
            u1RetVal =
                FsRMONv2CollectStats (pInput->FlowStatsTuple,
                                      &(pInput->StatsCnt));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_DISABLE_PROBE:
        {
            tRmonv2RemoteNpWrFsRMONv2DisableProbe *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2DisableProbe;
            u1RetVal =
                FsRMONv2DisableProbe (pInput->u4InterfaceIdx, pInput->u2VlanId,
                                      pInput->PortList);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_DISABLE_PROTOCOL:
        {
            tRmonv2RemoteNpWrFsRMONv2DisableProtocol *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2DisableProtocol;
            u1RetVal = FsRMONv2DisableProtocol (pInput->u4ProtocolLclIdx);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_ENABLE_PROBE:
        {
            tRmonv2RemoteNpWrFsRMONv2EnableProbe *pInput = NULL;
            pInput = &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2EnableProbe;
            u1RetVal =
                FsRMONv2EnableProbe (pInput->u4InterfaceIdx, pInput->u2VlanId,
                                     pInput->PortList);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_ENABLE_PROTOCOL:
        {
            tRmonv2RemoteNpWrFsRMONv2EnableProtocol *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2EnableProtocol;
            u1RetVal = FsRMONv2EnableProtocol (pInput->Rmon2ProtoIdfr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_HW_SET_STATUS:
        {
            tRmonv2RemoteNpWrFsRMONv2HwSetStatus *pInput = NULL;
            pInput = &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2HwSetStatus;
            u1RetVal = FsRMONv2HwSetStatus (pInput->u4RMONv2Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_NP_DE_INIT:
        {
            FsRMONv2NPDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_NP_INIT:
        {
            u1RetVal = FsRMONv2NPInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_REMOVE_FLOW_STATS:
        {
            tRmonv2RemoteNpWrFsRMONv2RemoveFlowStats *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2RemoveFlowStats;
            u1RetVal = FsRMONv2RemoveFlowStats (pInput->FlowStatsTuple);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_RMONV2_GET_PORT_ID:
        {
            tRmonv2RemoteNpWrFsRMONv2GetPortID *pInput = NULL;
            pInput =
                &pRmonv2RemoteNpModInfo->Rmonv2RemoteNpFsRMONv2GetPortID;
            u1RetVal = FsRMONv2GetPortID (pInput->au1Mac,pInput->u4VlanIndex,&pInput->i4Index);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;

        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* RMON2_WANTED */
#endif /* __RMON2NPUTIL_C__ */
