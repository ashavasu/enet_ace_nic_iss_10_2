/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: npbcmsup.h,v 1.22 2018/02/02 09:47:36 siva Exp $
 *
 * Description:This file contains the BCM supported calls
 *             for Hardware Programming, specified on module basis.
 *
 *******************************************************************/
#ifndef _NPBCMSUP_H_
#define _NPBCMSUP_H_
#ifdef _NPSTACKUTL_C_ 
/* For addition of new NP calls in the following modules
 * the corresponding array needs to be updated
 * 1. LA
 * 2. VLAN
 * 3. CFA 
 * 4. IP
 * 5. ISS
 * {0,  - 0/1 NP call is supported/not supported
 * FNP_TRUE, - FNP_TRUE/FNP_FALSE  Acknowledgement required/Not required 
 * 0}  
 * */
tNpMapTable gLaNpMapTable [FS_LA_HW_MAX_NP]=
{
   {0, FNP_TRUE, 0},/* FS_LA_HW_CREATE_AGG_GROUP                            */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_ADD_LINK_TO_AGG_GROUP                       */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_SET_SELECTION_POLICY                        */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST               */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP                  */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_DELETE_AGGREGATOR                           */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_INIT                                        */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_DE_INIT                                     */ 
   {1, FNP_TRUE, 0},/* FS_LA_HW_ENABLE_COLLECTION                           */ 
   {1, FNP_TRUE, 0},/* FS_LA_HW_ENABLE_DISTRIBUTION                         */ 
   {1, FNP_TRUE, 0},/* FS_LA_HW_DISABLE_COLLECTION                          */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_SET_PORT_CHANNEL_STATUS                     */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP                  */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP             */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR                 */ 
   {0, FNP_TRUE, 0},/* FS_LA_GET_NEXT_AGGREGATOR                            */ 
   {0, FNP_TRUE, 0},/* FS_LA_RED_HW_INIT                                    */ 
   {0, FNP_TRUE, 0},/* FS_LA_RED_HW_NP_DELETE_AGGREGATOR                    */ 
   {0, FNP_TRUE, 0},/* FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR               */ 
   {0, FNP_TRUE, 0},/* FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT              */ 
   {0, FNP_TRUE, 0},/* FS_LA_HW_DLAG_STATUS                                 */ 
   {1, FNP_TRUE, 0},/* FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP                  */ 
   {1, FNP_TRUE, 0},/* FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP             */              
   {0, FNP_FALSE, 0},/* FS_LA_MBSM_HW_CREATE_AGG_GROUP                       */ 
   {0, FNP_FALSE, 0},/* FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP             */ 
   {0, FNP_FALSE, 0},/* FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP                  */ 
   {0, FNP_FALSE, 0},/* FS_LA_MBSM_HW_SET_SELECTION_POLICY                   */ 
   {0, FNP_FALSE, 0},/* FS_LA_MBSM_HW_INIT                                   */ 
   {0, FNP_FALSE, 0} /* Last entry should be dummy entry */
};

tNpMapTable gVlanNpMapTable[FS_MI_VLAN_HW_MAX_NP ] =
{
    {0, FNP_TRUE, 0},/* FS_MI_VLAN_HW_INIT */
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DE_INIT */  
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS                     */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS                   */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS                */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS                  */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_FDB_ENTRY                            */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_FDB_COUNT                            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY                    */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_MCAST_ENTRY                          */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_MCAST_PORT                           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_MCAST_PORT                         */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_MCAST_ENTRY                          */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_VLAN_ENTRY                           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_VLAN_ENTRY                           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT                     */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_PVID                            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID                      */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID                     */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL                   */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP                    */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SCAN_MULTICAST_TBL                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY                       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SCAN_UNICAST_TBL                         */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR         */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE                  */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_ING_FILTERING                   */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_VLAN_ENABLE                              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_VLAN_DISABLE                             */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE                   */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT             */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT          */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT                */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY                    */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES                */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY                  */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP                      */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GMRP_ENABLE                              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GMRP_DISABLE                            */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GVRP_ENABLE                             */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GVRP_DISABLE                            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_NP_DELETE_ALL_FDB_ENTRIES                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_PORT_STATS                          */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_PORT_STATS64                        */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_VLAN_STATS                          */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_VLAN_STATS                        */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE                    */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_TUNNEL_FILTER                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS                     */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS                    */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_CREATE_FDB_ID                           */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DELETE_FDB_ID                           */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB                      */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST                     */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_FLUSH_PORT                              */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_FLUSH_FDB_ID                            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_SHORT_AGEOUT                        */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_SHORT_AGEOUT                      */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_VLAN_INFO                           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_MCAST_ENTRY                         */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT                    */ 
    {0, FNP_TRUE, 0},/*  FS_MI_BRG_SET_AGING_TIME                              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_BRG_MODE                            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE                    */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY                 */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY           */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_NP_HW_RUN_MAC_AGEING                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_MAC_LEARNING_LIMIT                      */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT               */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_MAC_LEARNING_STATUS                     */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS            */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT      */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS               */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX               */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX               */ 
    {1, FNP_TRUE, 0},/*  FS_VLAN_HW_FORWARD_PKT_ON_PORTS                       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS                */ 
    {0, FNP_TRUE, 0},/*  FS_VLAN_HW_GET_MAC_LEARNING_MODE                      */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE           */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE             */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS      */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY            */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY            */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS         */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY               */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY               */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_ADD_S_VLAN_MAP                          */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DELETE_S_VLAN_MAP                       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD         */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT                 */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT               */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN                  */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN                */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT               */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT                  */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PEP_PVID                            */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE                  */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY               */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PEP_ING_FILTERING                   */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP                      */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP                      */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL                       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PCP_DECOD_TBL                       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_USE_DEI                        */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING              */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION                  */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY             */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS                  */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP                     */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY       */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_PORT_PROPERTY                       */ 
    {1, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_LOOPBACK_STATUS                      */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE        */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE         */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY                  */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE      */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY       */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS    */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY          */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP                     */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD    */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT            */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN             */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT          */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL                  */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL                  */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI                   */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING         */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION             */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY        */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT          */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_INIT                               */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID                */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY             */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT                     */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT                   */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY             */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY                    */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT                     */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT                   */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY                     */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT               */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT             */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_PVID                      */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE            */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING             */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY              */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT               */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP                */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY                 */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY                 */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES          */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY            */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT    */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT          */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS               */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS            */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER                  */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY         */ 
    {0, FNP_FALSE, 0},/*  FS_MI_BRG_MBSM_SET_AGING_TIME                         */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS       */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS          */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT          */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS           */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT                 */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX          */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO                       */ 
    {1, FNP_FALSE, 0},/*  FS_MI_VLAN_MBSM_HW_SET_BRG_MODE                       */ 
    {0, FNP_FALSE, 0},/*  FS_MI_VLAN_HW_SET_EVC_ATTRIBUTE                       */ 
    {0, FNP_TRUE, 0}, /*  FS_NP_HW_GET_PORT_FROM_FDB                            */ 
    {1, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_SET_CVLAN_STAT                          */ 
    {1, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_GET_CVLAN_STAT                          */ 
    {1, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_CLEAR_CVLAN_STAT                        */ 
    {1, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_PORT_UNICAST_MAC_SEC_TYPE               */ 
    {0, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE                   */ 
    {0, FNP_TRUE, 0}, /*  FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE              */ 
    {0, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE                    */
    {0,FNP_TRUE,  0}, /*  FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE               */
    {0, FNP_TRUE, 0}, /*  FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS                 */
    {0, FNP_FALSE, 0},/* FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS             */ 
    {0, FNP_TRUE, 0},/*  FS_MI_VLAN_HW_SET_MCAST_INDEX                         */ 
    {0, FNP_FALSE, 0}  /* Last entry should be dummy entry */
}; 

tNpMapTable gCfaNpMapTable[FS_CFA_HW_MAX_NP ] = 
{
    {0, FNP_TRUE, 0},/* CFA_NP_GET_LINK_STATUS                                */ 
    {0, FNP_TRUE, 0},/* CFA_REGISTER_WITH_NP_DRV                              */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_CLEAR_STATS                                 */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_GET_IF_FLOW_CONTROL                         */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_GET_MTU                                     */ 
    {1, FNP_TRUE, 0},/* FS_CFA_HW_SET_MAC_ADDR                                */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_SET_MTU                                     */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_SET_WAN_TYE                                 */ 
    {0, FNP_TRUE, 0},/* FS_ETHER_HW_GET_CNTRL                                 */ 
    {1, FNP_TRUE, 0},/* FS_ETHER_HW_GET_COLL_FREQ                             */ 
    {0, FNP_TRUE, 0},/* FS_ETHER_HW_GET_PAUSE_FRAMES                          */ 
    {0, FNP_TRUE, 0},/* FS_ETHER_HW_GET_PAUSE_MODE                            */ 
    {0, FNP_TRUE, 0},/* FS_ETHER_HW_GET_STATS                                 */ 
    {0, FNP_TRUE, 0},/* FS_ETHER_HW_SET_PAUSE_ADMIN_MODE                      */ 
    {0, FNP_TRUE, 0},/* FS_HW_GET_ETHERNET_TYPE                               */ 
    {0, FNP_TRUE, 0},/* FS_HW_GET_STAT                                        */ 
    {0, FNP_TRUE, 0},/* FS_HW_GET_STAT64                                      */ 
    {1, FNP_TRUE, 0},/* FS_HW_SET_CUST_IF_PARAMS                              */ 
    {0, FNP_TRUE, 0},/* FS_HW_UPDATE_ADMIN_STATUS_CHANGE                      */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG                   */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS                   */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS                  */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_CAP_BITS                       */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS                  */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT                   */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD                   */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING               */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_RESTART                        */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_AUTO_NEG_SUPPORTED                      */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_FALSE_CARRIERS                          */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_JABBER_STATE                            */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_JABBERING_STATE_ENTERS                  */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_JACK_TYPE                               */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_MAU_TYPE                                */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS                 */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_MEDIA_AVAILABLE                         */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_GET_TYPE_LIST_BITS                          */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS                   */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS                  */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT                   */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_SET_AUTO_NEG_RESTART                        */ 
    {0, FNP_TRUE, 0},/* FS_MAU_HW_SET_MAU_STATUS                              */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_STATUS                                  */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_SET_DEFAULT_TYPE                            */ 
    {1, FNP_TRUE, 0},/* FS_MAU_HW_GET_DEFAULT_TYPE                            */ 
    {0, FNP_TRUE, 0},/* NP_CFA_FRONT_PANEL_PORTS                              */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_MAC_LOOKUP                                  */ 
    {0, FNP_TRUE, 0},/* CFA_NP_SET_HW_PORT_INFO                               */ 
    {0, FNP_TRUE, 0},/* CFA_NP_GET_HW_PORT_INFO                               */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_DELETE_PKT_FILTER                           */ 
    {0, FNP_TRUE, 0},/* FS_CFA_HW_CREATE_PKT_FILTER                           */ 
    {1, FNP_TRUE, 0},/* FS_NP_CFA_SET_DLF_STATUS                              */ 
    {0, FNP_TRUE, 0},/* CFA_NP_SET_STACKING_MODEL                             */ 
    {0, FNP_TRUE, 0},/* CFA_NP_GET_HW_INFO                                    */ 
    {0, FNP_TRUE, 0},/* CFA_NP_REMOTE_SET_HW_INFO                             */ 
    {0, FNP_TRUE, 0},/* CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH                 */ 
#ifdef IP6_WANTED
    {0, FNP_TRUE, 0},/* CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH                */ 
#endif
    {0, FNP_TRUE, 0},/* CFA_NP_GET_HW_STACKING_INDEX                          */
    {0, FNP_TRUE, 0},/* FS_HW_GET_VLAN_INTF_STAT                              */
    {0, FNP_FALSE, 0},/* CFA_MBSM_NP_SLOT_DE_INIT                              */
    {0, FNP_FALSE, 0},/* CFA_MBSM_NP_SLOT_INIT                                 */
    {0, FNP_FALSE, 0},/* CFA_MBSM_REGISTER_WITH_NP_DRV                         */
    {0, FNP_FALSE, 0},/* CFA_MBSM_UPDATE_PORT_MAC_ADDRESS                      */
    {0, FNP_FALSE, 0},/* FS_CFA_MBSM_SET_MAC_ADDR                              */
    {0, FNP_FALSE, 0},/* FS_NP_CFA_MBSM_ARP_MODIFY_FILTER                      */
    {0, FNP_FALSE, 0},/* FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER                     */
    {0, FNP_FALSE, 0},/* FS_HW_MBSM_SET_CUST_IF_PARAMS                         */
    {0, FNP_FALSE, 0}  /* Last entry should be dummy entry */
};


tNpMapTable gVcmNpMapTable [FS_NP_VCM_MAX_NP] =
{
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_CREATE_CONTEXT                             */ 
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_DELETE_CONTEXT                             */
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_MAP_PORT_TO_CONTEXT                        */
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_UNMAP_PORT_FROM_CONTEXT                    */
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_MAP_IFINDEX_TO_BRGPORT                     */
  {1, FNP_TRUE, 0}, /* FS_VCM_HW_MAP_VIRTUAL_ROUTER                         */
  {1, FNP_TRUE, 0}, /* FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS                  */
  {1, FNP_TRUE, 0}, /* FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING                 */
#ifdef MBSM_WANTED
    {1, FNP_TRUE, 0}, /* FS_VCM_MBSM_HW_CREATE_CONTEXT                        */
    {1, FNP_TRUE, 0}, /* FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT              */
    {1, FNP_TRUE, 0}, /* FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT                   */
    {1, FNP_TRUE, 0}, /* FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER                    */
    {1, FNP_TRUE, 0}, /* FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS             */
    {1, FNP_TRUE, 0}, /* FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING            */
#endif /* MBSM_WANTED */
  {0, FNP_FALSE, 0} /* Last entry should be dummy entry                     */
};

tNpMapTable gRstpNpMapTable [FS_NP_RSTP_MAX_NP] = 
{
  {1, FNP_TRUE, 0}, /* FS_MI_STP_NP_HW_INIT                                 */
  {1, FNP_TRUE, 0}, /* FS_MI_RSTP_NP_INIT_HW                                */
  {0, FNP_TRUE, 0}, /* FS_MI_RSTP_NP_SET_PORT_STATE                         */
#ifdef MBSM_WANTED
  {0, FNP_FALSE, 0}, /* FS_MI_RSTP_MBSM_NP_SET_PORT_STATE                    */
  {1, FNP_FALSE, 0}, /* FS_MI_RSTP_MBSM_NP_INIT_HW                           */
#endif
  {0, FNP_TRUE, 0}, /* FS_MI_RST_NP_GET_PORT_STATE                          */
#ifdef PB_WANTED
  {1, FNP_TRUE, 0}, /* FS_MI_PB_RSTP_NP_SET_PORT_STATE                      */
  {1, FNP_TRUE, 0}, /* FS_MI_PB_RST_NP_GET_PORT_STATE                       */
#ifdef MBSM_WANTED
  {1, FNP_FALSE, 0}, /* FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE                 */
#endif
#endif
#ifdef PBB_WANTED
  {1, FNP_TRUE, 0},/*FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS      */
#endif
  {0, FNP_FALSE,0} /* Last Entry should be dummy Entry                      */
};

tNpMapTable gMstpNpMapTable [FS_NP_MSTP_MAX_NP] =
{
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_CREATE_INSTANCE                        */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING                  */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING             */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_DELETE_INSTANCE                        */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING                  */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING             */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE                */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_INIT_HW                                */
  {0, FNP_TRUE, 0}, /* FS_MI_MSTP_NP_DE_INIT_HW                             */
  {0, FNP_TRUE, 0}, /* FS_MI_MST_NP_GET_PORT_STATE                          */
#ifdef MBSM_WANTED
  {0, FNP_FALSE, 0}, /* FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE                   */
  {0, FNP_FALSE, 0}, /* FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING             */
  {0, FNP_FALSE, 0}, /* FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE           */
  {0, FNP_FALSE, 0}, /* FS_MI_MSTP_MBSM_NP_INIT_HW                           */
#endif
  {0, FNP_FALSE,0} /* Last Entry should be dummy Entry                      */
};

tNpMapTable gPvrStpNpMapTable [FS_NP_PVRST_MAX_NP] =
{
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE             */
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE             */
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_INIT_HW                               */
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_DE_INIT_HW                            */
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_SET_VLAN_PORT_STATE                   */
  {0, FNP_TRUE, 0}, /* FS_MI_PVRST_NP_GET_VLAN_PORT_STATE                   */
#ifdef MBSM_WANTED
  {1, FNP_FALSE, 0}, /* FS_MI_PVRST_MBSM_NP_SET_VLAN_PORT_STATE              */
  {1, FNP_FALSE, 0}, /* FS_MI_PVRST_MBSM_NP_INIT_HW                          */
#endif
  {0, FNP_FALSE,0}  /* Last Entry should be dummy Entry                     */
};

tNpMapTable gQosNpMapTable [FS_QOS_HW_MAX_NP]=
{
    {0, FNP_TRUE, 0}, /* QO_S_HW_INIT = 1 */
    {0, FNP_TRUE, 0},/* QO_S_HW_MAP_CLASS_TO_POLICY*/
    {0, FNP_TRUE, 0},/* QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS*/
    {1, FNP_TRUE, 0},/* QO_S_HW_UNMAP_CLASS_FROM_POLICY*/
    {0, FNP_TRUE, 0},/* QO_S_HW_DELETE_CLASS_MAP_ENTRY*/
    {1, FNP_TRUE, 0},/* QO_S_HW_METER_CREATE*/
    {1, FNP_TRUE, 0},/* QO_S_HW_METER_DELETE*/
    {0, FNP_TRUE, 0},/* QO_S_HW_SCHEDULER_ADD*/
    {0, FNP_TRUE, 0},/* QO_S_HW_SCHEDULER_UPDATE_PARAMS*/
    {0, FNP_TRUE, 0},/* QO_S_HW_SCHEDULER_DELETE*/
    {0, FNP_TRUE, 0},/* QO_S_HW_QUEUE_CREATE*/
    {0, FNP_TRUE, 0},/* QO_S_HW_QUEUE_DELETE*/
    {1, FNP_TRUE, 0},/* QO_S_HW_MAP_CLASS_TO_QUEUE*/
    {0, FNP_TRUE, 0},/* QO_S_HW_MAP_CLASS_TO_QUEUE_ID*/
    {0, FNP_TRUE, 0},/* QO_S_HW_SCHEDULER_HIERARCHY_MAP*/
    {0, FNP_TRUE, 0},/* QO_S_HW_SET_DEF_USER_PRIORITY*/
    {0, FNP_TRUE, 0},/* QO_S_HW_GET_METER_STATS*/
    {0, FNP_TRUE, 0},/* QO_S_HW_GET_CO_S_Q_STATS*/
    {0, FNP_TRUE, 0}, /* FS_QOS_HW_CONFIG_PFC */
    {1, FNP_TRUE, 0},/* QO_S_HW_GET_COUNT_ACT_STATS*/
    {1, FNP_TRUE, 0},/* QO_S_HW_GET_ALG_DROP_STATS*/
    {1, FNP_TRUE, 0},/* QO_S_HW_GET_RANDOM_DROP_STATS*/
    {0, FNP_TRUE, 0}, /* QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP */
    {0, FNP_TRUE, 0}, /* QO_S_HW_SET_CPU_RATE_LIMIT */
    {0, FNP_TRUE, 0}, /* QO_S_HW_MAP_CLASSTO_PRI_MAP */
    {0, FNP_TRUE, 0}, /* QOS_HW_MAP_CLASS_TO_INT_PRIORITY */
    {1, FNP_TRUE, 0}, /* QO_S_HW_GET_DSCP_Q_MAP */
#ifdef MBSM_WANTED
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_QUEUE_CREATE */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID */
    {0, FNP_TRUE, 0}, /* FS_QOS_MBSM_HW_CONFIG_PFC */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SCHEDULER_ADD */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_METER_CREATE */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_INIT */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SET_CPU_RATE_LIMIT */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_MAP_CLASS_TO_POLICY */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP */
    {0, FNP_TRUE, 0}, /* QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY */
    {0, FNP_TRUE, 0}, /* QO_S_MBSM_HW_SET_DEF_USER_PRIORITY */
#endif /* MBSM_WANTED */
    {0, FNP_TRUE, 0}, /* QO_S_HW_SET_TRAF_CLASS_TO_PCP */
    {0, FNP_TRUE, 0}, /* QO_S_HW_METER_STAT_UPDATE */
    {0, FNP_TRUE, 0}, /* QO_S_HW_METER_STAT_CLEAR */
    {0, FNP_TRUE, 0}, /* QO_S_HW_SET_VLAN_QUEUING_STATUS */
    {0, FNP_TRUE, 0}, /* QOS_HW_EGRESS_TX_STATUS */
    {0, FNP_TRUE, 0}, /* QOS_HW_CONFIGURE_WRED_PROFILE */
    {0, FNP_TRUE, 0}, /* QOS_HW_DISABLE_WRED_PROFILE */
    {0, FNP_TRUE, 0}, /* QOS_HW_CLASSIFIER_DEL_ACTION_FP */
    {0, FNP_TRUE, 0}, /* QOS_HW_CLASSIFIER_ADD_ACTION_FP */
    {0, FNP_TRUE, 0},/* QOS_SET_IN_PROFILE_ACTION_FP*/
    {0, FNP_TRUE, 0},/* QOS_SET_OUT_PROFILE_ACTION_FP*/
    {0, FNP_TRUE, 0},/* QOS_HW_CLASSIFIER_UPD_ACTION_FP*/
    {0, FNP_TRUE, 0},/* QOS_UPD_OUT_PROFILE_ACTION_FP*/
    {0, FNP_TRUE, 0}, /* QOS_UPD_IN_PROFILE_ACTION_FP */
    {0, FNP_TRUE, 0},/* QOS_SET_EXCEED_PROFILE_ACTION_FP*/
    {0, FNP_TRUE, 0},/* QOS_ADD_L2_L3_FILTER_ACTION_AND_INSTALL*/
    {0, FNP_TRUE, 0},/* QOS_HW_OUT_PRIORITY_REGENERATE*/
    {0, FNP_TRUE, 0},/* QOS_HW_UPDATE_CLASS_TO_INT_PRI*/
    {0, FNP_TRUE, 0},/* QOS_HW_UPDATE_OUT_PKT_PRI*/
    {0, FNP_TRUE, 0},/* QOS_HW_CHECK_PHB_CONFLICT*/
    {0, FNP_TRUE, 0},/* QOS_CHK_AND_VALIDATE_ACTION_FPS*/
    {0, FNP_TRUE, 0},/* QOS_HW_DEFAULT_DSCP_CONFIG*/
    {0, FNP_TRUE, 0},/* QOS_HW_STACK_SCHEDULER_SET*/
    {0, FNP_TRUE, 0},/* QOS_HW_CPU_SCHEDULER_SET*/
    {0, FNP_TRUE, 0},/* QOS_HW_DEFAULT_MPLS_EXP_CONFIG*/
    {0, FNP_TRUE, 0}, /* FS_QOS_HW_GET_PFC_STATS */
    {0, FNP_FALSE, 0},/* FS_QOS_HW_MAX_NP */
};

tNpMapTable gIpNpMapTable [FS_NP_IPV4_MAX_NP ] =
{
  {0, FNP_TRUE, 0}, /* FS_NP_IP_INIT                                        */ 
  {0, FNP_TRUE, 0}, /* FS_NP_OSPF_INIT                                      */ 
  {0, FNP_TRUE, 0}, /* FS_NP_OSPF_DE_INIT                                   */ 
  {0, FNP_TRUE, 0}, /* FS_NP_DHCP_SRV_INIT                                  */ 
  {0, FNP_TRUE, 0}, /* FS_NP_DHCP_SRV_DE_INIT                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_DHCP_RLY_INIT                                  */ 
  {0, FNP_TRUE, 0}, /* FS_NP_DHCP_RLY_DE_INIT                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_RIP_INIT                                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_RIP_DE_INIT                                    */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CREATE_IP_INTERFACE                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_MODIFY_IP_INTERFACE                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_DELETE_IP_INTERFACE                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_SET_FORWARDING_STATUS                     */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CLEAR_ROUTE_TABLE                         */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CLEAR_ARP_TABLE                           */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_UC_DEL_ROUTE                              */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_ARP_ADD                                   */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_ARP_MODIFY                                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_ARP_DEL                                   */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CHECK_HIT_ON_ARP_ENTRY                    */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO                     */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_UC_ADD_ROUTE                              */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_ARP_ADD                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_ARP_MODIFY                            */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_ARP_DEL                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_CLEAR_ARP_TABLE                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR                 */ 
  {0, FNP_TRUE, 0}, /* FS_NP_CFA_VRF_SET_DLF_STATUS                         */ 
  {0, FNP_TRUE, 0}, /* FS_NP_L3_IPV4_VRF_ARP_ADD                            */ 
  {0, FNP_TRUE, 0}, /* FS_NP_L3_IPV4_VRF_ARP_MODIFY                         */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_ARP_GET                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRF_ARP_GET_NEXT                          */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRRP_INTF_CREATE_WR                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CREATE_VRRP_INTERFACE                     */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRRP_INTF_DELETE_WR                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_DELETE_VRRP_INTERFACE                     */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_GET_VRRP_INTERFACE                        */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRRP_INSTALL_FILTER                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRRP_REMOVE_FILTER                        */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_IS_RT_PRESENT_IN_FAST_PATH                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_GET_STATS                                 */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_VRM_ENABLE_VR                             */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_BIND_IF_TO_VR_ID                          */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_GET_NEXT_HOP_INFO                         */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_UC_ADD_TRAP                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_CLEAR_FOWARDING_TBL                       */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_GET_SRC_MOVED_IP_ADDR                     */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE                 */ 
  {0, FNP_TRUE, 0}, /* FS_NP_ISIS_HW_PROGRAM                                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_NAT_DISABLE_ON_INTF                            */ 
  {0, FNP_TRUE, 0}, /* FS_NP_NAT_ENABLE_ON_INTF                             */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_INTF_STATUS                               */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_ARP_GET                                   */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_ARP_GET_NEXT                              */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_UC_GET_ROUTE                              */ 
  {0, FNP_TRUE, 0}, /* FS_NP_L3_IPV4_ARP_ADD                                */ 
  {0, FNP_TRUE, 0}, /* FS_NP_L3_IPV4_ARP_MODIFY                             */ 
  {0, FNP_TRUE, 0}, /* FS_NP_IPV4_L3_IP_INTERFACE                           */ 
  {0, FNP_TRUE, 0}, /* FS_NP_VRRP_HW_PROGRAM                                */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IP_INIT                                   */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_OSPF_INIT                                 */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_DHCP_SRV_INIT                             */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_DHCP_RLY_INIT                             */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE                  */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_ARP_ADD                              */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_UC_ADD_ROUTE                         */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_VRM_ENABLE_VR                        */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_BIND_IF_TO_VR_ID                     */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_UC_ADD_TRAP                          */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE            */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_VRF_ARP_ADD                          */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_CREATE_VRRP_INTERFACE                */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_VRRP_INSTALL_FILTER                  */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_ARP_ADDITION                         */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_VRF_ARP_ADDITION                     */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS           */ 
  {0, FNP_FALSE, 0}, /* FS_NP_MBSM_ISIS_HW_PROGRAM                           */ 
  {1, FNP_TRUE, 0}, /* FS_NP_IPV4_DELETE_SEC_IP_INTERFACE                   */ 
  {0, FNP_FALSE, 0}  /* Last entry should be dummy entry */
};

tNpMapTable gIssNpMapTable [FS_NP_ISS_MAX_NP] = 
{
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_EGRESS_STATUS                         */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_STATS_COLLECTION                      */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_MODE                                  */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_DUPLEX                                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_SPEED                                 */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_FLOW_CONTROL                          */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_RENEGOTIATE                           */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_MAX_MAC_ADDR                          */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_MAX_MAC_ACTION                        */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_MIRRORING_STATUS                      */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_MIRROR_TO_PORT                             */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_INGRESS_MIRRORING                          */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_EGRESS_MIRRORING                           */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_RATE_LIMITING_VALUE                        */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_EGRESS_PKT_RATE                       */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_EGRESS_PKT_RATE                       */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_RESTART_SYSTEM                                 */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_INIT_FILTER                                    */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_DUPLEX                                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_SPEED                                 */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_FLOW_CONTROL                          */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_UPDATE_L2_FILTER                               */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_UPDATE_L3_FILTER                               */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_UPDATE_L4_S_FILTER                             */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SEND_BUFFER_TO_LINUX                           */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_FAN_STATUS                                 */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_INIT_MIRR_DATA_BASE                            */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_MIRRORING                                  */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_MIRROR_ADD_REMOVE_PORT                         */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_MAC_LEARNING_RATE_LIMIT                    */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_LEARNED_MAC_ADDR_COUNT                     */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_UPDATE_USER_DEFINED_FILTER                     */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_LEARNING_MODE                              */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_MDI_OR_MDIX_CAP                       */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_GET_PORT_MDI_OR_MDIX_CAP                       */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_PORT_FLOW_CONTROL_RATE                     */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_CONFIG_PORT_ISOLATION_ENTRY                    */ 
  {0, FNP_TRUE, 0}, /*  NP_SET_TRACE                                          */ 
  {0, FNP_TRUE, 0}, /*  NP_GET_TRACE                                          */ 
#ifdef NPAPI_WANTED
  {1, FNP_TRUE, 0}, /*  NP_SET_TRACE_LEVEL                                    */ 
  {1, FNP_TRUE, 0}, /*  NP_GET_TRACE_LEVEL                                    */ 
#endif /* NPAPI_WANTED */
  {0, FNP_TRUE, 0}, /*  ISS_HW_SET_SET_TRAFFIC_SEPERATION_CTRL                */ 
  {0, FNP_TRUE, 0}, /*  ISS_HW_ENABLE_HW_CONSOLE                              */  
  {0, FNP_TRUE, 0}, /*  ISS_HW_CPU_MIRRORING                                */
  {0, FNP_FALSE, 0}, /*  ISS_MBSM_HW_SET_PORT_EGRESS_STATUS                    */ 
  {0, FNP_FALSE, 0}, /*  ISS_MBSM_HW_SET_PORT_STATS_COLLECTION                 */ 
  {0, FNP_FALSE, 0}, /*  ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS                 */ 
  {0, FNP_FALSE, 0}, /*  ISS_MBSM_HW_SET_MIRROR_TO_PORT                        */ 
  {0, FNP_TRUE, 0}, /*  ISS_MBSM_HW_UPDATE_L2_FILTER                          */ 
  {0, FNP_TRUE, 0}, /*  ISS_MBSM_HW_UPDATE_L3_FILTER                          */ 
  {0, FNP_FALSE, 0}, /*  ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY           */
  {0, FNP_FALSE, 0}, /*  ISS_MBSM_HW_CPU_MIRRORING                                */
  {0, FNP_TRUE, 0}, /* FS_ISS_HW_GET_CAPABILITIES                               */
  {0, FNP_TRUE, 0}, /* ISS_NP_HW_UPDATE_RESERV_FRAME_ACTION                     */
  {0, FNP_FALSE, 0} /* Last entry should be dummy entry */
};

tNpMapTable gIgsNpMapTable [FS_MI_IGS_MAX_NP] =
{
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES        */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING         */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING      */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING          */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING       */
  {1, FNP_TRUE, 0}, /* FS_MI_IGS_HW_ENHANCED_MODE                 */
  {1, FNP_TRUE, 0}, /* FS_MI_IGS_HW_PORT_RATE_LIMIT               */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_UPDATE_IPMC_ENTRY             */
  {0, FNP_TRUE, 0}, /* FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS      */
  {0, FNP_TRUE, 0}, /* FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES           */
  {1, FNP_TRUE, 0}, /* FS_MI_IGS_HW_ADD_RTR_PORT                  */
  {1, FNP_TRUE, 0}, /* FS_MI_IGS_HW_DEL_RTR_PORT                  */
  {1, FNP_TRUE, 0}, /* FS_MI_IGS_HW_GET_IP_FWD_ENTRY_HIT_BIT_STATUS  */
  {0, FNP_TRUE, 0}, /* FS_MI_IGS_HW_SPARSE_MODE                      */
#ifdef MBSM_WANTED
  {0, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING       */
  {0, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING    */
  {1, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_HW_ENHANCED_MODE              */
  {1, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT            */
  {0, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_HW_SPARSE_MODE                */
  {0, FNP_FALSE, 0}, /* FS_MI_IGS_MBSM_SYNC_I_P_M_C_IN                */
#endif /* MBSM_WANTED */
  {0, FNP_FALSE, 0} /* Last entry should be dummy entry              */
};

tNpMapTable gFsbNpMapTable [FS_NP_FSB_MAX_NP] =
{
  {0, FNP_TRUE, 0}, /* FS_MI_NP_FSB_HW_INIT                 */
  {0, FNP_TRUE, 0}, /* FS_FSB_HW_CREATE_FILTER              */
  {0, FNP_TRUE, 0}, /* FS_FSB_HW_DELETE_FILTER              */
  {0, FNP_TRUE, 0}, /* FS_MI_NP_FSB_HW_DE_INIT              */
  {0, FNP_TRUE, 0}, /* FS_FSB_HW_CREATE_S_CHANNEL_FILTER    */
  {0, FNP_TRUE, 0}, /* FS_FSB_HW_DELETE_S_CHANNEL_FILTER    */
#ifdef MBSM_WANTED
  {0, FNP_FALSE, 0}, /* FS_MI_NP_FSB_MBSM_HW_INIT           */
  {0, FNP_FALSE, 0}, /* FS_FSB_MBSM_HW_CREATE_FILTER        */
#endif /* MBSM_WANTED */
  {0, FNP_TRUE, 0}, /* FS_FSB_HW_SET_FCOE_PARAMS            */
  {0, FNP_FALSE, 0} /* Last entry should be dummy entry     */
};
tNpMapTable gVxlanNpMapTable [VXLAN_HW_MAX_NP] =
{
  {0, FNP_FALSE, 0}, /*  VXLAN_HW_CONFIGURE*/
  {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
    {0, FNP_FALSE, 0}, /*  VXLAN_HW_CLEAR*/
};
#else
extern tNpMapTable gLaNpMapTable [FS_LA_HW_MAX_NP ];
extern tNpMapTable gVlanNpMapTable[FS_MI_VLAN_HW_MAX_NP ]; 
extern tNpMapTable gCfaNpMapTable[FS_CFA_HW_MAX_NP ]; 
extern tNpMapTable gIssNpMapTable[FS_NP_ISS_MAX_NP ]; 
extern tNpMapTable gIpNpMapTable[FS_NP_IPV4_MAX_NP]; 
extern tNpMapTable gVcmNpMapTable [FS_NP_VCM_MAX_NP];
extern tNpMapTable gRstpNpMapTable [FS_NP_RSTP_MAX_NP];
extern tNpMapTable gMstpNpMapTable [FS_NP_MSTP_MAX_NP];
extern tNpMapTable gPvrStpNpMapTable[FS_NP_PVRST_MAX_NP];
extern tNpMapTable gQosNpMapTable [FS_QOS_HW_MAX_NP];
extern tNpMapTable gIgsNpMapTable [FS_MI_IGS_MAX_NP];
extern tNpMapTable gFsbNpMapTable [FS_NP_FSB_MAX_NP];
extern tNpMapTable gVxlanNpMapTable [VXLAN_HW_MAX_NP];

#endif /* _NPSTACKUTL_C_ */
#endif /* _NPBCMSUP_H_ */
