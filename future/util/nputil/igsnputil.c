/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnputil.c,v 1.10 2017/08/01 13:50:25 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __IGSNPUTIL_C__
#define __IGSNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskIgsNpPorts                             */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef IGS_WANTED
VOID
NpUtilMaskIgsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsRemoteNpModInfo *pIgsRemoteNpModInfo = NULL;
    UINT4               u4Port = 0;
#ifdef MBSM_WANTED
    UINT4               u4Index = 0;
    UINT4               u4RemoteStackPort = 0;
#endif
    INT4                i4StackPort = 0;
    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (i4StackPort);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    CfaGetLocalUnitPortInformation (&HwPortInfo);
    pIgsNpModInfo = &(pFsHwNp->IgsNpModInfo);
    pIgsRemoteNpModInfo = &(pRemoteHwNp->IgsRemoteNpModInfo);
    i4StackPort = NpUtilGetStackingPortIndex (&HwPortInfo, 0);
#ifdef MBSM_WANTED
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);
#endif

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENHANCED_MODE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        {
            tIgsNpWrFsMiIgsHwPortRateLimit *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwPortRateLimit;

            u4Port = pEntry->pIgsHwRateLmt->u4Port;
            break;
        }
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        {
            tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;
            pRemEntry =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwUpdateIpmcEntry;

            NpUtilIgsHwUpdateIpmcEntry (pEntry->pIgsHwIpFwdInfo,
                                        &pRemEntry->IgsHwIpFwdInfo,
                                        &HwPortInfo, pu1NpCallStatus);
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO:
        {
            tPortList          *pPortList = NULL;
            tPortList          *pUntagPortList = NULL;
            tPortList          *pRemotePortList = NULL;
            tPortList          *pRemoteUntagPortList = NULL;
            UINT4               u4LocalPortCount = 0;
            UINT4               u4RemotePortCount = 0;

            tIgsNpWrFsMiIgsMbsmSyncIPMCInfo *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsMbsmSyncIPMCInfo *pRemEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmSyncIPMCInfo;
            pRemEntry =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsMbsmSyncIPMCInfo;
            if (pEntry->pIgsIPMCInfoArray != NULL)
            {
                MEMCPY (&(pRemEntry->IgsIPMCInfoArray),
                        pEntry->pIgsIPMCInfoArray, sizeof (tIgsIPMCInfoArray));

                for (u4Index = 0;
                     u4Index <
                     (UINT4) pEntry->pIgsIPMCInfoArray->u4IgsHwIPMCCount;
                     u4Index++)
                {

                    pPortList =
                        (tPortList *) pEntry->pIgsIPMCInfoArray->
                        aIgsHwIpFwdInfo[u4Index].PortList;
                    pRemotePortList =
                        &pRemEntry->IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].
                        PortList;;

                    NpUtilMaskHwPortList ((UINT1 *) pPortList,
                                          (UINT1 *) pRemotePortList,
                                          &HwPortInfo,
                                          &u4LocalPortCount,
                                          &u4RemotePortCount,
                                          BRG_PORT_LIST_SIZE);
                    OSIX_BITLIST_SET_BIT ((*pRemotePortList), u4RemoteStackPort,
                                          BRG_MAX_PHY_PLUS_LOG_PORTS);

                    pUntagPortList =
                        (tPortList *) pEntry->pIgsIPMCInfoArray->
                        aIgsHwIpFwdInfo[u4Index].UntagPortList;
                    pRemoteUntagPortList =
                        &pRemEntry->IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].
                        UntagPortList;

                    NpUtilMaskHwPortList ((UINT1 *) pUntagPortList,
                                          (UINT1 *) pRemoteUntagPortList,
                                          &HwPortInfo,
                                          &u4LocalPortCount,
                                          &u4RemotePortCount,
                                          BRG_PORT_LIST_SIZE);
                    OSIX_BITLIST_SET_BIT ((*pRemoteUntagPortList),
                                          u4RemoteStackPort,
                                          BRG_MAX_PHY_PLUS_LOG_PORTS);
                }
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;

        }
#endif
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        {
            tPortList          *pPortList = NULL;
            tPortList          *pUntagPortList = NULL;
            tPortList          *pRemotePortList = NULL;
            tPortList          *pRemoteUntagPortList = NULL;
            UINT4               u4LocalPortCount = 0;
            UINT4               u4RemotePortCount = 0;
            tIgsNpWrFsMiNpUpdateIpmcFwdEntries *pEntry = NULL;
            tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiNpUpdateIpmcFwdEntries;
            pRemEntry =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpUpdateIpmcFwdEntries;

            pPortList = (tPortList *) pEntry->PortList;
            pRemotePortList = &pRemEntry->PortList;

            NpUtilMaskHwPortList ((UINT1 *) pPortList,
                                  (UINT1 *) pRemotePortList,
                                  &HwPortInfo,
                                  &u4LocalPortCount,
                                  &u4RemotePortCount, BRG_PORT_LIST_SIZE);

            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (u4LocalPortCount != 0)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
            else if (u4RemotePortCount != 0)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }

            pUntagPortList = (tPortList *) pEntry->UntagPortList;
            pRemoteUntagPortList = &pRemEntry->UntagPortList;

            NpUtilMaskHwPortList ((UINT1 *) pUntagPortList,
                                  (UINT1 *) pRemoteUntagPortList,
                                  &HwPortInfo,
                                  &u4LocalPortCount,
                                  &u4RemotePortCount, BRG_PORT_LIST_SIZE);

            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (u4LocalPortCount != 0)
            {
                if (*pu1NpCallStatus != NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                }
            }
            else if (u4RemotePortCount != 0)
            {
                if (*pu1NpCallStatus != NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                }
            }

            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING:
        {
            pRemoteHwNp->u4Opcode = FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING;
            tIgsNpWrFsMiIgsMbsmHwEnableIgmpSnooping *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIgmpSnooping;
            pRemEntry =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIgmpSnooping;

            pRemEntry->u4Instance = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            pRemoteHwNp->u4Opcode = FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING;
            tIgsNpWrFsMiIgsMbsmHwEnableIpIgmpSnooping *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIpIgmpSnooping;
            pRemEntry =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIpIgmpSnooping;

            pRemEntry->u4Instance = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_MI_IGS_MBSM_HW_ENHANCED_MODE:
        {
            pRemoteHwNp->u4Opcode = FS_MI_IGS_HW_ENHANCED_MODE;
            tIgsNpWrFsMiIgsMbsmHwEnhancedMode *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnhancedMode *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnhancedMode;
            pRemEntry = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnhancedMode;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u1EnhStatus = pEntry->u1EnhStatus;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT:
        {
            pRemoteHwNp->u4Opcode = FS_MI_IGS_HW_PORT_RATE_LIMIT;
            tIgsNpWrFsMiIgsMbsmHwPortRateLimit *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwPortRateLimit *pRemEntry = NULL;

            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwPortRateLimit;
            pRemEntry = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwPortRateLimit;

            pRemEntry->u4Instance = pEntry->u4Instance;
            MEMCPY (&(pRemEntry->IgsHwRateLmt), &(pEntry->pIgsHwRateLmt),
                    sizeof (tIgsHwRateLmt));
            pRemEntry->u1Action = pEntry->u1Action;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_MI_IGS_MBSM_HW_SPARSE_MODE:
        {
            tIgsNpWrFsMiIgsMbsmHwSparseMode *pEntry = NULL;
            tIgsRemoteNpWrFsMiIgsHwSparseMode *pRemEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwSparseMode;
            pRemEntry = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwSparseMode;
            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u1SparseStatus = pEntry->u1SparseStatus;
            pRemoteHwNp->u4Opcode = FS_MI_IGS_HW_SPARSE_MODE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
#endif /* MBSM_WANTED  */
        case FS_MI_IGS_HW_SPARSE_MODE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        default:
            break;
    }

    if (u4Port != 0)
    {
        if (NpUtilIsPortChannel (u4Port) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
        else if (NpUtilIsRemotePort (u4Port, &HwPortInfo) == FNP_SUCCESS)
        {
            /* This is for remote port 
             * set the NP Call status NPUTIL_INVOKE_REMOTE_NP
             * for executing this call at the remote unit
             */
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING) ||
        ((pFsHwNp->u4Opcode > FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_IGS_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgsConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgsConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsRemoteNpModInfo *pIgsRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pIgsNpModInfo = &(pFsHwNp->IgsNpModInfo);
    pIgsRemoteNpModInfo = &(pRemoteHwNp->IgsRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIgsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiIgsHwClearIpmcFwdEntries *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwClearIpmcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwClearIpmcFwdEntries;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwClearIpmcFwdEntries;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwDisableIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIgmpSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwDisableIpIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIpIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIpIgmpSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIgmpSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIpIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIpIgmpSnooping;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENHANCED_MODE:
        {
            tIgsNpWrFsMiIgsHwEnhancedMode *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnhancedMode *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnhancedMode;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnhancedMode;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1EnhStatus = pLocalArgs->u1EnhStatus;
            break;
        }
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        {
            tIgsNpWrFsMiIgsHwPortRateLimit *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwPortRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwPortRateLimit;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwPortRateLimit;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            if (pLocalArgs->pIgsHwRateLmt != NULL)
            {
                MEMCPY (&(pRemoteArgs->IgsHwRateLmt),
                        (pLocalArgs->pIgsHwRateLmt), sizeof (tIgsHwRateLmt));
            }
            pRemoteArgs->u1Action = pLocalArgs->u1Action;
            break;
        }
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        {
            tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwUpdateIpmcEntry;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            if (pLocalArgs->pIgsHwIpFwdInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->IgsHwIpFwdInfo),
                        (pLocalArgs->pIgsHwIpFwdInfo),
                        sizeof (tIgsHwIpFwdInfo));
            }
            pRemoteArgs->u1Action = pLocalArgs->u1Action;
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tIgsNpWrFsMiNpGetFwdEntryHitBitStatus *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiNpGetFwdEntryHitBitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiNpGetFwdEntryHitBitStatus;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpGetFwdEntryHitBitStatus;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tSnoopVlanId));
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            break;
        }
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiNpUpdateIpmcFwdEntries *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiNpUpdateIpmcFwdEntries;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpUpdateIpmcFwdEntries;
            pRemoteArgs->u4Instance = pLocalArgs->u4Instance;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tSnoopVlanId));
            pRemoteArgs->u4GrpAddr = pLocalArgs->u4GrpAddr;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->PortList), (pLocalArgs->PortList),
                        sizeof (tPortList));
            }
            if (pLocalArgs->UntagPortList != NULL)
            {
                MEMCPY (&(pRemoteArgs->UntagPortList),
                        (pLocalArgs->UntagPortList), sizeof (tPortList));
            }
            pRemoteArgs->u1EventType = pLocalArgs->u1EventType;
            break;
        }
        case FS_MI_IGS_HW_SPARSE_MODE:
        {
            tIgsNpWrFsMiIgsHwSparseMode *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwSparseMode *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwSparseMode;
            pRemoteArgs = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwSparseMode;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1SparseStatus = pLocalArgs->u1SparseStatus;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING:
        case FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_MBSM_HW_ENHANCED_MODE:
        case FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT:
        case FS_MI_IGS_MBSM_HW_SPARSE_MODE:
        case FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgsConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgsConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsRemoteNpModInfo *pIgsRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pIgsNpModInfo = &(pFsHwNp->IgsNpModInfo);
    pIgsRemoteNpModInfo = &(pRemoteHwNp->IgsRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiIgsHwClearIpmcFwdEntries *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwClearIpmcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwClearIpmcFwdEntries;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwClearIpmcFwdEntries;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwDisableIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIgmpSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwDisableIpIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIpIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIpIgmpSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIgmpSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIpIgmpSnooping;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIpIgmpSnooping;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            break;
        }
        case FS_MI_IGS_HW_ENHANCED_MODE:
        {
            tIgsNpWrFsMiIgsHwEnhancedMode *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwEnhancedMode *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwEnhancedMode;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnhancedMode;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u1EnhStatus = pRemoteArgs->u1EnhStatus;
            break;
        }
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        {
            tIgsNpWrFsMiIgsHwPortRateLimit *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwPortRateLimit *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwPortRateLimit;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwPortRateLimit;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            if (pLocalArgs->pIgsHwRateLmt != NULL)
            {
                MEMCPY ((pLocalArgs->pIgsHwRateLmt),
                        &(pRemoteArgs->IgsHwRateLmt), sizeof (tIgsHwRateLmt));
            }
            pLocalArgs->u1Action = pRemoteArgs->u1Action;
            break;
        }
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        {
            tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwUpdateIpmcEntry;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            if (pLocalArgs->pIgsHwIpFwdInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pIgsHwIpFwdInfo),
                        &(pRemoteArgs->IgsHwIpFwdInfo),
                        sizeof (tIgsHwIpFwdInfo));
            }
            pLocalArgs->u1Action = pRemoteArgs->u1Action;
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tIgsNpWrFsMiNpGetFwdEntryHitBitStatus *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiNpGetFwdEntryHitBitStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiNpGetFwdEntryHitBitStatus;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpGetFwdEntryHitBitStatus;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tSnoopVlanId));
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4SrcAddr = pRemoteArgs->u4SrcAddr;
            break;
        }
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiNpUpdateIpmcFwdEntries *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiNpUpdateIpmcFwdEntries;
            pRemoteArgs =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpUpdateIpmcFwdEntries;
            pLocalArgs->u4Instance = pRemoteArgs->u4Instance;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tSnoopVlanId));
            pLocalArgs->u4GrpAddr = pRemoteArgs->u4GrpAddr;
            pLocalArgs->u4SrcAddr = pRemoteArgs->u4SrcAddr;
            if (pLocalArgs->PortList != NULL)
            {
                MEMCPY ((pLocalArgs->PortList), &(pRemoteArgs->PortList),
                        sizeof (tPortList));
            }
            if (pLocalArgs->UntagPortList != NULL)
            {
                MEMCPY ((pLocalArgs->UntagPortList),
                        &(pRemoteArgs->UntagPortList), sizeof (tPortList));
            }
            pLocalArgs->u1EventType = pRemoteArgs->u1EventType;
            break;
        }
        case FS_MI_IGS_HW_SPARSE_MODE:
        {
            tIgsNpWrFsMiIgsHwSparseMode *pLocalArgs = NULL;
            tIgsRemoteNpWrFsMiIgsHwSparseMode *pRemoteArgs = NULL;
            pLocalArgs = &pIgsNpModInfo->IgsNpFsMiIgsHwSparseMode;
            pRemoteArgs = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwSparseMode;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u1SparseStatus = pRemoteArgs->u1SparseStatus;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING:
        case FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_MBSM_HW_ENHANCED_MODE:
        case FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT:
        case FS_MI_IGS_MBSM_HW_SPARSE_MODE:
        case FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO:

        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIgsRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIgsNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIgsRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                             tRemoteHwNp * pRemoteHwNpOutput)
{
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
#ifdef MBSM_WANTED
    UINT4               u4Index = 0;
#endif
    tIgsRemoteNpModInfo *pIgsRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIgsRemoteNpModInfo = &(pRemoteHwNpInput->IgsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        {
            tIgsRemoteNpWrFsMiIgsHwClearIpmcFwdEntries *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwClearIpmcFwdEntries;
            FsMiIgsHwClearIpmcFwdEntries (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        {
            tIgsRemoteNpWrFsMiIgsHwDisableIgmpSnooping *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIgmpSnooping;
            i4RetVal = FsMiIgsHwDisableIgmpSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        {
            tIgsRemoteNpWrFsMiIgsHwDisableIpIgmpSnooping *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwDisableIpIgmpSnooping;
            i4RetVal = FsMiIgsHwDisableIpIgmpSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        {
            tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIgmpSnooping;
            i4RetVal = FsMiIgsHwEnableIgmpSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnableIpIgmpSnooping;
            i4RetVal = FsMiIgsHwEnableIpIgmpSnooping (pInput->u4Instance);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_ENHANCED_MODE:
        {
            tIgsRemoteNpWrFsMiIgsHwEnhancedMode *pInput = NULL;
            pInput = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwEnhancedMode;
            i4RetVal =
                FsMiIgsHwEnhancedMode (pInput->u4ContextId,
                                       pInput->u1EnhStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        {
            tIgsRemoteNpWrFsMiIgsHwPortRateLimit *pInput = NULL;
            pInput = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwPortRateLimit;
            i4RetVal =
                FsMiIgsHwPortRateLimit (pInput->u4Instance,
                                        &(pInput->IgsHwRateLmt),
                                        pInput->u1Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        {
            tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry *pInput = NULL;
            pInput = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwUpdateIpmcEntry;
            i4RetVal =
                FsMiIgsHwUpdateIpmcEntry (pInput->u4Instance,
                                          &(pInput->IgsHwIpFwdInfo),
                                          pInput->u1Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tIgsRemoteNpWrFsMiNpGetFwdEntryHitBitStatus *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpGetFwdEntryHitBitStatus;
            i4RetVal =
                FsMiNpGetFwdEntryHitBitStatus (pInput->u4Instance,
                                               pInput->VlanId,
                                               pInput->u4GrpAddr,
                                               pInput->u4SrcAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        {
            tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries *pInput = NULL;
            pInput =
                &pIgsRemoteNpModInfo->IgsRemoteNpFsMiNpUpdateIpmcFwdEntries;
            i4RetVal =
                FsMiNpUpdateIpmcFwdEntries (pInput->u4Instance, pInput->VlanId,
                                            pInput->u4GrpAddr,
                                            pInput->u4SrcAddr, pInput->PortList,
                                            pInput->UntagPortList,
                                            pInput->u1EventType);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MI_IGS_HW_SPARSE_MODE:
        {
            tIgsRemoteNpWrFsMiIgsHwSparseMode *pInput = NULL;
            pInput = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsHwSparseMode;
            i4RetVal =
                FsMiIgsHwSparseMode (pInput->u4ContextId,
                                     pInput->u1SparseStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO:
        {
            tIgsRemoteNpWrFsMiIgsMbsmSyncIPMCInfo *pInput = NULL;
            pInput = &pIgsRemoteNpModInfo->IgsRemoteNpFsMiIgsMbsmSyncIPMCInfo;
            for (u4Index = 0;
                 u4Index < (UINT4) pInput->IgsIPMCInfoArray.u4IgsHwIPMCCount;
                 u4Index++)
            {
                i4RetVal =
                    FsMiNpUpdateIpmcFwdEntries (pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                u4Instance,
                                                pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                OuterVlanId,
                                                pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                u4GrpAddr,
                                                pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                u4SrcAddr,
                                                pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                PortList,
                                                pInput->IgsIPMCInfoArray.
                                                aIgsHwIpFwdInfo[u4Index].
                                                UntagPortList,
                                                SNOOP_HW_CREATE_ENTRY);
                if (i4RetVal == FNP_FAILURE)
                {
                    break;
                }
            }
            break;
        }
#endif

        default:
            i4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return ((UINT1) i4RetVal);
}

/***************************************************************************/
/*  Function Name       : NpUtilIgsMergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                        */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp                */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value          */
/*                    pi4RemoteNpRetVal - Remote Np Return Value           */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilIgsMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                   tRemoteHwNp * pRemoteHwNp,
                                   INT4 *pi4LocalNpRetVal,
                                   INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        case FS_MI_IGS_HW_ENHANCED_MODE:
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        case FS_MI_IGS_HW_ADD_RTR_PORT:
        case FS_MI_IGS_HW_DEL_RTR_PORT:
        {
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            if ((*pi4LocalNpRetVal == FNP_SUCCESS)
                || (*pi4RemoteNpRetVal == FNP_SUCCESS))
            {
                *pi4LocalNpRetVal = FNP_SUCCESS;
                *pi4RemoteNpRetVal = FNP_SUCCESS;
            }
            else
            {
                *pi4LocalNpRetVal = FNP_FAILURE;
                *pi4RemoteNpRetVal = FNP_FAILURE;
            }
            break;
        }
        default:
            break;

    }
    UNUSED_PARAM (pRemoteHwNp);
    return u1RetVal;
}
#endif /* IGS_WANTED */

#endif /* __IGSNPUTIL_C__ */
