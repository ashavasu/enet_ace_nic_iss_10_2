/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnputil.c,v 1.4 2014/03/01 10:58:18 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __EOAMNPUTIL_C__
#define __EOAMNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskEoamNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef EOAM_WANTED
VOID
NpUtilMaskEoamNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case EOAM_NP_DE_INIT:
        case EOAM_NP_INIT:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case EOAM_LM_NP_GET_STAT:
        case EOAM_LM_NP_GET_STAT64:
        case EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY:
        case EOAM_NP_HANDLE_LOCAL_LOOP_BACK:
        case EOAM_NP_HANDLE_REMOTE_LOOP_BACK:
        {
            if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex,
                                    &HwPortInfo) == FNP_SUCCESS)
            {
                /* This is for remote port 
                 * set the NP Call status NPUTIL_INVOKE_REMOTE_NP
                 * for executing this call at the remote unit
                 */
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
#ifdef MBSM_WANTED
        case EOAM_MBSM_HW_INIT:
        {
            pRemoteHwNp->u4Opcode = EOAM_NP_INIT;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamRemoteNpModInfo *pEoamRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pEoamNpModInfo = &(pFsHwNp->EoamNpModInfo);
    pEoamRemoteNpModInfo = &(pRemoteHwNp->EoamRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tEoamRemoteNpModInfo);

    switch (u4Opcode)
    {
        case EOAM_LM_NP_GET_STAT:
        {
            tEoamNpWrEoamLmNpGetStat *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamLmNpGetStat *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamLmNpGetStat;
            pRemoteArgs = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1StatType = pLocalArgs->u1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4Value), (pLocalArgs->pu4Value),
                        sizeof (UINT4));
            }
            break;
        }
        case EOAM_LM_NP_GET_STAT64:
        {
            tEoamNpWrEoamLmNpGetStat64 *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamLmNpGetStat64 *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamLmNpGetStat64;
            pRemoteArgs = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat64;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1StatType = pLocalArgs->u1StatType;
            if (pLocalArgs->pu8Value != NULL)
            {
                MEMCPY (&(pRemoteArgs->U8Value), (pLocalArgs->pu8Value),
                        sizeof (FS_UINT8));
            }
            break;
        }
        case EOAM_NP_DE_INIT:
        {
            break;
        }
        case EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY:
        {
            tEoamNpWrEoamNpGetUniDirectionalCapability *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpGetUniDirectionalCapability *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pEoamNpModInfo->EoamNpEoamNpGetUniDirectionalCapability;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->
                EoamRemoteNpEoamNpGetUniDirectionalCapability;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu1Capability != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1Capability),
                        (pLocalArgs->pu1Capability), sizeof (UINT1));
            }
            break;
        }
        case EOAM_NP_HANDLE_LOCAL_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleLocalLoopBack *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpHandleLocalLoopBack *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamNpHandleLocalLoopBack;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleLocalLoopBack;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->SrcMac != NULL)
            {
                MEMCPY (&(pRemoteArgs->SrcMac), (pLocalArgs->SrcMac),
                        sizeof (tMacAddr));
            }
            if (pLocalArgs->DestMac != NULL)
            {
                MEMCPY (&(pRemoteArgs->DestMac), (pLocalArgs->DestMac),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case EOAM_NP_HANDLE_REMOTE_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleRemoteLoopBack *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpHandleRemoteLoopBack *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamNpHandleRemoteLoopBack;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleRemoteLoopBack;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->SrcMac != NULL)
            {
                MEMCPY (&(pRemoteArgs->SrcMac), (pLocalArgs->SrcMac),
                        sizeof (tMacAddr));
            }
            if (pLocalArgs->PeerMac != NULL)
            {
                MEMCPY (&(pRemoteArgs->PeerMac), (pLocalArgs->PeerMac),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case EOAM_NP_INIT:
        {
            break;
        }
#ifdef MBSM_WANTED
        case EOAM_MBSM_HW_INIT:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamRemoteNpModInfo *pEoamRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pEoamNpModInfo = &(pFsHwNp->EoamNpModInfo);
    pEoamRemoteNpModInfo = &(pRemoteHwNp->EoamRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case EOAM_LM_NP_GET_STAT:
        {
            tEoamNpWrEoamLmNpGetStat *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamLmNpGetStat *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamLmNpGetStat;
            pRemoteArgs = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1StatType = pRemoteArgs->u1StatType;
            if (pLocalArgs->pu4Value != NULL)
            {
                MEMCPY ((pLocalArgs->pu4Value), &(pRemoteArgs->u4Value),
                        sizeof (UINT4));
            }
            break;
        }
        case EOAM_LM_NP_GET_STAT64:
        {
            tEoamNpWrEoamLmNpGetStat64 *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamLmNpGetStat64 *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamLmNpGetStat64;
            pRemoteArgs = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat64;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1StatType = pRemoteArgs->u1StatType;
            if (pLocalArgs->pu8Value != NULL)
            {
                MEMCPY ((pLocalArgs->pu8Value), &(pRemoteArgs->U8Value),
                        sizeof (FS_UINT8));
            }
            break;
        }
        case EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY:
        {
            tEoamNpWrEoamNpGetUniDirectionalCapability *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpGetUniDirectionalCapability *pRemoteArgs =
                NULL;
            pLocalArgs =
                &pEoamNpModInfo->EoamNpEoamNpGetUniDirectionalCapability;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->
                EoamRemoteNpEoamNpGetUniDirectionalCapability;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->pu1Capability != NULL)
            {
                MEMCPY ((pLocalArgs->pu1Capability),
                        &(pRemoteArgs->u1Capability), sizeof (UINT1));
            }
            break;
        }
        case EOAM_NP_HANDLE_LOCAL_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleLocalLoopBack *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpHandleLocalLoopBack *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamNpHandleLocalLoopBack;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleLocalLoopBack;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->SrcMac != NULL)
            {
                MEMCPY ((pLocalArgs->SrcMac), &(pRemoteArgs->SrcMac),
                        sizeof (tMacAddr));
            }
            if (pLocalArgs->DestMac != NULL)
            {
                MEMCPY ((pLocalArgs->DestMac), &(pRemoteArgs->DestMac),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case EOAM_NP_HANDLE_REMOTE_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleRemoteLoopBack *pLocalArgs = NULL;
            tEoamRemoteNpWrEoamNpHandleRemoteLoopBack *pRemoteArgs = NULL;
            pLocalArgs = &pEoamNpModInfo->EoamNpEoamNpHandleRemoteLoopBack;
            pRemoteArgs =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleRemoteLoopBack;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            if (pLocalArgs->SrcMac != NULL)
            {
                MEMCPY ((pLocalArgs->SrcMac), &(pRemoteArgs->SrcMac),
                        sizeof (tMacAddr));
            }
            if (pLocalArgs->PeerMac != NULL)
            {
                MEMCPY ((pLocalArgs->PeerMac), &(pRemoteArgs->PeerMac),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
#ifdef MBSM_WANTED
        case EOAM_MBSM_HW_INIT:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilEoamRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEoamNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilEoamRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamRemoteNpModInfo *pEoamRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pEoamRemoteNpModInfo = &(pRemoteHwNpInput->EoamRemoteNpModInfo);

    switch (u4Opcode)
    {
        case EOAM_LM_NP_GET_STAT:
        {
            tEoamRemoteNpWrEoamLmNpGetStat *pInput = NULL;
            pInput = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat;
            u1RetVal =
                EoamLmNpGetStat (pInput->u4IfIndex, pInput->u1StatType,
                                 &(pInput->u4Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_LM_NP_GET_STAT64:
        {
            tEoamRemoteNpWrEoamLmNpGetStat64 *pInput = NULL;
            pInput = &pEoamRemoteNpModInfo->EoamRemoteNpEoamLmNpGetStat64;
            u1RetVal =
                EoamLmNpGetStat64 (pInput->u4IfIndex, pInput->u1StatType,
                                   &(pInput->U8Value));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_NP_DE_INIT:
        {
            u1RetVal = EoamNpDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY:
        {
            tEoamRemoteNpWrEoamNpGetUniDirectionalCapability *pInput = NULL;
            pInput =
                &pEoamRemoteNpModInfo->
                EoamRemoteNpEoamNpGetUniDirectionalCapability;
            u1RetVal =
                EoamNpGetUniDirectionalCapability (pInput->u4IfIndex,
                                                   &(pInput->u1Capability));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_NP_HANDLE_LOCAL_LOOP_BACK:
        {
            tEoamRemoteNpWrEoamNpHandleLocalLoopBack *pInput = NULL;
            pInput =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleLocalLoopBack;
            u1RetVal =
                EoamNpHandleLocalLoopBack (pInput->u4IfIndex, pInput->SrcMac,
                                           pInput->DestMac, pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_NP_HANDLE_REMOTE_LOOP_BACK:
        {
            tEoamRemoteNpWrEoamNpHandleRemoteLoopBack *pInput = NULL;
            pInput =
                &pEoamRemoteNpModInfo->EoamRemoteNpEoamNpHandleRemoteLoopBack;
            u1RetVal =
                EoamNpHandleRemoteLoopBack (pInput->u4IfIndex, pInput->SrcMac,
                                            pInput->PeerMac, pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case EOAM_NP_INIT:
        {
            u1RetVal = EoamNpInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* EOAM_FM_WANTED */

#endif /* __EOAMNPUTIL_C__ */
