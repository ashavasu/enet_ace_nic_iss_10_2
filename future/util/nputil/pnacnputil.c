/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacnputil.c,v 1.7 2014/10/08 11:01:24 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __PNACNPUTIL_C__
#define __PNACNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskPnacNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef PNAC_WANTED
VOID
NpUtilMaskPnacNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacRemoteNpModInfo *pPnacRemoteNpModInfo = NULL;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    BOOL1               bIsCentProtOpCode = FNP_FALSE;

    UINT4               u4Port = 0;
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    pPnacNpModInfo = &(pFsHwNp->PnacNpModInfo);
    pPnacRemoteNpModInfo = &(pRemoteHwNp->PnacRemoteNpModInfo);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    switch (pFsHwNp->u4Opcode)
    {
#ifdef L2RED_WANTED
        case FS_PNAC_RED_HW_UPDATE_D_B:
        {
            tPnacNpWrFsPnacRedHwUpdateDB *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpFsPnacRedHwUpdateDB;
            u4Port = pEntry->u4Port;

            break;
        }
#endif /* L2RED_WANTED */

        case PNAC_HW_ADD_OR_DEL_MAC_SESS:
        {
            tPnacNpWrPnacHwAddOrDelMacSess *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwAddOrDelMacSess;
            u4Port = pEntry->u4Port;

            break;
        }
        case PNAC_HW_DISABLE:
        case PNAC_HW_ENABLE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case PNAC_HW_GET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwGetAuthStatus *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwGetAuthStatus;
            u4Port = pEntry->u4PortNum;

            break;
        }
        case PNAC_HW_GET_SESSION_COUNTER:
        {
            tPnacNpWrPnacHwGetSessionCounter *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwGetSessionCounter;
            u4Port = pEntry->u4Port;

            break;
        }
        case PNAC_HW_SET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwSetAuthStatus *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwSetAuthStatus;
            u4Port = pEntry->u4Port;

            bIsCentProtOpCode = FNP_TRUE;

            break;
        }
        case PNAC_HW_START_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStartSessionCounters *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwStartSessionCounters;
            u4Port = pEntry->u4Port;

            break;
        }
        case PNAC_HW_STOP_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStopSessionCounters *pEntry = NULL;
            pEntry = &pPnacNpModInfo->PnacNpPnacHwStopSessionCounters;
            u4Port = pEntry->u4Port;

            break;
        }
#ifdef MBSM_WANTED
        case PNAC_MBSM_HW_ENABLE:
        {
            pRemoteHwNp->u4Opcode = PNAC_HW_ENABLE;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case PNAC_MBSM_HW_SET_AUTH_STATUS:
        {
            pRemoteHwNp->u4Opcode = PNAC_HW_SET_AUTH_STATUS;
            tPnacNpWrPnacMbsmHwSetAuthStatus *pEntry = NULL;
            tPnacRemoteNpWrPnacHwSetAuthStatus *pRemEntry = NULL;

            pEntry = &pPnacNpModInfo->PnacNpPnacMbsmHwSetAuthStatus;
            pRemEntry = &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwSetAuthStatus;

            pRemEntry->u4Port = pEntry->u4Port;
            if (pEntry->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemEntry->u1SuppAddr),
                        (pEntry->pu1SuppAddr), sizeof (tMacAddr));
            }
            pRemEntry->u1AuthMode = pEntry->u1AuthMode;
            pRemEntry->u1AuthStatus = pEntry->u1AuthStatus;
            pRemEntry->u1CtrlDir = pEntry->u1CtrlDir;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
#else
            UNUSED_PARAM (pPnacRemoteNpModInfo);
#endif /* MBSM_WANTED  */

        default:
            break;
    }

    if (u4Port != 0)
    {
        if (NpUtilIsRemotePort (u4Port, &HwPortInfo) == FNP_SUCCESS)
        {
            /* This is for remote port 
             * set the NP Call status NPUTIL_INVOKE_REMOTE_NP
             * for executing this call at the remote unit
             */
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
    }

    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * PNAC protocol will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if ((u4StackingModel == ISS_DISS_STACKING_MODEL) &&
        (bIsCentProtOpCode != FNP_TRUE))
    {
        if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            /* don't do anything */
        }
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < PNAC_MBSM_HW_ENABLE) ||
        ((pFsHwNp->u4Opcode > PNAC_MBSM_HW_SET_AUTH_STATUS)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_PNAC_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPnacConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPnacConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacRemoteNpModInfo *pPnacRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pPnacNpModInfo = &(pFsHwNp->PnacNpModInfo);
    pPnacRemoteNpModInfo = &(pRemoteHwNp->PnacRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tPnacRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case FS_PNAC_RED_HW_UPDATE_D_B:
        {
            tPnacNpWrFsPnacRedHwUpdateDB *pLocalArgs = NULL;
            tPnacRemoteNpWrFsPnacRedHwUpdateDB *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpFsPnacRedHwUpdateDB;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpFsPnacRedHwUpdateDB;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1AuthMode = pLocalArgs->u1AuthMode;
            pRemoteArgs->u1AuthStatus = pLocalArgs->u1AuthStatus;
            pRemoteArgs->u1CtrlDir = pLocalArgs->u1CtrlDir;
            break;
        }
#endif /* L2RED_WANTED */
        case PNAC_HW_ADD_OR_DEL_MAC_SESS:
        {
            tPnacNpWrPnacHwAddOrDelMacSess *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwAddOrDelMacSess *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwAddOrDelMacSess;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwAddOrDelMacSess;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1SessStatus = pLocalArgs->u1SessStatus;
            break;
        }
        case PNAC_HW_DISABLE:
        {
            break;
        }
        case PNAC_HW_ENABLE:
        {
            break;
        }
        case PNAC_HW_GET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwGetAuthStatus *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwGetAuthStatus *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwGetAuthStatus;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetAuthStatus;
            pRemoteArgs->u4PortNum = pLocalArgs->u4PortNum;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1AuthMode = pLocalArgs->u1AuthMode;
            MEMCPY (&(pRemoteArgs->u2AuthStatus),
                    (pLocalArgs->pu2AuthStatus), sizeof (UINT2));
            MEMCPY (&(pRemoteArgs->u2CtrlDir), (pLocalArgs->pu2CtrlDir),
                    sizeof (UINT2));
            break;
        }
        case PNAC_HW_GET_SESSION_COUNTER:
        {
            tPnacNpWrPnacHwGetSessionCounter *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwGetSessionCounter *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwGetSessionCounter;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetSessionCounter;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1CounterType = pLocalArgs->u1CounterType;
            if (pLocalArgs->pu4HiCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4HiCounter), (pLocalArgs->pu4HiCounter),
                        sizeof (UINT4));
            }
            if (pLocalArgs->pu4LoCounter != NULL)
            {
                MEMCPY (&(pRemoteArgs->u4LoCounter), (pLocalArgs->pu4LoCounter),
                        sizeof (UINT4));
            }
            break;
        }
        case PNAC_HW_SET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwSetAuthStatus *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwSetAuthStatus *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwSetAuthStatus;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwSetAuthStatus;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1AuthMode = pLocalArgs->u1AuthMode;
            pRemoteArgs->u1AuthStatus = pLocalArgs->u1AuthStatus;
            pRemoteArgs->u1CtrlDir = pLocalArgs->u1CtrlDir;
            break;
        }
        case PNAC_HW_START_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStartSessionCounters *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwStartSessionCounters *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwStartSessionCounters;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStartSessionCounters;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            break;
        }
        case PNAC_HW_STOP_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStopSessionCounters *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwStopSessionCounters *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwStopSessionCounters;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStopSessionCounters;
            pRemoteArgs->u4Port = pLocalArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1SuppAddr), (pLocalArgs->pu1SuppAddr),
                        sizeof (tMacAddr));
            }
            break;
        }
#ifdef MBSM_WANTED
        case PNAC_MBSM_HW_ENABLE:
        case PNAC_MBSM_HW_SET_AUTH_STATUS:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPnacConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPnacConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPnacNpModInfo     *pPnacNpModInfo = NULL;
    tPnacRemoteNpModInfo *pPnacRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pPnacNpModInfo = &(pFsHwNp->PnacNpModInfo);
    pPnacRemoteNpModInfo = &(pRemoteHwNp->PnacRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case FS_PNAC_RED_HW_UPDATE_D_B:
        {
            tPnacNpWrFsPnacRedHwUpdateDB *pLocalArgs = NULL;
            tPnacRemoteNpWrFsPnacRedHwUpdateDB *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpFsPnacRedHwUpdateDB;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpFsPnacRedHwUpdateDB;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1AuthMode = pRemoteArgs->u1AuthMode;
            pLocalArgs->u1AuthStatus = pRemoteArgs->u1AuthStatus;
            pLocalArgs->u1CtrlDir = pRemoteArgs->u1CtrlDir;
            break;
        }
#endif /* L2RED_WANTED */
        case PNAC_HW_ADD_OR_DEL_MAC_SESS:
        {
            tPnacNpWrPnacHwAddOrDelMacSess *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwAddOrDelMacSess *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwAddOrDelMacSess;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwAddOrDelMacSess;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1SessStatus = pRemoteArgs->u1SessStatus;
            break;
        }
        case PNAC_HW_GET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwGetAuthStatus *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwGetAuthStatus *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwGetAuthStatus;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetAuthStatus;
            pLocalArgs->u4PortNum = pRemoteArgs->u4PortNum;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1AuthMode = pRemoteArgs->u1AuthMode;
            MEMCPY ((pLocalArgs->pu2AuthStatus),
                    &(pRemoteArgs->u2AuthStatus), sizeof (UINT2));
            MEMCPY ((pLocalArgs->pu2CtrlDir), &(pRemoteArgs->u2CtrlDir),
                    sizeof (UINT2));
            break;
        }
        case PNAC_HW_GET_SESSION_COUNTER:
        {
            tPnacNpWrPnacHwGetSessionCounter *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwGetSessionCounter *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwGetSessionCounter;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetSessionCounter;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1CounterType = pRemoteArgs->u1CounterType;
            if (pLocalArgs->pu4HiCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu4HiCounter), &(pRemoteArgs->u4HiCounter),
                        sizeof (UINT4));
            }
            if (pLocalArgs->pu4LoCounter != NULL)
            {
                MEMCPY ((pLocalArgs->pu4LoCounter), &(pRemoteArgs->u4LoCounter),
                        sizeof (UINT4));
            }
            break;
        }
        case PNAC_HW_SET_AUTH_STATUS:
        {
            tPnacNpWrPnacHwSetAuthStatus *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwSetAuthStatus *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwSetAuthStatus;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwSetAuthStatus;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            pLocalArgs->u1AuthMode = pRemoteArgs->u1AuthMode;
            pLocalArgs->u1AuthStatus = pRemoteArgs->u1AuthStatus;
            pLocalArgs->u1CtrlDir = pRemoteArgs->u1CtrlDir;
            break;
        }
        case PNAC_HW_START_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStartSessionCounters *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwStartSessionCounters *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwStartSessionCounters;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStartSessionCounters;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            break;
        }
        case PNAC_HW_STOP_SESSION_COUNTERS:
        {
            tPnacNpWrPnacHwStopSessionCounters *pLocalArgs = NULL;
            tPnacRemoteNpWrPnacHwStopSessionCounters *pRemoteArgs = NULL;
            pLocalArgs = &pPnacNpModInfo->PnacNpPnacHwStopSessionCounters;
            pRemoteArgs =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStopSessionCounters;
            pLocalArgs->u4Port = pRemoteArgs->u4Port;
            if (pLocalArgs->pu1SuppAddr != NULL)
            {
                MEMCPY ((pLocalArgs->pu1SuppAddr), &(pRemoteArgs->u1SuppAddr),
                        sizeof (tMacAddr));
            }
            break;
        }
#ifdef MBSM_WANTED
        case PNAC_MBSM_HW_ENABLE:
        case PNAC_MBSM_HW_SET_AUTH_STATUS:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilPnacRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tPnacNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilPnacRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tPnacRemoteNpModInfo *pPnacRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pPnacRemoteNpModInfo = &(pRemoteHwNpInput->PnacRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef L2RED_WANTED
        case FS_PNAC_RED_HW_UPDATE_D_B:
        {
            tPnacRemoteNpWrFsPnacRedHwUpdateDB *pInput = NULL;
            pInput = &pPnacRemoteNpModInfo->PnacRemoteNpFsPnacRedHwUpdateDB;
            u1RetVal =
                FsPnacRedHwUpdateDB ((UINT2) pInput->u4Port,
                                     (UINT1 *) &(pInput->u1SuppAddr),
                                     pInput->u1AuthMode, pInput->u1AuthStatus,
                                     pInput->u1CtrlDir);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* L2RED_WANTED */
        case PNAC_HW_ADD_OR_DEL_MAC_SESS:
        {
            tPnacRemoteNpWrPnacHwAddOrDelMacSess *pInput = NULL;
            pInput = &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwAddOrDelMacSess;
            u1RetVal =
                PnacHwAddOrDelMacSess ((UINT2) pInput->u4Port,
                                       (UINT1 *) &(pInput->u1SuppAddr),
                                       pInput->u1SessStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_DISABLE:
        {
            u1RetVal = PnacHwDisable ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_ENABLE:
        {
            u1RetVal = PnacHwEnable ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_GET_AUTH_STATUS:
        {
            tPnacRemoteNpWrPnacHwGetAuthStatus *pInput = NULL;
            pInput = &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetAuthStatus;
            u1RetVal =
                PnacHwGetAuthStatus ((UINT2) pInput->u4PortNum,
                                     (UINT1 *) &(pInput->u1SuppAddr),
                                     pInput->u1AuthMode,
                                     &(pInput->u2AuthStatus),
                                     &(pInput->u2CtrlDir));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_GET_SESSION_COUNTER:
        {
            tPnacRemoteNpWrPnacHwGetSessionCounter *pInput = NULL;
            pInput = &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwGetSessionCounter;
            u1RetVal =
                PnacHwGetSessionCounter ((UINT2) pInput->u4Port,
                                         (UINT1 *) &(pInput->u1SuppAddr),
                                         pInput->u1CounterType,
                                         &(pInput->u4HiCounter),
                                         &(pInput->u4LoCounter));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_SET_AUTH_STATUS:
        {
            tPnacRemoteNpWrPnacHwSetAuthStatus *pInput = NULL;
            pInput = &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwSetAuthStatus;
            u1RetVal =
                PnacHwSetAuthStatus ((UINT2) pInput->u4Port,
                                     (UINT1 *) &(pInput->u1SuppAddr),
                                     pInput->u1AuthMode, pInput->u1AuthStatus,
                                     pInput->u1CtrlDir);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_START_SESSION_COUNTERS:
        {
            tPnacRemoteNpWrPnacHwStartSessionCounters *pInput = NULL;
            pInput =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStartSessionCounters;
            u1RetVal =
                PnacHwStartSessionCounters ((UINT2) pInput->u4Port,
                                            (UINT1 *) &(pInput->u1SuppAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case PNAC_HW_STOP_SESSION_COUNTERS:
        {
            tPnacRemoteNpWrPnacHwStopSessionCounters *pInput = NULL;
            pInput =
                &pPnacRemoteNpModInfo->PnacRemoteNpPnacHwStopSessionCounters;
            u1RetVal =
                PnacHwStopSessionCounters ((UINT2) pInput->u4Port,
                                           (UINT1 *) &(pInput->u1SuppAddr));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* PNAC_WANTED */

#endif /* __PNACNPUTIL_C__ */
