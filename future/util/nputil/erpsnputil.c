
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsnputil.c,v 1.3 2014/10/08 11:01:24 siva Exp $
 *
 * Description:This file contains the single NP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __ERPS_NPUTIL_C__
#define __ERPS_NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilErpsConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilErpsConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    tErpsNpModInfo     *pErpsNpModInfo = NULL;
    tErpsRemoteNpModInfo *pErpsRemoteNpModInfo = NULL;
    UINT4               u4Opcode = 0;
    UINT1               u1RetVal = FNP_SUCCESS;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pErpsNpModInfo = &(pFsHwNp->ErpsNpModInfo);
    pErpsRemoteNpModInfo = &(pRemoteHwNp->ErpsRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tErpsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_ERPS_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsHwRingConfig *pLocalArgs = NULL;
            tErpsRemoteNpWrFsErpsHwRingConfig *pRemoteArgs = NULL;

            pLocalArgs = &pErpsNpModInfo->ErpsNpFsErpsHwRingConfig;
            pRemoteArgs = &pErpsRemoteNpModInfo->ErpsRemoteNpFsErpsHwRingConfig;
            MEMCPY (&(pRemoteArgs->ErpsHwRingInfo.VlanList),
                    &(pLocalArgs->pErpsHwRingInfo->VlanList),
                    sizeof (tVlanList));

            if (pLocalArgs->pErpsHwRingInfo->pErpsLspPwInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->ErpsHwRingInfo.ErpsLspPwInfo),
                        (pLocalArgs->pErpsHwRingInfo->pErpsLspPwInfo),
                        sizeof (tErpsLspPwInfo));
            }

            pRemoteArgs->ErpsHwRingInfo.u4ContextId =
                pLocalArgs->pErpsHwRingInfo->u4ContextId;
            pRemoteArgs->ErpsHwRingInfo.u4Port1IfIndex =
                pLocalArgs->pErpsHwRingInfo->u4Port1IfIndex;
            pRemoteArgs->ErpsHwRingInfo.u4Port2IfIndex =
                pLocalArgs->pErpsHwRingInfo->u4Port2IfIndex;
            pRemoteArgs->ErpsHwRingInfo.u4RingId =
                pLocalArgs->pErpsHwRingInfo->u4RingId;
            MEMCPY (&(pRemoteArgs->ErpsHwRingInfo.ai4HwRingHandle),
                    &(pLocalArgs->pErpsHwRingInfo->ai4HwRingHandle),
                    (sizeof (INT4) * ERPS_MAX_HW_HANDLE));
            MEMCPY (&(pRemoteArgs->ErpsHwRingInfo.VlanId),
                    &(pLocalArgs->pErpsHwRingInfo->VlanId), sizeof (tVlanId));
            pRemoteArgs->ErpsHwRingInfo.u2VlanGroupId =
                pLocalArgs->pErpsHwRingInfo->u2VlanGroupId;
            pRemoteArgs->ErpsHwRingInfo.u1Port1Action =
                pLocalArgs->pErpsHwRingInfo->u1Port1Action;
            pRemoteArgs->ErpsHwRingInfo.u1Port2Action =
                pLocalArgs->pErpsHwRingInfo->u1Port2Action;
            pRemoteArgs->ErpsHwRingInfo.u1RingAction =
                pLocalArgs->pErpsHwRingInfo->u1RingAction;
            pRemoteArgs->ErpsHwRingInfo.u1ProtectionType =
                pLocalArgs->pErpsHwRingInfo->u1ProtectionType;
            pRemoteArgs->ErpsHwRingInfo.u1HwUpdatedPort =
                pLocalArgs->pErpsHwRingInfo->u1HwUpdatedPort;
            pRemoteArgs->ErpsHwRingInfo.u1UnBlockRAPSChannel =
                pLocalArgs->pErpsHwRingInfo->u1UnBlockRAPSChannel;
            pRemoteArgs->ErpsHwRingInfo.u1HighestPriorityRequest =
                pLocalArgs->pErpsHwRingInfo->u1HighestPriorityRequest;
            pRemoteArgs->ErpsHwRingInfo.u1Service =
                pLocalArgs->pErpsHwRingInfo->u1Service;
            break;
        }
#ifdef MBSM_WANTED
        case FS_ERPS_MBSM_HW_RING_CONFIG:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilErpsConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP 
                            Arguments to Local NP Arguments. Remote Np can have 
                            values in the input. Local Np will have only values.                            Remote NP Pointers are dereferenced and the values 
                            are copied to the Local NP.
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilErpsConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    tErpsNpModInfo     *pErpsNpModInfo = NULL;
    tErpsRemoteNpModInfo *pErpsRemoteNpModInfo = NULL;
    UINT4               u4Opcode = 0;
    UINT1               u1RetVal = FNP_SUCCESS;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pErpsNpModInfo = &(pFsHwNp->ErpsNpModInfo);
    pErpsRemoteNpModInfo = &(pRemoteHwNp->ErpsRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_ERPS_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsHwRingConfig *pLocalArgs = NULL;
            tErpsRemoteNpWrFsErpsHwRingConfig *pRemoteArgs = NULL;
            pLocalArgs = &pErpsNpModInfo->ErpsNpFsErpsHwRingConfig;
            pRemoteArgs = &pErpsRemoteNpModInfo->ErpsRemoteNpFsErpsHwRingConfig;
            MEMCPY (&(pLocalArgs->pErpsHwRingInfo->VlanList),
                    &(pRemoteArgs->ErpsHwRingInfo.VlanList),
                    sizeof (tVlanList));

            if (pLocalArgs->pErpsHwRingInfo->pErpsLspPwInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pErpsHwRingInfo->pErpsLspPwInfo),
                        &(pRemoteArgs->ErpsHwRingInfo.ErpsLspPwInfo),
                        sizeof (tErpsLspPwInfo));
            }

            pLocalArgs->pErpsHwRingInfo->u4ContextId =
                pRemoteArgs->ErpsHwRingInfo.u4ContextId;
            pLocalArgs->pErpsHwRingInfo->u4Port1IfIndex =
                pRemoteArgs->ErpsHwRingInfo.u4Port1IfIndex;
            pRemoteArgs->ErpsHwRingInfo.u4Port2IfIndex =
                pLocalArgs->pErpsHwRingInfo->u4Port2IfIndex;
            pLocalArgs->pErpsHwRingInfo->u4RingId =
                pRemoteArgs->ErpsHwRingInfo.u4RingId;
            MEMCPY (&(pLocalArgs->pErpsHwRingInfo->ai4HwRingHandle),
                    &(pRemoteArgs->ErpsHwRingInfo.ai4HwRingHandle),
                    (sizeof (INT4) * ERPS_MAX_HW_HANDLE));
            MEMCPY (&(pLocalArgs->pErpsHwRingInfo->VlanId),
                    &(pRemoteArgs->ErpsHwRingInfo.VlanId), sizeof (tVlanId));
            pLocalArgs->pErpsHwRingInfo->u2VlanGroupId =
                pRemoteArgs->ErpsHwRingInfo.u2VlanGroupId;
            pLocalArgs->pErpsHwRingInfo->u1Port1Action =
                pRemoteArgs->ErpsHwRingInfo.u1Port1Action;
            pLocalArgs->pErpsHwRingInfo->u1Port2Action =
                pRemoteArgs->ErpsHwRingInfo.u1Port2Action;
            pLocalArgs->pErpsHwRingInfo->u1RingAction =
                pRemoteArgs->ErpsHwRingInfo.u1RingAction;
            pLocalArgs->pErpsHwRingInfo->u1ProtectionType =
                pRemoteArgs->ErpsHwRingInfo.u1ProtectionType;
            pLocalArgs->pErpsHwRingInfo->u1HwUpdatedPort =
                pRemoteArgs->ErpsHwRingInfo.u1HwUpdatedPort;
            pLocalArgs->pErpsHwRingInfo->u1UnBlockRAPSChannel =
                pRemoteArgs->ErpsHwRingInfo.u1UnBlockRAPSChannel;
            pLocalArgs->pErpsHwRingInfo->u1HighestPriorityRequest =
                pRemoteArgs->ErpsHwRingInfo.u1HighestPriorityRequest;
            pLocalArgs->pErpsHwRingInfo->u1Service =
                pRemoteArgs->ErpsHwRingInfo.u1Service;
            break;
        }
#ifdef MBSM_WANTED
        case FS_ERPS_MBSM_HW_RING_CONFIG:
        {
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilErpsRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tErpsNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilErpsRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    tErpsRemoteNpModInfo *pErpsRemoteNpModInfo = NULL;
    UINT4               u4Opcode = 0;
    UINT1               u1RetVal = FNP_SUCCESS;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pErpsRemoteNpModInfo = &(pRemoteHwNpInput->ErpsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_ERPS_HW_RING_CONFIG:
        {
            tErpsRemoteNpWrFsErpsHwRingConfig *pInput = NULL;
            tErpsHwRingInfo     ErpsHwRingInfo;

            MEMSET (&ErpsHwRingInfo, 0, sizeof (tErpsHwRingInfo));
            pInput = &pErpsRemoteNpModInfo->ErpsRemoteNpFsErpsHwRingConfig;

            MEMCPY (&(ErpsHwRingInfo.VlanList),
                    &(pInput->ErpsHwRingInfo.VlanList), sizeof (tVlanList));
            ErpsHwRingInfo.pErpsLspPwInfo =
                &pInput->ErpsHwRingInfo.ErpsLspPwInfo;
            ErpsHwRingInfo.u4ContextId = pInput->ErpsHwRingInfo.u4ContextId;
            ErpsHwRingInfo.u4Port1IfIndex =
                pInput->ErpsHwRingInfo.u4Port1IfIndex;
            ErpsHwRingInfo.u4Port2IfIndex =
                pInput->ErpsHwRingInfo.u4Port2IfIndex;
            ErpsHwRingInfo.u4RingId = pInput->ErpsHwRingInfo.u4RingId;
            MEMCPY (&(ErpsHwRingInfo.ai4HwRingHandle),
                    &(pInput->ErpsHwRingInfo.ai4HwRingHandle),
                    (sizeof (INT4) * ERPS_MAX_HW_HANDLE));
            MEMCPY (&(ErpsHwRingInfo.VlanId), &(pInput->ErpsHwRingInfo.VlanId),
                    sizeof (tVlanId));
            ErpsHwRingInfo.u2VlanGroupId = pInput->ErpsHwRingInfo.u2VlanGroupId;
            ErpsHwRingInfo.u1Port1Action = pInput->ErpsHwRingInfo.u1Port1Action;
            ErpsHwRingInfo.u1Port2Action = pInput->ErpsHwRingInfo.u1Port2Action;
            ErpsHwRingInfo.u1RingAction = pInput->ErpsHwRingInfo.u1RingAction;
            ErpsHwRingInfo.u1ProtectionType =
                pInput->ErpsHwRingInfo.u1ProtectionType;
            ErpsHwRingInfo.u1HwUpdatedPort =
                pInput->ErpsHwRingInfo.u1HwUpdatedPort;
            ErpsHwRingInfo.u1UnBlockRAPSChannel =
                pInput->ErpsHwRingInfo.u1UnBlockRAPSChannel;
            ErpsHwRingInfo.u1HighestPriorityRequest =
                pInput->ErpsHwRingInfo.u1HighestPriorityRequest;
            ErpsHwRingInfo.u1Service = pInput->ErpsHwRingInfo.u1Service;

            u1RetVal = FsErpsHwRingConfig (&(ErpsHwRingInfo));

            MEMCPY (&(pInput->ErpsHwRingInfo.VlanList),
                    &(ErpsHwRingInfo.VlanList), sizeof (tVlanList));
            pInput->ErpsHwRingInfo.u4ContextId = ErpsHwRingInfo.u4ContextId;
            pInput->ErpsHwRingInfo.u4Port1IfIndex =
                ErpsHwRingInfo.u4Port1IfIndex;
            pInput->ErpsHwRingInfo.u4Port2IfIndex =
                ErpsHwRingInfo.u4Port2IfIndex;
            pInput->ErpsHwRingInfo.u4RingId = ErpsHwRingInfo.u4RingId;
            MEMCPY (&(pInput->ErpsHwRingInfo.ai4HwRingHandle),
                    &(ErpsHwRingInfo.ai4HwRingHandle),
                    (sizeof (INT4) * ERPS_MAX_HW_HANDLE));
            MEMCPY (&(pInput->ErpsHwRingInfo.VlanId),
                    &(ErpsHwRingInfo.VlanId), sizeof (tVlanId));
            pInput->ErpsHwRingInfo.u2VlanGroupId = ErpsHwRingInfo.u2VlanGroupId;
            pInput->ErpsHwRingInfo.u1Port1Action = ErpsHwRingInfo.u1Port1Action;
            pInput->ErpsHwRingInfo.u1Port2Action = ErpsHwRingInfo.u1Port2Action;
            pInput->ErpsHwRingInfo.u1RingAction = ErpsHwRingInfo.u1RingAction;
            pInput->ErpsHwRingInfo.u1ProtectionType =
                ErpsHwRingInfo.u1ProtectionType;
            pInput->ErpsHwRingInfo.u1HwUpdatedPort =
                ErpsHwRingInfo.u1HwUpdatedPort;
            pInput->ErpsHwRingInfo.u1UnBlockRAPSChannel =
                ErpsHwRingInfo.u1UnBlockRAPSChannel;
            pInput->ErpsHwRingInfo.u1HighestPriorityRequest =
                ErpsHwRingInfo.u1HighestPriorityRequest;
            pInput->ErpsHwRingInfo.u1Service = ErpsHwRingInfo.u1Service;

            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************/
/*  Function Name       : NpUtilMaskErpsNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskErpsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tErpsNpModInfo     *pErpsNpModInfo = NULL;
    tErpsRemoteNpModInfo *pErpsRemoteNpModInfo = NULL;

    UINT4               u4Port1IfIndex = 0;
    UINT4               u4Port2IfIndex = 0;
    UINT4               u4RemPort1IfIndex = 0;
    UINT4               u4RemPort2IfIndex = 0;

    UINT1               u1Port1IfIndexStatus = FNP_SUCCESS;
    UINT1               u1Port2IfIndexStatus = FNP_SUCCESS;
    UINT1               u1PortChannel1IfIndexStatus = FNP_SUCCESS;
    UINT1               u1PortChannel2IfIndexStatus = FNP_SUCCESS;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    pErpsNpModInfo = &pFsHwNp->ErpsNpModInfo;
    pErpsRemoteNpModInfo = &pRemoteHwNp->ErpsRemoteNpModInfo;
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_ERPS_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsHwRingConfig *pEntry = NULL;
            tErpsRemoteNpWrFsErpsHwRingConfig *pRemEntry = NULL;
            pEntry = &pErpsNpModInfo->ErpsNpFsErpsHwRingConfig;
            pRemEntry = &pErpsRemoteNpModInfo->ErpsRemoteNpFsErpsHwRingConfig;

            switch (pEntry->pErpsHwRingInfo->u1RingAction)
            {
                case ERPS_HW_RING_CREATE:
                case ERPS_HW_RING_MODIFY:
                case ERPS_HW_RING_DELETE:
                {
                    u4Port1IfIndex = pEntry->pErpsHwRingInfo->u4Port1IfIndex;
                    u4Port2IfIndex = pEntry->pErpsHwRingInfo->u4Port2IfIndex;
                    u4RemPort1IfIndex =
                        pRemEntry->ErpsHwRingInfo.u4Port1IfIndex;
                    u4RemPort2IfIndex =
                        pRemEntry->ErpsHwRingInfo.u4Port2IfIndex;
                    u1Port1IfIndexStatus =
                        NpUtilIsRemotePort (u4Port1IfIndex, &HwPortInfo);
                    u1Port2IfIndexStatus =
                        NpUtilIsRemotePort (u4Port2IfIndex, &HwPortInfo);
                    u1PortChannel1IfIndexStatus =
                        NpUtilIsPortChannel (u4Port1IfIndex);
                    u1PortChannel2IfIndexStatus =
                        NpUtilIsPortChannel (u4Port2IfIndex);

                    if ((u1PortChannel1IfIndexStatus == FNP_SUCCESS) &&
                        (u1PortChannel2IfIndexStatus == FNP_SUCCESS))
                    {
                        /*u4Port1IfIndex and u4Port2IfIndex are port channel */
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if (u1PortChannel1IfIndexStatus == FNP_SUCCESS)
                    {
                        /* u4Port1IfIndex is Port channel */
                        if (u1Port2IfIndexStatus == FNP_SUCCESS)
                        {
                            /* u4Port2IfIndex is Remote Port and as the call 
                               has to be invoked in both local and remote 
                               it is made zero in local unit */
                            u4Port2IfIndex = 0;
                        }
                        else
                        {
                            u4RemPort2IfIndex = 0;
                        }
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if (u1PortChannel2IfIndexStatus == FNP_SUCCESS)
                    {
                        /* u4Port2IfIndex is Port channel */
                        if (u1Port1IfIndexStatus == FNP_SUCCESS)
                        {
                            /* u4Port1IfIndex is Remote Port and as the call 
                               has to be invoked in both local and remote 
                               it is made zero in local unit */
                            u4Port1IfIndex = 0;
                        }
                        else
                        {
                            u4RemPort1IfIndex = 0;
                        }
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if ((u1Port1IfIndexStatus == FNP_SUCCESS) &&
                             (u1Port2IfIndexStatus == FNP_SUCCESS))
                    {
                        /* Both u4Port1IfIndex and u4Port2IfIndex are 
                           remote ports and hence invoked in remote unit alone */
                        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
                    }
                    else if ((u1Port1IfIndexStatus != FNP_SUCCESS) &&
                             (u1Port2IfIndexStatus == FNP_SUCCESS))
                    {
                        /* u4Port1IfIndex is local port and u4Port2IfIndex 
                           is remote port.As the call has to be invoked in 
                           both local and remote,u4Port1IfIndex in remote unit 
                           is made zero and u4Port2IfIndex in local unit 
                           is made zero */
                        u4RemPort1IfIndex = 0;
                        u4Port2IfIndex = 0;
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else if ((u1Port1IfIndexStatus == FNP_SUCCESS) &&
                             (u1Port2IfIndexStatus != FNP_SUCCESS))
                    {
                        /* u4Port1IfIndex is remote port and u4Port2IfIndex is 
                           local port.As the call has to be invoked in both 
                           local and remote,u4Port1IfIndex in local unit is 
                           made zero and u4Port2IfIndex in remote unit is made 
                           zero */
                        u4Port1IfIndex = 0;
                        u4RemPort2IfIndex = 0;
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    }
                    else
                    {
                        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
                    }

                    pEntry->pErpsHwRingInfo->u4Port1IfIndex = u4Port1IfIndex;
                    pEntry->pErpsHwRingInfo->u4Port2IfIndex = u4Port2IfIndex;
                    pRemEntry->ErpsHwRingInfo.u4Port1IfIndex =
                        u4RemPort1IfIndex;
                    pRemEntry->ErpsHwRingInfo.u4Port2IfIndex =
                        u4RemPort2IfIndex;

                    break;
                }
                case ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING:
                case ERPS_HW_ADD_VLAN_MAPPING:
                case ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING:
                case ERPS_HW_DEL_VLAN_MAPPING:
                {
                    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
                    break;
                }
            }
            break;
        }
#ifdef MBSM_WANTED
        case FS_ERPS_MBSM_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsMbsmHwRingConfig *pEntry = NULL;
            tErpsRemoteNpWrFsErpsHwRingConfig *pRemEntry = NULL;

            pEntry = &pErpsNpModInfo->ErpsNpFsErpsMbsmHwRingConfig;
            pRemEntry = &pErpsRemoteNpModInfo->ErpsRemoteNpFsErpsHwRingConfig;
            pRemoteHwNp->u4Opcode = FS_ERPS_HW_RING_CONFIG;

            MEMCPY (&(pRemEntry->ErpsHwRingInfo.VlanList),
                    &(pEntry->pErpsHwRingInfo->VlanList), sizeof (tVlanList));

            if (pEntry->pErpsHwRingInfo->pErpsLspPwInfo != NULL)
            {
                MEMCPY (&(pRemEntry->ErpsHwRingInfo.ErpsLspPwInfo),
                        (pEntry->pErpsHwRingInfo->pErpsLspPwInfo),
                        sizeof (tErpsLspPwInfo));
            }

            pRemEntry->ErpsHwRingInfo.u4ContextId =
                pEntry->pErpsHwRingInfo->u4ContextId;
            pRemEntry->ErpsHwRingInfo.u4Port1IfIndex =
                pEntry->pErpsHwRingInfo->u4Port1IfIndex;
            pRemEntry->ErpsHwRingInfo.u4Port2IfIndex =
                pEntry->pErpsHwRingInfo->u4Port2IfIndex;
            pRemEntry->ErpsHwRingInfo.u4RingId =
                pEntry->pErpsHwRingInfo->u4RingId;
            MEMCPY (&(pRemEntry->ErpsHwRingInfo.ai4HwRingHandle),
                    &(pEntry->pErpsHwRingInfo->ai4HwRingHandle),
                    (sizeof (INT4) * ERPS_MAX_HW_HANDLE));
            MEMCPY (&(pRemEntry->ErpsHwRingInfo.VlanId),
                    &(pEntry->pErpsHwRingInfo->VlanId), sizeof (tVlanId));
            pRemEntry->ErpsHwRingInfo.u2VlanGroupId =
                pEntry->pErpsHwRingInfo->u2VlanGroupId;
            pRemEntry->ErpsHwRingInfo.u1Port1Action =
                pEntry->pErpsHwRingInfo->u1Port1Action;
            pRemEntry->ErpsHwRingInfo.u1Port2Action =
                pEntry->pErpsHwRingInfo->u1Port2Action;
            pRemEntry->ErpsHwRingInfo.u1RingAction =
                pEntry->pErpsHwRingInfo->u1RingAction;
            pRemEntry->ErpsHwRingInfo.u1ProtectionType =
                pEntry->pErpsHwRingInfo->u1ProtectionType;
            pRemEntry->ErpsHwRingInfo.u1HwUpdatedPort =
                pEntry->pErpsHwRingInfo->u1HwUpdatedPort;
            pRemEntry->ErpsHwRingInfo.u1UnBlockRAPSChannel =
                pEntry->pErpsHwRingInfo->u1UnBlockRAPSChannel;
            pRemEntry->ErpsHwRingInfo.u1HighestPriorityRequest =
                pEntry->pErpsHwRingInfo->u1HighestPriorityRequest;
            pRemEntry->ErpsHwRingInfo.u1Service =
                pEntry->pErpsHwRingInfo->u1Service;

            u4Port1IfIndex = pRemEntry->ErpsHwRingInfo.u4Port1IfIndex;
            u4Port2IfIndex = pRemEntry->ErpsHwRingInfo.u4Port2IfIndex;

            u1Port1IfIndexStatus = NpUtilIsRemotePort (u4Port1IfIndex,
                                                       &HwPortInfo);
            u1Port2IfIndexStatus = NpUtilIsRemotePort (u4Port2IfIndex,
                                                       &HwPortInfo);
            u1PortChannel1IfIndexStatus = NpUtilIsPortChannel (u4Port1IfIndex);
            u1PortChannel2IfIndexStatus = NpUtilIsPortChannel (u4Port2IfIndex);

            if ((u1PortChannel1IfIndexStatus == FNP_SUCCESS) ||
                (u1PortChannel2IfIndexStatus == FNP_SUCCESS))
            {
                if ((u1PortChannel1IfIndexStatus == FNP_SUCCESS) &&
                    (u1PortChannel2IfIndexStatus != FNP_SUCCESS))
                {
                    /* u4Port1IfIndex is Port channel */
                    if (u1Port2IfIndexStatus != FNP_SUCCESS)
                    {
                        /* u4Port2IfIndex is Local Port and 
                           hence it is made zero */
                        u4Port2IfIndex = 0;
                    }
                }
                else if ((u1PortChannel1IfIndexStatus != FNP_SUCCESS) &&
                         (u1PortChannel2IfIndexStatus == FNP_SUCCESS))
                {
                    /* u4Port2IfIndex is Port channel */
                    if (u1Port1IfIndexStatus != FNP_SUCCESS)
                    {
                        /* u4Port1IfIndex is Local Port and 
                           hence it is made zero */
                        u4Port1IfIndex = 0;
                    }
                }
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else if ((u1Port1IfIndexStatus == FNP_SUCCESS) ||
                     (u1Port2IfIndexStatus == FNP_SUCCESS))
            {
                if ((u1Port1IfIndexStatus != FNP_SUCCESS) &&
                    (u1Port2IfIndexStatus == FNP_SUCCESS))
                {
                    /* u4Port1IfIndex is local port and 
                       u4Port2IfIndex is remote port.
                       As the call has to be invoked in remote,
                       u4Port1IfIndex in remote unit is made zero */
                    u4Port1IfIndex = 0;
                }
                else if ((u1Port1IfIndexStatus == FNP_SUCCESS) &&
                         (u1Port2IfIndexStatus != FNP_SUCCESS))
                {
                    /* u4Port1IfIndex is remote port and 
                       u4Port2IfIndex is local port.
                       As the call has to be invoked in remote,
                       u4Port2IfIndex in remote unit is made zero */
                    u4Port2IfIndex = 0;
                }
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }

            pRemEntry->ErpsHwRingInfo.u4Port1IfIndex = u4Port1IfIndex;
            pRemEntry->ErpsHwRingInfo.u4Port2IfIndex = u4Port2IfIndex;
            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode != FS_ERPS_MBSM_HW_RING_CONFIG))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_ERPS_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif

    return;
}

#endif /* __ERPS_NPUTIL_C__ */
