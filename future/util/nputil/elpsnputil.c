
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnputil.c,v 1.4 2014/07/02 10:24:23 siva Exp $
 *
 * Description:This file contains the single NP function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __ELPS_NPUTIL_C__
#define __ELPS_NPUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilElpsConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilElpsConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;
    tElpsRemoteNpModInfo *pElpsRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pElpsNpModInfo = &(pFsHwNp->ElpsNpModInfo);
    pElpsRemoteNpModInfo = &(pRemoteHwNp->ElpsRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tElpsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsNpWrFsMiElpsHwPgSwitchDataPath *pLocalArgs = NULL;
            tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath *pRemoteArgs = NULL;
            pLocalArgs = &pElpsNpModInfo->ElpsNpFsMiElpsHwPgSwitchDataPath;
            pRemoteArgs =
                &pElpsRemoteNpModInfo->ElpsRemoteNpFsMiElpsHwPgSwitchDataPath;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4PgId = pLocalArgs->u4PgId;
            if (pLocalArgs->pPgHwInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->PgHwInfo), (pLocalArgs->pPgHwInfo),
                        sizeof (tElpsHwPgSwitchInfo));
            }
            break;
        }
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilElpsConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilElpsConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;
    tElpsRemoteNpModInfo *pElpsRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pElpsNpModInfo = &(pFsHwNp->ElpsNpModInfo);
    pElpsRemoteNpModInfo = &(pRemoteHwNp->ElpsRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsNpWrFsMiElpsHwPgSwitchDataPath *pLocalArgs = NULL;
            tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath *pRemoteArgs = NULL;
            pLocalArgs = &pElpsNpModInfo->ElpsNpFsMiElpsHwPgSwitchDataPath;
            pRemoteArgs =
                &pElpsRemoteNpModInfo->ElpsRemoteNpFsMiElpsHwPgSwitchDataPath;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4PgId = pRemoteArgs->u4PgId;
            if (pLocalArgs->pPgHwInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pPgHwInfo), &(pRemoteArgs->PgHwInfo),
                        sizeof (tElpsHwPgSwitchInfo));
            }
            break;
        }
        default:
            u1RetVal = FNP_SUCCESS;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilElpsRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tElpsNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilElpsRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tElpsRemoteNpModInfo *pElpsRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pElpsRemoteNpModInfo = &(pRemoteHwNpInput->ElpsRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath *pInput = NULL;
            pInput =
                &pElpsRemoteNpModInfo->ElpsRemoteNpFsMiElpsHwPgSwitchDataPath;
            u1RetVal =
                FsMiElpsHwPgSwitchDataPath (pInput->u4ContextId, pInput->u4PgId,
                                            &(pInput->PgHwInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************/
/*  Function Name       : NpUtilMaskElpsNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskElpsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;
    tElpsRemoteNpModInfo *pElpsRemoteNpModInfo = NULL;

    pElpsNpModInfo = &pFsHwNp->ElpsNpModInfo;
    pElpsRemoteNpModInfo = &pRemoteHwNp->ElpsRemoteNpModInfo;

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsNpWrFsMiElpsHwPgSwitchDataPath *pEntry = NULL;
            tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath *pRemEntry = NULL;

            pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsHwPgSwitchDataPath;
            pRemEntry =
                &pElpsRemoteNpModInfo->ElpsRemoteNpFsMiElpsHwPgSwitchDataPath;

            NpUtilIsPortOrService (pEntry->pPgHwInfo, &pRemEntry->PgHwInfo,
                                   &HwPortInfo, pu1NpCallStatus);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ELPS_MBSM_HW_PG_SWITCH_DATA_PATH:
        {
            pRemoteHwNp->u4Opcode = FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH;
            tElpsNpWrFsMiElpsMbsmHwPgSwitchDataPath *pEntry = NULL;
            tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath *pRemEntry = NULL;

            pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsMbsmHwPgSwitchDataPath;
            pRemEntry =
                &pElpsRemoteNpModInfo->ElpsRemoteNpFsMiElpsHwPgSwitchDataPath;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u4PgId = pEntry->u4PgId;
            if (pEntry->pPgHwInfo != NULL)
            {
                MEMCPY (&(pRemEntry->PgHwInfo), (pEntry->pPgHwInfo),
                        sizeof (tElpsHwPgSwitchInfo));
                NpUtilIsPortOrService (pEntry->pPgHwInfo,
                                       &pRemEntry->PgHwInfo,
                                       &HwPortInfo, pu1NpCallStatus);
            }

            break;
        }
#endif /* MBSM_WANTED */
        default:
            break;
    }
    return;
}

VOID
NpUtilElpsPortDisjoint (tElpsHwPgSwitchInfo * pElpsHwPgSwitchInfo,
                        tElpsHwPgSwitchInfo * pRemElpsHwPgSwitchInfo,
                        tHwPortInfo * pLocalHwPortInfo, UINT1 *pu1NpCallStatus)
{

    UINT4               u4WorkingPortId = 0;
    UINT4               u4ProtectionPortId = 0;
    UINT4               u4RemWorkingPortId = 0;
    UINT4               u4RemProtectionPortId = 0;

    UINT1               u1WorkingPortStatus = FNP_SUCCESS;
    UINT1               u1ProtectionPortStatus = FNP_SUCCESS;
    UINT1               u1WorkingPortChannelStatus = FNP_SUCCESS;
    UINT1               u1ProtectionPortChannelStatus = FNP_SUCCESS;

    u4WorkingPortId = pElpsHwPgSwitchInfo->u4PgWorkingPortId;
    u4ProtectionPortId = pElpsHwPgSwitchInfo->u4PgProtectionPortId;
    u4RemWorkingPortId = pRemElpsHwPgSwitchInfo->u4PgWorkingPortId;
    u4RemProtectionPortId = pRemElpsHwPgSwitchInfo->u4PgProtectionPortId;

    u1WorkingPortStatus =
        NpUtilIsRemotePort (pElpsHwPgSwitchInfo->u4PgWorkingPhyPort,
                            pLocalHwPortInfo);
    u1ProtectionPortStatus =
        NpUtilIsRemotePort (pElpsHwPgSwitchInfo->u4PgProtectionPhyPort,
                            pLocalHwPortInfo);
    u1WorkingPortChannelStatus = NpUtilIsPortChannel (u4WorkingPortId);
    u1ProtectionPortChannelStatus = NpUtilIsPortChannel (u4ProtectionPortId);

    if ((u1WorkingPortChannelStatus == FNP_SUCCESS) &&
        (u1ProtectionPortChannelStatus == FNP_SUCCESS))
    {
        /* Both Working port and protection port are port channel */
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u1WorkingPortChannelStatus == FNP_SUCCESS)
    {
        /* Working port is port channel */
        if (u1ProtectionPortStatus == FNP_SUCCESS)
        {
            /* Protection port is remote port and the call has to be invoked in both local and remote , it is made zero in local unit */
            u4ProtectionPortId = 0;
        }
        else
        {
            u4RemProtectionPortId = 0;
        }
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u1ProtectionPortChannelStatus == FNP_SUCCESS)
    {
        /* Protection port is port channel */
        if (u1WorkingPortStatus == FNP_SUCCESS)
        {
            /* Working port is remote port and the call has to be invoked in both local and remote unit, it is made zero in local unit */
            u4WorkingPortId = 0;
        }
        else
        {
            u4RemWorkingPortId = 0;
        }
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    if ((u1WorkingPortStatus == FNP_SUCCESS) &&
        (u1ProtectionPortStatus == FNP_SUCCESS))
    {
        /* Both working and protection port are remote ports */
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
    else if ((u1WorkingPortStatus != FNP_SUCCESS) &&
             (u1ProtectionPortStatus == FNP_SUCCESS))
    {
        /* Working port is local and protection port is remote.
           As the call has to be invoked in both local and remote unit,
           working port in remote unit is made zero and protection port in local unit is made zero */
        u4RemWorkingPortId = 0;
        u4ProtectionPortId = 0;
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if ((u1WorkingPortStatus == FNP_SUCCESS) &&
             (u1ProtectionPortStatus != FNP_SUCCESS))
    {
        /* Working port is remote and protection port is local.
           As the call has to be invoked in both local and remote unit,
           working port in local unit is made zero and protection port in remote unit is made zero */
        u4WorkingPortId = 0;
        u4RemProtectionPortId = 0;
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else
    {
        /* Both the ports are local ports.
           Hence invoked in local unit only */
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }

    pElpsHwPgSwitchInfo->u4PgWorkingPortId = u4WorkingPortId;
    pElpsHwPgSwitchInfo->u4PgProtectionPortId = u4ProtectionPortId;
    pRemElpsHwPgSwitchInfo->u4PgWorkingPortId = u4RemWorkingPortId;
    pRemElpsHwPgSwitchInfo->u4PgProtectionPortId = u4RemProtectionPortId;

    return;
}

#ifdef MBSM_WANTED

VOID
NpUtilElpsMbsmPortDisjoint (tElpsHwPgSwitchInfo * pRemElpsMbsmHwPgSwitchInfo,
                            tHwPortInfo * pLocalHwPortInfo,
                            UINT1 *pu1NpCallStatus)
{

    UINT4               u4WorkingPortId = 0;
    UINT4               u4ProtectionPortId = 0;

    UINT1               u1WorkingPortStatus = FNP_SUCCESS;
    UINT1               u1ProtectionPortStatus = FNP_SUCCESS;
    UINT1               u1WorkingPortChannelStatus = FNP_SUCCESS;
    UINT1               u1ProtectionPortChannelStatus = FNP_SUCCESS;

    u4WorkingPortId = pRemElpsMbsmHwPgSwitchInfo->u4PgWorkingPortId;
    u4ProtectionPortId = pRemElpsMbsmHwPgSwitchInfo->u4PgProtectionPortId;
    u1WorkingPortStatus =
        NpUtilIsRemotePort (pRemElpsMbsmHwPgSwitchInfo->u4PgWorkingPhyPort,
                            pLocalHwPortInfo);
    u1ProtectionPortStatus =
        NpUtilIsRemotePort (pRemElpsMbsmHwPgSwitchInfo->u4PgProtectionPhyPort,
                            pLocalHwPortInfo);

    u1WorkingPortChannelStatus = NpUtilIsPortChannel (u4WorkingPortId);
    u1ProtectionPortChannelStatus = NpUtilIsPortChannel (u4ProtectionPortId);

    if ((u1WorkingPortChannelStatus == FNP_SUCCESS) ||
        (u1ProtectionPortChannelStatus == FNP_SUCCESS))
    {
        /* Both Working and protection are port channel */
        if ((u1ProtectionPortChannelStatus != FNP_SUCCESS))
        {
            /* Working port is port channel */
            if (u1ProtectionPortStatus != FNP_SUCCESS)
            {
                /* Protection port is local port and hence made zero */
                u4ProtectionPortId = 0;
            }
        }
        else if ((u1WorkingPortChannelStatus != FNP_SUCCESS))
        {
            /* Protection prt is port channel */
            if (u1WorkingPortStatus != FNP_SUCCESS)
            {
                /* Working port is local port and hence made zero */
                u4WorkingPortId = 0;
            }
        }
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
    else if ((u1WorkingPortStatus == FNP_SUCCESS) ||
             (u1ProtectionPortStatus == FNP_SUCCESS))
    {
        if (u1WorkingPortStatus != FNP_SUCCESS)
        {
            /* Working port is local port and protection port is remote port.
               As the call has to be invoked in remote alone,
               working port is made zero */
            u4WorkingPortId = 0;
        }
        else if (u1ProtectionPortStatus != FNP_SUCCESS)
        {
            /* Protection port is local port and working port is remote port.
               As the call has to be invoked in remote alone,
               protection port is made zero */
            u4ProtectionPortId = 0;
        }
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }

    pRemElpsMbsmHwPgSwitchInfo->u4PgWorkingPortId = u4WorkingPortId;
    pRemElpsMbsmHwPgSwitchInfo->u4PgProtectionPortId = u4ProtectionPortId;

    return;
}
#endif /* MBSM_WANTED */
VOID
NpUtilIsPortOrService (tElpsHwPgSwitchInfo * pElpsPortOrServiceHwPgSwitchInfo,
                       tElpsHwPgSwitchInfo *
                       pRemElpsPortOrServiceHwPgSwitchInfo,
                       tHwPortInfo * pHwPortInfo, UINT1 *pu1NpCallStatus)
{

    if (pElpsPortOrServiceHwPgSwitchInfo->u4PgWorkingServiceValue !=
        pElpsPortOrServiceHwPgSwitchInfo->u4PgProtectionServiceValue)
    {
        /* port & service disjoint or service disjoint */
        if (pElpsPortOrServiceHwPgSwitchInfo->u4PgWorkingPortId !=
            pElpsPortOrServiceHwPgSwitchInfo->u4PgProtectionPortId)
        {
            /* port & service disjoint case */
            NpUtilElpsPortDisjoint (pElpsPortOrServiceHwPgSwitchInfo,
                                    pRemElpsPortOrServiceHwPgSwitchInfo,
                                    pHwPortInfo, pu1NpCallStatus);
#ifdef MBSM_WANTED
            NpUtilElpsMbsmPortDisjoint (pRemElpsPortOrServiceHwPgSwitchInfo,
                                        pHwPortInfo, pu1NpCallStatus);
#endif /* MBSM_WANTED */

        }
        else
        {
            /* service disjoint case Both working port and protection port are the same */
            if (NpUtilIsRemotePort
                (pElpsPortOrServiceHwPgSwitchInfo->u4PgWorkingPhyPort,
                 pHwPortInfo) == FNP_SUCCESS)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            else
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
#ifdef MBSM_WANTED
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;

#endif /* MBSM_WANTED */

            }
        }
    }
    else
    {
        NpUtilElpsPortDisjoint (pElpsPortOrServiceHwPgSwitchInfo,
                                pRemElpsPortOrServiceHwPgSwitchInfo,
                                pHwPortInfo, pu1NpCallStatus);
#ifdef MBSM_WANTED
        NpUtilElpsMbsmPortDisjoint (pRemElpsPortOrServiceHwPgSwitchInfo,
                                    pHwPortInfo, pu1NpCallStatus);
#endif /* MBSM_WANTED */
    }
    return;
}
#endif /*__ELPS_NPUTIL_C__ */
