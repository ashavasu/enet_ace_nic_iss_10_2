
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: nputlbcmx.c,v 1.7 2014/07/03 10:07:30 siva Exp $
 *
 * Description:This file contains platform specific utility functions 
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __NPUTIL_BCMX_C__
#define __NPUTIL_BCMX_C__

#include "nputlpltfrm.h"
/***************************************************************************/
/*  Function Name       : CustNpUtilMaskNpPorts                            */
/*                                                                         */
/*  Description         : This is a Customized function to Implement       */
/*                        masking for NPAPI's which requires special       */
/*                        handling for each platform.This function splits  */
/*                        the NPAPI PortList argument into Self and Remote */
/*                        ports as per the information and updates in which*/
/*                        Node the NpCall should be executed.              */
/*                        If NP call is a MBSM call,corresponding NON      */
/*                        MBSM call will be executed in Remote Unit        */
/*                                                                         */
/*  Input(s)            : pFsHwNp - Pointer containing the entire NPAPI    */
/*                                  Information.                           */
/*                                                                         */
/*  Output(s)           : pFsHwNp - Port List is updated. Ports in the     */
/*                                  Remote unit will be removed based on   */
/*                                  the Masking Logic of NPAPI             */

/*                      : pRemoteHwNp - Port List is updated. Ports in the */
/*                                      Local unit will be removed based   */
/*                                      on the Masking Logic of NPAPI      */
/*                      : pu1NpCallStatus - In which Node the NPAPI call   */
/*                                          should be executed.            */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
INT4
CustNpUtilMaskNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus)
{
    switch (pFsHwNp->u4Opcode)
    {

#if defined (VLAN_WANTED) && defined (MBSM_WANTED)
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            /* Masking is not required .Ports should be programmed as
               such in both the units */
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#else
            UNUSED_PARAM (pu1NpCallStatus);
            UNUSED_PARAM (pRemoteHwNp);
#endif

#if defined (VLAN_WANTED) && defined (MBSM_WANTED)
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
        {
            tVlanNpWrFsMiVlanMbsmSyncFDBInfo *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmSyncFDBInfo *pRemoteArgs = NULL;
            tVlanNpModInfo     *pVlanNpModInfo = NULL;
            tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
            pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
            pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmSyncFDBInfo;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanMbsmSyncFDBInfo;
            if (pLocalArgs->pFDBInfoArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->FDBInfoArray),
                        (pLocalArgs->pFDBInfoArray), sizeof (tFDBInfoArray));
            }
            /* Since the function with this op code is used to sync the
             * FDB entries learnt in Active(when standby is down) to
             * standby, the function call status is Remote unit
             * execution always */
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
        {
            /* When Stand-by Node comes up,MBSM call for programming 
               Static Unicast MAC Entry will be triggered .It is converted
               as NON MBSM call and executed in Remote Unit */

            tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pRemoteArgs = NULL;
            tFsHwNp             FsHwLocalNp;
            tVlanNpModInfo     *pVlanNpModInfo = NULL;
            tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
            tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pEntry = NULL;

            pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
            pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4FdbId;
            if (pLocalArgs->MacAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u4Port = pLocalArgs->u4RcvPort;
            if (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->HwAllowedToGoPorts.au4PortArray[0]),
                        (pLocalArgs->pHwAllowedToGoPorts->pu4PortArray),
                        sizeof (UINT4) *
                        (pLocalArgs->pHwAllowedToGoPorts->i4Length));
            }
            pRemoteArgs->HwAllowedToGoPorts.i4Length =
                pLocalArgs->pHwAllowedToGoPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            /* When a Static MAC is added for Stand-by port before Stand-by 
               comes up, it will not be programmed in Active Unit .
               This is because the port will be in Not-Present State .
               Hence  Same NPAPI has to be executed in Self (Active) Unit */
            NP_UTIL_FILL_PARAMS (FsHwLocalNp,    /*Generic NP structure */
                                 NP_VLAN_MOD,    /* Module ID */
                                 FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY,
                                 /* Function/OpCode */
                                 0,    /* IfIndex value if applicable */
                                 1,    /* No. of Port Params */
                                 0);    /* No. of PortList Parms */
            pVlanNpModInfo = &(FsHwLocalNp.VlanNpModInfo);
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;

            pEntry->u4ContextId = pLocalArgs->u4ContextId;
            pEntry->u4Fid = pLocalArgs->u4FdbId;
            pEntry->MacAddr = pLocalArgs->MacAddr;
            pEntry->u4Port = pLocalArgs->u4RcvPort;
            pEntry->pHwAllowedToGoPorts = pLocalArgs->pHwAllowedToGoPorts;
            pEntry->u1Status = pLocalArgs->u1Status;
            if (VlanNpWrHwProgram (&FsHwLocalNp) == FNP_FAILURE)
            {
                PRINTF ("Unable to Add Static MAC Entry in the Active Node for \
                        Port %d VLAN %d \n", pEntry->u4Port, pEntry->u4Fid);
                return FNP_FAILURE;
            }

            break;
        }
#endif
        default:
            break;
    }
    return FNP_SUCCESS;;
}
#endif
