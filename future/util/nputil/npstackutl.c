/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: npstackutl.c,v 1.31 2017/08/01 13:50:26 siva Exp $
 *
 * Description:This file contains the definition of the utility 
 *            functions used in the nputil APIs
 *
 *******************************************************************/

#ifndef _NPSTACKUTL_C_
#define _NPSTACKUTL_C_

#include "npstackutl.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilRemoteSemCreate                              */
/*                                                                           */
/*     DESCRIPTION      : This function creates a semaphore for the          */
/*              Remote NP call execution                 */
/*                                                                           */
/*     INPUT            : NONE                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
NpUtilRemoteSemCreate (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteSemCreate : ENTRY\n");
    if (OsixCreateSem ((CONST UINT1 *) "NPREM", 1, OSIX_GLOBAL,
                       &gRemoteNpSemId) != OSIX_SUCCESS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilRemoteSemCreate failed \n");
        return OSIX_FAILURE;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteSemCreate : EXIT\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilRemoteNpLock                                 */
/*                                                                           */
/*     DESCRIPTION      : This function is used to acquire the               */
/*              Remote NP call execution semaphore             */
/*                                                                           */
/*     INPUT            : NONE                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : FNP_SUCCESS/FNP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
NpUtilRemoteNpLock (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpLock: ENTRY\n");
    if (gRemoteNpSemId == 0)
    {
        if (NpUtilRemoteSemCreate () != OSIX_SUCCESS)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilRemoteSemCreate failed \n");
            return FNP_FAILURE;
        }
    }
    if (OsixSemTake (gRemoteNpSemId) != OSIX_SUCCESS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilRemoteNpLock OsixSemTake failed \n");
        return FNP_FAILURE;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpLock: EXIT\n");
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilRemoteNpUnLock                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to release the               */
/*              Remote NP call execution semaphore             */
/*                                                                           */
/*     INPUT            : NONE                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
NpUtilRemoteNpUnLock (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpUnLock: ENTRY\n");
    OsixSemGive (gRemoteNpSemId);
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoteNpUnLock: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilAllocRemoteHwNpMem                           */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get memory                  */
/*              for the variable of type tRemoteHwNp               */
/*                                                                           */
/*     INPUT            : NONE                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Pointer to the Memory of the global structure      */
/*              variable gRemoteHwNp of type tRemoteHwNp         */
/*                                                                           */
/*****************************************************************************/
tRemoteHwNp        *
NpUtilAllocRemoteHwNpMem (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilAllocRemoteHwNpMem: ENTRY\n");
    /* This can be converted to MemPool If Needed */
    MEMSET (&gRemoteHwNp, 0, sizeof (tRemoteHwNp));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilAllocRemoteHwNpMem: EXIT\n");
    return (&gRemoteHwNp);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilRelRemoteHwNpMem                             */
/*                                                                           */
/*     DESCRIPTION      : This function is used to free the memory           */
/*              acquired for the varaible of type tRemoteHwNp      */
/*                                                                           */
/*     INPUT            : Pointer to the Memory of the global structure      */
/*                        variable gRemoteHwNp of type tRemoteHwNp           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilRelRemoteHwNpMem (tRemoteHwNp * pRemoteHwNp)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRelRemoteHwNpMem: ENTRY\n");
    UNUSED_PARAM (pRemoteHwNp);
    /* This can be converted to MemPool If Needed */
    MEMSET (&gRemoteHwNp, 0, sizeof (tRemoteHwNp));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRelRemoteHwNpMem: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteClntHwNpInMemAlloc                         */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the memory            */
/*                        for the variable of type tRemoteHwNp               */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Pointer to the Memory of the global structure      */
/*                        variable gRemoteSvcInputHwNp of type tRemoteHwNp   */
/*                                                                           */
/*****************************************************************************/
tRemoteHwNp        *
NpRemoteClntHwNpInMemAlloc (VOID)
{
    tRemoteHwNp        *pRemoteHwNp = NULL;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpInMemAlloc: ENTRY\n");
    /* This can be converted to MemPool If Needed */

    pRemoteHwNp = MEM_MALLOC (sizeof (tRemoteHwNp), tRemoteHwNp);

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpInMemAlloc: EXIT\n");
    return (pRemoteHwNp);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteClntHwNpOutMemAlloc                        */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the memory            */
/*                        for the variable of type tRemoteHwNp               */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Pointer to the Memory of the global structure      */
/*                        variable gRemoteSvcOutputHwNp of type tRemoteHwNp  */
/*                                                                           */
/*****************************************************************************/
tRemoteHwNp        *
NpRemoteClntHwNpOutMemAlloc (VOID)
{
    /* This can be converted to MemPool If Needed */
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpOutMemAlloc: ENTRY\n");
    MEMSET (&gRemoteSvcOutputHwNp, 0, sizeof (tRemoteHwNp));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpOutMemAlloc: EXIT\n");
    return (&gRemoteSvcOutputHwNp);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteExecSemCreate                              */
/*                                                                           */
/*     DESCRIPTION      : This function creates a semaphore for the          */
/*                        Remote NP call execution                           */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
NpRemoteExecSemCreate (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemCreate: ENTRY\n");
    /* NPRC - NP Remote Client  */
    if (OsixCreateSem ((CONST UINT1 *) "NPRC", 1, OSIX_GLOBAL,
                       &gRemoteExecSemId) != OSIX_SUCCESS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpRemoteExecSemCreate: OsixCreateSem failed \n");
        return OSIX_FAILURE;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemCreate: EXIT\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteExecSemLock                                */
/*                                                                           */
/*     DESCRIPTION      : This function is used to acquire the semaphore     */
/*                        for Remote NP call execution                       */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : FNP_SUCCESS/FNP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
NpRemoteExecSemLock (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemLock: ENTRY\n");
    if (gRemoteExecSemId == 0)
    {
        if (NpRemoteExecSemCreate () != OSIX_SUCCESS)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR, "NpRemoteExecSemCreate failed \n");
            return FNP_FAILURE;
        }
    }
    if (OsixSemTake (gRemoteExecSemId) != OSIX_SUCCESS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpRemoteExecSemLock: OsixSemTake failed \n");
        return FNP_FAILURE;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemLock: EXIT\n");
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteExecSemUnLock                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to release the               */
/*                        Remote NP call execution semaphore                 */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
NpRemoteExecSemUnLock (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemUnLock: ENTRY\n");
    OsixSemGive (gRemoteExecSemId);
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteExecSemUnLock: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteClntHwNpInMemRel                           */
/*                                                                           */
/*     DESCRIPTION      : This function is used to free the memory           */
/*                        acquired for the variable of type tRemoteHwNp      */
/*                                                                           */
/*     INPUT            : Pointer to the memory of the Global structure      */
/*                        variable gRemoteSvcInputHwNp of type tRemoteHwNp   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpRemoteClntHwNpInMemRel (tRemoteHwNp * pRemoteHwNp)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpInMemRel: ENTRY\n");
    UNUSED_PARAM (pRemoteHwNp);
    MEM_FREE (pRemoteHwNp);
    /* This can be converted to MemPool If Needed */
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpInMemRel: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpRemoteClntHwNpOutMemRel                          */
/*                                                                           */
/*     DESCRIPTION      : This function is used to free the memory           */
/*                        acquired for the variable of type tRemoteHwNp      */
/*                                                                           */
/*     INPUT            : Pointer to the memory of the Global structure      */
/*                        variable gRemoteSvcOutputHwNp of type tRemoteHwNp  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpRemoteClntHwNpOutMemRel (tRemoteHwNp * pRemoteHwNp)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpOutMemRel: ENTRY\n");
    UNUSED_PARAM (pRemoteHwNp);
    /* This can be converted to MemPool If Needed */
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpRemoteClntHwNpOutMemRel: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpGetCallHwNpMemAlloc                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the memory            */
/*                        for the variable of type tRemoteHwNp               */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Pointer to the Memory of the global structure      */
/*                        variable gGetCallUpdateHwNp of type tRemoteHwNp    */
/*                                                                           */
/*****************************************************************************/
tRemoteHwNp        *
NpGetCallHwNpMemAlloc (VOID)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpGetCallHwNpMemAlloc: ENTRY\n");
    /* This can be converted to MemPool If Needed */
    MEMSET (&gGetCallUpdateHwNp, 0, sizeof (tRemoteHwNp));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpGetCallHwNpMemAlloc: EXIT\n");
    return (&gGetCallUpdateHwNp);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpGetCallHwNpMemRel                                */
/*                                                                           */
/*     DESCRIPTION      : This function is used to free the memory           */
/*                        acquired for the variable of type tRemoteHwNp      */
/*                                                                           */
/*     INPUT            : Pointer to the memory of the Global structure      */
/*                        variable gGetCallUpdateHwNp of type tRemoteHwNp    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpGetCallHwNpMemRel (tRemoteHwNp * pRemoteHwNp)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpGetCallHwNpMemRel: ENTRY\n");
    UNUSED_PARAM (pRemoteHwNp);
    /* This can be converted to MemPool If Needed */
    MEMSET (&gGetCallUpdateHwNp, 0, sizeof (tRemoteHwNp));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpGetCallHwNpMemRel: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIsPortChannel                                */
/*                                                                           */
/*     DESCRIPTION      : This function is used to check whether the         */
/*              given interface index is port channel or NOT       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : FNP_SUCCESS/FNP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
NpUtilIsPortChannel (UINT4 u4IfIndex)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIsPortChannel: ENTRY\n");
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "IfIndex is %d\n", u4IfIndex);
    if ((u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
        (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIsPortChannel: EXIT\n");
        return FNP_SUCCESS;
    }
    if ((u4IfIndex >= CFA_MIN_PSW_IF_INDEX) &&
        (u4IfIndex <= CFA_MAX_PSW_IF_INDEX))
    {
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                    "NpUtilIsPortChannel: PW Interface EXIT\n");
        return FNP_SUCCESS;
    }
    NPUTIL_DBG (NPUTIL_DBG_ERR,
                "NpUtilIsPortChannel: Interface is not a  port channel\n");
    return FNP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIsRemotePort                                 */
/*                                                                           */
/*     DESCRIPTION      : This function is used to check whether the         */
/*               given port is Remote port or NOT                   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*              pLocalUnitHwPortRange - pointer of type            */
/*                                       tHwPortInfo                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : FNP_SUCCESS/FNP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
NpUtilIsRemotePort (UINT4 u4IfIndex, tHwPortInfo * pLocalUnitHwPortRange)
{
    INT4                i4Index = 0;
    UINT4               u4PhyPort = 0;
    UINT2               u2Svid = 0;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIsRemotePort: ENTRY\n");
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "IfIndex is %d\n", u4IfIndex);

    if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
        (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
    {
        VlanApiGetSChInfoFromSChIfIndex (u4IfIndex, &u4PhyPort, &u2Svid);
        u4IfIndex = u4PhyPort;
    }

    UNUSED_PARAM (u2Svid);

    if ((u4IfIndex >= pLocalUnitHwPortRange->u4StartIfIndex) &&
        (u4IfIndex <= pLocalUnitHwPortRange->u4EndIfIndex))
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilIsRemotePort :Not a valid interface\n");
        return FNP_FAILURE;
    }

    /* Connecting ports of self Node should not be treated as Remote port */
    for (i4Index = 0; i4Index < SYS_DEF_MAX_INFRA_SYS_PORT_COUNT; i4Index++)
    {
        /* Return Failure if the Port is one of the connecting port */
        if (pLocalUnitHwPortRange->au4ConnectingPortIfIndex[i4Index] ==
            u4IfIndex)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilIsRemotePort: Connecting port.Hence return failure\n");
            return FNP_FAILURE;

        }
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIsRemotePort: EXIT\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilCheckIsValidRemotePort                       */
/*                                                                           */
/*     DESCRIPTION      : This function is used to check whether the         */
/*                        given port is a valid  Remote(Physical) port       */
/*                                                                           */
/*     INPUT            : u4IfIndex - If Index of the interface              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : FNP_TRUE - Given port is a valid Physical port in  */
/*                                   Remote Unit                             */
/*                                                                           */
/*                        FNP_FALSE - Given port is not a valid Remote Port  */
/*                                    It cannot be assumed that the given    */
/*                                    port is a valid port in Self Unit      */
/*                                                                           */
/*****************************************************************************/
UINT1
NpUtilCheckIsValidRemotePort (UINT4 u4IfIndex)
{
    tHwPortInfo         HwPortInfo;

    INT4                i4Index = 0;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilCheckIsValidRemotePort: ENTRY\n");
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "IfIndex is %d\n", u4IfIndex);

    CfaGetLocalUnitPortInformation (&HwPortInfo);

    if ((u4IfIndex >= HwPortInfo.u4StartIfIndex) &&
        (u4IfIndex <= HwPortInfo.u4EndIfIndex))
    {
        /* Valid Physical port in Self Unit */
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilCheckIsValidRemotePort: Valid physical port in self unit\n");
        return FNP_FALSE;
    }
    /* Check if the port belongs to any of the connecting Port of self Unit */
    for (i4Index = 0; i4Index < SYS_DEF_MAX_INFRA_SYS_PORT_COUNT; i4Index++)
    {
        if (HwPortInfo.au4ConnectingPortIfIndex[i4Index] == u4IfIndex)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilCheckIsValidRemotePort: Port is connecting port of self unit\n");
            return FNP_FALSE;
        }
    }

    if ((u4IfIndex > 0) && (u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        /* Valid Remote Port - Physical Iterface */
        NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                    "NpUtilCheckIsValidRemotePort: EXIT\n");
        return FNP_TRUE;
    }

    /* Invalid Port */
    NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilCheckIsValidRemotePort: Invalid port\n");
    return FNP_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilGetStackingPortIndex                         */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the Stacking port     */
/*               of the Local unit                     */
/*                                                                           */
/*     INPUT            : pLocalUnitHwPortRange - pointer of type            */
/*                             tHwPortInfo             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Interface Index of Connecting Port of the          */
/*              Local Unit                                         */
/*                                                                           */
/*****************************************************************************/
INT4
NpUtilGetStackingPortIndex (tHwPortInfo * pLocalUnitHwPortRange, INT4 i4Index)
{
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilGetStackingPortIndex: ENTRY\n");
    /* This function can used to choose the Connecting Stacking Port to be used
       for Dual Unit Stacking .
       At Present first port is returned as Default Connecting Port */
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "Connecting port IfIndex is %d\n",
                 pLocalUnitHwPortRange->au4ConnectingPortIfIndex[0]);
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilGetStackingPortIndex: EXIT\n");
    return pLocalUnitHwPortRange->au4ConnectingPortIfIndex[i4Index];
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilGetRemoteStackingPortIndex                   */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the stackinig port    */
/*              of the Remote unit                     */
/*                                                                           */
/*     INPUT            : pHwPort of type tHwPortInfo                        */
/*                                                                           */
/*     OUTPUT           : pHwPort of type tHwPortInfo                        */
/*                                                                           */
/*     RETURNS          : Interface Index of Connecting Port of the          */
/*               Remote Unit                         */
/*                                                                           */
/*****************************************************************************/
INT4
NpUtilGetRemoteStackingPortIndex (tHwPortInfo * pHwPort)
{
#ifdef CFA_WANTED
    tCfaRemoteNpWrCfaNpGetHwPortInfo *pEntry = NULL;
    INT4                i4RpcCallStatus = OSIX_WAIT;
    UINT1               u1IPCSyncStatus = OSIX_TRUE;
#endif

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilGetRemoteStackingPortIndex: ENTRY\n");

    if (STANDBY_NOT_PRESENT == CfaGetPeerNodeCount ())
    {
        gRemoteStackingPort = FNP_RESET;
        MEMSET (&(gRemoteHwPortInfo), 0, sizeof (tHwPortInfo));
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilGetRemoteStackingPortIndex: standby node not present\n");
        return FNP_ZERO;
    }

    if (gRemoteStackingPort == FNP_SET)
    {
        MEMCPY (&(pHwPort->au4ConnectingPortIfIndex),
                &(gRemoteHwPortInfo.au4ConnectingPortIfIndex), sizeof (UINT4));
    }
    else
    {
#ifdef CFA_WANTED
        tRemoteHwNp        *pRemoteStackingHwNp;
        tRemoteHwNp         RemoteHwNp;

        MEMSET (&RemoteHwNp, 0, sizeof (tRemoteHwNp));
        MEMCPY (&RemoteHwNp, &gRemoteHwNp, sizeof (tRemoteHwNp));

        /* Check whether IPC call Invocation is allowed for NPAPI
           execution in Stand-by . IPC sync status will be disabled
           during protocol restart in which NP sync should not be
           done to remote unit. */
        NpUtilGetIPCSyncStatus (&u1IPCSyncStatus);
        if (u1IPCSyncStatus == OSIX_FALSE)
        {
            /* When IPC Sync up is disabled , no remote invocation is allowed. */
            NPUTIL_DBG (NPUTIL_DBG_ERR,
                        "NpUtilGetRemoteStackingPortIndex: IPC sync up is disabled\n");
            return FNP_ZERO;
        }

        pRemoteStackingHwNp = NpUtilAllocRemoteHwNpMem ();

        if (pRemoteStackingHwNp == NULL)
        {
            NPUTIL_DBG (NPUTIL_DBG_ERR, "RemoteStackingHwNp is NULL\n");
            return FNP_ZERO;
        }

        pEntry =
            &pRemoteStackingHwNp->
            CfaRemoteNpModInfo.CfaRemoteNpCfaNpGetHwPortInfo;

        pRemoteStackingHwNp->NpModuleId = NP_CFA_MOD;
        pRemoteStackingHwNp->u4Opcode = CFA_NP_GET_HW_PORT_INFO;
        pRemoteStackingHwNp->i4Length =
            sizeof (UINT4) + sizeof (INT4) + sizeof (tNpModule) +
            sizeof (tCfaRemoteNpModInfo);

        /* Fill the self unit stack port information */
        CfaNpGetHwPortInfo (&(pEntry->HwPortInfo));
        if ((NpUtilRemoteClntHwProgram (pRemoteStackingHwNp, i4RpcCallStatus))
            == FNP_FAILURE)
        {
            NpUtilRelRemoteHwNpMem (pRemoteStackingHwNp);
            NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilRemoteClntHwProgram failed\n");
            return 0;
        }
        MEMSET (&(gRemoteHwPortInfo), 0, sizeof (tHwPortInfo));
        MEMCPY (&(gRemoteHwPortInfo), &(pEntry->HwPortInfo),
                sizeof (tHwPortInfo));
        MEMCPY (&(pHwPort->au4ConnectingPortIfIndex),
                &(gRemoteHwPortInfo.au4ConnectingPortIfIndex), sizeof (UINT4));
        gRemoteStackingPort = FNP_SET;
        NpUtilRelRemoteHwNpMem (pRemoteStackingHwNp);
        MEMCPY (&gRemoteHwNp, &RemoteHwNp, sizeof (tRemoteHwNp));
#endif
    }
    NPUTIL_DBG1 (NPUTIL_DBG_MSG, "Connecting port IfIndex is %d\n",
                 pHwPort->au4ConnectingPortIfIndex[0]);

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilGetRemoteStackingPortIndex: EXIT\n");
    return pHwPort->au4ConnectingPortIfIndex[0];
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilMaskHwPortArray                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to separate the Local and    */
/*              Remote ports to the local h/w port array           */
/*              and Remote h/w port array respectively             */
/*                                                                           */
/*     INPUT            : pLocalHwPorts of type tHwPortArray                 */
/*               pRemoteHwPorts of type tRemoteHwPortArray          */
/*                  pHwPortInfo of type tHwPortInfo                    */
/*                                                                           */
/*     OUTPUT           : pLocalHwPorts - Updated with Local ports alone     */
/*               pRemoteHwPorts - Updated with Remote ports alone   */
/*               pu4LocalPortCount - Pointer to the Number of       */
/*              Local Ports                                        */
/*              pu4RemotePortCount - Pointer to the Number of      */
/*              Remote Ports                         */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilMaskHwPortArray (tHwPortArray * pLocalHwPorts,
                       tRemoteHwPortArray * pRemoteHwPorts,
                       tHwPortInfo * pHwPortInfo, UINT4 *pu4LocalPortCount,
                       UINT4 *pu4RemotePortCount)
{
    INT4                i4Arrayidx = 0;
    INT4                i4LastValidLocalArrayidx = 0;
    INT4                i4LastValidRemoteArrayidx = 0;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskHwPortArray: ENTRY\n");

    if ((pLocalHwPorts->i4Length > VLAN_MAX_PORTS)
        || (pRemoteHwPorts->i4Length > VLAN_MAX_PORTS))
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilMaskHwPortArray: Invalid ports\n");
        return;
    }

    MEMSET (&(gLocalPortArray), 0, sizeof (gLocalPortArray));
    MEMSET (&(gRemotePortArray), 0, sizeof (gRemotePortArray));

    for (i4Arrayidx = 0; i4Arrayidx < pLocalHwPorts->i4Length; i4Arrayidx++)
    {

        if ((NpUtilIsPortChannel (pLocalHwPorts->pu4PortArray[i4Arrayidx]) ==
             FNP_SUCCESS))
        {
            /* Port channel is added to Local and Remote Port Array */
            gLocalPortArray.au4PortArray[i4LastValidLocalArrayidx] =
                pLocalHwPorts->pu4PortArray[i4Arrayidx];
            i4LastValidLocalArrayidx++;

            gRemotePortArray.au4PortArray[i4LastValidRemoteArrayidx] =
                pLocalHwPorts->pu4PortArray[i4Arrayidx];
            i4LastValidRemoteArrayidx++;
        }
        else if (NpUtilIsRemotePort (pLocalHwPorts->pu4PortArray[i4Arrayidx],
                                     pHwPortInfo) == FNP_SUCCESS)
        {
            gRemotePortArray.au4PortArray[i4LastValidRemoteArrayidx] =
                pLocalHwPorts->pu4PortArray[i4Arrayidx];
            i4LastValidRemoteArrayidx++;
        }
        else
        {
            gLocalPortArray.au4PortArray[i4LastValidLocalArrayidx] =
                pLocalHwPorts->pu4PortArray[i4Arrayidx];
            i4LastValidLocalArrayidx++;
        }
    }

    if (pLocalHwPorts->pu4PortArray != NULL)
    {
        MEMCPY ((pLocalHwPorts->pu4PortArray),
                &(gLocalPortArray.au4PortArray[0]),
                ((i4LastValidLocalArrayidx) * sizeof (UINT4)));
        *pu4LocalPortCount = i4LastValidLocalArrayidx;
        pLocalHwPorts->i4Length = i4LastValidLocalArrayidx;
    }

    MEMCPY (&(pRemoteHwPorts->au4PortArray[0]),
            &(gRemotePortArray.au4PortArray[0]),
            ((i4LastValidRemoteArrayidx) * sizeof (UINT4)));

    *pu4RemotePortCount = i4LastValidRemoteArrayidx;
    pRemoteHwPorts->i4Length = i4LastValidRemoteArrayidx;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskHwPortArray: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilMaskAndAddStackingPort                       */
/*                                                                           */
/*     DESCRIPTION      : This function is used to separate the Local ports  */
/*               and Remote ports to the Local h/w port array       */
/*              and Remote h/w port array respectively. Also       */
/*              adds the corresponding stacking port to          */
/*              both the arrays                     */
/*                                                                           */
/*     INPUT            : pLocalHwPorts of type tHwPortArray                 */
/*                        pRemoteHwPorts of type tRemoteHwPortArray          */
/*                   pHwPortInfo of type tHwPortInfo                    */
/*                                         */
/*     OUTPUT           : pLocalHwPorts - Updated with Local ports and       */
/*              stacking port                         */
/*                        pRemoteHwPorts - Updated with Remote ports and     */
/*              stacking port                         */
/*                        pu4LocalPortCount - Pointer to the Number of       */
/*                        Local Ports                                        */
/*                        pu4RemotePortCount - Pointer to the Number of      */
/*                        Remote Ports                         */
/*                                         */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilMaskAndAddStackingPort (tHwPortArray * pLocalHwPorts,
                              tRemoteHwPortArray * pRemoteHwPorts,
                              tHwPortInfo * pHwPortInfo,
                              UINT4 *pu4LocalPortCount,
                              UINT4 *pu4RemotePortCount)
{
    INT4                i4Index = 0;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskAndAddStackingPort: ENTRY\n");
    NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts, pHwPortInfo,
                           pu4LocalPortCount, pu4RemotePortCount);
    /* Local Stack Port is updated in the Port Array */
    if (pLocalHwPorts->pu4PortArray != NULL)
    {
        pLocalHwPorts->pu4PortArray[*pu4LocalPortCount] =
            NpUtilGetStackingPortIndex (pHwPortInfo, i4Index);
        (*pu4LocalPortCount)++;
        pLocalHwPorts->i4Length++;
    }
    /* Remote stack port is updated in the Port Array */
    pRemoteHwPorts->au4PortArray[*pu4RemotePortCount] =
        NpUtilGetRemoteStackingPortIndex (pHwPortInfo);
    (*pu4RemotePortCount)++;
    pRemoteHwPorts->i4Length++;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskAndAddStackingPort: EXIT\n");
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilMaskAndAddStkPortInRemPorts                  */
/*                                                                           */
/*     DESCRIPTION      : This function is used to mask the Local ports      */
/*               in the remote port list and adds the remote stacking port   */
/*                                                                           */
/*     INPUT            : pRemoteHwPorts of type tRemoteHwPortArray          */
/*                   pHwPortInfo of type tHwPortInfo                    */
/*                                         */
/*     OUTPUT           : pRemoteHwPorts - Updated with Remote ports and     */
/*              stacking port                         */
/*                        pu4RemotePortCount - Pointer to the Number of      */
/*                        Remote Ports                         */
/*                                         */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilMaskAndAddStkPortInRemPorts (tRemoteHwPortArray * pRemoteHwPorts,
                                   tHwPortInfo * pHwPortInfo,
                                   UINT4 *pu4RemotePortCount)
{
    INT4                i4HwPortArrayLen = pRemoteHwPorts->i4Length;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilMaskAndAddStkPortInRemPorts: ENTRY\n");

    NpUtilMaskRemoteHwPortArray (pRemoteHwPorts, pHwPortInfo,
                                 pu4RemotePortCount);
    if ((*pu4RemotePortCount <= (UINT4) i4HwPortArrayLen) &&
        (*pu4RemotePortCount < VLAN_MAX_PORTS))
    {
        /* Remote stack port is updated in the Port Array */
        pRemoteHwPorts->au4PortArray[*pu4RemotePortCount] =
            NpUtilGetRemoteStackingPortIndex (pHwPortInfo);
        (*pu4RemotePortCount)++;
        pRemoteHwPorts->i4Length++;
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC,
                "NpUtilMaskAndAddStkPortInRemPorts: EXIT\n");
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilMaskRemoteHwPortArray                        */
/*                                                                           */
/*     DESCRIPTION      : This function is used to mask the local ports in   */
/*              the remote hw port array                                     */
/*                                                                           */
/*     INPUT            : pRemoteHwPorts of type tRemoteHwPortArray          */
/*                        pHwPortInfo of type tHwPortInfo                    */
/*                                                                           */
/*     OUTPUT           : pRemoteHwPorts - Updated with Remote ports alone   */
/*                        pu4RemotePortCount - Pointer to the Number of      */
/*                        Remote Ports                         */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilMaskRemoteHwPortArray (tRemoteHwPortArray * pRemoteHwPorts,
                             tHwPortInfo * pHwPortInfo,
                             UINT4 *pu4RemotePortCount)
{
    INT4                i4Arrayidx = 0;
    INT4                i4LastValidRemoteArrayidx = 0;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskRemoteHwPortArray: ENTRY\n");

    if (pRemoteHwPorts->i4Length > VLAN_MAX_PORTS)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR,
                    "NpUtilMaskRemoteHwPortArray:Invalid ports\n");
        return;
    }

    MEMSET (&(gRemotePortArray), 0, sizeof (gRemotePortArray));

    for (i4Arrayidx = 0; i4Arrayidx < pRemoteHwPorts->i4Length; i4Arrayidx++)
    {
        if (i4Arrayidx < L2IWF_MAX_PORTS_PER_CONTEXT)
        {
            if ((NpUtilIsPortChannel (pRemoteHwPorts->au4PortArray[i4Arrayidx])
                 == FNP_SUCCESS))
            {
                /* Port channel is added to Local and Remote Port Array */
                gRemotePortArray.au4PortArray[i4LastValidRemoteArrayidx] =
                    pRemoteHwPorts->au4PortArray[i4Arrayidx];
                i4LastValidRemoteArrayidx++;
            }
            else if (NpUtilIsRemotePort
                     (pRemoteHwPorts->au4PortArray[i4Arrayidx],
                      pHwPortInfo) == FNP_SUCCESS)
            {
                gRemotePortArray.au4PortArray[i4LastValidRemoteArrayidx] =
                    pRemoteHwPorts->au4PortArray[i4Arrayidx];
                i4LastValidRemoteArrayidx++;
            }
        }
    }

    MEMCPY (&(pRemoteHwPorts->au4PortArray[0]),
            &(gRemotePortArray.au4PortArray[0]),
            ((i4LastValidRemoteArrayidx) * sizeof (UINT4)));

    *pu4RemotePortCount = i4LastValidRemoteArrayidx;
    pRemoteHwPorts->i4Length = i4LastValidRemoteArrayidx;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskRemoteHwPortArray: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilRemoveStackPort                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to remove the stack port     */
/*              in the given the h/w port array             */
/*                                                                           */
/*     INPUT            : pLocalHwPorts of type tHwPortArray                 */
/*                                                                           */
/*     OUTPUT           : pLocalHwPorts - Pointer to the port array          */
/*                        with no stackinig port         */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilRemoveStackPort (tHwPortArray * pLocalHwPorts)
{
    INT4                i4Arrayidx = 0;
    INT4                i4LastValidArrayidx = 0;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoveStackPort: ENTRY\n");

    CfaGetLocalUnitPortInformation (&LocalHwPortInfo);
    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);
    if (pLocalHwPorts->i4Length > L2IWF_MAX_PORTS_PER_CONTEXT)
    {
        NPUTIL_DBG (NPUTIL_DBG_ERR, "NpUtilRemoveStackPort:Invalid ports\n");
        return;
    }

    MEMSET (&(gLocalPortArray), 0, sizeof (gLocalPortArray));

    for (i4Arrayidx = 0; i4Arrayidx < pLocalHwPorts->i4Length; i4Arrayidx++)
    {
        if ((pLocalHwPorts->pu4PortArray[i4Arrayidx] != u4LocalStackPort)
            && (pLocalHwPorts->pu4PortArray[i4Arrayidx] != u4RemoteStackPort)
            && (i4LastValidArrayidx < L2IWF_MAX_PORTS_PER_CONTEXT)
            && (i4Arrayidx < L2IWF_MAX_PORTS_PER_CONTEXT))
        {
            gLocalPortArray.au4PortArray[i4LastValidArrayidx] =
                pLocalHwPorts->pu4PortArray[i4Arrayidx];
            i4LastValidArrayidx++;
        }
    }

    pLocalHwPorts->i4Length = i4LastValidArrayidx;

    if ((pLocalHwPorts->pu4PortArray != NULL)
        && (pLocalHwPorts->i4Length < L2IWF_MAX_PORTS_PER_CONTEXT))
    {
        MEMCPY ((pLocalHwPorts->pu4PortArray),
                &(gLocalPortArray.au4PortArray[0]),
                ((pLocalHwPorts->i4Length) * sizeof (UINT4)));
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilRemoveStackPort: EXIT\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilMaskHwPortList                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to separate the Local and    */
/*              Remote ports to the local h/w port list            */
/*              and Remote h/w port list respectively              */
/*                                                                           */
/*     INPUT            : pu1LocalUnitHwPortList, pu1RemoteUnitHwPortList,   */
/*               pLocalUnitHwPortRange of type tHwPortInfo,         */
/*               u4ListSize                         */
/*                                                                           */
/*     OUTPUT           : pu1LocalUnitHwPortList, pu1RemoteUnitHwPortList,   */
/*              pu4LocalPortCount - Pointer to the Number of       */
/*                          Local ports             */
/*                pu4RemotePortCount - Pointer to the Number of      */
/*                           Remote ports                 */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilMaskHwPortList (UINT1 *pu1LocalUnitHwPortList,
                      UINT1 *pu1RemoteUnitHwPortList,
                      tHwPortInfo * pLocalUnitHwPortRange,
                      UINT4 *pu4LocalPortCount,
                      UINT4 *pu4RemotePortCount, UINT4 u4ListSize)
{
    INT4                i4LastValidLocalArrayidx = 0;
    INT4                i4LastValidRemoteArrayidx = 0;
    BOOL1               bRetVal;
    UINT4               u2IfIdx;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskHwPortList: ENTRY\n");
    for (u2IfIdx = 1; u2IfIdx < (SYS_DEF_MAX_PHYSICAL_INTERFACES +
                                 LA_MAX_AGG_INTF); u2IfIdx++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1LocalUnitHwPortList, u2IfIdx, u4ListSize,
                                 bRetVal);
        if (bRetVal == OSIX_TRUE)
        {
            if (NpUtilIsPortChannel ((UINT4) u2IfIdx))
            {
                /* Port Channel has to be there in both port list. */
                i4LastValidLocalArrayidx++;
                i4LastValidRemoteArrayidx++;
            }
            else if (NpUtilIsRemotePort ((UINT4) u2IfIdx,
                                         pLocalUnitHwPortRange) == FNP_SUCCESS)
            {
                /* Reset the corresponding bit in Local port */
                OSIX_BITLIST_RESET_BIT (pu1LocalUnitHwPortList, u2IfIdx,
                                        u4ListSize);
                i4LastValidRemoteArrayidx++;
            }
            else
            {
                /* Reset the corresponding bit in Remote port */
                OSIX_BITLIST_RESET_BIT (pu1RemoteUnitHwPortList, u2IfIdx,
                                        u4ListSize);
                i4LastValidLocalArrayidx++;
            }
        }
    }
    *pu4LocalPortCount = i4LastValidLocalArrayidx;
    *pu4RemotePortCount = i4LastValidRemoteArrayidx;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilMaskHwPortList: EXIT\n");

    return;
}

#ifdef ISS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilGetMirrorPortsCount                          */
/*                                                                           */
/*     DESCRIPTION      : This function to get the number of source port and */
/*                        the number of destination ports from the port array*/
/*                                                                           */
/*     INPUT            : pSourceList of type tIssHwSourceId                 */
/*                                                                           */
/*     OUTPUT           : pu4DestList, pu4SrcList,                           */
/*              pu4SrcPortNum, pu4DestPortNum                 */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilGetMirrorPortsCount (tIssHwSourceId * pSourceList,
                           UINT4 *pu4DestList, UINT4 *pu4SrcList,
                           UINT4 *pu4SrcPortNum, UINT4 *pu4DestPortNum)
{
    UINT4               u4IdxCount = 0;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilGetMirrorPortsCount: ENTRY\n");

    for (u4IdxCount = 0; pSourceList[u4IdxCount].u4SourceNo != 0; u4IdxCount++)
    {
        pu4SrcList[u4IdxCount] = pSourceList[u4IdxCount].u4SourceNo;
        u4IdxCount++;
    }
    *pu4SrcPortNum = u4IdxCount;
    u4IdxCount = 0;
    for (u4IdxCount = 0; pu4DestList[u4IdxCount] != 0; u4IdxCount++)
    {
        u4IdxCount++;
    }
    *pu4DestPortNum = u4IdxCount;
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilGetMirrorPortsCount: EXIT\n");
    return;
}

#ifdef ISS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIssHwUpdateL2Filter                   */
/*                                                                           */
/*     DESCRIPTION      : This function seperates the port Lists as Local or */
/*                        remote for L2 filter and fills the pu1NpCallStatus */
/*                        accordingly.                                       */
/*                                                                           */
/*     INPUT            : pIssL2FilterEntry of type tIssL2FilterEntry,       */
/*              pRemoteIssL2FilterEntry of type tIssL2FilterEntry, */
/*              pLocalUnitHwPortRange of type tHwPortInfo          */
/*                                                                           */
/*     OUTPUT           : pu1NpCallStatus                                    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilIssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry,
                           tRemoteIssL2FilterEntry * pRemoteIssL2FilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange,
                           UINT1 *pu1NpCallStatus)
{
    tIssPortList       *pLocalHwInPortList = NULL;
    tIssPortList       *pLocalHwOutPortList = NULL;
    tIssPortList       *pRemoteHwInPortList = NULL;
    tIssPortList       *pRemoteHwOutPortList = NULL;
    tIssPortChannelList *pLocalHwInPortChannelList = NULL;
    tIssPortChannelList *pLocalHwOutPortChannelList = NULL;
    tIssPortChannelList *pRemoteHwInPortChannelList = NULL;
    tIssPortChannelList *pRemoteHwOutPortChannelList = NULL;
    UINT4               u4LocalPortCount;
    UINT4               u4RemotePortCount;
    UINT4               u4EgIfIdx;
    UINT1               u1EgressType;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateL2Filter: ENTRY\n");
/* In Port List */
    pLocalHwInPortList = &pIssL2FilterEntry->IssL2FilterInPortList;
    pRemoteHwInPortList = &pRemoteIssL2FilterEntry->IssL2FilterInPortList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwInPortList,
                          (UINT1 *) pRemoteHwInPortList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }
    else if (u4RemotePortCount != 0)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
    else
    {
        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
    }
/* Out Port List */
    pLocalHwOutPortList = &pIssL2FilterEntry->IssL2FilterOutPortList;
    pRemoteHwOutPortList = &pRemoteIssL2FilterEntry->IssL2FilterOutPortList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwOutPortList,
                          (UINT1 *) pRemoteHwOutPortList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
/* In Port Channel List */
    pLocalHwInPortChannelList =
        &pIssL2FilterEntry->IssL2FilterInPortChannelList;
    pRemoteHwInPortChannelList =
        &pRemoteIssL2FilterEntry->IssL2FilterInPortChannelList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwInPortChannelList,
                          (UINT1 *) pRemoteHwInPortChannelList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_CHANNEL_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }

/* Out Port Channel List */
    pLocalHwOutPortChannelList =
        &pIssL2FilterEntry->IssL2FilterOutPortChannelList;
    pRemoteHwOutPortChannelList =
        &pRemoteIssL2FilterEntry->IssL2FilterOutPortChannelList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwOutPortChannelList,
                          (UINT1 *) pRemoteHwOutPortChannelList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_CHANNEL_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    u1EgressType = pIssL2FilterEntry->RedirectIfGrp.u1EgressIfType;
    u4EgIfIdx = pIssL2FilterEntry->RedirectIfGrp.u4EgressIfIndex;
    /* ISS_REDIRECT_TO_ETHERNET case alone is implemented */
    if (u1EgressType == ISS_REDIRECT_TO_ETHERNET)
    {
        if (NpUtilIsRemotePort (u4EgIfIdx, pLocalUnitHwPortRange) ==
            FNP_SUCCESS)
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }
        else
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }

    }
    /* The following is hit only for S-Channel interface */
    if (pIssL2FilterEntry->u4SChannelIfIndex != 0)
    {
        if (NpUtilIsRemotePort (pIssL2FilterEntry->u4SChannelIfIndex,
                                pLocalUnitHwPortRange) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateL2Filter: EXIT\n");
    UNUSED_PARAM (i4Value);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIssHwUpdateL3Filter                          */
/*                                                                           */
/*     DESCRIPTION      : This function seperates the port Lists as Local or */
/*                        remote for L3 filter and fills the pu1NpCallStatus */
/*                        accordingly.                                       */
/*                                                                           */
/*     INPUT            : pIssL3FilterEntry of type tIssL3FilterEntry,       */
/*               pRemoteIssL3FilterEntry of type tIssL3FilterEntry, */
/*              pLocalUnitHwPortRange of type tHwPortInfo         */
/*                                                                           */
/*     OUTPUT           : pu1NpCallStatus                                    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilIssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry,
                           tRemoteIssL3FilterEntry * pRemoteIssL3FilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange,
                           UINT1 *pu1NpCallStatus)
{
    tIssPortList       *pLocalHwInPortList = NULL;
    tIssPortList       *pLocalHwOutPortList = NULL;
    tIssPortList       *pRemoteHwInPortList = NULL;
    tIssPortList       *pRemoteHwOutPortList = NULL;
    tIssPortChannelList *pLocalHwInPortChannelList = NULL;
    tIssPortChannelList *pLocalHwOutPortChannelList = NULL;
    tIssPortChannelList *pRemoteHwInPortChannelList = NULL;
    tIssPortChannelList *pRemoteHwOutPortChannelList = NULL;
    UINT4               u4LocalPortCount;
    UINT4               u4RemotePortCount;
    UINT4               u4EgIfIdx;
    UINT1               u1EgressType;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateL3Filter: ENTRY\n");

/* In Port Lsit */
    pLocalHwInPortList = &pIssL3FilterEntry->IssL3FilterInPortList;
    pRemoteHwInPortList = &pRemoteIssL3FilterEntry->IssL3FilterInPortList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwInPortList,
                          (UINT1 *) pRemoteHwInPortList,
                          pLocalUnitHwPortRange,
                          &u4LocalPortCount, &u4RemotePortCount,
                          ISS_PORT_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }
    else if (u4RemotePortCount != 0)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
    else
    {
        *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
    }

/* Out Port List */
    pLocalHwOutPortList = &pIssL3FilterEntry->IssL3FilterOutPortList;
    pRemoteHwOutPortList = &pRemoteIssL3FilterEntry->IssL3FilterOutPortList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwOutPortList,
                          (UINT1 *) pRemoteHwOutPortList,
                          pLocalUnitHwPortRange,
                          &u4LocalPortCount, &u4RemotePortCount,
                          ISS_PORT_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
/* In Port Channel List */
    pLocalHwInPortChannelList =
        &pIssL3FilterEntry->IssL3FilterInPortChannelList;
    pRemoteHwInPortChannelList =
        &pRemoteIssL3FilterEntry->IssL3FilterInPortChannelList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwInPortChannelList,
                          (UINT1 *) pRemoteHwInPortChannelList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_CHANNEL_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }

/* Out Port Channel List */
    pLocalHwOutPortChannelList =
        &pIssL3FilterEntry->IssL3FilterOutPortChannelList;
    pRemoteHwOutPortChannelList =
        &pRemoteIssL3FilterEntry->IssL3FilterOutPortChannelList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwOutPortChannelList,
                          (UINT1 *) pRemoteHwOutPortChannelList,
                          pLocalUnitHwPortRange, &u4LocalPortCount,
                          &u4RemotePortCount, ISS_PORT_CHANNEL_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    else if (u4RemotePortCount != 0)
    {
        if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
            (*pu1NpCallStatus == NPUTIL_NO_NP_INVOKE))
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
        }
    }
    u1EgressType = pIssL3FilterEntry->RedirectIfGrp.u1EgressIfType;
    u4EgIfIdx = pIssL3FilterEntry->RedirectIfGrp.u4EgressIfIndex;
    /* ISS_REDIRECT_TO_ETHERNET case alone is implemented */
    if (u1EgressType == ISS_REDIRECT_TO_ETHERNET)
    {
        if (NpUtilIsRemotePort (u4EgIfIdx, pLocalUnitHwPortRange) ==
            FNP_SUCCESS)
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }
        else
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }

    }
    if (pIssL3FilterEntry->u4SChannelIfIndex != 0)
    {
        if (NpUtilIsRemotePort (pIssL3FilterEntry->u4SChannelIfIndex,
                                pLocalUnitHwPortRange) == FNP_SUCCESS)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
        }
        else
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
    }

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateL3Filter: EXIT\n");
    UNUSED_PARAM (i4Value);
    return;
}
#endif /* ISS_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIssHwUpdateUDFilter                          */
/*                                                                           */
/*     DESCRIPTION      : This function seperates the port Lists as Local or */
/*                        remote for UD filter and fills the pu1NpCallStatus */
/*                        accordingly.                                       */
/*                                                                           */
/*     INPUT            : pIssUDBFilterEntry of type tIssUDBFilterEntry,     */
/*              pRemoteIssUDBFilterEntry of type             */
/*              tIssUDBFilterEntry,                                */
/*              pLocalUnitHwPortRange of type tHwPortInfo         */
/*                                                                           */
/*     OUTPUT           : pu1NpCallStatus                                    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilIssHwUpdateUDFilter (tIssUDBFilterEntry * pIssUDBFilterEntry,
                           tIssUDBFilterEntry * pRemoteIssUDBFilterEntry,
                           INT4 i4Value,
                           tHwPortInfo * pLocalUnitHwPortRange,
                           UINT1 *pu1NpCallStatus)
{
    tIssPortList       *pLocalHwInPortList = NULL;
    tIssPortList       *pRemoteHwInPortList = NULL;
    UINT4               u4LocalPortCount;
    UINT4               u4RemotePortCount;
    UINT4               u4EgIfIdx;
    UINT1               u1EgressType;

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateUDFilter: ENTRY\n");
    pLocalHwInPortList = &pIssUDBFilterEntry->IssUdbFilterInPortList;
    pRemoteHwInPortList = &pRemoteIssUDBFilterEntry->IssUdbFilterInPortList;
    NpUtilMaskHwPortList ((UINT1 *) pLocalHwInPortList,
                          (UINT1 *) pRemoteHwInPortList,
                          pLocalUnitHwPortRange,
                          &u4LocalPortCount, &u4RemotePortCount,
                          ISS_PORT_LIST_SIZE);
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    }
    else if (u4LocalPortCount != 0)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }
    else
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
    }
    u1EgressType = pIssUDBFilterEntry->RedirectIfGrp.u1EgressIfType;
    u4EgIfIdx = pIssUDBFilterEntry->RedirectIfGrp.u4EgressIfIndex;
    /* ISS_REDIRECT_TO_ETHERNET case alone is implemented */
    if (u1EgressType == ISS_REDIRECT_TO_ETHERNET)
    {
        if (NpUtilIsRemotePort (u4EgIfIdx, pLocalUnitHwPortRange) ==
            FNP_SUCCESS)
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }
        else
        {
            if ((*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP) ||
                (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP))
            {
                /* Limitation - should not add both Local and Remote ports, 
                 * if action is redirect */
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
        }

    }
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIssHwUpdateUDFilter: EXIT\n");
    UNUSED_PARAM (i4Value);
    return;
}
#endif /* ISS_WANTED */

#ifdef IGS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIgsHwUpdateIpmcEntry                         */
/*                                                                           */
/*     DESCRIPTION      : Utility function for masking the ports/portlist    */
/*                        for the Active/Stby NP call execution in the       */
/*                        Dual Unit Stacking environment.                    */
/*                        This can only be used for the                      */
/*                        FS_MI_IGS_HW_UPDATE_IPMC_ENTRY  case               */
/*                                                                           */
/*     INPUT            : pIgsHwIpFwdInfo of type tIgsHwIpFwdInfo,           */
/*                        pRemoteIgsHwIpFwdInfo of type tIgsHwIpFwdInfo,     */
/*                        pHwPortInfo of type tHwPortInfo,                   */
/*                                                                           */
/*     OUTPUT           : pu1NpCallStatus                                    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
NpUtilIgsHwUpdateIpmcEntry (tIgsHwIpFwdInfo * pIgsHwIpFwdInfo,
                            tIgsHwIpFwdInfo * pRemoteIgsHwIpFwdInfo,
                            tHwPortInfo * pHwPortInfo, UINT1 *pu1NpCallStatus)
{
    tPortList          *pPortList = NULL;
    tPortList          *pUntagPortList = NULL;
    tPortList          *pRemotePortList = NULL;
    tPortList          *pRemoteUntagPortList = NULL;
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    /* Get the stacking port of the Local and Remote unit */
    CfaNpGetHwPortInfo (&LocalHwPortInfo);
    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIgsHwUpdateIpmcEntry: ENTRY\n");
    pPortList = &pIgsHwIpFwdInfo->PortList;
    pRemotePortList = &pRemoteIgsHwIpFwdInfo->PortList;
    NpUtilMaskHwPortList ((UINT1 *) pPortList, (UINT1 *) pRemotePortList,
                          pHwPortInfo, &u4LocalPortCount, &u4RemotePortCount,
                          BRG_PORT_LIST_SIZE);
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
    OSIX_BITLIST_SET_BIT ((*pPortList), u4LocalStackPort, BRG_PORT_LIST_SIZE);
    OSIX_BITLIST_SET_BIT ((*pRemotePortList), u4RemoteStackPort,
                          BRG_PORT_LIST_SIZE);

    pUntagPortList = &pIgsHwIpFwdInfo->UntagPortList;
    pRemoteUntagPortList = &pRemoteIgsHwIpFwdInfo->UntagPortList;
    NpUtilMaskHwPortList ((UINT1 *) pUntagPortList,
                          (UINT1 *) pRemoteUntagPortList,
                          pHwPortInfo, &u4LocalPortCount, &u4RemotePortCount,
                          BRG_PORT_LIST_SIZE);
    OSIX_BITLIST_SET_BIT ((*pUntagPortList), u4LocalStackPort,
                          BRG_PORT_LIST_SIZE);
    OSIX_BITLIST_SET_BIT ((*pRemoteUntagPortList), u4RemoteStackPort,
                          BRG_PORT_LIST_SIZE);
    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "NpUtilIgsHwUpdateIpmcEntry: EXIT\n");
    return;
}
#endif /* IGS_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIPCWaitStatus                                */
/*                                                                           */
/*     DESCRIPTION      : Utility function for fetching the IPC wait status  */
/*                        required or not for the input opcode and moduleid  */
/*                                                                           */
/*     INPUT            : u4ModuleId - Module Id (CFA/VLAN/LA...)            */
/*                        u4OpCode - NP Opcode                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : FNP_TRUE - if the NPAPI has to wait for remote node*/
/*                                   acknowledgement.                        */
/*                        FNP_FALSE - otherwise                              */
/*                                                                           */
/*****************************************************************************/
UINT4
NpUtilIPCWaitStatus (UINT4 u4ModuleId, UINT4 u4OpCode)
{
    UINT1               u1RetVal = FNP_TRUE;

    switch (u4ModuleId)
    {
        case NP_VLAN_MOD:
        {
            if (u4OpCode < FS_MI_VLAN_HW_MAX_NP)
            {
                u1RetVal = gVlanNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_LA_MOD:
        {
            if (u4OpCode < FS_LA_HW_MAX_NP)
            {
                u1RetVal = gLaNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_CFA_MOD:
        {
            if (u4OpCode < FS_CFA_HW_MAX_NP)
            {
                u1RetVal = gCfaNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_ISSSYS_MOD:
        {
            if (u4OpCode < FS_NP_ISS_MAX_NP)
            {
                u1RetVal = gIssNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_IP_MOD:
        {
            if (u4OpCode < FS_NP_IPV4_MAX_NP)
            {
                u1RetVal = gIpNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }

        case NP_MSTP_MOD:
        {
            if (u4OpCode < FS_NP_MSTP_MAX_NP)
            {
                u1RetVal = gMstpNpMapTable[u4OpCode - 1].u1WaitStatus;
            }

            break;
        }

        case NP_RSTP_MOD:
        {
            if (u4OpCode < FS_NP_RSTP_MAX_NP)
            {
                u1RetVal = gRstpNpMapTable[u4OpCode - 1].u1WaitStatus;
            }

            break;
        }
        case NP_PVRST_MOD:
        {
            if (u4OpCode < FS_NP_PVRST_MAX_NP)
            {
                u1RetVal = gPvrStpNpMapTable[u4OpCode - 1].u1WaitStatus;
            }

            break;
        }
        case NP_VCM_MOD:
        {
            if (u4OpCode < FS_NP_VCM_MAX_NP)
            {
                u1RetVal = gVcmNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_QOSX_MOD:
        {
            if (u4OpCode < FS_QOS_HW_MAX_NP)
            {
                u1RetVal = gQosNpMapTable[u4OpCode - 1].u1WaitStatus;
            }
            break;
        }
        case NP_PNAC_MOD:
        case NP_RMON_MOD:
        case NP_RMONv2_MOD:
        case NP_IGS_MOD:
        case NP_MLDS_MOD:
        case NP_DSMON_MOD:
        case NP_EOAM_MOD:
        case NP_EOAMFM_MOD:
        case NP_ELPS_MOD:
        case NP_ECFM_MOD:
        case NP_ERPS_MOD:
        case NP_RM_MOD:
        case NP_MBSM_MOD:
        case NP_SYNCE_MOD:
        case NP_CAPWAP_MODULE:
        case NP_WLAN_MODULE:
        case NP_WLAN_CLIENT_MODULE:
        case NP_RADIO_MODULE:
        case NP_OFC_MOD:
        case NP_IPV6_MOD:
        case NP_MPLS_MOD:
            break;
    }                            /* End of switch */
    if (u1RetVal == FNP_FALSE)
    {
        return OSIX_NO_WAIT;
    }
    return OSIX_WAIT;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : NpUtilIsNpSupported                                */
/*                                                                           */
/*     DESCRIPTION      : Utility function for checking the availability of  */
/*                        NPAPI  support in the platform.                    */
/*                                                                           */
/*     INPUT            : u4ModuleId - Module Id (CFA/VLAN/LA...)            */
/*                        u4OpCode - NP Opcode                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : FNP_TRUE - if the NPAPI is supported in the        */
/*                                   platform. The availability is           */
/*                                   specified in util/nputil/npbcmsup.h file*/
/*                                   for BCMX platform. For all others, the  */
/*                                   opcode support is specified in          */
/*                                   npgensup.h file.                        */
/*                        FNP_FALSE - otherwise                              */
/*                                                                           */
/*****************************************************************************/
UINT1
NpUtilIsNpSupported (UINT4 u4ModuleId, UINT4 u4OpCode)
{
    UINT1               u1RetVal = OSIX_SUCCESS;

    switch (u4ModuleId)
    {
        case NP_VLAN_MOD:
        {
            if (u4OpCode < FS_MI_VLAN_HW_MAX_NP)
            {
                u1RetVal = gVlanNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }
        case NP_LA_MOD:
        {
            if (u4OpCode < FS_LA_HW_MAX_NP)
            {
                u1RetVal = gLaNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }
        case NP_CFA_MOD:
        {
            if (u4OpCode < FS_CFA_HW_MAX_NP)
            {
                u1RetVal = gCfaNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }
        case NP_ISSSYS_MOD:
        {
            if (u4OpCode < FS_NP_ISS_MAX_NP)
            {
                u1RetVal = gIssNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }
        case NP_IP_MOD:
        {
            if (u4OpCode < FS_NP_IPV4_MAX_NP)
            {
                u1RetVal = gIpNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }

        case NP_MSTP_MOD:
        {
            if (u4OpCode < FS_NP_MSTP_MAX_NP)
            {
                u1RetVal = gMstpNpMapTable[u4OpCode - 1].u1NpSupport;
            }

            break;
        }

        case NP_RSTP_MOD:
        {
            if (u4OpCode < FS_NP_RSTP_MAX_NP)
            {
                u1RetVal = gRstpNpMapTable[u4OpCode - 1].u1NpSupport;
            }

            break;
        }
        case NP_PVRST_MOD:
        {
            if (u4OpCode < FS_NP_PVRST_MAX_NP)
            {
                u1RetVal = gPvrStpNpMapTable[u4OpCode - 1].u1NpSupport;
            }

            break;
        }
        case NP_VCM_MOD:
        {
            if (u4OpCode < FS_NP_VCM_MAX_NP)
            {
                u1RetVal = gVcmNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }
        case NP_IGS_MOD:
        {
            if (u4OpCode < FS_MI_IGS_MAX_NP)
            {
                u1RetVal = gIgsNpMapTable[u4OpCode - 1].u1NpSupport;
            }
            break;
        }

        case NP_PNAC_MOD:
        case NP_RMON_MOD:
        case NP_RMONv2_MOD:
        case NP_MLDS_MOD:
        case NP_DSMON_MOD:
        case NP_EOAM_MOD:
        case NP_EOAMFM_MOD:
        case NP_ELPS_MOD:
        case NP_ECFM_MOD:
        case NP_ERPS_MOD:
        case NP_QOSX_MOD:
        case NP_RM_MOD:
        case NP_MBSM_MOD:
        case NP_SYNCE_MOD:
        case NP_CAPWAP_MODULE:
        case NP_WLAN_MODULE:
        case NP_WLAN_CLIENT_MODULE:
        case NP_RADIO_MODULE:
        case NP_OFC_MOD:
        case NP_IPV6_MOD:
        case NP_MPLS_MOD:
            break;
    }                            /* End of switch */
    return u1RetVal;
}

#endif /* _NPSTACKUTL_C_ */
