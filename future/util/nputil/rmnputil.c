/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rmnputil.c,v 1.2 2015/03/25 06:10:32 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __RMNPUTIL_C__
#define __RMNPUTIL_C__

#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskRmNpPorts                              */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskRmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{

    UNUSED_PARAM (pi4RpcCallStatus);
    UNUSED_PARAM (pRemoteHwNp);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case RM_NP_UPDATE_NODE_STATE:
	case FS_NP_H_R_SET_STDY_ST_INFO:
	     {
		*pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
		break;
	     }
        default:
            break;
    }
    return;
}
/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRmConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmNpModInfo       *pRmNpModInfo = NULL;
    tRmRemoteNpModInfo *pRmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRmNpModInfo = &(pFsHwNp->RmNpModInfo);
    pRmRemoteNpModInfo = &(pRemoteHwNp->RmRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length = pRemoteHwNp->i4Length + sizeof (tRmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case RM_NP_UPDATE_NODE_STATE:
        {
            tRmNpWrRmNpUpdateNodeState *pLocalArgs = NULL;
            tRmRemoteNpWrRmNpUpdateNodeState *pRemoteArgs = NULL;
            pLocalArgs = &pRmNpModInfo->RmNpRmNpUpdateNodeState;
            pRemoteArgs = &pRmRemoteNpModInfo->RmRemoteNpRmNpUpdateNodeState;
            pRemoteArgs->u1Event = pLocalArgs->u1Event;
            break;
        }
        case FS_NP_H_R_SET_STDY_ST_INFO:
        {
            tRmNpWrFsNpHRSetStdyStInfo *pLocalArgs = NULL;
            tRmRemoteNpWrFsNpHRSetStdyStInfo *pRemoteArgs = NULL;
            pLocalArgs = &pRmNpModInfo->RmNpFsNpHRSetStdyStInfo;
            pRemoteArgs = &pRmRemoteNpModInfo->RmRemoteNpFsNpHRSetStdyStInfo;
            MEMCPY (&(pRemoteArgs->eAction), &(pLocalArgs->eAction),
                    sizeof (tRmHRAction));
            if (pLocalArgs->pRmHRPktInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->RmHRPktInfo), (pLocalArgs->pRmHRPktInfo),
                        sizeof (tRmHRPktInfo));
            }
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRmConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmNpModInfo       *pRmNpModInfo = NULL;
    tRmRemoteNpModInfo *pRmRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNp->u4Opcode;
    pRmNpModInfo = &(pFsHwNp->RmNpModInfo);
    pRmRemoteNpModInfo = &(pRemoteHwNp->RmRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case RM_NP_UPDATE_NODE_STATE:
        {
            tRmNpWrRmNpUpdateNodeState *pLocalArgs = NULL;
            tRmRemoteNpWrRmNpUpdateNodeState *pRemoteArgs = NULL;
            pLocalArgs = &pRmNpModInfo->RmNpRmNpUpdateNodeState;
            pRemoteArgs = &pRmRemoteNpModInfo->RmRemoteNpRmNpUpdateNodeState;
            pLocalArgs->u1Event = pRemoteArgs->u1Event;
            break;
        }
        case FS_NP_H_R_SET_STDY_ST_INFO:
        {
            tRmNpWrFsNpHRSetStdyStInfo *pLocalArgs = NULL;
            tRmRemoteNpWrFsNpHRSetStdyStInfo *pRemoteArgs = NULL;
            pLocalArgs = &pRmNpModInfo->RmNpFsNpHRSetStdyStInfo;
            pRemoteArgs = &pRmRemoteNpModInfo->RmRemoteNpFsNpHRSetStdyStInfo;
            MEMCPY (&(pLocalArgs->eAction), &(pRemoteArgs->eAction),
                    sizeof (tRmHRAction));
            if (pLocalArgs->pRmHRPktInfo != NULL)
            {
                MEMCPY ((pLocalArgs->pRmHRPktInfo), &(pRemoteArgs->RmHRPktInfo),
                        sizeof (tRmHRPktInfo));
            }
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilRmRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRmNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilRmRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                            tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmRemoteNpModInfo *pRmRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pRmRemoteNpModInfo = &(pRemoteHwNpInput->RmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case RM_NP_UPDATE_NODE_STATE:
        {
            tRmRemoteNpWrRmNpUpdateNodeState *pInput = NULL;
            pInput = &pRmRemoteNpModInfo->RmRemoteNpRmNpUpdateNodeState;
            u1RetVal = RmNpUpdateNodeState (pInput->u1Event);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_H_R_SET_STDY_ST_INFO:
        {
            tRmRemoteNpWrFsNpHRSetStdyStInfo *pInput = NULL;
            pInput = &pRmRemoteNpModInfo->RmRemoteNpFsNpHRSetStdyStInfo;
            u1RetVal =
                FsNpHRSetStdyStInfo (pInput->eAction, &(pInput->RmHRPktInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __RMNPUTIL_C__ */
