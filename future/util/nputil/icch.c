
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: icch.c
 *
 * Description:This file contains the single NP ICCH function
 *             responsible for Hardware Programming
 *
 *******************************************************************/


#include "icch.h"
#include "npstackutl.h"
#include "size.h"

extern INT4 MbsmGetConnectingPortFromSlotId (UINT4 u4SlotId, UINT4 *pu4ConnectingPort);

/*****************************************************************************/
/* Function Name      : ICCHProcessRxMessage                                 */
/*                                                                           */
/* Description        : This function processes the recieved ICCH message,   */
/*                      and posts the message to resepctive Distributed      */
/*                      modules based on the AppId                           */
/*                                                                           */
/* Input(s)           : pHwInfo  - Received Hw Info structure                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
ICCHProcessRxMessage (tHwInfo * pHwInfo)
{
#ifdef MBSM_WANTED
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT1                 *pu1RxBuf = NULL;
    UINT4                 u4PktSize = 0;
    UINT4                 u4IfIndex = 0;
    UINT4                 u4AppId = 0;
    UINT4                 u4SrcSlot = 0;
    UINT2                 u2AggPortIndex = 0;
	tMacAddr 			  DestMacAddr;


    NPUTIL_DBG1 (NPUTIL_DBG_MSG,"Entering %s \r\n", __FUNCTION__);

    if (pHwInfo == NULL)
    {
        NPUTIL_DBG2 (NPUTIL_DBG_ERR,"%s:%d Received message is NULL \r\n",
                     __FUNCTION__, __LINE__);
        return FNP_FAILURE;
    }
    
    pu1RxBuf = pHwInfo->uHwMsgInfo.HwDissMsgInfo.pu1Data;

    if (pu1RxBuf == NULL)
    {
        NPUTIL_DBG2 (NPUTIL_DBG_ERR,"%s:%d Received Data Buffer is NULL \r\n",
                     __FUNCTION__, __LINE__);
        return FNP_FAILURE;
    }

    u4AppId = pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId;
    u4SrcSlot = (UINT4) pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot;
    MbsmGetConnectingPortFromSlotId (u4SrcSlot, &u4IfIndex);
    u4PktSize = pHwInfo->u4PktSize;

	/* Map the SlotId to Mac Address*/ 
	MEMCPY (&DestMacAddr, &pu1RxBuf[MAC_ADDR_LEN],sizeof (tMacAddr));
	if (MbsmSetMacAddressToSlotId(DestMacAddr,u4SrcSlot) != FNP_SUCCESS)
	{
        NPUTIL_DBG2 (NPUTIL_DBG_ERR,"%s:%d MbsmSetMacAddressToSlotId failed \r\n",
                     __FUNCTION__, __LINE__);

	}

    pCruBuf =
        (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pCruBuf == NULL)
    {
        NPUTIL_DBG2 (NPUTIL_DBG_ERR, "%s:%d CRU buffer allocation failed \r\n",
                     __FUNCTION__, __LINE__);
        return FNP_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pCruBuf, pu1RxBuf,
                                   0, u4PktSize) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        NPUTIL_DBG2 (NPUTIL_DBG_ERR, "%s:%d CRU_BUF_Copy_OverBufChain: "
                     "ERROR!\r\n", __FUNCTION__, __LINE__);
        return FNP_FAILURE;
    }

    switch (pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId)
    {
#ifdef PNAC_WANTED
        case ICCH_PNAC:
            /* Post an event to PNAC module */
            if (PnacHandleInFrame (pCruBuf,(UINT2) u4IfIndex, L2_PNAC_CTRL_FRAME) != PNAC_CONTROL)
            {
                NPUTIL_DBG2 (NPUTIL_DBG_ERR, "%s:%d Error In sending APP_DPNAC \r\n",
                             __FUNCTION__, __LINE__);
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                return FNP_FAILURE;
            }
			break;
#endif
#ifdef LA_WANTED
		case ICCH_LA:
			/* Post an event to LA module */
            if(LaHandleIncomingFrame(pCruBuf, (UINT2) u4IfIndex,
                                     &u2AggPortIndex)!= LA_CONTROL)
			{
			  NPUTIL_DBG2 (NPUTIL_DBG_ERR, "%s:%d Error In sending APP_DLAG \r\n",
				  __FUNCTION__, __LINE__);
			  CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
			  return FNP_FAILURE;
			}
			break;
#endif
		default:
			NPUTIL_DBG2 (NPUTIL_DBG_ERR, "%s:%d Invalid AppID: Not sending packets \r\n",
                    __FUNCTION__, __LINE__);
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            return FNP_FAILURE;
    }
#else
	UNUSED_PARAM (pHwInfo);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ICCHHandOverTxFrame                                  */
/*                                                                           */
/* Description        : This function sends ICCH message.                    */
/*                                                                           */
/* Input(s)           : pIcchInfo - pointer of tIcchInfo                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
ICCHHandOverTxFrame (tIcchInfo *pIcchInfo)
{
#ifdef MBSM_WANTED
    tHwInfo             HwInfo;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
	INT4                i4SlotStartIndex = 0;
	INT4                i4MaxSlots = 1;
	INT4                i4SlotId = 0;

    NPUTIL_DBG1 (NPUTIL_DBG_MSG,"Entering %s \r\n", __FUNCTION__);
    /* Status of Remote NP Programming */
    /*  HwInfo.uHwMsgInfo.HwDissMsgInfo.u4Status = FNP_FAILURE; */

    u4StackingModel = (UINT4) ISS_GET_STACKING_MODEL ();

    if (u4StackingModel == ISS_DISS_STACKING_MODEL)
    {
        /* If Port is part of Peer Unit,Send the pkt to Peer CPU
         * through IPC Mechanism */
        if(pIcchInfo->u4SlotId == ICCH_TO_ALL_SLOTS)
        {
            i4SlotStartIndex = MBSM_SLOT_INDEX_START;
            i4MaxSlots = (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START);
        }
        else
        {
            i4SlotStartIndex = (INT4) pIcchInfo->u4SlotId;
            i4MaxSlots = i4SlotStartIndex + 1;
        }

        for (i4SlotId = i4SlotStartIndex;
             i4SlotId < i4MaxSlots; i4SlotId++)
        {
            /* check if it is not Self Slot and slot is Active*/

            if ((MbsmGetSlotIndexStatus(i4SlotId) == MBSM_STATUS_ACTIVE) &&
                (i4SlotId != IssGetSwitchid ()))
            {
                MEMSET (&HwInfo, 0, sizeof (tHwInfo));

                NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "ICCHHandOverTxFrame: ENTRY\n");

                HwInfo.u4MessageType = DISS_MSG;
                HwInfo.u4IfIndex = 0;    /* IfIndex is 0 for Higig Transport */

                HwInfo.u4PktSize = (UINT4) pIcchInfo->u2PktLength;    /* PKT size differs based on each module */
                HwInfo.u4DestSlotId = (UINT4) i4SlotId;
                HwInfo.uHwMsgInfo.HwDissMsgInfo.u4AppId = (UINT4) pIcchInfo->u2AppId;
                HwInfo.uHwMsgInfo.HwDissMsgInfo.i4SrcSlot = IssGetSwitchid ();

                NPUTIL_DBG2 (NPUTIL_DBG_MSG,
                             "ICCHHandOverTxFrame:  SrcSlot[%d], AppId[%d]\n",
                              HwInfo.uHwMsgInfo.HwDissMsgInfo.i4SrcSlot, HwInfo.uHwMsgInfo.HwDissMsgInfo.u4AppId);

                /* Actual data to be sent */
                HwInfo.uHwMsgInfo.HwDissMsgInfo.pu1Data = (UINT1 *) pIcchInfo->pu1Data;

                if (FsCfaHwSendIPCMsg (&HwInfo) != FNP_SUCCESS)
                {
                    NPUTIL_DBG1 (NPUTIL_DBG_MSG,
                           "FsCfaHwSendIPCMsg: Failed for Slot %d\n",i4SlotId);
                }
            }
        } 
	}

    NPUTIL_DBG (NPUTIL_CTRL_PATH_TRC, "ICCHHandOverTxFrame: EXIT\n");
#else
	UNUSED_PARAM (pIcchInfo);
#endif
    return FNP_SUCCESS;
}
