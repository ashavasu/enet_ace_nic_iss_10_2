/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6nputil.c,v 1.9 2015/03/25 06:10:32 siva Exp $
 *
 * Description:This file contains the NP utility functions of IPv6
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __IPV6_NPUTIL_C__
#define __IPV6_NPUTIL_C__

#include "npstackutl.h"
/***************************************************************************/
/*  Function Name       : NpUtilMaskIpv6NpPorts                             */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskIpv6NpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
#ifdef MBSM_WANTED
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6RemoteNpModInfo *pIpv6RemoteNpModInfo = NULL;

    pIpv6NpModInfo = &pFsHwNp->Ipv6NpModInfo;
    pIpv6RemoteNpModInfo = &pRemoteHwNp->Ipv6RemoteNpModInfo;
#endif
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_NP_IPV6_INIT:
        case FS_NP_IPV6_DEINIT:
        case FS_NP_IPV6_NEIGH_CACHE_ADD:
        case FS_NP_IPV6_NEIGH_CACHE_DEL:
        case FS_NP_IPV6_UC_ROUTE_ADD:
        case FS_NP_IPV6_UC_ROUTE_DELETE:
        case FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH:
        case FS_NP_IPV6_UC_ROUTING:
        case FS_NP_IPV6_GET_STATS:
        case FS_NP_IPV6_INTF_STATUS:
        case FS_NP_IPV6_ADDR_CREATE:
        case FS_NP_IPV6_ADDR_DELETE:
        case FS_NP_IPV6_TUNL_PARAM_SET:
        case FS_NP_IPV6_ADD_MCAST_M_A_C:
        case FS_NP_IPV6_DEL_MCAST_M_A_C:
        case FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY:
        case FS_NP_IPV6_ENABLE:
        case FS_NP_IPV6_DISABLE:
#ifdef OSPF3_WANTED
        case FS_NP_OSPF3_INIT:
        case FS_NP_OSPF3_DE_INIT:
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
        case FS_NP_RIP6_INIT:
#endif /* RIP6_WANTED */
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_GET:
        case FS_NP_IPV6_NEIGH_CACHE_GET_NEXT:
        case FS_NP_IPV6_UC_GET_ROUTE:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV6_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#ifdef RIP6_WANTED
        case FS_NP_MBSM_RIP6_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_RIP6_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
        case FS_NP_MBSM_OSPF3_INIT:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_NP_OSPF3_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* OSPF3_WANTED */
        case FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD:
        {
            tIpv6NpWrFsNpMbsmIpv6NeighCacheAdd *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6NeighCacheAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6NeighCacheAdd;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6NeighCacheAdd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu1HwAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1HwAddr), (pLocalArgs->pu1HwAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1HwAddrLen = pLocalArgs->u1HwAddrLen;
            pRemoteArgs->u1ReachStatus = pLocalArgs->u1ReachStatus;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_NEIGH_CACHE_ADD;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;

        }
        case FS_NP_MBSM_IPV6_UC_ROUTE_ADD:
        {
            tIpv6NpWrFsNpMbsmIpv6UcRouteAdd *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6UcRouteAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6UcRouteAdd;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteAdd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4NHType = pLocalArgs->u4NHType;
            MEMCPY (&(pRemoteArgs->IntInfo), (pLocalArgs->pIntInfo),
                    sizeof (tFsNpIntInfo));
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_UC_ROUTE_ADD;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_NP_MBSM_IPV6_INTF_STATUS:
        {
            tIpv6NpWrFsNpMbsmIpv6IntfStatus *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6IntfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6IntfStatus;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6IntfStatus;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfStatus = pLocalArgs->u4IfStatus;
            MEMCPY (&(pRemoteArgs->IntfInfo), (pLocalArgs->pIntInfo),
                    sizeof (tFsNpIntInfo));
            pRemoteHwNp->u4Opcode = FS_NP_IPV6_INTF_STATUS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#else
        UNUSED_PARAM (pRemoteHwNp);
#endif /* MBSM_WANTED */
        default:
            break;

    }
#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_NP_MBSM_IPV6_INIT) ||
        ((pFsHwNp->u4Opcode > FS_NP_MBSM_IPV6_INTF_STATUS)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_IPV6_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilIpv6MergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                        */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp                */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value          */
/*                    pi4RemoteNpRetVal - Remote Np Return Value           */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilIpv6MergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp,
                                    INT4 *pi4LocalNpRetVal,
                                    INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6RemoteNpModInfo *pIpv6RemoteNpModInfo = NULL;

    pIpv6NpModInfo = &(pFsHwNp->Ipv6NpModInfo);
    pIpv6RemoteNpModInfo = &(pRemoteHwNp->Ipv6RemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_NP_IPV6_INIT:
        case FS_NP_IPV6_DEINIT:
        case FS_NP_IPV6_NEIGH_CACHE_ADD:
        case FS_NP_IPV6_NEIGH_CACHE_DEL:
        case FS_NP_IPV6_UC_ROUTE_DELETE:
        case FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH:
        case FS_NP_IPV6_UC_ROUTING:
        case FS_NP_IPV6_INTF_STATUS:
        case FS_NP_IPV6_ADDR_CREATE:
        case FS_NP_IPV6_ADDR_DELETE:
        case FS_NP_IPV6_TUNL_PARAM_SET:
        case FS_NP_IPV6_ADD_MCAST_M_A_C:
        case FS_NP_IPV6_DEL_MCAST_M_A_C:
        case FS_NP_IPV6_ENABLE:
        case FS_NP_IPV6_DISABLE:
#ifdef OSPF3_WANTED
        case FS_NP_OSPF3_INIT:
        case FS_NP_OSPF3_DE_INIT:
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
        case FS_NP_RIP6_INIT:
#endif /* RIP6_WANTED */
        {
            break;
        }
        case FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY:
        {
            tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6CheckHitOnNDCacheEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6CheckHitOnNDCacheEntry;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6CheckHitOnNDCacheEntry;
            pLocalArgs->u1HitBitStatus =  pLocalArgs->u1HitBitStatus | pRemoteArgs->u1HitBitStatus;
            break;
        }
        case FS_NP_IPV6_GET_STATS:
        {
            tIpv6NpWrFsNpIpv6GetStats *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6GetStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6GetStats;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6GetStats;
            (*pLocalArgs->pu4StatsValue) =
                (pRemoteArgs->u4StatsValue) + (*pLocalArgs->pu4StatsValue);
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        }
       case FS_NP_IPV6_UC_ROUTE_ADD:
       {
            tIpv6NpWrFsNpIpv6UcRouteAdd *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6UcRouteAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteAdd;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteAdd;
            (pLocalArgs->u4HwIntfId[1]) = (pRemoteArgs->u4HwIntfId[0]);
            (pRemoteArgs->u4HwIntfId[1]) = (pLocalArgs->u4HwIntfId[0]);
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;

  
        }
        default:
            break;

    }
    return u1RetVal;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpv6ConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpv6ConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6RemoteNpModInfo *pIpv6RemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pIpv6NpModInfo = &(pFsHwNp->Ipv6NpModInfo);
    pIpv6RemoteNpModInfo = &(pRemoteHwNp->Ipv6RemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tIpv6RemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_IPV6_INIT:
        {
            break;
        }
        case FS_NP_IPV6_DEINIT:
        {
            break;
        }
         case FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY:
        { 
            tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6CheckHitOnNDCacheEntry *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6CheckHitOnNDCacheEntry;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6CheckHitOnNDCacheEntry;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr), sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1HitBitStatus = 0; 
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_ADD:
        {
            tIpv6NpWrFsNpIpv6NeighCacheAdd *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6NeighCacheAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheAdd;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6NeighCacheAdd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            if (pLocalArgs->pu1HwAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1HwAddr), (pLocalArgs->pu1HwAddr),
                        sizeof (tMacAddr));
            }
            pRemoteArgs->u1HwAddrLen = pLocalArgs->u1HwAddrLen;
            pRemoteArgs->u1ReachStatus = pLocalArgs->u1ReachStatus;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_DEL:
        {
            tIpv6NpWrFsNpIpv6NeighCacheDel *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6NeighCacheDel *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheDel;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6NeighCacheDel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_ADD:
        {
            tIpv6NpWrFsNpIpv6UcRouteAdd *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6UcRouteAdd *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteAdd;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteAdd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4NHType = pLocalArgs->u4NHType;
            MEMCPY (&(pRemoteArgs->IntInfo), (pLocalArgs->pIntInfo),
                    sizeof (tFsNpIntInfo));
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_DELETE:
        {
            tIpv6NpWrFsNpIpv6UcRouteDelete *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6UcRouteDelete *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteDelete;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteDelete;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH:
        {
            tIpv6NpWrFsNpIpv6RtPresentInFastPath *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6RtPresentInFastPath *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6RtPresentInFastPath;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6RtPresentInFastPath;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                    sizeof (tIp6Addr));
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_NP_IPV6_UC_ROUTING:
        {
            tIpv6NpWrFsNpIpv6UcRouting *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6UcRouting *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouting;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouting;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4RoutingStatus = pLocalArgs->u4RoutingStatus;
            break;
        }
        case FS_NP_IPV6_GET_STATS:
        {
            tIpv6NpWrFsNpIpv6GetStats *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6GetStats *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6GetStats;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6GetStats;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4StatsFlag = pLocalArgs->u4StatsFlag;
            MEMCPY (&(pRemoteArgs->u4StatsValue),
                    (pLocalArgs->pu4StatsValue), sizeof (UINT4));
            break;
        }
        case FS_NP_IPV6_INTF_STATUS:
        {
            tIpv6NpWrFsNpIpv6IntfStatus *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6IntfStatus *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6IntfStatus;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6IntfStatus;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfStatus = pLocalArgs->u4IfStatus;
            MEMCPY (&(pRemoteArgs->IntfInfo), (pLocalArgs->pIntfInfo),
                    sizeof (tFsNpIntInfo));
            break;
        }
        case FS_NP_IPV6_ADDR_CREATE:
        {
            tIpv6NpWrFsNpIpv6AddrCreate *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6AddrCreate *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrCreate;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddrCreate;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4AddrType = pLocalArgs->u4AddrType;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            break;
        }
        case FS_NP_IPV6_ADDR_DELETE:
        {
            tIpv6NpWrFsNpIpv6AddrDelete *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6AddrDelete *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrDelete;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddrDelete;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4AddrType = pLocalArgs->u4AddrType;
            MEMCPY (&(pRemoteArgs->u1Ip6Addr), (pLocalArgs->pu1Ip6Addr),
                    sizeof (tIp6Addr));
            pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
            break;
        }
        case FS_NP_IPV6_TUNL_PARAM_SET:
        {
            tIpv6NpWrFsNpIpv6TunlParamSet *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6TunlParamSet *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6TunlParamSet;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6TunlParamSet;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4TunlType = pLocalArgs->u4TunlType;
            pRemoteArgs->u4SrcAddr = pLocalArgs->u4SrcAddr;
            pRemoteArgs->u4DstAddr = pLocalArgs->u4DstAddr;
            break;
        }
        case FS_NP_IPV6_ADD_MCAST_M_A_C:
        {
            tIpv6NpWrFsNpIpv6AddMcastMAC *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6AddMcastMAC *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddMcastMAC;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddMcastMAC;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->u1MacAddr), (pLocalArgs->pu1MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u1MacAddrLen = pLocalArgs->u1MacAddrLen;
            break;
        }
        case FS_NP_IPV6_DEL_MCAST_M_A_C:
        {
            tIpv6NpWrFsNpIpv6DelMcastMAC *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6DelMcastMAC *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6DelMcastMAC;
            pRemoteArgs =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6DelMcastMAC;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->u1MacAddr), (pLocalArgs->pu1MacAddr),
                    sizeof (tMacAddr));
            pRemoteArgs->u1MacAddrLen = pLocalArgs->u1MacAddrLen;
            break;
        }
       case FS_NP_IPV6_ENABLE:
        {
            tIpv6NpWrFsNpIpv6Enable *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6Enable *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Enable;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6Enable;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
 case FS_NP_IPV6_DISABLE:
        {
            tIpv6NpWrFsNpIpv6Disable *pLocalArgs = NULL;
            tIpv6RemoteNpWrFsNpIpv6Disable *pRemoteArgs = NULL;
            pLocalArgs = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Disable;
            pRemoteArgs = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6Disable;
            pRemoteArgs->u2VlanId = pLocalArgs->u2VlanId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }

#ifdef OSPF3_WANTED
        case FS_NP_OSPF3_INIT:
        case FS_NP_OSPF3_DE_INIT:
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
        case FS_NP_RIP6_INIT:
#endif /* RIP6_WANTED */
        case FS_NP_IPV6_NEIGH_CACHE_GET:
        case FS_NP_IPV6_NEIGH_CACHE_GET_NEXT:
        case FS_NP_IPV6_UC_GET_ROUTE:
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV6_INIT:
#ifdef RIP6_WANTED
        case FS_NP_MBSM_RIP6_INIT:
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
        case FS_NP_MBSM_OSPF3_INIT:
#endif /* OSPF3_WANTED */
        case FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD:
        case FS_NP_MBSM_IPV6_UC_ROUTE_ADD:
        case FS_NP_MBSM_IPV6_INTF_STATUS:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpv6ConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpv6ConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;

    u4Opcode = pRemoteHwNp->u4Opcode;

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV6_INIT:
#ifdef RIP6_WANTED
        case FS_NP_MBSM_RIP6_INIT:
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
        case FS_NP_MBSM_OSPF3_INIT:
#endif /* OSPF3_WANTED */
        case FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD:
        case FS_NP_MBSM_IPV6_UC_ROUTE_ADD:
        case FS_NP_MBSM_IPV6_INTF_STATUS:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilIpv6RemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpv6NpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilIpv6RemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpv6RemoteNpModInfo *pIpv6RemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pIpv6RemoteNpModInfo = &(pRemoteHwNpInput->Ipv6RemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_NP_IPV6_INIT:
        {
            u1RetVal = FsNpIpv6Init ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_DEINIT:
        {
            u1RetVal = FsNpIpv6Deinit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_ADD:
        {
            tIpv6RemoteNpWrFsNpIpv6NeighCacheAdd *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6NeighCacheAdd;
            u1RetVal =
                FsNpIpv6NeighCacheAdd (pInput->u4VrId,
                                       (UINT1 *) &(pInput->u1Ip6Addr),
                                       pInput->u4IfIndex,
                                       (UINT1 *) &(pInput->u1HwAddr),
                                       pInput->u1HwAddrLen,
                                       pInput->u1ReachStatus, pInput->u2VlanId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_DEL:
        {
            tIpv6RemoteNpWrFsNpIpv6NeighCacheDel *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6NeighCacheDel;
            u1RetVal =
                FsNpIpv6NeighCacheDel (pInput->u4VrId,
                                       (UINT1 *) &(pInput->u1Ip6Addr),
                                       pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_ADD:
        {
            tIpv6RemoteNpWrFsNpIpv6UcRouteAdd *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteAdd;
            u1RetVal =
                FsNpIpv6UcRouteAdd (pInput->u4VrId,
                                    (UINT1 *) &(pInput->u1Ip6Prefix),
                                    pInput->u1PrefixLen,
                                    (UINT1 *) &(pInput->u1NextHop),
                                    pInput->u4NHType, &(pInput->IntInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_DELETE:
        {
            tIpv6RemoteNpWrFsNpIpv6UcRouteDelete *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouteDelete;
            u1RetVal =
                FsNpIpv6UcRouteDelete (pInput->u4VrId,
                                       (UINT1 *) &(pInput->u1Ip6Prefix),
                                       pInput->u1PrefixLen,
                                       (UINT1 *) &(pInput->u1NextHop),
                                       pInput->u4IfIndex, &(pInput->RouteInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH:
        {
            tIpv6RemoteNpWrFsNpIpv6RtPresentInFastPath *pInput = NULL;
            pInput =
                &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6RtPresentInFastPath;
            u1RetVal =
                FsNpIpv6RtPresentInFastPath (pInput->u4VrId,
                                             (UINT1 *) &(pInput->u1Ip6Prefix),
                                             pInput->u1PrefixLen,
                                             (UINT1 *) &(pInput->u1NextHop),
                                             pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_UC_ROUTING:
        {
            tIpv6RemoteNpWrFsNpIpv6UcRouting *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6UcRouting;
            u1RetVal =
                FsNpIpv6UcRouting (pInput->u4VrId, pInput->u4RoutingStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_GET_STATS:
        {
            tIpv6RemoteNpWrFsNpIpv6GetStats *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6GetStats;
            u1RetVal =
                FsNpIpv6GetStats (pInput->u4VrId, pInput->u4IfIndex,
                                  pInput->u4StatsFlag, &(pInput->u4StatsValue));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_INTF_STATUS:
        {
            tIpv6RemoteNpWrFsNpIpv6IntfStatus *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6IntfStatus;
            u1RetVal =
                FsNpIpv6IntfStatus (pInput->u4VrId, pInput->u4IfStatus,
                                    &(pInput->IntfInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY:
        {
            tIpv6RemoteNpWrFsNpIpv6CheckHitOnNDCacheEntry  *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6CheckHitOnNDCacheEntry;
            pInput->u1HitBitStatus  = FsNpIpv6CheckHitOnNDCacheEntry (pInput->u4VrId,(UINT1 *) &(pInput->u1Ip6Addr),
                                                                      pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;  
        }
        case FS_NP_IPV6_ADDR_CREATE:
        {
            tIpv6RemoteNpWrFsNpIpv6AddrCreate *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddrCreate;
            u1RetVal =
                FsNpIpv6AddrCreate (pInput->u4VrId, pInput->u4IfIndex,
                                    pInput->u4AddrType,
                                    (UINT1 *) &(pInput->u1Ip6Addr),
                                    pInput->u1PrefixLen);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_ADDR_DELETE:
        {
            tIpv6RemoteNpWrFsNpIpv6AddrDelete *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddrDelete;
            u1RetVal =
                FsNpIpv6AddrDelete (pInput->u4VrId, pInput->u4IfIndex,
                                    pInput->u4AddrType,
                                    (UINT1 *) &(pInput->u1Ip6Addr),
                                    pInput->u1PrefixLen);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
	}
	case FS_NP_IPV6_ENABLE:
	{
		tIpv6RemoteNpWrFsNpIpv6Enable *pInput = NULL;
		pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6Enable;
		u1RetVal =
			FsNpIpv6Enable (pInput->u4IfIndex, pInput->u2VlanId);
		MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
		break;
	}
	case FS_NP_IPV6_DISABLE:
	{
		tIpv6RemoteNpWrFsNpIpv6Disable *pInput = NULL;
		pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6Disable;
		u1RetVal =
			FsNpIpv6Disable (pInput->u4IfIndex, pInput->u2VlanId);
		MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
		break;
	}

        case FS_NP_IPV6_TUNL_PARAM_SET:
        {
            tIpv6RemoteNpWrFsNpIpv6TunlParamSet *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6TunlParamSet;
            u1RetVal =
                FsNpIpv6TunlParamSet (pInput->u4VrId, pInput->u4IfIndex,
                                      pInput->u4TunlType, pInput->u4SrcAddr,
                                      pInput->u4DstAddr);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_ADD_MCAST_M_A_C:
        {
            tIpv6RemoteNpWrFsNpIpv6AddMcastMAC *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6AddMcastMAC;
            u1RetVal =
                FsNpIpv6AddMcastMAC (pInput->u4VrId, pInput->u4IfIndex,
                                     (UINT1 *) &(pInput->u1MacAddr),
                                     pInput->u1MacAddrLen);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_IPV6_DEL_MCAST_M_A_C:
        {
            tIpv6RemoteNpWrFsNpIpv6DelMcastMAC *pInput = NULL;
            pInput = &pIpv6RemoteNpModInfo->Ipv6RemoteNpFsNpIpv6DelMcastMAC;
            u1RetVal =
                FsNpIpv6DelMcastMAC (pInput->u4VrId, pInput->u4IfIndex,
                                     (UINT1 *) &(pInput->u1MacAddr),
                                     pInput->u1MacAddrLen);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef OSPF3_WANTED
        case FS_NP_OSPF3_INIT:
        {
            u1RetVal = FsNpOspf3Init ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_NP_OSPF3_DE_INIT:
        {
            FsNpOspf3DeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
        case FS_NP_RIP6_INIT:
        {
            u1RetVal = FsNpRip6Init ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* RIP6_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __IPV6_NPUTIL_C */
