/*************************************************************************
 * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 *
 * $Id: vxnputil.c,v 1.2 2018/01/05 15:26:47 siva Exp $
 *
 * Description: This file contains the NP utility functions
 *              responsible for Hardware Programming
 *
 **************************************************************************/
#ifndef _VXNPUTIL_C_
#define _VXNPUTIL_C_

#include "nputlremnp.h"
#include "npstackutl.h"
/***************************************************************************
 *
 *    Function Name       : NpUtilVxlanConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
PUBLIC UINT1
NpUtilVxlanConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanRemoteNpModInfo *pVxlanRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    pVxlanNpModInfo = &(pFsHwNp->VxlanNpModInfo);
    pVxlanRemoteNpModInfo = &(pRemoteHwNp->VxlanRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tVxlanRemoteNpModInfo);

    switch (pVxlanNpModInfo->u4InfoType)
    {
        case VXLAN_HW_INIT:
            pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_INIT;
            break;
        case VXLAN_HW_UDP_PORT:
        {
            pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_UDP_PORT;
            pVxlanRemoteNpModInfo->u4VxlanRemNpHwUdpPortNo =
                pVxlanNpModInfo->u4VxlanNpHwUdpPortNo;
            break;
        }
        case VXLAN_HW_NVE_DATABASE:
        case VXLAN_HW_NVE_L2_TABLE:
        {
            tVxlanHwNveInfo    *pLocalArgs = NULL;
            tVxlanRemoteNpWrNveInfo *pRemoteArgs = NULL;
            if (VXLAN_HW_NVE_L2_TABLE == pVxlanNpModInfo->u4InfoType)
            {
                pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;
            }
            else
            {
                pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_NVE_DATABASE;
            }
            pLocalArgs = &pVxlanNpModInfo->VxlanNpHwNveInfo;
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwNveInfo;
            MEMCPY (pRemoteArgs, pLocalArgs, sizeof (tVxlanHwNveInfo));
            break;
        }
        case VXLAN_HW_MCAST_DATABASE:
        {
            tVxlanHwMcastInfo  *pLocalArgs = NULL;
            tVxlanRemoteNpWrMcastInfo *pRemoteArgs = NULL;
            pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_MCAST_DATABASE;
            pLocalArgs = &pVxlanNpModInfo->VxlanNpHwMcastInfo;
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwMcastInfo;
            MEMCPY (pRemoteArgs, pLocalArgs, sizeof (tVxlanHwMcastInfo));
            break;
        }
        case VXLAN_HW_VNI_VLAN_MAPPING:
        case VXLAN_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_HW_GET_STATS:
        {
            tVxlanHwVniVlanInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrVniVlanInfo *pRemoteArgs = NULL;
            if (VXLAN_HW_VNI_VLAN_PORT_MAPPING == pVxlanNpModInfo->u4InfoType)
            {
                pVxlanRemoteNpModInfo->u4InfoType =
                    VXLAN_HW_VNI_VLAN_PORT_MAPPING;
            }
            else if (VXLAN_HW_VNI_VLAN_MAPPING == pVxlanNpModInfo->u4InfoType)
            {
                pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;
            }
            else
            {
                pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_GET_STATS;
            }
            pLocalArgs = &pVxlanNpModInfo->VxlanNpHwVniVlanInfo;
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwVniVlanInfo;
            MEMCPY (pRemoteArgs, pLocalArgs, sizeof (tVxlanHwVniVlanInfo));
            break;
        }
        case VXLAN_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_HW_BUM_REPLICA_MCAST:
        {
            tVxlanHwBUMReplicaInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrBUMReplicaInfo *pRemoteArgs = NULL;
            if (VXLAN_HW_BUM_REPLICA_SOFTWARE == pVxlanNpModInfo->u4InfoType)
            {
                pVxlanRemoteNpModInfo->u4InfoType =
                    VXLAN_HW_BUM_REPLICA_SOFTWARE;
            }
            else if (VXLAN_HW_BUM_REPLICA_MCAST == pVxlanNpModInfo->u4InfoType)
            {
                pVxlanRemoteNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_MCAST;
            }

            pLocalArgs = &pVxlanNpModInfo->VxlanNpHwBUMReplicaInfo;
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwBUMReplicaInfo;
            MEMCPY (pRemoteArgs, pLocalArgs, sizeof (tVxlanHwBUMReplicaInfo));
            break;
        }
#ifdef MBSM_WANTED
        case VXLAN_MBSM_HW_INIT:
        case VXLAN_MBSM_HW_UDP_PORT:
        case VXLAN_MBSM_HW_MCAST_DATABASE:
        case VXLAN_MBSM_HW_NVE_DATABASE:
        case VXLAN_MBSM_HW_NVE_L2_TABLE:
        case VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_MBSM_HW_BUM_REPLICA_MCAST:
        case VXLAN_MBSM_HW_VNI_VLAN_MAPPING:
        case VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING:
            break;
#endif

        default:
            u1RetVal = FNP_FAILURE;
            break;
    }
    return u1RetVal;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilVxlanConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
PUBLIC UINT1
NpUtilVxlanConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanRemoteNpModInfo *pVxlanRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    pVxlanNpModInfo = &(pFsHwNp->VxlanNpModInfo);
    pVxlanRemoteNpModInfo = &(pRemoteHwNp->VxlanRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (pVxlanNpModInfo->u4InfoType)
    {
        case VXLAN_HW_INIT:
        case VXLAN_HW_UDP_PORT:
        case VXLAN_HW_NVE_DATABASE:
        case VXLAN_HW_MCAST_DATABASE:
        case VXLAN_HW_VNI_VLAN_MAPPING:
        case VXLAN_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_HW_BUM_REPLICA_MCAST:
        case VXLAN_HW_GET_STATS:
        case VXLAN_HW_NVE_L2_TABLE:
            break;
            /*If any value should be passed from standby to active
             * for  programming this cases should be handled*/
#ifdef MBSM_WANTED
        case VXLAN_MBSM_HW_INIT:
        case VXLAN_MBSM_HW_UDP_PORT:
        case VXLAN_MBSM_HW_NVE_DATABASE:
        case VXLAN_MBSM_HW_NVE_L2_TABLE:
        case VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_MBSM_HW_BUM_REPLICA_MCAST:
        case VXLAN_MBSM_HW_VNI_VLAN_MAPPING:
        case VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING:
            break;
#endif
        default:
            u1RetVal = FNP_FAILURE;
    }
    return u1RetVal;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilVxlanRemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tVxlanHwInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVxlanRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                               tRemoteHwNp * pRemoteHwNpOutput)
{
    tVxlanRemoteNpModInfo *pVxlanRemoteNpModInfo = NULL;

    if (NULL == pRemoteHwNpInput || NULL == pRemoteHwNpOutput)
    {
        return FNP_FAILURE;
    }
    pVxlanRemoteNpModInfo = &(pRemoteHwNpInput->VxlanRemoteNpModInfo);
    switch (pVxlanRemoteNpModInfo->u4InfoType)
    {
        case VXLAN_HW_INIT:
        {
            tVxlanHwInfo        VxlanHwInfo;
            MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));

            VxlanHwInfo.u4InfoType = VXLAN_HW_INIT;
#ifdef NPAPI_WANTED
            if (pRemoteHwNpInput->u4Opcode == VXLAN_HW_CONFIGURE)
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
            else
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CLEAR) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
            break;
#endif
        }
        case VXLAN_HW_UDP_PORT:
        {
            tVxlanHwInfo        VxlanHwInfo;
            MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));

            VxlanHwInfo.u4InfoType = VXLAN_HW_UDP_PORT;
            VxlanHwInfo.u4VxlanNpHwUdpPortNo =
                pVxlanRemoteNpModInfo->u4VxlanRemNpHwUdpPortNo;
#ifdef NPAPI_WANTED

            if (pRemoteHwNpInput->u4Opcode == VXLAN_HW_CONFIGURE)
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
            else
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CLEAR) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
#endif
            break;
        }
        case VXLAN_HW_NVE_DATABASE:
        case VXLAN_HW_NVE_L2_TABLE:
        {
            tVxlanHwInfo        VxlanHwInfo;
            tVxlanRemoteNpWrNveInfo *pRemoteArgs = NULL;

            MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwNveInfo;

            if (VXLAN_HW_NVE_L2_TABLE == pVxlanRemoteNpModInfo->u4InfoType)
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_NVE_L2_TABLE;
            }
            else
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_NVE_DATABASE;
            }

            MEMCPY (&(VxlanHwInfo.VxlanNpHwNveInfo), pRemoteArgs,
                    sizeof (tVxlanHwNveInfo));
#ifdef NPAPI_WANTED
            if (pRemoteHwNpInput->u4Opcode == VXLAN_HW_CONFIGURE)
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
                if (VxlanHwInfo.u4InfoType == VXLAN_HW_NVE_DATABASE)
                {
                    VxlanUpdateNvePortEntry (VxlanHwInfo.VxlanNpHwNveInfo.
                                             au1RemoteVtepAddress,
                                             VxlanHwInfo.VxlanNpHwNveInfo.
                                             i4RemoteVtepAddressType,
                                             VxlanHwInfo.VxlanNpHwNveInfo.
                                             u4NetworkPort, 0);
                }

            }
            else
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CLEAR) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
#endif
            break;
        }
        case VXLAN_HW_VNI_VLAN_MAPPING:
        case VXLAN_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_HW_GET_STATS:
        {
            tVxlanHwInfo        VxlanHwInfo;
            tVxlanRemoteNpWrVniVlanInfo *pRemoteArgs = NULL;

            MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));
            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwVniVlanInfo;
            if (VXLAN_HW_GET_STATS == pVxlanRemoteNpModInfo->u4InfoType)
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_GET_STATS;
            }
            else if (VXLAN_HW_VNI_VLAN_PORT_MAPPING ==
                     pVxlanRemoteNpModInfo->u4InfoType)
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_PORT_MAPPING;
            }
            else
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;
            }
            MEMCPY (&(VxlanHwInfo.VxlanNpHwVniVlanInfo), pRemoteArgs,
                    sizeof (tVxlanHwVniVlanInfo));
#ifdef NPAPI_WANTED
            if (pRemoteHwNpInput->u4Opcode == VXLAN_HW_CONFIGURE)
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
                if (VxlanHwInfo.u4InfoType == VXLAN_HW_VNI_VLAN_PORT_MAPPING)
                {
                    VxlanUpdateAccessPort (VxlanHwInfo.VxlanNpHwVniVlanInfo.
                                           u4IfIndex,
                                           VxlanHwInfo.VxlanNpHwVniVlanInfo.
                                           u4AccessPort,
                                           VxlanHwInfo.VxlanNpHwVniVlanInfo.
                                           u4VniNumber,
                                           VxlanHwInfo.VxlanNpHwVniVlanInfo.
                                           u4VlanId);
                }

            }
            else
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CLEAR) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
#endif
            break;
        }
        case VXLAN_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_HW_BUM_REPLICA_MCAST:
        {
            tVxlanHwInfo        VxlanHwInfo;
            tVxlanRemoteNpWrBUMReplicaInfo *pRemoteArgs = NULL;
            MEMSET (&VxlanHwInfo, 0, sizeof (tVxlanHwInfo));

            pRemoteArgs = &pVxlanRemoteNpModInfo->VxlanNpRemHwBUMReplicaInfo;
            if (VXLAN_HW_BUM_REPLICA_SOFTWARE ==
                pVxlanRemoteNpModInfo->u4InfoType)
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_BUM_REPLICA_SOFTWARE;
            }
            else
            {
                VxlanHwInfo.u4InfoType = VXLAN_HW_BUM_REPLICA_MCAST;
            }
            MEMCPY (&(VxlanHwInfo.VxlanNpHwBUMReplicaInfo), pRemoteArgs,
                    sizeof (tVxlanHwBUMReplicaInfo));
#ifdef NPAPI_WANTED
            if (pRemoteHwNpInput->u4Opcode == VXLAN_HW_CONFIGURE)
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CONFIGURE) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
                if (VXLAN_HW_BUM_REPLICA_SOFTWARE == VxlanHwInfo.u4InfoType)
                {
                    VxlanUpdateNvePortEntry (VxlanHwInfo.
                                             VxlanNpHwBUMReplicaInfo.
                                             au1RemoteVtepAddress,
                                             VxlanHwInfo.
                                             VxlanNpHwBUMReplicaInfo.
                                             i4RemoteVtepAddressType,
                                             VxlanHwInfo.
                                             VxlanNpHwBUMReplicaInfo.
                                             u4NetworkPort,
                                             VxlanHwInfo.
                                             VxlanNpHwBUMReplicaInfo.
                                             u4NetworkBUMPort);
                }
            }
            else
            {
                if (FsNpHwVxlanInfo (&VxlanHwInfo, VXLAN_HW_CLEAR) ==
                    FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }
#endif
            break;
        }
        default:
            return FNP_FAILURE;

    }
    return FNP_SUCCESS;
}

/***************************************************************************/
/*  Function Name       : NpUtilVxlanMergeLocalRemoteNPOutput              */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                        and Remote NP call execution                     */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                        pRemoteHwNp Param of type tRemoteHwNp            */
/*                        pi4LocalNpRetVal - Lcoal Np Return Value         */
/*                        pi4RemoteNpRetVal - Remote Np Return Value       */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilVxlanMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                     tRemoteHwNp * pRemoteHwNp,
                                     INT4 *pi4LocalNpRetVal,
                                     INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanRemoteNpModInfo *pVxlanRemoteNpModInfo = NULL;

    if ((pFsHwNp == NULL) || (pRemoteHwNp == NULL))
    {
        return FNP_FAILURE;
    }

    pVxlanNpModInfo = &(pFsHwNp->VxlanNpModInfo);
    pVxlanRemoteNpModInfo = &(pRemoteHwNp->VxlanRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (pVxlanNpModInfo->u4InfoType)
    {
        case VXLAN_HW_INIT:
        case VXLAN_HW_UDP_PORT:
        case VXLAN_HW_NVE_DATABASE:
        case VXLAN_HW_MCAST_DATABASE:
        case VXLAN_HW_VNI_VLAN_MAPPING:
        case VXLAN_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_HW_BUM_REPLICA_MCAST:
        case VXLAN_HW_GET_STATS:
        case VXLAN_HW_NVE_L2_TABLE:
#ifdef MBSM_WANTED
        case VXLAN_MBSM_HW_INIT:
        case VXLAN_MBSM_HW_UDP_PORT:
        case VXLAN_MBSM_HW_NVE_DATABASE:
        case VXLAN_MBSM_HW_NVE_L2_TABLE:
        case VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_MBSM_HW_BUM_REPLICA_MCAST:
        case VXLAN_MBSM_HW_VNI_VLAN_MAPPING:
        case VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_MBSM_HW_MCAST_DATABASE:
#endif
            u1RetVal = FNP_SUCCESS;
            *pi4LocalNpRetVal = FNP_SUCCESS;
            *pi4RemoteNpRetVal = FNP_SUCCESS;
            break;
        default:
            u1RetVal = FNP_FAILURE;
    }
    return u1RetVal;
}

/***************************************************************************/
/*  Function Name       : NpUtilMaskVxlanNpPorts                           */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskVxlanNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                        UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
    tVxlanHwInfo       *pVxlanNpModInfo = NULL;
    tVxlanRemoteNpModInfo *pRemoteVxlanNpModInfo = NULL;

    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    /* Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    pVxlanNpModInfo = &(pFsHwNp->VxlanNpModInfo);
    pRemoteVxlanNpModInfo = &(pRemoteHwNp->VxlanRemoteNpModInfo);

    switch (pVxlanNpModInfo->u4InfoType)
    {
        case VXLAN_HW_INIT:
        case VXLAN_HW_UDP_PORT:
        case VXLAN_HW_NVE_DATABASE:
        case VXLAN_HW_MCAST_DATABASE:
        case VXLAN_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_HW_VNI_VLAN_MAPPING:
        case VXLAN_HW_VNI_VLAN_PORT_MAPPING:
        case VXLAN_HW_BUM_REPLICA_MCAST:
        case VXLAN_HW_NVE_L2_TABLE:
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
#ifdef MBSM_WANTED
        case VXLAN_MBSM_HW_INIT:
        {
            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_INIT;
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case VXLAN_MBSM_HW_UDP_PORT:
        {
            tVxlanMbsmHwUdpPortInfo *pLocalArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwUdpPortInfo;
            pRemoteVxlanNpModInfo->u4VxlanRemNpHwUdpPortNo =
                pLocalArgs->u4VxlanUdpPortNo;
            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_UDP_PORT;
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case VXLAN_MBSM_HW_NVE_DATABASE:
        {
            tVxlanMbsmHwNveInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrNveInfo *pRemoteArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;
            pRemoteArgs = &pRemoteVxlanNpModInfo->VxlanNpRemHwNveInfo;

            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_DATABASE;

            pRemoteArgs->i4NveIfIndex = pLocalArgs->i4NveIfIndex;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            pRemoteArgs->u4VlanIdOutTunnel = pLocalArgs->u4VlanIdOutTunnel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4VtepAddressType = pLocalArgs->i4VtepAddressType;
            pRemoteArgs->i4RemoteVtepAddressType =
                pLocalArgs->i4RemoteVtepAddressType;

            MEMCPY (pRemoteArgs->au1VtepAddress, pLocalArgs->au1VtepAddress,
                    sizeof (pLocalArgs->au1VtepAddress));

            MEMCPY (pRemoteArgs->au1RemoteVtepAddress,
                    pLocalArgs->au1RemoteVtepAddress,
                    sizeof (pLocalArgs->au1RemoteVtepAddress));

            MEMCPY (pRemoteArgs->DestVmMac, pLocalArgs->DestVmMac,
                    sizeof (pLocalArgs->DestVmMac));

            pRemoteArgs->b1NveFlushAllVmMacOnly =
                pLocalArgs->b1NveFlushAllVmMacOnly;
            pRemoteArgs->u1MacType = pLocalArgs->u1MacType;
            MEMCPY (pRemoteArgs->au1NextHopMac, pLocalArgs->au1NextHopMac,
                    MAC_ADDR_LEN);
            pRemoteArgs->u4NetworkPort = pLocalArgs->u4NetworkPort;
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case VXLAN_MBSM_HW_NVE_L2_TABLE:
        {
            tVxlanMbsmHwNveInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrNveInfo *pRemoteArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwNveInfo;
            pRemoteArgs = &pRemoteVxlanNpModInfo->VxlanNpRemHwNveInfo;

            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_NVE_L2_TABLE;

/*                pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
                
                MEMCPY(pRemoteArgs->DestVmMac, pLocalArgs->DestVmMac,
                        sizeof(pLocalArgs->DestVmMac));

                pRemoteArgs->u1MacType = pLocalArgs->u1MacType;
                pRemoteArgs->u4NetworkPort = pLocalArgs->u4NetworkPort; */
            pRemoteArgs->i4NveIfIndex = pLocalArgs->i4NveIfIndex;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            pRemoteArgs->u4VlanIdOutTunnel = pLocalArgs->u4VlanIdOutTunnel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4VtepAddressType = pLocalArgs->i4VtepAddressType;
            pRemoteArgs->i4RemoteVtepAddressType =
                pLocalArgs->i4RemoteVtepAddressType;

            MEMCPY (pRemoteArgs->au1VtepAddress, pLocalArgs->au1VtepAddress,
                    sizeof (pLocalArgs->au1VtepAddress));

            MEMCPY (pRemoteArgs->au1RemoteVtepAddress,
                    pLocalArgs->au1RemoteVtepAddress,
                    sizeof (pLocalArgs->au1RemoteVtepAddress));

            MEMCPY (pRemoteArgs->DestVmMac, pLocalArgs->DestVmMac,
                    sizeof (pLocalArgs->DestVmMac));

            pRemoteArgs->b1NveFlushAllVmMacOnly =
                pLocalArgs->b1NveFlushAllVmMacOnly;
            pRemoteArgs->u1MacType = pLocalArgs->u1MacType;
            MEMCPY (pRemoteArgs->au1NextHopMac, pLocalArgs->au1NextHopMac,
                    MAC_ADDR_LEN);
            pRemoteArgs->u4NetworkPort = pLocalArgs->u4NetworkPort;

            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case VXLAN_MBSM_HW_MCAST_DATABASE:
        {
            tVxlanMbsmHwMcastInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrMcastInfo *pRemoteArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwMcastInfo;

            pRemoteArgs = &pRemoteVxlanNpModInfo->VxlanNpRemHwMcastInfo;

            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_MCAST_DATABASE;

            pRemoteArgs->i4NveIfIndex = pLocalArgs->i4NveIfIndex;
            pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            pRemoteArgs->u4VlanIdOutTunnel = pLocalArgs->u4VlanIdOutTunnel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4VtepAddressType = pLocalArgs->i4VtepAddressType;
            pRemoteArgs->i4GroupAddressType = pLocalArgs->i4GroupAddressType;

            MEMCPY (pRemoteArgs->au1VtepAddress, pLocalArgs->au1VtepAddress,
                    sizeof (pLocalArgs->au1VtepAddress));

            MEMCPY (pRemoteArgs->au1GroupAddress, pLocalArgs->au1GroupAddress,
                    sizeof (pLocalArgs->au1GroupAddress));

            MEMCPY (pRemoteArgs->McastMac, pLocalArgs->McastMac,
                    sizeof (pLocalArgs->McastMac));
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE:
        case VXLAN_MBSM_HW_BUM_REPLICA_MCAST:
        {
            tVxlanMbsmHwBUMReplicaInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrBUMReplicaInfo *pRemoteArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwBUMReplicaInfo;

            pRemoteArgs = &pRemoteVxlanNpModInfo->VxlanNpRemHwBUMReplicaInfo;
            if (pVxlanNpModInfo->u4InfoType ==
                VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE)
            {
                pRemoteVxlanNpModInfo->u4InfoType =
                    VXLAN_HW_BUM_REPLICA_SOFTWARE;
            }
            else
            {
                pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_BUM_REPLICA_MCAST;
            }

            pRemoteArgs->i4NveIfIndex = pLocalArgs->i4NveIfIndex;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            pRemoteArgs->u4VlanIdOutTunnel = pLocalArgs->u4VlanIdOutTunnel;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->i4VtepAddressType = pLocalArgs->i4VtepAddressType;
            pRemoteArgs->i4RemoteVtepAddressType =
                pLocalArgs->i4RemoteVtepAddressType;

            MEMCPY (pRemoteArgs->au1VtepAddress, pLocalArgs->au1VtepAddress,
                    sizeof (pLocalArgs->au1VtepAddress));

            pRemoteArgs->pau1RemoteVtepsReplicaTo =
                pLocalArgs->pau1RemoteVtepsReplicaTo;
            MEMCPY (pRemoteArgs->au1RemoteVtepAddress,
                    pLocalArgs->au1RemoteVtepAddress,
                    sizeof (pLocalArgs->au1RemoteVtepAddress));
            pRemoteArgs->u1ArpSupFlag = pLocalArgs->u1ArpSupFlag;
            MEMCPY (pRemoteArgs->au1NextHopMac, pLocalArgs->au1NextHopMac,
                    MAC_ADDR_LEN);
            pRemoteArgs->u4NetworkBUMPort = pLocalArgs->u4NetworkBUMPort;
            pRemoteArgs->u4NetworkPort = pLocalArgs->u4NetworkPort;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            break;
        }
        case VXLAN_MBSM_HW_VNI_VLAN_MAPPING:
        case VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING:
        {
            tVxlanMbsmHwVniVlanInfo *pLocalArgs = NULL;
            tVxlanRemoteNpWrVniVlanInfo *pRemoteArgs = NULL;

            pLocalArgs = &pVxlanNpModInfo->VxlanNpMbsmHwVniVlanInfo;

            pRemoteArgs = &pRemoteVxlanNpModInfo->VxlanNpRemHwVniVlanInfo;

            pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;

            if (pVxlanNpModInfo->u4InfoType == VXLAN_MBSM_HW_VNI_VLAN_MAPPING)
            {
                pRemoteVxlanNpModInfo->u4InfoType = VXLAN_HW_VNI_VLAN_MAPPING;
            }
            else
            {
                pRemoteVxlanNpModInfo->u4InfoType =
                    VXLAN_HW_VNI_VLAN_PORT_MAPPING;
            }

            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u4VniNumber = pLocalArgs->u4VniNumber;
            pRemoteArgs->u4VlanId = pLocalArgs->u4VlanId;
            pRemoteArgs->u4PktSent = pLocalArgs->u4PktSent;
            pRemoteArgs->u4PktRcvd = pLocalArgs->u4PktRcvd;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4AccessPort = pLocalArgs->u4AccessPort;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            pRemoteHwNp->u4Opcode = VXLAN_HW_CONFIGURE;
            break;
        }
#endif

        default:
            break;

    }
}
#endif
