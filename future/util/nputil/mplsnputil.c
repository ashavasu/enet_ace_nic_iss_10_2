/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnputil.c,v 1.1 2014/04/30 09:28:14 siva Exp 
 *
 * Description:This file contains the NP utility functions of MPLS
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __MPLS_NPUTIL_C__
#define __MPLS_NPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskMplsNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskMplsNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
#ifdef MBSM_WANTED
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsRemoteNpModInfo *pMplsRemoteNpModInfo = NULL;
    pMplsNpModInfo = &pFsHwNp->MplsNpModInfo;
    pMplsRemoteNpModInfo = &pRemoteHwNp->MplsRemoteNpModInfo;
#else
    UNUSED_PARAM (pRemoteHwNp);
#endif

    UNUSED_PARAM (pi4RpcCallStatus);

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MPLS_HW_GET_PW_CTRL_CHNL_CAPABILITIES:
        case FS_MPLS_HW_TRAVERSE_PW_VC:
        case FS_MPLS_HW_TRAVERSE_I_L_M:
        case FS_MPLS_HW_TRAVERSE_L3_F_T_N:
        {
            break;
        }
        case FS_MPLS_HW_ENABLE_MPLS:
        case FS_MPLS_HW_DISABLE_MPLS:
        case FS_MPLS_HW_CREATE_PW_VC:
        case FS_MPLS_HW_DELETE_PW_VC:
        case FS_MPLS_HW_CREATE_I_L_M:
        case FS_MPLS_HW_DELETE_I_L_M:
        case FS_MPLS_HW_CREATE_L3_F_T_N:
        case FS_MPLS_WP_HW_DELETE_L3_F_T_N:
        case FS_MPLS_HW_CREATE_VPLS_VPN:
        case FS_MPLS_HW_DELETE_VPLS_VPN:
        case FS_MPLS_HW_VPLS_ADD_PW_VC:
        case FS_MPLS_HW_VPLS_DELETE_PW_VC:
        case NP_MPLS_CREATE_MPLS_INTERFACE:
        case FS_MPLS_HW_ADD_MPLS_ROUTE:
#ifdef HVPLS_WANTED
        case FS_MPLS_REGISTER_FWD_ALARM:
        case FS_MPLS_DE_REGISTER_FWD_ALARM:
#endif
        case FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE: 
        case FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE:
        case FS_MPLS_HW_GET_MPLS_STATS:
        case FS_MPLS_HW_ADD_VPN_AC:
        case FS_MPLS_HW_DELETE_VPN_AC:
        case FS_MPLS_HW_P2MP_ADD_I_L_M:
        case FS_MPLS_HW_P2MP_REMOVE_I_L_M:
        case FS_MPLS_HW_P2MP_TRAVERSE_I_L_M:
        case FS_MPLS_HW_P2MP_ADD_BUD:
        case FS_MPLS_HW_P2MP_REMOVE_BUD:
        case FS_MPLS_HW_MODIFY_PW_VC:
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_HW_L3VPN_INGRESS_MAP:
        case FS_MPLS_HW_L3VPN_EGRESS_MAP:
#endif
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
#ifdef MBSM_WANTED
        case FS_MPLS_MBSM_HW_ENABLE_MPLS:
        {
            /* No Arguments */
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_ENABLE_MPLS;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwCreatePwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreatePwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreatePwVc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreatePwVc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVcInfo),
                        (pLocalArgs->pMplsHwVcInfo), sizeof (tMplsHwVcTnlInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_CREATE_PW_VC;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

        case FS_MPLS_MBSM_HW_CREATE_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwCreateILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateILM;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateILM;
            if (pLocalArgs->pMplsHwIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwIlmInfo),
                        (pLocalArgs->pMplsHwIlmInfo), sizeof (tMplsHwIlmInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_CREATE_I_L_M;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_L3_F_T_N:
        {
            tMplsNpWrFsMplsMbsmHwCreateL3FTN *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateL3FTN *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateL3FTN;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateL3FTN;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3FTNInfo),
                        (pLocalArgs->pMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_CREATE_L3_F_T_N;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

        case FS_MPLS_MBSM_HW_CREATE_VPLS_VPN:
        {
            tMplsNpWrFsMplsMbsmHwCreateVplsVpn *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateVplsVpn *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateVplsVpn;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateVplsVpn;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVpnInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVpnInfo),
                        (pLocalArgs->pMplsHwVplsVpnInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_CREATE_VPLS_VPN;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwVplsAddPwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwVplsAddPwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwVplsAddPwVc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwVplsAddPwVc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_VPLS_ADD_PW_VC;
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MPLS_MBSM_HW_ADD_VPN_AC:
        {
            tMplsNpWrFsMplsMbsmHwAddVpnAc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwAddVpnAc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwAddVpnAc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddVpnAc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_ADD_VPN_AC;
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case NP_MPLS_MBSM_CREATE_MPLS_INTERFACE:
        {
            tMplsNpWrNpMplsMbsmCreateMplsInterface *pLocalArgs = NULL;
            tMplsRemoteNpWrNpMplsMbsmCreateMplsInterface *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpNpMplsMbsmCreateMplsInterface;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->
                MplsRemoteNpNpMplsMbsmCreateMplsInterface;
             if (pLocalArgs->pMplsHwMplsIntInfo != NULL)
              {

                 MEMCPY (&(pRemoteArgs->MplsHwMplsIntInfo),
                         (pLocalArgs->pMplsHwMplsIntInfo), sizeof (tMplsHwMplsIntInfo));
              }

                       pRemoteHwNp->u4Opcode = NP_MPLS_CREATE_MPLS_INTERFACE;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwP2mpAddILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpAddILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwP2mpAddILM;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpAddILM;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            if (pLocalArgs->pMplsHwP2mpIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwP2mpIlmInfo),
                        (pLocalArgs->pMplsHwP2mpIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_P2MP_ADD_I_L_M;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP:
        {
            tMplsNpWrFsMplsMbsmHwL3vpnIngressMap *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwL3vpnIngressMap *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnIngressMap;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnIngressMap;
            if (pLocalArgs->pMplsHwL3vpnIgressInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3vpnIgressInfo),
                        (pLocalArgs->pMplsHwL3vpnIgressInfo),
                        sizeof (tMplsHwL3vpnIgressInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_L3VPN_INGRESS_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

        case FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP:
        {
            tMplsNpWrFsMplsMbsmHwL3vpnEgressMap *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwL3vpnEgressMap *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnEgressMap;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnEgressMap;
            if (pLocalArgs->pMplsHwL3vpnEgressInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3vpnEgressInfo),
                        (pLocalArgs->pMplsHwL3vpnEgressInfo),
                        sizeof (tMplsHwL3vpnEgressInfo));
            }
            pRemoteHwNp->u4Opcode = FS_MPLS_HW_L3VPN_EGRESS_MAP;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }

#endif
#endif
        default:
            break;
    }

#ifdef MBSM_WANTED
#ifdef MPLS_L3VPN_WANTED
    if ((pFsHwNp->u4Opcode < FS_MPLS_MBSM_HW_ENABLE_MPLS) ||
        ((pFsHwNp->u4Opcode > FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP)))
#else
    if ((pFsHwNp->u4Opcode < FS_MPLS_MBSM_HW_ENABLE_MPLS) ||
		((pFsHwNp->u4Opcode > FS_MPLS_MBSM_HW_P2MP_REMOVE_I_L_M)))
#endif
    {
        if (MbsmIsNpBulkSyncInProgress (NP_PNAC_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }
        }
    }
#endif

    return;
}

/***************************************************************************/
/*  Function Name       : NpUtilMplsMergeLocalRemoteNpOutput               */
/*                                                                         */
/*  Description         : This function merges the output of the Local     */
/*                     and Remote NP call execution                        */
/*                                                                         */
/*  Input(s)            : pFsHwNp Param of type tFsHwNp                    */
/*                    pRemoteHwNp Param of type tRemoteHwNp                */
/*                       pi4LocalNpRetVal - Lcoal Np Return Value          */
/*                    pi4RemoteNpRetVal - Remote Np Return Value           */
/*                                                                         */
/*  Output(s)           : pFsHwNp param of type tFsHwNp                    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                    */
/***************************************************************************/
UINT1
NpUtilMplsMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,
                                    tRemoteHwNp * pRemoteHwNp,
                                    INT4 *pi4LocalNpRetVal,
                                    INT4 *pi4RemoteNpRetVal)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsRemoteNpModInfo *pMplsRemoteNpModInfo = NULL;

    pMplsNpModInfo = &(pFsHwNp->MplsNpModInfo);
    pMplsRemoteNpModInfo = &(pRemoteHwNp->MplsRemoteNpModInfo);

    switch (pFsHwNp->u4Opcode)
    {
        case FS_MPLS_HW_ENABLE_MPLS:
        case FS_MPLS_HW_DISABLE_MPLS:
        case FS_MPLS_HW_CREATE_PW_VC:
        case FS_MPLS_HW_DELETE_PW_VC:
        case FS_MPLS_HW_CREATE_I_L_M:
        case FS_MPLS_HW_DELETE_I_L_M:
        case FS_MPLS_HW_CREATE_L3_F_T_N:
        case FS_MPLS_WP_HW_DELETE_L3_F_T_N:
        case FS_MPLS_HW_CREATE_VPLS_VPN:
        case FS_MPLS_HW_DELETE_VPLS_VPN:
        case FS_MPLS_HW_VPLS_ADD_PW_VC:
        case FS_MPLS_HW_VPLS_DELETE_PW_VC:
        case NP_MPLS_CREATE_MPLS_INTERFACE:
        case FS_MPLS_HW_ADD_MPLS_ROUTE:
#ifdef HVPLS_WANTED
        case FS_MPLS_REGISTER_FWD_ALARM:
        case FS_MPLS_DE_REGISTER_FWD_ALARM:
#endif
        case FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE:
        case FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE:
        case FS_MPLS_HW_ADD_VPN_AC:
        case FS_MPLS_HW_DELETE_VPN_AC:
        case FS_MPLS_HW_P2MP_ADD_I_L_M:
        case FS_MPLS_HW_P2MP_REMOVE_I_L_M:
        case FS_MPLS_HW_P2MP_TRAVERSE_I_L_M:
        case FS_MPLS_HW_P2MP_ADD_BUD:
        case FS_MPLS_HW_P2MP_REMOVE_BUD:
        case FS_MPLS_HW_MODIFY_PW_VC:
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_HW_L3VPN_INGRESS_MAP:
        case FS_MPLS_HW_L3VPN_EGRESS_MAP:
#endif
        {
            break;
        }
        case FS_MPLS_HW_GET_MPLS_STATS:
        {
            tMplsNpWrFsMplsHwGetMplsStats *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwGetMplsStats *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwGetMplsStats;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwGetMplsStats;
            if (*pi4RemoteNpRetVal == FNP_SUCCESS)
            {
                (pLocalArgs->pStatsInfo->u8Value.u4Hi) =
                    (pRemoteArgs->StatsInfo.u8Value.u4Hi) +
                    (pLocalArgs->pStatsInfo->u8Value.u4Hi);

                if ((pLocalArgs->pStatsInfo->u8Value.u4Lo) +
                    (pRemoteArgs->StatsInfo.u8Value.u4Lo)
                    < (pLocalArgs->pStatsInfo->u8Value.u4Lo))
                {
                    /* handling wrap around scenario */
                    pLocalArgs->pStatsInfo->u8Value.u4Hi =
                        pLocalArgs->pStatsInfo->u8Value.u4Hi + 1;
                }
                (pLocalArgs->pStatsInfo->u8Value.u4Lo) =
                    (pRemoteArgs->StatsInfo.u8Value.u4Lo) +
                    (pLocalArgs->pStatsInfo->u8Value.u4Lo);
                *pi4LocalNpRetVal = FNP_SUCCESS;
                *pi4RemoteNpRetVal = FNP_SUCCESS;
            }
            else
            {
                *pi4RemoteNpRetVal = FNP_FAILURE;
            }
            break;
        }
        default:
            break;

    }
    return u1RetVal;

}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMplsConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMplsConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsRemoteNpModInfo *pMplsRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pMplsNpModInfo = &(pFsHwNp->MplsNpModInfo);
    pMplsRemoteNpModInfo = &(pRemoteHwNp->MplsRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tMplsRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
        case FS_MPLS_HW_ENABLE_MPLS:
        {
            break;
        }
        case FS_MPLS_HW_DISABLE_MPLS:
        {
            break;
        }
        case FS_MPLS_HW_GET_PW_CTRL_CHNL_CAPABILITIES:
        {
            tMplsNpWrFsMplsHwGetPwCtrlChnlCapabilities *pLocalArgs = NULL;
       tMplsRemoteNpWrFsMplsHwGetPwCtrlChnlCapabilities *pRemoteArgs =
           NULL;
       FsMplsHwEnableMpls ();
       pLocalArgs =
                &pMplsNpModInfo->MplsNpFsMplsHwGetPwCtrlChnlCapabilities;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->
                MplsRemoteNpFsMplsHwGetPwCtrlChnlCapabilities;
            if (pLocalArgs->pu1PwCCTypeCapabs != NULL)
            {
                MEMCPY (&(pRemoteArgs->u1PwCCTypeCapabs),
                        (pLocalArgs->pu1PwCCTypeCapabs), sizeof (UINT1));
            }
            break;
        }
        case FS_MPLS_HW_CREATE_PW_VC:
        {
            tMplsNpWrFsMplsHwCreatePwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreatePwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwCreatePwVc;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreatePwVc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVcInfo),
                        (pLocalArgs->pMplsHwVcInfo), sizeof (tMplsHwVcTnlInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_GET_PW_VC:
        case FS_MPLS_HW_TRAVERSE_PW_VC:
        {
            tMplsNpWrFsMplsHwTraversePwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwTraversePwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwTraversePwVc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwTraversePwVc;
            if (pLocalArgs->pMplsHwVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVcInfo),
                        (pLocalArgs->pMplsHwVcInfo), sizeof (tMplsHwVcTnlInfo));
            }
            if (pLocalArgs->pNextMplsHwVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMplsHwVcInfo),
                        (pLocalArgs->pNextMplsHwVcInfo),
                        sizeof (tMplsHwVcTnlInfo));
            }
            break;
        }
        case FS_MPLS_HW_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsHwDeletePwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwDeletePwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwDeletePwVc;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeletePwVc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwDelVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwDelVcInfo),
                        (pLocalArgs->pMplsHwDelVcInfo),
                        sizeof (tMplsHwVcTnlInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_CREATE_I_L_M:
        {
            tMplsNpWrFsMplsHwCreateILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwCreateILM;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateILM;
            if (pLocalArgs->pMplsHwIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwIlmInfo),
                        (pLocalArgs->pMplsHwIlmInfo), sizeof (tMplsHwIlmInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_GET_I_L_M:
        case FS_MPLS_HW_TRAVERSE_I_L_M:
        {
            tMplsNpWrFsMplsHwTraverseILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwTraverseILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwTraverseILM;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwTraverseILM;
            if (pLocalArgs->pMplsHwIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwIlmInfo),
                        (pLocalArgs->pMplsHwIlmInfo), sizeof (tMplsHwIlmInfo));
            }
            if (pLocalArgs->pNextMplsHwIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMplsHwIlmInfo),
                        (pLocalArgs->pNextMplsHwIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            break;
        }
        case FS_MPLS_HW_DELETE_I_L_M:
        {
            tMplsNpWrFsMplsHwDeleteILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwDeleteILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwDeleteILM;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteILM;
            if (pLocalArgs->pMplsHwDelIlmParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwDelIlmParams),
                        (pLocalArgs->pMplsHwDelIlmParams),
                        sizeof (tMplsHwIlmInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_CREATE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwCreateL3FTN *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateL3FTN *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwCreateL3FTN;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateL3FTN;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3FTNInfo),
                        (pLocalArgs->pMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            break;
        }
        case FS_MPLS_HW_GET_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwGetL3FTN *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwGetL3FTN *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwGetL3FTN;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwGetL3FTN;
            if (pLocalArgs->pMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3FTNInfo),
                        (pLocalArgs->pMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            if (pLocalArgs->pNextMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMplsHwL3FTNInfo),
                        (pLocalArgs->pNextMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            break;
        }
        case FS_MPLS_HW_TRAVERSE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwTraverseL3FTN *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwTraverseL3FTN *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwTraverseL3FTN;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwTraverseL3FTN;
            if (pLocalArgs->pMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3FTNInfo),
                        (pLocalArgs->pMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            if (pLocalArgs->pNextMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMplsHwL3FTNInfo),
                        (pLocalArgs->pNextMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            break;
        }
        case FS_MPLS_WP_HW_DELETE_L3_F_T_N:
        {
            tMplsNpWrFsMplsWpHwDeleteL3FTN *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsWpHwDeleteL3FTN *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsWpHwDeleteL3FTN;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsWpHwDeleteL3FTN;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwL3FTNInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3FTNInfo),
                        (pLocalArgs->pMplsHwL3FTNInfo),
                        sizeof (tMplsHwL3FTNInfo));
            }
            break;
        }
        case FS_MPLS_HW_CREATE_VPLS_VPN:
        {
            tMplsNpWrFsMplsHwCreateVplsVpn *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwCreateVplsVpn *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwCreateVplsVpn;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateVplsVpn;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVpnInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVpnInfo),
                        (pLocalArgs->pMplsHwVplsVpnInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            break;
        }
        case FS_MPLS_HW_DELETE_VPLS_VPN:
        {
            tMplsNpWrFsMplsHwDeleteVplsVpn *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwDeleteVplsVpn *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVplsVpn;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteVplsVpn;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVpnInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVpnInfo),
                        (pLocalArgs->pMplsHwVplsVpnInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            break;
        }
        case FS_MPLS_HW_VPLS_ADD_PW_VC:
        {
            tMplsNpWrFsMplsHwVplsAddPwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwVplsAddPwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwVplsAddPwVc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwVplsAddPwVc;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_VPLS_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsHwVplsDeletePwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwVplsDeletePwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwVplsDeletePwVc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwVplsDeletePwVc;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
#ifdef HVPLS_WANTED
        case FS_MPLS_REGISTER_FWD_ALARM:
        {
            tMplsNpWrFsMplsRegisterFwdAlarm *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsRegisterFwdAlarm *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsRegisterFwdAlarm;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsRegisterFwdAlarm;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVpnInfo != NULL)
             {
                 MEMCPY (&(pRemoteArgs->MplsHwVplsVpnInfo),
                        (pLocalArgs->pMplsHwVplsVpnInfo), sizeof (tMplsHwVplsInfo));
             }

            break;
        }
        case FS_MPLS_DE_REGISTER_FWD_ALARM:
        {
            tMplsNpWrFsMplsDeRegisterFwdAlarm *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsDeRegisterFwdAlarm *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsDeRegisterFwdAlarm;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsDeRegisterFwdAlarm;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            break;
        }
#endif

        case NP_MPLS_CREATE_MPLS_INTERFACE:
        {
            tMplsNpWrNpMplsCreateMplsInterface *pLocalArgs = NULL;
            tMplsRemoteNpWrNpMplsCreateMplsInterface *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpNpMplsCreateMplsInterface;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpNpMplsCreateMplsInterface;
           
            if (pLocalArgs->pMplsHwMplsIntInfo != NULL)
            {

                MEMCPY (&(pRemoteArgs->MplsHwMplsIntInfo),
                        (pLocalArgs->pMplsHwMplsIntInfo), sizeof (tMplsHwMplsIntInfo));
            }
            break;
            
        }
        case FS_MPLS_HW_ADD_MPLS_ROUTE:
        {
            tMplsNpWrFsMplsHwAddMplsRoute *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwAddMplsRoute *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsRoute;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddMplsRoute;
            pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
            pRemoteArgs->u4IpDestAddr = pLocalArgs->u4IpDestAddr;
            pRemoteArgs->u4IpSubNetMask = pLocalArgs->u4IpSubNetMask;
            MEMCPY (&(pRemoteArgs->routeEntry), &(pLocalArgs->routeEntry),
                    sizeof (tFsNpNextHopInfo));
            if (pLocalArgs->pbu1TblFull != NULL)
            {
                MEMCPY (&(pRemoteArgs->bu1TblFull), (pLocalArgs->pbu1TblFull),
                        sizeof (UINT1));
            }
            break;
        }
        #if defined (IP6_WANTED) && defined (MPLS_IPV6_WANTED)
        case FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE:
           {
               tMplsNpWrFsMplsHwAddMplsIpv6Route *pLocalArgs = NULL;
               tMplsRemoteNpWrFsMplsHwAddMplsIpv6Route *pRemoteArgs = NULL;
               pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsIpv6Route;
               pRemoteArgs =
                    &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddMplsIpv6Route;
               pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
               if (pLocalArgs->pu1Ip6Prefix != NULL)
                   {
                    MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                              sizeof (tIp6Addr));
                   }
               pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
               if (pLocalArgs->pu1NextHop != NULL)
                   {
                     MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                            sizeof (tIp6Addr));
                   }  
               pRemoteArgs->u4NHType = pLocalArgs->u4NHType;
               if (pLocalArgs->pIntInfo != NULL)
                   {
                    MEMCPY (&(pRemoteArgs->IntInfo), (pLocalArgs->pIntInfo),
                            sizeof (tFsNpIntInfo));
                   }
               break;
           } 
       case FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE:
          {
              tMplsNpWrFsMplsHwDeleteMplsIpv6Route *pLocalArgs = NULL;
              tMplsRemoteNpWrFsMplsHwDeleteMplsIpv6Route *pRemoteArgs = NULL;
              pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwDeleteMplsIpv6Route;
              pRemoteArgs =
                   &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteMplsIpv6Route;
              pRemoteArgs->u4VrId = pLocalArgs->u4VrId;
              if (pLocalArgs->pu1Ip6Prefix != NULL)
                   {
                      MEMCPY (&(pRemoteArgs->u1Ip6Prefix), (pLocalArgs->pu1Ip6Prefix),
                                 sizeof (tIp6Addr));
                   }
              pRemoteArgs->u1PrefixLen = pLocalArgs->u1PrefixLen;
              if (pLocalArgs->pu1NextHop != NULL)
                   {
                    MEMCPY (&(pRemoteArgs->u1NextHop), (pLocalArgs->pu1NextHop),
                        sizeof (tIp6Addr));
                   } 
              pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
              if (pLocalArgs->pRouteInfo != NULL)
                   {
                    MEMCPY (&(pRemoteArgs->RouteInfo), (pLocalArgs->pRouteInfo),
                            sizeof (tFsNpRouteInfo));
                   }

              break;
           }
        #endif
        case FS_MPLS_HW_GET_MPLS_STATS:
        {
            tMplsNpWrFsMplsHwGetMplsStats *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwGetMplsStats *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwGetMplsStats;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwGetMplsStats;
            if (pLocalArgs->pInputParams != NULL)
            {
                MEMCPY (&(pRemoteArgs->InputParams), (pLocalArgs->pInputParams),
                        sizeof (tMplsInputParams));
            }
            MEMCPY (&(pRemoteArgs->StatsType), &(pLocalArgs->StatsType),
                    sizeof (tStatsType));
            if (pLocalArgs->pStatsInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->StatsInfo), (pLocalArgs->pStatsInfo),
                        sizeof (tStatsInfo));
            }
            break;
        }
        case FS_MPLS_HW_ADD_VPN_AC:
        {
            tMplsNpWrFsMplsHwAddVpnAc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwAddVpnAc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwAddVpnAc;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddVpnAc;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_DELETE_VPN_AC:
        {
            tMplsNpWrFsMplsHwDeleteVpnAc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwDeleteVpnAc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVpnAc;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteVpnAc;
            pRemoteArgs->u4VplsInstance = pLocalArgs->u4VplsInstance;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            if (pLocalArgs->pMplsHwVplsVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVplsVcInfo),
                        (pLocalArgs->pMplsHwVplsVcInfo),
                        sizeof (tMplsHwVplsInfo));
            }
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpAddILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpAddILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddILM;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpAddILM;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            if (pLocalArgs->pMplsHwP2mpIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwP2mpIlmInfo),
                        (pLocalArgs->pMplsHwP2mpIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpRemoveILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpRemoveILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveILM;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpRemoveILM;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            if (pLocalArgs->pMplsHwP2mpIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwP2mpIlmInfo),
                        (pLocalArgs->pMplsHwP2mpIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            break;
        }
        case FS_MPLS_HW_P2MP_TRAVERSE_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpTraverseILM *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpTraverseILM *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwP2mpTraverseILM;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpTraverseILM;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            if (pLocalArgs->pMplsHwP2mpIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwP2mpIlmInfo),
                        (pLocalArgs->pMplsHwP2mpIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            if (pLocalArgs->pNextMplsHwP2mpIlmInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->NextMplsHwP2mpIlmInfo),
                        (pLocalArgs->pNextMplsHwP2mpIlmInfo),
                        sizeof (tMplsHwIlmInfo));
            }
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_BUD:
        {
            tMplsNpWrFsMplsHwP2mpAddBud *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpAddBud *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddBud;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpAddBud;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_BUD:
        {
            tMplsNpWrFsMplsHwP2mpRemoveBud *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwP2mpRemoveBud *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveBud;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpRemoveBud;
            pRemoteArgs->u4P2mpId = pLocalArgs->u4P2mpId;
            pRemoteArgs->u4Action = pLocalArgs->u4Action;
            break;
        }
        case FS_MPLS_HW_MODIFY_PW_VC:
        {
            tMplsNpWrFsMplsHwModifyPwVc *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwModifyPwVc *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwModifyPwVc;
            pRemoteArgs = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwModifyPwVc;
            pRemoteArgs->u4VpnId = pLocalArgs->u4VpnId;
            pRemoteArgs->u4OperActVpnId = pLocalArgs->u4OperActVpnId;
            if (pLocalArgs->pMplsHwVcInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwVcInfo),
                        (pLocalArgs->pMplsHwVcInfo), sizeof (tMplsHwVcTnlInfo));
            }
            break;
        }
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_HW_L3VPN_INGRESS_MAP:
        {
            tMplsNpWrFsMplsHwL3vpnIngressMap *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwL3vpnIngressMap *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnIngressMap;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnIngressMap;
            if (pLocalArgs->pMplsHwL3vpnIgressInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3vpnIgressInfo),
                        (pLocalArgs->pMplsHwL3vpnIgressInfo),
                        sizeof (tMplsHwL3vpnIgressInfo));
            }
            break;
        }
        case FS_MPLS_HW_L3VPN_EGRESS_MAP:
        {
            tMplsNpWrFsMplsHwL3vpnEgressMap *pLocalArgs = NULL;
            tMplsRemoteNpWrFsMplsHwL3vpnEgressMap *pRemoteArgs = NULL;
            pLocalArgs = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnEgressMap;
            pRemoteArgs =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnEgressMap;
            if (pLocalArgs->pMplsHwL3vpnEgressInfo != NULL)
            {
                MEMCPY (&(pRemoteArgs->MplsHwL3vpnEgressInfo),
                        (pLocalArgs->pMplsHwL3vpnEgressInfo),
                        sizeof (tMplsHwL3vpnEgressInfo));
            }
            break;
        }
#endif
#endif /* MPLS_WANTED */
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_

        case FS_MPLS_MBSM_HW_ENABLE_MPLS:
        case FS_MPLS_MBSM_HW_CREATE_PW_VC:
        case FS_MPLS_MBSM_HW_CREATE_I_L_M:
        case FS_MPLS_MBSM_HW_CREATE_L3_F_T_N:
        case FS_MPLS_MBSM_HW_CREATE_VPLS_VPN:
        case FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC:
        case FS_MPLS_MBSM_HW_ADD_VPN_AC:
        case NP_MPLS_MBSM_CREATE_MPLS_INTERFACE:
        case FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M:
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP:
        case FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP:
#endif
        {
            break;
        }
#endif
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMplsConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMplsConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;

    u4Opcode = pRemoteHwNp->u4Opcode;

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_

        case FS_MPLS_MBSM_HW_ENABLE_MPLS:
        case FS_MPLS_MBSM_HW_CREATE_PW_VC:
        case FS_MPLS_MBSM_HW_CREATE_I_L_M:
        case FS_MPLS_MBSM_HW_CREATE_L3_F_T_N:
        case FS_MPLS_MBSM_HW_CREATE_VPLS_VPN:
        case FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC:
        case FS_MPLS_MBSM_HW_ADD_VPN_AC:
        case NP_MPLS_MBSM_CREATE_MPLS_INTERFACE:
        case FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M:
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP:
        case FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP:
#endif
        {
            break;
        }

#endif
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilMplsRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMplsNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilMplsRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                              tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMplsRemoteNpModInfo *pMplsRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pMplsRemoteNpModInfo = &(pRemoteHwNpInput->MplsRemoteNpModInfo);

    switch (u4Opcode)
    {
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
        case FS_MPLS_HW_ENABLE_MPLS:
        {
            u1RetVal = FsMplsHwEnableMpls ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DISABLE_MPLS:
        {
            u1RetVal = (UINT1)FsMplsHwDisableMpls ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_CREATE_PW_VC:
        {
            tMplsRemoteNpWrFsMplsHwCreatePwVc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreatePwVc;
            u1RetVal =
                (UINT1)FsMplsHwCreatePwVc (pInput->u4VpnId, &(pInput->MplsHwVcInfo),
                                    pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DELETE_PW_VC:
        {
            tMplsRemoteNpWrFsMplsHwDeletePwVc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeletePwVc;
            u1RetVal =
                (UINT1)FsMplsHwDeletePwVc (pInput->u4VpnId, &(pInput->MplsHwDelVcInfo),
                                    pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_CREATE_I_L_M:
        {
            tMplsRemoteNpWrFsMplsHwCreateILM *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateILM;
            u1RetVal =
                (UINT1)FsMplsHwCreateILM (&(pInput->MplsHwIlmInfo), pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DELETE_I_L_M:
        {
            tMplsRemoteNpWrFsMplsHwDeleteILM *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteILM;
            u1RetVal =
                (UINT1)FsMplsHwDeleteILM (&(pInput->MplsHwDelIlmParams),
                                   pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_CREATE_L3_F_T_N:
        {
            tMplsRemoteNpWrFsMplsHwCreateL3FTN *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateL3FTN;
            u1RetVal =
                (UINT1)FsMplsHwCreateL3FTN (pInput->u4VpnId,
                                     &(pInput->MplsHwL3FTNInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_WP_HW_DELETE_L3_F_T_N:
        {
            tMplsRemoteNpWrFsMplsWpHwDeleteL3FTN *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsWpHwDeleteL3FTN;
            u1RetVal =
                (UINT1)FsMplsWpHwDeleteL3FTN (pInput->u4VpnId,
                                       &(pInput->MplsHwL3FTNInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_CREATE_VPLS_VPN:
        {
            tMplsRemoteNpWrFsMplsHwCreateVplsVpn *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwCreateVplsVpn;
            u1RetVal =
                (UINT1)FsMplsHwCreateVplsVpn (pInput->u4VplsInstance, pInput->u4VpnId,
                                       &(pInput->MplsHwVplsVpnInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DELETE_VPLS_VPN:
        {
            tMplsRemoteNpWrFsMplsHwDeleteVplsVpn *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteVplsVpn;
            u1RetVal =
                (UINT1)FsMplsHwDeleteVplsVpn (pInput->u4VplsInstance, pInput->u4VpnId,
                                       &(pInput->MplsHwVplsVpnInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_VPLS_ADD_PW_VC:
        {
            tMplsRemoteNpWrFsMplsHwVplsAddPwVc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwVplsAddPwVc;
            u1RetVal =
                (UINT1)FsMplsHwVplsAddPwVc (pInput->u4VplsInstance, pInput->u4VpnId,
                                     &(pInput->MplsHwVplsVcInfo),
                                     pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_VPLS_DELETE_PW_VC:
        {
            tMplsRemoteNpWrFsMplsHwVplsDeletePwVc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwVplsDeletePwVc;
            u1RetVal =
                (UINT1)FsMplsHwVplsDeletePwVc (pInput->u4VplsInstance, pInput->u4VpnId,
                                        &(pInput->MplsHwVplsVcInfo),
                                        pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef HVPLS_WANTED
        case FS_MPLS_REGISTER_FWD_ALARM:
        {
            tMplsRemoteNpWrFsMplsRegisterFwdAlarm *pInput = NULL;
            pInput =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsRegisterFwdAlarm;
            u1RetVal =
                (UINT1) FsMplsRegisterFwdAlarm (pInput->u4VpnId, &(pInput->MplsHwVplsVpnInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_DE_REGISTER_FWD_ALARM:
        {
            tMplsRemoteNpWrFsMplsDeRegisterFwdAlarm *pInput = NULL;
            pInput =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsDeRegisterFwdAlarm;
            u1RetVal =
                (UINT1) FsMplsDeRegisterFwdAlarm (pInput->u4VpnId);
            break;
        }
#endif
        case NP_MPLS_CREATE_MPLS_INTERFACE:
        {
            tMplsRemoteNpWrNpMplsCreateMplsInterface *pInput = NULL;
            pInput =
                &pMplsRemoteNpModInfo->MplsRemoteNpNpMplsCreateMplsInterface;
            u1RetVal =
                (UINT1)NpMplsCreateMplsInterfaceWr (&(pInput->MplsHwMplsIntInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_ADD_MPLS_ROUTE:
        {
            tMplsRemoteNpWrFsMplsHwAddMplsRoute *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddMplsRoute;
            u1RetVal =
                (UINT1)FsMplsHwAddMplsRoute (pInput->u4VrId, pInput->u4IpDestAddr,
                                      pInput->u4IpSubNetMask,
                                      pInput->routeEntry,
                                      &(pInput->bu1TblFull));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef MPLS_IPV6_WANTED
        case FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE:
        {
            tMplsRemoteNpWrFsMplsHwAddMplsIpv6Route *pInput = NULL;
            pInput =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddMplsIpv6Route;
            u1RetVal =
                (UINT1)FsMplsHwAddMplsIpv6Route (pInput->u4VrId,
                                          (UINT1*)(&(pInput->u1Ip6Prefix)),
                                          pInput->u1PrefixLen,
                                          (UINT1*)(&(pInput->u1NextHop)),
                                          pInput->u4NHType, &(pInput->IntInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE:
        {
            tMplsRemoteNpWrFsMplsHwDeleteMplsIpv6Route *pInput = NULL;
            pInput =
                &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteMplsIpv6Route;
            u1RetVal =
                (UINT1)FsMplsHwDeleteMplsIpv6Route (pInput->u4VrId,
                                             (UINT1*)(&(pInput->u1Ip6Prefix)),
                                             pInput->u1PrefixLen,
                                             (UINT1*)(&(pInput->u1NextHop)),
                                             pInput->u4IfIndex,&(pInput->RouteInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif
        case FS_MPLS_HW_GET_MPLS_STATS:
        {
            tMplsRemoteNpWrFsMplsHwGetMplsStats *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwGetMplsStats;
            u1RetVal =
                (UINT1)FsMplsHwGetMplsStats (&(pInput->InputParams), pInput->StatsType,
                                      &(pInput->StatsInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_ADD_VPN_AC:
        {
            tMplsRemoteNpWrFsMplsHwAddVpnAc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwAddVpnAc;
            u1RetVal =
                (UINT1)FsMplsHwAddVpnAc (pInput->u4VplsInstance, pInput->u4VpnId,
                                  &(pInput->MplsHwVplsVcInfo),
                                  pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_DELETE_VPN_AC:
        {
            tMplsRemoteNpWrFsMplsHwDeleteVpnAc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwDeleteVpnAc;
            u1RetVal =
                (UINT1)FsMplsHwDeleteVpnAc (pInput->u4VplsInstance, pInput->u4VpnId,
                                     &(pInput->MplsHwVplsVcInfo),
                                     pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_I_L_M:
        {
            tMplsRemoteNpWrFsMplsHwP2mpAddILM *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpAddILM;
            u1RetVal =
                (UINT1)FsMplsHwP2mpAddILM (pInput->u4P2mpId,
                                    &(pInput->MplsHwP2mpIlmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_I_L_M:
        {
            tMplsRemoteNpWrFsMplsHwP2mpRemoveILM *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpRemoveILM;
            u1RetVal =
                (UINT1)FsMplsHwP2mpRemoveILM (pInput->u4P2mpId,
                                       &(pInput->MplsHwP2mpIlmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_P2MP_TRAVERSE_I_L_M:
        {
            tMplsRemoteNpWrFsMplsHwP2mpTraverseILM *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpTraverseILM;
            u1RetVal =
                (UINT1)FsMplsHwP2mpTraverseILM (pInput->u4P2mpId,
                                         &(pInput->MplsHwP2mpIlmInfo),
                                         &(pInput->NextMplsHwP2mpIlmInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_BUD:
        {
            tMplsRemoteNpWrFsMplsHwP2mpAddBud *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpAddBud;
            u1RetVal = (UINT1)FsMplsHwP2mpAddBud (pInput->u4P2mpId, pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_BUD:
        {
            tMplsRemoteNpWrFsMplsHwP2mpRemoveBud *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwP2mpRemoveBud;
            u1RetVal =
                (UINT1)FsMplsHwP2mpRemoveBud (pInput->u4P2mpId, pInput->u4Action);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_MODIFY_PW_VC:
        {
            tMplsRemoteNpWrFsMplsHwModifyPwVc *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwModifyPwVc;
            u1RetVal =
                (UINT1)FsMplsHwModifyPwVc (pInput->u4VpnId, pInput->u4OperActVpnId,
                                    &(pInput->MplsHwVcInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_HW_L3VPN_INGRESS_MAP:
        {
            tMplsRemoteNpWrFsMplsHwL3vpnIngressMap *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnIngressMap;
            u1RetVal =
                (UINT1)FsMplsHwL3vpnIngressMap (&(pInput->MplsHwL3vpnIgressInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_MPLS_HW_L3VPN_EGRESS_MAP:
        {
            tMplsRemoteNpWrFsMplsHwL3vpnEgressMap *pInput = NULL;
            pInput = &pMplsRemoteNpModInfo->MplsRemoteNpFsMplsHwL3vpnEgressMap;
            u1RetVal =
                (UINT1)FsMplsHwL3vpnEgressMap (&(pInput->MplsHwL3vpnEgressInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* MPLS_WANTED */
#endif
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __MPLS_NPUTIL_C__ */
