/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: lanputil.c,v 1.7 2014/10/08 11:01:24 siva Exp $
 *
 * Description:This file contains the single NP LA function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __LAUTIL_C__
#define __LAUTIL_C__

#include "nputlremnp.h"
#include "npstackutl.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskLaNpPorts                            */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
VOID
NpUtilMaskLaNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                     UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    UINT4               u4StackingModel = 0;
    BOOL1               bIsCentProtOpCode = FNP_FALSE;
#ifdef MBSM_WANTED
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaRemoteNpModInfo *pLaRemoteNpModInfo = NULL;

    pLaNpModInfo = &(pFsHwNp->LaNpModInfo);
    pLaRemoteNpModInfo = &(pRemoteHwNp->LaRemoteNpModInfo);
#else
    UNUSED_PARAM (pRemoteHwNp);
#endif
    UNUSED_PARAM (pi4RpcCallStatus);
    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    switch (pFsHwNp->u4Opcode)
    {
        case FS_LA_HW_CREATE_AGG_GROUP:
        case FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
        case FS_LA_HW_SET_SELECTION_POLICY:
        case FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST:
        case FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
        case FS_LA_HW_DELETE_AGGREGATOR:
        case FS_LA_HW_INIT:
        case FS_LA_HW_DE_INIT:
        case FS_LA_HW_ENABLE_COLLECTION:
        case FS_LA_HW_ENABLE_DISTRIBUTION:
        case FS_LA_HW_DISABLE_COLLECTION:
        case FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        case FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP:
#ifdef L2RED_WANTED
        case FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR:
        case FS_LA_RED_HW_INIT:
        case FS_LA_RED_HW_NP_DELETE_AGGREGATOR:
        case FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR:
        case FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT:
#endif /* L2RED_WANTED */
        {
            break;
        }
        case FS_LA_HW_SET_PORT_CHANNEL_STATUS:
        {
            bIsCentProtOpCode = FNP_TRUE;
            break;
        }
        case FS_LA_HW_DLAG_STATUS:
        case FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP:
        case FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#ifdef L2RED_WANTED
        case FS_LA_GET_NEXT_AGGREGATOR:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            break;
        }
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
        case FS_LA_MBSM_HW_CREATE_AGG_GROUP:
        {
            pRemoteHwNp->u4Opcode = FS_LA_HW_CREATE_AGG_GROUP;

            tLaNpWrFsLaMbsmHwCreateAggGroup *pEntry = NULL;
            tLaRemoteNpWrFsLaHwCreateAggGroup *pRemEntry = NULL;

            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwCreateAggGroup;
            pRemEntry = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwCreateAggGroup;

            pRemEntry->u4AggIndex = pEntry->u4AggIndex;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        {
            pRemoteHwNp->u4Opcode = FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP;

            tLaNpWrFsLaMbsmHwAddPortToConfAggGroup *pEntry = NULL;
            tLaRemoteNpWrFsLaHwAddPortToConfAggGroup *pRemEntry = NULL;

            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddPortToConfAggGroup;
            pRemEntry =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddPortToConfAggGroup;

            pRemEntry->u4PortNumber = pEntry->u4PortNumber;
            pRemEntry->u4AggIndex = pEntry->u4AggIndex;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP:
        {
            pRemoteHwNp->u4Opcode = FS_LA_HW_ADD_LINK_TO_AGG_GROUP;

            tLaNpWrFsLaMbsmHwAddLinkToAggGroup *pEntry = NULL;
            tLaRemoteNpWrFsLaHwAddLinkToAggGroup *pRemEntry = NULL;
            tLaNpWrFsLaHwAddLinkToAggGroup *pLocalEntry = NULL;

            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddLinkToAggGroup;
            pRemEntry = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddLinkToAggGroup;
            pLocalEntry = &pLaNpModInfo->LaNpFsLaHwAddLinkToAggGroup;

            pRemEntry->u4PortNumber = pEntry->u4PortNumber;
            pRemEntry->u4AggIndex = pEntry->u4AggIndex;
            pLocalEntry->pu2HwAggId = NULL;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_LA_MBSM_HW_SET_SELECTION_POLICY:
        {
            pRemoteHwNp->u4Opcode = FS_LA_HW_SET_SELECTION_POLICY;

            tLaNpWrFsLaMbsmHwSetSelectionPolicy *pEntry = NULL;
            tLaRemoteNpWrFsLaHwSetSelectionPolicy *pRemEntry = NULL;

            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwSetSelectionPolicy;
            pRemEntry = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetSelectionPolicy;

            pRemEntry->u4AggIndex = pEntry->u4AggIndex;
            pRemEntry->u1SelectionPolicy = pEntry->u1SelectionPolicy;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
        case FS_LA_MBSM_HW_INIT:
        {
            pRemoteHwNp->u4Opcode = FS_LA_HW_INIT;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif /* MBSM_WANTED */

        default:
            break;
    }
    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * LA protocol will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if ((u4StackingModel == ISS_DISS_STACKING_MODEL) &&
        (bIsCentProtOpCode != FNP_TRUE))
    {
        if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
        }
        else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
        }
        else
        {
            /* don't do anything */
        }
    }

#ifdef MBSM_WANTED
    if ((pFsHwNp->u4Opcode < FS_LA_MBSM_HW_CREATE_AGG_GROUP) ||
        ((pFsHwNp->u4Opcode > FS_LA_MBSM_HW_INIT)))
    {
        if (MbsmIsNpBulkSyncInProgress (NP_LA_MOD) == OSIX_TRUE)
        {
            if (*pu1NpCallStatus == NPUTIL_INVOKE_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_NO_NP_INVOKE;
            }
            else if (*pu1NpCallStatus == NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP)
            {
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
            }

        }
    }
#endif
    return;
}

/***************************************************************************
 *
 *    Function Name       : NpUtilLaConvertLocalToRemoteNp
 *
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *
 *    Input(s)            : pFsHwNp  - Local NP Arguments
 *
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilLaConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaRemoteNpModInfo *pLaRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pLaNpModInfo = &(pFsHwNp->LaNpModInfo);
    pLaRemoteNpModInfo = &(pRemoteHwNp->LaRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length = pRemoteHwNp->i4Length + sizeof (tLaRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_LA_HW_CREATE_AGG_GROUP:
        {
            tLaNpWrFsLaHwCreateAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwCreateAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwCreateAggGroup;
            pRemoteArgs = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwCreateAggGroup;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            MEMCPY (&(pRemoteArgs->u2HwAggId),
                    (pLocalArgs->pu2HwAggId), sizeof (UINT2));
            break;
        }
        case FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
        {
            tLaNpWrFsLaHwAddLinkToAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwAddLinkToAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwAddLinkToAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddLinkToAggGroup;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            MEMCPY (&(pRemoteArgs->u2HwAggId),
                    (pLocalArgs->pu2HwAggId), sizeof (UINT2));
            break;
        }
        case FS_LA_HW_SET_SELECTION_POLICY:
        {
            tLaNpWrFsLaHwSetSelectionPolicy *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwSetSelectionPolicy *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicy;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetSelectionPolicy;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u1SelectionPolicy = pLocalArgs->u1SelectionPolicy;
            break;
        }
        case FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST:
        {
            tLaNpWrFsLaHwSetSelectionPolicyBitList *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwSetSelectionPolicyBitList *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicyBitList;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetSelectionPolicyBitList;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u4SelectionPolicyBitList =
                pLocalArgs->u4SelectionPolicyBitList;
            break;
        }
        case FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaNpWrFsLaHwRemoveLinkFromAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwRemoveLinkFromAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwRemoveLinkFromAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwRemoveLinkFromAggGroup;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            break;
        }
        case FS_LA_HW_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaHwDeleteAggregator *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwDeleteAggregator *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwDeleteAggregator;
            pRemoteArgs = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDeleteAggregator;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
        case FS_LA_HW_DE_INIT:
        {
            break;
        }
        case FS_LA_HW_INIT:
        {
            break;
        }
        case FS_LA_HW_DLAG_STATUS:
        {
            tLaNpWrFsLaHwDlagStatus *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwDlagStatus *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwDlagStatus;
            pRemoteArgs = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagStatus;
            pRemoteArgs->u1DlagStatus = pLocalArgs->u1DlagStatus;

            break;
        }
        case FS_LA_HW_ENABLE_COLLECTION:
        {
            tLaNpWrFsLaHwEnableCollection *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwEnableCollection *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwEnableCollection;
            pRemoteArgs = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwEnableCollection;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
        case FS_LA_HW_ENABLE_DISTRIBUTION:
        {
            tLaNpWrFsLaHwEnableDistribution *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwEnableDistribution *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwEnableDistribution;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwEnableDistribution;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
        case FS_LA_HW_DISABLE_COLLECTION:
        {
            tLaNpWrFsLaHwDisableCollection *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwDisableCollection *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwDisableCollection;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDisableCollection;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
        case FS_LA_HW_SET_PORT_CHANNEL_STATUS:
        {
            tLaNpWrFsLaHwSetPortChannelStatus *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwSetPortChannelStatus *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwSetPortChannelStatus;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetPortChannelStatus;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u2Inst = pLocalArgs->u2Inst;
            pRemoteArgs->u1StpState = pLocalArgs->u1StpState;
            break;
        }
        case FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        {
            tLaNpWrFsLaHwAddPortToConfAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwAddPortToConfAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwAddPortToConfAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddPortToConfAggGroup;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            break;
        }
        case FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP:
        {
            tLaNpWrFsLaHwRemovePortFromConfAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwRemovePortFromConfAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwRemovePortFromConfAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwRemovePortFromConfAggGroup;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            break;
        }
        case FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP:
        {
            tLaNpWrFsLaHwDlagAddLinkToAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwDlagAddLinkToAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwDlagAddLinkToAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagAddLinkToAggGroup;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            pRemoteArgs->pu2HwAggId = pLocalArgs->pu2HwAggId;
            break;
        }
        case FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaNpWrFsLaHwDlagRemoveLinkFromAggGroup *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwDlagRemoveLinkFromAggGroup *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwDlagRemoveLinkFromAggGroup;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagRemoveLinkFromAggGroup;
            pRemoteArgs->u4PortNumber = pLocalArgs->u4PortNumber;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
#ifdef L2RED_WANTED
        case FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaHwCleanAndDeleteAggregator *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaHwCleanAndDeleteAggregator *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaHwCleanAndDeleteAggregator;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwCleanAndDeleteAggregator;
            pRemoteArgs->u4HwAggIndex = pLocalArgs->u4HwAggIndex;
            break;
        }
        case FS_LA_GET_NEXT_AGGREGATOR:
        {
            tLaNpWrFsLaGetNextAggregator *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaGetNextAggregator *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaGetNextAggregator;
            pRemoteArgs = &pLaRemoteNpModInfo->LaRemoteNpFsLaGetNextAggregator;
            pRemoteArgs->i4HwAggIndex = pLocalArgs->i4HwAggIndex;
            MEMCPY (&(pRemoteArgs->HwInfo), (pLocalArgs->pHwInfo),
                    sizeof (tLaHwInfo));
            break;
        }
        case FS_LA_RED_HW_INIT:
        {
            break;
        }
        case FS_LA_RED_HW_NP_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaRedHwNpDeleteAggregator *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaRedHwNpDeleteAggregator *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaRedHwNpDeleteAggregator;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwNpDeleteAggregator;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
        case FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR:
        {
            tLaNpWrFsLaRedHwUpdateDBForAggregator *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaRedHwUpdateDBForAggregator *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaRedHwUpdateDBForAggregator;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwUpdateDBForAggregator;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            MEMCPY (&(pRemoteArgs->ConfigPorts), &(pLocalArgs->ConfigPorts),
                    sizeof (tPortList));
            MEMCPY (&(pRemoteArgs->ActivePorts), &(pLocalArgs->ActivePorts),
                    sizeof (tPortList));
            pRemoteArgs->u1SelectionPolicy = pLocalArgs->u1SelectionPolicy;
            break;
        }
        case FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT:
        {
            tLaNpWrFsLaRedHwNpUpdateDlfMcIpmcPort *pLocalArgs = NULL;
            tLaRemoteNpWrFsLaRedHwNpUpdateDlfMcIpmcPort *pRemoteArgs = NULL;
            pLocalArgs = &pLaNpModInfo->LaNpFsLaRedHwNpUpdateDlfMcIpmcPort;
            pRemoteArgs =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwNpUpdateDlfMcIpmcPort;
            pRemoteArgs->u4AggIndex = pLocalArgs->u4AggIndex;
            break;
        }
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
        case FS_LA_MBSM_HW_CREATE_AGG_GROUP:
        case FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        case FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP:
        case FS_LA_MBSM_HW_SET_SELECTION_POLICY:
        case FS_LA_MBSM_HW_INIT:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilLaConvertRemoteToLocalNp
 *
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilLaConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;

    u4Opcode = pRemoteHwNp->u4Opcode;

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
#ifdef MBSM_WANTED
        case FS_LA_MBSM_HW_CREATE_AGG_GROUP:
        case FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        case FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP:
        case FS_LA_MBSM_HW_SET_SELECTION_POLICY:
        case FS_LA_MBSM_HW_INIT:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *
 *    Function Name       : NpUtilLaRemoteSvcHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tLaNpModInfo
 *
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp
 *
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilLaRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                            tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tLaRemoteNpModInfo *pLaRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pLaRemoteNpModInfo = &(pRemoteHwNpInput->LaRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_LA_HW_CREATE_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwCreateAggGroup *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwCreateAggGroup;
            u1RetVal =
                FsLaHwCreateAggGroup ((UINT2) pInput->u4AggIndex,
                                      &(pInput->u2HwAggId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwAddLinkToAggGroup *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddLinkToAggGroup;
            u1RetVal =
                FsLaHwAddLinkToAggGroup ((UINT2) pInput->u4AggIndex,
                                         (UINT2) pInput->u4PortNumber,
                                         &(pInput->u2HwAggId));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_SET_SELECTION_POLICY:
        {
            tLaRemoteNpWrFsLaHwSetSelectionPolicy *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetSelectionPolicy;
            u1RetVal =
                FsLaHwSetSelectionPolicy ((UINT2) pInput->u4AggIndex,
                                          pInput->u1SelectionPolicy);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST:
        {
            tLaRemoteNpWrFsLaHwSetSelectionPolicyBitList *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetSelectionPolicyBitList;
            u1RetVal =
                FsLaHwSetSelectionPolicyBitList ((UINT2) pInput->u4AggIndex,
                                                 pInput->
                                                 u4SelectionPolicyBitList);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwRemoveLinkFromAggGroup *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwRemoveLinkFromAggGroup;
            u1RetVal =
                FsLaHwRemoveLinkFromAggGroup ((UINT2) pInput->u4AggIndex,
                                              (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DELETE_AGGREGATOR:
        {
            tLaRemoteNpWrFsLaHwDeleteAggregator *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDeleteAggregator;
            u1RetVal = FsLaHwDeleteAggregator ((UINT2) pInput->u4AggIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DE_INIT:
        {
            u1RetVal = FsLaHwDeInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_INIT:
        {
            u1RetVal = FsLaHwInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_ENABLE_COLLECTION:
        {
            tLaRemoteNpWrFsLaHwEnableCollection *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwEnableCollection;
            u1RetVal =
                FsLaHwEnableCollection ((UINT2) pInput->u4AggIndex,
                                        (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_ENABLE_DISTRIBUTION:
        {
            tLaRemoteNpWrFsLaHwEnableDistribution *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwEnableDistribution;
            u1RetVal =
                FsLaHwEnableDistribution ((UINT2) pInput->u4AggIndex,
                                          (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DISABLE_COLLECTION:
        {
            tLaRemoteNpWrFsLaHwDisableCollection *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDisableCollection;
            u1RetVal =
                FsLaHwDisableCollection ((UINT2) pInput->u4AggIndex,
                                         (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_SET_PORT_CHANNEL_STATUS:
        {
            tLaRemoteNpWrFsLaHwSetPortChannelStatus *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwSetPortChannelStatus;
            u1RetVal =
                FsLaHwSetPortChannelStatus ((UINT2) pInput->u4AggIndex,
                                            pInput->u2Inst, pInput->u1StpState);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwAddPortToConfAggGroup *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwAddPortToConfAggGroup;
            u1RetVal =
                FsLaHwAddPortToConfAggGroup ((UINT2) pInput->u4AggIndex,
                                             (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwRemovePortFromConfAggGroup *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwRemovePortFromConfAggGroup;
            u1RetVal =
                FsLaHwRemovePortFromConfAggGroup ((UINT2) pInput->u4AggIndex,
                                                  (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DLAG_STATUS:
        {
            tLaRemoteNpWrFsLaHwDlagStatus *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagStatus;
            u1RetVal = FsLaHwDlagStatus (pInput->u1DlagStatus);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwDlagAddLinkToAggGroup *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagAddLinkToAggGroup;
            u1RetVal =
                FsLaHwDlagAddLinkToAggGroup ((UINT2) pInput->u4AggIndex,
                                             (UINT2) pInput->u4PortNumber,
                                             pInput->pu2HwAggId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaRemoteNpWrFsLaHwDlagRemoveLinkFromAggGroup *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwDlagRemoveLinkFromAggGroup;
            u1RetVal =
                FsLaHwDlagRemoveLinkFromAggGroup ((UINT2) pInput->u4AggIndex,
                                                  (UINT2) pInput->u4PortNumber);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#ifdef L2RED_WANTED
        case FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR:
        {
            tLaRemoteNpWrFsLaHwCleanAndDeleteAggregator *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaHwCleanAndDeleteAggregator;
            u1RetVal =
                FsLaHwCleanAndDeleteAggregator ((UINT2) pInput->u4HwAggIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_RED_HW_INIT:
        {
            u1RetVal = FsLaRedHwInit ();
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_RED_HW_NP_DELETE_AGGREGATOR:
        {
            tLaRemoteNpWrFsLaRedHwNpDeleteAggregator *pInput = NULL;
            pInput = &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwNpDeleteAggregator;
            u1RetVal = FsLaRedHwNpDeleteAggregator ((UINT2) pInput->u4AggIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR:
        {
            tLaRemoteNpWrFsLaRedHwUpdateDBForAggregator *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwUpdateDBForAggregator;
            u1RetVal =
                FsLaRedHwUpdateDBForAggregator ((UINT2) pInput->u4AggIndex,
                                                pInput->ConfigPorts,
                                                pInput->ActivePorts,
                                                pInput->u1SelectionPolicy);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT:
        {
            tLaRemoteNpWrFsLaRedHwNpUpdateDlfMcIpmcPort *pInput = NULL;
            pInput =
                &pLaRemoteNpModInfo->LaRemoteNpFsLaRedHwNpUpdateDlfMcIpmcPort;
            u1RetVal =
                FsLaRedHwNpUpdateDlfMcIpmcPort ((UINT2) pInput->u4AggIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
#endif /* L2RED_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* __LAUTIL_C__ */
