/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmnputil.c,v 1.6 2017/11/14 07:31:19 siva Exp $
 *
 * Description:This file contains the Masking functionalities
 *             required for the remote NP Hardware Programming
 *             (Dual Unit Stacking Environment)
 *******************************************************************/

#ifndef __VCMNPUTIL_C__
#define __VCMNPUTIL_C__

#include "npstackutl.h"
#include "nputlremnp.h"

/***************************************************************************/
/*  Function Name       : NpUtilMaskVcmNpPorts                             */
/*                                                                         */
/*  Description         : This function mask/split the Active and Standby  */
/*                        ports as per the tFsHwNp information and update  */
/*                        the NpCallStatus accordingly. For MBSM calls     */
/*                        corresponding NP of the NON-MBSM calls will be   */
/*                        updated.                                         */
/*                                                                         */
/*  Input(s)            : FsHwNp Param of type tfsHwNp                     */
/*                                                                         */
/*  Output(s)           : NpCallStauts of type UINT1                       */
/*                      : RemoteHwNp of type tRemoteHwNp for MBSM cases    */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
#ifdef VCM_WANTED
VOID
NpUtilMaskVcmNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                      UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus)
{
    tHwPortInfo         HwPortInfo;
#ifdef MBSM_WANTED
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmRemoteNpModInfo *pVcmRemoteNpModInfo = NULL;
#endif
    UNUSED_PARAM (pi4RpcCallStatus);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
#ifdef MBSM_WANTED
    pVcmNpModInfo = &(pFsHwNp->VcmNpModInfo);
    pVcmRemoteNpModInfo = &(pRemoteHwNp->VcmRemoteNpModInfo);
#else
    UNUSED_PARAM (pRemoteHwNp);
#endif

    /* By Default set the call status as NPUTIL_INVOKE_LOCAL_NP */
    *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;

    switch (pFsHwNp->u4Opcode)
    {
        case FS_VCM_HW_CREATE_CONTEXT:
        case FS_VCM_HW_DELETE_CONTEXT:
        case FS_VCM_HW_VRF_COUNTERS:
        {
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }

        case FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        case FS_VCM_HW_MAP_PORT_TO_CONTEXT:
        case FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT:
        case FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS:
        case FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING:
        {
            if (NpUtilIsPortChannel (pFsHwNp->u4IfIndex) == FNP_SUCCESS)
            {
                /* This is a port channel interface
                 * This should be executed both in local and remote 
                 */
                *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            }
            else if (NpUtilIsRemotePort (pFsHwNp->u4IfIndex,
                                         &HwPortInfo) == FNP_SUCCESS)
            {
                /* This is for remote port 
                 * set the NP Call status NPUTIL_INVOKE_REMOTE_NP
                 * for executing this call at the remote unit
                 */
                *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            }
            break;
        }
        case FS_VCM_HW_MAP_VIRTUAL_ROUTER:
        {
            /* VRF Creation/Deletion should be invoked in both units
             * L3 IVR interface to VRF mapping should be invoked in both units
             * L3 Router port to VRF mapping should be done in both uints
             */
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;

            break;
        }

#ifdef MBSM_WANTED

        case FS_VCM_MBSM_HW_CREATE_CONTEXT:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_HW_CREATE_CONTEXT;

            tVcmNpWrFsVcmMbsmHwCreateContext *pEntry = NULL;
            tVcmRemoteNpWrFsVcmHwCreateContext *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwCreateContext;
            pRemEntry = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwCreateContext;

            pRemEntry->u4ContextId = pEntry->u4ContextId;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT;

            tVcmNpWrFsVcmMbsmHwMapIfIndexToBrgPort *pEntry = NULL;
            tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapIfIndexToBrgPort;
            pRemEntry =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapIfIndexToBrgPort;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            pRemEntry->ContextInfo = pEntry->ContextInfo;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_HW_MAP_PORT_TO_CONTEXT;

            tVcmNpWrFsVcmMbsmHwMapPortToContext *pEntry = NULL;
            tVcmRemoteNpWrFsVcmHwMapPortToContext *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapPortToContext;
            pRemEntry =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapPortToContext;

            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_HW_MAP_VIRTUAL_ROUTER;

            tVcmNpWrFsVcmMbsmHwMapVirtualRouter *pEntry = NULL;
            tVcmRemoteNpWrFsVcmHwMapVirtualRouter *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapVirtualRouter;
            pRemEntry =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapVirtualRouter;

            MEMCPY (&(pRemEntry->VrMapAction), &(pEntry->VrMapAction),
                    sizeof (tVrMapAction));

            MEMCPY (&(pRemEntry->VcmVrMapInfo),
                    (pEntry->pVcmVrMapInfo), sizeof (tFsNpVcmVrMapInfo));

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS;

            tVcmNpWrFsVcmSispMbsmHwSetPortCtrlStatus *pEntry = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortCtrlStatus;
            pRemEntry =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortCtrlStatus;

            pRemEntry->u1Status = pEntry->u1Status;
            pRemEntry->u4IfIndex = pEntry->u4IfIndex;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
        case FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING:
        {
            pRemoteHwNp->u4Opcode = FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING;

            tVcmNpWrFsVcmSispMbsmHwSetPortVlanMapping *pEntry = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping *pRemEntry = NULL;

            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortVlanMapping;
            pRemEntry =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortVlanMapping;

            pRemEntry->u4IfIndex = pEntry->u4IfIndex;
            MEMCPY (&(pRemEntry->VlanId), &(pEntry->VlanId), sizeof (tVlanId));
            pRemEntry->u4ContextId = pEntry->u4ContextId;
            pRemEntry->u1Status = pEntry->u1Status;

            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;

            break;
        }
#endif /* MBSM_WANTED */

        default:
            break;
    }

    /* In case of Stacking model as ISS_DISS_STACKING_MODEL, 
     * VCM protocol will operate in Distributed mode and hence
     * the NPAPI invocation type is LOCAL for LOCAL_AND_REMOTE, and
     * NO_NP_INVOKE for REMOTE */
    if (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL)
    {
        *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_NP;
    }
    return;
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVcmConvertLocalToRemoteNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Local NP Arguments
 *                          to Remote NP Arguments. Local Np can have pointers in the
 *                          input. Remote Np will have only values. Local NP Pointers
 *                          are dereferenced and the values are copied to the Remote NP
 *                                                                          
 *    Input(s)            : pFsHwNp  - Local NP Arguments                     
 *                                                                          
 *    Output(s)           : pRemoteHwNp - Remote NP Arguments               
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVcmConvertLocalToRemoteNp (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmRemoteNpModInfo *pVcmRemoteNpModInfo = NULL;

    u4Opcode = pFsHwNp->u4Opcode;
    pVcmNpModInfo = &(pFsHwNp->VcmNpModInfo);
    pVcmRemoteNpModInfo = &(pRemoteHwNp->VcmRemoteNpModInfo);

    pRemoteHwNp->NpModuleId = pFsHwNp->NpModuleId;
    pRemoteHwNp->u4Opcode = pFsHwNp->u4Opcode;
    pRemoteHwNp->i4Length =
        pRemoteHwNp->i4Length + sizeof (tVcmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_VCM_HW_CREATE_CONTEXT:
        {
            tVcmNpWrFsVcmHwCreateContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwCreateContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwCreateContext;
            pRemoteArgs = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwCreateContext;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_VCM_HW_DELETE_CONTEXT:
        {
            tVcmNpWrFsVcmHwDeleteContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwDeleteContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwDeleteContext;
            pRemoteArgs = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwDeleteContext;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            break;
        }
        case FS_VCM_HW_VRF_COUNTERS:
        {

            tVcmNpWrFsVrfCounters *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVrfCounters *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpWrFsVrfCounters;
            pRemoteArgs = &pVcmRemoteNpModInfo->VcmRemoteNpWrFsVrfCounters;
            pRemoteArgs->VrfCounter = pLocalArgs->VrfCounter;
            break;

        }
        case FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            tVcmNpWrFsVcmHwMapIfIndexToBrgPort *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapIfIndexToBrgPort;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapIfIndexToBrgPort;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->ContextInfo), &(pLocalArgs->ContextInfo),
                    sizeof (tContextMapInfo));
            break;
        }
        case FS_VCM_HW_MAP_PORT_TO_CONTEXT:
        {
            tVcmNpWrFsVcmHwMapPortToContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapPortToContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapPortToContext;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapPortToContext;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_VCM_HW_MAP_VIRTUAL_ROUTER:
        {
            tVcmNpWrFsVcmHwMapVirtualRouter *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapVirtualRouter *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapVirtualRouter;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapVirtualRouter;
            MEMCPY (&(pRemoteArgs->VrMapAction), &(pLocalArgs->VrMapAction),
                    sizeof (tVrMapAction));
            MEMCPY (&(pRemoteArgs->VcmVrMapInfo),
                    (pLocalArgs->pVcmVrMapInfo), sizeof (tFsNpVcmVrMapInfo));
            break;
        }
        case FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT:
        {
            tVcmNpWrFsVcmHwUnMapPortFromContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwUnMapPortFromContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwUnMapPortFromContext;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwUnMapPortFromContext;
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS:
        {
            tVcmNpWrFsVcmSispHwSetPortCtrlStatus *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortCtrlStatus;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortCtrlStatus;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING:
        {
            tVcmNpWrFsVcmSispHwSetPortVlanMapping *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortVlanMapping;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortVlanMapping;
            pRemoteArgs->u4IfIndex = pLocalArgs->u4IfIndex;
            MEMCPY (&(pRemoteArgs->VlanId), &(pLocalArgs->VlanId),
                    sizeof (tVlanId));
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            break;
        }
#ifdef MBSM_WANTED
        case FS_VCM_MBSM_HW_CREATE_CONTEXT:
        case FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        case FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT:
        case FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER:
        case FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS:
        case FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVcmConvertRemoteToLocalNp                                         
 *                                                                          
 *    Description         : This function takes care of converting Remote NP Arguments
 *                          to Local NP Arguments. Remote Np can have values in the
 *                          input. Local Np will have only values. Remote NP Pointers
 *                          are dereferenced and the values are copied to the Local NP
 *                                                                          
 *    Input(s)            : pRemoteHwNp - Remote NP Arguments
 *                                                                          
 *    Output(s)           : pFsHwNp  - Local NP Arguments
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVcmConvertRemoteToLocalNp (tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmRemoteNpModInfo *pVcmRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNp->u4Opcode;
    pVcmNpModInfo = &(pFsHwNp->VcmNpModInfo);
    pVcmRemoteNpModInfo = &(pRemoteHwNp->VcmRemoteNpModInfo);

    pFsHwNp->NpModuleId = pRemoteHwNp->NpModuleId;
    pFsHwNp->u4Opcode = pRemoteHwNp->u4Opcode;

    switch (u4Opcode)
    {
        case FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            tVcmNpWrFsVcmHwMapIfIndexToBrgPort *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapIfIndexToBrgPort;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapIfIndexToBrgPort;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->ContextInfo), &(pRemoteArgs->ContextInfo),
                    sizeof (tContextMapInfo));
            break;
        }
        case FS_VCM_HW_MAP_PORT_TO_CONTEXT:
        {
            tVcmNpWrFsVcmHwMapPortToContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapPortToContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapPortToContext;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapPortToContext;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_VCM_HW_MAP_VIRTUAL_ROUTER:
        {
            tVcmNpWrFsVcmHwMapVirtualRouter *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwMapVirtualRouter *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwMapVirtualRouter;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapVirtualRouter;
            MEMCPY (&(pLocalArgs->VrMapAction), &(pRemoteArgs->VrMapAction),
                    sizeof (tVrMapAction));
            MEMCPY ((pLocalArgs->pVcmVrMapInfo),
                    &(pRemoteArgs->VcmVrMapInfo), sizeof (tFsNpVcmVrMapInfo));
            break;
        }
        case FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT:
        {
            tVcmNpWrFsVcmHwUnMapPortFromContext *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmHwUnMapPortFromContext *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmHwUnMapPortFromContext;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwUnMapPortFromContext;
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS:
        {
            tVcmNpWrFsVcmSispHwSetPortCtrlStatus *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortCtrlStatus;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortCtrlStatus;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING:
        {
            tVcmNpWrFsVcmSispHwSetPortVlanMapping *pLocalArgs = NULL;
            tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping *pRemoteArgs = NULL;
            pLocalArgs = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortVlanMapping;
            pRemoteArgs =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortVlanMapping;
            pLocalArgs->u4IfIndex = pRemoteArgs->u4IfIndex;
            MEMCPY (&(pLocalArgs->VlanId), &(pRemoteArgs->VlanId),
                    sizeof (tVlanId));
            pLocalArgs->u4ContextId = pRemoteArgs->u4ContextId;
            pLocalArgs->u1Status = pRemoteArgs->u1Status;
            break;
        }
#ifdef MBSM_WANTED
        case FS_VCM_MBSM_HW_CREATE_CONTEXT:
        case FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        case FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT:
        case FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER:
        case FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS:
        case FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING:
        {
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NpUtilVcmRemoteSvcHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tVcmNpModInfo
 *                                                                          
 *    Input(s)            : pRemoteHwNpInput of type tRemoteHwNp                     
 *                                                                          
 *    Output(s)           : pRemoteHwNpOutput of type tRemoteHwNp                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
NpUtilVcmRemoteSvcHwProgram (tRemoteHwNp * pRemoteHwNpInput,
                             tRemoteHwNp * pRemoteHwNpOutput)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVcmRemoteNpModInfo *pVcmRemoteNpModInfo = NULL;

    u4Opcode = pRemoteHwNpInput->u4Opcode;
    pVcmRemoteNpModInfo = &(pRemoteHwNpInput->VcmRemoteNpModInfo);

    switch (u4Opcode)
    {
        case FS_VCM_HW_CREATE_CONTEXT:
        {
            tVcmRemoteNpWrFsVcmHwCreateContext *pInput = NULL;
            pInput = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwCreateContext;
            u1RetVal = FsVcmHwCreateContext (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_HW_DELETE_CONTEXT:
        {
            tVcmRemoteNpWrFsVcmHwDeleteContext *pInput = NULL;
            pInput = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwDeleteContext;
            u1RetVal = FsVcmHwDeleteContext (pInput->u4ContextId);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }

        case FS_VCM_HW_VRF_COUNTERS:
        {

            tVcmRemoteNpWrFsVrfCounters *pRemoteArgs = NULL;
            pRemoteArgs = &pVcmRemoteNpModInfo->VcmRemoteNpWrFsVrfCounters;
            u1RetVal = FsVcmHwHandleVrfCounter (pRemoteArgs->VrfCounter);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;

        }

        case FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort *pInput = NULL;
            pInput =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapIfIndexToBrgPort;
            u1RetVal =
                FsVcmHwMapIfIndexToBrgPort (pInput->u4ContextId,
                                            pInput->u4IfIndex,
                                            pInput->ContextInfo);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_HW_MAP_PORT_TO_CONTEXT:
        {
            tVcmRemoteNpWrFsVcmHwMapPortToContext *pInput = NULL;
            pInput = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapPortToContext;
            u1RetVal =
                FsVcmHwMapPortToContext (pInput->u4ContextId,
                                         pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_HW_MAP_VIRTUAL_ROUTER:
        {
            tVcmRemoteNpWrFsVcmHwMapVirtualRouter *pInput = NULL;
            pInput = &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwMapVirtualRouter;
            u1RetVal =
                FsVcmHwMapVirtualRouter (pInput->VrMapAction,
                                         &(pInput->VcmVrMapInfo));
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT:
        {
            tVcmRemoteNpWrFsVcmHwUnMapPortFromContext *pInput = NULL;
            pInput =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmHwUnMapPortFromContext;
            u1RetVal =
                FsVcmHwUnMapPortFromContext (pInput->u4ContextId,
                                             pInput->u4IfIndex);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS:
        {
            tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus *pInput = NULL;
            pInput =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortCtrlStatus;
            u1RetVal =
                FsVcmSispHwSetPortCtrlStatus (pInput->u4IfIndex,
                                              pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING:
        {
            tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping *pInput = NULL;
            pInput =
                &pVcmRemoteNpModInfo->VcmRemoteNpFsVcmSispHwSetPortVlanMapping;
            u1RetVal =
                FsVcmSispHwSetPortVlanMapping (pInput->u4IfIndex,
                                               pInput->VlanId,
                                               pInput->u4ContextId,
                                               pInput->u1Status);
            MEMCPY (pRemoteHwNpOutput, pRemoteHwNpInput, sizeof (tRemoteHwNp));
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /*VCM_WANTED */

#endif /* __VCMNPUTIL_C__ */
