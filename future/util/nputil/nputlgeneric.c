
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: nputlgeneric.c,v 1.8 2015/05/21 13:43:17 siva Exp $
 *
 * Description:This file contains platform specific utility functions 
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef __NPUTIL_GENERIC_C__
#define __NPUTIL_GENERIC_C__

#include "nputlpltfrm.h"
/***************************************************************************/
/*  Function Name       : CustNpUtilMaskNpPorts                            */
/*                                                                         */
/*  Description         : This is a Customized function to Implement       */
/*                        masking for NPAPI's which requires special       */
/*                        handling for each platform.This function splits  */
/*                        the NPAPI PortList argument into Self and Remote */
/*                        ports as per the information and updates in which*/
/*                        Node the NpCall should be executed.              */
/*                                                                         */
/*  Input(s)            : pFsHwNp - Pointer containing the entire NPAPI    */
/*                                  Information.                           */
/*                                                                         */
/*  Output(s)           : pFsHwNp - Port List is updated. Ports in the     */
/*                                  Remote unit will be removed based on   */
/*                                  the Masking Logic of NPAPI             */

/*                      : pRemoteHwNp - Port List is updated. Ports in the */
/*                                      Local unit will be removed based   */
/*                                      on the Masking Logic of NPAPI      */
/*                      : pu1NpCallStatus - In which Node the NPAPI call   */
/*                                          should be executed.            */
/*                                                                         */
/*  Global Variables Referred : None                                       */
/*                                                                         */
/*  Global Variables Modified : None.                                      */
/*                                                                         */
/*  Exceptions or Operating                                                */
/*  System Error Handling     : None.                                      */
/*                                                                         */
/*  Use of Recursion          : None.                                      */
/*                                                                         */
/*  Returns                   : NONE(VOID)                                 */
/***************************************************************************/
INT4
CustNpUtilMaskNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                       UINT1 *pu1NpCallStatus)
{
    tHwPortInfo         LocalHwPortInfo;
    tHwPortInfo         RemoteHwPortInfo;
    UINT4               u4LocalStackPort = 0;
    UINT4               u4RemoteStackPort = 0;

    MEMSET (&LocalHwPortInfo, 0, sizeof (tHwPortInfo));
    MEMSET (&RemoteHwPortInfo, 0, sizeof (tHwPortInfo));

    /* Get the stacking port of the Local and Remote unit */
    CfaGetLocalUnitPortInformation (&LocalHwPortInfo);
    u4LocalStackPort = NpUtilGetStackingPortIndex (&LocalHwPortInfo, 0);
    u4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&RemoteHwPortInfo);

    switch (pFsHwNp->u4Opcode)
    {
#ifdef VLAN_WANTED
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tHwPortArray       *pLocalHwPorts = NULL;
            tRemoteHwPortArray *pRemoteHwPorts = NULL;
            tVlanNpModInfo     *pVlanNpModInfo = NULL;
            tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
            tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pEntry = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pRemEntry = NULL;
            UINT4               u4LocalPortCount = 0;
            UINT4               u4RemotePortCount = 0;

            pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
            pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;
            pRemEntry =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            pLocalHwPorts = pEntry->pHwAllowedToGoPorts;
            pRemoteHwPorts = &(pRemEntry->HwAllowedToGoPorts);

            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
            {
                /* Adding the stacking port to the Local and Remote
                 * Port Array */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else if (u4LocalPortCount != 0)
            {
                /* Adding the stacking port to the Remote
                 * Port Array alone */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
                pRemoteHwPorts->i4Length++;
            }
            else
            {
                /* Adding the stacking port to the Local
                 * Port Array alone */
                if (pLocalHwPorts->pu4PortArray != NULL)
                {
                    pLocalHwPorts->pu4PortArray[u4LocalPortCount] =
                        u4LocalStackPort;
                    pLocalHwPorts->i4Length++;
                }
            }
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            /* Masking is not required .Ports should be programmed as
               such in both the units */
            *pu1NpCallStatus = NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP;
            break;
        }

#else
 	    UNUSED_PARAM(u4RemoteStackPort);
	    UNUSED_PARAM(u4LocalStackPort);
            UNUSED_PARAM (pu1NpCallStatus);
            UNUSED_PARAM (pRemoteHwNp);

#endif

#if defined (VLAN_WANTED) && defined (MBSM_WANTED)
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
        {
            tVlanNpWrFsMiVlanMbsmSyncFDBInfo *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanMbsmSyncFDBInfo *pRemoteArgs = NULL;
            tVlanNpModInfo     *pVlanNpModInfo = NULL;
            tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;

            pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
            pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);
            pLocalArgs = &pVlanNpModInfo->VlanNpFsMiVlanMbsmSyncFDBInfo;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->VlanRemoteNpFsMiVlanMbsmSyncFDBInfo;

            if (pLocalArgs->pFDBInfoArray != NULL)
            {
                MEMCPY (&(pRemoteArgs->FDBInfoArray),
                        (pLocalArgs->pFDBInfoArray), sizeof (tFDBInfoArray));
            }
            /* Since the function with this op code is used to sync the
             * FDB entries learnt in Active(when standby is down) to
             * standby, the function call status is Remote unit
             * execution always */
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;

        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
        {
            /* When Stand-by Node comes up,MBSM call for programming 
               Static Unicast MAC Entry will be triggered .It is converted
               as NON MBSM call and executed in Remote Unit */

            tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry *pLocalArgs = NULL;
            tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry *pRemoteArgs = NULL;
            tVlanNpModInfo     *pVlanNpModInfo = NULL;
            tVlanRemoteNpModInfo *pRemoteVlanNpModInfo = NULL;
            tHwPortArray       *pLocalHwPorts = NULL;
            tRemoteHwPortArray *pRemoteHwPorts = NULL;
            UINT4               u4LocalPortCount = 0;
            UINT4               u4RemotePortCount = 0;

            pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);
            pRemoteVlanNpModInfo = &(pRemoteHwNp->VlanRemoteNpModInfo);
            pLocalArgs =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntry;
            pRemoteArgs =
                &pRemoteVlanNpModInfo->
                VlanRemoteNpFsMiVlanHwAddStaticUcastEntry;
            pLocalHwPorts = pLocalArgs->pHwAllowedToGoPorts;

            pRemoteHwPorts = &(pRemoteArgs->HwAllowedToGoPorts);
            pRemoteArgs->u4ContextId = pLocalArgs->u4ContextId;
            pRemoteArgs->u4Fid = pLocalArgs->u4FdbId;
            pRemoteArgs->u4Port = pLocalArgs->u4RcvPort;

            if (pLocalArgs->MacAddr != NULL)
            {
                MEMCPY (&(pRemoteArgs->MacAddr), (pLocalArgs->MacAddr),
                        sizeof (tMacAddr));
            }

            if (pLocalHwPorts->pu4PortArray != NULL)
            {
                MEMCPY (&(pRemoteHwPorts->au4PortArray[0]),
                        (pLocalHwPorts->pu4PortArray),
                        sizeof (UINT4) * (pLocalHwPorts->i4Length));
            }
            /* Filter the Local ports and Remote ports to the Local
             *  and Remote Port Array respectively */
            NpUtilMaskHwPortArray (pLocalHwPorts, pRemoteHwPorts,
                                   &LocalHwPortInfo, &u4LocalPortCount,
                                   &u4RemotePortCount);
            if ((u4LocalPortCount != 0) && (u4RemotePortCount == 0))
            {
                /* Adding the stacking port to the Remote
                 * Port Array */
                pRemoteHwPorts->au4PortArray[u4RemotePortCount] =
                    u4RemoteStackPort;
            }
            pRemoteHwPorts->i4Length = pLocalHwPorts->i4Length;
            pRemoteArgs->u1Status = pLocalArgs->u1Status;
            pRemoteHwNp->u4Opcode = FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY;
            *pu1NpCallStatus = NPUTIL_INVOKE_REMOTE_NP;
            break;
        }
#endif
        default:
            break;
    }
    return FNP_SUCCESS;
}
#endif
