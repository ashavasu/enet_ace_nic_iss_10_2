/********************************************************************
 * Copyright (C) Aricent Sotware,2008
 *
 * $Id: utilmath.c,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the math library functions
 *              such as exponential,log,power,ceil and floor 
 *
 *******************************************************************/

#include "utilmath.h"

#if (MATH_SUPPORT == FSAP_ON)

/************************************************************************/
/*  Function Name   : UtilCeil                                          */
/*  Description     : This function computes the ceil value             */
/*  Input(s)        : d8Input - for which ceil value is to be computed  */
/*  Output(s)       :                                                   */
/*  Returns         : Ceil value of d8Input                             */
/************************************************************************/

DBL8
UtilCeil (DBL8 d8Input)
{
    INT4                i4HighWord = 0, i4LowWord = 0, i4Exp = 0;
    UINT4               u4Pres = 0, u4Temp = 0;

    if (d8Input == 0)
    {
        return d8Input;
    }

    MATH_HIGH_WORD (i4HighWord, d8Input);
    MATH_LOW_WORD (i4LowWord, d8Input);

    i4Exp = ((i4HighWord >> 20) & 0x7ff) - 0x3ff;    /* Exponent */

    if (i4Exp > 51)
    {
        /* Return NAN or Integer */
        return (i4Exp == 1024) ? MATH_NAN : d8Input;
    }
    else if (i4Exp < 20)
    {
        /* X has no integer part */
        if (i4Exp < 0)
        {
            return (d8Input < 0) ? 0 : 1;
        }
        else
        {
            /* computing fractional part */
            u4Pres = i4HighWord << (12 + i4Exp);
            u4Pres = u4Pres >> (12 + i4Exp);

            /* X has no fractional part */
            if ((u4Pres == 0) && (i4LowWord == 0))
            {
                return d8Input;
            }
            else
            {
                /* Eliminating fractional part */
                i4HighWord = i4HighWord & (~u4Pres);

                if (d8Input > 0)
                {
                    /* adding 1 to the integer part */
                    i4HighWord += (0x00000001 << (20 - i4Exp));
                }
                i4LowWord = 0;
            }
        }
    }
    else
    {
        /* Fractional part */
        u4Pres = ((UINT4) (0xffffffff)) >> (i4Exp - 20);

        if ((i4LowWord & u4Pres) == 0)
        {
            return d8Input;        /* X has no fractional part */
        }
        else
        {
            if (d8Input > 0)
            {
                if (i4Exp == 20)
                {
                    i4HighWord += 1;
                }
                else
                {
                    /* adding 1 to the integer part */
                    u4Temp = i4LowWord + (0x00000001 << (52 - i4Exp));

                    if (u4Temp < (UINT4) i4LowWord)
                    {
                        i4HighWord += 1;    /* got a carry */
                    }
                    i4LowWord = u4Temp;
                }
            }
            /* Eliminating fractional part */
            i4LowWord = i4LowWord & (~u4Pres);
        }
    }

    MATH_INSERT_HIGH_WORD (d8Input, i4HighWord);
    MATH_INSERT_LOW_WORD (d8Input, i4LowWord);
    return d8Input;
}

/************************************************************************/
/*  Function Name   : UtilFloor                                         */
/*  Description     : This function computes the floor value            */
/*  Input(s)        : d8Input - for which floor value is to be computed */
/*  Output(s)       :                                                   */
/*  Returns         : floor value of d8Input                            */
/************************************************************************/

DBL8
UtilFloor (DBL8 d8Input)
{
    INT4                i4HighWord = 0, i4LowWord = 0, i4Exp = 0;
    UINT4               u4Pres = 0, u4Temp = 0;

    if (d8Input == 0)
    {
        return d8Input;
    }

    MATH_HIGH_WORD (i4HighWord, d8Input);
    MATH_LOW_WORD (i4LowWord, d8Input);

    i4Exp = ((i4HighWord >> 20) & 0x7ff) - 0x3ff;    /* Exponent */

    if (i4Exp > 51)
    {
        /* Return NAN or Integer */
        return (i4Exp == 1024) ? MATH_NAN : d8Input;
    }
    else if (i4Exp < 20)
    {
        if (i4Exp < 0)
        {
            /* X has no integer part */
            return (d8Input < 0) ? -1 : 0;
        }
        else
        {
            /* computing fractional part */
            u4Pres = i4HighWord << (12 + i4Exp);
            u4Pres = u4Pres >> (12 + i4Exp);

            /* X has no fractional part */
            if ((u4Pres == 0) && (i4LowWord == 0))
            {
                return d8Input;
            }
            else
            {
                /* Eliminating fractional part */
                i4HighWord = i4HighWord & (~u4Pres);

                if (d8Input < 0)
                {
                    /* adding 1 to the integer part */
                    i4HighWord += (0x00000001 << (20 - i4Exp));
                }
                i4LowWord = 0;
            }
        }
    }
    else
    {
        /* Fractional part */
        u4Pres = ((UINT4) (0xffffffff)) >> (i4Exp - 20);

        if ((i4LowWord & u4Pres) == 0)
        {
            return d8Input;        /* X has no fractional part */
        }
        else
        {
            if (d8Input < 0)
            {
                if (i4Exp == 20)
                {
                    i4HighWord += 1;
                }
                else
                {
                    /* adding 1 to the integer part */
                    u4Temp = i4LowWord + (0x00000001 << (52 - i4Exp));

                    if (u4Temp < (UINT4) i4LowWord)
                    {
                        i4HighWord += 1;    /* got a carry */
                    }
                    i4LowWord = u4Temp;
                }
            }
            /* Eliminating fractional part */
            i4LowWord = i4LowWord & (~u4Pres);
        }
    }

    MATH_INSERT_HIGH_WORD (d8Input, i4HighWord);
    MATH_INSERT_LOW_WORD (d8Input, i4LowWord);

    return d8Input;
}

/************************************************************************/
/*  Function Name   : UtilPow                                           */
/*  Description     : This function computes the power value            */
/*  Input(s)        : dbBase - Base value                               */
/*                  : d8Exp  - Exponent value                           */
/*  Output(s)       :                                                   */
/*  Returns         : d8Pow - power value, base ^ exponent              */
/************************************************************************/

DBL8
UtilPow (DBL8 d8Base, DBL8 d8Exp)
{

    DBL8                d8Pow = 0;
    INT4                i4BaseHighWord = 0, i4BaseLowWord = 0, i4BaseExponent =
        0, i4BaseUnsignedHighWord = 0, i4BaseIsInt = 0, i4BaseMantissa = 0;
    INT4                i4ExpHighWord = 0, i4ExpLowWord = 0, i4ExpExponent =
        0, i4ExpUnsignedHighWord = 0, i4ExpIsInt = 0, i4Sign =
        1, i4ExpMantissa = 0;
    DBL8                d8ExpBy2 = 0;
    DBL8                d8Pres = 0;

    /* Extract high and low words */

    MATH_HIGH_WORD (i4BaseHighWord, d8Base);
    MATH_LOW_WORD (i4BaseLowWord, d8Base);

    MATH_HIGH_WORD (i4ExpHighWord, d8Exp);
    MATH_LOW_WORD (i4ExpLowWord, d8Exp);

    /* unsigned High words */
    i4BaseUnsignedHighWord = i4BaseHighWord & 0x7fffffff;
    i4ExpUnsignedHighWord = i4ExpHighWord & 0x7fffffff;

    if (d8Exp == 0)
    {
        return 1;
    }
    else if (d8Exp == 1)
    {
        return d8Base;
    }
    else
    {
        /* finding exponent whether even, odd or not an Integer */
        /* i4ExpIsInt = 0, Not an Integer */
        /* i4ExpIsInt = 1, Odd Integer */
        /* i4ExpIsInt = 2, Even Integer */
        if (i4ExpUnsignedHighWord >= 0x43400000)
        {
            i4ExpIsInt = 2;        /* even integer */
        }
        else if (i4ExpUnsignedHighWord >= 0x3ff00000)
        {
            i4ExpExponent = ((i4ExpHighWord >> 20) & 0x7ff) - 0x3ff;

            if (i4ExpExponent > 20)
            {
                i4ExpMantissa = i4ExpLowWord >> (52 - i4ExpExponent);

                if ((UINT4) (i4ExpMantissa << (52 - i4ExpExponent))
                    == (UINT4) i4ExpLowWord)
                {
                    i4ExpIsInt = 2 - (i4ExpMantissa & 1);
                }
            }
            else if (i4ExpLowWord == 0)
            {
                i4ExpMantissa = i4ExpUnsignedHighWord >> (20 - i4ExpExponent);

                if ((INT4) (i4ExpMantissa << (20 - i4ExpExponent))
                    == i4ExpUnsignedHighWord)
                {
                    i4ExpIsInt = 2 - (i4ExpMantissa & 1);
                }
            }
        }

    }

    /* If base or exponent is NAN, return NAN */
    if (((i4BaseUnsignedHighWord >= 0x7ff00000) && (i4BaseLowWord != 0)) ||
        ((i4ExpUnsignedHighWord >= 0x7ff00000) && (i4ExpLowWord != 0)))
    {
        return (d8Base + d8Exp);
    }

    /* If Exponent is infinity, return 0 or infinity */
    else if (((i4ExpHighWord == (INT4) 0x7ff00000) ||
              (i4ExpHighWord == (INT4) 0xfff00000)) && (i4BaseLowWord == 0))
    {
        MATH_INSERT_HIGH_WORD (d8Pow, i4BaseUnsignedHighWord);
        MATH_INSERT_LOW_WORD (d8Pow, i4BaseLowWord);

        if (d8Pow > 1)
        {
            return (i4ExpHighWord == 0x7ff00000) ? MATH_INFINITY : 0;
        }
        else if (d8Pow < 1)
        {
            return (i4ExpHighWord == 0x7ff00000) ? 0 : MATH_INFINITY;
        }
        else
        {
            return MATH_NAN;
        }
    }

    /* If base = 0, return 0 or infinity */
    else if (d8Base == 0)
    {
        return (d8Exp > 0) ? 0 : MATH_INFINITY;
    }

    /* If Base is infinity, return 0 or infinity */
    else if (((i4BaseHighWord == (INT4) 0x7ff00000) ||
              (i4BaseHighWord == (INT4) 0xfff00000)) && (i4BaseLowWord == 0))
    {
        return (d8Exp > 0) ? MATH_INFINITY : 0;
    }

    /* If base is negative */
    else if (d8Base < 0)
    {
        if (i4ExpIsInt == 0)
        {
            return MATH_NAN;    /* If exponent is not an Integer */
        }
        else
        {
            MATH_INSERT_HIGH_WORD (d8Base, i4BaseUnsignedHighWord);
            MATH_INSERT_LOW_WORD (d8Base, i4BaseLowWord);

            if (i4ExpIsInt == 1)
            {
                i4Sign = -1;    /* If exponent is Odd */
            }
        }
    }

    /* If exponent is 2, base * base */
    if (d8Exp == 2)
    {
        return d8Base * d8Base;
    }
    else
    {

        /* finding base whether even, odd or not an Integer */
        /* i4BaseIsInt = 0, Not an Integer */
        /* i4BaseIsInt = 1, Odd Integer */
        /* i4BaseIsInt = 2, Even Integer */
        if (i4BaseUnsignedHighWord >= 0x3ff00000)
        {
            i4BaseExponent = ((i4BaseHighWord >> 20) & 0x7ff) - 0x3ff;

            if (i4BaseExponent > 20)
            {
                i4BaseMantissa = i4BaseLowWord >> (52 - i4BaseExponent);

                if ((UINT4) (i4BaseMantissa << (52 - i4BaseExponent))
                    == (UINT4) i4BaseLowWord)
                {
                    i4BaseIsInt = 2 - (i4BaseMantissa & 1);
                }
            }
            else if (i4BaseLowWord == 0)
            {
                i4BaseMantissa = i4BaseUnsignedHighWord >>
                    (20 - i4BaseExponent);

                if ((INT4) (i4BaseMantissa << (20 - i4BaseExponent))
                    == i4BaseUnsignedHighWord)
                {
                    i4BaseIsInt = 2 - (i4BaseMantissa & 1);
                }
            }
        }

    }

    /* compute log(x) */
    d8Pow = UtilLog (d8Base);

    /* If base not an integer */
    if (i4ExpIsInt == 0)
    {
        d8Pow = d8Exp * d8Pow;
        d8Pow = UtilExp (d8Pow);
        return i4Sign * d8Pow;
    }

    /* x^y = x^(y/2) * x^(y/2) */
    d8ExpBy2 = d8Exp / 2;

    /* z = y * (log(x)) */
    d8Pow = d8Pow * d8ExpBy2;

    /* z = exp (y * (log(x))) */
    d8Pow = UtilExp (d8Pow);

    /* checking for underflow and overflow */
    if (d8Pow == 0)
    {
        return 0;
    }
    else if (d8Pow > MATH_INFINITY)
    {
        return MATH_INFINITY;
    }

    /* If Base is integer, rounding to get accurate values */
    if (i4BaseIsInt != 0)
    {
        d8Pres = d8Pow;
        d8Pow = UtilFloor (d8Pow);

        d8Pres = d8Pres - d8Pow;
        d8Pres = (INT4) (d8Pres * 1000);
        d8Pres = d8Pres / 1000;
        d8Pres = d8Pres - (INT4) d8Pres;

        if (d8Pres > 0.998)
        {
            d8Pow = d8Pow + 1;
        }
    }

    if (i4ExpIsInt == 2)
    {
        d8Pow = d8Pow * d8Pow;    /* If exponent is even */
    }
    else if (i4ExpIsInt == 1)
    {
        d8Pow = d8Pow * d8Pow * d8Base;    /* If exponent is odd */
    }

    /* checking for overflow */
    if (d8Pow > MATH_INFINITY)
    {
        return MATH_INFINITY;
    }

    d8Pow = i4Sign * d8Pow;

    return d8Pow;
}

/****************************************************************************/
/* Function Name  : UtilExp                                                 */
/* Description    : This function calculates the exponential value of the   */
/*                  given input x i.e 'e' raised to the power of x where 'e'*/
/*                  is Euler number. e = 2.718281828459045235360285         */
/* Input          : Value x to which 'e' is raised                          */
/* Output         : None                                                    */
/* Returns        : Exponential value of x                                  */
/****************************************************************************/

DBL8
UtilExp (DBL8 x)
{
    INT4                i4UnbExp = 0, i4Sign = 0, i4OutExp = 0;
    DBL8                d8RedArgHi = 0.0, d8RedArgLow = 0.0;
    DBL8                d8ApproxExp = 0.0, d8RedArg = 0.0, d8R1 = 1;
    UINT4               u4HighWord = 0, u4LowWord = 0;

    /* exp(0) = 1 */
    if (x == 0.0)
    {
        return 1.0;
    }

    /* Getting the high & low words of x */

    MATH_HIGH_WORD (u4HighWord, x);
    MATH_LOW_WORD (u4LowWord, x);

    /* Extracting the sign of x */
    i4Sign = u4HighWord & 0x80000000;

    /* Exception Handling  */
    if (((u4HighWord & 0x7ff00000) >> 20) == MATH_MAX_EXPONENT)
    {
        if (((u4HighWord & 0x000fffff) != 0) || (u4LowWord != 0))
        {
            return MATH_NAN;    /*exp(NaN) = NaN */
        }
        else if (i4Sign == 0)
        {
            return MATH_INFINITY;    /* exp(+inf) = inf */
        }
        else
        {
            return 0;            /* exp(-inf) = 0 */
        }
    }

    /* Overflow & Underflow */
    if (x > MATH_OVERFLOW)
    {
        return MATH_INFINITY;
    }
    else if (x < MATH_UNDERFLOW)
    {
        return 0;
    }

    /* exp(x< 2**-26) = 1 + x */
    else if ((u4HighWord & 0x7fffffff) < MATH_LESS_VALUE_OF_X)
    {
        return 1 + x;
    }

    /* Argument Reduction */

    if (u4HighWord > 0x3F862E42)    /* greater than ln2/64 */
    {
        if (i4Sign != 0)        /* sign is negative    */
        {
            i4UnbExp = (INT4) (x * MATH_INVL2 - (1 / 64));
            i4Sign = -1;
        }
        else
        {
            i4Sign = 1;
            i4UnbExp = (INT4) (x * MATH_INVL2 + (1 / 64));
        }
        /* Reduced Argument r = hi(r) + low(r) */
        d8RedArgHi = x - (i4UnbExp * MATH_LN2_HIGH);
        d8RedArgLow = -i4UnbExp * MATH_LN2_LOW;
        d8RedArg = d8RedArgHi + d8RedArgLow;

        d8RedArg = d8RedArg * i4Sign;

        /* Further reducing x to the range of |r| <= ln2/64 */
        if (d8RedArg >= 0.5)
        {
            d8RedArg = d8RedArg - 0.5;
            if (i4Sign != 1)
            {
                d8R1 = d8R1 * (1 / MATH_E_POW_PT_5);
            }
            else
            {
                d8R1 = d8R1 * MATH_E_POW_PT_5;
            }
        }
        if (d8RedArg >= 0.25)
        {
            d8RedArg = d8RedArg - 0.25;
            if (i4Sign != 1)
            {
                d8R1 = d8R1 * (1 / MATH_E_POW_PT_25);
            }
            else
            {
                d8R1 = d8R1 * MATH_E_POW_PT_25;
            }
        }
        if (d8RedArg >= 0.125)
        {
            d8RedArg = d8RedArg - 0.125;
            if (i4Sign != 1)
            {
                d8R1 = d8R1 * (1 / MATH_E_POW_PT_125);
            }
            else
            {
                d8R1 = d8R1 * MATH_E_POW_PT_125;
            }
        }
        if (d8RedArg >= 0.0625)
        {
            d8RedArg = d8RedArg - 0.0625;
            if (i4Sign != 1)
            {
                d8R1 = d8R1 * (1 / MATH_E_POW_PT_0625);
            }
            else
            {
                d8R1 = d8R1 * MATH_E_POW_PT_0625;
            }
        }
        if (d8RedArg >= 0.03125)
        {
            d8RedArg = d8RedArg - 0.03125;
            if (i4Sign != 1)
            {
                d8R1 = d8R1 * (1 / MATH_E_POW_PT_03125);
            }
            else
            {
                d8R1 = d8R1 * MATH_E_POW_PT_03125;
            }
        }
        d8RedArg = d8RedArg * i4Sign;

    }
    else
    {
        /* As x within the range of r make  k = 0 & r = x */
        i4UnbExp = 0;
        d8RedArg = x;
    }

    /* Polynomial Approximation of exp(r)
       exp(r) = 1 + R
       where R = r * (P1 +(r*(P2 + r * (P3 + r* (P4 + r * (P5+ r*P6))))))
       P1 to P5 are polynomial coefficients calculated
       using Remez algorithm in the range (0,0.01084) */

    d8ApproxExp = d8RedArg * (MATH_EXP_P1 + d8RedArg * (MATH_EXP_P2 +
                                                        d8RedArg *
                                                        (MATH_EXP_P3 +
                                                         d8RedArg *
                                                         (MATH_EXP_P4 +
                                                          d8RedArg *
                                                          (MATH_EXP_P5 +
                                                           d8RedArg *
                                                           MATH_EXP_P6)))));
    d8ApproxExp = 1 + d8ApproxExp;
    d8ApproxExp = d8ApproxExp * d8R1;

    if (i4UnbExp == 0)
    {
        return d8ApproxExp;
    }

    /* when unbiased exp of o/p < -1021
       then k<<20 provides negative value
       so scale up k & finally multiply 2 ** -100 */
    else if (i4UnbExp < -1021)
    {
        i4UnbExp = (i4UnbExp + 100) << 20;
        MATH_HIGH_WORD (i4OutExp, d8ApproxExp);
        i4OutExp = i4OutExp + i4UnbExp;
        MATH_INSERT_HIGH_WORD (d8ApproxExp, i4OutExp);
        return (d8ApproxExp * MATH_TWO_POW_M100);
    }
    else
    {
        i4UnbExp = i4UnbExp << 20;
        MATH_HIGH_WORD (i4OutExp, d8ApproxExp);
        i4OutExp = i4OutExp + i4UnbExp;
        MATH_INSERT_HIGH_WORD (d8ApproxExp, i4OutExp);
        return (d8ApproxExp);
    }
}

/****************************************************************************/
/* Function Name  : UtilLog                                                 */
/* Description    : This function calculates the natural logarithmic value  */
/*                  of the given input x                                    */
/* Input          : Value x for which logarithmic value is to be calculated */
/* Output         : None                                                    */
/* Returns        : Logarithmic value of x                                  */
/****************************************************************************/

DBL8
UtilLog (DBL8 x)
{
    INT4                i4OutExp = 0, i4InpExp = 0, i4InpMantissa = 0;
    DBL8                d8ApproxLog = 0.0, d8RedArg = 0.0;
    UINT4               u4HighWord = 0, u4LowWord = 0;

    /* Exceptioin Handling */

    if (x == 0)
    {
        return -MATH_INFINITY;
    }
    else if (x < 0)
    {
        return MATH_NAN;
    }

    MATH_HIGH_WORD (u4HighWord, x);
    MATH_LOW_WORD (u4LowWord, x);

    i4InpExp = (u4HighWord & 0x7ff00000) >> 20;
    i4InpMantissa = u4HighWord & 0x000fffff;

    if (((i4InpMantissa) != 0) || (u4LowWord != 0))
    {
        if (i4InpExp == MATH_MAX_EXPONENT)
        {
            return MATH_NAN;
        }
        else if ((u4HighWord & 0x7ff00000) == 0)
        {
            /* Denormalised numbers so scale up x by multiplying
               2**64 */
            x = x * MATH_SCALE_UP_VALUE;
            i4OutExp = -64;
            MATH_HIGH_WORD (u4HighWord, x);
            i4InpExp = u4HighWord & 0x7ff00000;
        }
    }
    else if (i4InpExp == MATH_MAX_EXPONENT)
    {
        return MATH_INFINITY;
    }

    i4OutExp = i4OutExp + (i4InpExp - 1023);

    /* if mantissa of x is not within the range of 1+f */
    if (i4InpMantissa > 0x0006A09E)
    {
        /* normalize x as x/2 */
        i4InpMantissa = i4InpMantissa | 0x3fe00000;
        MATH_INSERT_HIGH_WORD (x, i4InpMantissa);
        i4OutExp = i4OutExp + 1;
    }
    else
    {
        /* Normalize x */
        i4InpMantissa = i4InpMantissa | 0x3ff00000;
        MATH_INSERT_HIGH_WORD (x, i4InpMantissa);
    }

    /* value f */
    d8RedArg = x - 1;

    /*  Polynomial Approximation of log (1+f)
       log(1+f) = R = f *(P1 -(f*(P2 +f*(P3-f*(p4+f*(p5-f*p6))))))
       Using Remez algorithm the values of the polynomial
       coefficients are pre-computed as P1 to P6 */

    d8ApproxLog = d8RedArg * (MATH_LOG_P1 + d8RedArg * (MATH_LOG_P2 +
                                                        d8RedArg *
                                                        (MATH_LOG_P3 +
                                                         d8RedArg *
                                                         (MATH_LOG_P4 +
                                                          d8RedArg *
                                                          (MATH_LOG_P5 +
                                                           d8RedArg *
                                                           (MATH_LOG_P6 +
                                                            d8RedArg *
                                                            (MATH_LOG_P7 +
                                                             d8RedArg *
                                                             (MATH_LOG_P8 +
                                                              d8RedArg *
                                                              (MATH_LOG_P9 +
                                                               d8RedArg *
                                                               (MATH_LOG_P10 +
                                                                d8RedArg *
                                                                (MATH_LOG_P11 +
                                                                 d8RedArg *
                                                                 (MATH_LOG_P12 +
                                                                  d8RedArg *
                                                                  (MATH_LOG_P13
                                                                   +
                                                                   d8RedArg *
                                                                   (MATH_LOG_P14
                                                                    +
                                                                    d8RedArg *
                                                                    (MATH_LOG_P15
                                                                     +
                                                                     d8RedArg *
                                                                     (MATH_LOG_P16
                                                                      +
                                                                      d8RedArg *
                                                                      MATH_LOG_P17))))))))))))))));

    /* Reconstruction of log (x)  */
    if (i4OutExp == 0)
    {
        /* if k & f equals 0 */
        if (d8RedArg == 0)
        {
            return 0;            /* log(1) = 0 */
        }
        /* if k = 0 but not f */
        else
        {
            return d8ApproxLog;
        }
    }
    else
    {
        return (i4OutExp * MATH_LN2 + d8ApproxLog);
    }
}

#endif
