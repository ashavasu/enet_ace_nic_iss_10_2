# $Id: make.h,v 1.1 2015/04/28 12:53:04 siva Exp $                     #
# Top level make include files
# ----------------------------

include ../../LR/make.h
include ../../LR/make.rule

# Compilation switches
# --------------------
MATHLIB_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES} -UTRACE_WANTED

# Directories
# -----------
MATHLIB_BASE_DIR    = ${BASE_DIR}/util/mathlib

# Include files
# -------------

MATHLIB_INCLUDE_FILES =

MATHLIB_FINAL_INCLUDE_FILES = ${MATHLIB_INCLUDE_FILES}

# Include directories
# -------------------

MATHLIB_INCLUDE_DIRS = -I${MATHLIB_BASE_DIR}

MATHLIB_FINAL_INCLUDE_DIRS = ${MATHLIB_INCLUDE_DIRS}\
                         ${COMMON_INCLUDE_DIRS}


# Project dependencies
# --------------------

MATHLIB_DEPENDENCIES = $(COMMON_DEPENDENCIES)\
                   $(MATHLIB_FINAL_INCLUDE_FILES) \
                   $(MATHLIB_BASE_DIR)/Makefile \
                   $(MATHLIB_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
