# include "utilmath.h"

#define INSERT_LOW_WORD(x)  *(int*)&x
#define INSERT_HIGH_WORD(x)  *(1+(int*)&x)
int
main ()
{
    double              x, y, z, k;
    int                 hy, ly;

    INSERT_HIGH_WORD (x) = 0x7ff00000;
    INSERT_LOW_WORD (x) = 1;

    INSERT_HIGH_WORD (z) = 0x7ff00000;
    INSERT_LOW_WORD (z) = 0;

    printf ("\n.....FLOOR TEST CASES......\n");

    printf ("\nTest Case 1:\nInput : 0\nExpected Output : 0");

    y = UtilFloor (0);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 2:\nInput : nan\nExpected Output : nan");

    y = UtilFloor (x);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;

    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 3:\nInput : 10\nExpected Output : 10");

    y = UtilFloor (10);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 10)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 4:\nInput : 10.25\nExpected Output : 10");

    y = UtilFloor (10.25);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 10)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 5:\nInput : -10.25\nExpected Output : -11");

    y = UtilFloor (-10.25);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == -11)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\n.....CEIL TEST CASES......\n");

    printf ("\nTest Case 1:\nInput : 0\nExpected Output : 0");

    y = UtilCeil (0);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 2:\nInput : nan\nExpected Output : nan");

    y = UtilCeil (x);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 3:\nInput : 10\nExpected Output : 10");

    y = UtilCeil (10);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 10)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 4:\nInput : 10.25\nExpected Output : 11");

    y = UtilCeil (10.25);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 11)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 5:\nInput : -10.25\nExpected Output : -10");

    y = UtilCeil (-10.25);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == -10)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\n.....POWER TEST CASES......\n");

    printf ("\nTest Case 1:\nInput : 10, 0\nExpected Output : 1");

    y = UtilPow (10, 0);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 1)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 2:\nInput : 10, 1\nExpected Output : 10");

    y = UtilPow (10, 1);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 10)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 3:\nInput : 10, nan\nExpected Output : nan");

    y = UtilPow (10, x);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)

        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 4:\nInput : nan, 4\nExpected Output : nan");

    y = UtilPow (x, 4);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)

        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 5:\nInput : 10, inf\nExpected Output : inf");

    y = UtilPow (10, z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 6:\nInput : 10, -inf\nExpected Output : 0");

    y = UtilPow (10, -z);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 7:\nInput : 0.5, inf\nExpected Output : 0");

    y = UtilPow (0.5, z);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 8:\nInput : 0.5, -inf\nExpected Output : inf");

    y = UtilPow (0.5, -z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 9:\nInput : 1, inf\nExpected Output : nan");

    y = UtilPow (1, z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy > 0x7ff00000)
        printf ("Pass\n\n");
    else if ((hy == 0x7ff00000) && (ly != 0))
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 10:\nInput : 1, -inf\nExpected Output : nan");

    y = UtilPow (1, -z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy > 0x7ff00000)
        printf ("Pass\n\n");
    else if ((hy == 0x7ff00000) && (ly != 0))
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 11:\nInput : 0, 2\nExpected Output : 0");

    y = UtilPow (0, 2);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 12:\nInput : 0, -2\nExpected Output : inf");

    y = UtilPow (0, -2);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 13:\nInput : inf, 2\nExpected Output : inf");

    y = UtilPow (z, 2);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 14:\nInput : -inf, 2\nExpected Output : inf");

    y = UtilPow (-z, 2);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 15:\nInput : inf, -2\nExpected Output : 0");

    y = UtilPow (z, -2);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 16:\nInput : -4, 2\nExpected Output : 16");

    y = UtilPow (-4, 2);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 16)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 17:\nInput : -4, 3\nExpected Output : -64");

    y = UtilPow (-4, 3);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == -64)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 18:\nInput : -4, 2.5\nExpected Output : nan");

    y = UtilPow (-4, 2.5);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)

        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 19:\nInput : 1.5, 3\nExpected Output : 3.375");

    y = UtilPow (1.5, 3);
    printf ("\nOutput : %lf\nResult : ", y);

    y = y * 1000;
    y = UtilCeil (y);
    y = y / 1000;

    if (y == 3.375)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\n.....EXPONENT TEST CASES......\n");

    printf ("\nTest Case 1:\nInput : -745.133219\nExpected Output : 0");

    y = UtilExp (-745.133219);
    printf ("\nOutput : %lf\nResult : ", y);

    if ((int) y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 2:\nInput : 709.78272\nExpected Output : inf");

    y = UtilExp (709.78272);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 3:\nInput : 0\nExpected Output : 1");

    y = UtilExp (0);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 1)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 4:\nInput : nan\nExpected Output : nan");

    y = UtilExp (x);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)

        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 5:\nInput : inf\nExpected Output : inf");

    y = UtilExp (z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 6:\nInput : -inf\nExpected Output : 0");

    y = UtilExp (-z);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf
        ("\nTest Case 7:\nInput : 0.00000001490116119384765625\nExpected Output : 1");

    y = UtilExp (0.00000001490116119384765625);
    printf ("\nOutput : %lf\nResult : ", y);

    if (y >= 1)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 8:\nInput : 10\nExpected Output : 22026.465795");

    y = UtilExp (10);
    printf ("\nOutput : %lf\nResult : ", y);

    k = (int) y;
    k = y - k;

    k = (int) (k * 1000000);
    k = k / 1000000;
    k = (int) y + k;

    if (k == 22026.465794)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 9:\nInput : -12\nExpected Output : 0.000006");

    y = UtilExp (-12);
    printf ("\nOutput : %lf\nResult : ", y);

    k = (int) (y * 1000000);

    k = k / 1000000;

    if (k == 0.000006)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\n.....NATURAL LOGARITHM TEST CASES......\n");

    printf ("\nTest Case 1:\nInput : 0\nExpected Output : -inf");

    y = UtilLog (0);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0xfff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 2:\nInput : -4\nExpected Output : nan");

    y = UtilLog (-4);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 3:\nInput : nan\nExpected Output : nan");

    y = UtilLog (x);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)

        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 4:\nInput : inf\nExpected Output : inf");

    y = UtilLog (z);
    MATH_HIGH_WORD (hy, y);
    MATH_LOW_WORD (ly, y);
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy == 0x7ff00000 && ly == 0)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 5:\nInput : -inf\nExpected Output : nan");

    y = UtilLog (-z);
    MATH_HIGH_WORD (hy, y);
    hy = hy & 0x7fffffff;
    printf ("\nOutput : %lf\nResult : ", y);

    if (hy >= 0x7ff00000)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 6:\nInput : 10\nExpected Output : 2.302585");

    y = UtilLog (10);
    printf ("\nOutput : %lf\nResult : ", y);

    k = (int) (y * 1000000);

    k = k / 1000000;

    if (k == 2.302585)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 7:\nInput : 2.5\nExpected Output : 0.916290");

    y = UtilLog (2.5);
    printf ("\nOutput : %lf\nResult : ", y);

    k = (int) (y * 1000000);

    k = k / 1000000;

    if (k == 0.916290)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

    printf ("\nTest Case 8:\nInput : 1e-200\nExpected Output : -460.517018");

    y = UtilLog (1e-200);
    printf ("\nOutput : %lf\nResult : ", y);

    k = (int) (y * 1000000);

    k = k / 1000000;

    if (k == -460.517018)
        printf ("Pass\n\n");
    else
        printf ("Fail\n\n");

}
