/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triesz.h,v 1.1 2015/04/28 12:51:07 siva Exp $
 *
 * Description: This file contains mempool creation/deletion prototype
 *              declarations in TRIE2 module.
 *********************************************************************/
enum {
    MAX_TRIE2_NUM_LEAF_NODES_SIZING_ID,
    MAX_TRIE2_NUM_RADIX_NODES_SIZING_ID,
    MAX_TRIE2_NUM_TRIE2_KEYS_SIZING_ID,
    TRIE_MAX_SIZING_ID
};


#ifdef  _TRIESZ_C
tMemPoolId TRIEMemPoolIds[ TRIE_MAX_SIZING_ID];
INT4  TrieSizingMemCreateMemPools(VOID);
VOID  TrieSizingMemDeleteMemPools(VOID);
INT4  TrieSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TRIESZ_C  */
extern tMemPoolId TRIEMemPoolIds[ ];
extern INT4  TrieSizingMemCreateMemPools(VOID);
extern VOID  TrieSizingMemDeleteMemPools(VOID);
#endif /*  _TRIESZ_C  */


#ifdef  _TRIESZ_C
tFsModSizingParams FsTRIESizingParams [] = {
{ "tLeafNode", "MAX_TRIE2_NUM_LEAF_NODES", sizeof(tLeafNode),MAX_TRIE2_NUM_LEAF_NODES, MAX_TRIE2_NUM_LEAF_NODES,0 },
{ "tRadixNode", "MAX_TRIE2_NUM_RADIX_NODES", sizeof(tRadixNode),MAX_TRIE2_NUM_RADIX_NODES, MAX_TRIE2_NUM_RADIX_NODES,0 },
{ "UINT1[32]", "MAX_TRIE2_NUM_TRIE2_KEYS", sizeof(UINT1[32]),MAX_TRIE2_NUM_TRIE2_KEYS, MAX_TRIE2_NUM_TRIE2_KEYS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TRIESZ_C  */
extern tFsModSizingParams FsTRIESizingParams [];
#endif /*  _TRIESZ_C  */


