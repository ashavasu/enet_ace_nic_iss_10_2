/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triedll.h,v 1.4 2015/04/28 12:51:07 siva Exp $
 */

#define DLIST_NODE(type, ele) \
struct {            \
   type *pNext; \
   type *pPrev; \
} ele

#define DLIST_HEAD(type, ele) \
struct {              \
type *pNext; \
type *pPrev; \
} ele

#define DLIST_FIRSTNODE(root, ele)       ((root)->ele.pNext)
#define DLIST_LASTNODE(root, ele)        ((root)->ele.pPrev)
#define DLIST_NEXT(leaf, ele)            ((leaf)->ele.pNext)
#define DLIST_PREV(leaf, ele)            ((leaf)->ele.pPrev)


#define DLIST_INSERT_HEAD(root, head_ele, leaf , leaf_ele)\
do {                    \
       if ((leaf->leaf_ele.pNext = root->head_ele.pNext)) {     \
           root->head_ele.pNext->leaf_ele.pPrev = leaf;     \
       }                        \
       leaf->leaf_ele.pPrev = NULL;            \
       root->head_ele.pNext = leaf;            \
}while (0)


/* insert after */
#define DLIST_INSERT(root, root_ele, leaf, leaf_ele, after ) \
do {                \
    if ((leaf->leaf_ele.pNext = after->leaf_ele.pNext)) {      \
        after->leaf_ele.pNext->leaf_ele.pPrev = leaf;         \
    }                        \
    else {                        \
        root->root_ele.pPrev = leaf;        \
    }                        \
    leaf->leaf_ele.pPrev = after;            \
    after->leaf_ele.pNext = leaf;            \
}while (0)


#define DLIST_REMOVE_HEAD(root, head_ele, leaf_ele) \
do {                           \
    if (root->head_ele.pNext) {                    \
       if ((root->head_ele.pNext = root->head_ele.pNext->leaf_ele.pNext)) {\
            root->head_ele.pNext ->leaf_ele.pPrev = NULL;       \
        }\
        else                                  {  \
            root->head_ele.pPrev = NULL;       \
        }\
    }                            \
} while (0)

/* remove after */
#define DLIST_REMOVE(root, head_ele, leaf, leaf_ele, prev_leaf) \
do {            \
    if ((prev_leaf->leaf_ele.pNext = leaf->leaf_ele.pNext)) {        \
        leaf->leaf_ele.pNext->leaf_ele.pPrev = prev_leaf;    \
    }                                \
    else {                                \
        root->head_ele.pPrev = prev_leaf;     \
    }                                \
}while (0)

