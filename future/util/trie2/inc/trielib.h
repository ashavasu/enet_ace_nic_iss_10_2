/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trielib.h,v 1.9 2015/04/28 12:51:07 siva Exp $
 *
 * Internal file of Trie Library.
 *
 */

#ifndef _TRIELIB_H_
#define _TRIELIB_H_

#define  RADIX_NODE                0
#define  LEAF_NODE                 1
#define  U4_SIZE                   4
#define  BYTE_LENGTH               8

#define  KEY_POOL_ID               2
#define  ALL_PROTO_ID             -1

/* error codes */
#define  LEAF_ALLOC_FAIL           0
#define  RADIX_ALLOC_FAIL          1
#define  KEY_ALLOC_FAIL            2
#define  MAX_KEY_SIZE_OVERFLOW     3
#define  MAX_APP_OVERFLOW          4
#define  MAX_INSTANCE_OVERFLOW     5
#define  SEM_CREATE_FAIL           6
#define  ALREADY_USED_APP_ID       7
#define  MEMORY_CREATE_FAIL        8

/* configurable variables */
/* MAX_NUM_TRIE = (MAX_OSPF_CONTEXT * NO_OF_TRIE_INSTANCES_IN_OSPF) 
 *                 +(MAX_RIP_CONTEXT * NO_OF_TRIE_INSTANCES_IN_RIP)
 *                 + 10 = (255*3)+(255 * 1) +10 = 1030) */

/* change MAX_NUM_TRIE if any new trie instance
 * is created as part of MI support */ 
#define  MAX_KEY_SIZE            256
#define  MAX_NUM_KEYS              4
#define  MAX_TRIE_SEM_NAME_SIZE   16

/* Maximum size of trie2 key.
 * which will be used in mempool creation */
#define  MAX_TRIE2_KEY_SIZE       32

#define  TRIE2_MAX_ROUTING_PROTOCOLS 16 /* MAX Routing protocols value is
                                         * 16 as per stdip.mib and 9 for
                                         * stdipv6.mib So setting for max
                                         * value. */

/* When FutureIp is not used IP_CFG_GET_NUM_MULTIPATH cannot   */
/* be called. Instead use a pre-defined value for the maximum  */
/* number of multipaths that can be stored.                    */
/* We choose the maximum number of multipaths to be the number */
/* of TOSs supported by FutureOspf, the other user of Trie     */

/* variables used by TrieConfigParams structure */
#define  NUM_1ST_KEY             4000

#define  KEY1_SIZE                  8
#define  KEY2_SIZE                 16
#define  KEY3_SIZE                 32
#define  KEY4_SIZE                 64

#define  NUM_2ND_KEY                0
#define  NUM_3RD_KEY                0
#define  NUM_4TH_KEY                0
#define  INVALID_NEXTHOP   0xffffffff
#define  TRIE_SEM_COUNT             1

struct _RadixNode;

typedef struct _RadixNodeHead
{
    struct _RadixNode   *pRadixNodeTop;
    struct TrieAppFns   *AppFns;
    tOsixSemId          SemId;
    UINT4               u4RouteCount;
    UINT4               u4Type;
    UINT2               u2KeySize;
    UINT2               u2AppMask;
    DLIST_HEAD (struct _LeafNode, NodeHead);
    BOOLEAN             bPoolPerInst; /* TRUE - Indicates MEMPOOL per Trie instance 
                                       * FALSE - Indicates common MEMPOOL */
    BOOLEAN             bSemPerInst;  /* TRUE - Indicates Sem per TrieInstance
                                       * FALSE - Indicates common MEMPOOL */
    BOOLEAN             bValidateType;/* TRUE - Validation will be done to avoid
                                       *        creating new trie instances with same key    
                                       * FALSE - New trie instance will be created
                                       *         without validation */
    UINT1               u1Reserved;
} tRadixNodeHead;

typedef struct _RadixNode
{
    struct _RadixNode *pParent;
    tKey              Mask;
    UINT1             u1NodeType;
    UINT1             u1Reserved1;
    UINT1             u1ByteToTest;
    UINT1             u1BitToTest;
    void              *pLeft;
    void              *pRight;
} tRadixNode;
                    
typedef struct _LeafNode
{
    tRadixNode  *pParent;
    tKey        Mask;
    UINT1       u1NodeType;
    UINT1       u1ReservedByte;
    UINT2       u2ReservedWord;  
    VOID        *pAppSpecPtr;
    tKey        Key;
    DLIST_NODE  (struct _LeafNode, NodeLink);
} tLeafNode;

typedef struct _TrieConfigParams
{
    UINT4     u4NumRadixNodes;
    UINT4     u4NumLeafNodes;
    UINT4     au4NumKey[MAX_KEY_SIZE + 1];
    UINT4     u4AlignmentByte;
} tTrieConfigParams;

INT4 TrieCrtMemPool (UINT2 u2ObjSize, UINT4 u2ObjCount);
void TrieDelMemPool (INT4 i4QueueId);

                
#define TRIE_INIT_ROUTES_BITMASK(u1NoOfMultipath)   u1NoOfMultipath = 0
#define TRIE_INIT_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath = u1NoOfMultipath & 0xf0;
#define TRIE_INC_TOTAL_NUM_OF_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath += 16
#define TRIE_INC_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath += 1
#define TRIE_SET_NUM_EQUAL_COST_ROUTES_TO_ONE(u1NoOfMultipath) \
        { \
        u1NoOfMultipath = u1NoOfMultipath & 0xf0; \
        u1NoOfMultipath += 1; \
        }

#define TRIE_DEC_TOTAL_NUM_OF_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath -= 16

#define TRIE_DEC_NUM_EQUAL_COST_ROUTES(u1NoOfMultipath) \
        u1NoOfMultipath -= 1

#define TRIE_COPY_TOTAL_NUM_OF_ROUTES(u1BitMask, u1NumOfMultipath) \
        u1NumOfMultipath = (u1BitMask >> 4)

#define TRIE_COPY_NUM_OF_EQUAL_COST_ROUTES(u1BitMask, u1NumOfMultipath) \
        u1NumOfMultipath = (u1BitMask & 0x0f )

#define  COPY_INFO_FROM_OUTPARAMS_TO_DELETEOUT(pOutputParams,pDeleteOutParams,u4TmpNumEntries) \
   while (((tTrieApp *)pOutputParams->pAppSpecInfo) != NULL) {\
         ((tTrieApp **) pDeleteOutParams->pAppSpecInfo)[u4TmpNumEntries++] = pOutputParams->pAppSpecInfo;\
            pOutputParams->pAppSpecInfo = ((tTrieApp *)pOutputParams->pAppSpecInfo)->pNextAlternatepath;\
               }

/* Macros for Key test/manipulation */
#define   BIT_TEST(a, b)                  (a) & (b)
#define   SET_BIT(u1AppId, u2AppMask)     (u2AppMask) = (UINT2)((u2AppMask)| (0x0001 << (u1AppId)))
#define   RESET_BIT(i1AppId, u2AppMask)   (u2AppMask) = (UINT2)((u2AppMask)& (~(0x0001 << (i1AppId))))
#define   CHECK_BIT(i4AppId, u2AppMask)   ((u2AppMask) & (0x0001 << (i4AppId)))

#define DIFFBYTE(Key1, Key2, u1Size, u4Byte, u1Diff)\
 if ((u1Size) == U4_SIZE) {DIFF4BYTE(Key1.u4Key, Key2.u4Key, u4Byte, u1Diff);}\
 else               {DIFFNBYTE(Key1.pKey, Key2.pKey, u1Size, u4Byte, u1Diff);}

#define DIFF4BYTE(u4Key1, u4Key2, u4Byte, u1Diff) {u4Byte = (u4Key1 ^ u4Key2);\
    u4Byte =  (u1Diff  = (UINT1)(u4Byte >> 24)) ? 0 :\
              ((u1Diff = (UINT1)(u4Byte >> 16)) ? 1 :\
              ((u1Diff = (UINT1)(u4Byte >> 8))  ? 2 :\
              ((u1Diff = (UINT1)(u4Byte))       ? 3 : 4)));}

#define DIFFNBYTE(pKey1, pKey2, u1Size, u4Byte, u1Diff)\
   for (u4Byte = 0; (u4Byte < (UINT4) u1Size) &&\
                     !(u1Diff = (pKey1[u4Byte] ^ pKey2[u4Byte])); u4Byte++);

#define DIFFBIT(u1Bit, u1Diff) for(u1Bit = 0; !(u1Diff >> (7-u1Bit)); u1Bit++)

#define KEYCMP(Key1, Key2, u1Size)  (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key < (Key2).u4Key) ? -1 : 1) :\
         memcmp ((Key1).pKey, (Key2).pKey, (u1Size)))

#define KEYEQUAL(Key1, Key2, u1Size) (((u1Size) == U4_SIZE) ?\
         (((Key1).u4Key == (Key2).u4Key) ? 0 : 1) :\
         (memcmp ((Key1).pKey, (Key2).pKey, (u1Size))))

#define GETBYTE(Key, u1ByteNum, u1Size) (UINT1) (((u1Size) == U4_SIZE) ?\
 ((Key).u4Key >> BYTE_LENGTH*(U4_SIZE - 1 - u1ByteNum)) :\
 ((Key).pKey [u1ByteNum]))

#define KEYCOPY(Dst, Src, u1Size) do {             \
    if ((u1Size) > U4_SIZE)                        \
        MEMCPY ((Dst).pKey, (Src).pKey, (u1Size)); \
    else                                           \
       (Dst).u4Key = (Src).u4Key;                  \
    } while (0)

/* extern declarations of globals */
extern tMemPoolId gai4RadixPool[MAX_NUM_TRIE];
extern INT4 gai4LeafPool[MAX_NUM_TRIE];
extern INT4 gi4KeyPoolIdx[MAX_NUM_TRIE];
extern tRadixNodeHead    gaTrieInstance[MAX_NUM_TRIE];
extern tTrieConfigParams gTrieCfg;

/* COMMON MEM POOL IDs */
extern tMemPoolId   gTrieCmnLeafPoolId;
extern tMemPoolId   gTrieCmnRadixPoolId;
extern tMemPoolId   gTrieCmnKeyPoolId;

/* Sem for common memory pools */
extern tOsixSemId   gTrieRadixPoolSem;
extern tOsixSemId   gTrieLeafPoolSem;
extern tOsixSemId   gTrieKeyPoolSem;

/* Macros for common memory pools */
#define   TRIE_RADIXPOOL_SEM_NAME     (const UINT1 *)"TRRP"
#define   TRIE_LEAFPOOL_SEM_NAME      (const UINT1 *)"TRLP"
#define   TRIE_KEYPOOL_SEM_NAME       (const UINT1 *)"TRKP"
#define   TRIE_RADIX_SEM_ID           gTrieRadixPoolSem
#define   TRIE_LEAF_SEM_ID            gTrieLeafPoolSem
#define   TRIE_KEY_SEM_ID             gTrieKeyPoolSem

/* Function Prototypes */
void
TrieLibInitMemManager (void);

void
TrieRelease(INT4 i4QueueId, UINT1 * pu1BuffAddr);

UINT1 *
TrieAllocate (INT4 i4QueueId);

tRadixNodeHead     *
TrieGetInstance (UINT4 u4Type, UINT2 u2KeySize, UINT4 *pu4Instance);

tRadixNodeHead     *
TrieGetFreeInstance (UINT4 *pu4Instance);

INT4
TrieDoTraverse (UINT2 u2KeySize, tRadixNode *pRoot, tKey Key, tLeafNode **ppNode);


INT4
TrieInsertLeaf (UINT2   u2KeySize, tInputParams * pInputParams,
             VOID * pAppSpecInfo, tRadixNode * pParentNode,
             VOID *pOutputParams, tLeafNode **ppNewLeaf);

INT4
TrieInsertRadix (UINT2   u2KeySize, UINT1 u1BitToTest, UINT1 u1ByteToTest,
              tInputParams * pInputParams, VOID * pAppSpecInfo,
              tRadixNode *pParentNode, VOID * pOutputParams);

VOID
TrieDelRadixLeaf (tRadixNodeHead * pRoot, tLeafNode * pLeafNode,
                  UINT4 u4Instance);

VOID
TrieSetMask (UINT2   u2KeySize, VOID *pNode);

INT4
TrieGetBestMatch (UINT2   u2KeySize, tInputParams * pInputParams, VOID *pNode,
                   VOID *pOutputParams);

INT4
TrieGetBestMatch4 (UINT2   u2KeySize,
                                   tInputParams * pInputParams,
                                   VOID *pNode, VOID * pOutputParams);

VOID               *
TrieAllocateateNode (UINT2 u2KeySize, UINT1 u1NodeType, UINT4 u4Instance);

VOID
TrieError (UINT1 u1NumError);

VOID
TrieLibInitVariables (void);

INT4
TrieCreateSemName (INT4 u4Type, UINT1 *pu1SemName);

INT4
TrieGetSemId (VOID *pRoot, tOsixSemId *pSemId);

INT4
TrieGetTrieInstance (VOID *pRoot, UINT4 *pu4Instance);

INT4
TrieGetLinkListIndex (INT1 i1AppId,
                           UINT2 u2AppMask, UINT1 * pu1LinkListIndex);

UINT4
TrieGetBitDifference (UINT1 u1NumBytes,
                            UINT1 u1MaskFlag,
                            UINT1 * pu1Key1, UINT1 * pu1Key2);

INT4
TrieWalkAll (tInputParams * pInputParams,
                           INT4 (*AppSpecScanFunc) (VOID *),
                           VOID * pScanOutParams);

tLeafNode*
TrieGetPrevLeafNode (tLeafNode* pLeaf);

INT4
TrieOverlapMatch (tKey Key, UINT2 u2KeySize, tLeafNode* pNode, UINT1 *pu1OvlType);

INT4
TrieKeyMatches(tKey Key1, tKey Key2, UINT2 u2PrefixLen, UINT2 u2KeySize);

UINT2
TriePrefixLen (tKey Key, UINT2 u2KeySize);

INT4
TrieDefaultRoute (tKey Key, UINT2 u2KeySize);

tLeafNode *
TrieFindFirstMatch (UINT2 u2KeySize, tInputParams *pInputParams, void *pNode);

tLeafNode *
TrieFindFirstMatch4 (UINT2 u2KeySize, tInputParams *pInputParams, void *pNode);

tLeafNode *
TrieFirstMatch (tInputParams *pInputParams);

tLeafNode *
TrieDoTraverseLessMoreSpecific  (tInputParams *pInputParams);
tRadixNode *
TrieDoTraverseLessSpecific (tInputParams *pInputParams);

tLeafNode *
TrieLeftmost (tRadixNode *pNode);

tLeafNode *
TrieRightmost (tRadixNode *pNode);

VOID *
TrieAllocateRadixNode (UINT2 u2KeySize, UINT4 u4Instance);

VOID *
TrieAllocateLeafNode (UINT2 u2KeySize, UINT4 u4Instance);

VOID
TrieReleaseNode (UINT1 u1NodeType, UINT4 u4Instance, UINT1 * pNode);

VOID
Trie2TakeSem (UINT4, tOsixSemId);

VOID
Trie2GiveSem (UINT4, tOsixSemId);

VOID
Trie2DelSem (UINT4, tOsixSemId);

#endif
