/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieinc.h,v 1.4 2015/04/28 12:51:07 siva Exp $
 *
 * This file is included in C files.
 *
 *******************************************************************/
#ifndef _TRIE_INC_H
#define _TRIE_INC_H

#include "lr.h"
#include "trie.h"
#include "triedll.h"
#include "trielib.h"
#include "triesz.h"

/* Klocwork false positive warning fix */
#define   TRIE_KW_FALSEPOSITIVE_FIX(x)   gpTrieKwFp = ((void*) x)

#endif /* _TRIE_INC_H */
