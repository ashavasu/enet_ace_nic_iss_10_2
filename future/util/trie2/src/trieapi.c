/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieapi.c,v 1.11 2015/04/28 12:51:07 siva Exp $
 *
 * Description: Implementation of the trie library APIs.
 *
 *******************************************************************/

#include "trieinc.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
static tOsixSemId   gTrieSem;
tRadixNodeHead      gaTrieInstance[MAX_NUM_TRIE];

/* For Klocwork false positive warning */
VOID               *gpTrieKwFp;

/* MEM POOL ID PER INSTANCE */
tMemPoolId          gai4RadixPool[MAX_NUM_TRIE];
INT4                gai4LeafPool[MAX_NUM_TRIE];
INT4                gi4KeyPoolIdx[MAX_NUM_TRIE];

#define    TRIE_SEM_NAME      (const UINT1 *)"tris"
#define    TRIE_SEM_ID         gTrieSem
/* COMMON MEM POOL ID */
tMemPoolId          gTrieCmnLeafPoolId;
tMemPoolId          gTrieCmnRadixPoolId;
tMemPoolId          gTrieCmnKeyPoolId;

/* Sem for common mem pool */
tOsixSemId          gTrieRadixPoolSem;
tOsixSemId          gTrieLeafPoolSem;
tOsixSemId          gTrieKeyPoolSem;

/**************************************************************************/

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLibInit ()                                           */
/*                                                                         */
/*  Description   This function initialises an array. Each member of this  */
/*                array is a tRadixNodeHead structure and represents       */
/*                different instance of Trie. It also initialises integer  */
/*                fields of all members. It creates global semaphore.      */
/*                This ensures that even in presence of more applications  */
/*                TrieCrt () is always called in  mutual exclusion.        */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family.*/
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieLibInit (VOID)
{
    MEMSET ((tRadixNodeHead *) gaTrieInstance, 0,
            MAX_NUM_TRIE * sizeof (tRadixNodeHead));

    if (OsixCreateSem (TRIE_SEM_NAME, TRIE_SEM_COUNT, 0, &TRIE_SEM_ID)
        != OSIX_SUCCESS)
    {
        TrieError (SEM_CREATE_FAIL);
        return (TRIE_FAILURE);
    }
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLibMemPoolInit ()                                    */
/*                                                                         */
/*  Description   This function creates common memory pools for            */
/*                Radix nodes, Leaf nodes and KeyIds. It creates           */
/*                semphore for each pool                                   */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      u4NumNodes - Total number of trie2 nodes needed          */
/*                             by all protocols.                           */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family.*/
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieLibMemPoolInit (VOID)
{

    if (TrieSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        TrieError (MEMORY_CREATE_FAIL);
        return (TRIE_FAILURE);
    }
    /*tRadixNode */
    gTrieCmnRadixPoolId = TRIEMemPoolIds[MAX_TRIE2_NUM_RADIX_NODES_SIZING_ID];
    /*tLeafNode */
    gTrieCmnLeafPoolId = TRIEMemPoolIds[MAX_TRIE2_NUM_LEAF_NODES_SIZING_ID];
    /*MAX_TRIE2_KEY_SIZE */
    gTrieCmnKeyPoolId = TRIEMemPoolIds[MAX_TRIE2_NUM_TRIE2_KEYS_SIZING_ID];

    /* Creating sem for common radix memory pool */
    if (OsixCreateSem (TRIE_RADIXPOOL_SEM_NAME,
                       TRIE_SEM_COUNT, 0, &TRIE_RADIX_SEM_ID) != OSIX_SUCCESS)
    {
        TrieError (SEM_CREATE_FAIL);
        TrieSizingMemDeleteMemPools ();
        return (TRIE_FAILURE);
    }

    /* Creating sem for common leaf memory pool */
    if (OsixCreateSem (TRIE_LEAFPOOL_SEM_NAME,
                       TRIE_SEM_COUNT, 0, &TRIE_LEAF_SEM_ID) != OSIX_SUCCESS)
    {
        TrieError (SEM_CREATE_FAIL);
        TrieSizingMemDeleteMemPools ();
        OsixSemDel (TRIE_RADIX_SEM_ID);
        return (TRIE_FAILURE);
    }

    /* Creating sem for common key memory pool */
    if (OsixCreateSem (TRIE_KEYPOOL_SEM_NAME,
                       TRIE_SEM_COUNT, 0, &TRIE_KEY_SEM_ID) != OSIX_SUCCESS)
    {
        TrieError (SEM_CREATE_FAIL);
        TrieSizingMemDeleteMemPools ();
        OsixSemDel (TRIE_RADIX_SEM_ID);
        OsixSemDel (TRIE_LEAF_SEM_ID);
        return (TRIE_FAILURE);
    }

    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLibShut ()                                           */
/*                                                                         */
/*  Description   This function frees all the resources  occupied by all   */
/*                the instances of Trie.                                   */
/*                                                                         */
/*  Call          When all the resources are to be                         */
/*  condition     freed.                                                   */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
VOID
TrieLibShut (VOID)
{
    UINT4               u4Instance = 0;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_TRUE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
        }
        Trie2DelSem (u4Instance, gaTrieInstance[u4Instance].SemId);
    }
    OsixSemDel (TRIE_SEM_ID);
    /* Deleting common mem pools which are
     * created in TrieLibMemPoolInit */
    TrieSizingMemDeleteMemPools ();
    /* Deleting common sems which are 
     * created in TrieLibMemPoolInit */
    OsixSemDel (TRIE_RADIX_SEM_ID);
    OsixSemDel (TRIE_LEAF_SEM_ID);
    OsixSemDel (TRIE_KEY_SEM_ID);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieCrt ()                                               */
/*                                                                         */
/*  Description   This function is used to register the application as     */
/*                well as to create a Trie instance. It returns pRoot      */
/*                parameter  with the root of the selected Trie instance.  */
/*                It returns unique identifier for each application as     */
/*                application identity number.                             */
/*                                                                         */
/*  Call          Should be called once before using                       */
/*  condition     any other Trie library function.                         */
/*                                                                         */
/*  Input(s)      pCreateParams - Pointer to the tTrieCrtParams            */
/*                                structure.                               */
/*                                                                         */
/*  Output(s)     pRoot -         Pointer to the root of the instance of   */
/*                                the Trie (the tRadixNodeHead             */
/*                                structure), if successful.               */
/*                                                                         */
/*  Access        Applications who want to use the Trie library.           */
/*  privileges                                                             */
/*                                                                         */
/*  Return        Valid unique positive application identifier,            */
/*                if  successful.                                          */
/*                TRIE_FAILURE - Otherwise. (A negative number represents  */
/*                              TRIE_FAILURE.)                             */
/*                                                                         */
/* Status        This function is Obseleted. Recommended to use            */
/*               TrieCreateInstance                                        */
/*                                                                         */
/***************************************************************************/
tRadixNodeHead     *
TrieCrt (tTrieCrtParams * pCreateParams)
{
    UINT1               u1AppId;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance = 0;
    UINT4               u4rc;
    UINT1               au1SemName[MAX_TRIE_SEM_NAME_SIZE];
    tOsixSemId          TrieSemId = 0;

    OsixSemTake (TRIE_SEM_ID);

    if (NULL == pCreateParams->AppFns)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->u2KeySize > MAX_KEY_SIZE)
    {
        TrieError (MAX_KEY_SIZE_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    u1AppId = (UINT1) pCreateParams->u1AppId;
    if ((u1AppId > TRIE2_MAX_ROUTING_PROTOCOLS) || (u1AppId == 0))
    {
        TrieError (MAX_APP_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    pHead =
        TrieGetInstance ((UINT4) pCreateParams->u4Type,
                         (UINT2) pCreateParams->u2KeySize, &u4Instance);

    if (pHead == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    /* Verify whether the Trie instance is already initialized or not. If
     * instance not initialized then initialize the new instance. */
    if (gaTrieInstance[u4Instance].u2KeySize == 0)
    {
        gaTrieInstance[u4Instance].bPoolPerInst = OSIX_TRUE;
        gaTrieInstance[u4Instance].bSemPerInst = OSIX_TRUE;
        /* Instance not initialized earlier. */
        TrieCreateSemName ((INT4) u4Instance, au1SemName);
        if (OsixCreateSem (au1SemName, TRIE_SEM_COUNT, 0, &TrieSemId)
            != OSIX_SUCCESS)
        {
            TrieError (SEM_CREATE_FAIL);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* Create memory pool for the new instance and create the radix node
         * top for the new TRIE instance */
        /* create memory pool for radix nodes */
        u4rc =
            MemCreateMemPool (sizeof (tRadixNode),
                              pCreateParams->u4NumRadixNodes,
                              MEM_HEAP_MEMORY_TYPE, &gai4RadixPool[u4Instance]);

        if (u4rc == MEM_FAILURE)
        {
            TrieError (RADIX_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* create memory pool for leaf nodes */
        u4rc =
            MemCreateMemPool (sizeof (tLeafNode), pCreateParams->u4NumLeafNodes,
                              MEM_HEAP_MEMORY_TYPE,
                              (tMemPoolId *) & gai4LeafPool[u4Instance]);
        if (u4rc == MEM_FAILURE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            TrieError (LEAF_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* create memory pool for key */
        u4rc = MemCreateMemPool ((UINT2) pCreateParams->u2KeySize,
                                 2 * pCreateParams->u4NoofRoutes,
                                 MEM_HEAP_MEMORY_TYPE,
                                 (tMemPoolId *) & gi4KeyPoolIdx[u4Instance]);
        if (u4rc == MEM_FAILURE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            TrieError (KEY_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieInstance[u4Instance].pRadixNodeTop =
            TrieAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                 RADIX_NODE, u4Instance);
        if (gaTrieInstance[u4Instance].pRadixNodeTop == NULL)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieInstance[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
        gaTrieInstance[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
        gaTrieInstance[u4Instance].pRadixNodeTop->pLeft = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pRight = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pParent = NULL;
        gaTrieInstance[u4Instance].u4Type = pCreateParams->u4Type;
        gaTrieInstance[u4Instance].u2KeySize = (UINT2) pCreateParams->u2KeySize;
        gaTrieInstance[u4Instance].u4RouteCount = 0;
        gaTrieInstance[u4Instance].AppFns = pCreateParams->AppFns;
        gaTrieInstance[u4Instance].SemId = TrieSemId;
    }

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))
    {
        TrieError (ALREADY_USED_APP_ID);
        OsixSemGive (TRIE_SEM_ID);
        TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
        return (NULL);
    }

    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    OsixSemGive (TRIE_SEM_ID);
    TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
    return (pHead);
}

/****************************************************************************/
/*                                                                          */
/*  Function      TrieCreateInstance                                        */
/*                                                                          */
/*  Description   This function is used to register the application as      */
/*                well as to create a Trie instance. It returns pRoot       */
/*                parameter  with the root of the selected Trie instance.   */
/*                It returns unique identifier for each application as      */
/*                application identity number.                              */
/*                   bPoolPerInst - Mem pool will be created per instance,  */
/*                                  if it is set to true, otherwise         */
/*                                  common mem pools gTrieCmnRadixPoolId,   */
/*                                  gTrieCmnLeafPoolId and gTrieCmnKeyPoolId*/
/*                                  will be used.                           */
/*                   bSemPerInst -  Sem will be created per trie instance,  */
/*                                  if it is set to true.                   */
/*                   bValidateType- If there is any trie instance is present*/
/*                                  with the same type and key, same trie   */
/*                                  be used in case of true. Otherwise,     */
/*                                  New trie instance will created          */
/*                                                                          */
/*  Call          Should be called once before using                        */
/*  condition     any other Trie library function.                          */
/*                                                                          */
/*  Input(s)      pCreateParams - Pointer to the tTrieCrtParams             */
/*                                structure.                                */
/*                                                                          */
/*  Output(s)     pRoot -         Pointer to the root of the instance of    */
/*                                the Trie (the tRadixNodeHead              */
/*                                structure), if successful.                */
/*                                                                          */
/*  Return        Pointer to the root node of created trie instance         */
/*                if  successful, NULL  Otherwise.                          */
/*                                                                          */
/****************************************************************************/
tRadixNodeHead     *
TrieCreateInstance (tTrieCrtParams * pCreateParams)
{
    tOsixSemId          TrieSemId = 0;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance = 0;
    UINT4               u4rc;
    UINT1               au1SemName[MAX_TRIE_SEM_NAME_SIZE];
    UINT1               u1AppId;

    if ((pCreateParams->bPoolPerInst == OSIX_TRUE) &&
        (pCreateParams->bSemPerInst == OSIX_TRUE))
    {
        return (TrieCrt (pCreateParams));
    }

    OsixSemTake (TRIE_SEM_ID);
    if (NULL == pCreateParams->AppFns)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->u2KeySize > MAX_TRIE2_KEY_SIZE)
    {
        TrieError (MAX_KEY_SIZE_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    u1AppId = (UINT1) pCreateParams->u1AppId;
    if ((u1AppId > TRIE2_MAX_ROUTING_PROTOCOLS) || (u1AppId == 0))
    {
        TrieError (MAX_APP_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->bValidateType == OSIX_TRUE)
    {
        /* If any trie instance matches with this
         * type and key value, get the same entry
         * else get free instance */
        pHead =
            TrieGetInstance ((UINT4) pCreateParams->u4Type,
                             (UINT2) pCreateParams->u2KeySize, &u4Instance);
    }
    else
    {
        /* No need to validate u4Type field,
         * separate trie needs to be created */
        pHead = TrieGetFreeInstance (&u4Instance);

    }

    if (pHead == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    /* Verify whether the Trie instance is already initialized or not. If
     * instance not initialized then initialize the new instance. */
    if (gaTrieInstance[u4Instance].u2KeySize == 0)
    {
        if (pCreateParams->bSemPerInst == OSIX_TRUE)
        {
            /* Creating sem for this particular trie instance */
            TrieCreateSemName ((INT4) u4Instance, au1SemName);
            if (OsixCreateSem (au1SemName, TRIE_SEM_COUNT, 0, &TrieSemId)
                != OSIX_SUCCESS)
            {
                TrieError (SEM_CREATE_FAIL);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }
            gaTrieInstance[u4Instance].bSemPerInst = OSIX_TRUE;
        }
        else
            gaTrieInstance[u4Instance].bSemPerInst = OSIX_FALSE;

        if (pCreateParams->bPoolPerInst == OSIX_TRUE)
        {
            /* Create memory pool for the new instance and
             *  create the radix node for the new TRIE instance */
            u4rc =
                MemCreateMemPool (sizeof (tRadixNode),
                                  pCreateParams->u4NumRadixNodes,
                                  MEM_HEAP_MEMORY_TYPE,
                                  &gai4RadixPool[u4Instance]);

            if (u4rc == MEM_FAILURE)
            {
                TrieError (RADIX_ALLOC_FAIL);
                Trie2DelSem (u4Instance, TrieSemId);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }

            /* create memory pool for leaf nodes */
            u4rc =
                MemCreateMemPool (sizeof (tLeafNode),
                                  pCreateParams->u4NumLeafNodes,
                                  MEM_HEAP_MEMORY_TYPE,
                                  (tMemPoolId *) & gai4LeafPool[u4Instance]);
            if (u4rc == MEM_FAILURE)
            {
                MemDeleteMemPool (gai4RadixPool[u4Instance]);
                TrieError (LEAF_ALLOC_FAIL);
                Trie2DelSem (u4Instance, TrieSemId);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }

            /* create memory pool for key */
            u4rc = MemCreateMemPool ((UINT2) pCreateParams->u2KeySize,
                                     2 * pCreateParams->u4NoofRoutes,
                                     MEM_HEAP_MEMORY_TYPE,
                                     (tMemPoolId *) &
                                     gi4KeyPoolIdx[u4Instance]);
            if (u4rc == MEM_FAILURE)
            {
                MemDeleteMemPool (gai4RadixPool[u4Instance]);
                MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
                TrieError (KEY_ALLOC_FAIL);
                Trie2DelSem (u4Instance, TrieSemId);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }
            gaTrieInstance[u4Instance].bPoolPerInst = OSIX_TRUE;

            gaTrieInstance[u4Instance].pRadixNodeTop =
                TrieAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                     RADIX_NODE, u4Instance);
            if (gaTrieInstance[u4Instance].pRadixNodeTop == NULL)
            {
                MemDeleteMemPool (gai4RadixPool[u4Instance]);
                MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
                MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
                Trie2DelSem (u4Instance, TrieSemId);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }
        }
        else
        {
            gai4RadixPool[u4Instance] = gTrieCmnRadixPoolId;
            gai4LeafPool[u4Instance] = (INT4) gTrieCmnLeafPoolId;
            gi4KeyPoolIdx[u4Instance] = (INT4) gTrieCmnKeyPoolId;
            gaTrieInstance[u4Instance].bPoolPerInst = OSIX_FALSE;

            gaTrieInstance[u4Instance].pRadixNodeTop =
                TrieAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                     RADIX_NODE, u4Instance);
            if (gaTrieInstance[u4Instance].pRadixNodeTop == NULL)
            {
                Trie2DelSem (u4Instance, TrieSemId);
                OsixSemGive (TRIE_SEM_ID);
                return (NULL);
            }

        }

        gaTrieInstance[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
        gaTrieInstance[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
        gaTrieInstance[u4Instance].pRadixNodeTop->pLeft = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pRight = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pParent = NULL;
        gaTrieInstance[u4Instance].u2KeySize = (UINT2) pCreateParams->u2KeySize;
        gaTrieInstance[u4Instance].u4RouteCount = 0;
        gaTrieInstance[u4Instance].AppFns = pCreateParams->AppFns;
        gaTrieInstance[u4Instance].SemId = TrieSemId;

        if (pCreateParams->bValidateType == OSIX_TRUE)
            gaTrieInstance[u4Instance].u4Type = pCreateParams->u4Type;
        else
            gaTrieInstance[u4Instance].u4Type = 0xffffffff;

    }

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))
    {
        TrieError (ALREADY_USED_APP_ID);
        OsixSemGive (TRIE_SEM_ID);
        TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
        return (NULL);
    }

    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    OsixSemGive (TRIE_SEM_ID);
    TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
    return (pHead);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDel ()                                               */
/*                                                                         */
/*  Description   This function may delete internal structures of Trie     */
/*                after giving application specific information to         */
/*                AppSpecDelFunc () for deletion. *AppSpecDelFunc () is    */
/*                called after traversing the fixed number of application  */
/*                specific entries.                                        */
/*                                                                         */
/*  Call          When there is urgent need for shutdown operation.        */
/*  condition     (A typical condition is when resources are               */
/*                blocked and freeing the resources might help.)           */
/*                                                                         */
/*  Input(s)      pInputParams -         Pointer to the                    */
/*                                       tInputParams structure.           */
/*                (AppSpecDelFunc ()) -  A function to delete the          */
/*                                       application specific              */
/*                                       information.                      */
/*                pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Output(s)     pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
INT4
TrieDel (tInputParams * pInputParams,
         void (*AppSpecDelFunc) (VOID *), VOID *pDeleteOutParams)
{
    UINT4               u4StopFlag;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pDeleteNode = NULL;
    tRadixNodeHead     *pHead;
    INT4                i4RetVal;
    UINT4               u4Instance = 0;
    tOsixSemId          TrieSemId;
    VOID               *pDelCtx;

    VOID               *(*pInitDeleteCtx) (tInputParams *,
                                           VOID (*AppSpecDelFunc1) (VOID *),
                                           VOID *);
    INT4                (*pDelete) (VOID *pDelCtx1, VOID **ppAppPtr, tKey Key);
    VOID                (*pDeInitDeleteCtx) (VOID *pDelCtx1);

    OsixSemTake (TRIE_SEM_ID);
    pHead = pInputParams->pRoot;

    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;

    Trie2TakeSem (u4Instance, TrieSemId);

    pInitDeleteCtx = pHead->AppFns->pInitDeleteCtx;
    pDelete = pHead->AppFns->pDelete;
    pDeInitDeleteCtx = pHead->AppFns->pDeInitDeleteCtx;

    if (NULL == (pDelCtx = (pInitDeleteCtx) (pInputParams, AppSpecDelFunc,
                                             pDeleteOutParams)))
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    pHead = pInputParams->pRoot;

    u4StopFlag = 0;

    pPrevNode = (tRadixNode *) pHead->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            u4StopFlag = 1;
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    while (!(u4StopFlag))
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            if (NULL != pDeleteNode)
            {
                TrieDelRadixLeaf (pHead, pDeleteNode, u4Instance);
                pDeleteNode = NULL;
                pPrevNode = ((tLeafNode *) pCurrNode)->pParent;
            }

            i4RetVal =
                (pDelete) (pDelCtx,
                           &((tLeafNode *) pCurrNode)->pAppSpecPtr,
                           ((tLeafNode *) pCurrNode)->Key);
            if (i4RetVal == TRIE_FAILURE)
            {
                pDeInitDeleteCtx (pDelCtx);
                Trie2GiveSem (u4Instance, TrieSemId);
                return i4RetVal;
            }

            if (NULL == ((tLeafNode *) pCurrNode)->pAppSpecPtr)
            {
                /* App has cleared AppSpecPtr ..
                   meaning no use for this TrieNode */
                pHead->u4RouteCount--;
                pDeleteNode = pCurrNode;
            }
            else
            {
                pDeleteNode = NULL;
            }

            /* pointer updation to reach the next leaf node */
            if (pPrevNode == NULL)
            {
                pDeInitDeleteCtx (pDelCtx);
                Trie2GiveSem (u4Instance, TrieSemId);
                return TRIE_FAILURE;
            }
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (VOID *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        /*  the node is either the null node or the radix node */
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            pPrevNode = (tRadixNode *) pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
    }

    if (NULL != pDeleteNode)
    {
        TrieDelRadixLeaf (pHead, pDeleteNode, u4Instance);
        pDeleteNode = NULL;
    }

    pDeInitDeleteCtx (pDelCtx);

    if (pInputParams->i1AppId == -1)
    {
        /* Clearing all the Application Specific info from the TRIE */
        pHead->u2AppMask = 0;
    }
    else
    {
        /* Clearing a Specific Application Info from the TRIE */
        RESET_BIT (pInputParams->i1AppId - 1, pHead->u2AppMask);
    }

    /* no entries are there in this instance */
    if (pHead->u2AppMask == 0)
    {
        if ((gaTrieInstance[u4Instance].u2KeySize) > U4_SIZE)
        {
            TrieReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pHead->pRadixNodeTop->Mask.pKey);
        }

        TrieReleaseNode (RADIX_NODE, u4Instance,
                         (UINT1 *) pHead->pRadixNodeTop);
        pHead->u2KeySize = 0;
        pHead->u4Type = 0;
        /* Delete the mempools that were allocated during create
         * RadixNode pool, LeafNode pool and Key pool
         */
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_TRUE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
        }
        Trie2GiveSem (u4Instance, TrieSemId);
        Trie2DelSem (u4Instance, TrieSemId);
    }
    else
    {
        Trie2GiveSem (u4Instance, TrieSemId);
    }

    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieCrtInstance ()                                       */
/*                                                                         */
/*  Description   This function is used to create a Trie instance. It      */
/*                 returns pRoot parameter  with the root of the created   */
/*                 Trie  instance.                                         */
/*                                                                         */
/*  Call          Should be called once before using                       */
/*  condition     any other Trie library function.                         */
/*                                                                         */
/*  Input(s)      pCreateParams - Pointer to the tTrieCrtParams            */
/*                                structure.                               */
/*                                                                         */
/*  Output(s)     None                                                     */
/*                                                                         */
/*  Access        Applications who want to use the Trie library.           */
/*  privileges                                                             */
/*                                                                         */
/*  Return        pRoot -         Pointer to the root of the instance of   */
/*                                the Trie (the tRadixNodeHead             */
/*                                structure), if successful.               */
/*                NULL  -          if trie creation fails                  */
/*                                                                         */
/* Status        This function is Obseleted. Recommended to use            */
/*               TrieCreateInstance                                        */
/*                                                                         */
/***************************************************************************/
tRadixNodeHead     *
TrieCrtInstance (tTrieCrtParams * pCreateParams)
{
    UINT1               u1AppId;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance = 0;
    UINT4               u4rc;
    UINT1               au1SemName[MAX_TRIE_SEM_NAME_SIZE];
    tOsixSemId          TrieSemId = 0;

    OsixSemTake (TRIE_SEM_ID);

    if (NULL == pCreateParams->AppFns)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->u2KeySize > MAX_KEY_SIZE)
    {
        TrieError (MAX_KEY_SIZE_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    u1AppId = (UINT1) pCreateParams->u1AppId;
    if ((u1AppId > TRIE2_MAX_ROUTING_PROTOCOLS) || (u1AppId == 0))
    {
        TrieError (MAX_APP_OVERFLOW);
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    pHead = TrieGetFreeInstance (&u4Instance);
    if (pHead == NULL)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    /* Verify whether the Trie instance is already initialized or not. If
     * instance not initialized then initialize the new instance. */
    if (gaTrieInstance[u4Instance].u2KeySize == 0)
    {
        gaTrieInstance[u4Instance].bPoolPerInst = OSIX_TRUE;
        gaTrieInstance[u4Instance].bSemPerInst = OSIX_TRUE;
        /* Instance not initialized earlier. */
        TrieCreateSemName ((INT4) u4Instance, au1SemName);
        if (OsixCreateSem (au1SemName, TRIE_SEM_COUNT, 0, &TrieSemId)
            != OSIX_SUCCESS)
        {
            TrieError (SEM_CREATE_FAIL);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* Create memory pool for the new instance and create the radix node
         * top for the new TRIE instance */
        /* create memory pool for radix nodes */
        u4rc =
            MemCreateMemPool (sizeof (tRadixNode),
                              pCreateParams->u4NumRadixNodes,
                              MEM_HEAP_MEMORY_TYPE, &gai4RadixPool[u4Instance]);

        if (u4rc == MEM_FAILURE)
        {
            TrieError (RADIX_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* create memory pool for leaf nodes */
        u4rc =
            MemCreateMemPool (sizeof (tLeafNode), pCreateParams->u4NumLeafNodes,
                              MEM_HEAP_MEMORY_TYPE,
                              (tMemPoolId *) & gai4LeafPool[u4Instance]);
        if (u4rc == MEM_FAILURE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            TrieError (LEAF_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        /* create memory pool for key */
        u4rc = MemCreateMemPool ((UINT2) pCreateParams->u2KeySize,
                                 2 * pCreateParams->u4NoofRoutes,
                                 MEM_HEAP_MEMORY_TYPE,
                                 (tMemPoolId *) & gi4KeyPoolIdx[u4Instance]);
        if (u4rc == MEM_FAILURE)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            TrieError (KEY_ALLOC_FAIL);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieInstance[u4Instance].pRadixNodeTop =
            TrieAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                 RADIX_NODE, u4Instance);
        if (gaTrieInstance[u4Instance].pRadixNodeTop == NULL)
        {
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
            Trie2DelSem (u4Instance, TrieSemId);
            OsixSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieInstance[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
        gaTrieInstance[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
        gaTrieInstance[u4Instance].pRadixNodeTop->pLeft = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pRight = NULL;
        gaTrieInstance[u4Instance].pRadixNodeTop->pParent = NULL;
        gaTrieInstance[u4Instance].u4Type = 0xffffffff;
        gaTrieInstance[u4Instance].u2KeySize = (UINT2) pCreateParams->u2KeySize;
        gaTrieInstance[u4Instance].u4RouteCount = 0;
        gaTrieInstance[u4Instance].AppFns = pCreateParams->AppFns;
        gaTrieInstance[u4Instance].SemId = TrieSemId;
    }

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))
    {
        TrieError (ALREADY_USED_APP_ID);
        OsixSemGive (TRIE_SEM_ID);
        TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
        return (NULL);
    }

    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    OsixSemGive (TRIE_SEM_ID);
    TRIE_KW_FALSEPOSITIVE_FIX (TrieSemId);
    return (pHead);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDelInstance ()                                       */
/*                                                                         */
/*  Description   This function may delete internal structures of Trie     */
/*                after giving application specific information to         */
/*                AppSpecDelFunc () for deletion. *AppSpecDelFunc () is    */
/*                called after traversing the fixed number of application  */
/*                specific entries.                                        */
/*                                                                         */
/*  Call          When there is urgent need for shutdown operation.        */
/*  condition     (A typical condition is when resources are               */
/*                blocked and freeing the resources might help.)           */
/*                                                                         */
/*  Input(s)      pInputParams -         Pointer to the                    */
/*                                       tInputParams structure.           */
/*                (AppSpecDelFunc ()) -  A function to delete the          */
/*                                       application specific              */
/*                                       information.                      */
/*                pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Output(s)     pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS/TRIE_FAILURE                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieDelInstance (tInputParams * pInputParams,
                 void (*AppSpecDelFunc) (VOID *), VOID *pDeleteOutParams)
{
    UINT4               u4StopFlag;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pDeleteNode = NULL;
    tRadixNodeHead     *pHead;
    INT4                i4RetVal;
    UINT4               u4Instance = 0;
    tOsixSemId          TrieSemId;
    VOID               *pDelCtx;

    VOID               *(*pInitDeleteCtx) (tInputParams *,
                                           VOID (*AppSpecDelFunc1) (VOID *),
                                           VOID *);
    INT4                (*pDelete) (VOID *pDelCtx1, VOID **ppAppPtr, tKey Key);
    VOID                (*pDeInitDeleteCtx) (VOID *pDelCtx1);

    OsixSemTake (TRIE_SEM_ID);
    pHead = pInputParams->pRoot;

    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;

    Trie2TakeSem (u4Instance, TrieSemId);

    pInitDeleteCtx = pHead->AppFns->pInitDeleteCtx;
    pDelete = pHead->AppFns->pDelete;
    pDeInitDeleteCtx = pHead->AppFns->pDeInitDeleteCtx;

    if (NULL == (pDelCtx = (pInitDeleteCtx) (pInputParams, AppSpecDelFunc,
                                             pDeleteOutParams)))
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    pHead = pInputParams->pRoot;

    u4StopFlag = 0;

    pPrevNode = (tRadixNode *) pHead->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            u4StopFlag = 1;
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    while (!(u4StopFlag))
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            if (NULL != pDeleteNode)
            {
                TrieDelRadixLeaf (pHead, pDeleteNode, u4Instance);
                pDeleteNode = NULL;
                pPrevNode = ((tLeafNode *) pCurrNode)->pParent;
            }

            i4RetVal =
                (pDelete) (pDelCtx,
                           &((tLeafNode *) pCurrNode)->pAppSpecPtr,
                           ((tLeafNode *) pCurrNode)->Key);
            if (i4RetVal == TRIE_FAILURE)
            {
                pDeInitDeleteCtx (pDelCtx);
                Trie2GiveSem (u4Instance, TrieSemId);
                return i4RetVal;
            }

            if (NULL == ((tLeafNode *) pCurrNode)->pAppSpecPtr)
            {
                /* App has cleared AppSpecPtr ..
                   meaning no use for this TrieNode */
                pHead->u4RouteCount--;
                pDeleteNode = pCurrNode;
            }
            else
            {
                pDeleteNode = NULL;
            }

            /* pointer updation to reach the next leaf node */
            if (pPrevNode == NULL)
            {
                pDeInitDeleteCtx (pDelCtx);
                Trie2GiveSem (u4Instance, TrieSemId);
                return TRIE_FAILURE;
            }
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (VOID *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        /*  the node is either the null node or the radix node */
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            pPrevNode = (tRadixNode *) pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
    }

    if (NULL != pDeleteNode)
    {
        TrieDelRadixLeaf (pHead, pDeleteNode, u4Instance);
        pDeleteNode = NULL;
    }

    pDeInitDeleteCtx (pDelCtx);

    if (pInputParams->i1AppId == -1)
    {
        /* Clearing all the Application Specific info from the TRIE */
        pHead->u2AppMask = 0;
    }
    else
    {
        /* Clearing a Specific Application Info from the TRIE */
        RESET_BIT (pInputParams->i1AppId - 1, pHead->u2AppMask);
    }

    /* no entries are there in this instance */
    if (pHead->u2AppMask == 0)
    {
        if ((gaTrieInstance[u4Instance].u2KeySize) > U4_SIZE)
        {
            TrieReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pHead->pRadixNodeTop->Mask.pKey);
        }

        TrieReleaseNode (RADIX_NODE, u4Instance,
                         (UINT1 *) pHead->pRadixNodeTop);
        pHead->u2KeySize = 0;
        pHead->u4Type = 0;
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_TRUE)
        {
            /* Delete the mempools that were allocated during create
             * RadixNode pool, LeafNode pool and Key pool
             */
            MemDeleteMemPool (gai4RadixPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gai4LeafPool[u4Instance]);
            MemDeleteMemPool ((tMemPoolId) gi4KeyPoolIdx[u4Instance]);
        }
        Trie2GiveSem (u4Instance, TrieSemId);
        Trie2DelSem (u4Instance, TrieSemId);
    }
    else
    {
        Trie2GiveSem (u4Instance, TrieSemId);
    }
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieAdd ()                                               */
/*                                                                         */
/*  Description   This function adds the pAppSpecInfo in an instance of    */
/*                Trie. If the application specific information is         */
/*                already present in the leaf node, it is replaced with    */
/*                the new information and the old information is           */
/*                returned using tOutputParams. If this operation is       */
/*                aborted due to resource problems, it has the return      */
/*                value TRIE_FAILURE.                                      */
/*                                                                         */
/*  Call          Whenever an application wants to add the entry or        */
/*  condition     update already existing entry.                           */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                pAppSpecInfo -    Pointer to the application specific    */
/*                                  information.                           */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If  adding  operation   is              */
/*                            successful.                                  */
/*                TRIE_FAILURE -   If it fails due to any                  */
/*                            reasons.                                     */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieAdd (tInputParams * pInputParams, VOID *pAppSpecInfo, VOID *pOutputParams)
{
    UINT2               u2KeySize;
    UINT1               u1DiffBit;
    UINT1               u1BitToTest;
    UINT1               u1DiffByteVal = 0;
    UINT4               u4DiffByte;
    tKey                InKey;
    tRadixNode         *pRadix;
    tRadixNode         *pParentNode;
    tLeafNode          *pLeaf;
    tLeafNode          *pPrevLeaf;
    tLeafNode          *pNewLeaf;
    tOsixSemId          TrieSemId;

    INT4                (*pAddAppSpecInfo) (tInputParams * pInputParams1,
                                            VOID *pOutputParams1,
                                            VOID **ppAppPtr,
                                            VOID *pAppSpecInfo1);

    UINT4               u4Instance;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return (TRIE_FAILURE);
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    pAddAppSpecInfo = pInputParams->pRoot->AppFns->pAddAppSpecInfo;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    pLeaf = NULL;

    /* When TrieTravese fails to find the nearest leaf node
     * this function tries to add a leaf.
     */
    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        if ((TrieInsertLeaf (u2KeySize, pInputParams, pAppSpecInfo,
                             ((tRadixNode *) pLeaf),
                             pOutputParams, &pNewLeaf)) == TRIE_FAILURE)
        {
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_FAILURE);
        }

        /* instead of calling TrieSetMask () with u2KeySize
         * It is called here with size of the mask.
         */

        /* pLeaf represents parent of the leaf node here */
        TrieSetMask (u2KeySize, pLeaf);
        /* this is done in TrieInsertLeaf()
           pOutputParams->pAppSpecInfo = (VOID *) NULL;
         */

        if (NULL == (pPrevLeaf = TrieGetPrevLeafNode (pNewLeaf)))
        {
            /* means we are the left most node of the trie */
            DLIST_INSERT_HEAD ((pInputParams->pRoot), NodeHead, pNewLeaf,
                               NodeLink);
        }
        else
        {
            DLIST_INSERT ((pInputParams->pRoot), NodeHead, pNewLeaf, NodeLink,
                          pPrevLeaf);
        }
        pInputParams->pRoot->u4RouteCount++;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_SUCCESS);
    }

    /* TrieDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    DIFFBYTE (pLeaf->Key, InKey, u2KeySize, u4DiffByte, u1DiffByteVal);

    if (u4DiffByte == u2KeySize)
    {
        if (TRIE_FAILURE == (pAddAppSpecInfo) (pInputParams, pOutputParams,
                                               &pLeaf->pAppSpecPtr,
                                               pAppSpecInfo))
        {
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_FAILURE;
        }
    }
    else
    {
        DIFFBIT (u1DiffBit, u1DiffByteVal);
        u1BitToTest = (UINT1) (0x80 >> u1DiffBit);

        /*u1DiffBit   += BYTE_LENGTH*u4DiffByte; */
        pParentNode = pLeaf->pParent;
        while ((u4DiffByte < pParentNode->u1ByteToTest) ||
               ((u4DiffByte == pParentNode->u1ByteToTest) &&
                (u1BitToTest > pParentNode->u1BitToTest)))
        {
            pParentNode = pParentNode->pParent;
        }
        if (TrieInsertRadix
            (u2KeySize, u1BitToTest, (UINT1) u4DiffByte, pInputParams,
             pAppSpecInfo, pParentNode, pOutputParams) == TRIE_FAILURE)
        {
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_FAILURE);
        }
    }

    /* Make output parameters null to represent fresh addition */
    /* this is done in TrieInsertLeaf()
       pOutputParams->pAppSpecInfo = (VOID *) NULL;
     */

    (pInputParams->pRoot)->u4RouteCount++;
    Trie2GiveSem (u4Instance, TrieSemId);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieRemove()                                             */
/*                                                                         */
/*  Description   Delete operation is always preceded by an exact search   */
/*                internally. After finding the required information, the  */
/*                application specific information and the key is          */
/*                returned to the application using the tOutputParams      */
/*                structure to delete actually. If search operation is     */
/*                unsuccessful, this function returns with TRIE_FAILURE.   */
/*                This function deletes unnecessary radix/leaf node,if any.*/
/*                                                                         */
/*  Call          Whenever an application wants to remove an entry from    */
/*  condition     this instance of Trie.                                   */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If  deleting  operation  is successful. */
/*                TRIE_FAILURE  -  If  it  fails  due  to  any reasons.    */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieRemove (tInputParams * pInputParams, VOID *pOutputParams, VOID *pNxtHop)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    INT4                i4RetVal = TRIE_SUCCESS;
    UINT4               u4Instance = 0;
    tOsixSemId          TrieSemId;

    INT4                (*pDeleteEntry) (tInputParams * pInputParams1,
                                         VOID **ppAppPtr, VOID *pOutputParams1,
                                         VOID *pNxtHop1, tKey Key);

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pDeleteEntry = pInputParams->pRoot->AppFns->pDeleteEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    /* TrieDoTraverse returned a leaf node. */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    /* TODO ::if the leaf is found .. 
       1. call app spec delete fn 
       2. if the function returns NULL, delete LeafNode
       3. else assign the return value to leafnode->appspecinfo
     */
    if (TRIE_FAILURE == (pDeleteEntry) (pInputParams, &pLeaf->pAppSpecPtr,
                                        pOutputParams, pNxtHop, pLeaf->Key))
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_FAILURE;
    }

    if (NULL == (tLeafNode *) pLeaf->pAppSpecPtr)
    {
        /* App has cleared AppSpecPtr .. meaning no use for this TrieNode */
        TrieDelRadixLeaf ((tRadixNodeHead *) pInputParams->pRoot, pLeaf,
                          u4Instance);
        pInputParams->pRoot->u4RouteCount--;
    }

    Trie2GiveSem (u4Instance, TrieSemId);
    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*  Function      INT4 TrieSearch ()                                       */
/*                                                                         */
/*  Description   This function searches for the specified key given by    */
/*                the tInputParams in an instance of Trie. If the key is   */
/*                found, this function copies the application specific     */
/*                information and key of the searched entry to the         */
/*                tOutputParams. If the  matching key is not found,        */
/*                it returns TRIE_FAILURE. This is an exact match operation*/
/*                The result of this operation should not be used to       */
/*                delete entries present in the Trie instance.             */
/*                TrieRemove () must be called for deleting entries.       */
/*                                                                         */
/*  Call          whenever an application needs to search for              */
/*  condition     information associated with the given key only.          */
/*                                                                         */
/*  Input(s)      pInputParams -  Pointer to the tInputParams structure.   */
/*                                                                         */
/*  Output(s)     pOutputParams - Pointer to the tOutputParams structure.  */
/*                ppNode - Trie Node where this route is found. Application*/
/*                         should not do any processing with this node. It */
/*                         can just pass this info in the next Search      */
/*                         request before the Trie is updated for enabling */
/*                         a faster search.                                */
/*                                                                         */
/*  Access        The applications using the Trie library.                 */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If the key is found.                      */
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieSearch (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf = NULL;
    tOsixSemId          TrieSemId;
    INT4                (*pSearchEntry) (tInputParams * pInputParams1,
                                         VOID *pOutputParams1, VOID *pLeaf1);

    UINT4               u4Instance;

    *ppNode = NULL;
    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;

    Trie2TakeSem (u4Instance, TrieSemId);

    pSearchEntry = pInputParams->pRoot->AppFns->pSearchEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (pInputParams->pLeafNode != NULL)
    {
        /* Input parameter is carrying the pointer to the Leaf node. Validate
         * whether it is a valid pointer or not. If yes, then use it else
         * traverse and find appropriate leaf */
        pLeaf = (tLeafNode *) pInputParams->pLeafNode;
        i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
        if (i4Eql != 0)
        {
            /* Seems the input leaf node is not valid. */
            pLeaf = NULL;
        }
    }

    if (pLeaf == NULL)
    {
        if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
        {
            *ppNode = NULL;
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_FAILURE);
        }
    }

    /* TrieDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    if (TRIE_FAILURE ==
        (pSearchEntry) (pInputParams, pOutputParams, pLeaf->pAppSpecPtr))
    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_FAILURE;
    }

    *ppNode = (VOID *) pLeaf;
    Trie2GiveSem (u4Instance, TrieSemId);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLookup()                                             */
/*                                                                         */
/*  Description   This function first searches for an exact match in an    */
/*                instance of Trie. However, if it is not successful in    */
/*                finding exact match, it looks for the best match. If it  */
/*                fails in finding the best match also, it returns         */
/*                TRIE_FAILURE. The result of this operation should not be */
/*                used to delete entries which are present in Trie. If     */
/*                the return value is TRIE_SUCCESS, tOutputParams will     */
/*                contain related information of exact/best match including*/
/*                the key.                                                 */
/*                                                                         */
/*  Call          This function is mostly appropriate to routing           */
/*  condition     tables. Here, a key of the tInputParams represents the   */
/*                IP address. The application, by calling this function,   */
/*                requests best match.                                     */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                ppNode - Trie Node where this route is found. Application*/
/*                         should not do any processing with this node. It */
/*                         can just pass this info in the next Search      */
/*                         request before the Trie is updated for enabling */
/*                         a faster search.                                */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS  -  If it finds the exact/best match.       */
/*                TRIE_FAILURE  -  If it fails in finding  the             */
/*                            best match also.                             */
/*                                                                         */
/***************************************************************************/
INT4
TrieLookup (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    INT4                i4Return;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    tOsixSemId          TrieSemId;

    INT4                (*pLookupEntry) (tInputParams * pInputParams1,
                                         VOID *pOutputParams1, VOID *pAppPtr,
                                         UINT2 u2KeySize1, tKey Key1);

    UINT4               u4Instance;
    *ppNode = NULL;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;

    Trie2TakeSem (u4Instance, TrieSemId);

    pLookupEntry = pInputParams->pRoot->AppFns->pLookupEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        if (u2KeySize > U4_SIZE)
        {
            i4Return =
                TrieGetBestMatch (u2KeySize, pInputParams, (VOID *) pLeaf,
                                  pOutputParams);
        }
        else
        {
            i4Return =
                TrieGetBestMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf,
                                   pOutputParams);
        }
        if (i4Return == TRIE_FAILURE)
        {
            *ppNode = NULL;
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_FAILURE);
        }
        else
        {
            *ppNode = pLeaf;
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_SUCCESS);
        }
    }

    /* TrieDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql == 0)
    {
        /* call pLookupentry (), if success return if failure continue 
         */
        i4Return =
            (pLookupEntry) (pInputParams, pOutputParams, pLeaf->pAppSpecPtr,
                            u2KeySize, pLeaf->Key);
        if (TRIE_SUCCESS == i4Return)
        {
            *ppNode = pLeaf;
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_SUCCESS;
        }
    }
    /* else continue !! */

    if (u2KeySize > U4_SIZE)
    {
        i4Return =
            TrieGetBestMatch (u2KeySize, pInputParams, (VOID *) pLeaf,
                              pOutputParams);
    }
    else
    {
        i4Return =
            TrieGetBestMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf,
                               pOutputParams);
    }
    if (i4Return == TRIE_SUCCESS)
    {
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_SUCCESS);
    }
    else
    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieWalk ()                                              */
/*                                                                         */
/*  Description   This function scans entire instance of Trie for a        */
/*                given application. AppSpecScanFunc () is called after    */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns TRIE_FAILURE, this*/
/*                function also returns TRIE_FAILURE.                      */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     Trie has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS -  If this operation  is  done successfully.*/
/*                TRIE_FAILURE -  If this operation is aborted due to the  */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieWalk (tInputParams * pInputParams,
          INT4 (*AppSpecScanFunc) (VOID *), VOID *pScanOutParams)
{
    VOID               *pCurrNode;
    VOID               *pNextNode = NULL;
    VOID               *pScanCtx;
    UINT4               u4Instance = 0;
    tOsixSemId          TrieSemId;
    VOID               *(*pInitScanCtx) (tInputParams * pInputParams1,
                                         VOID (*AppSpecScanFunc1) (VOID *),
                                         VOID *pScanOutParams1);
    INT4                (*pScan) (VOID *pScanCtx1, VOID **ppAppPtr1, tKey Key1);
    VOID                (*pDeInitScanCtx) (VOID *pScanCtx1);

    OsixSemTake (TRIE_SEM_ID);

    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pInitScanCtx = pInputParams->pRoot->AppFns->pInitScanCtx;
    pScan = pInputParams->pRoot->AppFns->pScan;
    pDeInitScanCtx = pInputParams->pRoot->AppFns->pDeInitScanCtx;

    if (NULL ==
        (pScanCtx = (pInitScanCtx) (pInputParams,
                                    (VOID (*)(VOID *)) AppSpecScanFunc,
                                    pScanOutParams)))
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    pCurrNode = pInputParams->pRoot->NodeHead.pNext;
    if (pCurrNode == NULL)
    {
        /* TRIE is EMPTY */
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_SUCCESS);
    }

    while (pCurrNode != NULL)
    {
        /* Use the DLIST_NODE to Scan through the TRIE. First Get and
         * store the next Leaf Node information from the current
         * leaf node and process the current node. */

        pNextNode = ((tLeafNode *) pCurrNode)->NodeLink.pNext;

        if (TRIE_FAILURE ==
            (pScan) (pScanCtx, &(((tLeafNode *) pCurrNode)->pAppSpecPtr),
                     ((tLeafNode *) pCurrNode)->Key))
        {
            pDeInitScanCtx (pScanCtx);
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_FAILURE;
        }

        if (((tLeafNode *) pCurrNode)->pAppSpecPtr == NULL)
        {
            /* All the AppSpec information within this node has been
             * cleared, within the Scan Call-back Routine.
             * This node can be removed. */
            TrieDelRadixLeaf ((tRadixNodeHead *) pInputParams->pRoot,
                              pCurrNode, u4Instance);
            pInputParams->pRoot->u4RouteCount--;
        }
        pCurrNode = pNextNode;
    }
    pDeInitScanCtx (pScanCtx);

    Trie2GiveSem (u4Instance, TrieSemId);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetNext                                              */
/*                                                                         */
/*  Description   This function finds the next key and the associated      */
/*                application specfic entry  using a given key and         */
/*                application id. The input key need not be present        */
/*                inside the trie structure. If input id is a generic id,  */
/*                it returns every application specific information along  */
/*                with the key.                                            */
/*                                                                         */
/*  Call         When next entry in the routing table is needed.           */
/*  condition    typically for SNMP.                                       */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*               ppNode - Trie Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the Trie is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the Trie library.                 */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the next entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetNext (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Cmp;
    tKey                InKey;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    tLeafNode          *pLeaf;
    tOsixSemId          TrieSemId;
    INT4                i4RetVal = TRIE_SUCCESS;
    INT4                i4Eql = 0;
    INT4                (*pGetNextEntry) (tInputParams * pInputParams1,
                                          VOID *pOutputParams1, VOID *pAppPtr1,
                                          tKey Key1);

    UINT4               u4Instance = 0;

    *ppNode = NULL;
    pLeaf = NULL;

    OsixSemTake (TRIE_SEM_ID);

    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pGetNextEntry = pInputParams->pRoot->AppFns->pGetNextEntry;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    pPrevNode = pInputParams->pRoot->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    InKey = pInputParams->Key;
    if ((pPrevNode->pRight == NULL) && (pPrevNode->pLeft == NULL))

    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    if (pInputParams->pLeafNode != NULL)
    {
        /* Input parameter is carrying the pointer to the Leaf node. Validate
         * whether it is a valid pointer or not. If yes, then use it else
         * traverse and find appropriate leaf */
        pLeaf = (tLeafNode *) pInputParams->pLeafNode;
        i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
        if (i4Eql != 0)
        {
            /* Seems the input leaf node is not valid. */
            pLeaf = NULL;
        }
    }

    if (pLeaf == NULL)
    {
        i4RetVal = TrieDoTraverse (u2KeySize, pPrevNode, InKey,
                                   (VOID *) &(pLeaf));
    }

    if (i4RetVal == TRIE_FAILURE)
    {
        pCurrNode = NULL;
        pPrevNode = (tRadixNode *) pLeaf;
    }
    else
    {
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u2KeySize);
        if (i4Cmp < 0)
        {
            if (TRIE_SUCCESS == (pGetNextEntry) (pInputParams, pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
            {
                *ppNode = pLeaf;
                Trie2GiveSem (u4Instance, TrieSemId);
                return TRIE_SUCCESS;
            }
        }

        pPrevNode = pLeaf->pParent;
        pCurrNode = pLeaf;
    }

    if (pPrevNode == NULL)
    {
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }
    for (;;)
    {

        /* here logic is same as that used in TrieWalk */
        if (pPrevNode->pLeft == pCurrNode)
        {
            pCurrNode = pPrevNode->pRight;
        }
        else
        {
            while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
            {
                pCurrNode = (VOID *) pPrevNode;
                pPrevNode = pPrevNode->pParent;
            }
            if (pPrevNode == NULL)
            {
                *ppNode = NULL;
                Trie2GiveSem (u4Instance, TrieSemId);
                return (TRIE_FAILURE);
            }
            else
            {
                pCurrNode = pPrevNode->pRight;
            }
        }

        /* find out the next leaf node or NULL */
        while ((pCurrNode != NULL) &&
               (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE))

        {
            pPrevNode = pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
        if (pCurrNode == NULL)

        {
            *ppNode = NULL;
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_FAILURE);
        }
        pLeaf = (tLeafNode *) pCurrNode;
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u2KeySize);
        if (i4Cmp < 0)
        {
            if (TRIE_SUCCESS == (pGetNextEntry) (pInputParams, pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
            {
                *ppNode = pLeaf;
                Trie2GiveSem (u4Instance, TrieSemId);
                return TRIE_SUCCESS;
            }
        }
    }

}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieRevise                                               */
/*                                                                         */
/*  Description   This function updates the alternate path list.           */
/*                Whenever there is a change in Metric or Row Status, this */
/*                function should be called. This sorts based on the new   */
/*                metric. This function helps to aVOID the row status check*/
/*                in Forwarding path.                                      */
/*                                                                         */
/*  Call         Whenever there is a change in Metric for the existing     */
/*  condition    route.                                                    */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the Trie library.                 */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the next entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieRevise (tInputParams * pInputParams,
            VOID *pAppSpecInfo, UINT4 u4NewMetric, VOID *pOutputParams)
{

    UINT2               u2KeySize;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    tOsixSemId          TrieSemId;

    INT4                (*pUpdate) (tInputParams * pInputParams1,
                                    VOID *pOutputParams1, VOID **ppAppPtr1,
                                    VOID *pAppSpecInfo1, UINT4 u4NewMetric1);

    UINT4               u4Instance = 0;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pUpdate = pInputParams->pRoot->AppFns->pUpdate;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    pLeaf = NULL;
    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    /* TrieDoTraverse returned a leaf node. */
    if (TRIE_FAILURE ==
        pUpdate (pInputParams, pOutputParams, &pLeaf->pAppSpecPtr, pAppSpecInfo,
                 u4NewMetric))

    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }
    Trie2GiveSem (u4Instance, TrieSemId);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieWalkAll ()                                           */
/*                                                                         */
/*  Description   This function scans entire instance of Trie for all      */
/*                applications. AppSpecScanFunc () is called after         */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns TRIE_FAILURE, this*/
/*                function also returns TRIE_FAILURE.                      */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     Trie has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*  Access        Applications using the Trie library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS -  If this operation  is  done successfully.*/
/*                TRIE_FAILURE -  If this operation is aborted due to the  */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieWalkAll (tInputParams * pInputParams,
             INT4 (*AppSpecScanFunc) (VOID *), VOID *pScanOutParams)
{

    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pScanCtx;
    tOsixSemId          TrieSemId;

    VOID               *(*pInitScanCtx) (tInputParams * pInputParams1,
                                         VOID (*AppSpecScanFunc1) (VOID *),
                                         VOID *pScanOutParams1);
    INT4                (*pScanAll) (VOID *pScanCtx1, VOID *pAppPtr, tKey Key);
    VOID                (*pDeInitScanCtx) (VOID *pScanCtx1);

    UINT4               u4Instance = 0;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pInitScanCtx = pInputParams->pRoot->AppFns->pInitScanCtx;
    pScanAll = pInputParams->pRoot->AppFns->pScanAll;
    pDeInitScanCtx = pInputParams->pRoot->AppFns->pDeInitScanCtx;

    if (NULL == (pScanCtx = (pInitScanCtx) (pInputParams,
                                            (VOID (*)(VOID *)) AppSpecScanFunc,
                                            pScanOutParams)))
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }

    pPrevNode = pInputParams->pRoot->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode == NULL)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        return (TRIE_FAILURE);
    }
    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            Trie2GiveSem (u4Instance, TrieSemId);
            return (TRIE_SUCCESS);
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    for (;;)
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            /* give the leafnode, fnptr, out blah blah..
               to be freed by the code !! */
            if (TRIE_FAILURE ==
                (pScanAll) (pScanCtx, ((tLeafNode *) pCurrNode)->pAppSpecPtr,
                            ((tLeafNode *) pCurrNode)->Key))
            {
                pDeInitScanCtx (pScanCtx);
                Trie2GiveSem (u4Instance, TrieSemId);
                return TRIE_FAILURE;
            }

            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (void *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            while (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE)
            {
                pPrevNode = (tRadixNode *) pCurrNode;
                pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
            }
        }
    }

    pDeInitScanCtx (pScanCtx);

    Trie2GiveSem (u4Instance, TrieSemId);
    return (TRIE_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetFirstNode                                         */
/*                                                                         */
/*  Description   This function finds the first node in the TRIE and       */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When first entry in a TRIE is needed.                     */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the AppSpec Information          */
/*               ppNode - Trie Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the Trie is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the Trie library.                  */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the first entry.  */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetFirstNode (tInputParams * pInputParams, VOID **pAppSpecPtr,
                  VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tOsixSemId          TrieSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    if (NULL != (pLeaf = DLIST_FIRSTNODE (pInputParams->pRoot, NodeHead)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    Trie2GiveSem (u4Instance, TrieSemId);
    return TRIE_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetLastNode                                          */
/*                                                                         */
/*  Description   This function finds the last node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When last entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the AppSpec Information          */
/*               ppNode - Trie Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the Trie is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the Trie library.                  */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the last entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetLastNode (tInputParams * pInputParams, VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tOsixSemId          TrieSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    if (NULL != (pLeaf = DLIST_LASTNODE (pInputParams->pRoot, NodeHead)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    Trie2GiveSem (u4Instance, TrieSemId);
    return TRIE_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetNextNode                                          */
/*                                                                         */
/*  Description   This function finds the next node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When next entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*               pGetCtx      -  Pointer to the node where the current     */
/*                               entry is present.                         */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the next AppSpec Information     */
/*               ppNode - Trie Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the Trie is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the Trie library.                  */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the next entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetNextNode (tInputParams * pInputParams, VOID *pGetCtx,
                 VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tKey                InKey;
    UINT2               u2KeySize;
    tRadixNode         *pRadix;
    INT4                i4Eql;
    tOsixSemId          TrieSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    if (NULL != pGetCtx)
    {
        if (NULL != (pLeaf = DLIST_NEXT ((tLeafNode *) pGetCtx, NodeLink)))
        {
            *pAppSpecPtr = pLeaf->pAppSpecPtr;
            *ppNode = pLeaf;
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_SUCCESS;
        }
        else
        {
            *pAppSpecPtr = NULL;
            *ppNode = NULL;
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_FAILURE;
        }
    }

    /* we should use the key to scan now */
    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_FAILURE;
    }

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_FAILURE;
    }

    if (NULL != (pLeaf = DLIST_NEXT ((tLeafNode *) pLeaf, NodeLink)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    Trie2GiveSem (u4Instance, TrieSemId);
    return TRIE_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieGetNextNode                                          */
/*                                                                         */
/*  Description   This function finds the prev node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When prev entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*               pGetCtx      -  Pointer to the node where the current     */
/*                               entry is present.                         */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the prev AppSpec Information     */
/*               ppNode - Trie Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the Trie is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the Trie library.                  */
/*                                                                         */
/*  Return       TRIE_SUCCESS - If successful in finding the prev entry.   */
/*               TRIE_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieGetPrevNode (tInputParams * pInputParams, VOID *pGetCtx,
                 VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tKey                InKey;
    UINT2               u2KeySize;
    tRadixNode         *pRadix;
    INT4                i4Eql;
    tOsixSemId          TrieSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    /* If incoming Context is valid, use it */
    if (NULL != pGetCtx)
    {
        if (NULL != (pLeaf = DLIST_PREV ((tLeafNode *) pGetCtx, NodeLink)))
        {
            *pAppSpecPtr = pLeaf->pAppSpecPtr;
            *ppNode = pLeaf;
            Trie2GiveSem (u4Instance, TrieSemId);
            return TRIE_SUCCESS;
        }
        else
        {
            Trie2GiveSem (u4Instance, TrieSemId);
            *pAppSpecPtr = NULL;
            *ppNode = NULL;
            return TRIE_FAILURE;
        }
    }

    /* we should use the key to scan now */
    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == TRIE_FAILURE)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        return TRIE_FAILURE;
    }

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        Trie2GiveSem (u4Instance, TrieSemId);
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        return TRIE_FAILURE;
    }

    if (NULL != (pLeaf = DLIST_PREV ((tLeafNode *) pLeaf, NodeLink)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return TRIE_SUCCESS;
    }

    Trie2GiveSem (u4Instance, TrieSemId);
    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    return TRIE_FAILURE;
}

INT4
TrieLookupOverlap (tInputParams * pInputParams, VOID *pOutputParams,
                   VOID **ppNode)
{
    /* pseudo code:
     * ~~~~~~~~~~~~
     * Get (FirstMatch)
     * if (FirstMatch)
     *   return FirstMatch;
     * else
     *   return NULL;
     */

    UINT2               u2KeySize;
    INT4                i4Ret;
    tLeafNode          *pLeaf = NULL;
    tOsixSemId          TrieSemId;

    INT4                (*pLookupEntryOverlap) (tInputParams * pInputParams1,
                                                VOID *pOutputParams1,
                                                VOID *AppSpecPtr1,
                                                UINT2 u2KeySize1, tKey Key);

    UINT4               u4Instance = 0;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        *ppNode = NULL;
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pLookupEntryOverlap =
        ((tRadixNodeHead *) pInputParams->pRoot)->AppFns->pLookupEntryOverlap;
    u2KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u2KeySize;
    pLeaf = TrieFirstMatch (pInputParams);
    if (NULL != pLeaf)
    {
        /* not an exact match... a first match return it */
        i4Ret = pLookupEntryOverlap (pInputParams, pOutputParams,
                                     pLeaf->pAppSpecPtr, u2KeySize, pLeaf->Key);
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return i4Ret;
    }

    /* No matching entry present. */
    *ppNode = NULL;
    Trie2GiveSem (u4Instance, TrieSemId);
    return TRIE_FAILURE;
}

INT4
TrieLookupOverlapLessMoreSpecific (tInputParams * pInputParams,
                                   VOID *pOutputParams, VOID **ppNode)
{
    /* pseudo code:
     * ~~~~~~~~~~~~
     * Get (FirstMatch)
     * if (FirstMatch)
     *   return FirstMatch;
     * else
     *   return NULL;
     */

    UINT2               u2KeySize;
    INT4                i4Ret;
    tLeafNode          *pLeaf = NULL;
    tOsixSemId          TrieSemId;

    INT4                (*pLookupEntryOverlap) (tInputParams * pInputParams1,
                                                VOID *pOutputParams1,
                                                VOID *AppSpecPtr1,
                                                UINT2 u2KeySize1, tKey Key);

    UINT4               u4Instance = 0;

    OsixSemTake (TRIE_SEM_ID);
    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        *ppNode = NULL;
        OsixSemGive (TRIE_SEM_ID);
        return TRIE_FAILURE;
    }
    OsixSemGive (TRIE_SEM_ID);
    TrieSemId = gaTrieInstance[u4Instance].SemId;
    Trie2TakeSem (u4Instance, TrieSemId);

    pLookupEntryOverlap =
        ((tRadixNodeHead *) pInputParams->pRoot)->AppFns->pLookupEntryOverlap;
    u2KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u2KeySize;
    pLeaf = TrieDoTraverseLessMoreSpecific (pInputParams);
    if (NULL != pLeaf)
    {
        /* not an exact match... a first match return it */
        i4Ret = pLookupEntryOverlap (pInputParams, pOutputParams,
                                     pLeaf->pAppSpecPtr, u2KeySize, pLeaf->Key);
        *ppNode = pLeaf;
        Trie2GiveSem (u4Instance, TrieSemId);
        return i4Ret;
    }

    /* No matching entry present. */
    *ppNode = NULL;
    Trie2GiveSem (u4Instance, TrieSemId);
    return TRIE_FAILURE;
}

/************************************************************************/
/*  Function Name   : Trie2TakeSem                                      */
/*  Description     : This function is used to acquire a semaphore.     */
/*  Input(s)        : The semaphore Id.                                   */
/*  Output(s)       : None                                              */
/*  Returns         : None                                    */
/************************************************************************/

VOID
Trie2TakeSem (UINT4 u4Inst, tOsixSemId TrieSemId)
{
    if (gaTrieInstance[u4Inst].bSemPerInst == OSIX_TRUE)
    {
        OsixSemTake (TrieSemId);
    }
}

/************************************************************************/
/*  Function Name   : Trie2GiveSem                                      */
/*  Description     : This function is used to release a semaphore.     */
/*  Input(s)        : The semaphore Id.                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                */
/************************************************************************/

VOID
Trie2GiveSem (UINT4 u4Inst, tOsixSemId TrieSemId)
{
    if (gaTrieInstance[u4Inst].bSemPerInst == OSIX_TRUE)
    {
        OsixSemGive (TrieSemId);
    }
}

/************************************************************************/
/*  Function Name   : Trie2DelSem                                       */
/*  Description     : This function is used to delete a semaphore.      */
/*  Input(s)        : The semaphore Id.                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                    */
/************************************************************************/

VOID
Trie2DelSem (UINT4 u4Inst, tOsixSemId TrieSemId)
{
    if (TrieSemId != NULL)
    {
        if (gaTrieInstance[u4Inst].bSemPerInst == OSIX_TRUE)
        {
            OsixSemDel (TrieSemId);
        }
    }
}
