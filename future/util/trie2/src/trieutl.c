/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trieutl.c,v 1.13 2017/12/26 13:34:30 siva Exp $
 *
 * Description: Internal utility routines.
 *
 *******************************************************************/

#include "trieinc.h"

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieGetInstance ()                                    */
 /*                                                                         */
 /*  Description   This function selects the instance of Trie from the      */
 /*                array pointed to by tTrieFamily. It may create a new     */
 /*                instance and the associated semaphore. This function     */
 /*                returns the pointer to the tRadixNodeHead of the         */
 /*                associated instance. It returns NULL, if it fails due    */
 /*                to any reasons.                                          */
 /*                                                                         */
 /*  Call          This function is called to select the matching           */
 /*  condition     instance of Trie.                                        */
 /*                                                                         */
 /*  Input(s)      u4Type -        Type of the Trie instance.               */
 /*                u2KeySize -     Size of the key.                         */
 /*                                                                         */
 /*  Output(s)     pu4Instance - The Trie instance                          */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieCrt ().                                              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        tRadixNodeHead * - Pointer to the tRadixNodeHead         */
 /*                                   structure.                            */
 /*                                                                         */
 /*                                                                         */
 /***************************************************************************/

tRadixNodeHead     *
TrieGetInstance (UINT4 u4Type, UINT2 u2KeySize, UINT4 *pu4Instance)
{
    UINT4               u4Instance;

    *pu4Instance = 0;
    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if ((gaTrieInstance[u4Instance].u4Type == u4Type) &&
            (gaTrieInstance[u4Instance].u2KeySize == u2KeySize))
        {
            *pu4Instance = u4Instance;
            return (&(gaTrieInstance[u4Instance]));
        }
    }

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (gaTrieInstance[u4Instance].u2KeySize == 0)
        {
            break;
        }
    }

    if (u4Instance == MAX_NUM_TRIE)
    {
        TrieError (MAX_INSTANCE_OVERFLOW);
        return ((tRadixNodeHead *) NULL);
    }
    *pu4Instance = u4Instance;

    return (&(gaTrieInstance[u4Instance]));
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieGetFreeInstance ()                                   */
 /*                                                                         */
 /*  Description   This function selects the instance of Trie from the      */
 /*                array pointed to by tTrieFamily. It may create a new     */
 /*                instance and the associated semaphore. This function     */
 /*                returns the pointer to the tRadixNodeHead of the         */
 /*                associated instance. It returns NULL, if it fails due    */
 /*                to any reasons.                                          */
 /*                                                                         */
 /*  Call          This function is called to select the matching           */
 /*  condition     instance of Trie.                                        */
 /*                                                                         */
 /*  Input(s)      None                                                     */
 /*                                                                         */
 /*  Output(s)     pu4Instance - The Trie instance                          */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieCrtInstance ().                                      */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        tRadixNodeHead * - Pointer to the tRadixNodeHead         */
 /*                                   structure.                            */
 /*                                                                         */
 /*                                                                         */
 /***************************************************************************/

tRadixNodeHead     *
TrieGetFreeInstance (UINT4 *pu4Instance)
{
    UINT4               u4Instance;

    *pu4Instance = 0;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (gaTrieInstance[u4Instance].u2KeySize == 0)
        {
            break;
        }
    }

    if (u4Instance == MAX_NUM_TRIE)
    {
        TrieError (MAX_INSTANCE_OVERFLOW);
        return ((tRadixNodeHead *) NULL);
    }
    *pu4Instance = u4Instance;

    return (&(gaTrieInstance[u4Instance]));
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      INT4 TrieDoTraverse ()                                     */
 /*                                                                         */
 /*  Description   This function traverse an instance of Trie pointed to    */
 /*                by the parameter pRoot. When it reaches the leaf node    */
 /*          or NULL, it returns TRIE_SUCCESS or TRIE_FAILURE respectively. */
 /*          In case of TRIE_FAILURE, it also returns the pointer to the    */
 /*           parent node. In case of TRIE_SUCCESS, it returns the pointer  */
 /*                to the leaf node.                                        */
 /*                                                                         */
 /*  Call          When the functions want to search for the key.           */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize      Size of the key.                          */
 /*                pRoot -        Pointer to the tRadixNode structure.      */
 /*                pKey -         Pointer to the key.                       */
 /*                                                                         */
 /*  Output(s)     pNode -        Pointer to the  chosen node.              */
 /*                                                                         */
 /*  Access   TrieAdd (), TrieRemove (),TrieLookup ()                       */
 /*  privileges    TrieSearch ().                                           */
 /*                                                                         */
 /*  Return        TRIE_SUCCESS - If the leaf node is found.                */
 /*                TRIE_FAILURE - Otherwise.                                */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieDoTraverse (UINT2 u2KeySize, tRadixNode * pRoot, tKey Key,
                tLeafNode ** ppNode)
{
    UINT1               u1ByteVal;
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pParent = NULL;
    pCurrNode = pParent = pRoot;
    while ((pCurrNode != NULL) && (pCurrNode->u1NodeType == RADIX_NODE))

    {
        pParent = pCurrNode;
        u1ByteVal = (UINT1) GETBYTE (Key, pCurrNode->u1ByteToTest, u2KeySize);
        pCurrNode = (BIT_TEST (u1ByteVal, pCurrNode->u1BitToTest)) ?
            (tRadixNode *) pCurrNode->pRight : (tRadixNode *) pCurrNode->pLeft;
    }

    /* fails in finding the nearest leaf node */
    if (pCurrNode == NULL)

    {
        *((tRadixNode **) ppNode) = pParent;
        return (TRIE_FAILURE);
    }
    *ppNode = (tLeafNode *) pCurrNode;
    return (TRIE_SUCCESS);
}

/****************************************************************************/
/*                                                                         */
/*  Function      INT4 TrieInsertLeaf (                                       */
/*                UINT2   u2KeySize,                                       */
/*                tInputParams     *pInputParams,                          */
/*                tTrieApp         *pAppSpecInfo,                          */
/*                tRadixNode       *pParentNode)                           */
/*                                                                         */
/*  Description   This function creates the leaf node. It copies from      */
/*                tInputParams to relevant fields of the leaf node. It     */
/*                also assigns the parent pointer of the leaf node to      */
/*                the pParentNode.                                         */
/*                                                                         */
/*  Call          When a leaf node is to be added.                         */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)      u2KeySize       Size of the key.                         */
/*                pInputParams -  Pointer to the tInputParams structure.   */
/*                pAppSpecInfo -  Pointer to the application specific      */
/*                                information.                             */
/*                pParentNode -   Pointer to the parent node.              */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*  Access        TrieAdd (), TrieInsertRadix ()                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful creation of the leaf node.  */
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieInsertLeaf (UINT2 u2KeySize, tInputParams * pInputParams,
                VOID *pAppSpecInfo, tRadixNode * pParentNode,
                VOID *pOutputParams, tLeafNode ** ppLeafNode)
{
    UINT1               u1ByteVal;
    tLeafNode          *pLeafNode = NULL;
    UINT4               u4Instance;

    INT4                (*pAddAppSpecInfo) (tInputParams * pInputParams1,
                                            VOID *pOutputParams1,
                                            VOID **ppAppPtr,
                                            VOID *pAppSpecInfo1);

    pAddAppSpecInfo = pInputParams->pRoot->AppFns->pAddAppSpecInfo;

    if (u2KeySize * 8 < pInputParams->u1PrefixLen)
        return TRIE_FAILURE;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (&(gaTrieInstance[u4Instance]) ==
            (tRadixNodeHead *) pInputParams->pRoot)
        {
            break;
        }
    }
    if (u4Instance == MAX_NUM_TRIE)
    {
        /* There's something wrong */
        return (TRIE_FAILURE);
    }

    pLeafNode =
        (tLeafNode *) TrieAllocateateNode (u2KeySize, LEAF_NODE, u4Instance);
    if (pLeafNode == NULL)
    {
        return (TRIE_FAILURE);
    }

    {
        INT1                i1Ctr = (INT1) (pInputParams->u1PrefixLen / 8);
        INT4                i4Idx = 0;

        if (u2KeySize > U4_SIZE)
        {
            MEMSET (pLeafNode->Mask.pKey, 0, u2KeySize);
            while (i1Ctr--)
            {
                pLeafNode->Mask.pKey[i4Idx++] = 0xff;
            }
            i1Ctr = (INT1) (pInputParams->u1PrefixLen % 8);
            while (i1Ctr--)
            {
                pLeafNode->Mask.pKey[i4Idx] =
                    (pLeafNode->Mask.pKey[i4Idx] | (UINT1) (1 << (7 - i1Ctr)));
            }
        }
        else
        {
            pLeafNode->Mask.u4Key = 0;
            while (i1Ctr--)
            {
                pLeafNode->Mask.u4Key =
                    (UINT4) (pLeafNode->Mask.
                             u4Key | (UINT4) (1 << (31 - i1Ctr)));
            }
        }
    }

    KEYCOPY (pLeafNode->Key, pInputParams->Key, u2KeySize);

    /* call app specific add function to add this
       pLeafNode->pAppSpecInfo = pAppSpecInfo; */

    pLeafNode->pAppSpecPtr = NULL;    /* Initialising *** */
    if (TRIE_FAILURE == (pAddAppSpecInfo) (pInputParams, pOutputParams,
                                           &pLeafNode->pAppSpecPtr,
                                           pAppSpecInfo))
    {
        TrieReleaseNode (LEAF_NODE, u4Instance, (UINT1 *) pLeafNode);
        return TRIE_FAILURE;
    }

    /* pointer updation */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))
    {
        pParentNode->pRight = (void *) pLeafNode;
    }
    else
    {
        pParentNode->pLeft = (void *) pLeafNode;
    }
    pLeafNode->pParent = pParentNode;

    *ppLeafNode = pLeafNode;

    return (TRIE_SUCCESS);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieInsertRadix ()                                          */
 /*                                                                         */
 /*  Description   This function creates the radix node. It modifies the    */
 /*                pointers to reflect this change. It also creates a       */
 /*                leaf node by calling another function. If it fails due   */
 /*                to resource problems, it returns TRIE_FAILURE.           */
 /*                                                                         */
 /*  Call          When a radix node needs to be added.                     */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize       Size of the key.                         */
 /*                u1BitToTest -   Bit mask of the selected byte.           */
 /*                u1ByteToTest -  Selected Byte.                           */
 /*                pInputParams -  Pointer to the tInputParams structure.   */
 /*                pAppSpecInfo -  Pointer to the application specific      */
 /*                                information.                             */
 /*                pParentNode -   Pointer to the parent node.              */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieAdd ().                                              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        TRIE_SUCCESS - If successful creation of the radix node. */
 /*                TRIE_FAILURE - Otherwise.                                */
 /*                                                                         */
 /***************************************************************************/
INT4
TrieInsertRadix (UINT2 u2KeySize, UINT1 u1BitToTest, UINT1 u1ByteToTest,
                 tInputParams * pInputParams, VOID *pAppSpecInfo,
                 tRadixNode * pParentNode, VOID *pOutputParams)
{
    tRadixNode         *pRadixNode = NULL;
    void               *pTmpNode = NULL;
    tLeafNode          *pNewLeaf = NULL;
    tLeafNode          *pPrevLeaf = NULL;
    UINT1               u1ByteVal;
    INT4                i4Cmp;
    UINT4               u4Instance;

    if (TrieGetTrieInstance (pInputParams->pRoot, &u4Instance) == TRIE_FAILURE)
    {
        return TRIE_FAILURE;
    }

    pRadixNode =
        (tRadixNode *) TrieAllocateateNode (u2KeySize, RADIX_NODE, u4Instance);
    if (pRadixNode == NULL)
    {
        return TRIE_FAILURE;
    }
    pRadixNode->u1ByteToTest = u1ByteToTest;
    pRadixNode->u1BitToTest = u1BitToTest;

    /* add the leaf node */
    if ((TrieInsertLeaf (u2KeySize, pInputParams, pAppSpecInfo, pRadixNode,
                         pOutputParams, &pNewLeaf)) == TRIE_FAILURE)
    {
        if (u2KeySize > U4_SIZE)
        {
            TrieReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pRadixNode->Mask.pKey);
        }

        TrieReleaseNode (RADIX_NODE, u4Instance, (UINT1 *) pRadixNode);
        return (TRIE_FAILURE);
    }

    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))
    {
        pTmpNode = pParentNode->pRight;
        pParentNode->pRight = pRadixNode;
    }
    else
    {
        pTmpNode = pParentNode->pLeft;
        pParentNode->pLeft = pRadixNode;
    }
    pRadixNode->pParent = pParentNode;

    /* update the pointers in the child of new radix node */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pRadixNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pRadixNode->u1BitToTest))
    {
        pRadixNode->pLeft = pTmpNode;
    }
    else
    {
        pRadixNode->pRight = pTmpNode;
    }

    i4Cmp = KEYCMP (((tRadixNode *) pRadixNode->pLeft)->Mask,
                    ((tRadixNode *) pRadixNode->pRight)->Mask, u2KeySize);

    if (i4Cmp < 0)
    {
        KEYCOPY (pRadixNode->Mask, ((tRadixNode *) pRadixNode->pLeft)->Mask,
                 u2KeySize);
    }
    else
    {
        KEYCOPY (pRadixNode->Mask, ((tRadixNode *) pRadixNode->pRight)->Mask,
                 u2KeySize);
    }

    ((tRadixNode *) pTmpNode)->pParent = pRadixNode;

    /* update the mask 
     * Here TrieSetMask () is supposed to be called with u2KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieSetMask (u2KeySize, pRadixNode->pParent);

    if (NULL == (pPrevLeaf = TrieGetPrevLeafNode (pNewLeaf)))
    {
        /* means we are the left most node of the trie */
        DLIST_INSERT_HEAD (((tRadixNodeHead *) pInputParams->pRoot), NodeHead,
                           pNewLeaf, NodeLink);
    }
    else
    {
        DLIST_INSERT (((tRadixNodeHead *) pInputParams->pRoot), NodeHead,
                      pNewLeaf, NodeLink, pPrevLeaf);
    }

    return (TRIE_SUCCESS);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDelRadixLeaf ()                                      */
 /*                                                                         */
 /*  Description   This function deletes the leaf node and the associated   */
 /*                radix node if any, pointed to by the parameters. it      */
 /*                copies the deleted leaf node information in the          */
 /*                relevant fields of tOutputParams structure.              */
 /*                                                                         */
 /*  Call          When the only residing application in the leaf node      */
 /*  condition     is also deleted.                                         */
 /*                                                                         */
 /*  Input(s)      u2KeySize -      Size of the key.                        */
 /*                pLeafNode -      Pointer to the tLeafNode to be          */
 /*                                 deleted.                                */
 /*                                                                         */
 /*  Output(s)     pOutputParams -  Pointer to the tOutputParams            */
 /*                                 structure.                              */
 /*                                                                         */
 /*  Access        TrieRemove (), TrieDel ().                               */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/
VOID
TrieDelRadixLeaf (tRadixNodeHead * pRoot, tLeafNode * pLeafNode,
                  UINT4 u4Instance)
{
    tRadixNode         *pTmpParent = NULL;
    tRadixNode         *pPredNode = NULL;
    tRadixNode         *pOtherTree = NULL;
    tLeafNode          *pPrevLeaf = NULL;
    UINT2               u2KeySize = pRoot->u2KeySize;

    if (NULL == (pPrevLeaf = TrieGetPrevLeafNode (pLeafNode)))
    {
        /* means we are the left most node of the trie */
        DLIST_REMOVE_HEAD (pRoot, NodeHead, NodeLink);
    }
    else
    {
        DLIST_REMOVE (pRoot, NodeHead, pLeafNode, NodeLink, pPrevLeaf);
    }

    pTmpParent = pLeafNode->pParent;
    if (pTmpParent->pRight == pLeafNode)
    {
        pTmpParent->pRight = NULL;
        pOtherTree = pTmpParent->pLeft;
    }
    else
    {
        pTmpParent->pLeft = NULL;
        pOtherTree = pTmpParent->pRight;
    }

    /* free the memory of the key, if allocated */
    if (u2KeySize > U4_SIZE)
    {
        TrieReleaseNode (KEY_POOL_ID, u4Instance,
                         (UINT1 *) pLeafNode->Key.pKey);
        TrieReleaseNode (KEY_POOL_ID, u4Instance,
                         (UINT1 *) pLeafNode->Mask.pKey);
    }

    /* free the leaf node */
    TrieReleaseNode (LEAF_NODE, u4Instance, (UINT1 *) pLeafNode);

    /* free the parent node except root */
    pPredNode = pTmpParent->pParent;

    if (pPredNode != NULL)
    {
        /* update the pointers */
        if (pPredNode->pRight == pTmpParent)
        {
            pPredNode->pRight = pOtherTree;
        }
        else
        {
            pPredNode->pLeft = pOtherTree;
        }
        pOtherTree->pParent = pPredNode;

        /* free the parent radix node */

        if (u2KeySize > U4_SIZE)
        {
            TrieReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pTmpParent->Mask.pKey);
        }

        TrieReleaseNode (RADIX_NODE, u4Instance, (UINT1 *) pTmpParent);
        pTmpParent = pPredNode;
    }

    /* update the mask 
     * Here TrieSetMask () is supposed to be called with u2KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieSetMask (u2KeySize, (void *) pTmpParent);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieSetMask ()                                        */
 /*                                                                         */
 /*  Description   This function assigns mask irrespective of the node      */
 /*                type. In every assignment of the mask, it selects the    */
 /*                mask with the least number of leading ones.              */
 /*                                                                         */
 /*  Call          When mask needs to be reassigned.                        */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -  Size of the key.                            */
 /*                pNode -      Pointer to the node whose mask needs to     */
 /*                             be reassigned.                              */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        TrieAdd (), TrieDelAppAndUpdateLeaf (),                  */
 /*  privileges    TrieDelRadixLeaf ().                                     */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

void
TrieSetMask (UINT2 u2KeySize, void *pNode)
{
    INT4                i4Cmp;
    INT4                i4Eql;
    tKey                PrvMask;
    tRadixNode         *pRadix = NULL;

    pRadix = (tRadixNode *) pNode;
    while (pRadix != NULL)
    {
        PrvMask = pRadix->Mask;
        if ((pRadix->pRight != NULL) && (pRadix->pLeft != NULL))
        {
            i4Cmp = KEYCMP (((tRadixNode *) pRadix->pRight)->Mask,
                            ((tRadixNode *) pRadix->pLeft)->Mask, u2KeySize);

            if (i4Cmp < 0)
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pRight)->Mask,
                         u2KeySize);
            }
            else
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pLeft)->Mask,
                         u2KeySize);
            }
        }
        else
        {
            if ((pRadix->pRight == NULL) && (pRadix->pLeft == NULL))
            {
                break;
            }

            if (pRadix->pRight == NULL)
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pLeft)->Mask,
                         u2KeySize);
            }
            else
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pRight)->Mask,
                         u2KeySize);
            }
        }
        i4Eql = KEYEQUAL (pRadix->Mask, PrvMask, u2KeySize);
        if (i4Eql == 0)
        {
            break;
        }
        pRadix = pRadix->pParent;
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieGetBestMatch ()                                     */
 /*                                                                         */
 /*  Description   This functions starts to find the best match with the    */
 /*                given node and in the worst case, goes to the root. If   */
 /*                this operation is successful, it returns TRIE_SUCCESS. In */
 /*                case of successful return, it also copies relevant       */
 /*                fields in the tOutputParams structure.                   */
 /*                                                                         */
 /*  Call          When best match needs to be found.                       */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -       Size of the key.                       */
 /*                pInputParams -    Pointer to the tInputParams            */
 /*                                  structure.                             */
 /*                pNode -           Pointer to the  node from where        */
 /*                                  search  for the best match begins.     */
 /*                                                                         */
 /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
 /*                                  structure.                             */
 /*                                                                         */
 /*  Access        TrieLookup ().                                           */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        TRIE_SUCCESS  - If it is able to  find  the best match.  */
 /*                TRIE_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieGetBestMatch (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode,
                  VOID *pOutputParams)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tKey                LeafKey;
    UINT1              *pTmpKey = NULL;
    UINT1              *pau1Mask = NULL;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;

    UINT4               u4TmpNumBytes;
    UINT1               au1Key[MAX_KEY_SIZE];
    UINT1               au1PrvKey[MAX_KEY_SIZE];

    INT4                (*pTrieBestMatch) (UINT2 u2KeySize,
                                           tInputParams * pInputParams,
                                           VOID *pOutputParams, VOID *pAppPtr,
                                           tKey Key);
    pTrieBestMatch = pInputParams->pRoot->AppFns->pBestMatch;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
        LeafKey.pKey = &(au1PrvKey[0]);

        /* a temporary solution once mask size is more than 
         *  4 byte shouls not be problem
         */
        pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
        /* here assumption is IP address is of 4 byte (U4_SIZE) 
         * this logic can be made more efficient by diretly ANDING with
         * 4 byte mask only. Here it is byte by byte.
         */
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)

        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
            LeafKey.pKey[u4TmpNumBytes] =
                (pLeaf->Key).pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        if (memcmp (LeafKey.pKey, CurrKey.pKey, u2KeySize) == 0)

        {
            if (TRIE_SUCCESS == pTrieBestMatch (u2KeySize, pInputParams,
                                                pOutputParams,
                                                pLeaf->pAppSpecPtr, pLeaf->Key))
                return TRIE_SUCCESS;
        }

        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
    }

    PrvKey.pKey = &(au1PrvKey[0]);
    pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

    au1Key[0] = (UINT1) (~(au1PrvKey[0] & pau1Mask[0]));
    while (pCurrNode != NULL)
    {
        pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

        /* change the arrays to reflect correct value */
        pTmpKey = CurrKey.pKey;
        CurrKey.pKey = PrvKey.pKey;
        PrvKey.pKey = pTmpKey;
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        /* ideally upper limit should be 2*U4_SIZE, but here both are same
         * (u2KeySize and 2*U4_SIZE)
         */
        for (u4TmpNumBytes = u2KeySize; u4TmpNumBytes < u2KeySize;
             u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] = pau1Mask[u4TmpNumBytes - u2KeySize];
        }

        /* to prevent reduntant lookup */
        if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) != 0)
        {
            if (TrieDoTraverse (u2KeySize, pCurrNode, CurrKey,
                                &pLeaf) == TRIE_SUCCESS)
            {
                pau1Mask = (UINT1 *) pLeaf->Mask.pKey;

                for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize;
                     u4TmpNumBytes++)
                {
                    PrvKey.pKey[u4TmpNumBytes] =
                        pLeaf->Key.
                        pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                    CurrKey.pKey[u4TmpNumBytes] =
                        InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                }

                /* if keys match */
                if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) == 0)
                {
                    if (TRIE_SUCCESS == pTrieBestMatch (u2KeySize, pInputParams,
                                                        pOutputParams,
                                                        pLeaf->pAppSpecPtr,
                                                        pLeaf->Key))
                        return TRIE_SUCCESS;
                }
            }
            else
            {
                return (TRIE_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (TRIE_FAILURE);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieGetBestMatch4 ()                                    */
 /*                                                                         */
 /*  Description   This functions starts to find the best match with the    */
 /*                given node and in the worst case, goes to the root. If   */
 /*                this operation is successful, it returns TRIE_SUCCESS. In */
 /*                case of successful return, it also copies relevant       */
 /*                fields in the tOutputParams structure. This function     */
 /*                handles only 4 byte key.                                 */
 /*                                                                         */
 /*  Call          When best match needs to be found.                       */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -       Size of the key.                       */
 /*                pInputParams -    Pointer to the tInputParams            */
 /*                                  structure.                             */
 /*                pNode -           Pointer to the  node from where        */
 /*                                  search  for the best match begins.     */
 /*                                                                         */
 /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
 /*                                  structure.                             */
 /*                                                                         */
 /*  Access        TrieLookup ().                                           */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        TRIE_SUCCESS  - If it is able to  find  the best match.  */
 /*                TRIE_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieGetBestMatch4 (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode,
                   VOID *pOutputParams)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;
    INT4                (*pTrieBestMatch4) (UINT2 u2KeySize,
                                            tInputParams * pInputParams,
                                            VOID *pOutputParams, VOID *pAppPtr,
                                            tKey Key);

    pTrieBestMatch4 = pInputParams->pRoot->AppFns->pBestMatch;

    InKey = pInputParams->Key;
    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.u4Key = InKey.u4Key & (pLeaf->Mask).u4Key;
        if (CurrKey.u4Key == ((pLeaf->Key).u4Key & (pLeaf->Mask).u4Key))

        {
            if (TRIE_SUCCESS == pTrieBestMatch4 (u2KeySize, pInputParams,
                                                 pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
                return TRIE_SUCCESS;
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
    }

    CurrKey.u4Key = ~(InKey.u4Key & (pCurrNode->Mask).u4Key);
    while (pCurrNode != NULL)
    {
        /* change the arrays to reflect correct value */
        PrvKey.u4Key = CurrKey.u4Key;
        CurrKey.u4Key = InKey.u4Key & (pCurrNode->Mask).u4Key;
        if (CurrKey.u4Key != PrvKey.u4Key)
        {
            if (TrieDoTraverse (u2KeySize, pCurrNode, CurrKey, &(pLeaf))
                == TRIE_SUCCESS)
            {
                if (CurrKey.u4Key == (pLeaf->Key.u4Key & pLeaf->Mask.u4Key))
                {
                    if (TRIE_SUCCESS == pTrieBestMatch4 (u2KeySize,
                                                         pInputParams,
                                                         pOutputParams,
                                                         pLeaf->pAppSpecPtr,
                                                         pLeaf->Key))
                    {
                        return TRIE_SUCCESS;
                    }
                }
            }
            else
            {
                return (TRIE_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (TRIE_FAILURE);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieAllocateateNode ()                                   */
 /*                                                                         */
 /*  Description   This function allocates the memory for the node as       */
 /*                requested by the u1NodeType parameter. It also           */
 /*                initialises the fields of the structure. If it fails     */
 /*                in allocating memory, it returns TRIE_FAILURE. In case of */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create either the leaf node   */
 /*  condition     or the radix node.                                       */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u1NodeType -    Indicates the type of the node.          */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        TrieInsertLeaf (), BgpTrieInsertRadix ().                */
 /*  privileges    TrieGetInstance ().                                      */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                TRIE_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieAllocateateNode (UINT2 u2KeySize, UINT1 u1NodeType, UINT4 u4Instance)
{
    VOID               *pTrieNode = NULL;

    if (u1NodeType == RADIX_NODE)
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_FALSE)
        {
            /* Allocating radix node memory from Common
             * Radix memory pool */
            OsixSemTake (TRIE_RADIX_SEM_ID);
            OsixSemTake (TRIE_KEY_SEM_ID);
            pTrieNode = TrieAllocateRadixNode (u2KeySize, u4Instance);
            OsixSemGive (TRIE_KEY_SEM_ID);
            OsixSemGive (TRIE_RADIX_SEM_ID);
            return (pTrieNode);
        }
        else
        {
            /* Allocating radix node memory from 
             * Radix memory pool which is created for 
             * this particular instance */
            return (TrieAllocateRadixNode (u2KeySize, u4Instance));
        }
    }
    else
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_FALSE)
        {
            /* Allocating leaf node memory from Common
             * Leaf memory pool */
            OsixSemTake (TRIE_LEAF_SEM_ID);
            OsixSemTake (TRIE_KEY_SEM_ID);
            pTrieNode = TrieAllocateLeafNode (u2KeySize, u4Instance);
            OsixSemGive (TRIE_KEY_SEM_ID);
            OsixSemGive (TRIE_LEAF_SEM_ID);
            return (pTrieNode);
        }
        else
        {
            /* Allocating leaf node memory from 
             * Leaf memory pool which is created for 
             * this particular instance */
            return (TrieAllocateLeafNode (u2KeySize, u4Instance));
        }
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieAllocateRadixNode ()                                 */
 /*                                                                         */
 /*  Description   This function allocates the memory for radix node and    */
 /*                keyId and initialises the fields of the structure. If it */
 /*                fails in allocating memory, it returns NULL. In case of  */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create the radix node         */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u4Instance -    Identifier of Trie instance.             */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                NULL -  Otherwise.                                       */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieAllocateRadixNode (UINT2 u2KeySize, UINT4 u4Instance)
{

    tRadixNode         *pRadix = NULL;
    UINT4               u4rc;

    pRadix = (tRadixNode *) MemAllocMemBlk (gai4RadixPool[u4Instance]);
    if (pRadix == NULL)
    {
        TrieError (RADIX_ALLOC_FAIL);
        return ((void *) NULL);
    }
    if (u2KeySize > U4_SIZE)
    {
        u4rc = MemAllocateMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance],
                                    (UINT1 **) &(pRadix->Mask.pKey));
        if (u4rc == MEM_FAILURE)
        {
            MemReleaseMemBlock (gai4RadixPool[u4Instance], (UINT1 *) pRadix);
            TrieError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }
    }
    pRadix->u1NodeType = RADIX_NODE;
    return ((void *) pRadix);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieAllocateLeafNode ()                                  */
 /*                                                                         */
 /*  Description   This function allocates the memory for leaf node and     */
 /*                keyId and initialises the fields of the structure. If it */
 /*                fails in allocating memory, it returns NULL. In case of  */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create the radix node         */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u4Instance -    Identifier of Trie instance.             */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                NULL -  Otherwise.                                       */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieAllocateLeafNode (UINT2 u2KeySize, UINT4 u4Instance)
{
    tLeafNode          *pLeaf = NULL;
    UINT4               u4rc;

    pLeaf =
        (tLeafNode *) MemAllocMemBlk ((tMemPoolId) gai4LeafPool[u4Instance]);
    if (pLeaf == NULL)
    {
        TrieError (LEAF_ALLOC_FAIL);
        return ((void *) NULL);
    }
    if (u2KeySize > U4_SIZE)
    {
        u4rc = MemAllocateMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance],
                                    (UINT1 **) &(pLeaf->Key.pKey));
        if (u4rc == MEM_FAILURE)
        {
            MemReleaseMemBlock ((tMemPoolId) gai4LeafPool[u4Instance],
                                (UINT1 *) pLeaf);
            TrieError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }

        u4rc = MemAllocateMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance],
                                    (UINT1 **) &(pLeaf->Mask.pKey));
        if (u4rc == MEM_FAILURE)
        {
            MemReleaseMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance],
                                (UINT1 *) pLeaf->Key.pKey);
            MemReleaseMemBlock ((tMemPoolId) gai4LeafPool[u4Instance],
                                (UINT1 *) pLeaf);
            TrieError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }
    }

    pLeaf->u1NodeType = LEAF_NODE;
    return ((void *) pLeaf);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieReleaseNode ()                                       */
 /*                                                                         */
 /*  Description   This function releases the memory of the incoming trie   */
 /*                node to corresponding mem pool as per u1NodeType.        */
 /*                                                                         */
 /*  Input(s)      u1NodeType - Indicates the type of the node.             */
 /*                u4Instance - Identifier of trie instance                 */
 /*                pNode      - Reference to the trie node which            */
 /*                              is to be released                          */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

VOID
TrieReleaseNode (UINT1 u1NodeType, UINT4 u4Instance, UINT1 *pNode)
{

    if (u1NodeType == RADIX_NODE)
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_FALSE)
        {
            /* Releasing radix node memory to Common
             * Radix memory pool */
            OsixSemTake (TRIE_RADIX_SEM_ID);
            MemReleaseMemBlock (gai4RadixPool[u4Instance], pNode);
            OsixSemGive (TRIE_RADIX_SEM_ID);
            return;
        }
        else
        {
            /* Releasing radix node memory to  Radix memory pool
             * which is created for this particular instance */

            MemReleaseMemBlock (gai4RadixPool[u4Instance], pNode);
            return;
        }
    }
    else if (u1NodeType == LEAF_NODE)
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_FALSE)
        {
            /* Releasing leaf node memory to Common
             * Leaf memory pool */
            OsixSemTake (TRIE_LEAF_SEM_ID);
            MemReleaseMemBlock ((tMemPoolId) gai4LeafPool[u4Instance], pNode);
            OsixSemGive (TRIE_LEAF_SEM_ID);
            return;
        }
        else
        {
            /* Releasing leaf node memory to  leaf memory pool
             * which is created for this particular instance */
            MemReleaseMemBlock ((tMemPoolId) gai4LeafPool[u4Instance], pNode);
            return;
        }

    }
    else
    {
        if (gaTrieInstance[u4Instance].bPoolPerInst == OSIX_FALSE)
        {
            /* Releasing memory of key to Common
             * Key memory pool */
            OsixSemTake (TRIE_KEY_SEM_ID);
            MemReleaseMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance], pNode);
            OsixSemGive (TRIE_KEY_SEM_ID);
            return;
        }
        else
        {
            /* Releasing memory of key to  key memory pool
             * which is created for this particular instance */
            MemReleaseMemBlock ((tMemPoolId) gi4KeyPoolIdx[u4Instance], pNode);
            return;
        }
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      void TrieError ()                                  */
 /*                                                                         */
 /*  Description   This function reports the error encountered by the       */
 /*                different functions. As per the u1NumError field, it     */
 /*                sends error message to the standard error device.        */
 /*                                                                         */
 /*  Call          When calling function encounters errors.                 */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u1NumError -     Error number                            */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        Higher level functions of the Trie library.              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

void
TrieError (UINT1 u1NumError)
{
    UINT1               u1StrLength;
    CHR1                buf[100];
    const CHR1         *aErr[] =
        { "Memory allocation failure for the leaf node.",
        "Memory allocation failure for the radix node.",
        "Memory allocation failure for the key.",
        "Key size can not be accommodated.",
        "Application id can not be accommodated.",
        "Number of instances are more than the maximum value.",
        "Semaphore creation failure.",
        "Application id is already taken before.",
        "Memory allocation failure."
    };
    u1StrLength = (UINT1) STRLEN (aErr[u1NumError]);
    if (u1StrLength > 99)
    {
        u1StrLength = 99;
    }
    SNPRINTF (buf, u1StrLength, "TRIE - %s\n", aErr[u1NumError]);
    buf[u1StrLength] = 0;

    UtlTrcPrint ((const CHR1 *) buf);
}

tLeafNode          *
TrieGetPrevLeafNode (tLeafNode * pLeaf)
{
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pParent = NULL;

    /* we go up the trie once and come down ... */
    pCurrNode = (tRadixNode *) pLeaf;
    pParent = pLeaf->pParent;

    /* UP on the left */
    while (pParent != NULL)
    {
        if ((pParent->pLeft == pCurrNode) || (NULL == pParent->pLeft))
        {
            pCurrNode = pParent;
            pParent = pParent->pParent;
        }
        else
            break;
    }

    if (NULL == pParent)
        return NULL;            /* the left most node was given !! */

    /* switch to the leftie */
    pCurrNode = pParent->pLeft;

    /* and come DOWN on the right */
    while (RADIX_NODE == pCurrNode->u1NodeType)
    {
        pCurrNode =
            (NULL == pCurrNode->pRight) ? pCurrNode->pLeft : pCurrNode->pRight;
    }
    return (tLeafNode *) pCurrNode;
}

#define OTHER_NODE(x)                      \
           ((((tRadixNode*)((tLeafNode*)(x))->pParent)->pLeft == (void*)(x))?    \
            ((tRadixNode*)((tLeafNode*)(x))->pParent)->pRight :                \
            ((tRadixNode*)((tLeafNode*)(x))->pParent)->pLeft                    \
           )
#define TRIE_OVERLAP_LESS      1
#define TRIE_OVERLAP_MORE      2

INT4
TrieOverlapMatch (tKey Key, UINT2 u2KeySize, tLeafNode * pNode,
                  UINT1 *pu1OvlType)
{
    UINT2               u2InPrefixLen, u2NodePrefixLen, u2Len;

    u2InPrefixLen = TriePrefixLen (Key, u2KeySize);
    u2NodePrefixLen = TriePrefixLen (pNode->Key, u2KeySize);

    if (u2NodePrefixLen > u2InPrefixLen)
    {
        *pu1OvlType = TRIE_OVERLAP_MORE;
        u2Len = u2InPrefixLen;
    }
    else if (u2NodePrefixLen < u2InPrefixLen)
    {
        *pu1OvlType = TRIE_OVERLAP_LESS;
        u2Len = u2NodePrefixLen;
    }
    else
    {
        /* exact match not needed !! */
        return 0;
    }

    if (TrieKeyMatches (Key, pNode->Key, u2Len, u2KeySize))
    {
        return 1;
    }
    return 0;
}

INT4
TrieDefaultRoute (tKey Key, UINT2 u2KeySize)
{

    if (u2KeySize <= sizeof (UINT4))
    {
        if (Key.u4Key == 0)
            return 1;
    }
    else
    {
        while (u2KeySize--)
        {
            if (Key.pKey[u2KeySize])
            {
                return 0;
            }
            return 1;
        }
    }
    return 0;
}

INT4
TrieKeyMatches (tKey Key1, tKey Key2, UINT2 u2PrefixLen, UINT2 u2KeySize)
{
    UINT4               u4Mask = (UINT4) ~0;
    UINT1               u1Mask = (UINT1) ~0;
    UINT2               u2PrefixBytes;

    if (u2KeySize <= sizeof (UINT4))
    {
        u2PrefixLen = (UINT2) (u2PrefixLen ? (32 - u2PrefixLen) : 32);
        while (u2PrefixLen--)
        {
            u4Mask <<= 1;
        }

        if ((Key1.u4Key & u4Mask) == (Key2.u4Key & u4Mask))
        {
            return 1;
        }
    }
    else
    {
        u2PrefixBytes = (UINT2) ((u2PrefixLen) >> 3);    /* bytes */
        u2PrefixLen = u2PrefixLen & (0x7);    /* bits */

        u2PrefixLen = (UINT2) (u2PrefixLen ? (8 - u2PrefixLen) : 8);
        while (u2PrefixLen--)
            u1Mask = (UINT1) (u1Mask << 1);

        if ((Key1.pKey[u2PrefixBytes] & u1Mask) == (Key2.pKey[u2PrefixBytes] &
                                                    u1Mask))
        {
            while (u2PrefixBytes--)
            {
                if (Key1.pKey[u2PrefixBytes] != Key2.pKey[u2PrefixBytes])
                    return 0;
            }
            return 1;
        }
    }

    return 0;
}

const UINT1         TrieBitsSetTable256[] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

UINT2
TriePrefixLen (tKey Key, UINT2 u2KeySize)
{
    UINT2               u2Count = 0;
    UINT4               u2MaskOff;

    if (u2KeySize <= U4_SIZE)
    {
        u2Count = (UINT2) (u2Count +
                           TrieBitsSetTable256[(UINT1)
                                               (Key.
                                                u4Key & (UINT4) 0x000000ff)]);
        if (u2KeySize == U4_SIZE)
        {
            u2Count = (UINT2) (u2Count +
                               TrieBitsSetTable256[(UINT1)
                                                   ((Key.
                                                     u4Key & (UINT4) 0x0000ff00)
                                                    >> 8)]);
        }
    }
    else
    {
        u2MaskOff = u2KeySize >> 1;
        u2KeySize = 0;
        while ((u2KeySize < u2MaskOff) && Key.pKey[u2KeySize + u2MaskOff])
        {
            u2Count =
                (UINT2) (u2Count +
                         TrieBitsSetTable256[Key.pKey[u2MaskOff + u2KeySize]]);
            u2KeySize++;
        }
    }

    return u2Count;
}

#define SET_LASTBESTMATCH() do {\
           if (TrieOverlapMatch (pInputParams->Key, u2KeySize, pLeaf, &u1OvlType))\
               pLastBestMatch = pLeaf;                    \
           else \
               return NULL;\
    } while (0)

tLeafNode          *
TrieFindFirstMatch (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tKey                LeafKey;
    UINT1              *pTmpKey = NULL;
    UINT1              *pau1Mask = NULL;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;
    tLeafNode          *pLastBestMatch = NULL;
    UINT4               u4TmpNumBytes;
    UINT1               au1Key[MAX_KEY_SIZE];
    UINT1               au1PrvKey[MAX_KEY_SIZE];
    UINT1               u1OvlType;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)

    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
        LeafKey.pKey = &(au1PrvKey[0]);

        /* a temporary solution once mask size is more than 
         *  4 byte shouls not be problem
         */
        pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
        /* here assumption is IP address is of 4 byte (U4_SIZE) 
         * this logic can be made more efficient by diretly ANDING with
         * 4 byte mask only. Here it is byte by byte.
         */
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
            LeafKey.pKey[u4TmpNumBytes] =
                (pLeaf->Key).pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        if (memcmp (LeafKey.pKey, CurrKey.pKey, u2KeySize) == 0)
        {
            SET_LASTBESTMATCH ();
        }

        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
    }

    PrvKey.pKey = &(au1PrvKey[0]);
    pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

    au1Key[0] = (UINT1) (~(au1PrvKey[0] & pau1Mask[0]));
    while (pCurrNode != NULL)
    {
        pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

        /* change the arrays to reflect correct value */
        pTmpKey = CurrKey.pKey;
        CurrKey.pKey = PrvKey.pKey;
        PrvKey.pKey = pTmpKey;
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        /* ideally upper limit should be 2*U4_SIZE, but here both are same
         * (u2KeySize and 2*U4_SIZE)
         */
        for (u4TmpNumBytes = u2KeySize; u4TmpNumBytes < u2KeySize;
             u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] = pau1Mask[u4TmpNumBytes - u2KeySize];
        }

        /* to prevent reduntant lookup */
        if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) != 0)
        {
            if (TrieDoTraverse (u2KeySize, pCurrNode, CurrKey, &pLeaf)
                == TRIE_SUCCESS)
            {

                pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
                for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize;
                     u4TmpNumBytes++)
                {
                    PrvKey.pKey[u4TmpNumBytes] =
                        pLeaf->Key.
                        pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                    CurrKey.pKey[u4TmpNumBytes] =
                        InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                }
                SET_LASTBESTMATCH ();
            }
            else
                return pLastBestMatch;
        }

        pCurrNode = pCurrNode->pParent;
    }

    return pLastBestMatch;
}

tLeafNode          *
TrieFindFirstMatch4 (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;
    tLeafNode          *pLastBestMatch = NULL;
    UINT1               u1OvlType;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.u4Key = InKey.u4Key & (pLeaf->Mask).u4Key;
        if (CurrKey.u4Key == ((pLeaf->Key).u4Key & (pLeaf->Mask).u4Key))
        {
            SET_LASTBESTMATCH ();
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
        pCurrNode = (tRadixNode *) pNode;

    CurrKey.u4Key = ~(InKey.u4Key & (pCurrNode->Mask).u4Key);
    while (pCurrNode != NULL)
    {
        /* change the arrays to reflect correct value */
        PrvKey.u4Key = CurrKey.u4Key;
        CurrKey.u4Key = InKey.u4Key & (pCurrNode->Mask).u4Key;

        if (CurrKey.u4Key != PrvKey.u4Key)
        {
            if (TrieDoTraverse (u2KeySize, pCurrNode, CurrKey, &pLeaf)
                == TRIE_SUCCESS)
            {

                if (CurrKey.u4Key == (pLeaf->Key.u4Key & pLeaf->Mask.u4Key))
                    SET_LASTBESTMATCH ();
            }
            else
                return pLastBestMatch;
        }
        pCurrNode = pCurrNode->pParent;
    }

    return pLastBestMatch;
}

tLeafNode          *
TrieFirstMatch (tInputParams * pInputParams)
{
    UINT2               u2KeySize;
    INT4                i4Return;
    tRadixNode         *pRadix = NULL;
    tLeafNode          *pLeaf = NULL;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
        return NULL;            /* empty trie */

    i4Return = TrieDoTraverse (u2KeySize, pRadix, pInputParams->Key, &(pLeaf));
    if (i4Return == TRIE_FAILURE)
    {
        return NULL;
    }

    /* else continue !! */
    if (u2KeySize > U4_SIZE)
        return TrieFindFirstMatch (u2KeySize, pInputParams, (VOID *) pLeaf);
    else
        return TrieFindFirstMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf);
}

tRadixNode         *
TrieDoTraverseLessSpecific (tInputParams * pInputParams)
{
    UINT2               u2KeySize, u2InPrefixLen, u2CurPrefixLen;
    UINT1               u1ByteVal;
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pTmpParent = NULL;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    pCurrNode = pTmpParent = pInputParams->pRoot->pRadixNodeTop;

    /*
     *  for an empty trie, return NULL
     */
    if ((NULL == pCurrNode->pRight) && (NULL == pCurrNode->pLeft))
    {
        return NULL;
    }

    u2InPrefixLen = pInputParams->u1PrefixLen;
    while ((pCurrNode != NULL) && (pCurrNode->u1NodeType == RADIX_NODE))
    {
        u2CurPrefixLen = TriePrefixLen (pCurrNode->Mask, u2KeySize);
        if (u2InPrefixLen <= u2CurPrefixLen)
        {
            break;
        }
        pTmpParent = pCurrNode;
        u1ByteVal =
            (UINT1) GETBYTE (pInputParams->Key, pCurrNode->u1ByteToTest,
                             u2KeySize);
        pCurrNode =
            (BIT_TEST (u1ByteVal, pCurrNode->u1BitToTest)) ? (tRadixNode *)
            pCurrNode->pRight : (tRadixNode *) pCurrNode->pLeft;
    }

    if ((pCurrNode == NULL) || (pCurrNode->u1NodeType != RADIX_NODE))
        return pTmpParent;

    return pCurrNode;
}

tLeafNode          *
TrieLeftmost (tRadixNode * pNode)
{
    while ((NULL != pNode) && (RADIX_NODE == pNode->u1NodeType))
        pNode = (tRadixNode *) pNode->pLeft;

    return (tLeafNode *) pNode;
}

tLeafNode          *
TrieRightmost (tRadixNode * pNode)
{
    while ((NULL != pNode) && (RADIX_NODE == pNode->u1NodeType))
        pNode = (tRadixNode *) pNode->pRight;

    return (tLeafNode *) pNode;
}

INT4
TrieCreateSemName (INT4 i4Type, UINT1 *pu1SemName)
{
    UINT2               u2SemNameLen = 0;

    SPRINTF ((CHR1 *) (pu1SemName + u2SemNameLen), "t%d", i4Type);
    return TRIE_SUCCESS;

}

INT4
TrieGetSemId (VOID *pRoot, tOsixSemId * pSemId)
{
    UINT4               u4Instance;
    if (TrieGetTrieInstance (pRoot, &u4Instance) == TRIE_FAILURE)
    {
        return TRIE_FAILURE;
    }
    *pSemId = gaTrieInstance[u4Instance].SemId;
    return TRIE_SUCCESS;
}

INT4
TrieGetTrieInstance (VOID *pRoot, UINT4 *pu4Instance)
{
    UINT4               u4TempInstance;

    for (u4TempInstance = 0; u4TempInstance < MAX_NUM_TRIE; u4TempInstance++)
    {
        if (&(gaTrieInstance[u4TempInstance]) == pRoot)
        {
            *pu4Instance = u4TempInstance;
            return TRIE_SUCCESS;
        }
    }
    return TRIE_FAILURE;
}

tLeafNode          *
TrieDoTraverseLessMoreSpecific (tInputParams * pInputParams)
{
    UINT2               u2KeySize;
    INT4                i4Return;
    tRadixNode         *pRadix = NULL;
    tLeafNode          *pLeaf = NULL;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
        return NULL;            /* empty trie */
    i4Return = TrieDoTraverse (u2KeySize, pRadix, pInputParams->Key, &(pLeaf));
    if (i4Return == TRIE_FAILURE)
    {
        return NULL;
    }
    else
    {
        return (pLeaf);
    }
}
