/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: triesz.c,v 1.1 2015/04/28 12:51:07 siva Exp $
 * Description: This file contains all mempool creation/deletion 
 *              functions for TRIE2 Module.
****************************************************************************/
#define _TRIESZ_C
#include "trieinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
TrieSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRIE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsTRIESizingParams[i4SizingId].u4StructSize,
                              FsTRIESizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(TRIEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            TrieSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

VOID
TrieSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRIE_MAX_SIZING_ID; i4SizingId++)
    {
        if (TRIEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TRIEMemPoolIds[i4SizingId]);
            TRIEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
