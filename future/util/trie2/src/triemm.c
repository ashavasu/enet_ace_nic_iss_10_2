/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: triemm.c,v 1.4 2015/04/28 12:51:07 siva Exp $
 *
 * Description: Functions related to memory management of Trie library.          
 *
 *******************************************************************/
#include "trieinc.h"
extern INT4         TrieSzRegisterModuleSizingParams (CHR1 * pu1ModName);
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TrieSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTRIESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TRIEMemPoolIds);
    return OSIX_SUCCESS;
}
