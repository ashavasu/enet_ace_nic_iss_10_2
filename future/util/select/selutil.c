/************************************************************************ 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: selutil.c,v 1.13 2015/04/28 12:51:04 siva Exp $
 *
 * Desription:
 *    The purpose of this select Library is to enable 
 *    the users to indefinitely wait on the file descriptors 
 *    till some data is available. 

 *    This library provides an abstraction and  enables the 
 *    users to process their data only subject to the availability.
 *
 *    This library provides exportable APIs such  as SelAddFsFd() 
 *    and SelRemoveFsFd() which aid the users to add their 
 *    file descriptors and the respective call back functions.
 *    This File contains Functions and Macros specific to FS Sockets
###########################################################################*/
#ifdef SLI_WANTED
#include "lr.h"
#include "fssocket.h"

/* Data structure definition */
typedef struct
{
    INT4                i4Fd;
    INT4                (*pCallBkFn) (INT4);
}
tFdTbl;

typedef struct
{
    fd_set              fds;
    fd_set              wrfds;
    INT4                i4MaxFd;
}
tSelectParams;

tOsixSemId          gFdSemId;

#define SEL_TSK_NAME       ((UINT1 *) "SLT")
#define FD_SEM_NAME        ((const UINT1 *) "FDSM")
#define MAX_SEL_FDS        1024
#define SEL_TASK_PRIORITY  (80 | OSIX_SCHED_RR)

#define SEL_WAKEUP_SOCKET_PORT   6123

#define SEL_READ_FD  1
#define SEL_WRITE_FD 2
#define SEL_SEM_ID   gFdSemId

tSelectParams       gSelFds;
tSelectParams       gSelDupFds;
INT4                gi4SelectWakeupFd;
UINT4               gu4SelTaskId;
UINT1               gu1SelTaskFlag = OSIX_FALSE;
INT4                gi4AddFdCount;
VOID                (*gpCallBkFn[MAX_SEL_FDS]) (INT4);
VOID                (*gpWrCallBkFn[MAX_SEL_FDS]) (INT4);

/* Function Prototypes */
PRIVATE VOID        SelAddToSelectParams (tSelectParams * pSelParams,
                                          INT4 i4Fd, INT1 i1Type);
PRIVATE VOID        SelRemoveFromSelectParams (tSelectParams * pSelParams,
                                               INT4 i4Fd, INT1 i1Type);
PRIVATE INT4        SelCreateAddSelWakeupFd (VOID);
PRIVATE VOID        SelTaskMain (INT1 *);
PRIVATE INT4        SelTaskInit (VOID);
VOID                SelLibShutdown (VOID);
PRIVATE INT4        SelSendPktToWakeupFd (VOID);
PRIVATE VOID        SelRcvPktFromWakeupFd (INT4 i4SockFd);

/****************************************************************************
 * Function     : SelRemoveFromSelectParams
 *                                                                           
 * Description  : Removes the fd from SelectParams 
 *                                                     
 * Input        : pSelParams - Pointer to SelectParams
 *                
 *                 i4Fd      - File Descriptor to be removed
 *                 i1Tye     - Read/Write Fd
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : None 
****************************************************************************/

PRIVATE VOID
SelRemoveFromSelectParams (tSelectParams * pSelParams, INT4 i4Fd, INT1 i1Type)
{
    INT4                i4MaxFd;
    fd_set             *pFds = NULL;
    pFds = (i1Type == SEL_READ_FD) ? (&(pSelParams->fds)) :
        (&(pSelParams->wrfds));
    if (!(FD_ISSET (i4Fd, pFds)))
        return;
    FD_CLR (i4Fd, pFds);
    gi4AddFdCount--;
    if (i4Fd < (pSelParams->i4MaxFd - 1))
        return;
    for (i4MaxFd = pSelParams->i4MaxFd - 1; i4MaxFd >= 0; i4MaxFd--)
    {
        if ((FD_ISSET (i4MaxFd, &pSelParams->fds)) ||
            (FD_ISSET (i4MaxFd, &pSelParams->wrfds)))
        {
            break;
        }
    }
    pSelParams->i4MaxFd = i4MaxFd + 1;
}

/****************************************************************************
 * Function     : SelAddToSelectParams
 *                                                                           
 * Description  : Adds the fd to SelectParams 
 *                                                     
 * Input        : pSelParams - Pointer to SelectParams
 *                
 *                 i4Fd      - File Descriptor to be removed
 *                 i1Tye     - Read/Write Fd
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : None 
****************************************************************************/
PRIVATE VOID
SelAddToSelectParams (tSelectParams * pSelParams, INT4 i4Fd, INT1 i1Type)
{
    fd_set             *pFds = NULL;
    pFds = (i1Type == SEL_READ_FD) ? (&(pSelParams->fds)) :
        (&(pSelParams->wrfds));
    FD_SET (i4Fd, pFds);
    if (pSelParams->i4MaxFd < (i4Fd + 1))
        pSelParams->i4MaxFd = i4Fd + 1;
    gi4AddFdCount++;
}

/****************************************************************************
 * Function     : SelCreateAddSelWakeupFd
 *                                                                           
 * Description  : Creates a Dummy socket and add the file descriptor to 
 *                the File Descriptor Set. 
 *                                                     
 * Input        : None
 *                                                    
 * Output       : None
 *                                                 
 * Returns      : Returns OSIX_SUCCESS on Success or Else OSIX_FAILURE 
****************************************************************************/

PRIVATE INT4
SelCreateAddSelWakeupFd ()
{
    struct sockaddr_in  WakeupSockAddress;
    INT4                i4Flags = 0;
    char                u1log[256];

    gi4SelectWakeupFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (gi4SelectWakeupFd < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelWakeupFd::Wakeup Socket Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));

    WakeupSockAddress.sin_family = AF_INET;
    WakeupSockAddress.sin_port = OSIX_HTONS (SEL_WAKEUP_SOCKET_PORT);
    WakeupSockAddress.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if ((bind (gi4SelectWakeupFd, (struct sockaddr *) &WakeupSockAddress,
               sizeof (WakeupSockAddress))) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelWakeupFd::Wakeup Socket bind Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags = O_NONBLOCK;
    if (fcntl (gi4SelectWakeupFd, F_SETFL, i4Flags) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelWakeupFd::Wakeup Socket Fcntl SET Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    SelAddToSelectParams (&gSelFds, gi4SelectWakeupFd, SEL_READ_FD);

    gpCallBkFn[gi4SelectWakeupFd] = SelRcvPktFromWakeupFd;
    gpWrCallBkFn[gi4SelectWakeupFd] = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelTaskInit 
 *                                                                           
 * Description  : Select Library initialization routine
 *                This initialization routine is non-reentrant
 *                and initialize the File Descriptor table. The
 *                table is protected by the mutex sema4 "FDSM"
 *                                                     
 * Input        : takes Number of Descriptors to initialise the file 
 *                descriptor table
 *                
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : SUCCESS, if initialization succeeds 
 *                FAILURE, otherwise                 
 *                                                   
 ****************************************************************************/
PRIVATE INT4
SelTaskInit ()
{
    char                u1log[256];

    FD_ZERO (&gSelFds.fds);
    FD_ZERO (&gSelFds.wrfds);
    gSelFds.i4MaxFd = 0;
    FD_ZERO (&gSelDupFds.fds);
    FD_ZERO (&gSelDupFds.wrfds);
    gSelDupFds.i4MaxFd = 0;

    MEMSET (&gpCallBkFn, 0, MAX_SEL_FDS * sizeof (VOID *));
    MEMSET (&gpWrCallBkFn, 0, MAX_SEL_FDS * sizeof (VOID *));

    /* Create the Muxtex Sema4 to protect the gspFdTbl data structure */
    if (OsixCreateSem (FD_SEM_NAME, 1, 0, &SEL_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelTaskInit::Mutex Sema4 Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    if (SelCreateAddSelWakeupFd () == OSIX_FAILURE)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelTaskInit::Sel Wakeup Fd Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelLibShutdown
 *                                                                           
 * Description  : Select Library Shutdown routine
 *                This shutdown routine does the graceful shutdown, releases
 *                the resources
 *                                                     
 * Input        : NONE
 *                
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : NONE
 *                                                   
 ****************************************************************************/
VOID
SelLibShutdown (VOID)
{
    char                u1log[256];

    OsixSemDel (SEL_SEM_ID);

    SelRemoveFromSelectParams (&gSelFds, gi4SelectWakeupFd, SEL_READ_FD);

    if (gi4SelectWakeupFd != -1)
    {
        close (gi4SelectWakeupFd);
    }
    /* Delete the Tasks created for Select Task */
    OsixTskDel (gu4SelTaskId);
    gu4SelTaskId = 0;
    SNPRINTF (u1log, sizeof (u1log), "Select Task Deleted!\n");
    UtlTrcPrint (u1log);
}

/****************************************************************************
 * Function     : SelAddFsFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which does add the File Descriptor that's
 *    already in use into the Locally maintained File Descriptor table along
 *    with the Call back function. Upon reception of data, the call back
 *    function will be invoked to process the just received data.Call back
 *    function should read data from the Filedescriptor and process them.
 
 *    CAVEAT:
 *   The Callback function will run in Select Task Context if Select is 
 *   spawned as the task, otherwise will run in the kernel context if at all
 *   select library is compiled with kernel. It's recommended to write
 *   a call back routine to just enqueue or buffer the received data and
 *   post an event to the respective processes to avoid inheritence of
 *   the owner of Select Library. Usage of Blocking I/O calls inside the
 *   callback routine should be avoided
 *                                                     
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 *  pCallbk  - Pointer to the Callback function that processes the
 *             received packet
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if registration succeeds, 
 *                OSIX_FAILURE, otherwise
 *                                                   
 ****************************************************************************/
INT4
SelAddFsFd (INT4 i4Fd, VOID (*pCallBk) (INT4))
{
    UINT4               u4CallerTaskId;
    char                u1log[256];
    /* Creating a Select task, which will be waiting in the blocking 
     * mode for reading all the messages from Socket
     */
    if (gu1SelTaskFlag == OSIX_FALSE)
    {
        SelTaskInit ();
        if (OsixTskCrt (SEL_TSK_NAME, SEL_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) SelTaskMain,
                        0, &gu4SelTaskId) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log), "Select Task Creation Failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }
        gu1SelTaskFlag = OSIX_TRUE;
    }

    u4CallerTaskId = OsixGetCurTaskId ();

    if ((i4Fd < 0) || (i4Fd >= MAX_SEL_FDS))
    {
        return OSIX_FAILURE;
    }

    if (u4CallerTaskId != gu4SelTaskId)
    {
        /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
        if (OsixSemTake (SEL_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsFd::Sem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        gpCallBkFn[i4Fd] = pCallBk;

        SelAddToSelectParams (&gSelFds, i4Fd, SEL_READ_FD);

        if (SelSendPktToWakeupFd () == OSIX_FAILURE)
        {
            OsixSemGive (SEL_SEM_ID);
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsFd::Failed to send packet to wake up socket!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;

        }

        OsixSemGive (SEL_SEM_ID);

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 * Function     : SelRemoveFsFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which deletes the File Descriptor that's
 *    already in use from the Locally maintained File Descriptor table along
 *    with the Call back function. This API has to be called before closing
 *    the file descriptor.
 *
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if deregistration succeeds, 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
INT4
SelRemoveFsFd (INT4 i4Fd)
{
    char                u1log[256];

    if ((i4Fd < 0) || (i4Fd >= gSelFds.i4MaxFd))
    {
        return (OSIX_FAILURE);
    }

    /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
    if (OsixSemTake (SEL_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsFd::Sem4 Take operation failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    gpCallBkFn[i4Fd] = NULL;

    SelRemoveFromSelectParams (&gSelFds, i4Fd, SEL_READ_FD);

    if (SelSendPktToWakeupFd () == OSIX_FAILURE)
    {
        OsixSemGive (SEL_SEM_ID);
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsFd::Failed to send packet to wake up socket!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    OsixSemGive (SEL_SEM_ID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelAddFsWrFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which does add the File Descriptor that's
 *    already in use into the Locally maintained File Descriptor table along
 *    with the Call back function. The call back function will be invoked to 
 *    iinform the send buffer availability. Call back
 *    function should read data from the Filedescriptor and process them.
 * 
 *   CAVEAT:
 *   The Callback function will run in Select Task Context if Select is 
 *   spawned as the task, otherwise will run in the kernel context if at all
 *   select library is compiled with kernel. It's recommended to write
 *   a call back routine to just post an event to the respective processes to 
 *   avoid inheritence of the owner of Select Library. Usage of Blocking I/O 
 *   calls inside the callback routine should be avoided
 *                                                     
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 *  pCallbk  - Pointer to the Callback function that processes the
 *             write packet
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if registration succeeds, 
 *                OSIX_FAILURE, otherwise
 *                                                   
 ****************************************************************************/
INT4
SelAddFsWrFd (INT4 i4Fd, VOID (*pCallBk) (INT4))
{
    UINT4               u4CallerTaskId;
    char                u1log[256];

    /* Creating a Select task, which will be waiting in the blocking 
     * mode for reading all the messages from Socket
     */
    if (gu1SelTaskFlag == OSIX_FALSE)
    {
        SelTaskInit ();
        if (OsixTskCrt (SEL_TSK_NAME, SEL_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) SelTaskMain,
                        0, &gu4SelTaskId) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log), "Select Task Creation Failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }
        gu1SelTaskFlag = OSIX_TRUE;
    }

    u4CallerTaskId = OsixGetCurTaskId ();

    if ((i4Fd < 0) || (i4Fd >= MAX_SEL_FDS))
    {
        return OSIX_FAILURE;
    }

    if (u4CallerTaskId != gu4SelTaskId)
    {
        /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
        if (OsixSemTake (SEL_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsWrFd::Sem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        gpWrCallBkFn[i4Fd] = pCallBk;

        SelAddToSelectParams (&gSelFds, i4Fd, SEL_WRITE_FD);

        if (SelSendPktToWakeupFd () == OSIX_FAILURE)
        {
            OsixSemGive (SEL_SEM_ID);
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsWrFd::Failed to send packet to wake up socket!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        OsixSemGive (SEL_SEM_ID);

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 * Function     : SelRemoveFsWrFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which deletes the File Descriptor that's
 *    already in use from the Locally maintained File Descriptor table along
 *    with the Call back function. This API has to be called before closing
 *    the file descriptor.
 *
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if deregistration succeeds, 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
INT4
SelRemoveFsWrFd (INT4 i4Fd)
{
    char                u1log[256];

    if ((i4Fd < 0) || (i4Fd >= gSelFds.i4MaxFd))
    {
        return (OSIX_FAILURE);
    }

    /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
    if (OsixSemTake (SEL_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsWrFd::Sem4 Take operation failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    gpWrCallBkFn[i4Fd] = NULL;

    SelRemoveFromSelectParams (&gSelFds, i4Fd, SEL_WRITE_FD);

    if (SelSendPktToWakeupFd () == OSIX_FAILURE)
    {
        OsixSemGive (SEL_SEM_ID);
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsWrFd::Failed to send packet to wake up socket!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    OsixSemGive (SEL_SEM_ID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelTaskMain
 *                                                                           
 * Description  : 
 *           This routine continously scans the File Descriptor Table and
 *           Calls the CallBack Function corresponding to the File Descriptor
 *           on which data is received.
 *
 * Input        : i1pParam -Uuused Parameter
 *
 * Output       : None
 *                                                 
 * Returns      :  None
 *                                                   
 ****************************************************************************/
PRIVATE VOID
SelTaskMain (INT1 *i1pParam)
{
    INT4                i4Fd;
    INT4                i4FdCount;
    char                u1log[256];

#ifdef OS_TMO
    struct timeval      tv;
#define SEL_POLL
#endif

    UNUSED_PARAM (i1pParam);

    while (1)
    {
        gSelDupFds = gSelFds;

/* SEL_POLL - which when enabled will scan the Socket for incoming Packets
 * in polling mode. When the switch is disabled the select work in timeout mode.  
 * When works in Timeout mode(No packet is there),it will not work for Single 
 * threaded OS (tmo). By default the switch is enabled (Polling Mode).*/
#ifdef SEL_POLL
        tv.tv_sec = 0;
        tv.tv_usec = 0;
        OsixTskDelay (1);
        i4FdCount = select (gSelDupFds.i4MaxFd, &gSelDupFds.fds,
                            &gSelDupFds.wrfds, NULL, &tv);
#else
        i4FdCount = select (gSelDupFds.i4MaxFd, &gSelDupFds.fds,
                            &gSelDupFds.wrfds, NULL, NULL);
#endif
        if (i4FdCount <= 0)
        {
            continue;
        }

        /* Take the mutex so that none other
         * process could access the File Descriptor
         * table
         */
        if (OsixSemTake (SEL_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelTaskMainSem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return;
        }

        for (i4Fd = SLI_FIRST_SOCKET_FD; i4Fd <= gSelDupFds.i4MaxFd; i4Fd++)
        {
            if (FD_ISSET (i4Fd, &gSelDupFds.fds))
            {
                if (gpCallBkFn[i4Fd] != NULL)
                {
                    SelRemoveFromSelectParams (&gSelFds, i4Fd, SEL_READ_FD);
                    gpCallBkFn[i4Fd] (i4Fd);
                }
            }
            if (FD_ISSET (i4Fd, &gSelDupFds.wrfds))
            {
                if (gpWrCallBkFn[i4Fd] != NULL)
                {
                    SelRemoveFromSelectParams (&gSelFds, i4Fd, SEL_WRITE_FD);
                    gpWrCallBkFn[i4Fd] (i4Fd);
                }
            }

        }
        OsixSemGive (SEL_SEM_ID);
        /* Release the Mutex upon completion of processing */
    }
}

/****************************************************************************
 * Function     : SelSendPktToWakeupFd
 *                                                                           
 * Description  : 
 *    This function is used to send one byte packet to the wake up socket  
 *    which is used to refresh the descriptor list to be listened by select
 *    system call                                                            
 *
 * Input        : None        
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if packet send succeeds 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PRIVATE INT4
SelSendPktToWakeupFd (VOID)
{
    UINT1               u1Pkt = 1;
    char                u1log[256];

    struct sockaddr_in  WakeupSockAddress;

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));

    WakeupSockAddress.sin_family = AF_INET;
    WakeupSockAddress.sin_port = OSIX_HTONS (SEL_WAKEUP_SOCKET_PORT);
    WakeupSockAddress.sin_addr.s_addr = OSIX_HTONL (INADDR_LOOPBACK);

    if ((sendto (gi4SelectWakeupFd, &u1Pkt, sizeof (u1Pkt), 0,
                 (struct sockaddr *) &WakeupSockAddress,
                 sizeof (WakeupSockAddress))) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelSendPktToWakeupFd:sendto failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelRcvPktFromWakeupFd
 *                                                                           
 * Description  : 
 *    This callback function is used to receive the packet sent on wake up 
 *    socket                                                                 
 *
 * Input        : i4SockFd - Wakeup socket descriptor        
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if packet send succeeds 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PRIVATE VOID
SelRcvPktFromWakeupFd (INT4 i4SockFd)
{
    struct sockaddr_in  WakeupSockAddress;
    INT4                i4RcvAddrlen = sizeof (WakeupSockAddress);
    UINT1               u1Pkt;

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));
    i4RcvAddrlen = sizeof (WakeupSockAddress);

    while (recvfrom (i4SockFd, &u1Pkt, sizeof (u1Pkt), 0,
                     (struct sockaddr *) &WakeupSockAddress, &i4RcvAddrlen) > 0)
    {
        /* Do Nothing !!! */
    }

    SelAddToSelectParams (&gSelFds, i4SockFd, SEL_READ_FD);

    gpCallBkFn[i4SockFd] = SelRcvPktFromWakeupFd;
    gpWrCallBkFn[i4SockFd] = NULL;

    return;
}

/*********************************selLib.c*************************************/
#endif
