/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: selutil.h,v 1.6 2015/04/28 12:51:04 siva Exp $
 * 
 * Description:                         
 *    Prototyes for exportable APIs such  as SelAddFd() 
 *    and SelRemoveFd() which aid the users to add their 
 *    file descriptors and the respective call back functions.
 *    
 * NOTE: This file should be included in the protocol source files 
 *       only if it uses Linux sockets and not FS sockets, irrespective 
 *       of whether it uses FS SLI or BSD SLI. If such a dependency
 *       exists, then "fssocket.h" should be included which will include
 *       the correct header file based on the SLI type selected for
 *       compilation.
 *
 *******************************************************************/

#ifndef SELUTIL_H
#define SELUTIL_H
/* Enable the protocol modules to add their File Descriptors
 * i4Fd - File Descriptor to be added
 * pCallBk - CallBack function which needs to be registered 
 *           with Select Utility */

PUBLIC INT4 SelAddFd PROTO ((const INT4 i4Fd, VOID (*pCallBk)(INT4)));

/* Enable the protocol modules to remove their File Descriptor
 * i4Fd - File Descriptor to be removed */
 
PUBLIC INT4 SelRemoveFd PROTO ((const INT4 i4Fd));

PUBLIC INT4 SelAddWrFd  PROTO ((INT4 i4Fd, VOID (*pCallBk) (INT4)));

PUBLIC INT4 SelRemoveWrFd PROTO ((INT4 i4Fd));

#endif /* SELUTIL_H */
