/************************************************************************ 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sellnxutil.c,v 1.14 2015/09/25 11:47:33 siva Exp $
 *
 * Desription:
 *    The purpose of this select Library is to enable 
 *    the users to indefinitely wait on the file descriptors 
 *    till some data is available. 

 *    This library provides an abstraction and  enables the 
 *    users to process their data only subject to the availability.
 *
 *    This library provides exportable APIs such  as SelAddFd() 
 *    and SelRemoveFd() which aid the users to add their 
 *    file descriptors and the respective call back functions.
 *    This File contains Functions and Macros specific to Linux Sockets
##############################################################################*/

#include "lr.h"
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "selutil.h"

/* Data structure definition */
typedef struct
{
    INT4                i4Fd;
    INT4                (*pCallBkFn) (INT4);
}
tFdTbl;

typedef struct
{
    fd_set              fds;
    fd_set              wrfds;
    INT4                i4MaxFd;
}
tSelectParams;

tOsixSemId          gLinFdSemId;    /* SelTaskLnxInit Mutex Semaphore ID */

UINT2               gu2WakeupPortNo = 0;

#define SEL_LNX_TSK_NAME       ((UINT1 *) "LNXSLT")
#define LNX_FD_SEM_NAME        ((const UINT1 *) "LNXFDSM")
#define MAX_SEL_LNX_FDS        1024
#define SEL_LNX_INVALID_FD      -1
#define SEL_LNX_TASK_PRIORITY  (7 | OSIX_SCHED_RR)

#define SEL_LNX_READ_FD          1
#define SEL_LNX_WRITE_FD         2

#define SEL_WAKEUP_SOCKET_PORT  gu2WakeupPortNo
#define SEL_LNX_SEM_ID          gLinFdSemId

tSelectParams       gSelLnxFds;
tSelectParams       gSelDupLnxFds;
INT4                gi4SelectLnxWakeupFd;
UINT4               gu4SelLnxTaskId;
UINT1               gu1SelLnxTaskFlag = OSIX_FALSE;
INT4                gi4AddLnxFdCount;
VOID                (*gpCallBkFnLnxFd[MAX_SEL_LNX_FDS]) (INT4);
VOID                (*gpCallBkFnLnxWrFd[MAX_SEL_LNX_FDS]) (INT4);

/* Function Prototypes */
PRIVATE VOID        SelAddToLnxSelectParams (tSelectParams * pSelParams,
                                             INT4 i4Fd, INT1 i1Type);
PRIVATE VOID        SelRemoveFromLnxSelectParams (tSelectParams * pSelParams,
                                                  INT4 i4Fd, INT1 i1Type);
PRIVATE INT4        SelCreateAddSelLnxWakeupFd (VOID);
PRIVATE INT4        SelTaskLnxInit (VOID);
PRIVATE VOID        SelLnxTaskMain (INT1 *);
VOID                SelLibLnxShutdown (VOID);
PRIVATE INT4        SelLnxSendPktToWakeupFd (VOID);
PRIVATE VOID        SelLnxRcvPktFromWakeupFd (INT4 i4SockFd);

/****************************************************************************
 * Function     : SelRemoveFromLnxSelectParams
 *                                                                           
 * Description  : Removes the fd from SelectParams 
 *                                                     
 * Input        : pSelParams - Pointer to SelectParams
 *                
 *                i4Fd       - File Descriptor to be removed
 *                 i1Tye     - Read/Write Fd
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : None 
****************************************************************************/
PRIVATE VOID
SelRemoveFromLnxSelectParams (tSelectParams * pSelParams, INT4 i4Fd,
                              INT1 i1Type)
{
    INT4                i4MaxFd;
    fd_set             *pFds = NULL;

    pFds = (i1Type == SEL_LNX_READ_FD) ? (&(pSelParams->fds)) :
        (&(pSelParams->wrfds));

    if (!(FD_ISSET (i4Fd, pFds)))
        return;

    FD_CLR (i4Fd, pFds);
    gi4AddLnxFdCount--;

    if (i4Fd < (pSelParams->i4MaxFd - 1))
    {
        return;
    }
    for (i4MaxFd = pSelParams->i4MaxFd - 1; i4MaxFd >= 0; i4MaxFd--)
    {
        if ((FD_ISSET (i4MaxFd, &pSelParams->fds)) ||
            (FD_ISSET (i4MaxFd, &pSelParams->wrfds)))
        {
            break;
        }
    }
    pSelParams->i4MaxFd = i4MaxFd + 1;
}

/****************************************************************************
 * Function     : SelAddToLnxSelectParams
 *                                                                           
 * Description  : Adds the fd to SelectParams 
 *                                                     
 * Input        : pSelParams - Pointer to SelectParams
 *                
 *                i4Fd       - File Descriptor to be removed
 *                 i1Tye     - Read/Write Fd
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : None 
****************************************************************************/
PRIVATE VOID
SelAddToLnxSelectParams (tSelectParams * pSelParams, INT4 i4Fd, INT1 i1Type)
{
    fd_set             *pFds = NULL;

    pFds = (i1Type == SEL_LNX_READ_FD) ? (&(pSelParams->fds)) :
        (&(pSelParams->wrfds));

    FD_SET (i4Fd, pFds);

    if (pSelParams->i4MaxFd < (i4Fd + 1))
    {
        pSelParams->i4MaxFd = i4Fd + 1;
    }
    gi4AddLnxFdCount++;
}

/****************************************************************************
 * Function     : SelCreateAddSelLnxWakeupFd
 *                                                                           
 * Description  : Creates a Dummy socket and add the file descriptor to 
 *                the File Descriptor Set. 
 *                                                     
 * Input        : None
 *                                                    
 * Output       : None
 *                                                 
 * Returns      : Returns OSIX_SUCCESS on Success or Else OSIX_FAILURE 
****************************************************************************/

PRIVATE INT4
SelCreateAddSelLnxWakeupFd ()
{
    struct sockaddr_in  WakeupSockAddress;
    INT4                i4Flags = 0;
    UINT4               u4Addrlen;
    char                u1log[256];

    gi4SelectLnxWakeupFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (gi4SelectLnxWakeupFd < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelLnxWakeupFd::Wakeup Socket Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));

    WakeupSockAddress.sin_family = AF_INET;
    WakeupSockAddress.sin_port = 0;
    WakeupSockAddress.sin_addr.s_addr = OSIX_HTONL (INADDR_LOOPBACK);

    if ((bind (gi4SelectLnxWakeupFd, (struct sockaddr *) &WakeupSockAddress,
               sizeof (WakeupSockAddress))) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelLnxWakeupFd::Wakeup Socket bind Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags = fcntl (gi4SelectLnxWakeupFd, F_GETFL, 0)) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelLnxWakeupFd::Wakeup Socket Fcntl GET Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (gi4SelectLnxWakeupFd, F_SETFL, i4Flags) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelLnxWakeupFd::Wakeup Socket Fcntl SET Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));

    u4Addrlen = sizeof (WakeupSockAddress);

    if (getsockname (gi4SelectLnxWakeupFd,
                     (struct sockaddr *) &WakeupSockAddress,
                     (socklen_t *) & u4Addrlen) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelCreateAddSelLnxWakeupFd::getsockname Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    SEL_WAKEUP_SOCKET_PORT = OSIX_NTOHS (WakeupSockAddress.sin_port);

    SelAddToLnxSelectParams (&gSelLnxFds, gi4SelectLnxWakeupFd,
                             SEL_LNX_READ_FD);

    gpCallBkFnLnxFd[gi4SelectLnxWakeupFd] = SelLnxRcvPktFromWakeupFd;
    gpCallBkFnLnxWrFd[gi4SelectLnxWakeupFd] = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelTaskLnxInit
 *                                                                           
 * Description  : Select Library initialization routine
 *                This initialization routine is non-reentrant
 *                and initialize the File Descriptor table. The
 *                table is protected by the mutex sema4 "FDSM"
 *                                                     
 * Input        : takes Number of Descriptors to initialise the file 
 *                descriptor table
 *                
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : SUCCESS, if initialization succeeds 
 *                FAILURE, otherwise                 
 *                                                   
 ****************************************************************************/
PRIVATE INT4
SelTaskLnxInit (VOID)
{
    char                u1log[256];
    INT4                i4Fd;

    FD_ZERO (&gSelLnxFds.fds);
    FD_ZERO (&gSelLnxFds.wrfds);
    gSelLnxFds.i4MaxFd = 0;
    FD_ZERO (&gSelDupLnxFds.fds);
    FD_ZERO (&gSelDupLnxFds.wrfds);
    gSelDupLnxFds.i4MaxFd = 0;
    INT4                i4RetStatus = 0;

    for (i4Fd = 0; i4Fd < MAX_SEL_LNX_FDS; i4Fd++)
    {
        gpCallBkFnLnxFd[i4Fd] = NULL;
        gpCallBkFnLnxWrFd[i4Fd] = NULL;
    }

    /* TODO: The loopback interface creation needs to be removed once loopback
     * support is available in ISS. This is done for sento to succeed while sending
     * packet to WakeupFd.
     */
    i4RetStatus = system ("/sbin/ifconfig lo 127.0.0.1");

    /* Create the Muxtex Sema4 to protect the gspFdTbl data structure */
    if (OsixCreateSem (LNX_FD_SEM_NAME, 1, 0, &SEL_LNX_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelTaskLnxInit::Mutex Sema4 Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    if (SelCreateAddSelLnxWakeupFd () == OSIX_FAILURE)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelTaskLnxInit::Sel Wakeup Fd Creation Failure!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    UNUSED_PARAM( i4RetStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelLibLnxShutdown
 *                                                                           
 * Description  : Select Library Shutdown routine
 *                This shutdown routine does the graceful shutdown, releases
 *                the resources
 *                                                     
 * Input        : NONE
 *                
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : NONE
 *                                                   
 ****************************************************************************/
VOID
SelLibLnxShutdown (VOID)
{
    char                u1log[256];

    OsixSemDel (SEL_LNX_SEM_ID);

    SelRemoveFromLnxSelectParams (&gSelLnxFds, gi4SelectLnxWakeupFd,
                                  SEL_LNX_READ_FD);

    if (gi4SelectLnxWakeupFd != -1)
    {
        close (gi4SelectLnxWakeupFd);
    }
    /* Delete the Tasks created for Select Task */
    OsixTskDel (gu4SelLnxTaskId);
    gu4SelLnxTaskId = 0;
    SNPRINTF (u1log, sizeof (u1log), "Select Task Deleted!\n");
    UtlTrcPrint (u1log);
}

/****************************************************************************
 * Function     : SelAddFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which does add the File Descriptor that's
 *    already in use into the Locally maintained File Descriptor table along
 *    with the Call back function. Upon reception of data, the call back
 *    function will be invoked to process the just received data.Call back
 *    function should read data from the Filedescriptor and process them.
 
 *    CAVEAT:
 *   The Callback function will run in Select Task Context if Select is 
 *   spawned as the task, otherwise will run in the kernel context if at all
 *   select library is compiled with kernel. It's recommended to write
 *   a call back routine to just enqueue or buffer the received data and
 *   post an event to the respective processes to avoid inheritence of
 *   the owner of Select Library. Usage of Blocking I/O calls inside the
 *   callback routine should be avoided
 *                                                     
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 *  pCallbk  - Pointer to the Callback function that processes the
 *             received packet
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if registration succeeds, 
 *                OSIX_FAILURE, otherwise
 *                                                   
 ****************************************************************************/
PUBLIC INT4
SelAddFd (const INT4 i4Fd, VOID (*pCallBk) (INT4))
{
    UINT4               u4CallerTaskId;
    char                u1log[256];

    /* Creating a Select task, which will be waiting in the blocking 
     * mode for reading all the messages from Socket
     */
    if (gu1SelLnxTaskFlag == OSIX_FALSE)
    {
        gu1SelLnxTaskFlag = OSIX_TRUE;
        SelTaskLnxInit ();
        if (OsixTskCrt (SEL_LNX_TSK_NAME, SEL_LNX_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) SelLnxTaskMain,
                        0, &gu4SelLnxTaskId) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log), "Select Task Creation Failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }
    }

    u4CallerTaskId = OsixGetCurTaskId ();

    if ((i4Fd < 0) || (i4Fd >= MAX_SEL_LNX_FDS))
    {
        return OSIX_FAILURE;
    }

    if (u4CallerTaskId != gu4SelLnxTaskId)
    {
        /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
        if (OsixSemTake (SEL_LNX_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFd::Sem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        gpCallBkFnLnxFd[i4Fd] = pCallBk;

        SelAddToLnxSelectParams (&gSelLnxFds, i4Fd, SEL_LNX_READ_FD);

        if (SelLnxSendPktToWakeupFd () == OSIX_FAILURE)
        {
            OsixSemGive (SEL_LNX_SEM_ID);
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFd::Failed to send packet to wake up socket!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;

        }

        OsixSemGive (SEL_LNX_SEM_ID);

        return OSIX_SUCCESS;

    }

    return OSIX_FAILURE;
}

/****************************************************************************
 * Function     : SelRemoveFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which deletes the File Descriptor that's
 *    already in use from the Locally maintained File Descriptor table along
 *    with the Call back function. This API has to be called before closing
 *    the file descriptor.
 *
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if deregistration succeeds, 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PUBLIC INT4
SelRemoveFd (const INT4 i4Fd)
{
    char                u1log[256];
    if ((i4Fd < 0) || (i4Fd >= gSelLnxFds.i4MaxFd))
    {
        return (OSIX_FAILURE);
    }

    /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
    if (OsixSemTake (SEL_LNX_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFd::Sem4 Take operation failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }
    gpCallBkFnLnxFd[i4Fd] = NULL;

    SelRemoveFromLnxSelectParams (&gSelLnxFds, i4Fd, SEL_LNX_READ_FD);

    if (SelLnxSendPktToWakeupFd () == OSIX_FAILURE)
    {
        OsixSemGive (SEL_LNX_SEM_ID);
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFd::Failed to send packet to wake up socket\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    OsixSemGive (SEL_LNX_SEM_ID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelAddFsWrFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which does add the File Descriptor that's
 *    already in use into the Locally maintained File Descriptor table along
 *    with the Call back function. The call back function will be invoked to 
 *    iinform the send buffer availability. Call back
 *    function should read data from the Filedescriptor and process them.
 * 
 *   CAVEAT:
 *   The Callback function will run in Select Task Context if Select is 
 *   spawned as the task, otherwise will run in the kernel context if at all
 *   select library is compiled with kernel. It's recommended to write
 *   a call back routine to just post an event to the respective processes to 
 *   avoid inheritence of the owner of Select Library. Usage of Blocking I/O 
 *   calls inside the callback routine should be avoided
 *                                                     
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 *  pCallbk  - Pointer to the Callback function that processes the
 *             write packet
 *                                                   
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if registration succeeds, 
 *                OSIX_FAILURE, otherwise
 *                                                   
 ****************************************************************************/
PUBLIC INT4
SelAddWrFd (INT4 i4Fd, VOID (*pCallBk) (INT4))
{
    UINT4               u4CallerTaskId;
    char                u1log[256];

    /* Creating a Select task, which will be waiting in the blocking 
     * mode for reading all the messages from Socket
     */
    if (gu1SelLnxTaskFlag == OSIX_FALSE)
    {
        gu1SelLnxTaskFlag = OSIX_TRUE;
        SelTaskLnxInit ();
        if (OsixTskCrt (SEL_LNX_TSK_NAME, SEL_LNX_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) SelLnxTaskMain,
                        0, &gu4SelLnxTaskId) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log), "Select Task Creation Failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }
    }

    u4CallerTaskId = OsixGetCurTaskId ();

    if ((i4Fd < 0) || (i4Fd >= MAX_SEL_LNX_FDS))
    {
        return OSIX_FAILURE;
    }

    if (u4CallerTaskId != gu4SelLnxTaskId)
    {
        /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
        if (OsixSemTake (SEL_LNX_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsWrFd::Sem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        gpCallBkFnLnxWrFd[i4Fd] = pCallBk;

        SelAddToLnxSelectParams (&gSelLnxFds, i4Fd, SEL_LNX_WRITE_FD);

        if (SelLnxSendPktToWakeupFd () == OSIX_FAILURE)
        {
            OsixSemGive (SEL_LNX_SEM_ID);
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelAddFsWrFd::Failed to send packet to wake up socket!\n");
            UtlTrcPrint (u1log);
            return OSIX_FAILURE;
        }

        OsixSemGive (SEL_LNX_SEM_ID);

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 * Function     : SelRemoveWrFd
 *                                                                           
 * Description  : 
 *    This API is an exported API, which deletes the File Descriptor that's
 *    already in use from the Locally maintained File Descriptor table along
 *    with the Call back function. This API has to be called before closing
 *    the file descriptor.
 *
 * Input: 
 *  i4Fd - File Descriptor that is already in use. In other words,
 *         the device or file or socket should have been opened 
 *         before registering with select library
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if deregistration succeeds, 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PUBLIC INT4
SelRemoveWrFd (INT4 i4Fd)
{
    char                u1log[256];

    if ((i4Fd < 0) || (i4Fd >= gSelLnxFds.i4MaxFd))
    {
        return (OSIX_FAILURE);
    }

    /* Take Mutex Sema4 to operate on gspFdTbl Data Structure */
    if (OsixSemTake (SEL_LNX_SEM_ID) != OSIX_SUCCESS)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsWrFd::Sem4 Take operation failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    gpCallBkFnLnxWrFd[i4Fd] = NULL;

    SelRemoveFromLnxSelectParams (&gSelLnxFds, i4Fd, SEL_LNX_WRITE_FD);

    if (SelLnxSendPktToWakeupFd () == OSIX_FAILURE)
    {
        OsixSemGive (SEL_LNX_SEM_ID);
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelRemoveFsWrFd::led to send packet to wake up socket!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    OsixSemGive (SEL_LNX_SEM_ID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelLnxTaskMain
 *                                                                           
 * Description  : 
 *           This routine continously scans the File Descriptor Table and
 *           Calls the CallBack Function corresponding to the File Descriptor
 *           on which data is received.
 *
 * Input        : i1pParam -Uuused Parameter
 *
 * Output       : None
 *                                                 
 * Returns      :  None
 *                                                   
 ****************************************************************************/
PUBLIC VOID
SelLnxTaskMain (INT1 *i1pParam)
{
    INT4                i4Fd;
    INT4                i4FdCount;
    char                u1log[256];

#ifdef OS_TMO
    struct timeval      tv;
#define LNX_SEL_POLL
#endif

    UNUSED_PARAM (i1pParam);

    while (1)
    {
        gSelDupLnxFds = gSelLnxFds;

/* LNX_SEL_POLL - which when enabled will scan the Socket for incoming Packets
 * in polling mode. When the switch is disabled the select work in timeout mode.  
 * When works in Timeout mode(No packet is there),it will not work for Single 
 * threaded OS (tmo). By default the switch is enabled (Polling Mode).*/
#ifdef  LNX_SEL_POLL
        tv.tv_sec = 0;
        tv.tv_usec = 0;
        OsixTskDelay (1);
        i4FdCount =
            select (gSelDupLnxFds.i4MaxFd, &gSelDupLnxFds.fds,
                    &gSelDupLnxFds.wrfds, NULL, &tv);
#else
        i4FdCount =
            select (gSelDupLnxFds.i4MaxFd, &gSelDupLnxFds.fds,
                    &gSelDupLnxFds.wrfds, NULL, NULL);
#endif

        if (i4FdCount <= 0)
        {
            continue;
        }

        /* Take the mutex so that none other
         * process could access the File Descriptor
         * table
         */
        if (OsixSemTake (SEL_LNX_SEM_ID) != OSIX_SUCCESS)
        {
            SNPRINTF (u1log, sizeof (u1log),
                      "-E- SelLnxTaskMainSem4 Take operation failed!\n");
            UtlTrcPrint (u1log);
            return;
        }

        for (i4Fd = 0; i4Fd <= gSelDupLnxFds.i4MaxFd; i4Fd++)
        {
            if (FD_ISSET (i4Fd, &gSelDupLnxFds.fds))
            {
                SelRemoveFromLnxSelectParams (&gSelLnxFds, i4Fd,
                                              SEL_LNX_READ_FD);
                if (gpCallBkFnLnxFd[i4Fd] != NULL)
                {
                    gpCallBkFnLnxFd[i4Fd] (i4Fd);
                }
            }
            if (FD_ISSET (i4Fd, &gSelDupLnxFds.wrfds))
            {
                if (gpCallBkFnLnxWrFd[i4Fd] != NULL)
                {
                    SelRemoveFromLnxSelectParams (&gSelLnxFds, i4Fd,
                                                  SEL_LNX_WRITE_FD);
                    gpCallBkFnLnxWrFd[i4Fd] (i4Fd);
                }
            }

        }
        OsixSemGive (SEL_LNX_SEM_ID);
        /* Release the Mutex upon completion of processing */
    }
}

/****************************************************************************
 * Function     : SelLnxSendPktToWakeupFd
 *                                                                           
 * Description  : 
 *    This function is used to send one byte packet to the wake up socket  
 *    which is used to refresh the descriptor list to be listened by select
 *    system call                                                            
 *
 * Input        : None        
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if packet send succeeds 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PRIVATE INT4
SelLnxSendPktToWakeupFd (VOID)
{
    UINT1               u1Pkt = 1;
    struct sockaddr_in  WakeupSockAddress;
    char                u1log[256];

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));

    WakeupSockAddress.sin_family = AF_INET;
    WakeupSockAddress.sin_port = OSIX_HTONS (SEL_WAKEUP_SOCKET_PORT);
    WakeupSockAddress.sin_addr.s_addr = OSIX_HTONL (INADDR_LOOPBACK);

    if ((sendto (gi4SelectLnxWakeupFd, &u1Pkt, sizeof (u1Pkt), 0,
                 (struct sockaddr *) &WakeupSockAddress,
                 sizeof (WakeupSockAddress))) < 0)
    {
        SNPRINTF (u1log, sizeof (u1log),
                  "-E- SelLnxSendPktToWakeupFd:sendto failed!\n");
        UtlTrcPrint (u1log);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : SelLnxRcvPktFromWakeupFd
 *                                                                           
 * Description  : 
 *    This callback function is used to receive the packet sent on wake up 
 *    socket                                                                 
 *
 * Input        : i4SockFd - Wakeup socket descriptor        
 *
 * Output       : None
 *                                                 
 * Returns      : OSIX_SUCCESS, if packet send succeeds 
 *                OSIX_FAILURE, otherwise                 
 *                                             
 ****************************************************************************/
PRIVATE VOID
SelLnxRcvPktFromWakeupFd (INT4 i4SockFd)
{
    struct sockaddr_in  WakeupSockAddress;
    INT4                i4RcvAddrlen = sizeof (WakeupSockAddress);
    UINT1               u1Pkt;

    MEMSET ((UINT1 *) &WakeupSockAddress, 0, sizeof (WakeupSockAddress));
    i4RcvAddrlen = sizeof (WakeupSockAddress);

    while (recvfrom (i4SockFd, &u1Pkt, sizeof (u1Pkt), 0,
                     (struct sockaddr *) &WakeupSockAddress,
                     (socklen_t *) & i4RcvAddrlen) > 0)
    {
        /* Do Nothing !!! */
    }

    SelAddToLnxSelectParams (&gSelLnxFds, i4SockFd, SEL_LNX_READ_FD);

    gpCallBkFnLnxFd[i4SockFd] = SelLnxRcvPktFromWakeupFd;
    gpCallBkFnLnxWrFd[i4SockFd] = NULL;

    return;
}

/*********************************sellnxutil.c*************************************/
