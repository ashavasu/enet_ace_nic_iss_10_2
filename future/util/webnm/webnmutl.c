/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: webnmutl.c,v 1.15 2017/12/11 10:02:03 siva Exp $
 *
 * Description: Utility Routines for WebNM.
 *******************************************************************/
#include "lr.h"
#include "fssnmp.h"
#include "fswebnm.h"
#include "webnmutl.h"
#include "cli.h"

extern UINT4        gu4HttpMask;
INT1                WEBNM_UTIL_ERROR_STR[] = "Error";
UINT1               au1WebnmUtilGambit[ENM_MAX_NAME_LEN];
UINT1               WEBNM_UTIL_GAMBIT[] = "GAMBIT";

#ifdef WEBNM_WANTED
/************************************************************************
 *  Function Name   : WebnmConvertDataToString 
 *  Description     : Convert the given multidata to string for webnm to
 *                    write the data to client.
 *  Input           : pData - Pointer to multidata
 *                    u1Type - Type of element in multidata
 *  Output          : pu1String - Pointer to a string where the result will
 *                    written
 *  Returns         : None
 ************************************************************************/

VOID
WebnmConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                          UINT1 *pu1String, UINT1 u1Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    UINT1               au1Temp[WEBNM_MAX_STRING];
    pu1String[0] = '\0';

    switch (u1Type)
    {
        case ENM_INTEGER32:
            SPRINTF ((char *) pu1String, "%d", pData->i4_SLongValue);
            break;
        case ENM_UINTEGER32:
        case ENM_TIMETICKS:
        case ENM_GAUGE:
        case ENM_COUNTER32:
            SPRINTF ((char *) pu1String, "%u", pData->u4_ULongValue);
            break;
        case ENM_COUNTER64:
            SPRINTF ((char *) pu1String, "%u", pData->u8_Counter64Value.msn);
            SPRINTF ((char *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            HTTP_STRCAT (pu1String, au1Temp);
            break;
        case ENM_IPADDRESS:
            u4Value = pData->u4_ULongValue;
            for (u4Count = 0; u4Count < (ENM_IPADDRESS_LEN - 1); u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                SPRINTF ((char *) au1Temp, "%u.", u4Temp);
                HTTP_STRCAT (pu1String, au1Temp);
                u4Value = u4Value << ENM_BYTE_LEN;
            }
            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((char *) au1Temp, "%u", u4Temp);
            HTTP_STRCAT (pu1String, au1Temp);
            break;
        case ENM_OCTETSTRING:
            for (u4Count = 0;
                 u4Count < (UINT4) (pData->pOctetStrValue->i4_Length);
                 u4Count++)
            {
                if (u4Count == (UINT4) pData->pOctetStrValue->i4_Length - 1)
                {
                    SPRINTF ((char *) au1Temp, "%02x",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                }
                else
                {
                    SPRINTF ((char *) au1Temp, "%02x.",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                }
                HTTP_STRCAT (pu1String, au1Temp);
            }
            break;
        case ENM_OIDTYPE:
            for (u4Count = 0; u4Count < pData->pOidValue->u4_Length; u4Count++)
            {
                if (u4Count == pData->pOidValue->u4_Length - 1)
                {
                    SPRINTF ((char *) au1Temp, "%u",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                else
                {
                    SPRINTF ((char *) au1Temp, "%u.",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                HTTP_STRCAT (pu1String, au1Temp);
            }
            break;
        case ENM_DISPLAYSTRING:
            for (u4Count = 0; u4Count <
                 (UINT4) pData->pOctetStrValue->i4_Length; u4Count++)
            {
                SPRINTF ((char *) au1Temp, "%c",
                         pData->pOctetStrValue->pu1_OctetList[u4Count]);
                HTTP_STRCAT (pu1String, au1Temp);
            }
            break;
        case ENM_PORT_LIST:
            if (pData->pOctetStrValue->i4_Length == 0)
                break;
            IssGetStringPort (pData->pOctetStrValue->pu1_OctetList,
                              pData->pOctetStrValue->i4_Length, pu1String);
            break;
        default:
            HTTP_STRCPY (pu1String, WEBNM_UTIL_ERROR_STR);
    }
}

/************************************************************************
 *  Function Name   : WebnmConvertStringToData 
 *  Description     : Convert a data in string format to multidata.
 *  Input           : pu1String - String contain data in string format.
 *                    u1Type - Type of the data 
 *  Output          : None
 *  Returns         : pointer to a multidata or null
 ************************************************************************/

tSNMP_MULTI_DATA_TYPE *
WebnmConvertStringToData (UINT1 *pu1String, UINT1 u1Type)
{
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT1              *pau1Ptr = NULL;

    if ((pData = WebnmAllocMultiData ()) == NULL)
    {
        return NULL;
    }
    switch (u1Type)
    {
        case ENM_INTEGER32:
            pData->i4_SLongValue = ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            break;
        case ENM_UINTEGER32:
            pData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            break;
        case ENM_GAUGE:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_GAUGE;
            break;
        case ENM_COUNTER32:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER32;
            break;
        case ENM_IPADDRESS:
            WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
            MEMCPY (&pData->u4_ULongValue,
                    pData->pOctetStrValue->pu1_OctetList, 4);
            pData->u4_ULongValue = OSIX_NTOHL (pData->u4_ULongValue);
            pData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
            break;
        case ENM_OCTETSTRING:
            WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_OIDTYPE:
            WebnmConvertStringToOid (pData->pOidValue, pu1String, ENM_DEC_DATA);
            pData->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            break;
        case ENM_DISPLAYSTRING:
            pData->pOctetStrValue->i4_Length = (INT4) HTTP_STRLEN (pu1String);
            MEMCPY (pData->pOctetStrValue->pu1_OctetList, pu1String,
                    pData->pOctetStrValue->i4_Length);
            pData->pOctetStrValue->pu1_OctetList
                [pData->pOctetStrValue->i4_Length] = '\0';
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_TIMETICKS:
            pData->i4_SLongValue = ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
            break;
        case ENM_PORT_LIST:
            issDecodeSpecialChar (pu1String);
            pau1Ptr = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

            if (pau1Ptr == NULL)
            {
                ENM_TRACE ("WebnmConvertStringToData: "
                           "Error in allocating memory for pau1Ptr\r\n");
                return NULL;
            }
            MEMSET (pau1Ptr, 0, BRG_PORT_LIST_SIZE);
            if (ConvertStrToPortList (pu1String, pau1Ptr, BRG_PORT_LIST_SIZE,
                                      BRG_MAX_PHY_PLUS_LAG_INT_PORTS)
                == OSIX_FAILURE)
            {
                FsUtilReleaseBitList (pau1Ptr);
                return NULL;
            }
            pData->pOctetStrValue->i4_Length = BRG_PORT_LIST_SIZE;
            MEMCPY (pData->pOctetStrValue->pu1_OctetList, pau1Ptr,
                    pData->pOctetStrValue->i4_Length);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            FsUtilReleaseBitList (pau1Ptr);
            break;
        default:
            ENM_TRACE ("Webnm:Unknow Data Type\n");
            return NULL;
    }
    return pData;
}

/************************************************************************
 ** Function Name   : allocmem_EnmBlk
 ** Description     : function to allocate ENM Block
 ** Input           : None
 ** Output          : None
 ** Returns         : pointer to ENM block or null
 ***************************************************************************/
UINT1              *
allocmem_EnmBlk ()
{
    UINT1              *pEnmBlk = NULL;

    if ((pEnmBlk = (UINT1 *) MemAllocMemBlk (gHttpMaxEnmPoolId)) == NULL)
    {
        return NULL;
    }
    return pEnmBlk;
}

/************************************************************************
 ** Function Name   : free_EnmBlk
 ** Description     : function to free ENM Block pointer
 ** Input           : pRespBlk - pointer to ENM Response block
 ** Output          : None
 ** Returns         : None
 ***************************************************************************/
VOID
free_EnmBlk (UINT1 *pEnmBlk)
{
    if (pEnmBlk != NULL)
    {
        if (MemReleaseMemBlock (gHttpMaxEnmPoolId,
                                (UINT1 *) pEnmBlk) != MEM_SUCCESS)
        {
            ENM_TRACE ("MEMORY release failure\n");
        }
    }
}
#else

/*********************************************************************
 * Function Name : WebnmGetWebUsrNameForSysLogMsg
 * Description   : This function is used to get the name of the user  
 *                 for sending in syslog message. This function acts
 *                 as a stub function for actual function defined in
 *                 webnm.c and will memset the username to be displayed.
 * Input(s)      : None.
 * Output(s)     : pu1UserName
 * Return Values : ENM_SUCCESS
 *********************************************************************/
INT1
WebnmGetWebUsrNameForSysLogMsg (UINT1 *pu1UserName)
{
    MEMSET (pu1UserName, 0, ENM_MAX_USERNAME_LEN);
    return (ENM_SUCCESS);
}

/*********************************************************************
 * Function Name : WebnmSetWebUsrNameForSysLogMsg
 * Description   : This function is used to set the name of the user  
 *                 for sending in syslog message. This function acts as
 *                 a stub function when WEBNM is disabled. Actual 
 *                 function is present in webnm.c
 * Input(s)      : pu1UserName
 * Output(s)     : None.
 * Return Values : ENM_SUCCESS
 *********************************************************************/
INT1
WebnmSetWebUsrNameForSysLogMsg (UINT1 *pu1UserName)
{
    UNUSED_PARAM (pu1UserName);
    return (ENM_SUCCESS);
}

/*********************************************************************
 * Function Name : WebnmGetSnmpQueryFromWebStatus
 * Description   : This function is used to get the current SNMP query
 *                 status. This function acts as a stub for actual
 *                 function present in webnm.c when WEBNM flag is
 *                 disabled.
 * Input(s)      : None
 * Output(s)     : OSIX_FALSE
 * Return Values : OSIX_FALSE
 *********************************************************************/
UINT1
WebnmGetSnmpQueryFromWebStatus ()
{
    /* Indicates that the request is always from SNMP, not from Web */
    UINT1               u1StubRetVal = OSIX_FALSE;
    return (u1StubRetVal);
}

/*********************************************************************
 * Function Name : WebnmSetSnmpQueryFromWebStatus
 * Description   : This function is sets gu1WebnmSnmpQueryFromWebStatus
 *                 if the SNMP set request is arised from web page.
 *                 This fucntion acts as a stub for actual function
 *                 present in webnm.c when WEBNM flag is disabled.
 * Input(s)      : None
 * Output(s)     : None
 * Return Values : ENM_SUCCESS
 *********************************************************************/
UINT1
WebnmSetSnmpQueryFromWebStatus (UINT1 u1WebnmSnmpQueryFromWebStatus)
{
    UNUSED_PARAM (u1WebnmSnmpQueryFromWebStatus);
    return ENM_SUCCESS;
}
#endif /* WEBNM_WANTED */

/************************************************************************
 *  Function Name   : WebnmConvertStringToOctet 
 *  Description     : Convert a string to octet string
 *  Input           : pu1Data - String to be converted to octet 
 *  Output          : pOctetStrValue - Pointer to octet string
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/

INT4
WebnmConvertStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                           UINT1 *pu1Data)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;
    pOctetStrValue->i4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
            u4Dot++;
    }
    if (u4Dot == 0)
    {
        return ENM_FAILURE;
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isdigit ((INT4) *pu1String))
        {
            i1RetVal = WebnmConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                return ENM_FAILURE;
            }
            pOctetStrValue->pu1_OctetList[u4Count] = (UINT1) u4Value;
            pOctetStrValue->i4_Length++;
        }
        else
        {
            return ENM_FAILURE;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            return ENM_FAILURE;
        }
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : WebnmConvertColStringToOctet
 *  Description     : Convert a string to octet string
 *  Input           : pu1Data - String to be converted to octet 
 *  Output          : pOctetStrValue - Pointer to octet string
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/

INT4
WebnmConvertColonStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                                UINT1 *pu1Data)
{
    UINT4               u4Count = 0, u4Dot = 0, u4Value = 0;
    INT4                i4RetValue = ENM_SUCCESS;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetStatus = 0;

    pOctetStrValue->i4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == ':')
        {
            pu1String[u4Count] = '.';
            u4Dot++;
        }
    }

    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isxdigit ((INT4) *pu1String))
        {
            i1RetStatus = WebnmHexConvertSubOid (&pu1String, &u4Value);
            if (i1RetStatus == -1)
            {
                i4RetValue = ENM_FAILURE;
                break;
            }
            pOctetStrValue->pu1_OctetList[u4Count] = (UINT1) u4Value;
            pOctetStrValue->i4_Length++;
        }
        else
        {
            i4RetValue = ENM_FAILURE;
            break;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            i4RetValue = ENM_FAILURE;
            break;
        }
    }
    pu1String = pu1Data;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
        {
            pu1String[u4Count] = ':';
        }
    }
    return i4RetValue;
}

/************************************************************************
 *  Function Name   : WebnmConvertStringToOid 
 *  Description     : Convert string to OID Type
 *  Input           : pu1Data - String to be converted into oid
 *  Output          : pOid - Pointer where the OID will be written
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
WebnmConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;
    pOid->u4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
            u4Dot++;
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isdigit ((INT4) *pu1String))
        {
            if (u1HexFlag == ENM_HEX_DATA)
                i1RetVal = WebnmHexConvertSubOid (&pu1String, &u4Value);
            else
                i1RetVal = WebnmConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                return ENM_FAILURE;
            }
            pOid->pu4_OidList[u4Count] = u4Value;
            pOid->u4_Length++;
        }
        else
        {
            return ENM_FAILURE;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            return ENM_FAILURE;
        }
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : WebnmConvertColonStringToOid 
 *  Description     : Convert string to OID Type
 *  Input           : pu1Data - String to be converted into oid
 *  Output          : pOid - Pointer where the OID will be written
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
WebnmConvertColonStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data,
                              UINT1 u1HexFlag)
{
    UINT4               u4Count = 0, u4Dot = 0, u4Value = 0;
    INT4                i4RetValue = ENM_SUCCESS;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;
    pOid->u4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == ':')
        {
            pu1String[u4Count] = '.';
            u4Dot++;
        }
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isxdigit ((INT4) *pu1String))
        {
            if (u1HexFlag == ENM_HEX_DATA)
                i1RetVal = WebnmHexConvertSubOid (&pu1String, &u4Value);
            else
                i1RetVal = WebnmConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                i4RetValue = ENM_FAILURE;
                break;
            }
            pOid->pu4_OidList[u4Count] = u4Value;
            pOid->u4_Length++;
        }
        else
        {
            i4RetValue = ENM_FAILURE;
            break;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            i4RetValue = ENM_FAILURE;
            break;
        }
    }
    pu1String = pu1Data;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
        {
            pu1String[u4Count] = ':';
        }
    }
    return i4RetValue;
}

/************************************************************************
 *  Function Name   : WebnmConvertSubOid 
 *  Description     : Convert String pointer to decemial integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
WebnmConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!isdigit (**ppu1String))
        {
            return -1;
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 10) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

/************************************************************************
 *  Function Name   : WebnmHexConvertSubOid 
 *  Description     : Convert String pointer to Hexa integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
WebnmHexConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1TempChar = '\0';
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!((**ppu1String >= 'a' && **ppu1String < 'g')
              || isdigit (**ppu1String)))
        {
            return -1;
        }
        else
        {
            if (!isdigit (**ppu1String))
            {
                u1TempChar = (UINT1) (10 + ((**ppu1String) - 'a'));
            }
            else
            {
                u1TempChar = **ppu1String;
            }
        }
        u4Value = (u4Value * 16) + (0x0f & (UINT4) u1TempChar);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

/************************************************************************
 *  Function Name   : WebnmCopyOid 
 *  Description     : Copy one OID to another one
 *  Input           : pSrcOid - Source OID Pointer 
 *  Output          : pDestOid - Destination OID Pointer
 *  Returns         : None
 ************************************************************************/

VOID
WebnmCopyOid (tSNMP_OID_TYPE * pDestOid, tSNMP_OID_TYPE * pSrcOid)
{
    UINT4               u4Count = 0;
    for (u4Count = 0; u4Count < pSrcOid->u4_Length; u4Count++)
    {
        pDestOid->pu4_OidList[pDestOid->u4_Length] =
            pSrcOid->pu4_OidList[u4Count];
        pDestOid->u4_Length++;
    }
}

/************************************************************************
 *  Function Name   : WebnmGetInstanceOid 
 *  Description     : Get the Instance OID from the Given OID and Table
 *                    element actual OID
 *  Input           : pCurOid - Table element Actual OID
 *                    pNextOid - Table element OID with Instance
 *  Output          : pInstance - Instance OID Pointer 
 *  Returns         : None
 ************************************************************************/

VOID
WebnmGetInstanceOid (tSNMP_OID_TYPE * pCurOid,
                     tSNMP_OID_TYPE * pNextOid, tSNMP_OID_TYPE * pInstance)
{
    UINT4               u4Count = 0, u4Index = 0;
    pInstance->u4_Length = 0;
    if (pNextOid->u4_Length > pCurOid->u4_Length)
    {
        if (MEMCMP (pCurOid->pu4_OidList, pNextOid->pu4_OidList,
                    (pCurOid->u4_Length * 4)) != 0)
        {
            return;
        }
        for (u4Count = pCurOid->u4_Length; u4Count < pNextOid->u4_Length;
             u4Count++, u4Index++)
        {
            pInstance->pu4_OidList[u4Index] = pNextOid->pu4_OidList[u4Count];
            pInstance->u4_Length++;
        }
    }
}

/************************************************************************
 *  Function Name   : WebnmConvertOidToString 
 *  Description     : Convert the given oid to string
 *  Input           : pOid - Pointer to OID List
 *  Output          : pu1String - String contains OID in string format
 *  Returns         : None
 ************************************************************************/

VOID
WebnmConvertOidToString (tSNMP_OID_TYPE * pOid, UINT1 *pu1String)
{
    UINT4               u4Count;
    UINT1               au1Array[WEBNM_MAX_STRING];
    pu1String[0] = '\0';
    au1Array[0] = '\0';
    if (pOid->u4_Length == 0)
    {
        return;
    }
    for (u4Count = 0; u4Count < (pOid->u4_Length - 1); u4Count++)
    {
        /*If the value is greater than MAX value of integer
         * copy the value in au1Array using %u notation
         * else copy the value using %d notation.*/
        if (pOid->pu4_OidList[u4Count] > MAX_INT)
        {
            SPRINTF ((char *) au1Array, "%u.", pOid->pu4_OidList[u4Count]);
        }
        else
        {
            SPRINTF ((char *) au1Array, "%d.", pOid->pu4_OidList[u4Count]);
        }
        HTTP_STRCAT (pu1String, au1Array);
    }
    if (pOid->pu4_OidList[u4Count] > MAX_INT)
    {
        SPRINTF ((char *) au1Array, "%u", pOid->pu4_OidList[u4Count]);
    }
    else
    {
        SPRINTF ((char *) au1Array, "%d", pOid->pu4_OidList[u4Count]);
    }
    HTTP_STRCAT (pu1String, au1Array);
    return;
}

/************************************************************************
 *  Function Name   : WebnmConvertBitsStringToOctet
 *  Description     : Convert string to BITS Type
 *  Input           : pu1Data - Bits String to be converted into Octet
 *  Output          : pOctetStrValue - Pointer where the Octet Value will be 
 *                    written.
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
WebnmConvertBitsStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                               INT4 i4Value)
{
    UINT4               u4Temp = 0, u4Count = 0;
    UINT1               u1Len = 0;
    INT4                i4Val = i4Value;
    pOctetStrValue->i4_Length = 0;

    if (i4Value & (INT4) 0x000000FF)
    {
        u1Len = 1;
    }
    if (i4Value & (INT4) 0x0000FF00)
    {
        u1Len = 2;
    }
    if (i4Value & (INT4) 0x00FF0000)
    {
        u1Len = 3;
    }
    if (i4Value & (INT4) 0xFF000000)
    {
        u1Len = 4;
    }

    for (u4Count = (UINT1) (u1Len); u4Count > 0; u4Count--)
    {
        u4Temp = (i4Val & 0xff);
        pOctetStrValue->pu1_OctetList[u4Count - 1] = (UINT1) (u4Temp);
        i4Val = i4Val >> ENM_BYTE_LEN;
    }

    pOctetStrValue->i4_Length = (INT4) u1Len;

    return ENM_SUCCESS;
}
