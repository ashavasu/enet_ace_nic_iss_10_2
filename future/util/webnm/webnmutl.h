/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: webnmutl.h,v 1.7 2015/04/28 12:51:07 siva Exp $
 *
 * Description: Prototypes of WebNM utilitiy functions.
 *******************************************************************/

#ifndef _WEBNMUTL_H_
#define _WEBNMUTL_H_

/*Macro's used in webnmutl.c*/
#define MAX_INT     2147483647
#define WEBNM_MAX_STRING 128

/* MultiData Related Proto Types */
VOID WebnmConvertDataToString    PROTO ((tSNMP_MULTI_DATA_TYPE *, UINT1*,
                                         UINT1));
tSNMP_MULTI_DATA_TYPE *
WebnmConvertStringToData         PROTO ((UINT1 *, UINT1));

/* OID/OCTET Related Proto Types */
VOID WebnmConvertOidToString     PROTO ((tSNMP_OID_TYPE *, UINT1*));
INT4 WebnmConvertStringToOid     PROTO ((tSNMP_OID_TYPE *, UINT1 *,UINT1));
INT4 WebnmConvertColonStringToOid     PROTO ((tSNMP_OID_TYPE *, UINT1 *,UINT1));
VOID WebnmCopyOid                PROTO ((tSNMP_OID_TYPE *, tSNMP_OID_TYPE *));
INT1 WebnmConvertSubOid          PROTO ((UINT1 **, UINT4 *));
INT1 WebnmHexConvertSubOid       PROTO ((UINT1 **, UINT4 *));
INT4 WebnmConvertStringToOctet   PROTO ((tSNMP_OCTET_STRING_TYPE *, UINT1 *));
INT4 WebnmConvertColonStringToOctet   PROTO ((tSNMP_OCTET_STRING_TYPE *, UINT1 *));
VOID WebnmGetInstanceOid         PROTO ((tSNMP_OID_TYPE *, tSNMP_OID_TYPE *,
                                         tSNMP_OID_TYPE * ));
INT4 WebnmConvertBitsStringToOctet PROTO((tSNMP_OCTET_STRING_TYPE * , INT4));
extern INT4 IssGetStringPort PROTO ((UINT1 *, INT4 , UINT1 *));
/*#ifdef WEBNM_WANTED*/
UINT1 *
allocmem_EnmBlk PROTO ((VOID));
VOID
free_EnmBlk PROTO((UINT1 *));

extern tMemPoolId gHttpMaxEnmPoolId;
/*#endif*/
#endif
