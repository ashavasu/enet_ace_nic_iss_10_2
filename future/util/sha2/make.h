##############################################################################
# Copyright (C) 2011 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.1 2015/04/28 12:53:05 siva Exp $
#
# Description: Specifies the options and modules to be included for building 
#              sha2 module 
##############################################################################
include ../../LR/make.h
include ../../LR/make.rule

SHA2_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
    ${SYSTEM_COMPILATION_SWITCHES}

COMN_INCL_DIR = $(BASE_DIR)/inc
CRYPTO_SHA2_BASE_DIR = $(BASE_DIR)/util/sha2
CRYPTO_SHA2_SRC_DIR = $(CRYPTO_SHA2_BASE_DIR)/src
CRYPTO_SHA2_INC_DIR = $(CRYPTO_SHA2_BASE_DIR)/inc
CRYPTO_SHA2_OBJ_DIR = $(CRYPTO_SHA2_BASE_DIR)/obj

CRYPTO_INCLUDES = -I$(CRYPTO_SHA2_INC_DIR) -I$(COMN_INCL_DIR) -I ${COMMON_INCLUDE_DIRS}

