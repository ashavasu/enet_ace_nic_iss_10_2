/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sha2prot.h,v 1.1 2015/04/28 12:53:48 siva Exp $
 *            
 * Description: This file contains proto types required for sha2 module
 *****************************************************************************/
#ifndef _SHA2_PROT_H_
#define _SHA2_PROT_H_
/* SHA-224 */
PUBLIC INT4 Sha224Reset (tSha224Context *);
PUBLIC INT4 Sha224Input PROTO ((tSha224Context *, CONST UINT1 *,
		           UINT4));

PUBLIC INT4 Sha224FinalBits(tSha224Context *, CONST UINT1 bits,
                           UINT4 bitcount);
PUBLIC INT4 Sha224Result(tSha224Context *,
                        UINT1 Message_Digest[SHA224_HASH_SIZE]);

/* SHA-256 */
PUBLIC INT4 Sha256Reset(tSha256Context *);
PUBLIC INT4 Sha256Input(tSha256Context *, CONST UINT1 *u1bytes,
                       UINT4 bytecount);
PUBLIC INT4 Sha256FinalBits(tSha256Context *, CONST UINT1 u1bits,
                           UINT4 bitcount);
PUBLIC INT4 Sha256Result(tSha256Context *,
                        UINT1 u1Message_Digest[SHA256_HASH_SIZE]);

/* SHA-384 */
PUBLIC INT4 Sha384Reset(tSha384Context *);
PUBLIC INT4 Sha384Input(tSha384Context *, CONST UINT1 *u1bytes,
                       UINT4 bytecount);
PUBLIC INT4 Sha384FinalBits(tSha384Context *, CONST UINT1 u1bits,
                           UINT4 u4bitcount);
PUBLIC INT4 Sha384Result(tSha384Context *,
                        UINT1 u1Message_Digest[SHA384_HASH_SIZE]);

/* SHA-512 */
PUBLIC INT4 Sha512Reset(tSha512Context *);
PUBLIC INT4 Sha512Input(tSha512Context *, CONST UINT1 *bytes,
                       UINT4 u4bytecount);
PUBLIC INT4 Sha512FinalBits(tSha512Context *, CONST UINT1 u1bits,
                           UINT4 bitcount);
PUBLIC INT4 Sha512Result(tSha512Context *,
                        UINT1 Message_Digest[SHA512_HASH_SIZE]);

#endif /* _SHA2_PROT_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  sha2prot.h                      */
/*-----------------------------------------------------------------------*/
