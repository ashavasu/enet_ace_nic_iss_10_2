/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: sha2.h,v 1.1 2015/04/28 12:53:48 siva Exp $
 *
 * Description: It contains sha2 related common typedefs and prototypes 
 ********************************************************************/
#ifndef _SHA2_H_
#define _SHA2_H_

#include "utltrc.h"
#include "cryardef.h"

/* This macro is defined to used 32-bit integers. Undefine this macro if
 * 64-bit integers have to be used */
#define USE_32BIT_ONLY 1 
#ifndef USE_32BIT_ONLY
/* FSAP does not support Shift Right, Shift Left, Rotate Right, Rotate Left, 
 * OR, XOR, AND, TILDA operations on 64-bit values. Hence included system hdr
 * directly. */
#include <stdint.h>
#endif /* USE_32BIT_ONLY */


/*
 * If you do not have the ISO standard stdint.h header file, then you
 * must typedef the following:
 *    name              meaning
 *  uint64_t         unsigned 64 bit integer
 *  UINT4         unsigned 32 bit integer
 *  UINT1          unsigned 8 bit integer (i.e., unsigned char)
 *  INT2   integer of >= 16 bits
 *
 *
 * All SHA functions return one of these values.
 */

enum {
    SHA_SUCCESS = 0,
    SHA_NULL,            /* Null pointer parameter */
    SHA_INPUT_TOO_LONG,    /* input data too long */
    SHA_STATE_ERROR,      /* called Input after FinalBits or Result */
    SHA_BAD_PARAM         /* passed a bad parameter */
};


/*
 *  These constants are used in the USHA (unified sha) functions.
 */
typedef enum SHAversion {
    AR_SHA1_ALGO, AR_SHA224_ALGO, AR_SHA256_ALGO, AR_SHA384_ALGO, AR_SHA512_ALGO
} eShaVersion;


/*
 *  These constants hold size information for each of the SHA
 *  hashing operations
 */
enum {
    SHA1_MESSAGE_BLOCK_SIZE = 64, SHA224_MESSAGE_BLOCK_SIZE = 64,
    SHA256_MESSAGE_BLOCK_SIZE = 64, SHA384_MESSAGE_BLOCK_SIZE = 128,
    SHA512_MESSAGE_BLOCK_SIZE = 128,
    USHA_MAX_MESSAGE_BLOCK_SIZE = SHA512_MESSAGE_BLOCK_SIZE,

    SHA1_HASH_SIZE = 20, SHA224_HASH_SIZE = 28, SHA256_HASH_SIZE = 32,
    SHA384_HASH_SIZE = 48, SHA512_HASH_SIZE = 64,
    USHA_MAX_HASH_SIZE = SHA512_HASH_SIZE,

    SHA1_HASH_SIZE_BITS = 160, SHA224_HASH_SIZE_BITS = 224,
    SHA256_HASH_SIZE_BITS = 256, SHA384_HASH_SIZE_BITS = 384,
    SHA512_HASH_SIZE_BITS = 512, USHA_MAX_HASH_SIZE_BITS = SHA512_HASH_SIZE_BITS
};


/*
 *   This structure will hold context information for the SHA-1
 *   hashing operation.
 */
typedef struct _Sha1Context 
{
    UINT4 au4IntermediateHash[SHA1_HASH_SIZE/4]; /* Message Digest */

    UINT4 u4LengthLow;                /* Message length in bits */
    UINT4 u4LengthHigh;               /* Message length in bits */


    INT4  i4Computed;                 /* Is the digest computed? */
    INT4  i4Corrupted;                /* Is the digest corrupted? */
    INT2  i2MessageBlock_Index;       /* Message_Block array index */
    UINT1 a1MessageBlock[SHA1_MESSAGE_BLOCK_SIZE];
                                      /* 512-bit message blocks */

    UINT1 au1Resvd[2];
} tSha1Context;

/*
 *  This structure will hold context information for the SHA-256
 *  hashing operation.
 */
typedef struct _Sha256Context
{
    UINT4 au4IntermediateHash[SHA256_HASH_SIZE/4]; /* Message Digest */

    UINT4 u4LengthLow;                /* Message length in bits */
    UINT4 u4LengthHigh;               /* Message length in bits */

    INT4 i4Computed;                  /* Is the digest computed? */
    INT4 i4Corrupted;                 /* Is the digest corrupted? */
 
    INT2 i2MessageBlockIndex;         /* Message_Block array index */
    UINT1 au1MessageBlock[SHA256_MESSAGE_BLOCK_SIZE];
                                      /* 512-bit message blocks */
    UINT1 au1Resv[2];
} tSha256Context;


/*
 *  This structure will hold context information for the SHA-512
 *  hashing operation.
 */
typedef struct _Sha512Context
{
#ifdef USE_32BIT_ONLY
    UINT4 au4IntermediateHash[SHA512_HASH_SIZE/4]; /* Message Digest  */
    UINT4 u4Length[4];                    /* Message length in bits */
#else /* !USE_32BIT_ONLY */
    uint64_t au8IntermediateHash[SHA512_HASH_SIZE/8]; /* Message Digest */
    uint64_t u8LengthLow, u8LengthHigh;   /* Message length in bits */
#endif /* USE_32BIT_ONLY */

    INT4 i4Computed;                      /* Is the digest computed?*/
    INT4 i4Corrupted;                     /* Is the digest corrupted? */

    INT2 i2MessageBlockIndex;  /* Message_Block array index */
    UINT1 au1MessageBlock[SHA512_MESSAGE_BLOCK_SIZE];
                                        /* 1024-bit message blocks */

    UINT1 au1Resv[2];
} tSha512Context;

/*
 *  This structure will hold context information for the SHA-224
 *  hashing operation. It uses the SHA-256 structure for computation.
 */
typedef struct _Sha256Context tSha224Context;

/*
 *  This structure will hold context information for the SHA-384
 *  hashing operation. It uses the SHA-512 structure for computation.
 */
typedef struct _Sha512Context tSha384Context;



/*
 *  This structure holds context information for all SHA
 *  hashing operations.
 */
typedef struct _UshaContext
{
    INT4 i4WhichSha;               /* which SHA is being used */
    union _Ctx
    {
        tSha1Context sha1Context;
        tSha224Context sha224Context; tSha256Context sha256Context;
        tSha384Context sha384Context; tSha512Context sha512Context;
    } unCtx;

} tUshaContext;

/* Unified SHA functions, chosen by whichSha */
PUBLIC INT4 UshaReset(tUshaContext *, eShaVersion whichSha);

PUBLIC INT4 UshaInput(tUshaContext *,
                     CONST UINT1 *u1Bytes, UINT4 u4ByteCount);

PUBLIC INT4 UshaFinalBits(tUshaContext *, CONST UINT1 u1Bits, 
                          UINT4 u4BitCount);

PUBLIC INT4 UshaResult(tUshaContext *, UINT1 *pu1MessageDigest);
PUBLIC INT4 UshaHashSizeBits(enum SHAversion whichSha);



PUBLIC INT4 UshaBlockSize(enum SHAversion whichSha);

PUBLIC INT4 UshaHashSize(enum SHAversion whichSha);
VOID Sha224ArAlgo (UINT1 *, INT4, UINT1 *);
VOID Sha256ArAlgo (UINT1 *, INT4, UINT1 *);
VOID Sha384ArAlgo (UINT1 *, INT4, UINT1 *);
VOID Sha512ArAlgo (UINT1 *, INT4, UINT1 *);
#endif /* _SHA2_H_  */

/*-----------------------------------------------------------------------*/
/*                       End of the file  sha2.h                         */
/*-----------------------------------------------------------------------*/

