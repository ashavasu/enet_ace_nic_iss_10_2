/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sha2defn.h,v 1.1 2015/04/28 12:53:48 siva Exp $
 *                    
 * Description: This file contains definitions of sha2 module
 *****************************************************************************/
#ifndef _SHA2_DEFN_H_
#define _SHA2_DEFN_H_

#define SHA_CH(x,y,z)        (((x) & (y)) ^ ((~(x)) & (z)))
#define SHA_MAJ(x,y,z)       (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

/*
 * The following definitions are equivalent and potentially faster.
 */


#define SHA_PARITY(x, y, z)  ((x) ^ (y) ^ (z))


/* Define the SHA shift, rotate left and rotate right macro */
#define SHA256_SHR(bits,word)      ((word) >> (bits))
#define SHA256_ROTL(bits,word)                         \
  (((word) << (bits)) | ((word) >> (32-(bits))))
#define SHA256_ROTR(bits,word)                         \
  (((word) >> (bits)) | ((word) << (32-(bits))))

/* Define the SHA SIGMA and sigma macros */
#define SHA256_SIGMA0(word)   \
  (SHA256_ROTR( 2,word) ^ SHA256_ROTR(13,word) ^ SHA256_ROTR(22,word))
#define SHA256_SIGMA1(word)   \
  (SHA256_ROTR( 6,word) ^ SHA256_ROTR(11,word) ^ SHA256_ROTR(25,word))
#define SHA256_SIGMA_0(word)   \
  (SHA256_ROTR( 7,word) ^ SHA256_ROTR(18,word) ^ SHA256_SHR( 3,word))
#define SHA256_SIGMA_1(word)   \
  (SHA256_ROTR(17,word) ^ SHA256_ROTR(19,word) ^ SHA256_SHR(10,word))

/*********end sha256 macro's *****/

/******* below macro's belongs to sha384_512.c ***start ********/

#ifdef USE_32BIT_ONLY
/*
 * Define 64-bit arithmetic in terms of 32-bit arithmetic.
 * Each 64-bit number is represented in a 2-word array.
 * All macros are defined such that the result is the last parameter.
 */

/*
 * Define shift, rotate left and rotate right functions
 */
#define SHA512_SHR(bits, word, ret) (                          \
    /* (((uint64_t)((word))) >> (bits)) */                     \
    (ret)[0] = (((bits) < 32) && ((bits) >= 0)) ?              \
      ((word)[0] >> (bits)) : 0,                               \
    (ret)[1] = ((bits) > 32) ? ((word)[0] >> ((bits) - 32)) :  \
      ((bits) == 32) ? (word)[0] :                             \
      ((bits) >= 0) ?                                          \
        (((word)[0] << (32 - (bits))) |                        \
        ((word)[1] >> (bits))) : 0 )

#define SHA512_SHL(bits, word, ret) (                          \
    /* (((uint64_t)(word)) << (bits)) */                       \
    (ret)[0] = ((bits) > 32) ? ((word)[1] << ((bits) - 32)) :  \
         ((bits) == 32) ? (word)[1] :                          \
         ((bits) >= 0) ?                                       \
           (((word)[0] << (bits)) |                            \
           ((word)[1] >> (32 - (bits)))) :                     \
 0,                                                    \
    (ret)[1] = (((bits) < 32) && ((bits) >= 0)) ?              \
        ((word)[1] << (bits)) : 0 )

/*
 * Define 64-bit OR
 */
#define SHA512_OR(word1, word2, ret) (                         \
    (ret)[0] = (word1)[0] | (word2)[0],                        \
    (ret)[1] = (word1)[1] | (word2)[1] )

/*
 * Define 64-bit XOR
 */
#define SHA512_XOR(word1, word2, ret) (                        \
    (ret)[0] = (word1)[0] ^ (word2)[0],                        \
    (ret)[1] = (word1)[1] ^ (word2)[1] )

/*
 * Define 64-bit AND
 */
#define SHA512_AND(word1, word2, ret) (                        \
    (ret)[0] = (word1)[0] & (word2)[0],                        \
    (ret)[1] = (word1)[1] & (word2)[1] )


/*
 * Define 64-bit TILDA
 */
#define SHA512_TILDA(word, ret)                                \
  ( (ret)[0] = ~(word)[0], (ret)[1] = ~(word)[1] )

/*
 * Define 64-bit ADD
 */
#define SHA512_ADD(word1, word2, ret) (                        \
    (ret)[1] = (word1)[1], (ret)[1] += (word2)[1],             \
    (ret)[0] = (word1)[0] + (word2)[0] + ((ret)[1] < (word1)[1]) )


#endif /*  USE_32BIT_ONLY */
#endif /* _SHA2_DEFN_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  sha2defn.h                     */
/*-----------------------------------------------------------------------*/



