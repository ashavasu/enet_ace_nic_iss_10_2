/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sha2inc.h,v 1.1 2015/04/28 12:53:48 siva Exp $
 *
 * Description: This file contains header files required for sha2 module   
 *****************************************************************************/
#ifndef _SHA2_INC_H
#define _SHA2_INC_H
#include "lr.h"
#include "cli.h"
#include "cryardef.h"
#include "cryartdf.h"
#include "utltrc.h"
#include "sha2.h" 
#include "sha2prot.h"
#include "sha2defn.h"

#endif /* _SHA2_INC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  sha2inc.h                      */
/*-----------------------------------------------------------------------*/
