/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: sha384_512.c,v 1.1 2015/04/28 12:53:48 siva Exp $
 *
 * Description:
 *  "The algorithm implemented is from the RFC 4634. The code from the
 *   RFC 4634 has been modified to conform to organizational coding guidelines"
 *
 *   This file implements the Secure Hash Signature Standard algorithms as 
 *   defined in the National Institute of Standards and Technology Federal 
 *   Information Processing Standards Publication (FIPS PUB) 180-1 published 
 *   on April 17, 1995, 180-2 published on August 1, 2002, and the FIPS PUB 
 *   180-2 Change Notice published on February 28, 2004.
 *
 *   The SHA-384 and SHA-512 algorithms produce 384-bit and 512-bit message 
 *   digests for a given data stream. It should take about 2**n steps to find
 *   a message with the same digest as a given message and 2**(n/2) to find 
 *   any two messages with the same digest, when n is the digest size in bits. 
 *   Therefore, this algorithm can serve as a means of providing a  
 *   "fingerprint" for a message.
 *
 *   SHA-384 and SHA-512 are designed to work with messages less than 2^128 
 *   bits long. This implementation uses SHA384/512Input() to hash the bits 
 *   that are a multiple of the size of an 8-bit character, and then uses 
 *   SHA384/256FinalBits() to hash the final few bits of the input.
 *
 ******************************************************************************/
#include "sha2inc.h"

/* Local Function Prototypes */
PRIVATE VOID        Sha384_512Finalize (tSha512Context * pContext,
                                        UINT1 Pad_Byte);
PRIVATE VOID        Sha384_512PadMessage (tSha512Context * pContext,
                                          UINT1 Pad_Byte);
PRIVATE VOID        Sha384_512ProcessMessageBlock (tSha512Context * pContext);
PRIVATE INT4        Sha384_512ResultN (tSha512Context * pContext,
                                       UINT1 *pu1MessageDigest, INT4 HashSize);
#ifdef USE_32BIT_ONLY
PRIVATE INT4        Sha384_512Reset (tSha512Context * pContext,
                                     UINT4 *pu4Htemp0);
#else
PRIVATE INT4        Sha384_512Reset (tSha512Context * pContext,
                                     uint64_t * pu8Htemp0);
#endif

#ifdef USE_32BIT_ONLY
/*
 * Add the 4word value in word2 to word1.
 */
PRIVATE UINT4       gu4AddToTemp, gu4AddToTemp2;
#define SHA512_ADDTO4(word1, word2) (gu4AddToTemp = (word1)[3], \
 (word1)[3] += (word2)[3], gu4AddToTemp2 = (word1)[2],          \
 (word1)[2] += (word2)[2] + ((word1)[3] < gu4AddToTemp), gu4AddToTemp = \
 (word1)[1], (word1)[1] += (word2)[1] + ((word1)[2] < gu4AddToTemp2),    \
    (word1)[0] += (word2)[0] + ((word1)[1] < gu4AddToTemp) )

/*
 * Add the 2word value in word2 to word1.
 */
#define SHA512_ADDTO2(word1, word2) (                          \
    gu4AddToTemp = (word1)[1],                                  \
    (word1)[1] += (word2)[1],                                  \
    (word1)[0] += (word2)[0] + ((word1)[1] < gu4AddToTemp) )

/*
 * SHA rotate   ((word >> bits) | (word << (64-bits)))
 */
PRIVATE UINT4       gau4RotrTemp1[2], gau4RotrTemp2[2];
#define SHA512_ROTR(bits, word, ret) (                         \
    SHA512_SHR((bits), (word), gau4RotrTemp1),                    \
    SHA512_SHL(64-(bits), (word), gau4RotrTemp2),                 \
    SHA512_OR(gau4RotrTemp1, gau4RotrTemp2, (ret)) )

/*
 * Define the SHA SIGMA and sigma macros
 *  SHA512_ROTR(28,word) ^ SHA512_ROTR(34,word) ^ SHA512_ROTR(39,word)
 */
PRIVATE UINT4       gau4Sigma0Temp1[2], gau4Sigma0Temp2[2],
    gau4Sigma0Temp3[2], gau4Sigma0Temp4[2];
#define SHA512_SIGMA0(word, ret) (                             \
    SHA512_ROTR(28, (word), gau4Sigma0Temp1),                     \
    SHA512_ROTR(34, (word), gau4Sigma0Temp2),                     \
    SHA512_ROTR(39, (word), gau4Sigma0Temp3),                     \
    SHA512_XOR(gau4Sigma0Temp2, gau4Sigma0Temp3, gau4Sigma0Temp4),      \
    SHA512_XOR(gau4Sigma0Temp1, gau4Sigma0Temp4, (ret)) )

/*
 * SHA512_ROTR(14,word) ^ SHA512_ROTR(18,word) ^ SHA512_ROTR(41,word)
 */
PRIVATE UINT4       gau4Sigma1Temp1[2], gau4Sigma1Temp2[2],
    gau4Sigma1Temp3[2], gau4Sigma1Temp4[2];
#define SHA512_SIGMA1(word, ret) (                             \
    SHA512_ROTR(14, (word), gau4Sigma1Temp1),                     \
    SHA512_ROTR(18, (word), gau4Sigma1Temp2),                     \
    SHA512_ROTR(41, (word), gau4Sigma1Temp3),                     \
    SHA512_XOR(gau4Sigma1Temp2, gau4Sigma1Temp3, gau4Sigma1Temp4),      \
    SHA512_XOR(gau4Sigma1Temp1, gau4Sigma1Temp4, (ret)) )

/*
 * (SHA512_ROTR( 1,word) ^ SHA512_ROTR( 8,word) ^ SHA512_SHR( 7,word))
 */

PRIVATE UINT4       gau4Sigma0_Temp1[2], gau4Tigma0_Temp2[2],
    gau4Tigma0_Temp3[2], gau4Tigma0_Temp4[2];
#define SHA512_SIGMA_0(word, ret) (                             \
    SHA512_ROTR( 1, (word), gau4Sigma0_Temp1),                     \
    SHA512_ROTR( 8, (word), gau4Tigma0_Temp2),                     \
    SHA512_SHR( 7, (word), gau4Tigma0_Temp3),                      \
    SHA512_XOR(gau4Tigma0_Temp2, gau4Tigma0_Temp3, gau4Tigma0_Temp4),      \
    SHA512_XOR(gau4Sigma0_Temp1, gau4Tigma0_Temp4, (ret)) )

/*
 * (SHA512_ROTR(19,word) ^ SHA512_ROTR(61,word) ^ SHA512_SHR( 6,word))
 */
PRIVATE UINT4       gau4Tigma1_Temp1[2], gau4Sigma1_Temp2[2],
    gau4Sigma1_Temp3[2], gau4Sigma1_Temp4[2];
#define SHA512_SIGMA_1(word, ret) (                             \
    SHA512_ROTR(19, (word), gau4Tigma1_Temp1),                     \
    SHA512_ROTR(61, (word), gau4Sigma1_Temp2),                     \
    SHA512_SHR( 6, (word), gau4Sigma1_Temp3),                      \
    SHA512_XOR(gau4Sigma1_Temp2, gau4Sigma1_Temp3, gau4Sigma1_Temp4),      \
    SHA512_XOR(gau4Tigma1_Temp1, gau4Sigma1_Temp4, (ret)) )

#undef SHA_CH
#undef SHA_MAJ

/*
 * These definitions are the ones used in FIPS-180-2, section 4.1.3
 *  Ch(x,y,z)   ((x & y) ^ (~x & z))
 */
PRIVATE UINT4       gau4ChTemp1[2], gau4ChTemp2[2], gau4ChTemp3[2];
#define SHA_CH(x, y, z, ret) (                                 \
    SHA512_AND(x, y, gau4ChTemp1),                                \
    SHA512_TILDA(x, gau4ChTemp2),                                 \
    SHA512_AND(gau4ChTemp2, z, gau4ChTemp3),                         \
    SHA512_XOR(gau4ChTemp1, gau4ChTemp3, (ret)) )
/*
 *  Maj(x,y,z)  (((x)&(y)) ^ ((x)&(z)) ^ ((y)&(z)))
 */
PRIVATE UINT4       gau4MajTemp1[2], gau4MajTemp2[2], gau4MajTemp3[2],
    gau4MajTemp4[2];
#define SHA_MAJ(x, y, z, ret) (                                \
    SHA512_AND(x, y, gau4MajTemp1),                               \
    SHA512_AND(x, z, gau4MajTemp2),                               \
    SHA512_AND(y, z, gau4MajTemp3),                               \
    SHA512_XOR(gau4MajTemp2, gau4MajTemp3, gau4MajTemp4),               \
    SHA512_XOR(gau4MajTemp1, gau4MajTemp4, (ret)) )

/*
 * add "length" to the length
 */
PRIVATE UINT4       gau4addTemp[4] = { 0, 0, 0, 0 };

#define SHA384_512ADD_LENGTH(context, length) (                        \
 gau4addTemp[3] = (length), SHA512_ADDTO4((context)->u4Length, gau4addTemp), \
    (context)->i4Corrupted = (((context)->u4Length[3] == 0) && ((context)->\
    u4Length[2] == 0) && ((context)->u4Length[1] == 0) && ((context)->\
    u4Length[0] < 8)) ? 1 : 0 )

/* Initial Hash Values: FIPS-180-2 sections 5.3.3 and 5.3.4 */
PRIVATE UINT4       gau4Sha384_H0[SHA512_HASH_SIZE / 4] = {
    0xCBBB9D5D, 0xC1059ED8, 0x629A292A, 0x367CD507, 0x9159015A,
    0x3070DD17, 0x152FECD8, 0xF70E5939, 0x67332667, 0xFFC00B31,
    0x8EB44A87, 0x68581511, 0xDB0C2E0D, 0x64F98FA7, 0x47B5481D,
    0xBEFA4FA4
};

PRIVATE UINT4       gau4Sha512_H0[SHA512_HASH_SIZE / 4] = {
    0x6A09E667, 0xF3BCC908, 0xBB67AE85, 0x84CAA73B, 0x3C6EF372,
    0xFE94F82B, 0xA54FF53A, 0x5F1D36F1, 0x510E527F, 0xADE682D1,
    0x9B05688C, 0x2B3E6C1F, 0x1F83D9AB, 0xFB41BD6B, 0x5BE0CD19,
    0x137E2179
};

#else /* !USE_32BIT_ONLY */

/* Define the SHA shift, rotate left and rotate right macro */
#define SHA512_SHR(bits,word)  (((uint64_t)(word)) >> (bits))
#define SHA512_ROTR(bits,word) ((((uint64_t)(word)) >> (bits)) | \
                                (((uint64_t)(word)) << (64-(bits))))

/* Define the SHA SIGMA and sigma macros */
#define SHA512_SIGMA0(word)   \
 (SHA512_ROTR(28,word)^SHA512_ROTR(34,word)^SHA512_ROTR(39,word))
#define SHA512_SIGMA1(word)   \
 (SHA512_ROTR(14,word)^SHA512_ROTR(18,word)^SHA512_ROTR(41,word))
#define SHA512_SIGMA_0(word)   \
 (SHA512_ROTR( 1,word)^SHA512_ROTR( 8,word)^SHA512_SHR( 7,word))
#define SHA512_SIGMA_1(word)   \
 (SHA512_ROTR(19,word)^SHA512_ROTR(61,word)^SHA512_SHR( 6,word))

/*
 * add "length" to the length
 */
PRIVATE uint64_t    gu8addTemp;
#define SHA384_512ADD_LENGTH(context, length)                   \
   (gu8addTemp = context->u8LengthLow, context->i4Corrupted =        \
   ((context->u8LengthLow += length) < gu8addTemp) && (++context->\
   u8LengthHigh == 0) ? 1 : 0)

/* Initial Hash Values: FIPS-180-2 sections 5.3.3 and 5.3.4 */
PRIVATE uint64_t    gau8Sha384_H0[] = {
    0xCBBB9D5DC1059ED8ll, 0x629A292A367CD507ll, 0x9159015A3070DD17ll,
    0x152FECD8F70E5939ll, 0x67332667FFC00B31ll, 0x8EB44A8768581511ll,
    0xDB0C2E0D64F98FA7ll, 0x47B5481DBEFA4FA4ll
};
PRIVATE uint64_t    gau8Sha512_H0[] = {
    0x6A09E667F3BCC908ll, 0xBB67AE8584CAA73Bll, 0x3C6EF372FE94F82Bll,
    0xA54FF53A5F1D36F1ll, 0x510E527FADE682D1ll, 0x9B05688C2B3E6C1Fll,
    0x1F83D9ABFB41BD6Bll, 0x5BE0CD19137E2179ll
};

#endif /* USE_32BIT_ONLY */
/*******************************************************************************
 * Function           : Sha384Reset
 *
 * Description        : This function will initialize the SHA384Context in
 *                      preparation for computing a new SHA384 message digest.
 *
 * Input(s)           : pContext - The pContext to reset
 *
 * Output(s)          : pContext - that contains initialized u4IntermediateHash
 *                                 to context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha384Reset (tSha384Context * pContext)
{
#ifdef USE_32BIT_ONLY
    return Sha384_512Reset (pContext, gau4Sha384_H0);
#else
    return Sha384_512Reset (pContext, gau8Sha384_H0);
#endif
}

/*******************************************************************************
 * Function           : Sha384Input
 *
 * Description        : This function accepts an array of octets as the next 
 *                      portion of the message.
 *
 * Input(s)           : pContext - The pContext to update
 *                      pu1MessageArray - An array of characters representing 
 *                                        the next portion of the message.
 *                      u4Length - The u4Length of the message in 
 *                                 pu1MessageArray
 * Output(s)          : pContext - that contains computed Intermediated hash
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha384Input (tSha384Context * pContext,
             CONST UINT1 *pu1MessageArray, UINT4 u4Length)
{
    return Sha512Input (pContext, pu1MessageArray, u4Length);
}

/*******************************************************************************
 * Function           : Sha384FinalBits
 *
 * Description        : This function will add in any final bits of the message.
 *
 * Input(s)           : pContext - The pContext to update                                                                                 u1MessageBits - The final bits of the message, in the 
 *                      upper portion of the byte. (Use 0b###00000 instead of 
 *                      0b00000### to input the three bits ###.)
 *                      u4Length - The u4Length of the message in 
 *                                 pu1MessageArray
 * Output(s)          : pContext - that contains computed Intermediated hash
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha384FinalBits (tSha384Context * pContext,
                 CONST UINT1 u1MessageBits, UINT4 u4Length)
{
    return Sha512FinalBits (pContext, u1MessageBits, u4Length);

}

/*******************************************************************************
 * Function           :  SHA384Result
 *
 * Description        : This function will return the 384-bit message digest 
                        into the pu1MessageDigest array provided by the caller.
 *                      NOTE: The first octet of hash is stored in the 0th 
                        element, the last octet of hash in the 48th element.
 *
 * Input(s)           : pContext - The pContext to use to calculate the SHA 
 *                                 hash.
 * Output(s)          : pu1MessageDigest - Where the digest is returned.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha384Result (tSha384Context * pContext, UINT1 *pu1MessageDigest)
{
    return Sha384_512ResultN (pContext, pu1MessageDigest, SHA384_HASH_SIZE);
}

/*******************************************************************************
 * Function           :  Sha512Reset
 *
 * Description        : This function will initialize the tSha512Context in 
 *                      preparation for computing a new SHA512 message digest.
 *
 * Input(s)           : pContext - The pContext to use reset.
 * Output(s)          : pu1MessageDigest -  that contains initialized u4IntermediateHash 
 *                                          context info 
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha512Reset (tSha512Context * pContext)
{
#ifdef USE_32BIT_ONLY
    return Sha384_512Reset (pContext, gau4Sha512_H0);
#else
    return Sha384_512Reset (pContext, gau8Sha512_H0);
#endif
}

/*******************************************************************************
 * Function           :  Sha512Input
 *
 * Description        : This function accepts an array of octets as the next 
 *                      portion of the message.
 *
 * Input(s)           : pContext - The pContext to use to update.
 *                      pu1MessageArray - An array of characters representing 
 *                                        the next portion of the message. 
 *                      u4Length - The u4Length of the message in 
 *                                 pu1MessageArray
 *
 * Output(s)          : that contains computed Intermediated hash info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha512Input (tSha512Context * pContext,
             CONST UINT1 *pu1MessageArray, UINT4 u4Length)
{
    if (u4Length == 0)
    {
        return OSIX_SUCCESS;
    }

    if ((pContext == NULL) || (pu1MessageArray == NULL))
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\nSha512Input: Input pContext or pu1MessageArray are null\n");
#endif
        return OSIX_FAILURE;
    }

    if (pContext->i4Computed != 0)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\nSha512Input: pContext is corrupted\n");
#endif
        pContext->i4Corrupted = SHA_STATE_ERROR;
        return OSIX_FAILURE;
    }

    while ((u4Length--) && (pContext->i4Corrupted) == (INT4) 0)
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] =
            (*pu1MessageArray & 0xFF);

        if (!SHA384_512ADD_LENGTH (pContext, 8) &&
            (pContext->i2MessageBlockIndex == SHA512_MESSAGE_BLOCK_SIZE))
        {
            Sha384_512ProcessMessageBlock (pContext);
        }

        pu1MessageArray++;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           :  Sha512FinalBits
 *
 * Description        : this function will add in any final bits of the message.
 *                      
 *
 * Input(s)           : pContext - The pContext to use to update.
 *                      u1MessageBits - The final bits of the message, in the   
 *                                      portion of the byte. (Use 0b###00000
 *                                      instead of 0b00000### to input the 
 *                                      three bits ###.) 
 *                      u4Length - The number of bits in message_bits, between 1
                                    and 7.
 *
 * Output(s)          : pContext - that contains updated  updated final bits to 
 *                                 context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha512FinalBits (tSha512Context * pContext,
                 CONST UINT1 u1MessageBits, UINT4 u4Length)
{
    UINT1               au1masks[8] = {
        /* 0 0b00000000 */ 0x00, /* 1 0b10000000 */ 0x80,
        /* 2 0b11000000 */ 0xC0, /* 3 0b11100000 */ 0xE0,
        /* 4 0b11110000 */ 0xF0, /* 5 0b11111000 */ 0xF8,
        /* 6 0b11111100 */ 0xFC, /* 7 0b11111110 */ 0xFE
    };
    UINT1               au1markbit[8] = {
        /* 0 0b10000000 */ 0x80, /* 1 0b01000000 */ 0x40,
        /* 2 0b00100000 */ 0x20, /* 3 0b00010000 */ 0x10,
        /* 4 0b00001000 */ 0x08, /* 5 0b00000100 */ 0x04,
        /* 6 0b00000010 */ 0x02, /* 7 0b00000001 */ 0x01
    };

    if (u4Length == (UINT4) 0)
    {
        return OSIX_SUCCESS;
    }

    if (pContext == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n Sha512FinalBits: Input parameter pContext is a null\n");
#endif
        return OSIX_FAILURE;
    }

    if ((pContext->i4Computed) || (u4Length >= 8) || (u4Length == 0))
    {
        pContext->i4Corrupted = SHA_STATE_ERROR;
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n Sha512FinalBits: Input parameter pContext corrupted so"
             " returning sha state error\n");
#endif
        return OSIX_FAILURE;
    }

    SHA384_512ADD_LENGTH (pContext, u4Length);
    Sha384_512Finalize (pContext, (UINT1)
                        ((u1MessageBits & au1masks[u4Length]) |
                         au1markbit[u4Length]));

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           :  SHA384_512Finalize
 *
 * Description        : This helper function finishes off the digest 
 *                      calculations.
 *
 *
 * Input(s)           : pContext - The pContext to use to update.
 *                      Pad_Byte - The last byte to add to the digest before 
 *                                 the 0-padding and u4Length. This will 
 *                                 contain the last bits of the message 
 *                                 followed by another single bit. If the 
 *                                 message was an exact multiple 8-bits long, 
 *                                 Pad_Byte will be 0x80.
 * Output(s)          : that contains the calculated digest.
 * 
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE VOID
Sha384_512Finalize (tSha512Context * pContext, UINT1 u1PadByte)
{
    INT2                i2Count = 0;
    Sha384_512PadMessage (pContext, u1PadByte);
    /* message may be sensitive, clear it out */
    for (i2Count = 0; i2Count < SHA512_MESSAGE_BLOCK_SIZE; ++i2Count)
    {
        pContext->au1MessageBlock[i2Count] = 0;
    }
#ifdef USE_32BIT_ONLY            /* and clear u4Length */
    pContext->u4Length[0] = pContext->u4Length[1] = 0;
    pContext->u4Length[2] = pContext->u4Length[3] = 0;
#else /* !USE_32BIT_ONLY */
    pContext->u8LengthLow = 0;
    pContext->u8LengthHigh = 0;
#endif /* USE_32BIT_ONLY */
    pContext->i4Computed = 1;
}

/*******************************************************************************
 * Function           :  Sha512Result
 *
 * Description        : This function will return the 512-bit message
 *                      digest into the pu1MessageDigest array provided by the
 *                      caller.
                         NOTE: The first octet of hash is stored in the 0th
 *                       element, the last octet of hash in the 64th element.

 *
 *
 * Input(s)           : pContext - The pContext to use to calculate the SHA 
 *                                 hash.
 * Output(s)          : pu1MessageDigest - Where the digest is returned
                        pContext - That contains updated Intermediate hash            
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha512Result (tSha512Context * pContext, UINT1 *pu1MessageDigest)
{
    return Sha384_512ResultN (pContext, pu1MessageDigest, SHA512_HASH_SIZE);
}

/*******************************************************************************
 * Function           :  Sha384_512PadMessage
 *
 * Description        :  According to the standard, the message must be padded 
 *                       to an even 1024 bits. The first padding bit must be 
 *                       a '1'. The last 128 bits represent the u4Length of the
 *                       original message. All bits in between should be 0. 
 *                       This helper function will pad the message according to
 *                        those rules by filling the au1MessageBlock array 
 *                        accordingly. When it returns, it can be assumed that 
 *                        the message digest has been computed.
 *
 *
 * Input(s)           : pContext - The pContext to pad
 *                      Pad_Byte - The last byte to add to the digest before the 
 *                                 0-paddingand u4Length. This willcontain the
 *                                 last bits of the message followed by another
 *                                 single bit. If the message was anexact 
 *                                 multiple of 8-bits long, Pad_Byte will be 
 *                                 0x80.
 * Output(s)          : pu1MessageDigest - Where the digest is returned
                        pContext - That contains the updated message parsed context 
                                   
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE VOID
Sha384_512PadMessage (tSha512Context * pContext, UINT1 u1PadByte)
{
    /*
     * Check to see if the current message block is too small to hold
     * the initial padding bits and u4Length. If so, we will pad the
     * block, process it, and then continue padding into a second
     * block.
     */
    if (pContext->i2MessageBlockIndex >= (SHA512_MESSAGE_BLOCK_SIZE - 16))
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = u1PadByte;
        while (pContext->i2MessageBlockIndex < SHA512_MESSAGE_BLOCK_SIZE)
        {
            pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = 0;
        }

        Sha384_512ProcessMessageBlock (pContext);
    }
    else
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = u1PadByte;
    }

    while (pContext->i2MessageBlockIndex < (SHA512_MESSAGE_BLOCK_SIZE - 16))
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = 0;
    }

    /*
     * Store the message u4Length as the last 16 octets
     */
#ifdef USE_32BIT_ONLY
    pContext->au1MessageBlock[112] = (UINT1) (pContext->u4Length[0] >> 24);
    pContext->au1MessageBlock[113] = (UINT1) (pContext->u4Length[0] >> 16);
    pContext->au1MessageBlock[114] = (UINT1) (pContext->u4Length[0] >> 8);
    pContext->au1MessageBlock[115] = (UINT1) (pContext->u4Length[0]);
    pContext->au1MessageBlock[116] = (UINT1) (pContext->u4Length[1] >> 24);
    pContext->au1MessageBlock[117] = (UINT1) (pContext->u4Length[1] >> 16);
    pContext->au1MessageBlock[118] = (UINT1) (pContext->u4Length[1] >> 8);
    pContext->au1MessageBlock[119] = (UINT1) (pContext->u4Length[1]);

    pContext->au1MessageBlock[120] = (UINT1) (pContext->u4Length[2] >> 24);
    pContext->au1MessageBlock[121] = (UINT1) (pContext->u4Length[2] >> 16);
    pContext->au1MessageBlock[122] = (UINT1) (pContext->u4Length[2] >> 8);
    pContext->au1MessageBlock[123] = (UINT1) (pContext->u4Length[2]);
    pContext->au1MessageBlock[124] = (UINT1) (pContext->u4Length[3] >> 24);
    pContext->au1MessageBlock[125] = (UINT1) (pContext->u4Length[3] >> 16);
    pContext->au1MessageBlock[126] = (UINT1) (pContext->u4Length[3] >> 8);
    pContext->au1MessageBlock[127] = (UINT1) (pContext->u4Length[3]);
#else /* !USE_32BIT_ONLY */
    pContext->au1MessageBlock[112] = (UINT1) (pContext->u8LengthHigh >> 56);
    pContext->au1MessageBlock[113] = (UINT1) (pContext->u8LengthHigh >> 48);
    pContext->au1MessageBlock[114] = (UINT1) (pContext->u8LengthHigh >> 40);
    pContext->au1MessageBlock[115] = (UINT1) (pContext->u8LengthHigh >> 32);
    pContext->au1MessageBlock[116] = (UINT1) (pContext->u8LengthHigh >> 24);
    pContext->au1MessageBlock[117] = (UINT1) (pContext->u8LengthHigh >> 16);
    pContext->au1MessageBlock[118] = (UINT1) (pContext->u8LengthHigh >> 8);
    pContext->au1MessageBlock[119] = (UINT1) (pContext->u8LengthHigh);

    pContext->au1MessageBlock[120] = (UINT1) (pContext->u8LengthLow >> 56);
    pContext->au1MessageBlock[121] = (UINT1) (pContext->u8LengthLow >> 48);
    pContext->au1MessageBlock[122] = (UINT1) (pContext->u8LengthLow >> 40);
    pContext->au1MessageBlock[123] = (UINT1) (pContext->u8LengthLow >> 32);
    pContext->au1MessageBlock[124] = (UINT1) (pContext->u8LengthLow >> 24);
    pContext->au1MessageBlock[125] = (UINT1) (pContext->u8LengthLow >> 16);
    pContext->au1MessageBlock[126] = (UINT1) (pContext->u8LengthLow >> 8);
    pContext->au1MessageBlock[127] = (UINT1) (pContext->u8LengthLow);
#endif /* USE_32BIT_ONLY */

    Sha384_512ProcessMessageBlock (pContext);
}

/*******************************************************************************
 * Function           :  Sha384_512PadMessage
 *
 * Description        :  This helper function will process the next 1024 bits of 
 *                        the message stored in the au1MessageBlock array.
 *
 * Input(s)           : pContext - The pContext to update
 * Output(s)          : pContext - That contains the updated message padded or 
 *                                 parsed pContext.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE VOID
Sha384_512ProcessMessageBlock (tSha512Context * pContext)
{
    /* Constants defined in FIPS-180-2, section 4.2.3 */
#ifdef USE_32BIT_ONLY
    PRIVATE CONST UINT4 au4Ktemp[80 * 2] = {
        0x428A2F98, 0xD728AE22, 0x71374491, 0x23EF65CD, 0xB5C0FBCF,
        0xEC4D3B2F, 0xE9B5DBA5, 0x8189DBBC, 0x3956C25B, 0xF348B538,
        0x59F111F1, 0xB605D019, 0x923F82A4, 0xAF194F9B, 0xAB1C5ED5,
        0xDA6D8118, 0xD807AA98, 0xA3030242, 0x12835B01, 0x45706FBE,
        0x243185BE, 0x4EE4B28C, 0x550C7DC3, 0xD5FFB4E2, 0x72BE5D74,
        0xF27B896F, 0x80DEB1FE, 0x3B1696B1, 0x9BDC06A7, 0x25C71235,
        0xC19BF174, 0xCF692694, 0xE49B69C1, 0x9EF14AD2, 0xEFBE4786,
        0x384F25E3, 0x0FC19DC6, 0x8B8CD5B5, 0x240CA1CC, 0x77AC9C65,
        0x2DE92C6F, 0x592B0275, 0x4A7484AA, 0x6EA6E483, 0x5CB0A9DC,
        0xBD41FBD4, 0x76F988DA, 0x831153B5, 0x983E5152, 0xEE66DFAB,
        0xA831C66D, 0x2DB43210, 0xB00327C8, 0x98FB213F, 0xBF597FC7,
        0xBEEF0EE4, 0xC6E00BF3, 0x3DA88FC2, 0xD5A79147, 0x930AA725,
        0x06CA6351, 0xE003826F, 0x14292967, 0x0A0E6E70, 0x27B70A85,
        0x46D22FFC, 0x2E1B2138, 0x5C26C926, 0x4D2C6DFC, 0x5AC42AED,
        0x53380D13, 0x9D95B3DF, 0x650A7354, 0x8BAF63DE, 0x766A0ABB,
        0x3C77B2A8, 0x81C2C92E, 0x47EDAEE6, 0x92722C85, 0x1482353B,
        0xA2BFE8A1, 0x4CF10364, 0xA81A664B, 0xBC423001, 0xC24B8B70,
        0xD0F89791, 0xC76C51A3, 0x0654BE30, 0xD192E819, 0xD6EF5218,
        0xD6990624, 0x5565A910, 0xF40E3585, 0x5771202A, 0x106AA070,

        0x32BBD1B8, 0x19A4C116, 0xB8D2D0C8, 0x1E376C08, 0x5141AB53,
        0x2748774C, 0xDF8EEB99, 0x34B0BCB5, 0xE19B48A8, 0x391C0CB3,
        0xC5C95A63, 0x4ED8AA4A, 0xE3418ACB, 0x5B9CCA4F, 0x7763E373,
        0x682E6FF3, 0xD6B2B8A3, 0x748F82EE, 0x5DEFB2FC, 0x78A5636F,
        0x43172F60, 0x84C87814, 0xA1F0AB72, 0x8CC70208, 0x1A6439EC,
        0x90BEFFFA, 0x23631E28, 0xA4506CEB, 0xDE82BDE9, 0xBEF9A3F7,
        0xB2C67915, 0xC67178F2, 0xE372532B, 0xCA273ECE, 0xEA26619C,
        0xD186B8C7, 0x21C0C207, 0xEADA7DD6, 0xCDE0EB1E, 0xF57D4F7F,
        0xEE6ED178, 0x06F067AA, 0x72176FBA, 0x0A637DC5, 0xA2C898A6,
        0x113F9804, 0xBEF90DAE, 0x1B710B35, 0x131C471B, 0x28DB77F5,
        0x23047D84, 0x32CAAB7B, 0x40C72493, 0x3C9EBE0A, 0x15C9BEBC,
        0x431D67C4, 0x9C100D4C, 0x4CC5D4BE, 0xCB3E42B6, 0x597F299C,
        0xFC657E2A, 0x5FCB6FAB, 0x3AD6FAEC, 0x6C44198C, 0x4A475817
    };
    INT4                i4Temp = 0, i4TempT2 = 0, i4Temp8 = 0;    /* Loop counter */
    UINT4               au4Temp1[2], au4Temp2[2],    /* Temporary word values */
                        au4Temp3[2], au4Temp4[2], au4Temp5[2];
    UINT4               au4Word[2 * 80];    /* Word sequence */
    UINT4               au4Aword[2], au4Bword[2], au4Cword[2],
        au4Dword[2], au4Eword[2], au4Fword[2], au4Gword[2], au4Hword[2];
    UINT4              *pu4Wt2 = NULL;
    UINT4              *pu4Wt7 = NULL;
    UINT4              *pu4Wt15 = NULL;
    UINT4              *pu4Wt16 = NULL;

    MEMSET (au4Temp1, 0, sizeof (au4Temp1));
    MEMSET (au4Temp2, 0, sizeof (au4Temp2));
    MEMSET (au4Temp3, 0, sizeof (au4Temp3));
    MEMSET (au4Temp4, 0, sizeof (au4Temp4));
    MEMSET (au4Temp5, 0, sizeof (au4Temp5));
    MEMSET (au4Aword, 0, sizeof (au4Aword));
    MEMSET (au4Bword, 0, sizeof (au4Bword));
    MEMSET (au4Cword, 0, sizeof (au4Cword));
    MEMSET (au4Dword, 0, sizeof (au4Dword));
    MEMSET (au4Eword, 0, sizeof (au4Eword));
    MEMSET (au4Fword, 0, sizeof (au4Fword));
    MEMSET (au4Gword, 0, sizeof (au4Gword));
    MEMSET (au4Hword, 0, sizeof (au4Hword));
    MEMSET (au4Word, 0, sizeof (au4Word));

#else /* !USE_32BIT_ONLY */
    PRIVATE CONST uint64_t au8Ktemp[80] = {
        0x428A2F98D728AE22ll, 0x7137449123EF65CDll, 0xB5C0FBCFEC4D3B2Fll,
        0xE9B5DBA58189DBBCll, 0x3956C25BF348B538ll, 0x59F111F1B605D019ll,
        0x923F82A4AF194F9Bll, 0xAB1C5ED5DA6D8118ll, 0xD807AA98A3030242ll,
        0x12835B0145706FBEll, 0x243185BE4EE4B28Cll, 0x550C7DC3D5FFB4E2ll,
        0x72BE5D74F27B896Fll, 0x80DEB1FE3B1696B1ll, 0x9BDC06A725C71235ll,
        0xC19BF174CF692694ll, 0xE49B69C19EF14AD2ll, 0xEFBE4786384F25E3ll,
        0x0FC19DC68B8CD5B5ll, 0x240CA1CC77AC9C65ll, 0x2DE92C6F592B0275ll,
        0x4A7484AA6EA6E483ll, 0x5CB0A9DCBD41FBD4ll, 0x76F988DA831153B5ll,
        0x983E5152EE66DFABll, 0xA831C66D2DB43210ll, 0xB00327C898FB213Fll,
        0xBF597FC7BEEF0EE4ll, 0xC6E00BF33DA88FC2ll, 0xD5A79147930AA725ll,
        0x06CA6351E003826Fll, 0x142929670A0E6E70ll, 0x27B70A8546D22FFCll,
        0x2E1B21385C26C926ll, 0x4D2C6DFC5AC42AEDll, 0x53380D139D95B3DFll,
        0x650A73548BAF63DEll, 0x766A0ABB3C77B2A8ll, 0x81C2C92E47EDAEE6ll,
        0x92722C851482353Bll, 0xA2BFE8A14CF10364ll, 0xA81A664BBC423001ll,
        0xC24B8B70D0F89791ll, 0xC76C51A30654BE30ll, 0xD192E819D6EF5218ll,
        0xD69906245565A910ll, 0xF40E35855771202All, 0x106AA07032BBD1B8ll,
        0x19A4C116B8D2D0C8ll, 0x1E376C085141AB53ll, 0x2748774CDF8EEB99ll,
        0x34B0BCB5E19B48A8ll, 0x391C0CB3C5C95A63ll, 0x4ED8AA4AE3418ACBll,
        0x5B9CCA4F7763E373ll, 0x682E6FF3D6B2B8A3ll, 0x748F82EE5DEFB2FCll,
        0x78A5636F43172F60ll, 0x84C87814A1F0AB72ll, 0x8CC702081A6439ECll,
        0x90BEFFFA23631E28ll, 0xA4506CEBDE82BDE9ll, 0xBEF9A3F7B2C67915ll,
        0xC67178F2E372532Bll, 0xCA273ECEEA26619Cll, 0xD186B8C721C0C207ll,
        0xEADA7DD6CDE0EB1Ell, 0xF57D4F7FEE6ED178ll, 0x06F067AA72176FBAll,
        0x0A637DC5A2C898A6ll, 0x113F9804BEF90DAEll, 0x1B710B35131C471Bll,
        0x28DB77F523047D84ll, 0x32CAAB7B40C72493ll, 0x3C9EBE0A15C9BEBCll,
        0x431D67C49C100D4Cll, 0x4CC5D4BECB3E42B6ll, 0x597F299CFC657E2All,
        0x5FCB6FAB3AD6FAECll, 0x6C44198C4A475817ll
    };
    INT4                i4Temp = 0, i4Temp8 = 0;    /* Loop counter */
    uint64_t            u8temp1 = 0, u8temp2 = 0;    /* Temporary word value */
    uint64_t            au8Word[80] = { 0 };    /* Word sequence */
    uint64_t            u8Aword = 0, u8Bword = 0, u8Cword = 0, u8Dword = 0,
        u8Eword = 0, u8Fword = 0, u8Gword = 0;
    uint64_t            u8Hword = 0;    /* Word buffers */

#endif /* USE_32BIT_ONLY */

#ifdef USE_32BIT_ONLY
    /* Initialize the first 16 words in the array u4Word */
    for (i4Temp = i4TempT2 = i4Temp8 = 0; i4Temp < 16; i4Temp++, i4Temp8 += 8)
    {
        au4Word[i4TempT2++] =
            ((((UINT4) pContext->au1MessageBlock[i4Temp8])) << 24) |
            ((((UINT4) pContext->au1MessageBlock[i4Temp8 + 1])) << 16) |
            ((((UINT4) pContext->au1MessageBlock[i4Temp8 + 2])) << 8) |
            ((((UINT4) pContext->au1MessageBlock[i4Temp8 + 3])));

        au4Word[i4TempT2++] = ((((UINT4) pContext->au1MessageBlock[i4Temp8 +
                                                                   4])) << 24) |
            ((((UINT4) pContext->
               au1MessageBlock[i4Temp8 +
                               5])) << 16) | ((((UINT4) pContext->
                                                au1MessageBlock[i4Temp8 +
                                                                6])) << 8) |
            ((((UINT4) pContext->au1MessageBlock[i4Temp8 + 7])));
    }

    for (i4Temp = 16; i4Temp < 80; i4Temp++, i4TempT2 += 2)
    {
        /* W[t] = SHA512_SIGMA_1(W[t-2]) + W[t-7] +
           SHA512_SIGMA_0(W[t-15]) + W[t-16]; */
        /* UINT4              *pu4Wt2 = &au4Word[i4TempT2 - 2 * 2]; */
        if (i4TempT2 >= 4)
        {
            pu4Wt2 = &au4Word[i4TempT2 - 2 * 2];
            SHA512_SIGMA_1 (pu4Wt2, au4Temp1);

            if (i4TempT2 >= 14)
            {
                pu4Wt7 = &au4Word[i4TempT2 - 7 * 2];
                SHA512_ADD (au4Temp1, pu4Wt7, au4Temp2);
            }
            if (i4TempT2 >= 30)
            {
                pu4Wt15 = &au4Word[i4TempT2 - 15 * 2];
                SHA512_SIGMA_0 (pu4Wt15, au4Temp1);

                if (i4TempT2 >= 32)
                {
                    pu4Wt16 = &au4Word[i4TempT2 - 16 * 2];
                    SHA512_ADD (au4Temp1, pu4Wt16, au4Temp3);

                    SHA512_ADD (au4Temp2, au4Temp3, &au4Word[i4TempT2]);
                }
            }
        }
    }

    au4Aword[0] = pContext->au4IntermediateHash[0];

    au4Aword[1] = pContext->au4IntermediateHash[1];
    au4Bword[0] = pContext->au4IntermediateHash[2];
    au4Bword[1] = pContext->au4IntermediateHash[3];
    au4Cword[0] = pContext->au4IntermediateHash[4];
    au4Cword[1] = pContext->au4IntermediateHash[5];
    au4Dword[0] = pContext->au4IntermediateHash[6];
    au4Dword[1] = pContext->au4IntermediateHash[7];
    au4Eword[0] = pContext->au4IntermediateHash[8];
    au4Eword[1] = pContext->au4IntermediateHash[9];
    au4Fword[0] = pContext->au4IntermediateHash[10];
    au4Fword[1] = pContext->au4IntermediateHash[11];
    au4Gword[0] = pContext->au4IntermediateHash[12];
    au4Gword[1] = pContext->au4IntermediateHash[13];
    au4Hword[0] = pContext->au4IntermediateHash[14];
    au4Hword[1] = pContext->au4IntermediateHash[15];

    for (i4Temp = i4TempT2 = 0; i4Temp < 80; i4Temp++, i4TempT2 += 2)
    {
        /*
         * temp1 = H + SHA512_SIGMA1(E) + SHA_CH(E,F,G) + K[t] + au4Word[t];
         */
        SHA512_SIGMA1 (au4Eword, au4Temp1);
        SHA512_ADD (au4Hword, au4Temp1, au4Temp2);
        SHA_CH (au4Eword, au4Fword, au4Gword, au4Temp3);
        SHA512_ADD (au4Temp2, au4Temp3, au4Temp4);
        SHA512_ADD (&au4Ktemp[i4TempT2], &au4Word[i4TempT2], au4Temp5);
        SHA512_ADD (au4Temp4, au4Temp5, au4Temp1);
        /*
         * temp2 = SHA512_SIGMA0(A) + SHA_MAJ(A,B,C);
         */
        SHA512_SIGMA0 (au4Aword, au4Temp3);
        SHA_MAJ (au4Aword, au4Bword, au4Cword, au4Temp4);
        SHA512_ADD (au4Temp3, au4Temp4, au4Temp2);
        au4Hword[0] = au4Gword[0];
        au4Hword[1] = au4Gword[1];
        au4Gword[0] = au4Fword[0];
        au4Gword[1] = au4Fword[1];
        au4Fword[0] = au4Eword[0];
        au4Fword[1] = au4Eword[1];
        SHA512_ADD (au4Dword, au4Temp1, au4Eword);
        au4Dword[0] = au4Cword[0];
        au4Dword[1] = au4Cword[1];
        au4Cword[0] = au4Bword[0];
        au4Cword[1] = au4Bword[1];
        au4Bword[0] = au4Aword[0];
        au4Bword[1] = au4Aword[1];
        SHA512_ADD (au4Temp1, au4Temp2, au4Aword);
    }

    SHA512_ADDTO2 (&pContext->au4IntermediateHash[0], au4Aword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[2], au4Bword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[4], au4Cword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[6], au4Dword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[8], au4Eword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[10], au4Fword);

    SHA512_ADDTO2 (&pContext->au4IntermediateHash[12], au4Gword);
    SHA512_ADDTO2 (&pContext->au4IntermediateHash[14], au4Hword);

#else /* !USE_32BIT_ONLY */

    /*
     * Initialize the first 16 words in the array W
     */
    for (i4Temp = i4Temp8 = 0; i4Temp < 16; i4Temp++, i4Temp8 += 8)
    {
        au8Word[i4Temp] =
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8]) << 56) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 1]) << 48) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 2]) << 40) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 3]) << 32) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 4]) << 24) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 5]) << 16) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 6]) << 8) |
            ((uint64_t) (pContext->au1MessageBlock[i4Temp8 + 7]));
    }

    for (i4Temp = 16; i4Temp < 80; i4Temp++)
    {
        au8Word[i4Temp] =
            SHA512_SIGMA_1 (au8Word[i4Temp - 2]) + au8Word[i4Temp - 7] +
            SHA512_SIGMA_0 (au8Word[i4Temp - 15]) + au8Word[i4Temp - 16];
    }

    u8Aword = pContext->au8IntermediateHash[0];
    u8Bword = pContext->au8IntermediateHash[1];
    u8Cword = pContext->au8IntermediateHash[2];
    u8Dword = pContext->au8IntermediateHash[3];
    u8Eword = pContext->au8IntermediateHash[4];
    u8Fword = pContext->au8IntermediateHash[5];
    u8Gword = pContext->au8IntermediateHash[6];
    u8Hword = pContext->au8IntermediateHash[7];

    for (i4Temp = 0; i4Temp < 80; i4Temp++)
    {
        u8temp1 = u8Hword + SHA512_SIGMA1 (u8Eword) +
            SHA_CH (u8Eword, u8Fword, u8Gword) + au8Ktemp[i4Temp] +
            au8Word[i4Temp];
        u8temp2 = SHA512_SIGMA0 (u8Aword) + SHA_MAJ (u8Aword, u8Bword, u8Cword);
        u8Hword = u8Gword;
        u8Gword = u8Fword;
        u8Fword = u8Eword;
        u8Eword = u8Dword + u8temp1;
        u8Dword = u8Cword;
        u8Cword = u8Bword;
        u8Bword = u8Aword;
        u8Aword = u8temp1 + u8temp2;
    }

    pContext->au8IntermediateHash[0] += u8Aword;
    pContext->au8IntermediateHash[1] += u8Bword;
    pContext->au8IntermediateHash[2] += u8Cword;
    pContext->au8IntermediateHash[3] += u8Dword;
    pContext->au8IntermediateHash[4] += u8Eword;
    pContext->au8IntermediateHash[5] += u8Fword;
    pContext->au8IntermediateHash[6] += u8Gword;
    pContext->au8IntermediateHash[7] += u8Hword;
#endif /* USE_32BIT_ONLY */

    pContext->i2MessageBlockIndex = 0;
}

/*******************************************************************************
 * Function           :  Sha384_512Reset
 *
 * Description        :  This helper function will initialize the tSha512Context
 *                       in preparation for computing a new SHA384 or Sha512
 *                       message digest.
 *
 * Input(s)           : pContext - The pContext to reset                         
 * Output(s)          : pContext - That contains the initialized 
 *                          au4IntermediateHash context info. 
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
#ifdef USE_32BIT_ONLY
PRIVATE INT4
Sha384_512Reset (tSha512Context * pContext, UINT4 *pu4Htemp0)
#else /* !USE_32BIT_ONLY */
PRIVATE INT4
Sha384_512Reset (tSha512Context * pContext, uint64_t * pu8Htemp0)
#endif                            /* USE_32BIT_ONLY */
{
    INT4                i4Temp = 0;

    if (pContext == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\nSha256Reset: Input parameter pContext is null\n");
#endif
        return OSIX_FAILURE;
    }

    pContext->i2MessageBlockIndex = 0;

#ifdef USE_32BIT_ONLY
    pContext->u4Length[0] = pContext->u4Length[1] = 0;
    pContext->u4Length[2] = pContext->u4Length[3] = 0;

    for (i4Temp = 0; i4Temp < SHA512_HASH_SIZE / 4; i4Temp++)
    {
        pContext->au4IntermediateHash[i4Temp] = pu4Htemp0[i4Temp];
    }
#else /* !USE_32BIT_ONLY */
    pContext->u8LengthHigh = pContext->u8LengthLow = 0;

    for (i4Temp = 0; i4Temp < SHA512_HASH_SIZE / 8; i4Temp++)
    {
        pContext->au8IntermediateHash[i4Temp] = pu8Htemp0[i4Temp];
    }
#endif /* USE_32BIT_ONLY */

    pContext->i4Computed = 0;
    pContext->i4Corrupted = 0;

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           :  Sha384_512ResultN
 *
 * Description        :  This helper function will return the 384-bit or 512-bit
 *                       message digest into the pu1MessageDigest array provided
 *                       by the caller. 
 *                        NOTE: The first octet of hash is stored in the 0th
 *                        element, the last octet of hash in the 48th/64th  
 *                       element.
 *
 * Input(s)           : pContext - The pContext to use to calculate the SHA 
 *                                 hash.
 *
 * Output(s)          : pContext - That contains the Intermediate hash.
 *                      pu1MessageDigest - That contains the calculated digest.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Sha384_512ResultN (tSha512Context * pContext,
                   UINT1 *pu1MessageDigest, INT4 HashSize)
{
    INT4                i4Temp = 0;

#ifdef USE_32BIT_ONLY
    INT4                i4Temp2 = 0;
#endif /* USE_32BIT_ONLY */

    if ((pContext == NULL) || (pu1MessageDigest == NULL))
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\nSha384_512ResultN: Input parameter pContext is null\n");
#endif
        return OSIX_FAILURE;
    }

    if (pContext->i4Corrupted)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\nSha384_512ResultN: pContext is corrupted\n");
#endif
        return OSIX_FAILURE;
    }

    if (pContext->i4Computed == (INT4) 0)
    {
        Sha384_512Finalize (pContext, 0x80);
    }

#ifdef USE_32BIT_ONLY
    for (i4Temp = i4Temp2 = 0; i4Temp < HashSize;)
    {
        pu1MessageDigest[i4Temp++] = (UINT1) (pContext->
                                              au4IntermediateHash[i4Temp2] >>
                                              24);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2] >> 16);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2] >> 8);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2++]);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2] >> 24);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2] >> 16);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2] >> 8);
        pu1MessageDigest[i4Temp++] =
            (UINT1) (pContext->au4IntermediateHash[i4Temp2++]);
    }
#else /* !USE_32BIT_ONLY */
    for (i4Temp = 0; i4Temp < HashSize; ++i4Temp)
    {
        pu1MessageDigest[i4Temp] = (UINT1)
            (pContext->au8IntermediateHash[i4Temp >> 3] >> 8 * (7 -
                                                                (i4Temp % 8)));
    }
#endif /* USE_32BIT_ONLY */

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file sha384_512.c                    */
/*-----------------------------------------------------------------------*/
