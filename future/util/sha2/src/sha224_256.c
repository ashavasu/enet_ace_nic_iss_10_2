/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sha224_256.c,v 1.1 2015/04/28 12:53:48 siva Exp $
 *
 * Description:
 *   "The algorithm implemented is from the RFC 4634. The code from the 
 *   RFC 4634 has been modified to conform to organizational coding guidelines"
 *
 *   This file implements the Secure Hash Signature Standard
 *   algorithms as defined in the National Institute of Standards
 *   and Technology Federal Information Processing Standards
 *   Publication (FIPS PUB) 180-1 published on April 17, 1995, 180-2
 *   published on August 1, 2002, and the FIPS PUB 180-2 Change
 *   Notice published on February 28, 2004.
 *
 *   The SHA-224 and SHA-256 algorithms produce 224-bit and 256-bit
 *   message digests for a given data stream. It should take about
 *   2**n steps to find a message with the same digest as a given
 *   message and 2**(n/2) to find any two messages with the same
 *   digest, when n is the digest size in bits. Therefore, this
 *   algorithm can serve as a means of providing a
 *   "fingerprint" for a message.
 *
 *   SHA-224 and SHA-256 are designed to work with messages less
 *   than 2^64 bits long. This implementation uses SHA224/256Input()
 *   to hash the bits that are a multiple of the size of an 8-bit
 *   character, and then uses SHA224/256FinalBits() to hash the
 *   final few bits of the input.
 *****************************************************************************/
#include "sha2inc.h"

PRIVATE VOID        Sha224_256Finalize (tSha256Context * pContext,
                                        UINT1 u1PadByte);
PRIVATE VOID        Sha224_256PadMessage (tSha256Context * pContext,
                                          UINT1 u1PadByte);
PRIVATE VOID        Sha224_256ProcessMessageBlock (tSha256Context * pContext);
PRIVATE INT4        Sha224_256Reset (tSha256Context * pContext, UINT4 *H0);
PRIVATE INT4        Sha224_256ResultN (tSha256Context * pContext,
                                       UINT1 pu1MessageDigest[],
                                       INT4 i4HashSize);

/* Initial Hash Values: FIPS-180-2 Change Notice 1 */
PRIVATE UINT4       gau4Sha224_H0[SHA256_HASH_SIZE / 4] = {
    0xC1059ED8, 0x367CD507, 0x3070DD17, 0xF70E5939,
    0xFFC00B31, 0x68581511, 0x64F98FA7, 0xBEFA4FA4
};

/* Initial Hash Values: FIPS-180-2 section 5.3.2 */
PRIVATE UINT4       gau4Sha256_H0[SHA256_HASH_SIZE / 4] = {
    0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
    0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
};

PRIVATE UINT4       gu4addTemp;
#define SHA224_256ADDLENGTH(context, length)               \
  (gu4addTemp = (context)->u4LengthLow, (context)->i4Corrupted = \
    (((context)->u4LengthLow += (length)) < gu4addTemp) && (++(context)\
       ->u4LengthHigh == 0) ? 1 : 0)

/*******************************************************************************
 * Function           : Sha224Reset 
 *
 * Description        : This function will initialize the tSha224Context in 
 *                      preparation for computing a new SHA224 message digest.
 *
 * Input(s)           : pContext -  The pContext that needs to be reset
 * 
 * Output(s)          : pContext - pointer that contains initialized 
 *                                 context info 
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha224Reset (tSha224Context * pContext)
{
    return Sha224_256Reset (pContext, gau4Sha224_H0);
}

/*******************************************************************************
 * Function           : Sha224Input
 *
 * Description        : This function accepts an array of octets as the next 
 *                      portion of the message.
 *
 * Input(s)           : pContext - The SHA pContext to update 
 *                      pu1MessageArray - array of characters representing the 
 *                      next portion of the message.
 *                      u4Length - The length of the message in pu1MessageArray 
 *
 * Output(s)          : pContext - that contains computed Intermediate Hash
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha224Input (tSha224Context * pContext, CONST UINT1 *pu1MessageArray,
             UINT4 u4Length)
{
    return Sha256Input (pContext, pu1MessageArray, u4Length);
}

/*******************************************************************************
 * Function           : Sha224FinalBits
 *
 * Description        : This function will add in any final bits of the message.
 *
 * Input(s)           : pContext     - SHA context to update.
 *                      u1MessageBits - The final bits of the message, in the 
 *                                      upper portion of the byte. (Use 
 *                                      0b###00000 instead of 0b00000### to 
 *                                      input the three bits ###.)
 *                      u4Length      - The number of bits in message_bits, 
 *                                      between 1 and 7.
 *
 * Output(s)          : pContext - that contains updated Final bits to context 
 *                                 info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha224FinalBits (tSha224Context * pContext,
                 CONST UINT1 u1MessageBits, UINT4 u4Length)
{
    return Sha256FinalBits (pContext, u1MessageBits, u4Length);
}

/*******************************************************************************
 * Function           : Sha224Result
 *
 * Description        : This function will return the 224-bit message digest 
                        into the Message_Digest array provided by the caller.
 *                      NOTE: 
                         The first octet of hash is stored in the 0th element,
 *                       the last octet of hash in the 28th element.
 *
 * Input(s)           : pContext  - The context to use to calculate the SHA 
 *                                  hash.
 *                      pu1MessageDigest - Where the digest is returned.
 *
 * Output(s)          : pContext - That contains updated Intermediate hash. 
 *                      
 *                      pu1MessageDigest - That contains the caluclated 224 
 *                                         message digest.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha224Result (tSha224Context * pContext, UINT1 *pu1MessageDigest)
{
    return Sha224_256ResultN (pContext, pu1MessageDigest, SHA224_HASH_SIZE);
}

/*******************************************************************************
 * Function           : Sha256Reset
 *
 * Description        : This function will initialize the tSha256Context in 
                        preparation for computing a new SHA256 message digest.
 *
 * Input(s)           : pContext  - The context to reset.
 *
 * Output(s)          : pContext - that pointer contains initialized 
 *                                 au4IntermediateHash context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha256Reset (tSha256Context * pContext)
{
    return Sha224_256Reset (pContext, gau4Sha256_H0);
}

/*******************************************************************************
 * Function           : Sha256Input 
 * Description        : This function accepts an array of octets as the next
 *                      portion of the message.
 *
 * Input(s)           : pContext - The SHA context to update
 *                      pu1MessageArray - An array of characters representing 
 *                                        the next portion of the message. 
 *                      u4Length - The length of the message in pu1MessageArray
 *
 * Output(s)          : pContext -that pointer contains the computed 
 *                                Intermediated Hash
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha256Input (tSha256Context * pContext, CONST UINT1 *pu1MessageArray,
             UINT4 u4Length)
{
    if (u4Length == 0)
    {
        return OSIX_SUCCESS;
    }

    if ((pContext == NULL) || (pu1MessageArray == NULL))
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\nSha256Input: input parameter pContext or pu1MessageArray"
             " are null\n");

#endif
        return OSIX_FAILURE;
    }

    if ((pContext->i4Computed) != 0)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\nSha256Input: pContext is corrupted\n");
#endif
        pContext->i4Corrupted = SHA_STATE_ERROR;
        return OSIX_FAILURE;

    }

    if ((pContext->i4Corrupted) != 0)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\nSha256Input: pContext is corrupted\n");
#endif
        return OSIX_FAILURE;
    }

    while ((u4Length--) && ((pContext->i4Corrupted) == (UINT4) 0))
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] =
            (*pu1MessageArray & 0xFF);

        if (!SHA224_256ADDLENGTH (pContext, 8) &&
            (pContext->i2MessageBlockIndex == SHA256_MESSAGE_BLOCK_SIZE))
        {
            Sha224_256ProcessMessageBlock (pContext);
        }

        pu1MessageArray++;
    }

    return OSIX_SUCCESS;

}

/*******************************************************************************
 * Function           : Sha256FinalBits
 * Description        : This function will add in any final bits of the message.
 *
 * Input(s)           : pContext - The SHA context to update
 *                      u1MessageBits - The final bits of the message, in the 
 *                                      upper portion of the byte. (Use 
 *                                      0b###00000 instead of 0b00000### to 
 *                                      input the three bits ###.)
 *                                       the next portion of the message.
 *                      u4Length - The number of bits in message_bits, between 1 
 *                                   and 7.
 *
 * Output(s)          : pContext - that contains updated final bits to context
 *                                 info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha256FinalBits (tSha256Context * pContext,
                 CONST UINT1 u1MessageBits, UINT4 u4Length)
{
    UINT1               au1Masks[8] = {
        /* 0 0b00000000 */ 0x00, /* 1 0b10000000 */ 0x80,
        /* 2 0b11000000 */ 0xC0, /* 3 0b11100000 */ 0xE0,
        /* 4 0b11110000 */ 0xF0, /* 5 0b11111000 */ 0xF8,
        /* 6 0b11111100 */ 0xFC, /* 7 0b11111110 */ 0xFE
    };

    UINT1               au1Markbit[8] = {
        /* 0 0b10000000 */ 0x80, /* 1 0b01000000 */ 0x40,
        /* 2 0b00100000 */ 0x20, /* 3 0b00010000 */ 0x10,
        /* 4 0b00001000 */ 0x08, /* 5 0b00000100 */ 0x04,
        /* 6 0b00000010 */ 0x02, /* 7 0b00000001 */ 0x01
    };

    if (u4Length == (UINT4) 0)
    {
        return OSIX_SUCCESS;
    }
    if (pContext == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n Sha256FinalBits:  Input parameter pContext is NULL\n");
#endif
        return OSIX_FAILURE;
    }

    if ((pContext->i4Computed) || (u4Length >= 8) || (u4Length == 0))
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\nSha256FinalBits: pContext corrupted\n");
#endif
        pContext->i4Corrupted = SHA_STATE_ERROR;
        return OSIX_FAILURE;
    }

    SHA224_256ADDLENGTH (pContext, u4Length);
    Sha224_256Finalize (pContext, (UINT1) ((u1MessageBits & au1Masks[u4Length])
                                           | au1Markbit[u4Length]));

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           : Sha256Result
 * Description        : This function will return the 256-bit message digest 
 *                      into the Message_Digest array provided by the caller.
 *                      NOTE: The first octet of hash is stored in the 0th 
 *                      element, the last octet of hash in the 32nd element.
 *
 * Input(s)           : pContext - The context to use to calculate the SHA hash.
 *
 * Output(s)          : pu1MessageDigest - Where the digest is returned. 
 *                      pContext - That contains updated context
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Sha256Result (tSha256Context * pContext, UINT1 *pu1MessageDigest)
{

    return Sha224_256ResultN (pContext, pu1MessageDigest, SHA256_HASH_SIZE);
}

/*******************************************************************************
 * Function           : Sha224_256Finalize
 * Description        : This helper function finishes off the digest 
 *                      calculations.
 *
 * Input(s)           : pContext -  The SHA context to update
 *                      u1PadByte - The last byte to add to the digest before 
 *                                  the 0-padding and length. This will contain
 *                                  the last bits of the message followed by 
 *                                  another single bit. If the message was an 
 *                                  exact multiple of 8-bits long, u1PadByte 
 *                                  will be 0x80.
 *
 * Output(s)          : pContext - That contains the calculated digest.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE VOID
Sha224_256Finalize (tSha256Context * pContext, UINT1 u1PadByte)
{
    INT4                i4Count = 0;
    Sha224_256PadMessage (pContext, u1PadByte);
    /* message may be sensitive, so clear it out */
    for (i4Count = 0; i4Count < SHA256_MESSAGE_BLOCK_SIZE; ++i4Count)
    {
        pContext->au1MessageBlock[i4Count] = 0;
    }
    pContext->u4LengthLow = 0;    /* and clear length */
    pContext->u4LengthHigh = 0;
    pContext->i4Computed = 1;
}

/*******************************************************************************
 * Function           : Sha224_256PadMessage
 *
 * Description        : According to the standard, the message must be padded to 
 *                      an even 512 bits. The first padding bit must be a '1'. 
 *                      The last 64 bits represent the length of the original 
 *                      message. All bits in between should be 0. This helper 
 *                      function will pad the message according to those rules 
 *                      by filling the     Message_Block array accordingly. When 
 *                      it returns, it can be assumed that the message digest 
 *                      has been computed.
 *
 *
 * Input(s)           : pContext -  The pContext to pad
 *                     u1PadByte - The last byte to add to the digest before the
 *                                 0-padding and length. This will contain the
 *                                 last bits of the message followed by another
 *                                 single bit. If the message was an exact
 *                                 multiple of 8-bits long, u1PadByte will be
 *                                 0x80.
 *
 * Output(s)          : pContext -  that contains updated message padded or 
 *                                  parsed context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE VOID
Sha224_256PadMessage (tSha256Context * pContext, UINT1 u1PadByte)
{
    /*
     * Check to see if the current message block is too small to hold
     * the initial padding bits and length. If so, we will pad the
     * block, process it, and then continue padding into a second
     * block.
     */
    if (pContext->i2MessageBlockIndex >= (SHA256_MESSAGE_BLOCK_SIZE - 8))
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = u1PadByte;
        while (pContext->i2MessageBlockIndex < SHA256_MESSAGE_BLOCK_SIZE)
        {
            pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = 0;
        }
        Sha224_256ProcessMessageBlock (pContext);
    }
    else
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = u1PadByte;
    }

    while (pContext->i2MessageBlockIndex < (SHA256_MESSAGE_BLOCK_SIZE - 8))
    {
        pContext->au1MessageBlock[pContext->i2MessageBlockIndex++] = 0;
    }

    /*
     * Store the message length as the last 8 octets
     */
    pContext->au1MessageBlock[56] = (UINT1) (pContext->u4LengthHigh >> 24);
    pContext->au1MessageBlock[57] = (UINT1) (pContext->u4LengthHigh >> 16);
    pContext->au1MessageBlock[58] = (UINT1) (pContext->u4LengthHigh >> 8);
    pContext->au1MessageBlock[59] = (UINT1) (pContext->u4LengthHigh);
    pContext->au1MessageBlock[60] = (UINT1) (pContext->u4LengthLow >> 24);
    pContext->au1MessageBlock[61] = (UINT1) (pContext->u4LengthLow >> 16);
    pContext->au1MessageBlock[62] = (UINT1) (pContext->u4LengthLow >> 8);
    pContext->au1MessageBlock[63] = (UINT1) (pContext->u4LengthLow);

    Sha224_256ProcessMessageBlock (pContext);
}

/*******************************************************************************
 * Function           : Sha224_256ProcessMessageBlock
 *
 * Description        : This function will process the next 512 bits of the 
                        message stored in the au1MessageBlock array.
 *
 * Input(s)           : pContext - the pContext to update
 *
 * Output(s)          : pContext - that contains computed Intermediated Hash
 *                      
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/

PRIVATE VOID
Sha224_256ProcessMessageBlock (tSha256Context * pContext)
{
    /* Constants defined in FIPS-180-2, section 4.2.2 */
    PRIVATE CONST UINT4 au4Ktemp[64] = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b,
        0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
        0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7,
        0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152,
        0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
        0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
        0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819,
        0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
        0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f,
        0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };
    INT4                i4Count = 0, i4Temp = 0;    /* Loop counter */
    UINT4               u4Temp1 = 0, u4Temp2 = 0;    /* Temporary word value */
    UINT4               au4Word[64];    /* Word sequence */
    UINT4               u4Atemp = 0, u4Btemp = 0, u4Ctemp = 0, u4Dtemp = 0,
        u4Etemp = 0, u4Ftemp = 0, u4Gtemp = 0, u4Htemp = 0;
    /* Word buffers */

    MEMSET (au4Word, 0, sizeof (au4Word));
    /*
     * Initialize the first 16 words in the array au4Word
     */
    for (i4Count = i4Temp = 0; i4Count < 16; i4Count++, i4Temp += 4)
    {
        au4Word[i4Count] = (((UINT4) pContext->au1MessageBlock[i4Temp]) << 24) |
            (((UINT4) pContext->au1MessageBlock[i4Temp + 1]) << 16) |
            (((UINT4) pContext->au1MessageBlock[i4Temp + 2]) << 8) |
            (((UINT4) pContext->au1MessageBlock[i4Temp + 3]));
    }

    for (i4Count = 16; i4Count < 64; i4Count++)
    {
        au4Word[i4Count] = SHA256_SIGMA_1 (au4Word[i4Count - 2]) +
            au4Word[i4Count - 7] + SHA256_SIGMA_0 (au4Word[i4Count - 15])
            + au4Word[i4Count - 16];
    }

    u4Atemp = pContext->au4IntermediateHash[0];
    u4Btemp = pContext->au4IntermediateHash[1];
    u4Ctemp = pContext->au4IntermediateHash[2];
    u4Dtemp = pContext->au4IntermediateHash[3];
    u4Etemp = pContext->au4IntermediateHash[4];
    u4Ftemp = pContext->au4IntermediateHash[5];
    u4Gtemp = pContext->au4IntermediateHash[6];
    u4Htemp = pContext->au4IntermediateHash[7];

    for (i4Count = 0; i4Count < 64; i4Count++)
    {
        u4Temp1 = u4Htemp + SHA256_SIGMA1 (u4Etemp) + SHA_CH (u4Etemp, u4Ftemp,
                                                              u4Gtemp) +
            au4Ktemp[i4Count] + au4Word[i4Count];
        u4Temp2 = SHA256_SIGMA0 (u4Atemp) + SHA_MAJ (u4Atemp, u4Btemp, u4Ctemp);
        u4Htemp = u4Gtemp;
        u4Gtemp = u4Ftemp;
        u4Ftemp = u4Etemp;
        u4Etemp = u4Dtemp + u4Temp1;
        u4Dtemp = u4Ctemp;
        u4Ctemp = u4Btemp;
        u4Btemp = u4Atemp;
        u4Atemp = u4Temp1 + u4Temp2;
    }

    pContext->au4IntermediateHash[0] += u4Atemp;
    pContext->au4IntermediateHash[1] += u4Btemp;
    pContext->au4IntermediateHash[2] += u4Ctemp;
    pContext->au4IntermediateHash[3] += u4Dtemp;
    pContext->au4IntermediateHash[4] += u4Etemp;
    pContext->au4IntermediateHash[5] += u4Ftemp;
    pContext->au4IntermediateHash[6] += u4Gtemp;
    pContext->au4IntermediateHash[7] += u4Htemp;

    pContext->i2MessageBlockIndex = 0;
}

/*******************************************************************************
 * Function           : Sha224_256Reset
 *
 * Description        : This helper function will initialize the tSha256Context 
 *                      in preparation for computing a new SHA256 message 
 *                       digest.
 *
 * Input(s)           : pContext - This function will initialize the 
 *                                 tSha256Context in preparation for computing 
 *                                 a new SHA256 message digest.
 *                      pu4H0 - The initial hash value to use. 
 *
 * Output(s)          : pContext - that pointer contains the initialized 
 *                                 u4IntermediateHash context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Sha224_256Reset (tSha256Context * pContext, UINT4 *pu4H0)
{
    if (pContext == NULL)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n Sha224_256Reset:  Input parameter pContext is NULL\n");
#endif
        return OSIX_FAILURE;
    }

    pContext->u4LengthLow = 0;
    pContext->u4LengthHigh = 0;
    pContext->i2MessageBlockIndex = 0;

    pContext->au4IntermediateHash[0] = pu4H0[0];
    pContext->au4IntermediateHash[1] = pu4H0[1];
    pContext->au4IntermediateHash[2] = pu4H0[2];
    pContext->au4IntermediateHash[3] = pu4H0[3];
    pContext->au4IntermediateHash[4] = pu4H0[4];
    pContext->au4IntermediateHash[5] = pu4H0[5];
    pContext->au4IntermediateHash[6] = pu4H0[6];
    pContext->au4IntermediateHash[7] = pu4H0[7];

    pContext->i4Computed = 0;
    pContext->i4Corrupted = 0;

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function           : Sha224_256ResultN
 *
 * Description        : This helper function will return the 224-bit or 256-bit 
 *                      message digest into the Message_Digest array provided by 
 *                      the caller.
 *                      NOTE: The first octet of hash is stored in the 0th 
 *                      element, the last octet of hash in the 28th/32nd 
 *                      element. sha256Context in preparation for computing a 
 *                      new SHA256 message digest.
 *
 * Input(s)           : pContext - The context to use to calculate the SHA hash
                        HashSize - The size of the hash, either 28 or 32.
 *
 * Output(s)          : pu1MessageDigest - That contains the calculated digest.
 *                      pContext - That contains the Intermediate hash.
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/

PRIVATE INT4
Sha224_256ResultN (tSha256Context * pContext,
                   UINT1 *pu1MessageDigest, INT4 i4HashSize)
{
    INT4                i4Temp = 0;

    if ((pContext == NULL) || (pu1MessageDigest == NULL))
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n Sha224_256ResultN:  Input parameter pContext is NULL\n");
#endif
        return OSIX_FAILURE;
    }

    if (pContext->i4Corrupted)
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT ("\n Sha224_256ResultN: pContext is corrupted\n");
#endif
        return OSIX_FAILURE;
    }

    if (pContext->i4Computed == 0)
    {
        Sha224_256Finalize (pContext, 0x80);
    }

    for (i4Temp = 0; i4Temp < i4HashSize; ++i4Temp)
    {
        pu1MessageDigest[i4Temp] = (UINT1)
            (pContext->au4IntermediateHash[i4Temp >> 2] >> 8 * (3 -
                                                                (i4Temp &
                                                                 0x03)));
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file sha224_256.c                    */
/*-----------------------------------------------------------------------*/
