/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: usha.c,v 1.1 2015/04/28 12:53:48 siva Exp $
 * Description:
 *      "The algorithm implemented is from the RFC 4634. The code from the
 *       RFC 4634 has been modified to conform to organizational coding 
 *       guidelines"
 *  
 *       This file implements a unified interface to the SHA algorithms.
 ***************************************************************************/
#include "sha2inc.h"
#include "lr.h"
#include "fsutltrc.h"
#include "trace.h"

extern UINT4        gu4UtlTrcFlag;

/*******************************************************************************
 * Function           : UshaReset
 *
 * Description        : This function will initialize the SHA Context in 
 *                      preparation for computing a new SHA message digest.
 *                       
 * Input(s)           : pUshaCtx - The pUshaCtx to reset
 *                    : whichSha - Selects which SHA reset to call
 *
 * Output(s)          : pUshaCtx - that contains initialized au4IntermediateHash
 *                                 to pUshaCtx info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
UshaReset (tUshaContext * pUshaCtx, enum SHAversion whichSha)
{
    if (pUshaCtx)
    {
        pUshaCtx->i4WhichSha = whichSha;
        switch (whichSha)
        {
            case AR_SHA224_ALGO:
                return Sha224Reset ((tSha224Context *) & pUshaCtx->unCtx);
            case AR_SHA256_ALGO:
                return Sha256Reset ((tSha256Context *) & pUshaCtx->unCtx);
            case AR_SHA384_ALGO:
                return Sha384Reset ((tSha384Context *) & pUshaCtx->unCtx);
            case AR_SHA512_ALGO:
                return Sha512Reset ((tSha512Context *) & pUshaCtx->unCtx);
            case AR_SHA1_ALGO:
            default:
            {
#ifdef CRYPTO_DEBUG_LOG
                CRYPTO_DEBUG_PRINT
                    ("\n UshaReset: Input parameter whichsha is bad parameter \n");
#endif
                UTIL_TRC (ALL_FAILURE_TRC, "UshaReset: Input parameter "
                          "whichsha is bad parameter\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n UshaReset: Input parameter pUshaCtx is NULL \n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "UshaReset: Input parameter "
                  "pUshaCtx is NULL \n");
        return OSIX_FAILURE;
    }
}

/*******************************************************************************
 * Function           : UshaInput
 *
 * Description        : This function accepts an array of octets as the next 
 *                      portion of the message.
 *
 * Input(s)           : pUshaCtx - The SHA pUshaCtx to update and An array of 
 *                                 characters representing the next portion of
 *                                 the message.
 *                      u1Bytes -  An array of characters representing the next 
 *                                 portion of the message.
 *                      u4ByteCount - The length of the message in u1Bytes.
 *
 * Output(s)          : pUshaCtx - that contains computed Intermediated Hash
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
UshaInput (tUshaContext * pUshaCtx, CONST UINT1 *u1Bytes, UINT4 u4ByteCount)
{
    if (pUshaCtx)
    {
        switch (pUshaCtx->i4WhichSha)
        {
            case AR_SHA224_ALGO:
                return Sha224Input ((tSha224Context *) & pUshaCtx->unCtx,
                                    u1Bytes, u4ByteCount);
            case AR_SHA256_ALGO:
                return Sha256Input ((tSha256Context *) & pUshaCtx->unCtx,
                                    u1Bytes, u4ByteCount);
            case AR_SHA384_ALGO:
                return Sha384Input ((tSha384Context *) & pUshaCtx->unCtx,
                                    u1Bytes, u4ByteCount);
            case AR_SHA512_ALGO:
                return Sha512Input ((tSha512Context *) & pUshaCtx->unCtx,
                                    u1Bytes, u4ByteCount);
            default:
            {
#ifdef CRYPTO_DEBUG_LOG
                CRYPTO_DEBUG_PRINT
                    ("\n UshaInput: Input parameter whichsha is bad parameter \n");
#endif
                UTIL_TRC (ALL_FAILURE_TRC, "UshaInput: Input parameter "
                          "whichsha is bad parameter\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n UshaInput: Input parameter pUshaCtx is NULL \n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "UshaInput: Input parameter "
                  "pUshaCtx is NULL\n");
        return OSIX_FAILURE;
    }
}

/*******************************************************************************
 * Function           : UshaFinalBits
 *
 * Description        : This function will add in any final bits of the message.
 *
 * Input(s)           : pUshaCtx - SHA context to update.
 *                      u1Bits - The final bits of the message, in the
 *                               upper portion of the byte. (Use 0b###00000 
 *                               instead of 0b00000### to input the three bits 
 *                               ###.)
 *                      u4BitCount - The number of bits in message_bits,
 *                                      between 1 and 7.
 *
 * Output(s)          : pUshaCtx - that contains updated Final bits to 
 *                                 context info
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
UshaFinalBits (tUshaContext * pUshaCtx, CONST UINT1 u1Bits, UINT4 u4BitCount)
{
    if (pUshaCtx)
    {
        switch (pUshaCtx->i4WhichSha)
        {
            case AR_SHA224_ALGO:
                return Sha224FinalBits ((tSha224Context *) & pUshaCtx->unCtx,
                                        u1Bits, u4BitCount);
            case AR_SHA256_ALGO:
                return Sha256FinalBits ((tSha256Context *) & pUshaCtx->unCtx,
                                        u1Bits, u4BitCount);
            case AR_SHA384_ALGO:
                return Sha384FinalBits ((tSha384Context *) & pUshaCtx->unCtx,
                                        u1Bits, u4BitCount);
            case AR_SHA512_ALGO:
                return Sha512FinalBits ((tSha512Context *) & pUshaCtx->unCtx,
                                        u1Bits, u4BitCount);
            default:
            {
#ifdef CRYPTO_DEBUG_LOG
                CRYPTO_DEBUG_PRINT
                    ("\n UshaFinalBits: I/p parameter whichsha is bad parameter\n");
#endif
                UTIL_TRC (ALL_FAILURE_TRC, "UshaFinalBits: I/p parameter "
                          "whichsha is bad parameter\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n UshaFinalBits: Input parameter pUshaCtx is NUll \n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "UshaFinalBits: Input parameter "
                  "pUshaCtx is NUll\n");
        return OSIX_FAILURE;
    }
}

/*******************************************************************************
 * Function           : UshaResult
 *
 * Description        : This function will return the 224-bit message digest
 *                      into the pu1MessageDigest array provided by the caller.
 *                      NOTE:
 *                       The first octet of hash is stored in the 0th element,
 *                       the last octet of hash in the 28th element.
 *
 * Input(s)           : pUshaCtx - The context to use to calculate the SHA
 *                                      hash.
 *
 * Output(s)          : pUshaCtx - That contains the caluclated sha context.
 *                      pu1MessageDigest - That contains the caluclated message
 *                                         digest. 
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
UshaResult (tUshaContext * pUshaCtx, UINT1 *pu1MessageDigest)
{
    if (pUshaCtx)
    {
        switch (pUshaCtx->i4WhichSha)
        {
            case AR_SHA224_ALGO:
                return Sha224Result ((tSha224Context *) & pUshaCtx->unCtx,
                                     pu1MessageDigest);
            case AR_SHA256_ALGO:
                return Sha256Result ((tSha256Context *) & pUshaCtx->unCtx,
                                     pu1MessageDigest);
            case AR_SHA384_ALGO:
                return Sha384Result ((tSha384Context *) & pUshaCtx->unCtx,
                                     pu1MessageDigest);
            case AR_SHA512_ALGO:
                return Sha512Result ((tSha512Context *) & pUshaCtx->unCtx,
                                     pu1MessageDigest);
            default:
            {
#ifdef CRYPTO_DEBUG_LOG
                CRYPTO_DEBUG_PRINT
                    ("\n UshaResult: Input parameter whichsha is bad parameter\n");
#endif
                UTIL_TRC (ALL_FAILURE_TRC, "UshaResult: Input parameter "
                          "whichsha is bad parameter\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
#ifdef CRYPTO_DEBUG_LOG
        CRYPTO_DEBUG_PRINT
            ("\n UshaResult: Input parameter pUshaCtx is Null\n");
#endif
        UTIL_TRC (ALL_FAILURE_TRC, "UshaResult: Input parameter "
                  "whichsha is bad parameter\n");
        return OSIX_FAILURE;
    }
}

/*******************************************************************************
 * Function           : UshaBlockSize
 *
 * Description        : This function will return the blocksize for the given 
 *                      SHA algorithm.
 *
 * Input(s)           : whichSha - which SHA algorithm to query
 *
 * Output(s)          : None.
 *
 * Returns            : Message block size.
 ******************************************************************************/
PUBLIC INT4
UshaBlockSize (eShaVersion whichSha)
{
    switch (whichSha)
    {
        case AR_SHA1_ALGO:
            return SHA1_MESSAGE_BLOCK_SIZE;
        case AR_SHA224_ALGO:
            return SHA224_MESSAGE_BLOCK_SIZE;
        case AR_SHA256_ALGO:
            return SHA256_MESSAGE_BLOCK_SIZE;
        case AR_SHA384_ALGO:
            return SHA384_MESSAGE_BLOCK_SIZE;
        default:
        case AR_SHA512_ALGO:
            return SHA512_MESSAGE_BLOCK_SIZE;
    }
}

/*******************************************************************************
 * Function           : UshaHashSize
 *
 * Description        : This function will return the hashsize for the given
 *                      SHA algorithm.
 *
 * Input(s)           : i4whichSha - which SHA algorithm to query
 *
 * Output(s)          : None.
 *
 * Returns            : hash size.
 ******************************************************************************/
PUBLIC INT4
UshaHashSize (enum SHAversion whichSha)
{
    switch (whichSha)
    {
        case AR_SHA1_ALGO:
            return SHA1_HASH_SIZE;
        case AR_SHA224_ALGO:
            return SHA224_HASH_SIZE;
        case AR_SHA256_ALGO:
            return SHA256_HASH_SIZE;
        case AR_SHA384_ALGO:
            return SHA384_HASH_SIZE;
        default:
        case AR_SHA512_ALGO:
            return SHA512_HASH_SIZE;
    }
}

/*******************************************************************************
 * Function           : UshaHashSizeBits
 *
 * Description        : This function will return the hashsize for the given
 *                      SHA algorithm, expressed in bits. 
 *
 * Input(s)           : whichSha - which SHA algorithm to query
 *
 * Output(s)          : None.
 * Returns            : hash size in bits.
 ******************************************************************************/
PUBLIC INT4
UshaHashSizeBits (enum SHAversion whichSha)
{
    switch (whichSha)
    {
        case AR_SHA1_ALGO:
            return SHA1_HASH_SIZE_BITS;
        case AR_SHA224_ALGO:
            return SHA224_HASH_SIZE_BITS;
        case AR_SHA256_ALGO:
            return SHA256_HASH_SIZE_BITS;
        case AR_SHA384_ALGO:
            return SHA384_HASH_SIZE_BITS;
        default:
        case AR_SHA512_ALGO:
            return SHA512_HASH_SIZE_BITS;
    }
}

/************************************************************************/
/*  Function Name   : Sha224ArAlgo                                      */
/*  Description     : The interface function to SHA224                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA digest value                  */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Sha224ArAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    tUshaContext        ShaCtx;

    UshaReset (&ShaCtx, AR_SHA224_ALGO);
    UshaInput (&ShaCtx, (const UINT1 *) pu1Buf, (UINT4)i4BufLen);
    UshaResult (&ShaCtx, pu1Digest);
    return;
}

/************************************************************************/
/*  Function Name   : Sha256ArAlgo                                      */
/*  Description     : The interface function to SHA256                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA digest value                  */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Sha256ArAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    tUshaContext        ShaCtx;

    UshaReset (&ShaCtx, AR_SHA256_ALGO);
    UshaInput (&ShaCtx, (const UINT1 *) pu1Buf, (UINT4)i4BufLen);
    UshaResult (&ShaCtx, pu1Digest);
    return;
}

/************************************************************************/
/*  Function Name   : Sha384ArAlgo                                      */
/*  Description     : The interface function to SHA384                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA digest value                  */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Sha384ArAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    tUshaContext        ShaCtx;

    UshaReset (&ShaCtx, AR_SHA384_ALGO);
    UshaInput (&ShaCtx, (const UINT1 *) pu1Buf, (UINT4)i4BufLen);
    UshaResult (&ShaCtx, pu1Digest);
    return;
}

/************************************************************************/
/*  Function Name   : Sha512ArAlgo                                      */
/*  Description     : The interface function to SHA512                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA digest value                  */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Sha512ArAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    tUshaContext        ShaCtx;

    UshaReset (&ShaCtx, AR_SHA512_ALGO);
    UshaInput (&ShaCtx, (const UINT1 *) pu1Buf, (UINT4)i4BufLen);
    UshaResult (&ShaCtx, pu1Digest);
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file usha.c                    */
/*-----------------------------------------------------------------------*/
