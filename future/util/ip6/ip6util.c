/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6util.c,v 1.19 2017/02/06 10:45:30 siva Exp $
 *
 * Description: Routines related to handling of IP6 addresses.
 */
#include "ip6util.h"

#define  IP6_ADDR_ACCESS_IN_BYTES      16
#define  IP6_ADDR_ACCESS_IN_SHORT_INT  8
#define  IP6_ADDR_SPLITTER             6
#define  IP6_ADDR_CURR_POS             7

/*
 * Flag to control print messages based on error level
 */

INT4                i4Ip6ErrlvlFlag = ERROR_MINOR;
/*
 * Buffers and variables used in the printing of addresses and tokens
 * - these are used in the error routines
 */
char                ip6AddrPrintBuf[3][50];
char                ip6TokErrBuf[3][50];
char                ip6AddrErrBuf[3][50];
UINT2               u2ErrTokBufNum = 0, u2_err_addr_buf_num = 0;
UINT2               u2_addr_buf_num = 0;
#define MAX_INCR(x,y)    ((x >= (y-1)) ? x = 0 : ++x)

static int
TestBit (INT4 i4Nr, INT4 *p_i4_addr)
{
    INT4                i4Mask;

    p_i4_addr += i4Nr >> 5;
    i4Mask = 1 << (i4Nr & 0x1f);
    return ((i4Mask & *p_i4_addr) != 0);
}

/******************************************************************************
 * DESCRIPTION : Does the address match between 2 given address
 *
 * INPUTS      : The addresses to match and the prefixlen till which to match
 *               (i4Prefixlen)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : TRUE if match occurs
 *               FALSE is match doesn't occur
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
INT4
Ip6AddrMatch (tIp6Addr * pA1, tIp6Addr * pA2, INT4 i4Prefixlen)
#else
INT4
Ip6AddrMatch (pA1, pA2, i4Prefixlen)
     tIp6Addr           *pA1;
     tIp6Addr           *pA2;
     INT4                i4Prefixlen;
#endif
{

    INT4                i4Pdw = 0, i4_pbi = 0;

    if ((!(pA1)) || (!(pA2)))
    {
        return FALSE;
    }

    i4Pdw = i4Prefixlen >> 0x05;    /* Num of whole unsigned int in prefix */
    i4_pbi = i4Prefixlen & 0x1f;    /* Num of bits in incomplete UINT4 in 
                                     * prefix 
                                     */
    if (i4Pdw && ((i4Pdw << 2) <= 16))
    {
        if (MEMCMP (pA1, pA2, i4Pdw << 2))
        {
            return FALSE;
        }
    }

    if (i4_pbi)
    {
        UINT4               u4W1 = 0, u4_w2 = 0;
        UINT4               u4Mask = 0;
        if (i4Pdw >= 4)
        {
            return FALSE;
        }
        u4W1 = pA1->u4_addr[i4Pdw];
        u4_w2 = pA2->u4_addr[i4Pdw];

        u4Mask = OSIX_HTONL ((0xffffffff) << (0x20 - i4_pbi));

        if ((u4W1 ^ u4_w2) & u4Mask)
        {
            return FALSE;
        }
    }

    return TRUE;
}

/******************************************************************************
 * DESCRIPTION : Checks whether the given bit of the address is set or not
 *
 * INPUTS      : The address(pAddr) and bit to check(i4FnBit) starting 
 *               from 0 onwards
 *
 * OUTPUTS     : None
 *
 * RETURNS     : 0 , if address bit is not set
 *               non-zero ,if the address bit is set
 *
 * NOTES       :
 ******************************************************************************/

#ifdef __STDC__
INT4
Ip6AddrBitSet (tIp6Addr * pAddr, INT4 i4FnBit)
#else
INT4
Ip6AddrBitSet (pAddr, i4FnBit)
     tIp6Addr           *pAddr;
     INT4                i4FnBit;
#endif
{

    INT4                i4Dw = 0, i4Bit = i4FnBit;
    UINT4               u4B1 = 0, u4Mask = 0;

    i4Dw = i4Bit >> 0x05;

    u4B1 = pAddr->u4_addr[i4Dw];

    i4Bit = ~i4Bit;
    i4Bit &= 0x1f;
    u4Mask = OSIX_HTONL ((UINT4) 1 << i4Bit);
    return ((INT4) (u4B1 & u4Mask));
}

/******************************************************************************
 * DESCRIPTION : Checks if the addresses match at the given bit
 *
 * INPUTS      : The addresses (pA1,pA2) and the bit which is to be 
 *               matched (i4FnBit) starting from 0
 *
 * OUTPUTS     : None
 *
 * RETURNS     : 0 if the address do not match,non-zero if they match
 *
 * NOTES       :
 ******************************************************************************/

#ifdef __STDC__
INT4
Ip6AddrBitEqual (tIp6Addr * pA1, tIp6Addr * pA2, INT4 i4FnBit)
#else
INT4
Ip6AddrBitEqual (pA1, pA2, i4FnBit)
     tIp6Addr           *pA1;
     tIp6Addr           *pA2;
     INT4                i4FnBit;
#endif
{
    INT4                i4Dw = 0, i4Bit = i4FnBit;
    UINT4               u4B1 = 0, u4B2 = 0, u4Mask = 0;

    if ((!(pA1)) || (!(pA2)))
    {
        return 0;
    }

    i4Dw = i4Bit >> 0x05;

    u4B1 = pA1->u4_addr[i4Dw];
    u4B2 = pA2->u4_addr[i4Dw];

    i4Bit = ~i4Bit;
    i4Bit &= 0x1f;
    u4Mask = OSIX_HTONL ((UINT4) 1 << i4Bit);

    return !((u4B1 ^ u4B2) & u4Mask);
}

/******************************************************************************
 * DESCRIPTION : Gives the bit at which the addresses are differing
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The difference bit  between the 2 addresses
 *
 * NOTES       :
 ******************************************************************************/

#ifdef __STDC__
INT4
Ip6AddrDiff (tIp6Addr * pA1, tIp6Addr * pA2)
#else
INT4
Ip6AddrDiff (pA1, pA2)
     tIp6Addr           *pA1;
     tIp6Addr           *pA2;
#endif
{
    INT4                i, i4_res = 0, j = 31;
    UINT4               u4B2 = 0, u4_xb = 0, u4_bit = 0;

    for (i = 0; i < 4; i++)
    {
        u4_bit = pA1->u4_addr[i];
        u4B2 = pA2->u4_addr[i];

        u4_xb = u4_bit ^ u4B2;

        if (u4_xb)
        {
            u4_xb = OSIX_NTOHL (u4_xb);

            while (TestBit (j, (INT4 *) (VOID *) &u4_xb) == 0)
            {
                i4_res++;
                j--;
            }

            return (i * 32 + i4_res);
        }
    }

    return 128;
}

/******************************************************************************
 * DESCRIPTION : Copies One address pointer to another.
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to copied address.
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
VOID
Ip6AddrCopy (tIp6Addr * pAddrdst, tIp6Addr * pAddrsrc)
#else
VOID
Ip6AddrCopy (pAddrdst, pAddrsrc)
     tIp6Addr           *pAddrdst;
     tIp6Addr           *pAddrsrc;
#endif
{
    MEMCPY (pAddrdst, pAddrsrc, sizeof (tIp6Addr));
}

/******************************************************************************
 * DESCRIPTION : The address hash routine.The hash is performed over the
 *               last 64 bits of the address.This routine will a return value
 *               limited to max of 16.The levels till which u go on ex-or-ing
 *               can be made to depend on the number of Hash Buckets.
 *
 * INPUTS      : The IPv6 address (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The hash key
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
UINT1
Ip6AddrHash (tIp6Addr * pAddr)
#else
UINT1
Ip6AddrHash (pAddr)
     tIp6Addr           *pAddr;
#endif
{
    UINT4               u4Word = 0;
    UINT1               u1Tmp = 0;

    u4Word = pAddr->u4_addr[2] ^ pAddr->u4_addr[3];
    u1Tmp = (UINT1) (u4Word ^ (u4Word >> 16));
    u1Tmp = (UINT1) (u1Tmp ^ (u1Tmp >> 8));

    return ((UINT1) ((u1Tmp ^ (u1Tmp >> 4)) & 0x0f));
}

/*****************************************************************************
* DESCRIPTION : Fills the bits upto the prefix len from an
*               IPv6 address into another address and copies
*               zeroes into the remaining bits
*
* INPUTS      : pPref       -  Pointer to address to get copied
*               pAddr       -  Pointer to address from which
*                               bits are copied
*               i4NumBits  -  The number of bits to get copied
* OUTPUTS     : None
*
* RETURNS     : None
****************************************************************************/

#ifdef __STDC__
VOID
Ip6CopyAddrBits (tIp6Addr * pPref, tIp6Addr * pAddr, INT4 i4NumBits)
#else
VOID
Ip6CopyAddrBits (pPref, pAddr, i4NumBits)
     tIp6Addr           *pPref;
     tIp6Addr           *pAddr;
     INT4                i4NumBits;
#endif
{

    INT4                i4Dw = 0, i4_set_bit, i4Bit;
    UINT4               u4B1 = 0, u4Mask = 0;

    MEMSET (pPref, 0, sizeof (tIp6Addr));

    for (i4_set_bit = 0; i4_set_bit < i4NumBits; i4_set_bit++)
    {
        i4Dw = 0;
        u4B1 = u4Mask = 0;
        i4Bit = i4_set_bit;

        i4Dw = i4Bit >> 0x05;

        u4B1 = pAddr->u4_addr[i4Dw];
        i4Bit = ~i4Bit;
        i4Bit &= 0x1f;
        u4Mask = (UINT4) (OSIX_HTONL ((UINT4) (1 << i4Bit)));

        pPref->u4_addr[i4Dw] |= (u4B1 & u4Mask);
    }
}

/******************************************************************************
 * DESCRIPTION : Checks Whether an Address is inbetween 2 given addresses.
 *               The check is done in lexicographical order. 
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SUCCESS if the given address is between 2 addresses.
 *               FAILURE if the given address is not inbetween 2 addresses.     
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
INT4
Ip6IsAddrInBetween (tIp6Addr * pIp6Addr, tIp6Addr * p_ip6_addr2,
                    tIp6Addr * p_ip6_addr3)
#else
INT4
Ip6IsAddrInBetween (pIp6Addr, p_ip6_addr2, p_ip6_addr3)
     tIp6Addr           *pIp6Addr, *p_ip6_addr2, *p_ip6_addr3;
#endif
{
    if (pIp6Addr == NULL)
    {
        return (Ip6IsAddrGreater (p_ip6_addr3, p_ip6_addr2));
    }
    if (Ip6IsAddrGreater (pIp6Addr, p_ip6_addr2) == IP6_SUCCESS)
    {
        if (Ip6IsAddrGreater (p_ip6_addr2, p_ip6_addr3) == IP6_SUCCESS)
        {
            return IP6_FAILURE;
        }
        else
        {
            if (Ip6AddrMatch (p_ip6_addr2, p_ip6_addr3, IP6_ADDR_SIZE_IN_BITS))
                return IP6_FAILURE;

            return IP6_SUCCESS;
        }
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Checks if the given address is greater
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : SUCCESS, if the address passed as arg1 is greater
 *               FAILURE, if the address passed as arg1 is not greater
 *
 * RETURNS     : The difference bit  between the 2 addresses
 *
 * NOTES       :
 ******************************************************************************/

#ifdef __STDC__
INT4
Ip6IsAddrGreater (tIp6Addr * pIp6Addr1, tIp6Addr * p_ip6_addr2)
#else
INT4
Ip6IsAddrGreater (pIp6Addr1, p_ip6_addr2)
     tIp6Addr           *pIp6Addr1, *p_ip6_addr2;
#endif
{
    UINT1               u1I = 0;

    while ((u1I < IP6_ADDR_ACCESS_IN_BYTES) &&
           (p_ip6_addr2->ip6_addr_u.u1ByteAddr[u1I] ==
            pIp6Addr1->ip6_addr_u.u1ByteAddr[u1I]))
    {
        u1I++;
    }

    if (u1I == IP6_ADDR_ACCESS_IN_BYTES)
        return IP6_FAILURE;

    if (p_ip6_addr2->ip6_addr_u.u1ByteAddr[u1I] >
        pIp6Addr1->ip6_addr_u.u1ByteAddr[u1I])
        return IP6_SUCCESS;

    return IP6_FAILURE;
}

/* 
 * Error handling routines
 */

/******************************************************************************
 * DESCRIPTION : Returns the IPv6 address as string
 *
 * INPUTS      : Error Value,  address (pA1)
 *
 * OUTPUTS     : SUCCESS, if the address passed as arg1 is greater
 *               FAILURE, if the address passed as arg1 is not greater
 *
 * RETURNS     : IPv6 address as string
 *
 * NOTES       :
 ******************************************************************************/
UINT1              *
Ip6ErrPrintAddr (UINT1 u1ErrLvl, tIp6Addr * pAddr)
{
    UINT4               i;
    char               *cp;
    char               *ch;

    if (u1ErrLvl > i4Ip6ErrlvlFlag)
        return NULL;

    MAX_INCR (u2_err_addr_buf_num, 3);
    ch = ip6AddrErrBuf[u2_err_addr_buf_num];

    cp = ch;

    if (pAddr == NULL)
        return (NULL);

    memset (ch, 0, 50);

    for (i = 0; i < sizeof (tIp6Addr);)
    {
        SPRINTF (ch, "%02X%02X:", pAddr->u1_addr[i], pAddr->u1_addr[i + 1]);
        ch += 5;
        i += 2;
    }
    *(ch - 1) = '\0';

    return ((UINT1 *) cp);
}

/******************************************************************************
 * DESCRIPTION : Prints the IPv6 Interface Token.
 *
 * INPUTS      : Error level, interface token, and token length.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Null or the address.
 *
 * NOTES       :
 ******************************************************************************/
UINT1              *
Ip6ErrPrintIftok (UINT1 u1ErrLvl, UINT1 *pToken, UINT1 u1TokLen)
{
    INT4                i;
    char               *p, *cp;

    if (u1ErrLvl > i4Ip6ErrlvlFlag)
        return (NULL);

    MAX_INCR (u2ErrTokBufNum, 3);
    p = ip6TokErrBuf[u2ErrTokBufNum];

    cp = p;

    if (pToken == NULL || !u1TokLen)
        return (NULL);

    memset (p, 0, 50);

    for (i = 0; i < u1TokLen; i++)
    {
        SPRINTF (p, "%02X ", pToken[i]);
        p += 3;
    }
    *p = '\0';

    return ((UINT1 *) cp);
}

/******************************************************************************
 * DESCRIPTION : Routine for changing the error level flag for printing Messages
 *
 * INPUTS      : One value
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID.
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
VOID
Ip6SetErrlvl (UINT4 u4Value)
#else
VOID
Ip6SetErrlvl (u4Value)
     UINT4               u4Value;
#endif
{
    if (u4Value < ERROR_FATAL)
        return;

    i4Ip6ErrlvlFlag = (INT4) u4Value;
}

/******************************************************************************
 * DESCRIPTION : Routine for generating random number
 *
 * INPUTS      : Minumum and Maximum values for range restriction
 *
 * OUTPUTS     : Random Number
 *
 * RETURNS     : Null or the address.
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
UINT4
Ip6Random (UINT4 u4Min, UINT4 u4Max)
#else
UINT4
Ip6Random (u4Min, u4Max)
     UINT4               u4Min;
     UINT4               u4Max;
#endif
{
    UINT4               u4Rand;

    u4Rand = ((UINT4) rand () % u4Max);

    if (u4Rand < u4Min)
        u4Rand += (u4Max - u4Min);

    return (u4Rand);

}

/******************************************************************************
 * DESCRIPTION : Prints the IPv6 address in the standard format
 *
 * INPUTS      : One address (pA1)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Null or the address.
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
UINT1              *
Ip6PrintAddr (tIp6Addr * pAddr)
#else
UINT1              *
Ip6PrintAddr (pAddr)
     tIp6Addr           *pAddr;
#endif
{
    if (pAddr == NULL)
        return (NULL);
    else
        return (Ip6PrintNtop (pAddr));

}

static UINT1        hexc[] = "0123456789abcdef";

/******************************************************************************
 * DESCRIPTION : Prints the IPv6 address 
 *
 * INPUTS      : One address (pA1)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Null or the address.
 *
 * NOTES       :
 ******************************************************************************/
UINT1              *
Ip6PrintNtop (tIp6Addr * pAddr)
{
	return (Ip6PrintNtopWithBuf (pAddr, ip6AddrPrintBuf));
}

/******************************************************************************
 * FUNCTION    : Ip6PrintNtopWithBuf 
 *
 * DESCRIPTION : Prints the given IP address to the given address buffer 
 *               in proper IPv4 or IPv6 format
 *
 * INPUTS      : Input Address, Output Buffer
 *
 * OUTPUTS     : au1AddrPrintBuf
 *
 * RETURNS     : Null or the address.
 *
 ******************************************************************************/
UINT1              *
Ip6PrintNtopWithBuf (tIp6Addr * pAddr, CHR1  au1AddrPrintBuf[][50])
{
    INT4                value;
    char               *tmp, *cp;
    INT4                start = -1, count = 0, i, i4Position = -1, i4PreCount =
        0;

    if (pAddr == NULL)
        return NULL;

    MAX_INCR (u2_addr_buf_num, 3);
    cp = au1AddrPrintBuf[u2_addr_buf_num];
    memset (cp, 0, 50);

    tmp = cp;

    /* weird special cases for IPv4 address */
    if ((pAddr->u4_addr[0] == 0) && (pAddr->u4_addr[1] == 0))
    {
        if (pAddr->u4_addr[2] == 0)
        {
            if (pAddr->u4_addr[3] == 0)
            {
                strncpy (cp, "::", STRLEN("::"));
		cp[STRLEN("::")] = '\0';
            }
            else if (OSIX_NTOHL (pAddr->u4_addr[3]) <= 0xff)
            {
                SPRINTF (cp, "::%x", pAddr->u1_addr[15]);
            }
            else
                SPRINTF (cp, "%d.%d.%d.%d", pAddr->u1_addr[12],
                         pAddr->u1_addr[13], pAddr->u1_addr[14],
                         pAddr->u1_addr[15]);

            return (UINT1 *) cp;
        }
    }

    /*
     * Find the first string of consecutive zeros
     */
    for (i = 0; i < IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        if ((pAddr->u1_addr[2 * i] == 0) && (pAddr->u1_addr[2 * i + 1] == 0))
        {
            if (start > -1)
            {
                count++;
            }
            else
            {
                start = i;
                count = 1;
            }
        }
        else
        {
            if (count > 1)
            {
                if ((i < IP6_ADDR_SPLITTER) && (i4PreCount == 0))
                {
                    i4Position = start;
                    i4PreCount = count;
                    start = -1;
                    count = 0;
                }
                else
                {
                    break;
                }
            }
            else
            {
                start = -1;
                count = 0;
            }
        }
    }
    if (i4PreCount >= count)
    {
        count = i4PreCount;
        start = i4Position;
    }

    if (start == 0)
        *tmp++ = ':';

    for (i = 0; i < IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        if ((i == start) && (IP6_ADDR_CURR_POS != start))
        {
            *tmp++ = ':';
            i += count - 1;
        }
        else
        {
            value = (pAddr->u1_addr[2 * i] << 8) | pAddr->u1_addr[2 * i + 1];
            if (value >> 12)
                *tmp++ = (char) hexc[(value >> 12) & 0xf];

            if (value >> 8)
                *tmp++ = (char) hexc[(value >> 8) & 0xf];
            if (value >> 4)
                *tmp++ = (char) hexc[(value >> 4) & 0xf];

            *tmp++ = (char) hexc[value & 0xf];
            if (i != 7)
                *tmp++ = ':';
        }
    }
    *tmp = '\0';

    return (UINT1 *) cp;
}

/****************************************************************************** 
    * DESCRIPTION : Finds the type of the address 
    * 
    * INPUTS      : The address (pAddr) 
    * 
    * OUTPUTS     : None 
    * 
    * RETURNS     : ADDR6_MULTI,ADDR6_LLOCAL,ADDR6_V4_COMPAT,ADDR6_UNSPECIFIED, 
    *               ADDR6_UNICAST based on the type of the address 
    * 
    * NOTES       : 
******************************************************************************/

INT1
Ip6AddrType (pAddr)
     tIp6Addr           *pAddr;
{

    if (IS_ADDR_MULTI (*pAddr))
    {
        return ADDR6_MULTI;
    }

    if (IS_ADDR_LLOCAL (*pAddr))
    {
        return ADDR6_LLOCAL;
    }

    if (IS_ADDR_LOOPBACK (*pAddr))
    {
        return ADDR6_LOOPBACK;
    }

    if (IS_ADDR_V4_COMPAT (*pAddr))
    {
        return ADDR6_V4_COMPAT;
    }

    if (IS_ADDR_UNSPECIFIED (*pAddr))
    {
        return ADDR6_UNSPECIFIED;
    }

    return ADDR6_UNICAST;
}

/******************************************************************************
 *  Function Name   : UtilConvMcastIPV62Mac
 *  Description     : This function converts the IPv6 multicast address to the 
 *                    euivalent multicast MAC address
 *  Input(s)        : None
 *  Output(s)       : None
 *  Returns         : Void
 ******************************************************************************/

PUBLIC VOID
UtilConvMcastIPV62Mac (UINT1 *pu1Addr, UINT1 *pu1McMacAddr)
{
    pu1McMacAddr[0] = 0x33;
    pu1McMacAddr[1] = 0x33;
    pu1McMacAddr[2] = pu1Addr[12];
    pu1McMacAddr[3] = pu1Addr[13];
    pu1McMacAddr[4] = pu1Addr[14];
    pu1McMacAddr[5] = pu1Addr[15];

}

/******************************************************************************
 * DESCRIPTION : Compares the given IPv6 addresses
 *
 * INPUTS      : pAddr1, pAddr2
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_RB_LESSER if pAddr1 < pAddr2
 *               IP6_RB_GREATER if pAddr1 > pAddr2
 *               IP6_RB_EQUAL if pAddr1 == pAddr2
 ******************************************************************************/

INT4
Ip6AddrCompare (tIp6Addr Addr1, tIp6Addr Addr2)
{
    UINT1               u1Cntr = 0;

    while ((u1Cntr < IP6_ADDR_ACCESS_IN_BYTES) &&
           (Addr1.ip6_addr_u.u1ByteAddr[u1Cntr] ==
            Addr2.ip6_addr_u.u1ByteAddr[u1Cntr]))
    {
        u1Cntr++;
    }

    if (u1Cntr == IP6_ADDR_ACCESS_IN_BYTES)
    {
        return IP6_ZERO;
    }

    if (Addr1.ip6_addr_u.u1ByteAddr[u1Cntr] <
        Addr2.ip6_addr_u.u1ByteAddr[u1Cntr])
    {
        return IP6_MINUS_ONE;
    }
    else
    {
        return IP6_ONE;
    }
}

/******************************************************************************
 * DESCRIPTION : Finds the scope of the address
 *
 * INPUTS      : IPv6 address (Addr)
 * 
 * RETURNS     : Scope of the IPv6 Address 
 *
 * Comment     : Added as a part of code change for RFC4007
 * 
 ********************************************************************************/
UINT1
Ip6GetAddrScope (tIp6Addr * pAddr)
{
    UINT1               u1Scope = 0;
    UINT1               u1Addr = 0;
    /* Scope Identification for Mulitcast Address  */
    if (IS_ADDR_MULTI (*pAddr))
    {
        u1Addr = (UINT1) (pAddr->u1_addr[1] & 0x0F);
        if (u1Addr == 0x01)
        {
            u1Scope = ADDR6_SCOPE_INTLOCAL;
        }
        else if (u1Addr == 0x02)
        {
            u1Scope = ADDR6_SCOPE_LLOCAL;
        }
        else if (u1Addr == 0x03)
        {
            u1Scope = ADDR6_SCOPE_SUBNETLOCAL;
        }
        else if (u1Addr == 0x04)
        {
            u1Scope = ADDR6_SCOPE_ADMINLOCAL;
        }
        else if (u1Addr == 0x05)
        {
            u1Scope = ADDR6_SCOPE_SITELOCAL;
        }
        else if (u1Addr == 0x06)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGN6;
        }
        else if (u1Addr == 0x07)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGN7;
        }
        else if (u1Addr == 0x08)
        {
            u1Scope = ADDR6_SCOPE_ORGLOCAL;
        }
        else if (u1Addr == 0x09)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGN9;
        }
        else if (u1Addr == 0x0A)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGNA;
        }
        else if (u1Addr == 0x0B)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGNB;
        }
        else if (u1Addr == 0x0C)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGNC;
        }
        else if (u1Addr == 0x0D)
        {
            u1Scope = ADDR6_SCOPE_UNASSIGND;
        }
        else if (u1Addr == 0x0E)
        {
            u1Scope = ADDR6_SCOPE_GLOBAL;
        }
        else if ((u1Addr == 0x00) || (u1Addr == 0x0F))
        {
            u1Scope = ADDR6_SCOPE_RESERVEDF;
        }
        else
        {
            u1Scope = ADDR6_SCOPE_INVALID;
        }
        return u1Scope;
    }
    /* Scope Identification for unicast Link-local Address and Unspecified Address  */
    if (IS_ADDR_LLOCAL (*pAddr) || IS_ADDR_UNSPECIFIED (*pAddr))
    {
        u1Scope = ADDR6_SCOPE_LLOCAL;
        return u1Scope;
    }
    /* Scope Identification for Loopback  */
    if (IS_ADDR_LOOPBACK (*pAddr))
    {
        u1Scope = ADDR6_SCOPE_INTLOCAL;
        return u1Scope;
    }
    u1Scope = ADDR6_SCOPE_GLOBAL;
    return u1Scope;
}

/******************************************************************************
 * DESCRIPTION : Copies One char(scope-zone name)  pointer to another.
 *
 * INPUTS      : Two Character Pointer (pau1ZoneName1,pau1ZoneName2)
 *
 * OUTPUTS     : Pointer to copied Scope-zone name.
 *
 * RETURNS     : None.
 
 * NOTES       : This functions is added as a part of RFC4007 Code change
 *****************************************************************************/
VOID
Ip6ScopeZoneCopy (pZonedst, pZonesrc)
     UINT1              *pZonedst;
     UINT1              *pZonesrc;

{
    SPRINTF ((CHR1 *) pZonedst, "%s", pZonesrc);
}

/*************************************************************************************
 * DESCRIPTION : Routine for calculating the checksum
 *
 * INPUTS      : Pointer to Source Address (pSrc), Pointer to Dest Address (pDst), 
 *               Length of the Packet (u4Len), Protocol (u1Proto), Buffer pointer (pBuf),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : Checksum
 *
 * NOTES       :
 **************************************************************************************/

UINT2
UtlIp6LinChecksum (tIp6Addr * pSrc, tIp6Addr * pDst, UINT4 u4Len, UINT1 u1Proto,
                   UINT1 *pu1Buf)
{
    UINT4               u4Sum = 0;
    UINT2              *pu2Buf = NULL;
    UINT2               u2Proto = (UINT2) u1Proto;
    UINT2               u2Len = 0;
    UINT2               u2Count = 0;
    UINT2               u2Temp = 0;
    UINT2               u2CheckSum = 0;

    for (u2Count = 0; u2Count < IP6_ADDR_ACCESS_IN_SHORT_INT; u2Count++)
    {
        u4Sum += OSIX_HTONS (pSrc->u2_addr[u2Count]);
    }

    for (u2Count = 0; u2Count < IP6_ADDR_ACCESS_IN_SHORT_INT; u2Count++)
    {
        u4Sum += OSIX_HTONS (pDst->u2_addr[u2Count]);
    }

    u4Sum += (OSIX_HTONL (u4Len));
    u4Sum += (OSIX_HTONS (u2Proto));

    u2Len = (UINT2) (u4Len / 2);

    pu2Buf = (UINT2 *) (VOID *) pu1Buf;

    for (u2Count = 0; u2Count < u2Len; u2Count++)
    {
        u2Temp = pu2Buf[u2Count];
        u4Sum += u2Temp;
    }

    while (u4Sum >> 16)
    {
        u4Sum = (u4Sum & 0xffff) + (u4Sum >> 16);
    }

    u2CheckSum = (UINT2) (~(u4Sum));

    return (u2CheckSum);
}
