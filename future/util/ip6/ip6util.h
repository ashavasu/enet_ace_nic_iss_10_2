/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6util.h,v 1.18 2017/02/06 10:45:30 siva Exp $
 *
 * Description: Macros and prototypes for ip6 address related functions.
 */

#ifndef _IP6UTIL_H_
#define _IP6UTIL_H_
#include "lr.h"
#include "ipv6.h"

#define  ERROR_FATAL            1
#define  ERROR_MINOR            2


INT4 Ip6AddrMatch  PROTO ((tIp6Addr * p_addr1,
                            tIp6Addr * p_addr2, INT4 i4Prefixlen));

INT4 Ip6AddrBitSet PROTO ((tIp6Addr * pAddr, INT4 i4Bit));

INT4 Ip6AddrBitEqual PROTO ((tIp6Addr * p_addr1,
                                tIp6Addr * p_addr2, INT4 i4Bit));

INT4 Ip6AddrDiff  PROTO ((tIp6Addr * p_addr1, tIp6Addr * p_addr2));

UINT1 Ip6AddrHash PROTO ((tIp6Addr * pAddr));

VOID Ip6AddrCopy  PROTO ((tIp6Addr * pDst, tIp6Addr * pSrc));

VOID Ip6CopyAddrBits PROTO ((tIp6Addr * pPref, tIp6Addr * pAddr,
                                INT4 i4_pref_len));
INT4 Ip6IsAddrGreater PROTO ((tIp6Addr * pIp6Addr1,
                                       tIp6Addr * p_ip6_addr2));

INT4 Ip6IsAddrInBetween PROTO ((tIp6Addr * pIp6Addr,
                                         tIp6Addr * p_ip6_addr2,
                                         tIp6Addr * p_ip6_addr3));

UINT1 *Ip6PrintAddr PROTO ((tIp6Addr * pAddr));

UINT1 *Ip6PrintNtop PROTO ((tIp6Addr * pAddr));

UINT1 * Ip6PrintNtopWithBuf PROTO ((tIp6Addr * pAddr, CHR1 au1AddrPrintBuf[][50]));

UINT4 Ip6Random    PROTO ((UINT4 minVal, UINT4 max_val));

VOID Ip6SetErrlvl PROTO ((UINT4 u4Value));

UINT1 *Ip6ErrPrintAddr
PROTO ((UINT1 u1ErrLvl, tIp6Addr * pAddr));

UINT1 *Ip6ErrPrintIftok
PROTO ((UINT1 u1ErrLvl, UINT1 *pToken, UINT1 u1TokLen));

INT1 Ip6AddrType  PROTO ((tIp6Addr * pAddr));

PUBLIC VOID
UtilConvMcastIPV62Mac (UINT1 *pu1Addr, UINT1 *pu1McMacAddr);

INT4 Ip6AddrCompare PROTO ((tIp6Addr Addr1, tIp6Addr Addr2));
/* Prototype Added For RFC4007 Code Changes- Start*/
UINT1
Ip6GetAddrScope PROTO ((tIp6Addr *pAddr));

VOID
Ip6ScopeZoneCopy PROTO ((UINT1 *pZonedst,UINT1 *pZonesrc));

UINT2
UtlIp6LinChecksum PROTO ((tIp6Addr * pSrc, tIp6Addr * pDst, UINT4 u4Len,
                          UINT1 u1Proto, UINT1 *pu1Buf));

/* Prototype Added For RFC4007 Code Changes- End*/
#endif
