 /********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: fslib.h,v 1.1 2015/04/28 12:53:03 siva Exp $ 
 *
 * Description:This files contains the macros and definitions used for
 *             view log
 *
 *******************************************************************/

#ifndef __FSLIB_H__
#define __FSLIB_H__ 1


#include "lr.h"
#include "fssocket.h"
#include "cli.h"
#include "utlmacro.h"

/* defines for View system log */


#define DAY_OF_YEAR                DayOfYear
#define NUM_DAYS_IN_YEAR               DaysInYear
#define DAY_OF_MONTH               DayOfMonth
   
#define MAX_DISP                  1200

typedef struct _fs_tm
{
        int     tm_sec;         /* Seconds: 0-59 */
        int     tm_min;         /* Minutes: 0-59 */
        int     tm_hour;        /* Hours since midnight: 0-23 */
        int     tm_mday;        /* Day of the month: 1-31 */
        int     tm_mon;         /* Months *since* january: 0-11 */
        int     tm_year;        /* Years since 1900 */
        int     tm_wday;        /* Days since Sunday (0-6) */
        int     tm_yday;        /* Days since Jan. 1: 0-365 */
        int     tm_isdst;       /* +1 Daylight Savings Time, 0 No DST,
                                 * -1 don't know */
}tFslibTm;

typedef tUtlTm tFsTm;

extern UINT4 gu4CurrentTicks; 
extern tFsTm SysTime;           /* Time stamp containing date and time */

/* To log the ip address of the web browser host or telnet client host
 * whenever the host computer contacts the FS router
 */
extern UINT4 gu4WebHostIpAddr; 
extern UINT4 gu4TelnetHostIpAddr; 

UINT4 GetCurrentTime(VOID);
int   DaysInYear(int year);
int   DayOfYear(int year, int mday, int mon);
void  DayOfMonth (int yday, int year, int * pmday, int * pmon);
UINT1 * GetTimeStamp (void);
#endif /* __FSLIB_H__ */
