/********************************************************************
*  Copyright (C) Future Software Limited, 1997-98,2001
*  
*  $Id: fslib.c,v 1.1 2015/04/28 12:53:03 siva Exp $
* 
*  Description: Code for view log mechanism
*  
*******************************************************************/

#include "fslib.h"
#ifdef OS_VXWORKS
#include "stat.h"
#endif

UINT4               gu4CurrentTicks;    /* For storing the time ticks */
tFsTm               SysTime;    /* Time stamp containing date and time */

static const unsigned short int mon_yday[2][13] = {
/* Normal Year */ {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334,
                   365},
/* Leap Year   */ {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335,
                   366}
};

/* the following structure stores the days of the week */
const UINT1        *au1WeekDay[7] = {
    (const UINT1 *) "Sun",
    (const UINT1 *) "Mon",
    (const UINT1 *) "Tue",
    (const UINT1 *) "Wed",
    (const UINT1 *) "Thu",
    (const UINT1 *) "Fri",
    (const UINT1 *) "Sat"
};

/* the following structure stores the months in a year */
const UINT1        *au1Month[12] = {
    (const UINT1 *) "Jan",
    (const UINT1 *) "Feb",
    (const UINT1 *) "Mar",
    (const UINT1 *) "Apr",
    (const UINT1 *) "May",
    (const UINT1 *) "Jun",
    (const UINT1 *) "Jul",
    (const UINT1 *) "Aug",
    (const UINT1 *) "Sep",
    (const UINT1 *) "Oct",
    (const UINT1 *) "Nov",
    (const UINT1 *) "Dec"
};

/****************************************************************************/
/*    Function Name      : GetCurrentTime                                   */
/*    Description        : This function calculates the current system time */
/*                         updates in SysTime Structure.                    */
/*    Input(s)           : None                                             */
/*    Output(s)          : None                                             */
/*    Returns            : 1 on success                                     */
/****************************************************************************/

UINT4
GetCurrentTime (VOID)
{
    UINT4               u4ElapsedSecs;
    tFsTm              *t1;

    tOsixSysTime        CurTime;
    UINT4               u4Temp1;
    UINT4               u4Temp2;
    UINT4               u4Temp3;

    /* Get the Current System Ticks.Substract the Stored Ticks
     * (the ticks taken while initialising the system time) from
     * this value to know how much time elapsed after the system 
     * time init.
     */

    OsixGetSysTime (&CurTime);
    u4ElapsedSecs = (CurTime - gu4CurrentTicks) / 100;
    gu4CurrentTicks = CurTime;

    t1 = &SysTime;

    /* Update the SysTime Structure by adding the elapsed time (u4ElapsedSecs)
     * to the system time when this was last updated(or initialised).
     */
    t1->tm_sec += u4ElapsedSecs;

    /* Get the current day of the year */
    if (t1->tm_yday == 0)
    {
        t1->tm_yday =
            (UINT4) DAY_OF_YEAR ((int) t1->tm_year, (int) t1->tm_mday,
                                 (int) t1->tm_mon);
    }
    /* Check if the current time has crossed the minute boundary */
    if (t1->tm_sec >= 60)
    {
        /* Calculate the number of minutes and seconds elapsed and update
         * the current time with it 
         */
        u4Temp1 = t1->tm_sec - ((t1->tm_sec / 60) * 60);
        u4Temp2 = (t1->tm_sec - u4Temp1) / 60;
        t1->tm_sec = u4Temp1;
        t1->tm_min += u4Temp2;

        /* Check if the current time has crossed the hour boundary */
        if (t1->tm_min >= 60)
        {
            /* Calculate the number of hours and minutes elapsed and update
             * the current time with it.
             */
            u4Temp1 = t1->tm_min - ((t1->tm_min / 60) * 60);
            u4Temp2 = (t1->tm_min - u4Temp1) / 60;
            t1->tm_min = u4Temp1;
            t1->tm_hour += u4Temp2;
            /* Check if the current time has crossed the day boundary */
            if (t1->tm_hour >= 24)
            {
                /* Calculate the number of days and hours elapsed and update
                 * the current time with it.
                 */
                u4Temp1 = t1->tm_hour - (t1->tm_hour / 24) * 24;
                u4Temp2 = (t1->tm_hour - u4Temp1) / 24;
                t1->tm_hour = u4Temp1;
                t1->tm_yday += u4Temp2;
                t1->tm_mday += u4Temp2;
                t1->tm_wday += u4Temp2;
                u4Temp3 = DAYS_IN_YEAR (t1->tm_year);
                /* Get the proper day of the month and day of the year */
                if (t1->tm_yday >= u4Temp3)
                {
                    u4Temp1 = t1->tm_yday - (t1->tm_yday / u4Temp3) * u4Temp3;
                    u4Temp2 = (t1->tm_yday - u4Temp1) / u4Temp3;
                    t1->tm_yday = u4Temp1;
                    t1->tm_year += u4Temp2;
                    DAY_OF_MONTH ((int) t1->tm_yday, (int) t1->tm_year,
                                  (int *) &t1->tm_mday, (int *) &t1->tm_mon);
                }
                /* Get the day of the week */
                if (t1->tm_wday >= 7)
                {
                    u4Temp1 = t1->tm_wday - (t1->tm_wday / 7) * 7;
                    u4Temp2 = (t1->tm_wday - u4Temp1) / 7;
                    t1->tm_wday = u4Temp1;
                }
            }
        }
    }
    return (1);
}

/****************************************************************************/
/*    Function Name      : DaysInYear                                       */
/*    Description        : This function gets the number of days in the     */
/*                         current year                                     */
/*    Input(s)           : year : the current year                          */
/*    Output(s)          : None                                             */
/*    Returns            : The number of days in the year                   */
/****************************************************************************/
int
DaysInYear (int year)
{
    if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
    {
        return (366);
    }
    else
    {
        return (365);
    }
}

/****************************************************************************/
/*    Function Name      : DayOfYear                                        */
/*    Description        : This function returns the day of year            */
/*    Input(s)           : year : the current year                          */
/*                         mday : the day of the month                      */
/*                         mon  : the current month                         */
/*    Output(s)          : None                                             */
/*    Returns            : The day of the year                              */
/****************************************************************************/
int
DayOfYear (int year, int mday, int mon)
{
    int                 i1Index1;
    if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
    {
        i1Index1 = 1;
    }
    else
    {
        i1Index1 = 0;
    }
    return (mon_yday[i1Index1][mon] + mday);
}

/****************************************************************************/
/*    Function Name      : DayOfMonth                                       */
/*    Description        : This function returns the day of the month       */
/*    Input(s)           : yday  : day of the year                          */
/*                         year  : the current year                         */
/*    Output(s)          : pmday : pointer to the day of the month          */
/*                         pmon  : pointer to the current month             */
/*    Returns            : None                                             */
/****************************************************************************/
void
DayOfMonth (int yday, int year, int *pmday, int *pmon)
{
    int                 i1Index1;
    int                 i1Index2;

    if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
    {
        i1Index1 = 1;
    }
    else
    {
        i1Index1 = 0;
    }
    for (i1Index2 = 1; i1Index2 <= 12; i1Index2++)
    {
        if (yday <= mon_yday[i1Index1][i1Index2])
        {
            *pmday = yday - mon_yday[i1Index1][i1Index2 - 1];
            *pmon = i1Index2;
            return;
        }
    }
    return;
}

/****************************************************************************/
/*    Function Name      : GetTimeStamp                                     */
/*    Description        : This function returns the current time stamp     */
/*                         after updating the current time in SysTime       */
/*                         structure                                        */
/*    Input(s)           : NONE                                             */
/*    Output(s)          : NONE                                             */
/*    Returns            : None                                             */
/****************************************************************************/

UINT1              *
GetTimeStamp (void)
{
    tFsTm              *t1;
    char               *pu1TimeStamp;
    UINT2               u2Size = 0;
    char                au1TempTimeStamp[64];
    static UINT1        au1TimeStamp[128];

    /* Update the Current time in SysTime Structure by calling this fn */
    GetCurrentTime ();

    /* Get the Current System Time */
    t1 = &SysTime;
    pu1TimeStamp = &au1TempTimeStamp[0];

    /* Format into readable form */

    SPRINTF ((char *) pu1TimeStamp, "%s ", au1WeekDay[t1->tm_wday]);
    u2Size = (UINT2) (u2Size + 4);
    pu1TimeStamp = (pu1TimeStamp + 4);
    SPRINTF ((char *) pu1TimeStamp, "%s ", au1Month[t1->tm_mon]);
    u2Size = (UINT2) (u2Size + 4);
    pu1TimeStamp = (pu1TimeStamp + 4);
    SPRINTF ((char *) pu1TimeStamp, "%2u ", t1->tm_mday);
    u2Size = (UINT2) (u2Size + 3);
    pu1TimeStamp = (pu1TimeStamp + 3);
    SPRINTF ((char *) pu1TimeStamp, "%2u:", t1->tm_hour);
    u2Size = (UINT2) (u2Size + 3);
    pu1TimeStamp = (pu1TimeStamp + 3);
    SPRINTF ((char *) pu1TimeStamp, "%2u:", t1->tm_min);
    u2Size = (UINT2) (u2Size + 3);
    pu1TimeStamp = (pu1TimeStamp + 3);
    SPRINTF ((char *) pu1TimeStamp, "%2u ", t1->tm_sec);
    u2Size = (UINT2) (u2Size + 3);
    pu1TimeStamp = (pu1TimeStamp + 3);
    SPRINTF ((char *) pu1TimeStamp, "%4u ", t1->tm_year);
    u2Size = (UINT2) (u2Size + 5);
    pu1TimeStamp = (pu1TimeStamp + 5);
    pu1TimeStamp -= u2Size;

    SPRINTF ((char *) au1TimeStamp, "%s", pu1TimeStamp);
    return (au1TimeStamp);
}
