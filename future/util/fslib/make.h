
# $Id: make.h,v 1.1 2015/04/28 12:53:03 siva Exp $                     #
# Top level make include files
# ----------------------------

include ../../LR/make.h
include ../../LR/make.rule

# Compilation switches
# --------------------
FSLIB_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES} -UTRACE_WANTED

# Directories
# -----------
FSLIB_BASE_DIR    = ${BASE_DIR}/util/fslib

# Include files
# -------------

FSLIB_INCLUDE_FILES =

FSLIB_FINAL_INCLUDE_FILES = ${FSLIB_INCLUDE_FILES}

# Include directories
# -------------------

FSLIB_INCLUDE_DIRS = -I${FSLIB_BASE_DIR}

FSLIB_FINAL_INCLUDE_DIRS = ${FSLIB_INCLUDE_DIRS}\
                         ${COMMON_INCLUDE_DIRS}


# Project dependencies
# --------------------

FSLIB_DEPENDENCIES = $(COMMON_DEPENDENCIES)\
                   $(FSLIB_FINAL_INCLUDE_FILES) \
                   $(FSLIB_BASE_DIR)/fslib.h \
                   $(FSLIB_BASE_DIR)/Makefile \
                   $(FSLIB_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
