/******************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpon.h,v 1.13 2017/08/31 14:02:01 siva Exp $
 *
 * Description: This file includes the generated *on.h files for all the mibs 
 *             
 *******************************************************************************/

#ifndef _SNMP_ON_H
#define _SNMP_ON_H


typedef struct MIB_ON {
    tRBNodeEmbd   NameRbNode;
    tRBNodeEmbd   OidRbNode;
    char *pName;
    char *pNumber;
}tMIB_ON;

tMIB_ON MibONTable[] = {


{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"ccitt","0"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"iso","1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"lldpExtensions","1.0.8802.1.1.2.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"lldpV2Xdot1MIB","1.0.8802.1.13.1.5.32962"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"org","1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"dod","1.3.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"internet","1.3.6.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"directory","1.3.6.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"mgmt","1.3.6.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"mib-2","1.3.6.1.2.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"ip","1.3.6.1.2.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0 , 0 , 0 }}},{{{0x0, 0x0}, 0x0,  0 , {0 , 0 , 0 }}},"transmission","1.3.6.1.2.1.10"},

#ifdef LLDP_WANTED
#include "sd1lv2on.h"
#include "sd3lv2on.h"
#include "stdlldpon.h"
#include "stdot1lldpon.h"
#include "stdot3lldpon.h"
#include "slldv2on.h"
#include "fslldpon.h"
#include "fslldpv2on.h"
#include "fslldpmedon.h"
#include "stdlldpmedxon.h"
#endif

#ifdef BGP_WANTED
#include "bgp4Teston.h"
#include "fsbgp4on.h"
#include "fsmpbgp4on.h"
#include "stdbgp4on.h"
#endif

#ifdef VXLAN_WANTED
#include "fsvxlanon.h"
#endif

#ifdef DHCP_SRV_WANTED
#include "fsdhcpson.h"
#endif

#ifdef DVMRP_WANTED
#include "fsdvmrpon.h"
#endif

#ifdef RM_WANTED
#include "fsrmon.h"
#endif

#ifdef HB_WANTED
#include "fshbon.h"
#endif

#ifdef ICCH_WANTED
#include "fsicchon.h"
#endif

#ifdef LA_WANTED
#include "fslaon.h"
#include "stdlaon.h"
#endif

#include "fscfaon.h"
#include "fsmptunlon.h"
#include "fstunlon.h"
#include "ifmibon.h"
#include "stdetheron.h"
#include "stdmauon.h"
#include "fsipdbon.h"
#include "fsmiipdbon.h"
#include "fsdhcsnpon.h"
#include "fsmidhcsnpon.h"
#include "fsusermgmon.h"

#ifdef RMON2_WANTED
#include "fsrmon2on.h"
#include "stdrmon2on.h"
#endif

#ifdef RMON_WANTED
#include "fsrmonon.h"
#include "stdrmonon.h"
#include "stdhcrmonon.h"
#endif

#ifdef SYSLOG_WANTED
#include "fssyslgon.h"
#endif

#ifdef ISS_WANTED
#include "fsisson.h"
#include "fsissmeton.h"
#include "fsissaclon.h"
#include "fsissexton.h"
#include "stdenton.h"
#endif

#ifdef DSMON_WANTED
#include "fsdsmonon.h"
#include "stddsmonon.h"
#endif

#ifdef BEEP_SERVER_WANTED
#include "fsbpsrvon.h"
#endif

#ifdef BFD_WANTED
#include "fsbfdon.h"
#include "fsmsbfdon.h"
#include "fsstdbfdon.h"
#include "fsmpbfdon.h"
#endif

#ifdef ISSU_WANTED
#include "fsissuon.h"
#endif

#ifdef EVB_WANTED
#include "std1evbon.h"
#include "std1lldevbon.h"
#include "fsmid1evbon.h"
#endif

#ifdef ECFM_WANTED
#include "fscfmexton.h"
#include "fscfmmion.h"
#include "stdecfmon.h"
#include "cfmv2exton.h"
#include "fsecfmon.h"
#include "fsmiy1731on.h"
#endif

#ifdef CLKIWF_WANTED
#include "fsclkiwfon.h"
#endif 

#ifdef CN_WANTED
#include "fscnon.h"
#include "stdcnon.h"
#endif

#ifdef DCBX_WANTED
#include "fsdcbxon.h"
#include "stddcbon.h"
#endif
#ifdef RBRG_WANTED
#include "fsrbridgeon.h"
#endif
#ifdef DHCP6_SRV_WANTED
#include "fsdh6son.h"
#endif

#ifdef DHCP6_CLNT_WANTED
#include "fsdh6con.h"
#endif

#ifdef DHCPC_WANTED
#include "fsdhclienton.h"
#endif

#ifdef DHCP6_RLY_WANTED
#include "fsdh6ron.h"
#endif

#ifdef PBB_WANTED
#include "fsdot1ahon.h"
#endif

#ifdef PBBTE_WANTED
#include "fspbton.h"
#include "fsdot1ayon.h"
#endif

#ifdef ELMI_WANTED
#include "fselmion.h"
#endif

#ifdef ELPS_WANTED
#include "fselpson.h"
#endif

#ifdef ERPS_WANTED
#include "fserpson.h"
#endif

#ifdef Y1564_WANTED
#include "fsy1564on.h"
#endif


#ifdef RFC2544_WANTED
#include "fs2544on.h"
#endif

#ifdef RFC6374_WANTED
#include "fs6374on.h"
#endif

#ifdef EOAM_FM_WANTED
#include "fsfmon.h"
#endif
#ifdef EOAM_WANTED
#include "stdeoamon.h"
#include "fseoamexon.h"
#endif

#ifdef FIREWALL_WANTED
#include "fsfwlon.h"
#endif

#ifdef WEBNM_WANTED
#include "fshttpon.h"
#endif

#ifdef IGMP_WANTED
#include "fsigmpon.h"
#include "stdigmpon.h"
#include "fsmldon.h"
#include "stdmldon.h"
#include "fsmgmdon.h"
#include "stdmgmdon.h"
#endif

#ifdef IGMPPRXY_WANTED
#include "fsigpon.h"
#endif

#ifdef DIFFSRV_WANTED
/*MRVLLS supports QOS instead of DIFFSRV*/
#ifdef MRVLLS
#include "fsqoson.h"
#else
#include "fsissdfson.h"
#endif
#include "stdqoson.h"
#endif


#ifdef MBSM_WANTED
#include "fsmbsmon.h"
#endif

#ifdef MFWD_WANTED
#include "fsmfwdon.h"
#include "mfwdcmnon.h"
#endif

#ifdef OSPF3_WANTED
#include "fsmio3on.h"
#include "fsmisos3on.h"
#include "fsmitos3on.h"
#include "fso3teston.h"
#include "fsos3on.h"
#include "ospf3on.h"
#endif

#include "fsmirtm6on.h"
#include "fsmirtmon.h"

#ifdef VRRP_WANTED
#include "fsvrrpon.h"
#include "stdvrrpon.h"
#include "stdvrrp3on.h"
#include "fsvrrp3on.h"
#endif

#ifdef VCM_WANTED
#include "fsvcmon.h"
#include "fssispon.h"
#endif

#ifdef TCP_WANTED
#include "fstcpon.h"
#include "stdtcpon.h"
#include "fsmptcpon.h"
#include "fsmstcpipvxon.h"
#endif

#ifdef DNS_WANTED
#include "fsdnson.h"
#include "stddnson.h"
#endif

#ifdef VLAN_WANTED
#include "fsdot1adon.h"
#include "stdvlanon.h"
#include "fsm1adon.h"
#include "fsmpbon.h"
#include "fsmpvlanon.h"
#include "fsmsbexton.h"
#include "fsmsbrgon.h"
#include "fsbridgeon.h"
#include "fsmsvlanon.h"
#include "fsmvlexton.h"
#include "fspbon.h"
#include "fsvlanon.h"
#include "fsvlnexton.h"
#include "stdbrgexton.h"
#include "stdbridgeon.h"
#include "std1q1apon.h"
#include "std1d1apon.h"
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
#include "fspimon.h"
#include "stdpimon.h"
#include "fspimcmnon.h"
#include "fspimstdon.h"
#endif

#ifdef IP_WANTED
#include "fspingon.h"
#endif

#ifdef ROUTEMAP_WANTED
#include "fsrmapon.h"
#endif

#ifdef POE_WANTED
#include "fspoeon.h"
#include "stdpoeon.h"
#endif

#ifdef PTP_WANTED
#include "fsptpon.h"
#include "fsG8261Tson.h"
#endif

#ifdef MPLS_WANTED  
#include "fsmplson.h"
#include "stdmplsron.h"
#include "fsmplsron.h"
#include "stdpwmon.h"
#include "stdmpftnon.h"
#include "stdldatmon.h"
#include "fsmpfrron.h"
#include "stdpwon.h"
#include "stdpweon.h"
#include "fsmpteon.h"
#include "stdteon.h"
#include "fsmptpon.h"
#include "fshlspon.h"
#include "fsmpnotifon.h"
#include "fsmspwon.h"
#include "fsmtpoamon.h"
#include "stdgmteon.h"
#include "stdgmlsron.h"
#include "fsl2vpnon.h"    
#include "fsVplsBgpon.h"    

#include "fsp2mpon.h"
#include "ianapwe3on.h"

#ifdef MPLS_SIG_WANTED
#include "stdgnldpon.h"
#include "stdldpon.h"
#include "fsrsvpteon.h"
#endif

#ifdef MPLS_L3VPN_WANTED
#include "stdl3vpnon.h"
#endif

#ifdef LDP_TEST_WANTED
#include "fsldptston.h"
#endif

#endif

#ifdef PNAC_WANTED
#include "fspnacon.h"
#include "stdpnacon.h"
#endif

#ifdef QOSX_WANTED
#include "fsqosxtdon.h"
#endif

#ifdef RADIUS_WANTED
#include "radauthon.h"
#include "fsradexton.h"
#include "fsradiuson.h"
#include "radaccon.h"
#endif

#ifdef RIP6_WANTED
#include "fsrip6on.h"
#endif

#ifdef RIP_WANTED
#include "fsmiripon.h"
#include "fsmistdripon.h"
#include "fsripon.h"
#include "stdripon.h"
#endif

#ifdef SNMP_3_WANTED
#include "fssnmp3on.h"
#include "stdsncomon.h"
#include "stdsntgton.h"
#include "stdsnproxyon.h"
#include "stdsnnoton.h"
#include "stdsnusmon.h"
#include "stdvacmon.h"
#endif

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#include "fssnpon.h"
#endif

#ifdef LSPP_WANTED
#include "fslsppon.h"
#endif

#ifdef SSH_WANTED
#include "fssshmibon.h"
#endif

#ifdef SSL_WANTED
#include "fssslon.h"
#endif

#ifdef TACACS_WANTED
#include "fstacacson.h"
#include "fstacsxton.h"
#endif

#ifdef TAC_WANTED
#include "fstacon.h"
#endif

#ifdef ARP_WANTED
#include "fsarpon.h"
#include "fsmparpon.h"
#endif

#ifdef PBB_WANTED
#include "fspbbon.h"
#endif

#ifdef DHCP_RLY_WANTED
#include "fsdhcpRelayon.h"
#include "fsmidhcpRelayon.h"
#endif

#ifdef IPSECv6_WANTED
#include "fssecv6on.h"
#endif

#ifdef TLM_WANTED
#include "stdtlmon.h"
#include "fstlmon.h"
#endif

#ifdef OSPFTE_WANTED
#include "fsoteappon.h"
#include "fsoteon.h"
#include "fsotermon.h"
#endif

#ifdef IP6_WANTED
#include "fsipv6on.h"
#include "stdipv6on.h"
#include "fsrtm6on.h"
#endif

#ifdef RRD_WANTED
#include "fsrtmon.h"
#endif

#ifdef SNMP_2_WANTED
#include "fssnmp2on.h"
#include "stdsnmpdon.h"
#include "stdsnmpon.h"
#endif

#ifdef IP_WANTED
#include "fsipon.h"
#include "stdipon.h"
#include "fsmpipon.h"
#endif

#ifdef ISIS_WANTED
#include "fsisison.h"
#include "stdisison.h"
#endif

#ifdef MRI_WANTED
#include "stdmrion.h"
#endif

#ifdef MRP_WANTED
#include "fsmrpon.h"
#include "std1dmrpon.h"
#endif

#ifdef MSDP_WANTED
#include "fsmsdpon.h"
#include "stdmsdpon.h"
#endif

#ifdef NAT_WANTED
#include "fsnaton.h"
#endif

#ifdef IPVX_WANTED
#include "stdipvxon.h"
#include "stdtcpipvxon.h"
#include "stdudpipvxon.h"
#include "fsmpipvxon.h"
#include "fsmsipvxon.h"
#include "fsmsudpipvxon.h"
#include "fsipvxon.h"
#endif

#ifdef HOTSPOT2_WANTED
#include "dot11uon.h"
#endif

#ifdef OSPF_WANTED
#include "fsospfon.h"
#include "fsosmiteston.h"
#include "fsmiospfon.h"
#include "fsmistdospfon.h"
#include "fsosteston.h"
#include "fsstdmiostrpon.h"
#include "stdospfon.h"
#include "stdostrpon.h"
#endif

#ifdef SNTP_WANTED
#include "fssntpon.h"
#endif

#ifdef MSTP_WANTED
#include "fsmston.h"
#include "fsmpmston.h"
#include "std1s1apon.h"
#endif

#ifdef RSTP_WANTED
#include "fsmprston.h"
#include "fspbrston.h"
#include "fsrston.h"
#include "stdrston.h"
#include "fsmpbrston.h"
#include "fsmsrston.h"
#include "std1w1apon.h"
#endif

#ifdef PVRST_WANTED
#include "fsmppvrston.h"
#include "fspvrston.h"
#endif

#ifdef VPN_WANTED
#include "fsvpnpolicyon.h"
#endif

#ifdef OPENFLOW_WANTED
#include "fsofcon.h"
#endif
#ifdef MEF_WANTED
#include "fsmefon.h"
#endif

#ifdef SYNCE_WANTED
#include "fssynceon.h"
#endif

#ifdef PPP_WANTED 
#include "ppipcpfson.h" 
#include "pppoeon.h" 
#include "stdauthon.h" 
#include "stdbcpon.h" 
#include "stdipcpon.h" 
#include "stdlcpon.h" 
#endif 

    
#include "tokenringon.h"
#include "fsmpipv6on.h"
#include "fsmppingon.h"
#include "INTEGRATED-SERVICESon.h"
#include "SNMP-FRAMEWORKon.h"
#include "std8021brgon.h"
#include "fsmpbrgon.h"
#include "stdpbon.h"

#ifdef FSB_WANTED
#include "fsfsbmion.h"
#endif

#ifdef CAPWAP_WANTED
#include "stdcapwapon.h"    
#include "fscapwon.h"    
#endif

#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
#include "fsrrmon.h"    
#include "stdwlanon.h"    
#include "stddot11on.h"    
#include "fswlanon.h"
#include "fsrsnaon.h"
#include "fs11acon.h"
#include "fswssuseron.h"
#include "std80211on.h"
#endif

#ifdef RFMGMT_WANTED
#ifdef WTP_WANTED
#include "teston.h"
#endif
#endif

#ifdef WPS_WANTED
#include "fswpson.h"
#endif

#ifdef L2TPV3_WANTED
#include "fsl2tpon.h"
#endif

#include "notif.h"
#include "custon.h" /* For customer specific inclusion. 
      * No other file inclusion should be
      * made below this file
      */ 

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}}, NULL, NULL}

};

#endif
