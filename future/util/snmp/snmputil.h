/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmputil.h,v 1.7 2017/11/20 13:11:27 siva Exp $
 *
 * Description: Prototypes of SNMP utilitiy functions.
 *******************************************************************/

#ifndef _SNMPUTIL_H_
#define _SNMPUTIL_H_
#include "cust.h"
#define MAX_OCTET_STRING_LEN                  512
#define ENTERPRISES_OID_LEN 12    /*strlen (1.3.6.1.4.1.) */
#define ISS_ENT_OID_1       2076
#define ISS_ENT_OID_2       29601
#define EOID_OFFSET         6     /*Position of enterprise OID (1.3.6.1.4.1.xx) */
#define SNMPUTIL_MAX_PASSWD_LEN 64
#define SNMPUTIL_PASSWD_TO_STRING_LEN 1048576
#define SNMP_MD5     2
#define SNMP_SHA     3
#define SNMP_SHA224     4
#define SNMP_SHA256     5
#define SNMP_SHA384     6
#define SNMP_SHA512     7


tSNMP_VAR_BIND *
SNMP_AGT_FormVarBind      PROTO((tSNMP_OID_TYPE *, INT2, UINT4, INT4,
                                 tSNMP_OCTET_STRING_TYPE *, tSNMP_OID_TYPE *,
                                 tSNMP_COUNTER64_TYPE));

tSNMP_OCTET_STRING_TYPE *
SNMP_AGT_FormOctetString  PROTO((UINT1 *, INT4));

tSNMP_OID_TYPE *
alloc_oid                 PROTO((INT4));

tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

tSnmpAgentParam *
alloc_agentparams         PROTO((VOID));

VOID
free_agentparams          PROTO((tSnmpAgentParam *));

VOID 
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));

VOID 
free_oid                  PROTO((tSNMP_OID_TYPE *));


tSNMP_OID_TYPE *
SNMP_AGT_GetOidFromString PROTO((INT1 *pi1_str));

INT4 
SNMP_AGT_ConvSubOIDToLong PROTO((UINT1 **ppu1_temp_ptr));

VOID 
SNMP_FreeOid              PROTO((tSNMP_OID_TYPE * pOidPtr));

VOID
SNMP_ReverseOctetString   PROTO((tSNMP_OCTET_STRING_TYPE *pInOctetStr,
                                 tSNMP_OCTET_STRING_TYPE *pOutOctetStr));

VOID
SNMP_AGT_FreeVarBindList (tSNMP_VAR_BIND * pVarBindLst);

VOID
SNMP_AGT_FreeOctetString (tSNMP_OCTET_STRING_TYPE *pOctetString);

VOID
SNMPUpdateEOID PROTO ((tSNMP_OID_TYPE *pOid, tSNMP_OID_TYPE *pRetOid,
                       UINT1 u1NextFlag));

VOID
SNMPRevertEOID PROTO ((tSNMP_OID_TYPE *pSrcOid, tSNMP_OID_TYPE *pDestOid));

VOID
SNMPInitEOID PROTO ((VOID));

VOID
SnmpUpdateEoidString PROTO ((UINT1 *pu1Oid));

VOID
SnmpRevertEoidString PROTO ((UINT1 *pu1Oid));

INT4
UtlSnmpMemInit PROTO ((VOID));

VOID
SNMPUpdateGetNextEOID (tSNMP_OID_TYPE * pOID, tMbDbEntry ** pNext,UINT4 * pLen, tSnmpDbEntry * SnmpDbEntry);


PUBLIC INT4 SnmpUtilEncodeOid (tSNMP_OID_TYPE *pInOidStr, UINT1 *pu1OutOidStr, UINT1 *u1ErrorCode);
PUBLIC INT4 SnmpUtilDecodeOid (UINT1 *pu1InOidStr, tSNMP_OID_TYPE* pOutOidStr);

PUBLIC INT4 SnmpUtilKeyGen (UINT1 *pu1Passwd, INT4 i4PasswdLen, UINT4 u4Auth, UINT1 *pu1Key);
INT4 SnmpCallBackRegister PROTO ((UINT4, tFsCbInfo *));

extern INT4        gi4SnmpMemInit;
extern tMemPoolId  gSnmpVarBindPoolId;
extern tMemPoolId  gSnmpOidTypePoolId;
extern tMemPoolId  gSnmpOidListPoolId;
extern tMemPoolId  gSnmpOctetListPoolId;
extern tMemPoolId  gSnmpOctetStrPoolId;
extern tMemPoolId  gSnmpMultiDataPoolId;
extern tMemPoolId  gSnmpMultiIndexPoolId;
extern tMemPoolId  gSnmpMultiDataIndexPoolId;
extern tMemPoolId  gSnmpMultiOidPoolId;
extern tMemPoolId  gSnmpDataPoolId;
extern tMemPoolId  gSnmpAuditInfoPoolId;
extern tMemPoolId  gSnmpTagListPoolId;
extern tMemPoolId  gSnmpAgentParamsPoolId;
extern tSNMP_OID_TYPE     *gEntOid1, *gEntOid2;

extern INT4 SnmpONGetOidFromName PROTO ((char *, char *));
extern INT4 SnmpONGetNameFromOid PROTO ((char *, char *));

typedef union SnmpCallBackEntry {
    INT4 (*pSnmpUpdateEOIDCallBack) (tSNMP_OID_TYPE *,tSNMP_OID_TYPE *,UINT1);
    INT4 (*pSnmpRevertEOIDCallBack) (tSNMP_OID_TYPE *, tSNMP_OID_TYPE *);
    INT4 (*pSnmpUpdateGetNextEOIDCallBack) (tSNMP_OID_TYPE *, tMbDbEntry **, UINT4 *, tSnmpDbEntry *);
}tSnmpCallBackEntry;

#ifdef __SNMPUTIL_C_
tSnmpCallBackEntry gSnmpCallBack[SNMP_CALL_BACK_EVENTS];

#define SNMP_CALLBACK   gSnmpCallBack

#else
extern tSnmpCallBackEntry gSnmpCallBack[SNMP_CALL_BACK_EVENTS];

#define SNMP_CALLBACK   gSnmpCallBack

#endif

#endif
