/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmputil.c,v 1.9 2017/11/20 13:11:27 siva Exp $
 *
 * Description: Utility Routines for Snmp Agent.
 *******************************************************************/
#ifndef __SNMPUTIL_C_
#define __SNMPUTIL_C_
#include "lr.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "cli.h"
#include "msr.h"
#include "lldp.h"
#include "sha2.h"
#include "cryartdf.h"
#include "arHmac_api.h"
#include  "arMD5_api.h"

UINT4               AllocBlocks = 0;

extern UINT1       *EoidGetEnterpriseOid (void);
extern UINT1       *EoidGetSecondEnterpriseOid (void);
UINT1               au1Eoid[] = "1.3.6.1.4.1.";

#ifndef SNMP_3_WANTED
INT4                gi4SnmpMemInit = SNMP_FAILURE;
tSNMP_OID_TYPE     *gEntOid1, *gEntOid2;
tMemPoolId          gSnmpVarBindPoolId;
tMemPoolId          gSnmpOidTypePoolId;
tMemPoolId          gSnmpOidListPoolId;
tMemPoolId          gSnmpOctetListPoolId;
tMemPoolId          gSnmpOctetStrPoolId;
tMemPoolId          gSnmpMultiDataPoolId;
tMemPoolId          gSnmpMultiIndexPoolId;
tMemPoolId          gSnmpMultiDataIndexPoolId;
tMemPoolId          gSnmpMultiOidPoolId;
tMemPoolId          gSnmpAgentParamsPoolId;

/************************************************************************
 *  Function Name   : UtlSnmpMemInit
 *  Description     : Function to initialise SNMP related utility mem pools
 *                    This will be called if SNMP is not enabled by default.
 *  Input           : None
 *  Output          : None   
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
UtlSnmpMemInit (VOID)
{
    if (MemCreateMemPool (sizeof (tSNMP_OID_TYPE), MAX_SNMP_OID_TYPE_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpOidTypePoolId))
        == (UINT4) MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool (sizeof (tSnmpOidListBlock), MAX_SNMP_OID_LIST_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpOidListPoolId))
        == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool
        (sizeof (tSNMP_OCTET_STRING_TYPE), MAX_SNMP_OCTET_STR_BLOCKS,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gSnmpOctetStrPoolId)) == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        return OSIX_FAILURE;
    }

    if (MemCreateMemPool
        (sizeof (tSnmpOctetListBlock), MAX_SNMP_OCTET_LIST_BLOCKS,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gSnmpOctetListPoolId)) == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool
        (sizeof (tSNMP_MULTI_DATA_TYPE), MAX_SNMP_MULTI_DATA_BLOCKS,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gSnmpMultiDataPoolId)) == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_LIST_BLOCKS);
        return OSIX_FAILURE;
    }

    if (MemCreateMemPool (sizeof (tSnmpIndex), MAX_SNMP_INDEX_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpMultiIndexPoolId))
        == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_BLOCKS);
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool
        (sizeof (tSnmpMultiDataIndexBlock), MAX_SNMP_MULTI_DATA_INDEX_BLOCKS,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gSnmpMultiDataIndexPoolId)) == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_INDEX_BLOCKS);
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool
        (sizeof (tSnmpMultiOidBlock), MAX_SNMP_MULTI_OID_BLOCKS,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gSnmpMultiOidPoolId)) == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_INDEX_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_INDEX_BLOCKS);
        return OSIX_FAILURE;
    }

    if (MemCreateMemPool (sizeof (tSNMP_VAR_BIND), MAX_SNMP_VARBIND_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpVarBindPoolId))
        == (UINT4) MEM_FAILURE)
    {
        MemDeleteMemPool (MAX_SNMP_OID_TYPE_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OID_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_STR_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_OCTET_LIST_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_INDEX_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_DATA_INDEX_BLOCKS);
        MemDeleteMemPool (MAX_SNMP_MULTI_OID_BLOCKS);
        return OSIX_FAILURE;
    }

    gi4SnmpMemInit = SNMP_SUCCESS;
    return OSIX_SUCCESS;

}
#endif

/************************************************************************
 *  Function Name   : free_octetstring 
 *  Description     : Function to free octet string
 *  Input           : pOctetStr - Octet string pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
free_octetstring (tSNMP_OCTET_STRING_TYPE * pOctetStr)
{

    if (pOctetStr != NULL)
    {
        if (pOctetStr->pu1_OctetList != NULL)
        {
            MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *)
                                (pOctetStr->pu1_OctetList));
        }
        MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *) pOctetStr);
        AllocBlocks--;

    }
}

/************************************************************************
 *  Function Name   : free_oid 
 *  Description     : function to free oid pointer
 *  Input           : pOid - oid pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
free_oid (tSNMP_OID_TYPE * pOid)
{
    if (pOid != NULL)
    {
        if (pOid->pu4_OidList != NULL)
        {
            MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                                (pOid->pu4_OidList));
        }
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
    }
}

/************************************************************************
 *  Function Name   : alloc_oid 
 *  Description     : function to allocate oid
 *  Input           : i4Size - size of oid to be allocated.
 *                    actual lenght in byte = i4Size * sizeof(UINT4)
 *  Output          : None
 *  Returns         : Oid pointer or null
 ************************************************************************/
tSNMP_OID_TYPE     *
alloc_oid (INT4 i4Size)
{
    tSNMP_OID_TYPE     *pOid = NULL;

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }

    if (i4Size > SNMP_MAX_OID_LENGTH)
    {
        return NULL;
    }
    if ((pOid = MemAllocMemBlk (gSnmpOidTypePoolId)) == NULL)
    {
        return NULL;
    }
    if ((pOid->pu4_OidList = MemAllocMemBlk (gSnmpOidListPoolId)) == NULL)
    {
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
        return NULL;
    }
    pOid->u4_Length = (UINT4) i4Size;
    return pOid;
}

/************************************************************************
 *  Function Name   : allocmem_octetstring 
 *  Description     : function to allocate octet string
 *  Input           : i4Size - size of octet string
 *  Output          : None
 *  Returns         : octet string pointer or null
 ************************************************************************/
tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring (INT4 i4Size)
{
    tSNMP_OCTET_STRING_TYPE *pOctet = NULL;
    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }
    if ((pOctet = MemAllocMemBlk (gSnmpOctetStrPoolId)) == NULL)
    {
        return NULL;
    }
    if ((pOctet->pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId)) == NULL)
    {
        MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *) (pOctet));
        return NULL;
    }
    MEMSET (pOctet->pu1_OctetList, 0, sizeof (tSnmpOctetListBlock));
    pOctet->i4_Length = i4Size;
    AllocBlocks++;
    return pOctet;
}

/************************************************************************
 *  Function Name   : SNMP_AGT_FormOctetString 
 *  Description     : Function to allocate octet string and copy the
 *                    input string to octet string  
 *  Input           : pu1String - Pointer to input string
 *                    i4Length - length of the input string  
 *  Output          : None
 *  Returns         : octet string pointer or null
 ************************************************************************/
tSNMP_OCTET_STRING_TYPE *
SNMP_AGT_FormOctetString (UINT1 *pu1String, INT4 i4Length)
{
    tSNMP_OCTET_STRING_TYPE *pOctetstring = NULL;

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }
    if (i4Length > SNMP_MAX_OCTETSTRING_SIZE)
    {
        return (NULL);
    }
    if ((pOctetstring = MemAllocMemBlk (gSnmpOctetStrPoolId)) == NULL)
    {
        return (NULL);
    }
    pOctetstring->pu1_OctetList = NULL;
    pOctetstring->i4_Length = i4Length;
    if (i4Length != 0)
    {
        if ((pOctetstring->pu1_OctetList =
             MemAllocMemBlk (gSnmpOctetListPoolId)) == NULL)
        {
            MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *) pOctetstring);
            return (NULL);
        }
        MEMCPY (pOctetstring->pu1_OctetList, pu1String, i4Length);
    }
    AllocBlocks++;
    return (pOctetstring);
}

/************************************************************************
 *  Function Name   : SNMP_AGT_FreeOctetString 
 *  Description     : Function to free octet string 
 *  Input           : pu1String - Pointer to input string
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/
VOID
SNMP_AGT_FreeOctetString (tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    free_octetstring (pOctetString);
}

/************************************************************************
 *  Function Name   : SNMP_AGT_FormVarBind 
 *  Description     : Function to form variable bind for the give oid and
 *                    value  
 *  Input           : pOid - Pointer to oid to be assigned in varbind
 *                    i2Type - Data type in the varbind value
 *                    u4Value - value of unsigned32 if the type is unsigned
 *                    i4Value - value of signed32 if the type is signed32
 *                    pOctet - pointer to octet string if the type is
 *                    octet string
 *                    pOidType - Pointer to oid if the type is oid
 *                    u8Value - Conter64 value if the type is conter64 
 *  Output          : None 
 *  Returns         : Variable bind pointer or null
 ************************************************************************/
tSNMP_VAR_BIND     *
SNMP_AGT_FormVarBind (tSNMP_OID_TYPE * pOid, INT2 i2Type, UINT4 u4Value,
                      INT4 i4Value, tSNMP_OCTET_STRING_TYPE * pOctet,
                      tSNMP_OID_TYPE * pOidType, tSNMP_COUNTER64_TYPE u8Value)
{
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
    tSNMP_OID_TYPE     *pEntRetOid = NULL;

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }
    if ((pVarBindPtr = MemAllocMemBlk (gSnmpVarBindPoolId)) == NULL)
    {
        return NULL;
    }
    MEMSET (pVarBindPtr, 0, sizeof (tSNMP_VAR_BIND));
    pVarBindPtr->pObjName = pOid;
    pVarBindPtr->ObjValue.i2_DataType = i2Type;
    pVarBindPtr->ObjValue.u4_ULongValue = 0;
    pVarBindPtr->ObjValue.i4_SLongValue = 0;
    pVarBindPtr->ObjValue.pOidValue = NULL;
    pVarBindPtr->ObjValue.pOctetStrValue = NULL;
    pVarBindPtr->ObjValue.u8_Counter64Value.lsn = 0;
    pVarBindPtr->ObjValue.u8_Counter64Value.msn = 0;
    pVarBindPtr->pNextVarBind = NULL;
    switch (i2Type)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVarBindPtr->ObjValue.u4_ULongValue = u4Value;
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            pVarBindPtr->ObjValue.i4_SLongValue = i4Value;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            if (pOidType == NULL)
            {
                MemReleaseMemBlock (gSnmpVarBindPoolId,
                                    (UINT1 *) (pVarBindPtr));
                return NULL;
            }
            pEntRetOid = alloc_oid ((INT4) pOidType->u4_Length);
            if (pEntRetOid == NULL)
            {
                UtlTrcLog (1, 1, "Utl", "Oid alloc Failed\n");
                MemReleaseMemBlock (gSnmpVarBindPoolId,
                                    (UINT1 *) (pVarBindPtr));
                return NULL;
            }
            SNMPRevertEOID (pOidType, pEntRetOid);
            SNMPOIDCopy (pOidType, pEntRetOid);
            pVarBindPtr->ObjValue.pOidValue = pOidType;
            free_oid (pEntRetOid);
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            pVarBindPtr->ObjValue.pOctetStrValue = pOctet;
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pVarBindPtr->ObjValue.pOctetStrValue = pOctet;
            MEMCPY (&(pVarBindPtr->ObjValue.u4_ULongValue),
                    pOctet->pu1_OctetList, pOctet->i4_Length);

            pVarBindPtr->ObjValue.u4_ULongValue =
                OSIX_HTONL (pVarBindPtr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pVarBindPtr->ObjValue.u8_Counter64Value.lsn = u8Value.lsn;
            pVarBindPtr->ObjValue.u8_Counter64Value.msn = u8Value.msn;
            break;
        case SNMP_DATA_TYPE_NULL:
            break;
        default:
            MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *) (pVarBindPtr));
            return NULL;
    }
    return pVarBindPtr;
}

/*****************************************************************************/
/* Function Name      : SNMP_AGT_FreeVarBindList                             */
/*                                                                           */
/* Description        : This function will free the memory allocated for     */
/*                      SNMP VAR Bind list                                   */
/*                                                                           */
/* Input(s)           : pVarBindLst - Pointer to the VAR Bind list           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SNMP_AGT_FreeVarBindList (tSNMP_VAR_BIND * pVarBindLst)
{
    tSNMP_VAR_BIND     *pVarBndNext = NULL;
    tSNMP_VAR_BIND     *pVarBind = NULL;

    for (pVarBind = pVarBindLst; pVarBind != NULL;)
    {
        pVarBndNext = pVarBind->pNextVarBind;
        SNMP_FreeOid (pVarBind->pObjName);
        SNMP_FreeOid (pVarBind->ObjValue.pOidValue);
        free_octetstring (pVarBind->ObjValue.pOctetStrValue);
        MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *) pVarBind);
        pVarBind = NULL;
        pVarBind = pVarBndNext;
    }
    return;
}

/*************************************************************************/
/* Function Name : SNMP_AGT_GetOidFromString                             */
/* Description   : To get Oid from Octet string form                     */
/* Input(s)      : Oid in Octet string form                              */
/* Output(s)     : Pointer to Oid                                        */
/* Returns       : pOid, in case of success                              */
/*                 NULL, in  failure cases                               */
/*************************************************************************/
tSNMP_OID_TYPE     *
SNMP_AGT_GetOidFromString (INT1 *pi1_str)
{
    INT4                i4DotCount;
    INT4                i4Int;
    UINT1              *pu1TempPtr = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    i4DotCount = 0;

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }

    for (i4Int = 0; pi1_str[i4Int] != '\0'; i4Int++)
    {
        if (pi1_str[i4Int] == '.')
            i4DotCount++;
    }

    if (i4DotCount > SNMP_MAX_OID_LENGTH)
    {
        return NULL;
    }

    if ((pOid = MemAllocMemBlk (gSnmpOidTypePoolId)) == NULL)
    {
        return (NULL);
    }
    pOid->pu4_OidList = NULL;
    pOid->u4_Length = 0;
    pu1TempPtr = (UINT1 *) pi1_str;

    if ((pOid->pu4_OidList = MemAllocMemBlk (gSnmpOidListPoolId)) != NULL)
    {
        for (i4Int = 0; i4Int < i4DotCount + 1; i4Int++)
        {
            if (isdigit ((INT4) *pu1TempPtr))
                pOid->pu4_OidList[i4Int] =
                    (UINT4) SNMP_AGT_ConvSubOIDToLong (&pu1TempPtr);
            else
            {
                SNMP_FreeOid (pOid);
                return (NULL);
            }
            if (*pu1TempPtr == '.')
                pu1TempPtr++;
            else if (*pu1TempPtr != '\0')
            {
                SNMP_FreeOid (pOid);
                return (NULL);
            }
        }
    }
    else
    {
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
        return (NULL);
    }

    pOid->u4_Length = (UINT4) (++i4DotCount);
    return (pOid);
}

/*************************************************************************/
/* Function Name : SNMP_AGT_ConvSubOIDToLong                             */
/* Description   : To convert sub oid into long int                      */
/* Input(s)      : Double pointer to the string                          */
/* Output(s)     : Long integer value                                    */
/* Returns       : u4Value, in case of success                           */
/*                 -1 , in  failure cases                                */
/*************************************************************************/
INT4
SNMP_AGT_ConvSubOIDToLong (UINT1 **ppu1TempPtr)
{
    INT2                i2Init;
    UINT4               u4Value;
    UINT1               u1TempValue;

    u4Value = 0;
    for (i2Init = 0; ((i2Init < 11) && (**ppu1TempPtr != '.') &&
                      (**ppu1TempPtr != '\0')); i2Init++)
    {
        if (!isdigit (**ppu1TempPtr))
        {
            return (INT4) (-1);
        }
        if (STRNCPY (&u1TempValue, *ppu1TempPtr, 1) == NULL)
        {
            return (INT4) (-1);
        }

        u4Value = (u4Value * (UINT4) 10) + (0x0f & (UINT4) u1TempValue);
        (*ppu1TempPtr)++;
    }
    return ((INT4) u4Value);
}

/*************************************************************************/
/* Function Name : SNMP_FreeOid                                          */
/* Description   : To convert sub oid into long int                      */
/* Input(s)      : Oid Pointer to be freed                               */
/* Output(s)     : None                                                  */
/* Returns       : None                                                  */
/*************************************************************************/

VOID
SNMP_FreeOid (tSNMP_OID_TYPE * pOidPtr)
{
    if (pOidPtr != NULL)
    {
        if (pOidPtr->pu4_OidList != NULL)
        {
            MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                                (pOidPtr->pu4_OidList));
            pOidPtr->pu4_OidList = NULL;
        }
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOidPtr);
    }
}

/*************************************************************************/
/* Function Name : SNMP_ReverseOctetString                               */
/* Description   : Reverse the Bit positions in pInOctetStr and store it */
/*                 in pOutOctetStr                                       */
/* Input(s)      : pInOctetStr - Input octetstring                       */
/* Output(s)     : pOutOctetStr - output octetstring                     */
/* Returns       : None                                                  */
/*************************************************************************/

VOID
SNMP_ReverseOctetString (tSNMP_OCTET_STRING_TYPE * pInOctetStr,
                         tSNMP_OCTET_STRING_TYPE * pOutOctetStr)
{
    UINT4               u4Count = 0;
    UINT4               u4MaxBits = 0;
    UINT4               u4RevBitNum = 0;
    INT4                i4MaxBytes = 0;
    BOOL1               bResult = OSIX_FALSE;

    /* Example
     *             BIT Positions 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
     * pInOctetStr:              1 1 1 . . . . .               .  .  1
     * pOutOctetStr:             1 . . . . .     . .  .  .  .  1  1  1
     */

    i4MaxBytes = pInOctetStr->i4_Length;
    pOutOctetStr->i4_Length = i4MaxBytes;

    u4MaxBits = (UINT4) (i4MaxBytes * BITS_PER_BYTE);

    for (u4Count = 1; u4Count <= u4MaxBits; u4Count++)
    {
        OSIX_BITLIST_IS_BIT_SET (pInOctetStr->pu1_OctetList, u4Count,
                                 i4MaxBytes, bResult);
        if (bResult == OSIX_TRUE)
        {
            u4RevBitNum = (u4MaxBits + 1) - u4Count;
            OSIX_BITLIST_SET_BIT (pOutOctetStr->pu1_OctetList, u4RevBitNum,
                                  i4MaxBytes);
        }
    }
    return;
}

/*************************************************************************/
/* Function Name : SnmpAllocMultiIndexAndData                            */
/* Description   : Allocates memory for multi index, multidata used for  */
/*                  configuration change event trigger after Set         */
/*                  operation                                            */
/* Input(s)      : pInOctetStr - Input octetstring                       */
/* Output(s)     : pOutOctetStr - output octetstring                     */
/* Returns       : None                                                  */
/*************************************************************************/
INT1
SnmpAllocMultiIndexAndData (tSnmpIndex * (*ppMultiIndex), UINT4 u4IndicesNo,
                            tRetVal ** ppMultiData, UINT1 **ppOid,
                            UINT4 u4OidLen)
{
    UINT4               u4Index = 0;
    INT4                i4RetVal;
    /*ppMultiData */
    *ppMultiData = NULL;
    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return FAILURE;
    }
    *ppMultiData = MemAllocMemBlk (gSnmpMultiDataPoolId);
    if (*ppMultiData == NULL)
    {
        return FAILURE;
    }

    MEMSET (*ppMultiData, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    AllocBlocks++;
    (*ppMultiData)->pOctetStrValue = MemAllocMemBlk (gSnmpOctetStrPoolId);
    if ((*ppMultiData)->pOctetStrValue == NULL)
    {
        (*ppMultiData)->pOidValue = NULL;
        free_MultiData (*ppMultiData);
        return FAILURE;
    }

    MEMSET ((*ppMultiData)->pOctetStrValue, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));

    (*ppMultiData)->pOctetStrValue->pu1_OctetList =
        MemAllocMemBlk (gSnmpOctetListPoolId);
    if ((*ppMultiData)->pOctetStrValue->pu1_OctetList == NULL)
    {
        (*ppMultiData)->pOidValue = NULL;
        free_MultiData (*ppMultiData);
        return FAILURE;
    }
    MEMSET ((*ppMultiData)->pOctetStrValue->pu1_OctetList, 0,
            MAX_OCTET_STRING_LEN);

    (*ppMultiData)->pOidValue = MemAllocMemBlk (gSnmpOidTypePoolId);
    if ((*ppMultiData)->pOidValue == NULL)
    {
        free_MultiData (*ppMultiData);
        return FAILURE;
    }
    MEMSET ((*ppMultiData)->pOidValue, 0, sizeof (tSNMP_OID_TYPE));

    (*ppMultiData)->pOidValue->pu4_OidList =
        MemAllocMemBlk (gSnmpOidListPoolId);
    if ((*ppMultiData)->pOidValue->pu4_OidList == NULL)
    {
        free_MultiData (*ppMultiData);
        return FAILURE;
    }
    MEMSET ((*ppMultiData)->pOidValue->pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_MAX_OID_LENGTH));

    /*ppMultiIndex */
    *ppMultiIndex = NULL;

    (*ppMultiIndex) = MemAllocMemBlk (gSnmpMultiIndexPoolId);
    if (*ppMultiIndex == NULL)
    {
        free_MultiData (*ppMultiData);
        return FAILURE;
    }
    else
    {
        MEMSET (*ppMultiIndex, 0, sizeof (tSnmpIndex));
        ((*ppMultiIndex))->u4No = u4IndicesNo;
    }

    if (u4IndicesNo != 0)
    {
        ((*ppMultiIndex))->pIndex = MemAllocMemBlk (gSnmpMultiDataIndexPoolId);
        if (((*ppMultiIndex))->pIndex == NULL)
        {
            free_MultiData (*ppMultiData);
            MemReleaseMemBlock (gSnmpMultiIndexPoolId,
                                (UINT1 *) (*ppMultiIndex));
            return FAILURE;
        }
        MEMSET ((*ppMultiIndex)->pIndex, 0,
                (sizeof (tSNMP_MULTI_DATA_TYPE) * u4IndicesNo));
    }

    i4RetVal = SUCCESS;
    for (u4Index = 0; u4Index < u4IndicesNo; u4Index++)
    {
        ((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue = NULL;
        AllocBlocks++;
        ((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue =
            MemAllocMemBlk (gSnmpOctetStrPoolId);
        if (((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue == NULL)
        {
            ((*ppMultiIndex)->pIndex + u4Index)->pOidValue = NULL;
            i4RetVal = FAILURE;
            break;
        }
        MEMSET (((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue, 0,
                sizeof (tSNMP_OCTET_STRING_TYPE));

        ((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue->pu1_OctetList =
            NULL;
        ((*ppMultiIndex)->pIndex + u4Index)->pOctetStrValue->pu1_OctetList =
            MemAllocMemBlk (gSnmpOctetListPoolId);
        if (((*ppMultiIndex)->pIndex +
             u4Index)->pOctetStrValue->pu1_OctetList == NULL)
        {
            ((*ppMultiIndex)->pIndex + u4Index)->pOidValue = NULL;
            i4RetVal = FAILURE;
            break;
        }
        MEMSET (((*ppMultiIndex)->pIndex +
                 u4Index)->pOctetStrValue->pu1_OctetList, 0,
                MAX_OCTET_STRING_LEN);

        ((*ppMultiIndex)->pIndex + u4Index)->pOidValue = NULL;
        ((*ppMultiIndex)->pIndex + u4Index)->pOidValue =
            MemAllocMemBlk (gSnmpOidTypePoolId);
        if (((*ppMultiIndex)->pIndex + u4Index)->pOidValue == NULL)
        {
            i4RetVal = FAILURE;
            break;
        }
        MEMSET (((*ppMultiIndex)->pIndex + u4Index)->pOidValue, 0,
                sizeof (tSNMP_OID_TYPE));

        ((*ppMultiIndex)->pIndex + u4Index)->pOidValue->pu4_OidList = NULL;
        ((*ppMultiIndex)->pIndex + u4Index)->pOidValue->pu4_OidList =
            MemAllocMemBlk (gSnmpOidListPoolId);
        if (((*ppMultiIndex)->pIndex + u4Index)->pOidValue->pu4_OidList == NULL)
        {
            i4RetVal = FAILURE;
            break;
        }
        MEMSET (((*ppMultiIndex)->pIndex + u4Index)->pOidValue->pu4_OidList,
                0, (sizeof (UINT4) * SNMP_MAX_OID_LENGTH));
    }
    if (i4RetVal == FAILURE)
    {
        free_MultiData (*ppMultiData);
        free_MultiIndex (*ppMultiIndex, u4IndicesNo);
        return FAILURE;
    }

    /* ppOid */
    (*ppOid) = MemAllocMemBlk (gSnmpMultiOidPoolId);
    if (*ppOid == NULL)
    {
        free_MultiData (*ppMultiData);
        free_MultiIndex (*ppMultiIndex, u4IndicesNo);
        return FAILURE;
    }

    MEMSET (*ppOid, 0, u4OidLen * sizeof (UINT1) + 1);
    return SUCCESS;
}

/************************************************************************
 *  Function Name   : free_MultiData
 *  Description     : Function deallocates the memory allocated for 
 *                     tSNMP_MULTI_DATA_TYPE
 *  Input           : pMultiData
 *  Output          : 
 *  Returns         : None
 ************************************************************************/
VOID
free_MultiData (tSNMP_MULTI_DATA_TYPE * pMultiData)
{
    if (pMultiData != NULL)
    {
        if (pMultiData->pOidValue != NULL)
        {
            if (pMultiData->pOidValue->pu4_OidList != NULL)
            {
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1 *) (pMultiData->pOidValue->
                                               pu4_OidList));
            }
            MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *)
                                (pMultiData->pOidValue));
        }
        if (pMultiData->pOctetStrValue != NULL)
        {
            if (pMultiData->pOctetStrValue->pu1_OctetList != NULL)
            {
                MemReleaseMemBlock (gSnmpOctetListPoolId,
                                    (UINT1 *) pMultiData->pOctetStrValue->
                                    pu1_OctetList);
            }
            AllocBlocks--;
            MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *)
                                (pMultiData->pOctetStrValue));
        }
        MemReleaseMemBlock (gSnmpMultiDataPoolId, (UINT1 *) pMultiData);
    }
}

/************************************************************************
 *  Function Name   : free_MultiIndex
 *  Description     : Function deallocates the memory allocated for 
 *                     tSnmpIndex
 *  Input           : pMultiData
 *  Output          : 
 *  Returns         : None
 ************************************************************************/
VOID
free_MultiIndex (tSnmpIndex * pMultiIndex, UINT4 u4Indices)
{
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < u4Indices; u4Index++)
    {
        if ((pMultiIndex->pIndex + u4Index)->pOctetStrValue != NULL)
        {
            if ((pMultiIndex->pIndex +
                 u4Index)->pOctetStrValue->pu1_OctetList != NULL)
            {
                MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *)
                                    (pMultiIndex->pIndex +
                                     u4Index)->pOctetStrValue->pu1_OctetList);
            }
            AllocBlocks--;
            MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *)
                                (pMultiIndex->pIndex +
                                 u4Index)->pOctetStrValue);
        }
        if ((pMultiIndex->pIndex + u4Index)->pOidValue != NULL)
        {
            if ((pMultiIndex->pIndex + u4Index)->pOidValue->pu4_OidList != NULL)
            {
                MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                                    (pMultiIndex->pIndex +
                                     u4Index)->pOidValue->pu4_OidList);

            }
            MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *)
                                ((pMultiIndex->pIndex + u4Index)->pOidValue));
        }
    }

    if (u4Indices != 0)
    {
        MemReleaseMemBlock (gSnmpMultiDataIndexPoolId,
                            (UINT1 *) pMultiIndex->pIndex);
    }
    MemReleaseMemBlock (gSnmpMultiIndexPoolId, (UINT1 *) (pMultiIndex));

    return;
}

/************************************************************************/
/*  Function Name   : SNMPFillMultiIndexAndData                         */
/*  Description     : Fills the pMultidata and pMultiIndex structures   */
/*                     from variable number of arguments passed.        */
/*  Input           :                                                   */
/*  Output          :                                                   */
/*  Returns         : None                                              */
/************************************************************************/

VOID
SNMPFillMultiIndexandData (UINT4 u4Indices, CHR1 * pVarIndexFmt,
                           tSnmpIndex * pMultiIndex,
                           tSNMP_MULTI_DATA_TYPE * pMultiData,
                           va_list DataValue)
{
    CHR1               *pcPtr;
    INT4                i4Indx = 0, i4Val = 0;
    UINT4              *args;
    UINT4               u4Val = 0;
    tSNMP_OID_TYPE     *pTempOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOString = NULL;

    pcPtr = pVarIndexFmt;

    pMultiIndex->u4No = u4Indices;

    for (pcPtr = pVarIndexFmt; *pcPtr && u4Indices; pcPtr++)
    {
        if (*pcPtr != '%')
        {
            continue;
        }
        /*Counter64 and Countr32 can never be of read-write or read-create type
           so no need to add a case for it */
        switch (*++pcPtr)
        {
            case 't':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_TIME_TICKS;
                pMultiIndex->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;
            case 'p':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiIndex->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;
            case 'i':
                i4Val = va_arg (DataValue, INT4);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_INTEGER32;
                pMultiIndex->pIndex[i4Indx].i4_SLongValue = i4Val;
                break;
            case 's':
                args = va_arg (DataValue, UINT4 *);
                pOString = (tSNMP_OCTET_STRING_TYPE *) (VOID *) args;
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex->pIndex[i4Indx].pOctetStrValue->i4_Length =
                    pOString->i4_Length;
                if (pOString->i4_Length < SNMP_MAX_OCTETLIST_SIZE)
                {
                    MEMCPY (pMultiIndex->pIndex[i4Indx].pOctetStrValue->
                            pu1_OctetList, pOString->pu1_OctetList,
                            pOString->i4_Length);
                }
                else
                {
                    UtlTrcLog (1, 1, "Utl",
                               "Octet string size is exceeding: Exceeding Length is %d\n",
                               (pOString->i4_Length - SNMP_MAX_OCTETLIST_SIZE));
                    MEMCPY (pMultiIndex->pIndex[i4Indx].pOctetStrValue->
                            pu1_OctetList, pOString->pu1_OctetList,
                            SNMP_MAX_OCTETLIST_SIZE);
                }
                break;
            case 'o':
                args = va_arg (DataValue, UINT4 *);
                pTempOid = (tSNMP_OID_TYPE *) (VOID *) args;
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OBJECT_ID;
                pMultiIndex->pIndex[i4Indx].pOidValue->u4_Length =
                    pTempOid->u4_Length;
                MEMCPY (pMultiIndex->pIndex[i4Indx].pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                break;
            case 'm':
                args = va_arg (DataValue, UINT4 *);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex->pIndex[i4Indx].pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiIndex->pIndex[i4Indx].pOctetStrValue->
                        pu1_OctetList, (UINT1 *) args, 6);
                break;
            case 'u':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_UNSIGNED32;
                pMultiIndex->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;
            case 'g':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_GAUGE32;
                pMultiIndex->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;

            default:
                return;
                break;
        }
        i4Indx++;
        u4Indices--;
    }

    while (*pcPtr != '%')
    {
        ++pcPtr;
    }

    if (*pcPtr == '%')
    {
        /*Counter64 and Countr32 can never be of read-write or read-create type
           so no need to add a case for it */
        switch (*++pcPtr)
        {
            case 't':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
                pMultiData->u4_ULongValue = u4Val;
                break;
            case 'p':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiData->u4_ULongValue = u4Val;
                break;
            case 'i':
                i4Val = va_arg (DataValue, INT4);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
                pMultiData->i4_SLongValue = i4Val;
                break;
            case 's':
                args = va_arg (DataValue, UINT4 *);
                pOString = (tSNMP_OCTET_STRING_TYPE *) (VOID *) args;
                pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData->pOctetStrValue->i4_Length = pOString->i4_Length;
                if (pOString->i4_Length < SNMP_MAX_OCTETLIST_SIZE)
                {
                    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                            pOString->pu1_OctetList, pOString->i4_Length);
                }
                else
                {
                    UtlTrcLog (1, 1, "Utl",
                               "Octet string size is exceeding: Exceeding Length is %d\n",
                               (pOString->i4_Length - SNMP_MAX_OCTETLIST_SIZE));
                    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                            pOString->pu1_OctetList, SNMP_MAX_OCTETLIST_SIZE);
                }
                break;
            case 'o':
                args = va_arg (DataValue, UINT4 *);
                pTempOid = (tSNMP_OID_TYPE *) (VOID *) args;
                pMultiData->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
                pMultiData->pOidValue->u4_Length = pTempOid->u4_Length;
                MEMCPY (pMultiData->pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                break;
            case 'm':
                args = va_arg (DataValue, UINT4 *);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData->pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                        (UINT1 *) args, 6);
                break;
            case 'u':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
                pMultiData->u4_ULongValue = u4Val;
                break;
            case 'g':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData->i2_DataType = SNMP_DATA_TYPE_GAUGE32;
                pMultiData->u4_ULongValue = u4Val;
                break;

            default:
                return;
                break;
        }
    }
}

/************************************************************************/
/*  Function Name   : SNMPFillTwoInstOfMultiIndexandData                */
/*  Description     : Fills the two instances of pMultidata and         */
/*                    pMultiIndex structures from variable              */
/*                    number of arguments passed.                       */
/*                    1. First instance of MultiIndexandData used by MSR*/
/*                    2. Second instance of MultiIndexandData is used   */
/*                    for high availability synchronization message     */
/*                    construction purpose.                             */
/*  Input           :                                                   */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
SNMPFillTwoInstOfMultiIndexandData (UINT4 u4Indices, CHR1 * pVarIndexFmt,
                                    tSnmpIndex * pMultiIndex1,
                                    tSNMP_MULTI_DATA_TYPE * pMultiData1,
                                    tSnmpIndex * pMultiIndex2,
                                    tSNMP_MULTI_DATA_TYPE * pMultiData2,
                                    va_list DataValue)
{
    CHR1               *pcPtr;
    INT4                i4Indx = 0, i4Val = 0;
    UINT4              *args;
    UINT4               u4Val = 0;
    tSNMP_OID_TYPE     *pTempOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOString = NULL;

    pcPtr = pVarIndexFmt;

    if ((pMultiIndex1 == NULL) || (pMultiIndex2 == NULL))
    {
        return;
    }

    pMultiIndex1->u4No = u4Indices;
    pMultiIndex2->u4No = u4Indices;

    for (pcPtr = pVarIndexFmt; *pcPtr && u4Indices; pcPtr++)
    {
        if (*pcPtr != '%')
        {
            continue;
        }
        /*Counter64 and Countr32 can never be of read-write or read-create type
           so no need to add a case for it */
        switch (*++pcPtr)
        {
            case 't':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_TIME_TICKS;
                pMultiIndex1->pIndex[i4Indx].u4_ULongValue = u4Val;
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_TIME_TICKS;
                pMultiIndex2->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;
            case 'p':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiIndex1->pIndex[i4Indx].u4_ULongValue = u4Val;
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiIndex2->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;

            case 'i':
                i4Val = va_arg (DataValue, INT4);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_INTEGER32;
                pMultiIndex1->pIndex[i4Indx].i4_SLongValue = i4Val;
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_INTEGER32;
                pMultiIndex2->pIndex[i4Indx].i4_SLongValue = i4Val;
                break;

            case 's':
                args = va_arg (DataValue, UINT4 *);
                pOString = (tSNMP_OCTET_STRING_TYPE *) (VOID *) args;
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex1->pIndex[i4Indx].pOctetStrValue->i4_Length =
                    pOString->i4_Length;
                MEMCPY (pMultiIndex1->pIndex[i4Indx].pOctetStrValue->
                        pu1_OctetList, pOString->pu1_OctetList,
                        pOString->i4_Length);
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex2->pIndex[i4Indx].pOctetStrValue->i4_Length =
                    pOString->i4_Length;
                MEMCPY (pMultiIndex2->pIndex[i4Indx].pOctetStrValue->
                        pu1_OctetList, pOString->pu1_OctetList,
                        pOString->i4_Length);
                break;

            case 'o':
                args = va_arg (DataValue, UINT4 *);
                pTempOid = (tSNMP_OID_TYPE *) (VOID *) args;
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OBJECT_ID;
                pMultiIndex1->pIndex[i4Indx].pOidValue->u4_Length =
                    pTempOid->u4_Length;
                MEMCPY (pMultiIndex1->pIndex[i4Indx].pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OBJECT_ID;
                pMultiIndex2->pIndex[i4Indx].pOidValue->u4_Length =
                    pTempOid->u4_Length;
                MEMCPY (pMultiIndex2->pIndex[i4Indx].pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                break;

            case 'm':
                args = va_arg (DataValue, UINT4 *);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex1->pIndex[i4Indx].pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiIndex1->pIndex[i4Indx].pOctetStrValue->
                        pu1_OctetList, (UINT1 *) args, 6);
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex2->pIndex[i4Indx].pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiIndex2->pIndex[i4Indx].pOctetStrValue->
                        pu1_OctetList, (UINT1 *) args, 6);
                break;

            case 'u':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_UNSIGNED32;
                pMultiIndex1->pIndex[i4Indx].u4_ULongValue = u4Val;
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_UNSIGNED32;
                pMultiIndex2->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;

            case 'g':
                u4Val = va_arg (DataValue, UINT4);
                pMultiIndex1->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_GAUGE32;
                pMultiIndex1->pIndex[i4Indx].u4_ULongValue = u4Val;
                pMultiIndex2->pIndex[i4Indx].i2_DataType =
                    SNMP_DATA_TYPE_GAUGE32;
                pMultiIndex2->pIndex[i4Indx].u4_ULongValue = u4Val;
                break;

            default:
                return;
                break;
        }
        i4Indx++;
        u4Indices--;
    }

    while (*pcPtr != '%')
    {
        ++pcPtr;
    }

    if (*pcPtr == '%')
    {
        /*Counter64 and Countr32 can never be of read-write or read-create type
           so no need to add a case for it */
        switch (*++pcPtr)
        {
            case 't':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
                pMultiData1->u4_ULongValue = u4Val;
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
                pMultiData2->u4_ULongValue = u4Val;
                break;
            case 'p':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiData1->u4_ULongValue = u4Val;
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiData2->u4_ULongValue = u4Val;
                break;
            case 'i':
                i4Val = va_arg (DataValue, INT4);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
                pMultiData1->i4_SLongValue = i4Val;
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
                pMultiData2->i4_SLongValue = i4Val;
                break;
            case 's':
                args = va_arg (DataValue, UINT4 *);
                pOString = (tSNMP_OCTET_STRING_TYPE *) (VOID *) args;
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData1->pOctetStrValue->i4_Length = pOString->i4_Length;
                MEMCPY (pMultiData1->pOctetStrValue->pu1_OctetList,
                        pOString->pu1_OctetList, pOString->i4_Length);
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData2->pOctetStrValue->i4_Length = pOString->i4_Length;
                MEMCPY (pMultiData2->pOctetStrValue->pu1_OctetList,
                        pOString->pu1_OctetList, pOString->i4_Length);

                break;
            case 'o':
                args = va_arg (DataValue, UINT4 *);
                pTempOid = (tSNMP_OID_TYPE *) (VOID *) args;
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
                pMultiData1->pOidValue->u4_Length = pTempOid->u4_Length;
                MEMCPY (pMultiData1->pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
                pMultiData2->pOidValue->u4_Length = pTempOid->u4_Length;
                MEMCPY (pMultiData2->pOidValue->pu4_OidList,
                        pTempOid->pu4_OidList,
                        (pTempOid->u4_Length * sizeof (UINT4)));
                break;
            case 'm':
                args = va_arg (DataValue, UINT4 *);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData1->pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiData1->pOctetStrValue->pu1_OctetList,
                        (UINT1 *) args, 6);
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiData2->pOctetStrValue->i4_Length = 6;
                MEMCPY (pMultiData2->pOctetStrValue->pu1_OctetList,
                        (UINT1 *) args, 6);
                break;
            case 'u':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
                pMultiData1->u4_ULongValue = u4Val;
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
                pMultiData2->u4_ULongValue = u4Val;
                break;
            case 'g':
                u4Val = va_arg (DataValue, UINT4);
                pMultiData1->i2_DataType = SNMP_DATA_TYPE_GAUGE32;
                pMultiData1->u4_ULongValue = u4Val;
                pMultiData2->i2_DataType = SNMP_DATA_TYPE_GAUGE32;
                pMultiData2->u4_ULongValue = u4Val;
                break;
            default:
                return;
                break;
        }
    }
}

/************************************************************************/
/*  Function Name   : SNMPGetOidString                                  */
/*  Description     : This function provides the string format of the   */
/*                     the  given oid.                                  */
/*  Input           : pu4OidObject  - Oid in the u4Array format         */
/*                    u4OidLen      - Oid length                        */
/*  Output          : pu1OidStr                                         */
/*  Returns         : None                                              */
/************************************************************************/

VOID
SNMPGetOidString (UINT4 *pu4OidObject, UINT4 u4OidLen, UINT1 *pu1OidStr)
{
    UINT1               u1Index = 0;

    if (u4OidLen == 0)
    {
        UtlTrcLog (1, 1, "Utl", "Oid length is 0\n");
        return;
    }
    MEMSET (pu1OidStr, '\0', SNMP_MAX_OID_LENGTH);

    for (u1Index = 0; u1Index < (u4OidLen - 1); u1Index++)
    {
        SPRINTF ((CHR1 *) pu1OidStr, "%u.", *pu4OidObject);
        pu1OidStr += STRLEN (pu1OidStr);
        pu4OidObject++;
    }
    SPRINTF ((CHR1 *) pu1OidStr, "%u", *pu4OidObject);
    return;
}

/*****************************************************************************/
/* Function Name      : SNMPUtilGetObjValueLength.                           */
/*                                                                           */
/* Description        : This function returns the size of the datat type.    */
/*                                                                           */
/* Input(s)           : pVal      - Object Value.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Length of the Object Value.                          */
/*****************************************************************************/
UINT4
SNMPUtilGetObjValueLength (tSNMP_MULTI_DATA_TYPE * pVal)
{
    UINT4               u4Size = 0;

    switch (pVal->i2_DataType)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:

            u4Size = sizeof (pVal->u4_ULongValue);
            break;

        case SNMP_DATA_TYPE_INTEGER32:

            u4Size = sizeof (pVal->i4_SLongValue);
            break;

        case SNMP_DATA_TYPE_OBJECT_ID:

            u4Size = sizeof (pVal->pOidValue->u4_Length) +
                (pVal->pOidValue->u4_Length * sizeof (UINT4));
            break;

        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:

            u4Size = (UINT4)
                sizeof (pVal->pOctetStrValue->i4_Length) +
                (UINT4) pVal->pOctetStrValue->i4_Length;
            break;

        case SNMP_DATA_TYPE_COUNTER64:
            u4Size = sizeof (pVal->u8_Counter64Value);
            break;

        case SNMP_DATA_TYPE_NULL:
            break;

        default:
            break;

    }

    return u4Size;
}

/************************************************************************
 *  Function Name   : SNMPConvertDataToString
 *  Description     : Converts the given multidata to string.
 *  Input           : pData - Pointer to multidata
 *                    u2Type - Type of element in multidata
 *  Output          : pu1String - Pointer to a string where the result will
 *                    written
 *  Returns         : None
 ************************************************************************/

VOID
SNMPConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                         UINT1 *pu1String, UINT2 u2Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH];
    pu1String[0] = '\0';
    switch (u2Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            SPRINTF ((char *) pu1String, "%d", pData->i4_SLongValue);
            break;
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_DATA_TYPE_UNSIGNED32:
        case SNMP_DATA_TYPE_COUNTER32:
            SPRINTF ((char *) pu1String, "%u", pData->u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            SPRINTF ((char *) pu1String, "%u", pData->u8_Counter64Value.msn);
            SPRINTF ((char *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            STRCAT (pu1String, au1Temp);
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            u4Value = pData->u4_ULongValue;
            for (u4Count = 0; u4Count < (SNMP_MAX_IPADDR_LEN - 1); u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                au1Temp[0] = '\0';
                SPRINTF ((char *) au1Temp, "%u.", u4Temp);
                STRCAT (pu1String, au1Temp);
                u4Value = u4Value << 8;
            }
            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((char *) au1Temp, "%u", u4Temp);
            STRCAT (pu1String, au1Temp);
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            if (pData->pOidValue->u4_Length == 0)
                break;
            for (u4Count = 0; u4Count < (pData->pOidValue->u4_Length - 1);
                 u4Count++)
            {
                au1Temp[0] = '\0';
                SPRINTF ((char *) au1Temp, "%u.",
                         pData->pOidValue->pu4_OidList[u4Count]);
                STRCAT (pu1String, au1Temp);
            }
            au1Temp[0] = '\0';
            SPRINTF ((char *) au1Temp, "%u",
                     pData->pOidValue->pu4_OidList[u4Count]);
            STRCAT (pu1String, au1Temp);
            au1Temp[0] = '\0';
            STRCAT (pu1String, au1Temp);
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            if (pData->pOctetStrValue->i4_Length == 0)
                break;
            /*Since memory corruption occurs when the length exceeds, 
             *added this check as  workaround for if crash  ccurs in future */
            if (pData->pOctetStrValue->i4_Length > MAX_OCTET_STRING_LEN)
                break;

            MEMCPY (pu1String, pData->pOctetStrValue->pu1_OctetList,
                    pData->pOctetStrValue->i4_Length);
            break;
        default:
            break;
    }
}

/************************************************************************
 *  Function Name   : free_multi_oid 
 *  Description     : function to free oid pointer
 *  Input           : pOid - oid pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
free_MultiOid (UINT1 *pu1Oid)
{
    if (pu1Oid != NULL)
    {
        MemReleaseMemBlock (gSnmpMultiOidPoolId, (UINT1 *) pu1Oid);
    }
}

/*****************************************************************************/
/* Function Name      : SNMPAddEnterpriseOid                                 */
/*                                                                           */
/* Description        : This function adds the Enterprise OID in between the */
/*                      given Prefix and Suffix OID                          */
/*                                                                           */
/* Input(s)           : pPrefixOid - OID Prefix                              */
/*                      pSuffixOid - OID Suffix                              */
/*                                                                           */
/* Output(s)          : pFullOid  - Added EOid in between Prefix and Suffix  */
/*                                  OID.                                     */
/*                                  Assumes that pFullOid memory is          */
/*                                  allocated in the calling function        */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*****************************************************************************/
UINT4
SNMPAddEnterpriseOid (tSNMP_OID_TYPE * pPrefixOid, tSNMP_OID_TYPE * pSuffixOid,
                      tSNMP_OID_TYPE * pFullOid)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    UINT4              *pu4Temp = NULL;
    INT1               *pi1Eoid = NULL;

    pu4Temp = pFullOid->pu4_OidList;

    if (pPrefixOid->u4_Length == 0 || pSuffixOid->u4_Length == 0)
    {
        return OSIX_FAILURE;
    }

    pi1Eoid = (INT1 *) EoidGetEnterpriseOid ();

    /* Copy OID Prefix */
    MEMCPY (pu4Temp, pPrefixOid->pu4_OidList,
            pPrefixOid->u4_Length * sizeof (UINT4));

    pu4Temp = pu4Temp + pPrefixOid->u4_Length;

    /* Convert String to OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString (pi1Eoid);

    if (pEnterpriseOid == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Copy Eoid */
    MEMCPY (pu4Temp, pEnterpriseOid->pu4_OidList,
            pEnterpriseOid->u4_Length * sizeof (UINT4));

    pu4Temp = pu4Temp + pEnterpriseOid->u4_Length;

    /* Copy OID Suffix */
    MEMCPY (pu4Temp, pSuffixOid->pu4_OidList,
            pSuffixOid->u4_Length * sizeof (UINT4));
    pFullOid->u4_Length = pPrefixOid->u4_Length + pEnterpriseOid->u4_Length
        + pSuffixOid->u4_Length;

    SNMP_FreeOid (pEnterpriseOid);

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPUpdateEOID 
 *  Description     : This function verifies if the Enterprise OId is 
 *                    modified. If yes, updates the custom modified EOid
 *                    with the default ISS oid to perform the user requested
 *                    operation.
 *  Input           : pOid - Custom modified Oid.
 *  Output          : pRetOid - Corresponding ISS Oid.
 *                    pi4Error - Error Value.
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
VOID
SNMPUpdateEOID (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pRetOid,
                UINT1 u1NextFlag)
{
    UINT4               u4RemLength = 0;
    UINT4               au1EntOid[] = { 1, 3, 6, 1, 4, 1 };
    INT1                i1RetValue = 0;
    UINT4               u4Itr = 0;
    UINT1               u1EntOid1 = 0;
    UINT1               u1EntOid2 = 0;
    UINT4               u4EntLength = 0;

    pRetOid->u4_Length = 0;

    if (SNMP_CALLBACK[SNMP_UPDATE_EOID_EVENT].pSnmpUpdateEOIDCallBack != NULL)
    {
        SNMP_CALLBACK[SNMP_UPDATE_EOID_EVENT].pSnmpUpdateEOIDCallBack (pOid,
                                                                       pRetOid,
                                                                       u1NextFlag);
        return;
    }

    MEMSET (pRetOid->pu4_OidList, 0, SNMP_MAX_OID_LENGTH * sizeof (UINT4));

    if ((gEntOid1->pu4_OidList[0] != ISS_ENT_OID_1)
        && (MEMCMP (pOid->pu4_OidList, au1EntOid, sizeof (au1EntOid)) == 0))
    {
        /* Below logic is take care variable length of Enterprise OID
         * If Enterprise OID length is more than 1, 
         * then given pOid's enterprise oid is compared to the 
         * EOID length defined in eoid.h.
         * */

        for (u4Itr = 0;
             ((u4Itr < gEntOid1->u4_Length)
              && ((EOID_OFFSET + u4Itr) < pOid->u4_Length)); u4Itr++)
        {
            if (gEntOid1->pu4_OidList[u4Itr] !=
                pOid->pu4_OidList[EOID_OFFSET + u4Itr])
            {
                u1EntOid1 = 1;
                break;
            }
        }
        if (u1EntOid1 == 0)
        {
            u4EntLength = u4Itr;
        }
        for (u4Itr = 0;
             ((u4Itr < gEntOid2->u4_Length)
              && ((EOID_OFFSET + u4Itr) < pOid->u4_Length)); u4Itr++)
        {
            if (gEntOid2->pu4_OidList[u4Itr] !=
                pOid->pu4_OidList[EOID_OFFSET + u4Itr])
            {
                u1EntOid2 = 1;
                break;
            }
        }

        /* This u4EntLength is the length of pOid that matches with gEntOid */

        if (u1EntOid2 == 0)
        {
            u4EntLength = u4Itr;
        }

        /* Enter this block, only when the given OID is equal to 
         * either ENTERPRISE_OID_1 or ENTERPRISE_OID_2 defined in eoid.h
         * */

        if ((u1EntOid1 == 0) || (u1EntOid2 == 0))
        {
            MEMCPY (pRetOid->pu4_OidList, pOid->pu4_OidList,
                    EOID_OFFSET * sizeof (UINT4));
            pRetOid->u4_Length = EOID_OFFSET;

            if (u1EntOid1 == 0)
            {
                if (pOid->pu4_OidList[EOID_OFFSET] <= gEntOid1->pu4_OidList[0])
                {
                    pRetOid->pu4_OidList[EOID_OFFSET] = ISS_ENT_OID_1;
                    pRetOid->u4_Length++;
                }
            }
            else
            {
                if (pOid->pu4_OidList[EOID_OFFSET] >= gEntOid1->pu4_OidList[0])
                {
                    pRetOid->pu4_OidList[EOID_OFFSET] = ISS_ENT_OID_2;
                    pRetOid->u4_Length++;
                }
            }

            if (((u1NextFlag == FALSE) && (i1RetValue != 0))
                || ((u1NextFlag == TRUE) && (i1RetValue > 0)))
            {
                MEMCPY (pRetOid->pu4_OidList, pOid->pu4_OidList,
                        pOid->u4_Length * sizeof (UINT4));
                pRetOid->u4_Length = pOid->u4_Length;
                return;
            }
            u4RemLength = pOid->u4_Length - (EOID_OFFSET + u4EntLength);
            MEMCPY (&pRetOid->pu4_OidList[7],
                    &pOid->pu4_OidList[EOID_OFFSET + u4EntLength],
                    (u4RemLength * sizeof (UINT4)));
            pRetOid->u4_Length += u4RemLength;
            return;
        }
    }
    MEMCPY (pRetOid->pu4_OidList, pOid->pu4_OidList,
            pOid->u4_Length * sizeof (UINT4));
    pRetOid->u4_Length = pOid->u4_Length;
    return;
}

/************************************************************************
 *  Function Name   : SNMPRevertEOID 
 *  Description     : This function reverses the given ISS Oid to its 
 *                    corresponding custom defined oid.
 *  Input           : pSrcOid - ISS Oid.
 *  Output          : pDestOid - Corresponding custom Oid.
 *                    pi4Error - Error Value.
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
VOID
SNMPRevertEOID (tSNMP_OID_TYPE * pSrcOid, tSNMP_OID_TYPE * pDestOid)
{
    UINT4               u4RemLength = 0;

    if (SNMP_CALLBACK[SNMP_REVERT_EOID_EVENT].pSnmpRevertEOIDCallBack != NULL)
    {
        SNMP_CALLBACK[SNMP_REVERT_EOID_EVENT].pSnmpRevertEOIDCallBack (pSrcOid,
                                                                       pDestOid);
        return;
    }
    if (pDestOid == NULL)
    {
        return;
    }
    MEMSET (pDestOid->pu4_OidList, 0, SNMP_MAX_OID_LENGTH * sizeof (UINT4));
    pDestOid->u4_Length = 0;
    if ((pSrcOid->pu4_OidList[6] != ISS_ENT_OID_1)
        && (pSrcOid->pu4_OidList[6] != ISS_ENT_OID_2))
    {
        MEMCPY (pDestOid->pu4_OidList, pSrcOid->pu4_OidList,
                pSrcOid->u4_Length * sizeof (UINT4));
        pDestOid->u4_Length = pSrcOid->u4_Length;
        return;
    }

    MEMCPY (pDestOid->pu4_OidList, pSrcOid->pu4_OidList, 6 * sizeof (UINT4));
    u4RemLength = pSrcOid->u4_Length - 7;
    if (pSrcOid->pu4_OidList[6] == ISS_ENT_OID_1)
    {
        MEMCPY (&pDestOid->pu4_OidList[6], gEntOid1->pu4_OidList,
                gEntOid1->u4_Length * sizeof (UINT4));
    }
    else if (pSrcOid->pu4_OidList[6] == ISS_ENT_OID_2)
    {
        MEMCPY (&pDestOid->pu4_OidList[6], gEntOid2->pu4_OidList,
                gEntOid2->u4_Length * sizeof (UINT4));
    }
    MEMCPY (&pDestOid->pu4_OidList[6 + gEntOid1->u4_Length],
            &pSrcOid->pu4_OidList[7], u4RemLength * sizeof (UINT4));
    pDestOid->u4_Length = 6 + gEntOid1->u4_Length + u4RemLength;
    return;
}

/************************************************************************
 *  Function Name   : SNMPInitEOID  
 *  Description     : This function initializes the enterprises oids for 
 *                     both the sub trees.
 *  Input           : None.
 *  Output          : None.
 *  Returns         : None.
 ************************************************************************/
VOID
SNMPInitEOID (VOID)
{
    SNMP_CALLBACK[SNMP_UPDATE_EOID_EVENT].pSnmpUpdateEOIDCallBack = NULL;
    SNMP_CALLBACK[SNMP_REVERT_EOID_EVENT].pSnmpRevertEOIDCallBack = NULL;
    SNMP_CALLBACK[SNMP_UPDATE_GETNEXT_EOID_EVENT].
        pSnmpUpdateGetNextEOIDCallBack = NULL;
    gEntOid1 = SNMP_AGT_GetOidFromString ((INT1 *) EoidGetEnterpriseOid ());
    gEntOid2 =
        SNMP_AGT_GetOidFromString ((INT1 *) EoidGetSecondEnterpriseOid ());
    return;
}

/************************************************************************
 *  Function Name   : SnmpRevertEoidString
 *  Description     : This function modify the Enterprise OID for the 
 *                     given oid string.
 *  Input(s)        : pu1Oid - Oid string.
 *  Output(s)       : pu1Oid - Updated oid string.
 *  Returns         : None
 *
 ************************************************************************/
VOID
SnmpRevertEoidString (UINT1 *pu1Oid)
{
    tSNMP_OID_TYPE     *pInputOid = NULL;
    tSNMP_OID_TYPE     *pOutputOid = NULL;

    pInputOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1Oid);
    if (pInputOid == NULL)
    {
        return;
    }
    pOutputOid = alloc_oid ((INT4) pInputOid->u4_Length);
    if (pOutputOid != NULL)
    {
        SNMPRevertEOID (pInputOid, pOutputOid);
        SNMPGetOidString (pOutputOid->pu4_OidList, pOutputOid->u4_Length,
                          pu1Oid);
        free_oid (pOutputOid);
    }
    SNMP_FreeOid (pInputOid);
}

/************************************************************************
 *  Function Name   : SnmpUpdateEoidString
 *  Description     : This function modifies the Enterprise OID for the 
 *                     given oid string to ISS oids.
 *  Input(s)        : pu1Oid - Oid string.
 *  Output(s)       : pu1Oid - Updated oid string.
 *  Returns         : None
 *
 ************************************************************************/
VOID
SnmpUpdateEoidString (UINT1 *pu1Oid)
{
    tSNMP_OID_TYPE     *pInputOid = NULL;
    tSNMP_OID_TYPE     *pOutputOid = NULL;

    pInputOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1Oid);
    if (pInputOid == NULL)
    {
        return;
    }
    pOutputOid = alloc_oid ((INT4) pInputOid->u4_Length);
    if (pOutputOid != NULL)
    {
        SNMPUpdateEOID (pInputOid, pOutputOid, TRUE);
        SNMPGetOidString (pOutputOid->pu4_OidList, pOutputOid->u4_Length,
                          pu1Oid);
        free_oid (pOutputOid);
    }
    SNMP_FreeOid (pInputOid);
}

#ifndef SNMP_3_WANTED
/************************************************************************
 *  Function Name   : SNMP_free_snmp_vb_list 
 *  Description     : Function to free the varbind List 
 *  Input           : vb_list - pointer to varbind list
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_free_snmp_vb_list (tSNMP_VAR_BIND * vb_list)
{
    UNUSED_PARAM (vb_list);
    return;
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_Trap 
 *  Description     : Called from protocol for v1 agent
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                    
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/

VOID
SNMP_AGT_RIF_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                          UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList)
{
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pVbList);
    return;
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_v2Trap 
 *  Description     : Function to send v2 trap from protocol
 *  Input           : pVbList - Variable Bind List Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SNMP_AGT_RIF_Notify_v2Trap (tSNMP_VAR_BIND * pVbList)
{
    UNUSED_PARAM (pVbList);
    return;
}

/************************************************************************
 *  Function Name   : SNMP_Context_Notify_Trap 
 *  Description     : Called from protocol for trap with protocool context name
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                   
 *                    pContext - Context Name pointer
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_Context_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                          UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList,
                          tSNMP_OCTET_STRING_TYPE * pContext)
{
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pVbList);
    UNUSED_PARAM (pContext);

    return;
}

/************************************************************************
 *  Function Name   : SNMPRegisterMibWithContextIdAndLock 
 *  Description     : Function to register a protocol to agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer 
 *                    pLockPointer - fucntion pointer to take the protocol lock.
 *                    pUnLockPointer - fucntion pointer to release
 *                                     the protocol lock. 
 *                    pSetContextPointer - function pointer to configure 
 *                                         the context Id                 
 *                    pSetContextPointer - function pointer to release the 
 *                                         configured context Id                
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
SNMPRegisterMibWithContextIdAndLock (tSNMP_OID_TYPE * MibID,
                                     tMibData * pMibData,
                                     INT4 (*pLockPointer) (VOID),
                                     INT4 (*pUnlockPointer) (VOID),
                                     INT4 (*pSetContextPointer) (UINT4),
                                     VOID (*pReleaseContextPointer) (VOID),
                                     tSnmpMsrBool SnmpMsrTgr)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);
    UNUSED_PARAM (pLockPointer);
    UNUSED_PARAM (pUnlockPointer);
    UNUSED_PARAM (pSetContextPointer);
    UNUSED_PARAM (pReleaseContextPointer);
    UNUSED_PARAM (SnmpMsrTgr);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name :SNMPCheckForNVTChars
*  Description   :This procedure finds if any non-printable characters
*                 present in the given string ( includes special chars,
*                 non-US ascii chars). Useful to check for valid inputs for
*                 DisplayString Type objects.(as per RFC: 1903).
*
*  Parameter(s)  : pu1TestStr - pointer to string to be validated,
*                  u4Length   - Length upto which the string will be
*                               validated. 
*
*  Return Values : OSIX_SUCCESS - if there are no such characters present.
*                  OSIX_FAILURE - if at all any non-printable characters 
*                                 present in the given string.
*********************************************************************/
UINT4
SNMPCheckForNVTChars (UINT1 *pu1TestStr, INT4 i4Length)
{
    UNUSED_PARAM (pu1TestStr);
    UNUSED_PARAM (i4Length);
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPSet 
 *  Description     : Function to do snmp set operation for a given oid
 *  Input           : OID - Given Oid
 *                    pIndex - Index Array pointer. Memory is allocated
 *                    by application. This is just to avoid memory 
 *                    allocation at DB access level.
 *                    pVal - Value to be set.
 *  Output          : pError - Error number in case failure
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SNMPSet (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError,
         tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pVal);
    UNUSED_PARAM (pError);
    UNUSED_PARAM (pIndex);
    UNUSED_PARAM (u4VcNum);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPGet  
 *  Description     : Get Routine for getting snmp value for a OID
 *  Input           : OID - OID for which get should be called
 *                    pIndex - Index Array pointer. Allocated by application
 *                    and snmp will use the memory. This just to avoid memory
 *                    allocation in DB module.
 *  Output          : pData - Multidata in which data will updated
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGet (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pData, UINT4 *pError,
         tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pError);
    UNUSED_PARAM (pIndex);
    UNUSED_PARAM (u4VcNum);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPTest
 *  Description     : Function will test  the value to be set for a given oid
 *  Input           : OID - Given Oid
 *                    pIndex - Index Array pointer. Memory is allocated
 *                    by application. This is just to avoid memory
 *                    allocation at DB access level.
 *                    pVal - Value to be set.
 *  Output          : pError - Error number in case failure
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SNMPTest (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError,
          tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pVal);
    UNUSED_PARAM (pError);
    UNUSED_PARAM (pIndex);
    UNUSED_PARAM (u4VcNum);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMP_Notify_Trap 
 *  Description     : Called from SNMP_AGT_RIF_Notify_Trap 
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                    
 *                    pEventCommunity - Pointer to Event Community name 
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                  UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList,
                  tSNMP_OCTET_STRING_TYPE * pEventCommunity)
{
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pVbList);
    UNUSED_PARAM (pEventCommunity);

    return;
}

/************************************************************************
 *  Function Name   : SNMPGetDefaultValueForObj
 *  Description     : Sends update event trigger to MSR if Set operation 
 *              for a varbind is successful inside nmhSet
 *  Input           : pMultiIndex - This variable contains the number of 
 *              index in the received OID and index value in case of 
 *              tabular OID and in case of scalar num index is 0
 *                    is_row_status_present- in case of tabular 
 *            True when row status oid recived in update.
 *            False when other object is received.
 *                    pMultiData - This variable contains the OID ,
 *             OID tye and its value. In case of tabular OID 
 *            shall be Rowstatus OID can be  ACTIVE or DESTROY.
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
INT4
SNMPGetDefaultValueForObj (tSNMP_OID_TYPE OID,
                           tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError)
{
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pVal);
    UNUSED_PARAM (pError);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPGetNextOID 
 *  Description     : Get the next OID and Value for the given OID
 *  Input           : OID - Given Oid
 *                    pCurIndex, pNextIndex - Index Array pointer. Index
 *                    memory is allocated at application level. This is
 *                    to avoid memory allocation at DB access level.
 *  Output          : pNextOid - Next OID 
 *                    pNextData - Value of next oid
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGetNextOID (tSNMP_OID_TYPE OID, tSNMP_OID_TYPE * pNextOid,
                tSNMP_MULTI_DATA_TYPE * pNextData,
                UINT4 *pError, tSnmpIndex * pCurIndex, tSnmpIndex * pNextIndex,
                UINT4 u4VcNum)
{
    UNUSED_PARAM (pNextOid);
    UNUSED_PARAM (pNextData);
    UNUSED_PARAM (pError);
    UNUSED_PARAM (pCurIndex);
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pNextIndex);
    UNUSED_PARAM (u4VcNum);

    return SNMP_FAILURE;

}

/************************************************************************
 *  Function Name   : SNMPGetNextReadWriteOID 
 *  Description     : Get the next read-write OID and Value for the given OID
 *  Input           : OID - Given Oid
 *                    pCurIndex, pNextIndex - Index Array pointer. Index
 *                    memory is allocated at application level. This is
 *                    to avoid memory allocation at DB access level.
 *  Output          : pNextOid - Next OID 
 *                    pNextData - Value of next oid
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGetNextReadWriteOID (tSNMP_OID_TYPE OID, tSNMP_OID_TYPE * pNextOid,
                         tSNMP_MULTI_DATA_TYPE * pNextData,
                         UINT4 *pError, tSnmpIndex * pCurIndex,
                         tSnmpIndex * pNextIndex, UINT4 u4VcNum)
{
    UNUSED_PARAM (pNextOid);
    UNUSED_PARAM (pNextData);
    UNUSED_PARAM (pError);
    UNUSED_PARAM (pCurIndex);
    UNUSED_PARAM (OID);
    UNUSED_PARAM (pNextIndex);
    UNUSED_PARAM (u4VcNum);

    return SNMP_FAILURE;

}

/************************************************************************
 *  Function Name   : GetMbDbEntry 
 *  Description     : This function searches for the given OID in the given
 *                    Mib database and if avalaible returns that entry along
 *                    with the next entry
 *  Input           : ObjectID - Oid Pointer
 *                    u4Count - match length                        
 *  Output          : pNext - Next Mbdb Pointer
 *  Returns         : Mbdb pointer or null 
 ************************************************************************/
VOID
GetMbDbEntry (tSNMP_OID_TYPE ObjectID, tMbDbEntry ** pNext, UINT4 *u4Count,
              tSnmpDbEntry * pSnmpDbEntry)
{
    UNUSED_PARAM (ObjectID);
    UNUSED_PARAM (pNext);
    UNUSED_PARAM (u4Count);
    UNUSED_PARAM (pSnmpDbEntry);

    return;
}

/************************************************************************
 *  Function Name   : SNMPFormOid 
 *  Description     : Function to form OID from index array and actual OID
 *  Input           : pIndex - Index Array
 *                    pCur - DB entry of a  OID for which full OID to be 
 *                    formed
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
VOID
SNMPFormOid (tSNMP_OID_TYPE * pOid, const tMbDbEntry * pCur,
             const tSnmpIndex * pIndex)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pCur);
    UNUSED_PARAM (pIndex);

    return;
}

/************************************************************************
 *  Function Name   : SNMPV3DefaultDelete 
 *  Description     : Function Removes all the SNMPv3 default table entries
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPV3DefaultDelete ()
{
    return;
}

INT4
SNMPConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u1HexFlag);

    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPCopyOid
*  Description   : Function to copy one Oid to another one.
*  Parameter(s)  : pFirstOid - Destination Oid Pointer
*                  pSecondOid - Source Oid Pointer
*  Return Values : None
*********************************************************************/
VOID
SNMPCopyOid (tSNMP_OID_TYPE * pFirstOid, tSNMP_OID_TYPE * pSecondOid)
{
    UNUSED_PARAM (pFirstOid);
    UNUSED_PARAM (pSecondOid);
    return;
}

/************************************************************************
 *  Function Name   : SNMPGetMbDbTgrValue
 *  Description     : This function searches for the given OID in the given
 *                    Mib database and if available returns the snmp trigger
 *                    registered for that OID.
 *  Input           : ObjectID - Oid Pointer
 *  Output          : pSnmpTgr - Trigger registered for the given oid.
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/
INT4
SNMPGetMbDbTgrValue (tSNMP_OID_TYPE ObjectID, BOOL1 * pbSnmpTgr)
{
    UNUSED_PARAM (ObjectID);
    UNUSED_PARAM (pbSnmpTgr);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : OIDCompare 
 *  Description     : This function compares OID1 with OID2
 *                    if OID1 is lesser than OID2 it returns SNMP_LESSER
 *                    if OID1 is greater than OID2 it returns SNMP_GREATER
 *                    if Both are and lengths are also  equal it returns
 *                    ABS_EQUAL
 *  Input           : OID1 - First Oid, OID2 - Second Oid 
 *  Output          : u4Count - Length upto which they are matching
 *  Returns         : GEATER/SNMP_LESSER/EQUAL
 ************************************************************************/
UINT4
OIDCompare (tSNMP_OID_TYPE OID1, tSNMP_OID_TYPE OID2, UINT4 *u4Count)
{
    UNUSED_PARAM (OID1);
    UNUSED_PARAM (OID2);
    UNUSED_PARAM (u4Count);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SnmpGetAgentParam
 *  
 *  Description     : This API returns the SNMP agent parameters.
 *  
 *  Pre-requisites  : NONE
 *  
 *  Input           : u4MIBSequence - Sequence of MIB in the list
 *  
 *  Output          : pAgentParam - SNMP agent parameters
 *                                       
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ************************************************************************/

PUBLIC INT1
SnmpGetAgentParam (UINT4 u4MIBSequence, tSnmpAgentParam * pAgentParam)
{
    UNUSED_PARAM (u4MIBSequence);
    UNUSED_PARAM (pAgentParam);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPRegisterMib 
 *  Description     : Function to register a protocol to agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer  
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
SNMPRegisterMibWithLock (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                         INT4 (*pLockPointer) (VOID),
                         INT4 (*pUnlockPointer) (VOID), tSnmpMsrBool SnmpMsrTgr)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);
    UNUSED_PARAM (pLockPointer);
    UNUSED_PARAM (pUnlockPointer);
    UNUSED_PARAM (SnmpMsrTgr);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPAddSysorEntry 
 *  Description     : Function add a protocol to sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPAddSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UNUSED_PARAM (pMibOid);
    UNUSED_PARAM (pu1MibName);

    return;
}

INT4
SNMPRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                 tSnmpMsrBool SnmpMsrTgr)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);
    UNUSED_PARAM (SnmpMsrTgr);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPUnRegisterMib 
 *  Description     : Function to unregister a protocol from agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer  
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/

INT4
SNMPUnRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPDelSysorEntry 
 *  Description     : Function Delete a protocol from sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPDelSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UNUSED_PARAM (pMibOid);
    UNUSED_PARAM (pu1MibName);

    return;
}

/****************************************************************************/
/*    Function Name      : SnmpApiSetSNMPUpdateDefault                      */
/*                                                                          */
/*    Description        : This function is invoked to set the global       */
/*                         variable gu1SNMPUpdateDefault. The vaule is set  */
/*                         to OSIX_TRUE for Aricent SNMP Defaults           */
/*                         initialization or OSIX_FALSE if Aricent SNMP     */
/*                         defaults is not                                  */
/*                         needed to be initialized                         */
/*                                                                          */
/*    Input(s)           : u1Status- OSIX_TRUE or OSIX_FALSE                */
/*                         needed to be initialized                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS or SNMP_FAILURE.                    */
/****************************************************************************/

INT4
SnmpApiSetSNMPUpdateDefault (UINT1 u1Status)
{
    UNUSED_PARAM (u1Status);
    return SNMP_FAILURE;
}

/****************************************************************************/
/*    Function Name      : SnmpApiGetSNMPUpdateDefault                      */
/*                                                                          */
/*    Description        : This function is invoked to get the global       */
/*                         variable gu1SNMPUpdateDefault                    */
/*                                                                          */
/*    Input(s)           : VOID                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Value of gu1SNMPUpdateDefault                    */
/****************************************************************************/

UINT1
SnmpApiGetSNMPUpdateDefault (VOID)
{
    return OSIX_FALSE;
}

#endif
/************************************************************************
 *  Function Name   : SNMPOIDCopy 
 *  Description     : Function to copy oid
 *  Input           : pSrc - Source Oid  
 *                    pDest - Destination oid
 *  Ouput           : None                    
 *  Returns         : None 
 ************************************************************************/
VOID
SNMPOIDCopy (tSNMP_OID_TYPE * pDest, tSNMP_OID_TYPE * pSrc)
{
    if ((pDest != NULL) && (pSrc != NULL))
    {
        pDest->u4_Length = pSrc->u4_Length;
        memcpy (pDest->pu4_OidList, pSrc->pu4_OidList,
                pDest->u4_Length * sizeof (UINT4));
    }
}

/************************************************************************
 *  Function Name   : SNMPUpdateGetNextEOID
 *  Description     : Function to copy oid
 *  Input           : pSrc - Source Oid
 *                    pDest - Destination oid
 *  Ouput           : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPUpdateGetNextEOID (tSNMP_OID_TYPE * pOID, tMbDbEntry ** pNext, UINT4 *pLen,
                       tSnmpDbEntry * SnmpDbEntry)
{
    UINT4               u4Len = 0;
    tSNMP_OID_TYPE     *pInputOid = NULL;
    tMbDbEntry         *pTempNext = NULL;
    tSnmpDbEntry        tmpSnmpDbEntry;

    /* Return if MIB Save is in Progress
     * */
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        return;
    }

    /* To check if Enteprise EOID is configured.
     * And Enterprise OID falls between 2076 and 29601
     * Only then perform these conversions
     * */
    if (SNMP_CALLBACK[SNMP_UPDATE_GETNEXT_EOID_EVENT].
        pSnmpUpdateGetNextEOIDCallBack != NULL)
    {
        SNMP_CALLBACK[SNMP_UPDATE_GETNEXT_EOID_EVENT].
            pSnmpUpdateGetNextEOIDCallBack (pOID, pNext, pLen, SnmpDbEntry);
        return;
    }

    if ((gEntOid1->pu4_OidList[0] != ISS_ENT_OID_1) &&
        ((gEntOid2->pu4_OidList[0] > ISS_ENT_OID_1) &&
         (gEntOid2->pu4_OidList[0] < ISS_ENT_OID_2)))
    {
        /* Perform below operation only if given OID falls in 2076 tree
         * */
        if (pOID->pu4_OidList[EOID_OFFSET] == ISS_ENT_OID_1)
        {
            /* If walk changes from 2076 tree to anyother tree, check is performed,
             * if next OID is not equal to Enterprise OID2, that means we have to walk 29601 tree first.
             * */
            if (*pNext != NULL)
            {
                if ((pOID->pu4_OidList[EOID_OFFSET] !=
                     (*pNext)->ObjectID.pu4_OidList[EOID_OFFSET])
                    && ((*pNext)->ObjectID.pu4_OidList[EOID_OFFSET] !=
                        ISS_ENT_OID_2))
                {
                    if ((*pNext)->ObjectID.pu4_OidList[EOID_OFFSET] !=
                        gEntOid2->pu4_OidList[0])
                    {
                        pInputOid = alloc_oid (EOID_OFFSET + 2);
                        if (pInputOid != NULL)
                        {
                            MEMCPY (pInputOid->pu4_OidList, pOID->pu4_OidList,
                                    EOID_OFFSET * sizeof (UINT4));
                            pInputOid->pu4_OidList[EOID_OFFSET] = ISS_ENT_OID_2;
                            pInputOid->pu4_OidList[EOID_OFFSET + 1] = 1;
                            GetMbDbEntry (*pInputOid, pNext, &u4Len,
                                          &tmpSnmpDbEntry);
                            SNMP_FreeOid (pInputOid);
                        }
                    }
                    else if (((*pNext)->ObjectID.pu4_OidList[EOID_OFFSET] ==
                              gEntOid2->pu4_OidList[0]) &&
                             ((*pNext)->ObjectID.pu4_OidList[EOID_OFFSET + 1] !=
                              gEntOid2->pu4_OidList[1]))
                    {
                        pInputOid = alloc_oid (EOID_OFFSET + 2);
                        if (pInputOid != NULL)
                        {
                            MEMCPY (pInputOid->pu4_OidList, pOID->pu4_OidList,
                                    EOID_OFFSET * sizeof (UINT4));
                            pInputOid->pu4_OidList[EOID_OFFSET] = ISS_ENT_OID_2;
                            pInputOid->pu4_OidList[EOID_OFFSET + 1] = 1;
                            GetMbDbEntry (*pInputOid, pNext, &u4Len,
                                          &tmpSnmpDbEntry);
                            SNMP_FreeOid (pInputOid);
                        }
                    }
                }
            }
        }
        /* Perform below operation only if given OID falls in 29601 tree
         * */
        if (pOID->pu4_OidList[EOID_OFFSET] == ISS_ENT_OID_2)
        {
            if (*pNext != NULL)
            {
                /* If walk changes from 29601 tree to any other tree, now all custom registered OIDs to be walked.
                 * So, Next OID is modified to end of 2076 tree.
                 * */
                if ((pOID->pu4_OidList[EOID_OFFSET] !=
                     (*pNext)->ObjectID.pu4_OidList[EOID_OFFSET])
                    && (pOID->pu4_OidList[EOID_OFFSET] >
                        (*pNext)->ObjectID.pu4_OidList[EOID_OFFSET]))
                {
                    pInputOid = alloc_oid (EOID_OFFSET + 2);
                    if (pInputOid != NULL)
                    {
                        MEMCPY (pInputOid->pu4_OidList, pOID->pu4_OidList,
                                EOID_OFFSET * sizeof (UINT4));
                        pInputOid->pu4_OidList[EOID_OFFSET] = 2076;
                        pInputOid->pu4_OidList[EOID_OFFSET + 1] = 0xFFF;
                        pInputOid->u4_Length = EOID_OFFSET + 2;
                        GetMbDbEntry (*pInputOid, &pTempNext, &u4Len,
                                      &tmpSnmpDbEntry);
                        SNMP_FreeOid (pInputOid);
                    }
                    if (pTempNext != NULL)
                    {
                        if ((pTempNext->ObjectID.pu4_OidList[EOID_OFFSET] !=
                             ISS_ENT_OID_2)
                            && (pTempNext->ObjectID.pu4_OidList[EOID_OFFSET] !=
                                ISS_ENT_OID_1))
                        {
                            pInputOid = alloc_oid (EOID_OFFSET + 2);
                            if (pInputOid != NULL)
                            {
                                MEMCPY (pInputOid->pu4_OidList,
                                        pOID->pu4_OidList,
                                        EOID_OFFSET * sizeof (UINT4));
                                pInputOid->pu4_OidList[EOID_OFFSET] =
                                    ISS_ENT_OID_1;
                                pInputOid->pu4_OidList[EOID_OFFSET + 1] = 0xFFF;
                                pInputOid->u4_Length = EOID_OFFSET + 2;
                                GetMbDbEntry (*pInputOid, pNext, &u4Len,
                                              &tmpSnmpDbEntry);
                                SNMP_FreeOid (pInputOid);
                            }
                        }
                    }
                }
            }

        }
        /* Perform below operation only if given OID does not fall under either 2076 or 29601
         * */
        if ((pOID->pu4_OidList[EOID_OFFSET] != ISS_ENT_OID_1) &&
            (pOID->pu4_OidList[EOID_OFFSET] != ISS_ENT_OID_2))
        {
            /* If walk changes from any custom tree to anyother tree, check is performed,
             * if next OID is equals 29601 tree, that means all custom OIDs are walked.
             * Now, we have to restore the walk to actual sequence.
             * So modifying the Next OID to end of 29601 tree.
             * */
            if (*pNext != NULL)
            {
                if (pOID->pu4_OidList[EOID_OFFSET] !=
                    (*pNext)->ObjectID.pu4_OidList[EOID_OFFSET])
                {
                    if ((*pNext)->ObjectID.pu4_OidList[EOID_OFFSET] ==
                        ISS_ENT_OID_2)
                    {
                        pInputOid = alloc_oid (EOID_OFFSET + 2);
                        if (pInputOid != NULL)
                        {
                            MEMCPY (pInputOid->pu4_OidList, pOID->pu4_OidList,
                                    EOID_OFFSET * sizeof (UINT4));
                            pInputOid->pu4_OidList[EOID_OFFSET] = ISS_ENT_OID_2;
                            pInputOid->pu4_OidList[EOID_OFFSET + 1] = 0xFFF;
                            pInputOid->u4_Length = EOID_OFFSET + 2;
                            GetMbDbEntry (*pInputOid, pNext, &u4Len,
                                          &tmpSnmpDbEntry);
                            SNMP_FreeOid (pInputOid);
                        }
                    }
                }
            }
        }
    }
}

/************************************************************************
 *  Function Name   : free_agentparams
 *  Description     : function to free tSnmpAgentParam pointer 
 *  Input           : pAgentParam - tSnmpAgentParam pointer
 *  Output          : None
 *  Returns         : None
 *************************************************************************/
VOID
free_agentparams (tSnmpAgentParam * pAgentParam)
{
    if (pAgentParam != NULL)
    {
        MemReleaseMemBlock (gSnmpAgentParamsPoolId, (UINT1 *) pAgentParam);
    }
}

/************************************************************************
 *  Function Name   : alloc_agentparams
 *  Description     : function to allocate SNMP Agent Params
 *  Input           : None
 *  Output          : None
 *  Returns         : Agent Params pointer
 *************************************************************************/
tSnmpAgentParam    *
alloc_agentparams ()
{
    tSnmpAgentParam    *pAgentParam = NULL;
    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }
    if ((pAgentParam = MemAllocMemBlk (gSnmpAgentParamsPoolId)) == NULL)
    {
        return NULL;
    }
    return pAgentParam;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpAllocateMemory                                   */
/*                                                                           */
/*     DESCRIPTION      : This function allocates memory using built-in
 *                        memory allocation functions.                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : &pNextDestAddr, &pNextNextHopAdd, &pNextRoutePolicy
 *                        &pCurrDestAddr, &pCurrNextHopAddr, &pCurrRoutePolicy*/
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT1
IpAllocateMemory (tSNMP_OCTET_STRING_TYPE ** pNextDestAddr,
                  tSNMP_OCTET_STRING_TYPE ** pNextNextHopAddr,
                  tSNMP_OID_TYPE ** pNextRoutePolicy,
                  tSNMP_OCTET_STRING_TYPE ** pCurrDestAddr,
                  tSNMP_OCTET_STRING_TYPE ** pCurrNextHopAddr,
                  tSNMP_OID_TYPE ** pCurrRoutePolicy)
{
    *pNextDestAddr = allocmem_octetstring (MAX_ADDR_LEN);
    if (*pNextDestAddr == NULL)
    {
        return OSIX_FAILURE;
    }
    *pNextNextHopAddr = allocmem_octetstring (MAX_ADDR_LEN);
    if (*pNextNextHopAddr == NULL)
    {
        free_octetstring (*pNextDestAddr);
        return OSIX_FAILURE;
    }
    *pNextRoutePolicy = alloc_oid (MAX_ADDR_LEN);
    if (*pNextRoutePolicy == NULL)
    {
        free_octetstring (*pNextDestAddr);
        free_octetstring (*pNextNextHopAddr);
        return OSIX_FAILURE;
    }
    *pCurrDestAddr = allocmem_octetstring (MAX_ADDR_LEN);
    if (*pCurrDestAddr == NULL)
    {
        free_octetstring (*pNextDestAddr);
        free_octetstring (*pNextNextHopAddr);
        free_oid (*pNextRoutePolicy);
        return OSIX_FAILURE;
    }
    *pCurrNextHopAddr = allocmem_octetstring (MAX_ADDR_LEN);
    if (*pCurrNextHopAddr == NULL)
    {
        free_octetstring (*pNextDestAddr);
        free_octetstring (*pNextNextHopAddr);
        free_oid (*pNextRoutePolicy);
        free_octetstring (*pCurrDestAddr);
        return OSIX_FAILURE;
    }
    *pCurrRoutePolicy = alloc_oid (MAX_ADDR_LEN);
    if (*pCurrRoutePolicy == NULL)
    {
        free_octetstring (*pNextDestAddr);
        free_octetstring (*pNextNextHopAddr);
        free_oid (*pNextRoutePolicy);
        free_octetstring (*pCurrDestAddr);
        free_octetstring (*pCurrNextHopAddr);
        return OSIX_FAILURE;
    }
    (*pCurrDestAddr)->i4_Length = 0;
    (*pCurrRoutePolicy)->u4_Length = 0;
    (*pCurrNextHopAddr)->i4_Length = 0;
    (*pNextDestAddr)->i4_Length = 0;
    (*pNextNextHopAddr)->i4_Length = 0;
    (*pNextRoutePolicy)->u4_Length = 0;

    MEMSET ((*pCurrDestAddr)->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET ((*pCurrNextHopAddr)->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET ((*pCurrRoutePolicy)->pu4_OidList, 0,
            SNMP_MAX_OID_LENGTH * sizeof (UINT4));
    MEMSET ((*pNextDestAddr)->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET ((*pNextNextHopAddr)->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET ((*pNextRoutePolicy)->pu4_OidList, 0,
            SNMP_MAX_OID_LENGTH * sizeof (UINT4));

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpDeAllocateMemory                                   */
/*                                                                           */
/*     DESCRIPTION      : This function de-allocates memory using built-in
 *      *                  memory de-allocation functions.
 *      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : &pNextDestAddr, &pNextNextHopAdd, &pNextRoutePolicy
 *      *                 &pCurrDestAddr, &pCurrNextHopAddr, &pCurrRoutePolicy*/
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IpDeAllocateMemory (tSNMP_OCTET_STRING_TYPE * pNextDestAddr,
                    tSNMP_OCTET_STRING_TYPE * pNextNextHopAddr,
                    tSNMP_OID_TYPE * pNextRoutePolicy,
                    tSNMP_OCTET_STRING_TYPE * pCurrDestAddr,
                    tSNMP_OCTET_STRING_TYPE * pCurrNextHopAddr,
                    tSNMP_OID_TYPE * pCurrRoutePolicy)
{
    if (pNextDestAddr != NULL)
    {
        free_octetstring (pNextDestAddr);
    }
    if (pNextNextHopAddr != NULL)
    {
        free_octetstring (pNextNextHopAddr);
    }
    if (pNextRoutePolicy != NULL)
    {
        free_oid (pNextRoutePolicy);
    }
    if (pCurrDestAddr != NULL)
    {
        free_octetstring (pCurrDestAddr);
    }
    if (pCurrNextHopAddr != NULL)
    {
        free_octetstring (pCurrNextHopAddr);
    }
    if (pCurrRoutePolicy != NULL)
    {
        free_oid (pCurrRoutePolicy);
    }

}

/*****************************************************************************
 *
 * Function     : SnmpUtilEncodeOid 
 *
 * Description  : This function encode the OID list to ASN.1 formatted OID string
 *                In Management address TLV OID is stored as ASN.1 encoded
 *                format. During construction of Management Address TLV this
 *                function is called to encode the OID.
 *
 * Input        : pInOidStr - OID string
 *
 * Output       : pu1OutOidStr - ASN.1 encode OID String
 *                u1ErrorCode  - Error code to identify the reason for Encoding
 *                               failure.
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
SnmpUtilEncodeOid (tSNMP_OID_TYPE * pInOidStr, UINT1 *pu1OutOidStr,
                   UINT1 *u1ErrorCode)
{
    UINT4               u4Cnt = 0, u4Check = 0, u4Count = 0;
    INT4                i4ByteCount = 0;
    UINT4               u4Index = 0;
    UINT1               au1Buf[LLDP_MAX_LEN_MAN_OID] = { 0 };
    UINT4               u4Byte0 = 0, u4Byte1 = 0;
    UINT1               u1LenFlag = OSIX_FALSE;

    if (pInOidStr->u4_Length == 0)
    {
        return OSIX_FAILURE;
    }
    /* Preserve the byte 0 and byte 1 of the Input OID as we change these bytes
     * during encoding; These bytes have to be restored after the completion of
     * encoding */
    u4Byte0 = pInOidStr->pu4_OidList[0];
    u4Byte1 = pInOidStr->pu4_OidList[1];

    if ((INT4) u4Byte0 >= 0 && (INT4) u4Byte0 <= 2)
    {
        if (((INT4) u4Byte0 == 0) || ((INT4) u4Byte0 == 1))
        {
            if (!(((INT4) u4Byte1 >= 0) && ((INT4) u4Byte1 <= 39)))
            {
                *u1ErrorCode = 1;
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        *u1ErrorCode = 2;
        return OSIX_FAILURE;
    }

    if (pInOidStr->u4_Length == 1)
    {
        pInOidStr->u4_Length++;
        u1LenFlag = OSIX_TRUE;
    }
    pInOidStr->pu4_OidList[1] =
        (pInOidStr->pu4_OidList[0] * 40) + pInOidStr->pu4_OidList[1];

    /* We start encoding of the OID from the last sub-identifier;
     * Array Index starts from 0; So u4Cnt Initialised to "pInOidStr->u4Length - 1"
     * All the bytes should be encoded as per ASN.1 BER encoding; even if the first
     * subidentifier (i.e. oid_component1 * 40 + oid_component2) exceeds the value of
     * 127, then that also should be encoded as multi-byte string*/
    for (u4Cnt = pInOidStr->u4_Length - 1; u4Cnt >= 1; u4Cnt--)
    {
        au1Buf[u4Index++] = (UINT1) (pInOidStr->pu4_OidList[u4Cnt] & 0x7f);
        u4Count = 1L;
        u4Check = 0x80;
        while ((pInOidStr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5))
        {
            u4Check = (UINT4) (u4Check << 7);
            au1Buf[u4Index++] = (UINT1) (((pInOidStr->pu4_OidList[u4Cnt])
                                          >> (7 * u4Count)) | 0x80);
            u4Count++;

            if ((u4Index > LLDP_MAX_LEN_MAN_OID) ||
                ((u4Index == LLDP_MAX_LEN_MAN_OID) &&
                 (pInOidStr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5)))
            {
                *u1ErrorCode = 3;
                pInOidStr->pu4_OidList[0] = u4Byte0;
                pInOidStr->pu4_OidList[1] = u4Byte1;
                if (u1LenFlag == OSIX_TRUE)
                {
                    pInOidStr->u4_Length = 1;
                }
                return OSIX_FAILURE;
            }
        }
        if ((u4Index > LLDP_MAX_LEN_MAN_OID)
            || ((u4Index == LLDP_MAX_LEN_MAN_OID) && ((u4Cnt - 1) >= 1)))
        {
            *u1ErrorCode = 3;
            pInOidStr->pu4_OidList[0] = u4Byte0;
            pInOidStr->pu4_OidList[1] = u4Byte1;
            if (u1LenFlag == OSIX_TRUE)
            {
                pInOidStr->u4_Length = 1;
            }
            return OSIX_FAILURE;
        }
    }
    /* Fill the last byte as the number of bytes that has been encoded
     * It will be required to decode the packet accordingly */
    au1Buf[u4Index] = (UINT1) u4Index;

    /* The encoding was done in a reverse manner. So finally reversing
     * the entire bytes to make it proper */
    for (i4ByteCount = (INT4) u4Index; i4ByteCount >= 0; i4ByteCount--)
    {
        *(pu1OutOidStr) = au1Buf[i4ByteCount];
        pu1OutOidStr += 1;
    }

    /* Restoring the byte 0 and byte 1 to the original OID array */
    pInOidStr->pu4_OidList[0] = u4Byte0;
    pInOidStr->pu4_OidList[1] = u4Byte1;
    if (u1LenFlag == OSIX_TRUE)
    {
        pInOidStr->u4_Length = 1;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : SnmpUtilDecodeOid 
 *
 * Description  : This function decode the ASN.1 encoded oid string. OID string
 *                in the management address TLV are encoded in ASN.1 format.
 *                This function helps to decode that OID.
 *
 * Input        : pu1InOidStr - ASN.1 encode OID string
 *
 * Output       : pOutOidStr  - Decoded OID string
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
SnmpUtilDecodeOid (UINT1 *pu1InOidStr, tSNMP_OID_TYPE * pOutOidStr)
{
    INT4                i4Seq = 0;
    INT4                i4Len = 0;
    UINT4               u4Split = 0;

    i4Len = *pu1InOidStr++;
    pOutOidStr->u4_Length = 0;
    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] = 0;

    for (i4Seq = 0; i4Seq < i4Len; i4Seq++)
    {
        if (pOutOidStr->u4_Length < LLDP_MAX_LEN_MAN_OID)
        {
            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] =
                (pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] << 7) +
                (*pu1InOidStr & 0x7f);
            if ((*pu1InOidStr++ & 0x80) == 0)
            {
                if (pOutOidStr->u4_Length == 0)
                {
                    if (pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] >
                        (UINT4) 79)
                    {
                        /* If the first sub-id value is greater than 79, then the first
                         * OID component is 2. Because, while encoding there is one
                         * condition that, if the first OID component is 0 or 1, then
                         * the second component should be within the range of 0 to 39.
                         * So the First sub-id value cannot exceed 79 if the first OID
                         * component is 0 or 1*/
                        u4Split =
                            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length];
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] = 2;
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split - (2 * 40);
                    }
                    else
                    {
                        u4Split =
                            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length];
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split / 40;
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split % 40;
                    }
                }
                else
                {
                    pOutOidStr->u4_Length++;
                }
                if (i4Seq < i4Len - 1)
                {
                    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] = 0;
                }
            }
            else
            {
                if ((i4Seq + 1) >= i4Len)
                {
                    /* This check is required for the following scenario. Assume the
                     * OID length is wrongly sent; So there may be a possibility that
                     * the sub-identifier cannot be decoded correctly. In that case
                     * We have to decode the value as 0.
                     * Eg: OID Length = 1 & OID Encoded string is 0x81.
                     * Since the Most significant bit of 0x81 is 1, we can know that
                     * it is a multi-byte encoded string; but the length is mentioned
                     * as 1. Length should actually to be greater than 1. We cannot
                     * decode it correctly. So decoding this value to 0 */

                    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] = 0;
                }
            }
        }
        else
        {
            (*pu1InOidStr)++;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
*
* Function     : SnmpUtilKeyGen
*
* Description  : This function decode the ASN.1 encoded oid string. OID string
*                in the management address TLV are encoded in ASN.1 format.
*                This function helps to decode that OID.
*
* Input        : pu1InOidStr - ASN.1 encode OID string
*
* Output       : pOutOidStr  - Decoded OID string
*
* Returns      : OSIX_SUCCESS/OSIX_FAILURE
*
*****************************************************************************/
PUBLIC INT4
SnmpUtilKeyGen (UINT1 *pu1Passwd, INT4 i4PasswdLen, UINT4 u4Auth, UINT1 *pu1Key)
{
    UINT1              *pu1Copy, au1PasswdBuf[SNMPUTIL_MAX_PASSWD_LEN];
    UINT4               u4Count = 0, u4Seq = 0;
    UINT4               u4PasswdIndex = 0;
    unArCryptoHash      MD;
    unArCryptoHash      Sha;
    tUshaContext        UshaCtx;

    MEMSET (au1PasswdBuf, 0, SNMPUTIL_MAX_PASSWD_LEN);
#ifdef EXT_CRYPTO_WANTED
    ExtSnmpUtilKeyGen (pu1Passwd, i4PasswdLen, u4Auth, pu1Key);
    UNUSED_PARAM (UshaCtx);
    UNUSED_PARAM (Sha);
    UNUSED_PARAM (MD);
    UNUSED_PARAM (u4PasswdIndex);
    UNUSED_PARAM (au1PasswdBuf);
    UNUSED_PARAM (u4Seq);
    UNUSED_PARAM (u4Count);
    UNUSED_PARAM (pu1Copy);
#else
    switch (u4Auth)
    {
        case SNMP_MD5:
            MEMSET (&MD, 0, sizeof (unArCryptoHash));
            arMD5_start (&MD);
            while (u4Count < SNMPUTIL_PASSWD_TO_STRING_LEN)
            {
                pu1Copy = au1PasswdBuf;
                for (u4Seq = 0; u4Seq < SNMPUTIL_MAX_PASSWD_LEN; u4Seq++)
                {
                    *pu1Copy++ =
                        pu1Passwd[u4PasswdIndex++ % (UINT4) i4PasswdLen];
                }
                arMD5_update (&MD, au1PasswdBuf, SNMPUTIL_MAX_PASSWD_LEN);
                u4Count += SNMPUTIL_MAX_PASSWD_LEN;
            }
            arMD5_finish (&MD, pu1Key);
            break;
        case SNMP_SHA:
            MEMSET (&Sha, 0, sizeof (unArCryptoHash));
            Sha1ArStart (&Sha);
            while (u4Count < SNMPUTIL_PASSWD_TO_STRING_LEN)
            {
                pu1Copy = au1PasswdBuf;
                for (u4Seq = 0; u4Seq < SNMPUTIL_MAX_PASSWD_LEN; u4Seq++)
                {
                    *pu1Copy++ =
                        pu1Passwd[u4PasswdIndex++ % (UINT4) i4PasswdLen];
                }
                Sha1ArUpdate (&Sha, au1PasswdBuf, SNMPUTIL_MAX_PASSWD_LEN);
                u4Count += SNMPUTIL_MAX_PASSWD_LEN;
            }
            Sha1ArFinish (&Sha, pu1Key);
            break;
        case SNMP_SHA256:
        case SNMP_SHA384:
        case SNMP_SHA512:
            if (u4Auth == SNMP_SHA256)
                UshaReset (&UshaCtx, AR_SHA256_ALGO);
            else if (u4Auth == SNMP_SHA384)
                UshaReset (&UshaCtx, AR_SHA384_ALGO);
            else if (u4Auth == SNMP_SHA512)
                UshaReset (&UshaCtx, AR_SHA512_ALGO);

            while (u4Count < SNMPUTIL_PASSWD_TO_STRING_LEN)
            {
                pu1Copy = au1PasswdBuf;
                for (u4Seq = 0; u4Seq < SNMPUTIL_MAX_PASSWD_LEN; u4Seq++)
                {
                    *pu1Copy++ =
                        pu1Passwd[u4PasswdIndex++ % (UINT4) i4PasswdLen];
                }
                UshaInput (&UshaCtx, au1PasswdBuf, SNMPUTIL_MAX_PASSWD_LEN);
                u4Count += SNMPUTIL_MAX_PASSWD_LEN;
            }
            UshaResult (&UshaCtx, pu1Key);
            break;
        default:
            return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}
#endif
