/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *  
 * $Id: snmpon.c,v 1.1 2015/04/28 12:51:05 siva Exp $
 *
 * Description: This file contains main loop and RBTree entries
 ********************************************************************/

#include "lr.h"
#include "cli.h"
#include "snmp3cli.h"
#include "snmpon.h"
#include "snmpcmn.h"
#include "snmpdb.h"
#include "fssnmp.h"
#include "webnmutl.h"

tRBTree             gSnmpONOidTree;
tRBTree             gSnmpONNameTree;

VOID SnmpONMain     PROTO ((VOID));
INT4 SnmpONBuildNameTree PROTO ((VOID));
INT4 SnmpONBuildOidTree PROTO ((VOID));
INT4 SnmpONPrintNameTree PROTO ((VOID));
INT4 SnmpONPrintOidTree PROTO ((VOID));
INT4 SnmpONCreateRBTreeEntry PROTO ((VOID));

INT4 SnmpONRBTreeOidEntryCmp PROTO ((tRBElem * OidEntryNode,
                                     tRBElem * OidEntryIn));

INT4 SnmpONRBTreeNameEntryCmp PROTO ((tRBElem * NameEntryNode,
                                      tRBElem * NameEntryIn));

INT4 SnmpONRBTreeAddOidEntry PROTO ((tMIB_ON * pSnmpONEntry));

INT4 SnmpONRBTreeAddNameEntry PROTO ((tMIB_ON * pSnmpONEntry));

/************************************************************************************
 *  Function Name   : SnmpONMain
 *  Description     : Snmp main routine for building and printing the RBTree entries
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 *************************************************************************************/
VOID
SnmpONMain (VOID)
{
    SnmpONCreateRBTreeEntry ();
    SnmpONBuildNameTree ();
    SnmpONBuildOidTree ();

    return;
}

/**********************************************************************************
 * Function Name   : SnmpONBuildNameTree
 * Description     : Used to build the Name RBTree by adding the Name entries
 * Input           : None
 * Output          : None
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ***********************************************************************************/
INT4
SnmpONBuildNameTree ()
{
    INT4                i4Index = 0;
    tMIB_ON            *pSnmpONEntry = NULL;

    pSnmpONEntry = &MibONTable[i4Index];

    while (pSnmpONEntry->pName != NULL)
    {
        SnmpONRBTreeAddNameEntry (pSnmpONEntry);
        i4Index++;
        pSnmpONEntry = &MibONTable[i4Index];
    }

    return SNMP_SUCCESS;
}

/**********************************************************************************
 * Function Name   : SnmpONBuildOidTree
 * Description     : Used to build the OID RBTree by adding the OID entries
 * Input           : None
 * Output          : None
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ************************************************************************************/
INT4
SnmpONBuildOidTree ()
{
    INT4                i4Index = 0;
    tMIB_ON            *pSnmpONEntry = NULL;

    pSnmpONEntry = &MibONTable[i4Index];

    while (pSnmpONEntry->pNumber != NULL)
    {
        SnmpONRBTreeAddOidEntry (pSnmpONEntry);
        i4Index++;
        pSnmpONEntry = &MibONTable[i4Index];
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONPrintNameTree
 * Description     : Used to get the name entries from the RBNode and print it
 * Input           : None
 * Output          : Prints the name entries
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ******************************************************************************/
INT4
SnmpONPrintNameTree ()
{
    tRBElem            *pRBElemNext = NULL;
    tMIB_ON            *pSnmpONEntry = NULL;

    pSnmpONEntry = (tMIB_ON *) RBTreeGetFirst (gSnmpONNameTree);

    while (pSnmpONEntry != NULL)
    {
        pRBElemNext = RBTreeGetNext (gSnmpONNameTree,
                                     (tRBElem *) pSnmpONEntry, NULL);
        printf ("\r\n Name : %s\t", pSnmpONEntry->pName);
        pSnmpONEntry = (tMIB_ON *) pRBElemNext;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONPrintOidTree
 * Description     : Used to get the oid entries from the RBNode and print it
 * Input           : None
 * Output          : Prints the oid entries
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 *******************************************************************************/
INT4
SnmpONPrintOidTree ()
{
    tRBElem            *pRBElemNext = NULL;
    tMIB_ON            *pSnmpONEntry = NULL;

    pSnmpONEntry = (tMIB_ON *) RBTreeGetFirst (gSnmpONOidTree);

    while (pSnmpONEntry != NULL)
    {
        pRBElemNext = RBTreeGetNext (gSnmpONOidTree,
                                     (tRBElem *) pSnmpONEntry, NULL);
        printf ("Oid : %s\t\n", pSnmpONEntry->pNumber);
        pSnmpONEntry = (tMIB_ON *) pRBElemNext;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONRBTreeOidEntryCmp
 * Description     : Used to compare the oid entries
 * Input(s)        : OidEntryNode
 *                      OidEntryIn
 * Output          : None
 * Returns         : i4Result
 ********************************************************************************/
INT4
SnmpONRBTreeOidEntryCmp (tRBElem * OidEntryNode, tRBElem * OidEntryIn)
{
    INT4                i4Result = 0;
    tMIB_ON            *pOidEntryNode = NULL;
    tMIB_ON            *pOidEntryIn = NULL;

    pOidEntryNode = (tMIB_ON *) OidEntryNode;
    pOidEntryIn = (tMIB_ON *) OidEntryIn;

    i4Result = STRCMP (pOidEntryNode->pNumber, pOidEntryIn->pNumber);
    return i4Result;
}

/*****************************************************************************
 * Function Name   : SnmpONRBTreeNameEntryCmp
 * Description     : Used to compare the name entries
 * Input(s)        : NameEntryNode
 *                   NameEntryIn
 * Output          : None
 * Returns         : i4Result
 ********************************************************************************/
INT4
SnmpONRBTreeNameEntryCmp (tRBElem * NameEntryNode, tRBElem * NameEntryIn)
{
    INT4                i4Result = 0;
    tMIB_ON            *pNameEntryNode = NULL;
    tMIB_ON            *pNameEntryIn = NULL;

    pNameEntryNode = (tMIB_ON *) NameEntryNode;
    pNameEntryIn = (tMIB_ON *) NameEntryIn;

    i4Result = STRCMP (pNameEntryNode->pName, pNameEntryIn->pName);
    return i4Result;
}

/*****************************************************************************
 * Function Name   : SnmpONRBTreeAddOidEntry
 * Description     : Used to add the oid entries into the RBTree
 * Input(s)        : pSnmpONEntry - Pointer to the entry
 * Output          : None
 * Returns         : SNMP_SUCCESS/RB_FAILURE
 ********************************************************************************/
INT4
SnmpONRBTreeAddOidEntry (tMIB_ON * pSnmpONEntry)
{
    if (RBTreeAdd (gSnmpONOidTree, pSnmpONEntry) == RB_FAILURE)
    {
        return RB_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONRBTreeAddNameEntry
 * Description     : Used to add the name entries into the RBTree
 * Input(s)        : pSnmpONEntry - pointer to the entry
 * Output          : None
 * Returns         : SNMP_SUCCESS/RB_FAILURE
 *******************************************************************************/
INT4
SnmpONRBTreeAddNameEntry (tMIB_ON * pSnmpONEntry)
{
    if (RBTreeAdd (gSnmpONNameTree, pSnmpONEntry) == RB_FAILURE)
    {
        RBTreeRem (gSnmpONOidTree, pSnmpONEntry);
        return RB_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONCreateRBTreeEntry
 * Description     : Used to create RBTree
 * Input(s)        : NONE
 * Output          : NONE
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 *******************************************************************************/
INT4
SnmpONCreateRBTreeEntry ()
{
    gSnmpONOidTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMIB_ON, OidRbNode),
                              SnmpONRBTreeOidEntryCmp);

    if (gSnmpONOidTree == NULL)
    {
        return SNMP_FAILURE;
    }
    gSnmpONNameTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMIB_ON, NameRbNode),
                              SnmpONRBTreeNameEntryCmp);
    if (gSnmpONNameTree == NULL)
    {
        RBTreeDestroy (gSnmpONOidTree, NULL, 0);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : SnmpONGetOidFromName
 * Description     : Used to convert the name to the corresponding OID
 * Input(s)        : pName
 * Output          : pNumberIdx - OID
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ******************************************************************************/
INT4
SnmpONGetOidFromName (char *pName, char *pNumberIdx)
{
    tMIB_ON             MibONName;
    tMIB_ON            *pSnmpONEntry = NULL;
    UINT4               u4OidLen = 0;
    const char         *pIndex = NULL;
    char               *pONName = NULL;
    char                aTemp[256];
    char                aName[256];
    char                aNumber[256];
    INT1                i1Index = 0;

    MEMSET (aTemp, 0, 256);
    MEMSET (aName, 0, 256);
    MEMSET (aNumber, 0, 256);

    if (pName == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (aTemp, pName, STRLEN (pName));
    pIndex = strstr (pName, ".");
    while (aTemp[i1Index] != '\0')
    {
        if (aTemp[i1Index] == '.')
        {
            break;
        }
        else
        {
            aName[i1Index] = aTemp[i1Index];
            i1Index++;
        }
    }
    pONName = aName;
    MibONName.pName = pONName;
    pSnmpONEntry = (tMIB_ON *) RBTreeGet (gSnmpONNameTree,
                                          (tRBElem *) & MibONName);

    if (pSnmpONEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    u4OidLen = STRLEN (pSnmpONEntry->pNumber);
    MEMCPY (aNumber, pSnmpONEntry->pNumber, u4OidLen);
    aNumber[u4OidLen] = '\0';

    STRNCPY (pNumberIdx, aNumber, STRLEN(aNumber));
    pNumberIdx[STRLEN(aNumber)] = '\0';

    if (pIndex != NULL)
    {
        STRCAT (pNumberIdx, pIndex);
    }

    return SNMP_SUCCESS;
}

/********************************************************************************
 * Function Name   : SnmpONGetNameFromOid
 * Description     : Used to convert the OID to the corresponding object name
 * Input(s)        : pNumber
 * Output          : pNameIdx - Mib Object name
 * Returns         : SNMP_SUCCESS/SNMP_FAILURE
 *******************************************************************************/
INT4
SnmpONGetNameFromOid (char *pNumber, char *pNameIdx)
{
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tMIB_ON             MibONOid;
    tMIB_ON            *pSnmpONEntry = NULL;
    char                aName[256];
    UINT1               au1Temp[24];
    UINT4               u4Len = 0;
    UINT4               u4Index = 0;
    UINT4               u4OidLen = 0;
    tSNMP_OID_TYPE     *pOID = NULL;

    pOID = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOID == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&MibONOid, 0, sizeof (tMIB_ON));
    MEMSET (aName, 0, sizeof (aName));

    WebnmConvertStringToOid (pOID, (UINT1 *) pNumber, 1);
    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur == NULL)
    {
        MibONOid.pNumber = pNumber;

        pSnmpONEntry = (tMIB_ON *) RBTreeGet (gSnmpONOidTree,
                                              (tRBElem *) & MibONOid);

        if (pSnmpONEntry == NULL)
        {
            free_oid (pOID);
            return SNMP_FAILURE;
        }

        u4OidLen = STRLEN (pSnmpONEntry->pName);
        MEMCPY (aName, pSnmpONEntry->pName, u4OidLen);
        aName[u4OidLen] = '\0';
        STRNCPY (pNameIdx, aName, STRLEN(aName));
	pNameIdx[STRLEN(aName)] = '\0';
        free_oid (pOID);
        return SNMP_SUCCESS;

    }
    MibONOid.pNumber = pNumber;
    WebnmConvertOidToString (&(SnmpDbEntry.pMibEntry->ObjectID),
                             (UINT1 *) MibONOid.pNumber);

    pSnmpONEntry = (tMIB_ON *) RBTreeGet (gSnmpONOidTree,
                                          (tRBElem *) & MibONOid);

    if (pSnmpONEntry == NULL)
    {
        free_oid (pOID);
        return SNMP_FAILURE;
    }

    u4OidLen = STRLEN (pSnmpONEntry->pName);
    MEMCPY (aName, pSnmpONEntry->pName, u4OidLen);
    aName[u4OidLen] = '\0';
    STRNCPY (pNameIdx, aName, STRLEN(aName));
    pNameIdx[STRLEN(aName)] = '\0';

    for (u4Index = SnmpDbEntry.pMibEntry->ObjectID.u4_Length;
         u4Index < pOID->u4_Length; u4Index++)
    {
        SPRINTF ((char *) au1Temp, ".%u", pOID->pu4_OidList[u4Index]);
        STRCAT (pNameIdx, (const char *) au1Temp);
    }

    free_oid (pOID);
    return SNMP_SUCCESS;
}
