/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: arMD5_ext.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the externs for ARMD5
 * module
 *********************************************************************/
#ifndef _ARMD5_EXT_H
#define _ARMD5_EXT_H
/********************************************************************/

extern const tArHashfunction gArMd5Hashfunction;

#endif
