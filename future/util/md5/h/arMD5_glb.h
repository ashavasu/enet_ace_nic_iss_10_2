/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: arMD5_glb.h,v 1.1 2015/04/28 12:53:04 siva Exp $
 *
 * Description: This file contains the global defines required for SHA1
 * module
 *********************************************************************/
#ifndef _ARMD5_GLB_H
#define _ARMD5_GLB_H
/********************************************************************/
#include "arMD5_inc.h"
#include "arMD5_api.h"

const tArHashfunction gArMd5Hashfunction = {
        16, 
        arMD5_start, 
        arMD5_update, 
    arMD5_finish        
};

#endif
