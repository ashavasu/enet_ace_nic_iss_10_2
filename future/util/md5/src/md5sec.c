/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: md5sec.c,v 1.13 2015/04/28 12:51:04 siva Exp $
 *
 * Description:This file contains the routines for
 *             initialization of Security Association
 *             table, lookup of the Security Association
 *             table for inbound and outbound datagrams,
 *             and calculation of authentication data.
 *
 *******************************************************************/
#ifdef  PACK_REQUIRED
#pragma pack  (1)
#endif

#define _SEC_

#include "arMD5_inc.h"
#include "arMD5_api.h"

/***************************************************************************
 * DESCRIPTION :  This routine will calculate the authentication digest if 
 *                the algorithm supported is Keyed MD5.
 *
 * INPUTS      :  IP datagram pointer           (tCRU_BUF_CHAIN_HEADER *p_buf)
 *                Algorithm Key pointer         (UINT1 *p_key)
 *                Key Length                    (UINT2 u2_key_len)
 *
 * OUTPUTS     :  Authentication Digest pointer (UINT1 *p_auth_digest)
 *
 * RETURNS     :  none
 *
 * NOTES       :  
 *
 ***************************************************************************/

#ifdef __STDC__
PUBLIC VOID
md5_get_keyed_digest (tCRU_BUF_CHAIN_HEADER * p_buf, UINT1 *p_key,
                      UINT2 u2_key_len, UINT1 *p_auth_digest)
#else
PUBLIC VOID
md5_get_keyed_digest (p_buf, p_key, u2_key_len, p_auth_digest)
     tCRU_BUF_CHAIN_HEADER *p_buf;
     UINT1              *p_key;
     UINT2               u2_key_len;
     UINT1              *p_auth_digest;
#endif
{
    tCRU_BUF_DATA_DESC *pTmpDataDesc;
    unArCryptoHash      context;

    p_key = p_key;
    u2_key_len = u2_key_len;

#ifdef IPSEC_DEBUG
    if (i4_ipsec_trace_flag & IPSEC_TRACE_SEC)
        ipsec_print
            ("Keyed_md5 : Key = %s Key Len = %d Bufptr = %p "
             "Digest Buffer = %p\n", p_key, u2_key_len, p_buf, p_auth_digest);
#endif /* IPSEC_DEBUG */

    arMD5_start (&context);

    for (pTmpDataDesc = CRU_BUF_GetFirstDataDesc (p_buf);
         pTmpDataDesc != NULL;
         pTmpDataDesc = CRU_BUF_GetNextDataDesc (pTmpDataDesc))
    {
        arMD5_update (&context, (UINT1 *) CRU_BUF_GetDataPtr (pTmpDataDesc),
                      CRU_BUF_GetBlockValidByteCount (pTmpDataDesc));

    }
    arMD5_finish (&context, p_auth_digest);

    return;

}

#ifdef __STDC__
PUBLIC VOID
Md5GetKeyDigest (UINT1 *pu1Buf, UINT2 u2MsgLen, UINT1 *p_auth_digest)
#else
PUBLIC VOID
Md5GetKeyDigest (pu1Buf, u2MsgLen, p_auth_digest)
     UINT1              *pu1Buf;
     UINT2               u2MsgLen;
     UINT1              *p_auth_digest;
#endif
{
    unArCryptoHash      context;

#ifdef IPSEC_DEBUG
    if (i4_ipsec_trace_flag & IPSEC_TRACE_SEC)
        ipsec_print
            ("Keyed_md5 : Key = %s Key Len = %d Bufptr = %p "
             "Digest Buffer = %p\n", p_key, u2_key_len, p_buf, p_auth_digest);
#endif /* IPSEC_DEBUG */

    arMD5_start (&context);

    arMD5_update (&context, (UINT1 *) pu1Buf, u2MsgLen);
    arMD5_finish (&context, p_auth_digest);

    return;

}

/************************************************************************/
/*  Function Name   :Md5Algo                                            */
/*  Description     :This Function is to compute the digest             */
/*                  :                                                   */
/*  Input(s)        :pu1Buf :The Data to be authenticated               */
/*                  :i4DataLen :Length of the Data                      */
/*                  :pu1Digest :Digest to be computed                   */
/*                  :                                                   */
/*  Output(s)       :pu1Digest:Digest value                             */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/

VOID
Md5Algo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unArCryptoHash      context;

    arMD5_start (&context);
    arMD5_update (&context, pu1Buf, (UINT4) i4BufLen);
    arMD5_finish (&context, pu1Digest);
    return;
}

/************************************************************************/
/*  Function Name   :KeyedMd5                                           */
/*  Description     :This Function is to compute the digest             */
/*                  :                                                   */
/*  Input(s)        :pu1Buf :The Data to be authenticated               */
/*                  :pu1Key :Pointer to au1Key                          */
/*                  :KeyLen :Length of the au1Key                       */
/*                  :i4DataLen :Length of the Data                      */
/*                  :                                                   */
/*  Output(s)       :pu1Digest:Digest value                             */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/

VOID
KeyedMd5 (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Key, INT4 i4KeyLen,
          UINT1 *pu1Digest)
{
    unArCryptoHash      context;

    arMD5_start (&context);
    arMD5_update (&context, pu1Key, (UINT4) i4KeyLen);
    arMD5_finish (&context, pu1Digest);

    arMD5_update (&context, pu1Buf, (UINT4) i4BufLen);
    arMD5_update (&context, pu1Key, (UINT4) i4KeyLen);
    arMD5_finish (&context, pu1Digest);

    return;

}

/***************************** END OF FILE **********************************/
