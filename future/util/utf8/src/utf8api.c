/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: utf8api.c,v 1.1 2015/04/28 12:53:48 siva Exp $
 *
 * Description: This file contains the API code for UTF8 module
 *********************************************************************/
#include "osxstd.h"
#include "utlmacro.h"
#include "lr.h"
#include "utf8.h"
/*******************************************************************************
 * Function           : UnicodeToUtf8
 *
 * Description        : This function is used to encode Unicode charaters
 *
 * Input(s)           : pu4Unicode - Address of Unicode characters array
 *                      u4Utf8Length - Length of the Utf8 array
 *                      u4UniLength - Length of the Unicode array
 *
 * Output(s)          : pu1Utf8 - Address of Utf characters array
 *
 * Returns            : UTF8_SUCCESS/UTF8_FAILURE
 ******************************************************************************/
PUBLIC UINT1
UnicodeToUtf8 (UINT4 *pu4Unicode, UINT4 u4UniLength,
               UINT1 *pu1Utf8, UINT4 u4Utf8Length)
{
    UINT4               u4LoopIndex = 0, u4ArrayIndex = 0;

    if ((pu4Unicode == NULL) || (pu1Utf8 == NULL))
    {
#ifdef UTF8_DEBUG_LOG
        UTF8_DEBUG_PRINT ("\n NULL Pointers passed to UnicodeToUtf8\n");
#endif
        return UTF8_FAILURE;
    }

    /* Encode the Unicode characters into UTF-8 */
    for (u4LoopIndex = 0;
         ((u4LoopIndex < u4UniLength) && (u4ArrayIndex < u4Utf8Length));
         u4LoopIndex++)
    {
        /* 0xxxxxxx */
        if (pu4Unicode[u4LoopIndex] < 0x80)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) pu4Unicode[u4LoopIndex];
            u4ArrayIndex++;
        }
        /* 110xxxxx 10xxxxxx */
        else if (pu4Unicode[u4LoopIndex] < 0x800)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASK2BYTES |
                                             (pu4Unicode[u4LoopIndex] >> 6));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             (pu4Unicode[u4LoopIndex] &
                                              MASKBITS));
            u4ArrayIndex++;
        }
        /* 1110xxxx 10xxxxxx 10xxxxxx */
        else if (pu4Unicode[u4LoopIndex] < 0x10000)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASK3BYTES |
                                             (pu4Unicode[u4LoopIndex] >> 12));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 6) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             (pu4Unicode[u4LoopIndex] &
                                              MASKBITS));
            u4ArrayIndex++;
        }
        /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
        else if (pu4Unicode[u4LoopIndex] < 0x200000)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASK4BYTES |
                                             (pu4Unicode[u4LoopIndex] >> 18));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 12) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 6) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             (pu4Unicode[u4LoopIndex] &
                                              MASKBITS));
            u4ArrayIndex++;
        }
        /* 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
        else if (pu4Unicode[u4LoopIndex] < 0x4000000)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASK5BYTES |
                                             (pu4Unicode[u4LoopIndex] >> 24));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 18) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 12) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 6) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             (pu4Unicode[u4LoopIndex] &
                                              MASKBITS));
            u4ArrayIndex++;
        }
        /* 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
        else if (pu4Unicode[u4LoopIndex] < 0x8000000)
        {
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASK6BYTES |
                                             (pu4Unicode[u4LoopIndex] >> 30));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 24) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 18) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 12) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             ((pu4Unicode[u4LoopIndex] >> 6) &
                                              MASKBITS));
            u4ArrayIndex++;
            pu1Utf8[u4ArrayIndex] = (UINT1) (MASKBYTE |
                                             (pu4Unicode[u4LoopIndex] &
                                              MASKBITS));
            u4ArrayIndex++;
        }
        else
        {
#ifdef UTF8_DEBUG_LOG
            UTF8_DEBUG_PRINT ("\n Invalid Range of character found in \
pu4Unicode unicode character\n");
#endif
            return UTF8_FAILURE;
        }
    }
    pu1Utf8[u4ArrayIndex] = '\0';
    return UTF8_SUCCESS;
}

/*******************************************************************************
 * Function        : Utf8ToUnicode
 * 
 * Description     : This function is used to decode UTF8 charaters.  
 * 
 * Input(s)        : pu1Utf8 - Address of Utf characters array
 *                   u4Utf8Length - Length of the Utf8 array 
 *                   u4UniLength - Length of the Unicode array
 * 
 * Output(s)       : pu4Unicode - Address of Unicode characters array
 * 
 * Returns         : UTF8_SUCCESS/UTF8_FAILURE
 *******************************************************************************/
PUBLIC UINT1
Utf8ToUnicode (UINT1 *pu1Utf8, UINT4 u4Utf8Length,
               UINT4 *pu4Unicode, UINT4 u4UniLength)
{
    UINT4               u4LoopIndex = 0, u4ArrayIndex = 0, u4TempVar = 0;

    if ((pu4Unicode == NULL) || (pu1Utf8 == NULL))
    {
#ifdef UTF8_DEBUG_LOG
        UTF8_DEBUG_PRINT ("\n NULL Pointers passed to Utf8ToUnicode\n");
#endif
        return UTF8_FAILURE;
    }

    /* Decode the UTF-8 encoded characters to equivalent Unicode characters */
    for (u4LoopIndex = 0;
         ((u4LoopIndex < u4Utf8Length) && (u4ArrayIndex < u4UniLength));)
    {
        /* 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
        if ((pu1Utf8[u4LoopIndex] & MASK6BYTES) == MASK6BYTES)
        {
            u4TempVar = ((UINT4) (pu1Utf8[u4LoopIndex] & 0x01) << 30) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 1] & MASKBITS) << 24) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 2] & MASKBITS) << 18) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 3] & MASKBITS) << 12) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 4] & MASKBITS) << 6) |
                (pu1Utf8[u4LoopIndex + 5] & MASKBITS);
            u4LoopIndex += 6;
        }
        /* 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx */
        else if ((pu1Utf8[u4LoopIndex] & MASK5BYTES) == MASK5BYTES)
        {
            u4TempVar = ((UINT4) (pu1Utf8[u4LoopIndex] & 0x03) << 24) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 1] & MASKBITS) << 18) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 2] & MASKBITS) << 12) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 3] & MASKBITS) << 6) |
                (pu1Utf8[u4LoopIndex + 4] & MASKBITS);
            u4LoopIndex += 5;
        }
        /* 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx */
        else if ((pu1Utf8[u4LoopIndex] & MASK4BYTES) == MASK4BYTES)
        {
            u4TempVar = ((UINT4) (pu1Utf8[u4LoopIndex] & 0x07) << 18) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 1] & MASKBITS) << 12) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 2] & MASKBITS) << 6) |
                (pu1Utf8[u4LoopIndex + 3] & MASKBITS);
            u4LoopIndex += 4;
        }
        /* 1110xxxx 10xxxxxx 10xxxxxx */
        else if ((pu1Utf8[u4LoopIndex] & MASK3BYTES) == MASK3BYTES)
        {
            u4TempVar = ((UINT4) (pu1Utf8[u4LoopIndex] & 0x0F) << 12) |
                ((UINT4) (pu1Utf8[u4LoopIndex + 1] & MASKBITS) << 6) |
                (pu1Utf8[u4LoopIndex + 2] & MASKBITS);
            u4LoopIndex += 3;
        }
        /* 110xxxxx 10xxxxxx */
        else if ((pu1Utf8[u4LoopIndex] & MASK2BYTES) == MASK2BYTES)
        {
            u4TempVar = (UINT4) ((pu1Utf8[u4LoopIndex] & 0x1F) << 6) |
                (pu1Utf8[u4LoopIndex + 1] & MASKBITS);
            u4LoopIndex += 2;
        }
        /* 0xxxxxxx */
        else if ((pu1Utf8[u4LoopIndex] & MASKBYTE) != MASKBYTE)
        {
            u4TempVar = pu1Utf8[u4LoopIndex];
            u4LoopIndex += 1;
        }
        else
        {
#ifdef UTF8_DEBUG_LOG
            UTF8_DEBUG_PRINT ("\n Invalid UTF-8 character Received\n");
#endif
            return UTF8_FAILURE;
        }

        pu4Unicode[u4ArrayIndex] = u4TempVar;
        u4ArrayIndex++;
    }
    pu4Unicode[u4ArrayIndex] = L'\0';
    return UTF8_SUCCESS;
}

/****************************************************************************
              End of File utf8api.c
****************************************************************************/
