######################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                #
# ------------------------------------------                          #
# $Id: make.h,v 1.1 2015/04/28 12:53:05 siva Exp $                     #
#    DESCRIPTION            : Specifies the options and modules to be #
#                             included for building the UTF8 module    #
#######################################################################
include ../../LR/make.h
include ../../LR/make.rule

UTF8_FINAL_COMPILATION_SWITCHES =${GENERAL_COMPILATION_SWITCHES} \
                ${SYSTEM_COMPILATION_SWITCHES} -UTRACE_WANTED -UUTF8_DEBUG_LOG

UTF8_BASE_DIR = ${BASE_DIR}/util/utf8
UTF8_SRC_DIR = $(UTF8_BASE_DIR)/src
UTF8_INC_DIR = $(BASE_DIR)/inc
UTF8_OBJ_DIR = $(UTF8_BASE_DIR)/obj
UTF8_INCLUDES = -I${COMMON_INCLUDE_DIRS} -I${UTF8_INC_DIR}
