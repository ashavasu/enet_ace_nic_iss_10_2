/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: digmd5.c,v 1.1
 *
 * Description: Utilities to perform DIGEST MD5 authentication
 *******************************************************************/

#include "digmd5.h"
#include <stdlib.h>

PRIVATE UINT1       gau1TmpStr[UTL_RESPONSE_MSG];
/***************************************************************************
 * DESCRIPTION :  This routine will parse the digest challenge string and 
 *                 update the values in the given DigestMd5Client structure.
 *
 * INPUTS      :  pu1SrvString - Digest challenge string.
 *                u4SrvStrLen - Challenge string length.
 *
 * OUTPUTS     :  pDigestMd5Client - Updated DigestMd5Client structure.
 *
 * RETURNS     :  UTL_AUTH_SUCCESS/UTL_AUTH_FAILURE/UTL_AUTH_ERROR.
 ***************************************************************************/
INT4
DigestMd5ParseSrvChallenge (tDigestMd5Client * pDigestMd5Client,
                            UINT1 *pu1SrvString, UINT4 u4SrvStrLen)
{
    tDigestMd5Parser    digMd5Parser;
    UINT1              *pu1Key = NULL;
    UINT1              *pu1Value = NULL;
    UINT1               au1HexStr[MD5_DIGEST_STRING_LEN];
    UINT1              *pu1Parse = NULL;
    UINT4               u4KeyLen;
    UINT4               u4ValLen;
    INT4                i4Flag = 0;
    INT4                i4RetVal = 0;

    if ((DigestMd5ParserUpdate (&digMd5Parser, pu1SrvString, u4SrvStrLen))
        != UTL_AUTH_SUCCESS)
    {
        return UTL_AUTH_FAILURE;
    }

    pDigestMd5Client->i4Flags = 0;
    pDigestMd5Client->u4MaxBufLen = UTL_RESPONSE_MSG;
    while ((i4RetVal = DigestMd5ParserProcess (&digMd5Parser, &pu1Key,
                                               &u4KeyLen, &pu1Value, &u4ValLen))
           == UTL_AUTH_SUCCESS)
    {
        if (pu1Value == NULL)
        {
            return UTL_AUTH_FAILURE;
        }

        if ((u4KeyLen == STRLEN ("realm"))
            && (MEMCMP (pu1Key, "realm", u4KeyLen) == 0))
        {
            if (u4ValLen > 0)
            {
                MEMCPY (pDigestMd5Client->au1Realm, pu1Value, u4ValLen);
            }
        }
        else if ((u4KeyLen == STRLEN ("nonce"))
                 && (MEMCMP (pu1Key, "nonce", u4KeyLen) == 0))
        {
            if (((i4Flag & DIG_MD5_NONCE) != 0) || (u4ValLen == 0))
            {
                return UTL_AUTH_ERROR;
            }
            UtlAuthStrCpy (pu1Value, u4ValLen, pDigestMd5Client->au1Nonce,
                           &pDigestMd5Client->u4NonceLen);
            if (pDigestMd5Client->u4NonceLen == 0)
            {
                return UTL_AUTH_FAILURE;
            }
            i4Flag |= DIG_MD5_NONCE;
        }
        else if ((u4KeyLen == STRLEN ("qop"))
                 && (MEMCMP (pu1Key, "qop", u4KeyLen) == 0))
        {
            if (u4ValLen == 0)
            {
                return UTL_AUTH_ERROR;
            }
            i4Flag |= DIG_MD5_AUTH;
            pu1Parse = (UINT1 *) STRSTR (pu1Value, "auth");
            if ((pu1Parse != NULL)
                && (MEMCMP (pu1Parse, "auth", STRLEN ("auth")) == 0))
            {
                pDigestMd5Client->i4Flags |= DIG_MD5_AUTH;
            }
            else
            {
                pu1Parse = (UINT1 *) STRSTR (pu1Value, "auth-int");
                if ((pu1Parse != NULL)
                    && (MEMCMP (pu1Parse, "auth-int",
                                STRLEN ("auth-int")) == 0))
                {
                    pDigestMd5Client->i4Flags |= DIG_MD5_AUTH_INT;
                }
                else
                {
                    pu1Parse = (UINT1 *) STRSTR (pu1Value, "auth-conf");
                    if ((pu1Parse != NULL)
                        && (MEMCMP (pu1Parse, "auth-conf",
                                    STRLEN ("auth-conf")) == 0))
                    {
                        pDigestMd5Client->i4Flags |= DIG_MD5_AUTH_CONF;
                    }
                    else
                    {
                        pDigestMd5Client->i4Flags |= DIG_MD5_AUTH;
                    }
                }
            }
        }
        else if ((u4KeyLen == STRLEN ("charset"))
                 && (MEMCMP (pu1Key, "charset", u4KeyLen) == 0))
        {
            if (((pDigestMd5Client->i4Flags & DIG_MD5_UTF8) != 0)
                || (u4ValLen != STRLEN ("utf-8"))
                || (MEMCMP (pu1Value, "utf-8", u4ValLen) != 0))
            {
                return UTL_AUTH_ERROR;
            }
            pDigestMd5Client->i4Flags |= DIG_MD5_UTF8;
        }
        else if ((u4KeyLen == STRLEN ("algorithm"))
                 && (MEMCMP (pu1Key, "algorithm", u4KeyLen) == 0))
        {
            if (((i4Flag & DIG_MD5_ALG) != 0)
                || (u4ValLen != STRLEN ("md5-sess"))
                || (MEMCMP (pu1Value, "md5-sess", u4ValLen) != 0))
            {
                return UTL_AUTH_ERROR;
            }
            i4Flag |= DIG_MD5_ALG;
        }
        else if ((u4KeyLen == STRLEN ("stale"))
                 && (MEMCMP (pu1Key, "stale", u4KeyLen) == 0))
        {
            if (((pDigestMd5Client->i4Flags & DIG_MD5_STALE) != 0)
                || (u4ValLen != STRLEN ("true"))
                || (MEMCMP (pu1Value, "true", u4ValLen) != 0))
            {
                return UTL_AUTH_ERROR;
            }
            pDigestMd5Client->i4Flags |= DIG_MD5_STALE;
        }
        else if ((u4KeyLen == STRLEN ("maxbuf"))
                 && (MEMCMP (pu1Key, "maxbuf", u4KeyLen) == 0))
        {
            if ((i4Flag & DIG_MD5_MBUF) != 0)
            {
                return UTL_AUTH_ERROR;
            }
            pDigestMd5Client->u4MaxBufLen = (UINT4) ATOI (pu1Value);
            i4Flag |= DIG_MD5_MBUF;
        }
        else if ((u4KeyLen == STRLEN ("cipher"))
                 && (MEMCMP (pu1Key, "cipher", u4KeyLen) == 0))
        {
            if (u4ValLen == 0)
            {
                return UTL_AUTH_ERROR;
            }
            if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_CONF) == 0)
            {
                /*
                   return UTL_AUTH_ERROR;
                 */
                continue;
            }

            pu1Parse = (UINT1 *) STRSTR (pu1Value, "3des");
            if ((pu1Parse != NULL)
                && (MEMCMP (pu1Parse, "3des", STRLEN ("3des")) == 0))
            {
                pDigestMd5Client->i4Flags |= DIG_MD5_CIP_3DES;
            }
            else
            {
                pu1Parse = (UINT1 *) STRSTR (pu1Value, "des");
                if ((pu1Parse != NULL)
                    && (MEMCMP (pu1Parse, "des", STRLEN ("des")) == 0))
                {
                    pDigestMd5Client->i4Flags |= DIG_MD5_CIP_DES;
                }
            }
        }
        else if ((u4KeyLen == STRLEN ("rspauth"))
                 && (MEMCMP (pu1Key, "rspauth", u4KeyLen) == 0)
                 && (u4ValLen == MD5_DIGEST_STRING_LEN)
                 && (*pDigestMd5Client->au1CNonce != '\0'))
        {
            UtlDigMd5Auth (pDigestMd5Client, 1, au1HexStr);
            if (MEMCMP (au1HexStr, pu1Value, u4ValLen) == 0)
            {
                return UTL_AUTH_SUCCESS;
            }
            else
            {
                return UTL_AUTH_ERROR;
            }
        }
    }

    if ((i4RetVal != UTL_AUTH_EOL)
        || ((i4Flag & DIG_MD5_NONCE) == 0) || ((i4Flag & DIG_MD5_ALG) == 0))
    {
        return UTL_AUTH_ERROR;
    }

    /* qop is optional, if not present take the default value as auth */
    if ((i4Flag & DIG_MD5_AUTH) == 0)
    {
        pDigestMd5Client->i4Flags |= DIG_MD5_AUTH;
    }

    return UTL_AUTH_SUCCESS;
}

/***************************************************************************
 * DESCRIPTION :  This routine will formulate the digest response string     
 *                 based on the values present the digest challenge string.
 *
 * INPUTS      :  pDigestMd5Client - DigestMd5Client structure with challenge
 *                                   information
 *
 * OUTPUTS     :  pDigestMd5Client - Updated DigestMd5Client structure with
 *                                   response message.
 *
 * RETURNS     :  UTL_AUTH_SUCCESS/UTL_AUTH_FAILURE.
 ***************************************************************************/
INT4
DigestMd5ClientResponse (tDigestMd5Client * pDigestMd5Client)
{
    UINT1               au1HexStr[MD5_DIGEST_STRING_LEN + 4];

    MEMSET (au1HexStr, 0, MD5_DIGEST_STRING_LEN + 4);
    if (pDigestMd5Client->u4NonceCnt == 0xffffffff)
    {
        return UTL_AUTH_FAILURE;
    }
    pDigestMd5Client->u4NonceCnt++;
    SPRINTF ((CHR1 *) pDigestMd5Client->au1URI, "smtp/%s",
             pDigestMd5Client->au1Realm);

    /* Generate the cnonce string and calculate the response digest */
    UtlDigMd5GenCNonce (pDigestMd5Client->au1CNonce,
                        pDigestMd5Client->u4NonceLen);
    UtlDigMd5Auth (pDigestMd5Client, 0, au1HexStr);

    MEMSET (gau1TmpStr, 0, UTL_RESPONSE_MSG);
    MEMSET (pDigestMd5Client->au1Buffer, 0, UTL_RESPONSE_MSG);
    pDigestMd5Client->u4BufLen = 0;

    /* fill client response string */
    if ((pDigestMd5Client->i4Flags & DIG_MD5_UTF8) != 0)
    {
        pDigestMd5Client->u4BufLen = (UINT4) SPRINTF ((CHR1 *) gau1TmpStr,
                                                      "charset=utf-8,");
    }

    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "username=\"%s\",", pDigestMd5Client->au1UserName);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "realm=\"%s\",", pDigestMd5Client->au1Realm);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "nonce=\"%s\",", pDigestMd5Client->au1Nonce);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "nc=%08x,", pDigestMd5Client->u4NonceCnt);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "cnonce=\"%s\",", pDigestMd5Client->au1CNonce);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "digest-uri=\"%s\",", pDigestMd5Client->au1URI);
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "response=%s,", au1HexStr);
    if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_INT) != 0)
    {
        pDigestMd5Client->u4BufLen +=
            (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                             "qop=auth-int,");
    }
    else if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_CONF) != 0)
    {
        pDigestMd5Client->u4BufLen +=
            (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                             "qop=auth-conf,");
    }
    else
    {
        pDigestMd5Client->u4BufLen +=
            (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                             "qop=auth,");
    }
    pDigestMd5Client->u4BufLen +=
        (UINT4) SPRINTF ((CHR1 *) (gau1TmpStr + pDigestMd5Client->u4BufLen),
                         "maxbuf=%d", pDigestMd5Client->u4MaxBufLen);

    /* TODO if cipher is allowed, it has to be added here */

    if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_INT) != 0)
    {
        MEMCPY (pDigestMd5Client->au1Buffer, gau1TmpStr,
                pDigestMd5Client->u4BufLen);
        UtlDigMd5GenIntegrityMAC (pDigestMd5Client);
    }
    UtlBase64EncodeString (gau1TmpStr, pDigestMd5Client->au1Buffer,
                           &pDigestMd5Client->u4BufLen);
    return UTL_AUTH_SUCCESS;
}

/***************************************************************************
 * DESCRIPTION :  This routine updates the DigestMd5Parser structure with 
 *                 the digest challenge string.
 *
 * INPUTS      :  pu1SrvString  - Digest challenge string
 *                u4SrvStrLen   - Digest challenge string length
 *
 * OUTPUTS     :  pDigMd5Parser - Updated DigestMd5Parser structure 
 *
 * RETURNS     :  UTL_AUTH_SUCCESS/UTL_AUTH_FAILURE.
 ***************************************************************************/
INT4
DigestMd5ParserUpdate (tDigestMd5Parser * pDigMd5Parser, UINT1 *pu1SrvString,
                       UINT4 u4SrvStrLen)
{
    if (u4SrvStrLen != 0)
    {
        pDigMd5Parser->pu1Start = pu1SrvString;
        pDigMd5Parser->pu1End = pu1SrvString + u4SrvStrLen;
        return UTL_AUTH_SUCCESS;
    }
    return UTL_AUTH_FAILURE;
}

/***************************************************************************
 * DESCRIPTION :  This routine parses the digest challenge string and returns
 *                 in the format as key=value.
 *
 * INPUTS      :  pDigMd5Parser - DigestMd5Parser having challenge string.
 *
 * OUTPUTS     :  pu1Key     - key name.
 *                pu4KeyLen  - key string length
 *                pu1Value   - value for the parsed key
 *                pu4ValLen  - value string length.
 *
 * RETURNS     :  UTL_AUTH_SUCCESS/UTL_AUTH_FAILURE/UTL_AUTH_EOL.
 ***************************************************************************/
INT4
DigestMd5ParserProcess (tDigestMd5Parser * pDigMd5Parser, UINT1 **pu1Key,
                        UINT4 *pu4KeyLen, UINT1 **pu1Value, UINT4 *pu4ValLen)
{
    UINT1              *pu1Start = NULL;
    UINT1              *pu1End = NULL;
    UINT1              *pu1Equal = NULL;
    UINT1               au1CharSet[8];
    UINT1               u1QFlag = 0;

    /* Format: key=value,key="value" */
    /* Parse the key */
    MEMSET (au1CharSet, 0, 8);
    STRNCPY (au1CharSet, DELIMITERS ",;", STRLEN(DELIMITERS ",;"));
    au1CharSet[STRLEN(DELIMITERS ",;")] = '\0';
    pu1Start = UtlAuthSrvStringParse (pDigMd5Parser->pu1Start,
                                      pDigMd5Parser->pu1End, au1CharSet);
    if ((pu1Start == NULL) || (*pu1Start == ';'))
    {
        return UTL_AUTH_EOL;
    }

    MEMSET (au1CharSet, 0, 8);
    STRNCPY (au1CharSet, "=,;", STRLEN("=,;"));
    au1CharSet[STRLEN("=,;")] = '\0';
    pu1Equal = UtlAuthSrvStringEndParse (pu1Start, pDigMd5Parser->pu1End,
                                         au1CharSet);
    if ((pu1Equal == NULL) || (*pu1Equal == ',') || (*pu1Equal == ';'))
    {
        return UTL_AUTH_FAILURE;
    }
    MEMSET (au1CharSet, 0, 8);
    STRNCPY (au1CharSet, DELIMITERS, STRLEN(DELIMITERS));
    au1CharSet[STRLEN(DELIMITERS)] = '\0';
    pu1End = UtlAuthSrvStringEndParse (pu1Start, pu1Equal, au1CharSet);
    if (pu1End == NULL)
    {
        pu1End = pu1Equal;
    }

    *pu1Key = pu1Start;
    *pu4KeyLen = (UINT4) (pu1End - pu1Start);
    *pu1Value = NULL;
    *pu4ValLen = 0;

    /* Parse the value for the retrived key */
    /* move pu1Equal to the next char i.e after = */
    pu1Equal++;
    if (pu1Equal >= pDigMd5Parser->pu1End)
    {
        /* value field is Null */
        pDigMd5Parser->pu1Start = pu1Equal;
        return UTL_AUTH_SUCCESS;
    }

    pu1Start = UtlAuthSrvStringParse (pu1Equal, pDigMd5Parser->pu1End,
                                      au1CharSet);
    if (pu1Start == NULL)
    {
        pu1Start = pu1Equal;
    }
    if (*pu1Start == '\"')
    {
        u1QFlag = 1;
        pu1Start++;
    }

    MEMSET (au1CharSet, 0, 8);
    if (u1QFlag == 1)
    {
        STRNCPY (au1CharSet, "\"", STRLEN("\""));
	au1CharSet[STRLEN("\"")] = '\0';
    }
    else
    {
        STRNCPY (au1CharSet, DELIMITERS ",;", STRLEN(DELIMITERS ",;"));
	au1CharSet[STRLEN(DELIMITERS ",;")] = '\0';
    }

    pu1End = UtlAuthSrvStringUnParse (pu1Start, pDigMd5Parser->pu1End,
                                      au1CharSet);
    if (pu1End == NULL)
    {
        if (u1QFlag == 1)
        {
            return UTL_AUTH_FAILURE;
        }
        pu1End = pDigMd5Parser->pu1End;
    }
    else if (*pu1End == ';')
    {
        pDigMd5Parser->pu1End = pu1End;
    }

    pDigMd5Parser->pu1Start = pu1End;
    if (u1QFlag == 1)
    {
        pDigMd5Parser->pu1Start++;
    }

    *pu1Value = pu1Start;
    *pu4ValLen = (UINT4) (pu1End - pu1Start);
    return UTL_AUTH_SUCCESS;
}

/***************************************************************************
 * DESCRIPTION :  This routine generates a random CNonce value for the given 
 *                 length.
 *
 * INPUTS      :  u4CNonceLen - Length of the CNonce string.
 *
 * OUTPUTS     :  pu1CNonce   - Generated CNonce string.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtlDigMd5GenCNonce (UINT1 *pu1CNonce, UINT4 u4CNonceLen)
{
    static const UINT1  au1StrArr[] =
        { "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" };
    static UINT4        u4StrLen = sizeof (au1StrArr) - 1;
    UINT4               u4Cntr = 0;
    UINT4               u4RndNo;

    for (u4Cntr = 0; u4Cntr < u4CNonceLen; u4Cntr++)
    {
        u4RndNo = (UINT4) rand ();
        pu1CNonce[u4Cntr] = au1StrArr[u4RndNo % u4StrLen];
    }
    pu1CNonce[u4CNonceLen] = '\0';
    return;
}

/***************************************************************************
 * DESCRIPTION :  This routine calculates the digest response Hex string     
 *                 based on the values present the digest challenge string.
 *
 * INPUTS      :  pDigestMd5Client - DigestMd5Client structure having 
 *                                    challenge information
 *                u1RspAuthFlag    - Response auth flag.
 *                                   if 0 - response value as calculated 
 *                                          by client.
 *                                   if 1 - response value as calculated 
 *                                          by server. This is used for 
 *                                          digest response verification.
 *
 * OUTPUTS     :  pu1HexStr - Calculated digest response as Hex value.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtlDigMd5Auth (tDigestMd5Client * pDigestMd5Client,
               UINT1 u1RspAuthFlag, UINT1 *pu1HexStr)
{
    unArCryptoHash      Md5Ctx;
    UINT1               au1Digest2[MD5_DIGEST_STRING_LEN];
    UINT1               au1Digest1[MD5_DIGEST_STRING_LEN];
    UINT1               au1Digest[MD5_DIGEST_LENGTH];
    UINT1               au1ZeroStr[MD5_DIGEST_STRING_LEN];
    INT4                i4Length;

    /*
       response = HEX(MD5(HEX(MD5(A1)) : {nonce : nonce-count : cnonce : qop 
       : HEX(MD5(A2))}))
     */

    /* Calculating HEX(MD5(A2)) 
       A2 = {AUTHENTICATE : digest-uri}  */
    arMD5_start (&Md5Ctx);

    if (u1RspAuthFlag == 0)
    {
        arMD5_update (&Md5Ctx, (UINT1 *) "AUTHENTICATE:",
                      STRLEN ("AUTHENTICATE:"));
    }
    else
    {
        arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    }
    i4Length = (INT4) STRLEN (pDigestMd5Client->au1URI);
    arMD5_update (&Md5Ctx, pDigestMd5Client->au1URI, (UINT4) i4Length);

    if (((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_CONF) != 0)
        || ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_INT) != 0))
    {
        arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
        MEMSET (au1ZeroStr, 0, MD5_DIGEST_STRING_LEN);
        arMD5_update (&Md5Ctx, au1ZeroStr, MD5_DIGEST_STRING_LEN);
    }

    arMD5_finish (&Md5Ctx, au1Digest);
    UtlAuthConvertMd5HashToHex (au1Digest, au1Digest2);

    /* Calculating HEX(MD5(A1)) 
       A1 = {MD5({user:realm:passwd}):nonce:cnonce} */
    arMD5_start (&Md5Ctx);
    if (pDigestMd5Client->au1UserName[0] != '\0')
    {
        i4Length = (INT4) STRLEN (pDigestMd5Client->au1UserName);
        if ((pDigestMd5Client->i4Flags & DIG_MD5_UTF8) != 0)
        {
            UtilMD5UTF8_8859_1 (&Md5Ctx, pDigestMd5Client->au1UserName,
                                i4Length);
        }
        else
        {
            arMD5_update (&Md5Ctx, pDigestMd5Client->au1UserName,
                          (UINT4) i4Length);
        }
    }

    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    if (pDigestMd5Client->au1Realm[0] != '\0')
    {
        i4Length = (INT4) STRLEN (pDigestMd5Client->au1Realm);
        if ((pDigestMd5Client->i4Flags & DIG_MD5_UTF8) != 0)
        {
            UtilMD5UTF8_8859_1 (&Md5Ctx, pDigestMd5Client->au1Realm, i4Length);
        }
        else
        {
            arMD5_update (&Md5Ctx, pDigestMd5Client->au1Realm,
                          (UINT4) i4Length);
        }
    }

    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    if (pDigestMd5Client->au1Passwd[0] != '\0')
    {
        i4Length = (INT4) STRLEN (pDigestMd5Client->au1Passwd);
        if ((pDigestMd5Client->i4Flags & DIG_MD5_UTF8) != 0)
        {
            UtilMD5UTF8_8859_1 (&Md5Ctx, pDigestMd5Client->au1Passwd, i4Length);
        }
        else
        {
            arMD5_update (&Md5Ctx, pDigestMd5Client->au1Passwd,
                          (UINT4) i4Length);
        }
    }
    arMD5_finish (&Md5Ctx, au1Digest);

    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, au1Digest, MD5_DIGEST_LENGTH);
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    if ((pDigestMd5Client->u4NonceLen > 0)
        && (pDigestMd5Client->au1Nonce[0] != '\0'))
    {
        arMD5_update (&Md5Ctx, pDigestMd5Client->au1Nonce,
                      pDigestMd5Client->u4NonceLen);
    }
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    arMD5_update (&Md5Ctx, pDigestMd5Client->au1CNonce,
                  (STRLEN (pDigestMd5Client->au1CNonce)));
    arMD5_finish (&Md5Ctx, au1Digest);

    if (u1RspAuthFlag == 0)
    {
        /* Store the digest of A1 for further use in calculating keys in 
         * integrity and privacy protection */
        MEMCPY (pDigestMd5Client->au1DigestA1, au1Digest, MD5_DIGEST_LENGTH);
    }
    UtlAuthConvertMd5HashToHex (au1Digest, au1Digest1);

    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, au1Digest1, MD5_DIGEST_STRING_LEN);
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    if ((pDigestMd5Client->u4NonceLen > 0)
        && (pDigestMd5Client->au1Nonce[0] != '\0'))
    {
        arMD5_update (&Md5Ctx, pDigestMd5Client->au1Nonce,
                      pDigestMd5Client->u4NonceLen);
    }
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    MEMSET (au1Digest, 0, MD5_DIGEST_LENGTH);
    SPRINTF ((CHR1 *) au1Digest, "%08x", pDigestMd5Client->u4NonceCnt);
    arMD5_update (&Md5Ctx, au1Digest, 8);
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    arMD5_update (&Md5Ctx, pDigestMd5Client->au1CNonce,
                  (STRLEN (pDigestMd5Client->au1CNonce)));
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_INT) != 0)
    {
        arMD5_update (&Md5Ctx, (UINT1 *) "auth-int", 8);
    }
    else if ((pDigestMd5Client->i4Flags & DIG_MD5_AUTH_CONF) != 0)
    {
        arMD5_update (&Md5Ctx, (UINT1 *) "auth-conf", 9);
    }
    else
    {
        arMD5_update (&Md5Ctx, (UINT1 *) "auth", 4);
    }
    arMD5_update (&Md5Ctx, (UINT1 *) ":", 1);
    arMD5_update (&Md5Ctx, au1Digest2, MD5_DIGEST_STRING_LEN);
    arMD5_finish (&Md5Ctx, au1Digest);
    UtlAuthConvertMd5HashToHex (au1Digest, pu1HexStr);
    return;
}

/***************************************************************************
 * DESCRIPTION :  This routine updates the MD5 context. If the string 
 *                 is entirely in the 8859-1 subset of UTF-8, then it is
 *                 translated to 8859-1 before performing the MD5 operations.
 *
 * INPUTS      :  pMd5Ctx  - MD5 Context information. 
 *                pu1Base  - Message block string.
 *                i4Length - Message string length.
 *
 * OUTPUTS     :  pMd5Ctx  - Updated  MD5 Context information.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtilMD5UTF8_8859_1 (unArCryptoHash * pMd5Ctx, UINT1 *pu1Base, INT4 i4Length)
{
    UINT1              *pu1Cntr = NULL, *pu1End = NULL;
    UINT1               u1Buf;

    pu1End = pu1Base + i4Length;
    for (pu1Cntr = pu1Base; pu1Cntr < pu1End; ++pu1Cntr)
    {
        if (*pu1Cntr > 0xC3)
        {
            break;
        }
        if ((*pu1Cntr >= 0xC0) && (*pu1Cntr <= 0xC3))
        {
            if ((++pu1Cntr == pu1End) || (*pu1Cntr < 0x80) || (*pu1Cntr > 0xBF))
            {
                break;
            }
        }
    }
    /* if we found a character outside 8859-1, don't alter string */
    if (pu1Cntr < pu1End)
    {
        arMD5_update (pMd5Ctx, pu1Base, (UINT4) i4Length);
        return;
    }

    /* convert to 8859-1 prior to applying hash */
    do
    {
        for (pu1Cntr = pu1Base; ((pu1Cntr < pu1End) && (*pu1Cntr < 0xC0));
             ++pu1Cntr)
            ;
        if (pu1Cntr != pu1Base)
        {
            arMD5_update (pMd5Ctx, pu1Base, (UINT4) (pu1Cntr - pu1Base));
        }
        if ((pu1Cntr + 1) >= pu1End)
        {
            break;
        }
        u1Buf =
            (UINT1) (((INT4) (pu1Cntr[0] & 0x3) << 6) |
                     (INT4) (pu1Cntr[1] & 0x3f));
        arMD5_update (pMd5Ctx, &u1Buf, 1);
        pu1Base = pu1Cntr + 2;
    }
    while (pu1Base < pu1End);
    return;
}

/***************************************************************************
 * DESCRIPTION :  This routine generates the integrity keys for the client
 *                 and the server based on the value of the digest challenge
 *                 string.
 *
 * INPUTS      :  pDigestMd5Client - Digest Md5 client contains the digest 
 *                                    challenge information.
 *
 * OUTPUTS     :  pDigestMd5Client - Digest Md5 client updated with the 
 *                                    integrity keys calculated.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtlDigMd5GenIntegrityKey (tDigestMd5Client * pDigestMd5Client)
{
    unArCryptoHash      Md5Ctx;

    /* Calculate Kic and Kis */
    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, pDigestMd5Client->au1DigestA1, MD5_DIGEST_LENGTH);
    arMD5_update (&Md5Ctx, (UINT1 *) CLIENT_INTEGRITY_KEY_STRING,
                  CLIENT_INTEGRITY_KEY_STRING_LEN);
    arMD5_finish (&Md5Ctx, pDigestMd5Client->au1Kic);

    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, pDigestMd5Client->au1DigestA1, MD5_DIGEST_LENGTH);
    arMD5_update (&Md5Ctx, (UINT1 *) SERVER_INTEGRITY_KEY_STRING,
                  SERVER_INTEGRITY_KEY_STRING_LEN);
    arMD5_finish (&Md5Ctx, pDigestMd5Client->au1Kis);
    return;
}

/***************************************************************************
 * DESCRIPTION :  This routine generates the integrity MAC block for the 
 *                 message using the integrity keys generated and appends 
 *                 the MAC block to the digest-response message formulated
 *                 if the authentication type is auth-int.
 *
 * INPUTS      :  pDigestMd5Client - Digest Md5 client contains the digest 
 *                                    challenge information.
 *
 * OUTPUTS     :  pDigestMd5Client - Digest Md5 client updated with the 
 *                                    MAC block updated to the digest 
 *                                    response string.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtlDigMd5GenIntegrityMAC (tDigestMd5Client * pDigestMd5Client)
{
    UINT1               au1MAC[MD5_DIGEST_LENGTH];
    UINT4               u4SeqNum = 0;

    /* convert the seq number to network byte order */
    u4SeqNum = OSIX_HTONL (pDigestMd5Client->u4SeqNo);
    if (pDigestMd5Client->u4SeqNo >= 0xFFFFFFFF)
    {
        pDigestMd5Client->u4SeqNo = 0;
    }
    else
    {
        pDigestMd5Client->u4SeqNo++;
    }

    /* calculate the HMAC for the message */
    MEMSET (gau1TmpStr, 0, UTL_RESPONSE_MSG);
    MEMCPY (gau1TmpStr, &u4SeqNum, UTL_MAC_SEQ_NUM_LEN);
    MEMCPY ((gau1TmpStr + UTL_MAC_SEQ_NUM_LEN), pDigestMd5Client->au1Buffer,
            pDigestMd5Client->u4BufLen);

    UtlDigMd5GenIntegrityKey (pDigestMd5Client);

    arHmac_MD5 (gau1TmpStr,
                (UTL_MAC_SEQ_NUM_LEN + (INT4) pDigestMd5Client->u4BufLen),
                pDigestMd5Client->au1Kic, MD5_DIGEST_LENGTH, au1MAC);

    /* Form the 16 byte MAC block 
     * 10 bytes HMAC digest 
     * 2  bytes MSG type
     * 4  bytes seqence no. */
    MEMCPY ((au1MAC + UTL_MAC_HMAC_LEN), "\x00\x01", UTL_MAC_MSG_TYPE_LEN);
    MEMCPY ((au1MAC + UTL_MAC_HMAC_LEN + UTL_MAC_MSG_TYPE_LEN), &u4SeqNum,
            UTL_MAC_SEQ_NUM_LEN);

    MEMCPY ((pDigestMd5Client->au1Buffer + pDigestMd5Client->u4BufLen),
            au1MAC, MD5_DIGEST_LENGTH);
    pDigestMd5Client->u4BufLen += MD5_DIGEST_LENGTH;
    MEMSET (gau1TmpStr, 0, UTL_RESPONSE_MSG);
    SPRINTF ((CHR1 *) gau1TmpStr, "%d",
             OSIX_HTONL (pDigestMd5Client->u4BufLen));
    MEMCPY (gau1TmpStr + 4, pDigestMd5Client->au1Buffer,
            pDigestMd5Client->u4BufLen);
    pDigestMd5Client->u4BufLen += 4;

    return;
}
