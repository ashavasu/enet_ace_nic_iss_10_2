/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlauth.h,v 1.1
 *
 * Description: Contains Constants and function prototypes for 
 *               the authentication utilities.
 *******************************************************************/

#include "lr.h"

#define   MD5_DIGEST_LENGTH           16
#define   UTL_MAX_SRV_CHA_LEN         100
#define   MD5_DIGEST_STRING_LEN      (MD5_DIGEST_LENGTH * 2)

VOID UtlAuthConvertMd5HashToHex (UINT1 *pu1Md5Hash, UINT1 *pu1Output);

UINT1 * UtlAuthSrvStringParse (UINT1 *pu1Start, UINT1 *pu1End, UINT1 *pu1CharSet);

UINT1 * UtlAuthSrvStringEndParse (UINT1 *pu1Start, UINT1 *pu1End,
                                  UINT1 *pu1CharSet);

UINT1 * UtlAuthSrvStringUnParse (UINT1 *pu1Start, UINT1 *pu1End, UINT1 *pu1CharSet);

VOID UtlAuthStrCpy (UINT1 *pu1Value, UINT4 u4ValLen, UINT1 *pu1Nonce,
                    UINT4 *pu4NonceLen);
