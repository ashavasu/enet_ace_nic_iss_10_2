/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: crammd5.c,v 1.1
 *
 * Description: Utility for performing CRAM MD5 digest
 *******************************************************************/

#include "crammd5.h"

/***************************************************************************
 * DESCRIPTION :  This routine generates the cram-md5 digest for the 
 *                 given user name, secret and server key.
 *
 * INPUTS      :  pu1UserName     - User name.
 *                pu1Secret       - Secret
 *                u2SecretLen     - Secret length
 *                pu1SrvChallenge - Server challenge string
 *                pu4ChaLen       - Server challenge string length
 *
 * OUTPUTS     :  pu1Digest - Digest message created
 *                pu4ChaLen - Digest message length
 *
 * RETURNS     :  None                   
 ***************************************************************************/
VOID
AuthCramMd5Digest (UINT1 *pu1UserName, UINT1 *pu1Secret, UINT2 u2SecretLen,
                   UINT1 *pu1SrvChallenge, UINT4 *pu4ChaLen, UINT1 *pu1Digest)
{
    UINT4               u4Len = 0;
    UINT1               au1SrvChallenge[UTL_MAX_SRV_CHA_LEN];
    UINT1               au1Md5Digest[MD5_DIGEST_LENGTH];

    /* Calculate the Md5 digest */
    MEMSET (au1Md5Digest, 0, MD5_DIGEST_LENGTH);

    arHmac_MD5 (pu1SrvChallenge, (INT4) *pu4ChaLen, pu1Secret,
                u2SecretLen, au1Md5Digest);

    /* Form the cram md5 digest as "username digestmessage" */
    MEMSET (au1SrvChallenge, 0, UTL_MAX_SRV_CHA_LEN);
    u4Len = SNPRINTF ((CHR1 *) au1SrvChallenge, UTL_MAX_SRV_CHA_LEN, "%s ",
                      pu1UserName);

    /* Append hex value of the digest obtained */
    UtlAuthConvertMd5HashToHex (au1Md5Digest, &au1SrvChallenge[u4Len]);
    u4Len = STRLEN (au1SrvChallenge);

    /* Encode the string obtained */
    UtlBase64EncodeString (au1SrvChallenge, pu1Digest, &u4Len);
    *pu4ChaLen = u4Len;
    return;
}
