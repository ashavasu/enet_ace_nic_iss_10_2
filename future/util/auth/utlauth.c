/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlauth.c,v 1.1
 *
 * Description: Utilties required for CRAM MD5 and DIGEST MD5
 *******************************************************************/

#include "utlauth.h"

/***************************************************************************
 * DESCRIPTION :  This routine converts the MD5 digest hash message to its 
 *                 corresponding Hex value in 32 bytes.
 *
 * INPUTS      :  pu1Md5Hash - MD5 digest hash message.
 *
 * OUTPUTS     :  pu1Output - Hex value of the MD5 digest hash.
 *
 * RETURNS     :  None                   
 ***************************************************************************/
VOID
UtlAuthConvertMd5HashToHex (UINT1 *pu1Md5Hash, UINT1 *pu1Output)
{
    UINT4               u4Len = 0;
    UINT1               au1TmpBuf[MD5_DIGEST_STRING_LEN + 4];
    UINT1               u1Cntr;

    for (u1Cntr = 0; u1Cntr < MD5_DIGEST_LENGTH; u1Cntr++)
    {
        u4Len += SNPRINTF ((CHR1 *) & au1TmpBuf[u4Len],
                           (sizeof (au1TmpBuf) - u4Len), "%02x",
                           pu1Md5Hash[u1Cntr]);
    }

    MEMCPY (pu1Output, au1TmpBuf, u4Len);
    return;
}

/***************************************************************************
 * DESCRIPTION :  This routine parses the given string to return the first 
 *                 valid character that is not provided in the given 
 *                 character set.
 *
 * INPUTS      :  pu1Start   - Start pointer of the string.
 *                pu1End     - End pointer of the string.
 *                pu1CharSet - character set.
 *
 * OUTPUTS     :  None.
 *
 * RETURNS     :  Parsed string.
 ***************************************************************************/
UINT1              *
UtlAuthSrvStringParse (UINT1 *pu1Start, UINT1 *pu1End, UINT1 *pu1CharSet)
{
    UINT1              *pu1Cntr = NULL;

    for (pu1Cntr = pu1Start; pu1Cntr < pu1End; pu1Cntr++)
    {
        if (STRCHR (pu1CharSet, *pu1Cntr) == NULL)
        {
            return pu1Cntr;
        }
    }
    return NULL;
}

/***************************************************************************
 * DESCRIPTION :  This routine parses the given string to return the first 
 *                 occurence of the character given in the character set.
 *
 * INPUTS      :  pu1Start   - Start pointer of the string.
 *                pu1End     - End pointer of the string.
 *                pu1CharSet - character set.
 *
 * OUTPUTS     :  None.
 *
 * RETURNS     :  Parsed string.
 ***************************************************************************/
UINT1              *
UtlAuthSrvStringEndParse (UINT1 *pu1Start, UINT1 *pu1End, UINT1 *pu1CharSet)
{
    UINT1              *pu1Cntr = NULL;

    for (pu1Cntr = pu1Start; pu1Cntr < pu1End; pu1Cntr++)
    {
        if (STRCHR (pu1CharSet, *pu1Cntr) != NULL)
        {
            return pu1Cntr;
        }
    }
    return NULL;
}

/***************************************************************************
 * DESCRIPTION :  This routine parses the given string to return the first 
 *                 occurence of the character given in the unescaped 
 *                 characterset.
 *
 * INPUTS      :  pu1Start   - Start pointer of the string.
 *                pu1End     - End pointer of the string.
 *                pu1CharSet - character set.
 *
 * OUTPUTS     :  None.
 *
 * RETURNS     :  Parsed string.
 ***************************************************************************/
UINT1              *
UtlAuthSrvStringUnParse (UINT1 *pu1Start, UINT1 *pu1End, UINT1 *pu1CharSet)
{
    UINT1              *pu1Cntr = NULL;
    UINT1               u1EscChar = 0;

    for (pu1Cntr = pu1Start; pu1Cntr < pu1End; pu1Cntr++)
    {
        if (*pu1Cntr == '\\')
        {
            u1EscChar = !u1EscChar;
        }
        else
        {
            if ((STRCHR (pu1CharSet, *pu1Cntr) != NULL) && (!u1EscChar))
            {
                return pu1Cntr;
            }
            u1EscChar = 0;
        }
    }
    return NULL;
}

/***************************************************************************
 * DESCRIPTION :  This routine copies the given string.
 *
 * INPUTS      :  pu1Src    - String to be copied.
 *                u4StrLen  - String length.
 *
 * OUTPUTS     :  pu1Dest    - Updated string.
 *                pu4DestLen - Updated string length.
 *
 * RETURNS     :  None.
 ***************************************************************************/
VOID
UtlAuthStrCpy (UINT1 *pu1Src, UINT4 u4SrcLen, UINT1 *pu1Dest, UINT4 *pu4DestLen)
{
    UINT1               u1EscFlag = 0;
    UINT1              *pu1Cntr = NULL;
    UINT1              *pu1End = pu1Src + u4SrcLen;
    UINT4               u4Len = 0;

    for (pu1Cntr = pu1Src; pu1Cntr < pu1End; pu1Cntr++)
    {
        if (*pu1Cntr == '\\')
        {
            if (u1EscFlag == 1)
            {
                *pu1Dest++ = '\\';
                u4Len++;
            }
            u1EscFlag = !u1EscFlag;
        }
        else
        {
            *pu1Dest++ = *pu1Cntr;
            u4Len++;
            u1EscFlag = 0;
        }
    }
    *pu4DestLen = u4Len;
    pu1Dest[u4Len] = '\0';
    return;
}
