/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: crammd5.h,v 1.1
 *
 * Description: Contains function prototypes for CRAM MD5 utility
 *******************************************************************/

#include "base64.h"
#include "arHmac_api.h"
#include "utlauth.h"

VOID AuthCramMd5Digest PROTO ((UINT1 *pu1UserName, UINT1 *pu1Secret,
                               UINT2 u2SecretLen,  UINT1 *pu1SrvChallenge,
                               UINT4 * pu4ChaLen, UINT1 *pu1Digest));

