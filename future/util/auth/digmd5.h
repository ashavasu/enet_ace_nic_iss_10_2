/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: digmd5.h,v 1.1
 *
 * Description: Contains Constant, structure definition  and 
 *               function prototypes for DIGEST MD5 authentication
 *               utility.
 *******************************************************************/

#include "crammd5.h"
#include "cryartdf.h"
#include "arMD5_api.h"

#define   UTL_AUTH_SUCCESS            0
#define   UTL_AUTH_FAILURE            1
#define   UTL_AUTH_EOL                2
#define   UTL_AUTH_ERROR              3

#define   UTL_MAX_STRING_LEN          64
#define   UTL_URI_STR_LEN             128
#define   UTL_RESPONSE_MSG            1024

typedef struct digestMd5Parser {
    UINT1      *pu1Start;
    UINT1      *pu1End;
}tDigestMd5Parser;

typedef struct digestMd5Client {
    UINT1      au1Nonce[UTL_MAX_STRING_LEN];
    UINT1      au1CNonce[UTL_MAX_STRING_LEN];
    UINT1      au1Buffer[UTL_RESPONSE_MSG]; /* To contain response
                                               message */
    UINT1      au1UserName[UTL_MAX_STRING_LEN];
    UINT1      au1Passwd[UTL_MAX_STRING_LEN];
    UINT1      au1URI[UTL_URI_STR_LEN];
    UINT1      au1Realm[UTL_MAX_STRING_LEN];
    UINT1      au1DigestA1[MD5_DIGEST_LENGTH];
    UINT1      au1Kic[MD5_DIGEST_LENGTH];
    UINT1      au1Kis[MD5_DIGEST_LENGTH];
    UINT4      u4NonceCnt;
    UINT4      u4NonceLen;
    UINT4      u4BufLen;
    UINT4      u4MaxBufLen;
    UINT4      u4SeqNo;
    UINT4      u4PeerSeqNo;
    INT4       i4Flags;
}tDigestMd5Client;

/* Digest request directives bitmask */
#define DIG_MD5_NONCE            0x001
#define DIG_MD5_ALG              0x002
#define DIG_MD5_MBUF             0x004

/* MD5 digest flags */
#define DIG_MD5_UTF8             0x010
#define DIG_MD5_STALE            0x020
#define DIG_MD5_AUTH             0x040
#define DIG_MD5_AUTH_INT         0x080
#define DIG_MD5_AUTH_CONF        0x100
#define DIG_MD5_CIP_DES          0x200
#define DIG_MD5_CIP_3DES         0x400

#define DELIMITERS               "\r\n \t"

#define CLIENT_INTEGRITY_KEY_STRING \
    "Digest session key to client-to-server signing key magic constant"
#define SERVER_INTEGRITY_KEY_STRING \
    "Digest session key to server-to-client signing key magic constant"
#define CLIENT_INTEGRITY_KEY_STRING_LEN  65 
#define SERVER_INTEGRITY_KEY_STRING_LEN  65
#define UTL_MAC_DATA_LEN                  4
#define UTL_MAC_HMAC_LEN                 10
#define UTL_MAC_MSG_TYPE_LEN              2
#define UTL_MAC_SEQ_NUM_LEN               4


INT4 DigestMd5ParseSrvChallenge (tDigestMd5Client * pDigestMd5Client,
                                 UINT1 * pu1SrvString, UINT4 u4SrvStrLen);

INT4 DigestMd5ClientResponse (tDigestMd5Client *pDigestMd5Client);

INT4 DigestMd5ParserUpdate (tDigestMd5Parser *pDigMd5Parser,
                            UINT1 *pu1SrvString, UINT4 u4SrvStrLen);

INT4 DigestMd5ParserProcess (tDigestMd5Parser *pDigMd5Parser, UINT1 **pu1Key,
                             UINT4 *pu4KeyLen, UINT1 **pu1Value,
                             UINT4 *pu4ValLen);

VOID UtlDigMd5GenCNonce (UINT1 *pu1CNonce, UINT4 u4CNonceLen);

VOID UtlDigMd5Auth (tDigestMd5Client *pDigestMd5Client,
                    UINT1 u1RspAuthFlag, UINT1 *pu1HexStr);

VOID UtilMD5UTF8_8859_1 (unArCryptoHash * pMd5Ctx, UINT1 *pu1Base,
                         INT4 i4Length);

VOID UtlDigMd5GenIntegrityKey (tDigestMd5Client *);

VOID UtlDigMd5GenIntegrityMAC (tDigestMd5Client *);
