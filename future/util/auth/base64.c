/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: base64.c,v 1.1
 *
 * Description: Base64 encoding and decoding routines
 *******************************************************************/

#ifndef _BASE64_C_
#define _BASE64_C_

#include "base64.h"

/***************************************************************************
 * DESCRIPTION :  This routine will encode the given value to its
 *                 corresponding base64 format. The invoking function has 
 *                 to take care of giving the input value in 6bits value.
 *
 * INPUTS      :  u1Value - The input value that is to be base64 encoded.
 *
 * OUTPUTS     :  None.
 *
 * RETURNS     :  Base64 encode value for the given variable.
 ***************************************************************************/
UINT1
Base64_Encode (UINT1 u1Value)
{
    UINT1               u1B64Value = 0;

    if (u1Value < 26)
    {
        u1B64Value = (UINT1) ('A' + u1Value);
    }
    else if (u1Value < 52)
    {
        u1B64Value = (UINT1) ('a' + (u1Value - 26));
    }
    else if (u1Value < 62)
    {
        u1B64Value = (UINT1) ('0' + (u1Value - 52));
    }
    else if (u1Value == 62)
    {
        u1B64Value = '+';
    }
    else
    {
        u1B64Value = '/';
    }
    return u1B64Value;
}

/***************************************************************************
 * DESCRIPTION :  This routine will decode the given base64 value to its
 *                 corresponding ascii format. 
 *
 * INPUTS      :  u1Value - The input bas64 encoded value 
 *
 * OUTPUTS     :  None.
 *
 * RETURNS     :  Base64 decoded value.
 ***************************************************************************/
UINT1
Base64_Decode (UINT1 u1Value)
{
    UINT1               u1DecValue = 0;

    if ((u1Value >= 'A') && (u1Value <= 'Z'))
    {
        u1DecValue = (UINT1) (u1Value - 'A');
    }
    else if ((u1Value >= 'a') && (u1Value <= 'z'))
    {
        u1DecValue = (UINT1) (u1Value - 'a' + 26);
    }
    else if ((u1Value >= '0') && (u1Value <= '9'))
    {
        u1DecValue = (UINT1) (u1Value - '0' + 52);
    }
    else if (u1Value == '+')
    {
        u1DecValue = 62;
    }
    else if (u1Value == '/')
    {
        u1DecValue = 63;
    }
    return u1DecValue;
}

/***************************************************************************
 * DESCRIPTION :  This function encodes the given string to its corresponding
 *                 base64 format. The length of the string pu1EncodedString
 *                 has to be sufficiently greater than the length of string
 *                 pu1String because, 3 bytes of pu1String is encoded into
 *                 4 bytes of the pu1EncodedString.
 *
 * INPUTS      :  pu1String - String that is to be base64 encoded.
 *                pu4Length - Length of the pu1String.
 *
 * OUTPUTS     :  pu1EncodedString - Base64 encoded string.
 *                pu4Length - Length of the encoded string.
 *
 * RETURNS     :  None.                     
 ***************************************************************************/
VOID
UtlBase64EncodeString (UINT1 *pu1String, UINT1 *pu1EncodedString,
                       UINT4 *pu4Length)
{
    UINT4               u4EncStrLen = 0;
    UINT4               u4Index = 0;
    UINT1               au1String[BASE64_ENC_STR_LEN];
    UINT1               u1Value = 0;

    while (u4Index < *pu4Length)
    {
        if ((u4Index + BASE64_ENC_STR_LEN) > *pu4Length)
        {
            MEMSET (au1String, 0, BASE64_ENC_STR_LEN);
            MEMCPY (au1String, (pu1String + u4Index),
                    (*pu4Length % BASE64_ENC_STR_LEN));
        }
        else
        {
            MEMCPY (au1String, (pu1String + u4Index), BASE64_ENC_STR_LEN);
        }
        u4Index += BASE64_ENC_STR_LEN;

        u1Value = au1String[0] >> 0x02;
        pu1EncodedString[u4EncStrLen++] = Base64_Encode (u1Value);
        u1Value =
            (UINT1) (((au1String[0] & 0x03) << 0x04) | (au1String[1] >> 0x04));
        pu1EncodedString[u4EncStrLen++] = Base64_Encode (u1Value);
        u1Value =
            (UINT1) (((au1String[1] & 0x0F) << 0x02) | (au1String[2] >> 0x06));

        if ((u1Value == 0) && (u4Index > *pu4Length)
            && (au1String[2] & 0x3F) == 0)
        {
            MEMCPY ((pu1EncodedString + u4EncStrLen), "==", 2);
            u4EncStrLen += 2;
            break;
        }
        pu1EncodedString[u4EncStrLen++] = Base64_Encode (u1Value);
        u1Value = au1String[2] & 0x3F;
        if ((u1Value == 0) && (u4Index > *pu4Length))
        {
            pu1EncodedString[u4EncStrLen++] = '=';
            break;
        }
        pu1EncodedString[u4EncStrLen++] = Base64_Encode (u1Value);
    }
    *pu4Length = u4EncStrLen;
    return;
}

/***************************************************************************
 * DESCRIPTION :  This function decodes the given base64 encoded string to 
 *                 ascii format. 
 *
 * INPUTS      :  pu1EncString - Base64 encoded string
 *                pu4Length - Length of the pu1EncString.
 *
 * OUTPUTS     :  pu1String - Base64 decoded string.
 *                pu4Length - Length of the decoded string.
 *
 * RETURNS     :  None.                     
 ***************************************************************************/
VOID
UtlBase64DecodeString (UINT1 *pu1EncString, UINT1 *pu1String, UINT4 *pu4Length)
{
    UINT4               u4DecStrLen = 0;
    UINT4               u4Index = 0;
    UINT1               au1String[BASE64_DEC_STR_LEN];
    UINT1               u1Counter = 0;

    while (u4Index < *pu4Length)
    {
        for (u1Counter = 0; u1Counter < BASE64_DEC_STR_LEN; u1Counter++)
        {
            au1String[u1Counter] =
                Base64_Decode (pu1EncString[u4Index + u1Counter]);
        }

        pu1String[u4DecStrLen++] =
            (UINT1) (au1String[0] << 0x02 | au1String[1] >> 0x04);
        pu1String[u4DecStrLen++] =
            (UINT1) (au1String[1] << 0x04 | au1String[2] >> 0x02);
        pu1String[u4DecStrLen++] =
            (UINT1) (au1String[2] << 0x06 | au1String[3]);
        u4Index += BASE64_DEC_STR_LEN;
    }
    *pu4Length = u4DecStrLen;
    return;
}
#endif
