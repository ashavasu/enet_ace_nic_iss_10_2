/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: base64.h,v 1.1
 *
 * Description: Contains Constants and function prototypes for 
 *               Base64 encoder/decoder
 *******************************************************************/

#include "lr.h"

#ifdef _BASE64_C_
#define BASE64_ENC_STR_LEN     3
#define BASE64_DEC_STR_LEN     4

UINT1 Base64_Encode PROTO ((UINT1 u1Value));
UINT1 Base64_Decode PROTO ((UINT1 u1Value));
#endif

VOID UtlBase64EncodeString PROTO ((UINT1 * pu1String,
                                   UINT1 * pu1EncodedString,
                                   UINT4 *pu4Length));
VOID UtlBase64DecodeString PROTO ((UINT1 * pu1EncString,
                                   UINT1 * pu1String,
                                   UINT4 *pu4Length));
