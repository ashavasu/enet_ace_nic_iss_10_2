/****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilcli.h,v 1.6 2015/04/28 12:51:02 siva Exp $
 *
 * Description: This will have prototypes and Macros for utility routines 
 *              used by modules using CLI
 *
 ****************************************************************************/
#ifndef __UTILCLI_H__
#define __UTILCLI_H__

#include "lr.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"
#include "cfa.h"
#include "cli.h"
#include "ipv6.h"
#include "utilipvx.h"

#define MAX_SERVICE_TOKENS       14

#define FTP_PORT                 21
#define TELNET_PORT              23
#define SMTP_PORT                25
#define WHOIS_PORT               43
#define DNS_PORT                 56
#define TFTP_PORT                69
#define GOPHER_PORT              70
#define FINGER_PORT              79
#define HTTP_PORT                80
#define POP3_PORT                110
#define NEWS_PORT                119
#define IRC_PORT                 194
#define PPTP_PORT                1723
#define OTHER_PORT               0 
#define INVALID_PORT             -1  

#define PORT_ZERO                0
#define CLI_PORTS_PER_BYTE       8

#define CLI_LIST_TYPE_VAL    1
#define CLI_LIST_TYPE_RANGE  2
#define CLI_LIST_TYPE_INV    255
#define CLI_VALID_SLOT_NO        0
#define CLI_LIST_DELIMIT     ","
#define CLI_RANGE_DELIMIT    "-"
#define CLI_SLOT_PORT_DELIMIT    "/"
#define CLI_VALIDPORT_LIST           "0123456789,-po"
#define CLI_VALIDIFACE_LIST           "0123456789,-/"

/*
 * This data structure for defining the Application service names and 
 * their associated port numbers.
 */

typedef struct t_SERVICE_TOKEN_TYPE {
    CONST CHR1 *pServiceName;
    INT4 i4Port;
} t_SERVICE_TOKEN_TYPE;

/* Structure for port list range values */
typedef struct tCliPortListRange
{
    UINT4 u4PortFrom;
    UINT4 u4PortTo;
} tCliPortListRange;

/* Structure for different types of port list values*/
typedef struct tCliPortList
{ 
    union 
    {
        tCliPortListRange PortListRange; 
        UINT4             u4PortNum;
    } uPortList;
} tCliPortList;

/* Structure for different types of iface list values*/
typedef struct tCliIfaceList
{ 
    union 
    {
        tCliPortListRange PortListRange; 
        UINT4             u4PortNum;
    } uPortList;
    INT4 i4SlotNum;
} tCliIfaceList;


INT1 *
CliGetToken     PROTO ((INT1 *pi1Str, CONST CHR1 *pi1Delimit,
                          CONST CHR1 *pi1ValidDelimit)); 
INT4
CliGetVal       PROTO ((tCliPortList *pCliPortList, INT1 *pi1Str));

INT4
CliGetValFromIfaceType PROTO ((tCliIfaceList * pCliIfaceList, INT1 *pi1Str, UINT4 u4IfType));

INT4
CliGetDecValue  PROTO ((UINT1));

INT4
CliGetSlotAndPort PROTO ((tCliIfaceList * pCliPortList, INT1 *pi1Str));

VOID
CliStrToIp4Addr (UINT1 *pu1Str, UINT1 *pu1DstStr);

tIp6Addr *
CliStrToIp6Addr PROTO ((UINT1 *));

#endif /* __UTILCLI_H__ */
