/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utilcli.c,v 1.18 2015/11/27 12:35:39 siva Exp $
 *
 * Description: Contains utility routines used by modules using CLI.
 *
 */

#include "utilcli.h"
#include "msr.h"
t_SERVICE_TOKEN_TYPE UtlServTokenType[MAX_SERVICE_TOKENS] = {
    {"ftp", FTP_PORT}
    ,
    {"telnet", TELNET_PORT}
    ,
    {"smtp", SMTP_PORT}
    ,
    {"whois", WHOIS_PORT}
    ,
    {"dns", DNS_PORT}
    ,
    {"tftp", TFTP_PORT}
    ,
    {"gopher", GOPHER_PORT}
    ,
    {"finger", FINGER_PORT}
    ,
    {"http", HTTP_PORT}
    ,
    {"pop3", POP3_PORT}
    ,
    {"news", NEWS_PORT}
    ,
    {"irc", IRC_PORT}
    ,
    {"pptp", PPTP_PORT}
    ,
    {"other", OTHER_PORT}
};

/* Port list bit mask list */
UINT1               gau1UtlPortBitMask[CLI_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};
INT4
 
 
 
 
 
 
 
 CliGetSftpParams (INT1 *pi1SftpStr, INT1 *pi1SftpFileName,
                   INT1 *pi1SftpUserName, INT1 *pi1SftpPassWd,
                   tIPvXAddr * pIpAddress, UINT1 *pu1HostName);

INT4
 
 
 
 
 
 
 
 CliGetTftpParams (INT1 *pi1TftpStr, INT1 *pi1TftpFileName,
                   tIPvXAddr * pIpAddress, UINT1 *pu1HostName);

/************************************************************************
 *  Function Name   : CliTrcStr                
 *  Description     : Converts a numeric trace level into a string
 *                    representation. This functions takes as input
 *                    the numeric trace level (u4TrcLvl), the list
 *                    of traces (pac1Trc) and fills the output buffer
 *                    (pc1Str) with the string representation.
 *
 *
 *                    IMPORTANT NOTE:
 *                    --------------
 *                    pac1Trc[] should be NULL terminated.
 *                    Eg.,
 *
 *                       *****************************
 *                       * const CHR1 *MyTraces[] ={ *
 *                       *  "traceA",                *
 *                       *   "traceB",               *
 *                       *   ...                     *
 *                       *   ...                     *
 *                       *   "traceM",               *
 *                       *   NULL             <<<<<<<<<<<<<<
 *                       * };                        *
 *                       *****************************
 *
 *                    The NULL field is used to determine the array size.
 *                    Absence of NULL could lead to a program fault.
 *
 *
 *                    The values 0 and ~0UL (all Fs) are interpreted to
 *                    mean traces disabled and traces enabled resp.
 *
 *
 *  Input           : u4TrcLvl  - The numeric trace level for which the
 *                                string representation is required.
 *
 *                                0    implies all traces OFF.
 *                                ~0UL implies all traces ON.
 *
 *                    pac1Trc   - Array of pointers containing trace strings.
 *                                as shown in example above.
 *
 *                    u4StrSize - Length of output buffer pc1Str.
 *                                This should be of sufficient length.
 *                                Length should be sum of strlens of all
 *                                trace names + one less number of spaces +
 *                                2 bytes for \r\n.
 *
 *  Output          : pc1Str   -  The output buffer which upon returning from
 *                                the function contains the string
 *                                representation of the u4TrcLvl.
 *  Returns         : None
 ************************************************************************/
VOID
CliTrcStr (UINT4 u4TrcLvl, const CHR1 * pac1Trc[], CHR1 * pc1Str,
           UINT4 u4StrSize)
{
    UINT4               u4Idx;
    UINT4               u4NumLevels;
    CHR1               *pc1StrPtr = pc1Str;
    const CHR1         *pc1Separator = " ";
    CHR1               *pc1StrEnd = pc1Str + u4StrSize;

    /* sizeof gets munged when pac1Trc[] is passed to a function    */
    /* Hence Iterate through pac1Trc and find the size of pac1Trc   */
    /* A NULL marks the end of pac1Trc                              */
    for (u4Idx = 0; pac1Trc[u4Idx]; u4Idx++);

    u4NumLevels = u4Idx;

    for (u4Idx = 0; u4Idx < u4NumLevels; u4Idx++)
    {
        if (u4TrcLvl & (UINT4) (1 << u4Idx))
        {
            if (pc1StrPtr + STRLEN (pac1Trc[u4Idx]) < pc1StrEnd)
            {
                STRCPY (pc1StrPtr, pac1Trc[u4Idx]);
                pc1StrPtr += STRLEN (pc1StrPtr);
            }

            if (pc1StrPtr + STRLEN (pc1Separator) < pc1StrEnd)
            {
                STRCPY (pc1StrPtr, pc1Separator);
                pc1StrPtr += STRLEN (pc1Separator);
            }
        }
    }

    if (pc1StrPtr + 2 < pc1StrEnd)
    {
        STRCPY (pc1StrPtr, "\r\n");
    }

    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : CliIsDelimit                                         *
 *                                                                          *
 *     Description   : The function checks if the given char matches with   *
 *                     any of the character in the delimiter string         *
 *                                                                          *
 *     Input(s)      : i1Char          : Character to check.                *
 *                     pi1Delimit      : Delimiter string.                  *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : i1Char if, it matches with any of the delimit char   *
 *                     else, return 0.                                      *
 *                                                                          *
 ****************************************************************************/

INT1
CliIsDelimit (INT1 i1Char, CONST CHR1 * pi1Delimit)
{
    if (pi1Delimit)
    {
        while (*pi1Delimit)
        {
            if (*pi1Delimit == i1Char)
                return (i1Char);
            pi1Delimit++;
        }
    }
    return (0);
}

/******* Functions necessary for L2 protocols handling *************/

/***************************************************************************
 *                                                                         *
 *     Function Name : CliDotStrToMac                                      *
 *                                                                         *
 *     Description   : This function which will accept strings of both     *
 *                     MAC Addr. formats and converts it to MAC value      *
 *                     stores in an array(6) of UINT1 data pointer.        *
 *                                                                         *
 *     Input(s)      : pu1DotStr : Pointer to the Mac addr. string         *
 *                     pu1Mac    : Pointer to the converted Mac addr       *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliDotStrToMac (UINT1 *pu1DotStr, UINT1 *pu1Mac)
{
    UINT4               u4Value = 0;
    UINT1               u1Char;
    UINT1               u1Pow = 1;
    UINT1               u1dotCount = 0;
    INT1                i1Index;
    INT1                i1CharCount = 1;
    INT1                i1StrIndex;
    INT1                i1MaxChar;
    INT1                i1BaseIndex;

    if (!pu1DotStr)
        return;

    i1StrIndex = (INT1) (STRLEN (pu1DotStr) - 1);

    for (i1Index = 0; i1Index <= i1StrIndex; i1Index++)
    {
        if (pu1DotStr[i1Index] == ':')
            u1dotCount++;
    }

    i1Index = CLI_MAC_ADDR_SIZE - 1;
    i1MaxChar = (u1dotCount == 2) ? 2 : 1;
    i1BaseIndex = (INT1) (i1Index - i1MaxChar);

    MEMSET (pu1Mac, 0, CLI_MAC_ADDR_SIZE);

    for (; (i1StrIndex >= 0) && (i1Index >= 0); i1StrIndex--, i1CharCount++)
    {
        if (ISXDIGIT (pu1DotStr[i1StrIndex]))
        {
            if (!ISDIGIT (pu1DotStr[i1StrIndex]))
            {
                u1Char = (UINT1) (10 + ((pu1DotStr[i1StrIndex]) - 'a'));
            }
            else
            {
                u1Char = pu1DotStr[i1StrIndex];
            }

            u4Value = (UINT4) ((0x0f & u1Char) * u1Pow) + u4Value;
            u1Pow = (UINT1) (u1Pow * 16);
        }
        (pu1Mac)[i1Index] = (UINT1) u4Value;

        if (!(i1CharCount % 2))
        {
            i1Index--;
            u4Value = 0;
            u1Pow = 1;
        }
        if (pu1DotStr[i1StrIndex] == ':')
        {
            i1Index = i1BaseIndex;
            i1BaseIndex = (INT1) (i1BaseIndex - i1MaxChar);
            u1Pow = 1;
            u4Value = 0;
            i1CharCount = 0;
        }
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliMacToStr                                         *
 *                                                                         *
 *     Description   : This function converts the given mac address        * 
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.(The string must be of length   *
 *                                  21 bytes minimum)                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), "   ");

}

 #ifdef EVPN_WANTED
 /***************************************************************************
  *                                                                         *
  *     Function Name : CliEthSegIdToStr                                    *
  *                                                                         *
  *     Description   : This function converts the given Ethernet Segment   *
  *                     Identifier in to a string of form aa:aa:aa:aa:aa:aa *
  *                                                                         *
  *     Input(s)      : pEthSegId  : Pointer to the Ethernet Segment value  *
  *                     pu1Temp    : Pointer to the converted Ethernet      *
  *                                  Segment to string.                     *
  *                                                                         *
  *     Output(s)     : NULL                                                *
  *                                                                         *
  *     Returns       : NULL                                                *
  *                                                                         *
  ***************************************************************************/
 VOID
 CliEthSegIdToStr (UINT1 *pEthSegId, UINT1 *pu1Temp)
 {
     UINT1               u1Byte;

     if (!(pEthSegId) || !(pu1Temp))
         return;

     for (u1Byte = 0; u1Byte < MAX_LEN_ETH_SEG_ID; u1Byte++)
     {
         pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pEthSegId + u1Byte));
     }
     SPRINTF ((CHR1 *) (pu1Temp - 1), "   ");

 }

 #endif

/***************************************************************************
 *                                                                         *
 *     Function Name : CliMacToFourOctetStr                                *
 *                                                                         *
 *     Description   : This function converts the given mac address        * 
 *                     in to a string of form aaaa.aaaa.aaaa               *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliMacToFourOctetStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{
    UINT1               au1MacAddr[MAC_LEN];
    UINT1               u1MacLen;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    MEMCPY (au1MacAddr, pMacAddr, MAC_LEN);

    for (u1MacLen = 0; u1MacLen < MAC_LEN; u1MacLen++)
    {
        if (au1MacAddr[u1MacLen] < 16)
        {
            SPRINTF ((CHR1 *) pu1Temp, "0%x", au1MacAddr[u1MacLen]);
        }
        else
        {
            SPRINTF ((CHR1 *) pu1Temp, "%x", au1MacAddr[u1MacLen]);
        }
        pu1Temp += STRLEN (pu1Temp);

        if ((u1MacLen != (MAC_LEN - 1)) && ((u1MacLen % 2) == 1))
        {
            STRCPY (pu1Temp, ".");
            pu1Temp += STRLEN (pu1Temp);
        }
    }
    SPRINTF ((CHR1 *) pu1Temp, "   ");
    pu1Temp += STRLEN (pu1Temp);

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliMacToDotStr                                      *
 *                                                                         *
 *     Description   : This function which will convert octet values in to *
 *                     strings of octets seperated by colon.               *
 *                                                                         *
 *     Input(s)      : pu1Mac    : Pointer to the converted Mac addr       *
 *                     pu1DotStr : Pointer to the Mac addr. string         *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliMacToDotStr (UINT1 *pu1Mac, UINT1 *pu1DotStr)
{
    UINT1               u1Index = 0;
    CHR1               *pTemp = (CHR1 *) pu1DotStr;

    if (!(pu1Mac) || !(pu1DotStr))
        return;

    for (u1Index = 0; u1Index < CLI_MAC_ADDR_SIZE; u1Index++)
    {
        SPRINTF ((CHR1 *) pTemp, "%02x", (pu1Mac)[u1Index]);
        pTemp += 2;
        *pTemp = ':';
        pTemp++;
    }
    pTemp--;
    *pTemp = '\0';
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrToMac                                         *
 *                                                                         *
 *     Description   : This function changes string to Mac format          *
 *                                                                         *
 *     Input(s)      : pu1String    : Pointer to the octet string.         *
 *                     pMac         : Pointer to the converted mac address *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       :  NULL                                               *
 *                                                                         *
 ***************************************************************************/

VOID
CliStrToMac (UINT1 *pu1String, UINT1 *pMac)
{
    UINT1               au1Temp[32];
    tMacAddress         tempMacAddr;
    tMacAddress         MacAddr;
    VOID               *pvTemp;

    if (!(pu1String) || !(pMac))
        return;

    MEMSET (au1Temp, 0, 32);

    CliDotStrToMac (pu1String, au1Temp);
    pvTemp = au1Temp;
    tempMacAddr.u4Dword = CRU_BMC_DWFROMPDU (pvTemp);
    pvTemp = &au1Temp[4];
    tempMacAddr.u2Word = (UINT2) CRU_BMC_WFROMPDU (pvTemp);

    MacAddr.u4Dword = OSIX_NTOHL (tempMacAddr.u4Dword);
    MacAddr.u2Word = (UINT2) OSIX_NTOHS (tempMacAddr.u2Word);

    MEMCPY (pMac, &MacAddr.u4Dword, sizeof (UINT4));
    MEMCPY (pMac + 4, &MacAddr.u2Word, sizeof (UINT2));
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliDotStrToStr                                      *
 *                                                                         *
 *     Description   : This function reads the Input Octet String          *
 *                     and converts to string for Storage.                 *
 *                     Hence while displaying the data, proper conversion  *
 *                     is ensured.                                         *
 *                                                                         *
 *     Input(s)      : pu1DotStr    : Pointer to the octet string.         *
 *                     pu1Val       : Pointer to the converted value.       *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       :  NULL                                               *
 *                                                                         *
 ***************************************************************************/

VOID
CliDotStrToStr (UINT1 *pu1DotStr, UINT1 *pu1Val)
{
    UINT1               u1Index = 0;
    UINT1              *pu1Temp = pu1DotStr;

    if (!(pu1DotStr) || !(pu1Val))
        return;
    for (u1Index = 0; u1Index < CliOctetLen (pu1DotStr); u1Index++)
    {
        UINT1               u1MacIndex = 0;
        UINT4               u4Value = 0;
        UINT1               u1Char;

        for (u1MacIndex = 0; ((*pu1Temp != ':') &&
                              (*pu1Temp != '\0')); u1MacIndex++)
        {
            if (ISXDIGIT (*pu1Temp))
            {
                if (!ISDIGIT (*pu1Temp))
                {
                    u1Char = (UINT1) (10 + ((*pu1Temp) - 'a'));
                }
                else
                {
                    u1Char = *pu1Temp;
                }
                u4Value = (u4Value * 16) + (0x0f & u1Char);
            }
            (pu1Temp)++;
        }
        (pu1Val)[u1Index] = (UINT1) u4Value;
        (pu1Temp)++;
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrToIp4Addr                                     *
 *                                                                         *
 *     Description   : This function reads the Input IPv4 address string   *
 *                     and converts it to IPv4 address format.             *
 *                                                                         *
 *     Input(s)      : pu1Str    : Pointer to the IPv4 address string.     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : pointer to the converted IPv4 address structure,    *
 *                     if, the string is a valid IPv4 address              *
 *                     else, NULL if it is an Invalid address.             *
 *                                                                         *
 ***************************************************************************/
VOID
CliStrToIp4Addr (UINT1 *pu1Str, UINT1 *pu1DstStr)
{
    UINT1               u1Char = '\0';
    if (pu1Str == NULL)
    {
        return;
    }
    SPRINTF ((CHR1 *) pu1DstStr, "%d.%d.%d.%d%c", pu1Str[0], pu1Str[1],
             pu1Str[2], pu1Str[3], u1Char);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrToIp6Addr                                     *
 *                                                                         *
 *     Description   : This function reads the Input IPv6 address string   *
 *                     and converts it to IPv6 address format.             *
 *                                                                         *
 *     Input(s)      : pu1Str    : Pointer to the IPv6 address string.     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : pointer to the converted IPv6 address structure,    *
 *                     if, the string is a valid IPv6 address              *
 *                     else, NULL if it is an Invalid address.             *
 *                                                                         *
 ***************************************************************************/

tIp6Addr           *
CliStrToIp6Addr (UINT1 *pu1Str)
{
    static tIp6Addr     Ip6Addr;
    CONST UINT1        *pu1TempStr = pu1Str;
    UINT4               u4Val = 0;
    int                 i2Ipv4Val[4];
    INT2                i2Count = 0, i2Index = 0;
    BOOL1               bIsLastColon = OSIX_FALSE;

    if (pu1Str == NULL)
        return (NULL);

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    /* quick and dirty tests for IPv4 compatible addresses */

    if (SSCANF ((CONST CHR1 *) pu1Str, "::%d.%d.%d.%d", &i2Ipv4Val[0],
                &i2Ipv4Val[1], &i2Ipv4Val[2], &i2Ipv4Val[3]) == 4)
    {
        for (i2Index = 0; i2Index < 4; i2Index++)
            Ip6Addr.u1_addr[i2Index + 12] = (UINT1) (i2Ipv4Val[i2Index] & 0xff);

        return (&Ip6Addr);
    }

    if (SSCANF ((CONST CHR1 *) pu1Str, "::%x:%d.%d.%d.%d",
                (unsigned int *) &u4Val, &i2Ipv4Val[0], &i2Ipv4Val[1],
                &i2Ipv4Val[2], &i2Ipv4Val[3]) == 5)
    {
        for (i2Index = 0; i2Index < 4; i2Index++)
            Ip6Addr.u1_addr[i2Index + 12] = (UINT1) (i2Ipv4Val[i2Index] & 0xff);

        Ip6Addr.u1_addr[10] = (UINT1) ((u4Val >> 8) & 0xff);
        Ip6Addr.u1_addr[11] = (UINT1) (u4Val & 0xff);

        return (&Ip6Addr);
    }

    for (pu1TempStr = pu1Str; *pu1TempStr != '\0'; pu1TempStr++)
        if (*pu1TempStr == ':')
            i2Count++;

    pu1TempStr = pu1Str;
    u4Val = 0;

    while ((*pu1TempStr != '\0') && (i2Index < 16))
    {
        if (*pu1TempStr == ':')
        {
            if (bIsLastColon == OSIX_TRUE)
            {
                i2Index = (INT2) (i2Index + 16 - 2 * i2Count);
                bIsLastColon = OSIX_FALSE;
            }
            else
            {
                Ip6Addr.u1_addr[i2Index++] = (UINT1) ((u4Val >> 8) & 0xff);
                if (i2Index < 16)
                {
                    Ip6Addr.u1_addr[i2Index++] = (UINT1) (u4Val & 0xff);
                }
                u4Val = 0;
                bIsLastColon = OSIX_TRUE;
            }
        }
        else
        {
            bIsLastColon = OSIX_FALSE;
            if ((*pu1TempStr >= '0') && (*pu1TempStr <= '9'))
            {
                u4Val = (UINT4) (u4Val << 4) | (UINT4) (*pu1TempStr - '0');
            }
            else if ((*pu1TempStr >= 'A') && (*pu1TempStr <= 'F'))
            {
                u4Val = (UINT4) (u4Val << 4) | (UINT4) (*pu1TempStr - 'A' + 10);
            }
            else if ((*pu1TempStr >= 'a') && (*pu1TempStr <= 'f'))
            {
                u4Val =
                    (UINT4) ((u4Val << 4) | (UINT4) (*pu1TempStr - 'a' + 10));
            }
            else
            {
                break;
            }
        }

        pu1TempStr++;
    }

    if (i2Index < 15)
    {
        Ip6Addr.u1_addr[i2Index++] = (UINT1) ((u4Val >> 8) & 0xff);
        Ip6Addr.u1_addr[i2Index++] = (UINT1) (u4Val & 0xff);
    }

    if (i2Index < 16)
        return (NULL);

    return (&Ip6Addr);

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliDotStrLength                                     *
 *                                                                         *
 *     Description   : This function reads the Input Octet string          *
 *                     and calculates its length.                          *
 *                                                                         *
 *     Input(s)      : pu1DotStr    : Pointer to the Octet string.         *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Length of the octet string.                         *
 *                                                                         *
 ***************************************************************************/

INT4
CliDotStrLength (UINT1 *pu1DotStr)
{
    INT4                i4Count = 0;
    UINT1              *pu1Char = pu1DotStr;
    INT4                i4Index = 0;

    if (pu1DotStr)
    {
        for (i4Index = 0; i4Index < (INT4) STRLEN (pu1DotStr); i4Index++)
        {
            if ((*pu1Char == ':') || (*pu1Char == '.'))
            {
                ++i4Count;
            }
            (pu1Char)++;
        }
        i4Count = i4Count + 1;
    }
    return i4Count;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliOctetLen                                         *
 *                                                                         *
 *     Description   : This function returns octet length                  *
 *                                                                         *
 *     Input(s)      : pu1DotStr    : Pointer to the octet string.         *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Length of the octets                                *
 *                                                                         *
 ***************************************************************************/

UINT4
CliOctetLen (UINT1 *pu1DotStr)
{
    UINT1              *pu1Temp = pu1DotStr;
    UINT4               u4Index = 0;
    UINT4               u4Count = 0;

    if (pu1DotStr)
    {
        for (u4Index = 0; u4Index < STRLEN (pu1DotStr); u4Index++)
        {
            if ((*pu1Temp != ':') && (*pu1Temp != '\0'))
            {
                ++u4Count;
            }
            pu1Temp++;
        }
        u4Count = (u4Count / 2);
    }
    return u4Count;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliHexStrToDecimal                                  *
 *                                                                         *
 *     Description   : This function converts hexa to integer              * 
 *                                                                         *
 *     Input(s)      : pu1Str     : Pointer to the HEX string              *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Decimal value of HEX string, if valid HEX string    *
 *                     else, HEX_FAILURE                                   *
 *                                                                         *
 ***************************************************************************/

INT4
CliHexStrToDecimal (UINT1 *pu1Str)
{
    INT4                i4Value = 0;
    UINT4               u4HexCount = 1;
    INT4                i4CurrValue = 0;
    UINT4               u4Count = 0;

    if (NULL != pu1Str)
    {
        u4Count = STRLEN (pu1Str);
    }

    if (!(pu1Str) || (*pu1Str != '0') ||
        (*(pu1Str + 1) != 'x') || (u4Count == 2))
    {
        return HEX_FAILURE;
    }

    while (u4Count > 2)
    {
        i4CurrValue = CliGetDecValue (*(pu1Str + u4Count - 1));

        if (i4CurrValue == HEX_FAILURE)
            return HEX_FAILURE;

        i4Value = (INT4) ((UINT4) i4Value + ((UINT4) i4CurrValue * u4HexCount));

        u4HexCount *= 16;

        u4Count--;
    }

    return (i4Value);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliHexStrToOctet                                    *
 *                                                                         *
 *     Description   : This function which will accept strings of both     *
 *                     MAC Addr. formats and converts it to MAC value      *
 *                     stores in an array(6) of UINT1 data pointer.        *
 *                                                                         *
 *     Input(s)      : pu1HexStr : Pointer to the Hex string               *
 *                     pu1Octet  : Pointer to the converted Octet string   *
 *                     i4Len     : Length of the Octet                     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliHexStrToOctet (UINT1 *pu1HexStr, UINT1 *pu1Octet, INT4 i4Len)
{
    UINT4               u4Value = 0;
    UINT1               u1Char;
    UINT1               u1Pow = 1;
    INT1                i1Index;
    INT1                i1CharCount = 1;
    INT1                i1StrIndex;

    if (!pu1HexStr)
        return;

    i1StrIndex = (INT1) (STRLEN (pu1HexStr) - 1);

    i1Index = (INT1) (i4Len - 1);

    MEMSET (pu1Octet, 0, i4Len);

    for (; (i1StrIndex >= 0) && (i1Index >= 0); i1StrIndex--, i1CharCount++)
    {
        if (ISXDIGIT (pu1HexStr[i1StrIndex]))
        {
            if (!ISDIGIT (pu1HexStr[i1StrIndex]))
            {
                u1Char = (UINT1) (10 + ((pu1HexStr[i1StrIndex]) - 'a'));
            }
            else
            {
                u1Char = pu1HexStr[i1StrIndex];
            }

            u4Value = (UINT4) ((0x0f & u1Char) * u1Pow) + u4Value;
            u1Pow = (UINT1) (u1Pow * 16);
        }
        (pu1Octet)[i1Index] = (UINT1) u4Value;

        if (!(i1CharCount % 2))
        {
            i1Index--;
            u4Value = 0;
            u1Pow = 1;
        }
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliOctetToHexStr                                    *
 *                                                                         *
 *     Description   : This function converts octet to hex string          * 
 *                                                                         *
 *                                                                         *
 *     Input(s)      : pOctet     : Pointer to the octet value             *
 *                     u4OctetLen : Length of the given octet string       *
 *                     pu1HexStr  : Pointer to the converted hex string    *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliOctetToHexStr (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1HexStr)
{
    UINT4               u4Len;

    if (!(pOctet) || !(pu1HexStr))
        return;

    for (u4Len = 0; u4Len < u4OctetLen; u4Len++)
    {
        SPRINTF ((CHR1 *) pu1HexStr, "%02x", pOctet[u4Len]);
        pu1HexStr += 2;
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetDecValue                                      *
 *                                                                         *
 *     Description   : This function gets decimal value of hexa character  * 
 *                                                                         *
 *     Input(s)      : u1Char    : Hex char                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Decimal value of the given HEX char                 *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetDecValue (UINT1 u1Char)
{
    INT4                i4DecValue = 0;

    if ((u1Char >= '0') && (u1Char <= '9'))
        i4DecValue = u1Char - '0';
    else
    {
        switch (u1Char)
        {
            case 'a':
            case 'A':
                i4DecValue = 10;
                break;
            case 'b':
            case 'B':
                i4DecValue = 11;
                break;
            case 'c':
            case 'C':
                i4DecValue = 12;
                break;
            case 'd':
            case 'D':
                i4DecValue = 13;
                break;
            case 'e':
            case 'E':
                i4DecValue = 14;
                break;
            case 'f':
            case 'F':
                i4DecValue = 15;
                break;
            default:
                i4DecValue = -1;
        }
    }
    return i4DecValue;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetServPortByName                                *
 *                                                                         *
 *     Description   : This function returns the port number of the        *
 *                     given application name                              *
 *                                                                         *
 *     Input(s)      : pu1App    : Pointer to the application name string  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Port number associated with the given application   *
 *                     if, pu1App. is a known application else, -1         *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetServPortByName (UINT1 *pu1App)
{
    INT4                i4Index;
    INT4                i4RetPort = INVALID_PORT;

    if (pu1App)
    {
        for (i4Index = 0; i4Index < MAX_SERVICE_TOKENS; i4Index++)
        {
            if (STRCASECMP (pu1App,
                            UtlServTokenType[i4Index].pServiceName) == 0)
            {
                i4RetPort = UtlServTokenType[i4Index].i4Port;
                break;
            }
        }
    }
    return i4RetPort;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrNCaseCmp                                      *
 *                                                                         *
 *     Description   : This function compares the two strings, ignoring    *
 *                     the  case of the characters, but compares only      *
 *                     'i4Len' characters of the two strings.              *
 *                                                                         *
 *     Input(s)      : pi1Str1, pi1Str2 : Pointer to the strings to be     *
 *                                        compared                         *
 *                     i4Len            : No. of characters to be compared *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : if equal will return 0                              *
 *                     else nonzero                                        * 
 *                                                                         *
 ***************************************************************************/

INT1
CliStrNCaseCmp (INT1 *pi1Str1, INT1 *pi1Str2, INT4 i4Len)
{
    INT4                i4Index;

    if (((INT4) STRLEN (pi1Str1) < i4Len) || ((INT4) STRLEN (pi1Str2) < i4Len))
    {
        return -1;
    }

    for (i4Index = 0; i4Index < i4Len; i4Index++)
    {
        if (TOLOWER (pi1Str1[i4Index]) != TOLOWER (pi1Str2[i4Index]))
        {
            return -1;
        }
    }
    return 0;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : CliSetMemberPort                                     *
 *                                                                          *
 *     Description   : The function is used to set port bits of a port list *
 *                                                                          *
 *     Input(s)      : pu1PortArray    : Pointer to the port list array.    *
 *                     u4PortArrayLen  : Length of the array.               *
 *                     u4Port          : Port number to set.                *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : OSIX_SUCCESS, if u4Port is less than                 *
 *                     given byte length of pu1PortArray,                   *
 *                     else OSIX_FAILURE.                                   *
 *                                                                          *
 ****************************************************************************/

INT4
CliSetMemberPort (UINT1 *pu1PortArray, UINT4 u4PortArrayLen, UINT4 u4Port)
{
    UINT4               u4PortBytePos;
    UINT4               u4PortBitPos;
    INT4                i4RetVal = OSIX_FAILURE;
    if (u4Port)
    {
        u4PortBytePos = (UINT4) (u4Port / CLI_PORTS_PER_BYTE);
        u4PortBitPos = (UINT4) (u4Port % CLI_PORTS_PER_BYTE);
        if (u4PortBitPos == 0)
        {
            u4PortBytePos -= 1;
        }
        if ((pu1PortArray) && (u4PortBytePos < u4PortArrayLen))
        {
            pu1PortArray[u4PortBytePos] =
                (UINT1) (pu1PortArray[u4PortBytePos]
                         | gau1UtlPortBitMask[u4PortBitPos]);
            i4RetVal = OSIX_SUCCESS;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : CliIsMemberPort                                      *
 *                                                                          *
 *     Description   : The function is used to check if a the bit for the   *
 *                     given port number is set or not.                     *
 *                                                                          *
 *     Input(s)      : pu1PortArray    : Pointer to the port list array.    *
 *                     u4PortArrayLen  : Length of the array.               *
 *                     u4Port          : Port number to check.              *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : OSIX_SUCCESS, if u4Port bit is set                   *
 *                     else OSIX_FAILURE.                                   *
 *                                                                          *
 ****************************************************************************/

INT4
CliIsMemberPort (UINT1 *pu1PortArray, UINT4 u4PortArrayLen, UINT4 u4Port)
{
    UINT4               u4PortBytePos;
    UINT4               u4PortBitPos;
    INT4                i4RetVal = OSIX_FAILURE;

    u4PortBytePos = (UINT4) (u4Port / CLI_PORTS_PER_BYTE);
    u4PortBitPos = (UINT4) (u4Port % CLI_PORTS_PER_BYTE);

    if (u4PortBitPos == 0)
    {
        u4PortBytePos -= 1;
    }
    if ((pu1PortArray) && (u4PortBytePos < u4PortArrayLen) &&
        ((pu1PortArray[u4PortBytePos] & gau1UtlPortBitMask[u4PortBitPos]) != 0))
    {
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetToken                                         *
 *                                                                         *
 *     Description   : The function is used to extract a string between    *
 *                     any of the specified character in the delimiter     *
 *                     string.                                             *
 *                                                                         *
 *     Input(s)      : pi1Str          : Pointer to the string.            *
 *                     pi1Delimit      : Pointer to the delimiter string   *
 *                     pi1ValidDelimit : Pointer to the valid characters   *
 *                                       that can be present in pi1Str     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : pointer to any of the delimiter char in the given   *
 *                     input string, if any of the delimiter char from the *
 *                     delimiter string is present, or NULL                *
 *                                                                         *
 ***************************************************************************/
INT1               *
CliGetToken (INT1 *pi1Str, CONST CHR1 * pi1Delimit,
             CONST CHR1 * pi1ValidDelimit)
{
    INT1               *pi1Base = pi1Str;
    if (pi1Str)
    {
        while ((*pi1Str))
        {
            /* check for delimit char */
            if (CliIsDelimit (*pi1Str, pi1Delimit))
                break;
            /* Check for valid inputs in the list string */
            if (!(CliIsDelimit (*pi1Str, pi1ValidDelimit)))
            {
                pi1Str = NULL;
                break;
            }
            pi1Str++;
        }
        if (pi1Str == pi1Base)
            pi1Str = NULL;
    }
    return pi1Str;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetVal                                           *
 *                                                                         *
 *     Description   : The function is used to extract values from the     *
 *                     port list string by identifying the type of port    *
 *                     list string.                                        *
 *                                                                         *
 *     Input(s)      : pCliPortList : pointer to the structure for         *  
 *                                    returning port list values.          *
 *                     pi1Str       : Pointer to the string.               *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : The type of the port list string.                   *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetVal (tCliPortList * pCliPortList, INT1 *pi1Str)
{
    INT1               *pi1Base = pi1Str;
    INT4                i4RetType = CLI_LIST_TYPE_INV;
    INT4                i4RetValue = 0;
    UINT4               pu4IfIndex;

    UNUSED_PARAM (i4RetValue);

    if ((pi1Str) && (*pi1Str))
    {
        /* Check for Range delimiter */
        pi1Str = CliGetToken (pi1Base, CLI_RANGE_DELIMIT, CLI_VALIDPORT_LIST);
        if (!pi1Str)
        {
            i4RetType = CLI_LIST_TYPE_INV;
        }
        else if (*pi1Str)
        {
            pCliPortList->uPortList.PortListRange.u4PortFrom =
                (UINT4) ATOI (pi1Base);

            pi1Base = pi1Str + 1;
            /* Check again for Range delimiter, 
             * if present it is an invalid port list string
             */
            pi1Str = CliGetToken (pi1Base,
                                  CLI_RANGE_DELIMIT, CLI_VALIDPORT_LIST);

            if ((!pi1Str) || (*pi1Str))
            {
                i4RetType = CLI_LIST_TYPE_INV;
            }
            else
            {
                i4RetType = CLI_LIST_TYPE_RANGE;
                pCliPortList->uPortList.PortListRange.u4PortTo =
                    (UINT4) ATOI (pi1Base);
            }
        }

        /* Check for LA Port delimiter */
        else if (*pi1Base == 'p')
        {
            if (*(pi1Base + 1) == 'o')
            {
                pi1Str = pi1Base + 2;
                i4RetValue =
                    CfaCliGetIndexFromType (CFA_LAGG, pi1Str, &pu4IfIndex);
                i4RetType = CLI_LIST_TYPE_VAL;
                pCliPortList->uPortList.u4PortNum = pu4IfIndex;
            }

        }

        else
        {
            i4RetType = CLI_LIST_TYPE_VAL;
            pCliPortList->uPortList.u4PortNum = (UINT4) ATOI (pi1Base);
        }
    }
    return i4RetType;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetSlotAndPort                                   *
 *                                                                         *
 *     Description   : The function is used to extract values from the     *
 *                     port list string by getting the slot and port       *
 *                     number values and validates the Slot Number.        *
 *                                                                         *
 *     Input(s)      : pCliIfaceList : pointer to the structure for        *  
 *                                    returning port list values.          *
 *                     pi1Str       : Pointer to the string.               *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : The type of the port list string.                   *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetSlotAndPort (tCliIfaceList * pCliIfaceList, INT1 *pi1Str)
{
    INT1               *pi1Base = pi1Str;
    INT1                i1TempChar;
    INT4                i4RetType = CLI_LIST_TYPE_INV;
    INT4                i4SlotNum = -1;

    /* Check for CLI_SLOT_PORT_DELIMIT delimiter */
    pi1Str = CliGetToken (pi1Base, CLI_SLOT_PORT_DELIMIT, CLI_VALIDIFACE_LIST);
    if (!(pi1Str) || !(*pi1Str))
    {
        i4RetType = CLI_LIST_TYPE_INV;
    }
    else
    {
        i1TempChar = *pi1Str;
        *pi1Str = '\0';

        i4SlotNum = ATOI (pi1Base);

        *pi1Str = i1TempChar;

        pi1Base = pi1Str + 1;
        pi1Str =
            CliGetToken (pi1Base, CLI_SLOT_PORT_DELIMIT, CLI_VALIDIFACE_LIST);
        if (!(pi1Str) || (*pi1Str))
        {
            i4RetType = CLI_LIST_TYPE_INV;
        }
        else
        {
            pCliIfaceList->i4SlotNum = i4SlotNum;
            pCliIfaceList->uPortList.u4PortNum = (UINT4) ATOI (pi1Base);
            i4RetType = CLI_LIST_TYPE_VAL;
        }
    }
    return i4RetType;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliOctetToPort                                      *
 *                                                                         *
 *     Description   : This function converts octet to ports               *
 *                                                                         *
 *     Input(s)      : pOctet       : Pointer to the octet value           *
 *                     u4OctetLen   : Length of the given octet string     *
 *                     pu1Temp      : Pointer to converted octet string    *
 *                     u4BufTempLen : Length of the given octet string     *
 *                     u4MaxPorts   : Max no. of ports.                    *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliOctetToPort (UINT1 *pOctet, UINT4 u4OctetLen,
                UINT1 *pu1Temp, UINT4 u4TempBufLen, UINT4 u4MaxPorts)
{
    UINT1               au1Str[17];
    UINT4               u4Port = 0;
    UINT4               u4BufLen = 0;

    if ((pOctet) && (pu1Temp))
    {
        *pu1Temp = '\0';
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pOctet, u4OctetLen, u4Port) == OSIX_SUCCESS)
            {
                SPRINTF ((CHR1 *) au1Str, "%d,", u4Port);
                u4BufLen += STRLEN (au1Str);

                if (u4BufLen >= u4TempBufLen)
                    break;

                STRCAT (pu1Temp, au1Str);
            }
        }
        if (u4BufLen)
        {
            pu1Temp[u4BufLen - 1] = '\0';
        }

        STRCAT (pu1Temp, "\t");
    }
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliOctetToStr                                       *
 *                                                                         *
 *     Description   : This function converts octet to string              *
 *                                                                         *
 *     Input(s)      : pOctet     : Pointer to the octet value             *
 *                     u4OctetLen : Length of the given octet string.      *
 *                     pu1Temp    : Pointer to the converted octet string. *
 *                     u4BufLen   : Length of the output buffer.           *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliOctetToStr (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp, UINT4 u4BufLen)
{
    UINT1               au1Str[10];
    UINT4               u4Len;
    UINT4               u4TempBufLen = 0;

    if ((pOctet) && (pu1Temp))
    {
        *pu1Temp = '\0';
        for (u4Len = 0; u4Len < u4OctetLen; u4Len++)
        {
            if (pOctet[u4Len] < 16)
            {
                SPRINTF ((CHR1 *) au1Str, "0%x", pOctet[u4Len]);
            }
            else
            {
                SPRINTF ((CHR1 *) au1Str, "%x", pOctet[u4Len]);
            }
            if (u4Len != (u4OctetLen - 1))
            {
                STRCAT (au1Str, ":");
            }
            if (STRLEN (pu1Temp) + STRLEN (au1Str) >= u4BufLen)
            {
                continue;
            }
            u4TempBufLen += STRLEN (au1Str);
            if (u4TempBufLen >= u4BufLen)
            {
                u4TempBufLen -= STRLEN (au1Str);
                break;
            }
            if (sizeof (pu1Temp) > 9)
            {
                continue;
            }
            STRCAT (pu1Temp, au1Str);
        }
        /*
         * The remaining buffer length needs to be validated before STRCAT into
         * pu1Temp to avoid segmentation fault.
         * */
        if ((u4BufLen - u4TempBufLen) == 1)
        {
            STRCAT (pu1Temp, "");
        }
        else if ((u4BufLen - u4TempBufLen) == 2)
        {
            STRCAT (pu1Temp, " ");
        }
        else if ((u4BufLen - u4TempBufLen) >= 3)
        {
            STRCAT (pu1Temp, "  ");
        }
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetFlashFileName                                 *
 *                                                                         *
 *     Description   : This function parses the given flash_url token and  *
 *                     if valid,returns the Flash filename                 *
 *                                                                         *
 *     Input(s)      : pi1FlashStr : String to validate                    *
 *                     pi1FlashFileName : Pointer to the return file name  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetFlashFileName (INT1 *pi1FlashStr, INT1 *pi1FlashFileName)
{
    INT1               *pi1TempToken = pi1FlashStr;
    INT1                ai1UrlStart[] = "flash:";
    INT1                i1UrlLen;

    if (!(pi1FlashStr) || !(*pi1FlashStr))
        return CLI_FAILURE;

    /* Format should be flash:<string_filename> */

    i1UrlLen = (INT1) (STRLEN (ai1UrlStart));
    if (STRNCASECMP (ai1UrlStart, pi1FlashStr, i1UrlLen))
    {
        return CLI_FAILURE;
    }
    pi1TempToken = pi1FlashStr + i1UrlLen;

    if (!(*pi1TempToken))
        return CLI_FAILURE;


    if (pi1FlashFileName != NULL)
    {
        STRCPY (pi1FlashFileName, pi1TempToken);
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetTftpParams                                    *
 *                                                                         *
 *     Description   : This function parses the given tftp_url token and   *
 *                     if valid,returns ip address & filename              *
 *                                                                         *
 *     Input(s)      : pi1FlashStr : String to validate                    *
 *                     pi1FlashFileName : Pointer to the return file name  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetTftpParams (INT1 *pi1TftpStr, INT1 *pi1TftpFileName,
                  tIPvXAddr * pIpAddress, UINT1 *pu1HostName)
{
    tUtlInAddr          UtlInAddr;
    tUtlIn6Addr         UtlIn6Addr;
    tIp6Addr            TempAddr;
    UINT4               u4IpAddr = 0;
    UINT4               u4Index = 0;
    INT1               *pi1TempToken = pi1TftpStr;
    INT1                ai1UrlStart[] = "tftp://";
    INT1                i1UrlLen;
    UINT1               u1Afi = 0;
    UINT1               u1TempLen = 0;

    if (!(pi1TftpStr) || !(*pi1TftpStr))
        return CLI_FAILURE;

    /* Format should be tftp://<ip_addr>/<string_filename> */
    i1UrlLen = (INT1) (STRLEN (ai1UrlStart));
    if (STRNCASECMP (ai1UrlStart, pi1TftpStr, i1UrlLen))
    {
        return CLI_FAILURE;
    }
    pi1TempToken = pi1TftpStr + i1UrlLen;

    while (!(CliIsDelimit (pi1TempToken[u4Index], "/")))
    {
        if (!(pi1TempToken[u4Index]))
            return CLI_FAILURE;
        u4Index++;
    }
    pi1TempToken[u4Index] = '\0';

    /* The filename is mandatory */
    if (pi1TempToken[u4Index + 1] == '\0')
        return CLI_FAILURE;

    MEMSET (&UtlInAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&UtlIn6Addr, 0, sizeof (tUtlIn6Addr));
    /* checks for correct format of Unicast Ip Address with dotted notation */

    if (UtlInetAton ((CONST CHR1 *) pi1TempToken, &UtlInAddr))
    {
        pi1TempToken[u4Index] = '/';
        u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV4;
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        if (((u4IpAddr & 0xff000000) >> 24) >= 224)
        {
            return CLI_FAILURE;
        }
    }
    else if (UtlInetAton6 ((CONST CHR1 *) pi1TempToken, &UtlIn6Addr))
    {
        pi1TempToken[u4Index] = '/';
        u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV6;
        MEMCPY (TempAddr.u1_addr, UtlIn6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }
    else if(STRLEN(pi1TempToken) <= DNS_MAX_QUERY_LEN)

    {
        if (CliUtilValidateHostName (pi1TempToken) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u1TempLen = (UINT1)STRLEN (pi1TempToken);
        pi1TempToken[u4Index] = '/';
        u1Afi = (UINT1)IPVX_DNS_FAMILY;
        if (NULL != pu1HostName)
        {
            STRNCPY (pu1HostName,(CONST CHR1 *) pi1TempToken, u1TempLen);
            pu1HostName[u1TempLen] = '\0'; 
            *pu1HostName = UtilStrToLower ((UINT1 *)pu1HostName);
        }
    }
    else
    {
        pi1TempToken[u4Index] = '/';
        return CLI_FAILURE;
    }

    if (STRLEN (&pi1TempToken[u4Index + 1]) >= MAX_TFTP_FILENAME_LEN)
        return CLI_FAILURE;

    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            return CLI_FAILURE;
        }
        if (pIpAddress != NULL)
        {
            pIpAddress->u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV4;
            pIpAddress->u1AddrLen = (UINT1)IPVX_IPV4_ADDR_LEN;
            u4IpAddr = OSIX_NTOHL (u4IpAddr);
            MEMCPY (pIpAddress->au1Addr, &u4IpAddr, IPVX_IPV4_ADDR_LEN);
        }
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (pIpAddress != NULL)
        {
            pIpAddress->u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV6;
            pIpAddress->u1AddrLen = (UINT1)IPVX_IPV6_ADDR_LEN;
            MEMCPY (pIpAddress->au1Addr, &UtlIn6Addr, IPVX_IPV6_ADDR_LEN);
            MEMCPY (&TempAddr, pIpAddress->au1Addr, IPVX_IPV6_ADDR_LEN);
        }
        if (IS_ADDR_UNSPECIFIED (TempAddr) || IS_ADDR_MULTI (TempAddr) ||
            IS_ADDR_LOOPBACK (TempAddr))
        {
            /* Address is Loop back adress of 127.x.x.x */
            return CLI_FAILURE;
        }
    }
    else if(u1Afi == IPVX_DNS_FAMILY)
    {
	if ((pIpAddress != NULL) && (pu1HostName != NULL))
	{
	    pIpAddress->u1Afi = (UINT1)IPVX_DNS_FAMILY;
	    pIpAddress->u1AddrLen = (UINT1)STRLEN(pu1HostName); 
	}
    }

    if (pi1TftpFileName != NULL)
    {
        STRNCPY (pi1TftpFileName, &pi1TempToken[u4Index + 1], STRLEN(&pi1TempToken[u4Index + 1]));
	pi1TftpFileName[STRLEN(&pi1TempToken[u4Index + 1])] = '\0';
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetSftpParams                                    *
 *                                                                         *
 *     Description   : This function parses the given Sftp_url token and   *
 *                     if valid,returns ip address & filename              *
 *                                                                         *
 *     Input(s)      : pi1FlashStr : String to validate                    *
 *                     pi1FlashFileName : Pointer to the return file name  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetSftpParams (INT1 *pi1SftpStr, INT1 *pi1SftpFileName,
                  INT1 *pi1SftpUserName, INT1 *pi1SftpPassWd,
                  tIPvXAddr * pIpAddress, UINT1 *pu1HostName)
{

    tUtlInAddr          UtlInAddr;
    tUtlIn6Addr         UtlIn6Addr;
    tIp6Addr            TempAddr;
    UINT4               u4IpAddr = 0;
    UINT4               u4Index = 0;
    UINT4               u4TempIndex = 0;
    UINT4               u4Length = 0;
    INT1               *pi1TempToken = pi1SftpStr;
    INT1                ai1UrlStart[] = "sftp://";
    INT1                i1UrlLen;
    UINT1               u1Afi = 0;
    UINT1 	        	u1TempLen = 0;
    UINT1               au1Temp[DNS_MAX_QUERY_LEN];

    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
    MEMSET (&UtlIn6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1Temp, 0, DNS_MAX_QUERY_LEN);

    if (!(pi1SftpStr) || !(*pi1SftpStr))
        return CLI_FAILURE;

    /* Format should be 
       sftp://<user_name>:<pass_word>@<ip_addr>/<string_filename> */

    i1UrlLen = (INT1) STRLEN (ai1UrlStart);

    if (STRNCASECMP (ai1UrlStart, pi1SftpStr, i1UrlLen))
    {
        return CLI_FAILURE;
    }

    pi1TempToken = pi1SftpStr + i1UrlLen;

    while (!(CliIsDelimit (pi1TempToken[u4Index], ":")))
    {
        if (!(pi1TempToken[u4Index]))
        {
            return CLI_FAILURE;
        }
        u4Index++;
    }

    pi1TempToken[u4Index] = '\0';

    u4Length = CLI_STRLEN (pi1TempToken);

    /* Restrict the length to MAX_LINE_LEN characters of User Name */
    if (u4Length > MAX_LINE_LEN - 1)
    {
        pi1TempToken[u4Index] = ':';

        return CLI_FAILURE;
    }

    if (pi1SftpUserName != NULL)
    {
        STRNCPY (pi1SftpUserName, &pi1TempToken[0], STRLEN(&pi1TempToken[0]));
	pi1SftpUserName[STRLEN(&pi1TempToken[0])] = '\0';
    }

    /* Check the Presence of Password */
    if (pi1TempToken[u4Index + 1] == '\0')
    {
        pi1TempToken[u4Index] = ':';
        return CLI_FAILURE;
    }
    else
    {
        pi1TempToken[u4Index] = ':';

    }

    u4Index = u4Index + 1;
    u4TempIndex = u4Index;

    while (!(CliIsDelimit (pi1TempToken[u4Index], "@")))
    {
        if (!(pi1TempToken[u4Index]))
        {
            return CLI_FAILURE;
        }
        u4Index++;
    }

    pi1TempToken[u4Index] = '\0';

    u4Length = CLI_STRLEN (pi1TempToken);

    /* Restrict the length to MAX_LINE_LEN characters of Password */
    if (u4Length > MAX_LINE_LEN - 1)
    {
        pi1TempToken[u4Index] = '@';

        return CLI_FAILURE;
    }

    if (pi1SftpPassWd != NULL)
    {
        STRNCPY (pi1SftpPassWd, &pi1TempToken[u4TempIndex], STRLEN(&pi1TempToken[u4TempIndex]));
	pi1SftpPassWd[STRLEN(&pi1TempToken[u4TempIndex])] = '\0';
    }

    /* Check the Presence of IP Address */
    if (pi1TempToken[u4Index + 1] == '\0')
    {
        pi1TempToken[u4Index] = '@';

        return CLI_FAILURE;
    }
    else
    {
        pi1TempToken[u4Index] = '@';
    }

    u4Index = u4Index + 1;
    u4TempIndex = u4Index;

    while (!(CliIsDelimit (pi1TempToken[u4Index], "/")))
    {
        if (!(pi1TempToken[u4Index]))
        {
            return CLI_FAILURE;
        }
        u4Index++;
    }

    pi1TempToken[u4Index] = '\0';

    /* Check the Presence of Valid IP Address */

    if (UtlInetAton ((CONST CHR1 *) & pi1TempToken[u4TempIndex], &UtlInAddr))
    {
        pi1TempToken[u4Index] = '/';
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV4;
        if (((u4IpAddr & 0xff000000) >> 24) >= 224)
        {
            return CLI_FAILURE;
        }
    }
    else if (UtlInetAton6 ((CONST CHR1 *) & pi1TempToken[u4TempIndex],
                           &UtlIn6Addr))
    {
        pi1TempToken[u4Index] = '/';
        u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV6;
        MEMCPY (TempAddr.u1_addr, UtlIn6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }
    else if(STRLEN(pi1TempToken) <= DNS_MAX_QUERY_LEN)
    {
        u1Afi = (UINT1)IPVX_DNS_FAMILY;
        u1TempLen = (UINT1) (STRLEN(pi1TempToken) - u4TempIndex);
        
        if (NULL != pu1HostName)
        {
            STRNCPY (pu1HostName,(CONST CHR1 *)& pi1TempToken[u4TempIndex], u1TempLen);
            pu1HostName[u1TempLen] = '\0' ;
            *pu1HostName = UtilStrToLower ((UINT1 *)pu1HostName);
        }

        STRNCPY (au1Temp,(CONST CHR1 *)& pi1TempToken[u4TempIndex],u1TempLen);
        
        if (CliUtilValidateHostName ((INT1 *)au1Temp) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        pi1TempToken[u4Index] = '/';
    }
    else
    {
        pi1TempToken[u4Index] = '/';

        return CLI_FAILURE;
    }

    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            return CLI_FAILURE;
        }
        if (pIpAddress != NULL)
        {
            pIpAddress->u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV4;
            pIpAddress->u1AddrLen = (UINT1)IPVX_IPV4_ADDR_LEN;
            u4IpAddr = OSIX_NTOHL (u4IpAddr);
            MEMCPY (pIpAddress->au1Addr, &u4IpAddr, IPVX_IPV4_ADDR_LEN);
        }
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (pIpAddress != NULL)
        {
            pIpAddress->u1Afi = (UINT1)IPVX_ADDR_FMLY_IPV6;
            pIpAddress->u1AddrLen = (UINT1)IPVX_IPV6_ADDR_LEN;
            MEMCPY (pIpAddress->au1Addr, &UtlIn6Addr, IPVX_IPV6_ADDR_LEN);
            MEMCPY (&TempAddr, pIpAddress->au1Addr, IPVX_IPV6_ADDR_LEN);
        }
        if (IS_ADDR_UNSPECIFIED (TempAddr) || IS_ADDR_MULTI (TempAddr) ||
            IS_ADDR_LOOPBACK (TempAddr))
        {
            /* Address is Loop back adress of 127.x.x.x */
            return CLI_FAILURE;
        }
    }
    else if (u1Afi == IPVX_DNS_FAMILY)
    {
        if ((pIpAddress != NULL) && (pu1HostName != NULL))
        {
            pIpAddress->u1Afi = (UINT1)IPVX_DNS_FAMILY;
            pIpAddress->u1AddrLen = (UINT1)STRLEN(pu1HostName);
        }
    }

    /* Check the Presence of IP Address */
    if (pi1TempToken[u4Index + 1] == '\0')
    {
        return CLI_FAILURE;
    }

    u4Index = u4Index + 1;
    u4TempIndex = u4Index;

    if (STRLEN (&pi1TempToken[u4TempIndex]) >= MAX_TFTP_FILENAME_LEN)
    {

        return CLI_FAILURE;
    }

    if (pi1SftpFileName != NULL)
    {
        STRNCPY (pi1SftpFileName, &pi1TempToken[u4TempIndex], STRLEN(&pi1TempToken[u4TempIndex]));
	pi1SftpFileName[STRLEN(&pi1TempToken[u4TempIndex])] = '\0';
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetMaskBits                                      *
 *                                                                         *
 *     Description   : This function is invoked to convert net mask value  *
 *                     in to number of mask bits                           *
 *                                                                         *
 *     Input(s)      : u4SubnetMask - Net mask value                       *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : Number of mask bits                                 *
 *                                                                         *
 ***************************************************************************/

UINT4
CliGetMaskBits (UINT4 u4SubnetMask)
{
    UINT4               u4MaskBits;
    UINT4               u4NetMask = 0x80000000;

    for (u4MaskBits = 0; u4MaskBits < MAX_ADDR_LEN; u4MaskBits++)
    {
        if (u4SubnetMask & u4NetMask)
        {
            u4SubnetMask ^= u4NetMask;
            u4NetMask >>= 1;
        }
        else
        {
            break;
        }
    }
    if (u4SubnetMask)
        return 0;
    return (u4MaskBits);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : ConvertStrToPortList                                *
 *                                                                         *
 *     Description   : The function is used by other modules to            *
 *                     convert the given string to port list.              *
 *                                                                         *
 *     Input(s)      : pu1Str         : Pointer to the string.             *
 *                     u4PortArrayLen : Array Length.                      *
 *                     u4MaxPorts     : Max no. of ports.                  *
 *                                                                         *
 *     Output(s)     : pu1PortArray   : Pointer to the string in which the *
 *                                      bits for the port list will be set *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
ConvertStrToPortList (UINT1 *pu1Str, UINT1 *pu1PortArray,
                      UINT4 u4PortArrayLen, UINT4 u4MaxPorts)
{
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetType;
    UINT4               u4Val1;
    UINT4               u4Val2;
    INT1                i1EndFlag = OSIX_FALSE;

    if (!(pi1Temp) || !(pu1PortArray))
        i4RetStatus = OSIX_FAILURE;

    while ((i4RetStatus != OSIX_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDPORT_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (!(*pi1Pos))
            i1EndFlag = OSIX_TRUE;

        *pi1Pos = '\0';

        /* Get values between the list seperaters based on the type */
        i4RetType = CliGetVal (&CliPortList, pi1Temp);

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                u4Val1 = CliPortList.uPortList.u4PortNum;
                u4Val2 = CliPortList.uPortList.u4PortNum;
                break;
            case CLI_LIST_TYPE_RANGE:
                u4Val1 = CliPortList.uPortList.PortListRange.u4PortFrom;
                u4Val2 = CliPortList.uPortList.PortListRange.u4PortTo;
                break;
            default:
                i4RetStatus = OSIX_FAILURE;
                continue;
        }

        if (u4Val1 > u4Val2)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }
        if (pu1PortArray != NULL)
        {

            for (; u4Val1 <= u4Val2; u4Val1++)
            {
                if ((u4Val1 > u4MaxPorts) || (u4Val1 == PORT_ZERO) ||
                    (CliSetMemberPort (pu1PortArray, u4PortArrayLen, u4Val1)
                     != OSIX_SUCCESS))
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                /* Set the member port bit in pu1PortArray */
            }
        }
        pi1Temp = pi1Pos + 1;
    }
    return i4RetStatus;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : ConvertStrToPortArray                               *
 *                                                                         *
 *     Description   : This function is invoked to convert the given       *
 *                     string to array of ports                            *
 *                                                                         *
 *     Input(s)      : pu1Str - Input String                               *
 *                     u4PortArrayLen - output port array length           *
 *                     u4MaxPorts     - Max. no of ports                   *
 *                                                                         *
 *     Output(s)     : pu4PortArray - output port array                    *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
ConvertStrToPortArray (UINT1 *pu1Str, UINT4 *pu4PortArray,
                       UINT4 u4PortArrayLen, UINT4 u4MaxPorts)
{
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetType;
    UINT4               u4Val1;
    UINT4               u4Val2;
    UINT4               u4Count = 0;
    INT1                i1EndFlag = OSIX_FALSE;

    if (!(pi1Temp) || !(pu4PortArray))
        i4RetStatus = OSIX_FAILURE;

    while ((i4RetStatus != OSIX_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDPORT_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (!(*pi1Pos))
            i1EndFlag = OSIX_TRUE;

        *pi1Pos = '\0';

        /* Get values between the list seperaters based on the type */
        i4RetType = CliGetVal (&CliPortList, pi1Temp);

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                u4Val1 = CliPortList.uPortList.u4PortNum;
                u4Val2 = CliPortList.uPortList.u4PortNum;
                break;
            case CLI_LIST_TYPE_RANGE:
                u4Val1 = CliPortList.uPortList.PortListRange.u4PortFrom;
                u4Val2 = CliPortList.uPortList.PortListRange.u4PortTo;
                break;
            default:
                i4RetStatus = OSIX_FAILURE;
                continue;
        }

        if (u4Val1 > u4Val2)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (pu4PortArray != NULL)
        {
            for (; u4Val1 <= u4Val2; u4Val1++)
            {

                if ((u4Val1 > u4MaxPorts) ||
                    (u4Val1 == PORT_ZERO) || (u4PortArrayLen < u4Count))
                {

                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                /* Set the member port bit in pu4PortArray */
                pu4PortArray[u4Count] = u4Val1;
                u4Count++;
            }
        }
        pi1Temp = pi1Pos + 1;
    }
    return i4RetStatus;
}

/***************************************************************************
 **                                                                        *
 **     Function Name : CliGetCustFileName                                 *
 **                                                                        *
 **     Description   : This function parses the given Cust_url token and  *
 **                     if valid,returns the Cust filename and file path   *
 **                                                                        *
 **     Input(s)      : pi1CustStr : String to validate                    *
 **                     pi1CustFileName : Pointer to the return file name  *
 **                     pi1CustFilePath : Pointer to the return file path  *
 **                                                                        *
 **     Output(s)     : NULL                                               *
 **                                                                        *
 **     Returns       : CLI_SUCCESS/CLI_FAILURE                            *
 **                                                                        *
 ***************************************************************************/
INT4
CliGetCustFileName (INT1 *pi1CustStr, INT1 *pi1CustFileName,
                    INT1 *pi1CustFilePath)
{
    INT1               *pi1TempToken = pi1CustStr;
    INT1               *pi1Token = NULL;
    INT1                ai1UrlStart[] = "cust:";
    INT4                i1UrlLen = 0;

    if (!(pi1CustStr) || !(*pi1CustStr))
        return CLI_FAILURE;

    /* Format should be cust:</path/string_filename> */

    i1UrlLen = (UINT1) STRLEN (ai1UrlStart);
    if (STRNCASECMP (ai1UrlStart, pi1CustStr, (INT4) i1UrlLen))
    {
        return CLI_FAILURE;
    }

    pi1TempToken = pi1CustStr + i1UrlLen;
    pi1Token = pi1TempToken;
    if (!(*pi1TempToken))
        return CLI_FAILURE;

    if (STRLEN (pi1TempToken) >= (MAX_FILEPATH_LEN + MAX_FLASH_FILENAME_LEN))
        return CLI_FAILURE;

    i1UrlLen = (INT1) STRLEN (pi1TempToken) - 1;
    /* pi1TempToken is holding the string which have path and file name  
       if `/` present at the end of the string indicates no filename 
       * always should be path in this format /path/filename
       * */

    if (pi1TempToken[i1UrlLen] == '/')
    {

        return CLI_FAILURE;
    }

    /* get the position of last ocuurence of  `/` */
    while ((pi1TempToken[i1UrlLen] != '/') && (i1UrlLen >= 0))
    {
        i1UrlLen--;
    }

    if (pi1CustFilePath != NULL)
    {
        /* if i1UrlLen is less than zero then there is no path in string */
        if (i1UrlLen < 0)
        {
            /*assign Default Path */
            if (STRLEN(ISS_IMAGE_PATH) != 0)
            {
               STRNCPY (pi1CustFilePath, (UINT1 *) ISS_IMAGE_PATH,
                     STRLEN (ISS_IMAGE_PATH));
            }        
            pi1CustFilePath[STRLEN (ISS_IMAGE_PATH)] ='\0';
        }
        else
        {
            if (i1UrlLen + 1 >= MAX_FILEPATH_LEN)
                return CLI_FAILURE;
            STRNCPY (pi1CustFilePath, pi1Token, (UINT4) (i1UrlLen + 1));
            pi1CustFilePath[(UINT4)(i1UrlLen + 1)] ='\0';
        }
    }

    /*now piTemp is having file name */
    pi1Token = pi1TempToken + (i1UrlLen + 1);

    if (STRLEN (pi1Token) >= MAX_FLASH_FILENAME_LEN)
        return CLI_FAILURE;

    if (pi1CustFileName != NULL)
    {
        STRNCPY (pi1CustFileName, pi1Token, STRLEN (pi1Token) + 1);
        pi1CustFileName[STRLEN (pi1Token) + 1]= '\0';
    }

    return CLI_SUCCESS;
}


/***************************************************************************
 **                                                                        *
 **     Function Name : CliUtilValidateHostName                            *
 **                                                                        *
 **     Description   : This function Validates the given host name,checks *
 **                     the presence of special characters.                *
 **     Input(s)      : pu1HostName : String to validate                   *
 **                                                                        *
 **     Output(s)     : NULL                                               *
 **                                                                        *
 **     Returns       : CLI_SUCCESS/CLI_FAILURE                            *
 **                                                                        *
 ***************************************************************************/
INT4
CliUtilValidateHostName (INT1 * pi1HostName)
{

    UINT1               u1AlphaPresentFlag = 0;
    INT1                i1ch = 0;
    UINT4               u4Ret = 0;
    UINT1               u1SpecialFlag = 0;
    /*to restrict user entering string more than 255 characters */
    u4Ret = CLI_STRLEN (pi1HostName);

    if (u4Ret > DNS_MAX_QUERY_LEN)
    {
        return CLI_FAILURE;
    }

    i1ch = *pi1HostName;

    u4Ret = ISALPHA (i1ch);
    /*first character must be a alphabet
     * as per RFC 1035 section 2.3.1*/
    if (u4Ret == 0)
    {
        return CLI_FAILURE;
    }
    while ((i1ch = *pi1HostName))
    {
        u4Ret = ISALPHA (i1ch);

        if (u4Ret == 0)
        {
            /*character is not a alphabet*/
            u4Ret = 0;
            u4Ret = ISDIGIT (i1ch);
            if (u4Ret == 0)
            {
                /*to check whether previous character is
                 * a special character and  next character also special character*/
                if (u1SpecialFlag == 1)
                {
                    return CLI_FAILURE;
                }
                /*its not a digit*/
                if ((i1ch != '.') && (i1ch != '-'))
                {
                    /*if the entered string is not an alphabet or digit or .
                     * then return failure*/
                    return CLI_FAILURE;
                }
                else
                {
                    u1SpecialFlag = 1;
                }

            }
            else
            {
                /*resetting the u1SpecialFlag, since
                 * the character is digit*/
                u1SpecialFlag = 0;
            }
        }
        else
        {
            /*resetting the u1SpecialFlag, since
             * the character is alphabet*/
            u1SpecialFlag = 0;
            u1AlphaPresentFlag = 1;
        }
        pi1HostName++;
    }

    /* should have atleast one alphabet somewhere */
    if (u1AlphaPresentFlag != 1)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
