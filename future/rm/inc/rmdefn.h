/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmdefn.h,v 1.54 2015/03/08 11:14:08 siva Exp $
*
* Description: This file contains RM module definitions
*********************************************************************/
#ifndef _RM_DEFN_H
#define _RM_DEFN_H

/* Message type used to identify whether it is a static configuration
 * information or dynamic sync-up message. If it is a static configuration
 * information, then the static configuration syncup message needs to be
 * constructed before it can be send to standby node. If the message type is
 * RM_DYNAMIC_SYNC_MSG then it can be directly send to the standby node.*/
enum
{
    RM_STATIC_CONF_INFO = 1,
    RM_DYNAMIC_SYNC_MSG = 2,
#ifdef L2RED_TEST_WANTED
    RM_DYN_AUDIT_CHKSUM_REQUEST = 3,
    RM_DYN_AUDIT_CHKSUM_RESPONSE = 4,
#endif
 RM_SSL_GET_CERT_REQ = 5,
    RM_V3_GET_CRYPTSEQ_REQ = 6
};

/* Indicates the Dynamic bulk update status of the system. Currently it is 
 * used by the static config sync task to re-apply the failure buffer */
enum
{
    RM_BULK_UPDATE_COMPLETED = 0, 
    RM_BULK_UPDATE_NOT_COMPLETED = 1
};

/* RM Core module Timer Types */
enum
{
    RM_SEQ_RECOV_TIMER              = 1,
    RM_ACK_RECOV_TIMER              = 2,
    RM_CONN_RETRY_TIMER             = 3,
    RM_BULK_UPDATE_TIMER            = 4,
    RM_CPU_TRAFFIC_CTRL_TIMER       = 5,
#ifdef L2RED_TEST_WANTED
    RM_DYN_SYNC_AUDIT_TIMER          = 6,
#endif
 RM_MAX_TIMER_TYPES              = 7 /* Max Timer types */
};

#define RM_BULK_UPDATE_TMR_INT       10 /* 10 milli seconds */
#ifdef L2RED_TEST_WANTED
#define RM_DYN_AUDIT_TMR_INT         (10 * 1000) /* 10 milli seconds */
#endif
#define RM_CPU_TRAFFIC_TMR_INT      (120 * 1000) /* 120 Seconds */

/* RM control messages */
enum
{
    RM_PEER_INFO_MSG = 0,
    RM_WR_SOCK_MSG   = 1,
    RM_PROTO_ACK_MSG = 2,
    RM_RD_SOCK_MSG   = 3
};

/* RM Q & Task related macros  */
#define RM_PKT_Q_NAME     "RMPQ"
#define RM_CTRL_Q_NAME    "RMCQ"
#define RM_APP_Q_NAME    "RMAP"
#define RM_PKT_Q_DEPTH     4000
#define RM_PROTO_SEM_NAME  (CONST UINT1 *) "RMPS"
#define RM_FT_ACT_TASK_NAME   "RMFA"
#define RM_FT_STA_TASK_NAME   "RMFS"
#define RM_FT_SRV_TASK_NAME   "RMFT"
#define RM_SHUT_START_TASK_NAME   "RMSS"
#define RM_CONFIG_SYNCUP_TASK_NAME "RMCS"
#define RM_FT_SRV_TASK_PRI     200
#define RM_FT_ACT_TASK_PRI     200
#define RM_FT_STA_TASK_PRI     200
#define RM_CONFIG_SYNCUP_TASK_PRIORITY     200
/* To relinquish RM task to handle higher priority messages (GO_ACTIVE, GO_STANDBY, 
PEER UP, PEER DOWN and ACK message from Protocols*/
#define RM_RELINQUISH_CNTR     10
/* This should be same as RM task priority 
   as RM task waits for this to be completed for RM state change*/
#define RM_SHUT_START_TASK_PRI 20 

#define RM_SHUT_START_IN_PROGRESS       1
#define RM_SHUT_START_NOT_OCCURED       2
#define RM_SHUT_START_COMPLETED         3

#define RM_FT_CLIENT_TASK_NAME "RMC"
#define RM_FT_CLIENT_TASK_PRI  200

#define RM_RESTART_PROTO_TASK_NAME "RMRP"
#define RM_RESTART_PROTO_TASK_PRI  200

#define RM_PEER_RECORD_SLL &RmPeerRecordSll

#define RM_SELF              SELF
/* 1 STUPS = 100 units = 1000msecs
             1 unit    = 1000/100 = 10msecs 
   1 second duration is divided into 100 units and
   Therefore, 1 unit = 10msecs  */
#define NO_OF_MSECS_PER_UINT  10
/* Time to peer switch comes up*/
#define STACK_DEFAULT_PEER_UP_INTERVAL       30000 /* 30 secs */
/* Time to receive SwitchInfo response after sending requset */
#define STACK_DEFAULT_SWITCH_INFO_INTERVAL   30    /* 30 msec */

/* Node Priority */
#define RM_DEF_PRIORITY  1

#define RM_TASK_PRIORITY  15

/* No of RM file transfer client sessions allowed */
#define RM_MAX_SESSIONS  MAX_NO_OF_FILES

#define RM_VERSION_NUM  1

#define RM_FT_CLIENT_INACTIVE  0
#define RM_FT_CLIENT_ACTIVE    1

#define RM_MAX_DATA_LEN  1024
#define RM_MAX_STRING    512
#define RM_MAX_OID_LEN   24 
 
#define RM_LINEAR_CALC_CKSUM UtlIpCSumLinBuf 
#define STACK_SWITCH_INFO_REQ     0x03
#define STACK_SWITCH_INFO_RESP    0x04
#define STACK_UPGRADE_SUCC        0x05
#define STACK_UPGRADE_FAIL        0x06
/* Max. read/write buffer size */
#define  RM_MAX_BUF_SIZE     512
#define  RM_IF_NAME_LEN  16

#define  RM_DEF_MSG_LEN            OSIX_DEF_MSG_LEN

#define RM_MAX_CONN_RETRY_CNT     10

#define RM_SOCK_CONNECTED            1
#define RM_SOCK_NOT_CONNECTED         2
#define RM_SOCK_CONNECT_IN_PROGRESS  3

/* Events to be posted from the RM task to the Config task*/
#define RM_CONFIG_MSG_RCVD 0x01
#define RM_DYNAMIC_BULK_UPDATE_COMPLETION  0x02

#define RM_CONF_ALL_EVENTS (RM_CONFIG_MSG_RCVD | RM_DYNAMIC_BULK_UPDATE_COMPLETION )

#define RM_CONF_Q_NAME           ((const UINT1 *)"RCSQ")

#define  RM_CONF_SYNC_SEM        "RCSM"

#define RM_TX_BUF_NODES_THRESHOLD (MAX_RM_TX_BUF_NODES * 0.75)

#define RM_MAX_FILE_NAME_LEN 32
#ifdef L2RED_TEST_WANTED
#define RM_DYN_AUDIT_TEMP_LEN 5
#define RM_DYN_AUDIT_TEMP_STR "/tmp/"
#define RM_DYN_AUDIT_FN_STDBY "_output_file_stdby"
#define RM_DYN_AUDIT_FN_STDBY_LEN 18
#endif

/* Syslog message max length */
#define RM_SYS_LENGTH          200

/****************************************************************************/

#define RM_RXBUF_MAX_PKT_SIZE    RM_MAX_SYNC_PKT_LEN
/* Note: RM_RXBUF_MAX_PKT_SIZE should be multiple of RM_RXBUF_MIN_PKT_SIZE */
#define RM_RXBUF_MIN_PKT_SIZE    (RM_MAX_SYNC_PKT_LEN/28)

#define RM_GET_TASK_ID           OsixGetTaskId
#define RM_SEND_EVENT            OsixEvtSend
#define RM_RECV_EVENT            OsixEvtRecv
#define RM_CREATE_QUEUE          OsixQueCrt
#define RM_DELETE_QUEUE            OsixQueDel
#define RM_DELETE_SEMAPHORE(semid) OsixSemDel(semid)
#define RM_SEND_TO_QUEUE         OsixQueSend 
#define RM_RECEIVE_FROM_QUEUE    OsixQueRecv
#define RM_SLL_INIT(x)           TMO_SLL_Init(x)
#define RM_SLL_COUNT(x)          TMO_SLL_Count(x)
#define RM_SLL_NODE              tTMO_SLL_NODE
#define RM_SLL_NODE_INIT         TMO_SLL_Init_Node
#define RM_SLL_ADD               TMO_SLL_Add
#define RM_RXBUF_MAX_HASH_BUCKET_COUNT  200

#define RM_SEQNUM_WRAP_AROUND_VALUE 0xffffffff
#define RM_GET_SWITCH_ID()       (gRmInfo.u4SwitchId)
#define RM_GET_PREV_SWITCH_ID()   (gRmInfo.u4PrevSwitchId)

#define RM_CONFIGURED_STATE()    (gRmInfo.u4ConfigState)
#define RM_COLDSTANDBY_STATE()    (gRmInfo.u4ColdStandbyState)

#define RM_GET_TRAP_SWITCH_ID()       (gRmInfo.u4TrapSwitchId)
#define STACK_INVALID_TIMER        0
#define STACK_PEER_UP_TIMER        1
#define STACK_SWITCH_INFO_TIMER    2
/* Max SwitchInfo request - retry count*/
#define STACK_MAX_RETRY_COUNT      2 
/* Length of Date and Time string used in trap message. */
#define STACK_DATE_TIME_STR_LEN        24
#define STACK_TRAP_STR_LEN             24
#define STACK_TRAP_EVENT                1
#define STACK_NOTIFY_STANDBY_TO_ACTIVE  1
#define STACK_NOTIFY_ACTIVE_TO_STANDBY  2
#define STACK_NOTIFY_PEER_ATTACH        3  
#define STACK_NOTIFY_PEER_DETACH        4

/* RM system recovery causes */
enum 
{
    RM_SYNCUP_ABORT, /* 0 */
    RM_TX_BUFF_ALLOC_FAILED, /* 1 */
    RM_TX_FAILED_AND_TX_BUFF_FULL, /* 2 */
    RM_RX_BUFF_ALLOC_FAILED, /* 3 */
    RM_RX_CRU_BUFF_ALLOC_FAILED, /* 4 */
    RM_RX_BUFF_NODE_ALLOC_FAILED, /* 5 */
    RM_MAX_CONN_RETRY_EXCEEDED, /* 6 */
    RM_CONF_APPLY_FAILED, /* 7 */
    RM_CONF_FAIL_BUFF_ALLOC_FAILED, /* 8 */
    RM_API_SYSTEM_RECOVERY, /* 9 */
    RM_STATE_CHG_DURING_STATIC_BLK, /* 10 */
    RM_STATE_CHG_DURING_DYNAMIC_BLK, /* 11 */
    RM_CONN_LOST_AND_RESTORED, /* 12 */
    RM_COLDSTDBY_REBOOT /* 13 */
};

/* RM reboot flags */
enum
{
    RM_RESTART_ENABLE = 1,
    RM_RESTART_DISABLE
};

#define RM_MIN_RESTART_CNT      0
#define RM_MAX_RESTART_CNT      10
#define RM_DEF_RESTART_CNT      3


#ifdef L2RED_TEST_WANTED
typedef struct {
    UINT4 u4AuditStatus;
    UINT4 u4IsAppSetForAudit;
    INT4  i4RecvdChkSum;
    INT4  i4AuditChkSumValue;
}tRmDynAuditInfo;

#endif

typedef struct PeerRecEntry {
   tTMO_SLL_NODE           NextNode;
   UINT4                   u4SlotIndex;
   UINT4                   u4StackIp;
   UINT4                   au4StackMac[RM_MAC_ADDR_LEN];
   UINT1                   au1SwitchBaseMac[RM_MAC_ADDR_LEN];
   UINT1                   au1Reserved[2];
}tPeerRecEntry;

typedef struct StackTimerRef
{
   /*tTmrAppTimer should be the first member of this struct*/
    tTmrAppTimer RefTimer;           /*Timer  to be started/Stopped*/
    UINT1        u1TimerType;        /* Timer Type PeerUp or SwitchInfo*/
    UINT1        au1Reserved[3];      /* For Padding */
} tStackTimerRef;


typedef struct StackTimer 
{
    tStackTimerRef      TmrRef;
    UINT4               u4SlotId;            /* slot number*/
    UINT1               u1NumofRetries;      /* Retry count*/
    UINT1               au1Reserved[3];      /* For Padding */
}tStackTimer;

typedef struct StackNotificationMsg
{
        CHR1  ac1DateTime[24];             /* Event Time */    
 UINT4  u4NodeId;                    /* Switch Id */  
 UINT4  u4State;                     /* Event Type */
}tStackNotificationMsg;

/* HITLESS RESTART related macros */
#define RM_HR_BULK_REQ_LEN             3
#define RM_HR_ADD_LEN_FOR_TYPE         3
#define RM_HR_MSG_TYPE_OFFSET          (RM_HDR_LENGTH)
#define RM_HR_MSG_LEN_OFFSET           (RM_HDR_LENGTH + 1)
#define RM_HR_HDR_READ_COUNT           8
#define RM_HR_SKIP_SOCKET_CONN         4
#define RM_HR_SSP_PORT_OFFSET    (RM_HR_MSG_LEN_OFFSET + sizeof (UINT2))
#define RM_HR_SSP_TIMEOUT_OFFSET (RM_HR_SSP_PORT_OFFSET + sizeof (UINT2))
#define RM_HR_SSP_BUFFER_OFFSET  (RM_HR_SSP_TIMEOUT_OFFSET + sizeof (UINT4))
#endif /* _RM_DEFN_H */
