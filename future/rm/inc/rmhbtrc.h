/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbtrc.h,v 1.4 2011/11/17 10:14:16 siva Exp $
*
* Description: Macros related to Trace of RM_HB_. 
*********************************************************************/
#ifdef TRACE_WANTED
#define RM_HB_TRC_FLAG   gRmHbInfo.u4HbTrc
#define RM_HB_NAME   "RM_HB"

#define RM_HB_MAX_TRC_NAME_LEN       60
#define RM_HB_TRC(Value, Fmt)\
{\
    if (RM_HB_TRC_FLAG & (Value))\
    {\
        CHR1        RmTrcName[RM_HB_MAX_TRC_NAME_LEN];\
        CHR1       *pu1RmTrcName;\
        tUtlInAddr   IpAddr;\
        pu1RmTrcName = RmTrcName;\
        IpAddr.u4Addr = OSIX_NTOHL (RM_HB_GET_SELF_NODE_ID());\
        SPRINTF(pu1RmTrcName, "%s:%s:", RM_HB_NAME, INET_NTOA (IpAddr));\
        pu1RmTrcName += STRLEN(pu1RmTrcName);\
        if (RM_HB_GET_NODE_STATE() == RM_ACTIVE)\
        {\
            SPRINTF(pu1RmTrcName, "Active");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_STANDBY)\
        {\
            SPRINTF(pu1RmTrcName, "Standby");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_INIT)\
        {\
            SPRINTF(pu1RmTrcName, "Init");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_TRANSITION_IN_PROGRESS)\
        {\
            SPRINTF(pu1RmTrcName, "Transition_In_Progress");\
        }\
        UtlTrcLog(RM_HB_TRC_FLAG, Value, (CONST CHR1 *) RmTrcName, \
                  (CONST CHR1 *) Fmt);\
    }\
}

#define RM_HB_TRC1(Value, Fmt, arg1)\
{\
    if (RM_HB_TRC_FLAG & (Value))\
    {\
        CHR1        RmTrcName[RM_HB_MAX_TRC_NAME_LEN];\
        CHR1       *pu1RmTrcName;\
        tUtlInAddr   IpAddr;\
        pu1RmTrcName = RmTrcName;\
        IpAddr.u4Addr = OSIX_NTOHL (RM_HB_GET_SELF_NODE_ID());\
        SPRINTF(pu1RmTrcName, "%s:%s:", RM_HB_NAME, INET_NTOA (IpAddr));\
        pu1RmTrcName += STRLEN(pu1RmTrcName);\
        if (RM_HB_GET_NODE_STATE() == RM_ACTIVE)\
        {\
            SPRINTF(pu1RmTrcName, "Active");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_STANDBY)\
        {\
            SPRINTF(pu1RmTrcName, "Standby");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_INIT)\
        {\
            SPRINTF(pu1RmTrcName, "Init");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_TRANSITION_IN_PROGRESS)\
        {\
            SPRINTF(pu1RmTrcName, "Transition_In_Progress");\
        }\
        UtlTrcLog(RM_HB_TRC_FLAG, Value, (CONST CHR1 *) RmTrcName, \
                  (CONST CHR1 *) Fmt, arg1);\
    }\
}

#define RM_HB_TRC2(Value, Fmt, arg1, arg2)\
{\
    if (RM_HB_TRC_FLAG & (Value))\
    {\
        CHR1        RmTrcName[RM_HB_MAX_TRC_NAME_LEN];\
        CHR1       *pu1RmTrcName;\
        tUtlInAddr   IpAddr;\
        pu1RmTrcName = RmTrcName;\
        IpAddr.u4Addr = OSIX_NTOHL (RM_HB_GET_SELF_NODE_ID());\
        SPRINTF(pu1RmTrcName, "%s:%s:", RM_HB_NAME, INET_NTOA (IpAddr));\
        pu1RmTrcName += STRLEN(pu1RmTrcName);\
        if (RM_HB_GET_NODE_STATE() == RM_ACTIVE)\
        {\
            SPRINTF(pu1RmTrcName, "Active");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_STANDBY)\
        {\
            SPRINTF(pu1RmTrcName, "Standby");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_INIT)\
        {\
            SPRINTF(pu1RmTrcName, "Init");\
        }\
        else if (RM_HB_GET_NODE_STATE() == RM_TRANSITION_IN_PROGRESS)\
        {\
            SPRINTF(pu1RmTrcName, "Transition_In_Progress");\
        }\
        UtlTrcLog(RM_HB_TRC_FLAG, Value, (CONST CHR1 *) RmTrcName, \
                  (CONST CHR1 *) Fmt, arg1, arg2);\
    }\
}
#else

#define RM_HB_TRC(Value, Fmt) 
#define RM_HB_TRC1(Value, Fmt, arg1) 
#define RM_HB_TRC2(Value, Fmt, arg1, arg2) 
#endif /* TRACE_WANTED */
