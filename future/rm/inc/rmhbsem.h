/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbsem.h,v 1.4 2008/10/10 13:16:28 premap-iss Exp $
*
* Description: Data structure used by Redundancy manager State event 
*              machine.
*********************************************************************/
#ifndef _RM_HB_SEM_H_
#define _RM_HB_SEM_H_

enum {
    RM0 = 0,
    RM1,
    RM2,
    RM3,
    RM4,
    RM5,
    RM6,
    RM7,
    RM8,
    RM9,
    RM10,
    RM11,
    RM12,
    MAX_RM_FN_PTRS
};

typedef void (*tRmActionProc) (VOID);

/* RM SEM function pointers */
tRmActionProc RmActionProc [MAX_RM_FN_PTRS] = 
/*void (*aRmActionProc[MAX_RM_FN_PTRS]) () =*/
{
   RmSemEvntIgnore,                   /* RM0 */
   RmSem1WayRcvdInInitState,          /* RM1 */
   RmSem2WayRcvdInInitState,          /* RM2 */
   RmSemActvElctdInInitState,         /* RM3 */
   RmSemPeerDownRcvdInInitState,      /* RM4 */ 
   RmSemPeerDownRcvdInActiveState,    /* RM5 */ 
   RmSemPeerDownRcvdInStandbyState,   /* RM6 */ 
   RmSemPeerDownRcvdInTrgsInPrgs,     /* RM7 */ 
   RmSemPriorityChg,                  /* RM8 */
   RmSem2WayRcvdInActvState,          /* RM9 */
   RmSemFSWRcvdInActiveState,        /* RM10 */
   RmSemFSWRcvdInStandbyState,        /* RM11 */
   RmSemFSWAckRcvdInTrgsInPrgsState   /* RM12 */
};

/* Rm State Event Table */
const UINT1 au1RmSem[RM_HB_MAX_EVENTS][RM_MAX_STATES] =
/*  RM     RM    RM    RM
 *  INIT   ACTV  STNBY TRANSITION
 *                     IN PROGRESS  */ /* States */
{  
   {RM1,   RM0,  RM0,  RM0  },  /* RM_HB_1WAY_RCVD */ /* Events */
   {RM2,   RM9,  RM0,  RM0  },  /* RM_HB_2WAY_RCVD */
   {RM3,   RM0,  RM0,  RM0  },  /* RM_HB_ACTV_ELCTD */
   {RM4,   RM5,  RM6,  RM7  },  /* RM_HB_PEER_DOWN */
   {RM8,   RM8,  RM8,  RM0  },  /* RM_HB_PRIORITY_CHG */
   {RM0,   RM0,  RM0,  RM0  },  /* RM_HB_IGNORE_PKT */
   {RM0,   RM10, RM11, RM0  },  /* RM_HB_FSW_RCVD */
   {RM0,   RM0,  RM0,  RM12 }   /* RM_HB_FSW_ACK_RCVD */
};

const CHR1 *au1RmStateStr[RM_MAX_STATES] = {
   "INIT",
   "ACTIVE",
   "STANDBY",
   "TRANSITION_IN_PROGRESS"
};

const CHR1 *au1RmEvntStr[RM_HB_MAX_EVENTS] = {
   "1WAY_RCVD",
   "2WAY_RCVD",
   "ACTV_ELCTD",
   "PEER_DOWN",
   "PRIORITY_CHG",
   "IGNORE_PKT",
   "RM_FSW_RCVD",
   "RM_FSW_ACK_RCVD"
};
#endif /* _RM_HB_SEM_H_ */
