/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmincs.h,v 1.34 2015/09/14 12:08:46 siva Exp $
*
* Description: Common header file for RM module. 
*********************************************************************/
#ifndef _RMINCS_H
#define _RMINCS_H

#include "lr.h"
#include "iss.h"
#include "cfa.h"
#include "selutil.h"
#include "ip.h"
#include "rmgr.h"
#include "rmtdfs.h"
#include "rmmacs.h"
#include "rmdefn.h"
#include "rmtrc.h"
#include "rmproto.h"
#include "rmglob.h"
#include "msr.h"
#include "cust.h"
#include "fm.h"
#include "hb.h"

#include "cli.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#ifdef PBBTE_WANTED
#include "pbbte.h"
#endif
#include "l2iwf.h"
#include "garp.h"
#include "elm.h"
#include "mrp.h"
#include "ecfm.h"
#include "elps.h"
#include "qosxtd.h"
#include "dcbx.h"
#include "rstp.h"
#include "pim.h"
#include "la.h"
#include "pnac.h"
#include "eoam.h"
#include "lldp.h"
#include "vcm.h"
#include "npapi.h"
#include "bridge.h"
#include "snp.h"
#include "rednp.h"
#include "fssyslog.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "fsbuddy.h"
#include "rmcli.h"
#include "erps.h"
#include "arp.h"
#include "rip.h"

#ifdef OSPF_WANTED
#include "ospf.h"
#endif
#include "ospf3.h"
#include "hwaud.h"

#ifdef NPAPI_WANTED
#include "rmnp.h"
#include "rmnpwr.h"
#endif

#ifdef L2RED_TEST_WANTED
#include "rmtstinc.h"
#endif

#ifdef IP6_WANTED
#include "ipv6.h"
#endif
#ifdef BGP_WANTED
#include "bgp.h"
#endif

#include "rmsz.h"

#include "issu.h"
#endif /* _RMINCS_H */
