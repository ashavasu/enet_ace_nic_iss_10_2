/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmmacs.h,v 1.12 2016/03/18 13:22:09 siva Exp $
*
* Description: RM Macro definitions
*********************************************************************/
#ifndef _RM_MACS_H
#define _RM_MACS_H

/*------------------------TX buffer related macros------------------*/
/* Tx buffer related macros */
#define RM_TX_MSG_BUF_NODE_OFFSET(x)   (x->u4MsgOffset)
#define RM_TX_MSG_BUF_NODE_MSG_SIZE(x) (x->u4MsgSize)
#define RM_TX_MSG_BUF_NODE_MSG(x)      (x->pu1Msg)
/* TX Statistics counters */
#define RM_TX_STAT_SYNC_MSG_SENT() \
    gRmInfo.RmTxStats.u4SyncMsgTxCount
#define RM_TX_STAT_SYNC_MSG_FAILED() \
    gRmInfo.RmTxStats.u4SyncMsgTxFailedCount
/*------------------------------------------------------------------*/
/*------------------------RX buffer related macros------------------*/
/* RX Statistics counters */
#define RM_RX_STAT_SYNC_MSG_RECVD() \
    gRmInfo.RmRxStats.u4SyncMsgRxCount
#define RM_RX_STAT_SYNC_MSG_PROCESSED() \
    gRmInfo.RmRxStats.u4SyncMsgProcCount
#define RM_RX_STAT_SYNC_MSG_MISSED() \
    gRmInfo.RmRxStats.u4SyncMsgMissedCount
#define RM_RX_STAT_STATIC_CONF_SYNC_FAILED() \
    gRmInfo.RmRxStats.u4SyncMsgStaticConfFailCount
#define   RM_HTONS(x)       OSIX_HTONS(x)
#define RM_RX_GET_HASH_INDEX(u4SeqNumber) \
    (u4SeqNumber % RM_RXBUF_MAX_HASH_BUCKET_COUNT)
#define RM_IS_RX_BUFF_PROCESSING_ALLOWED()  gRmInfo.bIsRxBufProcAllowed
#define RM_RX_LAST_SEQ_NUM_PROCESSED() gRmInfo.u4RxLastSeqNum
#define RM_RX_LAST_ACK_RCVD_SEQ_NUM() gRmInfo.u4RxAckRcvdSeqNum
#define RM_RX_LAST_APPID_PROCESSED() gRmInfo.u4RxLastAppId

#define RM_RX_GETNEXT_VALID_SEQNUM() \
    ((RM_RX_LAST_SEQ_NUM_PROCESSED() == RM_SEQNUM_WRAP_AROUND_VALUE)? \
     1 : (RM_RX_LAST_SEQ_NUM_PROCESSED() + 1))

#define RM_RX_INCR_LAST_SEQNUM_PROCESSED()  \
    ((RM_RX_LAST_SEQ_NUM_PROCESSED() == RM_SEQNUM_WRAP_AROUND_VALUE)?\
     (RM_RX_LAST_SEQ_NUM_PROCESSED() = 1) : RM_RX_LAST_SEQ_NUM_PROCESSED()++)

#define RM_RX_INIT_SEQ_NUMBER()  (gRmInfo.u4RxLastSeqNum = 0)

#define RM_RX_SEQ_MISSMATCH_FLG() gRmInfo.bSeqMissMatch
#define RM_RX_BUFF_GET_HASHNODE_OFFSET() \
    FSAP_OFFSETOF(tRmRxBufHashEntry, RxBufHashNode)
/*------------------------------------------------------------------*/
/*-----------RX configuration failure buffer related macros---------*/
#define RM_CONF_FAIL_BUFF_POOL_ID() gRmConfInfo.RmConfigFailBuffPoolId
/*------------------------------------------------------------------*/
/*-----------RM general macros--------------------------------------*/
#define RM_TCP_SRV_SOCK_FD()     (gRmInfo.i4SrvSockFd)
#define RM_GET_SSN_INFO(x)       (gRmInfo.RmSsnInfo[x])
#define RM_SSN_ENTRY_TX_LIST(x)  (x.TxList)
#define RM_GET_ACTIVE_NODE_ID()  (gRmInfo.u4ActiveNode)
#define RM_GET_SELF_NODE_ID()    (gRmInfo.u4SelfNode)
#define RM_GET_SELF_NODE_MASK()  (gRmInfo.u4SelfNodeMask)
#define RM_GET_PEER_NODE_ID()    (gRmInfo.u4PeerNode)
#define RM_GET_NODE_STATE()      (gRmInfo.u4NodeState)
#define RM_GET_PREV_NOTIF_NODE_STATE()      (gRmInfo.u4PrevNotifNodeState)
#define RM_GET_PREV_NODE_STATE() (gRmInfo.u4PrevNodeState)
#define RM_GET_PEER_NODE_STATE() (gRmInfo.u1PeerNodeState)
#define RM_GET_TRC_LEVEL()       (gRmInfo.u4RmTrc)
#define RM_GET_MOD_TRC()         (gRmInfo.u4RmModTrc)
#define RM_RX_BUFF_NODE_COUNT()       (gRmInfo.u4RxBufNodeCount)
#define RM_GET_FLAGS()           (gRmInfo.u4Flags)
#define RM_STATIC_CONFIG_STATUS()      (gRmInfo.u1StaticConfigStatus)
#define RM_FORCE_SWITCHOVER_FLAG() (gRmInfo.u1ForceSwitchOverFlag)
#define RM_PROTO_SEM()           (gRmInfo.RmProtoSemId)
#define RM_TASK_ID               (gRmInfo.RmTaskId)
#define RM_SHUT_START_TASK_ID     (gRmInfo.RmShutStartTaskId) /*Task Id of the
                                                        Shut and Start operation. */
#define RM_QUEUE_ID            (gRmInfo.RmQueueId)
#define RM_APP_QUEUE_ID            (gRmInfo.RmAppQueueId)
#define RM_CTRL_QUEUE_ID       (gRmInfo.RmCtrlQueueId)
#define RM_FT_SRV_TASK_ID     (gRmInfo.RmFtSrvTaskId) /*Task Id of the
                                                        File Transfer
                                                        Server task created
                                                        in ACTIVE node */
#define RM_FT_ACT_TASK_ID     (gRmInfo.RmFtActTaskId) 
#define RM_FT_STA_TASK_ID     (gRmInfo.RmFtStaTaskId) 
#define RM_PEER_NODE_COUNT()     (gRmInfo.u1NumPeers)
#define RM_ISSU_PEER_NODE_COUNT()     (gRmInfo.u1IssuNumPeers)
#define RM_CONF_SYNCUP_TASK_ID()   gRmConfInfo.RmConfigSyncTaskId
#define RM_CONF_SYNC_SEMID()     gRmConfInfo.RmConfSemId
#define RM_CONF_Q_ID()           gRmConfInfo.RmConfigSyncQId
#define RM_CONF_Q_MSG_POOL_ID()   gRmConfInfo.RmConfigSyncQPoolId
#define RM_SET_ACTIVE_NODE_ID(NodeIp) (gRmInfo.u4ActiveNode = NodeIp)
#define RM_SET_PEER_NODE_ID(NodeIp)  (gRmInfo.u4PeerNode = NodeIp)
#define RM_SET_NODE_STATE(state) (gRmInfo.u4NodeState = state)
#define RM_SET_PREV_NOTIF_NODE_STATE(state) \
                             (gRmInfo.u4PrevNotifNodeState = state)
#define RM_SET_PREV_NODE_STATE(state) \
                             (gRmInfo.u4PrevNodeState = state)
#define RM_SET_PEER_NODE_STATE(state) \
                             (gRmInfo.u1PeerNodeState = state)
#define RM_SET_FLAGS(flags) (gRmInfo.u4Flags |= flags)
#define RM_RESET_FLAGS(flags) ((gRmInfo.u4Flags) &= (UINT4)(~flags))
#define RM_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define RM_DELETE_SEMAPHORE(semid) OsixSemDel(semid)
#define RM_STAT_CONF_LOCK()    RmConfStaticConfDBLock ()
#define RM_STAT_CONF_UNLOCK()  RmConfStaticConfDBUnLock ()
#define RM_SLL_INIT(x)         TMO_SLL_Init(x)
#define RM_SLL_COUNT(x)        TMO_SLL_Count(x)
#define RM_IS_APPL_REGISTERED(EntId) \
        (gRmInfo.RmAppInfo[EntId] != NULL) && \
        (gRmInfo.RmAppInfo[EntId]->pFnRcvPkt != NULL)
#define RM_SHUT_START_STATE()     (gRmInfo.u1ShutStartState)
#define RM_IS_NODE_TRANSITION_IN_PROGRESS() gRmInfo.bIsTransInPrgs 
#define RM_IGNORE_ABORT_NOTIFICATION()  gRmInfo.bIgnoreAbort        

/* HITLESS RESTART */
#define RM_HR_GET_STATUS()     (gRmInfo.u1HRFlag)
/*------------------------------------------------------------------*/
/*-----------RX Memory operations related macros--------------------*/
/* Allocate CRU buf and set the valid offset to point to 0 */
#define RM_ALLOC_RX_BUF(size)    CRU_BUF_Allocate_MsgBufChain (size, 0)
 
#define RM_RXBUF_PKT_MEM_ALLOC(pu1Ptr, i4PktLen) \
{\
    if (MemAllocateMemBlock (gRmInfo.RmRxBufPktPoolId, \
                             (UINT1 **) &pu1Ptr) == MEM_FAILURE) \
    { \
        pu1Ptr = NULL; \
    } \
}

#define RM_RXBUF_PKT_MEM_FREE(pu1Ptr) \
{\
    MemReleaseMemBlock (gRmInfo.RmRxBufPktPoolId, \
                        pu1Ptr);\
    pu1Ptr = NULL;\
}

#define RM_RXBUF_PKT_MEMSET(pu1Ptr, Const, u4Count) \
    MEMSET(pu1Ptr, Const, u4Count)

#ifdef L2RED_TEST_WANTED
#undef RM_RXBUF_PKT_MEM_ALLOC
#undef RM_RXBUF_PKT_MEM_FREE
#define RM_RXBUF_PKT_MEM_ALLOC(pu1Ptr, i4PktLen) \
{\
    if (MemAllocateMemBlock (gRmInfo.RmRxBufPktPoolId, \
                             (UINT1 **) &pu1Ptr) == MEM_FAILURE) \
    { \
        pu1Ptr = NULL; \
    } \
    RM_TEST_BUDDY_LEAK_COUNT++;\
}

#define RM_RXBUF_PKT_MEM_FREE(pu1Ptr) \
{\
    MemReleaseMemBlock (gRmInfo.RmRxBufPktPoolId, \
                        pu1Ptr);\
    pu1Ptr = NULL;\
    RM_TEST_BUDDY_LEAK_COUNT--;\
}
#endif
/*------------------------------------------------------------------*/
/*-----------RM trace related macros--------------------------------*/
#define RM_SET_TRC_LEVEL(TrcLevel) \
                                 (gRmInfo.u4RmTrc = TrcLevel)
#define RM_SET_MOD_TRC(ModTrc) \
                                 (gRmInfo.u4RmModTrc = ModTrc)
#define RM_TAKE_TRC_BACKUP() \
    (gRmInfo.u4RmTrcBkp = RM_GET_TRC_LEVEL())
#define RM_RESTORE_TRC_BACKUP() \
    (RM_SET_TRC_LEVEL(gRmInfo.u4RmTrcBkp))

/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is SET */
#define RM_TRC_START_SWITCHOVER() \
{\
    if ((RM_GET_TRC_LEVEL() & RM_SWITCHOVER_TRC)) \
    {\
        RM_TAKE_TRC_BACKUP();\
        RM_SET_TRC_LEVEL(RM_SWITCHOVER_TIME_TRC); \
    }\
}

/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is RESET */
#define RM_TRC_FINISH_SWITCHOVER() \
{\
    if ((RM_GET_TRC_LEVEL() & RM_SWITCHOVER_TRC)) \
    {\
        RM_RESTORE_TRC_BACKUP();       \
    }\
}

#define RM_SET_NODE_TRANSITION_IN_PROGS_STATE(u4Val) \
{\
    if (RM_IS_NODE_TRANSITION_IN_PROGRESS() != u4Val)\
    {\
        if (u4Val == RM_TRUE)\
        {\
            RM_TRC (RM_SWITCHOVER_TRC, "---SwitchOver Started[RM]---\n");\
            RM_TRC_START_SWITCHOVER();\
            RM_IS_NODE_TRANSITION_IN_PROGRESS() = RM_TRUE; \
        }\
        else if (u4Val == RM_FALSE)\
        {\
            RM_TRC (RM_SWITCHOVER_TRC, "---SwitchOver Finished[RM]---\n");\
            RM_TRC_FINISH_SWITCHOVER();\
            RM_IS_NODE_TRANSITION_IN_PROGRESS() = RM_FALSE; \
        }\
    }\
}
/*------------------------------------------------------------------*/
/*-----------RX packets handling macros-----------------------------*/
#define RM_ISSCALAR(pMultiIndex) \
((pMultiIndex != NULL) && (pMultiIndex->u4No == 0))

#define RM_CLEAR_OID(x)\
 {\
 MEMSET(x->pu4_OidList, 0, SNMP_MAX_OID_LENGTH);\
 x->u4_Length = 0;\
 }

#define RM_GET_CONF_MSG_LEN(u2MsgLen, u4OidLength, pMultiData) \
{\
    u2MsgLen += 2; \
    u2MsgLen += sizeof (u4OidLength);\
    u2MsgLen += (u4OidLength * sizeof (UINT4));\
    u2MsgLen += 2; \
    u2MsgLen += SNMPUtilGetObjValueLength (pMultiData);\
}

#ifdef L2RED_TEST_WANTED
#define RM_DYN_AUDIT_CHKSUM_REQ_LEN 2
#endif

#define RM_SSL_CERT_REQ_LEN 2
#define RM_OSPF3_CERT_REQ_LEN 2

#define RM_COPY_TO_LINEAR_BUF(CruBuf, LinBuf, offset, Len) \
        CRU_BUF_Copy_FromBufChain (CruBuf, (UINT1 *)LinBuf, offset,Len)

/* Macro to prepend to RM Hdr to data */
#define RM_PREPEND_BUF(pBuf, pSrc, u4Size) \
        CRU_BUF_Prepend_BufChain(pBuf, pSrc, u4Size) 

#define RM_DATA_PUT_2_BYTE(pMsg, u4Offset, u2Value, i4RetVal) \
{\
   UINT2  LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   i4RetVal = \
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 2);\
}
 
 
#define RM_PUT_1_BYTE(pu1Buf, u1Val)    \
    *pu1Buf = u1Val;                     \
    pu1Buf += 1;

#define RM_PUT_2_BYTES(pu1Buf, u2Val)    \
    u2Val = (UINT2)OSIX_HTONS(u2Val);       \
    MEMCPY (pu1Buf, &u2Val, 2);          \
    pu1Buf += 2;

#define RM_PUT_4_BYTES(pu1Buf, u4Val)    \
    u4Val = OSIX_HTONL(u4Val);              \
    MEMCPY (pu1Buf, &u4Val, 4);          \
    pu1Buf += 4;

#define RM_PUT_N_BYTES(pu1Buf, psrc, u4Val) \
    MEMCPY (pu1Buf, psrc, u4Val); \
    pu1Buf += u4Val;

#define RM_CONF_GET_1_BYTE(pMsg, u4Offset, u1Val) \
{ \
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Val); \
    u4Offset += 1;\
}

#define RM_CONF_GET_2_BYTE(pMsg, u4Offset, u2Val) \
{ \
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Val); \
    u4Offset += 2;\
}

#define RM_CONF_GET_4_BYTE(pMsg, u4Offset, u4Val) \
{ \
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4Val); \
    u4Offset += 4;\
}

#define RM_CONF_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{ \
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}
/*------------------------------------------------------------------*/
#endif /* _RM_MACS_H */
