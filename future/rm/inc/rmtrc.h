/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmtrc.h,v 1.13 2010/11/11 05:17:51 prabuc Exp $
*
* Description: Macros related to Trace of RM. 
*********************************************************************/

#define RM_MAX_TRC_NAME_LEN       256

#ifdef TRACE_WANTED
#define RM_TRC_FLAG   gRmInfo.u4RmTrc 
#define RM_NAME   "RM"

#define RM_PKT_DUMP(TraceType, pBuf, Length) \
{\
    if ((RM_TRC_FLAG & TraceType) == DUMP_TRC)	\
    RmPktDumpTrc (pBuf, (UINT4) Length);\
}	


#define RM_TRC(Value, Fmt) \
{\
    RmTrcHandle(Value, Fmt, NULL, NULL, NULL, NULL);\
}

#define RM_TRC1(Value, Fmt, arg1)\
{\
    RmTrcHandle(Value, Fmt, arg1, NULL, NULL, NULL);\
}

#define RM_TRC2(Value, Fmt, arg1, arg2)\
{\
    RmTrcHandle(Value, Fmt, arg1, arg2, NULL, NULL);\
}

#define RM_TRC3(Value, Fmt, arg1, arg2, arg3)\
{\
    RmTrcHandle(Value, Fmt, arg1, arg2, arg3, NULL);\
}

#define RM_TRC4(Value, Fmt, arg1, arg2, arg3, arg4)\
{\
    RmTrcHandle(Value, Fmt, arg1, arg2, arg3, arg4);\
}
#else
#define RM_TRC_FLAG 0
#define RM_PKT_DUMP(TraceType, pBuf, Length) 
#define RM_TRC(Value, Fmt) 
#define RM_TRC1(Value, Fmt, arg1)\
{\
	UNUSED_PARAM(arg1);\
}
#define RM_TRC2(Value, Fmt, arg1, arg2)\
{\
	UNUSED_PARAM(arg1);\
	UNUSED_PARAM(arg2);\
}
#define RM_TRC3(Value, Fmt, arg1, arg2, arg3)\
{\
	UNUSED_PARAM(arg1);\
	UNUSED_PARAM(arg2);\
	UNUSED_PARAM(arg3);\
}
#define RM_TRC4(Value, Fmt, arg1, arg2, arg3, arg4)\
{\
	UNUSED_PARAM(arg1);\
	UNUSED_PARAM(arg2);\
	UNUSED_PARAM(arg3);\
	UNUSED_PARAM(arg4);\
}

#endif /* TRACE_WANTED */
