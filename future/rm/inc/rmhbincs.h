/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbincs.h,v 1.2 2008/10/01 16:08:52 prabuc-iss Exp $
*
* Description: Common header file for RM module. 
*********************************************************************/
#ifndef _RMHBINCS_H
#define _RMHBINCS_H

#include "lr.h"
#include "iss.h"
#include "selutil.h"
#include "ip.h"
#include "rmgr.h"
#include "rmhbdefn.h"
#include "rmhbtrc.h"
#include "rmhbprot.h"
#include "rmhbglob.h"

#endif /* _RMHBINCS_H */
