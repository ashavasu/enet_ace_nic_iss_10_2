/*$Id: fsrm.h,v 1.7 2014/06/27 11:18:43 siva Exp $*/
/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _FSRM_H
#define _FSRM_H

#ifdef SNMP_2_WANTED
/* If not defined include the below */
struct RM_MIB_OID {
 const char *pName;
 const char *pNumber;
};

/* SNMP-MIB translation table. */
static struct RM_MIB_OID fs_rm_mib_oid_table[] = {
   {"ccitt",        "0"},
   {"iso",        "1"},
   {"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
   {"org",        "1.3"},
   {"dod",        "1.3.6"},
   {"internet",        "1.3.6.1"},
   {"directory",        "1.3.6.1.1"},
   {"mgmt",        "1.3.6.1.2"},
   {"mib-2",        "1.3.6.1.2.1"},
   {"transmission",        "1.3.6.1.2.1.10"},
   {"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
   {"dot1dBridge",        "1.3.6.1.2.1.17"},
   {"dot1dStp",        "1.3.6.1.2.1.17.2"},
   {"dot1dTp",        "1.3.6.1.2.1.17.4"},
   {"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
   {"experimental",        "1.3.6.1.3"},
   {"private",        "1.3.6.1.4"},
   {"enterprises",        "1.3.6.1.4.1"},
   {"issExt",        "1.3.6.1.4.1.2076.81.8"},
   {"fsRmMIB",        "1.3.6.1.4.1.2076.99"},
   {"fsRmNotifications",        "1.3.6.1.4.1.2076.99.0"},
   {"fsRm",        "1.3.6.1.4.1.2076.99.1"},
   {"fsRmSelfNodeId",        "1.3.6.1.4.1.2076.99.1.1"},
   {"fsRmPeerNodeId",        "1.3.6.1.4.1.2076.99.1.2"},
   {"fsRmActiveNodeId",        "1.3.6.1.4.1.2076.99.1.3"},
   {"fsRmNodeState",        "1.3.6.1.4.1.2076.99.1.4"},
   {"fsRmHbInterval",        "1.3.6.1.4.1.2076.99.1.5"},
   {"fsRmPeerDeadInterval",        "1.3.6.1.4.1.2076.99.1.6"},
   {"fsRmTrcLevel",        "1.3.6.1.4.1.2076.99.1.7"},
   {"fsRmForceSwitchoverFlag",        "1.3.6.1.4.1.2076.99.1.8"},
   {"fsRmPeerDeadIntMultiplier",        "1.3.6.1.4.1.2076.99.1.9"},
   {"fsRmSwitchId",        "1.3.6.1.4.1.2076.99.1.10"},
   {"fsRmConfiguredState",        "1.3.6.1.4.1.2076.99.1.11"},
   {"fsRmStackMacAddr",        "1.3.6.1.4.1.2076.99.1.12"},
   {"fsRmPeerTable",        "1.3.6.1.4.1.2076.99.1.13"},
   {"fsRmPeerTableEntry",        "1.3.6.1.4.1.2076.99.1.13.1"},
   {"fsRmPeerSwitchId",        "1.3.6.1.4.1.2076.99.1.13.1.1"},
   {"fsRmPeerStackIpAddr",        "1.3.6.1.4.1.2076.99.1.13.1.2"},
   {"fsRmPeerStackMacAddr",        "1.3.6.1.4.1.2076.99.1.13.1.3"},
   {"fsRmPeerSwitchBaseMacAddr",        "1.3.6.1.4.1.2076.99.1.13.1.4"},
   {"fsRmStackPortCount",        "1.3.6.1.4.1.2076.99.1.14"},
   {"fsRmColdStandby",        "1.3.6.1.4.1.2076.99.1.15"},
   {"fsRmCopyPeerSyLogFile",        "1.3.6.1.4.1.2076.99.1.23"},
#ifdef L2RED_TEST_WANTED
   {"fsRmDynamicSyncAuditTrigger",        "1.3.6.1.4.1.2076.99.1.24"},
#endif
   {"fsRmTrap",        "1.3.6.1.4.1.2076.99.2"},
   {"fsRmTrapModuleId",        "1.3.6.1.4.1.2076.99.2.1"},
   {"fsRmTrapOperation",        "1.3.6.1.4.1.2076.99.2.2"},
   {"fsRmTrapOperationStatus",        "1.3.6.1.4.1.2076.99.2.3"},
   {"fsRmTrapError",        "1.3.6.1.4.1.2076.99.2.4"},
   {"fsRmTrapEventTime",        "1.3.6.1.4.1.2076.99.2.5"},
   {"fsRmTrapErrorStr",        "1.3.6.1.4.1.2076.99.2.6"},
   {"fsRmTrapSwitchId",        "1.3.6.1.4.1.2076.99.2.7"},
   {"fsRmStatistics",        "1.3.6.1.4.1.2076.99.3"},
   {"fsRmStatsSyncMsgTxCount",        "1.3.6.1.4.1.2076.99.3.1"},
   {"fsRmStatsSyncMsgTxFailedCount",        "1.3.6.1.4.1.2076.99.3.2"},
   {"fsRmStatsSyncMsgRxCount",        "1.3.6.1.4.1.2076.99.3.3"},
   {"fsRmStatsSyncMsgProcCount",        "1.3.6.1.4.1.2076.99.3.4"},
   {"fsRmStatsSyncMsgMissedCount",        "1.3.6.1.4.1.2076.99.3.5"},
   {"fsRmStatsConfSyncMsgFailCount",        "1.3.6.1.4.1.2076.99.3.6"},
   {"fsRmTest",        "1.3.6.1.4.1.2076.99.4"},
#ifdef L2RED_TEST_WANTED
   {"fsRmDynamicSyncAuditTable",        "1.3.6.1.4.1.2076.99.4.2"},
   {"fsRmDynamicSyncAuditEntry",        "1.3.6.1.4.1.2076.99.4.2.1"},
   {"fsRmDynAuditAppId",        "1.3.6.1.4.1.2076.99.4.2.1.1"},
   {"fsRmDynamicSyncAuditStatus",        "1.3.6.1.4.1.2076.99.4.2.1.2"},
#endif
   {"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
   {"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
   {"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
   {"security",        "1.3.6.1.5"},
   {"snmpV2",        "1.3.6.1.6"},
   {"snmpDomains",        "1.3.6.1.6.1"},
   {"snmpProxys",        "1.3.6.1.6.2"},
   {"snmpModules",        "1.3.6.1.6.3"},
   {"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
   {"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
   {"ieee802dot1mibs",        "1.111.2.802.1"},
   {"joint-iso-ccitt",        "2"},
   {0,0}
};
#endif/*SNMP_2_WANTED*/
#endif /* _FSRM_H */
