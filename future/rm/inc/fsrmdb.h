/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmdb.h,v 1.18 2016/03/18 13:15:32 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRMDB_H
#define _FSRMDB_H

UINT1 FsRmPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRmSwitchoverTimeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
#ifdef L2RED_TEST_WANTED
UINT1 FsRmDynamicSyncAuditTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
#endif

UINT4 fsrm [] ={1,3,6,1,4,1,2076,99};
tSNMP_OID_TYPE fsrmOID = {8, fsrm};


UINT4 FsRmSelfNodeId [ ] ={1,3,6,1,4,1,2076,99,1,1};
UINT4 FsRmPeerNodeId [ ] ={1,3,6,1,4,1,2076,99,1,2};
UINT4 FsRmActiveNodeId [ ] ={1,3,6,1,4,1,2076,99,1,3};
UINT4 FsRmNodeState [ ] ={1,3,6,1,4,1,2076,99,1,4};
UINT4 FsRmHbInterval [ ] ={1,3,6,1,4,1,2076,99,1,5};
UINT4 FsRmPeerDeadInterval [ ] ={1,3,6,1,4,1,2076,99,1,6};
UINT4 FsRmTrcLevel [ ] ={1,3,6,1,4,1,2076,99,1,7};
UINT4 FsRmForceSwitchoverFlag [ ] ={1,3,6,1,4,1,2076,99,1,8};
UINT4 FsRmPeerDeadIntMultiplier [ ] ={1,3,6,1,4,1,2076,99,1,9};
UINT4 FsRmSwitchId [ ] ={1,3,6,1,4,1,2076,99,1,10};
UINT4 FsRmConfiguredState [ ] ={1,3,6,1,4,1,2076,99,1,11};
UINT4 FsRmStackMacAddr [ ] ={1,3,6,1,4,1,2076,99,1,12};
UINT4 FsRmPeerSwitchId [ ] ={1,3,6,1,4,1,2076,99,1,13,1,1};
UINT4 FsRmPeerStackIpAddr [ ] ={1,3,6,1,4,1,2076,99,1,13,1,2};
UINT4 FsRmPeerStackMacAddr [ ] ={1,3,6,1,4,1,2076,99,1,13,1,3};
UINT4 FsRmPeerSwitchBaseMacAddr [ ] ={1,3,6,1,4,1,2076,99,1,13,1,4};
UINT4 FsRmStackPortCount [ ] ={1,3,6,1,4,1,2076,99,1,14};
UINT4 FsRmColdStandby [ ] ={1,3,6,1,4,1,2076,99,1,15};
UINT4 FsRmModuleTrc [ ] ={1,3,6,1,4,1,2076,99,1,16};
UINT4 FsRmProtocolRestartFlag [ ] ={1,3,6,1,4,1,2076,99,1,17};
UINT4 FsRmProtocolRestartRetryCnt [ ] ={1,3,6,1,4,1,2076,99,1,18};
UINT4 FsRmHitlessRestartFlag [ ] ={1,3,6,1,4,1,2076,99,1,19};
UINT4 FsRmAppId [ ] ={1,3,6,1,4,1,2076,99,4,1,1,1};
UINT4 FsRmAppName [ ] ={1,3,6,1,4,1,2076,99,4,1,1,2};
UINT4 FsRmEntryTime [ ] ={1,3,6,1,4,1,2076,99,4,1,1,3};
UINT4 FsRmExitTime [ ] ={1,3,6,1,4,1,2076,99,4,1,1,4};
UINT4 FsRmSwitchoverTime [ ] ={1,3,6,1,4,1,2076,99,4,1,1,5};
#ifdef L2RED_TEST_WANTED
UINT4 FsRmDynAuditAppId [ ] ={1,3,6,1,4,1,2076,99,4,2,1,1};
UINT4 FsRmDynamicSyncAuditStatus [ ] ={1,3,6,1,4,1,2076,99,4,2,1,2};
#endif
UINT4 FsRmIpAddress [ ] ={1,3,6,1,4,1,2076,99,1,20};
UINT4 FsRmSubnetMask [ ] ={1,3,6,1,4,1,2076,99,1,21};
UINT4 FsRmStackInterface [ ] ={1,3,6,1,4,1,2076,99,1,22};
UINT4 FsRmCopyPeerSyLogFile [ ] ={1,3,6,1,4,1,2076,99,1,23};
#ifdef L2RED_TEST_WANTED
UINT4 FsRmDynamicSyncAuditTrigger [ ] ={1,3,6,1,4,1,2076,99,1,24};
#endif
UINT4 FsRmTrapModuleId [ ] ={1,3,6,1,4,1,2076,99,2,1};
UINT4 FsRmTrapOperation [ ] ={1,3,6,1,4,1,2076,99,2,2};
UINT4 FsRmTrapOperationStatus [ ] ={1,3,6,1,4,1,2076,99,2,3};
UINT4 FsRmTrapError [ ] ={1,3,6,1,4,1,2076,99,2,4};
UINT4 FsRmTrapEventTime [ ] ={1,3,6,1,4,1,2076,99,2,5};
UINT4 FsRmTrapErrorStr [ ] ={1,3,6,1,4,1,2076,99,2,6};
UINT4 FsRmTrapSwitchId [ ] ={1,3,6,1,4,1,2076,99,2,7};
UINT4 FsRmStatsSyncMsgTxCount [ ] ={1,3,6,1,4,1,2076,99,3,1};
UINT4 FsRmStatsSyncMsgTxFailedCount [ ] ={1,3,6,1,4,1,2076,99,3,2};
UINT4 FsRmStatsSyncMsgRxCount [ ] ={1,3,6,1,4,1,2076,99,3,3};
UINT4 FsRmStatsSyncMsgProcCount [ ] ={1,3,6,1,4,1,2076,99,3,4};
UINT4 FsRmStatsSyncMsgMissedCount [ ] ={1,3,6,1,4,1,2076,99,3,5};
UINT4 FsRmStatsConfSyncMsgFailCount [ ] ={1,3,6,1,4,1,2076,99,3,6};
UINT4 FsRmStaticBulkStatus [ ] ={1,3,6,1,4,1,2076,99,3,7};
UINT4 FsRmDynamicBulkStatus [ ] ={1,3,6,1,4,1,2076,99,3,8};
UINT4 FsRmOverallBulkStatus [ ] ={1,3,6,1,4,1,2076,99,3,9};




tMbDbEntry fsrmMibEntry[]= {

{{10,FsRmSelfNodeId}, NULL, FsRmSelfNodeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmPeerNodeId}, NULL, FsRmPeerNodeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmActiveNodeId}, NULL, FsRmActiveNodeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmNodeState}, NULL, FsRmNodeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmHbInterval}, NULL, FsRmHbIntervalGet, FsRmHbIntervalSet, FsRmHbIntervalTest, FsRmHbIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "500"},

{{10,FsRmPeerDeadInterval}, NULL, FsRmPeerDeadIntervalGet, FsRmPeerDeadIntervalSet, FsRmPeerDeadIntervalTest, FsRmPeerDeadIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{10,FsRmTrcLevel}, NULL, FsRmTrcLevelGet, FsRmTrcLevelSet, FsRmTrcLevelTest, FsRmTrcLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "67174400"},

{{10,FsRmForceSwitchoverFlag}, NULL, FsRmForceSwitchoverFlagGet, FsRmForceSwitchoverFlagSet, FsRmForceSwitchoverFlagTest, FsRmForceSwitchoverFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRmPeerDeadIntMultiplier}, NULL, FsRmPeerDeadIntMultiplierGet, FsRmPeerDeadIntMultiplierSet, FsRmPeerDeadIntMultiplierTest, FsRmPeerDeadIntMultiplierDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,FsRmSwitchId}, NULL, FsRmSwitchIdGet, FsRmSwitchIdSet, FsRmSwitchIdTest, FsRmSwitchIdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRmConfiguredState}, NULL, FsRmConfiguredStateGet, FsRmConfiguredStateSet, FsRmConfiguredStateTest, FsRmConfiguredStateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRmStackMacAddr}, NULL, FsRmStackMacAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRmPeerSwitchId}, GetNextIndexFsRmPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRmPeerTableINDEX, 1, 0, 0, NULL},

{{12,FsRmPeerStackIpAddr}, GetNextIndexFsRmPeerTable, FsRmPeerStackIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsRmPeerTableINDEX, 1, 0, 0, NULL},

{{12,FsRmPeerStackMacAddr}, GetNextIndexFsRmPeerTable, FsRmPeerStackMacAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsRmPeerTableINDEX, 1, 0, 0, NULL},

{{12,FsRmPeerSwitchBaseMacAddr}, GetNextIndexFsRmPeerTable, FsRmPeerSwitchBaseMacAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsRmPeerTableINDEX, 1, 0, 0, NULL},

{{10,FsRmStackPortCount}, NULL, FsRmStackPortCountGet, FsRmStackPortCountSet, FsRmStackPortCountTest, FsRmStackPortCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRmColdStandby}, NULL, FsRmColdStandbyGet, FsRmColdStandbySet, FsRmColdStandbyTest, FsRmColdStandbyDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRmModuleTrc}, NULL, FsRmModuleTrcGet, FsRmModuleTrcSet, FsRmModuleTrcTest, FsRmModuleTrcDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "8388607"},

{{10,FsRmProtocolRestartFlag}, NULL, FsRmProtocolRestartFlagGet, FsRmProtocolRestartFlagSet, FsRmProtocolRestartFlagTest, FsRmProtocolRestartFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRmProtocolRestartRetryCnt}, NULL, FsRmProtocolRestartRetryCntGet, FsRmProtocolRestartRetryCntSet, FsRmProtocolRestartRetryCntTest, FsRmProtocolRestartRetryCntDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,FsRmHitlessRestartFlag}, NULL, FsRmHitlessRestartFlagGet, FsRmHitlessRestartFlagSet, FsRmHitlessRestartFlagTest, FsRmHitlessRestartFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRmIpAddress}, NULL, FsRmIpAddressGet, FsRmIpAddressSet, FsRmIpAddressTest, FsRmIpAddressDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "2851995905"},

{{10,FsRmSubnetMask}, NULL, FsRmSubnetMaskGet, FsRmSubnetMaskSet, FsRmSubnetMaskTest, FsRmSubnetMaskDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "4278190080"},

{{10,FsRmStackInterface}, NULL, FsRmStackInterfaceGet, FsRmStackInterfaceSet, FsRmStackInterfaceTest, FsRmStackInterfaceDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRmCopyPeerSyLogFile}, NULL, FsRmCopyPeerSyLogFileGet, FsRmCopyPeerSyLogFileSet, FsRmCopyPeerSyLogFileTest, FsRmCopyPeerSyLogFileDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

#ifdef L2RED_TEST_WANTED
{{10,FsRmDynamicSyncAuditTrigger}, NULL, FsRmDynamicSyncAuditTriggerGet, FsRmDynamicSyncAuditTriggerSet, FsRmDynamicSyncAuditTriggerTest, FsRmDynamicSyncAuditTriggerDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
#endif

{{10,FsRmTrapModuleId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapOperation}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapOperationStatus}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapError}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapErrorStr}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRmTrapSwitchId}, NULL, FsRmTrapSwitchIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsSyncMsgTxCount}, NULL, FsRmStatsSyncMsgTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsSyncMsgTxFailedCount}, NULL, FsRmStatsSyncMsgTxFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsSyncMsgRxCount}, NULL, FsRmStatsSyncMsgRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsSyncMsgProcCount}, NULL, FsRmStatsSyncMsgProcCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsSyncMsgMissedCount}, NULL, FsRmStatsSyncMsgMissedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStatsConfSyncMsgFailCount}, NULL, FsRmStatsConfSyncMsgFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmStaticBulkStatus}, NULL, FsRmStaticBulkStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmDynamicBulkStatus}, NULL, FsRmDynamicBulkStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRmOverallBulkStatus}, NULL, FsRmOverallBulkStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRmAppId}, GetNextIndexFsRmSwitchoverTimeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRmSwitchoverTimeTableINDEX, 1, 0, 0, NULL},

{{12,FsRmAppName}, GetNextIndexFsRmSwitchoverTimeTable, FsRmAppNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRmSwitchoverTimeTableINDEX, 1, 0, 0, NULL},

{{12,FsRmEntryTime}, GetNextIndexFsRmSwitchoverTimeTable, FsRmEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRmSwitchoverTimeTableINDEX, 1, 0, 0, NULL},

{{12,FsRmExitTime}, GetNextIndexFsRmSwitchoverTimeTable, FsRmExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRmSwitchoverTimeTableINDEX, 1, 0, 0, NULL},

{{12,FsRmSwitchoverTime}, GetNextIndexFsRmSwitchoverTimeTable, FsRmSwitchoverTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRmSwitchoverTimeTableINDEX, 1, 0, 0, NULL},

#ifdef L2RED_TEST_WANTED
{{12,FsRmDynAuditAppId}, GetNextIndexFsRmDynamicSyncAuditTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRmDynamicSyncAuditTableINDEX, 1, 0, 0, NULL},

{{12,FsRmDynamicSyncAuditStatus}, GetNextIndexFsRmDynamicSyncAuditTable, FsRmDynamicSyncAuditStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRmDynamicSyncAuditTableINDEX, 1, 0, 0, "0"},
};
tMibData fsrmEntry = { 50, fsrmMibEntry };
#else
};
tMibData fsrmEntry = { 47, fsrmMibEntry };
#endif

#endif /* _FSRMDB_H */

