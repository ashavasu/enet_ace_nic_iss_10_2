/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmlw.h,v 1.17 2016/03/18 13:15:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmSelfNodeId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsRmPeerNodeId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRmActiveNodeId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRmNodeState ARG_LIST((INT4 *));

INT1
nmhGetFsRmHbInterval ARG_LIST((INT4 *));

INT1
nmhGetFsRmPeerDeadInterval ARG_LIST((INT4 *));

INT1
nmhGetFsRmTrcLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsRmForceSwitchoverFlag ARG_LIST((INT4 *));

INT1
nmhGetFsRmPeerDeadIntMultiplier ARG_LIST((INT4 *));

INT1
nmhGetFsRmSwitchId ARG_LIST((INT4 *));

INT1
nmhGetFsRmConfiguredState ARG_LIST((INT4 *));

INT1
nmhGetFsRmStackMacAddr ARG_LIST((tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRmHbInterval ARG_LIST((INT4 ));

INT1
nmhSetFsRmPeerDeadInterval ARG_LIST((INT4 ));

INT1
nmhSetFsRmTrcLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsRmForceSwitchoverFlag ARG_LIST((INT4 ));

INT1
nmhSetFsRmPeerDeadIntMultiplier ARG_LIST((INT4 ));

INT1
nmhSetFsRmSwitchId ARG_LIST((INT4 ));

INT1
nmhSetFsRmConfiguredState ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRmHbInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmPeerDeadInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmTrcLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmForceSwitchoverFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmPeerDeadIntMultiplier ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmSwitchId ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmConfiguredState ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRmHbInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmPeerDeadInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmTrcLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmForceSwitchoverFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmPeerDeadIntMultiplier ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmSwitchId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmConfiguredState ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRmPeerTable. */
INT1
nmhValidateIndexInstanceFsRmPeerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRmPeerTable  */

INT1
nmhGetFirstIndexFsRmPeerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRmPeerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmPeerStackIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRmPeerStackMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsRmPeerSwitchBaseMacAddr ARG_LIST((INT4 ,tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmStackPortCount ARG_LIST((INT4 *));

INT1
nmhGetFsRmColdStandby ARG_LIST((INT4 *));

INT1
nmhGetFsRmModuleTrc ARG_LIST((UINT4 *));

INT1
nmhGetFsRmProtocolRestartFlag ARG_LIST((INT4 *));

INT1
nmhGetFsRmProtocolRestartRetryCnt ARG_LIST((UINT4 *));

INT1
nmhGetFsRmHitlessRestartFlag ARG_LIST((INT4 *));

INT1
nmhGetFsRmIpAddress ARG_LIST((UINT4 *));

INT1
nmhGetFsRmSubnetMask ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStackInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRmCopyPeerSyLogFile ARG_LIST((INT4 *));

#ifdef L2RED_TEST_WANTED
INT1
nmhGetFsRmDynamicSyncAuditTrigger ARG_LIST((UINT4 *));
#endif

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRmStackPortCount ARG_LIST((INT4 ));

INT1
nmhSetFsRmColdStandby ARG_LIST((INT4 ));

INT1
nmhSetFsRmModuleTrc ARG_LIST((UINT4 ));

INT1
nmhSetFsRmProtocolRestartFlag ARG_LIST((INT4 ));

INT1
nmhSetFsRmProtocolRestartRetryCnt ARG_LIST((UINT4 ));

INT1
nmhSetFsRmHitlessRestartFlag ARG_LIST((INT4 ));

INT1
nmhSetFsRmIpAddress ARG_LIST((UINT4 ));

INT1
nmhSetFsRmSubnetMask ARG_LIST((UINT4 ));

INT1
nmhSetFsRmStackInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRmCopyPeerSyLogFile ARG_LIST((INT4 ));

#ifdef L2RED_TEST_WANTED
INT1
nmhSetFsRmDynamicSyncAuditTrigger ARG_LIST((UINT4 ));
#endif

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRmStackPortCount ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmColdStandby ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmModuleTrc ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmProtocolRestartFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmProtocolRestartRetryCnt ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmHitlessRestartFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRmIpAddress ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmSubnetMask ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRmStackInterface ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRmCopyPeerSyLogFile ARG_LIST((UINT4 *  ,INT4 ));

#ifdef L2RED_TEST_WANTED
INT1
nmhTestv2FsRmDynamicSyncAuditTrigger ARG_LIST((UINT4 *  ,UINT4 ));
#endif

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRmStackPortCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmColdStandby ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmModuleTrc ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmProtocolRestartFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmProtocolRestartRetryCnt ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmHitlessRestartFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRmSwitchoverTimeTable. */
INT1
nmhValidateIndexInstanceFsRmSwitchoverTimeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRmSwitchoverTimeTable  */

INT1
nmhGetFirstIndexFsRmSwitchoverTimeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRmSwitchoverTimeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmAppName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRmEntryTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRmExitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRmSwitchoverTime ARG_LIST((INT4 ,UINT4 *));

#ifdef L2RED_TEST_WANTED
/* Proto Validate Index Instance for FsRmDynamicSyncAuditTable. */
INT1
nmhValidateIndexInstanceFsRmDynamicSyncAuditTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRmDynamicSyncAuditTable  */

INT1
nmhGetFirstIndexFsRmDynamicSyncAuditTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRmDynamicSyncAuditTable ARG_LIST((INT4 , INT4 *));
#endif

/* Proto type for Low Level GET Routine All Objects.  */

#ifdef L2RED_TEST_WANTED
INT1
nmhGetFsRmDynamicSyncAuditStatus ARG_LIST((INT4 ,INT4 *));
#endif

INT1
nmhDepv2FsRmIpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmSubnetMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmStackInterface ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRmCopyPeerSyLogFile ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#ifdef L2RED_TEST_WANTED
INT1
nmhDepv2FsRmDynamicSyncAuditTrigger ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmTrapSwitchId ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmStatsSyncMsgTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStatsSyncMsgTxFailedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStatsSyncMsgRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStatsSyncMsgProcCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStatsSyncMsgMissedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStatsConfSyncMsgFailCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRmStaticBulkStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRmDynamicBulkStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRmOverallBulkStatus ARG_LIST((INT4 *));
