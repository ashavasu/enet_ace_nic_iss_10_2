/*$Id: rmsz.h,v 1.4 2014/06/27 11:18:43 siva Exp $*/
enum {
    MAX_RM_APPS_SIZING_ID,
    MAX_RM_CONF_FAIL_BUF_BLOCKS_SIZING_ID,
    MAX_RM_CONF_Q_DEPTH_SIZING_ID,
    MAX_RM_CTRL_MSG_NODES_SIZING_ID,
    MAX_RM_NODE_INFO_BLOCKS_SIZING_ID,
    MAX_RM_NOTIFICATION_INFO_SIZING_ID,
    MAX_RM_PKT_MSG_BLOCKS_SIZING_ID,
    MAX_RM_PKTQ_MSG_SIZING_ID,
    MAX_RM_RX_BUF_NODES_SIZING_ID,
    MAX_RM_RX_BUF_PKT_NODES_SIZING_ID,
    MAX_RM_SYNC_SSNS_SIZING_ID,
    MAX_RM_TX_BUF_NODES_SIZING_ID,
#ifdef L2RED_TEST_WANTED
    MAX_RM_APP_MSG_SIZING_ID,
#endif
    RM_MAX_SIZING_ID
};


#ifdef  _RMSZ_C
tMemPoolId RMMemPoolIds[ RM_MAX_SIZING_ID];
INT4  RmSizingMemCreateMemPools(VOID);
VOID  RmSizingMemDeleteMemPools(VOID);
INT4  RmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RMSZ_C  */
extern tMemPoolId RMMemPoolIds[ ];
extern INT4  RmSizingMemCreateMemPools(VOID);
extern VOID  RmSizingMemDeleteMemPools(VOID);
#endif /*  _RMSZ_C  */


#ifdef  _RMSZ_C
tFsModSizingParams FsRMSizingParams [] = {
{ "tRmAppInfo", "MAX_RM_APPS", sizeof(tRmAppInfo),MAX_RM_APPS, MAX_RM_APPS,0 },
{ "tRmConfFailBuff", "MAX_RM_CONF_FAIL_BUF_BLOCKS", sizeof(tRmConfFailBuff),MAX_RM_CONF_FAIL_BUF_BLOCKS, MAX_RM_CONF_FAIL_BUF_BLOCKS,0 },
{ "tRmConfigQMsg", "MAX_RM_CONF_Q_DEPTH", sizeof(tRmConfigQMsg),MAX_RM_CONF_Q_DEPTH, MAX_RM_CONF_Q_DEPTH,0 },
{ "tRmCtrlQMsg", "MAX_RM_CTRL_MSG_NODES", sizeof(tRmCtrlQMsg),MAX_RM_CTRL_MSG_NODES, MAX_RM_CTRL_MSG_NODES,0 },
{ "tRmNodeInfo", "MAX_RM_NODE_INFO_BLOCKS", sizeof(tRmNodeInfo),MAX_RM_NODE_INFO_BLOCKS, MAX_RM_NODE_INFO_BLOCKS,0 },
{ "tRmNotificationMsg", "MAX_RM_NOTIFICATION_INFO", sizeof(tRmNotificationMsg),MAX_RM_NOTIFICATION_INFO, MAX_RM_NOTIFICATION_INFO,0 },
{ "tRmPktBlock", "MAX_RM_PKT_MSG_BLOCKS", sizeof(tRmPktBlock),MAX_RM_PKT_MSG_BLOCKS, MAX_RM_PKT_MSG_BLOCKS,0 },
{ "tRmPktQMsg", "MAX_RM_PKTQ_MSG", sizeof(tRmPktQMsg),MAX_RM_PKTQ_MSG, MAX_RM_PKTQ_MSG,0 },
{ "tRmRxBufHashEntry", "MAX_RM_RX_BUF_NODES", sizeof(tRmRxBufHashEntry),MAX_RM_RX_BUF_NODES, MAX_RM_RX_BUF_NODES,0 },
{ "tRmPktBlock", "MAX_RM_RX_BUF_PKT_NODES", sizeof(tRmPktBlock),MAX_RM_RX_BUF_PKT_NODES, MAX_RM_RX_BUF_PKT_NODES,0 },
{ "tPeerRecEntry", "MAX_RM_SYNC_SSNS", sizeof(tPeerRecEntry),MAX_RM_SYNC_SSNS, MAX_RM_SYNC_SSNS,0 },
{ "tRmTxBufNode", "MAX_RM_TX_BUF_NODES", sizeof(tRmTxBufNode),MAX_RM_TX_BUF_NODES, MAX_RM_TX_BUF_NODES,0 },
#ifdef L2RED_TEST_WANTED
{ "tRmDynAuditChkSumMsg", "MAX_RM_APP_MSG", sizeof(tRmDynAuditChkSumMsg),MAX_RM_APP_MSG, MAX_RM_APP_MSG,0 },
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _RMSZ_C  */
extern tFsModSizingParams FsRMSizingParams [];
#endif /*  _RMSZ_C  */


