/* $Id: fsrmwr.h,v 1.15 2016/03/18 13:15:32 siva Exp $ */
#ifndef _FSRMWR_H
#define _FSRMWR_H

VOID RegisterFSRM(VOID);

VOID UnRegisterFSRM(VOID);
INT4 FsRmSelfNodeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerNodeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRmActiveNodeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRmNodeStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRmHbIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsRmTrcLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsRmForceSwitchoverFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntMultiplierGet(tSnmpIndex *, tRetVal *);
INT4 FsRmSwitchIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRmConfiguredStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStackMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsRmHbIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsRmTrcLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsRmForceSwitchoverFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntMultiplierSet(tSnmpIndex *, tRetVal *);
INT4 FsRmSwitchIdSet(tSnmpIndex *, tRetVal *);
INT4 FsRmConfiguredStateSet(tSnmpIndex *, tRetVal *);
INT4 FsRmHbIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmTrcLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmForceSwitchoverFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmPeerDeadIntMultiplierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmSwitchIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmConfiguredStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmHbIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmPeerDeadIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmTrcLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmForceSwitchoverFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmPeerDeadIntMultiplierDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmSwitchIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmConfiguredStateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsRmPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsRmPeerStackIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerStackMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsRmPeerSwitchBaseMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStackPortCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmColdStandbyGet(tSnmpIndex *, tRetVal *);
INT4 FsRmModuleTrcGet(tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartRetryCntGet(tSnmpIndex *, tRetVal *);
INT4 FsRmHitlessRestartFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsRmIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsRmSubnetMaskGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStackInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FsRmCopyPeerSyLogFileGet(tSnmpIndex *, tRetVal *);
#ifdef L2RED_TEST_WANTED
INT4 FsRmDynamicSyncAuditTriggerGet(tSnmpIndex *, tRetVal *);
#endif
INT4 FsRmStackPortCountSet(tSnmpIndex *, tRetVal *);
INT4 FsRmColdStandbySet(tSnmpIndex *, tRetVal *);
INT4 FsRmModuleTrcSet(tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartRetryCntSet(tSnmpIndex *, tRetVal *);
INT4 FsRmHitlessRestartFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsRmIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsRmSubnetMaskSet(tSnmpIndex *, tRetVal *);
INT4 FsRmStackInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 FsRmCopyPeerSyLogFileSet(tSnmpIndex *, tRetVal *);
#ifdef L2RED_TEST_WANTED
INT4 FsRmDynamicSyncAuditTriggerSet(tSnmpIndex *, tRetVal *);
#endif
INT4 FsRmStackPortCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmColdStandbyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmModuleTrcTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmProtocolRestartRetryCntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmHitlessRestartFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmSubnetMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmStackInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRmCopyPeerSyLogFileTest(UINT4 *, tSnmpIndex *, tRetVal *);
#ifdef L2RED_TEST_WANTED
INT4 FsRmDynamicSyncAuditTriggerTest(UINT4 *, tSnmpIndex *, tRetVal *);
#endif
INT4 FsRmStackPortCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmColdStandbyDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmModuleTrcDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmProtocolRestartFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmProtocolRestartRetryCntDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmHitlessRestartFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsRmSwitchoverTimeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsRmAppNameGet(tSnmpIndex *, tRetVal *);
INT4 FsRmEntryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsRmExitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsRmSwitchoverTimeGet(tSnmpIndex *, tRetVal *);
#ifdef L2RED_TEST_WANTED
INT4 GetNextIndexFsRmDynamicSyncAuditTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsRmDynamicSyncAuditStatusGet(tSnmpIndex *, tRetVal *);
#endif
INT4 FsRmIpAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmSubnetMaskDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmStackInterfaceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRmCopyPeerSyLogFileDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#ifdef L2RED_TEST_WANTED
INT4 FsRmDynamicSyncAuditTriggerDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif
INT4 FsRmTrapSwitchIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsSyncMsgTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsSyncMsgTxFailedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsSyncMsgRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsSyncMsgProcCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsSyncMsgMissedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStatsConfSyncMsgFailCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRmStaticBulkStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRmDynamicBulkStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRmOverallBulkStatusGet(tSnmpIndex *, tRetVal *);
#endif /* _FSRMWR_H */
