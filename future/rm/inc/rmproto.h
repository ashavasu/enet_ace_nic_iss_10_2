/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmproto.h,v 1.41 2017/08/25 13:44:37 siva Exp $
*
* Description: Redundancy manager Core module function prototypes. 
*********************************************************************/
/* Function Prototypes */
/* rmmain.c */
UINT4  RmTaskInit       (VOID);
UINT4  RmInit           (VOID);
VOID   RmDeInit         (VOID);
UINT4  RmSendMsg (UINT1 u1SsnIndex, INT4 i4ConnFd, 
                  UINT1 *pu1Data, UINT4 u4TotLen);
#ifdef L2RED_TEST_WANTED
UINT4  RmSendAuditReqRespToPeer (UINT1 u1SsnIndex, INT4 i4ConnFd, 
                  UINT1 *pu1Data, UINT4 u4TotLen);
UINT4 RmSendDynAuditChkSumReqToStdBy (tRmMsg *pRmMsg, UINT2 u2DataLen);
UINT4 RmSendDynAuditChkSumRespToActive (tRmMsg *pRmMsg, UINT2 u2DataLen);
#endif
UINT4  RmSendCertGenMsgtoPeer (UINT1 u1SsnIndex, INT4 i4ConnFd, 
                  UINT1 *pu1Data, UINT4 u4TotLen);
VOID   RmPktRcvd (INT4 i4SockFd);
UINT4  RmPrependRmHdr (tRmMsg *pRmMsg, UINT4 u4Size);
VOID   RmHandleForceSwitchover (VOID);
INT4   RmWaitForOtherModulesBootUp (VOID);
INT4   RmInitBulkUpdatesStatusMask (UINT4 u4AppId, UINT1 u1SetBit);
INT4   RmClearBulkUpdatesStatus (VOID);
INT4   RmCheckBulkUpdatesStatus (VOID);
VOID   RmHandleShutAndStartCompleted (VOID);
VOID   RmShutStartTaskMain (INT1 *pi1Arg);
VOID   RmSetTxEnable (UINT1 u1Status);
VOID   RmUpdatePeerNodeCntToProtocols  (UINT1 u1Event);
VOID   RmPrcsPendingRxBufMsgs (VOID);
VOID   RmCtrlMsgHandler (VOID);
VOID   RmPeerInfoMsgHandle (tRmPeerCtrlMsg *pRmPeerCtrlMsg);
VOID   RmHandleNodeStateUpdate (UINT1 u1State);
VOID   RmHandleCommLostAndRestoredEvent (VOID);
VOID   RmAllowStaticAndDynamicSyncMsgs (VOID);
VOID   RmHandleSystemRecovery (UINT2 u2Cause);
VOID   RmHandleTriggerSelfAttachEvent (VOID);
UINT1  RmRestartProtocols PROTO ((VOID));
tRmMsg *RmAllocTxBuf PROTO ((UINT4 u4SyncMsgSize));
VOID   RmSslCertGenMsgFromApp PROTO ((VOID));
UINT4  RmSendCertGenMsgToStdBy PROTO ((tRmMsg *pMsg, UINT2 u2BufLen));
VOID   RmV3CryptSeqMsgFromApp PROTO ((VOID));
UINT4  RmSendCryptSeqMsgToStdBy PROTO ((tRmMsg *pMsg, UINT2 u2BufLen));

/* rmque.c */
INT4  RmGetLinearBufAndLenFromPktQMsg (tRmPktQMsg *pRmPktQMsg,
                                 UINT1 **ppu1Data, UINT4 *pu4RmPktLen);
VOID RmQueProcessPendingMsg PROTO ((VOID));

/* rmtmr.c */
UINT4  RmTmrInit        (VOID);
UINT4  RmTmrDeInit (VOID);
VOID   RmTmrExpHandler (VOID);
UINT4  RmTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal);
UINT4  RmTmrStopTimer (UINT1 u1TmrId);
VOID   RmHandleSeqRecovTmrExpiry (VOID *pArg);
VOID   RmHandleAckRecovTmrExpiry (VOID *pArg);
VOID   RmHandleConnRetryTmrExpiry (VOID *pArg);
VOID   RmHandleBulkUpdateTmrExpiry (VOID *pArg);
VOID   RmHandleDynSynchAuditTmrExpiry (VOID *pArg);
VOID   RmHandleCpuTrafficCtrlTmrExpiry (VOID *pArg);


/* rmutil.c */
VOID  RmNotifyProtocols (UINT1 u1Event);
VOID  RmProcessPktFromApp (VOID);
#ifdef L2RED_TEST_WANTED
INT4 RmTriggerAuditEvent (INT4 i4AppId);
VOID RmDynAuditPktFromApp (VOID);
#endif
VOID  RmProcessConfigSyncUp (VOID);
UINT1 RmUtilTriggerNextHigherLayer (UINT4 u4AppId, UINT1 u1Event);
UINT1 RmUtilGetNextAppId (UINT4 u4AppId, UINT4 *pu4NextId);
UINT1 RmUtilGetPrevAppId (UINT4 u4AppId, UINT4 *pu4PrevId);
UINT1 RmUtilGetPrevBulkSupportedAppId (UINT4 u4AppId, UINT4 *pu4PrevId);
UINT1 RmUtilCheckForSwCompletion (UINT4 u4AppId);
UINT1 RmUtilStoreInArray (tRmNotificationMsg *pNotifMsg);
UINT1 RmUtilGetPendingNotificationEntry (tRmNotificationMsg **ppNotifMsg, 
                                         UINT4 *pu4IsEntryExist);
UINT1 RmUtilReleaseMem(tMemPoolId u4MemPoolId, UINT1 *pu1Blk);
VOID  RmAddSyncMsgFromQToTxList (UINT1 u1SsnIndex);
VOID  RmAddSyncMsgIntoTxList (UINT1 u1SsnIndex, UINT1 *pu1Data,
                        UINT4 u4RmPktLen, UINT4 u4RmMsgOffset);
VOID  RmClearSyncMsgQueue (VOID);
UINT1 RmGetSsnIndexFromPeerAddr (UINT4 u4PeerAddr, UINT1 *pu1SsnIndex);
UINT1 RmGetSsnIndexFromConnFd (INT4 i4ConnFd, UINT1 *pu1SsnIndex);
UINT1 RmMemAllocForResBuf (UINT1 **ppResBuf);
VOID  RmUtilResetStatistics (VOID);
INT1  RmUtilFormFullOID (UINT1 *pu1Oid, tSnmpIndex *pMultiIndex,
                        tSNMP_OID_TYPE *pOid);
VOID  RmUtilFormConfigSyncUpMsg (UINT1 u1MessageType, UINT2 u2MsgLen, 
                                UINT1 *pu1Buf, tSNMP_OID_TYPE *pOid, 
                                tSNMP_MULTI_DATA_TYPE *pMultiData);
VOID  RmUtilFillRMHeader (UINT1 *pMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId, UINT4 u4SeqNo);
INT1  RmUtilDelFailBuff (VOID);
INT1  RmUtilReleaseMemForPktQMsg (tRmPktQMsg *pRmPktQMsg);
VOID  RmUtilHandleProtocolAck(tRmProtoAck ProtoAck);
INT4  RmUtilPostDataOrEvntToProtocol (UINT4 u4DestEntId, UINT1 u1Event,
                                     tRmMsg * pPkt, UINT4 u4PktLen, 
                                     UINT4 u4SeqNum);
INT4  RmUtilIsDynamicSyncAllowed(UINT4 u4AppId);
VOID  RmTcpUpdateSsnTable (INT4 i4CliSockFd, UINT4 u4IpAddr);
VOID  RmSystemRestart (VOID);
VOID  RmUtilCloseSockConn (UINT1 u1SsnIndex);
VOID  RmHandleProtocolRestart PROTO ((VOID));
VOID  RmRestartProtoTaskMain PROTO ((INT1 *pi1Arg));
VOID  RmReInitToStandbyState PROTO ((VOID));
VOID  RmHandleInitBulkUpdate PROTO ((VOID));
UINT1 RmHandleProtocolEvent PROTO ((tRmProtoEvt * pEvt));
VOID RmUtilCheckAndCompleteBulkUpdt (VOID);


/* rmsock.c */
INT4   RmSockInit (VOID);
UINT4  RmSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr);
INT4   RmSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr);
UINT4  RmGetIfIpInfo (UINT1 * pu1IfName,tNodeInfo *pNodeInfo);
INT4   RmCloseSocket (INT4 i4SocketFd);
INT4   RmAcceptIncomingConn (INT4 i4FtSrvSockFd);
INT4   RmFtServSockInit (VOID);
UINT4  RmSendFile (INT4 i4CliSockFd);
size_t ReadNbytes( int fd, CHR1 *pc1Ptr, size_t n);
size_t WriteNbytes( int fd, CHR1 *pc1Ptr, size_t n);
size_t FileReadNbytes( int fd, CHR1 *pc1Ptr, size_t n);
size_t FileWriteNbytes( int fd, CHR1 *pc1Ptr, size_t n);
INT4   RmTcpSrvSockInit (INT4 *pi4SrvSockFd);
VOID   RmTcpSrvSockCallBk (INT4 i4SockFd);
VOID   RmTcpWriteCallBackFn (INT4 i4SockFd);
INT4   RmQueEnqCtrlMsg (tRmCtrlQMsg * pQMsg);
VOID   RmSockWritableHandle (INT4 i4ConnFd);
INT4   RmTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd, 
                          UINT4 *pu4PeerAddr);
INT4   RmTcpSockConnect (UINT4 u4PeerAddr, INT4 *pi4ConnFd, 
                         INT4 *pi4ConnState);
UINT4  RmTcpSockSend (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2PktLen, 
                      INT4 *pi4WrBytes);
UINT4  RmTcpSockRcv (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2MsgLen,
                            INT4 *pi4RdBytes);
UINT4  RmSendFileForColdStandBy (INT1 u1FileId);
INT4   RmColdAcceptConnection(INT4 i4FtSrvSockFd);
UINT4  RmSendFirmwareImage (INT4 i4TskIndex);
UINT4  RmSendFiletoAttachedPeer (INT4 i4TskIndex);
UINT4
RmRecvFileInActive (INT4 i4CliSockFd, tFileInfo *pFileInfo);
UINT4
RmSendFileToActive (tFileInfo * pFileName, INT4 i4CliSockFd);
UINT4
RmStandbySendFile (const CHR1 * pc1FileName);
   
/* rmfiletr.c */
UINT4  RmFtSrvTaskInit (VOID);
VOID   RmFtSrvTaskDeInit (VOID);
VOID   RmFtSrvTaskMain (INT1 *pi1Arg);
VOID   RmFtNewConnReq (INT4 i4SockFd);
VOID   RmFtCliTaskMain (INT4 i4ConnIdx);
UINT4  RmFtGetFreeConnIdx (VOID);
VOID   RmGetCliTaskName (UINT4 u4ConnIdx);
VOID   RmTaskSendImage (INT4 i4TskIndex);
VOID   RmSendFilesToNewPeer (UINT4 u4SwitchId);
UINT4  RmUploadFile (UINT1 u1FileId);
VOID   RmFileStandbyToActiveTaskMain (INT4 i4Index);

/* rmext.c */
VOID RmExtRmSetPeerNodeStatus (UINT4 u4PeerNodeId, UINT4 u4NodeStatus);
VOID RmExtRmSetNodeState (UINT4 u4RmNodeState);
VOID RmExtRmSetForceSwitchOverFlag (UINT1 u1Flag);

/* rmtrap.c */
tSNMP_OID_TYPE * RmMakeObjIdFrmDotNew (INT1 *pi1TextStr);
UINT1* RmParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value);
VOID   RmTrapSendNotifications (tRmNotificationMsg *pNotifyInfo);
INT4   RmTrapSendNotificationMsgToActive(tRmNotificationMsg *pNotifyInfo);
VOID   RmTrapProcessInfoFromStandby(tRmMsg *pPkt);

/* rmrxbuf.c */
INT4 RmRxBufAddPkt (UINT1 *pu1Pkt, UINT4 u4SeqNum, UINT4 u4PktLen);
                    tRmRxBufHashEntry * RmRxBufSearchPkt (UINT4 u4SeqNum);
INT4 RmRxBufDeleteNode (tRmRxBufHashEntry *pRxBufNode);
INT4 RmRxBufCreate (VOID);
VOID RmRxBufDestroy (VOID);
VOID RmRxBufFreeHashNode (tTMO_HASH_NODE * pNode);
VOID RmRxBufClearRxBuffer (VOID);

/* rmrx.c */
UINT4 RmProcessRcvEvt (tRmSsnInfo * pRmSsnInfo);
INT4  RmRxProcessBufferedPkt (VOID);
INT4  RmRxStripRmHeader (VOID);
INT4  RmRxPostBufferedPktToProtocol (tRmRxBufHashEntry *pRxBufNode);
VOID  RmRxSeqNumMisMatchHandler (UINT4 u4SeqNum);
VOID  RmRxResetRxBuffering (VOID);
UINT4 RmHandleResBufData (UINT1 *pu1Start, tRmSsnInfo *pRmSsnInfo,
                    UINT2 *pu2HeaderLen, UINT4 *pu4DataLen);
UINT4 RmRecvRmHdrOrPayLoadFromConnFd (UINT1 **ppu1Data, UINT1 *pu1Start,
                                tRmSsnInfo *pRmSsnInfo, UINT2 u2MsgLen,
                                BOOL1 *pbIsPartialDataRcvd);
UINT4 RmHandleCompleteSyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen);
UINT4 RmProcessStandbySyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen,
                         UINT4 u4SeqNum);
#ifdef L2RED_TEST_WANTED
UINT4 RmHandleChecksumRequest (VOID);
UINT4 RmHandleChecksumResponseMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen);
UINT4 RmConstructChkSumResponseMsg (VOID);
VOID RmDynAuditValidateRecvdChkSum (VOID);
#endif
UINT4 RmProcessActiveSyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen,
                        UINT4 u4DestEntId);

/* rmconf.c */
VOID RmConfSyncUpTaskMain (INT1 *pi1Arg);
VOID RmConfHandleBulkUpdCompEvent (VOID);
VOID RmConfProcessSyncMsg (tRmConfigQMsg *pConfigMsg);
VOID RmConfProcessValidMsg (tRmMsg *pMesg, UINT2 u2Length);
VOID RmConfDeqConfigSyncMsg (VOID);
INT4 RmConfAddToFailBuff (tSNMP_OID_TYPE *pOID, tSNMP_MULTI_DATA_TYPE * pVal);
INT1 RmConfSearchOIDInFailBuff (tSNMP_OID_TYPE *pOID);
INT1 RmConfApplyConfiguration (tSNMP_OID_TYPE *pOID, 
                                tSNMP_MULTI_DATA_TYPE * pVal);
INT1 RmConfIsOIDInFilterList (tSNMP_OID_TYPE *pOID);
INT4 RmConfEnqConfigMsg (tRmMsg *pData, UINT4 u4SeqNo, UINT2 u2DataLen);
UINT4 RmConfProcessConfigSyncUpInfo (tRmNotifConfChg * pRmNotifConfChg, 
                                     UINT1 **pu1CfgSyncData, UINT4 *pu4MsgLen);
VOID RmConfPostBulkUpdCompEvent (UINT4 u4Event);

INT4 RmConfStaticConfDBLock (VOID);
INT4 RmConfStaticConfDBUnLock (VOID);
INT1 RmErrorStringOID (tSNMP_OID_TYPE * pOID, UINT1  * pu1Oid,
                  tSNMP_MULTI_DATA_TYPE * pObjVal, UINT1 * pu1Data); 


/* rmtrc.c */
VOID RmPktDumpTrc (UINT1* pu1Buf, UINT4 u4Length);
VOID RmTrcHandle (UINT4 u4FlagVal, CONST CHR1 *pc1Fmt, ...);
UINT1 RmTrcIsDebugTrcAllowed (UINT4 u4EntId);
VOID  RmTrcPrintModSyncMsgTrace (UINT1 *pu1SyncMsg);
UINT4 RmGetStackIpForSlot (UINT4);
VOID  RmGetStackMacForThisSwitch (UINT4);
VOID  RmGetNodePriority (UINT4*);
VOID  RmProcessInfoFromMbsm (UINT4,UINT1);
VOID  RmProcessSelfAttach (VOID);
VOID  RmProcessSelfDetach (VOID);
INT4  RmGetPeerEntry (UINT4, tPeerRecEntry **);
INT4  RmGetFirstPeerEntry (tPeerRecEntry ** pPeerEntry);
INT4  RmGetNextPeerEntry (tPeerRecEntry *, tPeerRecEntry **);
VOID  RmAddPeerToPeerTable (tPeerRecEntry *);
VOID  RmDeleteFromPeerTable (UINT4);
INT4  RmCreatePeerNodeEntry (UINT4,tPeerRecEntry **);
VOID  RmRestoreStaticConfig (VOID);
UINT4 RmTcpSendMsg( UINT4 u4DestIp, tRmPeerCtrlMsg *pPeerCtrlMsg );

VOID StackSendSwitchInfoReqMsg (UINT4 u4SlotId);
UINT4 StackProcessSwitchInfoMsg (tRmPeerCtrlMsg *pPeerInfo, UINT4 u4DataLen, UINT4 u4PeerAddr);
VOID StackSendSwitchInfoRespMsg (UINT4 u4PeerAddr);
VOID StackCreateTimer (VOID);
VOID StackHandleTimerExpiry (VOID);
VOID StackRestartPeerUpTimer (UINT4 u4SlotId);
VOID StackStopSwitchInfoTimer (UINT4 u4Switchid);
VOID StackSendSlaveStatustoMaster (UINT4 u4ActiveIP,UINT4 u4MsgType);
UINT4 RmGetTaskIdFromSwitchId (UINT4 u4SwitchId);
VOID RmChangeStacktoStandaloneswitch (VOID);

VOID StackTrapSendNotifications (tStackNotificationMsg * pNotifyInfo);
INT4 StackTrapSendNotificationMsgToActive (tStackNotificationMsg *);
tSNMP_OID_TYPE     * StackMakeObjIdFrmDotNew (INT1 *);
UINT1              * StackParseSubIdNew (UINT1 *, UINT4 *);
VOID  RmHbRegisterApps (VOID);

/* HITLESS RESTART */
/* Prototypes for the functions present in rmhr.c */
INT4  RmHRFileStore           (UINT1 *pu1Data, UINT4 u4Length, UINT4 u4EntId);
INT4  RmHRFileRestore         (UINT4 u4ModId);
INT1  RmHRStartBulkReq        (UINT4 u4ModId);
VOID  RmHRBulkMsgRestore      (UINT4 u4ModId);
INT1  RmHRProcessBulkMsg      (UINT1 *pu1Data, UINT1 u1MsgType, 
                               UINT4 u4SrcEntId);
INT1  RmHRStartStdyStReq      (UINT4 u4ModId);
VOID  RmHRProcessStdyStPktMsg (UINT1 *pu1Data, UINT4 u4SrcEntId);
VOID  RmHRProcessStdyStTail   (UINT1 *pu1Data, UINT4 u4SrcEntId);
UINT1 RmHRUtilGetNextAppId    (UINT4 u4AppId, UINT4 *pu4NextId);
VOID  RmHRDelFiles            (VOID);
INT1  RmHRValidateChecksum    (UINT1 *pu1Data);
VOID  RmHRGetRmHdrInfo        (UINT1 *pu1Data, UINT1 *pu1MsgType, 
                               UINT4 *pu4SrcEntId);
VOID RmIssuMaintenanceModeEnable PROTO ((VOID));

extern VOID MsrRmFileReadForSelfAttach (VOID);
extern VOID MbsmStartStackTaskInFsw (VOID);
extern UINT1 RmMbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr);



/* External functions*/
extern INT4 WebnmConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, 
                              UINT1 u1HexFlag);
extern VOID WebnmCopyOid (tSNMP_OID_TYPE * pDestOid, tSNMP_OID_TYPE * pSrcOid);
extern VOID WebnmConvertOidToString (tSNMP_OID_TYPE * pOid, UINT1 *pu1String);
extern VOID WebnmConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                                      UINT1 *pu1String, UINT1 u1Type);
extern UINT1  RmMbsmNpHandleNodeTransition (INT4 i4Event, UINT1 u1PrevState, 
                                         UINT1 u1State);
extern INT4 IssSzInitSizingInfoForHR (VOID);
extern INT1  nmhGetMbsmSlotModuleStatus ARG_LIST((INT4 ,INT4 *));
extern INT1  nmhGetMbsmLCConfigCardName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1  nmhGetIfOutErrors ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfOutDiscards ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfOutUcastPkts ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfHCOutOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1  nmhGetIfHCInOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1  nmhGetIfInOctets ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfInUcastPkts ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfOutOctets ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfInDiscards ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfInErrors ARG_LIST((INT4 ,UINT4 *));
extern INT1  nmhGetIfAdminStatus (INT4, INT4 *);
extern INT1  nmhGetIfOperStatus ARG_LIST((INT4 ,INT4 *));
