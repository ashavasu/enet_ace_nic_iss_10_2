/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmglob.h,v 1.20 2014/07/10 12:45:45 siva Exp $
*
* Description: This file contains the global variable declarations  
*              used in RM Module
*********************************************************************/
#ifndef _RMGLOB_H
#define _RMGLOB_H
#ifdef _RMMAIN_C_
tRmInfo             gRmInfo;
tRmConfInfo         gRmConfInfo;
tMacAddr gNullMacAdress = { 0x0 ,0x0 ,0x0 ,0x0 ,0x0 ,0x0 };

/* For config sync-up task */ 
tSnmpIndex              RmSNMPIndexPool[SNMP_MAX_INDICES_2]; 
tSNMP_MULTI_DATA_TYPE   RmSNMPMultiPool[SNMP_MAX_INDICES_2]; 
tSNMP_OCTET_STRING_TYPE RmSNMPOctetPool[SNMP_MAX_INDICES_2]; 
tSNMP_OID_TYPE          RmSNMPOIDPool[SNMP_MAX_INDICES_2]; 
UINT1                   gau1RmSNMPData[SNMP_MAX_INDICES_2][1024]; 
tTMO_SLL            RmPeerRecordSll;
tRmSwitchOverTime gaRmSwitchOverTime[RM_MAX_APPS];
#ifdef L2RED_TEST_WANTED
tRmDynAuditInfo gRmDynAuditInfo[RM_MAX_APPS];
UINT1 gu1IsAuditChksumRespPend;
#endif
UINT4   gRmTotalSwitchOverTime; 
UINT4                   gu4RmStackingInterfaceType;
#ifdef MBSM_WANTED
tTimerListId        gStackTmrLst;
tStackTimer         gaStackTimer[MBSM_MAX_SLOTS];
tMemPoolId          gStackNotifMsgPoolId;
#endif
UINT4               gau4RmFilesTrTaskId[80]; 
UINT1               gu1ModuleBootup; 
UINT1               gu1RmLockVariable;
#ifdef L2RED_TEST_WANTED
UINT4 gu4DynAuditDoneForAllApp = FALSE;
#endif

UINT4 gu4TrafficToCpuAllowed = RM_TRUE;
UINT4 gu4BulkUpdateInProgress = RM_FALSE;

#else

extern tRmInfo      gRmInfo;
extern tMacAddr gNullMacAdress;
extern tRmConfInfo  gRmConfInfo;
/* For config sync-up task */ 
extern tSnmpIndex              RmSNMPIndexPool[SNMP_MAX_INDICES_2]; 
extern tSNMP_MULTI_DATA_TYPE   RmSNMPMultiPool[SNMP_MAX_INDICES_2]; 
extern tSNMP_OCTET_STRING_TYPE RmSNMPOctetPool[SNMP_MAX_INDICES_2]; 
extern tSNMP_OID_TYPE          RmSNMPOIDPool[SNMP_MAX_INDICES_2]; 
extern UINT1                   gau1RmSNMPData[SNMP_MAX_INDICES_2][1024]; 
extern tTMO_SLL           RmPeerRecordSll;
extern tRmSwitchOverTime gaRmSwitchOverTime[RM_MAX_APPS];
#ifdef L2RED_TEST_WANTED
extern tRmDynAuditInfo gRmDynAuditInfo[RM_MAX_APPS];
extern UINT1 gu1IsAuditChksumRespPend;
#endif
extern UINT4   gRmTotalSwitchOverTime; 
extern UINT4                   gu4RmStackingInterfaceType;
#ifdef MBSM_WANTED
extern tTimerListId       gStackTmrLst;
extern tStackTimer        gaStackTimer[MBSM_MAX_SLOTS];
extern tMemPoolId         gStackNotifMsgPoolId;
#endif /* MBSM */
extern UINT1               gu1ModuleBootup; 
extern UINT4              gau4RmFilesTrTaskId[80]; 
extern UINT1              gu1RmLockVariable;
extern UINT4      gu4BulkUpdateInProgress;
extern UINT4              gu4TrafficToCpuAllowed;
#ifdef L2RED_TEST_WANTED
extern UINT4              gu4DynAuditDoneForAllApp;
#endif

#endif /*  _RMMAIN_C_ */
#endif /* _RMGLOB_H */
