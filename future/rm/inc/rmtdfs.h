/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmtdfs.h,v 1.15 2016/03/18 13:22:09 siva Exp $
*
* Description: This file contains type definitions of RM module
*********************************************************************/
#ifndef _RM_TDFS_H
#define _RM_TDFS_H

#include "rmdefn.h"
/* Application specific info stored in RM */
typedef struct {
   UINT4 u4ValidEntry;
   UINT4 u4SrcEntId;
   UINT4 u4DestEntId;

   /* Rm delivers the pkt rcvd to protocols thru' this FnPtr. */
   INT4 (*pFnRcvPkt) (UINT1 u1Event, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PktLen);
}tRmAppInfo;

/* Rm File Transfer Client connection table */
typedef struct 
{
    INT4   i4FtCliSockFd; /* File transfer client sockfd */
    UINT1  au1TskName[OSIX_NAME_LEN + 4]; /* RM FT client task name */
    UINT4  u4TskId;   
    UINT4  u4Status; /* Status to indicate whether active or not */
} tRmFtConnInfo;

typedef struct 
{
    INT4   i4RmAcSd; 
} tRmAcSock;
typedef struct 
{
    INT4   i4RmStSd; 
} tRmStSock;
typedef struct {
    tTMO_SLL_NODE   TxNode;  /* Tx buffer node information */
    UINT1           *pu1Msg;  /* Pointer of the send buffer */
    UINT4           u4MsgSize; /* Message size of the residual buffer */
    UINT4           u4MsgOffset; /* Residual buffer message offset.*/
} tRmTxBufNode;

typedef struct {
    tTMO_SLL        TxList;    /* Tx buffer list */
    UINT1           *pResBuf;  /* Used at sync data reception side to wait
                                 for the complete data. */
    INT4            i4ConnFd;  /* Peer Connection identifier */
    UINT4           u4PeerAddr;  /* Peer address */
    UINT2           u2ResBufLen;    /* Residual buffer length */
    INT1            i1ConnStatus; /* Connection status */
    UINT1           u1Rsvd;
}tRmSsnInfo;

/* Once the peer is discovered by the HB task, it posts an event to Rm sync task
For session table updation and connection establishment with the peer. */

typedef struct  {
  INT4 i4SockId; /* socket identifier */
}tRmSktCtrlMsg;

typedef struct {
    UINT4 u4MsgType; /* Rm queue message type */
    union
    {
        tRmPeerCtrlMsg  RmPeerCtrlMsg; /* RM control message */
        tRmSktCtrlMsg RmSktCtrlMsg;
        tRmProtoAck     RmProtoAckCtrlMsg;
    }unCtrlMsgParam;
#define PeerCtrlMsg  unCtrlMsgParam.RmPeerCtrlMsg 
#define SktCtrlMsg unCtrlMsgParam.RmSktCtrlMsg
#define ProtoAckCtrlMsg   unCtrlMsgParam.RmProtoAckCtrlMsg
}tRmCtrlQMsg;

/* TX Statistic counters */
typedef struct  _RmTxStats
{
    UINT4 u4SyncMsgTxCount; /* Number of sync-up message successfully sent */
    UINT4 u4SyncMsgTxFailedCount; /* Number of sync-up messages failed while
                                     sending */
}tRmTxStats;

/* RX Statistic counters */
typedef struct  _RmRxStats
{
   UINT4  u4SyncMsgRxCount; /* Total number of synchronization messages 
                               received and added to Rx Buffer */
   UINT4  u4SyncMsgProcCount; /* Total number of synchronization messages 
                                 processed from the RX buffer. */
   UINT4  u4SyncMsgMissedCount; /* Total number of synchronization messages
                                   not found in the RX Buffer during search */
   UINT4  u4SyncMsgStaticConfFailCount; /* Total number of static configuration
                                           sync-up messages that have been
                                           failed. */
}tRmRxStats;


/* This structure is used to implement the Hash table for the RX buffering */
typedef struct _RmRxBufHashEntry 
{
   tTMO_HASH_NODE  RxBufHashNode;
   UINT4           u4SeqNum; /* Index of the entry */
   UINT4           u4PktLen;
   UINT1           *pu1Pkt; /* pointer of the received packet*/
}tRmRxBufHashEntry;

typedef struct  _RmSyncStatus
{
    UINT4 u4StaticSyncStatus; /* Status of Static sync-up */
    UINT4 u4StaticBulkSyncStatus; /* Status of Static Sync */
    UINT4 u4DynBulkSyncStatus; /* Status of dynamic bulk sync-up */
    UINT1 au1DynSyncStatus[RM_MAX_APPS]; /* Status of dynamic Sync */
    UINT1 u1Padding; /*Padding bytes of tRmSyncStatus*/
}tRmSyncStatus;

/* global RM data structure */
typedef struct {
    tOsixSemId   RmProtoSemId; /* RM protocol Semaphore Identifier */
    tOsixTaskId  RmTaskId; /* RM Task Identifier */
    tOsixTaskId  RmFtSrvTaskId; /* File transfer server Task Identifier */
    tOsixTaskId  RmFtActTaskId; 
    tOsixTaskId  RmFtStaTaskId; 
    tOsixTaskId  RmShutStartTaskId;
    tOsixQId     RmQueueId; /* RM message Queue Identifier */
    tOsixQId     RmAppQueueId; 
    tOsixQId     RmCtrlQueueId; /* Queue to handle high priority informations
                                   from HB task */
    /* MemPool for RM Registration Tbl */
    tMemPoolId     RmRegMemPoolId;
    tMemPoolId     RmCtrlMsgPoolId;
    tMemPoolId     RmPktQMsgPoolId;
#ifdef L2RED_TEST_WANTED
    tMemPoolId     RmDynCksumPktQMsgPoolId;
#endif
    tMemPoolId     RmMsgPoolId;
    tMemPoolId     RmNotifMsgPoolId;
    tMemPoolId     RmTxBufMemPoolId; /* Tx buffer pool id */
    tMemPoolId     RmRxBufPoolId;
    tMemPoolId     RmRxBufPktPoolId;/* MemPool Id for                                                                                        the RxBuffer Packets */ 
    tMemPoolId     RmPktMsgPoolId;
    tMemPoolId     RmPeerTblPoolId;
    /* Timers*/
    tTmrDesc        aTmrDesc[RM_MAX_TIMER_TYPES];
    /* Timer data struct that contains
       func ptrs for timer handling and
       offsets to identify the data
       struct containing timer block.
       Timer ID is the index to this
       data structure */ 
    /* RM HB Timer List ID */
    tTimerListId    TmrListId;
    /* RM HB Timer block to hold the timer id */
    tTmrBlk    RmTmrBlk[RM_MAX_TIMER_TYPES];
    tRmFtConnInfo RmFtConnTbl[RM_MAX_SESSIONS]; /* File Trans ClientInfo */
    tRmAppList   RmBulkUpdSts; /* Indicates the list of applications
                                  in the active node that completes
                                  it's bulk updates with standby node */
    tRmAppList   RmBulkUpdStsMask; /* Indicates the list of applications
                                      in the active node that should
                                      complete it's bulk updates before
                                      accepting force switchover */
    tRmAppList   RmSwitchoverSts; /* Indicates the list of applications
                                     that completes it's switchover */
    tRmAppList   RmRegMask; /* Indicates the list of applications
                               that has registered with RM */
    tRmAppInfo  *RmAppInfo[RM_MAX_APPS]; /* Application specific info */
    tRmSsnInfo   RmSsnInfo[RM_MAX_SYNC_SSNS_LIMIT]; /* Session information */
    tRmNotificationMsg *apNotifInfo[RM_MAX_NOTIFICATION_INFO_LIMIT]; /* Array pointer 
                                                                  to hold the 
                                                                  notification 
                                                                  information.*/
    tRmTxStats   RmTxStats; /* TX Statistics */
    tRmRxStats   RmRxStats; /* RX Statistics */
/* HITLESS RESTART */
    tRmHRInfo    RmHRInfo[RM_HR_MAX_STORAGE]; /* Hitless restart related information */
    tTMO_HASH_TABLE    *pRxBufHashTable;/* Pointer to Received sequenced
                                           buffer Hash Table*/
    UINT1  au1RmStackInterface[ISS_RM_INTERFACE_LEN]; /* Interface Name used for
                                                      RM TCP / IP communication.
                                                      Used only RM runs over FSIP */
    UINT4  u4RxLastSeqNum; /* Last processed seq number. Bootup time
                              Or switchover time Initialized to 0 */
    UINT4  u4RxAckRcvdSeqNum; /* Last seq number for which the ACK has been
                                 received */
    UINT4  u4RxLastAppId; /* AppId of Last sync-up message processed. 
                             Bootup time Or switchover time Initialized to 0 */
    UINT4  u4RmTxEnabled; /* Flag used by RM in the ACTIVE node to decide 
                             whether to send out the trap or not*/
    UINT4  u4PrevNodeState; /* Previous State of the node */
    UINT4  u4NodeState; /* State of the node */
    UINT4  u4RmTrc;
    UINT4  u4RmModTrc; /* To view HA enabled modules' trace messages */
    UINT4  u4RmTrcBkp;
    UINT4  u4PrevNotifNodeState; /* RM notified NODE status to RM enabled
                                    protocols */
    UINT4  u4ActiveNode; /* Ip addr of ACTIVE node */
    UINT4  u4SelfNode; /* Ip addr of this node */
    UINT4  u4SelfNodeMask; /* Subnet Mask of this node */
    UINT4  u4PeerNode; /* Ip addr of peer node */
    UINT4  u4FtConnCnt;  /* No. of File Transfer Client connections */
    UINT4  u4Flags; /* Flag to identify the bit set in rcvd msg */
    UINT4  u4DynamicSyncStatus; /* To disable sending dynamic sync up Msg*/ 
    UINT4  u4RxBufNodeCount; /* Shows number of entries currently buffered
                                in RX Buff */
    INT4   i4FtSrvSockFd; /* File Transfer server sockfd - TCP socket */ 
    INT4   i4SrvSockFd; 
    UINT4  u4SwitchId;      /* Current slot number */
    UINT4  u4PrevSwitchId;  /* Previous slot number */
    UINT4  u4ConfigState;
    UINT4  u4ColdStandbyState; 
    UINT4  u4SysLogId;       /* Syslog ID for RM module*/
    UINT4  u4TrapSwitchId;
    UINT1  u1PeerNodeState; /* State of the peer node */
    UINT1  u1StaticConfigStatus; /* Static configuration restoration status */
    UINT1  u1ForceSwitchOverFlag; /* To indicate transition occured, because
                                     of force switchover or failover */
    UINT1  u1NumPeers; /* Number of peers that are up*/
    UINT1  u1ShutStartState; /* Shut and start task status */
    UINT1  u1IsBulkUpdtInProgress; /* Flg to indicate bulk updt is in
                                      progress or not. If the flag is
                                      set bulk update is in progress.
                                      Only applicable to standby side*/
    UINT1  u1IsBulkUpdtFromSwAudit;/* Flg to indicate switchover
                                      notification must be sent
                                      along with sync up completed
                                      notification. Should not 
                                      send switchover notification
                                      in S/W Audit failure case */
    UINT1  u1NxtFreeNotifArrayId;
    UINT1  u1FileRcvCount;         /* No of files transferred from the
                                    * active node
                                    */
    BOOL1  bIsRxBufProcAllowed; /* RM_TRUE / RM_FALSE
                                   Default Value  is RM_TRUE */
    BOOL1  bSeqMissMatch; /* RM_TRUE = Sequence Missmatch detected and 
                             recovery timer is running */
    BOOL1  bIsTransInPrgs; /* RM_TRUE = Transition in progress state
                              Buffered messages cleaning is in progress*/
    UINT1  au1StkMacAddr[RM_MAC_ADDR_LEN];
    UINT1  u1ConnRetryCnt; /* Connection retry count to reach the peer*/
    BOOL1  bIgnoreAbort; /* Indicates whether to supress sync failures or not */
    BOOL1  bRestartFlag;  /* Indicates whether to restart the standby node 
                             on or not on receiving abort or buffer 
                             outage messages */
    UINT1  u1RestartRetryCnt; /* Indicates the max. number of times the standby
                                node will be restarted on receiving abort or 
                                buffer outage messages */
    UINT1  u1RestartCntr;/* The number of times the standby node is restarted */
    BOOL1  bIsRxBuffFull; /* Rx buffer full indicator */
    BOOL1  bIsTxBuffFull; /* Sync message transmission failed indicator */
    BOOL1  bIsRestartInPrgs;  /*Flag to inidicate whether the standby node is 
                               restarted */
    /* HITLESS RESTART */
    UINT1  u1HRFlag; /* Flag to indicate the status of hitless restart 
                        feature. (RM_HR_STATUS_DISABLE/RM_HR_STATUS_STORE/
                        RM_HR_STATUS_RESTORE) */ 
    UINT1  u1SysLogFileStatus;
    UINT4  u4StaticSyncStatus; /* Status of Static sync-up */
    UINT4  u4StaticBulkSyncStatus; /* Status of Static sync-up */
    UINT4  u4DynBulkSyncStatus; /* Status of dynamic bulk sync-up */
    UINT1  au1DynSyncStatus[RM_MAX_APPS]; /* Status of dynamic sync-up */
    UINT1  u1LastBulkSyncAppId; /* This variable will contain the Last App ID
                                 * for which Bulk request is initiated */
    UINT1  u1IssuNumPeers; /* Number of peers that are up during ISSU */
    UINT1  au1Pad[3];
} tRmInfo;

/* Used by the protocols and the management modules to post the dynamic
 * sync-up message and static configuration information to the RM task
 * respectively.*/
typedef struct {
    union {
        tRmMsg *pRmMsg; /* Message posted by protocols*/
        tRmNotifConfChg RmNotifConfChg; /* Configuration specific information */
    } unMsgParam;
    UINT1      u1MsgType; /* RM_STATIC_CONF_INFO or RM_DYNAMIC_MSG*/
    UINT1      u1Padding;
    UINT2      u2Padding;
#define DynSyncMsg unMsgParam.pRmMsg
#define ConfInfo unMsgParam.RmNotifConfChg
}tRmPktQMsg;

/* global RM Conf Data structure*/
typedef struct {
    tTMO_SLL     RmConfigFailBuff ; /* SLL used to store the details of those
                                     * configurations (tRmConfFailBuff)which
                                     * have failed */
    tOsixTaskId  RmConfigSyncTaskId; /* Identifier of the task handling the
                                      * processing of static configuration 
                                      * syncup messages */
    tOsixSemId   RmConfSemId; /*Semaphore Identifier*/
    tOsixQId     RmConfigSyncQId ; /* Identifier of the queue to which the
                                    * static configuration syncup messages
                                    * will be posted by the standby RM */
    tMemPoolId  RmConfigFailBuffPoolId; /* Identifier of the memory pool from
                                         * which memory is allocated for each
                                         * entry in the failure buffer */
    tMemPoolId   RmConfigSyncQPoolId; /* mempool id for tRmConfigQMsg */
    BOOL1        bBulkUpdStatus; /* Indicates the bulk updation has been 
                                  * completed by all the protocols or not */
    UINT1        au1Pad [3]; 
} tRmConfInfo;

typedef struct {
    tTMO_SLL_NODE           NextConfMsgNode; /* Pointer to the node where the
                                              * information related to the next 
                                              * configuration which is failed,
                                              * is maintained */    
    tSNMP_OID_TYPE         *pFailedOid; /* Object Identifier of the failed
                                         * configuration */
    tSNMP_MULTI_DATA_TYPE  Data; /* Value to be set for the given mib
                                  * object */
}tRmConfFailBuff;

typedef struct {
    tRmMsg        *pData ; /* RM message pointer which contains the 
                            * configuration syncup message */
    UINT4         u4SeqNo; /* Sequence number associated with this message*/    
    UINT2         u2DataLen; /* Length of the message */
    UINT1         au1Pad [2];
} tRmConfigMsg;

/* Used for posting the message to CONF_SYNCUP task*/
typedef struct {
    tRmConfigMsg RmConfigMsg ; 
} tRmConfigQMsg;

/* Contains the list of OIDs which need to be skipped at Standby node*/
typedef struct {
    CHR1 *pOidString;
}tRmOidFilterList;

typedef struct __FileInfo
{
    CHR1 c1FileName[RM_MAX_FILE_NAME_LEN];
} tFileInfo;

typedef struct {
   UINT4 u4IpAddr;
   UINT4 u4NetMask;
}tNodeInfo;

/* RM Pkt Mem-Block */
typedef struct _tRmPktBlock
{
    UINT1   au1RmPktBlock[RM_MAX_SYNC_PKT_LEN];
}tRmPktBlock;

typedef struct _tRmSwitchOverTime
{
    UINT4   u4AppId;
    UINT4   u4EntryTime; 
    UINT4   u4ExitTime;
    UINT4   u4SwitchOverTime;
    UINT1   au1AppName[RM_MAX_FILE_NAME_LEN]; 
}tRmSwitchOverTime;
/* Sizing Params enum. */
enum
{
   RM_TX_BUF_SIZING_ID,
   RM_CTRL_MSG_SIZING_ID,
   RM_APP_INFO_SIZING_ID,
   RM_NODE_INFO_SIZING_ID,
   RM_NOTIFY_MSG_SIZING_ID,
   RM_RX_BUF_SIZINZG_ID,
   RM_RXBUF_PKT_SIZING_ID,
   RM_PKT_Q_SIZING_ID,
   RM_CFG_Q_SIZING_ID,
   RM_CFG_FAIL_BUF_SIZING_ID,
   RM_PKT_MSG_SIZING_ID,
   RM_PEER_TBL_SIZING_ID,
   RM_NO_SIZING_ID,
};
#endif /* _RM_TDFS_H */
