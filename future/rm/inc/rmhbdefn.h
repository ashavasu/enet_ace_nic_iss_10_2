/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbdefn.h,v 1.8 2013/03/08 13:37:47 siva Exp $
*
* Description: Data structure used by Redundancy manager module. 
*********************************************************************/
#ifndef _RM_HB_DEFN_H
#define _RM_HB_DEFN_H

#define RM_HB_PROTO_SEM_NAME  (CONST UINT1 *) "RMHS"

#define RM_HB_DEF_PRIORITY 1

/* Heart Beat interval - in terms of msecs */
/* To convert into STUPS, divide by NO_OF_MSECS_PER_UNIT
   10msec = 1STUPS,
   1000msecs = 1000/10 = 100 STUPS
   Increasing default HB interval results in increasing 4 times of
   the change in the peer dead interval. Hence switchover time
   will be increased and may lead to timer miscalulation when
   standby becomes active.*/

#define RM_HB_DEFAULT_HB_INTERVAL 500 /* 1000 msecs = 1/2 sec */
#define RM_HB_MIN_HB_INTERVAL     10   /* 10 msecs */
#define RM_HB_MAX_HB_INTERVAL     5000 /* 5 secs */

/* peer dead interval multiplier */
#define RM_HB_MIN_PEER_DEAD_INT_MULTIPLIER     4
#define RM_HB_MAX_PEER_DEAD_INT_MULTIPLIER     10
#define RM_HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER \
    RM_HB_MIN_PEER_DEAD_INT_MULTIPLIER

/* Peer node dead interval = 4 times of HB interval */
#define RM_HB_DEFAULT_PEER_DEAD_INTERVAL \
    (RM_HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER * RM_HB_DEFAULT_HB_INTERVAL)

#define RM_HB_MIN_PEER_DEAD_INTERVAL     40    /* 40 msecs */
#define RM_HB_MAX_PEER_DEAD_INTERVAL     20000 /* 20 secs */


/* ACTIVE side max wait time for the
 *  * force switchover ack from peer */
#define RM_HB_MAX_FSW_ACK_TIME  (2 * RM_HB_GET_PEER_DEAD_INTERVAL()) 

/* RM protocol number 250 is used (which is un-used in /etc/protocols). */
#define RM_HB_PROTO          250

/* RM  Multicast ip addr used is 224.0.0.250 */
#define RM_HB_MCAST_IPADDR   0xE00000FA

/* Allocate CRU buf and set the valid offset to point to 0 */
#define RM_HB_ALLOC_RX_BUF(size)    CRU_BUF_Allocate_MsgBufChain (size, 0)
#define RM_HB_ALLOC_TX_BUF(size)    \
            CRU_BUF_Allocate_MsgBufChain (size+RM_HDR_LENGTH, RM_HDR_LENGTH)
#define RM_HB_COPY_TO_LINEAR_BUF(CruBuf, LinBuf, offset, Len) \
        CRU_BUF_Copy_FromBufChain (CruBuf, (UINT1 *)LinBuf, offset,Len)

#define RM_HB_VERSION_NUM  1

#define RM_HB_CALC_CKSUM UtlIpCSumCruBuf 
#define RM_HB_LINEAR_CALC_CKSUM UtlIpCSumLinBuf 

/* Macro to prepend to RM_HB_ Hdr to data */
#define RM_HB_PREPEND_BUF(pBuf, pSrc, u4Size) \
        CRU_BUF_Prepend_BufChain(pBuf, pSrc, u4Size) 
   
/* Max. read/write buffer size */
#define  RM_HB_MAX_BUF_SIZE     100

#define  RM_HB_IF_NAME_LEN  16

#define  RM_HB_DEF_MSG_LEN            OSIX_DEF_MSG_LEN

#define  MAX_IP_HB_PKT_LEN 100

/* Rm Heart Beat message format */
typedef struct {

   /* MsgType - HEART_BEAT */
   UINT4 u4MsgType;

   /* Heart Beat Interval */
   UINT4 u4HbInterval;

   /* Peer Dead Interval */
   UINT4 u4PeerDeadInterval;

   /* Peer Dead Interval Multiplier */
   UINT4 u4PeerDeadIntMultiplier;

   /* Active Node IP addr */
   UINT4 u4ActiveNode;

   /* Self node IP addr */
   UINT4 u4SelfNode;

   /* Peer node IP addr */
   UINT4 u4PeerNode;
   
   /* Self node Priority */
   UINT4 u4SelfNodePriority;

   /* Peer node Priority */
   UINT4 u4PeerNodePriority;
    
   /* Bitmap - Bit 0 is currently used for force-switchover */   
   UINT4 u4Flags;

} tRmHbData;

typedef struct {
   UINT4 u4IpAddr;
   UINT4 u4NetMask;
}tHbNodeInfo;

/* RM HB Timer Types */
enum
{
    RM_HB_HEARTBEAT_TIMER              = 1, /* Heart Beat Timer */
    RM_HB_PEER_DEAD_TIMER              = 2, /* Peer Dead Timer */
    RM_HB_FSW_ACK_TIMER                = 3, /* Force Switchover Ack Timer */
    RM_HB_MAX_TIMER_TYPES              = 4 /* Max Timer types */
};

typedef struct { 
    tOsixSemId  HbProtoSemId; /* HB protocol Semaphore Identifier */ 
    tOsixTaskId HbTaskId; /* Trace value */
    tOsixQId     RmHbQueueId; /* Queue to Process RM HB Messages
                                 received on Stacking Port */
    /* Timers*/
    tTmrDesc    aTmrDesc[RM_HB_MAX_TIMER_TYPES];
                          /* Timer data struct that contains
                             func ptrs for timer handling and
                             offsets to identify the data
                             struct containing timer block.
                             Timer ID is the index to this
                             data structure */ 
    /* RM HB Timer List ID */
    tTimerListId    TmrListId;
    /* RM HB Timer block to hold the timer id */
    tTmrBlk    RmHbTmrBlk[RM_HB_MAX_TIMER_TYPES];
    UINT4  u4HbTrc; /* Trace value */
    UINT4  u4HbTrcBkp;
    UINT4  u4HbInterval; /* Hear Beat msg interval */
    UINT4  u4PeerDeadInterval; /* Peer node dead interval */
    UINT4  u4PeerDeadIntMultiplier; /* Peer node dead interval Multiplier */
    UINT4  u4PrevNodeState; /* Previous State of the node */
    UINT4  u4NodeState; /* State of the node */
    UINT4  u4ActiveNode; /* Ip addr of ACTIVE node */
    UINT4  u4SelfNode; /* Ip addr of this node */
    UINT4  u4SelfNodeMask; /* Subnet Mask of this node */
    UINT4  u4PeerNode; /* Ip addr of peer node */
    UINT4  u4SelfNodePriority;  /* Self node priority */
    UINT4  u4PeerNodePriority; /* Peer node priority */ 
    UINT4  u4Flags; /* Flag to identify the bit set in rcvd msg */ 
    UINT4  u4PrevNotifNodeState;  
    INT4   i4RawSockFd; /* RAW socket to send/rcv HB & other msgs */  
    UINT1  u1PeerNodeState; /* State of the peer node */ 
    UINT1  u1ForceSwitchOverFlag;
    UINT1  u1StdbySoftRbt;
    UINT1  u1Pad;
} tRmHbInfo;

/****************************************************************************/

#define RM_HB_GET_ACTIVE_NODE_ID()  (gRmHbInfo.u4ActiveNode)
#define RM_HB_GET_SELF_NODE_ID()    (gRmHbInfo.u4SelfNode)
#define RM_HB_GET_SELF_NODE_MASK()  (gRmHbInfo.u4SelfNodeMask)
#define RM_HB_GET_PEER_NODE_ID()    (gRmHbInfo.u4PeerNode)
#define RM_HB_GET_NODE_STATE()      (gRmHbInfo.u4NodeState)
#define RM_HB_GET_PREV_NOTIF_NODE_STATE()      (gRmHbInfo.u4PrevNotifNodeState)
#define RM_HB_GET_PREV_NODE_STATE() (gRmHbInfo.u4PrevNodeState)
#define RM_HB_GET_PEER_NODE_STATE() (gRmHbInfo.u1PeerNodeState)
#define RM_HB_GET_TRC_LEVEL()       (gRmHbInfo.u4HbTrc)
#define RM_HB_GET_SELF_NODE_PRIORITY()    (gRmHbInfo.u4SelfNodePriority)
#define RM_HB_GET_PEER_NODE_PRIORITY()    (gRmHbInfo.u4PeerNodePriority)
#define RM_HB_GET_HB_INTERVAL()     (gRmHbInfo.u4HbInterval)
#define RM_HB_GET_PEER_DEAD_INTERVAL()     (gRmHbInfo.u4PeerDeadInterval)
#define RM_HB_GET_PEER_DEAD_INT_MULTIPLIER() (gRmHbInfo.u4PeerDeadIntMultiplier)
#define RM_HB_GET_FLAGS()           (gRmHbInfo.u4Flags)
#define RM_HB_FORCE_SWITCHOVER_FLAG() (gRmHbInfo.u1ForceSwitchOverFlag)
#define RM_HB_PROTO_SEM()           (gRmHbInfo.HbProtoSemId)
#define RM_HB_TASK_ID            (gRmHbInfo.HbTaskId)
#define RM_HB_RAW_SOCK_FD()         (gRmHbInfo.i4RawSockFd)
#define RM_HB_QUEUE_ID         (gRmHbInfo.RmHbQueueId)

#define RM_HB_SET_TRC_LEVEL(TrcLevel) \
                                 (gRmHbInfo.u4HbTrc = TrcLevel)
#define RM_HB_SET_SELF_NODE_PRIORITY(Pri) \
                             (gRmHbInfo.u4SelfNodePriority = Pri)
#define RM_HB_SET_PEER_NODE_PRIORITY(Pri) \
                             (gRmHbInfo.u4PeerNodePriority = Pri)
#define RM_HB_SET_HB_INTERVAL(HbInt) \
{\
    gRmHbInfo.u4HbInterval = HbInt; \
    RM_HB_SET_PEER_DEAD_INTERVAL(gRmHbInfo.u4HbInterval * \
                              RM_HB_GET_PEER_DEAD_INT_MULTIPLIER()); \
}
#define RM_HB_SET_PEER_DEAD_INTERVAL(PeerDeadInt) \
                             (gRmHbInfo.u4PeerDeadInterval = PeerDeadInt)

#define RM_HB_SET_PEER_DEAD_INT_MULTIPLIER(PeerDeadIntMultiplier) \
{\
    gRmHbInfo.u4PeerDeadIntMultiplier = PeerDeadIntMultiplier; \
    RM_HB_SET_PEER_DEAD_INTERVAL(RM_HB_GET_HB_INTERVAL() * \
                                 gRmHbInfo.u4PeerDeadIntMultiplier);\
}

#define RM_HB_SET_ACTIVE_NODE_ID(NodeIp) \
                             (gRmHbInfo.u4ActiveNode = NodeIp)
#define RM_HB_SET_PEER_NODE_ID(NodeIp) \
                             (gRmHbInfo.u4PeerNode = NodeIp)

#define RM_HB_TAKE_TRC_BACKUP() \
    (gRmHbInfo.u4HbTrcBkp = RM_HB_GET_TRC_LEVEL())
#define RM_HB_RESTORE_TRC_BACKUP() \
    (RM_HB_SET_TRC_LEVEL(gRmHbInfo.u4HbTrcBkp))

/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is SET */
#define RM_HB_TRC_START_SWITCHOVER() \
{\
    if ((RM_HB_GET_TRC_LEVEL() & RM_SWITCHOVER_TRC)) \
    {\
        RM_HB_TAKE_TRC_BACKUP();         \
        RM_HB_TRC (RM_SWITCHOVER_TRC, "---SwitchOver Started[HB]---\n");\
        RM_HB_SET_TRC_LEVEL(RM_SWITCHOVER_TIME_TRC); \
    }\
}
    
/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is RESET */
#define RM_HB_TRC_FINISH_SWITCHOVER() \
{\
    if ((RM_HB_GET_TRC_LEVEL() & RM_SWITCHOVER_TRC)) \
    {\
        RM_HB_TRC (RM_SWITCHOVER_TRC, "---SwitchOver Finished[HB]---\n");\
        RM_HB_RESTORE_TRC_BACKUP();       \
    }\
}

#define RM_HB_SET_NODE_STATE(state) \
{\
    if (RM_TRANSITION_IN_PROGRESS == state) \
    {\
        RM_HB_TRC_START_SWITCHOVER();          \
    }\
    else if (RM_TRANSITION_IN_PROGRESS == RM_HB_GET_PREV_NODE_STATE()) \
    {\
        RM_HB_TRC_FINISH_SWITCHOVER(); \
    }\
    gRmHbInfo.u4NodeState = state; \
}
#define RM_HB_SET_PREV_NOTIF_NODE_STATE(state) \
                             (gRmHbInfo.u4PrevNotifNodeState = state)
#define RM_HB_SET_PREV_NODE_STATE(state) \
                             (gRmHbInfo.u4PrevNodeState = state)
#define RM_HB_SET_PEER_NODE_STATE(state) \
                             (gRmHbInfo.u1PeerNodeState = state)
#define RM_HB_SET_FLAGS(flags) \
                             (gRmHbInfo.u4Flags |= flags)

#define RM_HB_RESET_FLAGS(flags) \
                             (gRmHbInfo.u4Flags &= ~flags)

#define RM_HB_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define RM_HB_DATA_PUT_2_BYTE(pMsg, u4Offset, u2Value, i4RetVal) \
{\
   UINT2  LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   i4RetVal = \
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 2);\
}

#define RM_HB_LOCK()              RmHbLock ()
#define RM_HB_UNLOCK()            RmHbUnLock ()

#define RM_HB_GET_TASK_ID           OsixGetTaskId
#define RM_HB_SEND_EVENT            OsixEvtSend
#define RM_HB_RECV_EVENT            OsixEvtRecv
#define RM_HB_CREATE_QUEUE          OsixQueCrt
#define RM_HB_DELETE_QUEUE            OsixQueDel
#define RM_HB_DELETE_SEMAPHORE(semid) OsixSemDel(semid)
#define RM_HB_SEND_TO_QUEUE         OsixQueSend 
#define RM_HB_RECEIVE_FROM_QUEUE    OsixQueRecv
#define RM_HB_Q_NAME                "RMHQ"
/* RM ACTIVE node discovery state machine - STATES & EVENTS */
/* RM events */
enum {
   RM_HB_1WAY_RCVD = 0,
   RM_HB_2WAY_RCVD,
   RM_HB_ACTV_ELCTD,
   RM_HB_PEER_DOWN,
   RM_HB_PRIORITY_CHG,
   RM_HB_IGNORE_PKT,
   RM_HB_FSW_RCVD,
   RM_HB_FSW_ACK_RCVD,
   RM_HB_MAX_EVENTS  /* Count of events. If any new events, add before this  */
};

/* RM software reboot states */
enum
{
    RM_HB_STDBY_SOFT_RBT_NOT_OCCURED = 0,
    RM_HB_STDBY_SOFT_RBT_NODE_DOWN
};

#endif /* _RM_HB_DEFN_H */
