/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbprot.h,v 1.8 2013/03/08 13:37:47 siva Exp $
*
* Description: Redundancy manager HB module function prototypes. 
*********************************************************************/
/* Function Prototypes */

/* rmhbmain.c */
VOID  RmHbProcessRcvEvt (VOID);
UINT4 RmHbTaskInit (VOID);
UINT4 RmHbInit (VOID);
VOID  RmProcessHbMsg (tRmMsg * pRmData, UINT4 u4DataLen, 
                      UINT4 u4PeerAddr);
VOID  RmProcessHbMsgRcvdOnStkPort (VOID);
UINT4 RmProcessNodeId (tRmHbData * pRmHbData, UINT1 *pu1Event);
UINT4 RmProcessNodePriority (tRmHbData * pRmHbData, UINT1 *pu1Event);
UINT4 RmFormAndSendHbMsg (VOID);
UINT4 RmFormHbMsg (tRmHbData * pHbData);
INT4  RmHbWaitForOtherModulesBootUp (VOID);
UINT4 RmHbPrependRmHdr (tRmMsg * pRmMsg, UINT4 u4Size);
UINT4 RmHbSendMsg (tRmMsg * pRmMsg, UINT4 u4DestAddr);
UINT4 RmSendMsgToPeer (UINT4 u4Msg);
VOID  RmHbHandleFSWEvent (VOID);
VOID  RmHbHandleRxBufProcessedEvt (VOID);
VOID  RmHbChgNodeStateToTransInPrgs (VOID);
VOID RmPostSelfAttachEvent(VOID);
INT4 RmHbWaitForSelfAttachCompletion (VOID);
PUBLIC VOID  RmHbTxTsk (VOID);

/* rmhbtmr.c */
UINT4 RmHbTmrInit (VOID);
UINT4 RmHbTmrDeInit (VOID);
VOID  RmHbTmrExpHandler (VOID);
UINT4 RmHbTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal);
UINT4 RmHbTmrStopTimer (UINT1 u1TmrId);
UINT4 RmHbTmrRestartTimer (UINT1 u1TmrId, UINT4 u4TmrVal);
VOID  RmHbTmrHdlHbTmrExp (VOID *pArg);
VOID  RmHbTmrHdlPeerDeadTmrExp (VOID *pArg);
VOID  RmHbTmrHdlFswAckTmrExp (VOID *pArg);
INT4  RmResumeProtocolOperation (VOID);

/* rmhbsock.c */
INT4  RmHbRawSockInit (INT4 *pi4HbSockFd);
VOID  RmHbRawPktRcvd (INT4 i4SockFd);
UINT4 RmHbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, 
                       UINT4 u4DestAddr);
INT4  RmHbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, 
                      UINT4 *pu4PeerAddr);
UINT4 RmGetIfIndexFromName (UINT1 *pu1IfName);
UINT4 RmHbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo);
INT4  RmHbCloseSocket (INT4 i4SockFd);

/* rmhbsem.c */
VOID RmStateMachine (UINT1 u1Event);
VOID RmSem1WayRcvdInInitState (VOID);
VOID RmSem2WayRcvdInInitState (VOID);
VOID RmSem2WayRcvdInActvState (VOID);
VOID RmSemActvElctdInInitState (VOID);
VOID RmSemPeerDownRcvdInInitState (VOID);
VOID RmSemPeerDownRcvdInActiveState (VOID);
VOID RmSemPeerDownRcvdInStandbyState (VOID);
VOID RmSemPeerDownRcvdInTrgsInPrgs (VOID);
VOID RmSemFSWRcvdInActiveState (VOID);
VOID RmSemFSWRcvdInStandbyState (VOID);
VOID RmSemFSWAckRcvdInTrgsInPrgsState (VOID);
VOID RmSemEvntIgnore (VOID);
VOID RmSemPriorityChg (VOID);
VOID RmStartElection (VOID);
VOID RmUpdateState (UINT1 u1State);

/* rmhbapi.c */
UINT4 RmHbNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event);
VOID  RmHbNotifyProtocols (UINT1 u1Event);
VOID  RmHandleAppInitCompleted (VOID);

/*rmclr.c */
VOID  RmHbHandleShutAndStartCompleted (VOID);
