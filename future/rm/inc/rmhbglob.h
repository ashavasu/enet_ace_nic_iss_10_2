/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbglob.h,v 1.3 2013/06/19 13:25:18 siva Exp $
*
* Description: This file contains the global variable declarations  
*              used in RM Module
*********************************************************************/
#ifndef _RMHBGLOB_H
#define _RMHBGLOB_H
#ifdef _RMHBMAIN_C_
tRmHbInfo       gRmHbInfo;
UINT1           gu1KeepAliveFlag = RM_FALSE;
#else
extern tRmHbInfo       gRmHbInfo;
extern UINT1           gu1KeepAliveFlag;
#endif /* _RMHBMAIN_C_ */
#endif /* _RMHBGLOB_H */
