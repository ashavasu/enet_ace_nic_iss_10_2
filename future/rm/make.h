####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.11 2015/01/05 12:26:29 siva Exp $
#
# Description: Linux make.h for RM.
####################################################

MODULE_NAME = FutureRM

RM_BASE_DIR = $(BASE_DIR)/rm
RM_SRC_DIR = $(RM_BASE_DIR)/src
RM_INC_DIR = $(RM_BASE_DIR)/inc
RM_OBJ_DIR = $(RM_BASE_DIR)/obj
ifeq (DL2RED_TEST_WANTED, $(findstring DL2RED_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RM_TEST_DIR = $(RM_BASE_DIR)/test
endif

## currently there are no compilation switches
RM_COMPILATION_SWITCHES = -UFORTIFY

RM_INCLUDE_FILES = $(RM_INC_DIR)/rmdefn.h \
                $(RM_INC_DIR)/rmproto.h  \
                $(RM_INC_DIR)/rmtrc.h  \
                $(RM_INC_DIR)/rmincs.h \
                $(RM_INC_DIR)/rmglob.h

ifeq (DL2RED_TEST_WANTED, $(findstring DL2RED_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RM_INCLUDE_FILES += $(RM_TEST_DIR)/rmtstinc.h
endif

RM_INCLUDE_DIRS = -I$(RM_INC_DIR) \
                   $(COMMON_INCLUDE_DIRS)
                   

ifeq (DL2RED_TEST_WANTED, $(findstring DL2RED_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RM_INCLUDE_DIRS += -I$(RM_TEST_DIR)
endif

ifneq (${TARGET_OS},OS_VXWORKS)
ifeq (${RM_LNXIP},YES)
RM_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                           -DIS_SLI_WRAPPER_MODULE
endif
endif

RM_DEPENDENCIES = $(COMMON_DEPENDENCIES)  \
               $(RM_INCLUDE_FILES)     \
               $(RM_BASE_DIR)/Makefile \
               $(RM_BASE_DIR)/make.h 
