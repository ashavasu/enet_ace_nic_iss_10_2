/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmtstinc.h,v 1.1.1.1 2008/10/06 14:35:46 prabuc-iss Exp $
 *
 * Description: Header file for RM test module
 *********************************************************************/
#ifndef _RMTEST_INC_H_
#define _RMTEST_INC_H_

typedef struct {
    UINT4 u4Tx;
    UINT4 u4Rx;
    UINT1 u1FlgSyncBlocked;
    UINT1 au1Pad[3];
}tRmTstProtoInfo;

/* Test Info Structure */
typedef struct {
    tRmTstProtoInfo ProtoInfo[RM_MAX_APPS];
    UINT4  u4RxBufPeakCount; /* Shows the peak value used for Rx buffer */
    INT4   i4RxBufBuddyLeak; /* Shows Buddy Leak */
    UINT1  u1ProtoAckStatus; /* RM_TRUE = Enabled; RM_FALSE = Disable */
    UINT1  u1SeqMissStatus; /* RM_TRUE = Enabled; RM_FALSE = Disable */
    UINT1  au1Pad[2];
} tRmTestInfo;


/* Global variable declearation */
#ifdef _RMMAIN_C_
tRmTestInfo         gRmTestInfo;
#else /* ! (_RMMAIN_C_) */
#ifdef L2RED_TEST_WANTED

/* Extern declearation */

extern tRmTestInfo  gRmTestInfo;
#endif
#endif


/* Macro Definitions */

#define RM_TEST_BUDDY_LEAK_COUNT (gRmTestInfo.i4RxBufBuddyLeak)
#define RM_RX_BUFF_PEAK_COUNT()  (gRmTestInfo.u4RxBufPeakCount)
#define RM_IS_PROTO_ACK_ENABLED() (gRmTestInfo.u1ProtoAckStatus)
#define RM_IS_SEQ_MISS_ENABLED() (gRmTestInfo.u1SeqMissStatus)

#define RM_TEST_GET_PROTO_INFO(u4AppId) (gRmTestInfo.ProtoInfo[u4AppId])
#define RM_TEST_BLOCK_PROTO_SYNCUP(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u1FlgSyncBlocked = RM_TRUE)
#define RM_TEST_ALLOW_PROTO_SYNCUP(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u1FlgSyncBlocked = RM_FALSE)
#define RM_TEST_IS_PROTO_SYNCUP_BLOCKED(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u1FlgSyncBlocked)
#define RM_TEST_GET_PROTO_TX_COUNT(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u4Tx)
#define RM_TEST_GET_PROTO_RX_COUNT(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u4Rx)
#define RM_TEST_INCR_PROTO_TX_COUNT(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u4Tx++)
#define RM_TEST_INCR_PROTO_RX_COUNT(u4AppId) \
    (gRmTestInfo.ProtoInfo[u4AppId].u4Rx++)

/* Prototype Definitions */
VOID RmTestInit (VOID);
VOID RmTestShowDebugStats (tCliHandle CliHandle);
VOID RmTestSetProtoAck (INT4 i4SetVal);
VOID RmTestSetSeqMiss(INT4 i4SetVal);
VOID RmTestShowDebuggingInfo(tCliHandle CliHandle);
VOID RmTestSetProtoSyncBlock(UINT4 u4AppId, UINT4 u4BlockFlg);
#endif /* _RMTEST_INC_H_ */
