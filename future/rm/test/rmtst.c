/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmtst.c,v 1.2 2010/08/09 09:51:17 prabuc Exp $
*
* Description: RM Test routines. Mainly used for debugging purpose.
* NOTE: This file should not be include in release packaging.
***********************************************************************/
#ifndef _RM_TEST_C_
#define _RM_TEST_C_
#include "rmincs.h"

VOID
RmTestInit (VOID)
{
    UINT4               u4AppId = 0;
    MEMSET (&gRmTestInfo, 0, sizeof (tRmTestInfo));

    /* protocol Ack is enabled */
    RM_IS_PROTO_ACK_ENABLED () = RM_TRUE;
    /* Sequence miss-match is disabled */
    RM_IS_SEQ_MISS_ENABLED () = RM_FALSE;

    for (u4AppId = 0; u4AppId < RM_MAX_APPS; u4AppId++)
    {
        RM_TEST_ALLOW_PROTO_SYNCUP (u4AppId);
    }
}

VOID
RmTestShowDebugStats (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\nDebugging Info:\n");
    CliPrintf (CliHandle, "--------------\n");
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Rx Buffer peak count", RM_RX_BUFF_PEAK_COUNT ());
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Rx Buffer Buddy Leak", RM_TEST_BUDDY_LEAK_COUNT);
    CliPrintf (CliHandle, "\n");
}

VOID
RmTestSetProtoAck (INT4 i4SetVal)
{
    if (i4SetVal == RM_TRUE)
    {
        RM_IS_PROTO_ACK_ENABLED () = RM_TRUE;
    }
    else if (i4SetVal == RM_FALSE)
    {
        RM_IS_PROTO_ACK_ENABLED () = RM_FALSE;
    }
}

VOID
RmTestSetSeqMiss (INT4 i4SetVal)
{
    if (i4SetVal == RM_TRUE)
    {
        RM_IS_SEQ_MISS_ENABLED () = RM_TRUE;
    }
    else if (i4SetVal == RM_FALSE)
    {
        RM_IS_SEQ_MISS_ENABLED () = RM_FALSE;
    }
}

VOID
RmTestSetProtoSyncBlock (UINT4 u4AppId, UINT4 u4BlockFlg)
{
    if (u4AppId < RM_MAX_APPS)
    {
        if (u4BlockFlg == RM_TRUE)
        {
            RM_TEST_BLOCK_PROTO_SYNCUP (u4AppId);
        }
        else if (u4BlockFlg == RM_FALSE)
        {
            RM_TEST_ALLOW_PROTO_SYNCUP (u4AppId);
        }
    }
}

VOID
RmTestShowDebuggingInfo (tCliHandle CliHandle)
{
    CHR1                ac1RegStatus[2];
    CHR1                ac1SyncFlg[2];
    UINT4               u4Tx = 0;
    UINT4               u4Rx = 0;
    UINT4               u4Count = 0;
    UINT4               u4RegProtoCount = 0;

    CliPrintf (CliHandle, "\nDebugging Status:\n");
    CliPrintf (CliHandle, "--------------\n");

    if (RM_IS_PROTO_ACK_ENABLED () == RM_TRUE)
    {
        CliPrintf (CliHandle, "%-30s : %-10s\n", "Protocol Ack", "Allowed");
    }
    else
    {
        CliPrintf (CliHandle, "%-30s : %-10s\n", "Protocol Ack", "Droped");
    }

    if (RM_IS_SEQ_MISS_ENABLED () == RM_TRUE)
    {
        CliPrintf (CliHandle, "%-30s : %-10s\n",
                   "Sequence Number Miss-match", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%-30s : %-10s\n",
                   "Sequence Number Miss-match", "Disabled");
    }

    CliPrintf (CliHandle, "\nDebugging Information:\n");
    CliPrintf (CliHandle, "---------------------\n");

    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Rx Buffer peak count", RM_RX_BUFF_PEAK_COUNT ());
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Rx Buffer Buddy Leak", RM_TEST_BUDDY_LEAK_COUNT);
    CliPrintf (CliHandle, "\n");

    CliPrintf (CliHandle, "\nProtocols Information:\n");
    CliPrintf (CliHandle, "---------------------\n");
    CliPrintf (CliHandle, "A - Allowed, B - Blocked, Y - Yes, N - No\n");

    CliPrintf (CliHandle, "%-7s%-12s%-4s%-5s%-10s%-10s\n",
               "AppId", "Name", "Reg", "Sync", "Tx", "Rx");
    CliPrintf (CliHandle, "%-7s%-12s%-4s%-5s%-10s%-10s\n",
               "-----", "----", "---", "----", "--", "--");

    u4RegProtoCount = 0;

    for (u4Count = 0; u4Count < RM_MAX_APPS; u4Count++)
    {
        /* Get Registration Status */
        if (gRmInfo.RmAppInfo[u4Count] != NULL)
        {
            u4RegProtoCount++;
            SPRINTF (&ac1RegStatus[0], "%s", "Y");
        }
        else
        {
            SPRINTF (&ac1RegStatus[0], "%s", "N");
        }

        /* Get Sync-Blocked status */
        if (RM_TEST_IS_PROTO_SYNCUP_BLOCKED (u4Count) == RM_TRUE)
        {
            SPRINTF (&ac1SyncFlg[0], "%s", "B");
        }
        else
        {
            SPRINTF (&ac1SyncFlg[0], "%s", "A");
        }

        /* Get Tx Count */
        u4Tx = RM_TEST_GET_PROTO_TX_COUNT (u4Count);
        /* Get Rx Count */
        u4Rx = RM_TEST_GET_PROTO_RX_COUNT (u4Count);

        CliPrintf (CliHandle, "%-7d%-12s%-4s%-5s%-10d%-10d\n",
                   u4Count, gapc1AppName[u4Count], ac1RegStatus, ac1SyncFlg,
                   u4Tx, u4Rx);
    }
    CliPrintf (CliHandle, "Total Protocol Registered with RM : %d\n",
               u4RegProtoCount);
    CliPrintf (CliHandle, "\n");

}

#endif /* _RM_TEST_C_ */
