/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmtrc.c,v 1.4 2010/08/09 09:51:15 prabuc Exp $
*
* Description: RM utility functions for trace message modifier.
***********************************************************************/
#ifndef _RM_TRC_C_
#define _RM_TRC_C_
#include "rmincs.h"

/******************************************************************************
 * Function           : RmTrcPrintModSyncMsgTrace
 * Input(s)           : pu1SyncMsg - RM Sync message
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function prints the module specific sync messages 
 ******************************************************************************/
VOID
RmTrcPrintModSyncMsgTrace (UINT1 *pu1SyncMsg)
{
    UINT4               u4RmSrcEntId = 0;
    UINT4               u4RmSeqNo = 0;
    UINT4               u4RmPktLen = 0;
    UINT4               u4RmSyncMsgSubVal = 0;

    PTR_FETCH4 (u4RmSrcEntId, (pu1SyncMsg + RM_HDR_OFF_SRC_ENTID));
    PTR_FETCH4 (u4RmSeqNo, (pu1SyncMsg + RM_HDR_OFF_SEQ_NO));
    PTR_FETCH4 (u4RmPktLen, (pu1SyncMsg + RM_HDR_OFF_TOT_LENGTH));

    if (RmTrcIsDebugTrcAllowed (u4RmSrcEntId) == RM_FAILURE)
    {
        /* This module trace is not enabled */
        return;
    }
    switch (u4RmSrcEntId)
    {
        case RM_EOAM_APP_ID:
        case RM_PNAC_APP_ID:
        case RM_LA_APP_ID:
        case RM_NP_APP_ID:
            /* These modules use 4 bytes length for message type */
            PTR_FETCH4 (u4RmSyncMsgSubVal, (pu1SyncMsg + RM_HDR_LENGTH));
            break;
        default:
            /* Other modules use 1 byte for message type */
            u4RmSyncMsgSubVal = *((UINT1 *) (pu1SyncMsg + RM_HDR_LENGTH));
            break;
    }

    if (u4RmSeqNo != 0)
    {
        RM_TRC4 (RM_SYNCUP_TRC | RM_CTRL_PATH_TRC | RM_SYNC_MSG_TRC |
                 RM_DUMP_TRC,
                 "SyncMsg: Seq# %d, PktLen %d, SubMsgVal %d, %s->RM\r\n",
                 u4RmSeqNo, (u4RmPktLen - RM_HDR_LENGTH), u4RmSyncMsgSubVal,
                 gapc1AppName[u4RmSrcEntId]);
    }
    else
    {
        /* Currently, SeqNo 0 can come for two reasons
         * 1. Bulk request message
         * 2. Trap message */
        if (u4RmSrcEntId != RM_APP_ID)
        {
            RM_TRC3 (RM_SYNCUP_TRC | RM_CTRL_PATH_TRC | RM_SYNC_MSG_TRC |
                     RM_DUMP_TRC, "SyncMsg: Seq# 0, PktLen %d, SubMsgVal %d,"
                     "Bulk request, %s->RM\r\n", (u4RmPktLen - RM_HDR_LENGTH),
                     u4RmSyncMsgSubVal, gapc1AppName[u4RmSrcEntId]);
        }
        else
        {
            RM_TRC2 (RM_SYNCUP_TRC | RM_CTRL_PATH_TRC | RM_SYNC_MSG_TRC |
                     RM_DUMP_TRC, "SyncMsg: Seq# 0, PktLen %d, SubMsgVal %d "
                     "Trap message, RM[STANDBY]->RM[ACTIVE]\r\n",
                     (sizeof (tRmNotificationMsg) + sizeof (UINT4)),
                     u4RmSyncMsgSubVal);
        }
    }
    /* Dumping the sync messsage before transmission */
    RM_PKT_DUMP (RM_DUMP_TRC, pu1SyncMsg, u4RmPktLen);
    return;
}

/******************************************************************************
 * Function           : RmTrcIsDebugTrcAllowed
 * Input(s)           : u4EntId -Module Id
 * Output(s)          : None.
 * Returns            : RM_SUCCESS - If module is enabled for traces
 *                      RM_FAILURE - If module is not enabled for traces
 * Action             : This function checks whether module debug message 
 *                      is enabled or not for a particular module.
 ******************************************************************************/
UINT1
RmTrcIsDebugTrcAllowed (UINT4 u4EntId)
{
    if ((RM_TRC_FLAG & RM_SYNC_MSG_TRC) || (RM_TRC_FLAG & RM_DUMP_TRC))
    {
        /* Filter the RM module level traces for sync message and 
         * dump traces.
         * 1. RM module can be verified by checking the first bit.
         * 2. For other module, 
         *    left shift with the entity id value to check that particular 
         *    module enabled for trace */

        if (!(gRmInfo.u4RmModTrc & (0x1 << u4EntId)))
        {
            /* Module level is not enabled for this trace */
            return RM_FAILURE;
        }
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmTrcHandle
 * Input(s)           : u4FlagVal - Debug messages flag value
 *                      pc1Fmt    - Format of the trace message
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function is used to print the trace message.
 ******************************************************************************/
VOID
RmTrcHandle (UINT4 u4FlagVal, CONST CHR1 * pc1Fmt, ...)
{
    static CHR1         ac1Buf[RM_MAX_TRC_NAME_LEN];
    va_list             ap;
    CHR1               *pTemp = NULL;

    /* Verify the flag value with the global trace value */
    if (!(RM_TRC_FLAG & u4FlagVal))
    {
        /* This trace flag is not set */
        return;
    }

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));
    pTemp = ac1Buf;
    /* Copy the trace time string */
    UtlGetTimeStr (pTemp);
    pTemp = pTemp + STRLEN (ac1Buf);
    switch (RM_GET_NODE_STATE ())
    {
        case RM_ACTIVE:
            SPRINTF (pTemp, ": RM[ACTIVE]: ");
            break;
        case RM_STANDBY:
            SPRINTF (pTemp, ": RM[STANDBY]: ");
            break;
        case RM_INIT:
            SPRINTF (pTemp, ": RM[INIT]: ");
            break;
        default:
            /* Invalid state for trace */
            return;
    }
    pTemp = pTemp + STRLEN (ac1Buf);
    va_start (ap, pc1Fmt);
    vsprintf (&ac1Buf[STRLEN (ac1Buf)], pc1Fmt, ap);
    if (u4FlagVal & RM_NOTIF_TRC)
    {
        /* Send the notification messages to SysLog module */
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gRmInfo.u4SysLogId, " %s ", ac1Buf));
    }
    else
    {
        /* Other traces are printed in file or console using the 
         * below utility function */
        UtlTrcPrint (ac1Buf);
    }
    va_end (ap);
    return;
}

/******************************************************************************
 * Function           : RmPktDumpTrc
 * Input(s)           : pBuf      - Liner buffer pointer for the packet to be
 *                                  dumped
 *                      Length    - Length of the packet to be dumped
 *                      Str       -
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function dumped the packet and also print the
 *                      details of the RM header. Payload is shown with Hex 
 *                      values.
 ******************************************************************************/
VOID
RmPktDumpTrc (UINT1 *pu1Buf, UINT4 u4Length)
{
#define RM_MAX_DUMP_LEN        512
#define RM_PRINT_BUF_LEN       80

    UINT4               u4Count = 0;
    UINT4               u4PrintCount = 0;
    CHR1                au1OutBuf[RM_PRINT_BUF_LEN];    /* Print Buf */
    tRmHdr              RmHdr;

    if (u4Length > RM_MAX_DUMP_LEN)
    {
        u4Length = RM_MAX_DUMP_LEN;
    }

    /* Read RM Header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));
    PTR_FETCH2 (RmHdr.u2Version, (pu1Buf + RM_HDR_OFF_VERSION));
    PTR_FETCH2 (RmHdr.u2Chksum, (pu1Buf + RM_HDR_OFF_CKSUM));
    PTR_FETCH4 (RmHdr.u4TotLen, (pu1Buf + RM_HDR_OFF_TOT_LENGTH));
    PTR_FETCH4 (RmHdr.u4MsgType, (pu1Buf + RM_HDR_OFF_MSG_TYPE));
    PTR_FETCH4 (RmHdr.u4SrcEntId, (pu1Buf + RM_HDR_OFF_SRC_ENTID));
    PTR_FETCH4 (RmHdr.u4DestEntId, (pu1Buf + RM_HDR_OFF_DEST_ENTID));
    PTR_FETCH4 (RmHdr.u4SeqNo, (pu1Buf + RM_HDR_OFF_SEQ_NO));

    /* Print the RM Header */
    UtlTrcPrint ("================== RM HEADER ==========================\r\n");
    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %d\r\n",
             "RM Version", RmHdr.u2Version);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: 0x%02x\r\n",
             "Checksum", RmHdr.u2Chksum);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u [Hdr(%d)+Payload(%u)]\r\n",
             "Total Length", RmHdr.u4TotLen, (INT4) RM_HDR_LENGTH,
             (RmHdr.u4TotLen - (INT4) RM_HDR_LENGTH));
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u\r\n",
             "Message Type", RmHdr.u4MsgType);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %s\r\n",
             "Source Entity Id", gapc1AppName[RmHdr.u4SrcEntId]);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %s\r\n",
             "Destination Entity Id", gapc1AppName[RmHdr.u4DestEntId]);
    UtlTrcPrint (au1OutBuf);

    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) au1OutBuf, "%-22s: %u\r\n",
             "Sequence Number", RmHdr.u4SeqNo);
    UtlTrcPrint (au1OutBuf);

    /* Read and Print the Payload */
    UtlTrcPrint ("----- PAYLOAD: -------\r\n");
    MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);

    u4PrintCount = 0;
    while (u4Count < u4Length)
    {
        SPRINTF ((CHR1 *) & au1OutBuf[u4PrintCount], "%02x ",
                 *(pu1Buf + u4Count));

        u4Count++;
        u4PrintCount = u4PrintCount + 3;    /* need 3 bytes for "%02x " */

        if (((u4Count % 16) == 0) ||    /* Go to next line */
            (u4Count == u4Length))    /* Payload may be less than 16 */
        {
            SPRINTF ((CHR1 *) & au1OutBuf[u4PrintCount], "\n");
            /* Tailing '\0' char will be added along with the above 
             * statement */
            UtlTrcPrint (au1OutBuf);
            MEMSET (&au1OutBuf[0], 0, RM_PRINT_BUF_LEN);
            u4PrintCount = 0;
        }
    }
    UtlTrcPrint
        ("====================== END ============================\r\n\n");
}

#endif /* _RM_TRC_C_ */
