/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmclr.c,v 1.32 2015/01/05 12:26:30 siva Exp $
*
* Description: RM backplane comm. lost and restored handling
***********************************************************************/
#include "rmincs.h"
#ifdef MBSM_WANTED
#include "mbsnp.h"
#endif

/******************************************************************************
 * Function           : RmHandleCommLostAndRestoredEvent
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Handles the backplane comm. lost and restored case 
 ******************************************************************************/

VOID
RmHandleCommLostAndRestoredEvent (VOID)
{
    RM_UNLOCK ();
    /*Since connection is lost and system reqiures restart IssSystemRestart() is called */
    IssSystemRestart ();
}

/******************************************************************************
 * Function           : RmShutStartTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Rm Shutdown and start of registered modules.
 ******************************************************************************/
VOID
RmShutStartTaskMain (INT1 *pi1Arg)
{
    UNUSED_PARAM (pi1Arg);
    /* This deletes the static and dynamic entries of registered modules */
    /* This prevents the RM NODE transitions until 
     * this shut and start is finished. */
    MGMT_LOCK ();
    RM_SET_PREV_NODE_STATE (RM_INIT);
    RM_SET_NODE_STATE (RM_INIT);
#ifdef CLI_WANTED
    /* It is used to logout the existing CLI console */
    CliBlockConsole ();
#endif
    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_TRUE);
    RM_TRC (RM_CRITICAL_TRC, "RM communication is restored. \n"
            "Node state transition in progress...\n");
    RM_SET_PREV_NOTIF_NODE_STATE (RM_INIT);
#ifdef L2RED_WANTED
#ifdef MBSM_WANTED
    /* This stops the stack task to avoid unnecessary ATTACH and DETACH 
     * when shut and start in progress */
    if (ISS_GET_STACKING_MODEL () != ISS_CTRL_PLANE_STACKING_MODEL)
    {
        RmMbsmNpProcRmNodeTransition (0, RM_ACTIVE, RM_INIT);
    }
#endif
#endif

    if (RmRestartProtocols () == RM_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmShutStartTaskMain: "
                "Restarting of the protocols failed. Rebooting "
                "the standby node.\r\n");
        MGMT_UNLOCK ();
        RmSystemRestart ();
        return;
    }

    /* Inform the RM task to proceed for node transition based on the 
     * current state */

    HbApiSendRmEventToHb (HB_APP_INIT_COMPLETED);
}

/******************************************************************************
 * Function           : RmRestartProtocols 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Rm shutdowns and restarts the registered modules.
 ******************************************************************************/
UINT1
RmRestartProtocols (VOID)
{
    UINT4               u4ConnIdx = 0;
    UINT1               RmShutModList[RM_APP_LIST_SIZE];
    BOOL1               bResult = OSIX_FALSE;

    RM_SHUT_START_STATE () = RM_SHUT_START_IN_PROGRESS;
    /* Delete the tasks if the STANDBY requested files construction is in progress */
    MEMSET (&RmShutModList, 0, sizeof (RmShutModList));
    for (u4ConnIdx = 0; u4ConnIdx < RM_MAX_SESSIONS; u4ConnIdx++)
    {
        if ((gRmInfo.RmFtConnTbl[u4ConnIdx].u4Status == RM_FT_CLIENT_ACTIVE) &&
            (gRmInfo.RmFtConnTbl[u4ConnIdx].u4TskId != 0))
        {
            OsixTskDel (gRmInfo.RmFtConnTbl[u4ConnIdx].u4TskId);
            MEMSET (&(gRmInfo.RmFtConnTbl[u4ConnIdx]), 0,
                    sizeof (tRmFtConnInfo));
        }
    }
    /* Modules shutdown 
     * Following shutdown of RM registered modules do the 
     * deallocation / deletion of the Timers, Memory pools etc.*/
#ifdef MBSM_WANTED
    if (RM_IS_APPL_REGISTERED (RM_MBSM_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_MBSM_APP_ID,
                              sizeof (RmShutModList));
        if (MbsmRedRmInitAndDeRegister () == MBSM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MBSM RED deregistration is failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef MSR_WANTED
    if (RM_IS_APPL_REGISTERED (RM_MSR_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_MSR_APP_ID,
                              sizeof (RmShutModList));
        MsrRmDeInit ();
    }
#endif
#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (RM_IS_APPL_REGISTERED (RM_NP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_NP_APP_ID,
                              sizeof (RmShutModList));
        if (NpRedDeInit () == FNP_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "NP RED deinitialization is failed\n");
            return RM_FAILURE;
        }
    }
#endif
#endif
#ifdef BGP_WANTED
    if (RM_IS_APPL_REGISTERED (RM_BGP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_BGP_APP_ID,
                              sizeof (RmShutModList));
        Bgp4ApiModuleDisable ();
    }
#endif
#ifdef LLDP_WANTED
    if (RM_IS_APPL_REGISTERED (RM_LLDP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_LLDP_APP_ID,
                              sizeof (RmShutModList));
        LldpApiModuleShutDown ();
    }
#endif
#ifdef OSPF_WANTED
    if (RM_IS_APPL_REGISTERED (RM_OSPF_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_OSPF_APP_ID,
                              sizeof (RmShutModList));
        if (OspfShutdown () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "OSPF module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef OSPF3_WANTED
    if (RM_IS_APPL_REGISTERED (RM_OSPFV3_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_OSPFV3_APP_ID,
                              sizeof (RmShutModList));
        V3OspfApiModuleShutDown ();
    }
#endif
#ifdef ARP_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ARP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ARP_APP_ID,
                              sizeof (RmShutModList));
        ArpDeInit ();
    }
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ND6_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ND6_APP_ID,
                              sizeof (RmShutModList));
        Ip6Shutdown ();
    }
#endif
#endif

#ifdef MPLS_WANTED
    if (RM_IS_APPL_REGISTERED (RM_MPLS_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_MPLS_APP_ID,
                              sizeof (RmShutModList));
        if (MplsShutdownModule () == MPLS_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MPLS module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    if (RM_IS_APPL_REGISTERED (RM_SNOOP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_SNOOP_APP_ID,
                              sizeof (RmShutModList));
        if (SnoopModuleShutdown () == SNOOP_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "SNOOPING module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef ELMI_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ELMI_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ELMI_APP_ID,
                              sizeof (RmShutModList));
        ElmModuleShutdown ();
    }
#endif
#ifdef ERPS_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ERPS_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ERPS_APP_ID,
                              sizeof (RmShutModList));

        if (ErpsApiModuleShutDown () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ERPS module shutdown failed\n");
        }
    }
#endif
#ifdef ELPS_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ELPS_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ELPS_APP_ID,
                              sizeof (RmShutModList));
        ElpsApiModuleShutDown ();
    }
#endif
    if (RM_IS_APPL_REGISTERED (RM_ACL_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ACL_APP_ID,
                              sizeof (RmShutModList));
        AclShutdown ();
    }
    if (RM_IS_APPL_REGISTERED (RM_ISS_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ISS_APP_ID,
                              sizeof (RmShutModList));
        IssPIDeInitPortIsolationTable ();
#ifdef L2RED_WANTED
        IssRedDeInit ();
        IssRedInit ();
#endif
    }
#ifdef ECFM_WANTED
    if (RM_IS_APPL_REGISTERED (RM_ECFM_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_ECFM_APP_ID,
                              sizeof (RmShutModList));
        if (EcfmCcAndLbLtShutDownModule () == ECFM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ECFM module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef PBB_WANTED
    if (RM_IS_APPL_REGISTERED (RM_PBB_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_PBB_APP_ID,
                              sizeof (RmShutModList));
        if (PbbShutdownPbbModule () == PBB_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PBB module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef PBBTE_WANTED
    if (RM_IS_APPL_REGISTERED (RM_PBBTE_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_PBBTE_APP_ID,
                              sizeof (RmShutModList));
        PbbTeShutdownModule ();
    }
#endif
#ifdef MRP_WANTED
    if (RM_IS_APPL_REGISTERED (RM_MRP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_MRP_APP_ID,
                              sizeof (RmShutModList));
        if (MrpApiShutdownModule () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MRP module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif

#ifdef VLAN_WANTED
    if (RM_IS_APPL_REGISTERED (RM_VLANGARP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_VLANGARP_APP_ID,
                              sizeof (RmShutModList));
#ifdef GARP_WANTED
        GarpAppReleaseMemory ();
#endif
        if (VlanShutdownModule () == VLAN_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "VLAN module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    if (RM_IS_APPL_REGISTERED (RM_RSTPMSTP_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_RSTPMSTP_APP_ID,
                              sizeof (RmShutModList));
        if (AstShutdownModule () == RST_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "STP module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif

#if defined(PIM_WANTED) || defined(PIMV6_WANTED)

    if (RM_IS_APPL_REGISTERED (RM_PIM_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_PIM_APP_ID,
                              sizeof (RmShutModList));
        PimModuleShut ();
    }

#endif

#ifdef LA_WANTED
    if (RM_IS_APPL_REGISTERED (RM_LA_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_LA_APP_ID,
                              sizeof (RmShutModList));
        if (LaShutDown () == LA_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "LA module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef PNAC_WANTED
    if (RM_IS_APPL_REGISTERED (RM_PNAC_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_PNAC_APP_ID,
                              sizeof (RmShutModList));
        PnacModuleShutDown ();
    }
#endif
#ifdef EOAM_WANTED
    if (RM_IS_APPL_REGISTERED (RM_EOAM_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_EOAM_APP_ID,
                              sizeof (RmShutModList));
        EoamModuleShutDown ();
    }
#endif
#ifdef QOSX_WANTED
    if (RM_IS_APPL_REGISTERED (RM_QOS_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_QOS_APP_ID,
                              sizeof (RmShutModList));
        if (QoSApiModuleShutdown () == QOS_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "QOS module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef DCBX_WANTED
    if (RM_IS_APPL_REGISTERED (RM_DCBX_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_DCBX_APP_ID,
                              sizeof (RmShutModList));
        DcbxApiModuleShutDown ();
    }
#endif

    L2IwfMemDeInit ();
#ifdef VCM_WANTED
    if (RM_IS_APPL_REGISTERED (RM_VCM_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_VCM_APP_ID,
                              sizeof (RmShutModList));
        if (VcmModuleShutdown () == VCM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "VCM module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef CFA_WANTED
    if (RM_IS_APPL_REGISTERED (RM_CFA_APP_ID))
    {
        OSIX_BITLIST_SET_BIT (RmShutModList, RM_CFA_APP_ID,
                              sizeof (RmShutModList));
        if (CfaShutdown () == CFA_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "CFA module shutdown failed\n");
            return RM_FAILURE;
        }
    }
#endif
    if (L2IwfMemInit () == L2IWF_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "L2IWF start failed\n");
        return RM_FAILURE;
    }
    /* Modules start 
     * Following start of RM registered modules do the 
     * allocation / creation of the Timers, Memory pools etc.*/
#ifdef SNMP_3_WANTED
#ifdef L2RED_WANTED
    if (RM_IS_APPL_REGISTERED (RM_SNMP_APP_ID))
    {
        SnmpRedSetNodeStateToInit ();
    }
#endif
#endif
#ifdef CLI_WANTED
    if (RM_IS_APPL_REGISTERED (RM_CLI_APP_ID))
    {
        CliRmRoleToInit ();
    }
#endif
#ifdef VCM_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_VCM_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (VcmModuleStart () == VCM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "VCM module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef MBSM_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_MBSM_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (MbsmRedRmInitAndRegister () == MBSM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MBSM RED registration is failed\n");
            return RM_FAILURE;
        }
    }
#endif

#ifdef MSR_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_MSR_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (MsrRmInit () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MSR initialization failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef CFA_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_CFA_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (CfaModuleStart () == CFA_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "CFA module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef EOAM_WANTED

    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_EOAM_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (EoamModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "EOAM module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef PNAC_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_PNAC_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (PnacModuleStart () == PNAC_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PNAC module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef LA_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_LA_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (LaInit () == LA_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "LA module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_RSTPMSTP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (AstStartModule () == RST_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "STP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef VLAN_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_VLANGARP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (VlanStartModule () == VLAN_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "VLAN module start failed\n");
            return RM_FAILURE;
        }
#ifdef GARP_WANTED
        if (GarpStartModule () == GARP_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "GARP module start failed\n");
            return RM_FAILURE;
        }
#endif
    }
#endif
#ifdef ERPS_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ERPS_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (ErpsApiModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ERPS module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef PBBTE_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_PBBTE_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (PbbTeStartModule () == PBBTE_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PBB-TE module start failed\n");
            return RM_FAILURE;
        }
    }
#endif

#ifdef PBB_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_PBB_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (PbbStartPbbModule () == PBB_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PBB module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef MRP_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_MRP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (MrpApiStartModule () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MRP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ND6_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (Ip6TaskInit () == IP6_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "IP6 module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#endif

#ifdef ECFM_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ECFM_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (EcfmCcAndLbLtStartModule () == ECFM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ECFM module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef ELPS_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ELPS_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (ElpsApiModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ELPS module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef QOSX_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_QOS_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (QoSApiModuleStart () == QOS_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "QOS module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ACL_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (AclStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ACL module start failed\n");
            return RM_FAILURE;
        }
    }
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ISS_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (IssPIInitPortIsolationTable () == ISS_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PI module start failed\n");
            return RM_FAILURE;
        }
    }
#ifdef DCBX_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_DCBX_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (DcbxApiModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "DCBX module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef ELMI_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ELMI_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (ElmModuleInit () == ELM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ELMI module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_SNOOP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (SnoopStartModule () == SNOOP_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "SNOOP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef MPLS_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_MPLS_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (MplsStartModule () == MPLS_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "MPLS module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef LLDP_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_LLDP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (LldpApiModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "LLDP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef OSPF_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_OSPF_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (OspfInit () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "OSPF module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef OSPF3_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_OSPFV3_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (V3OspfApiModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "LLDP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef ARP_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_ARP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (ArpTaskInit () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "ARP module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#if defined(PIM_WANTED) || defined(PIMV6_WANTED)
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_PIM_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (PimModuleStart () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "PIM module start failed\n");
            return RM_FAILURE;
        }
    }
#endif
#ifdef BGP_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_BGP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (Bgp4ApiModuleEnable () == OSIX_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "BGP4 module start failed\n");
            return RM_FAILURE;
        }
    }
#endif

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    OSIX_BITLIST_IS_BIT_SET (RmShutModList, RM_NP_APP_ID,
                             sizeof (RmShutModList), bResult);
    if (bResult == OSIX_TRUE)
    {
        if (NpRedInit () == FNP_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "NP RED initialization failed\n");
            return RM_FAILURE;
        }
    }
#endif
#endif
    /* Make the Default VLAN interface admin up and configure the IP 
     * address from issnvramt.txt */
    CfaIfmMakeDefaultRouterVlanUp ();
#ifdef L2RED_WANTED
#ifdef MBSM_WANTED
    /* Update the preconfigured slots to Not Present state */
    MbsmRedPreConfSlotUpdateNotifyToCfa ();
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    /* Enable the STP module as we do as a default operation */
    if (AstModuleDefaultCxtStart () == RST_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "STP module enable failed\n");
        return RM_FAILURE;
    }
#endif
#endif

    RM_SHUT_START_STATE () = RM_SHUT_START_COMPLETED;

    return RM_SUCCESS;
}
