/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmsock.c,v 1.56 2016/03/18 13:13:06 siva Exp $
*
* Description: Redundancy manager socket interface functions.
*********************************************************************/
#include "rmincs.h"
#include "fssocket.h"
#include "fsssl.h"
#include <stdio.h>
#include <string.h>

#ifdef SSH_WANTED
extern INT4         SshArReadServerKey (VOID);
extern void         SshArKeyGen (tOsixTaskId TaskId);
#endif
extern UINT1        gu1FirmwareImageName[ISS_STATIC_MAX_NAME_LEN];
#define RM_FT_SERVER_TCP_PORT 7215
#define RM_MAX_CONN_REQ_PEND  MAX_NO_OF_FILES

#define RM_SYNC_TCP_SERVER_PORT 7217
#define RM_SYNC_TCP_MAX_CONN  5

CHR1               *gp1StaticFileName[MAX_NO_OF_FILES] = {
    "iss.conf", "SlotModule.conf", "issnvram.txt", "privil", "users",
    "system.size", "server_key_1", "server_key_2", "sslservcert", "ospf3at"
};

/******************************************************************************
 * Function           : RmTcpSrvSockInit 
 * Input(s)           : pi4SrvSockFd - Pointer to server socket fd
 * Output(s)          : TCP server socket descriptor
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create a TCP server scoket, set socket options 
 *                      for accepting the connections from peer.
 ******************************************************************************/
INT4
RmTcpSrvSockInit (INT4 *pi4SrvSockFd)
{
    INT4                i4SockFd = RM_INV_SOCK_FD;
    INT4                i4Flags = -1;
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4OptVal = TRUE;
    struct sockaddr_in  serv_addr;

    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockInit: ENTRY \r\n");
    /* Check if the TCP server already started */
    /* Open the tcp socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < 0)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSrvSockInit: TCP server socket "
                "creation failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    /* Get current socket flags */
    if (u4RetVal != RM_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
        {
            RM_TRC (RM_FAILURE_TRC, "RmTcpSrvSockInit: TCP server Fcntl "
                    "GET Failure !!!\n");
            RmCloseSocket (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
        {
            RM_TRC (RM_FAILURE_TRC, "RmTcpSrvSockInit: TCP server Fcntl "
                    "SET Failure !!!\n");
            RmCloseSocket (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        i4RetVal = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEADDR,
                               (INT4 *) &i4OptVal, sizeof (i4OptVal));
        if (i4RetVal < 0)
        {
            perror ("RmTcpSrvSockInit: Unable to set SO_REUSEADDR options "
                    "for socket\n");
            RmCloseSocket (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    /*  This should be enabled when socket layer supports.
       if (u4RetVal != RM_FAILURE)
       {   
       i4RetVal = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEPORT,
       (INT4 *) &i4OptVal, sizeof (i4OptVal));
       if (i4RetVal < 0)
       {
       perror ("RmTcpSrvSockInit: Unable to set SO_REUSEPORT options "
       "for socket\n");
       RmCloseSocket (i4SockFd);
       u4RetVal = RM_FAILURE;
       } 
       }
     */
    if (u4RetVal != RM_FAILURE)
    {
        MEMSET (&serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (RM_GET_SELF_NODE_ID ());
        serv_addr.sin_port = OSIX_HTONS (RM_SYNC_TCP_SERVER_PORT);

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr));
        if (i4RetVal < 0)
        {
            perror ("RmTcpSrvSockInit: Unable to bind with TCP server "
                    "address and port for socket\n");
            RmCloseSocket (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    if (u4RetVal != RM_FAILURE)
    {
        i4RetVal = listen (i4SockFd, RM_SYNC_TCP_MAX_CONN);
        if (i4RetVal < 0)
        {
            perror ("RmTcpSrvSockInit: Unable to listen for " "the socket\n");
            RmCloseSocket (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    if (u4RetVal == RM_FAILURE)
    {
        RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockInit: "
                "Server socket creation failed \r\n");
        return (RM_FAILURE);
    }
    *pi4SrvSockFd = i4SockFd;
    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, RmTcpSrvSockCallBk);
    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockInit: EXIT \r\n");
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmTcpAcceptNewConnection
 * Input(s)           : i4SrvSockFd - Server socket fd
 *                      pi4SockFd - Pointer to client socket fd
 *                      pu4PeerAddr - Peer IP address
 * Output(s)          : New connection identifier 
 * Returns            : None
 * Action             : This routine accepts the 
 *                      connection from the TCP peer. 
 ******************************************************************************/
INT4
RmTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd, UINT4 *pu4PeerAddr)
{
    INT4                i4CliSockFd = RM_INV_SOCK_FD;
    INT4                i4Flags = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    struct sockaddr_in  CliSockAddr;

    RM_TRC (RM_SOCK_TRC, "RmTcpAcceptNewConnection: ENTRY \r\n");
    if (i4SrvSockFd == RM_INV_SOCK_FD)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpAcceptNewConnection: "
                "Invalid server socket id \r\n");
        return RM_FAILURE;
    }
    MEMSET (&CliSockAddr, 0, u4CliLen);

    i4CliSockFd =
        accept (i4SrvSockFd, (struct sockaddr *) &CliSockAddr,
                (socklen_t *) & u4CliLen);
    if (i4CliSockFd < 0)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpAcceptNewConnection: Connection accept "
                "failed !!!\r\n");
        perror ("Accept error");
        return RM_FAILURE;
    }
    u4IpAddr = OSIX_NTOHL (CliSockAddr.sin_addr.s_addr);

    if ((i4Flags = fcntl (i4CliSockFd, F_GETFL, 0)) < 0)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpAcceptNewConnection: TCP server Fcntl "
                "GET Failure !!!\r\n");
        RmCloseSocket (i4CliSockFd);
        return RM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4CliSockFd, F_SETFL, i4Flags) < 0)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpAcceptNewConnection: TCP server Fcntl "
                "SET Failure !!!\r\n");
        RmCloseSocket (i4CliSockFd);
        return RM_FAILURE;
    }
    if (SelAddFd (i4CliSockFd, RmPktRcvd) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmTcpAcceptNewConnection : "
                "SelAddFd Failure while adding RM TCP socket\r\n");
        RmCloseSocket (i4CliSockFd);
        return RM_FAILURE;
    }
    SelAddFd (i4SrvSockFd, RmTcpSrvSockCallBk);
    *pi4SockFd = i4CliSockFd;
    *pu4PeerAddr = u4IpAddr;
    RM_TRC (RM_SOCK_TRC, "RmTcpAcceptNewConnection: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmTcpSockConnect 
 * Input(s)           : u4PeerAddr -> Peer address to be connected
 *                      pi4Connfd -> Pointer to the connection fd
 *                      pi4ConnState -> Pointer to the connection status
 * Output(s)          : None 
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine used for connecting the TCP peer 
 ******************************************************************************/
INT4
RmTcpSockConnect (UINT4 u4PeerAddr, INT4 *pi4Connfd, INT4 *pi4ConnState)
{
    INT4                i4SockFd = 0;
    INT4                i4RetVal = 0;
    INT4                i4Flags = 0;
    UINT4               u4RetVal = RM_SUCCESS;
    struct sockaddr_in  serv_addr;

    RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: ENTRY \r\n");
    if (*pi4Connfd == RM_INV_SOCK_FD)
    {
        RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: New socket connection \r\n");
        /* Open the tcp socket */
        i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (i4SockFd < 0)
        {
            RM_TRC (RM_FAILURE_TRC, "RmTcpSockConnect: TCP client socket "
                    "creation failure !!!\n");
            u4RetVal = RM_FAILURE;
        }
        if (IssGetColdStandbyFromNvRam () != ISS_COLDSTDBY_ENABLE)
        {
            /* Get current socket flags */
            if (u4RetVal != RM_FAILURE)
            {
                if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
                {
                    RM_TRC (RM_FAILURE_TRC,
                            "RmTcpSockConnect: TCP client Fcntl "
                            "GET Failure !!!\n");
                    RmCloseSocket (i4SockFd);
                    u4RetVal = RM_FAILURE;
                }
            }
            if (u4RetVal != RM_FAILURE)
            {
                /* Set the socket is non-blocking mode */
                i4Flags |= O_NONBLOCK;
                if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
                {
                    RM_TRC (RM_FAILURE_TRC,
                            "RmTcpSockConnect: TCP client Fcntl "
                            "SET Failure !!!\n");
                    RmCloseSocket (i4SockFd);
                    u4RetVal = RM_FAILURE;
                }
            }
        }
        if (u4RetVal != RM_FAILURE)
        {
            MEMSET (&serv_addr, 0, sizeof (serv_addr));
            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = OSIX_HTONL (RM_GET_SELF_NODE_ID ());
            serv_addr.sin_port = 0;

            i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                             sizeof (serv_addr));
            if (i4RetVal < 0)
            {
                perror ("RmTcpSockConnect: Unable to bind with TCP client "
                        "source address \r\n");
                RmCloseSocket (i4SockFd);
                u4RetVal = RM_FAILURE;
            }
        }
    }
    else
    {
        RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: Connection in progress "
                "case reconnect\r\n");
        i4SockFd = *pi4Connfd;
    }

    if (u4RetVal != RM_FAILURE)
    {
        MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (u4PeerAddr);
        serv_addr.sin_port = OSIX_HTONS (RM_SYNC_TCP_SERVER_PORT);

        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            if ((errno == EINPROGRESS) || (errno == EALREADY))
            {
                *pi4Connfd = i4SockFd;
                *pi4ConnState = RM_SOCK_CONNECT_IN_PROGRESS;
                SelAddWrFd (i4SockFd, RmTcpWriteCallBackFn);
                RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: "
                        "Connection in progress \r\n");
                return RM_SUCCESS;
            }
            RM_TRC (RM_CRITICAL_TRC,
                    "RmTcpSockConnect: TCP client socket connect failed\n");
            RmCloseSocket (i4SockFd);
            return RM_FAILURE;
        }

        *pi4Connfd = i4SockFd;
        *pi4ConnState = RM_SOCK_CONNECTED;
        RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: "
                "Connection is successful \r\n");
        if (IssGetColdStandbyFromNvRam () != ISS_COLDSTDBY_ENABLE)
        {
            if (SelAddFd (i4SockFd, RmPktRcvd) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC,
                        "RmTcpAcceptNewConnection :"
                        "SelAddFd Failure while adding TCP client socket\n");
            }
        }
        RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: EXIT \r\n");
        return RM_SUCCESS;
    }
    RM_TRC (RM_FAILURE_TRC, "RmTcpSockConnect: FAILED \r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmTcpSockSend 
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : i4WrBytes - No. of bytes written
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send RM protocol packets.
 ******************************************************************************/
UINT4
RmTcpSockSend (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2PktLen, INT4 *pi4WrBytes)
{
    INT4                i4WrBytes = 0;

    RM_TRC (RM_SOCK_TRC, "RmTcpSockSend: ENTRY \r\n");
    /* Send the file information to request the file */
    i4WrBytes = send (i4ConnFd, pu1Data, u2PktLen, MSG_NOSIGNAL);
    if (i4WrBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases, 
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            *pi4WrBytes = 0;
            return RM_SUCCESS;
        }
        *pi4WrBytes = 0;
        if (errno == ECONNRESET)
        {
            perror ("\n RmTcpSockSend: send failed may be due to peer down "
                    "detection/corrupted packet on socket at the peer\n");
        }
        else
        {
            perror ("\n RmTcpSockSend: RM sync message send failed \n");
        }
        return RM_FAILURE;
    }
    *pi4WrBytes = i4WrBytes;
    RM_TRC (RM_SOCK_TRC, "RmTcpSockSend: Successful EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmTcpSockRcv 
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : pi4RdBytes - No. of bytes received
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send RM protocol packets.
 ******************************************************************************/
UINT4
RmTcpSockRcv (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2MsgLen, INT4 *pi4RdBytes)
{
    INT4                i4RdBytes = 0;

    RM_TRC (RM_SOCK_TRC, "RmTcpSockRcv: ENTRY \r\n");
    i4RdBytes = recv (i4ConnFd, pu1Data, u2MsgLen, MSG_NOSIGNAL);

    if (i4RdBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases, 
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            *pi4RdBytes = i4RdBytes;
            return RM_SUCCESS;
        }
        perror ("\n RmTcpSockRcv: RM sync message receive failed \n");
        return RM_FAILURE;
    }

    RM_TRC (RM_SOCK_TRC, "RmTcpSockRcv: Successful EXIT \r\n");
    *pi4RdBytes = i4RdBytes;
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmFtServSockInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Socket descriptor
 * Action             : Routine to open a server socket for file transfer 
 *                      operation.
 ******************************************************************************/
INT4
RmFtServSockInit (VOID)
{
    INT4                i4SockFd = -1;
    INT4                i4RetVal;
    struct sockaddr_in  serv_addr;
    UINT4               u4SrvTcpPort;
    INT4                i4OptVal = TRUE;

    RM_TRC (RM_SOCK_TRC, "RmFtServSockInit: ENTRY \r\n");
    if ((i4SockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("FT Server: Unable to open stream socket\n");
        return (i4SockFd);
    }

    i4RetVal = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEADDR,
                           (INT4 *) &i4OptVal, sizeof (i4OptVal));

    if (i4RetVal < 0)
    {
        perror ("FT Server: Unable to set SO_REUSEADDR options "
                "for socket\n");
        RmCloseSocket (i4SockFd);
        return 0;
    }

    MEMSET (&serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = OSIX_HTONL (RM_GET_SELF_NODE_ID ());
    serv_addr.sin_port = OSIX_HTONS (RM_FT_SERVER_TCP_PORT);

    i4RetVal =
        bind (i4SockFd, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    if (i4RetVal < 0)
    {
        u4SrvTcpPort = RM_FT_SERVER_TCP_PORT + 1;
        serv_addr.sin_port = OSIX_HTONS (u4SrvTcpPort);

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr));
        if (i4RetVal < 0)
        {
            perror ("FT Server: Unable to bind Address\n");
            RmCloseSocket (i4SockFd);
        }
    }

    listen (i4SockFd, RM_MAX_CONN_REQ_PEND);
    RM_TRC (RM_SOCK_TRC, "RmFtServSockInit: EXIT \r\n");
    return (i4SockFd);
}

/******************************************************************************
 * Function           : RmAcceptIncomingConn
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Client Socket descriptor
 * Action             : Routine to accept incoming connection req from various
 *                      clients. 
 ******************************************************************************/
INT4
RmAcceptIncomingConn (INT4 i4FtSrvSockFd)
{
    INT4                i4CliSockFd = -1;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    struct sockaddr_in  CliSockAddr;

    MEMSET (&CliSockAddr, 0, u4CliLen);
    i4CliSockFd = accept (i4FtSrvSockFd, (struct sockaddr *) &CliSockAddr,
                          (socklen_t *) & u4CliLen);
    if (i4CliSockFd < 0)
    {
        perror ("Accept error");
    }
    return (i4CliSockFd);
}

/******************************************************************************
 * Function           : RmSendFile
 * Input(s)           : i4CliSockFd - SockFd to which the file has to be sent.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the file to the specified SockFd. 
 ******************************************************************************/
UINT4
RmSendFile (INT4 i4CliSockFd)
{
    tFileInfo           FileInfo;
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4FileFd = -1;
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
#ifdef L2RED_TEST_WANTED
    UINT1              *pu1SubStr = NULL;
#endif
    INT4                i4ReadSize;
    struct stat         fStatBuf;
#ifdef SSH_WANTED
    tOsixTaskId         u4SelfTaskId;
#endif
    /* Rcv the FileInfo request sent by client */
    if (ReadNbytes (i4CliSockFd, (CHR1 *) & FileInfo, sizeof (tFileInfo)) !=
        sizeof (tFileInfo))
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmSendFile - read fileinfo from sock failed..\n");
        RmCloseSocket (i4CliSockFd);
        return RM_FAILURE;
    }
    if (STRNCMP (FileInfo.c1FileName, "fsir.log", 8) == 0)
    {
        /* For Syslog file: file transfer happens from Standby to active
         */
        RmRecvFileInActive (i4CliSockFd, &FileInfo);
        return RM_SUCCESS;

    }
#ifdef L2RED_TEST_WANTED
    pu1SubStr = (UINT1 *) STRSTR (FileInfo.c1FileName, RM_DYN_AUDIT_FN_STDBY);
    if (pu1SubStr != NULL)
    {
        /* For dynamic sync audit framework file: file transfer happens from
           standby to active */
        RmRecvFileInActive (i4CliSockFd, &FileInfo);
        return RM_SUCCESS;

    }
#endif
    /* For all other files, transfer happens from active to standby */

    RM_LOCK ();
    /* Get the total size of the file in bytes */
    if (stat (FileInfo.c1FileName, &fStatBuf) < 0)
    {
#ifdef SSH_WANTED
        /* if the server key files are not present then server key files needs to 
         *  be generated by calling SshArKeyGen and then has to be sent to stand by node*/
        if ((STRCMP
             (FileInfo.c1FileName, gp1StaticFileName[FILE_SERVER_KEY_ID1]) == 0)
            ||
            (STRCMP
             (FileInfo.c1FileName,
              gp1StaticFileName[FILE_SERVER_KEY_ID2]) == 0))
        {
            OsixTskIdSelf (&u4SelfTaskId);
            SshArKeyGen (u4SelfTaskId);
            if (stat (FileInfo.c1FileName, &fStatBuf) < 0)
            {
                RM_TRC1 (RM_FAILURE_TRC,
                         "stat() call failed while getting the status of file for %s\n",
                         FileInfo.c1FileName);
            }
        }
        else
        {
            RM_TRC1 (RM_FAILURE_TRC,
                     "stat() call failed while getting the size of file for %s\n",
                     FileInfo.c1FileName);
            RmCloseSocket (i4CliSockFd);
            u4RetVal = RM_FAILURE;
        }
#else
        RM_TRC1 (RM_FAILURE_TRC,
                 "stat() call failed while getting the size of file for %s\n",
                 FileInfo.c1FileName);
        RmCloseSocket (i4CliSockFd);
        u4RetVal = RM_FAILURE;
#endif
    }
    RM_UNLOCK ();


    if (u4RetVal != RM_FAILURE)
    {
        /* Open the file requested by client */
        i4FileFd = FileOpen ((UINT1 *) FileInfo.c1FileName, OSIX_FILE_RO);
        if (i4FileFd == -1)
        {
            perror("Error in opening file\n");
            RM_TRC1 (RM_FT_TRC, "File-Transfer: FileName =%s,"
                     "RM[ACTIVE]->RM[STANDBY], File not found!!!\n",
                     FileInfo.c1FileName);
            RmCloseSocket (i4CliSockFd);
            u4RetVal = RM_FAILURE;
        }
        else
        {
            RM_TRC2 (RM_FT_TRC, "File-Transfer: FileName =%s, FileSize =%d byte(s), "
                     "RM[ACTIVE]->RM[STANDBY], Initiated\n",
                     FileInfo.c1FileName, fStatBuf.st_size);
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Read from file and write to socket */
        while ((i4ReadSize =
                (INT4) FileRead (i4FileFd, (&ac1Buf[0]), RM_MAX_BUF_SIZE)) >= 0)
        {
            if (i4ReadSize == 0)
            {
                break;
            }
            WriteNbytes (i4CliSockFd, (&ac1Buf[0]), i4ReadSize);
        }

        FileClose (i4FileFd);
        RmCloseSocket (i4CliSockFd);

        if (i4ReadSize == -1)
        {
            RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
                     "FileSize =%d byte(s), RM[ACTIVE]->RM[STANDBY], Failed!!!\n",
                     FileInfo.c1FileName, fStatBuf.st_size);
            u4RetVal = RM_FAILURE;
        }
        else
        {
            RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
                     "FileSize =%d byte(s), RM[ACTIVE]->RM[STANDBY], Success!!!\n",
                     FileInfo.c1FileName, fStatBuf.st_size);
        }

    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmRcvFile
 * Input(s)           : pc1FileName - Name of the file to be rcvd.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to receive the file (read from socket and store
 *                      in file). If the file is present, it is overwritten. If
 *                      the file is not present, file is created.
 ******************************************************************************/
UINT4
RmRcvFile (const CHR1 * pc1FileName)
{
    struct sockaddr_in  serv_addr;
    struct stat         fStatBuf;
    INT4                i4SockFd = -1;
    tFileInfo           FileInfo;
    int                 fileFd;
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
    INT4                i4ReadSize;
    UINT4               u4SrvTcpPort;
    INT4                i4Len = sizeof (tFileInfo);

    RM_TRC (RM_SOCK_TRC, "Rm File Transfer Client receiving file...\n");

    /* connect to server for receiving file */
    MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = OSIX_HTONL (RM_GET_ACTIVE_NODE_ID ());
    serv_addr.sin_port = OSIX_HTONS (RM_FT_SERVER_TCP_PORT);

    if ((i4SockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("Client: Unable to open stream socket\n");
        return (RM_FAILURE);
    }

    if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                 sizeof (serv_addr)) < 0)
    {
        perror ("Unable to connect to port 7215\n");
        u4SrvTcpPort = RM_FT_SERVER_TCP_PORT + 1;
        serv_addr.sin_port = OSIX_HTONS (u4SrvTcpPort);
        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            perror ("Unable to connect to port 7216\n");
            RmCloseSocket (i4SockFd);
            return RM_FAILURE;
        }
    }

    /* Copy the file name in FileInfo and send the request to server */
    MEMSET (&FileInfo, 0, sizeof (tFileInfo));
    STRNCPY (FileInfo.c1FileName, pc1FileName, STRLEN (pc1FileName));
    FileInfo.c1FileName[STRLEN (pc1FileName)] = '\0';

    fileFd =
        FileOpen ((UINT1 *) FileInfo.c1FileName,
                  OSIX_FILE_CR | OSIX_FILE_RW | OSIX_FILE_TR);

    if (fileFd == -1)
    {
        perror ("Can't create file ...");
        RmCloseSocket (i4SockFd);
        return (RM_FAILURE);
    }
    /* Send the file information to request the file */
    if (write (i4SockFd, &FileInfo, sizeof (tFileInfo)) != i4Len)
    {
        perror ("File Info write error...");
        RmCloseSocket (fileFd);
        RmCloseSocket (i4SockFd);
        return (RM_FAILURE);
    }
    RM_TRC1 (RM_FT_TRC, "File-Transfer: FileName =%s, FileSize =0 byte, "
             "RM[ACTIVE]->RM[STANDBY], Initiated\n", FileInfo.c1FileName);
    /* Read from socket and write to file */
    while ((i4ReadSize =
            ReadNbytes (i4SockFd, &ac1Buf[0], RM_MAX_BUF_SIZE)) >= 0)
    {
        if (i4ReadSize == 0)
        {
            WriteNbytes (fileFd, &ac1Buf[0], i4ReadSize);
            break;
        }

        FileWrite (fileFd, &ac1Buf[0], (UINT4) i4ReadSize);
    }
    FileClose (fileFd);
    RmCloseSocket (i4SockFd);

    if (i4ReadSize == -1)
    {
        RM_TRC1 (RM_SOCK_CRIT_TRC | RM_FAILURE, "File-Transfer: FileName =%s, "
                 "FileSize =0 byte, RM[ACTIVE]->RM[STANDBY], Failed!!!\n",
                 FileInfo.c1FileName);
        return (RM_FAILURE);
    }
    else
    {
        /* Get the total size of the file in bytes */
        if (stat (FileInfo.c1FileName, &fStatBuf) < 0)
        {
            RM_TRC1 (RM_FAILURE_TRC,
                     "stat() call failed while getting the size of file for %s\n",
                     FileInfo.c1FileName);
            return (RM_FAILURE);
        }
        if (fStatBuf.st_size > 0)
        {
#ifdef SSH_WANTED
            if ((STRCMP
                 (FileInfo.c1FileName,
                  gp1StaticFileName[FILE_SERVER_KEY_ID1]) == 0)
                ||
                (STRCMP
                 (FileInfo.c1FileName,
                  gp1StaticFileName[FILE_SERVER_KEY_ID2]) == 0))
            {
                SshArReadServerKey ();
            }
#endif
#ifdef SSL_WANTED
		    if (STRCMP (FileInfo.c1FileName, gp1StaticFileName[FILE_SSL_SERV_CERT-1]) == 0)
		    {
		    	SSLArRestoreCert();    
	        }
#endif
#ifdef OSPF3_WANTED
            if (STRCMP (FileInfo.c1FileName, gp1StaticFileName[FILE_V3_CRYPT_SEQNUM-1]) == 0)
            {
                O3RedFileTransRestoreComplete();
            }
#endif
                RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
                         "FileSize =%d byte(s), RM[ACTIVE]->RM[STANDBY], Success!!!\n",
                         FileInfo.c1FileName, fStatBuf.st_size);
        }
        return (RM_SUCCESS);
    }
}

/******************************************************************************
 * Function           : ReadNbytes
 * Input(s)           : fd: file desc (or) sock desc.
 *                      pc1Ptr: ptr to which the data has to be read
 *                      n: length of bytes to be read
 * Output(s)          : None.
 * Returns            : No. of bytes read.
 * Action             : Read N bytes of data from the specified descriptor. 
 ******************************************************************************/
size_t
ReadNbytes (int fd, CHR1 * pc1Ptr, size_t n)
{
    ssize_t             nleft;
    ssize_t             nread;
    CHR1               *ptr;

    ptr = pc1Ptr;
    nleft = n;

    while (nleft > 0)
    {
        if ((nread = read (fd, ptr, nleft)) < 0)
        {
            if (errno == EINTR)
            {
                perror ("ReadNbytes - EINTR\n");
                nread = 0;
            }
            else
            {
                perror ("ReadNbytes - other than EINTR\n");
                return (-1);
            }
        }
        else if (nread == 0)
        {
            break;
        }

        nleft -= nread;
        ptr += nread;
    }
    return (n - nleft);
}

/******************************************************************************
 * Function           : WriteNbytes
 * Input(s)           : fd: file desc (or) sock desc.
 *                      pc1Ptr: ptr from which the data has to be taken 
 *                      for writing
 *                      n: length of bytes to be written
 * Output(s)          : None.
 * Returns            : No. of bytes written.
 * Action             : Write N bytes of data to the specified descriptor. 
 ******************************************************************************/
size_t
WriteNbytes (int fd, CHR1 * pc1Ptr, size_t n)
{
    ssize_t             nleft;
    ssize_t             nwritten;
    CHR1               *ptr;

    ptr = pc1Ptr;
    nleft = n;

    while (nleft > 0)
    {
        if ((nwritten = write (fd, ptr, nleft)) < 0)
        {
            if (errno == EINTR)
                nwritten = 0;
            else
                return (-1);
        }

        nleft -= nwritten;
        ptr += nwritten;
    }
    return (n);
}

/******************************************************************************
 * Function           : RmCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 ******************************************************************************/
INT4
RmCloseSocket (INT4 i4SockFd)
{
    INT4                i4RetVal = RM_SUCCESS;

    if (i4SockFd == RM_INV_SOCK_FD)
    {
        return (i4RetVal);
    }
    i4RetVal = close (i4SockFd);
    if (i4RetVal < 0)
    {
        RM_TRC (RM_FAILURE_TRC, "!!! socket close Failure !!!\n");
    }
    return (i4RetVal);
}

#if defined (BSDCOMP_SLI_WANTED) && (L2RED_WANTED)
/******************************************************************************
 * Function           : RmGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : RM_SUCCESS or RM_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 ******************************************************************************/
UINT4
RmGetIfIpInfo (UINT1 *pu1IfName, tNodeInfo * pNodeInfo)
{
    INT4                i4SockFd = -1;
    struct ifreq        ifreq;
    struct sockaddr_in *saddr;
    UINT4               u4RetVal = RM_SUCCESS;

    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd > 0)
    {
        STRNCPY (ifreq.ifr_name, pu1IfName, IFNAMSIZ);
        ifreq.ifr_name[IFNAMSIZ - 1] = '\0';

        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) < 0)
        {
            perror ("Error in getting RM interface\n");
            u4RetVal = RM_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            pNodeInfo->u4IpAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }

        if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) < 0)
        {
            u4RetVal = RM_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
            pNodeInfo->u4NetMask = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }
    }
    else
    {
        u4RetVal = RM_FAILURE;
    }

    RmCloseSocket (i4SockFd);
    return (u4RetVal);
}
#endif
/******************************************************************************
 * Function           : RmColdFileSendTask
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task to receive the requested file.
 ******************************************************************************/
UINT4
RmSendFiletoAttachedPeer (INT4 i4TskIndex)
{
    struct sockaddr_in  serv_addr;
    INT4                i4SockFd = -1;
    tFileInfo           FileInfo;
    INT4                i4FileIndex;
    INT4                i4FileFd = -1;
    INT4                i4ReadSize;
    UINT4               u4SrvTcpPort;
    UINT4               u4RetVal = 0;
    INT4                i4Len = sizeof (tFileInfo);
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
    UINT4               u4SwitchId = 0;

    RM_TRC (RM_SOCK_TRC, "Sending File to Peer...\n");
    /* To get DestIp and file name from i4TskIndex */
    u4SwitchId = RmGetSwitchIdFromTskId ((UINT4) i4TskIndex);
    i4FileIndex = RmGetFileIndexFromTskId ((UINT4) i4TskIndex);

    /* connect to server for receiving file */
    MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    /*serv_addr.sin_addr.s_addr = OSIX_HTONL (RmGetStackIpForSlot (u4SwitchId) */
    serv_addr.sin_addr.s_addr = RmGetStackIpForSlot (u4SwitchId);
    serv_addr.sin_port = OSIX_HTONS (RM_FT_SERVER_TCP_PORT);

    /* Open a socket and connect to PEER */
    if ((i4SockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("Client: Unable to open stream socket\n");
        return (RM_FAILURE);
    }
    if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                 sizeof (serv_addr)) < 0)
    {
        perror ("Unable to connect to port 6666\n");
        u4SrvTcpPort = RM_FT_SERVER_TCP_PORT + 1;
        serv_addr.sin_port = OSIX_HTONS (u4SrvTcpPort);
        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            perror ("Unable to connect to port 6667\n");
            close (i4SockFd);
            return RM_FAILURE;
        }
    }

    /* Copy the file name in FileInfo  */
    MEMSET (&FileInfo, 0, sizeof (tFileInfo));
    SPRINTF (FileInfo.c1FileName, "%s%s", FLASH,
             gp1StaticFileName[i4FileIndex]);

    /* Send the file information to peer */
    if (write (i4SockFd, &FileInfo, sizeof (tFileInfo)) != i4Len)
    {
        RM_TRC1 (RM_FT_TRC, "RmSendFiletoAttachedPeer - err to write : %ld\n",
                 i4SockFd);
        perror ("File Info write error...");
        close (i4SockFd);
        return (RM_FAILURE);
    }

    if (u4RetVal != RM_FAILURE)
    {
        i4FileFd = FileOpen ((UINT1 *) FileInfo.c1FileName, OSIX_FILE_RO);
        if (i4FileFd == -1)
        {
            perror ("Error in opening file\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Read from file and write to socket */
        while ((i4ReadSize =
                ReadNbytes (i4FileFd, (&ac1Buf[0]), RM_MAX_BUF_SIZE)) >= 0)
        {
            if (i4ReadSize == 0)
            {
                break;
            }
            WriteNbytes (i4SockFd, (&ac1Buf[0]), i4ReadSize);
        }
        close (i4FileFd);
        close (i4SockFd);
        OsixTskDelay (700);

        if (i4ReadSize == -1)
        {
            u4RetVal = RM_FAILURE;
        }

    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmSendFirmwareImage
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task to receive the requested file.
 ******************************************************************************/
UINT4
RmSendFirmwareImage (INT4 i4TskIndex)
{
    struct sockaddr_in  serv_addr;
    INT4                i4SockFd = -1;
    tFileInfo           FileInfo;
    INT4                i4FileFd = -1;
    INT4                i4ReadSize;
    UINT4               u4SrvTcpPort;
    UINT4               u4RetVal = 0;
    INT4                i4Len = sizeof (tFileInfo);
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
    UINT4               u4SwitchId = 0;

    RM_TRC (RM_SOCK_TRC, "Sending Image to Peer...\n");

    /* To get DestIp and file name from i4TskIndex */
    u4SwitchId = RmGetSwitchIdFromTskId ((UINT4) i4TskIndex);

    /* connect to server for receiving file */
    MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr =
        OSIX_HTONL (RM_GET_SSN_INFO (u4SwitchId).u4PeerAddr);
    serv_addr.sin_port = OSIX_HTONS (RM_FT_SERVER_TCP_PORT);

    /* Open a socket and connect to PEER */
    if ((i4SockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("Client: Unable to open stream socket\n");
        return (RM_FAILURE);
    }
    if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                 sizeof (serv_addr)) < 0)
    {
        perror ("Unable to connect to port 6666\n");
        u4SrvTcpPort = RM_FT_SERVER_TCP_PORT + 1;
        serv_addr.sin_port = OSIX_HTONS (u4SrvTcpPort);
        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            perror ("Unable to connect to port 6667\n");
            close (i4SockFd);
            return RM_FAILURE;
        }
    }

    /* Copy the file name in FileInfo  */
    MEMSET (&FileInfo, 0, sizeof (tFileInfo));
/*    u4Len = ((STRLEN (gu1FirmwareImageName) >= RM_MAX_FILE_NAME_LEN)?RM_MAX_FILE_NAME_LEN:STRLEN (gu1FirmwareImageName));*/
    if (STRLEN (gu1FirmwareImageName) >= RM_MAX_FILE_NAME_LEN)
    {
        RM_TRC (RM_FT_TRC, "RmRcvFile: File size is big");
        perror ("File Info write error...");
        close (i4SockFd);
        return (RM_FAILURE);
    }
    MEMCPY (FileInfo.c1FileName, gu1FirmwareImageName,
            (sizeof (FileInfo.c1FileName) - 1));
    FileInfo.c1FileName[STRLEN (gu1FirmwareImageName)] = '\0';

    /* Send the file information to peer */
    if (write (i4SockFd, &FileInfo, sizeof (tFileInfo)) != i4Len)
    {
        RM_TRC1 (RM_FT_TRC, "RmRcvFile: err to write : %ld\n", i4SockFd);
        perror ("File Info write error...");
        close (i4SockFd);
        return (RM_FAILURE);
    }

    if (u4RetVal != RM_FAILURE)
    {
        i4FileFd = FileOpen ((UINT1 *) FileInfo.c1FileName, OSIX_FILE_RO);
        if (i4FileFd == -1)
        {
            perror ("Error in opening file\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    if (STRCMP (FileInfo.c1FileName, "/mnt/issnvram.txt") == 0)
    {
        system ("cat /mnt/issnvram.txt");
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Read from file and write to socket */
        while ((i4ReadSize =
                ReadNbytes (i4FileFd, (&ac1Buf[0]), RM_MAX_BUF_SIZE)) >= 0)
        {
            if (i4ReadSize == 0)
            {
                break;
            }
            WriteNbytes (i4SockFd, (&ac1Buf[0]), i4ReadSize);
        }
        close (i4FileFd);
        close (i4SockFd);
        OsixTskDelay (700);

        if (i4ReadSize == -1)
        {
            u4RetVal = RM_FAILURE;
        }
        else
        {
            PRINTF ("RmSendFile success !!! for %s\n", FileInfo.c1FileName);
        }
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmGetFileFromPeer
 * Input(s)           : i4FtSrvSockFd
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task to receive the requested file.
 ******************************************************************************/
INT4
RmGetFileFromPeer (INT4 i4FtSrvSockFd)
{
    tFileInfo           FileInfo;
    INT4                i4ReadSize = 0;
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
    INT4                i4FileFd;
    UINT4               u4RetVal = RM_FAILURE;
    UINT4               u4ActiveIP = 0;

    if (ReadNbytes (i4FtSrvSockFd, (CHR1 *) & FileInfo, sizeof (tFileInfo)) !=
        sizeof (tFileInfo))
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmGetFileFromPeer - read fileinfo from sock failed..\n");
        close (i4FtSrvSockFd);
        return RM_FAILURE;
    }

    i4FileFd = FileOpen ((UINT1 *) FileInfo.c1FileName,
                         OSIX_FILE_CR | OSIX_FILE_RW | OSIX_FILE_TR);

    if (i4FileFd == -1)
    {
        RM_TRC1 (RM_FT_TRC, "RmGetFileFromPeer-err to open : %s\n",
                 FileInfo.c1FileName);
        perror ("File open error...");
        close (i4FtSrvSockFd);
        return (RM_FAILURE);
    }

    while ((i4ReadSize =
            ReadNbytes (i4FtSrvSockFd, &ac1Buf[0], RM_MAX_BUF_SIZE)) >= 0)
    {
        if (i4ReadSize == 0)
        {
            WriteNbytes (i4FileFd, &ac1Buf[0], i4ReadSize);
            break;
        }

        WriteNbytes (i4FileFd, &ac1Buf[0], i4ReadSize);
    }
    close (i4FileFd);
    close (i4FtSrvSockFd);

    if (i4ReadSize == -1)
    {
        return (RM_FAILURE);
    }
    else
    {
        if ((STRCMP (FileInfo.c1FileName, ISS_FIRMWARE_NORMAL) == 0) ||
            (STRCMP (FileInfo.c1FileName, ISS_FIRMWARE_FALLBACK) == 0))
        {
            u4RetVal = (UINT4) IssCustReadFile ((UINT1 *) FileInfo.c1FileName);
            if (u4RetVal == ISS_SUCCESS)
            {
                StackSendSlaveStatustoMaster (u4ActiveIP, STACK_UPGRADE_SUCC);
                RM_TRC (RM_FT_TRC, "Firmware upgradation successful \r\n");
                return (RM_SUCCESS);
            }
            else if (u4RetVal == ISS_FAILURE)
            {
                StackSendSlaveStatustoMaster (u4ActiveIP, STACK_UPGRADE_FAIL);
                RM_TRC (RM_FT_TRC, " Firmware upgradation failed ! \r\n");
                return (RM_FAILURE);
            }
        }
        else
        {
            RM_TRC1 (RM_FT_TRC, "writing into %s completed \r\n",
                     FileInfo.c1FileName);
            return (RM_SUCCESS);
        }
    }
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmRecvFileInActive
 * Input(s)           : i4CliSockFd - SockFd to which the file has to be sent.
 *                      pFileInfo - File to be written
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to recv and write the file to local flash          
 ******************************************************************************/
UINT4
RmRecvFileInActive (INT4 i4CliSockFd, tFileInfo * pFileInfo)
{
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4FileFd = -1;
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
#ifdef L2RED_TEST_WANTED
    UINT1              *pu1SubStr = NULL;
    UINT1              *pu1StripStr = NULL;
#endif
    INT4                i4ReadSize;
    struct stat         fStatBuf;

    MEMSET (&fStatBuf, 0, sizeof (struct stat));

    if (STRNCMP (pFileInfo->c1FileName, "fsir.log", 8) == 0)
    {
        STRCPY (pFileInfo->c1FileName, "fsir.peer");
    }
#ifdef L2RED_TEST_WANTED
    pu1SubStr = (UINT1 *) STRSTR (pFileInfo->c1FileName, RM_DYN_AUDIT_FN_STDBY);
    if (pu1SubStr != NULL)
    {
        pu1StripStr = (UINT1 *) (pFileInfo->c1FileName +
                                 STRLEN (RM_DYN_AUDIT_TEMP_STR));
        STRCPY (pFileInfo->c1FileName, pu1StripStr);
    }
#endif
    else
    {
        RmCloseSocket (i4CliSockFd);
        return RM_FAILURE;
    }

    RM_TRC2 (RM_FT_TRC, "File-Transfer: FileName =%s, FileSize =%d byte(s), "
             "RM[STANDBY]->RM[ACTIVE], Initiated\n",
             pFileInfo->c1FileName, fStatBuf.st_size);

    if (u4RetVal != RM_FAILURE)
    {
        /* Open the file requested by client */
        i4FileFd = FileOpen ((UINT1 *) pFileInfo->c1FileName,
                             (OSIX_FILE_CR | OSIX_FILE_RW | OSIX_FILE_TR));
        if (i4FileFd == -1)
        {
            perror ("Error in opening file\n");
            RmCloseSocket (i4CliSockFd);
            u4RetVal = RM_FAILURE;
            return RM_FAILURE;
        }
    }
    if (u4RetVal != RM_FAILURE)
    {
        /* Read from socket and write to file */
        while ((i4ReadSize =
                ReadNbytes (i4CliSockFd, &ac1Buf[0], RM_MAX_BUF_SIZE)) >= 0)
        {
            if (i4ReadSize == 0)
            {
                WriteNbytes (i4FileFd, &ac1Buf[0], i4ReadSize);
                break;
            }

            FileWrite (i4FileFd, &ac1Buf[0], (UINT4) i4ReadSize);
        }

    }
    FileClose (i4FileFd);
    RmCloseSocket (i4CliSockFd);
    if (stat (pFileInfo->c1FileName, &fStatBuf) < 0)
    {
        u4RetVal = RM_FAILURE;
    }

    RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
             "FileSize =%d byte(s), RM[STANDBY]->RM[ACTIVE], Success!!!\n",
             pFileInfo->c1FileName, fStatBuf.st_size);
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmStandbySendFile
 * Input(s)           : pc1FileName - file to be sent from stdby to active.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send file from stdby to active                  
 ******************************************************************************/
UINT4
RmStandbySendFile (const CHR1 * pc1FileName)
{
    struct sockaddr_in  serv_addr;
    INT4                i4SockFd = -1;
    tFileInfo           FileInfo;
    UINT4               u4SrvTcpPort;
    INT4                i4Len = sizeof (tFileInfo);
    UINT2               u2Len = 0;

    RM_TRC (RM_SOCK_TRC, "Rm File Transfer Client receiving file...\n");

    /* connect to server for receiving file */
    MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = OSIX_HTONL (RM_GET_ACTIVE_NODE_ID ());
    serv_addr.sin_port = OSIX_HTONS (RM_FT_SERVER_TCP_PORT);

    if ((i4SockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("Client: Unable to open stream socket\n");
        return (RM_FAILURE);
    }

    if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                 sizeof (serv_addr)) < 0)
    {
        perror ("Unable to connect to port 7215\n");
        u4SrvTcpPort = RM_FT_SERVER_TCP_PORT + 1;
        serv_addr.sin_port = OSIX_HTONS (u4SrvTcpPort);
        if (connect (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr)) < 0)
        {
            perror ("Unable to connect to port 7216\n");
            RmCloseSocket (i4SockFd);
            return RM_FAILURE;
        }
    }
    /* Copy the file name in FileInfo and send the request to server */
    MEMSET (&FileInfo, 0, sizeof (tFileInfo));
    u2Len =
        (UINT2) (STRLEN (pc1FileName) <
                 sizeof (FileInfo.
                         c1FileName) ? STRLEN (pc1FileName) : sizeof (FileInfo.
                                                                      c1FileName)
                 - 1);
    STRNCPY (FileInfo.c1FileName, pc1FileName, u2Len);
    FileInfo.c1FileName[u2Len] = '\0';

    /* Send the file information to request the file */
    if (write (i4SockFd, &FileInfo, sizeof (tFileInfo)) != i4Len)
    {
        perror ("File Info write error...");
        RmCloseSocket (i4SockFd);
        return (RM_FAILURE);
    }
    RmSendFileToActive (&FileInfo, i4SockFd);
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmSendFileToActive
 * Input(s)           : i4CliSockFd - SockFd to which the file has to be sent.
 *                      pFileInfo - File to be written
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to read and write the file to socket
 ******************************************************************************/
UINT4
RmSendFileToActive (tFileInfo * pFileName, INT4 i4CliSockFd)
{
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4FileFd = -1;
    CHR1                ac1Buf[RM_MAX_BUF_SIZE];
    INT4                i4ReadSize;
    struct stat         fStatBuf;
    /* Get the total size of the file in bytes */
    if (stat (pFileName->c1FileName, &fStatBuf) < 0)
    {
        u4RetVal = RM_FAILURE;
    }
    RM_TRC2 (RM_FT_TRC, "File-Transfer: FileName =%s, FileSize =%d byte(s), "
             "RM[STANDBY]->RM[ACTIVE], Initiated\n",
             pFileName->c1FileName, fStatBuf.st_size);
    if (u4RetVal != RM_FAILURE)
    {
        /* Open the file requested by client */
        i4FileFd = FileOpen ((UINT1 *) pFileName->c1FileName, OSIX_FILE_RO);
        if (i4FileFd == -1)
        {
            perror ("Error in opening file\n");
            RmCloseSocket (i4CliSockFd);
            u4RetVal = RM_FAILURE;
            return RM_FAILURE;
        }
    }
    if (u4RetVal != RM_FAILURE)
    {
        /* Read from file and write to socket */
        while ((i4ReadSize =
                (INT4) FileRead (i4FileFd, (&ac1Buf[0]), RM_MAX_BUF_SIZE)) >= 0)
        {
            if (i4ReadSize == 0)
            {
                break;
            }
            WriteNbytes (i4CliSockFd, (&ac1Buf[0]), i4ReadSize);
        }

        FileClose (i4FileFd);

        if (i4ReadSize == -1)
        {
            RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
                     "FileSize =%d byte(s), RM[STANDBY]->RM[ACTIVE], Failed!!!\n",
                     pFileName->c1FileName, fStatBuf.st_size);
            u4RetVal = RM_FAILURE;
        }
        else
        {
            RM_TRC2 (RM_SOCK_CRIT_TRC, "File-Transfer: FileName =%s, "
                     "FileSize =%d byte(s), RM[STANDBY]->RM[ACTIVE], Success!!!\n",
                     pFileName->c1FileName, fStatBuf.st_size);
        }
    }
    RmCloseSocket (i4CliSockFd);
    return (u4RetVal);
}
