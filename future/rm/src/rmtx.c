/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmtx.c,v 1.18 2016/06/16 12:21:56 siva Exp $
*
* Description: RM sync messages transmission related functions.
***********************************************************************/
#include "rmincs.h"
#include "fssocket.h"

/******************************************************************************
 * Function           : RmSendMsg
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync linear data
 *                      u4TotLen -> Total length of linear data 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to send out the RM msgs (which are arrived from 
 *                      applications) to the peer.
*******************************************************************************/
UINT4
RmSendMsg (UINT1 u1SsnIndex, INT4 i4ConnFd, UINT1 *pu1Data, UINT4 u4TotLen)
{
    INT4                i4WrittenBytes = 0;
    UINT4               u4RetVal = RM_SUCCESS;
    UINT4               u4TxListCnt = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: ENTRY \r\n");
    u4TxListCnt = RM_SLL_COUNT (&RM_SSN_ENTRY_TX_LIST
            (RM_GET_SSN_INFO (u1SsnIndex)));

    /* if pending list is empty, try for new message sending.
     * Else, add this incoming message to pending list, to
     * ensure sequence number is maintained in ascending order
     */
    if (u4TxListCnt == 0)
    {
    u4RetVal = RmTcpSockSend (i4ConnFd, pu1Data, (UINT2) u4TotLen,
                              &i4WrittenBytes);
    if (u4RetVal == RM_FAILURE)
    {
        /* Send failure case to be handled */
        /* Restart the standby node */
        RM_TRC (RM_CRITICAL_TRC, "RmSendMsg: "
                "Reliable sync message send failed\n");
        RM_TX_STAT_SYNC_MSG_FAILED ()++;
        RmAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4TotLen, i4WrittenBytes);
        RmUtilCloseSockConn (u1SsnIndex);
        if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmSendMsg\n");
        }
        return (RM_FAILURE);
    }
    if (i4WrittenBytes != (UINT2) u4TotLen)
    {
        /* Partial data handling */
        RmAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4TotLen, i4WrittenBytes);
        RM_TRC1 (RM_SYNCUP_TRC,
                 "RmSendMsg: " "TCP socket send is successful with "
                 "partial data and size=%d\r\n", i4WrittenBytes);
        SelAddWrFd (i4ConnFd, RmTcpWriteCallBackFn);
        return (RM_SUCCESS);
    }
    else
    {
        RM_TX_STAT_SYNC_MSG_SENT ()++;
    }
    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        gRmInfo.bIsTxBuffFull = RM_FALSE;
    }
    /* Free the memory allocated for linear buf */
    MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
    }
    else
    {
        /* add the incoming message to pending list and register for select
         * to get trigger for writing retry
         */
        RmAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4TotLen, i4WrittenBytes);
        SelAddWrFd (i4ConnFd, RmTcpWriteCallBackFn);
    }
    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: EXIT \r\n");
    return (RM_SUCCESS);
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmSendAuditReqRespToPeer
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync linear data
 *                      u4TotLen -> Total length of linear data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send out the checksum related RM msgs (which 
 *                      are arrived from applications) to the peer.
*******************************************************************************/
UINT4
RmSendAuditReqRespToPeer (UINT1 u1SsnIndex, INT4 i4ConnFd, UINT1 *pu1Data,
                          UINT4 u4TotLen)
{
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4WrittenBytes = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: ENTRY \r\n");
    u4RetVal = RmTcpSockSend (i4ConnFd, pu1Data, (UINT2) u4TotLen,
                              &i4WrittenBytes);
    if (u4RetVal == RM_FAILURE)
    {
        /* Send failure case to be handled */
        /* Restart the standby node */
        RM_TRC (RM_CRITICAL_TRC, "RmSendMsg: "
                "Reliable sync message send failed\n");
        RmUtilCloseSockConn (u1SsnIndex);
        if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmSendMsg\n");
        }
        MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
        return (RM_FAILURE);
    }
    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        gRmInfo.bIsTxBuffFull = RM_FALSE;
    }
    /* Free the memory allocated for linear buf */
    MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: EXIT \r\n");
    return (RM_SUCCESS);
}
#endif

/******************************************************************************
 * Function           : RmSockWritableHandle
 * Input(s)           : i4ConnFd -> Connection identifier 
 * Output(s)          : None
 * Returns            : None
 * Action             : Routines writes the TX list messages into TCP 
 *                      socket send buffer.
 ******************************************************************************/
VOID
RmSockWritableHandle (INT4 i4ConnFd)
{
    tRmTxBufNode       *pRmTxBufNode = NULL;
    UINT1              *pCurTxBuf = NULL;
    UINT4               u4CurTxBufSize = 0;
    UINT4               u4RetVal = RM_SUCCESS;
    UINT1               u1SsnIndex = 0;
    INT4                i4ConnState = 0;
    INT4                i4WrittenBytes = 0;
    UINT1              *pResBuf = NULL;

    RM_TRC (RM_SYNCUP_TRC, "RmSockWritableHandle: ENTRY \r\n");
    if (RmGetSsnIndexFromConnFd (i4ConnFd, &u1SsnIndex) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmSockWritableHandle: Session doesnot "
                "exist for the connection fd\n");
        return;
    }
    if (RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus
        == RM_SOCK_CONNECT_IN_PROGRESS)
    {
        if (RmTcpSockConnect (RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr,
                              &i4ConnFd, &i4ConnState) == RM_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC, "RmSockWritableHandle: Connection retry "
                    "failed\n");
            /* If connection failed, donot clear the queue, instead wait for
             * the peer to connect, if connection lost with peer,
             * HB task will intimate about the peer dead
             * upon this event arrival clear the TX List */
            RmAddSyncMsgFromQToTxList (u1SsnIndex);
            RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = RM_INV_SOCK_FD;
            RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_NOT_CONNECTED;
            return;
        }
        if (i4ConnState == RM_SOCK_CONNECT_IN_PROGRESS)
        {
            RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
            RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                RM_SOCK_CONNECT_IN_PROGRESS;
            RM_TRC (RM_SYNCUP_TRC, "RmSockWritableHandle: Socket is in "
                    "Connection In progress state\r\n");
            KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
            return;
        }
        /* This residual memory allocation only be freed in the 
         * peer down event handle */
        if (RM_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
        {
            if (RmMemAllocForResBuf (&pResBuf) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC,
                        "RmSockWritableHandle: Residual buffer memory "
                        "allocation failed\n");
                RmUtilCloseSockConn (u1SsnIndex);
                RmCloseSocket (i4ConnFd);
                return;
            }
            RM_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
        }
        RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
        RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_CONNECTED;
    }
    if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnIndex))) ==
        0)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmSockWritableHandle: "
                "No data in the Tx List\r\n");
        KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
        return;
    }
    /* If connection status connection in progress */

    pRmTxBufNode = (tRmTxBufNode *) TMO_SLL_First
        (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnIndex)));
    while (pRmTxBufNode)
    {
        if (gRmInfo.u4DynamicSyncStatus == RM_TRUE)
        {
            u4CurTxBufSize = (RM_TX_MSG_BUF_NODE_MSG_SIZE (pRmTxBufNode)) -
                (RM_TX_MSG_BUF_NODE_OFFSET (pRmTxBufNode));
            pCurTxBuf = (RM_TX_MSG_BUF_NODE_MSG (pRmTxBufNode)) +
                (RM_TX_MSG_BUF_NODE_OFFSET (pRmTxBufNode));

            u4RetVal = RmTcpSockSend (i4ConnFd, pCurTxBuf,
                                      (UINT2) u4CurTxBufSize, &i4WrittenBytes);
            if (u4RetVal == RM_FAILURE)
            {
                /* Send failure case to be handled */
                /* Restart the standby node */
                RM_TRC (RM_CRITICAL_TRC, "RmSendMsg: "
                        "Reliable sync message send failed\n");
                RM_TX_STAT_SYNC_MSG_FAILED ()++;
                RmUtilCloseSockConn (u1SsnIndex);
                if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
                {
                    RM_TRC (RM_FAILURE_TRC, "SendEvent failed in "
                            "RmSockWritableHandle\n");
                }
                RmCloseSocket (i4ConnFd);
                return;
            }
            if (i4WrittenBytes != (UINT2) u4CurTxBufSize)
            {
                RM_TRC1 (RM_SYNCUP_TRC, "RmSockWritableHandle: "
                         "Tx List partial sync data written size=%d\r\n",
                         i4WrittenBytes);
                KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
                RM_TX_MSG_BUF_NODE_OFFSET (pRmTxBufNode) += i4WrittenBytes;
                SelAddWrFd (i4ConnFd, RmTcpWriteCallBackFn);
                return;
            }
            RM_TX_STAT_SYNC_MSG_SENT ()++;
            RM_TRC1 (RM_SYNCUP_TRC, "RmSockWritableHandle: "
                     "Tx List complete sync data written size=%d\r\n",
                     i4WrittenBytes);
        }
        TMO_SLL_Delete (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnIndex)),
                        &pRmTxBufNode->TxNode);
        MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId,
                            (UINT1 *) (RM_TX_MSG_BUF_NODE_MSG (pRmTxBufNode)));
        if (MemReleaseMemBlock
            (gRmInfo.RmTxBufMemPoolId, (UINT1 *) pRmTxBufNode) != MEM_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "RmCtrlMsgHandler: Mem release from ctrl msg "
                    "pool failed\n");
        }
        pRmTxBufNode = (tRmTxBufNode *) TMO_SLL_First
            (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnIndex)));
    }
    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        gRmInfo.bIsTxBuffFull = RM_FALSE;
    }
    if ((RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE) &&
        (RM_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED) &&
        (RM_GET_NODE_STATE () == RM_ACTIVE))
    {
        RM_TRC (RM_SYNCUP_TRC, "RmSockWritableHandle: FSW is triggered after "
                "TX LIST completion\r\n");
        /* Call the HB API to send HB message with FSW flag set */
        /* In this case, there can be scenario where a protocol might have taken
         * the sequence number and posted to queue. Suppose if a higher value
         * sequence number is already sent to the standby node and a lower value
         * sequence number is to be sent, then the messages need to transmitted
         * to standby node to avoid sequence mismatch. Process all the pending
         * messages in the queue.
         */
        RmQueProcessPendingMsg ();

        if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                           (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
        {
            /* Since the Tx list is not empty, some entries are not sent
             * while processing the queue. Return without sending event
             * to HB
             */
            KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
            return;
        }
        HbApiSendRmEventToHb (HB_FSW_EVENT);
    }

    if ((IssuGetMaintModeOperation() == OSIX_TRUE) &&
            (RM_GET_NODE_STATE () == RM_ACTIVE))
    {
        /* Process pending RM messages */
        RmQueProcessPendingMsg ();

        if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                           (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
        {
            KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
            return;
        }

        /* User has set Maintenance mode Admin status ,
         * so we need to set maintenance operstatus */ 
        IssuSetOperMaintenanceMode (ISSU_MAINTENANCE_OPER_ENABLE);
    }

    RM_TRC (RM_SYNCUP_TRC, "RmSockWritableHandle: EXIT \r\n");
    KW_FALSEPOSITIVE_FIX3 (i4ConnFd);

    return;
}

/******************************************************************************
 * Function           : RmSendCertGenMsgtoPeer 
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync linear data
 *                      u4TotLen -> Total length of linear data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send out the SSL Certificate generated msg
 * 						(which is arrived from applications) to the peer.
*******************************************************************************/
UINT4
RmSendCertGenMsgtoPeer (UINT1 u1SsnIndex, INT4 i4ConnFd, UINT1 *pu1Data,
                          UINT4 u4TotLen)
{
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4WrittenBytes = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: ENTRY \r\n");
    u4RetVal = RmTcpSockSend (i4ConnFd, pu1Data, (UINT2) u4TotLen,
                              &i4WrittenBytes);
    if (u4RetVal == RM_FAILURE)
    {
        /* Send failure case to be handled */
        /* Restart the standby node */
        RM_TRC (RM_CRITICAL_TRC, "RmSendMsg: "
                "Reliable sync message send failed\n");
        RmUtilCloseSockConn (u1SsnIndex);
        if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmSendMsg\n");
        }
        MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
        return (RM_FAILURE);
    }
    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        gRmInfo.bIsTxBuffFull = RM_FALSE;
    }
    /* Free the memory allocated for linear buf */
    MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
    RM_TRC (RM_SYNCUP_TRC, "RmSendMsg: EXIT \r\n");
    return (RM_SUCCESS);
}
