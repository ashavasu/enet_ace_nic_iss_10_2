/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmsz.c,v 1.5 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RMSZ_C
#include "rmincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsRMSizingParams[i4SizingId].u4StructSize,
                                     FsRMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(RMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RM_MAX_SIZING_ID; i4SizingId++)
    {
        if (RMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RMMemPoolIds[i4SizingId]);
            RMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
