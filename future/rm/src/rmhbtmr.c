/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmhbtmr.c,v 1.9 2013/06/19 13:25:16 siva Exp $
*
* Description: Timer related functions of RM Heart Beat module.
*********************************************************************/
#include "rmhbincs.h"

PRIVATE VOID        RmHbTmrInitTmrDesc (VOID);
/******************************************************************************
 * Function           : RmHbTmrInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create RM HB timer.
 ******************************************************************************/
UINT4
RmHbTmrInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrInit: ENTRY\r\n");
    if (TmrCreateTimerList ((CONST UINT1 *) RM_HB_TASK_NAME,
                            RM_HB_TMR_EXPIRY_EVENT, NULL,
                            (tTimerListId *) & (gRmHbInfo.TmrListId))
        != TMR_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbTmrInit: Heart Beat timer list creation Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    RmHbTmrInitTmrDesc ();
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrInit: EXIT\r\n");
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmHbTmrDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to delete RM HB timer list
 ******************************************************************************/
UINT4
RmHbTmrDeInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrDeInit: ENTRY\r\n");
    if (gRmHbInfo.TmrListId == NULL)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbTmrDeInit: " "Timer list is deleted already !!!\n");
        return (u4RetVal);
    }
    if (TmrDeleteTimerList (gRmHbInfo.TmrListId) != TMR_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbTmrDeInit: "
                   "Heart Beat timer list deletion Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    gRmHbInfo.TmrListId = NULL;
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrDeInit: EXIT\r\n");
    return (u4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RmHbTmrInitTmrDesc
 *    
 *    DESCRIPTION      : This function intializes the timer desc for all
 *                       the timers in RM HB module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
RmHbTmrInitTmrDesc (VOID)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrInitTmrDesc: ENTRY\r\n");
    gRmHbInfo.aTmrDesc[RM_HB_HEARTBEAT_TIMER].TmrExpFn = RmHbTmrHdlHbTmrExp;
    gRmHbInfo.aTmrDesc[RM_HB_PEER_DEAD_TIMER].TmrExpFn =
        RmHbTmrHdlPeerDeadTmrExp;
    gRmHbInfo.aTmrDesc[RM_HB_FSW_ACK_TIMER].TmrExpFn = RmHbTmrHdlFswAckTmrExp;
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrInitTmrDesc: EXIT\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RmHbTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       message is received by RM HB task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
RmHbTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;

    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrExpHandler: ENTRY\r\n");
    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gRmHbInfo.TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        RM_HB_TRC1 (RM_TMR_TRC, "RmHbTmrExpHandler: "
                    "Timer Id=%d expired\r\n", u1TimerId);
        /* The timer function does not take any parameter. */
        (*(gRmHbInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrExpHandler: EXIT\r\n");
}

/******************************************************************************
 * Function           : RmHbTmrStartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to start the RM HB module timers
 ******************************************************************************/
UINT4
RmHbTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrStartTimer: ENTRY\r\n");

    if (TmrStart (gRmHbInfo.TmrListId,
                  &(gRmHbInfo.RmHbTmrBlk[u1TmrId]),
                  u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        RM_HB_TRC1 (RM_TMR_TRC, "RmHbTmrStartTimer: Timer Id=%d "
                    "started successfully - EXIT\r\n", u1TmrId);
        return RM_SUCCESS;
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrStartTimer: Timer start failed !!!\r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmHbTmrStopTimer
 * Input(s)           : u1TmrId - Timer id
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to stop the RM HB module timers
 ******************************************************************************/
UINT4
RmHbTmrStopTimer (UINT1 u1TmrId)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrStopTimer: ENTRY\r\n");

    if (TmrStop (gRmHbInfo.TmrListId,
                 &(gRmHbInfo.RmHbTmrBlk[u1TmrId])) != TMR_FAILURE)
    {
        RM_HB_TRC1 (RM_TMR_TRC, "RmHbTmrStopTimer: Timer Id=%d "
                    "stopped successfully - EXIT\r\n", u1TmrId);
        return RM_SUCCESS;
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrStopTimer: Timer stopping failed !!!\r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmHbTmrRestartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to restart the RM HB module timers
 ******************************************************************************/
UINT4
RmHbTmrRestartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrRestartTimer: ENTRY\r\n");

    if (TmrRestart (gRmHbInfo.TmrListId, &(gRmHbInfo.RmHbTmrBlk[u1TmrId]),
                    u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        RM_HB_TRC1 (RM_TMR_TRC, "RmHbTmrRestartTimer: Timer Id=%d "
                    "started successfully - EXIT\r\n", u1TmrId);
        return RM_SUCCESS;
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrRestartTimer: Timer start failed !!!\r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmHbTmrHdlHbTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle Heart Beat timer expiry. Sends the 
 *                      HeartBeat message after HB Tmr expiry and restarts 
 *                      the timer.  
 ******************************************************************************/
VOID
RmHbTmrHdlHbTmrExp (VOID *pArg)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlHbTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);
    if (RmFormAndSendHbMsg () == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmHbTmrHdlHbTmrExp: "
                   "RmFormAndSendHbMsg -- Failure !!!\n");
    }
    if (RM_HB_GET_HB_INTERVAL () != RM_HB_MIN_HB_INTERVAL)
    {
        if (RmHbTmrRestartTimer
            (RM_HB_HEARTBEAT_TIMER, RM_HB_GET_HB_INTERVAL ()) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "RmHbTmrHdlHbTmrExp: "
                       "Heart Beat re-start timer Failure !!!\n");
        }
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlHbTmrExp: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmHbTmrHdlPeerDeadTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle PeerDead timer expiry. Sets the current
 *                      node in ACTIVE state and declares PEER DOWN.  
 ******************************************************************************/
VOID
RmHbTmrHdlPeerDeadTmrExp (VOID *pArg)
{
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlPeerDeadTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);

    if (gu1KeepAliveFlag == RM_TRUE)
    {
        if (RmHbTmrRestartTimer (RM_HB_PEER_DEAD_TIMER,
                                 RM_HB_GET_PEER_DEAD_INTERVAL ()) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Peer Dead restart timer Failure !!!\n");
        }
        gu1KeepAliveFlag = RM_FALSE;
        return;
    }
    RM_HB_RESET_FLAGS (RM_STANDBY_REBOOT);

    /* When the PEER node has gone down, STANDBY_SOFT_REBOOT
     * and STANDBY_PEER_UP flags used to inform the state to
     * the peer node can be reset. */
    RM_HB_RESET_FLAGS (RM_STDBY_PROTOCOL_RESTART);
    RM_HB_RESET_FLAGS (RM_STDBY_PEER_UP);

    /* Hear beat msgs are not rcvd for 4*HBInterval.
     * Peer down. If this node can participate in election (priority!=0),
     * change the state of this node to ACTIVE */
    if (RM_HB_GET_SELF_NODE_PRIORITY () != 0)
    {
        RM_HB_SET_ACTIVE_NODE_ID (RM_HB_GET_SELF_NODE_ID ());
        RM_HB_TRC1 (RM_SEM_TRC, "ACTIVE NODE elected: %0x\n",
                    RM_HB_GET_ACTIVE_NODE_ID ());
        RmStateMachine (RM_HB_PEER_DOWN);
    }
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlPeerDeadTmrExp: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmHbTmrHdlFswAckTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Handles the Force switchover ack timer expiry - 
 *                      Sends an event to RM core module to allow 
 *                      the static and dynamic sync. messages.
 ******************************************************************************/
VOID
RmHbTmrHdlFswAckTmrExp (VOID *pArg)
{
    tRmNotificationMsg  NotifMsg;

    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlFswAckTmrExp: "
               "FSW Ack timer expired... ENTRY\n");
    UNUSED_PARAM (pArg);

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
    NotifMsg.u4NodeId = RM_HB_GET_SELF_NODE_ID ();
    NotifMsg.u4State = RM_ACTIVE;
    NotifMsg.u4AppId = RM_APP_ID;
    NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
    NotifMsg.u4Status = RM_FAILED;
    NotifMsg.u4Error = RM_NONE;
    UtlGetTimeStr (NotifMsg.ac1DateTime);
    SPRINTF (NotifMsg.ac1ErrorStr,
             "Force-Switchover ACK is not received from standby in %d milli secs.",
             RM_HB_MAX_FSW_ACK_TIME);
    RmApiTrapSendNotifications (&NotifMsg);
    RM_HB_TRC (RM_TMR_TRC, "RmHbTmrHdlFswAckTmrExp: "
               "FSW Ack timer expired... EXIT\n");
}

/******************************************************************************
 * Function           : RmResumeProtocolOperation
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : This routine is used to resume RM protocol operations 
 *                      once initialization of other modules is completed.
 ******************************************************************************/
INT4
RmResumeProtocolOperation (VOID)
{
    tRegPeriodicTxInfo  RegnInfo;
    RM_HB_TRC (RM_TMR_TRC, "RmResumeProtocolOperation: ENTRY\n");

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));

    /* Start the Heart Beat Timer */
    /* HB Timer to be started only when hb interval is > 10 ms */
    if (RM_HB_GET_HB_INTERVAL () == RM_HB_MIN_HB_INTERVAL)
    {
        RegnInfo.SendToApplication = RmHbTxTsk;
        CfaRegisterPeriodicPktTx (CFA_APP_RM, &RegnInfo);
    }
    else
    {
        CfaDeRegisterPeriodicPktTx (CFA_APP_RM);
        if (RmHbTmrStartTimer (RM_HB_HEARTBEAT_TIMER,
                               RM_HB_GET_HB_INTERVAL ()) != RM_SUCCESS)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Heart Beat start timer Failure !!!\n");
            return RM_FAILURE;
        }
    }
    /* Initial HB message to detect the peer is rebooted */
    if (RmFormAndSendHbMsg () == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmResumeProtocolOperation: "
                   "RmFormAndSendHbMsg -- Failure !!!\n");
    }
    /* Start the Peer Dead Timer */
    if (RmHbTmrStartTimer (RM_HB_PEER_DEAD_TIMER,
                           RM_HB_GET_PEER_DEAD_INTERVAL ()) != RM_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "!!! Peer Dead start timer Failure !!!\n");
        return RM_FAILURE;
    }
    RM_HB_TRC (RM_TMR_TRC, "RmResumeProtocolOperation: EXIT\n");
    return RM_SUCCESS;
}
