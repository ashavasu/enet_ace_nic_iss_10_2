/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmmain.c,v 1.100 2016/07/30 10:41:31 siva Exp $
*
* Description: Redundancy manager main functions.
*********************************************************************/
#ifndef _RMMAIN_C_
#define _RMMAIN_C_

#include "rmincs.h"
#ifdef SNMP_2_WANTED
#include "fsrmwr.h"
#endif
#ifdef MBSM_WANTED
#include "mbsnp.h"
#endif
#include "fssocket.h"
#include "size.h"
#include "rmglob.h"

#ifdef MBSM_WANTED
/* array of MAX_LC elements - used to check whether the LM has been cleared */
UINT1               gau1LcTbl[MBSM_MAX_SLOTS];
#endif /* MBSM_WANTED */

extern tRcvFileInfo RcvFileInfo[MAX_NO_OF_FILES];
/******************************************************************************
 * Function           : RmTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Rm Task main routine to initialize RM task parameters, 
 *                      main loop processing.
 ******************************************************************************/
VOID
RmTaskMain (INT1 *pArg)
{
#ifdef L2RED_WANTED
    tRmProtoEvt         ProtoEvt;
#endif
#ifdef MBSM_WANTED
    UINT4               u4StackingModel = 0;
    tPeerRecEntry      *pPeerEntry;
#endif
    UINT4               u4Event = 0;
    INT4                i4CliSockFd = RM_INV_SOCK_FD;
    UINT4               u4PeerIpAddr = 0;

    UNUSED_PARAM (pArg);

    /* RM core module processing */
    if (OsixTskIdSelf (&RM_TASK_ID) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Get Task Id  for RM Task failed\n");
        RM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* RM task related initializations */
    if (RmTaskInit () == RM_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "!!! RM Task Init Failure !!!\n");

        /* Indicate the status of initialization to main routine */
        RM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create a File Transfer Server task in ACTIVE node */
    if (OsixTskCrt ((UINT1 *) RM_FT_SRV_TASK_NAME,
                    RM_FT_SRV_TASK_PRI,
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) RmFtSrvTaskMain, 0,
                    &RM_FT_SRV_TASK_ID) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_FAILURE_TRC,
                "RM FileTransfer server task creation failed\n");
        RM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create a Configuration Syncup task in STANDBY node */
    if (OsixTskCrt ((UINT1 *) RM_CONFIG_SYNCUP_TASK_NAME,
                    (RM_CONFIG_SYNCUP_TASK_PRIORITY | OSIX_SCHED_RR),
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) RmConfSyncUpTaskMain, 0,
                    &RM_CONF_SYNCUP_TASK_ID ()) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC,
                "RM Configuration SyncUp task creation failed\n");
        RM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    RM_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register MIBs if there are any */
    RegisterFSRM ();
#endif /* SNMP_2_WANTED */

    if (RM_HB_MODE != ISS_RM_HB_MODE_INTERNAL)
    {
        if (RmWaitForOtherModulesBootUp () != RM_SUCCESS)
        {
            RmDeInit ();
            RM_TRC (RM_FAILURE_TRC,
                    "RmTaskMain: RM's wait for HA modules boot up failed\n");
            return;
        }
    }

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {

        /* File Transfer Task Init */
        if (RmFtSrvTaskInit () == RM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "RM File Transfer Task Init Failure\n");
            return;
        }

        /* Syslog */
        gRmInfo.u4SysLogId =
            SYS_LOG_REGISTER ((CONST UINT1 *) "RM", SYSLOG_CRITICAL_LEVEL);
        if (gRmInfo.u4SysLogId <= 0)
        {
            RM_TRC (RM_CRITICAL_TRC, "RM Syslog Init Failure\n");
            return;
        }
    }

    /* main loop */
    while (1)
    {
        RM_RECV_EVENT (RM_TASK_ID, RM_MODULE_EVENTS, OSIX_WAIT, &u4Event);

        RM_LOCK ();

        if (u4Event & RM_CTRL_QMSG_EVENT)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_CTRL_QMSG_EVENT\n");
            /* Control Queue messages handling */
            RmCtrlMsgHandler ();
        }

        if (u4Event & RM_ISSU_MAINT_MODE_EVENT)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_ISSU_MAINT_MODE_EVENT\n");

            /* Event from ISSU to set a node to maintenance mode */
            RmIssuMaintenanceModeEnable ();
        }

#ifdef L2RED_WANTED
        if (u4Event & RM_STANDBY_FSW_RCVD)
        {
            /* This is referred by MBSM and MSR modules to differentiate the 
             * force-switchover and failover and do the necessary actions */
            RM_TRC (RM_CRITICAL_TRC,
                    "RmTaskMain: RM_STANDBY_FSW_RCVD\n");
            RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
            RM_SET_FLAGS (HB_FORCE_SWITCHOVER);
        }

        if (u4Event & RM_GO_ACTIVE)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                   "RM Curent State "
                   " Event received to transition to ACTIVE \n");
            /* RM node status ACTIVE handle */
            RmHandleNodeStateUpdate (RM_ACTIVE);
        }

        if (u4Event & RM_GO_STANDBY)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                     "RM Curent State "
                    " Event received to transition to STANDBY \n");
            /* RM node status STANDBY handle */
            RmHandleNodeStateUpdate (RM_STANDBY);
        }

        if (u4Event & RM_PROCESS_PENDING_SYNC_MSG)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_PROCESS_PENDING_SYNC_MSG\n");
            /* RM to process all the pending RX buffer messages */
            RmPrcsPendingRxBufMsgs ();
        }

        if (u4Event & RM_COMM_LOST_AND_RESTORED_EVT)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_COMM_LOST_AND_RESTORED_EVT\n");
            /* Backplane communication lost and 
             * restored handle */
            RmHandleCommLostAndRestoredEvent ();
        }

        if (u4Event & RM_TRIG_FSW_FROM_MGMT)
        {
            /* Event received  from nmh routine when FSW is triggered */
            RM_TRC (RM_CRITICAL_TRC,
                    "RmTaskMain: RM_TRIG_FSW_FROM_MGMT\n");
            RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
            RM_SET_FLAGS (HB_FORCE_SWITCHOVER);
            RmHandleForceSwitchover ();
        }

        if (u4Event & RM_ACTIVE_FSW_FAILED)
        {
            RM_TRC (RM_CRITICAL_TRC,
                    "RmTaskMain: RM_ACTIVE_FSW_FAILED\n");
            /* Max. FSW Ack wait timer is expired to get 
             * the Ack from peer STANDBY/Peer is dead 
             * so change the trans in prgs to FALSE
             * and continue to operate as ACTIVE.
             * */
            RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
            RM_RESET_FLAGS (HB_FORCE_SWITCHOVER);
            RmAllowStaticAndDynamicSyncMsgs ();
        }

#ifdef L2RED_TEST_WANTED
        if (u4Event & RM_DYNAMIC_AUDIT_PROCESSED)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_RESTART_PROTOCOLS\n");
            RmDynAuditPktFromApp ();
        }
#endif
#endif
        if (u4Event & RM_SSL_CERT_GENERATED)
		{
			RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
			        "RmTaskMain: RM_CERT_GEN_MSG\n");
			RmSslCertGenMsgFromApp ();
		}
        if (u4Event & RM_V3_CRYPTSEQ_UPD)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_V3CRYPTSEQ_MSG\n");
            RmV3CryptSeqMsgFromApp ();
        }
        if (u4Event & RM_RESTART_PROTOCOLS)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_RESTART_PROTOCOLS\n");
            RmHandleProtocolRestart ();
        }

        if (u4Event & RM_REINIT_STANDBY)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_REINIT_STANDBY \n");
            RmReInitToStandbyState ();
        }

        if (u4Event & RM_INIT_BULK_REQUEST)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_INIT_BULK_REQUEST\n");
            RmHandleInitBulkUpdate ();
        }

        if (u4Event & RM_TCP_NEW_CONN_RCVD)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_TCP_NEW_CONN_RCVD\n");
            /* New Connection accept */
            if (RmTcpAcceptNewConnection (RM_TCP_SRV_SOCK_FD (),
                                          &i4CliSockFd, &u4PeerIpAddr)
                == RM_SUCCESS)
            {
                RmTcpUpdateSsnTable (i4CliSockFd, u4PeerIpAddr);
            }
        }

        if (u4Event & RM_PKT_FROM_APP)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_PKT_FROM_APP\n");
            /* Handle pkts from applications */
            RmProcessPktFromApp ();
        }

        if (u4Event & RM_PROCS_RX_BUFF_SELF_EVENT)
        {
            /* This will help to resume the rx buffer processing */
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_PROCS_RX_BUFF_SELF_EVENT\n");
            RmRxProcessBufferedPkt ();
        }

        if (u4Event & RM_SEND_TO_FILES_PEERS)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_SEND_TO_FILES_PEERS\n");
            /* RM Timers expiry handling */
            RmSendFileToPeers ();
        }
#ifdef L2RED_WANTED
        if (u4Event & RM_TMR_EXPIRY_EVENT)
        {
            RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                    "RmTaskMain: RM_TMR_EXPIRY_EVENT\n");
            /* RM Timers expiry handling */
            RmTmrExpHandler ();
        }

        if (u4Event & MSR_RESTORE_COMPLETE)
        {
            /* Received restoration complete trigger from the MSR module
             * Send an event to trigger the dynamic bulk update process
             * for other protocols
             */
            ProtoEvt.u4AppId = RM_MSR_APP_ID;
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;
            RM_UNLOCK ();
            RmApiHandleProtocolEvent (&ProtoEvt);
            RM_LOCK ();

            if (RM_GET_NODE_STATE () == RM_STANDBY)
            {
                /* Rx buffer processing is blocked when the MSR
                 * receives GO_STANDBY event. Since the restoration
                 * is completed, unblock the Rx buffer processing
                 */
                RM_TRC (RM_SYNCUP_TRC, "RmTaskMain: " "UNLOCKING Rx Buff\r\n");
                RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;

                /* Process the packets buffered during MSR restoration */
                RmRxProcessBufferedPkt ();
            }
        }
#endif
#ifdef MBSM_WANTED
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            if (u4Event & STACK_TMR_EXP)
            {
                /* Stack timer expiry */
                StackHandleTimerExpiry ();
            }
            if (u4Event & CFA_PORT_CREATION_COMPLETED)
            {
                /* Ports are created according to 
                 * the slotmodule.conf entries */

                TMO_SLL_Scan (RM_PEER_RECORD_SLL, pPeerEntry, tPeerRecEntry *)
                {

                    IssCustGetSlotMACAddress (pPeerEntry->u4SlotIndex,
                                              pPeerEntry->au1SwitchBaseMac);
                    if ((MEMCMP
                         (pPeerEntry->au1SwitchBaseMac,
                          gIssSysGroupInfo.BaseMacAddr, RM_MAC_ADDR_LEN) != 0)
                        &&
                        (MEMCMP
                         (pPeerEntry->au1SwitchBaseMac, gNullMacAdress,
                          RM_MAC_ADDR_LEN) != 0))
                    {
                        CfaUpdatePortMacAddress (pPeerEntry->u4SlotIndex,
                                                 pPeerEntry->au1SwitchBaseMac);
                    }
                }
            }

        }
        u4StackingModel = ISS_GET_STACKING_MODEL ();
        if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
        {
            if (u4Event & RM_TRIGGER_SELF_ATTACH_EVENT)
            {
                RmHandleTriggerSelfAttachEvent ();
            }
        }
#endif
        RM_UNLOCK ();
    }
}

/******************************************************************************
 * Function           : RmHandleTriggerSelfAttachEvent
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to Trigger Self Atatch Event to MBSM for Dual 
                        Unit Stacking Model.
 ******************************************************************************/
VOID
RmHandleTriggerSelfAttachEvent (VOID)
{
    RmUtilPostDataOrEvntToProtocol (RM_MBSM_APP_ID, RM_TRIGGER_SELF_ATTACH,
                                    NULL, 0, 0);
    return;

}

/******************************************************************************
 * Function           : RmHandleForceSwitchover
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to handle the force-switchover.
 ******************************************************************************/
VOID
RmHandleForceSwitchover (VOID)
{
    UINT1               u1SsnIndex = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleForceSwitchover: ENTRY \r\n");
    /* Management lock is taken to ensure that static configured not occurred */
    MGMT_LOCK ();
    /* Dynamic sync block */
    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_TRUE);
    /* Dynamic sync messages will be blocked by checking this flag */

    /* Call HB API to avoid any election during 
     * pending TX buffer messages processing */

    HbApiSendRmEventToHb (HB_CHG_TO_TRANS_IN_PRGS);

    if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                       (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        /* FSW will be sent to peer once the TX buffer 
         * messages are cleared */
        RM_TRC (RM_CTRL_PATH_TRC,
                "RmHandleForceSwitchover: TX LIST exists \r\n");
        return;
    }

    /* In this case, there can be scenario where a protocol might have taken
     * the sequence number and posted to queue. Suppose if a higher value
     * sequence number is already sent to the standby node and a lower value
     * sequence number is to be sent, then the messages need to transmitted
     * to standby node to avoid sequence mismatch. Process all the pending
     * messages in the queue.
     */
    RmQueProcessPendingMsg ();
    if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                       (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        /* Since the Tx list is not empty, some entries are not sent
         * while processing the queue. Return without sending event
         * to HB
         */
        return;
    }

    /* Call HB API to send the HB with FSW bit set to peer */
    HbApiSendRmEventToHb (HB_FSW_EVENT);

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleForceSwitchover: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmAllowStaticAndDynamicSyncMsgs
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to allow static and dynamic sync messages
 ******************************************************************************/
VOID
RmAllowStaticAndDynamicSyncMsgs (VOID)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmAllowStaticAndDynamicSyncMsgs: ENTRY \r\n");
    /* Management lock is taken to ensure that static configured not occurred */
    MGMT_UNLOCK ();
    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_FALSE);    /* Dynamic sync block */
    /* Dynamic sync messages will be blocked by checking the this flag */
    RM_TRC (RM_CTRL_PATH_TRC, "RmAllowStaticAndDynamicSyncMsgs: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmTaskInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Rm Task initialization routine to initialize RM task 
 *                      parameters. 
 ******************************************************************************/
UINT4
RmTaskInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    /* Initialize the default value of the global struct */
    if (RmInit () == RM_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "!!! Init Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }

#ifdef L2RED_WANTED
    /* Create timers for RM */
    if (u4RetVal != RM_FAILURE)
    {
        if (RmTmrInit () == RM_FAILURE)
        {
            RmDeInit ();
            RM_TRC (RM_CRITICAL_TRC, "!!! TmrInit Failure !!!\n");
            u4RetVal = RM_FAILURE;
        }
    }

#endif
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        StackCreateTimer ();
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to initialize RM global data structure. 
 ******************************************************************************/
UINT4
RmInit (VOID)
{
    tOsixTaskId         TempTskId;
    /* Dummy pointers for system sizing during run time */
    tRmPktBlock        *pRmPktBlock = NULL;
    tRmHRInfo           RmHRInfo;
#if defined (BSDCOMP_SLI_WANTED) && defined (L2RED_WANTED)
    tNodeInfo           SelfNodeInfo;
#endif
    UINT4               u4EntId;
    UINT4               u4ConnIdx;
    UINT4               u4AppIndex = 0;
#ifdef L2RED_TEST_WANTED
    UINT2               u2AppdId = 0;
#endif
    INT1               *pRmIfType = NULL;
    UINT1              *pu1RmStackInterface = NULL;

    UNUSED_PARAM (pRmPktBlock);

    /* Before clearing the global info. 
     * Store the RM task id in temporary variable 
     * then move it to task id variable */
    TempTskId = gRmInfo.RmTaskId;

    MEMSET (&gRmInfo, 0, sizeof (tRmInfo));

    gRmInfo.RmTaskId = TempTskId;
    gRmInfo.i4SrvSockFd = RM_INV_SOCK_FD;
    gRmInfo.i4FtSrvSockFd = RM_INV_SOCK_FD;
    gRmInfo.u4RmTrc = RM_DEFAULT_TRC;
    gRmInfo.u4RmModTrc = RM_MOD_ALL_TRC;
    gRmInfo.u4RmTrcBkp = gRmInfo.u4RmTrc;
    gRmInfo.u4PrevNodeState = RM_INIT;
    gRmInfo.u4NodeState = RM_INIT;
    gRmInfo.u1PeerNodeState = RM_INIT;
    gRmInfo.u4DynamicSyncStatus = RM_TRUE;
    /* Active node is yet to be identified */
    gRmInfo.u4ActiveNode = 0;

    /* Initialise the Tx and Rx buffers full status as false */
    gRmInfo.bIsTxBuffFull = RM_FALSE;
    gRmInfo.bIsRxBuffFull = RM_FALSE;
/* HITLESS RESTART */
    /* Initialise the hitless restart flag as disable */
    gRmInfo.u1HRFlag = RM_HR_STATUS_DISABLE;
    MEMSET (&RmHRInfo, 0, sizeof (tRmHRInfo));
    RmHRInfo.pFnRmHRBulkStore = &RmHRFileStore;
    RmHRInfo.pFnRmHRBulkRestore = &RmHRFileRestore;
    RmHRInfo.u1StorageType = RM_HR_FILE_STORAGE;
    RmHRRegister (&RmHRInfo);
    RmHbRegisterApps ();

#ifdef L2RED_TEST_WANTED
    MEMSET (&gRmDynAuditInfo, 0, (RM_MAX_APPS * sizeof (tRmDynAuditInfo)));
    for (u2AppdId = 0; u2AppdId < RM_MAX_APPS; u2AppdId++)
    {
        gRmDynAuditInfo[u2AppdId].u4AuditStatus = RM_DYN_AUDIT_NOT_TRIGGERED;
        gRmDynAuditInfo[u2AppdId].u4IsAppSetForAudit = RM_FALSE;
        gRmDynAuditInfo[u2AppdId].i4AuditChkSumValue = RM_INVALID_CHKSUM_VALUE;
        gRmDynAuditInfo[u2AppdId].i4RecvdChkSum = RM_INVALID_CHKSUM_VALUE;
    }
    gu1IsAuditChksumRespPend = RM_FALSE;
#endif
    /* Peer node Ip addr is not yet learnt */
    gRmInfo.u4PeerNode = 0;

    /* Initialize the no. of file transfer connection count */
    gRmInfo.u4FtConnCnt = 0;

    gRmInfo.u4Flags = 0;
    /* Notification array free Id */
    gRmInfo.u1NxtFreeNotifArrayId = 0;

    /* Initializing the variables. Normal Bulk Update/ Sw Audit failure
       Bulk update in progress */
    gRmInfo.u1IsBulkUpdtInProgress = RM_FALSE;
    gRmInfo.u1IsBulkUpdtFromSwAudit = RM_FALSE;

    /* Shutdown and start operation status */
    RM_SHUT_START_STATE () = RM_SHUT_START_NOT_OCCURED;

    /* Initialize with static configuration not restored */
    gRmInfo.u1StaticConfigStatus = RM_STATIC_CONFIG_NOT_RESTORED;

    /* Flag used by RM in the ACTIVE node to decide
     * whether to send out the trap or not*/
    gRmInfo.u4RmTxEnabled = RM_FALSE;

    /* Don't Ignore Abort messages unless you are asked to */
    RM_IGNORE_ABORT_NOTIFICATION () = RM_FALSE;

    /* Initize the RM notification state to protocols to RM_INIT */
    RM_SET_PREV_NOTIF_NODE_STATE (RM_INIT);

    /* Make the FSW flag RM_FSW_NOT_OCCURED */
    RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
    gu1ModuleBootup = 0;

    /* Initialize the Rx buffer related parameters */
    RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_FALSE;
    RM_RX_SEQ_MISSMATCH_FLG () = RM_FALSE;
    RM_RX_LAST_SEQ_NUM_PROCESSED () = 0;
    RM_RX_BUFF_NODE_COUNT () = 0;
    RM_RX_LAST_APPID_PROCESSED () = RM_APP_ID;    /* This is an invalid value
                                                   for Rx buffer processing */

    /* Initialize the reboot flag and reboot count */
    gRmInfo.bRestartFlag = RM_RESTART_DISABLE;
    gRmInfo.u1RestartRetryCnt = RM_DEF_RESTART_CNT;

    /* Reset the statistics counters */
    RmUtilResetStatistics ();

    gRmInfo.u4SwitchId = IssGetSwitchid ();
    gRmInfo.u4ConfigState = IssGetUserPreference ();

    /* Get RM Interface Name from NvRam */
    pRmIfType = IssGetRmIfNameFromNvRam ();

    /* If RM Stacking Interface type is oob, RM Interface read from issnvram
       should be used */
    if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        if ((STRNCMP (pRmIfType, ISS_RM_IF_TYPE_ETH,
                      STRLEN (ISS_RM_IF_TYPE_ETH)) == 0) ||
            (STRNCMP (pRmIfType, ISS_RM_IF_TYPE_LO,
                      STRLEN (ISS_RM_IF_TYPE_LO)) == 0))
        {
#if defined (BSDCOMP_SLI_WANTED) && defined (L2RED_WANTED)
            if (RmGetIfIpInfo ((UINT1 *) pRmIfType, &SelfNodeInfo) !=
                RM_FAILURE)
            {
                gRmInfo.u4SelfNode = SelfNodeInfo.u4IpAddr;
                gRmInfo.u4SelfNodeMask = SelfNodeInfo.u4NetMask;
            }
            else
            {
                RM_TRC (RM_CRITICAL_TRC, "!!! Get self node ip Failure !!!\n");
                return RM_FAILURE;
            }
#endif
        }
        else if (STRCMP (pRmIfType, ISS_RM_IF_TYPE_STACK) == 0)
        {
#ifdef MBSM_WANTED
            /* Get self node ip addr and initialize in data struct */
            gRmInfo.u4SelfNode = RmGetStackIpForSlot (gRmInfo.u4SwitchId);
            RmMbsmNpGetStackMac (gRmInfo.u4SwitchId, gRmInfo.au1StkMacAddr);
            gRmInfo.u4SelfNodeMask = 0xffff0000;
            TMO_SLL_Init (RM_PEER_RECORD_SLL);
#endif
        }
        else
        {
            RM_TRC (RM_CRITICAL_TRC, "!!! Invalid RM Interface type !!!\n");
            return RM_FAILURE;
        }
    }
    /* If RM Stacking Interface is Inband, RM Stacking Interface read from 
       NodeID should be used for communication. */

    /* IP Address and Subnet Mask to be used should be read from Nodeid file */
    else
    {

        pu1RmStackInterface = IssGetRMStackInterface ();
        gRmInfo.u4SelfNode = IssGetRMIpAddress ();
        gRmInfo.u4SelfNodeMask = IssGetRMSubnetMask ();
        MEMCPY (gRmInfo.au1RmStackInterface, pu1RmStackInterface,
                STRLEN (pu1RmStackInterface));
    }
    /* Initialize the global registration table */
    for (u4EntId = 0; u4EntId < RM_MAX_APPS; u4EntId++)
    {
        gRmInfo.RmAppInfo[u4EntId] = NULL;
    }

    /* Initialize the number of peers */
    gRmInfo.u1NumPeers = 0;
    gRmInfo.u1IssuNumPeers = 1;

#ifdef L2RED_WANTED
    MEMSET (gRmInfo.RmBulkUpdStsMask, 0, sizeof (tRmAppList));
#endif

    for (u4ConnIdx = 0; u4ConnIdx < RM_MAX_SESSIONS; u4ConnIdx++)
    {
        MEMSET (&(gRmInfo.RmFtConnTbl[u4ConnIdx]), 0, sizeof (tRmFtConnInfo));
        gRmInfo.RmFtConnTbl[u4ConnIdx].u4Status = RM_FT_CLIENT_INACTIVE;
    }

    for (u4ConnIdx = 0; u4ConnIdx < RM_MAX_SYNC_SSNS_LIMIT; u4ConnIdx++)
    {
        RM_GET_SSN_INFO (u4ConnIdx).pResBuf = NULL;
        RM_GET_SSN_INFO (u4ConnIdx).i4ConnFd = RM_INV_SOCK_FD;
        RM_GET_SSN_INFO (u4ConnIdx).u4PeerAddr = 0;
        RM_GET_SSN_INFO (u4ConnIdx).u2ResBufLen = 0;
        RM_GET_SSN_INFO (u4ConnIdx).i1ConnStatus = RM_SOCK_NOT_CONNECTED;
        RM_SLL_INIT (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u4ConnIdx)));
    }

    /* Create the RM Buffer (hash Table) */
    if (RmRxBufCreate () == RM_FAILURE)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC, "RX buffer creation Failed in RmInit\n");
        return RM_FAILURE;
    }

    if (RmSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC,
                "MemPool Creation for RM Peer table Pool Failed in RmInit\n");
        return RM_FAILURE;
    }

    gRmInfo.RmTxBufMemPoolId = RMMemPoolIds[MAX_RM_TX_BUF_NODES_SIZING_ID];
    gRmInfo.RmCtrlMsgPoolId = RMMemPoolIds[MAX_RM_CTRL_MSG_NODES_SIZING_ID];
    gRmInfo.RmRegMemPoolId = RMMemPoolIds[MAX_RM_APPS_SIZING_ID];
    gRmInfo.RmMsgPoolId = RMMemPoolIds[MAX_RM_NODE_INFO_BLOCKS_SIZING_ID];
    gRmInfo.RmNotifMsgPoolId = RMMemPoolIds[MAX_RM_NOTIFICATION_INFO_SIZING_ID];
    gRmInfo.RmRxBufPoolId = RMMemPoolIds[MAX_RM_RX_BUF_NODES_SIZING_ID];
    gRmInfo.RmRxBufPktPoolId = RMMemPoolIds[MAX_RM_RX_BUF_PKT_NODES_SIZING_ID];
    gRmInfo.RmPktQMsgPoolId = RMMemPoolIds[MAX_RM_PKTQ_MSG_SIZING_ID];
#ifdef L2RED_TEST_WANTED
    gRmInfo.RmDynCksumPktQMsgPoolId = RMMemPoolIds[MAX_RM_APP_MSG_SIZING_ID];
#endif
    gRmInfo.RmPktMsgPoolId = RMMemPoolIds[MAX_RM_PKT_MSG_BLOCKS_SIZING_ID];
    gRmInfo.RmPeerTblPoolId = RMMemPoolIds[MAX_RM_SYNC_SSNS_SIZING_ID];

    /* Create a Q in RM to listen on the msgs sent by Applications. 
     * RM recvs msgs from Apps and send it to the peer. */
    if (RM_CREATE_QUEUE ((UINT1 *) RM_PKT_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                         RM_PKT_Q_DEPTH, &(RM_QUEUE_ID)) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC, "Creation of RM Q Failed in RmInit\n");
        return RM_FAILURE;
    }

    if (RM_CREATE_QUEUE ((UINT1 *) RM_APP_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                         RM_PKT_Q_DEPTH, &(RM_APP_QUEUE_ID)) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC, "Creation of RM Q Failed in RmInit\n");
        return RM_FAILURE;
    }

    if (RM_CREATE_QUEUE ((UINT1 *) RM_CTRL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                         MAX_RM_CTRL_MSG_NODES,
                         &(RM_CTRL_QUEUE_ID)) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC, "Creation of RM Q Failed in RmInit\n");
        return RM_FAILURE;
    }

    if (OsixCreateSem (RM_PROTO_SEM_NAME, 1, 0,
                       (tOsixSemId *) & (RM_PROTO_SEM ())) != OSIX_SUCCESS)
    {
        RmDeInit ();
        RM_TRC (RM_CRITICAL_TRC, "Rm Protocol Sem Creation failed in RmInit\n");
        return RM_FAILURE;
    }
#ifdef MBSM_WANTED
    MEMSET (gau1LcTbl, RM_FALSE, (MBSM_MAX_SLOTS * sizeof (UINT1)));
#endif

#ifdef L2RED_TEST_WANTED
    RmTestInit ();
#endif

    gRmTotalSwitchOverTime = 0;
    gu1RmLockVariable = OSIX_FALSE;
    for (u4AppIndex = 0; u4AppIndex < RM_MAX_APPS; u4AppIndex++)
    {
        MEMSET (&(gaRmSwitchOverTime[u4AppIndex]), 0,
                sizeof (tRmSwitchOverTime));
        gaRmSwitchOverTime[u4AppIndex].u4AppId = u4AppIndex;
        MEMCPY ((gaRmSwitchOverTime[u4AppIndex].au1AppName),
                gapc1AppName[u4AppIndex],
                (RM_MAX_FILE_NAME_LEN * sizeof (UINT1)));
    }

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to deinitialize RM global data structure. 
 ******************************************************************************/
VOID
RmDeInit (VOID)
{
    if (RM_TCP_SRV_SOCK_FD () != RM_INV_SOCK_FD)
    {
        RmCloseSocket (RM_TCP_SRV_SOCK_FD ());
    }

    RmSizingMemDeleteMemPools ();

    RmRxBufDestroy ();

    if (RM_QUEUE_ID != NULL)
    {
        RM_DELETE_QUEUE (RM_QUEUE_ID);
        RM_QUEUE_ID = NULL;
    }
    if (RM_APP_QUEUE_ID != NULL)
    {
        RM_DELETE_QUEUE (RM_APP_QUEUE_ID);
        RM_APP_QUEUE_ID = NULL;
    }
    if (RM_PROTO_SEM () != NULL)
    {
        RM_DELETE_SEMAPHORE (RM_PROTO_SEM ());
        RM_PROTO_SEM () = NULL;
    }
    if (RM_CTRL_QUEUE_ID != NULL)
    {
        RM_DELETE_QUEUE (RM_CTRL_QUEUE_ID);
        RM_CTRL_QUEUE_ID = NULL;
    }
    RmTmrDeInit ();
/* HITLESS RESTART */
    RmHRDeRegister ();
}

/******************************************************************************
 * Function           : RmPeerInfoMsgHandle
 * Input(s)           : pRmPeerCtrlMsg - Peer control message (Peer up/ Peer down)
 * Output(s)          : None
 * Returns            : None
 * Action             : Provides the peer id and the status (UP/DOWN) 
 ******************************************************************************/
VOID
RmPeerInfoMsgHandle (tRmPeerCtrlMsg * pRmPeerCtrlMsg)
{
    tRmTxBufNode       *pRmTxBufNode = NULL;
    UINT1               u1SsnCnt = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmPeerInfoMsgHandle: ENTRY \r\n");
    for (u1SsnCnt = 0; u1SsnCnt < RM_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (pRmPeerCtrlMsg->u1PeerState == RM_PEER_UP)
        {
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmPeerInfoMsgHandle: "
                     "Peer UP is received for the peer=%x\r\n",
                     pRmPeerCtrlMsg->u4PeerId);
            /* Session table initializations are required only when
             * connection status is RM_SOCK_NOT_CONNECTED, otherwise while
             * accepting the connection initialization will be done */
            if (RM_GET_SSN_INFO (u1SsnCnt).i1ConnStatus
                == RM_SOCK_NOT_CONNECTED)
            {
                RM_TRC (RM_CTRL_PATH_TRC,
                        "RmPeerInfoMsgHandle: New peer received \r\n");
                RM_SLL_INIT (&
                             (RM_SSN_ENTRY_TX_LIST
                              (RM_GET_SSN_INFO (u1SsnCnt))));
                RM_GET_SSN_INFO (u1SsnCnt).u4PeerAddr =
                    pRmPeerCtrlMsg->u4PeerId;
                RM_SET_PEER_NODE_ID (pRmPeerCtrlMsg->u4PeerId);
                RM_PEER_NODE_COUNT ()++;
            }
            RM_SET_PEER_NODE_STATE (RM_PEER_UP);

            if (RM_GET_NODE_STATE () == RM_ACTIVE)
            {
               /* Ignore to increment the count the case of standby node
                * comes up with Issu maintenance mode */  
                if(IssuGetMaintModeOperation() == OSIX_TRUE)
                {
                        RM_ISSU_PEER_NODE_COUNT()++;
                }
                if (gu4BulkUpdateInProgress == RM_FALSE)
                {
                    gu4BulkUpdateInProgress = RM_TRUE;
                    RmTmrStartTimer (RM_BULK_UPDATE_TIMER,
                                     RM_BULK_UPDATE_TMR_INT);
                }
                if (gu4TrafficToCpuAllowed == RM_TRUE)
                {
                    gu4TrafficToCpuAllowed = RM_FALSE;
#ifdef NPAPI_WANTED
                    RmFsNpHRSetStdyStInfo (NP_RM_HR_SUSPEND_COPY_TO_CPU_TRAFFIC,
                                           NULL);
#endif
                    RmTmrStartTimer (RM_CPU_TRAFFIC_CTRL_TIMER,
                                     RM_CPU_TRAFFIC_TMR_INT);
                }
                /* Reset the Rx buffer related flags and seq number */
                RmRxResetRxBuffering ();
            }
#ifdef L2RED_WANTED
            if (RM_GET_NODE_STATE () == RM_ACTIVE)
            {
                RmUpdatePeerNodeCntToProtocols (RM_PEER_UP);
            }
#endif
        }
        else if (pRmPeerCtrlMsg->u1PeerState == RM_PEER_DOWN)
        {
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmPeerInfoMsgHandle: "
                     "Peer DOWN is received for the peer=%x\r\n",
                     pRmPeerCtrlMsg->u4PeerId);
            if (gu4BulkUpdateInProgress == RM_TRUE)
            {
                RmTmrStopTimer (RM_BULK_UPDATE_TIMER);
                gu4BulkUpdateInProgress = RM_FALSE;
            }
            if (gu4TrafficToCpuAllowed == RM_FALSE)
            {
                RmTmrStopTimer (RM_CPU_TRAFFIC_CTRL_TIMER);
#ifdef NPAPI_WANTED
                RmFsNpHRSetStdyStInfo (NP_RM_HR_RESUME_COPY_TO_CPU_TRAFFIC,
                                       NULL);
#endif
                gu4TrafficToCpuAllowed = RM_TRUE;
            }
            if ((RM_GET_SSN_INFO (u1SsnCnt).u4PeerAddr) !=
                (pRmPeerCtrlMsg->u4PeerId))
            {
                continue;
            }
            gRmInfo.bIsTxBuffFull = RM_FALSE;
            gRmInfo.bIsRxBuffFull = RM_FALSE;
            RM_SET_PEER_NODE_ID (0);
            RmClearBulkUpdatesStatus ();
            if (RM_SLL_COUNT
                (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnCnt))) != 0)
            {
                pRmTxBufNode = (tRmTxBufNode *) TMO_SLL_First
                    (&(RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnCnt))));
                while (pRmTxBufNode)
                {
                    TMO_SLL_Delete (&
                                    (RM_SSN_ENTRY_TX_LIST
                                     (RM_GET_SSN_INFO (u1SsnCnt))),
                                    &pRmTxBufNode->TxNode);
                    MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId,
                                        (UINT1 *) pRmTxBufNode->pu1Msg);
                    if (MemReleaseMemBlock
                        (gRmInfo.RmTxBufMemPoolId,
                         (UINT1 *) pRmTxBufNode) != MEM_SUCCESS)
                    {
                        RM_TRC (RM_FAILURE_TRC,
                                "RmPeerInfoMsgHandle: Mem release from ctrl msg "
                                "pool failed\n");
                    }
                    pRmTxBufNode = (tRmTxBufNode *) TMO_SLL_First
                        (&(RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnCnt))));
                }
                SelRemoveWrFd (RM_GET_SSN_INFO (u1SsnCnt).i4ConnFd);
            }
            if (RM_GET_SSN_INFO (u1SsnCnt).pResBuf != NULL)
            {
                MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId,
                                    (UINT1 *) RM_GET_SSN_INFO (u1SsnCnt).
                                    pResBuf);
                RM_GET_SSN_INFO (u1SsnCnt).pResBuf = NULL;
                RM_GET_SSN_INFO (u1SsnCnt).u2ResBufLen = 0;
                RM_TRC (RM_CTRL_PATH_TRC,
                        "RmPeerInfoMsgHandle: Residual memory freed \r\n");
            }
            RM_TRC1 (RM_CRITICAL_TRC,
                         "RmPeerInfoMsgHandle: Closing Socket connection "
                         "fd: %d\r\n",RM_GET_SSN_INFO (u1SsnCnt).i4ConnFd);
            RmUtilCloseSockConn (u1SsnCnt);
            RM_GET_SSN_INFO (u1SsnCnt).u4PeerAddr = 0;
            RM_SLL_INIT (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnCnt)));
            RM_GET_SSN_INFO (u1SsnCnt).i1ConnStatus = RM_SOCK_NOT_CONNECTED;
            RM_PEER_NODE_COUNT ()--;
            RM_SET_PEER_NODE_STATE (RM_PEER_DOWN);

#ifdef L2RED_WANTED
            if(IssuGetMaintModeOperation() == OSIX_TRUE)
            {
                if ((RM_ISSU_PEER_NODE_COUNT () > 0))
                {
                    RM_ISSU_PEER_NODE_COUNT()--;
                }
            }


            if (RM_GET_NODE_STATE () == RM_ACTIVE)
            {
              if(IssuGetMaintModeOperation() != OSIX_TRUE) 
              {
                 RmUpdatePeerNodeCntToProtocols (RM_PEER_DOWN);
              }
               if (RM_PEER_NODE_COUNT () == 0)
                {
                    /* Reset the value of static config status */
                    RM_STATIC_CONFIG_STATUS () = RM_STATIC_CONFIG_RESTORED;
                }
            }
#endif
        }
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmPeerInfoMsgHandle: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmUpdatePeerNodeCntToProtocols
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : Updates the peer up/peer down status to protocols. 
 ******************************************************************************/
VOID
RmUpdatePeerNodeCntToProtocols (UINT1 u1Event)
{
    UINT4               u4EntId = 0;
    tRmNodeInfo        *pRmNodeInfo[RM_MAX_APPS];

    RM_TRC (RM_CTRL_PATH_TRC, "RmUpdatePeerNodeCntToProtocols: ENTRY \r\n");
    for (u4EntId = 0; u4EntId < RM_MAX_APPS; u4EntId++)
    {
        /* Standby up / down event is not required to be sent to CLI, MSR
         * and MPLS as bulk updates are not sent from these modules
         */
        if ((u4EntId == RM_MPLS_APP_ID) || (u4EntId == RM_ISS_APP_ID))
        {
            continue;
        }

        if ((gRmInfo.RmAppInfo[u4EntId] != NULL) &&
            (gRmInfo.RmAppInfo[u4EntId]->pFnRcvPkt != NULL) &&
            (gRmInfo.RmAppInfo[u4EntId]->u4ValidEntry == TRUE))
        {
            if (MemAllocateMemBlock
                (gRmInfo.RmMsgPoolId, (UINT1 **) &(pRmNodeInfo[u4EntId]))
                != MEM_SUCCESS)
            {
                RM_TRC1 (RM_CRITICAL_TRC,
                         "Unable to allocate memory for "
                         "peer node state (up/down) updation to protocol %d\n",
                         u4EntId);
                continue;
            }

            MEMSET (pRmNodeInfo[u4EntId], 0, sizeof (tRmNodeInfo));
            pRmNodeInfo[u4EntId]->u1NumStandby = RM_PEER_NODE_COUNT ();

            if (RmUtilPostDataOrEvntToProtocol (u4EntId, u1Event,
                                                (tRmMsg *) (VOID *)
                                                pRmNodeInfo[u4EntId],
                                                RM_NODE_COUNT_MSG_SIZE,
                                                0) == RM_FAILURE)
            {
                RM_FREE ((tRmMsg *) (VOID *) pRmNodeInfo[u4EntId]);
            }
        }
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmUpdatePeerNodeCntToProtocols: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmPrcsPendingRxBufMsgs 
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None 
 * Action             : RX buffer processing is carried out 
 *                      in standby RM core module when the following 
 *                      situations occurred,
 *                      1. Force switchover
 *                      2. Failover
 ******************************************************************************/
VOID
RmPrcsPendingRxBufMsgs (VOID)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmPrcsPendingRxBufMsgs: ENTRY \r\n");

    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_TRUE);
    if (RM_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS)
    {
        /* Static configuration in progress */
        /* Process the packets buffered during MSR restoration */
        RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
        RmRxProcessBufferedPkt ();
    }
    else if ((RM_RX_BUFF_NODE_COUNT () == 0) &&
             (RM_RX_LAST_SEQ_NUM_PROCESSED () ==
              RM_RX_LAST_ACK_RCVD_SEQ_NUM ()))
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmPrcsPendingRxBufMsgs: "
                "No Pending RX buffer\r\n");

        HbApiSendRmEventToHb (HB_SYNC_MSG_PROCESSED);

        RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_FALSE);
    }
    else
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmPrcsPendingRxBufMsgs: "
                "RX buffer is pending\r\n");

        if (RM_RX_SEQ_MISSMATCH_FLG () == RM_TRUE)
        {
            RM_TRC (RM_SYNCUP_TRC,
                    "RmPrcsPendingRxBufMsgs: Seq Recov timer is already running due to sequence no. mismatch\r\n");
            RmTmrStopTimer (RM_SEQ_RECOV_TIMER);
            RM_RX_SEQ_MISSMATCH_FLG () = RM_FALSE;

            /* Update the last processed sequence number */
            RM_RX_INCR_LAST_SEQNUM_PROCESSED ();

            /* Update statistics - for the buffered messages that is processed */
            RM_RX_STAT_SYNC_MSG_PROCESSED ()++;

            RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
            /*Now process the Rx Buffer */
            RmRxProcessBufferedPkt ();
        }
        else
        {
            RM_TRC (RM_SYNCUP_TRC,
                    "RmPrcsPendingRxBufMsgs: Buffer processing is going on for pending Rx packets \r\n");
        }
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmPrcsPendingRxBufMsgs: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmHandleSystemRecovery
 * Input(s)           : u2Cause -> Cause of an issue
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine handles the system recovery in the system
 *                       is in inconsistent state by restarting the protocols
 *                       registered with RM. If Active node reaches the inconsistent
 *                       state, it asks the standby node to restart its protocols.
 *                       If the standby node is in inconsistent state, it directly
 *                       restarts all the protocols registered with the Rmgr.
 *                       The standby node restarts its protocols rather than
 *                       restarting the node to avoid the delay in node bootup.
 ******************************************************************************/
VOID
RmHandleSystemRecovery (UINT2 u2Cause)
{
    tRmPeerCtrlMsg      RmPeerCtrlMsg;

    /* During RM_STATE_CHG_DURING_STATIC_BLK, RM_STATE_CHG_DURING_DYNAMIC_BLK,
     * RM_COLDSTDBY_REBOOT the peer node will be already dead. Hence, process
     * with the system recovery even when the peer count is zero */
    if (((RM_PEER_NODE_COUNT () == 0) &&
         ((u2Cause != RM_STATE_CHG_DURING_STATIC_BLK)
          && (u2Cause != RM_STATE_CHG_DURING_DYNAMIC_BLK)
          && (u2Cause != RM_COLDSTDBY_REBOOT)))
        || (RM_GET_NODE_STATE () == RM_INIT))
    {
        /* System recovery should be ignored if 
         * the peer node count is zero or node state is INIT*/
        return;
    }

    /* Perform the following actions on standby node */
    switch (u2Cause)
    {
            /* Add different cases based on the action 
             * to be performed for the corresponding cause */
        case RM_SYNCUP_ABORT:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Sync-up bulk abort is occurred\n");
            break;
        case RM_TX_BUFF_ALLOC_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Tx buffer allocation is failed\n");
            break;
        case RM_TX_FAILED_AND_TX_BUFF_FULL:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: RM Transmission is failed "
                    "and Tx buffer is full\n");
            break;
        case RM_RX_BUFF_ALLOC_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Rx buffer allocation is failed\n");
            break;
        case RM_RX_CRU_BUFF_ALLOC_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Rx CRU buffer allocation is failed\n");
            break;
        case RM_RX_BUFF_NODE_ALLOC_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Rx buffer node allocation is failed\n");
            break;
        case RM_MAX_CONN_RETRY_EXCEEDED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Max connection retry is exceeded\n");
            /* the MAX_CONN_RETRY_EXCEEDED is received only in standby node.
             * Hence directly restart the node to start fresh connections 
             * with the Active node */
            RmSystemRestart ();
            return;
        case RM_CONF_APPLY_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Configuration apply is failed\n");
            return;
        case RM_CONF_FAIL_BUFF_ALLOC_FAILED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Configuration failure buffer "
                    "allocation is failed\n");
            break;
        case RM_API_SYSTEM_RECOVERY:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: System recovery handling "
                    "from external module\n");
            break;
        case RM_STATE_CHG_DURING_STATIC_BLK:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Active down during "
                    "static bulk update in progress\n");
            /* RM_STATE_CHG_DURING_STATIC_BLK can occur only when active fails
             * during static bulk update in the standby. Hence, restart the standby node
             * as no configurations are present in it. */
            RmSystemRestart ();
            break;
        case RM_STATE_CHG_DURING_DYNAMIC_BLK:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Active down during "
                    "bulk bulk update in progress\n");
            /* RM_STATE_CHG_DURING_DYNAMIC_BLK can occur only when active fails
             * during dynamic bulk update in the standby. Hence, restart the standby node
             * with only the static configurations */
            RmSystemRestart ();
            break;
        case RM_CONN_LOST_AND_RESTORED:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Connection lost and"
                    "restored handling\n");
            break;
        case RM_COLDSTDBY_REBOOT:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Cold standby reboot\n");
            /* in the cold standby support during a state change case,
             * both the active and the standby nodes reboot their nodes */
            RmSystemRestart ();
            return;
        default:
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                    "RmHandleSystemRecovery: Default recovery case\n");
            break;
    }

    /* When an abort or buffer outage occurs in the standby node, 
     * restart the protocols in the standby node. When the abort or 
     * buffer outage occurs in the active node, ask the standby node
     * to restart its protocols. */

    if ((gRmInfo.u1RestartRetryCnt != 0) &&
        (gRmInfo.u1RestartCntr >= gRmInfo.u1RestartRetryCnt))
    {
        if (RM_GET_NODE_STATE () == RM_STANDBY)
        {
            /* When the restart counter has exceeded in the standby node,
             * reboot the node. 
             */
            RmSystemRestart ();
            return;
        }
        else
        {
            /* When the restart counter has exceeded the limit in the active
             * node, do not ask the standby node to restart its protocols.
             * Just return. */
            return;
        }
    }

    /* When the Restart flag is disabled, dont restart the protocols 
     * in standby node. 
     * when the Restart counter has exceeded the max retry count configured,
     * reboot the standby node.
     * When the Restart retry count is configured as zero, always restart
     * the protocols in standby node.
     */
    if (gRmInfo.bRestartFlag == RM_RESTART_DISABLE)
    {
        return;
    }

    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        /* When the Active node asks the standby node to restart its
         * protocols, the active node manually initates a peer down event
         * to all the protocols so that the protocols stops sending the static
         * and dynamic sync up messages to the standby node. */
        RmPeerCtrlMsg.u4PeerId = RM_GET_PEER_NODE_ID ();
        RmPeerCtrlMsg.u1PeerState = RM_PEER_DOWN;
        RmPeerInfoMsgHandle (&RmPeerCtrlMsg);
    }

    /* Send the STANDBY_SOFT_REBOOT flag to the
     * peer node and increment the counter */
    HbApiSetFlagAndTxHbMsg (HB_STDBY_PROTOCOL_RESTART);

    if (gRmInfo.u1RestartRetryCnt != 0)
    {
        gRmInfo.u1RestartCntr++;
    }
    /* If the standby node is to be rebooted rather than just restarting the
     * protocols, then use the functions RmHbApiHandleStandbyReboot to reboot
     * the standby node from active and RmSystemRestart to directly restart
     * the standby node. */
    return;
}

/******************************************************************************
 * Function Name      : RmAllocTxBuf
 *
 * Description        : This is the API function used by external modules
                        to allocate memory for sync message
 * Input(s)           : sync message size
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Sync message buffer
 *****************************************************************************/

tRmMsg             *
RmAllocTxBuf (UINT4 u4SyncMsgSize)
{
    tRmMsg             *pMsg = NULL;

    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        RM_TRC1 (RM_CRITICAL_TRC, "RmAllocTxBuf: Buffer allocation "
                 "for sync msg Tx is not allowed because RM Tx is not ready "
                 "and Tx buffer threshold (%d * 0.75) is reached \r\n",
                 MAX_RM_TX_BUF_NODES);
        return NULL;
    }
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((u4SyncMsgSize + RM_HDR_LENGTH),
                                      RM_HDR_LENGTH);

    return pMsg;
}

/******************************************************************************
 * Function Name      : RmHbRegisterApps
 *
 * Description        : This API is used by RM to register with HB module
 *
 * Input(s)           : None
 *
 * Output(s)          : None                                                   
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
RmHbRegisterApps (VOID)
{
   HbRegisterApps (HB_RM_APP);
}
#endif /*_RMMAIN_C_*/
