/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmapi.c,v 1.50 2017/01/25 13:18:28 siva Exp $
*
* Description: RM APIs to interface with applications.
***********************************************************************/
#include "rmincs.h"

/******************************************************************************
 * Function           : RmApiSendHbEvtToRm 
 * Input(s)           : pRmHbMsg - Pointer to an RM HB message 
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : This is an exported API to interact with Heart Beat Module.
 *                      This API should be used appropriately 
 *                      when External RM is defined.
 ******************************************************************************/
UINT4
RmApiSendHbEvtToRm (tRmHbMsg * pRmHbMsg)
{
    tRmCtrlQMsg        *pRmCtrlQMsg = NULL;
    UINT4               u4Event = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmApiSendHbEvtToRm: ENTRY \r\n");

    /* Events processed by this API *
     * 1. GO_ACTIVE -> GO_ACTIVE notification to RM core module 
     * 2. GO_STANDBY -> GO_STANDBY notification to RM core module
     * 3. RM_PEER_UP -> Peer node up notification
     * 4. RM_PEER_DOWN -> Peer node down notification
     * 5. RM_PROC_PENDING_SYNC_MSG -> Event for completing 
     *                                the pending synchornization messages
     *                                in RM core module.
     * 6. RM_FSW_FAILED_IN_ACTIVE  -> Force-switchover ACK is not received in 
     *                                predetermined time, so, sending 
     *                                the notification to RM core module 
     *                                to allow static and 
     *                                dynamic sync messages.
     * 7. RM_FSW_RCVD_IN_STANDBY   -> Force switchover reception notification 
     *                                to STANDBY RM core module.
     * 8. RM_COMM_LOST_AND_RESTORED -> RM interface down and up notification.
     */

    switch (pRmHbMsg->u4Evt)
    {
        case GO_ACTIVE:
            u4Event = RM_GO_ACTIVE;
            RM_TRC (RM_CRITICAL_TRC,"Sending GO_ACTIVE notification to RM core module\r\n");
            break;
        case GO_STANDBY:
            u4Event = RM_GO_STANDBY;
            RM_TRC (RM_CRITICAL_TRC,"Sending GO_STANDBY notification to RM core module\r\n");
            break;
        case RM_PEER_UP:
            /* Intentional fall through */
        case RM_PEER_DOWN:
            if ((pRmCtrlQMsg = (tRmCtrlQMsg *) (MemAllocMemBlk
                                                (gRmInfo.RmCtrlMsgPoolId))) ==
                NULL)
            {
                RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                        "RmApiSendHbEvtToRm: Memory allocation from "
                        "Control message memory pool failed\n");
                return RM_FAILURE;
            }

            MEMSET (pRmCtrlQMsg, 0, sizeof (tRmCtrlQMsg));

            pRmCtrlQMsg->u4MsgType = RM_PEER_INFO_MSG;
            pRmCtrlQMsg->PeerCtrlMsg.u4PeerId = pRmHbMsg->u4PeerAddr;
            pRmCtrlQMsg->PeerCtrlMsg.u1PeerState = (UINT1) pRmHbMsg->u4Evt;
            if (RmQueEnqCtrlMsg (pRmCtrlQMsg) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC,
                        "RmApiSendHbEvtToRm: Send to Control Q failed\n");
                return RM_FAILURE;
            }
            return RM_SUCCESS;
        case RM_PROC_PENDING_SYNC_MSG:
            u4Event = RM_PROCESS_PENDING_SYNC_MSG;
            break;
        case RM_FSW_FAILED_IN_ACTIVE:
            u4Event = RM_ACTIVE_FSW_FAILED;
            break;
        case RM_FSW_RCVD_IN_STANDBY:
            u4Event = RM_STANDBY_FSW_RCVD;
            break;
        case RM_COMM_LOST_AND_RESTORED:
            u4Event = RM_COMM_LOST_AND_RESTORED_EVT;
            break;
        case RM_TRIGGER_SELF_ATTACH:
            u4Event = RM_TRIGGER_SELF_ATTACH_EVENT;
            break;
        default:
            RM_TRC (RM_FAILURE_TRC, "RmApiSendHbEvtToRm: "
                    "Invalid event received\r\n");
            return RM_FAILURE;
    }
    if (RM_SEND_EVENT (RM_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmApiSendHbEvtToRm: Send Event "
                "RM_TCP_NEW_CONN_RCVD Failed\n");
        return RM_FAILURE;
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmApiSendHbEvtToRm: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRegisterProtcols
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE. 
 * Action             : Routine used by protocols to register with RM.
 ******************************************************************************/
UINT4
RmRegisterProtocols (tRmRegParams * pRmReg)
{
    UINT4               u4SrcEntId;
    UINT1               u1Event = 0;

    if (pRmReg == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC,
                "Invalid Registration params from application\n");
        return RM_FAILURE;
    }

    /* Validate the AppId given by application */
    u4SrcEntId = pRmReg->u4EntId;
    if ((u4SrcEntId == 0) || (u4SrcEntId >= RM_MAX_APPS))
    {
        RM_TRC (RM_CRITICAL_TRC,
                "Invalid application id during registration\n");
        return RM_FAILURE;
    }

    RM_LOCK ();

    /* Allocate memory for the new entry */
    if (MemAllocateMemBlock
        (gRmInfo.RmRegMemPoolId,
         (UINT1 **) &(gRmInfo.RmAppInfo[u4SrcEntId])) != MEM_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC,
                "Unable to allocate memory for the new registration entry\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    MEMSET (gRmInfo.RmAppInfo[u4SrcEntId], 0, sizeof (tRmAppInfo));

    /* Update the application specific info */
    gRmInfo.RmAppInfo[u4SrcEntId]->pFnRcvPkt = pRmReg->pFnRcvPkt;
    gRmInfo.RmAppInfo[u4SrcEntId]->u4SrcEntId = u4SrcEntId;
    gRmInfo.RmAppInfo[u4SrcEntId]->u4DestEntId = u4SrcEntId;
    gRmInfo.RmAppInfo[u4SrcEntId]->u4ValidEntry = TRUE;
    OSIX_BITLIST_SET_BIT (gRmInfo.RmRegMask, pRmReg->u4EntId,
                          sizeof (tRmAppList));

    RmInitBulkUpdatesStatusMask (u4SrcEntId, RM_TRUE);

    /* Notify RM state to the protocol */
    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        /* If the module is started after the exe is booted then bulk update 
         * request will not be received for that module in Active node. As a
         * result the bulk update status for that module is not set and hence
         * force-switchover will not be allowed. To avoid this problem,
         * the bulk update status for that module is set as true.
         */
        OSIX_BITLIST_SET_BIT (gRmInfo.RmBulkUpdSts, u4SrcEntId,
                              sizeof (tRmAppList));
        u1Event = GO_ACTIVE;
        RmUtilPostDataOrEvntToProtocol (u4SrcEntId, u1Event, NULL, 0, 0);
    }
    else if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        /* In the standby node if all the protocols are restarted, 
         * then the GO_STANDBY event will be sent to the protocols
         * after the restart process is over. Hence no need to send
         * the event here */
        if (gRmInfo.bIsRestartInPrgs == RM_FALSE)
        {
            u1Event = GO_STANDBY;
            /* If the module has been shutdown and started again at Run-time
             * then RM_STATIC_CONFIG_RESTORED event needs to be send inorder
             * to change the protocol node state from IDLE to STANDBY
             */
            if (RM_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_RESTORED)
            {
                u1Event = RM_CONFIG_RESTORE_COMPLETE;
            }
            RmUtilPostDataOrEvntToProtocol (u4SrcEntId, u1Event, NULL, 0, 0);
        }
    }

    RM_UNLOCK ();
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmDeRegisterProtcols
 * Input(s)           : u4SrcEntId - Appl Id of the appl.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE. 
 * Action             : Routine used by protocols to de-register with RM.
 ******************************************************************************/
UINT4
RmDeRegisterProtocols (UINT4 u4SrcEntId)
{
    RM_LOCK ();
    if (gRmInfo.RmAppInfo[u4SrcEntId] == NULL)
    {
        RM_TRC1 (RM_CRITICAL_TRC, "RmDeRegistration failed for AppId %d\n",
                 u4SrcEntId);
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    /* Free the memory of this entry */
    if (MemReleaseMemBlock
        (gRmInfo.RmRegMemPoolId,
         (UINT1 *) gRmInfo.RmAppInfo[u4SrcEntId]) != MEM_SUCCESS)
    {
        RM_TRC1 (RM_CRITICAL_TRC,
                 "DeReg: Mem block release failed for AppId %d\n", u4SrcEntId);
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    OSIX_BITLIST_RESET_BIT (gRmInfo.RmRegMask, u4SrcEntId, sizeof (tRmAppList));
    gRmInfo.RmAppInfo[u4SrcEntId] = NULL;

    RmInitBulkUpdatesStatusMask (u4SrcEntId, RM_FALSE);

    RM_UNLOCK ();
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmEnqMsgToRmFromAppl
 * Input(s)           : pRmMsg - msg from appl.
 *                      u2DataLen - Len of msg
 *                      u4SrcEntId - EntId from which the msg has arrived
 *                      u4DestEntId - EntId to which the msg has to be sent
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to enq the msg from applications to RM task.
 *                      Unique sequence number is automatically assigned to all
 *                      sync-up messages before enqueue to RM pkt queue.
 ******************************************************************************/
UINT4
RmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                      UINT4 u4DestEntId)
{
    tRmPktQMsg         *pRmPktQMg = NULL;
    tRmHdr              RmHdr;
    UINT4               u4SeqNum = 0;

    if ((RM_HDR_LENGTH + u2DataLen) > RM_MAX_SYNC_PKT_LEN)
    {
        RM_TRC2 (RM_CRITICAL_TRC, "RmEnqMsgToRmFromAppl: "
                 "RM data size is exceeded the limit, "
                 "Sync-up is dropped for =%s application "
                 "and the dropped size %u\n",
                 gapc1AppName[u4DestEntId], u2DataLen);
        return RM_FAILURE;
    }

    RM_LOCK ();
    if ((RM_GET_NODE_STATE () == RM_ACTIVE) &&
        (RM_HR_GET_STATUS () == RM_HR_STATUS_DISABLE))
    {
        if (RM_PEER_NODE_COUNT () == 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC, "RmEnqMsgToRmFromAppl: "
                     "Peer Node does not exist. Dynamic Sync-up not required"
                     "for AppId=%s\n", gapc1AppName[u4SrcEntId]);
            RM_FREE (pRmMsg);
            RM_UNLOCK ();
            return RM_SUCCESS;
        }

        if (RmUtilIsDynamicSyncAllowed (u4DestEntId) == RM_FALSE)
        {
            RM_TRC1 (RM_SYNCUP_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl: "
                     "Dynamic Sync-up is not allowed for AppId=%s\n",
                     gapc1AppName[u4DestEntId]);
            RM_FREE (pRmMsg);
            RM_UNLOCK ();
            return RM_SUCCESS;
        }
    }
#ifdef L2RED_TEST_WANTED
    if ((RM_TEST_IS_PROTO_SYNCUP_BLOCKED (u4DestEntId) == RM_TRUE) &&
        (RM_HR_GET_STATUS () == RM_HR_STATUS_DISABLE))
    {
        RM_TRC1 (RM_CRITICAL_TRC, "RmEnqMsgToRmFromAppl: "
                 "[TEST] Sync-up is blocked for AppId=%s\n",
                 gapc1AppName[u4DestEntId]);
        RM_FREE (pRmMsg);
        RM_UNLOCK ();
        return RM_SUCCESS;
    }
#endif

    /* Form RM header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    /* Actual checksum will be calculated after copying the 
     * CRU buf to linear buf before sending to peer */
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4MsgType = OSIX_HTONL (RM_UNUSED);

    RmHdr.u4SrcEntId = OSIX_HTONL (u4SrcEntId);
    RmHdr.u4DestEntId = OSIX_HTONL (u4DestEntId);

    if (RM_GET_NODE_STATE () != RM_ACTIVE)    
    {
	/* Standby node need not send sync-up messages with sequence number.
         * For Active the Sequence No is filled during Packet TX.*/
        RmHdr.u4SeqNo = OSIX_HTONL (RM_UNUSED);
    }

    RM_TRC2 (RM_SYNCUP_TRC | RM_CTRL_PATH_TRC, "RmEnqMsgToRmFromAppl: "
             "Sync pkt to be sent [AppId=%s, Seq#=%d]\r\n",
             gapc1AppName[u4SrcEntId], u4SeqNum);
    /* Prepend RM hdr to RM data */
    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_DYNAMIC_SYNC_MSG;
    pRmPktQMg->DynSyncMsg = pRmMsg;

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMg)
            != MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmUtilReleaseMemForPktQMsg Mem block release failed for"
                     " block %u\n", pRmPktQMg);
        }
        pRmPktQMg = NULL;

        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    /* RM lock should be released after posting the message to queue, since
     * during force switch over, there should not be a scenario where a seq
     * number is reserved but message is not posted to queue. This is because
     * RM processes all the pending queue messages before aplying force switch
     * over
     */
    RM_UNLOCK ();

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
#ifdef L2RED_TEST_WANTED
    RM_LOCK ();
    RM_TEST_INCR_PROTO_TX_COUNT (u4DestEntId);
    RM_UNLOCK ();
#endif
    return (RM_SUCCESS);
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmSendDynAuditChkSumReqToStdBy
 * Input(s)           : pRmMsg - msg from appl.
 *                      u2DataLen - Len of msg
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send the checksum request msg to standby.
 ******************************************************************************/
UINT4
RmSendDynAuditChkSumReqToStdBy (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tRmPktQMsg         *pRmPktQMg = NULL;
    tRmHdr              RmHdr;
    /* Form RM header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    /* Actual checksum will be calculated after copying the
     * CRU buf to linear buf before sending to peer */
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4MsgType = OSIX_HTONL (RM_DYN_AUDIT_CHKSUM_PEER_REQ);

    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONL (RM_UNUSED);
    /* Prepend RM hdr to RM data */
    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        return RM_FAILURE;
    }

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_DYN_AUDIT_CHKSUM_REQUEST;
    pRmPktQMg->DynSyncMsg = pRmMsg;

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMg)
            != MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmUtilReleaseMemForPktQMsg Mem block release failed for"
                     " block %u\n", pRmPktQMg);
        }
        pRmPktQMg = NULL;
        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }

    /* RM lock should be released after posting the message to queue, since
     * during force switch over, there should not be a scenario where a seq
     * number is reserved but message is not posted to queue. This is because
     * RM processes all the pending queue messages before aplying force switch
     * over
     */

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmSendDynAuditChkSumRespToActive
 * Input(s)           : pRmMsg - msg from appl.
 *                      u2DataLen - Len of msg
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send the checksum response msg to acive.
 ******************************************************************************/
UINT4
RmSendDynAuditChkSumRespToActive (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tRmPktQMsg         *pRmPktQMg = NULL;
    tRmHdr              RmHdr;
    /* Form RM header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    /* Actual checksum will be calculated after copying the
     * CRU buf to linear buf before sending to peer */
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4MsgType = OSIX_HTONL (RM_DYN_AUDIT_CHKSUM_PEER_RESP);

    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONL (RM_UNUSED);
    /* Prepend RM hdr to RM data */
    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_DYN_AUDIT_CHKSUM_RESPONSE;
    pRmPktQMg->DynSyncMsg = pRmMsg;

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMg)
            != MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmUtilReleaseMemForPktQMsg Mem block release failed for"
                     " block %u\n", pRmPktQMg);
        }
        pRmPktQMg = NULL;
        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    /* RM lock should be released after posting the message to queue, since
     * during force switch over, there should not be a scenario where a seq
     * number is reserved but message is not posted to queue. This is because
     * RM processes all the pending queue messages before aplying force switch
     * over
     */
    RM_UNLOCK ();

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}
#endif

/******************************************************************************
 * Function           : RmEnqChkSumMsgToRmFromApp
 * Input(s)           : pRmMsg - msg from appl.
 *                      u2DataLen - Len of msg
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to enqueue checksum value to RM from particular 
 *                      module.
 ******************************************************************************/
UINT4
RmEnqChkSumMsgToRmFromApp (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef L2RED_TEST_WANTED
    tRmDynAuditChkSumMsg *pRmChkSumQMg = NULL;

    RM_LOCK ();
    if ((pRmChkSumQMg = (tRmDynAuditChkSumMsg *) MemAllocMemBlk
         (gRmInfo.RmDynCksumPktQMsgPoolId)) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    MEMSET (pRmChkSumQMg, 0, sizeof (tRmPktQMsg));

    pRmChkSumQMg->u2AppId = u2AppId;
    pRmChkSumQMg->u2ChkSumValue = u2ChkSum;

    if (RM_SEND_TO_QUEUE (RM_APP_QUEUE_ID,
                          (UINT1 *) &pRmChkSumQMg,
                          RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if ((MemReleaseMemBlock (gRmInfo.RmDynCksumPktQMsgPoolId,
                                 (UINT1 *) pRmChkSumQMg)) == MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmEnqChkSumMsgToRmFromApp Mem block release failed for"
                     " block %u\n", pRmChkSumQMg);
        }
        pRmChkSumQMg = NULL;

        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    /* RM lock should be released after posting the message to queue, since
     * during force switch over, there should not be a scenario where a seq
     * number is reserved but message is not posted to queue. This is because
     * RM processes all the pending queue messages before aplying force switch
     * over
     */

    RM_UNLOCK ();
    if (RM_SEND_EVENT (RM_TASK_ID, RM_DYNAMIC_AUDIT_PROCESSED) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmReleaseMemoryForMsg
 * Input(s)           : u4SrcEntId.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine used by the protocols to release the memory
 *                      allocated for sending the Standby node count
 ******************************************************************************/
UINT4
RmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    /* Free the memory of this entry */
    if (MemReleaseMemBlock (gRmInfo.RmMsgPoolId, pu1Block) != MEM_SUCCESS)
    {
        RM_TRC1 (RM_CRITICAL_TRC,
                 "Mem block release failed for blck %u\n", pu1Block);
        return RM_FAILURE;
    }
    pu1Block = NULL;
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmGetPeerNodeCount
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up.
 *                      The function name should be RmGetPeerNodeCount() 
 *                      instead of RmGetStandbyNodeCount().  But since this  
 *                      is an exported API, the function name is not changed. 
 ******************************************************************************/
UINT1
RmGetPeerNodeCount (VOID)
{
    return RM_PEER_NODE_COUNT ();
}

/******************************************************************************
 * Function           : RmApiSetIssuPeerNodeCount
 * Input(s)           : Number of ISSU peer nodes up.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to set the number of issu peer nodes that are up. 
 ******************************************************************************/
VOID 
RmApiSetIssuPeerNodeCount(UINT1 u1PeerCount)
{
    RM_LOCK ();
    RM_ISSU_PEER_NODE_COUNT() = u1PeerCount;
    RM_UNLOCK (); 
}

/******************************************************************************
 * Function           : RmGetStandbyNodeCount
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up.
 *                      The function name should be RmGetPeerNodeCount() 
 *                      instead of RmGetStandbyNodeCount().  But since this  
 *                      is an exported API, the function name is not changed. 
 ******************************************************************************/
UINT1
RmGetStandbyNodeCount (VOID)
{
    UINT1               u1NumPeers = 0;
    RM_LOCK ();
    u1NumPeers = RM_PEER_NODE_COUNT ();
    RM_UNLOCK ();
    return (u1NumPeers);
}

/******************************************************************************
 * Function           : RmGetIssuPeerNodeCount 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up 
                        during ISSU.
 ******************************************************************************/
UINT1
RmGetIssuPeerNodeCount (VOID)
{
    UINT1               u1NumOfIssuPeers = 0;
    RM_LOCK ();
    u1NumOfIssuPeers = RM_ISSU_PEER_NODE_COUNT ();
    RM_UNLOCK ();
    return (u1NumOfIssuPeers);
}

/******************************************************************************
 * Function           : RmGetStaticConfigStatus
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_STATIC_CONFIG_RESTORED/RM_STATIC_CONFIG_IN_PROGRESS/
 *                      RM_STATIC_CONFIG_NOT_RESTORED
 * Action             : Routine to get the status of static configuration
 *                      restoration.
 ******************************************************************************/
UINT1
RmGetStaticConfigStatus (VOID)
{
    UINT1               u1StaticConfigStatus = 0;
    RM_LOCK ();
    u1StaticConfigStatus = RM_STATIC_CONFIG_STATUS ();
    RM_UNLOCK ();
    return (u1StaticConfigStatus);
}

/******************************************************************************
 * Function           : RmSetStaticConfigStatus
 * Input(s)           : u1Status    -   Status of the flag
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to set the static configuration restoration
 *                      status.
 ******************************************************************************/
VOID
RmSetStaticConfigStatus (UINT1 u1Status)
{
    RM_LOCK ();
    RM_STATIC_CONFIG_STATUS () = u1Status;

    RM_TRC1 (RM_EVENT_TRC, "RmSetStaticConfigStatus: "
             "Set RM_STATIC_CONFIG_STATUS = %d\n", u1Status);

    if ((RM_GET_NODE_STATE () == RM_STANDBY) &&
        (u1Status == RM_STATIC_CONFIG_IN_PROGRESS))
    {
        RM_TRC (RM_SYNCUP_TRC, "RmSetStaticConfigStatus: " "LOCKING Rx Buff\n");
        RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_FALSE;
    }
    RM_UNLOCK ();

    return;
}

/******************************************************************************
 * Function           : RmGetForceSwitchOverFlag
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Force switchover flag status.
 * Action             : Routine to get the force switchover flag
 ******************************************************************************/
UINT1
RmGetForceSwitchOverFlag (VOID)
{
    UINT1               u1ForceSwitchOverFlag = 0;
    RM_LOCK ();
    u1ForceSwitchOverFlag = RM_FORCE_SWITCHOVER_FLAG ();
    RM_UNLOCK ();
    return u1ForceSwitchOverFlag;
}

/******************************************************************************
 * Function           : RmSetForceSwitchOverFlag
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Force switchover flag status.
 * Action             : Routine to Set the force switchover flag
 ******************************************************************************/
VOID
RmSetForceSwitchOverFlag (UINT1 u1Flag)
{
    RM_LOCK ();
    RM_FORCE_SWITCHOVER_FLAG () = u1Flag;
    RM_UNLOCK ();
}

/******************************************************************************
 * Function           : RmApiHandleProtocolEvent
 * Input(s)           : pEvt->u4AppId - Module Id.
 *                      pEvt->u4Event   - Event send by protocols
 *                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /
 *                      RM_INITIATE_BULK_UPDATE / RM_BULK_UPDT_ABORT / 
 *                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / 
 *                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED / 
 *                      RM_STANDBY_EVT_PROCESSED)
 *                      pEvt->u4Error   - Error code (RM_MEMALLOC_FAIL / 
 *                      RM_SENDTO_FAIL / RM_PROCESS_FAIL / RM_NONE)
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by protocols/applications to intimate RM 
 *                      about the protocol operations. RM decides upon the 
 *                      events and send appropriate notifications to the 
 *                      management application or events to the protocols. 
 ******************************************************************************/
UINT1
RmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    UINT1               u1RetVal = RM_SUCCESS;
    RM_WR_LOCK ();
    u1RetVal = RmHandleProtocolEvent (pEvt);
    RM_WR_UNLOCK ();
    return u1RetVal;
}

/******************************************************************************
 * Function           : RmSendEventToAppln
 * Input(s)           : APP_ID of the application which calls this API.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send Event to all Registered 
 *                      Application excet the called one.
 ******************************************************************************/
VOID
RmSendEventToAppln (UINT4 u4AppId)
{
    UINT4               u4EntId;

    /* MBSM processed GO_ACTIVE during standby to active transition.
     * Now GO_ACTIVE should be given to all protocols*/

    /* VCM processed GO_STANDBY during Idle to Standby transition.
     * Now GO_STANDBY should be given to all protocols*/
    RM_LOCK ();
    if (u4AppId == RM_MBSM_APP_ID)
    {
        RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
    }
    for (u4EntId = 0; u4EntId < RM_MAX_APPS; u4EntId++)
    {
        if ((u4EntId == u4AppId) ||
            ((u4EntId == RM_VCM_APP_ID)
             && (RM_GET_NODE_STATE () == RM_STANDBY)))

        {
            continue;
        }

        RmUtilPostDataOrEvntToProtocol (u4EntId,
                                        (UINT1) RM_GET_NODE_STATE (),
                                        NULL, 0, 0);
    }

    if ((u4AppId == RM_MBSM_APP_ID) && (RM_GET_NODE_STATE () == RM_ACTIVE))
    {
        /* u4AppId will be MBSM in case of GO_ACTIVE event processing. */
        /* TxEnabled is set to true as h/w attach is completed. */
        /* Meanwhile RM will change the node state to STANDBY and disables the
         * Tx Path. So lock is taken.*/

        RmSetTxEnable (RM_TRUE);
        /* Send the pending traps */
        RmTrapSendNotifications (NULL);
    }
    RM_UNLOCK ();
}

/******************************************************************************
 * Function           : RmSendEventToRmTask
 * Input(s)           : au1TakName - Name of the task to send the event
 * Output(s)          : u4Event - Event to be send
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Other modules should call this routine to
 *                      post any event to RM task.
 ******************************************************************************/

UINT4
RmSendEventToRmTask (UINT4 u4Event)
{
    if (RM_TASK_ID == RM_INIT)
    {
        if (RM_GET_TASK_ID (RM_SELF, (const UINT1 *) RM_TASK_NAME,
                            &RM_TASK_ID) != OSIX_SUCCESS)
        {

            RM_TRC (RM_FAILURE_TRC, "Failed to get RM Task ID\n");
            return RM_FAILURE;
        }
    }

    if (RM_SEND_EVENT (RM_TASK_ID, u4Event) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Failed to Send Event to RM task \n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmSetBulkUpdatesStatus                               */
/*                                                                           */
/* Description        : This function is triggered by the registered         */
/*                      protocols to inform the status of bulk updates       */
/*                      completion. This function will be used only the      */
/*                      protocols in the active node                         */
/*                                                                           */
/* Input(s)           : u4AppId - Protocol Application ID                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS or RM_FAILURE                             */
/*                                                                           */
/* Called By          : Registered protocols with RMGR                       */
/*****************************************************************************/
INT4
RmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    if ((u4AppId == RM_APP_ID) || (u4AppId >= RM_MAX_APPS))
    {
        return RM_FAILURE;
    }

    RM_LOCK ();

    OSIX_BITLIST_SET_BIT (gRmInfo.RmBulkUpdSts, u4AppId, sizeof (tRmAppList));

    RM_UNLOCK ();
    RmUtilCheckAndCompleteBulkUpdt ();
    RM_TRC1 (RM_CTRL_PATH_TRC | RM_EVENT_TRC | RM_SYNCUP_TRC,
             "RmSetBulkUpdatesStatus: "
             "Dynamic Bulk Update is completed for AppId=%s\n",
             gapc1AppName[u4AppId]);

    RM_TRC1 (RM_CRITICAL_TRC,
             "Dynamic Bulk Update completed "
             "for %s module \r\n", gapc1AppName[u4AppId]);

    if (u4AppId == (RM_MAX_APPS - 1))
    {
        RM_RESET_FLAGS (HB_FORCE_SWITCHOVER);
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmGetBulkUpdatesStatus                               */
/*                                                                           */
/* Description        : This function will be used by the registered         */
/*                      protocols in active node to check whether bulk       */
/*                      updates mechanism is completed by that protocol.     */
/*                                                                           */
/* Input(s)           : u4AppId - Protocol Application ID                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS or RM_FAILURE                             */
/*                                                                           */
/* Called By          : Registered protocols with RMGR                       */
/*****************************************************************************/
INT4
RmGetBulkUpdatesStatus (UINT4 u4AppId)
{
    BOOL1               bResult = OSIX_FALSE;

    if ((u4AppId == RM_APP_ID) || (u4AppId >= RM_MAX_APPS))
    {
        return RM_FAILURE;
    }

    RM_LOCK ();

    OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdStsMask, u4AppId,
                             sizeof (tRmAppList), bResult);
    if (bResult == OSIX_FALSE)
    {
        /* Bulk updates mechanism are not required for this protocol.
         * So we can treat it as successful completion.
         */
        RM_UNLOCK ();
        return RM_SUCCESS;
    }

    OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdSts, u4AppId,
                             sizeof (tRmAppList), bResult);
    if (bResult == OSIX_FALSE)
    {
        RM_UNLOCK ();
        return RM_FAILURE;
    }

    RM_UNLOCK ();
    return RM_SUCCESS;
}

/***************************************************************/
/*  Function Name   : RmValidateRmIfName                       */
/*                                                             */
/*  Description     : This function validates whether the given*/
/*                    string represents a valid interface of   */
/*                    the system.                              */
/*                                                             */
/*  Input(s)        : pu1IfName - String to  be passed.  */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Returns         : RM_SUCCESS  or RM_FAILURE                */
/***************************************************************/
#ifdef L2RED_WANTED
#ifdef BSDCOMP_SLI_WANTED
UINT4
RmValidateRmIfName (UINT1 *pu1IfName)
{
    tNodeInfo           NodeInfo;
    return (RmGetIfIpInfo (pu1IfName, &NodeInfo));
}
#endif
#endif
/*****************************************************************************/
/* Function Name      : RmDisableDynamicSyncup                               */
/*                                                                           */
/* Description        : This function is used to disable sending dynamic     */
/*                      sync up message to peer RM when reload command       */
/*                      is executed.This function is called only from        */
/*                      isssys.c when reload is given.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmDisableDynamicSyncup ()
{
    /* Set the flag to FALSE to disable dynamic sync-up. */
    RM_LOCK ();
    gRmInfo.u4DynamicSyncStatus = RM_FALSE;
    RM_UNLOCK ();
}

/******************************************************************************
 * Function           : RmRetrieveNodeState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Node state of RM.
 * Action             : Routine to get the node state of RM.
 *             This API is used to get RM node state without 
 *             invoking RM LOCK.
 ******************************************************************************/
UINT4
RmRetrieveNodeState (VOID)
{
    return RM_GET_NODE_STATE ();
}

/******************************************************************************
 * Function           : RmGetNodeState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Node state of RM.
 * Action             : Routine to get the node state of RM.
 ******************************************************************************/
UINT4
RmGetNodeState (VOID)
{
    UINT4               u4RmNodeState;

    RM_LOCK ();
    u4RmNodeState = RM_GET_NODE_STATE ();
    RM_UNLOCK ();
    return (u4RmNodeState);
}

/******************************************************************************
 * Function           : RmGetNodePrevState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Previous Node state of RM.
 * Action             : Routine to get the previous node state of RM.
 ******************************************************************************/
UINT4
RmGetNodePrevState (VOID)
{
    UINT4               u4RmNodeState;

    RM_LOCK ();
    u4RmNodeState = RM_GET_PREV_NODE_STATE ();
    RM_UNLOCK ();
    return (u4RmNodeState);
}

/******************************************************************************
 * Function           : RmApiGetSeqNumForSyncMsg           
 * Input(s)           : None.                             
 * Output(s)          : pu4SeqNum - return value for the next valid 
 *                                  sequence number. In every call of this API
 *                                  will reserve a unique sequence number for
 *                                  the sync-up message and provide it to 
 *                                  protocol.
 * Returns            : None.
 * Action             : This function used by protocols/applications to 
 *                      reserves  a unique sequence number for the sync-up 
 *                      message. Once a sequence number is reserved protocol
 *                      can form the sync-up message with this number and
 *                      transfer it to standby node through RM. Once a sequence
 *                      number is reserved it cannot be de-allocated. So 
 *                      protocol should send a sync-up message with the
 *                      allocated sequence number. It can send a dummy 
 *                      sync-up message if required in case it is not able to
 *                      send a valid sync-up message.
 ******************************************************************************/
VOID
RmApiGetSeqNumForSyncMsg (UINT4 *pu4SeqNum)
{

    *pu4SeqNum = 0;                /* If node status is not ACTIVE return seq number 0 */

    /* Sync-up for the nmhSet routines trigerred by clear config
     * operation need not be sent to standby node as the 
     * nmhSet routine of clear config is allowed in standby.
     * Hence sequence number is returned as 0 for this case*/
    if (IssIsClearConfigInProgress () == ISS_TRUE)
    {
        return;
    }
    RM_LOCK ();
    if (gRmInfo.bIsTxBuffFull == RM_TRUE)
    {
        RM_TRC1 (RM_CRITICAL_TRC, "RmApiGetSeqNumForSyncMsg: Buffer allocation "
                 "for sync msg Tx is not allowed because RM Tx is not ready "
                 "and Tx buffer threshold (%d * 0.75) is reached \r\n",
                 MAX_RM_TX_BUF_NODES);
        RmHandleSystemRecovery (RM_TX_FAILED_AND_TX_BUFF_FULL);
        RM_UNLOCK ();
        return;
    }
    if ((RM_GET_NODE_STATE () == RM_ACTIVE) &&
        (RM_PEER_NODE_COUNT () != 0) &&
        (RM_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_NOT_RESTORED))
    {
        *pu4SeqNum = RM_RX_GETNEXT_VALID_SEQNUM ();
        RM_RX_INCR_LAST_SEQNUM_PROCESSED ();
        RM_TRC1 (RM_SYNCUP_TRC,
                 "RmApiGetSeqNumForSyncMsg: Seq# %d is reserved\n", *pu4SeqNum);

        if (*pu4SeqNum == RM_SEQNUM_WRAP_AROUND_VALUE)
        {
            RM_TRC (RM_SYNCUP_TRC, "RmEnqMsgToRmFromAppl: WRAP AROUND happened "
                    "for sequence numbers\n");
        }
#ifdef L2RED_TEST_WANTED
        if (RM_IS_SEQ_MISS_ENABLED () == RM_TRUE)
        {
            RM_RX_INCR_LAST_SEQNUM_PROCESSED ();
        }
#endif
    }

    RM_UNLOCK ();
}

/******************************************************************************
 * Function           : RmApiSetNoLock
 * Input(s)           : None
 *               
 * Output(s)          : None.
 * Returns            : None
 * Action             : This function will set the global variable to true.
 *                      that variable is checked in the RmApiSendProtoAckToRM.
 *                      If the variable is true, then the RM lock will not be 
 *                      taken, else, RM lock will be taken in that scenario.
 ******************************************************************************/
VOID
RmApiSetNoLock ()
{
    gu1RmLockVariable = OSIX_TRUE;
}

/******************************************************************************
 * Function           : RmApiSetLock
 * Input(s)           : None
 *               
 * Output(s)          : None.
 * Returns            : None
 * Action             : This function will set the global variable to false.
 *                      that variable is checked in the RmApiSendProtoAckToRM.
 *                      If the variable is true, then the RM lock will not be 
 *                      taken, else, RM lock will be taken in that scenario.
 ******************************************************************************/
VOID
RmApiSetLock ()
{
    gu1RmLockVariable = OSIX_FALSE;
}

/******************************************************************************
 * Function           : RmApiSendProtoAckToRM           
 * Input(s)           : tRmProtoAck contains
 *                       u4AppId  - Protocol Identifier
 *                       u4SeqNum - Sequence number of the RM message for
 *                                  which this ACK is generated.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This is the function used by protocols/applications 
 *                      to send acknowledgement to RM after processing the
 *                      sync-up message.
 ******************************************************************************/
INT4
RmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    tRmCtrlQMsg        *pRmCtrlQMsg = NULL;

    RM_WR_LOCK ();
#ifdef L2RED_TEST_WANTED
    if (RM_IS_PROTO_ACK_ENABLED () == RM_FALSE)
    {
        /* Acknowledgement is disabled for testing purpose */
        RM_WR_UNLOCK ();
        return RM_SUCCESS;
    }
#endif
    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        RM_TRC2 (RM_FAILURE_TRC,
                 "RmApiSendProtoAckToRM: RM Receives Proto ACK at ACTIVE side "
                 "[AppId=%s, Seq# %d]. Ignoring it.\n",
                 gapc1AppName[pProtoAck->u4AppId], pProtoAck->u4SeqNumber);
        RM_WR_UNLOCK ();
        return RM_FAILURE;
    }

    RM_WR_UNLOCK ();

    if (pProtoAck->u4SeqNumber == 0)
    {
        RM_TRC1 (RM_FAILURE_TRC,
                 "RmApiSendProtoAckToRM: RM Receives Proto ACK from "
                 "[AppId=%s] with Seq#=0. Ignoring it\n",
                 gapc1AppName[pProtoAck->u4AppId]);
        return RM_SUCCESS;
    }

    RM_TRC2 (RM_SYNCUP_TRC | RM_BUFF_TRACE, "RmApiSendProtoAckToRM: "
             "Protocol=%s processed the seq# %d message\n",
             gapc1AppName[pProtoAck->u4AppId], pProtoAck->u4SeqNumber);

    if ((pRmCtrlQMsg = (tRmCtrlQMsg *) (MemAllocMemBlk
                                        (gRmInfo.RmCtrlMsgPoolId))) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "RmApiSendProtoAckToRM: Memory allocation for "
                "Control Queue message.\n");
        return RM_FAILURE;
    }

    pRmCtrlQMsg->u4MsgType = RM_PROTO_ACK_MSG;
    pRmCtrlQMsg->ProtoAckCtrlMsg.u4AppId = pProtoAck->u4AppId;
    pRmCtrlQMsg->ProtoAckCtrlMsg.u4SeqNumber = pProtoAck->u4SeqNumber;

    if (RmQueEnqCtrlMsg (pRmCtrlQMsg) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmApiHandlePeerInfo: Send to Control Q failed\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmApiSyncStaticConfig
 * Input(s)           : pRmNotifConfChg - pointer to the configuration related
 *                                        information
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : The Management modules invokes this function to post 
 *                      the configuration information to the RM task.
 ******************************************************************************/
INT4
RmApiSyncStaticConfig (tRmNotifConfChg * pRmNotifConfChg)
{
    tRmPktQMsg         *pRmPktQMg = NULL;

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmApiSyncStaticConfig "
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_STATIC_CONF_INFO;
    pRmPktQMg->ConfInfo.pMultiIndex = pRmNotifConfChg->pMultiIndex;
    pRmPktQMg->ConfInfo.pMultiData = pRmNotifConfChg->pMultiData;
    pRmPktQMg->ConfInfo.u4SeqNo = pRmNotifConfChg->u4SeqNo;
    pRmPktQMg->ConfInfo.i4OidLen = pRmNotifConfChg->i4OidLen;
    pRmPktQMg->ConfInfo.pu1ObjectId = pRmNotifConfChg->pu1ObjectId;
    pRmPktQMg->ConfInfo.i1ConfigSetStatus = pRmNotifConfChg->i1ConfigSetStatus;
    pRmPktQMg->ConfInfo.i1MsgType = pRmNotifConfChg->i1MsgType;

#ifdef L2RED_TEST_WANTED
    if (RM_TEST_IS_PROTO_SYNCUP_BLOCKED (RM_STATIC_CONF_APP_ID) == RM_TRUE)
    {
        /* This is done to avoid the sync-up of static configuration with
         * the standby node to test the Hardware audit functionality*/
        RM_TRC1 (RM_CRITICAL_TRC, "RmEnqMsgToRmFromAppl: "
                 "[TEST] Sync-up is blocked for AppId=%s\n",
                 gapc1AppName[RM_STATIC_CONF_APP_ID]);
        pRmPktQMg->ConfInfo.i1ConfigSetStatus = SNMP_FAILURE;
    }
#endif

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        RmUtilReleaseMemForPktQMsg (pRmPktQMg);
        RM_TRC (RM_CRITICAL_TRC, "SendToQ failed in"
                " RmApiSyncStaticConfig\n");
        return RM_FAILURE;
    }

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "SendEvent failed in "
                "RmApiSyncStaticConfig\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/* API to get the Active Node ID */
/******************************************************************************
 * Function           : RmGetActiveNodeId
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Active node Id
 * Action             : This API returns the active node Id
 ******************************************************************************/
UINT4
RmGetActiveNodeId (VOID)
{
    UINT4               u4ActiveNode = 0;

    RM_LOCK ();
    u4ActiveNode = RM_GET_ACTIVE_NODE_ID ();
    RM_UNLOCK ();
    return (u4ActiveNode);
}

/******************************************************************************
 * Function           : RmConfAddToFailBuff
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Adds the oid and value to failure buffer
 *                      The contents of the failure buffer will be processed
 *                      after completing the dynamic bulk updates
 * Action             : RM_SUCCESS / RM_FAILURE
 ******************************************************************************/
INT4
RmApiConfAddToFailBuff (tSNMP_OID_TYPE * pOID, tSNMP_MULTI_DATA_TYPE * pVal)
{
    INT4                i4RetVal = RM_SUCCESS;

    RM_STAT_CONF_LOCK ();
    if (gRmConfInfo.bBulkUpdStatus == RM_BULK_UPDATE_NOT_COMPLETED)
    {
        i4RetVal = RmConfAddToFailBuff (pOID, pVal);
    }
    RM_STAT_CONF_UNLOCK ();
    return (i4RetVal);
}

/******************************************************************************
 * Function           : RmApiConfIsOIDInFilterList 
 * Input(s)           : pOID  - Object identifier 
 * Output(s)          : None 
 * Returns            : OSIX_TRUE / OSIX_FALSE 
 * Action             : This API is called from the MSR module during static  
 *                      configuration restoration.                            
 *                      Configurations like specific to RM 
 *                      need not be applied on the Standby node. The OIDs 
 *                      corresponding to these mib-objects are maintained in a
 *                      list in rmconf.c. 
 *                      It returns OSIX_TRUE if there is a match, else 
 *                      OSIX_FALSE is returned.
 ******************************************************************************/
INT1
RmApiConfIsOIDInFilterList (tSNMP_OID_TYPE * pOID)
{
    return RmConfIsOIDInFilterList (pOID);
}

/******************************************************************************
 * Function Name      : RmApiTrapSendNotifications
 *
 * Description        : This is the API function used by external modules
                        to send notifications through RM module.
 * Input(s)           : pNotifyInfo - Pointer to the tRmNotificationMsg
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
RmApiTrapSendNotifications (tRmNotificationMsg * pNotifyInfo)
{
    RM_LOCK ();
    RmTrapSendNotifications (pNotifyInfo);
    RM_UNLOCK ();
}

/******************************************************************************
 * Function           : RmApiSystemRestart
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine restart the system.
 ******************************************************************************/
VOID
RmApiSystemRestart (VOID)
{
    RmSystemRestart ();
}

/******************************************************************************
 * Function           : RmApiAllProtocolRestart
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This API restarts all the protocols registered with RM.
 ******************************************************************************/
UINT4
RmApiAllProtocolRestart (VOID)
{
    if (RM_SEND_EVENT (RM_TASK_ID, RM_RESTART_PROTOCOLS) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "SendEvent failed in "
                "mApiProtocolRestart\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmApiInitiateBulkUpdate
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This API initiates the bulk update process in the 
 *                       standby node to obtain the configurations from the 
 *                       active node after all the protocols have restarted.
 ******************************************************************************/
UINT4
RmApiInitiateBulkRequest (VOID)
{
    if (RM_SEND_EVENT (RM_TASK_ID, RM_INIT_BULK_REQUEST) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "SendEvent failed in "
                "RmApiInitiateBulkUpdate\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/******************************************************************************
 * Function Name      : RmApiAllocTxBuf
 *
 * Description        : This is the API function used by external modules
                        to allocate memory for sync message
 * Input(s)           : sync message size
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Sync message buffer
 *****************************************************************************/

tRmMsg             *
RmApiAllocTxBuf (UINT4 u4SyncMsgSize)
{
    tRmMsg             *pMsg = NULL;

    RM_LOCK ();
    pMsg = RmAllocTxBuf (u4SyncMsgSize);
    RM_UNLOCK ();
    return pMsg;
}

/******************************************************************************
 * Function Name      : RmApiIncrementRebootCntr
 *
 * Description        : This API is called to increment the reboot counter 
 *                       when the active node initiates a software reboot
 *                       of the standby node.
 * Input(s)           : None.                
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None                   
 *****************************************************************************/
VOID
RmApiIncrementRebootCntr (VOID)
{
    RM_LOCK ();

    if (gRmInfo.u1RestartRetryCnt != 0)
    {
        gRmInfo.u1RestartCntr++;
    }
    RM_UNLOCK ();
}

/* External APIs */
/******************************************************************************
 * Function           : RmExtRmSetPeerNodeStatus
 * Input(s)           : u4PeerNodeId - Peer IP address
                        u4NodeStatus (RM_STANDBY_UP/ RM_STANDBY_DOWN)
 * Output(s)          : None 
 * Returns            : None
 * Action             : Routine to set the status of peer node in both active
 *                      & standby node.
 *******************************************************************************/
VOID
RmExtRmSetPeerNodeStatus (UINT4 u4PeerNodeId, UINT4 u4NodeStatus)
{
    tRmHbMsg            RmHbMsg;
    /* RM_STANDBY_UP <=> PEER_UP & RM_STANDBY_DOWN <=> PEER_DOWN.
     * These indications should be given to both Active & standby node's RM
     */
    if ((u4NodeStatus != RM_PEER_UP) && (u4NodeStatus != RM_PEER_DOWN))
    {
        RM_TRC (RM_FAILURE_TRC,
                "!!! Invalid standby node's status sent by "
                "external RM module !!!\n");
        return;
    }
    RmHbMsg.u4PeerAddr = u4PeerNodeId;
    RmHbMsg.u4Evt = u4NodeStatus;
    RmApiSendHbEvtToRm (&RmHbMsg);
}

/******************************************************************************
 * Function           : RmExtRmSetNodeState
 * Input(s)           : u4RmNodeState - Node State (RM_ACTIVE/RM_STANDBY)
 * Output(s)          : None
 * Returns            : None 
 * Action             : External RM module should call this routine to
 *                      update the node state of Aricent RM.
 ******************************************************************************/
VOID
RmExtRmSetNodeState (UINT4 u4RmNodeState)
{
    tRmHbMsg            RmHbMsg;

    if ((u4RmNodeState != GO_ACTIVE) && (u4RmNodeState != GO_STANDBY))
    {
        RM_TRC (RM_FAILURE_TRC,
                "!!! Invalid Node state sent by external RM module !!!\n");
        return;
    }
    /* It is assumed that, the external Redunadancy Manager will indicate the
     * peer node id, before sending GO_STANDBY.
     */
    RmHbMsg.u4Evt = u4RmNodeState;
    RmApiSendHbEvtToRm (&RmHbMsg);
}

/******************************************************************************
 * Function           : RmExtRmSetForceSwitchOverFlag
 * Input(s)           : u1Flag - (RM_FSW_NOT_OCCURED/RM_FSW_OCCURED)
 * Output(s)          : None
 * Returns            : None
 * Action             : External RM module should call this routine 
                        for switchover trigger.
 ******************************************************************************/
VOID
RmExtRmSetForceSwitchOverFlag (UINT1 u1Flag)
{
    tRmProtoEvt         ProtoEvt;

    if ((u1Flag != RM_FSW_NOT_OCCURED) && (u1Flag != RM_FSW_OCCURED))
    {
        RM_TRC (RM_FAILURE_TRC,
                "!!! Invalid flag sent by external RM module !!!\n");
        return;
    }

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
    ProtoEvt.u4SendEvent = RM_TRIG_FSW_FROM_MGMT;

    if (RmApiHandleProtocolEvent (&ProtoEvt) != RM_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC,
                "!!! Force switch over flag send event from external "
                "RM module failed !!!\n");
    }
}

/* HITLESS RESTART */
/******************************************************************************
 * Function           : RmGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/
UINT1
RmGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;

    RM_LOCK ();
    u1HRFlag = RM_HR_GET_STATUS ();
    RM_UNLOCK ();

    return (u1HRFlag);
}

/******************************************************************************
 * Function           : RmIsBulkUpdateComplete
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : This API returns
 *                         RM_SUCCESS -  Bulk update Completed
 *                         RM_FAILURE -  Bulk update Not Completed
 ******************************************************************************/
INT4
RmIsBulkUpdateComplete (VOID)
{
    if (gu4BulkUpdateInProgress == RM_TRUE)
    {
        /* Bulk update is in progress */
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmApiTcpSrvSockInit
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to create a TCP server scoket, set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/
VOID
RmApiTcpSrvSockInit (VOID)
{
    INT4 i4SrvSockFd = 0;

    if (RmTcpSrvSockInit (&i4SrvSockFd) == RM_SUCCESS)
    {
        RM_TCP_SRV_SOCK_FD () = i4SrvSockFd;
    }

    return;
}


/******************************************************************************
 * Function           : RmEnqCertGenMsgToRmFromApp 
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to enqueue certificate generated msg 
 * 						to RM from particular module.
 ******************************************************************************/
UINT4
RmEnqCertGenMsgToRmFromApp (VOID)
{
    if (RM_SEND_EVENT (RM_TASK_ID, RM_SSL_CERT_GENERATED) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}


/******************************************************************************
 * Function           : RmSendCertGenMsgToStdBy 
 * Input(s)           : pRmMsg - msg from appl.
 *                      u2DataLen - Len of msg
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send the certificate generated msg to standby.
 ******************************************************************************/
UINT4
RmSendCertGenMsgToStdBy (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tRmPktQMsg         *pRmPktQMg = NULL;
    tRmHdr              RmHdr;
    /* Form RM header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    /* Actual checksum will be calculated after copying the
     * CRU buf to linear buf before sending to peer */
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4MsgType = OSIX_HTONL (RM_SSL_GET_CERT_REQ_MSG);

    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONL (RM_UNUSED);

    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        return RM_FAILURE;
    }

	if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_SSL_GET_CERT_REQ;
    pRmPktQMg->DynSyncMsg = pRmMsg;

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMg)
            != MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmUtilReleaseMemForPktQMsg Mem block release failed for"
                     " block %u\n", pRmPktQMg);
        }
        pRmPktQMg = NULL;
        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}
/******************************************************************************
 * Function           : RmEnqCryptSeqMsgToRmFrmApp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to enqueue crypto seq number msg
 *                      to RM from particular module.
 ******************************************************************************/
UINT4
RmEnqCryptSeqMsgToRmFrmApp (VOID)
{
    if (RM_SEND_EVENT (RM_TASK_ID, RM_V3_CRYPTSEQ_UPD) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmSendCryptSeqMsgToStdBy
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send crypto seq update msg to standby node
 ******************************************************************************/
UINT4
RmSendCryptSeqMsgToStdBy (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tRmPktQMsg         *pRmPktQMg = NULL;
    tRmHdr              RmHdr;
    /* Form RM header */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    /* Actual checksum will be calculated after copying the
     * CRU buf to linear buf before sending to peer */
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2DataLen));

    /* u4MsgType - REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4MsgType = OSIX_HTONL (RM_V3_GET_CRYPTSEQ_REQ_MSG);

    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONL (RM_UNUSED);
    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        return RM_FAILURE;
    }

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        return RM_FAILURE;
    }

    MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

    pRmPktQMg->u1MsgType = RM_V3_GET_CRYPTSEQ_REQ;
    pRmPktQMg->DynSyncMsg = pRmMsg;

    if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                          (UINT1 *) &pRmPktQMg, RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMg)
            != MEM_SUCCESS)
        {
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmUtilReleaseMemForPktQMsg Mem block release failed for"
                     " block %u\n", pRmPktQMg);
        }
        pRmPktQMg = NULL;
        RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }

    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmEnqMsgToRmFromAppl\n");
        return RM_FAILURE;
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmApiIssuMaintenanceModeEnable
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : Routine to send Issu maintenance mode enable event to
 *                      RM queue
 ******************************************************************************/
UINT4
RmApiIssuMaintenanceModeEnable (VOID)
{
   /* Post Maintenance mode enable event to RM queue */
     if (RM_SEND_EVENT (RM_TASK_ID, RM_ISSU_MAINT_MODE_EVENT) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in RmApiIssuMaintenanceModeEnable\n");
        return RM_FAILURE;
    }

   return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmApiPostPeerEventToProtocols
 * Input(s)           : u1Event - RM_PEER_UP/RM_PEER_DOWN
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to send event to protocols
 ******************************************************************************/
VOID
RmApiPostPeerEventToProtocols (UINT1 u1Event)
{
   
   RM_LOCK ();

  /* Post peer event to all protocols */
   RmUpdatePeerNodeCntToProtocols(u1Event);

   RM_UNLOCK ();
}
/******************************************************************************
 * Function           : RmApiBulkUpdtStatus
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : TRUE/FLASE.
 * Action             : Verify Bulk Sync Status
 ******************************************************************************/
UINT1 RmApiBulkUpdtStatus (VOID)
{
    if (gRmInfo.u1IsBulkUpdtInProgress == RM_TRUE)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

