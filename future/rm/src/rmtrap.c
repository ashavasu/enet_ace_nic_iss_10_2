/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: rmtrap.c,v 1.21 2014/05/23 07:55:03 siva Exp $
 *
 * Description: This file contains RM Trap related Functions
 *****************************************************************************/
#ifndef RM_TRAP_C
#define RM_TRAP_C
#include "rmincs.h"
#include "mbsm.h"
#include "fm.h"
#include "fssnmp.h"
#include "fssocket.h"
#include "snmputil.h"
#include "fsrm.h"

#define RM_TRAP_EVENT    1

extern UINT1        gu1MsrTrcFlag;

CONST CHR1         *gapc1Operation[] = { NULL, "sync up", "switchover" };

CONST CHR1         *gapc1OperationStatus[] =
    { NULL, "started", "completed", "failed" };

CONST CHR1         *gapc1Failure[] =
    { NULL, "none", "- memory allocation failed", "- send failed",
    "- process failed"
};

CONST CHR1         *gapc1NodeState[] = { "IDLE", "ACTIVE", "STANDBY" };

#ifdef SNMP_2_WANTED
static INT1         ai1TempBuffer[SNMP_MAX_OID_LENGTH + 1];
/******************************************************************************
* Function :   RmMakeObjIdFrmDotNew
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
RmMakeObjIdFrmDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; fs_rm_mib_oid_table[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (fs_rm_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (fs_rm_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         fs_rm_mib_oid_table[u2Index].pNumber,
                         STRLEN (fs_rm_mib_oid_table[u2Index].pNumber));
                ai1TempBuffer[STRLEN (fs_rm_mib_oid_table[u2Index].pNumber)] =
                    '\0';

                break;
            }
        }

        if (fs_rm_mib_oid_table[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         (u2Index < SNMP_MAX_OID_LENGTH) && (ai1TempBuffer[u2Index] != '\0');
         u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) RmParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   RmParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : pu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
RmParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}
#endif /* SNMP_2_WANTED */

/******************************************************************************
 * Function Name      : RmTrapSendNotifications 
 *
 * Description        : This is the function used by RM to send notifications. 
 *                      RM in the active node sends the notification 
 *                      directly to the management application through 
 *                      fault management. RM in standby node informs 
 *                      the notification to the active node through 
 *                      RM notification message. 
 *
 * Input(s)           : pNotifyInfo - Pointer to the tRmNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
RmTrapSendNotifications (tRmNotificationMsg * pNotifyInfo)
{
#if defined(SNMP_3_WANTED) || defined(FM_WANTED)
    tSNMP_VAR_BIND     *pStartVb = NULL;
#endif
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      RmPrefixTrapOid;
    tSNMP_OID_TYPE      RmSuffixTrapOid;
    tSNMP_OID_TYPE     *pRmTrapOid = NULL;
    UINT4               au4RmPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4RmSuffixTrapOid[] = { 99, 0 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[RM_MAX_OID_LEN];
#endif /* SNMP_3_WANTED */
    UINT4               u4IsEntryExist = RM_FALSE;
    UINT1               au1LogBuf[RM_MAX_STRING];
    UINT1              *pu1Buf;
#ifdef FM_WANTED
    tFmFaultMsg         FmMsg;

    MEMSET (&FmMsg, 0, sizeof (tFmFaultMsg));
#endif /* FM_WANTED */

#ifdef SNMP_3_WANTED
    RmPrefixTrapOid.pu4_OidList = au4RmPrefixTrapOid;
    RmPrefixTrapOid.u4_Length = sizeof (au4RmPrefixTrapOid) / sizeof (UINT4);

    RmSuffixTrapOid.pu4_OidList = au4RmSuffixTrapOid;
    RmSuffixTrapOid.u4_Length = sizeof (au4RmSuffixTrapOid) / sizeof (UINT4);

    pRmTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pRmTrapOid == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "RmTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMSET (pRmTrapOid->pu4_OidList, 0,
            ((sizeof (UINT4)) * SNMP_MAX_OID_LENGTH));
    pRmTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&RmPrefixTrapOid, &RmSuffixTrapOid,
                              pRmTrapOid) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "RmTrapSendNotifications: Adding EnterpriseOid failed \n");
        SNMP_FreeOid (pRmTrapOid);
        return;
    }
#endif /* SNMP_3_WANTED */

    /* STANDBY node cannot send the notification. Forward the information
     * to ACTIVE node */

    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        if (RmTrapSendNotificationMsgToActive (pNotifyInfo) != RM_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Send to active failed \n");
        }
#ifdef SNMP_3_WANTED
        SNMP_FreeOid (pRmTrapOid);
#endif
        return;
    }

    /* Send all the notifications stored in the array, if it is an active node. */
    do
    {

        if (pNotifyInfo == NULL)
        {
            /* Send the pending traps. */
            continue;
        }

        /* Store notification information in an array until Tx is enabled */
        if (gRmInfo.u4RmTxEnabled == RM_FALSE)
        {
            /* Assumption that transmission is not enabled. So store it */
            if (RmUtilStoreInArray (pNotifyInfo) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                        "RmTrapSendNotifications: Storing in array failed \n");
            }
#ifdef SNMP_3_WANTED
            SNMP_FreeOid (pRmTrapOid);
#endif
            return;
        }

        MEMSET (au1LogBuf, 0, sizeof (au1LogBuf));

        RM_TRC (RM_NOTIF_TRC | RM_SNMP_TRC, "RM sending notification !!!\n");

#ifdef SNMP_3_WANTED
        MEMSET (au1Buf, 0, sizeof (au1Buf));

        pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

        if (pEnterpriseOid == NULL)
        {
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Memory Allocation Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        SnmpCnt64Type.msn = 0;
        SnmpCnt64Type.lsn = 0;

        MEMCPY (pEnterpriseOid->pu4_OidList, pRmTrapOid->pu4_OidList,
                pRmTrapOid->u4_Length * sizeof (UINT4));
        pEnterpriseOid->u4_Length = pRmTrapOid->u4_Length;
        pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
            RM_TRAP_EVENT;

        pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
        if (pSnmpTrapOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Memory Allocation Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }
        MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
                sizeof (au4SnmpTrapOid));
        pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

        pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                        SNMP_DATA_TYPE_OBJECT_ID,
                                        0, 0, NULL,
                                        pEnterpriseOid, SnmpCnt64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pStartVb = pVbList;

        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmSelfNodeId");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        MEMSET (au1Buf, 0, sizeof (au1Buf));
        pNotifyInfo->u4NodeId = OSIX_NTOHL (pNotifyInfo->u4NodeId);
        INET_NTOP (AF_INET, &pNotifyInfo->u4NodeId, au1Buf, INET_ADDRSTRLEN);
        pOstring = SNMP_AGT_FormOctetString (au1Buf, (INT4) STRLEN (au1Buf));
        if (pOstring == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Form Octet string Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_OCTET_PRIM,
                                                     0, 0, pOstring, NULL,
                                                     SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_AGT_FreeOctetString (pOstring);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmNodeState");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER, 0,
                                                      (INT4) pNotifyInfo->
                                                      u4State, NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapModuleId");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pOstring = SNMP_AGT_FormOctetString
            ((UINT1 *) gapc1AppName[pNotifyInfo->u4AppId],
             (INT4) STRLEN (gapc1AppName[pNotifyInfo->u4AppId]));
        if (pOstring == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Form Octet string Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_OCTET_PRIM,
                                                     0, 0, pOstring, NULL,
                                                     SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_AGT_FreeOctetString (pOstring);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapOperation");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind
            (pOid, SNMP_DATA_TYPE_INTEGER, 0,
             (INT4) pNotifyInfo->u4Operation, NULL, NULL, SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapOperationStatus");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER, 0,
                                                      (INT4) pNotifyInfo->
                                                      u4Status, NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapError");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER, 0,
                                                      (INT4) pNotifyInfo->
                                                      u4Error, NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapEventTime");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pNotifyInfo->ac1DateTime,
                                             (INT4) RM_DATE_TIME_STR_LEN);
        if (pOstring == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Form Octet string Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0, pOstring,
                                                      NULL, SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_AGT_FreeOctetString (pOstring);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapErrorStr");

        pOid = RmMakeObjIdFrmDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: OID Not Found\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pNotifyInfo->ac1ErrorStr,
                                             (INT4) RM_ERROR_STR_LEN);
        if (pOstring == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Form Octet string Failed\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0, pOstring,
                                                      NULL, SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_AGT_FreeOctetString (pOstring);
            SNMP_FreeOid (pOid);
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapSendNotifications: Variable Binding Failed\r\n");
            SNMP_FreeOid (pRmTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
#endif /* SNMP_3_WANTED */

        /* Form syslog msg */
        INET_NTOP (AF_INET, &pNotifyInfo->u4NodeId, au1LogBuf, INET_ADDRSTRLEN);
        SNPRINTF ((CHR1 *) au1LogBuf, (sizeof (au1LogBuf)),
                  "%s %s : %s %s %s %s :%s: %s", au1LogBuf,
                  gapc1AppName[pNotifyInfo->u4AppId],
                  gapc1NodeState[pNotifyInfo->u4State],
                  gapc1Operation[pNotifyInfo->u4Operation],
                  gapc1OperationStatus[pNotifyInfo->u4Status],
                  gapc1Failure[pNotifyInfo->u4Error], pNotifyInfo->ac1ErrorStr,
                  pNotifyInfo->ac1DateTime);

#ifdef FM_WANTED
        FmMsg.pTrapMsg = pStartVb;
        FmMsg.pSyslogMsg = au1LogBuf;
        FmMsg.u4ModuleId = FM_NOTIFY_MOD_ID_RM;
        /* Call FmApiNotifyFaults */
        RM_UNLOCK ();
        FmApiNotifyFaults (&FmMsg);
        RM_LOCK ();
#endif /* FM_WANTED */

        if (u4IsEntryExist == RM_TRUE)
        {
            pu1Buf = (UINT1 *) pNotifyInfo;
            RmUtilReleaseMem (gRmInfo.RmNotifMsgPoolId, pu1Buf);
        }

    }
    while (RmUtilGetPendingNotificationEntry (&pNotifyInfo, &u4IsEntryExist)
           != RM_SUCCESS);

#ifdef SNMP_3_WANTED
    SNMP_FreeOid (pRmTrapOid);
#endif
    return;
}

/******************************************************************************
 * Function Name      : RmTrapSendNotificationMsgToActive
 *
 * Description        : Routine to send from notification information to
 *                      ACTIVE node.
 *
 * Input(s)           : pNotifyInfo - Pointer to the tRmNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RM_SUCCESS / RM_FAILURE
 *****************************************************************************/
INT4
RmTrapSendNotificationMsgToActive (tRmNotificationMsg * pNotifyInfo)
{
    tRmNotificationMsg  NotifInfo;
    tRmMsg             *pRmMsg;
    tRmPktQMsg         *pRmPktQMg = NULL;
    UINT4               u4RetVal = RM_SUCCESS;
    UINT4               u4MsgType = 0;

    if (pNotifyInfo == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC,
                "RmTrapSendNotificationMsgToActive:" "pNotifyInfo is NULL\n");
        return (RM_FAILURE);
    }

    RM_TRC (RM_SOCK_TRC | RM_NOTIF_TRC,
            "RM sending trap message to ACTIVE !!!\n");
    /* Allocate memory for RM msg. */
    if ((pRmMsg = RmAllocTxBuf
         ((sizeof (tRmNotificationMsg) + sizeof (UINT4)))) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC, "RM alloc failed !!!\n");
        return (RM_FAILURE);
    }

    /* Form Notification inforamtion msg */
    MEMSET (&NotifInfo, 0, sizeof (tRmNotificationMsg));

    NotifInfo.u4NodeId = OSIX_HTONL (pNotifyInfo->u4NodeId);
    NotifInfo.u4State = OSIX_HTONL (pNotifyInfo->u4State);
    NotifInfo.u4AppId = OSIX_HTONL (pNotifyInfo->u4AppId);
    NotifInfo.u4Operation = OSIX_HTONL (pNotifyInfo->u4Operation);
    NotifInfo.u4Status = OSIX_HTONL (pNotifyInfo->u4Status);
    NotifInfo.u4Error = OSIX_HTONL (pNotifyInfo->u4Error);

    MEMCPY (NotifInfo.ac1DateTime, pNotifyInfo->ac1DateTime,
            RM_DATE_TIME_STR_LEN);

    STRNCPY (NotifInfo.ac1ErrorStr, pNotifyInfo->ac1ErrorStr,
             STRLEN (pNotifyInfo->ac1ErrorStr));
    /* Copy RM notification information to the appropriate 
     * offset by leaving space for RM hdr */
    if ((RM_COPY_TO_OFFSET (pRmMsg, &NotifInfo, 0,
                            sizeof (tRmNotificationMsg))) == CRU_FAILURE)
    {
        u4RetVal = RM_FAILURE;
    }

    /* Message type used by the RM active node to process the
     * incoming notification information message */
    u4MsgType = OSIX_HTONL (RM_NOTIFICATION_INFO);

    /* Prepend message type to notification information */
    if ((u4RetVal != RM_FAILURE) &&
        (RM_PREPEND_BUF (pRmMsg, (UINT1 *) &u4MsgType, sizeof (UINT4))) !=
        CRU_SUCCESS)
    {
        u4RetVal = RM_FAILURE;
    }

    /* Prepend RM message Hdr */
    if (u4RetVal != RM_FAILURE)
    {
        u4RetVal =
            RmPrependRmHdr (pRmMsg,
                            ((sizeof (tRmNotificationMsg) + sizeof (UINT4))));
    }

    if ((pRmPktQMg = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId)) ==
        NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmEnqMsgToRmFromAppl"
                "Msg ALLOC_MEM_BLOCK FAILED!! \n");
        u4RetVal = RM_FAILURE;
    }

    if (u4RetVal != RM_FAILURE)
    {
        MEMSET (pRmPktQMg, 0, sizeof (tRmPktQMsg));

        pRmPktQMg->u1MsgType = RM_DYNAMIC_SYNC_MSG;
        pRmPktQMg->DynSyncMsg = pRmMsg;

        if (RM_SEND_TO_QUEUE (RM_QUEUE_ID,
                              (UINT1 *) &pRmPktQMg,
                              RM_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "SendToQ failed in RmEnqMsgToRmFromAppl\n");
            RmUtilReleaseMemForPktQMsg (pRmPktQMg);
            return RM_FAILURE;
        }

        if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "SendEvent failed in RmEnqMsgToRmFromAppl\n");
            return RM_FAILURE;
        }
#ifdef L2RED_TEST_WANTED
        RM_TEST_INCR_PROTO_TX_COUNT (RM_APP_ID);
#endif
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pRmMsg, 0);
        if (pRmPktQMg != NULL)
        {
            RmUtilReleaseMemForPktQMsg (pRmPktQMg);
        }
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "RM copy to CRU buf failed !!!\n");
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function Name      : RmTrapProcessInfoFromStandby
 *
 * Description        : Routine to process notification information send from 
 *                      STANDBY node. Notifies the management application
 *                      about the information processed.
 *
 * Input(s)           : pNotifyInfo - Pointer to the tRmNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
RmTrapProcessInfoFromStandby (tRmMsg * pPkt)
{
    tRmNotificationMsg  NotifMsg;

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

    RM_TRC (RM_NOTIF_TRC,
            "RM process notification information got from Standby !!!\n");
    /* Now the pkt has [RM msg type + RM notification Info]. 
     * Strip off RM msg type */
    RM_PKT_MOVE_TO_DATA (pPkt, sizeof (UINT4));

    /* Copy the CRU buf content to linear buffer and 
     * free the CRU buf. */
    if (RM_COPY_TO_LINEAR_BUF (pPkt, &NotifMsg, 0,
                               sizeof (tRmNotificationMsg)) == CRU_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC, "CRU to linear copy failure\n");
        RM_FREE (pPkt);
        return;
    }

    RM_FREE (pPkt);

    /* Convert the rcvd msg from NETWORK byte 
     * order to HOST byte order */
    NotifMsg.u4NodeId = OSIX_NTOHL (NotifMsg.u4NodeId);
    NotifMsg.u4State = OSIX_NTOHL (NotifMsg.u4State);
    NotifMsg.u4AppId = OSIX_NTOHL (NotifMsg.u4AppId);
    NotifMsg.u4Operation = OSIX_NTOHL (NotifMsg.u4Operation);
    NotifMsg.u4Status = OSIX_NTOHL (NotifMsg.u4Status);
    NotifMsg.u4Error = OSIX_NTOHL (NotifMsg.u4Error);

    /* Transmission path is enabled so send the trap */
    if (gRmInfo.u4RmTxEnabled == RM_TRUE)
    {
        RmTrapSendNotifications (&NotifMsg);
    }
    else
    {
        /* Store in the array if transmission path is not enabled */
        if (RmUtilStoreInArray (&NotifMsg) == RM_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                    "RmTrapProcessInfoFromStandby: Storing in array failed \n");
        }
    }

    return;
}
#endif /* RM_TRAP_C */
