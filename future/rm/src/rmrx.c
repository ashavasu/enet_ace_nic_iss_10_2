/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmrx.c,v 1.35 2017/01/25 13:19:00 siva Exp $
*
* Description: RM utility functions for processing of received sync-up
* messages.
***********************************************************************/
#ifndef _RMRX_C_
#define _RMRX_C_
#include "rmincs.h"
#include "fssocket.h"
#include "rmglob.h"
#ifdef L2RED_TEST_WANTED
#include "fsrmlw.h"
#endif

/******************************************************************************
 * Function           : RmRxProcessBufferedPkt
 * Input(s)           : None.
 * Output(s)          : None
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Function to process the buffered sync-up message
 *                      present in the RX buffer.
 ******************************************************************************/
INT4
RmRxProcessBufferedPkt (VOID)
{
    UINT4               u4NextSeqNumber = 0;
    tRmRxBufHashEntry  *pRxBufNode = NULL;

    if (RM_IS_RX_BUFF_PROCESSING_ALLOWED () == RM_FALSE)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmRxProcessBufferedPkt: Rx buffer is LOCKED\n");
        return RM_SUCCESS;
    }

    if (RM_RX_BUFF_NODE_COUNT () == 0)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmRxProcessBufferedPkt: Rx buffer is Empty\n");

        if ((RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE) &&
            (RM_RX_LAST_SEQ_NUM_PROCESSED () == RM_RX_LAST_ACK_RCVD_SEQ_NUM ()))
        {
            RM_TRC (RM_SYNCUP_TRC, "RmRxProcessBufferedPkt: Node Transition "
                    "in progress is handled for Rx buffer.\r\n");
            /* This check is valid for failover case. When active crashes 
             * during bulk update in progress at standby, the new active 
             * node corrects the sync-up data by doing a h/w audit. When 
             * the lower most layer has not completed the bulk update, 
             * there is no use doing a h/w audit for higher layers. 
             * Some of the protocols does not have h/w audit. Those 
             * protocols may be in inconsistent state*/

            HbApiSendRmEventToHb (HB_SYNC_MSG_PROCESSED);

            RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_FALSE);
        }
        return RM_SUCCESS;
    }

    /* Get Next valid seqence number that needs to be fetched from the buffer */
    u4NextSeqNumber = RM_RX_GETNEXT_VALID_SEQNUM ();

    /* Search this seqence numbered pkt in the rx buffer */
    pRxBufNode = RmRxBufSearchPkt (u4NextSeqNumber);

    if (pRxBufNode == NULL)
    {
	if ((RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE) ||
                (RM_RX_BUFF_NODE_COUNT () > (0.75 * MAX_RM_RX_BUF_PKT_NODES)))
        {
            /* Case where 75% of the buffer is exhausted  with packets from active
             * to be processed. We need to adjust the global sequence No to ensure
             * that the remaining packets are processed.*/
            RM_TRC2 (RM_CRITICAL_TRC, "RmRxProcessBufferedPkt: "
		    "HASH MISSED[Seq# %d], (hash count = %d):: While Node "
                    "Transition is in progress or 0.75 buffer exausted\n",

                     u4NextSeqNumber, RM_RX_BUFF_NODE_COUNT ());
            do
            {
                /* Update the last processed sequence number */
                RM_RX_INCR_LAST_SEQNUM_PROCESSED ();

                /* Get Next valid seqence number that needs to be fetched
                 * from the buffer */
                u4NextSeqNumber = RM_RX_GETNEXT_VALID_SEQNUM ();

                /* Search this seqence numbered pkt in the rx buffer */
                pRxBufNode = RmRxBufSearchPkt (u4NextSeqNumber);
            }
            while ((RM_RX_BUFF_NODE_COUNT () != 0) && pRxBufNode == NULL);
        }
        else
        {
            /* Expected sequence numberd packet is not present in the buffer */
            RmRxSeqNumMisMatchHandler (u4NextSeqNumber);
            return RM_SUCCESS;
        }
    }

    /* Valid sequence numbered message found in the rx buffer */
    if (RM_RX_SEQ_MISSMATCH_FLG () == RM_TRUE)
        /* valid seq number received while seq recovery timer is running */
    {
        RmTmrStopTimer (RM_SEQ_RECOV_TIMER);
        RM_RX_SEQ_MISSMATCH_FLG () = RM_FALSE;
    }

    /* Update the last processed sequence number */
    RM_RX_INCR_LAST_SEQNUM_PROCESSED ();

    /* Post the buffer packet to protocol */
    if (RmRxPostBufferedPktToProtocol (pRxBufNode) == RM_FAILURE)
    {
        /* Increment the Last Ack received count as the Ack timer is not
         * going to be started */
        RM_RX_LAST_ACK_RCVD_SEQ_NUM () = pRxBufNode->u4SeqNum;

        RM_TRC1 (RM_FAILURE_TRC, "RmRxProcessBufferedPkt: Failed to "
                 "process the message with Seq# %d\r\n", u4NextSeqNumber);
        /* Precess the next bufferred pkt if any */
        if (RM_RX_BUFF_NODE_COUNT () != 0)
        {
            /* Post a self event which will invoke RmRxProcessBufferedPkt
             * to process the next buffered packet. This is done to
             * avoid the recursive call to RmRxProcessBufferedPk() from 
             * here */
            if (RM_SEND_EVENT (RM_TASK_ID, RM_PROCS_RX_BUFF_SELF_EVENT) ==
                OSIX_FAILURE)
            {
                RM_TRC (RM_EVENT_TRC | RM_FAILURE_TRC, "Send Event "
                        "RM_PROCS_RX_BUFF_SELF_EVENT Failed\n");
            }
        }
    }
    else                        /* Pkt is posted to Protocol Successfully */
    {
        /* Block the rx buffer processing, until ACK is received for this message
         * form protocol. */
        RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_FALSE;
        RM_TRC (RM_SYNCUP_TRC, "RmRxPostBufferedPktToProtocol: "
                "LOCKING Rx Buff\n");
        /* Update statistics - for the buffered messages that is processed */
        RM_RX_STAT_SYNC_MSG_PROCESSED ()++;

        /* Start the ACK recovery timer */
        RmTmrStartTimer (RM_ACK_RECOV_TIMER, RM_ACK_RECOV_TMR_INTERVAL);
    }

    /* Delete the Hash node from the rx buffer for this message */
    RmRxBufDeleteNode (pRxBufNode);

    RM_TRC1 (RM_SYNCUP_TRC, "RmRxProcessBufferedPkt: Hash node count = %d\n",
             RM_RX_BUFF_NODE_COUNT ());

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRxPostBufferedPktToProtocol
 * Input(s)           : pRxBufNode - Hash node pointer for the buffered 
 *                                   message present in the RX Buffer.
 * Output(s)          : None. 
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Allocate CRU buffer and copy the entire packet present
 *                      in the hash node to this buffer. This CRU buffer is 
 *                      used to post the sync-up message to the destination 
 *                      protocol based on u4DestEntId present in the RM header.
 *                      RM Header will not be stripped out from the CRU buffer.
 *                      protocol should take care of it.
 *                      NOTE: pRxBufNode->pu1Pkt will be freed by the caller 
 *                      case of RM_FAILURE / RM_SUCCESS both the cases. It
 *                      will be freed by Hash Delete node function.
 ******************************************************************************/
INT4
RmRxPostBufferedPktToProtocol (tRmRxBufHashEntry * pRxBufNode)
{
    tRmMsg             *pPkt = NULL;
    UINT4               u4SrcEntId;
    UINT4               u4DestEntId;
    UINT4               u4BufCount;
    if (pRxBufNode->pu1Pkt == NULL || pRxBufNode->u4PktLen == 0)
    {
        /* Packet buffer will be freed by the caller */
        RM_TRC (RM_FAILURE_TRC, "RmRxPostBufferedPktToProtocol: "
                "Invalid packet present in the Rx Buffer.\n");
        return RM_FAILURE;
    }

    if (pRxBufNode->u4SeqNum == RM_SEQNUM_WRAP_AROUND_VALUE)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmRxPostBufferedPktToProtocol: "
                "WRAP AROUND happened for sequence numbers\n");
    }

    /* Allocate memory for CRU buffer. This mem will be freed by appls. 
     * after processing. So, do not free this mem here. */
    if ((pPkt = RM_ALLOC_RX_BUF (pRxBufNode->u4PktLen)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "EXIT: Unable to allocate buf\n");
        RmHandleSystemRecovery (RM_RX_CRU_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }

    /* copy the linear buffer to CRU buffer */
    if (RM_COPY (pPkt, pRxBufNode->pu1Pkt, pRxBufNode->u4PktLen) == CRU_FAILURE)
    {
        RM_FREE (pPkt);            /* Release the CRU Buffer */
        RM_TRC (RM_FAILURE_TRC, "EXIT: Unable to copy linear to CRU buf\n");
        return RM_FAILURE;
    }

    /* pPkt should not be freed here. it will be freed while deleting the 
     * rx buff node */

    RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_SRC_ENTID, u4SrcEntId);
    RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_DEST_ENTID, u4DestEntId);

    /* Update the last processed AppId */
    RM_RX_LAST_APPID_PROCESSED () = u4DestEntId;

    /* Post the message to protocol */
    u4BufCount = RM_RX_BUFF_NODE_COUNT () - 1;
    if (RmUtilPostDataOrEvntToProtocol (u4DestEntId, (UINT1) RM_MESSAGE,
                                        pPkt, pRxBufNode->u4PktLen,
                                        pRxBufNode->u4SeqNum) == RM_FAILURE)
    {
        RM_FREE (pPkt);            /* Release the CRU Buffer */
        RM_TRC3 (RM_FAILURE_TRC | RM_BUFF_TRACE | RM_SYNCUP_TRC,
                 "RmRxPostBufferedPktToProtocol: "
                 "HASH DROPPED [Seq# %d AppId-%s], "
                 "(hash count = %d)\r\n", pRxBufNode->u4SeqNum,
                 gapc1AppName[u4DestEntId], u4BufCount);

        return RM_FAILURE;
    }

    RM_TRC3 (RM_BUFF_TRACE | RM_SYNCUP_TRC, "RmRxPostBufferedPktToProtocol: "
             "HASH PROCESSED [Seq# %d AppId-%s], "
             "(hash count = %d)\r\n", pRxBufNode->u4SeqNum,
             gapc1AppName[u4DestEntId], u4BufCount);

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRxSeqNumMisMatchHandler
 * Input(s)           : u4SeqNum - Sequence number that is not found in the 
 *                                 RX Buffer.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This Function handles the situation is the desired 
 *                      sequence number is not found in the RX Buffer while
 *                      processing. It starts the sequence recovery timer.
 *                      In case node transition in progress, no need to handle
 *                      the situation.
 ******************************************************************************/
VOID
RmRxSeqNumMisMatchHandler (UINT4 u4SeqNum)
{
    RM_TRC2 (RM_BUFF_TRACE | RM_SYNCUP_TRC,
             "RmRxSeqNumMisMatchHandler: HASH MISSED[Seq# %d], "
             "(hash count = %d)\r\n", u4SeqNum, RM_RX_BUFF_NODE_COUNT ());

    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        if (RM_RX_SEQ_MISSMATCH_FLG () == RM_TRUE)
        {
            RM_TRC (RM_SYNCUP_TRC, "RmRxSeqNumMisMatchHandler: Seq Recov timer "
                    "is already running\r\n");
            return;
        }
        else
        {
            RM_RX_SEQ_MISSMATCH_FLG () = RM_TRUE;
#if L2RED_WANTED
            RmTmrStartTimer (RM_SEQ_RECOV_TIMER, RM_SEQ_RECOV_TMR_INTERVAL);
#endif
            RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
            RM_TRC (RM_SYNCUP_TRC, "RmRxSeqNumMisMatchHandler: "
                    "UNLOCKING Rx Buff\r\n");
        }
    }
}

/******************************************************************************
 * Function           : RmRxResetRxBuffering
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function is used to reset the flags and counters
 *                      used for the RX buffer.
 ******************************************************************************/
VOID
RmRxResetRxBuffering (VOID)
{
    RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_FALSE;
    RM_TRC (RM_BUFF_TRACE | RM_SYNCUP_TRC, "RmRxResetRxBuffering: "
            "LOCKING Rx Buff\r\n");

    RM_RX_SEQ_MISSMATCH_FLG () = RM_FALSE;
    RM_RX_LAST_SEQ_NUM_PROCESSED () = 0;
    RM_RX_LAST_ACK_RCVD_SEQ_NUM () = 0;
    RM_RX_LAST_APPID_PROCESSED () = RM_APP_ID;
}

/******************************************************************************
 * Function           : RmProcessRcvEvt 
 * Input(s)           : pRmSsnInfo - RM session information
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to receive RM sync messages
 ******************************************************************************/
UINT4
RmProcessRcvEvt (tRmSsnInfo * pRmSsnInfo)
{
    tRmPeerCtrlMsg      PeerPkt;
    INT4                i4Read = 0;
    UINT1              *pu1RcvPkt;
    INT4                i4RecvBytes = 0;
    UINT2               u2Cksum = 0;
#ifdef L2RED_TEST_WANTED
    UINT4               u4MsgType = 0;
#endif
    UINT4               u4SslMsgType = 0;
    UINT1              *pu1Data = NULL;
    UINT1              *pu1Start = NULL;
    UINT2               u2HeaderLen = RM_HDR_LENGTH;
    UINT4               u4DataLen = 0;
    UINT4               u4RelinquishCntr = 0;
    UINT4               u4RetVal = RM_SUCCESS;
    BOOL1               bIsPartialDataRcvd = RM_FALSE;
    UINT1               u1SsnIndex = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmProcessRcvEvt: ENTRY \r\n");

#ifdef L2RED_WANTED
    if (pRmSsnInfo->i1ConnStatus != RM_SOCK_CONNECTED)
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessRcvEvt: connection is lost \r\n");
        return RM_FAILURE;
    }
#endif
    if (gRmInfo.bIsRxBuffFull == RM_TRUE)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmProcessRcvEvt: Rx buffer is full \r\n");
        if (SelAddFd (pRmSsnInfo->i4ConnFd, RmPktRcvd) == OSIX_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "RmProcessRcvEvt: SelAddFd Failure "
                    "while adding RM reliable socket\n");
        }
        return RM_SUCCESS;
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        MEMSET (&PeerPkt, 0, sizeof (tRmPeerCtrlMsg));

        u4RetVal = RmTcpSockRcv (pRmSsnInfo->i4ConnFd, (UINT1 *) &PeerPkt,
                                 (UINT2) sizeof (tRmPeerCtrlMsg), &i4Read);
        if (u4RetVal == RM_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC, " Peer information reception failed \n");
        }
        if (StackProcessSwitchInfoMsg (&PeerPkt,
                                       sizeof (tRmPeerCtrlMsg),
                                       gRmInfo.u4ActiveNode) != RM_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "StackProccessSwitchInfoMsg returned fail\r\n");
            return RM_FAILURE;
        }
        close (pRmSsnInfo->i4ConnFd);
        pRmSsnInfo->i4ConnFd = RM_INV_SOCK_FD;
        pRmSsnInfo->u4PeerAddr = 0;
        pRmSsnInfo->i1ConnStatus = RM_SOCK_NOT_CONNECTED;
        return RM_SUCCESS;
    }
    RM_RXBUF_PKT_MEM_ALLOC (pu1RcvPkt, RM_MAX_SYNC_PKT_LEN);
    if (pu1RcvPkt == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_CRITICAL_TRC,
                "RmProcessRcvEvt: "
                "Unable to allocate memory for RmProcessRcvEvt message\n");
        RmHandleSystemRecovery (RM_RX_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }

    MEMSET (pu1RcvPkt, 0, RM_MAX_SYNC_PKT_LEN);
    /* Do continuous RM synchronization message read */
    while (1)
    {
        /* RM_SYNC_PDU =  RM_HDR_LENGTH + SYNC_DATA. 
         * First Read the RM_DATA_LEN and based on 
         * RM_DATA_LEN Read SYNC_DATA. If data is not available
         * fully during RM_DATA_LEN or SYNC_DATA the copy the 
         * read data to pRmSsnInfo->u2ResBufLen and update
         * pRmSsnInfo->u2ResBufLen */

        RM_TRC (RM_SYNCUP_TRC, "RmProcessRcvEvt: RM sync packets read\r\n");
        pu1Data = pu1Start = pu1RcvPkt;
        u2HeaderLen = RM_HDR_LENGTH;
        u4DataLen = 0;

        /* If Residual Data is present then First Copy the residual 
         * data to pu1Data */

        if (pRmSsnInfo->u2ResBufLen != 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmProcessRcvEvt: Sync packets are exist "
                     "in residual buffer and size is=%d\r\n",
                     pRmSsnInfo->u2ResBufLen);

            /* This routine copies the residual buffer data to linear buffer
             * and based on the residual buffer length, it determines the 
             * RM header OR payload length to be read */
            /* Copy the residual buffer data to linear buffer */
            MEMCPY (pu1Data, pRmSsnInfo->pResBuf, pRmSsnInfo->u2ResBufLen);
            pu1Data += pRmSsnInfo->u2ResBufLen;
            if (RmHandleResBufData (pu1Start, pRmSsnInfo,
                                    &u2HeaderLen, &u4DataLen) == RM_FAILURE)
            {
                pRmSsnInfo->u2ResBufLen = 0;
                u4RetVal = RM_FAILURE;
                break;
            }
        }

        /* Read Header If it is already read and present in 
         * residual data then this may zero */
        if (u2HeaderLen != 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC, "RmProcessRcvEvt: "
                     "RM header read to be started for size =%d\r\n",
                     u2HeaderLen);
            if (RmRecvRmHdrOrPayLoadFromConnFd
                (&pu1Data, pu1Start, pRmSsnInfo, u2HeaderLen,
                 &bIsPartialDataRcvd) == RM_FAILURE)
            {
                u4RetVal = RM_FAILURE;
                break;
            }
            if (bIsPartialDataRcvd == RM_TRUE)
            {
                break;
            }
            /* If Full Header is present then read the Data Length
             * from the Header */
            MEMCPY ((UINT1 *) &u4DataLen,
                    (pu1Start + RM_HDR_OFF_TOT_LENGTH), sizeof (UINT4));
            u4DataLen = OSIX_NTOHL (u4DataLen);
            /* RM total length is [RM header length + payload length] so,
             * exclude the RM header to get the payload length */
            u4DataLen -= RM_HDR_LENGTH;
            if (u4DataLen > (RM_MAX_SYNC_PKT_LEN - RM_HDR_LENGTH))
            {
                /* Data Length is more than our  Buffer size so,
                 * drop the data. This may be peer data send
                 * issue  */
                RM_TRC1 (RM_CRITICAL_TRC, "RmProcessRcvEvt: "
                         "RM Data Length is more than the Buffer size "
                         "so, packet dropped size=%d\r\n", u4DataLen);
                pRmSsnInfo->u2ResBufLen = 0;
                u4RetVal = RM_FAILURE;
                break;
            }
        }

        /* Read Data */
        if (u4DataLen != 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC, "RmProcessRcvEvt: "
                     "RM payload read to be started for size =%d\r\n",
                     u4DataLen);

            if (RmRecvRmHdrOrPayLoadFromConnFd
                (&pu1Data, pu1Start, pRmSsnInfo, (UINT2) u4DataLen,
                 &bIsPartialDataRcvd) == RM_FAILURE)
            {
                u4RetVal = RM_FAILURE;
                break;
            }
            if (bIsPartialDataRcvd == RM_TRUE)
            {
                break;
            }
        }

        i4RecvBytes = pu1Data - pu1Start;
        pRmSsnInfo->u2ResBufLen = 0;
        u2Cksum = RM_LINEAR_CALC_CKSUM ((const INT1 *) pu1RcvPkt, i4RecvBytes);
        i4RecvBytes = MEM_MAX_BYTES (i4RecvBytes, RM_MAX_SYNC_PKT_LEN);
        if (u2Cksum != 0)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "!!! Pkt rcvd with invalid cksum. Discarding.... !!!\n");
            continue;
        }

        PTR_FETCH4 (u4SslMsgType, (pu1RcvPkt + RM_HDR_OFF_MSG_TYPE));
        if (u4SslMsgType == RM_SSL_GET_CERT_REQ_MSG)
        {
            if (RmRequestFile(FILE_SSL_SERV_CERT) == RM_FAILURE)
            {
				u4RetVal = RM_FAILURE;
            }
        }
        if (u4SslMsgType == RM_V3_GET_CRYPTSEQ_REQ_MSG)
        {

            if (RmRequestFile(FILE_V3_CRYPT_SEQNUM) == RM_FAILURE)
            {
                u4RetVal = RM_FAILURE;
            }
        }
        else
        {
#ifdef L2RED_TEST_WANTED
        PTR_FETCH4 (u4MsgType, (pu1RcvPkt + RM_HDR_OFF_MSG_TYPE));
        if (u4MsgType == RM_DYN_AUDIT_CHKSUM_PEER_REQ)
        {
            if (RmHandleChecksumRequest () == RM_FAILURE)
            {
                u4RetVal = RM_FAILURE;
            }
        }
        else if (u4MsgType == RM_DYN_AUDIT_CHKSUM_PEER_RESP)
        {
            if (RmHandleChecksumResponseMsg (pu1RcvPkt, i4RecvBytes) ==
                RM_FAILURE)
            {
                u4RetVal = RM_FAILURE;
            }
        }
        else
        {
#endif
            if (RmHandleCompleteSyncMsg (pu1RcvPkt, i4RecvBytes) == RM_FAILURE)
            {
                if (gRmInfo.bIsRxBuffFull == RM_TRUE)
                {
                    RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                            "!!! Sync message reception is stopped "
                            "because Rx buffer is full !!!\n");
                    u4RetVal = RM_SUCCESS;
                }
                else
                {
                    u4RetVal = RM_FAILURE;
                }
                break;
            }
#ifdef L2RED_TEST_WANTED
        }
#endif
        }
        /* This Relinquish point is for processing
         * the higher priority messages */
        u4RelinquishCntr++;
        if (u4RelinquishCntr > RM_RELINQUISH_CNTR)
        {
            RM_TRC (RM_SYNCUP_TRC, "RmProcessRcvEvt: Relinquished from "
                    "Sync. message reception \r\n");
            RmPktRcvd (pRmSsnInfo->i4ConnFd);
            break;
        }
    }

    if (u4RetVal == RM_SUCCESS)
    {
        if (SelAddFd (pRmSsnInfo->i4ConnFd, RmPktRcvd) == OSIX_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "RmProcessRcvEvt: SelAddFd Failure "
                    "while adding RM reliable socket\n");
        }
    }
    else
    {
        if (RmGetSsnIndexFromConnFd (pRmSsnInfo->i4ConnFd,
                                     &u1SsnIndex) == RM_FAILURE)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "RmProcessRcvEvt: Session does not exist "
                    "for the connection fd\n");
        }
        else
        {
           RM_TRC1 (RM_FAILURE_TRC,
                    "RmProcessRcvEvt: Closing Socket connection "
                    "fd:%d\r\n",RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
            RmUtilCloseSockConn (u1SsnIndex);
        }
    }
    RM_TRC (RM_SYNCUP_TRC, "RmProcessRcvEvt: EXIT \r\n");
    RM_RXBUF_PKT_MEM_FREE (pu1RcvPkt);
    return u4RetVal;
}

/******************************************************************************
 * Function           : RmHandleResBufData
 * Input(s)           : pu1Data - Pointer to Linear buffer
 *                      pu1Start - Pointer to starting location of the pointer 
                        pRmSsnInfo - Session information
                        pu2HeaderLen - Pointer to RM header length
                        pu4DataLen - Pointer to RM data length
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine copies the residual buffer data to linear buffer
                        and determines the RM header length and RM payload length
                        based on the residual buffer data length.   
 ******************************************************************************/
UINT4
RmHandleResBufData (UINT1 *pu1Start, tRmSsnInfo * pRmSsnInfo,
                    UINT2 *pu2HeaderLen, UINT4 *pu4DataLen)
{
    UINT2               u2RmHdrLen = 0;
    UINT4               u4RmDataLen = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmHandleResBufData: ENTRY\r\n");

    /* Get the RM header length to be read based on the 
       residual buffer data */
    if (pRmSsnInfo->u2ResBufLen < RM_HDR_LENGTH)
    {
        RM_TRC (RM_SYNCUP_TRC, "RmHandleResBufData: "
                "Residual sync packet < RM header length\r\n");
        u2RmHdrLen = RM_HDR_LENGTH - pRmSsnInfo->u2ResBufLen;
    }
    else
    {
        RM_TRC (RM_SYNCUP_TRC, "RmHandleResBufData: "
                "Residual sync packet >= RM header length\r\n");
        u2RmHdrLen = 0;
        /* If Full Header is present then read the Data Length
         * from the Header */
        MEMCPY ((UINT1 *) &u4RmDataLen,
                (pu1Start + RM_HDR_OFF_TOT_LENGTH), sizeof (UINT4));
        u4RmDataLen = OSIX_NTOHL (u4RmDataLen);
        /* RM total length is [RM header length + payload length] so, 
           exclude the RM header to get the payload length */
        u4RmDataLen -= RM_HDR_LENGTH;

        if (u4RmDataLen > (RM_MAX_SYNC_PKT_LEN - RM_HDR_LENGTH))
        {
            /* Data Length is more than our  Buffer size so,
             * drop the data. This may be peer data send
             * issue  */

            RM_TRC1 (RM_CRITICAL_TRC, "RmHandleResBufData: "
                     "RM Data Length is more than the Buffer size "
                     "so, packet dropped size=%d\r\n", u4RmDataLen);
            return RM_FAILURE;
        }

        /* If pRmSsnInfo->u2ResBufLen is greater than
         * Header length then some part of Data is already read so,
         * Adjust the Data length */
        if (pRmSsnInfo->u2ResBufLen > RM_HDR_LENGTH)
        {
            u4RmDataLen -= (pRmSsnInfo->u2ResBufLen - RM_HDR_LENGTH);
            /* pu1Data is already adjusted  before this IF
             * condition */
        }
    }
    *pu2HeaderLen = u2RmHdrLen;
    *pu4DataLen = u4RmDataLen;
    RM_TRC (RM_SYNCUP_TRC, "RmHandleResBufData: EXIT\r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRecvRmHdrOrPayLoadFromConnFd
 * Input(s)           : pu1Data - Linear buffer
                        pRmSsnInfo - Session information
                        u2MsgLen - RM header length to be read
                        pu4DataLen - Pointer to sync message payload length
                        pbIsPartialDataRcvd - Partial data received indicator
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine receives the RM header from reliable channel
 ******************************************************************************/
UINT4
RmRecvRmHdrOrPayLoadFromConnFd (UINT1 **ppu1Data, UINT1 *pu1Start,
                                tRmSsnInfo * pRmSsnInfo, UINT2 u2MsgLen,
                                BOOL1 * pbIsPartialDataRcvd)
{
    INT4                i4Read = 0;
    UINT4               u4RetVal = RM_SUCCESS;
    UINT2               u2TotalRead = 0;
    UINT1              *pu1Data = *ppu1Data;

    RM_TRC (RM_SYNCUP_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: ENTRY\r\n");

    *pbIsPartialDataRcvd = RM_FALSE;

    u4RetVal = RmTcpSockRcv (pRmSsnInfo->i4ConnFd, pu1Data, u2MsgLen, &i4Read);

    if (u4RetVal == RM_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: "
                "Fatal error occurred in the reliable "
                "socket communication\r\n");
        u4RetVal = RM_FAILURE;
    }
    else if (i4Read == 0)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: "
                "Peer is performed an orderly shutdown\r\n");
        u4RetVal = RM_FAILURE;
    }
    else if (i4Read < u2MsgLen)
    {
        pRmSsnInfo->u2ResBufLen = 0;
        pu1Data = (i4Read < 0) ? pu1Data : (pu1Data + i4Read);
        u2TotalRead = (UINT2) (pu1Data - pu1Start);
        if (u2TotalRead != 0)
        {
            MEMCPY (pRmSsnInfo->pResBuf, pu1Start, u2TotalRead);
            pRmSsnInfo->u2ResBufLen = u2TotalRead;
        }
        if (i4Read > 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: "
                     "RM header received < RM header to be received "
                     "and the received size is =%d\r\n", i4Read);
        }
        *pbIsPartialDataRcvd = RM_TRUE;
    }
    else
    {
        pu1Data += u2MsgLen;
        *ppu1Data = pu1Data;
        RM_TRC (RM_SYNCUP_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: "
                "The data to be read from the socket is completed\r\n");
    }
    RM_TRC (RM_SYNCUP_TRC, "RmRecvRmHdrOrPayLoadFromConnFd: EXIT\r\n");
    return u4RetVal;
}

/******************************************************************************
 * Function           : RmHandleCompleteSyncMsg
 * Input(s)           : pu1RmSyncMsg - Linear sync message
 *                      i4RmSyncMsgLen - Sync message length
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to handle the complete synchronization message
 ******************************************************************************/
UINT4
RmHandleCompleteSyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen)
{
    UINT4               u4SeqNum = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmHandleCompleteSyncMsg: ENTRY\r\n");
    PTR_FETCH4 (u4SeqNum, (pu1RmSyncMsg + RM_HDR_OFF_SEQ_NO));
    PTR_FETCH4 (u4SrcEntId, (pu1RmSyncMsg + RM_HDR_OFF_SRC_ENTID));
    PTR_FETCH4 (u4DestEntId, (pu1RmSyncMsg + RM_HDR_OFF_DEST_ENTID));

    /* RM_STATIC_CONF_APP_ID registration check is not required, 
     * as it is not registered */
    if (u4SrcEntId != RM_STATIC_CONF_APP_ID)
    {
        /* Check if the pkt is rcvd from a valid Entity (i.e., Application) */
        if ((u4SrcEntId != RM_APP_ID) &&
            ((gRmInfo.RmAppInfo[u4SrcEntId] == NULL) ||
             (gRmInfo.RmAppInfo[u4SrcEntId]->u4ValidEntry == FALSE)))
        {
            /* Pkt rcvd from and invalid EntId. Discard this packet */
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                     "RmHandleCompleteSyncMsg: "
                     "Pkt rcvd from invalid EntId %d. Discarding "
                     ".... !!!\r\n", u4SrcEntId);
            return RM_SUCCESS;
        }
    }

    if (u4DestEntId >= RM_MAX_APPS)
    {
        RM_TRC (RM_FAILURE_TRC, "RmHandleCompleteSyncMsg: "
                "DestEntId invalid in rcvd pkt\r\n");
        return RM_SUCCESS;
    }

    RM_TRC3 (RM_SYNCUP_TRC,
             "RmHandleCompleteSyncMsg: Pkt received "
             "[SeqNum# %d, AppId=%s]\nLast processed seq# %d.\r\n",
             u4SeqNum, gapc1AppName[u4DestEntId],
             RM_RX_LAST_SEQ_NUM_PROCESSED ());
    /* Update the statistics for all the received sync-up message. 
     * 1. sync-up message which will be buffered 
     * 2. sync-up message which will not be bufferd and processed 
     *    directly */
    RM_RX_STAT_SYNC_MSG_RECVD ()++;

    /* Print the module level trace message */
    RmTrcPrintModSyncMsgTrace (pu1RmSyncMsg);
    /* STANDBY side sequenced packets handling */
    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        if ((u4DestEntId == RM_APP_ID) || (u4SeqNum == 0))
        {
            RM_TRC2 (RM_CRITICAL_TRC,
                    "RmHandleCompleteSyncMsg: Invalid sync message[%d] "
                    "received with DestApp[%d] \r\n", u4SeqNum, u4DestEntId);

            return RM_SUCCESS;
        }
        if (RmProcessStandbySyncMsg (pu1RmSyncMsg, i4RmSyncMsgLen,
                                     u4SeqNum) == RM_FAILURE)
        {
            RM_TRC2 (RM_CRITICAL_TRC,
                    "RmHandleCompleteSyncMsg: Process sync message seqno[%d] "
                    "with DestApp[%d] \r\n", u4SeqNum, u4DestEntId);

            return RM_FAILURE;
        }
        if (RM_RX_BUFF_NODE_COUNT () >= MAX_RM_RX_BUF_NODES)
        {
            /* Rx buff node count is reached the max limit, 
             * so do not process the packet from socket */
            RM_TRC (RM_CRITICAL_TRC,
                    "RmHandleCompleteSyncMsg: Rx buffer is full\n");
            gRmInfo.bIsRxBuffFull = RM_TRUE;
            return RM_FAILURE;
        }
    }
    else
    {
        if ((u4DestEntId == RM_STATIC_CONF_APP_ID) || (u4SeqNum != 0))
        {
            RM_TRC (RM_CRITICAL_TRC,
                    "RmHandleCompleteSyncMsg: Sequenced sync message "
                    "is not valid at ACTIVE and INIT\n");
            return RM_SUCCESS;
        }

        /* ACTIVE side non-sequenced packets handling */
        if (RmProcessActiveSyncMsg (pu1RmSyncMsg, i4RmSyncMsgLen,
                                    u4DestEntId) == RM_FAILURE)
        {
            return RM_FAILURE;
        }
    }
    RM_TRC (RM_SYNCUP_TRC, "RmHandleCompleteSyncMsg: EXIT\r\n");
    return RM_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmHandleChecksumResponseMsg
 * Input(s)           : pu1RmSyncMsg - Linear sync message
 *                      i4RmSyncMsgLen - Sync message length
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to handle the checksum response msg from standby
 ******************************************************************************/
UINT4
RmHandleChecksumResponseMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen)
{
    tRmMsg             *pPkt = NULL;
    UINT4               u4OffSet = 0;
    INT4                i4RmDataLen = 0;
    UINT2               u2AppId = 0;
    UINT2               u2ChkSum = 0;

    if ((pPkt = RM_ALLOC_RX_BUF ((UINT4) i4RmSyncMsgLen)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessActiveSyncMsg: "
                "Unable to allocate buffer for sync message\r\n");
        RmHandleSystemRecovery (RM_RX_CRU_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }
    i4RmDataLen = i4RmSyncMsgLen;

    /* copy the linear buffer to CRU buffer */
    if (RM_COPY (pPkt, pu1RmSyncMsg, (UINT4) i4RmDataLen) == CRU_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmProcessActiveSyncMsg: "
                "Unable perform copying operation for sync message\r\n");
        RM_FREE (pPkt);
        return RM_FAILURE;
    }

    /* Now the pkt has [RM Hdr + RM data]. Strip off RM header */
    RM_PKT_MOVE_TO_DATA (pPkt, RM_HDR_LENGTH);
    i4RmDataLen = i4RmDataLen - (INT4) RM_HDR_LENGTH;
    /* Get the message type to distinguish between the packets
     * received */
    while (i4RmDataLen > 0)
    {
        RM_GET_DATA_2_BYTE (pPkt, u4OffSet, u2AppId);
        u4OffSet = u4OffSet + sizeof (UINT2);
        RM_GET_DATA_2_BYTE (pPkt, u4OffSet, u2ChkSum);
        u4OffSet = u4OffSet + sizeof (UINT2);
        i4RmDataLen = i4RmDataLen - (INT4) (sizeof (UINT4));
        gRmDynAuditInfo[u2AppId].i4RecvdChkSum = (INT4) u2ChkSum;
    }
    RmDynAuditValidateRecvdChkSum ();
    RmTmrStopTimer (RM_DYN_SYNC_AUDIT_TIMER);
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHandleChecksumRequest
 * Input(s)           : None 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to handle the checksum request msg from active
 ******************************************************************************/
UINT4
RmHandleChecksumRequest (VOID)
{
    UINT2               u2AppId;
    UINT1               u1State;
    u1State = (UINT1) RM_GET_NODE_STATE ();
    if (u1State != RM_STANDBY)
    {
        return RM_FAILURE;
    }
    if (RmConstructChkSumResponseMsg () == RM_FAILURE)
    {
        gu4DynAuditDoneForAllApp = RM_FALSE;
        return RM_FAILURE;
    }
    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        gRmDynAuditInfo[u2AppId].u4AuditStatus = RM_DYN_AUDIT_NOT_TRIGGERED;
        gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit = RM_FALSE;
        gRmDynAuditInfo[u2AppId].i4AuditChkSumValue = RM_INVALID_CHKSUM_VALUE;
    }
    gu4DynAuditDoneForAllApp = RM_FALSE;

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmConstructChkSumResponseMsg
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to construct checksum response msg in standby
 ******************************************************************************/
UINT4
RmConstructChkSumResponseMsg (VOID)
{
    tSNMP_OCTET_STRING_TYPE AppName;
    tRmMsg             *pMsg = NULL;
    UINT1               au1FileName[RM_MAX_FILE_NAME_LEN];
    UINT1               au1AppName[RM_MAX_FILE_NAME_LEN -
                                   RM_DYN_AUDIT_TEMP_LEN -
                                   RM_DYN_AUDIT_FN_STDBY_LEN];
    UINT4               u4OffSet = 0;
    INT4                i4Index = 0;
    INT4                i4RetVal = 0;
    UINT2               u2AppId = 0;
    UINT2               u2BufLen = 0;
    UINT2               u2ChkSum = 0;

    u2BufLen = (4 * RM_MAX_APPS);    /*4 bytes = 2 bytes for filling AppId + 
                                       2 bytes for filling chksum value */
    if ((pMsg = RmAllocTxBuf (u2BufLen)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "Rm alloc failed\n");
        return RM_FAILURE;
    }

    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        if ((gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit == RM_TRUE))
        {
            if (gRmDynAuditInfo[u2AppId].i4AuditChkSumValue !=
                RM_INVALID_CHKSUM_VALUE)
            {
                u2ChkSum = (UINT2) gRmDynAuditInfo[u2AppId].i4AuditChkSumValue;
                RM_DATA_PUT_2_BYTE (pMsg, u4OffSet, u2AppId, i4RetVal);
                u4OffSet = u4OffSet + sizeof (UINT2);
                RM_DATA_PUT_2_BYTE (pMsg, u4OffSet, u2ChkSum, i4RetVal);
                u4OffSet = u4OffSet + sizeof (UINT2);
                if (i4RetVal == RM_FAILURE)
                {
                    RM_FREE (pMsg);
                    return RM_FAILURE;
                }
            }
            else
            {
                gu1IsAuditChksumRespPend = RM_TRUE;

                RM_FREE (pMsg);
                return RM_FAILURE;
            }
        }
    }
    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        if ((gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit == RM_TRUE))
        {
            MEMSET (au1FileName, 0, RM_MAX_FILE_NAME_LEN);
            MEMSET (au1AppName, 0,
                    (RM_MAX_FILE_NAME_LEN -
                     RM_DYN_AUDIT_TEMP_LEN - RM_DYN_AUDIT_FN_STDBY_LEN));
            STRNCPY (au1FileName, RM_DYN_AUDIT_TEMP_STR, RM_DYN_AUDIT_TEMP_LEN);
            AppName.pu1_OctetList = au1AppName;
            AppName.i4_Length = STRLEN (gaRmSwitchOverTime[u2AppId].au1AppName);
            MEMCPY (AppName.pu1_OctetList,
                    gaRmSwitchOverTime[u2AppId].au1AppName, AppName.i4_Length);

            for (i4Index = 0; i4Index < AppName.i4_Length; i4Index++)
            {
                au1FileName[(RM_DYN_AUDIT_TEMP_LEN + i4Index)] =
                    TOLOWER (AppName.pu1_OctetList[i4Index]);
            }
            STRNCAT (au1FileName, RM_DYN_AUDIT_FN_STDBY,
                     RM_DYN_AUDIT_FN_STDBY_LEN);
            if (RmStandbySendFile ((CONST CHR1 *) au1FileName) == RM_FAILURE)
            {
                RM_TRC1 (RM_FAILURE_TRC,
                         "RmStandbySendFile failed for the application %s\n",
                         AppName.pu1_OctetList);
            }
        }
    }
    return (RmSendDynAuditChkSumRespToActive ((pMsg), u4OffSet));
}

/******************************************************************************
 * Function           : RmDynAuditValidateRecvdChkSum
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to validate the receive checksum from standby
 ******************************************************************************/
VOID
RmDynAuditValidateRecvdChkSum (VOID)
{
    UINT2               u2AppId = 0;

    gu4DynAuditDoneForAllApp = RM_FALSE;
    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        if (gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit)
        {
            if (gRmDynAuditInfo[u2AppId].i4AuditChkSumValue
                == gRmDynAuditInfo[u2AppId].i4RecvdChkSum)
            {
                gRmDynAuditInfo[u2AppId].u4AuditStatus = RM_DYN_AUDIT_SUCCESS;
            }
            else
            {
                gRmDynAuditInfo[u2AppId].u4AuditStatus = RM_DYN_AUDIT_FAILED;
            }
            gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit = RM_FALSE;
            gRmDynAuditInfo[u2AppId].i4AuditChkSumValue =
                RM_INVALID_CHKSUM_VALUE;
        }
    }
    return;
}
#endif

/******************************************************************************
 * Function           : RmProcessStandbySyncMsg
 * Input(s)           : pu1RmSyncMsg - Linear sync message
 *                      i4RmSyncMsgLen - Sync message length
 *                      u4SeqNum - Sync message Sequenced Number
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to handle the standby side 
 *                      synchronization message
 ******************************************************************************/
UINT4
RmProcessStandbySyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen,
                         UINT4 u4SeqNum)
{
    UINT1              *pu1Pkt = NULL;

    RM_TRC (RM_SYNCUP_TRC, "RmProcessStandbySyncMsg: ENTRY\r\n");

    /*if incoming packet is old sequence numbered packet, dont post it
     *in the buffer
     */

    if (u4SeqNum < RM_RX_GETNEXT_VALID_SEQNUM ())
    {
        RM_TRC (RM_FAILURE_TRC | RM_CRITICAL_TRC,
                "RmProcessStandbySyncMsg: "
                "Discarding Old sequence numbered packet, silent ignore\n");
        return RM_SUCCESS;
    }

    /* Allocate memory for linear buffer to add it in the RX buff node.
     * Size of the buffers should be equeal to the received packet len.
     * pu1RmSyncMsg is reused for receive form call so it cannot be
     * stored in the Rx buffer node. */

    RM_RXBUF_PKT_MEM_ALLOC (pu1Pkt, i4RmSyncMsgLen);
    /* pu1Pkt will be freed after processing of RX buffer */

    if (pu1Pkt == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_CRITICAL_TRC,
                "RmProcessStandbySyncMsg: "
                "Unable to allocate memory for sync message\n");
        RmHandleSystemRecovery (RM_RX_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }

    MEMCPY (pu1Pkt, pu1RmSyncMsg, i4RmSyncMsgLen);

    if (RmRxBufAddPkt (pu1Pkt, u4SeqNum, i4RmSyncMsgLen) == RM_FAILURE)
    {
        /* clear the pkt memory */
        RM_RXBUF_PKT_MEM_FREE (pu1Pkt);
        RM_TRC (RM_CRITICAL_TRC, "RmProcessStandbySyncMsg: "
                "Failed to Add received packet " "to RX Buff.\n");
        return RM_FAILURE;
    }

    /* Call for RX buffer processing */
    RmRxProcessBufferedPkt ();
    RM_TRC (RM_SYNCUP_TRC, "RmProcessStandbySyncMsg: EXIT\r\n");
    KW_FALSEPOSITIVE_FIX (pu1Pkt);
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmProcessActiveSyncMsg
 * Input(s)           : pu1RmSyncMsg - Linear sync message
 *                      i4RmSyncMsgLen - Sync message length
 *                      u4DestEntId - Destination entity id
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Routine to handle the active side 
 *                      synchronization message
 ******************************************************************************/
UINT4
RmProcessActiveSyncMsg (UINT1 *pu1RmSyncMsg, INT4 i4RmSyncMsgLen,
                        UINT4 u4DestEntId)
{
    tRmMsg             *pPkt = NULL;
    UINT4               u4RmDataLen = 0;
    UINT4               u4MsgType = 0;

    RM_TRC (RM_SYNCUP_TRC, "RmProcessActiveSyncMsg: ENTRY\r\n");
    /* Allocate memory for CRU buffer. This mem will be freed by appls.
     * after processing. So, do not free this mem here. */
    if ((pPkt = RM_ALLOC_RX_BUF (i4RmSyncMsgLen)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessActiveSyncMsg: "
                "Unable to allocate buffer for sync message\r\n");
        RmHandleSystemRecovery (RM_RX_CRU_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }
    u4RmDataLen = i4RmSyncMsgLen;

    /* copy the linear buffer to CRU buffer */
    if (RM_COPY (pPkt, pu1RmSyncMsg, u4RmDataLen) == CRU_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmProcessActiveSyncMsg: "
                "Unable perform copying operation for sync message\r\n");
        RM_FREE (pPkt);
        return RM_FAILURE;
    }

    RM_TRC (RM_SYNCUP_TRC, "RmProcessActiveSyncMsg: "
            "RM Sync packet is not buffered.\n");

    /* Send RM data to appls. based on the dest entity id. 
     * for processing */
    if (u4DestEntId == RM_APP_ID)
    {
        /* Now the pkt has [RM Hdr + RM data]. Strip off RM header */
        RM_PKT_MOVE_TO_DATA (pPkt, RM_HDR_LENGTH);
        u4RmDataLen = u4RmDataLen - RM_HDR_LENGTH;

        /* Get the message type to distinguish between the packets
         * received */
        RM_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

        if ((u4MsgType == RM_NOTIFICATION_INFO) &&
            (RM_GET_NODE_STATE () == RM_ACTIVE))
        {
            RmTrapProcessInfoFromStandby (pPkt);
            /* Update statistics - for the processed Trap messages that 
             * are not bufferd in Rx buffer. */
            RM_RX_STAT_SYNC_MSG_PROCESSED ()++;
#ifdef L2RED_TEST_WANTED
            RM_TEST_INCR_PROTO_RX_COUNT (u4DestEntId);
#endif
        }
        else
        {
            RM_FREE (pPkt);
        }
    }
    else
    {
        if (RmUtilPostDataOrEvntToProtocol (u4DestEntId,
                                            (UINT1) RM_MESSAGE,
                                            pPkt, u4RmDataLen, 0) == RM_FAILURE)
        {
            RM_FREE (pPkt);
        }
        /* Update statistics - for the processed messages that 
         * are not bufferd in Rx buffer. */
        RM_RX_STAT_SYNC_MSG_PROCESSED ()++;
    }
    RM_TRC (RM_SYNCUP_TRC, "RmProcessActiveSyncMsg: EXIT\r\n");
    return RM_SUCCESS;
}

#endif /* _RMRX_C_ */
