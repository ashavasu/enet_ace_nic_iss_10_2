/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmwr.c,v 1.18 2016/03/18 13:15:33 siva Exp $
*
* Description: Protocol wrapper Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrmlw.h"
# include  "fsrmwr.h"
# include  "fsrmdb.h"

VOID
RegisterFSRM ()
{
    SNMPRegisterMib (&fsrmOID, &fsrmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsrmOID, (const UINT1 *) "fsrm");
}

VOID
UnRegisterFSRM ()
{
    SNMPUnRegisterMib (&fsrmOID, &fsrmEntry);
    SNMPDelSysorEntry (&fsrmOID, (const UINT1 *) "fsrm");
}

INT4
FsRmSelfNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmSelfNodeId (pMultiData->pOctetStrValue));
}

INT4
FsRmPeerNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmPeerNodeId (pMultiData->pOctetStrValue));
}

INT4
FsRmActiveNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmActiveNodeId (pMultiData->pOctetStrValue));
}

INT4
FsRmNodeStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmNodeState (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmHbIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmHbInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmPeerDeadIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmPeerDeadInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmTrcLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmTrcLevel (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmForceSwitchoverFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmForceSwitchoverFlag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmPeerDeadIntMultiplierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmPeerDeadIntMultiplier (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmSwitchIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmSwitchId (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmConfiguredStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmConfiguredState (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmStackMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    pMultiData->pOctetStrValue->i4_Length = 6;
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStackMacAddr
            ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
}

INT4
FsRmHbIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmHbInterval (pMultiData->i4_SLongValue));
}

INT4
FsRmPeerDeadIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmPeerDeadInterval (pMultiData->i4_SLongValue));
}

INT4
FsRmTrcLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmTrcLevel (pMultiData->u4_ULongValue));
}

INT4
FsRmForceSwitchoverFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmForceSwitchoverFlag (pMultiData->i4_SLongValue));
}

INT4
FsRmPeerDeadIntMultiplierSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmPeerDeadIntMultiplier (pMultiData->i4_SLongValue));
}

INT4
FsRmSwitchIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmSwitchId (pMultiData->i4_SLongValue));
}

INT4
FsRmConfiguredStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmConfiguredState (pMultiData->i4_SLongValue));
}

INT4
FsRmHbIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmHbInterval (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmPeerDeadIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmPeerDeadInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmTrcLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmTrcLevel (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmForceSwitchoverFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmForceSwitchoverFlag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmPeerDeadIntMultiplierTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmPeerDeadIntMultiplier
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmSwitchIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmSwitchId (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmConfiguredStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmConfiguredState (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmHbIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmHbInterval (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmPeerDeadIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmPeerDeadInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmTrcLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmTrcLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmForceSwitchoverFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmForceSwitchoverFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmPeerDeadIntMultiplierDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmPeerDeadIntMultiplier
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmSwitchIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmSwitchId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmConfiguredStateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmConfiguredState
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRmPeerTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRmPeerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRmPeerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRmPeerStackIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmPeerStackIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRmPeerStackMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsRmPeerStackMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (tMacAddr *) pMultiData->
                                        pOctetStrValue->pu1_OctetList));

}

INT4
FsRmPeerSwitchBaseMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsRmPeerSwitchBaseMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
FsRmStackPortCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStackPortCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmColdStandbyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmColdStandby (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmModuleTrcGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmModuleTrc (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmProtocolRestartFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmProtocolRestartFlag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmProtocolRestartRetryCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmProtocolRestartRetryCnt (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmHitlessRestartFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmHitlessRestartFlag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmIpAddress (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmSubnetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmSubnetMask (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStackInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStackInterface (pMultiData->pOctetStrValue));
}

#ifdef L2RED_TEST_WANTED
INT4
FsRmDynamicSyncAuditTriggerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmDynamicSyncAuditTrigger (&(pMultiData->u4_ULongValue)));
}
#endif

INT4
FsRmCopyPeerSyLogFileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmCopyPeerSyLogFile (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmStackPortCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmStackPortCount (pMultiData->i4_SLongValue));
}

INT4
FsRmColdStandbySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmColdStandby (pMultiData->i4_SLongValue));
}

INT4
FsRmModuleTrcSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmModuleTrc (pMultiData->u4_ULongValue));
}

INT4
FsRmProtocolRestartFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmProtocolRestartFlag (pMultiData->i4_SLongValue));
}

INT4
FsRmProtocolRestartRetryCntSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmProtocolRestartRetryCnt (pMultiData->u4_ULongValue));
}

INT4
FsRmHitlessRestartFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmHitlessRestartFlag (pMultiData->i4_SLongValue));
}

INT4
FsRmIpAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmIpAddress (pMultiData->u4_ULongValue));
}

INT4
FsRmSubnetMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmSubnetMask (pMultiData->u4_ULongValue));
}

INT4
FsRmStackInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmStackInterface (pMultiData->pOctetStrValue));
}

INT4
FsRmCopyPeerSyLogFileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmCopyPeerSyLogFile (pMultiData->i4_SLongValue));
}

#ifdef L2RED_TEST_WANTED
INT4
FsRmDynamicSyncAuditTriggerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmDynamicSyncAuditTrigger (pMultiData->u4_ULongValue));
}
#endif

INT4
FsRmStackPortCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmStackPortCount (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmColdStandbyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmColdStandby (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmModuleTrcTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmModuleTrc (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmProtocolRestartFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmProtocolRestartFlag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmProtocolRestartRetryCntTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmProtocolRestartRetryCnt
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmHitlessRestartFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmHitlessRestartFlag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmIpAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmIpAddress (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmSubnetMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmSubnetMask (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRmStackInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmStackInterface (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsRmCopyPeerSyLogFileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmCopyPeerSyLogFile
            (pu4Error, pMultiData->i4_SLongValue));
}

#ifdef L2RED_TEST_WANTED
INT4
FsRmDynamicSyncAuditTriggerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmDynamicSyncAuditTrigger
            (pu4Error, (UINT4) pMultiData->i4_SLongValue));
}
#endif

INT4
FsRmStackPortCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmStackPortCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmColdStandbyDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmColdStandby (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmModuleTrcDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmModuleTrc (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmProtocolRestartFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmProtocolRestartFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmProtocolRestartRetryCntDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmProtocolRestartRetryCnt
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmHitlessRestartFlagDep (UINT4 *pu4Error,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmHitlessRestartFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRmSwitchoverTimeTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRmSwitchoverTimeTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRmSwitchoverTimeTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRmAppNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmSwitchoverTimeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmAppName (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiData->pOctetStrValue));

}

INT4
FsRmEntryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmSwitchoverTimeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmEntryTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsRmExitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmSwitchoverTimeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmExitTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
FsRmSwitchoverTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmSwitchoverTimeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmSwitchoverTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

#ifdef L2RED_TEST_WANTED
INT4
GetNextIndexFsRmDynamicSyncAuditTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRmDynamicSyncAuditTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRmDynamicSyncAuditTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRmDynamicSyncAuditStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRmDynamicSyncAuditTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRmDynamicSyncAuditStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}
#endif

INT4
FsRmIpAddressDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmIpAddress (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmSubnetMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmSubnetMask (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmStackInterfaceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmStackInterface
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmCopyPeerSyLogFileDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmCopyPeerSyLogFile
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef L2RED_TEST_WANTED
INT4
FsRmDynamicSyncAuditTriggerDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmDynamicSyncAuditTrigger
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
#endif

INT4
FsRmTrapSwitchIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmTrapSwitchId (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmStatsSyncMsgTxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsSyncMsgTxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStatsSyncMsgTxFailedCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsSyncMsgTxFailedCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStatsSyncMsgRxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsSyncMsgRxCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStatsSyncMsgProcCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsSyncMsgProcCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStatsSyncMsgMissedCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsSyncMsgMissedCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsRmStatsConfSyncMsgFailCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmStatsConfSyncMsgFailCount (&(pMultiData->u4_ULongValue)));
}
INT4 FsRmStaticBulkStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRmStaticBulkStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsRmDynamicBulkStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRmDynamicBulkStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsRmOverallBulkStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRmOverallBulkStatus(&(pMultiData->i4_SLongValue)));
}
