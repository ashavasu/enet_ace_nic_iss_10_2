/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmhbmain.c,v 1.23 2014/05/23 07:55:03 siva Exp $
*
* Description: Heart Beat functionality of Redundancy manager module.
***********************************************************************/
#ifndef _RMHBMAIN_C_
#define _RMHBMAIN_C_

#include "rmhbincs.h"
#include "rmincs.h"
#ifdef NPAPI_WANTED
#include "l2iwf.h"
#include "nputil.h"
#include "cfanp.h"
#endif

/******************************************************************************
 * Function           : RmHbTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Rm HB Task main routine to initialize RM HB task parameters, 
 *                      main loop processing.
 ******************************************************************************/
VOID
RmHbTaskMain (INT1 *pArg)
{
    UINT4               u4Event;
    UINT4               u4StackingModel = 0;
    INT4                i4SrvSockFd = RM_INV_SOCK_FD;
    UINT4               u4RetVal = 0;

    UNUSED_PARAM (pArg);

    if (OsixTskIdSelf (&RM_HB_TASK_ID) != OSIX_SUCCESS)
    {
        RM_HB_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (RmHbTaskInit () == RM_FAILURE)
    {
        /* Indicate the status of initialization to main routine */
        RM_HB_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    RM_HB_INIT_COMPLETE (OSIX_SUCCESS);
    if (RmHbWaitForOtherModulesBootUp () != RM_SUCCESS)
    {
        RmHbCloseSocket (RM_HB_RAW_SOCK_FD ());
        OsixSemDel (RM_HB_PROTO_SEM ());
        RmHbTmrDeInit ();
        return;
    }

    u4StackingModel = ISS_GET_STACKING_MODEL ();
    if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        RmPostSelfAttachEvent ();
        if (RmHbWaitForSelfAttachCompletion () != RM_SUCCESS)
        {
            RmHbCloseSocket (RM_HB_RAW_SOCK_FD ());
            OsixSemDel (RM_HB_PROTO_SEM ());
            RmHbTmrDeInit ();
            return;
        }

    }
    /* If RM runs over FSIP, SLI Task has to be initialized 
       for creating Sockets . Hence initializing RM TCP Socket
       after all modules are initialized */
    u4RetVal = RmTcpSrvSockInit (&i4SrvSockFd);
    if (u4RetVal == RM_SUCCESS)
    {
        RM_TCP_SRV_SOCK_FD () = i4SrvSockFd;
    }

    /* main loop */
    while (1)
    {
        RM_HB_RECV_EVENT (RM_HB_TASK_ID, HB_MODULE_EVENTS, OSIX_WAIT, &u4Event);

        RM_HB_LOCK ();

        if (u4Event & RM_HB_PKT_RCVD)
        {
            /* Rm pkt rcvd in raw socket - 
             * processing the msgs are handled here */
            RmHbProcessRcvEvt ();
            if (SelAddFd (gRmHbInfo.i4RawSockFd, RmHbRawPktRcvd) == RM_FAILURE)
            {
                RM_HB_TRC (RM_FAILURE_TRC,
                           "Unable to add HB Raw socket with select -- Failure\n");
            }
        }
        if (u4Event & RM_HB_TMR_EXPIRY_EVENT)
        {
            /* HB module timers expiry handling */
            RmHbTmrExpHandler ();
        }

        if (u4Event & RM_HB_CHG_TO_TRANS_IN_PRGS)
        {
            /* RM core module sends this event when FSW is given */
            RmHbChgNodeStateToTransInPrgs ();
        }

        if (u4Event & RM_HB_FSW_EVENT)
        {
            /* RM core module sends this event after txing pending tx buffer */
            RmHbHandleFSWEvent ();
        }

        if (u4Event & RM_HB_SYNC_MSG_PROCESSED)
        {
            /* RM core module sends this event after proc pending RX buffer */
            RmHbHandleRxBufProcessedEvt ();
        }

        if (u4Event & RM_HB_APP_INIT_COMPLETED)
        {
            /* After backplane communication lost and restored,
             * the election determined that this node to go STANDBY.
             * Now the shutdown and start of RED enabled modules done
             * Further it determines the node state based on the
             * peer existance i.e.
             * 1. GO_ACTIVE -> If peer is dead
             * 2. GO_STANDBY -> If peer is alive */
            RmHandleAppInitCompleted ();
        }
        if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
        {
            /* Rm pkt rcvd in Ethernet port - 
             * processing the msgs are handled here */
            if (u4Event & RM_HB_PKT_RCVD_ON_ETH_PORT)
            {
                RmProcessHbMsgRcvdOnStkPort ();
            }
        }
        RM_HB_UNLOCK ();
    }
}

/******************************************************************************
 * Function           : RmPostSelfAttachEvent
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 *
 * Action             : This function is used to post RM_INIT Event to all
 *                      modules registered with RM. MBSM module on processing
 *                      this event provides Self Attach Indication.Other modules
 *                      does not handles this Event
 *
 * Returns            : None
 ******************************************************************************/
VOID
RmPostSelfAttachEvent (VOID)
{
    tRmHbMsg            RmHbMsg;

    MEMSET (&RmHbMsg, 0, sizeof (tRmHbMsg));

    RmHbMsg.u4Evt = RM_TRIGGER_SELF_ATTACH;
    RmApiSendHbEvtToRm (&RmHbMsg);
    return;

}

/******************************************************************************
=======
 * Function           : RmHbTxTsk
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Call back function invoked by cfa to tx rm hb msgs
 ******************************************************************************/
VOID
RmHbTxTsk (VOID)
{
    if (RmFormAndSendHbMsg () == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbTxTsk: " "Sending hb msg to peer failed !!!\n");
    }
}

/******************************************************************************
 * Function           : RmHbProcessRcvEvt
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received RM messages.
 ******************************************************************************/
VOID
RmHbProcessRcvEvt (VOID)
{
    UINT4               u4PeerAddr;
    UINT4               u4MsgType;
    UINT1               u1IpVerHdr;
    UINT4               u4IpHdrLen;
    UINT1               au1RcvPkt[MAX_IP_HB_PKT_LEN];
    tRmMsg             *pPkt = NULL;
    INT4                i4RecvBytes;
    UINT4               u4RmDataLen;
    UINT4               u4SrcEntId;
    UINT4               u4DestEntId;
    UINT4               u4TotLen;
    UINT2               u2Cksum;

    while ((i4RecvBytes =
            RmHbRawSockRcv (au1RcvPkt, MAX_IP_HB_PKT_LEN, &u4PeerAddr)) > 0)
    {
        /* Abort if i4RecvBytes > MAX_IP_HB_PKT_LEN */
        FSAP_ASSERT (i4RecvBytes <= MAX_IP_HB_PKT_LEN);

        /* Compare the SrcIp of the rcvd pkt and the SelfNodeIp. 
         * Drop the self-originated packets. */
        if (u4PeerAddr == RM_HB_GET_SELF_NODE_ID ())
        {
            continue;
        }

        /* Discard the pkts if the peer node IP and self node IP 
         * are not in same subnet. This is especially added to 
         * test the exe on Linux environment. i.e., to run more 
         * than 2 exes on the same m/c. 
         * Therefore configure the backplane interface IP with
         * the same subnet. 
         * N11: 127.1.0.1, N12: 127.1.0.2 - Chassis 1
         * N21: 127.2.0.1, N22: 127.2.0.2 - Chassis 2 */
        if ((u4PeerAddr & RM_HB_GET_SELF_NODE_MASK ()) !=
            (RM_HB_GET_SELF_NODE_ID () & RM_HB_GET_SELF_NODE_MASK ()))
        {
            continue;
        }

        /* Allocate memory for CRU buffer. This mem will be freed by appls. 
         * after processing. So, do not free this mem here. */
        if ((pPkt = RM_HB_ALLOC_RX_BUF (i4RecvBytes)) == NULL)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "EXIT: Unable to allocate buf\n");
            break;
        }

        /* copy the linear buffer to CRU buffer */
        if (RM_COPY (pPkt, au1RcvPkt, i4RecvBytes) == CRU_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "EXIT: Unable to copy linear to CRU buf\n");
            RM_FREE (pPkt);
            break;
        }

        RM_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = ((u1IpVerHdr & 0x0F) * 4);

        /* Received pkt has [IP Hdr + Rm Hdr + Rm Data].
         * Strip off the IP header and extract the RmMsg from the rcvd pkt */
        RM_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [RM hdr + RM data].
         * Extract the RM hdr info.  Verify checksum */
        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_TOT_LENGTH, u4TotLen);

        u2Cksum = RM_HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Pkt rcvd with invalid cksum. Discarding.... !!!\n");
            RM_FREE (pPkt);
            continue;
        }

        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_SRC_ENTID, u4SrcEntId);
        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [RM Hdr + RM data]. Strip off RM header */
        RM_PKT_MOVE_TO_DATA (pPkt, RM_HDR_LENGTH);

        u4RmDataLen = u4TotLen - RM_HDR_LENGTH;

        if (u4DestEntId >= RM_MAX_APPS)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "DestEntId invalid in rcvd pkt\n");
            RM_FREE (pPkt);
            continue;
        }

        /* Send RM data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == RM_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            RM_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == RM_HEART_BEAT_MSG)
            {
                RmProcessHbMsg (pPkt, u4RmDataLen, u4PeerAddr);
            }
            else
            {
                RM_FREE (pPkt);
            }

        }

        else
        {
            RM_FREE (pPkt);
        }
    }

}

/******************************************************************************
 * Function           : RmHbTaskInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_FAILURE/RM_SUCCESS 
 * Action             : Initialises the RM HB task related variables
 ******************************************************************************/
UINT4
RmHbTaskInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4SockFd = RM_INV_SOCK_FD;

    /* Initialize the default value of the global struct */
    if (RmHbInit () == RM_FAILURE)
    {
        u4RetVal = RM_FAILURE;
    }

    /* Open a socket connection and set the socket options to 
     * communicate with peer node */

    /* RM Heart beat messages will be transmiited over a socket only if RM 
       Stacking Interface is OOB . If Interface is an Inband ethernet port,
       Heart beat messages will be tranmitted as ethernet packets.
       So socket initializations for HB Messages  are done only if the
       stacking port is an oob port */

    if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        if (u4RetVal != RM_FAILURE)
        {
            if (RmHbRawSockInit (&i4SockFd) == RM_FAILURE)
            {
                OsixSemDel (RM_HB_PROTO_SEM ());
                u4RetVal = RM_FAILURE;
            }
            else
            {
                RM_HB_RAW_SOCK_FD () = i4SockFd;
            }
        }
    }

    /* Create timers for RM */
    if (u4RetVal != RM_FAILURE)
    {
        if (RmHbTmrInit () == RM_FAILURE)
        {
            RmHbCloseSocket (RM_HB_RAW_SOCK_FD ());
            OsixSemDel (RM_HB_PROTO_SEM ());
            u4RetVal = RM_FAILURE;
        }
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmHbInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to initialize RM HB global data structure. 
 ******************************************************************************/
UINT4
RmHbInit (VOID)
{
    tHbNodeInfo         SelfNodeInfo;
    tOsixTaskId         TempTskId;

    /* Before clearing the global info.
     * Store the RM task id in temporary variable
     * then move it to task id variable */
    TempTskId = gRmHbInfo.HbTaskId;

    MEMSET (&gRmHbInfo, 0, sizeof (tRmHbInfo));

    gu4RmStackingInterfaceType = IssGetRMTypeFromNvRam ();
    gRmHbInfo.HbTaskId = TempTskId;
    gRmHbInfo.i4RawSockFd = RM_INV_SOCK_FD;
    gRmHbInfo.u4HbTrc = RM_DEFAULT_TRC;
    gRmHbInfo.u4HbTrcBkp = gRmHbInfo.u4HbTrc;
    gRmHbInfo.u4HbInterval = RM_HB_DEFAULT_HB_INTERVAL;
    gRmHbInfo.u4PeerDeadInterval = RM_HB_DEFAULT_PEER_DEAD_INTERVAL;
    gRmHbInfo.u4PeerDeadIntMultiplier = RM_HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER;
    gRmHbInfo.u4PrevNodeState = RM_INIT;
    gRmHbInfo.u4NodeState = RM_INIT;
    gRmHbInfo.u1PeerNodeState = RM_INIT;
    /* Active node is yet to be identified */
    gRmHbInfo.u4ActiveNode = 0;

    gRmHbInfo.u4SelfNodePriority = RM_HB_DEF_PRIORITY;
    gRmHbInfo.u4PeerNodePriority = RM_HB_DEF_PRIORITY;
    /* Peer node Ip addr is not yet learnt */
    gRmHbInfo.u4PeerNode = 0;
    /* Make the FSW flag RM_FSW_NOT_OCCURED */
    gRmHbInfo.u1ForceSwitchOverFlag = RM_FSW_NOT_OCCURED;

    gRmHbInfo.u4Flags = 0;
    /* In Control plane stacking Model,Heart Beat Messages will be 
       transmitted through connecting Ethernet Port .
       A separate  Queue is created to Handle Heart Beat Messages */
    if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        if (RM_HB_CREATE_QUEUE ((UINT1 *) RM_HB_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                                MAX_RM_CTRL_MSG_NODES,
                                &(RM_HB_QUEUE_ID)) != OSIX_SUCCESS)
        {
            RM_HB_TRC (RM_CRITICAL_TRC, "Creation of RM Q Failed in RmInit\n");
            return RM_FAILURE;
        }
    }
    if (OsixCreateSem (RM_HB_PROTO_SEM_NAME, 1, 0,
                       (tOsixSemId *) & (RM_HB_PROTO_SEM ())) != OSIX_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "Rm HB Protocol Sem Creation failed in RmHbInit\n");
        return RM_FAILURE;
    }
    /* If RM Stacking Interface type is oob, RM Interface read from issnvram
       should be used */
    if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        /* Get self node ip addr and initialize in data struct */
        if (RmHbGetIfIpInfo
            ((UINT1 *) IssGetRmIfNameFromNvRam (), &SelfNodeInfo) != RM_FAILURE)
        {
            gRmHbInfo.u4SelfNode = SelfNodeInfo.u4IpAddr;
            gRmHbInfo.u4SelfNodeMask = SelfNodeInfo.u4NetMask;
        }
        else
        {
            OsixSemDel (RM_HB_PROTO_SEM ());
            return RM_FAILURE;
        }
    }
    /* If RM Stacking Interface is Inband, RM Stacking Interface read from 
       NodeID should be used for communication. */

    /* IP Address and Subnet Mask to be used should be read from Nodeid file */
    else
    {
        gRmHbInfo.u4SelfNode = IssGetRMIpAddress ();
        gRmHbInfo.u4SelfNodeMask = IssGetRMSubnetMask ();

    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmProcessHbMsgRcvdOnStkPort
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received HB messages in the 
                        connecting Stack port.
 ******************************************************************************/
VOID
RmProcessHbMsgRcvdOnStkPort (VOID)
{
    tRmMsg             *pPkt = NULL;
    UINT4               u4PeerAddr = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4IpHdrLen = 0;
    UINT4               u4RmDataLen = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;
    UINT4               u4TotLen = 0;
    UINT2               u2Cksum = 0;
    UINT1               u1IpVerHdr = 0;

    while (RM_HB_RECEIVE_FROM_QUEUE (RM_HB_QUEUE_ID, (UINT1 *) &pPkt,
                                     RM_HB_DEF_MSG_LEN, OSIX_NO_WAIT)
           == OSIX_SUCCESS)
    {
        RM_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = ((u1IpVerHdr & 0x0F) * 4);

        RM_GET_DATA_4_BYTE (pPkt, IP_PKT_OFF_SRC, u4PeerAddr);

        /* Received pkt has [IP Hdr + Rm Hdr + Rm Data].
         * Strip off the IP header and extract the RmMsg from the rcvd pkt */
        RM_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [RM hdr + RM data].
         * Extract the RM hdr info.  Verify checksum */
        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_TOT_LENGTH, u4TotLen);

        u2Cksum = RM_HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Pkt rcvd with invalid cksum. Discarding.... !!!\n");
            RM_FREE (pPkt);
            continue;
        }

        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_SRC_ENTID, u4SrcEntId);
        RM_GET_DATA_4_BYTE (pPkt, RM_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [RM Hdr + RM data]. Strip off RM header */
        RM_PKT_MOVE_TO_DATA (pPkt, RM_HDR_LENGTH);

        u4RmDataLen = u4TotLen - RM_HDR_LENGTH;

        if (u4DestEntId >= RM_MAX_APPS)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "DestEntId invalid in rcvd pkt\n");
            RM_FREE (pPkt);
            continue;
        }

        /* Send RM data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == RM_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            RM_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == RM_HEART_BEAT_MSG)
            {
                RmProcessHbMsg (pPkt, u4RmDataLen, u4PeerAddr);
            }
            else
            {
                RM_FREE (pPkt);
            }

        }

        else
        {
            RM_FREE (pPkt);
        }
    }

}

/******************************************************************************
 * Function           : RmProcessHbMsg
 * Input(s)           : pRmData - Rcvd HB msg.
 *                      u4DataLen - Length of pRmData
 *                      u4PeerAddr - Addr of the peer from which the msg arrived
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the HeartBeat msg received from peer.
 ******************************************************************************/
VOID
RmProcessHbMsg (tRmMsg * pRmData, UINT4 u4DataLen, UINT4 u4PeerAddr)
{
    UINT1               u1Event = RM_HB_MAX_EVENTS;    /* initialized to invalid value */
    UINT4               u4SelfNodeIp;
    UINT4               u4SelfNodePri;
    UINT4               u4PeerNodePri;
    BOOL1               bRestartPeerDeadTmr = OSIX_FALSE;
    tRmHbData           RmHbData;
    tRmHbMsg            RmHbMsg;
    tRegPeriodicTxInfo  RegnInfo;

    UNUSED_PARAM (u4DataLen);

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));

    /* Copy the CRU buf content to linear buffer and free the CRU buf. */
    if (RM_HB_COPY_TO_LINEAR_BUF (pRmData, &RmHbData, 0, sizeof (tRmHbData))
        == CRU_FAILURE)
    {
        RM_HB_TRC (RM_CRITICAL_TRC, "CRU to linear copy failure\n");
        RM_FREE (pRmData);
        return;
    }

    RM_FREE (pRmData);

    /* Convert the rcvd msg from NETWORK byte order to HOST byte order */
    RmHbData.u4MsgType = OSIX_NTOHL (RmHbData.u4MsgType);
    RmHbData.u4HbInterval = OSIX_NTOHL (RmHbData.u4HbInterval);
    RmHbData.u4PeerDeadInterval = OSIX_NTOHL (RmHbData.u4PeerDeadInterval);
    RmHbData.u4PeerDeadIntMultiplier =
        OSIX_NTOHL (RmHbData.u4PeerDeadIntMultiplier);
    RmHbData.u4ActiveNode = OSIX_NTOHL (RmHbData.u4ActiveNode);
    RmHbData.u4SelfNode = OSIX_NTOHL (RmHbData.u4SelfNode);
    RmHbData.u4PeerNode = OSIX_NTOHL (RmHbData.u4PeerNode);
    RmHbData.u4SelfNodePriority = OSIX_NTOHL (RmHbData.u4SelfNodePriority);
    RmHbData.u4PeerNodePriority = OSIX_NTOHL (RmHbData.u4PeerNodePriority);
    RmHbData.u4Flags = OSIX_NTOHL (RmHbData.u4Flags);

    /* If there is a change in HeartBeat/PeerDead interval in the HB msg rcvd 
     * from ACTIVE node, change the values accordingly in peer node 
     * to be in sync. */
    /* Allow to change the HB interval and peer dead interval 
     * only when the proper values (refer the below "if" check) 
     * are obtained in the HB message. */
    if ((RM_HB_GET_NODE_STATE () != RM_ACTIVE) &&
        (!((RmHbData.u4ActiveNode == 0) &&
           (RmHbData.u4PeerNode /* 0 */  != RM_HB_GET_SELF_NODE_ID ()) &&
           (RmHbData.u4SelfNode == RM_HB_GET_PEER_NODE_ID ()) &&
           (RM_HB_GET_NODE_STATE () == RM_STANDBY))))
    {
        if (RmHbData.u4HbInterval != RM_HB_GET_HB_INTERVAL ())
        {
            RM_HB_TRC1 (RM_CRITICAL_TRC,
                        "!!! Heart Beat Interval of ACTIVE node is modified. "
                        "Updating the Heart Beat Interval to %dms ...\n",
                        RmHbData.u4HbInterval);
            RM_HB_SET_HB_INTERVAL (RmHbData.u4HbInterval);

            /* HB Timer to be restarted only when hb interval is > 10 ms */
            /* HB module to register with CFA only when hb interval is = 10 ms */
            if (RM_HB_GET_HB_INTERVAL () == RM_HB_MIN_HB_INTERVAL)
            {
                RegnInfo.SendToApplication = RmHbTxTsk;
                CfaRegisterPeriodicPktTx (CFA_APP_RM, &RegnInfo);
            }
            else
            {
                CfaDeRegisterPeriodicPktTx (CFA_APP_RM);
                if (RmHbTmrRestartTimer (RM_HB_HEARTBEAT_TIMER,
                                         RM_HB_GET_HB_INTERVAL ()) ==
                    RM_FAILURE)
                {
                    RM_HB_TRC (RM_FAILURE_TRC,
                               "!!! Heart Beat re-start timer Failure !!!\n");
                }
            }
        }
        if (RmHbData.u4PeerDeadInterval != RM_HB_GET_PEER_DEAD_INTERVAL ())
        {
            RM_HB_TRC1 (RM_CRITICAL_TRC,
                        "!!! Peer Dead Interval of ACTIVE node is modified. "
                        "Updating the Peer Dead Interval to %dms ...\n",
                        RmHbData.u4PeerDeadInterval);
            RM_HB_SET_PEER_DEAD_INTERVAL (RmHbData.u4PeerDeadInterval);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }

        if (RmHbData.u4PeerDeadIntMultiplier !=
            RM_HB_GET_PEER_DEAD_INT_MULTIPLIER ())
        {
            RM_HB_TRC1 (RM_CRITICAL_TRC,
                        "!!! Peer Dead Interval Multiplier of ACTIVE node is "
                        "modified. Updating the Peer Dead Interval "
                        "Multiplier to %d ...\n",
                        RmHbData.u4PeerDeadIntMultiplier);
            RM_HB_SET_PEER_DEAD_INT_MULTIPLIER (RmHbData.
                                                u4PeerDeadIntMultiplier);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }
    }
    if (bRestartPeerDeadTmr == OSIX_TRUE)
    {
        if (RmHbTmrRestartTimer (RM_HB_PEER_DEAD_TIMER,
                                 RM_HB_GET_PEER_DEAD_INTERVAL ()) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Peer Dead restart timer Failure !!!\n");
            return;
        }
    }

    /* ACTIVE NODE CHECK,  
     * Below 'if' check is added to detect 
     * whether the peer is dead or not when the STANDBY node reboots 
     * in less than peer dead time. */
    if ((RmHbData.u4ActiveNode == 0) &&
        (RmHbData.u4PeerNode /* 0 */  != RM_HB_GET_SELF_NODE_ID ()) &&
        (RmHbData.u4SelfNode == RM_HB_GET_PEER_NODE_ID ()) &&
        (RM_HB_GET_NODE_STATE () == RM_ACTIVE))
    {
        /* STANDBY node is rebooted before ACTIVE detects 
         * the peer is dead */
        RM_HB_TRC (RM_FAILURE_TRC,
                   "!!! STANDBY node is rebooted before ACTIVE detects "
                   "the peer is dead !!!\n");
        RmHbTmrHdlPeerDeadTmrExp (NULL);
    }
    /* Update the Peer node ip addr. i.e., IpAddr of the node 
     * from which the pkt is rcvd */
    RM_HB_SET_PEER_NODE_ID (u4PeerAddr);

    /* Read the NodeId and Priority information. */
    u4SelfNodeIp = RM_HB_GET_SELF_NODE_ID ();
    u4SelfNodePri = RM_HB_GET_SELF_NODE_PRIORITY ();
    u4PeerNodePri = RM_HB_GET_PEER_NODE_PRIORITY ();

    /* Below 'if' check is added to detect 
     * whether the peer is dead or not when the ACTIVE node reboots 
     * in less than peer dead time. */
    if ((RmHbData.u4ActiveNode == 0) &&
        (RmHbData.u4PeerNode /* 0 */  != RM_HB_GET_SELF_NODE_ID ()) &&
        (RmHbData.u4SelfNode == RM_HB_GET_PEER_NODE_ID ()) &&
        (RM_HB_GET_NODE_STATE () == RM_STANDBY))
    {
        /* STANDBY NODE CHECK 
         * ACTIVE node is rebooted before STANDBY detects the 
         * peer is dead*/
        RM_HB_TRC (RM_FAILURE_TRC,
                   "!!! ACTIVE node is rebooted before STANDBY detects "
                   "the peer is dead !!!\n");
        RmHbTmrHdlPeerDeadTmrExp (NULL);
        u1Event = RM_HB_IGNORE_PKT;
    }
    else if (RmHbData.u4PeerNode == 0)
    {
        /* Peer node has not learnt about me. 
         * But this node has seen the peer node */
        u1Event = RM_HB_1WAY_RCVD;
    }
    else if (RmHbData.u4Flags & RM_FORCE_SWITCHOVER)
    {
        /* If there is force-switchover msg from peer, force this
         *          * node to become ACTIVE. */
        u1Event = RM_HB_FSW_RCVD;
    }
    else if (RmHbData.u4Flags & RM_FORCE_SWITCHOVER_ACK)
    {
        u1Event = RM_HB_FSW_ACK_RCVD;
    }
    else if ((RmHbData.u4Flags & RM_TRANS_IN_PROGRESS_FLAG) ||
             (RM_HB_GET_NODE_STATE () == RM_TRANSITION_IN_PROGRESS))
    {
        /* Don't allow any operation when HB message with 
         * "transition in progress" flag is received */
        u1Event = RM_HB_IGNORE_PKT;
    }
    else if ((RmHbData.u4Flags & RM_STANDBY_REBOOT) &&
             (RM_HB_GET_NODE_STATE () != RM_ACTIVE))
    {
        RmApiSystemRestart ();
    }
    else if (RmHbData.u4Flags & RM_STDBY_PROTOCOL_RESTART)
    {
        /* When the active node receives a RM_STDBY_PROTOCOL_RESTART flag,
         * the active node internally sends a peer down event to its
         * protocols so that they stop sending the static and dynamic
         * update messages. This is done because the standby node cannot
         * process these update messages, as the protocols are restarting
         * and will obtain all the configurations in the bulk update 
         * messages after the protocols have restarted.
         * Also, the active node sends a acknowledge message to the standby node.
         * When the standby node receives a RM_STDBY_PROTOCOL_RESTART flag,
         * the active node would already stopped all the sync msg 
         * transmission. Hence, send a acknowledge message and start
         * the standby software reboot. */
        if (RM_HB_GET_NODE_STATE () == RM_ACTIVE)
        {
            if (gRmHbInfo.u1StdbySoftRbt == RM_HB_STDBY_SOFT_RBT_NOT_OCCURED)
            {
                RmHbMsg.u4PeerAddr = RM_HB_GET_PEER_NODE_ID ();
                RmHbMsg.u4Evt = RM_PEER_DOWN;
                RmApiSendHbEvtToRm (&RmHbMsg);
                gRmHbInfo.u1StdbySoftRbt = RM_HB_STDBY_SOFT_RBT_NODE_DOWN;
            }
            if (RmSendMsgToPeer (RM_STDBY_PROTOCOL_RESTART_ACK) == RM_FAILURE)
            {
                RM_HB_TRC (RM_FAILURE_TRC, "Sending Standby node "
                           "software reboot flag failed !!!\n");
            }
        }
        else
        {
            if (RmSendMsgToPeer (RM_STDBY_PROTOCOL_RESTART_ACK) == RM_FAILURE)
            {
                RM_HB_TRC (RM_FAILURE_TRC, "Sending Standby node software "
                           "reboot flag failed !!!\n");
            }
            if (gRmHbInfo.u1StdbySoftRbt == RM_HB_STDBY_SOFT_RBT_NOT_OCCURED)
            {
                gRmHbInfo.u1StdbySoftRbt = RM_HB_STDBY_SOFT_RBT_NODE_DOWN;
                /* As the active node initiated the standby node's soft reboot
                 * increment the reboot counter  and then initiate the protocol
                 * restart */
                RmApiIncrementRebootCntr ();
                RmApiAllProtocolRestart ();
            }
        }
    }
    else if (RmHbData.u4Flags & RM_STDBY_PROTOCOL_RESTART_ACK)
    {
        /* When the STANDBY_SOFT_REBOOT acknowledge is received, 
         * reset the STANDBY_SOFT_REBOOT flag in HB message and stop
         * the timer. In addition, the standby node starts its
         * Software reboot. */
        RM_HB_RESET_FLAGS (RM_STDBY_PROTOCOL_RESTART);

        if (gRmHbInfo.u1StdbySoftRbt == RM_HB_STDBY_SOFT_RBT_NOT_OCCURED)
        {
            if (RM_HB_GET_NODE_STATE () != RM_ACTIVE)
            {
                RmApiAllProtocolRestart ();
            }
            gRmHbInfo.u1StdbySoftRbt = RM_HB_STDBY_SOFT_RBT_NODE_DOWN;
        }
    }
    else if ((RmHbData.u4Flags & RM_STDBY_PEER_UP) &&
             (RM_HB_GET_NODE_STATE () == RM_ACTIVE))
    {
        /* Active received a standby peer up message. Send the acknowledge */
        if (gRmHbInfo.u1StdbySoftRbt == RM_HB_STDBY_SOFT_RBT_NODE_DOWN)
        {
            RmHbMsg.u4PeerAddr = RM_HB_GET_PEER_NODE_ID ();
            RmHbMsg.u4Evt = RM_PEER_UP;
            RmApiSendHbEvtToRm (&RmHbMsg);
            gRmHbInfo.u1StdbySoftRbt = RM_HB_STDBY_SOFT_RBT_NOT_OCCURED;
        }
        if (RmSendMsgToPeer (RM_STDBY_PEER_UP_ACK) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "Sending Standby node software "
                       "reboot flag failed !!!\n");
        }
    }
    else if ((RmHbData.u4Flags & RM_STDBY_PEER_UP_ACK) &&
             (RM_HB_GET_NODE_STATE () != RM_ACTIVE))
    {
        if (gRmHbInfo.u1StdbySoftRbt == RM_HB_STDBY_SOFT_RBT_NODE_DOWN)
        {
            RmApiInitiateBulkRequest ();
            gRmHbInfo.u1StdbySoftRbt = RM_HB_STDBY_SOFT_RBT_NOT_OCCURED;
        }
        RM_HB_RESET_FLAGS (RM_STDBY_PEER_UP);
    }
    else if (RmHbData.u4PeerNode == u4SelfNodeIp)
    {
        /* Rcvd pkt's peer node has my ip addr. Therefore, 
         * peer node has learnt about me & I have seen the peer node. */

        /* If both the nodes priorities are same, and are not 0, 
         * go for normal election based on Priority/NodeId.
         * Node with Priority = 0, is not eligible for election. */
        if ((RmHbData.u4SelfNodePriority == u4PeerNodePri) &&
            (RmHbData.u4PeerNodePriority == u4SelfNodePri))
        {
            /* There is no change in node priority. */
            if (u4SelfNodePri != 0)
            {
                RmProcessNodeId (&RmHbData, &u1Event);
            }
        }
        else
        {
            /* There is a change in node priority. */
            RmProcessNodePriority (&RmHbData, &u1Event);
        }
    }
    else
    {
        /* Discard this packet */
        u1Event = RM_HB_IGNORE_PKT;
        RM_HB_TRC (RM_FAILURE_TRC,
                   "!!! HB rcvd from invalid Peer. Discarding.... !!!\n");
        return;
    }

    RM_HB_TRC2 (RM_SEM_TRC, "FSM Event: %d  State: %d\n",
                u1Event, RM_HB_GET_NODE_STATE ());
    /* StateMachine is initiated only if the event is valid */
    if (u1Event != RM_HB_MAX_EVENTS)
    {
        RmStateMachine (u1Event);
    }
    if (RmHbTmrRestartTimer (RM_HB_PEER_DEAD_TIMER,
                             RM_HB_GET_PEER_DEAD_INTERVAL ()) == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "!!! Peer Dead restart timer Failure !!!\n");
    }
}

/******************************************************************************
 * Function           : RmProcessNodeId
 * Input(s)           : pRmHbData - Rcvd RM msg,
 * Output(s)          : u1Event - Event to be set based on the msg.
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : Routine to process the NodeId information of 
 *                      HeartBeat msg.
 ******************************************************************************/
UINT4
RmProcessNodeId (tRmHbData * pRmHbData, UINT1 *pu1Event)
{

    if (RM_HB_GET_ACTIVE_NODE_ID () == 0)
    {
        /* This node doesn't know about ACTIVE node. */
        if (pRmHbData->u4ActiveNode == 0)
        {
            /* This node doesn't know about ACTIVE node. 
             * Peer doesn't know about ACTIVE node.
             * Active node is not yet elected. Start the election */
            *pu1Event = RM_HB_2WAY_RCVD;
        }
        else
        {
            /* If the active node id is not known and 
             * the received HB message has self node id as active node id 
             * then skip the HB message. 
             * This situation comes only when the STANDBY node 
             * does not detect the peer is dead */
            if (RM_HB_GET_NODE_STATE () == RM_INIT)
            {
                if (RM_HB_GET_SELF_NODE_ID () == pRmHbData->u4ActiveNode)
                {
                    RM_HB_TRC (RM_FAILURE_TRC,
                               "ACTIVE node is rebooted before STANDBY detects "
                               "the peer is dead !!!\n");
                    *pu1Event = RM_HB_IGNORE_PKT;
                    return (RM_SUCCESS);
                }
            }
            /* This node deosn't know about ACTIVE node. 
             * Peer knows about Active node. Do not do the election.
             * Reuse the same ACTIVE node. */
            RM_HB_SET_ACTIVE_NODE_ID (pRmHbData->u4ActiveNode);
            RM_HB_TRC1 (RM_SEM_TRC, "ACTIVE NODE elected: %0x\n",
                        RM_HB_GET_ACTIVE_NODE_ID ());

            RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_UP);
            *pu1Event = RM_HB_ACTV_ELCTD;
        }
    }
    else                        /* gRmHbInfo.u4ActiveNode != 0 */
    {
        /* This node knows the ACTIVE Node. */
        if (pRmHbData->u4ActiveNode == 0)
        {
            /* This node knows about ACTIVE node. 
             * Peer doesn't know about ACTIVE. */
            *pu1Event = RM_HB_2WAY_RCVD;
        }
        else if (RM_HB_GET_ACTIVE_NODE_ID () != pRmHbData->u4ActiveNode)
        {
            /* rcvd pkts active node != current active node, i.e.,
             * backplane communication is lost for a while and so both
             * the nodes declare themselves as 'active'. Now, when the backplane
             * communication comes up, the active node will not be the same. 
             * Start re-election here. */

            RM_HB_SET_ACTIVE_NODE_ID (0);
            RM_HB_SET_PEER_NODE_ID (0);
            RM_HB_SET_PREV_NODE_STATE (RM_HB_GET_NODE_STATE ());
            RM_HB_SET_NODE_STATE (RM_INIT);
            *pu1Event = RM_HB_1WAY_RCVD;
        }
        else
        {
            /* Peer node has learnt the active node info. */

            /* Update the PEER node state and notify the protocols */
            RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_UP);
            *pu1Event = RM_HB_2WAY_RCVD;
        }
    }
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmProcessNodePriority
 * Input(s)           : pRmHbData - Rcvd RM msg,
 * Output(s)          : u1Event - Event to be set based on the msg.
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : Routine to process the NodePriority information of 
 *                      HeartBeat msg.
 ******************************************************************************/
UINT4
RmProcessNodePriority (tRmHbData * pRmHbData, UINT1 *pu1Event)
{
    UINT4               u4SelfNodePri = RM_HB_GET_SELF_NODE_PRIORITY ();
    UINT4               u4PeerNodePri = RM_HB_GET_PEER_NODE_PRIORITY ();

    if (u4SelfNodePri == 0)
    {
        /* Do not participate in election. Do not process HeartBeat
         * messages, stay in INIT state. */
        RM_HB_SET_ACTIVE_NODE_ID (0);
        RM_HB_SET_NODE_STATE (RM_INIT);
        *pu1Event = RM_HB_IGNORE_PKT;
    }
    else if (u4PeerNodePri != pRmHbData->u4SelfNodePriority)
    {
        /* There is a change in node priority */
        /* Update the priority of peer node */
        RM_HB_SET_PEER_NODE_PRIORITY (pRmHbData->u4SelfNodePriority);

        /* Start re-election */
        RM_HB_SET_ACTIVE_NODE_ID (0);
        *pu1Event = RM_HB_PRIORITY_CHG;
    }
    else if (u4SelfNodePri != pRmHbData->u4PeerNodePriority)
    {
        *pu1Event = RM_HB_PRIORITY_CHG;
    }

    /* Update the priority of peer node */
    RM_HB_SET_PEER_NODE_PRIORITY (pRmHbData->u4SelfNodePriority);

    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmFormAndSendHbMsg 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to form heart-beat message and send it to the 
 *                      peer. 
 ******************************************************************************/
UINT4
RmFormAndSendHbMsg (VOID)
{
    tRmHbData           HbData;
    tRmMsg             *pRmMsg;
    UINT4               u4RetVal = RM_SUCCESS;

    /* Allocate memory for RM msg. */
    if ((pRmMsg = RM_HB_ALLOC_TX_BUF (sizeof (tRmHbData))) == NULL)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmFormAndSendHbMsg: RM alloc failed !!!\n");
        return (RM_FAILURE);
    }

    /* Form HeartBeat msg */
    RmFormHbMsg (&HbData);

    HbData.u4Flags = OSIX_HTONL (RM_HB_GET_FLAGS ());
    /* Copy HB data to the appropriate offset by leaving space for RM hdr */
    if ((RM_COPY_TO_OFFSET (pRmMsg, &HbData, 0, sizeof (tRmHbData)))
        == CRU_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmFormAndSendHbMsg: RM copy to CRU buf failed !!!\n");
        u4RetVal = RM_FAILURE;
    }

    if (u4RetVal != RM_FAILURE)
    {
        u4RetVal = RmHbPrependRmHdr (pRmMsg, sizeof (tRmHbData));
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Send the pkt out to peer thru' RAW socket */
        if (RmHbSendMsg (pRmMsg, RM_HB_MCAST_IPADDR) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! RmFormAndSendHbMsg: RmSendMsg -- Failed !!!\n");
            u4RetVal = RM_FAILURE;
        }
    }
    else
    {
        RM_FREE (pRmMsg);
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmFormHbMsg
 * Input(s)           : pHbData - ptr to RmHBMsg
 * Output(s)          : None. 
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : Routine to form the HeartBeat msg.
 ******************************************************************************/
UINT4
RmFormHbMsg (tRmHbData * pHbData)
{
    MEMSET (pHbData, 0, sizeof (tRmHbData));

    pHbData->u4MsgType = OSIX_HTONL (RM_HEART_BEAT_MSG);
    pHbData->u4HbInterval = OSIX_HTONL (RM_HB_GET_HB_INTERVAL ());
    pHbData->u4PeerDeadInterval = OSIX_HTONL (RM_HB_GET_PEER_DEAD_INTERVAL ());
    pHbData->u4PeerDeadIntMultiplier =
        OSIX_HTONL (RM_HB_GET_PEER_DEAD_INT_MULTIPLIER ());
    pHbData->u4ActiveNode = OSIX_HTONL (RM_HB_GET_ACTIVE_NODE_ID ());
    pHbData->u4SelfNode = OSIX_HTONL (RM_HB_GET_SELF_NODE_ID ());
    pHbData->u4PeerNode = OSIX_HTONL (RM_HB_GET_PEER_NODE_ID ());
    pHbData->u4SelfNodePriority = OSIX_HTONL (RM_HB_GET_SELF_NODE_PRIORITY ());
    pHbData->u4PeerNodePriority = OSIX_HTONL (RM_HB_GET_PEER_NODE_PRIORITY ());

    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmHbWaitForOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : This routine is used to hold the RM protocol operations
 *                      till other modules completes its initialization.
 ******************************************************************************/
INT4
RmHbWaitForOtherModulesBootUp (VOID)
{
    UINT4               u4Event;

    while (1)
    {
        RM_HB_RECV_EVENT (RM_HB_TASK_ID, RM_HB_RESUME_EVENT, OSIX_WAIT,
                          &u4Event);

        if (u4Event & RM_HB_RESUME_EVENT)
        {
            if (RmResumeProtocolOperation () != RM_SUCCESS)
            {
                return RM_FAILURE;
            }
            break;
        }
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbWaitForSelfAttachCompletion 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : This routine is used to hold the RM protocol operations
 *                      till Self Attach is handled by MBSM
 ******************************************************************************/
INT4
RmHbWaitForSelfAttachCompletion (VOID)
{
    UINT4               u4Event;

    while (1)
    {
        RM_HB_RECV_EVENT (RM_HB_TASK_ID, RM_HB_RESUME_EVENT, OSIX_WAIT,
                          &u4Event);

        if (u4Event & RM_HB_RESUME_EVENT)
        {
            break;
        }
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbInformOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine is used to inform RM HB module about the 
 *                      completion of other modules intialization.
 ******************************************************************************/
VOID
RmHbInformOtherModulesBootUp (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);
    if (RM_HB_SEND_EVENT (RM_HB_TASK_ID, RM_HB_RESUME_EVENT) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC,
                "SendEvent failed in RmHbInformOtherModulesBootUp\n");

    }
    /* Indicate the status of initialization to main routine */
    return;
}

/******************************************************************************
 * Function           : RmHbPrependRmHdr
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to form RM header and prepend it to the 
 *                      RM msg.
 ******************************************************************************/
UINT4
RmHbPrependRmHdr (tRmMsg * pRmMsg, UINT4 u4Size)
{
    tRmHdr              RmHdr;
    UINT2               u2Cksum;
    UINT4               u4RetVal = RM_SUCCESS;
    INT4                i4RetVal = CRU_FAILURE;

    /* From RM hdr */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_HB_VERSION_NUM);
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u4Size));
    RmHdr.u4MsgType = OSIX_HTONS (0);    /* REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONS (0);

    /* Prepend RM hdr to HB data */
    if ((RM_HB_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        u4RetVal = RM_FAILURE;
    }

    /* Calculate the checksum and fill-in here */
    u2Cksum = RM_HB_CALC_CKSUM (pRmMsg, (RM_HDR_LENGTH + u4Size), 0);

    RM_HB_DATA_PUT_2_BYTE (pRmMsg, RM_HDR_OFF_CKSUM, u2Cksum, i4RetVal);
    if (i4RetVal != CRU_SUCCESS)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "Update Cksum in RM hdr - failed\n");
        u4RetVal = RM_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmSendMsg
 * Input(s)           : pRmMsg - Msg to be sent.
 *                      u4DestAddr - Dest addr to which the msg has to be sent
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to send out the RM msgs (which are arrived from 
 *                      applications) to the peer.
*******************************************************************************/
UINT4
RmHbSendMsg (tRmMsg * pRmMsg, UINT4 u4DestAddr)
{
    UINT4               u4RetVal = RM_SUCCESS;
    UINT1               au1Data[RM_HB_SYNC_PKT_LEN + IP_HDR_LEN];
    UINT4               u4RmPktLen;
    UINT4               u4TotLen;
    t_IP_HEADER        *pIpHdr;
#ifdef NPAPI_WANTED
    tHwInfo             HwInfo;
#endif
    UINT2               u2Cksum;

    /* Get the length of RM pkt to be sent */
    RM_GET_DATA_4_BYTE (pRmMsg, RM_HDR_OFF_TOT_LENGTH, u4RmPktLen);

    /* Total length of pkt to be sent includes IP hdr length */
    u4TotLen = u4RmPktLen + IP_HDR_LEN;

    /* Form IP hdr as IP_HDRINCL option is set for the socket. */
    pIpHdr = (t_IP_HEADER *) (VOID *) au1Data;

    pIpHdr->u1Ver_hdrlen = (UINT1) IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Totlen = OSIX_HTONS (u4TotLen);
    pIpHdr->u2Id = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 1;
    pIpHdr->u1Proto = RM_HB_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = OSIX_HTONL (RM_HB_GET_SELF_NODE_ID ());
    pIpHdr->u4Dest = OSIX_HTONL (u4DestAddr);
    u2Cksum = RM_HB_LINEAR_CALC_CKSUM ((CONST INT1 *) pIpHdr, IP_HDR_LEN);
    pIpHdr->u2Cksum = OSIX_HTONS (u2Cksum);

    /* Copy the RM info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (RM_HB_COPY_TO_LINEAR_BUF (pRmMsg, au1Data + IP_HDR_LEN, 0, u4RmPktLen)
        == CRU_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "Copy from CRU buf to linear buf failed\n");
        u4RetVal = RM_FAILURE;
    }

    RM_FREE (pRmMsg);

    if (u4RetVal != RM_FAILURE)
    {
#ifdef NPAPI_WANTED
        /* RM Heart beat message channel should be decided based on RM Port
         * Type. If it is out of Band port, HB messages will be tranmitted 
         * through socket on OOB port running over Linux IP.
         * If it is an Inband Port,HB messages will be tranmitted through 
         * socket on stacking port running over Aricent IP */
        if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_INBAND)
        {
            MEMSET (&HwInfo, 0, sizeof (tHwInfo));
            HwInfo.u4MessageType = RM_HB_PKT;
            HwInfo.uHwMsgInfo.HwGenMsgInfo.pu1Data = au1Data;
            HwInfo.u4PktSize = u4TotLen;

            if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
            {
                RM_HB_TRC (RM_FAILURE_TRC,
                           "!!! FsCfaHwSendIPCMsg - failure !!!\n");
                u4RetVal = RM_FAILURE;
            }

            return (u4RetVal);

        }
#endif
        if (RmHbRawSockSend (au1Data, (UINT2) u4TotLen, u4DestAddr) ==
            RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "!!! RmSockSend - failure !!!\n");
            u4RetVal = RM_FAILURE;
        }
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmSendMsgToPeer
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to send message to peer.
 *                      e.g, force-switchover command on ACTIVE node, 
 *                      should force the ACTIVE node to STANDBY state and 
 *                      the STANDBY node to ACTIVE state.
 *                      Active node will send a msg to STANDBY to switch to
 *                      ACTIVE state.
 ******************************************************************************/
UINT4
RmSendMsgToPeer (UINT4 u4Msg)
{
    tRmHbData           HbData;
    tRmMsg             *pRmMsg;
    UINT4               u4RetVal = RM_SUCCESS;

    /* Allocate memory for RM msg. */
    if ((pRmMsg = RM_HB_ALLOC_TX_BUF (sizeof (tRmHbData))) == NULL)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RM alloc failed !!!\n");
        return (RM_FAILURE);
    }

    /* Form HeartBeat msg */
    RmFormHbMsg (&HbData);

    /* Set the bitmap to indicate that this is a force-switchover msg */
    HbData.u4Flags = OSIX_HTONL (u4Msg);

    /* Copy HB data to the appropriate offset by leaving space for RM hdr */
    if ((RM_COPY_TO_OFFSET (pRmMsg, &HbData, 0, sizeof (tRmHbData)))
        == CRU_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RM copy to CRU buf failed !!!\n");
        u4RetVal = RM_FAILURE;
    }

    if (u4RetVal != RM_FAILURE)
    {
        u4RetVal = RmHbPrependRmHdr (pRmMsg, sizeof (tRmHbData));
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Send the pkt out to peer thru' RAW socket */
        if (RmHbSendMsg (pRmMsg, RM_HB_MCAST_IPADDR) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "!!! RmSendMsg -- Failed !!!\n");
        }
    }
    else
    {
        RM_FREE (pRmMsg);
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmHbHandleFSWEvent
 * Input(s)           : None 
 * Output(s)          : None 
 * Returns            : None 
 * Action             : Sends the HB message with RM_FORCE_SWITCHOVER flag set. 
 ******************************************************************************/
VOID
RmHbHandleFSWEvent ()
{
    tRmNotificationMsg  NotifMsg;
    tRmHbMsg            RmHbMsg;

    /* ACTIVE side processing */
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbHandleFSWEvent ENTRY\r\n");
    RM_HB_TRC (RM_CTRL_PATH_TRC,
               "RmHbHandleFSWEvent: Force switchover event from "
               "ACTIVE RM core module\r\n");

    if ((RM_HB_GET_NODE_STATE () != RM_TRANSITION_IN_PROGRESS) &&
        (RM_HB_GET_PREV_NODE_STATE () != RM_ACTIVE))
    {
        MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
        NotifMsg.u4NodeId = RM_HB_GET_SELF_NODE_ID ();
        NotifMsg.u4State = RM_ACTIVE;
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
        NotifMsg.u4Status = RM_FAILED;
        NotifMsg.u4Error = RM_NONE;
        UtlGetTimeStr (NotifMsg.ac1DateTime);
        STRCPY (NotifMsg.ac1ErrorStr,
                "Force-Switchover is failed as RM HB node state "
                "is not in transition in progress");
        RmApiTrapSendNotifications (&NotifMsg);
        RmHbMsg.u4Evt = RM_FSW_FAILED_IN_ACTIVE;
        RmApiSendHbEvtToRm (&RmHbMsg);
        return;
    }
    if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        RM_HB_TRC (RM_CTRL_PATH_TRC,
                   "RmHbHandleFSWEvent: "
                   "Force switchover in progress state\r\n");
        return;
    }
    if (RM_HB_GET_PEER_NODE_ID () == 0)
    {
        /* Peer is dead but Force switchover event is received 
         * from RM core module */
        RM_HB_TRC (RM_CTRL_PATH_TRC,
                   "RmHbHandleFSWEvent: " "STANDBY is dead\r\n");
        return;
    }
    RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
    RM_HB_SET_FLAGS (RM_FORCE_SWITCHOVER);

    if (RmFormAndSendHbMsg () == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbHandleFSWEvent: "
                   "Sending force-switchover-ack to peer failed !!!\n");
    }
    if (RmHbTmrStartTimer (RM_HB_FSW_ACK_TIMER,
                           RM_HB_MAX_FSW_ACK_TIME) == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbHandleFSWEvent: FSW ACK timer start failed\n");
        return;
    }
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbHandleFSWEvent EXIT\r\n");
}

/******************************************************************************
 * Function           : RmHbHandleRxBufProcessedEvt 
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine performs the following
 *                      1. Force Switchover handle after processing RX messages
 *                      2. Failover handle after processing RX messages
 *                      finally changes the node state to ACTIVE.
 ******************************************************************************/
VOID
RmHbHandleRxBufProcessedEvt (VOID)
{
    tRmHbMsg            RmHbMsg;

    /* STANDBY side processing */
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbHandleRxBufProcessedEvt ENTRY\r\n");

    if ((RM_HB_GET_NODE_STATE () != RM_TRANSITION_IN_PROGRESS) &&
        (RM_HB_GET_PREV_NODE_STATE () != RM_STANDBY))
    {
        RM_HB_TRC (RM_CRITICAL_TRC, "RM HB module is not in "
                   "RM_TRANSITION_IN_PROGRESS state after processing "
                   "RX buffer message in RM core module\n");
        return;
    }
    RM_HB_RESET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);

    RM_HB_SET_ACTIVE_NODE_ID (RM_HB_GET_SELF_NODE_ID ());
    if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
        RmHbMsg.u4Evt = RM_FSW_RCVD_IN_STANDBY;
        RmApiSendHbEvtToRm (&RmHbMsg);
        if (RmSendMsgToPeer (RM_FORCE_SWITCHOVER_ACK) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "!!! Sending fore-switchover-ack to peer failed !!!\n");
        }
    }
    else if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_NOT_OCCURED)
    {
        /* 1. In Failover case, this event is received after processing the 
         * Rx buffer messages to give GO_ACTIVE to RM core module.
         * 2. When FSW flag is received in HB msg and indication to 
         * RM core module given for RX buffer processing, 
         * before getting the Rx buffer processed event from
         * RM core module, peer dead OR FSW RX buffer processing timer expiry 
         * might have set 
         * RM_HB_FORCE_SWITCHOVER_FLAG () as RM_FSW_NOT_OCCURED */
    }
    RmUpdateState (RM_ACTIVE);
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbHandleRxBufProcessedEvt EXIT\r\n");
}

/******************************************************************************
 * Function           : RmHbNotifyPeerInfo
 * Input(s)           : u1Event - Events like RM_STANDBY_UP/RM_STANDBY_DOWN.
 * Output(s)          : None.
 * Returns            : Nome. 
 * Action             : Routine used by RM to notify Standby state and 
 *                      peer address to protocols.
 ******************************************************************************/
UINT4
RmHbNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event)
{
    tRmHbMsg            RmHbMsg;

    /* Peer count need to maintained irrespective of the Node status */
    if (RM_HB_GET_PEER_NODE_STATE () != u1Event)
    {
        RM_HB_SET_PEER_NODE_STATE (u1Event);
        RM_HB_TRC2 (RM_CTRL_PATH_TRC, "RmHbNotifyPeerInfo: Peer id=%x "
                    "and status=%d\r\n", u4PeerAddr, u1Event);
        RmHbMsg.u4PeerAddr = u4PeerAddr;
        RmHbMsg.u4Evt = u1Event;
        RmApiSendHbEvtToRm (&RmHbMsg);
        /* Send peer up/down indication to RM task */
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbChgNodeStateToTransInPrgs
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None 
 * Action             : Routine to change RM HB level node state to 
 *                      RM_TRANSITION_IN_PROGRESS.
 ******************************************************************************/
VOID
RmHbChgNodeStateToTransInPrgs (VOID)
{
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbChgNodeStateToTransInPrgs: ENTRY \r\n");
    if (RM_HB_GET_PEER_NODE_ID () != 0)
    {
        RM_HB_SET_PREV_NODE_STATE (RM_HB_GET_NODE_STATE ());
        RM_HB_SET_NODE_STATE (RM_TRANSITION_IN_PROGRESS);
        RM_HB_SET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
    }
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbChgNodeStateToTransInPrgs: EXIT \r\n");
}

/*****************************************************************************/
/* Function Name      : RmHbLock                                             */
/*                                                                           */
/* Description        : This function is used to take the RM HB mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : RMGR, Protocols registered with RMGR & SNMP/CLI      */
/*****************************************************************************/
INT4
RmHbLock (VOID)
{
    if (OsixSemTake (RM_HB_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        RM_HB_TRC (RM_CRITICAL_TRC, "Failed to Take RM HB Protocol "
                   "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmHbUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the RM HB mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : RMGR, Protocols registered with RMGR & SNMP/CLI      */
/*****************************************************************************/
INT4
RmHbUnLock (VOID)
{
    if (OsixSemGive (RM_HB_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        RM_HB_TRC (RM_CRITICAL_TRC, "Failed to Give RM HB Protocol "
                   "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbRawPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a RM raw socket packet arrives. 
 ******************************************************************************/
VOID
RmHbRawPktRcvd (INT4 i4SockFd)
{
    RM_HB_TRC (RM_SOCK_TRC, "RmHbRawPktRcvd: Packet on "
               "raw socket entry...\n");
    UNUSED_PARAM (i4SockFd);
    if (RM_HB_SEND_EVENT (RM_HB_TASK_ID, RM_HB_PKT_RCVD) == OSIX_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmHbRawPktRcvd: Send Event RM_HB_PKT_RCVD "
                   "-- Failed\n");
        return;
    }
    RM_HB_TRC (RM_SOCK_TRC, "RmHbRawPktRcvd: Packet on "
               "raw socket exit...\n");
}

/********************************************************************************
 *    Function Name      : RmHbCallBackFn
 *
 *    Description        : This function is invoked whenever RM HB packet is 
 *                         received in the driver. This will take care of 
 *                         posting the packet in CruBuffer format to RM
 *
 *    Input(s)           : pBuf - pointer to the RM HB Data
 *                           
 *                            
 *
 *    Output(s)          : None
 *
 *    Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ********************************************************************************/

INT4
RmHbCallBackFn (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* post the buffer to RM queue */
    i4RetVal =
        RM_HB_SEND_TO_QUEUE (RM_HB_QUEUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);

    if (i4RetVal != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    /* trigger the interuppt event for RM */
    if (OsixEvtSend (RM_HB_TASK_ID, RM_HB_PKT_RCVD_ON_ETH_PORT) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}
#endif /* _RMHBMAIN_C_ */
