/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmhbsem.c,v 1.5 2012/01/20 13:08:17 siva Exp $
*
* Description: RM Heart Beat State event machine.
*********************************************************************/
#include "rmhbincs.h"
#include "rmhbsem.h"

/******************************************************************************
 * Function           : RmStateMachine
 * Input(s)           : u1Event - Event 
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure indexes into the state event table using 
 *                      the event and the state to get an interger. This integer
 *                      when indexed into an array of function pointers gives 
 *                      the action routine to be called. 
 ******************************************************************************/
VOID
RmStateMachine (UINT1 u1Event)
{
    UINT1               u1ActionProcIdx;
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) RM_HB_GET_NODE_STATE ();

    if (u1CurState < RM_MAX_STATES)
    {
        RM_HB_TRC2 (RM_SEM_TRC, "FSM Event: %s  State: %s\n",
                    au1RmEvntStr[u1Event], au1RmStateStr[u1CurState]);
    }
    /* Get the index of the action procedure */
    u1ActionProcIdx = au1RmSem[u1Event][RM_HB_GET_NODE_STATE ()];

    /* call the corresponding function pointer */
    (*RmActionProc[u1ActionProcIdx]) ();

    return;
}

/******************************************************************************
 * Function           : RmSem1WayRcvdInInitState 
 * Input(s)           : VOID
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when the peer node has not 
 *                      learnt about this node, but this node has learnt 
 *                      about peer (i.e. HeartBeat is one-way rcvd). 
 ******************************************************************************/
VOID
RmSem1WayRcvdInInitState (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "Entered RmSem1WayRcvdInInitState\n");
}

/******************************************************************************
 * Function           : RmSem2WayRcvdInInitState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when the peer node has
 *                      learnt about this node and this node has also learnt 
 *                      about peer node (i.e., HeartBeat msg is 2-way rcvd). 
 ******************************************************************************/
VOID
RmSem2WayRcvdInInitState (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "Entered RmSem2WayRcvdInInitState\n");

    RmStartElection ();
}

/******************************************************************************
 * Function           : RmSem2WayRcvdInActvState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a heart-beat msg is 
 *                      rcvd in 2-way rcvd state.  
 ******************************************************************************/
VOID
RmSem2WayRcvdInActvState (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "Entered RmSem2WayRcvdInActvState\n");
}

/******************************************************************************
 * Function           : RmSemActvElctdInInitState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Active node is already elected and this node has to
 *                      update its ACTIVE node list and change its state to
 *                      STANDBY.
 ******************************************************************************/
VOID
RmSemActvElctdInInitState (VOID)
{
    UINT1               u1State = RM_MAX_STATES;

    RM_HB_TRC (RM_SEM_TRC, "Entered RmSemActvElctdInInitState\n");

    /* Active node is already elected and this is the second node
     * to come up. Do not start election again. ACTIVE NodeId is already 
     * updated in global data struct and now set the state accordingly. */
    if (RM_HB_GET_ACTIVE_NODE_ID () == RM_HB_GET_SELF_NODE_ID ())
    {
        u1State = RM_ACTIVE;
    }
    else if (RM_HB_GET_ACTIVE_NODE_ID () == RM_HB_GET_PEER_NODE_ID ())
    {
        u1State = RM_STANDBY;
    }

    if ((u1State == RM_ACTIVE) || (u1State == RM_STANDBY))
    {
        RmUpdateState (u1State);
    }
}

/******************************************************************************
 * Function           : RmSemPeerDownRcvdInInitState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
RmSemPeerDownRcvdInInitState (VOID)
{
    RmUpdateState (RM_ACTIVE);
    RM_HB_TRC (RM_SEM_TRC, "RmSemPeerDownRcvdInInitState: ACTIVE notified\n");
}

/******************************************************************************
 * Function           : RmSemPeerDownRcvdInActiveState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when ACTIVE node 
 *                      Peer dead timer expires 
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
RmSemPeerDownRcvdInActiveState (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "RmSemPeerDownRcvdInActiveState: Standby is down\n");
    if (RM_HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* Set the peer node id to zero */
    RM_HB_SET_PEER_NODE_ID (0);
}

/******************************************************************************
 * Function           : RmSemPeerDownRcvdInStandbyState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
RmSemPeerDownRcvdInStandbyState (VOID)
{
    tRmHbMsg            RmHbMsg;

    if (RM_HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* This peer node id set to zero should be present before 
     * returning in curr node = transition in progress and 
     * prev state is standby OR INIT check, otherwise after completing 
     * pending operation at RM core module, peer down 
     * will not be detected and GO_ACTIVE will not be sent to 
     * RM core module*/
    RM_HB_SET_PEER_NODE_ID (0);

    /* Until we get a event from RM core module for RX buffer 
     * messages are processed, RM SEM will be in 
     * RM_TRANSITION_IN_PROGRESS state */
    RM_HB_SET_PREV_NODE_STATE (RM_STANDBY);
    RM_HB_SET_NODE_STATE (RM_TRANSITION_IN_PROGRESS);
    RM_HB_SET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
    RmHbMsg.u4Evt = RM_PROC_PENDING_SYNC_MSG;
    RmApiSendHbEvtToRm (&RmHbMsg);
    RM_HB_TRC (RM_SEM_TRC, "RmSemPeerDownRcvdInStandbyState: Active node dead, "
               "Pending RX buffer processing event triggered\n");
}

/******************************************************************************
 * Function           : RmSemPeerDownRcvdInTrgsInPrgs
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
RmSemPeerDownRcvdInTrgsInPrgs (VOID)
{
    tRmHbMsg            RmHbMsg;

    if (RM_HB_GET_PREV_NODE_STATE () == RM_ACTIVE)
    {
        RM_HB_RESET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
        if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
        {
            /* Standby node dead before getting the FSW ACK. */
            RM_HB_RESET_FLAGS (RM_FORCE_SWITCHOVER);
            RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
            /* FSW ACK timer should be greater than the peer dead timer value, 
             * so FSW ACK should be stopped here. */
            RmHbTmrStopTimer (RM_HB_FSW_ACK_TIMER);
            RmHbMsg.u4Evt = RM_FSW_FAILED_IN_ACTIVE;
            RmApiSendHbEvtToRm (&RmHbMsg);
            RM_HB_TRC (RM_SEM_TRC,
                       "RmSemPeerDownRcvdInTrgsInPrgs: Standby node dead "
                       "while waiting for FSW ACK HB message\n");
        }
        else
        {
            /* Failover case */
            /* Standby node dead, ACTIVE side RM core module 
             * does not have the RX buffer to process. 
             * So, special action is not required here.*/
            RM_HB_TRC (RM_SEM_TRC,
                       "RmSemPeerDownRcvdInTrgsInPrgs: Standby node dead\n");
        }
    }
    else if (RM_HB_GET_PREV_NODE_STATE () == RM_STANDBY)
    {
        if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
        {
            /* Active node dead before getting the FSW ACK. */
            RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
            RM_HB_TRC (RM_SEM_TRC,
                       "RmSemPeerDownRcvdInTrgsInPrgs: Active node dead "
                       "before sending the FSW ACK HB message\n");
        }
        else                    /* Failover case */
        {
            /* Failover case.
             * Ignore this peer down as RM core module is processing the 
             * RX buffer messages*/
            RM_HB_TRC (RM_SEM_TRC,
                       "RmSemPeerDownRcvdInTrgsInPrgs: Active node dead "
                       "while RX buffer processing at STANDBY\n");
        }
    }

    if (RM_HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* This peer node id set to zero should be present before 
     * returning in curr node = transition in progress and 
     * prev state is standby OR INIT check, otherwise after completing 
     * pending operation at RM core module, peer down 
     * will not be detected and GO_ACTIVE will not be sent to 
     * RM core module*/
    RM_HB_SET_PEER_NODE_ID (0);
    /* 1. RM core module is processing the RX buffer messages now, 
     * so don't give GO_ACTIVE, RM core module notifies once the RX buffer 
     * messages are processed and then give GO_ACTIVE indication 
     * from RM HB module.
     * 2. Prev state = RM_INIT and curr state = RM_TRANSITION_IN_PROGRESS 
     * is applicable only for the connection lost and restored case.
     * So, while RM core task doing the shut and start 
     * operation peer down is received. Just ignore this event now, 
     * after completing the shut and start operation, 
     * GO_ACTIVE will be given.
     * */
    if ((RM_HB_GET_PREV_NODE_STATE () == RM_STANDBY) ||
        (RM_HB_GET_PREV_NODE_STATE () == RM_INIT))
    {
        /* 1. RM core module is processing the RX buffer messages now, 
         * so don't give GO_ACTIVE, RM core module notifies once the RX buffer 
         * messages are processed and then give GO_ACTIVE indication 
         * from RM HB module.
         * 2. Prev state = RM_INIT and curr state = RM_TRANSITION_IN_PROGRESS 
         * is applicable only for the connection lost and restored case.
         * So, while RM core task doing the shut and start 
         * operation peer down is received. Just ignore this event now, 
         * after completing the shut and start operation, 
         * GO_ACTIVE will be given.
         * */
        return;
    }
    RmUpdateState (RM_ACTIVE);
    RM_HB_TRC (RM_SEM_TRC, "RmSemPeerDownRcvdInTrgsInPrgs: ACTIVE indicated\n");
}

/******************************************************************************
 * Function           : RmSemFSWRcvdInActiveState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a peer node does not 
 *                      receive the FSW ACK and here FSW ACK retransmission 
 *                      takes place.
 ******************************************************************************/
VOID
RmSemFSWRcvdInActiveState (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWRcvdInActiveState: ENTRY\n");
    if (RmSendMsgToPeer (RM_FORCE_SWITCHOVER_ACK) == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "!!! Sending fore-switchover-ack to peer failed !!!\n");
    }
    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWRcvdInActiveState: EXIT\n");
}

/******************************************************************************
 * Function           : RmSemFSWRcvdInStandbyState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a peer node is desired
 *                      to go standby.
 ******************************************************************************/
VOID
RmSemFSWRcvdInStandbyState (VOID)
{
    tRmHbMsg            RmHbMsg;

    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWRcvdInStandbyState: ENTRY\n");
    if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_NOT_OCCURED)
    {
        RM_HB_SET_PREV_NODE_STATE (RM_HB_GET_NODE_STATE ());
        RM_HB_SET_NODE_STATE (RM_TRANSITION_IN_PROGRESS);
        RM_HB_SET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
        RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
        RmHbMsg.u4Evt = RM_PROC_PENDING_SYNC_MSG;
        RmApiSendHbEvtToRm (&RmHbMsg);
        RM_HB_TRC (RM_SEM_TRC, "RmSemFSWRcvdInStandbyState: STANDBY side "
                   "pending RX buffer processing event triggered\n");
    }
    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWRcvdInStandbyState: EXIT\n");
}

/******************************************************************************
 * Function           : RmSemFSWAckRcvdInTrgsInPrgsState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a active 
 *                      to go to standby.
 ******************************************************************************/
VOID
RmSemFSWAckRcvdInTrgsInPrgsState (VOID)
{
    /* Since force switchover acknowledgement is recieved, reset 
     * the flag.
     */
    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWAckRcvdInTrgsInPrgsState: ENTRY\n");
    if (RM_HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        RM_HB_RESET_FLAGS (RM_FORCE_SWITCHOVER);
        RM_HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
        RM_HB_RESET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
        RmHbTmrStopTimer (RM_HB_FSW_ACK_TIMER);
        RM_HB_SET_ACTIVE_NODE_ID (RM_HB_GET_PEER_NODE_ID ());
        RmUpdateState (RM_STANDBY);
        RM_HB_TRC (RM_SEM_TRC, "RmSemFSWAckRcvdInTrgsInPrgsState: "
                   "FSW ACK is received from peer\n");
    }
    RM_HB_TRC (RM_SEM_TRC, "RmSemFSWAckRcvdInTrgsInPrgsState: EXIT\n");
}

/******************************************************************************
 * Function           : RmUpdateState
 * Input(s)           : State.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function updates the current state 
 *                      of the node and does the shut and start 
 *                      when init to standby transition desired from active.
 ******************************************************************************/
VOID
RmUpdateState (UINT1 u1State)
{
    UINT1               u1PrevState;
    UINT1               u1Event = RM_INIT;
    tRmHbMsg            RmHbMsg;
    UINT1               u1CurState = 0;
    if (u1State == RM_HB_GET_NODE_STATE ())
    {
        /* no change in node state */
        return;
    }

    RM_HB_TRC1 (RM_SEM_TRC, " RmUpdateState = %d\n", u1State);

    if ((RM_HB_GET_PREV_NODE_STATE () == RM_ACTIVE) &&
        (RM_HB_GET_NODE_STATE () == RM_INIT) && (u1State == RM_STANDBY))
    {
        RM_HB_SET_PREV_NODE_STATE (RM_HB_GET_NODE_STATE ());
        RM_HB_SET_NODE_STATE (RM_TRANSITION_IN_PROGRESS);
        RM_HB_SET_FLAGS (RM_TRANS_IN_PROGRESS_FLAG);
        RmHbMsg.u4Evt = RM_COMM_LOST_AND_RESTORED;
        RmApiSendHbEvtToRm (&RmHbMsg);
        return;
    }
    else if ((RM_HB_GET_PREV_NODE_STATE () == RM_ACTIVE) &&
             (RM_HB_GET_NODE_STATE () == RM_INIT) &&
             (u1State == RM_ACTIVE) &&
             (RM_HB_GET_PREV_NOTIF_NODE_STATE () == RM_ACTIVE))
    {
        /* RM level state transition, 
         * no need to update this status to RM enabled protolcols as their 
         * status is ACTIVE */
        u1PrevState = (UINT1) RM_HB_GET_NODE_STATE ();
        RM_HB_SET_PREV_NODE_STATE (u1PrevState);
        RM_HB_SET_NODE_STATE (u1State);
        RM_HB_TRC (RM_CRITICAL_TRC, " RM INIT->ACTIVE transition\n");
        return;
    }
    u1PrevState = (UINT1) RM_HB_GET_NODE_STATE ();
    RM_HB_SET_PREV_NODE_STATE (u1PrevState);
    RM_HB_SET_NODE_STATE (u1State);

    if (RM_HB_GET_PREV_NOTIF_NODE_STATE () == RM_HB_GET_NODE_STATE ())
    {
        /* No need to notify the same status again to RM core module */
        return;
    }

    if (RM_HB_GET_NODE_STATE () == RM_STANDBY)
    {
        u1Event = GO_STANDBY;
    }
    if (RM_HB_GET_NODE_STATE () == RM_ACTIVE)
    {
        u1Event = GO_ACTIVE;
    }

    if ((u1Event == GO_ACTIVE) || (u1Event == GO_STANDBY))
    {
        RM_HB_SET_PREV_NOTIF_NODE_STATE (RM_HB_GET_NODE_STATE ());
        RmHbMsg.u4Evt = u1Event;
        RmApiSendHbEvtToRm (&RmHbMsg);
    }

    u1CurState = (UINT1) RM_HB_GET_NODE_STATE ();

    if (u1CurState < RM_MAX_STATES)
    {
        RM_HB_TRC1 (RM_SEM_TRC, "FSM State Changed to: %s\n",
                    au1RmStateStr[u1CurState]);
    }
}

/******************************************************************************
 * Function           : RmSemEvntIgnore 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when an invalid event occurs.
 ******************************************************************************/
VOID
RmSemEvntIgnore (VOID)
{
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) RM_HB_GET_NODE_STATE ();

    if (u1CurState < RM_MAX_STATES)
    {
        RM_HB_TRC1 (RM_SEM_TRC, "Unexpected event to RM SEM. State:%s\n",
                    au1RmStateStr[u1CurState]);
    }
    return;
}

/******************************************************************************
 * Function           : RmSemPriorityChg 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked whenever the priority of the
 *                      node changes.
 ******************************************************************************/
VOID
RmSemPriorityChg (VOID)
{
    RM_HB_TRC (RM_SEM_TRC, "Entered RmSemPriorityChg\n");
    RmStartElection ();
}

/******************************************************************************
 * Function           : RmStartElection 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure performs the election to elect ACTIVE
 *                      and STANDBY node.
 ******************************************************************************/
VOID
RmStartElection (VOID)
{
    UINT1               u1NodeState = RM_MAX_STATES;

    RM_HB_TRC (RM_SEM_TRC, "RmStartElection: ENTRY\n");

    if (RM_HB_GET_SELF_NODE_PRIORITY () > RM_HB_GET_PEER_NODE_PRIORITY ())
    {
        RM_HB_SET_ACTIVE_NODE_ID (RM_HB_GET_SELF_NODE_ID ());
        u1NodeState = RM_ACTIVE;
        RM_HB_TRC1 (RM_SEM_TRC, "ACTIVE NODE elected: %0x\n",
                    RM_HB_GET_ACTIVE_NODE_ID ());
    }
    else if (RM_HB_GET_SELF_NODE_PRIORITY () < RM_HB_GET_PEER_NODE_PRIORITY ())
    {
        u1NodeState = RM_STANDBY;
    }
    else
        /* Priorities are equal. In the event of tie, NodeId is used for election */
    {
        /* Elect the node with higher IP address as the ACTIVE node */
        if (RM_HB_GET_SELF_NODE_ID () > RM_HB_GET_PEER_NODE_ID ())
        {
            RM_HB_SET_ACTIVE_NODE_ID (RM_HB_GET_SELF_NODE_ID ());
            u1NodeState = RM_ACTIVE;
            RM_HB_TRC1 (RM_SEM_TRC, "ACTIVE NODE elected: %0x\n",
                        RM_HB_GET_ACTIVE_NODE_ID ());
        }
        else if (RM_HB_GET_SELF_NODE_ID () < RM_HB_GET_PEER_NODE_ID ())
        {
            u1NodeState = RM_STANDBY;
        }
    }
    if (RM_HB_GET_ACTIVE_NODE_ID () == 0)
    {
        RM_HB_TRC (RM_SEM_TRC, "RmStartElection: "
                   "Active node id is zero...!!! \r\n");
        return;
    }
    if (u1NodeState == RM_STANDBY)
    {
        if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
        {
            RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_UP);
        }
        RmUpdateState (u1NodeState);
    }
    else                        /* NODE STATE is ACTIVE */
    {
        RmUpdateState (u1NodeState);

        if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
        {
            RmHbNotifyPeerInfo (RM_HB_GET_PEER_NODE_ID (), RM_PEER_UP);
        }
    }
    RM_HB_TRC (RM_SEM_TRC, "RmStartElection: EXIT\n");
}
