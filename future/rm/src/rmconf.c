
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmconf.c,v 1.40 2017/12/26 13:34:30 siva Exp $
*
* Description: This file contains the routines specific to the Static
*              Configuration Sync-Up Sub module of RM. This module is
*              responsible for receiving the static configurations from
*              Active node and applying them on the Standby node.
*********************************************************************/
#ifndef _RMCONF_C_
#define _RMCONF_C

#include "rmincs.h"
#define RM_OID_LIST_MAX 13
/* Update the MACRO RM_OID_LIST_MAX when any new oids are added*/

/* During gaRmOidList usage 2076 will be replaced with EOID */
tRmOidFilterList    gaRmOidList[] = {
    {"1.3.6.1.4.1.2076.106.1.1.1.2"}    /* FsPingDest */
    ,
    {"1.3.6.1.4.1.2076.106.1.1.1.3"}    /* FsPingTimeout */
    ,
    {"1.3.6.1.4.1.2076.106.1.1.1.4"}    /* FsPingTries */
    ,
    {"1.3.6.1.4.1.2076.106.1.1.1.5"}    /* FsPingDataSize */
    ,
    {"1.3.6.1.4.1.2076.106.1.1.1.12"}    /* FsPingEntryStatus */
    ,
    {"1.3.6.1.4.1.2076.99.1.5"}    /* FsRmHbInterval */
    ,
    {"1.3.6.1.4.1.2076.99.1.6"}    /* FsRmPeerDeadInterval */
    ,
    {"1.3.6.1.4.1.2076.99.1.7"}    /* FsRmTrcLevel */
    ,
    {"1.3.6.1.4.1.2076.99.1.8"}    /* FsRmForceSwitchoverFlag */
    ,
    {"1.3.6.1.4.1.2076.99.1.9"}    /* FsRmPeerDeadIntMultiplier */
    ,
    {"1.3.6.1.4.1.2076.99.1.16"}    /* FsRmModuleTrc */
    ,
    {"1.3.6.1.4.1.2076.81.1.9"}    /* IssRestart */
    ,
    {"1.3.6.1.4.1.2076.81.1.33"}    /* IssOOBInterface */
    ,
    {"1.3.6.1.4.1.29601.2.24.2.1.2"}    /*72 - fsMIOspfv3TraceLevel */
    ,
    {"1.3.6.1.4.1.29601.2.24.1.1"}    /*73 - fsMIOspfv3GlobalTraceLevel */
    ,
    {"1.3.6.1.4.1.29601.2.24.2.1.15"}    /*74 - fsMIOspfv3ExtTraceLevel */
    ,
    {"1.3.6.1.4.1.2076.90.1.2"}    /*75 - futOspfv3TraceLevel */
    ,
    {"1.3.6.1.4.1.2076.90.1.16"}    /*76 - futOspfv3ExtTraceLevel */

};

#if  defined (MSR_WANTED) && defined (SNMPV3_WANTED)
extern
    VOID         MsrConvertOidToString (tSNMP_OID_TYPE * pOid,
                                        UINT1 *pu1String);
#endif

/******************************************************************************
 * Function           : RmConfSyncUpTaskMain
 * Input(s)           : pi1Arg - Pointer to the arguments 
 * Output(s)          : None 
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function will be invoked to spawn the configuration
 *                      syncup task when the Rm node status becomes Standby.
 *                      This task will wait for the events RM_CONFIG_MSG_RCVD
 *                      and RM_BULK_UPDATE_COMPLETION 
 ******************************************************************************/

VOID
RmConfSyncUpTaskMain (INT1 *pi1Arg)
{
    UINT4               u4Event;
    UNUSED_PARAM (pi1Arg);

    OsixTskIdSelf (&RM_CONF_SYNCUP_TASK_ID ());

    /* Config syncup task semaphore */
    if (OsixSemCrt ((UINT1 *) RM_CONF_SYNC_SEM,
                    &(RM_CONF_SYNC_SEMID ())) == OSIX_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmConfSyncUpTaskMain: Semaphore"
                "creation FAILED!!!\n");
        return;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (RM_CONF_SYNC_SEMID ());

    if (OsixCreateQ (RM_CONF_Q_NAME, MAX_RM_CONF_Q_DEPTH, 0, (&RM_CONF_Q_ID ()))
        == OSIX_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmConfSyncUpTaskMain: Queue creation "
                "failed\n");
        RM_DELETE_SEMAPHORE (RM_CONF_SYNC_SEMID ());
        return;
    }

    RM_CONF_Q_MSG_POOL_ID () = RMMemPoolIds[MAX_RM_CONF_Q_DEPTH_SIZING_ID];
    RM_CONF_FAIL_BUFF_POOL_ID () =
        RMMemPoolIds[MAX_RM_CONF_FAIL_BUF_BLOCKS_SIZING_ID];

    TMO_SLL_Init (&(gRmConfInfo.RmConfigFailBuff));
    gRmConfInfo.bBulkUpdStatus = RM_BULK_UPDATE_NOT_COMPLETED;

    while (1)
    {
        if (OsixEvtRecv (RM_CONF_SYNCUP_TASK_ID (), RM_CONF_ALL_EVENTS,
                         OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
        {
            RM_STAT_CONF_LOCK ();
            if (u4Event & RM_CONFIG_MSG_RCVD)
            {
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmConfSyncUpTaskMain: RM_CONFIG_MSG_RCVD\n");
                RmConfDeqConfigSyncMsg ();
            }

            if (u4Event & RM_DYNAMIC_BULK_UPDATE_COMPLETION)
            {
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmConfSyncUpTaskMain: "
                        "RM_DYNAMIC_BULK_UPDATE_COMPLETION\n");
                gRmConfInfo.bBulkUpdStatus = RM_BULK_UPDATE_COMPLETED;
                if (TMO_SLL_Count (&(gRmConfInfo.RmConfigFailBuff)) != 0)
                {
                    RmConfHandleBulkUpdCompEvent ();
                }

                /* NPAPI call for restoring the value of filters dumped 
                 * only when ISSU is in full-compatible mode at the end
                 * of Bulk update sync-up */
                if ((IssuGetMaintModeOperation () == OSIX_TRUE)
                    && (RM_FORCE_SWITCHOVER_FLAG () != RM_FSW_OCCURED))
                {
                    IssuApiRestoreCacheFile ();
                }
                /* During maintenance mode, update the ISSU procedure status */
                /* In case of compatible mode, load version is performed in 
                 * standby node, so update procedure status at the end of 
                 * Bulk update sync-up */
                if (IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE)
                {
                    IssuUpdateProcedureStatus ();
                }
            }
            RM_STAT_CONF_UNLOCK ();
        }
    }
}

/******************************************************************************
 * Function           : RmConfDeqConfigSyncMsg
 * Input(s)           : None
 * Output(s)          : None 
 * Returns            : None 
 * Action             : This function dequeues the syncedup static configuration
 *                      message from the config queue and processes the
 *                      message.
 ******************************************************************************/
VOID
RmConfDeqConfigSyncMsg ()
{
    tRmConfigQMsg      *pRmConfigQMsg;

    while (OsixQueRecv (RM_CONF_Q_ID (), (UINT1 *) &pRmConfigQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        RmConfProcessSyncMsg (pRmConfigQMsg);
    }
}

/******************************************************************************
 * Function           : RmConfProcessSyncMsg 
 * Input(s)           : pConfigMsg - Pointer to the message
 * Output(s)          : None 
 * Returns            : None 
 * Action             : This function processes the message and sends the ACK to
 *                      RM.
 ******************************************************************************/
VOID
RmConfProcessSyncMsg (tRmConfigQMsg * pConfigMsg)
{
    tRmProtoAck         ProtoAck;
    UINT4               u4Offset = 0;
    UINT1               u1MsgType = 0;

    RM_CONF_GET_1_BYTE (pConfigMsg->RmConfigMsg.pData, u4Offset, u1MsgType);

    switch (u1MsgType)
    {
        case RM_CONFIG_VALID_MSG:
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmConfProcessSyncMsg: Received CONFIG_VALID_MSG with "
                     "Seq# %d\n", pConfigMsg->RmConfigMsg.u4SeqNo);
            RmConfProcessValidMsg (pConfigMsg->RmConfigMsg.pData,
                                   pConfigMsg->RmConfigMsg.u2DataLen);
            break;
        }
        case RM_CONFIG_DUMMY_MSG:
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmConfProcessSyncMsg: Received RM_CONFIG_DUMMY_MSG with "
                     "[Seq# %d]\n", pConfigMsg->RmConfigMsg.u4SeqNo);
            break;
        }
        case RM_CONFIG_IGNORE_ABORT:
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmConfProcessSyncMsg: Received RM_CONFIG_IGNORE_ABORT with "
                     "[Seq# %d]\n", pConfigMsg->RmConfigMsg.u4SeqNo);
            RM_IGNORE_ABORT_NOTIFICATION () = RM_TRUE;
            break;
        }
        case RM_CONFIG_ACCEPT_ABORT:
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmConfProcessSyncMsg: Received RM_CONFIG_ACCEPT_ABORT with "
                     "[Seq# %d]\n", pConfigMsg->RmConfigMsg.u4SeqNo);
            RM_IGNORE_ABORT_NOTIFICATION () = RM_FALSE;
            break;
        }
        default:
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "RmConfProcessSyncMsg: Received INVALID MSG with "
                     "[Seq# %d]\n", pConfigMsg->RmConfigMsg.u4SeqNo);
            break;
        }
    }

    RM_FREE ((tRmMsg *) pConfigMsg->RmConfigMsg.pData);
    ProtoAck.u4AppId = RM_STATIC_CONF_APP_ID;
    ProtoAck.u4SeqNumber = pConfigMsg->RmConfigMsg.u4SeqNo;

    /* Release the buffer to pool */
    if (MemReleaseMemBlock (RM_CONF_Q_MSG_POOL_ID (),
                            (UINT1 *) pConfigMsg) != MEM_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmConfDeqConfigSyncMsg: Memory free"
                " failed\n");
    }
    pConfigMsg = NULL;

    /* Sending ACK to RM for both valid and dummy sync-up message */
    RmApiSendProtoAckToRM (&ProtoAck);
}

/******************************************************************************
 * Function           : RmConfProcessValidMsg 
 * Input(s)           : pMesg    - Pointer to the configuration message
 *                      u2Length - Length of the message
 * Output(s)          : None 
 * Returns            : None 
 * Action             : This function processes the message and applies the 
 *                      configuration to the protocol modules. If the 
 *                      configuration fails and if the bulk update has not been
 *                      completed by all the protocols then it will be added to
 *                      the Failure buffer. 
 ******************************************************************************/
VOID
RmConfProcessValidMsg (tRmMsg * pMesg, UINT2 u2Length)
{
    tSNMP_OID_TYPE     *pOID = NULL;
    tSNMP_OID_TYPE     *pEntOid = NULL;
    tSNMP_MULTI_DATA_TYPE ObjVal;
    tRmNotificationMsg  NotifMsg;
    tFmFaultMsg         FmFaultMsg;
    UINT4               u4Offset = 1;    /* Message type had already been 
                                           retrieved */
    UINT4               u4OidLen = 0;
    UINT4               u4Value = 0;
    UINT2               u2MsgLen;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1Data[RM_ERROR_STR_LEN];

    MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1Data, 0, sizeof (au1Data));
    MEMSET (&(ObjVal), 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    RM_CONF_GET_2_BYTE (pMesg, u4Offset, u2MsgLen);

    if (u2MsgLen != u2Length)
    {
        RM_TRC (RM_SYNCUP_TRC | RM_FAILURE_TRC, "RmConfProcessValidMsg: Msg"
                " length does not match !!!\n");
        return;
    }

    RM_CONF_GET_4_BYTE (pMesg, u4Offset, u4OidLen);
    pOID = alloc_oid (u4OidLen);
    pEntOid = alloc_oid (u4OidLen);

    if ((pOID == NULL) || (pEntOid == NULL))
    {
        free_oid (pOID);
        free_oid (pEntOid);
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmConfProcessValidMsg: "
                "Memory allocation for OID failed!!\n");
        return;
    }

    RM_CONF_GET_N_BYTE (pMesg, (UINT1 *) pOID->pu4_OidList, u4Offset,
                        pOID->u4_Length * sizeof (UINT4));
    SNMPUpdateEOID (pOID, pEntOid, FALSE);

    if (RmConfIsOIDInFilterList (pEntOid) == OSIX_TRUE)
    {
        free_oid (pOID);
        free_oid (pEntOid);
        return;
    }

    RM_CONF_GET_2_BYTE (pMesg, u4Offset, ObjVal.i2_DataType);

    /* Get the Value */
    switch (ObjVal.i2_DataType)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, ObjVal.u4_ULongValue);
            break;

        case SNMP_DATA_TYPE_INTEGER32:
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, ObjVal.i4_SLongValue);
            break;

        case SNMP_DATA_TYPE_OBJECT_ID:
            u4Value = 0;
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, u4Value);
            ObjVal.pOidValue = alloc_oid (u4Value);
            if (ObjVal.pOidValue == NULL)
            {
                free_oid (pEntOid);
                free_oid (pOID);
                RM_TRC (RM_CRITICAL_TRC, "RmConfProcessValidMsg: Failed to "
                        "allocate memory for data type "
                        "SNMP_DATA_TYPE_OBJECT_ID\n");
                return;
            }
            RM_CONF_GET_N_BYTE (pMesg, (UINT1 *) ObjVal.pOidValue->pu4_OidList,
                                u4Offset,
                                (ObjVal.pOidValue->u4_Length * sizeof (UINT4)));
            break;

        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            u4Value = 0;
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, u4Value);
            ObjVal.pOctetStrValue = allocmem_octetstring (u4Value);
            if (ObjVal.pOctetStrValue == NULL)
            {
                RM_TRC (RM_CRITICAL_TRC, "Failed to allocate memory for the "
                        "data type\n");
                free_oid (pEntOid);
                free_oid (pOID);
                return;
            }
            RM_CONF_GET_N_BYTE (pMesg, ObjVal.pOctetStrValue->pu1_OctetList,
                                u4Offset, ObjVal.pOctetStrValue->i4_Length);
            break;

        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            u4Value = 0;
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, u4Value);
            ObjVal.u4_ULongValue = u4Value;
            break;

        case SNMP_DATA_TYPE_COUNTER64:
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, ObjVal.u8_Counter64Value.msn);
            RM_CONF_GET_4_BYTE (pMesg, u4Offset, ObjVal.u8_Counter64Value.lsn);
            break;

        case SNMP_DATA_TYPE_NULL:
            break;

        default:
            RM_TRC (RM_SYNCUP_TRC | RM_FAILURE_TRC,
                    "Invalid data type received\n");
            free_oid (pEntOid);
            free_oid (pOID);
            return;
    }
    /* If the bulk updation has been completed, then apply the configuration
     * and if it fails, send notification and increment the counter. If the
     * bulk updation has not been completed then check whether the same oid
     * is present in the failure buffer. If so add the configuration sync-up
     * message to the failure buffer else apply the configuration immediately.*/

    if (gRmConfInfo.bBulkUpdStatus == RM_BULK_UPDATE_COMPLETED)
    {

        if (RmConfApplyConfiguration (pEntOid, &ObjVal) == RM_FAILURE)
        {
            RmHandleSystemRecovery (RM_CONF_APPLY_FAILED);
            /* Send a notification and trap. */
            MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
            NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
            NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
            NotifMsg.u4AppId = RM_STATIC_CONF_APP_ID;
            NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
            NotifMsg.u4Status = RM_FAILED;
            NotifMsg.u4Error = RM_PROCESS_FAIL;
            UtlGetTimeStr (NotifMsg.ac1DateTime);
            if (RM_SUCCESS == RmErrorStringOID (pOID, au1Oid, &ObjVal, au1Data))
            {
                SNPRINTF (NotifMsg.ac1ErrorStr, RM_ERROR_STR_LEN,
                          "OID=%s, Val=%s", au1Oid, au1Data);
            }
            RmTrapSendNotifications (&NotifMsg);
            RM_TRC2 (RM_SYNCUP_TRC | RM_CRITICAL_TRC,
                     "Static configuration sync-up processing failed for"
                     "OID = %s, Value = %s\n", au1Oid, au1Data);
            RM_RX_STAT_STATIC_CONF_SYNC_FAILED ()++;
        }
        else
        {
            free_oid (pOID);
            return;
        }
    }
    else
    {
        /* Search if the OID is present in the failure buffer. If yes, add to 
         * the end of the list. Else process the message*/
        if (RmConfSearchOIDInFailBuff (pEntOid) == RM_FAILURE)
        {
            MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));

            FmFaultMsg.u4ModuleId = OsixGetCurTaskId ();
            MEMCPY (FmFaultMsg.ModuleName,
                    OsixExGetTaskName (FmFaultMsg.u4ModuleId),
                    sizeof (FmFaultMsg.ModuleName));

            if (RmConfApplyConfiguration (pEntOid, &ObjVal) == RM_SUCCESS)
            {
                FmFaultMsg.u4EventType = ISS_FM_RM_CRITICAL_EVENT_INDICATION;
                FmFaultMsg.u4CriticalInfo = RM_FM_SYNC_SUCCESS_IND;
                FmApiNotifyFaults (&FmFaultMsg);
                free_oid (pOID);
                return;
            }
            else
            {
                FmFaultMsg.u4EventType = ISS_FM_RM_CRITICAL_EVENT_INDICATION;
                FmFaultMsg.u4CriticalInfo = RM_FM_SYNC_FAIL_IND;
                FmApiNotifyFaults (&FmFaultMsg);
                if (RM_SUCCESS ==
                    RmErrorStringOID (pOID, au1Oid, &ObjVal, au1Data))
                {
                    RM_TRC2 (RM_SYNCUP_TRC,
                             "Static configuration sync-up processing failed for"
                             "OID = %s, Value = %sduring bulk update in progress\n",
                             au1Oid, au1Data);
                }
            }
        }
        else
        {
            if (RM_SUCCESS == RmErrorStringOID (pOID, au1Oid, &ObjVal, au1Data))
            {
                RM_TRC2 (RM_SYNCUP_TRC,
                         "Received static configuration syncup message with the "
                         "OID = %s, Value = %s which is already present in the "
                         "buffer.\n", au1Oid, au1Data);
            }
        }
        RmConfAddToFailBuff (pEntOid, &ObjVal);
    }
    /* Free the memory allocated for the oid and the multi data */
    if (ObjVal.pOctetStrValue != NULL)
    {
        free_octetstring (ObjVal.pOctetStrValue);
        ObjVal.pOctetStrValue = NULL;
    }
    else if (ObjVal.pOidValue != NULL)
    {
        free_oid (ObjVal.pOidValue);
        ObjVal.pOidValue = NULL;
    }
    free_oid (pEntOid);
    free_oid (pOID);
}

/******************************************************************************
 * Function           : RmConfIsOIDInFilterList 
 * Input(s)           : pOID  - Object identifier 
 * Output(s)          : None 
 * Returns            : OSIX_TRUE / OSIX_FALSE 
 * Action             : Configurations like ping, trace-route, specific to RM 
 *                      need not be applied on the Standby node. The OIDs 
 *                      corresponding to these mib-objects are maintained in a
 *                      list. This function is called to check if the OID in 
 *                      the syncup message matches with those in the list. 
 *                      It returns OSIX_TRUE if there is a match, else 
 *                      OSIX_FALSE is returned.
 ******************************************************************************/
INT1
RmConfIsOIDInFilterList (tSNMP_OID_TYPE * pOID)
{
    UINT4               u4Index = 0;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH + 1];

    MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH + 1);
    WebnmConvertOidToString (pOID, au1Oid);

    for (u4Index = 0; u4Index < RM_OID_LIST_MAX; u4Index++)
    {
        if (MEMCMP (au1Oid, gaRmOidList[u4Index].pOidString,
                    STRLEN (gaRmOidList[u4Index].pOidString)) == 0)
        {
            RM_TRC1 (RM_SYNCUP_TRC,
                     "Received message with oid %s which need"
                     "not be applied at Standby\n", au1Oid);
            return OSIX_TRUE;
        }
    }
    return OSIX_FALSE;
}

/******************************************************************************
 * Function           : RmConfApplyConfiguration
 * Input(s)           : pOID  - Object identifier of the mib-object whose value
 *                              is to be set.
 *                      pVal  - Value to be set.
 * Output(s)          : None 
 * Returns            : RM_SUCCESS / RM_FAILURE 
 * Action             : This function calls the SNMPSet function to apply the 
 *                      configuration. 
 ******************************************************************************/

INT1
RmConfApplyConfiguration (tSNMP_OID_TYPE * pOID, tSNMP_MULTI_DATA_TYPE * pVal)
{
    tSnmpIndex         *pIndex = NULL;
    UINT4               u4Error = 0;
    UINT4               u4Count = SNMP_MAX_INDICES_2;
    INT1                i1RetVal = RM_SUCCESS;

    MEMSET (&RmSNMPIndexPool[0], 0, (sizeof (tSnmpIndex) * SNMP_MAX_INDICES_2));
    MEMSET (&RmSNMPMultiPool[0], 0,
            (sizeof (tSNMP_MULTI_DATA_TYPE) * SNMP_MAX_INDICES_2));
    MEMSET (&RmSNMPOctetPool[0], 0,
            (sizeof (tSNMP_OCTET_STRING_TYPE) * SNMP_MAX_INDICES_2));
    MEMSET (&RmSNMPOIDPool[0], 0,
            (sizeof (tSNMP_OID_TYPE) * SNMP_MAX_INDICES_2));
    MEMSET (&gau1RmSNMPData[0][0], 0, (SNMP_MAX_INDICES_2 * 1024));

    /* Initialize the tSnmpIndex structure for use in SNMPSet() */
    for (u4Count = 0; u4Count < SNMP_MAX_INDICES_2; u4Count++)
    {
        RmSNMPIndexPool[u4Count].pIndex = &(RmSNMPMultiPool[u4Count]);
        RmSNMPMultiPool[u4Count].pOctetStrValue = &(RmSNMPOctetPool[u4Count]);
        RmSNMPMultiPool[u4Count].pOidValue = &(RmSNMPOIDPool[u4Count]);
        RmSNMPMultiPool[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) gau1RmSNMPData[u4Count];
        RmSNMPMultiPool[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) gau1RmSNMPData[u4Count];
    }

    pIndex = RmSNMPIndexPool;
    if (SNMPSet (*pOID, pVal, &u4Error, pIndex, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        if (pVal->pOctetStrValue != NULL)
        {
            free_octetstring (pVal->pOctetStrValue);
            pVal->pOctetStrValue = NULL;
        }
        else if (pVal->pOidValue != NULL)
        {
            free_oid (pVal->pOidValue);
            pVal->pOidValue = NULL;
        }

        free_oid (pOID);
        pOID = NULL;
    }
    else
    {
        i1RetVal = RM_FAILURE;
    }
    return i1RetVal;
}

/******************************************************************************
 * Function           : RmConfHandleBulkUpdCompEvent
 * Input(s)           : None
 * Output(s)          : None 
 * Returns            : None 
 * Action             : This function will be invoked if the failure buffer is 
 *                      not empty. Each configuration will be applied again by 
 *                      calling the SNMPSet function. If it fails again then
 *                      statistics counter is incremented and a trap is sent
 *                      and the configuration is then discarded. This process
 *                      is repeated until there are messages in the buffer.
 * Note: Messages are added to the failure buffer if SNMPSet fails before
 * bulk updation has been completed by all the protocols.
 ******************************************************************************/
VOID
RmConfHandleBulkUpdCompEvent ()
{
    tRmConfFailBuff    *pFailBuffNode = NULL;
    tSNMP_OID_TYPE     *pOID = NULL;
    tSNMP_MULTI_DATA_TYPE *pObjVal;
    tRmNotificationMsg  NotifMsg;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1Data[RM_ERROR_STR_LEN];

    MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1Data, 0, sizeof (au1Data));

    while ((pFailBuffNode = (tRmConfFailBuff *)
            TMO_SLL_Get (&(gRmConfInfo.RmConfigFailBuff))) != NULL)
    {

        pOID = pFailBuffNode->pFailedOid;
        pObjVal = &(pFailBuffNode->Data);

        if (RmConfApplyConfiguration (pOID, pObjVal) == RM_FAILURE)
        {
            RmHandleSystemRecovery (RM_CONF_APPLY_FAILED);
            /* Send trap and trace. */
            MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
            NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
            NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
            NotifMsg.u4AppId = RM_STATIC_CONF_APP_ID;
            NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
            NotifMsg.u4Status = RM_FAILED;
            NotifMsg.u4Error = RM_PROCESS_FAIL;
            UtlGetTimeStr (NotifMsg.ac1DateTime);
#ifdef WEBNM_WANTED
            WebnmConvertOidToString (pOID, au1Oid);

            /* As the array size of au1Data has been reduced to RM_ERROR_STR_LEN
             * from RM_MAX_DATA_LEN, this function(SNMPConvertDataToString) expects data length of ObjVal
             * to be of RM_ERROR_STR_LEN.
             * If the size is greater than RM_ERROR_STR_LEN, un-expected results may occur */

            SNMPConvertDataToString (pObjVal, au1Data, pObjVal->i2_DataType);
#endif
            SnmpRevertEoidString (au1Oid);
            SNPRINTF (NotifMsg.ac1ErrorStr, RM_ERROR_STR_LEN,
                      "OID=%s, Val=%s", au1Oid, au1Data);
            RmTrapSendNotifications (&NotifMsg);
            RM_TRC2 (RM_SYNCUP_TRC,
                     "Applying Static configuration syncup message in the "
                     "failure buffer failed for OID = %s, Value = %s\n", au1Oid,
                     au1Data);
            RM_RX_STAT_STATIC_CONF_SYNC_FAILED ()++;

            if (pObjVal->pOctetStrValue != NULL)
            {
                free_octetstring (pObjVal->pOctetStrValue);
                pObjVal->pOctetStrValue = NULL;
            }
            else if (pObjVal->pOidValue != NULL)
            {
                free_oid (pObjVal->pOidValue);
                pObjVal->pOidValue = NULL;
            }

            free_oid (pOID);
            pOID = NULL;
        }

        if (MemReleaseMemBlock (RM_CONF_FAIL_BUFF_POOL_ID (),
                                (UINT1 *) pFailBuffNode) == MEM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "RmConfHandleBulkUpdCompEvent: Memory release failed!\n");
            return;
        }
        pFailBuffNode = NULL;
    }
}

/******************************************************************************
 * Function           : RmConfAddToFailBuff
 * Input(s)           : pOID - Object identifier of the mib-object whose set 
 *                             routine failed
 *                      pVal - Value to be set
 * Output(s)          : None 
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function adds the failed configuration information
 *                      to the end of the SLL. This function takes care of 
 *                      allocating memory for the SLL node and adding to the 
 *                      SLL. 
 ******************************************************************************/
INT4
RmConfAddToFailBuff (tSNMP_OID_TYPE * pOID, tSNMP_MULTI_DATA_TYPE * pVal)
{
    tRmConfFailBuff    *pNewFailBuffNode = NULL;
    tSNMP_MULTI_DATA_TYPE *pMutliData = NULL;
#if  defined (MSR_WANTED) && defined (SNMPV3_WANTED)
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    UINT1               au1Data[RM_ERROR_STR_LEN];
#endif
    UINT4               u4Value = 0;
    INT4                i4RetVal = RM_SUCCESS;

#if  defined (MSR_WANTED) && defined (SNMPV3_WANTED)
    MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1Name, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1Data, 0, sizeof (au1Data));
#endif

    if ((pNewFailBuffNode = (tRmConfFailBuff *) MemAllocMemBlk
         (RM_CONF_FAIL_BUFF_POOL_ID ())) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "RmConfAddToFailBuff: "
                "Memory allocation failed!!..\n");
        RmHandleSystemRecovery (RM_CONF_FAIL_BUFF_ALLOC_FAILED);
        return RM_FAILURE;
    }

    MEMSET (pNewFailBuffNode, 0, sizeof (tRmConfFailBuff));

    TMO_SLL_Init_Node (&pNewFailBuffNode->NextConfMsgNode);

    if ((pNewFailBuffNode->pFailedOid = alloc_oid (pOID->u4_Length)) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmConfAddToFailBuff: Memory allocation for"
                " OID failed!!\n");
        if (MemReleaseMemBlock (RM_CONF_FAIL_BUFF_POOL_ID (),
                                (UINT1 *) pNewFailBuffNode) == MEM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "RmConfHandleBulkUpdCompEvent: Memory release failed!\n");
        }
        pNewFailBuffNode = NULL;
        return RM_FAILURE;
    }

    pNewFailBuffNode->pFailedOid->u4_Length = 0;
    WebnmCopyOid (pNewFailBuffNode->pFailedOid, pOID);
    pMutliData = &(pNewFailBuffNode->Data);
    pMutliData->i2_DataType = pVal->i2_DataType;

    switch (pVal->i2_DataType)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pMutliData->u4_ULongValue = pVal->u4_ULongValue;
            break;

        case SNMP_DATA_TYPE_INTEGER32:
            pMutliData->i4_SLongValue = pVal->i4_SLongValue;
            break;

        case SNMP_DATA_TYPE_OBJECT_ID:
            u4Value = pVal->pOidValue->u4_Length;
            pMutliData->pOidValue = alloc_oid (u4Value);

            if (pMutliData->pOidValue == NULL)
            {
                RM_TRC (RM_CRITICAL_TRC, "RmConfProcessValidMsg: Failed to "
                        "allocate memory for data type "
                        "SNMP_DATA_TYPE_OBJECT_ID\n");
                i4RetVal = RM_FAILURE;
            }
            else
            {
                pMutliData->pOidValue->u4_Length = 0;
                WebnmCopyOid (pMutliData->pOidValue, pVal->pOidValue);
            }
            break;

        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            u4Value = 0;
            u4Value = pVal->pOctetStrValue->i4_Length;
            pMutliData->pOctetStrValue = allocmem_octetstring (u4Value);

            if (pMutliData->pOctetStrValue == NULL)
            {
                RM_TRC (RM_CRITICAL_TRC, "Failed to allocate memory for the "
                        "data type\n");
                i4RetVal = RM_FAILURE;
            }
            else
            {
                MEMCPY (pMutliData->pOctetStrValue->pu1_OctetList,
                        pVal->pOctetStrValue->pu1_OctetList, u4Value);
            }
            break;

        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pMutliData->u4_ULongValue = pVal->u4_ULongValue;
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pMutliData->u8_Counter64Value.msn = pVal->u8_Counter64Value.msn;
            pMutliData->u8_Counter64Value.lsn = pVal->u8_Counter64Value.lsn;
            break;

        case SNMP_DATA_TYPE_NULL:
            break;

        default:
            RM_TRC (RM_FAILURE_TRC, "Invalid!!\n");
            i4RetVal = RM_FAILURE;
            break;
    }

#if  defined (MSR_WANTED) && defined (SNMPV3_WANTED)
    MsrConvertOidToString (pOID, au1Oid);
    SnmpONGetNameFromOid ((char *) au1Oid, (char *) au1Name);
    SNMPConvertDataToString (pMutliData, au1Data, pMutliData->i2_DataType);

    RM_TRC2 (RM_CRITICAL_TRC,
             "OID Value [%s] with NAME [%s] failed during synchup!!\n", au1Data,
             au1Name);
#endif

    if (i4RetVal == RM_SUCCESS)
    {
        TMO_SLL_Add (&(gRmConfInfo.RmConfigFailBuff),
                     &pNewFailBuffNode->NextConfMsgNode);
    }
    else
    {
        free_oid (pNewFailBuffNode->pFailedOid);
        pNewFailBuffNode->pFailedOid = NULL;
        if (MemReleaseMemBlock (RM_CONF_FAIL_BUFF_POOL_ID (),
                                (UINT1 *) pNewFailBuffNode) == MEM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "RmConfHandleBulkUpdCompEvent: Memory release failed!\n");
        }
        pNewFailBuffNode = NULL;
    }

    return i4RetVal;
}

/******************************************************************************
 * Function           : RmConfSearchOIDInFailBuff
 * Input(s)           : pOID - Object identifier 
 * Output(s)          : None 
 * Returns            : RM_SUCCESS (if given OID is present in the failure
 *                      buffer)/ RM_FAILURE
 * Action             : This function checks if the given OID is present in the
 *                      failure buffer.
 ******************************************************************************/
INT1
RmConfSearchOIDInFailBuff (tSNMP_OID_TYPE * pOID)
{
    tRmConfFailBuff    *pFailBuffNode = NULL;
    tSNMP_OID_TYPE     *pFailedOID = NULL;
    UINT4               u4Count;
    UINT1               u1MatchFound = OSIX_FALSE;

    TMO_SLL_Scan (&(gRmConfInfo.RmConfigFailBuff), pFailBuffNode,
                  tRmConfFailBuff *)
    {
        pFailedOID = pFailBuffNode->pFailedOid;

        if (pOID->u4_Length == pFailedOID->u4_Length)
        {
            u1MatchFound = OSIX_TRUE;
            for (u4Count = 0; u4Count < pOID->u4_Length; u4Count++)
            {
                if (pOID->pu4_OidList[u4Count] !=
                    pFailedOID->pu4_OidList[u4Count])
                {
                    u1MatchFound = OSIX_FALSE;
                    break;
                }
            }
            if (u1MatchFound == OSIX_TRUE)
            {
                return RM_SUCCESS;
            }
        }
    }
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmConfEnqConfigMsg
 * Input(s)           : pConfigMsg - pointer to the configuration message 
 * Output(s)          : None 
 * Returns            : RM_SUCCESS/ RM_FAILURE
 * Action             : This function will be invoked to post the configuration
 *                      syncup message to the SyncUp Task. After posting the
 *                      message, this function sends the event
 *                      RM_CONFIG_MSG_RCVD to the RM_CONFIG_SYNCUP_TASKID 

 ******************************************************************************/
INT4
RmConfEnqConfigMsg (tRmMsg * pData, UINT4 u4SeqNo, UINT2 u2DataLen)
{
    tRmConfigQMsg      *pConfigMsg;

    if (RM_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_NOT_RESTORED)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RM_STATIC_CONFIG_NOT_RESTORED -- Drop static config\n");
        return RM_FAILURE;
    }

    if ((pConfigMsg =
         (tRmConfigQMsg *) MemAllocMemBlk (RM_CONF_Q_MSG_POOL_ID ())) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmConfEnqConfigMsg: "
                "Memory allocation failed!!\n");
        return RM_FAILURE;
    }

    MEMSET (pConfigMsg, 0, sizeof (tRmConfigQMsg));

    pConfigMsg->RmConfigMsg.pData = pData;
    pConfigMsg->RmConfigMsg.u4SeqNo = u4SeqNo;
    pConfigMsg->RmConfigMsg.u2DataLen = u2DataLen;

    if (OsixQueSend (RM_CONF_Q_ID (), (UINT1 *) &pConfigMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmConfEnqConfigMsg: "
                "Osix Queue send Failed!!!\n");
        if (MemReleaseMemBlock (RM_CONF_Q_MSG_POOL_ID (), (UINT1 *) pConfigMsg)
            != MEM_SUCCESS)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmConfEnqConfigMsg: "
                    "Memory free Failed!!!\n");
        }
        return RM_FAILURE;
    }
    if (OsixEvtSend (RM_CONF_SYNCUP_TASK_ID (), RM_CONFIG_MSG_RCVD)
        == OSIX_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmConfEnqConfigMsg: "
                "Osix event send Failed!!!\n");
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmConfProcessConfigSyncUpInfo 
 * Input(s)           : pRmNotifConfChg - pointer to the configuration 
 *                                        information               
 * Output(s)          : *pu1CfgSyncData - pointer to the static configuration
 *                                        syncu-up message (including the RM
 *                                        header)
 *                      *pu4MsgLen      - length of the message
 * Returns            :  None
 * Action             : Invokes this function to form the static configuration
 *                      sync-up message based on the nmhSet status. If the
 *                      return value of the nmhSet is SNMP_SUCCESS then full
 *                      OID need to be constructed before forming the message.
 ******************************************************************************/
UINT4
RmConfProcessConfigSyncUpInfo (tRmNotifConfChg * pRmNotifConfChg,
                               UINT1 **pu1CfgSyncData, UINT4 *pu4MsgLen)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEntOid = NULL;
    UINT2               u2ConfMsgLen = 1;    /* Message type */
    UINT1               u1MessageType = 0;
    UINT1              *pu1Data = NULL;
    UINT1              *pu1BasePtr = NULL;
    UINT4               u4TotMsgLen = 0;

    /* Return from this function if any static conf. message received 
     * from applications on Standby/init node */

    if (RM_GET_NODE_STATE () != RM_ACTIVE)
    {
        RM_TRC1 (RM_CRITICAL_TRC,
                 "RmConfProcessConfigSyncUpInfo: "
                 "Static sync is valid only in Active "
                 "failed OID is %s !!\n", pRmNotifConfChg->pu1ObjectId);
        return RM_FAILURE;
    }

    if ((pRmNotifConfChg->i1ConfigSetStatus == SNMP_SUCCESS) &&
        (pRmNotifConfChg->i1MsgType == RM_UNUSED))
    {
        RM_TRC1 (RM_SYNCUP_TRC, "RmConfProcessConfigSyncUpInfo: Forming"
                 " RM_CONFIG_VALID_MSG with [Seq# %d]\n",
                 pRmNotifConfChg->u4SeqNo);

        pOid = alloc_oid (SNMP_MAX_OID_LENGTH);
        pEntOid = alloc_oid (SNMP_MAX_OID_LENGTH);

        if ((pOid == NULL) || (pEntOid == NULL))
        {
            free_oid (pOid);
            free_oid (pEntOid);
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "RmConfProcessConfigSyncUpInfo: MEM alloc failed!! \n");
            return RM_FAILURE;
        }

        if (RmUtilFormFullOID (pRmNotifConfChg->pu1ObjectId,
                               pRmNotifConfChg->pMultiIndex, pOid)
            != RM_SUCCESS)
        {
            RM_TRC1 (RM_SYNCUP_TRC | RM_FAILURE_TRC,
                     "RmConfProcessConfigSyncUpInfo: OID formation failed for "
                     "oid %s!!\n", pRmNotifConfChg->pu1ObjectId);
            free_oid (pOid);
            free_oid (pEntOid);
            return RM_FAILURE;
        }

        /* verify the OID is in FilterList */
        if (RmConfIsOIDInFilterList (pOid) == OSIX_TRUE)
        {
            free_oid (pOid);
            free_oid (pEntOid);
            return RM_FAILURE;
        }

        SNMPRevertEOID (pOid, pEntOid);

        u1MessageType = RM_CONFIG_VALID_MSG;
    }
    else
    {
        /* Configuration has failed at the Active node. But as the sequence
         * number had been taken before appling the configuration, a dummy 
         * message needs to be send to the standby to avoid the sequence 
         * number mismatch.
         */

        switch (pRmNotifConfChg->i1MsgType)
        {
            case RM_CONFIG_IGNORE_ABORT:
            {
                RM_TRC1 (RM_SYNCUP_TRC, "RmConfProcessConfigSyncUpInfo: Forming"
                         " RM_CONFIG_IGNORE_ABORT with [Seq# %d]\n",
                         pRmNotifConfChg->u4SeqNo);
                u1MessageType = RM_CONFIG_IGNORE_ABORT;

                break;
            }
            case RM_CONFIG_ACCEPT_ABORT:
            {

                RM_TRC1 (RM_SYNCUP_TRC, "RmConfProcessConfigSyncUpInfo: Forming"
                         " RM_CONFIG_ACCEPT_ABORT with [Seq# %d]\n",
                         pRmNotifConfChg->u4SeqNo);
                u1MessageType = RM_CONFIG_ACCEPT_ABORT;

                break;
            }
            default:
            {
                if (pRmNotifConfChg->pu1ObjectId != NULL)
                {
                    RM_TRC1 (RM_SYNCUP_TRC,
                             "RmConfProcessConfigSyncUpInfo: "
                             "Configuration failed OID is %s !!\n",
                             pRmNotifConfChg->pu1ObjectId);
                }

                RM_TRC1 (RM_SYNCUP_TRC, "RmConfProcessConfigSyncUpInfo: Forming"
                         " RM_CONFIG_DUMMY_MSG with [Seq# %d]\n",
                         pRmNotifConfChg->u4SeqNo);
                u1MessageType = RM_CONFIG_DUMMY_MSG;

                break;
            }
        }
    }

    if (u1MessageType == RM_CONFIG_VALID_MSG)
    {
        RM_GET_CONF_MSG_LEN (u2ConfMsgLen, pEntOid->u4_Length,
                             pRmNotifConfChg->pMultiData);
    }

    /* Total packet length includes RM Header length and the configuration
     * message length*/
    u4TotMsgLen = RM_HDR_LENGTH + u2ConfMsgLen;

    /* Allocate memory for linear buffer */
    if ((pu1Data = MemAllocMemBlk (gRmInfo.RmPktMsgPoolId)) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "RmConfFormAndSendConfigSyncUpMsg: Rm linear buf allocation "
                " failed !!!\n");
        free_oid (pOid);
        free_oid (pEntOid);
        return (RM_FAILURE);
    }

    MEMSET (pu1Data, 0, u4TotMsgLen);
    pu1BasePtr = pu1Data;

    pu1Data = (pu1Data + RM_HDR_LENGTH);

    RmUtilFormConfigSyncUpMsg (u1MessageType, u2ConfMsgLen, pu1Data,
                               pEntOid, pRmNotifConfChg->pMultiData);
    RmUtilFillRMHeader (pu1BasePtr, (UINT2) u4TotMsgLen,
                        RM_STATIC_CONF_APP_ID, RM_STATIC_CONF_APP_ID,
                        pRmNotifConfChg->u4SeqNo);

    RmTrcPrintModSyncMsgTrace (pu1BasePtr);
    *pu1CfgSyncData = pu1BasePtr;
    *pu4MsgLen = u4TotMsgLen;

    if (pOid != NULL)
    {
        free_oid (pOid);
        pOid = NULL;
    }
    free_oid (pEntOid);
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmConfPostBulkUpdCompEvent 
 * Input(s)           : u4Event - Event to be posted to the RM_CONF_SYNCUP_TASK 
 * Output(s)          : None 
 * Returns            : None
 * Action             : Invokes this function to post the event to the
 *                      static configuration sync-up task.
 ******************************************************************************/
VOID
RmConfPostBulkUpdCompEvent (UINT4 u4Event)
{
    if (RM_SEND_EVENT (RM_CONF_SYNCUP_TASK_ID (), u4Event) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "SendEvent failed in RmConfSyncUpTaskMain\n");
        return;
    }
}

/******************************************************************************
 * Function           : RmConfStaticConfDBLock 
 * Input(s)           : None 
 * Output(s)          : None 
 * Returns            : SNMP_SUCCESS / SNMP_FAILURE
 * Action             : This function is used to take the Static Configuration
 *                      mutual exclusion semaphore to avoid simultaneous access
 *                      to data structures used by the Static configuration
 *                      syncup task.
 ******************************************************************************/
INT4
RmConfStaticConfDBLock (VOID)
{
    if (OsixSemTake (RM_CONF_SYNC_SEMID ()) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "Failed to Take Static Configuration"
                "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : RmConfStaticConfDBUnLock 
 * Input(s)           : None 
 * Output(s)          : None 
 * Returns            : SNMP_SUCCESS / SNMP_FAILURE
 * Action             : This function is used to give the Static Configuration
 *                      mutual exclusion Semaphore.
 ******************************************************************************/
INT4
RmConfStaticConfDBUnLock (VOID)
{
    if (OsixSemGive (RM_CONF_SYNC_SEMID ()) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "Failed to Give Static Configuration"
                "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : RmErrorStringOID
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : SNMP_SUCCESS / SNMP_FAILURE
 * Action             : This function is used to update the Error string OID
 ******************************************************************************/
INT1
RmErrorStringOID (tSNMP_OID_TYPE * pOID, UINT1 *pu1Oid,
                  tSNMP_MULTI_DATA_TYPE * pObjVal, UINT1 *pu1Data)
{

#ifdef WEBNM_WANTED
    WebnmConvertOidToString (pOID, pu1Oid);

    /* As the array size of au1Data has been reduced to RM_ERROR_STR_LEN 
     * from RM_MAX_DATA_LEN, this function(SNMPConvertDataToString) expects data length of ObjVal 
     * to be of RM_ERROR_STR_LEN.
     * If the size is greater than RM_ERROR_STR_LEN, un-expected results may occur */

    if ((pObjVal->i2_DataType == SNMP_DATA_TYPE_OCTET_PRIM) &&
        pObjVal->pOctetStrValue->i4_Length >= RM_ERROR_STR_LEN)
    {
        RM_TRC1 (RM_SYNCUP_TRC | RM_FAILURE_TRC,
                 "Value exceeds the RM_ERROR_STR_LEN" "OID = %s\n", pu1Oid);
        return RM_FAILURE;
    }
    else
    {
        SNMPConvertDataToString (pObjVal, pu1Data, pObjVal->i2_DataType);
    }
#else
    UNUSED_PARAM (pOID);
    UNUSED_PARAM (pu1Oid);
    UNUSED_PARAM (pObjVal);
    UNUSED_PARAM (pu1Data);
#endif
    return RM_SUCCESS;
}

#endif /* _RMCONF_C_ */
