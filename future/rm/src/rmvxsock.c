/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmvxsock.c,v 1.13 2014/03/18 12:27:06 siva Exp $
*
* Description: Redundancy manager socket interface functions.
*********************************************************************/
#ifdef RM_WANTED
#include "rmincs.h"
#include "rmhbincs.h"

/* Include the proper inclusion files in vxworks environment
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>
*/

#ifdef write
#undef write
#endif
#ifdef read
#undef read
#endif
#include <unistd.h>

extern CHR1        *gapc1AppName[];
#define RM_FT_SERVER_TCP_PORT 7215
#define RM_MAX_CONN_REQ_PEND  MAX_NO_OF_FILES

#define RM_SYNC_TCP_SERVER_PORT 7217
#define RM_SYNC_TCP_MAX_CONN  5

/******************************************************************************
 * Function           : RmTcpSrvSockInit 
 * Input(s)           : pi4SrvSockFd - Pointer to server socket fd
 * Output(s)          : TCP server socket descriptor
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create a TCP server scoket, set socket options 
 *                      for accepting the connections from peer.
 ******************************************************************************/
INT4
RmTcpSrvSockInit (INT4 *pi4SrvSockFd)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockInit: ENTRY \r\n");
    UNUSED_PARAM (pi4SrvSockFd);
    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockInit: EXIT \r\n");
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmTcpAcceptNewConnection
 * Input(s)           : i4SrvSockFd - Server socket fd
 *                      pi4SockFd - Pointer to client socket fd
 *                      pu4PeerAddr - Peer IP address
 * Output(s)          : New connection identifier
 * Returns            : None
 * Action             : This routine accepts the
 *                      connection from the TCP peer.
 ******************************************************************************/
INT4
RmTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4SockFd, UINT4 *pu4PeerAddr)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpAcceptNewConnection: ENTRY \r\n");
    UNUSED_PARAM (i4SrvSockFd);
    UNUSED_PARAM (pi4SockFd);
    UNUSED_PARAM (pu4PeerAddr);
    RM_TRC (RM_SOCK_TRC, "RmTcpAcceptNewConnection: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmTcpSockConnect 
 * Input(s)           : u4PeerAddr -> Peer address to be connected
 *                      pi4Connfd -> Pointer to the connection fd
 *                      pi4ConnState -> Pointer to the connection status
 * Output(s)          : None 
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine used for connecting the TCP peer 
 ******************************************************************************/
INT4
RmTcpSockConnect (UINT4 u4PeerAddr, INT4 *pi4Connfd, INT4 *pi4ConnState)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: ENTRY \r\n");
    UNUSED_PARAM (u4PeerAddr);
    UNUSED_PARAM (pi4Connfd);
    UNUSED_PARAM (pi4ConnState);
    RM_TRC (RM_SOCK_TRC, "RmTcpSockConnect: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmTcpSockSend 
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : i4WrBytes - No. of bytes written
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send RM protocol packets.
 ******************************************************************************/
UINT4
RmTcpSockSend (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2PktLen, INT4 *pi4WrBytes)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpSockSend: ENTRY \r\n");
    UNUSED_PARAM (i4ConnFd);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2PktLen);
    UNUSED_PARAM (pi4WrBytes);
    RM_TRC (RM_SOCK_TRC, "RmTcpSockSend: Successful EXIT \r\n");
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmTcpSockRcv 
 * Input(s)           : i4ConnFd - Connection fd
 *                      pu1Data - Pointer to data to be send
 *                      u2PktLen - Packet length
 * Output(s)          : pi4RdBytes - No. of bytes received
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send RM protocol packets.
 ******************************************************************************/
UINT4
RmTcpSockRcv (INT4 i4ConnFd, UINT1 *pu1Data, UINT2 u2MsgLen, INT4 *pi4RdBytes)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpSockRcv: ENTRY \r\n");
    UNUSED_PARAM (i4ConnFd);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2MsgLen);
    UNUSED_PARAM (pi4RdBytes);
    RM_TRC (RM_SOCK_TRC, "RmTcpSockRcv: Successful EXIT \r\n");
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmFtServSockInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Socket descriptor
 * Action             : Routine to open a server socket for file transfer 
 *                      operation.
 ******************************************************************************/
INT4
RmFtServSockInit (VOID)
{
    RM_TRC (RM_SOCK_TRC, "RmFtServSockInit: ENTRY \r\n");
    RM_TRC (RM_SOCK_TRC, "RmFtServSockInit: EXIT \r\n");
    return (RM_INV_SOCK_FD);
}

/******************************************************************************
 * Function           : RmIsFatalErrorOccured
 * Input(s)           : i4SockFd - Socket for which the 
 *                                 criticality of the error is required.
 * Output(s)          : None.
 * Returns            : RM_TRUE/RM_FALSE
 * Action             : Routine get the error information on the socket
 ******************************************************************************/
BOOL1
RmIsFatalErrorOccured (INT4 i4SockFd)
{
    /*
     * Following option has to be enabled for the errno unsupported
     * socket implementation.
     INT1                i1OptVal = 0;
     INT4                i4OptLen = sizeof (INT1);
     getsockopt (i4SockFd, SOL_SOCKET, SO_ERROR, &i1OptVal, (UINT4 *)&i4OptLen);
     if ((i1OptVal == EWOULDBLOCK) || (i1OptVal == EAGAIN))
     */
    UNUSED_PARAM (i4SockFd);
    return RM_TRUE;
}

/******************************************************************************
 * Function           : RmAcceptIncomingConn
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Client Socket descriptor
 * Action             : Routine to accept incoming connection req from various
 *                      clients. 
 ******************************************************************************/
INT4
RmAcceptIncomingConn (INT4 i4FtSrvSockFd)
{
    UNUSED_PARAM (i4FtSrvSockFd);
    return (RM_INV_SOCK_FD);
}

/******************************************************************************
 * Function           : RmSendFile
 * Input(s)           : i4CliSockFd - SockFd to which the file has to be sent.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the file to the specified SockFd. 
 ******************************************************************************/
UINT4
RmSendFile (INT4 i4CliSockFd)
{
    UNUSED_PARAM (i4CliSockFd);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmRcvFile
 * Input(s)           : pc1FileName - Name of the file to be rcvd.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to receive the file (read from socket and store
 *                      in file). If the file is present, it is overwritten. If
 *                      the file is not present, file is created.
 ******************************************************************************/
UINT4
RmRcvFile (const CHR1 * pc1FileName)
{
    RM_TRC (RM_SOCK_TRC, "Rm File Transfer Client receiving file...\n");
    UNUSED_PARAM (pc1FileName);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : ReadNbytes
 * Input(s)           : fd: file desc (or) sock desc.
 *                      pc1Ptr: ptr to which the data has to be read
 *                      n: length of bytes to be read
 * Output(s)          : None.
 * Returns            : No. of bytes read.
 * Action             : Read N bytes of data from the specified descriptor. 
 ******************************************************************************/
size_t
ReadNbytes (int fd, CHR1 * pc1Ptr, size_t n)
{
    UNUSED_PARAM (fd);
    UNUSED_PARAM (pc1Ptr);
    UNUSED_PARAM (n);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : WriteNbytes
 * Input(s)           : fd: file desc (or) sock desc.
 *                      pc1Ptr: ptr from which the data has to be taken 
 *                      for writing
 *                      n: length of bytes to be written
 * Output(s)          : None.
 * Returns            : No. of bytes written.
 * Action             : Write N bytes of data to the specified descriptor. 
 ******************************************************************************/
size_t
WriteNbytes (int fd, CHR1 * pc1Ptr, size_t n)
{
    UNUSED_PARAM (fd);
    UNUSED_PARAM (pc1Ptr);
    UNUSED_PARAM (n);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 ******************************************************************************/
INT4
RmCloseSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    return (-1);
}

/******************************************************************************
 * Function           : RmGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : RM_SUCCESS or RM_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 ******************************************************************************/
UINT4
RmGetIfIpInfo (UINT1 *pu1IfName, tNodeInfo * pNodeInfo)
{
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pNodeInfo);
    return (RM_SUCCESS);
}

/****************************************************************************
 * Function           : RmHbRawSockInit 
 * Input(s)           : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          : RM HB Raw socket descriptor.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create a RAW IP socket, set socket options 
 *                      for  sending and receiving RM multicast packets.
 ****************************************************************************/
INT4
RmHbRawSockInit (INT4 *pi4HbSockFd)
{
    UNUSED_PARAM (pi4HbSockFd);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmHbRawSockSend 
 * Input(s)           : pu1Data - pointer to the data to be sent out
 *                      u2PktLen - Length of the data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the packet out thru' RAW IP socket.
 *                      This is socket is specifically used by RM module to
 *                      send the Heart Beat messages destined to the 
 *                      multicast ip address 224.0.0.250.
 ******************************************************************************/
UINT4
RmHbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2PktLen);
    UNUSED_PARAM (u4DestAddr);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmHbRawSockRcv 
 * Input(s)           : pu1Data - pointer to rcv the packet
 *                      u2BufSize - size of the buffer to be received.
 * Output(s)          : pu1Data - pointer to the packet rcvd. 
 *                      pu4PeerAddr - IP addr of the peer from which the 
 *                                    pkt is rcvd.   
 * Returns            : Length of the packet received.
 * Action             : Routine to receive RM protocol packets.
 ******************************************************************************/
INT4
RmHbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufSize);
    UNUSED_PARAM (pu4PeerAddr);
    return (-1);
}

/******************************************************************************
 * Function           : RmHbCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 ******************************************************************************/
INT4
RmHbCloseSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    return (-1);
}

/******************************************************************************
 * Function           : RmGetIfIndexFromName
 * Input(s)           : pu1IfName - Interface name.
 * Output(s)          : None.
 * Returns            : IfIndex
 * Action             : Routine to get the Interface index of the given 
 *                      Interface name.
 ******************************************************************************/
UINT4
RmGetIfIndexFromName (UINT1 *pu1IfName)
{
    UNUSED_PARAM (pu1IfName);
    return (0);
}

/******************************************************************************
 * Function           : RmHbGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : RM_SUCCESS or RM_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 ******************************************************************************/
UINT4
RmHbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo)
{
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pNodeInfo);
    return (RM_SUCCESS);
}

/******************************************************************************           * Function           : RmGetFileFromPeer                                                 * Input(s)           : i4FtSrvSockFd
 * Output(s)          : None.                                                             * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task to receive the requested file.
 ******************************************************************************/ INT4
RmGetFileFromPeer (INT4 i4FtSrvSockFd)
{
    UNUSED_PARAM (i4FtSrvSockFd);
    return (RM_SUCCESS);
}

/******************************************************************************           
* Function           : RmSendFiletoAttachedPeer                             
* Input(s)           : None
* Output(s)          : None.
* Returns            : RM_SUCCESS/RM_FAILURE                                  
* Action             : Task to receive the requested file.
******************************************************************************/
UINT4
RmSendFiletoAttachedPeer (INT4 i4TskIndex)
{
    UNUSED_PARAM (i4TskIndex);
    return (RM_SUCCESS);
}

/******************************************************************************
* Function           : RmSendFirmwareImage                                    
* Input(s)           : None
* Output(s)          : None.
* Returns            : RM_SUCCESS/RM_FAILURE                                  
* Action             : Task to receive the requested file.
******************************************************************************/
UINT4
RmSendFirmwareImage (INT4 i4TskIndex)
{
    UNUSED_PARAM (i4TskIndex);
    return (RM_SUCCESS);
}

/******************************************************************************
* Function           : RmStandbySendFile                                    
* Input(s)           : None
* Output(s)          : None.
* Returns            : RM_SUCCESS/RM_FAILURE                                  
* Action             : Task to receive the requested file.
******************************************************************************/
UINT4
RmStandbySendFile (const CHR1 * au1LogFile)
{
    UNUSED_PARAM (au1LogFile);
    return (RM_SUCCESS);
}
#endif
