/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmutil.c,v 1.72 2017/12/26 13:34:30 siva Exp $
*
* Description: RM utility functions.
***********************************************************************/
#include "rmincs.h"
#ifdef MBSM_WANTED
#include "mbsnp.h"
#endif
#include "fssocket.h"
#include "rmglob.h"

#ifdef MBSM_WANTED
/* array of MAX_LC elements - used to check whether the LM has been cleared */
extern UINT1        gau1LcTbl[MBSM_MAX_SLOTS];
#endif /* MBSM_WANTED */

/*****************************************************************************/
/* Function Name      : RmLock                                               */
/*                                                                           */
/* Description        : This function is used to take the RM mutual          */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : RMGR, Protocols registered with RMGR & SNMP/CLI      */
/*****************************************************************************/
INT4
RmLock (VOID)
{
    if (OsixSemTake (RM_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "Failed to Take RM Protocol "
                "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmUnLock                                             */
/*                                                                           */
/* Description        : This function is used to give the RM mutual          */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : RMGR, Protocols registered with RMGR & SNMP/CLI      */
/*****************************************************************************/
INT4
RmUnLock (VOID)
{
    if (OsixSemGive (RM_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "Failed to Give RM Protocol "
                "Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : RmMemAllocForResBuf
 * Input(s)           : ppResBuf -> Pointer to pointer residual buffer 
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : This routine allocates the memory for residual buffer
 ******************************************************************************/
UINT1
RmMemAllocForResBuf (UINT1 **ppResBuf)
{
    RM_TRC (RM_SOCK_TRC, "RmMemAllocForResBuf: ENTRY \r\n");
    *ppResBuf = MemAllocMemBlk (gRmInfo.RmPktMsgPoolId);
    if (*ppResBuf == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "RmMemAllocForResBuf: Memory allocation failed "
                "for the residual buffer !!!\r\n");
        return RM_FAILURE;
        /* Reception residual buffer memory allocation failed */
    }
    MEMSET (*ppResBuf, 0, RM_MAX_SYNC_PKT_LEN);
    RM_TRC (RM_SOCK_TRC, "RmMemAllocForResBuf: EXIT \r\n");
    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmInitBulkUpdatesStatusMask                          */
/*                                                                           */
/* Description        : This function is used by RMGR to initialize          */
/*                      the bulk updates completion status mask. The bits    */
/*                      of the applications for which bulk updates           */
/*                      completion needs to be verified before accepting     */
/*                      force switchover                                     */
/*                                                                           */
/* Input(s)           : u4AppId - Application Id                             */
/*                      u1SetBit - RM_TRUE (to set) / RM_FALSE (to reset)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS or RM_FAILURE                             */
/*                                                                           */
/* Called By          : RMGR                                                 */
/*****************************************************************************/
INT4
RmInitBulkUpdatesStatusMask (UINT4 u4AppId, UINT1 u1SetBit)
{
    UINT1               u1SetBulkUpdateMask = RM_FALSE;

#ifdef VCM_WANTED
    if (u4AppId == RM_VCM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef MBSM_WANTED
    if (u4AppId == RM_MBSM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef LA_WANTED
    if (u4AppId == RM_LA_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef ECFM_WANTED
    if (u4AppId == RM_ECFM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef ELPS_WANTED
    if (u4AppId == RM_ELPS_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
#ifdef QOSX_WANTED
    if (u4AppId == RM_QOS_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
    if (u4AppId == RM_ACL_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#ifdef DCBX_WANTED
    if (u4AppId == RM_DCBX_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
#ifdef PNAC_WANTED
    if (u4AppId == RM_PNAC_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef RSTP_WANTED
    if (u4AppId == RM_RSTPMSTP_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef PBB_WANTED
    if (u4AppId == RM_PBB_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef VLAN_WANTED
    if (u4AppId == RM_VLANGARP_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef ELMI_WANTED
    if (u4AppId == RM_ELMI_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#if defined(IGS_WANTED) || defined(MLDS_WANTED)
    if (u4AppId == RM_SNOOP_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef CFA_WANTED
    if (u4AppId == RM_CFA_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef EOAM_WANTED
    if (u4AppId == RM_EOAM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef PBBTE_WANTED
    if (u4AppId == RM_PBBTE_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef NPAPI_WANTED
    if (u4AppId == RM_NP_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef OSPF_WANTED
    if (u4AppId == RM_OSPF_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#ifdef OSPFV3_WANTED
    if (u4AppId == RM_OSPFV3_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

#if defined(PIM_WANTED) || defined(PIMV6_WANTED)
    if (u4AppId == RM_PIM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
#ifdef ARP_WANTED
    if (u4AppId == RM_ARP_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
#ifdef IP_WANTED
    if (u4AppId == RM_RTM_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif
#ifdef IP6_WANTED
    if (u4AppId == RM_ND6_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }

    if (u4AppId == RM_RTM6_APP_ID)
    {
        u1SetBulkUpdateMask = RM_TRUE;
    }
#endif

    if (u1SetBulkUpdateMask == RM_TRUE)
    {
        if (u1SetBit == RM_TRUE)
        {
            OSIX_BITLIST_SET_BIT (gRmInfo.RmBulkUpdStsMask, u4AppId,
                                  sizeof (tRmAppList));
            RM_TRC1 (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                     "RmInitBulkUpdatesStatusMask: "
                     "AppId=%s Register for Bulk\n", gapc1AppName[u4AppId]);
        }
        else
        {
            OSIX_BITLIST_RESET_BIT (gRmInfo.RmBulkUpdStsMask, u4AppId,
                                    sizeof (tRmAppList));
            RM_TRC1 (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                     "RmInitBulkUpdatesStatusMask: "
                     "AppId=%s De-Register for Bulk\n", gapc1AppName[u4AppId]);
        }
    }
    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmClearBulkUpdatesStatus                             */
/*                                                                           */
/* Description        : This function is used by RMGR to clear the           */
/*                      bulk updates status of all the registered protocols  */
/*                      either during peer down or when the node state       */
/*                      becomes standby                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS or RM_FAILURE                             */
/*                                                                           */
/* Called By          : RMGR                                                 */
/*****************************************************************************/
INT4
RmClearBulkUpdatesStatus (VOID)
{
    /* Triggered by RMGR during 
     * 1) Node state becomes standby
     * 2) Peer down.
     * So all registered application's configuration status should be cleared.
     */
    MEMSET (gRmInfo.RmBulkUpdSts, 0, sizeof (tRmAppList));

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmCheckBulkUpdatesStatus                             */
/*                                                                           */
/* Description        : This function is used by RMGR to check the           */
/*                      configuration status of all the registered protocols */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_TRUE or RM_FALSE                                  */
/*                                                                           */
/* Called By          : RMGR                                                 */
/*****************************************************************************/
INT4
RmCheckBulkUpdatesStatus (VOID)
{
    UINT4               u4Index = 0;
    INT4                i4RetVal = RM_TRUE;
    UINT4               u4AppId = 0;

    for (u4Index = 0; u4Index < sizeof (tRmAppList); u4Index++)
    {
        if (gRmInfo.RmBulkUpdStsMask[u4Index] == 0)
        {
            /* No applications present in this range of index */
            continue;
        }

        if ((gRmInfo.RmBulkUpdSts[u4Index] &
             gRmInfo.RmBulkUpdStsMask[u4Index]) !=
            gRmInfo.RmBulkUpdStsMask[u4Index])
        {
            i4RetVal = RM_FALSE;
            break;
        }
    }
    if (i4RetVal == RM_FALSE)
    {
        for (u4AppId = RM_VCM_APP_ID; u4AppId < RM_MAX_APPS; u4AppId++)
        {
            if ((u4AppId == RM_MPLS_APP_ID) ||
                (u4AppId == RM_SNMP_APP_ID) ||
                (u4AppId == RM_ISS_APP_ID) ||
                (u4AppId == RM_CLI_APP_ID) || (u4AppId == RM_MSR_APP_ID))
            {
                continue;
            }
            RM_UNLOCK ();
            if (RmGetBulkUpdatesStatus (u4AppId) == RM_FAILURE)
            {
                RM_TRC1 (RM_CRITICAL_TRC,
                         " Bulk update yet to be completed for %s module\n",
                         gapc1AppName[u4AppId]);
            }
            RM_LOCK ();
        }
        return i4RetVal;
    }
    return i4RetVal;
}

#ifdef L2RED_TEST_WANTED
/*****************************************************************************/
/* Function Name      : RmTriggerAuditEvent                                  */
/*                                                                           */
/* Description        : This function is used by RMGR to                     */
/*                      start dynamic synch audit for the given protocol     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gRmDynAuditInfo                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gRmDynAuditInfo                                      */
/*                                                                           */
/* Return Value(s)    : RM_TRUE or RM_FALSE                                  */
/*                                                                           */
/* Called By          : RMGR                                                 */
/*****************************************************************************/
INT4
RmTriggerAuditEvent (INT4 i4AppId)
{
    INT4                i4RetVal = RM_SUCCESS;
    UINT2               u2AppId;
    if (i4AppId & VCM_MOD)
    {
        gRmDynAuditInfo[RM_VCM_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_VCM_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & LA_MOD)
    {
        gRmDynAuditInfo[RM_LA_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_LA_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & VLAN_MOD)
    {
        gRmDynAuditInfo[RM_VLANGARP_APP_ID].u4AuditStatus =
            RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_VLANGARP_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & SNTP_MOD)
    {
        gRmDynAuditInfo[RM_SNTP_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_SNTP_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & MBSM_MOD)
    {
        gRmDynAuditInfo[RM_MBSM_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_MBSM_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & CFA_MOD)
    {
        gRmDynAuditInfo[RM_CFA_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_CFA_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & ACL_MOD)
    {
        gRmDynAuditInfo[RM_ACL_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_ACL_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & OSPF_MOD)
    {
        gRmDynAuditInfo[RM_OSPF_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_OSPF_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & RIP_MOD)
    {
        gRmDynAuditInfo[RM_RIP_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_RIP_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & BFD_MOD)
    {
        gRmDynAuditInfo[RM_BFD_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_BFD_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & ISIS_MOD)
    {
        gRmDynAuditInfo[RM_ISIS_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_ISIS_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & ARP_MOD)
    {
        gRmDynAuditInfo[RM_ARP_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_ARP_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & RTM_MOD)
    {
        gRmDynAuditInfo[RM_RTM_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_RTM_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & RTM6_MOD)
    {
        gRmDynAuditInfo[RM_RTM6_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_RTM6_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & ND6_MOD)
    {
        gRmDynAuditInfo[RM_ND6_APP_ID].u4AuditStatus = RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_ND6_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    if (i4AppId & OSPF3_MOD)
    {
        gRmDynAuditInfo[RM_OSPFV3_APP_ID].u4AuditStatus =
            RM_DYN_AUDIT_INPROGRESS;
        gRmDynAuditInfo[RM_OSPFV3_APP_ID].u4IsAppSetForAudit = RM_TRUE;
    }
    /*Posting Dynamic Audit trigger to all the Modules which are set in the bitlist */
    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        if (gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit == RM_TRUE)
        {
            i4RetVal =
                RmUtilPostDataOrEvntToProtocol (u2AppId, RM_DYNAMIC_SYNCH_AUDIT,
                                                NULL, 0, 0);
            if (i4RetVal == RM_FAILURE)
            {
                gRmDynAuditInfo[u2AppId].u4AuditStatus = RM_DYN_AUDIT_ABORTED;
                gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit = RM_FALSE;
            }
        }
    }
    return i4RetVal;
}
#endif

/******************************************************************************
 * Function           : RmHandleNodeStateUpdate
 * Input(s)           : u1State - RM_ACTIVE/ RM_STANDBY 
 * Output(s)          : None 
 * Returns            : None
 * Action             : This routine updates the node status 
 *                      and gives the GO_ACTIVE/GO_STANDBY to 
 *                      RED enabled modules.  
 ******************************************************************************/

VOID
RmHandleNodeStateUpdate (UINT1 u1State)
{
    tRmNotificationMsg  NotifMsg;
#ifndef L2RED_WANTED
    tStackNotificationMsg StackNotifyMsg;
#else
    UINT4               u4NextAppId = RM_APP_ID;
#ifdef MBSM_WANTED
    UINT1               u1MbsmEvt = 0;
#endif
#endif
    tRmProtoEvt         ProtoEvt;
#ifdef L2RED_TEST_WANTED
    UINT2               u2AppId = 0;
#endif /* L2RED_TEST_WANTED */
    UINT1               u1PrevState = 0;
    UINT1               u1Event = RM_INIT;
    UINT1               u1SsnCnt = 0;
    tFmFaultMsg         FmFaultMsg;

    /* FmFaultMsg is filled to post the critical event to FM module.
     * Module Id,Module name and critical event information is filled
     * here */
    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.u4ModuleId = OsixGetCurTaskId ();
    MEMCPY (FmFaultMsg.ModuleName, OsixExGetTaskName (FmFaultMsg.u4ModuleId),
            sizeof (FmFaultMsg.ModuleName));

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleNodeStateUpdate: ENTRY \r\n");
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

    /* Reset the Rx buffer related flags and seq number */
    RmRxResetRxBuffering ();

    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_FALSE);

    if (u1State == RM_GET_NODE_STATE ())
    {
        /* no change in node state */
        return;
    }

#ifdef L2RED_TEST_WANTED
    MEMSET (&gRmDynAuditInfo, 0, (RM_MAX_APPS * sizeof (tRmDynAuditInfo)));
    for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
    {
        gRmDynAuditInfo[u2AppId].u4AuditStatus = RM_DYN_AUDIT_NOT_TRIGGERED;
        gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit = RM_FALSE;
        gRmDynAuditInfo[u2AppId].i4AuditChkSumValue = RM_INVALID_CHKSUM_VALUE;
        gRmDynAuditInfo[u2AppId].i4RecvdChkSum = RM_INVALID_CHKSUM_VALUE;
    }
#endif

    u1PrevState = (UINT1) RM_GET_NODE_STATE ();
    RM_SET_PREV_NODE_STATE (u1PrevState);
    RM_SET_NODE_STATE (u1State);

    if ((u1PrevState == RM_STANDBY) && (u1State == RM_ACTIVE))
    {
        RM_IGNORE_ABORT_NOTIFICATION () = RM_FALSE;

        if (RM_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_RESTORED)
        {
            RM_TRC (RM_CRITICAL_TRC,
                    "Reloading ... the switch."
                    "Do erase the MSR configurations ... \r\n");
            IssEraseConfigurations ();
            /* To flush both software & hardware entries
             * reboot switch */
            RmHandleSystemRecovery (RM_STATE_CHG_DURING_STATIC_BLK);
            return;
        }

        if (gRmInfo.u1IsBulkUpdtInProgress == RM_TRUE)
        {
            RM_TRC (RM_CRITICAL_TRC, "Failover "
                    "occurred when Dynamic bulk update in progress\n");
            RM_TRC (RM_CRITICAL_TRC,
                    "Reloading ... the switch."
                    "Do save the MSR configurations ... \r\n");
            RmUnLock ();
            IssSaveConfigurations ();
            /* To flush both software & hardware entries
             * reboot switch */
            RmHandleSystemRecovery (RM_STATE_CHG_DURING_DYNAMIC_BLK);
            return;
        }
    }

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* In stacking environment, when a Active switch is 
         * getting attached with stack setup, it should be 
         * restarted after connected the stack cable */
        if (((u1PrevState == RM_ACTIVE) && (u1State == RM_STANDBY)) ||
            ((u1PrevState == RM_STANDBY) && (u1State == RM_ACTIVE)))
        {
            RM_TRC (RM_CRITICAL_TRC,
                    "Reloading ... the switch."
                    "Do not disturb stack connectivity... \r\n");
            /* To flush both software & hardware entries
             * reboot switch */
            RmHandleSystemRecovery (RM_COLDSTDBY_REBOOT);
        }
    }

    if (u1State == RM_ACTIVE)
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmHandleNodeStateUpdate: "
                "Node state changed to ACTIVE \r\n");
        RM_SET_ACTIVE_NODE_ID (RM_GET_SELF_NODE_ID ());
#ifdef L2RED_WANTED
        /* FSW case, STANDBY UP event is given to protocols here */
        if ((RM_PEER_NODE_COUNT () > 0) && (u1PrevState != RM_INIT))
        {
            RmUpdatePeerNodeCntToProtocols (RM_PEER_UP);
        }
#endif
    }
    else
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmHandleNodeStateUpdate: "
                "Node state changed to STANDBY\r\n");
        RM_SET_ACTIVE_NODE_ID (RM_GET_SSN_INFO (u1SsnCnt).u4PeerAddr);
    }
#ifdef L2RED_WANTED
    if ((u1PrevState == RM_ACTIVE) && (u1State == RM_STANDBY))
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmHandleNodeStateUpdate: "
                "RM_ACTIVE->RM_STANDBY switchover completed\r\n");
        RmAllowStaticAndDynamicSyncMsgs ();
    }
    else if ((u1PrevState == RM_STANDBY) && (u1State == RM_ACTIVE))
    {
        if (RM_PEER_NODE_COUNT () == 0)
        {
            RmUpdatePeerNodeCntToProtocols (RM_PEER_DOWN);
        }
    }
    if ((u1PrevState == RM_STANDBY) || (u1PrevState == RM_ACTIVE))
    {
        /* Clear switch over status */
        MEMSET (gRmInfo.RmSwitchoverSts, 0, sizeof (tRmAppList));

        NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
        NotifMsg.u4State = (UINT4) u1State;
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
        NotifMsg.u4Status = RM_STARTED;
        NotifMsg.u4Error = RM_NONE;
        UtlGetTimeStr (NotifMsg.ac1DateTime);
        RmTrapSendNotifications (&NotifMsg);
        /* After force standby completion, set ISSU command status
         * to success */
        if ((IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE) &&
            (u1State == GO_STANDBY) &&
            (RM_FORCE_SWITCHOVER_FLAG () != RM_FSW_OCCURED))
        {
            IssuApiForceStandbyCompleted ();
        }
        if (u1PrevState == RM_STANDBY)
        {
            /* Clear the buffer contents. */
            if (TMO_SLL_Count (&(gRmConfInfo.RmConfigFailBuff)) != 0)
            {
                RmUtilDelFailBuff ();
            }
        }

#ifdef MBSM_WANTED
        if (u1State == RM_ACTIVE)
        {
            u1MbsmEvt = RM_ACTIVE;
        }
        if (u1State == RM_STANDBY)
        {
            u1MbsmEvt = 0;
        }
        if (ISS_GET_STACKING_MODEL () != ISS_CTRL_PLANE_STACKING_MODEL)
        {
            RmMbsmNpProcRmNodeTransition (u1MbsmEvt, u1PrevState, u1State);
        }
#endif
    }
#endif
    /* Node state changes to STANDBY or IDLE */
    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        u1Event = GO_STANDBY;
#ifdef L2RED_WANTED
        RmSetTxEnable (RM_FALSE);
        RmFtSrvTaskDeInit ();

        RmClearBulkUpdatesStatus ();

        RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
        /* Processes the Rx Buffer */
        RmRxProcessBufferedPkt ();
        /* Set the Filecount to zero during Standby and Init state */
        gRmInfo.u1FileRcvCount = 0;
#endif
    }
    else if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        u1Event = GO_ACTIVE;
#ifdef L2RED_WANTED
#ifdef OSPF3_WANTED
        V3OspfRmHandleGoActiveNotify ();
#endif
        /* make sure you clean this up */
        RmFtSrvTaskDeInit ();

        if (RmFtSrvTaskInit () == RM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC, "RM File Transfer Task Init Failure\n");
            return;
        }
#endif
    }
    if ((u1Event == GO_ACTIVE) || (u1Event == GO_STANDBY))
    {
        /* Start Timer in active to block configurations during switch over */
        if ((u1Event == GO_ACTIVE) && (u1PrevState == GO_STANDBY))
        {
            if (gu4BulkUpdateInProgress == RM_FALSE)
            {
                gu4BulkUpdateInProgress = RM_TRUE;
                RmTmrStartTimer (RM_BULK_UPDATE_TIMER, RM_BULK_UPDATE_TMR_INT);
            }
        }
        /* Start timer in both active and standby to block the traffic */
        if (((u1Event == GO_STANDBY) && (u1PrevState == GO_ACTIVE)) ||
            ((u1Event == GO_ACTIVE) && (u1PrevState == GO_STANDBY)))
        {
            if (gu4TrafficToCpuAllowed == RM_TRUE)
            {
                gu4TrafficToCpuAllowed = RM_FALSE;
#ifdef NPAPI_WANTED
                RmFsNpHRSetStdyStInfo (NP_RM_HR_SUSPEND_COPY_TO_CPU_TRAFFIC,
                                       NULL);
#endif
                RmTmrStartTimer (RM_CPU_TRAFFIC_CTRL_TIMER,
                                 RM_CPU_TRAFFIC_TMR_INT);
            }
        }
        RM_SET_PREV_NOTIF_NODE_STATE (RM_GET_NODE_STATE ());
        RmNotifyProtocols (u1Event);
        /* Trigger bulk update initiation request,
         * after sending GO_STANDBY event */
#ifdef L2RED_WANTED
        if (u1Event == GO_STANDBY)
        {
            RM_TRC (RM_NOTIF_TRC,
                    "Initiate bulk request trigger after sending GO_STANDBY to protocols\n");
            RmUtilGetNextAppId (RM_APP_ID, &u4NextAppId);

            /* Intiate bulk update for the lowest layer protocol */
            if (u4NextAppId != RM_APP_ID)
            {
                ProtoEvt.u4AppId = RM_APP_ID;
                ProtoEvt.u4Error = RM_NONE;
                ProtoEvt.u4Event = RM_INITIATE_BULK_UPDATE;
                RM_UNLOCK ();
                RmApiHandleProtocolEvent (&ProtoEvt);
                RM_LOCK ();
            }
        }
#endif
    }
#ifndef MBSM_WANTED
    /* MBSM_WANTED Case, after processing self slot attach and 
     * remote slot attach, "FSW not occurred" will be set before sending
     * GO_ACTIVE to higher layer modules*/
    if (u1Event == GO_ACTIVE)
    {
        RM_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
    }
#endif
#ifndef L2RED_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if ((u1PrevState == RM_STANDBY) && (u1State == RM_ACTIVE))
        {
            gRmInfo.u4TrapSwitchId = RM_GET_SWITCH_ID ();
            StackNotifyMsg.u4NodeId = RM_GET_SWITCH_ID ();
            StackNotifyMsg.u4State = STACK_NOTIFY_STANDBY_TO_ACTIVE;
            UtlGetTimeStr (StackNotifyMsg.ac1DateTime);
            StackTrapSendNotifications (&StackNotifyMsg);
        }
    }
#endif
    FmFaultMsg.u4EventType = ISS_FM_RM_CRITICAL_EVENT_INDICATION;
    if (u1State == RM_ACTIVE)
    {
        FmFaultMsg.u4CriticalInfo = RM_FM_ACTIVE_IND;
    }
    else if (u1State == RM_STANDBY)
    {
        FmFaultMsg.u4CriticalInfo = RM_FM_STANDBY_IND;
    }
    FmApiNotifyFaults (&FmFaultMsg);
    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleNodeStateUpdate: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmGetSsnIndexFromPeerAddr
 * Input(s)           : u4PeerAddr -> Peer address
 *                      pu1SsnIndex -> Pointer to session index
 * Output(s)          : pu1SsnIndex -> Session index corresponding 
 *                                     to the peer address
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Gives the session index corresponding 
 *                      to the peer address
 ******************************************************************************/
UINT1
RmGetSsnIndexFromPeerAddr (UINT4 u4PeerAddr, UINT1 *pu1SsnIndex)
{
    UINT1               u1SsnCnt = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmGetSsnIndexFromPeerAddr: ENTRY \r\n");
    for (u1SsnCnt = 0; u1SsnCnt < RM_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (RM_GET_SSN_INFO (u1SsnCnt).u4PeerAddr == u4PeerAddr)
        {
            *pu1SsnIndex = u1SsnCnt;
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmGetSsnIndexFromPeerAddr:"
                     "Session entry found for the peer=%x EXIT\r\n",
                     u4PeerAddr);
            return (RM_SUCCESS);
        }
    }
    RM_TRC1 (RM_FAILURE_TRC, "RmGetSsnIndexFromPeerAddr: "
             "Session does not exist for the peer %x EXIT\r\n", u4PeerAddr);
    return (RM_FAILURE);
}

/******************************************************************************
 * Function           : RmGetSsnIndexFromConnFd
 * Input(s)           : i4ConnFd - Reliable connection file descriptor
 *                      pu1SsnIndex - Pointer of session table
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Gives the session table associated with 
 *                      the connection fd 
 ******************************************************************************/
UINT1
RmGetSsnIndexFromConnFd (INT4 i4ConnFd, UINT1 *pu1SsnIndex)
{
    UINT1               u1SsnCnt = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmGetSsnIndexFromPeerAddr: ENTRY \r\n");
    for (u1SsnCnt = 0; u1SsnCnt < RM_MAX_SYNC_SSNS_LIMIT; u1SsnCnt++)
    {
        if (RM_GET_SSN_INFO (u1SsnCnt).i4ConnFd == i4ConnFd)
        {
            *pu1SsnIndex = u1SsnCnt;
            RM_TRC (RM_CTRL_PATH_TRC, "RmGetSsnIndexFromPeerAddr: "
                    "Session found\r\n");
            return (RM_SUCCESS);
        }
    }
    RM_TRC (RM_FAILURE_TRC, "RmGetSsnIndexFromPeerAddr: "
            "Session does not exist for the connection fd\r\n");
    return (RM_FAILURE);
}

/******************************************************************************
 * Function           : RmNotifyProtcols
 * Input(s)           : u1Event - Events like GO_ACTIVE/GO_STANDBY.
 * Output(s)          : None.
 * Returns            : Nome. 
 * Action             : Routine used by RM to notify protocols.
 ******************************************************************************/
VOID
RmNotifyProtocols (UINT1 u1Event)
{
    UINT4               u4EntId = 0;
    UINT4               u4AppId = 0;
    UINT1               u1RetVal = RM_SUCCESS;
#ifdef NPAPI_WANTED
    if ((u1Event == GO_ACTIVE) || (u1Event == GO_STANDBY))
    {
        RmRmNpUpdateNodeState (u1Event);

    }
#endif

    /* GO_ACTIVE notification should be given only to MBSM during 
     * standby to active transition. After completing it's work during 
     * switchover, MBSM will call RmSendEventToAppln API to give GO_ACTIVE to 
     * all other protocols*/

#ifdef MBSM_WANTED
    if ((u1Event == GO_ACTIVE) &&
        (RM_GET_PREV_NODE_STATE () == RM_STANDBY) &&
        (RM_GET_NODE_STATE () == RM_ACTIVE))
    {
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            for (u4EntId = 1; u4EntId < RM_MAX_APPS; u4EntId++)
            {
                RmUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
            }
        }
        else
        {
            RmUtilPostDataOrEvntToProtocol (RM_MBSM_APP_ID, u1Event, NULL, 0,
                                            0);
        }
        return;
    }
#endif

#ifdef VCM_WANTED
    /* In case of VCM_WANTED, GO_STANDBY event should be send to VCM Module 
     * first and then MBSM for Sync-Up. After both module finished its work, 
     * it calls the RmSendEventToAppln API to send GO_STANDBY 
     * to all other protocols*/

    if ((u1Event == GO_STANDBY) &&
        (RM_GET_PREV_NODE_STATE () == RM_INIT) &&
        (RM_GET_NODE_STATE () == RM_STANDBY))
    {
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            for (u4EntId = 1; u4EntId < RM_MAX_APPS; u4EntId++)
            {
                RmUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
            }
        }
        else
        {
            RmUtilPostDataOrEvntToProtocol (RM_VCM_APP_ID, u1Event, NULL, 0, 0);
        }
        return;
    }
#else
#ifdef MBSM_WANTED
    /* MBSM Bulk Updates should come first and then other protocols */
    if ((u1Event == GO_STANDBY) &&
        (RM_GET_PREV_NODE_STATE () == RM_INIT) &&
        (RM_GET_NODE_STATE () == RM_STANDBY))
    {
        RmUtilPostDataOrEvntToProtocol (RM_MBSM_APP_ID, u1Event, NULL, 0, 0);
        return;
    }
#endif
#endif

    /* During INIT to ACTIVE transition, MSR should get the GO_ACTIVE
     * only after all other applications gets it.
     * Otherwise MSR will start restoring the configurations even before
     * the modules become active, and the modules may not program the
     * hw. */
    if ((u1Event == GO_ACTIVE) && (RM_GET_PREV_NODE_STATE () == RM_INIT))
    {
        u4AppId = RM_APP_ID;
        while ((u1RetVal =
                RmUtilGetNextAppId (u4AppId, &u4EntId)) == RM_SUCCESS)
        {
            if (u4EntId == RM_MSR_APP_ID)
            {
                u4AppId = u4EntId;
                continue;
            }
            /* Trigger to start bulk update by  the
             * next higher layer module registered. */
            RM_TRC1 (RM_NOTIF_TRC,
                     "Sending bulk update trigger to AppId %d\n", u4EntId);
            RmUtilPostDataOrEvntToProtocol (u4EntId, (UINT1) GO_ACTIVE,
                                            NULL, 0, 0);
            break;
        }
        if (u1RetVal == RM_FAILURE)
        {
            /* Give GO_ACTIVE to MSR after
             * all modules GO_ACTIVE given */
            u4EntId = RM_MSR_APP_ID;
            RM_TRC1 (RM_NOTIF_TRC,
                     "Sending bulk update trigger to AppId %d\n", u4EntId);
            RmUtilPostDataOrEvntToProtocol (u4EntId,
                                            (UINT1) GO_ACTIVE, NULL, 0, 0);

            RmSetTxEnable (RM_TRUE);
            RmTrapSendNotifications (NULL);
            return;
        }
        return;
    }
#ifdef L2RED_WANTED
    if (u1Event == RM_PEER_UP)
    {
        if (RM_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
        {
            return;
        }
    }
#endif
    /* Notify Node Status to the protcols registered with RM */
    for (u4EntId = 1; u4EntId < RM_MAX_APPS; u4EntId++)
    {
        /* During Force-standby , prevent notification to MSR module,
         * so that the node will not wait for update from ACTIVE node */
        if ((u1Event == GO_STANDBY) && (RM_MSR_APP_ID == u4EntId))
        {
            if ((IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE)
                && (RM_FORCE_SWITCHOVER_FLAG () != RM_FSW_OCCURED))

            {
                continue;
            }
        }
        RmUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
    }
#ifndef MBSM_WANTED
    if (u1Event == GO_ACTIVE)
    {
        /* No need to get hardware attach explicity. Trasnmit the pending
         * traps. In FSW*/
        RmSetTxEnable (RM_TRUE);
        RmTrapSendNotifications (NULL);
    }
#endif
}

/******************************************************************************
 * Function           : RmAddSyncMsgIntoTxList
 * Input(s)           : u1SsnIndex -> Session index
 *                      pu1Data -> Sync message
 *                      u4RmPktLen -> Sync message length
 *                      u4RmMsgOffset -> Message offset 
 *                      to be sent to the peer
 * Output(s)          : None
 * Returns            : None
 * Action             : Adds the sync message to the TX LIST
 ******************************************************************************/
VOID
RmAddSyncMsgIntoTxList (UINT1 u1SsnIndex, UINT1 *pu1Data,
                        UINT4 u4RmPktLen, UINT4 u4RmMsgOffset)
{
    tRmTxBufNode       *pRmTxBufNode = NULL;
    UINT4               u4TxListCnt = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmAddSyncMsgIntoTxList: ENTRY \r\n");
    /* Allocate BufNode and put the message in the peer tx queue */
    pRmTxBufNode = (tRmTxBufNode *) MemAllocMemBlk (gRmInfo.RmTxBufMemPoolId);
    if (pRmTxBufNode == NULL)
    {
        /* Send buffer overflow */
        /* Memory allocation failure */
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC, "TX Buffer is overflow, "
                "this may be due to TCP session problem"
                "Please restart the node.\r\n");
        RmHandleSystemRecovery (RM_TX_BUFF_ALLOC_FAILED);
        return;
    }
    RM_SLL_NODE_INIT (&pRmTxBufNode->TxNode);
    pRmTxBufNode->pu1Msg = pu1Data;
    pRmTxBufNode->u4MsgSize = u4RmPktLen;
    pRmTxBufNode->u4MsgOffset = u4RmMsgOffset;
    RM_SLL_ADD (&RM_SSN_ENTRY_TX_LIST
                (RM_GET_SSN_INFO (u1SsnIndex)), &pRmTxBufNode->TxNode);
    u4TxListCnt = RM_SLL_COUNT (&RM_SSN_ENTRY_TX_LIST
                                (RM_GET_SSN_INFO (u1SsnIndex)));
    if (u4TxListCnt >= RM_TX_BUF_NODES_THRESHOLD)
    {
        /* Tx buffer is full */
        gRmInfo.bIsTxBuffFull = RM_TRUE;
        RM_TRC1 (RM_CRITICAL_TRC, "RmAddSyncMsgIntoTxList: "
                 "Tx buffer threshold %d is reached \r\n", u4TxListCnt);
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmAddSyncMsgIntoTxList: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmUtilCloseSockConn
 * Input(s)           : u1SsnIndex - Session index
 * Output(s)          : None.
 * Returns            : None
 ******************************************************************************/
VOID
RmUtilCloseSockConn (UINT1 u1SsnIndex)
{
    if (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd == RM_INV_SOCK_FD)
    {
        RM_TRC (RM_CTRL_PATH_TRC,
                "RmUtilCloseSockConn: Invalid socket close \r\n");
        return;
    }
    if (RM_SLL_COUNT
        (&RM_SSN_ENTRY_TX_LIST (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        SelRemoveWrFd (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }
    SelRemoveFd (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    RmCloseSocket (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = RM_INV_SOCK_FD;
    RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_NOT_CONNECTED;
    RM_TRC (RM_CTRL_PATH_TRC, "RmUtilCloseSockConn: socket close done \r\n");
}

/******************************************************************************
 * Function           : RmUtilReleaseMemForPktQMsg
 * Input(s)           : pRmPktQMsg - Block to be release 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to release the memory allocated for the Pkt Q
 *                      Message.
 ******************************************************************************/
INT1
RmUtilReleaseMemForPktQMsg (tRmPktQMsg * pRmPktQMsg)
{
    if (pRmPktQMsg->u1MsgType == RM_DYNAMIC_SYNC_MSG)
    {
        if (pRmPktQMsg->DynSyncMsg != NULL)
        {
            RM_FREE (pRmPktQMsg->DynSyncMsg);
            pRmPktQMsg->DynSyncMsg = NULL;
        }
    }
    else if (pRmPktQMsg->u1MsgType == RM_STATIC_CONF_INFO)
    {
        free_MultiData (pRmPktQMsg->ConfInfo.pMultiData);
        pRmPktQMsg->ConfInfo.pMultiData = NULL;

        if (pRmPktQMsg->ConfInfo.pMultiIndex != NULL)
        {
            free_MultiIndex (pRmPktQMsg->ConfInfo.pMultiIndex,
                             pRmPktQMsg->ConfInfo.pMultiIndex->u4No);
        }
        pRmPktQMsg->ConfInfo.pMultiIndex = NULL;

        if (pRmPktQMsg->ConfInfo.pu1ObjectId != NULL)
        {
            free_MultiOid (pRmPktQMsg->ConfInfo.pu1ObjectId);
            pRmPktQMsg->ConfInfo.pu1ObjectId = NULL;
        }
    }

    if (MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, (UINT1 *) pRmPktQMsg)
        != MEM_SUCCESS)
    {
        RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmUtilReleaseMemForPktQMsg"
                 "Mem block release failed for blck %u\n", pRmPktQMsg);
        return RM_FAILURE;
    }
    pRmPktQMsg = NULL;
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmUtilTriggerNextHigherLayer
 * Input(s)           : u4AppId - Module Id.
 *                      u4Event   - Event to be send to the protocols
 *                      (L2_INITIATE_BULK_UPDATES)
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM to send events to higher level 
 *                      protocol modules. 
 ******************************************************************************/
UINT1
RmUtilTriggerNextHigherLayer (UINT4 u4AppId, UINT1 u1Event)
{
    UINT1               u1RetVal = RM_SUCCESS;
    UINT4               u4EntId = RM_APP_ID;
    switch (u1Event)
    {
        case L2_INITIATE_BULK_UPDATES:

            while ((u1RetVal =
                    RmUtilGetNextAppId (u4AppId, &u4EntId)) == RM_SUCCESS)
            {
                if ((u4EntId == RM_MSR_APP_ID) &&
                    (RM_GET_PREV_NODE_STATE () == RM_INIT))
                {
                    /* If the next higher layer protocol is MSR. Dont send 
                     * L2_INITIATE_BULK_UPDATES. Wait for config restoration. 
                     * On config restoration completion RM will send
                     * RM_PROTOCOL_BULK_UPDT_COMPLETION event */
                    break;
                }
                else if (((u4EntId == RM_MSR_APP_ID)
                          || (u4EntId == RM_NP_APP_ID))
                         && (RM_GET_PREV_NODE_STATE () == RM_ACTIVE))
                {
                    /* If the next higher layer protocol is MSR/NP. Dont send 
                     * L2_INITIATE_BULK_UPDATES. In ACTIVE to STANDBY node transition
                     * dont wait for MSR config restoration / 
                     * NP bulk update completion. Other protocols can start
                     * bulk update. */
                    u4AppId = u4EntId;
                    continue;
                }
                else if ((u4EntId == RM_MPLS_APP_ID)
                         || (u4EntId == RM_ISS_APP_ID)
                         || (u4EntId == RM_SNMP_APP_ID)
                         || (u4EntId == RM_CLI_APP_ID))
                {
                    /* If the next higher layer protocol is MPLS / SNMP / CLI. 
                     * dont send L2_INITIATE_BULK_UPDATES. Other protocols can start
                     * bulk update. */
                    u4AppId = u4EntId;
                    continue;
                }

                /* Trigger to start bulk update by  the
                 * next higher layer module registered. */
                RM_TRC1 (RM_NOTIF_TRC,
                         "Event: Initiating bulk update for %s module\n",
                         gapc1AppName[u4EntId]);
                u4EntId = MEM_MAX_BYTES (u4EntId, (RM_MAX_APPS - 1));
                RmUtilPostDataOrEvntToProtocol (u4EntId, u1Event, NULL, 0, 0);
                break;
            }

            break;
        case RM_INIT_TO_ACTIVE_COMPLETE:
            while ((u1RetVal =
                    RmUtilGetNextAppId (u4AppId, &u4EntId)) == RM_SUCCESS)
            {
                if (u4EntId == RM_MSR_APP_ID)
                {
                    u4AppId = u4EntId;
                    continue;
                }
                /* Trigger GO_ACTIVE for next higher layer 
                 * module registered. */

                RM_TRC1 (RM_SYNC_EVENT_TRC | RM_NOTIF_TRC,
                         "Event: INIT->ACTIVE completed by AppName=%s\n",
                         gapc1AppName[u4AppId]);
                RmUtilPostDataOrEvntToProtocol (u4EntId, (UINT1) GO_ACTIVE,
                                                NULL, 0, 0);
                break;
            }
            if (u1RetVal == RM_FAILURE)
            {
                /* Give GO_ACTIVE to MSR after 
                 * all modules GO_ACTIVE given */
                u4EntId = RM_MSR_APP_ID;

                RM_TRC (RM_NOTIF_TRC,
                        "Event: Initiating INIT->ACTIVE for AppName=MSR\n");
                RmUtilPostDataOrEvntToProtocol (u4EntId,
                                                (UINT1) GO_ACTIVE, NULL, 0, 0);
                if (RM_PEER_NODE_COUNT () > 0)
                {
                    RmUpdatePeerNodeCntToProtocols (RM_PEER_UP);
                }
                RmSetTxEnable (RM_TRUE);
                RmTrapSendNotifications (NULL);
            }
            break;
        default:
            break;
    }
    return u1RetVal;
}

/******************************************************************************
 * Function           : RmUtilGetNextAppId
 * Input(s)           : u4AppId - Module Id.
 *                      pu4NextId - Pointer to next valid 
 *                      registered module Id.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM to return next valid registered
 *                      module Id. 
 ******************************************************************************/
UINT1
RmUtilGetNextAppId (UINT4 u4AppId, UINT4 *pu4NextId)
{
    UINT1               u1RetVal = RM_FAILURE;
    UINT4               u4ModId = 0;

    /* Get the next valid module ID. */
    for (u4ModId = ++u4AppId; (u4ModId < RM_MAX_APPS && u1RetVal == RM_FAILURE);
         u4ModId++)
    {
        if ((gRmInfo.RmAppInfo[u4ModId] != NULL) &&
            (gRmInfo.RmAppInfo[u4ModId]->pFnRcvPkt != NULL))
        {
            /* Valid entry found */
            *pu4NextId = u4ModId;
            u1RetVal = RM_SUCCESS;
        }

    }

    return u1RetVal;
}

/******************************************************************************
 * Function           : RmUtilGetPrevAppId
 * Input(s)           : u4AppId - Module Id.
 *                      pu4PrevId - Pointer to previous valid 
 *                      registered module Id.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM to return previous valid registered
 *                      module Id. 
 ******************************************************************************/
UINT1
RmUtilGetPrevAppId (UINT4 u4AppId, UINT4 *pu4PrevId)
{
    UINT1               u1RetVal = RM_FAILURE;
    UINT4               u4ModId = 0;

    /* AppId 0 and max app id are not valid here */
    if ((u4AppId == 0) || (u4AppId >= RM_MAX_APPS))
    {
        return u1RetVal;
    }
    /* Get the previous valid module ID. */
    for (u4ModId = --u4AppId; (u4ModId > 0 && u1RetVal == RM_FAILURE);
         u4ModId--)
    {
        if ((gRmInfo.RmAppInfo[u4ModId] != NULL) &&
            (gRmInfo.RmAppInfo[u4ModId]->pFnRcvPkt != NULL))
        {
            /* Valid entry found */
            *pu4PrevId = u4ModId;
            u1RetVal = RM_SUCCESS;
        }

    }

    return u1RetVal;
}

/******************************************************************************
 * Function           : RmUtilGetPrevBulkSupportedAppId
 * Input(s)           : u4AppId - Current Module Id.
 *                      pu4PrevId - Pointer to previous valid bulk supported
 *                      registered module Id.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM to return previous valid bulk 
 *                      suppored registered module Id. 
 ******************************************************************************/
UINT1
RmUtilGetPrevBulkSupportedAppId (UINT4 u4AppId, UINT4 *pu4PrevId)
{
    UINT1               u1RetVal = RM_FAILURE;
    UINT4               u4ModId = 0;
    UINT4               u4ChkAppId = 0;
    BOOL1               bResult = OSIX_FALSE;

    u4ModId = u4AppId;

    do
    {
        u4ChkAppId = 0;
        if (RmUtilGetPrevAppId (u4ModId, &u4ChkAppId) == RM_SUCCESS)
        {
            OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdStsMask, u4ChkAppId,
                                     sizeof (tRmAppList), bResult);

            if (bResult == OSIX_FALSE)
            {
                /* Bulk updates mechanism is not supported for this
                 * protocol. So need to check for the next previous module*/
                u4ModId = u4ChkAppId;
                u1RetVal = RM_FAILURE;
            }
            else
            {
                *pu4PrevId = u4ChkAppId;
                u1RetVal = RM_SUCCESS;
                break;
            }
        }
        else
        {
            u1RetVal = RM_FAILURE;
            break;
        }
    }
    while (u4ModId > 0);

    return u1RetVal;
}

/******************************************************************************
 * Function           : RmUtilCheckForSwCompletion
 * Input(s)           : u4AppId - Module Id.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used to check switchover is completed by the 
 *                      protocol modules. 
 ******************************************************************************/
UINT1
RmUtilCheckForSwCompletion (UINT4 u4AppId)
{
    UINT4               u4Index = 0;
    /* IDLE node to ACTIVE / STANDBY node transition is not switchover. */
    if (((RM_GET_NODE_STATE () == RM_STANDBY) ||
         (RM_GET_NODE_STATE () == RM_ACTIVE)) &&
        (RM_GET_PREV_NODE_STATE () == RM_INIT))
    {
        RM_TRC (RM_NOTIF_TRC, "Not a switchover process \n");
        return RM_FAILURE;
    }

    /* ACTIVE / STANDBY node transition to INIT node is also not switchover. */
    if ((RM_GET_NODE_STATE () == RM_INIT) &&
        ((RM_GET_PREV_NODE_STATE () == RM_STANDBY) ||
         (RM_GET_PREV_NODE_STATE () == RM_ACTIVE)))
    {
        RM_TRC (RM_NOTIF_TRC, "Not a switchover process \n");
        return RM_FAILURE;
    }

    /* If u4AppId is RM_APP_ID just check for the completion of
     * switchover. Else set the SwitchoverSts bitmap and check for 
     * the completion */
    if (u4AppId != RM_APP_ID)
    {
        RM_TRC1 (RM_SYNC_EVENT_TRC | RM_NOTIF_TRC,
                 "Event: ACTIVE->STANDBY switchover completed by %s module\n",
                 gapc1AppName[u4AppId]);
        OSIX_BITLIST_SET_BIT (gRmInfo.RmSwitchoverSts, u4AppId,
                              sizeof (tRmAppList));
    }
    if (MEMCMP (gRmInfo.RmSwitchoverSts, gRmInfo.RmRegMask,
                sizeof (tRmAppList)) != 0)
    {
        /* Bit is not set for the protocol. Switchover is
         * not yet over. */
        RM_TRC (RM_NOTIF_TRC, "Switchover not completed - Current list");
        for (u4Index = RM_APP_ID; u4Index < sizeof (tRmAppList); u4Index++)
        {
            RM_TRC1 (RM_NOTIF_TRC, " %x", gRmInfo.RmSwitchoverSts[u4Index]);
        }
        RM_TRC (RM_NOTIF_TRC, "\n");
        return RM_FAILURE;
    }

    RM_TRC (RM_SYNC_EVENT_TRC | RM_NOTIF_TRC,
            "Event: ACTIVE->STANDBY switchover is completed "
            "by all the modules \n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmUtilStoreInArray
 * Input(s)           : pNotifMsg - pointer to the notification information 
 *                      to be stored.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used to add in notification array pointer. 
 ******************************************************************************/
UINT1
RmUtilStoreInArray (tRmNotificationMsg * pNotifMsg)
{
    UINT1               u1RetVal = RM_FAILURE;

    if (gRmInfo.u1NxtFreeNotifArrayId < RM_MAX_NOTIFICATION_INFO_LIMIT)
    {
        gRmInfo.apNotifInfo[gRmInfo.u1NxtFreeNotifArrayId] =
            (tRmNotificationMsg *) MemAllocMemBlk (gRmInfo.RmNotifMsgPoolId);
        RM_TRC2 (RM_NOTIF_TRC,
                 "Allocated memblock %x and ArrayId: %d\n",
                 gRmInfo.apNotifInfo[gRmInfo.u1NxtFreeNotifArrayId],
                 gRmInfo.u1NxtFreeNotifArrayId);
        if (gRmInfo.apNotifInfo[gRmInfo.u1NxtFreeNotifArrayId] != NULL)
        {
            /* Check */
            MEMCPY (gRmInfo.apNotifInfo[gRmInfo.u1NxtFreeNotifArrayId],
                    pNotifMsg, sizeof (tRmNotificationMsg));
            u1RetVal = RM_SUCCESS;
            gRmInfo.u1NxtFreeNotifArrayId++;
            RM_TRC1 (RM_NOTIF_TRC, "Free Array Index: %d\n",
                     gRmInfo.u1NxtFreeNotifArrayId);
        }
        else
        {
            RM_TRC (RM_CRITICAL_TRC | RM_NOTIF_TRC | RM_FAILURE_TRC,
                    "Unable to allocate memory for the message to store\n");
        }
    }
    return u1RetVal;
}

/******************************************************************************
 * Function           : RmTcpUpdateSsnTable 
 * Input(s)           : i4CliSockFd - Newly accpeted connection identifier
 *                      u4IpAddr - Peer IP address
 * Output(s)          : None
 * Returns            : It updates the session table based on the peer 
 *                      information received in the scoket accept call.
 ******************************************************************************/
VOID
RmTcpUpdateSsnTable (INT4 i4CliSockFd, UINT4 u4IpAddr)
{
    UINT1               u1SsnIndex = 0;
    UINT1              *pResBuf = NULL;

    RM_TRC (RM_SOCK_TRC, "RmTcpUpdateSsnTable: ENTRY \r\n");
    RM_TRC1 (RM_SOCK_TRC, "RmTcpUpdateSsnTable: Peer accepted=%x\r\n",
             u4IpAddr);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        for (u1SsnIndex = 0; u1SsnIndex < RM_MAX_SYNC_SSNS_LIMIT; u1SsnIndex++)
        {
            if (RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr != 0)
            {
                continue;
            }
            else
            {
                RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4CliSockFd;
                RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr = u4IpAddr;
                RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_CONNECTED;
                break;
            }
        }
        if (u1SsnIndex > RM_MAX_SYNC_SSNS_LIMIT)
        {
            RM_TRC (RM_SOCK_TRC, " Max sessions reached \n");
        }
        /* In ColdStandby case,  Active only initiate 'connect' 
         *  Standby can get Active IP address, while accepting this connection */
        if (RM_GET_NODE_STATE () != RM_ACTIVE)
        {
            gRmInfo.u4ActiveNode = u4IpAddr;
        }
        RM_TRC (RM_SOCK_TRC, "RmTcpUpdateSsnTable: EXIT \r\n");
        return;
    }
    else
    {
        for (u1SsnIndex = 0; u1SsnIndex < RM_MAX_SYNC_SSNS_LIMIT; u1SsnIndex++)
        {
            if (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd != RM_INV_SOCK_FD)
            {
                continue;
            }
            else
            {
                RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr = u4IpAddr;
                RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4CliSockFd;
                /* Allocate the memory for Rx residual buffer 
                 * only if not allocated i.e NULL */
                if (RM_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
                {
                    RmMemAllocForResBuf (&pResBuf);
                    RM_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
                }

                RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_CONNECTED;
                /* For 1:N redundancy, peer informations has to be moved to session table */
                if (RM_GET_PEER_NODE_ID () != u4IpAddr)
                {
                    /* Peer is not learnt thro. peer up, so, add it here */
                    RM_SET_PEER_NODE_ID (u4IpAddr);
                    RM_PEER_NODE_COUNT ()++;
                }
                /* Send the pending TX list thro. this new connection */

                RmSockWritableHandle (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
                break;
            }
        }
    }
    RM_TRC (RM_SOCK_TRC, "RmTcpUpdateSsnTable: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmTcpSrvSockCallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on 
 *                      TCP server.
 ******************************************************************************/
VOID
RmTcpSrvSockCallBk (INT4 i4SockFd)
{
    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockCallBk: ENTRY \r\n");
    UNUSED_PARAM (i4SockFd);

    if (RM_SEND_EVENT (RM_TASK_ID, RM_TCP_NEW_CONN_RCVD) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSrvSockCallBk: Send Event "
                "RM_TCP_NEW_CONN_RCVD Failed\n");
        return;
    }
    RM_TRC (RM_SOCK_TRC, "RmTcpSrvSockCallBk: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmTcpWriteCallBackFn
 * Input(s)           : i4SockFd - Writable socket fd 
 * Output(s)          : None 
 * Returns            : None 
 * Action             : Callback function to indicate socket is writable 
 ******************************************************************************/
VOID
RmTcpWriteCallBackFn (INT4 i4SockFd)
{
    tRmCtrlQMsg        *pRmCtrlQMsg = NULL;

    RM_TRC (RM_SOCK_TRC, "RmTcpWriteCallBackFn: ENTRY \r\n");

    if ((pRmCtrlQMsg = (tRmCtrlQMsg *) (MemAllocMemBlk
                                        (gRmInfo.RmCtrlMsgPoolId))) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "RmTcpWriteCallBackFn: Memory allocation "
                "failed for control message\r\n");
        return;
    }

    MEMSET (pRmCtrlQMsg, 0, sizeof (tRmCtrlQMsg));

    pRmCtrlQMsg->u4MsgType = RM_WR_SOCK_MSG;
    pRmCtrlQMsg->SktCtrlMsg.i4SockId = i4SockFd;
    if (RmQueEnqCtrlMsg (pRmCtrlQMsg) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpWriteCallBackFn: Send to control Q "
                "failed\r\n");
        return;
    }
    RM_TRC (RM_SOCK_TRC, "RmTcpWriteCallBackFn: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a RM packet arrives.
 ******************************************************************************/
VOID
RmPktRcvd (INT4 i4SockFd)
{
    tRmCtrlQMsg        *pRmCtrlQMsg = NULL;

    RM_TRC (RM_SOCK_TRC, "RmTcpWriteCallBackFn: ENTRY \r\n");

    HbApiSetKeepAliveFlag (OSIX_TRUE);

    if ((pRmCtrlQMsg = (tRmCtrlQMsg *) (MemAllocMemBlk
                                        (gRmInfo.RmCtrlMsgPoolId))) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "RmTcpWriteCallBackFn: Memory allocation"
                "failed for control message\r\n");
        return;
    }

    MEMSET (pRmCtrlQMsg, 0, sizeof (tRmCtrlQMsg));

    pRmCtrlQMsg->u4MsgType = RM_RD_SOCK_MSG;
    pRmCtrlQMsg->SktCtrlMsg.i4SockId = i4SockFd;
    if (RmQueEnqCtrlMsg (pRmCtrlQMsg) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpWriteCallBackFn: Send to control Q "
                "failed\r\n");
        return;
    }
    RM_TRC (RM_SOCK_TRC, "RmTcpWriteCallBackFn: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmUtilGetPendingNotificationEntry
 * Input(s)           : None.
 * Output(s)          : pNotifMsg - pointer to the notification information.
 *                      pu4IsEntryExist - pointer to flag representing entry
 *                      exist or not.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used to check if the notification array 
 *                      pointer is empty, if not get the pending notification 
 *                      entry. 
 ******************************************************************************/
UINT1
RmUtilGetPendingNotificationEntry (tRmNotificationMsg ** ppNotifMsg,
                                   UINT4 *pu4IsEntryExist)
{
    UINT4               u4ArrayId = RM_MAX_NOTIFICATION_INFO_LIMIT;

    for (u4ArrayId = 0; u4ArrayId < RM_MAX_NOTIFICATION_INFO_LIMIT; u4ArrayId++)
    {
        if (gRmInfo.apNotifInfo[u4ArrayId] != NULL)
        {
            *ppNotifMsg = gRmInfo.apNotifInfo[u4ArrayId];
            *pu4IsEntryExist = RM_TRUE;
            RM_TRC1 (RM_NOTIF_TRC,
                     "Notification array not empty !! ArrayId - %d\n",
                     u4ArrayId);
            return RM_FAILURE;
        }
    }

    pu4IsEntryExist = RM_FALSE;
    gRmInfo.u1NxtFreeNotifArrayId = 0;
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmUtilReleaseMem
 * Input(s)           : None.
 * Output(s)          : u4MemPoolId - mempool ID of the block to be freed.
 *                    : pu1Blk - pointer to the block to be freed,
 *                      if NULL, free the entire memory blocks allocated 
 *                      from the pool.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used to check if the notification array 
 *                      pointer is empty, if not outputs the notification 
 *                      message. 
 ******************************************************************************/
UINT1
RmUtilReleaseMem (tMemPoolId u4MemPoolId, UINT1 *pu1Blk)
{
    UINT4               u4ArrayId;
    /* pu1Blk is NULL for free the entire memblocks allocated from the pool. */
    if ((u4MemPoolId == gRmInfo.RmNotifMsgPoolId) && (pu1Blk == NULL))
    {
        for (u4ArrayId = 0; u4ArrayId < RM_MAX_NOTIFICATION_INFO_LIMIT;
             u4ArrayId++)
        {
            if (gRmInfo.apNotifInfo[u4ArrayId] != NULL)
            {
                pu1Blk = (UINT1 *) gRmInfo.apNotifInfo[u4ArrayId];
                if (MemReleaseMemBlock (gRmInfo.RmNotifMsgPoolId, pu1Blk) !=
                    MEM_SUCCESS)
                {
                    RM_TRC1 (RM_NOTIF_TRC | RM_FAILURE_TRC,
                             "MemReleaseMemBlk failed !! ArrayId - %d\n",
                             u4ArrayId);
                }
                gRmInfo.apNotifInfo[u4ArrayId] = NULL;
                pu1Blk = NULL;
            }

        }

        MEMSET (gRmInfo.apNotifInfo, 0, RM_MAX_NOTIFICATION_INFO_LIMIT);
        gRmInfo.u1NxtFreeNotifArrayId = 0;
    }

    if ((u4MemPoolId == gRmInfo.RmNotifMsgPoolId) && (pu1Blk != NULL))
    {
        for (u4ArrayId = 0; u4ArrayId < RM_MAX_NOTIFICATION_INFO_LIMIT;
             u4ArrayId++)
        {
            if (gRmInfo.apNotifInfo[u4ArrayId] ==
                (tRmNotificationMsg *) (VOID *) pu1Blk)
            {
                RM_TRC1 (RM_NOTIF_TRC, "Assigned NULL for the array index %d",
                         u4ArrayId);
                if (MemReleaseMemBlock (gRmInfo.RmNotifMsgPoolId, pu1Blk) !=
                    MEM_SUCCESS)
                {
                    RM_TRC1 (RM_NOTIF_TRC | RM_FAILURE_TRC,
                             "MemReleaseMemBlk failed !! pu1Blk - %u\n",
                             pu1Blk);
                }
                RM_TRC1 (RM_NOTIF_TRC, "Memblock freed: %x\n", pu1Blk);
                gRmInfo.apNotifInfo[u4ArrayId] = NULL;
                gRmInfo.u1NxtFreeNotifArrayId--;
            }
        }
        pu1Blk = NULL;
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmSetTxEnable
 * Input(s)           : u1Status - RM_TRUE/RM_FALSE.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to set transmission is enabled else free 
 *                      the notification array
 ******************************************************************************/
VOID
RmSetTxEnable (UINT1 u1Status)
{
    gRmInfo.u4RmTxEnabled = u1Status;
    if (u1Status == RM_FALSE)
    {
        RmUtilReleaseMem (gRmInfo.RmNotifMsgPoolId, NULL);
    }
}

/******************************************************************************
 * Function           : RmPrependRmHdr
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to form RM header and prepend it to the 
 *                      RM msg. Checksum is not updated in RM header here.
 *                      Once the message is posted to RM Q,
 *                      RmGetLinearBufAndLenFromPktQMsg routine does the 
 *                      CRUBUF to linear buf copy and calculates the checksum.
 ******************************************************************************/
UINT4
RmPrependRmHdr (tRmMsg * pRmMsg, UINT4 u4Size)
{
    tRmHdr              RmHdr;
    UINT4               u4RetVal = RM_SUCCESS;

    /* From RM hdr */
    MEMSET (&RmHdr, 0, sizeof (tRmHdr));

    RmHdr.u2Version = OSIX_HTONS (RM_VERSION_NUM);
    RmHdr.u2Chksum = OSIX_HTONS (0);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u4Size));
    RmHdr.u4MsgType = OSIX_HTONS (0);    /* REQ/RESP - to be filled while implementing re-transmission */
    RmHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    RmHdr.u4SeqNo = OSIX_HTONS (0);

    /* Prepend RM hdr to HB data */
    if ((RM_PREPEND_BUF (pRmMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr))) !=
        CRU_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "Prepend RM hdr - failed\n");
        u4RetVal = RM_FAILURE;
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmUtilResetStatistics
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine used to reset the statistics counters. 
 ******************************************************************************/
VOID
RmUtilResetStatistics (VOID)
{
    RM_TX_STAT_SYNC_MSG_SENT () = 0;
    RM_TX_STAT_SYNC_MSG_FAILED () = 0;
    RM_RX_STAT_SYNC_MSG_RECVD () = 0;
    RM_RX_STAT_SYNC_MSG_PROCESSED () = 0;
    RM_RX_STAT_SYNC_MSG_MISSED () = 0;
    RM_RX_STAT_STATIC_CONF_SYNC_FAILED () = 0;
}

/******************************************************************************
 * Function           : RmUtilFillRMHeader
 * Input(s)           : pMsg        - Pointer to the message in which RM 
 *                                    header needs to be filled
 *                      u2DataLen   - Total length of the message
 *                      u4SrcEntId  - EntId from which the msg has arrived
 *                      u4DestEntId - EntId to which the msg has to be sent
 *                      u4SeqNo     - Sequence number to be filled in the msg
 * Output(s)          : pMsg        - Message pointer containing the RM header
 *                                    and the data
 * Returns            : None. 
 * Action             : Invokes this function to fill the RM Header.
 ******************************************************************************/
VOID
RmUtilFillRMHeader (UINT1 *pMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                    UINT4 u4DestEntId, UINT4 u4SeqNo)
{
    tRmHdr             *pRmHdr;
    UINT2               u2Cksum = 0;

    pRmHdr = (tRmHdr *) (VOID *) pMsg;

    pRmHdr->u2Version = OSIX_HTONS (RM_VERSION_NUM);
    pRmHdr->u2Chksum = OSIX_HTONS (u2Cksum);
    pRmHdr->u4TotLen = OSIX_HTONL ((UINT4) u2DataLen);
    pRmHdr->u4MsgType = OSIX_HTONL (RM_UNUSED);
    pRmHdr->u4SrcEntId = OSIX_HTONL (u4SrcEntId);
    pRmHdr->u4DestEntId = OSIX_HTONL (u4DestEntId);
    pRmHdr->u4SeqNo = OSIX_HTONL (u4SeqNo);

    u2Cksum = RM_LINEAR_CALC_CKSUM ((const INT1 *) pMsg, u2DataLen);
    pRmHdr->u2Chksum = OSIX_HTONS (u2Cksum);
    RM_TRC2 (RM_SYNCUP_TRC, "RmUtilFillRMHeader: Sync pkt to be sent"
             "[AppId= %s, Seq#= %d]\n", gapc1AppName[u4SrcEntId], u4SeqNo);
}

/******************************************************************************
 * Function           : RmUtilFormConfigSyncUpMsg
 * Input(s)           : u1MessageType - Message type (RM_CONFIG_VALID_MSG/
 *                                                     RM_CONFIG_DUMMY_MSG) 
 *                      u2MsgLen      - Length of the configuration syncup msg
 *                      pu1Buf        - Pointer to the message
 *                      pOid          - Full Oid (Oid + Instance) for
 *                                      RM_CONFIG_VALID_MSG else NULL
 *                      pMultiData    - Multi Data for RM_CONFIG_VALID_MSG
 *                                      else NULL
 * Output(s)          : None 
 * Returns            : None 
 * Action             : Invokes this function to form the full OID from index 
 *                      array and actual OID. 
 ******************************************************************************/
VOID
RmUtilFormConfigSyncUpMsg (UINT1 u1MessageType, UINT2 u2MsgLen, UINT1 *pu1Buf,
                           tSNMP_OID_TYPE * pOid,
                           tSNMP_MULTI_DATA_TYPE * pMultiData)
{
    UINT4               u4Len = 0;
    UINT2               u2DataType = 0;

    RM_PUT_1_BYTE (pu1Buf, u1MessageType);

    if (u1MessageType == RM_CONFIG_VALID_MSG)
    {
        RM_PUT_2_BYTES (pu1Buf, u2MsgLen);
        u4Len = pOid->u4_Length;
        RM_PUT_4_BYTES (pu1Buf, u4Len);
        RM_PUT_N_BYTES (pu1Buf, (UINT1 *) pOid->pu4_OidList,
                        (pOid->u4_Length * sizeof (UINT4)));
        u2DataType = (UINT2) pMultiData->i2_DataType;
        RM_PUT_2_BYTES (pu1Buf, u2DataType);

        switch (pMultiData->i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
            case SNMP_DATA_TYPE_TIME_TICKS:

                RM_PUT_4_BYTES (pu1Buf, pMultiData->u4_ULongValue);
                break;

            case SNMP_DATA_TYPE_INTEGER32:

                RM_PUT_4_BYTES (pu1Buf, pMultiData->i4_SLongValue);
                break;

            case SNMP_DATA_TYPE_OBJECT_ID:
                u4Len = pMultiData->pOidValue->u4_Length;
                RM_PUT_4_BYTES (pu1Buf, u4Len);
                RM_PUT_N_BYTES (pu1Buf,
                                (UINT1 *) pMultiData->pOidValue->pu4_OidList,
                                (pMultiData->pOidValue->u4_Length *
                                 sizeof (UINT4)));
                break;

            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                u4Len = (UINT4) pMultiData->pOctetStrValue->i4_Length;
                RM_PUT_4_BYTES (pu1Buf, u4Len);
                RM_PUT_N_BYTES (pu1Buf,
                                pMultiData->pOctetStrValue->pu1_OctetList,
                                pMultiData->pOctetStrValue->i4_Length);
                break;

            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                RM_PUT_4_BYTES (pu1Buf, pMultiData->u4_ULongValue);
                break;

            case SNMP_DATA_TYPE_COUNTER64:

                RM_PUT_4_BYTES (pu1Buf, pMultiData->u8_Counter64Value.msn);
                RM_PUT_4_BYTES (pu1Buf, pMultiData->u8_Counter64Value.lsn);
                break;

            case SNMP_DATA_TYPE_NULL:
                break;

            default:
                break;
        }
    }
}

/******************************************************************************
 * Function           : RmUtilFormFullOID
 * Input(s)           : pu1Oid         - OID of the mib object 
 *                                       (does not contain the instance
 *                                       information)
 *                      pMultiIndex    - This variable contains the number of
 *                                       indices for the given OID and index
 *                                       type and value in case of tabular OID
 *                                       and in case of scalar, number of index
 *                                       is 0
 * Output(s)          : pOid           - Full Oid (Oid + Instance)
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Invokes this function to form the full OID from index 
 *                      array and actual OID. 
 ******************************************************************************/
INT1
RmUtilFormFullOID (UINT1 *pu1Oid, tSnmpIndex * pMultiIndex,
                   tSNMP_OID_TYPE * pOid)
{
    tMbDbEntry         *pNext = NULL;
    tMbDbEntry         *pCur = NULL;
    UINT4               u4Len = 0;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1Tmp[] = ".0";

    if (RM_ISSCALAR (pMultiIndex))
    {
        MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);
        STRNCPY (au1Oid, pu1Oid, STRLEN (pu1Oid));
        au1Oid[STRLEN (pu1Oid)] = '\0';
        if ((STRLEN (au1Tmp) + STRLEN (au1Oid)) < SNMP_MAX_OID_LENGTH)
        {
            SNPRINTF ((char *) au1Oid, (sizeof (au1Oid)), "%2s%s", au1Oid,
                      au1Tmp);
        }
        else
        {
            RM_TRC (RM_SYNCUP_TRC | RM_FAILURE_TRC,
                    "RmUtilFormFullOID: String length exceeds the max length "
                    "allowed \n");
            return RM_FAILURE;
        }
        WebnmConvertStringToOid (pOid, au1Oid, MSR_FALSE);
    }
    else
    {
        /* Tabular object */
        WebnmConvertStringToOid (pOid, pu1Oid, MSR_FALSE);
#ifdef SNMP_3_WANTED
        SNMPGetMbDbEntry (*pOid, &pNext, &u4Len, &pCur);
        RM_CLEAR_OID (pOid);
        if (pCur == NULL)
        {
            return RM_FAILURE;
        }
        else
        {
            SNMPFormOid (pOid, pCur, pMultiIndex);
        }
#else
        UNUSED_PARAM (pNext);
        UNUSED_PARAM (pCur);
        UNUSED_PARAM (u4Len);
#endif
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmUtilDelFailBuff
 * Input(s)           : None 
 * Output(s)          : None 
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Invokes this function to remove the contents of the 
 *                      Failure buffer during failover (Bulk update in progress)
 ******************************************************************************/
INT1
RmUtilDelFailBuff ()
{
    tRmConfFailBuff    *pFailBuffNode = NULL;
    tSNMP_OID_TYPE     *pOID = NULL;
    tSNMP_MULTI_DATA_TYPE *pObjVal;

    while ((pFailBuffNode = (tRmConfFailBuff *)
            TMO_SLL_Get (&(gRmConfInfo.RmConfigFailBuff))) != NULL)
    {
        pOID = pFailBuffNode->pFailedOid;
        pObjVal = &(pFailBuffNode->Data);

        if (pObjVal->pOctetStrValue != NULL)
        {
            free_octetstring (pObjVal->pOctetStrValue);
            pObjVal->pOctetStrValue = NULL;
        }
        else if (pObjVal->pOidValue != NULL)
        {
            free_oid (pObjVal->pOidValue);
            pObjVal->pOidValue = NULL;
        }

        free_oid (pOID);
        pOID = NULL;

        if (MemReleaseMemBlock (RM_CONF_FAIL_BUFF_POOL_ID (),
                                (UINT1 *) pFailBuffNode) == MEM_FAILURE)
        {
            RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                    "RmConfHandleBulkUpdCompEvent: Memory release failed!\n");
            return RM_FAILURE;
        }
        pFailBuffNode = NULL;

    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmInformOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine is used to inform RM module about the 
 *                      completion of other modules intialization.
 ******************************************************************************/
VOID
RmInformOtherModulesBootUp (INT1 *pi1Param)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        /* Inform  RM HB internal module */
        HbInformOtherModulesBootUp (pi1Param);
    }
    else
    {
        RM_SEND_EVENT (RM_TASK_ID, RM_RESUME_EVENT);
    }
    return;
}

/******************************************************************************
 * Function           : RmWaitForOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : This routine is used to hold the RM protocol operations
 *                      till other modules completes its initialization.
 ******************************************************************************/
INT4
RmWaitForOtherModulesBootUp (VOID)
{
    UINT4               u4Event;

    while (1)
    {
        RM_RECV_EVENT (RM_TASK_ID, RM_RESUME_EVENT, OSIX_WAIT, &u4Event);

        if (u4Event & RM_RESUME_EVENT)
        {
            gu1ModuleBootup = 1;
            break;
        }
    }
    /* This routine used to give the GO_ACTIVE to all modules 
     * when HB is not defined */
#ifdef L2RED_WANTED
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_SET_ACTIVE_NODE_ID (RM_GET_SELF_NODE_ID ());
        RM_TRC1 (RM_SEM_TRC, "ACTIVE NODE elected: %0x\n",
                 RM_GET_ACTIVE_NODE_ID ());
        RmHandleNodeStateUpdate (RM_ACTIVE);
    }
#endif
    return RM_SUCCESS;
}

#ifdef MBSM_WANTED
/******************************************************************************
 * Function           : RmHandleLCInsert
 * Input(s)           : SlotId.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to handle the Line Card Insert event from MBSM. 
 *                      All hardware tables are cleared when the node status 
 *                      changes from STANDBY to ACTIVE.
 ******************************************************************************/
UINT4
RmHandleLCInsert (INT4 i4SlotId)
{
    UINT1               u1PrevState = (UINT1) RM_GET_PREV_NODE_STATE ();
    UINT1               u1CurrState = (UINT1) RM_GET_NODE_STATE ();

    /* Do not clear the HW tbl if the LC is already in CLEAR state. */
    if (gau1LcTbl[i4SlotId] == RM_FALSE)
    {
        /* LC is not in CLEAR state. Check if the RM state changes from
         * STANDBY to ACTIVE and clear the HW table.  */
        if ((u1PrevState == RM_STANDBY) && (u1CurrState == RM_ACTIVE))
        {
            if (RmMbsmNpClearHwTbl (i4SlotId) == 0)
            {
                RM_TRC (RM_FAILURE_TRC, "MbsmNpBcmClear -- Failure\n");
                return (RM_FAILURE);
            }

            /* Set the flag to TRUE since the HW tbl is cleared */
            gau1LcTbl[i4SlotId] = RM_TRUE;
        }
    }

    return (RM_SUCCESS);
}
#endif /* MBSM_WANTED */

/******************************************************************************
 * Function           : RmUtilHandleProtocolAck
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function handles the acknowledgement sent by 
 *                      protocols.
 *****************************************************************************/
VOID
RmUtilHandleProtocolAck (tRmProtoAck ProtoAck)
{
    tRmNotificationMsg  NotifMsg;

    if (ProtoAck.u4SeqNumber != RM_RX_LAST_SEQ_NUM_PROCESSED ())
    {
        RM_TRC2 (RM_CRITICAL_TRC, "RmUtilHandleProtocolAck: INVALID protocol "
                 "ACK received.[AppId=%s, Seq# %d]. Ignoring it.\r\n",
                 gapc1AppName[ProtoAck.u4AppId], ProtoAck.u4SeqNumber);

        MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

        NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
        NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
        NotifMsg.u4Status = RM_FAILED;
        NotifMsg.u4Error = RM_PROCESS_FAIL;
        UtlGetTimeStr (NotifMsg.ac1DateTime);
        SPRINTF (NotifMsg.ac1ErrorStr,
                 "INVALID protocol ACK received from AppId=%s, Seq# %d",
                 gapc1AppName[ProtoAck.u4AppId], ProtoAck.u4SeqNumber);
        RmTrapSendNotifications (&NotifMsg);

        return;
    }
    else
    {
        RmTmrStopTimer (RM_ACK_RECOV_TIMER);
        RM_RX_LAST_ACK_RCVD_SEQ_NUM () = ProtoAck.u4SeqNumber;

        if ((RM_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS)
            && (RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_FALSE))
        {
            RM_TRC2 (RM_SYNCUP_TRC,
                     "RmUtilHandleProtocolAck: Received Protocol ACK "
                     "[AppId=%s, Seq# %d]. Static configuration restoration "
                     "is in progress\r\n",
                     gapc1AppName[ProtoAck.u4AppId], ProtoAck.u4SeqNumber);
        }
        else
        {
            /* This scenario occurs in the following case:
             * VCM has sent an ack to RM module after processing dynamic
             * bulk tail message and sent an event to MSR to start the
             * static configuration restaore. In this case, the RX buffer
             * should not be released. The lock will be received after the
             * static configuration restore is completed
             */
            if (RmTrcIsDebugTrcAllowed (ProtoAck.u4AppId) == RM_SUCCESS)
            {
                RM_TRC2 (RM_SYNCUP_TRC | RM_SYNC_MSG_TRC,
                         "SyncMsg: Seq# %d, ACK %s->RM\r\n",
                         ProtoAck.u4SeqNumber, gapc1AppName[ProtoAck.u4AppId]);
            }

            RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
            RmRxProcessBufferedPkt ();
        }
    }
}

/******************************************************************************
 * Function           : RmUtilPostDataOrEvntToProtocol
 * Input(s)           : u4DestEntId - App Id of the protocol the where the
 *                                    packet has to be sent.
 *                      u1Event  - Valid events are GO_ACTIVE/GO_STANDBY/
 *                                 RM_MESSAGE.
 *                      pPkt - Received sync-up message buffer. RM header
 *                             should not be included.(valid only if
 *                             u1Event==RM_MESSAGE)
 *                      u4PktLen - Length of the packet.
 *                      u4SeqNum - Sequence number of the packet. protocol
 *                                 should return this sequence number with
 *                                 the Acknoledgement message after 
 *                                 processing the packet.
 * Output(s)          : None. 
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : Function to send the sync-up data or RM events
 *                      to the destination protocol based on the u4DestEntId.
 *                     
 ******************************************************************************/
INT4
RmUtilPostDataOrEvntToProtocol (UINT4 u4DestEntId, UINT1 u1Event,
                                tRmMsg * pPkt, UINT4 u4PktLen, UINT4 u4SeqNum)
{
    tUtlSysPreciseTime  CurrentTime;
    UINT4               u4CurrentTime = 0;
#ifdef L2RED_WANTED
    INT4                i4RetVal = RM_FAILURE;

    if ((pPkt != NULL) && (u4DestEntId == RM_STATIC_CONF_APP_ID))
    {
        /* Now the pkt has [RM Hdr + RM data]. Strip off RM header */
        RM_PKT_MOVE_TO_DATA (pPkt, RM_HDR_LENGTH);
        u4PktLen = (u4PktLen - RM_HDR_LENGTH);

        i4RetVal = RmConfEnqConfigMsg (pPkt, u4SeqNum, (UINT2) u4PktLen);
        if (i4RetVal == OSIX_FAILURE)
        {
            RM_TRC3 (RM_FAILURE_TRC | RM_CRITICAL_TRC,
                     "RmConfEnqConfigMsg :Configuration of SynchUp message failed"
                     "[AppName=%s, Event=%s, Seq#=%d]\n",
                     gapc1AppName[u4DestEntId], gapc1RmEvntName[u1Event],
                     u4SeqNum);
        }
        else
        {
            RM_TRC2 (RM_CRITICAL_TRC, "Event: %s posting for %s module\n",
                     gapc1RmEvntName[u1Event], gapc1AppName[u4DestEntId]);
        }
#ifdef L2RED_TEST_WANTED
        RM_TEST_INCR_PROTO_RX_COUNT (u4DestEntId);
#endif
        return i4RetVal;
    }
#endif
    /* When ISSU MM operation is in progress, Prevent
     * Peer Down notifications to other modules */
    if ((u1Event == RM_PEER_DOWN) &&
        (IssuGetMaintModeOperation () == OSIX_TRUE) &&
        (RM_FORCE_SWITCHOVER_FLAG () == RM_FSW_NOT_OCCURED))
    {
        if (pPkt != NULL)
        {
            RM_FREE (pPkt);
        }

        /* Reset the sequence number as the standby is
         * undergoing software upgrade */
        RmRxResetRxBuffering ();
        return RM_SUCCESS;
    }

    if (gRmInfo.RmAppInfo[u4DestEntId] != NULL)
    {
        if (gRmInfo.RmAppInfo[u4DestEntId]->u4ValidEntry == TRUE)
        {
            if (gRmInfo.RmAppInfo[u4DestEntId]->pFnRcvPkt != NULL)
            {
                TmrGetPreciseSysTime (&CurrentTime);
                u4CurrentTime = (CurrentTime.u4Sec * 1000) +
                    (CurrentTime.u4NanoSec / 1000000);
                gaRmSwitchOverTime[u4DestEntId].u4EntryTime = u4CurrentTime;
                if (u1Event != RM_MESSAGE)
                {
                    RM_TRC2 (RM_CRITICAL_TRC,
                             "Event: %s posting for %s module\n",
                             gapc1RmEvntName[u1Event],
                             gapc1AppName[u4DestEntId]);
                }

                /* Posting data and event to destination protocol */
                (*(gRmInfo.RmAppInfo[u4DestEntId]->pFnRcvPkt))
                    (u1Event, pPkt, (UINT2) u4PktLen);

#ifdef L2RED_TEST_WANTED
                if ((pPkt != NULL) && (u1Event == RM_MESSAGE))
                {
                    RM_TEST_INCR_PROTO_RX_COUNT (u4DestEntId);
                }
#endif
                return RM_SUCCESS;
            }
        }
    }

    RM_TRC3 (RM_EVENT_TRC | RM_FAILURE_TRC | RM_CTRL_PATH_TRC,
             "RmUtilPostDataOrEvntToProtocol: Posting Msg/Event to Protocol "
             "failed [AppName=%s, Event=%s, Seq#=%d]\n",
             gapc1AppName[u4DestEntId], gapc1RmEvntName[u1Event], u4SeqNum);

    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmUtilIsDynamicSyncAllowed           
 * Input(s)           : u4AppId -  Protocol identifier that is known to RM.
 * Output(s)          : None.
 * Returns            : RM_TRUE  - If dynamic sync-up is allowed for the
 *                                 specified module
 *                      RM_FALSE - If dynamic sync-up is not allowed for the 
 *                                 specified module.
 * Action             : This is the function used by protocols/applications
 *                      to get whether dynamic sync-up messages formation for
 *                      a particular protocol is allowed or not. Before forming
 *                      the dynamic sync-up message/Bulk update message  
 *                      protocols/applications should refer this API.
 *                      Dynamic Sync-up will be blocked for a 
 *                      protocol/application in the following scenarios -
 *                      1. Static configuration restoration is in progress.
 *                      2. Node transition is in progress.
 *                      3. Lower layer of current protocol/application does
 *                         not completed its bulk update yet.
 ******************************************************************************/
INT4
RmUtilIsDynamicSyncAllowed (UINT4 u4AppId)
{
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4ChkAppId = RM_APP_ID;

    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        /* For standby node we need not to check more */
        return RM_TRUE;
    }

    /* 1. Static configuration restoration is not completed case handle. */
    /* For Vcm and Mbsm, dynamic bulk update is compeleted before static 
     * bulk update. So, allow further dynamic sync messages from these 
     * protocols even when static bulk update is in progress */
    if ((u4AppId != RM_VCM_APP_ID) && (u4AppId != RM_MBSM_APP_ID) &&
        (RM_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_RESTORED))
    {
        RM_TRC (RM_FAILURE_TRC, "RmUtilIsDynamicSyncAllowed: "
                "Static configuration restoration is not completed.\n");
        return RM_FALSE;
    }

    /* 2. Node transition is in progress. */
    if (RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmUtilIsDynamicSyncAllowed: "
                "Node transition is in progress\n");
        return RM_FALSE;
    }

    /* 3. Lower layer of current protocol/application does not 
     * completed its bulk update yet.*/
    if ((u4AppId == RM_APP_ID) || (u4AppId >= RM_MAX_APPS))
    {
        RM_TRC (RM_FAILURE_TRC, "RmUtilIsDynamicSyncAllowed: "
                "Invalid AppId\n");
        /* This API is not applicable for RM AppId or invalid AppId */
        return RM_FALSE;
    }

    /* Get the AppId for the lower layer protocol */
    if (RmUtilGetPrevBulkSupportedAppId (u4AppId, &u4ChkAppId) == RM_SUCCESS)
    {
        OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdSts, u4ChkAppId,
                                 sizeof (tRmAppList), bResult);
        if (bResult == OSIX_FALSE)
        {
            RM_TRC1 (RM_FAILURE_TRC, "RmUtilIsDynamicSyncAllowed: "
                     "Lower layer(%s) not completed its bulk update\n",
                     gapc1AppName[u4ChkAppId]);
            return RM_FALSE;
        }
    }
    return RM_TRUE;
}

/******************************************************************************
 * Function           : RmSystemRestart
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine restart the system.
 ******************************************************************************/
VOID
RmSystemRestart ()
{
#if !defined(NPSIM) && defined (NPAPI_WANTED)
    /* To restart the switch */
    system ("reboot");
#else
    RM_TRC (RM_CRITICAL_TRC, "\r\nRmSystemRestart: switch restart is not"
            " supported in NP Simulator. Administrator is requested to reboot"
            " the switch manually....\r\n");
#endif
    return;
}

/******************************************************************************
 * Function           : RmHandleProtocolRestart
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This function restarts all the protocols registered
 *                       with RM. And also initiates bulk updates to receive
 *                       the Active node configurations.
 ******************************************************************************/
VOID
RmHandleProtocolRestart (VOID)
{
    tOsixTaskId         TaskId = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleProtocolRestart: ENTRY \r\n");

    /* Clear the Rx buffer */
    RmRxBufClearRxBuffer ();
    gRmInfo.bIsRxBuffFull = RM_FALSE;
    gRmInfo.bIsRestartInPrgs = RM_TRUE;

    /* Set the Node transition in progreess to TRUE to block the 
     * static and dynamic configurations update */
    RM_SET_NODE_TRANSITION_IN_PROGS_STATE (RM_TRUE);

    /* Create a new task to restart all the protocols registered
     * with the RM module */
    if (OsixTskCrt ((UINT1 *) RM_RESTART_PROTO_TASK_NAME,
                    (RM_RESTART_PROTO_TASK_PRI | OSIX_SCHED_RR),
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) RmRestartProtoTaskMain, 0,
                    &TaskId) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "RM RESTART PROTOCOl task creation failed\n");
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleProtocolRestart: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmReInitToStandbyState
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This function reinitializes the RM node in standby 
 *                       state after protocols reinitialization to start
 *                       the sync up configurations update.
 ******************************************************************************/
VOID
RmReInitToStandbyState (VOID)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmReInitToStandbyState: ENTRY \r\n");

    /* Set the Restart in progress flag to false */
    gRmInfo.bIsRestartInPrgs = RM_FALSE;

    /* Reset the Rx buffer related flags and seq number */
    RmRxResetRxBuffering ();

    /* Clear switch over status */
    MEMSET (gRmInfo.RmSwitchoverSts, 0, sizeof (tRmAppList));

    /* Clear the buffer contents. */
    if (TMO_SLL_Count (&(gRmConfInfo.RmConfigFailBuff)) != 0)
    {
        RmUtilDelFailBuff ();
    }

    RmSetTxEnable (RM_FALSE);

    RmClearBulkUpdatesStatus ();

    RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;

    /* Notify the protocols to go to standby state */
    RmNotifyProtocols (GO_STANDBY);

    HbApiSetFlagAndTxHbMsg (HB_STDBY_PEER_UP);

    RM_TRC (RM_CTRL_PATH_TRC, "RmReInitToStandbyState: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmRestartProtoTaskMain
 * Input(s)           : pi1Arg.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Rm restarts the protocols registered wtih RM.
 ******************************************************************************/
VOID
RmRestartProtoTaskMain (INT1 *pi1Arg)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmRestartProtoTaskMain: ENTRY \r\n");

    UNUSED_PARAM (pi1Arg);

    MGMT_LOCK ();

#ifdef CLI_WANTED
    /* Logout the cli console as the protocols are restarted and
     * the existing configurations and the information learnt is lost. */
    CliBlockConsole ();
#endif

    if (RmRestartProtocols () == RM_FAILURE)
    {
        RM_TRC (RM_CRITICAL_TRC, "RmRestartProtoTaskMain: "
                "Restarting of the protocols failed. Rebooting "
                "the standby node.\r\n");
        MGMT_UNLOCK ();
        RmSystemRestart ();
        return;
    }

    if (RM_SEND_EVENT (RM_TASK_ID, RM_REINIT_STANDBY) != OSIX_SUCCESS)
    {
        RM_TRC (RM_CRITICAL_TRC, "SendEvent failed in "
                "RmRestartProtoTaskMain\n");
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmRestartProtoTaskMain: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHandleInitBulkUpdate
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function starts up the bulk update process in the
 *                       standby node after the software reboot of all the 
 *                       protocols registered with RM.
 ******************************************************************************/
VOID
RmHandleInitBulkUpdate (VOID)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4NextAppId = RM_APP_ID;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleInitBulkUpdate: ENTRY \r\n");
    RM_TRC (RM_NOTIF_TRC, "Initiate bulk request trigger after "
            "sending GO_STANDBY to protocols\n");
    RmUtilGetNextAppId (RM_APP_ID, &u4NextAppId);

    /* Intiate bulk update for the lowest layer protocol */
    if (u4NextAppId != RM_APP_ID)
    {
        ProtoEvt.u4AppId = RM_APP_ID;
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_INITIATE_BULK_UPDATE;
        RmHandleProtocolEvent (&ProtoEvt);
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHandleInitBulkUpdate: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHandleProtocolEvent
 * Input(s)           : pEvt->u4AppId - Module Id.
 *                      pEvt->u4Event   - Event send by protocols
 *                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /
 *                      RM_INITIATE_BULK_UPDATE / RM_BULK_UPDT_ABORT / 
 *                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / 
 *                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED / 
 *                      RM_STANDBY_EVT_PROCESSED)
 *                      pEvt->u4Error   - Error code (RM_MEMALLOC_FAIL / 
 *                      RM_SENDTO_FAIL / RM_PROCESS_FAIL / RM_NONE)
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by protocols/applications to intimate RM 
 *                      about the protocol operations. RM decides upon the 
 *                      events and send appropriate notifications to the 
 *                      management application or events to the protocols. 
 ******************************************************************************/
UINT1
RmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    UINT4               u4EntId = 0;
    UINT4               u4CurrentTime = 0;
    tUtlSysPreciseTime  CurrentTime;
    UINT1               u1IsNextHigherLayerPresent = RM_SUCCESS;
    UINT1               u1RetVal = RM_SUCCESS;
    UINT1               u1Event = 0;
    UINT1               u1SendSWONotification = RM_FALSE;    /* Flag tp prevent
                                                               sending switchover
                                                               completion 
                                                               notification on 
                                                               init to standby 
                                                               node transition */
    tRmNotificationMsg  NotifMsg;

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

    NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
    NotifMsg.u4State = RM_GET_NODE_STATE ();
    NotifMsg.u4AppId = RM_APP_ID;    /* Currently all notifications except
                                       failure notifcation is sent by 
                                       RM module. */
    NotifMsg.u4Error = pEvt->u4Error;
    UtlGetTimeStr (NotifMsg.ac1DateTime);

    RM_TRC2 (RM_EVENT_TRC | RM_NOTIF_TRC,
             "RmApiHandleProtocolEvent: AppId - %s Time - %s\n",
             gapc1AppName[pEvt->u4AppId], NotifMsg.ac1DateTime);
    switch (pEvt->u4Event)
    {
        case RM_INITIATE_BULK_UPDATE:
            /* Send start of bulk update notification. This event is send
               by 1) RM on  triggering GO_STANDBY to protocols and also during 
               2) software audit failure case, protocols trigger this event.
               3) During failure of bulk update protocols can trigger this
               event. */
            gRmInfo.u1IsBulkUpdtInProgress = RM_TRUE;
            RM_TRC1 (RM_CRITICAL_TRC, "RM Initiate bulk update for %s module\n",
                     gapc1AppName[pEvt->u4AppId]);
            NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
            NotifMsg.u4Status = RM_STARTED;
            RmTrapSendNotifications (&NotifMsg);

            u1Event = L2_INITIATE_BULK_UPDATES;

            if (pEvt->u4AppId == RM_APP_ID)
            {
                /* Node state transits to STANDBY. RM triggers the bulk request 
                 * initiation. It finds the lowest layer
                 * module registered and sends the
                 * L2_INITIATE_BULK_UPDATES event. */

                u1IsNextHigherLayerPresent = RmUtilTriggerNextHigherLayer
                    (pEvt->u4AppId, u1Event);
            }
            else
            {
                /* Protocol triggers the bulk update request initation (SWAudit
                 * mismatch / bulk update process failure).
                 * L2_INITIATE_BULK_UPDATES event will be posted to 
                 * the same module. */
                if (RmUtilPostDataOrEvntToProtocol (pEvt->u4AppId, u1Event,
                                                    NULL, 0, 0) == RM_SUCCESS)
                {

                    /* Only bulk update is triggered here. 
                     * Must not send switchover completion notification. in
                     * software audit failure. */
                    RM_TRC (RM_NOTIF_TRC,
                            "Switchover complete flag disabled \n");
                    gRmInfo.u1IsBulkUpdtFromSwAudit = RM_TRUE;
                }
            }
            break;

        case RM_PROTOCOL_BULK_UPDT_COMPLETION:

            if (pEvt->u4AppId == RM_MSR_APP_ID)
            {
                RmNotifyProtocols (RM_CONFIG_RESTORE_COMPLETE);
                /* If node is an ACTIVE node dont process 
                 * RM_PROTOCOL_BULK_UPDT_COMPLETION for MSR */
                if (RM_GET_NODE_STATE () == RM_ACTIVE)
                {
                    break;
                }
            }

            RM_TRC1 (RM_EVENT_TRC | RM_SYNCUP_TRC | RM_NOTIF_TRC |
                     RM_SYNC_EVENT_TRC,
                     "Event: Bulk update completed by %s module\n",
                     gapc1AppName[pEvt->u4AppId]);

            RM_TRC1 (RM_CRITICAL_TRC,
                     "Dynamic Bulk update completed "
                     "for %s module\n", gapc1AppName[pEvt->u4AppId]);

            /* Bulk update is completed by the lower layer module.
             * Send L2_INITIATE_BULK_UPDATES event to next higher layer
             * module.*/
            u1Event = L2_INITIATE_BULK_UPDATES;

            u1IsNextHigherLayerPresent = RmUtilTriggerNextHigherLayer
                (pEvt->u4AppId, u1Event);

            break;

        case RM_BULK_UPDT_ABORT:
        {
            if (RM_IGNORE_ABORT_NOTIFICATION () == RM_TRUE)
            {
                break;
            }

            RM_TRC1 (RM_CRITICAL_TRC | RM_NOTIF_TRC,
                     "RmApiHandleProtocolEvent: "
                     "RM bulk update aborted for : %s]\n",
                     gapc1AppName[pEvt->u4AppId]);

            RmHandleSystemRecovery (RM_SYNCUP_ABORT);
            /* Failure identified during bulk update process. Send bulk update
             * failure notification. */
            NotifMsg.u4AppId = pEvt->u4AppId;
            NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
            NotifMsg.u4Status = RM_FAILED;
            RmTrapSendNotifications (&NotifMsg);
            break;
        }

        case RM_IDLE_TO_ACTIVE_EVT_PROCESSED:
            RM_TRC1 (RM_CTRL_PATH_TRC, "\nINIT to ACTIVE transition completed "
                     "for : %s protocol\r\n", gapc1AppName[pEvt->u4AppId]);
            /* MSR is the last protocol to get the GO_ACTIVE event so,
             * all protocols got the GO_ACTIVE event.*/
            if (pEvt->u4AppId == RM_MSR_APP_ID)
            {
                return u1RetVal;
            }
            u1Event = RM_INIT_TO_ACTIVE_COMPLETE;
            u1IsNextHigherLayerPresent = RmUtilTriggerNextHigherLayer
                (pEvt->u4AppId, u1Event);
            if (u1IsNextHigherLayerPresent == RM_FAILURE)
            {
                /* During maintenance mode, update the ISSU procedure status */
                /* In case of in-compatible mode, load version is performed in
                 * Active node , so send the procedure status */
                if (IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE)
                {
                    IssuUpdateProcedureStatus ();
                }
            }
            return u1RetVal;

        case RM_STANDBY_EVT_PROCESSED:
            if (RmUtilCheckForSwCompletion (pEvt->u4AppId) == RM_SUCCESS)
            {
                RM_TRC (RM_NOTIF_TRC,
                        "GO_STANDBY event is processed by all protocols "
                        "Send switchover completion notification\n");
                u1SendSWONotification = RM_TRUE;
            }
            break;

        case RM_STANDBY_TO_ACTIVE_EVT_PROCESSED:

            RM_TRC (RM_NOTIF_TRC, "Process state change completion \n");

            TmrGetPreciseSysTime (&CurrentTime);
            u4CurrentTime = (CurrentTime.u4Sec * 1000) +
                (CurrentTime.u4NanoSec / 1000000);
            gaRmSwitchOverTime[pEvt->u4AppId].u4ExitTime = u4CurrentTime;

            gaRmSwitchOverTime[pEvt->u4AppId].u4SwitchOverTime =
                (gaRmSwitchOverTime[pEvt->u4AppId].u4ExitTime -
                 gaRmSwitchOverTime[pEvt->u4AppId].u4EntryTime);

            gRmTotalSwitchOverTime +=
                gaRmSwitchOverTime[pEvt->u4AppId].u4SwitchOverTime;

            if (RmUtilCheckForSwCompletion (pEvt->u4AppId) == RM_SUCCESS)
            {
                RM_TRC (RM_NOTIF_TRC,
                        "GO_ACTIVE event is processed by all protocols "
                        "Send switchover completion notification \n");
                u1SendSWONotification = RM_TRUE;

                /* If the data plane type is set to separate, then the 
                 * Hardware Audit is not required in New Active Node 
                 * After switchover */
                if (RM_GET_NODE_STATE () == RM_ACTIVE)
                    /* Node state might get changed in between */
                {
                    /* When ISSU is in progress, no need to perform
                     * Hardware Audit */
                    if (IssuGetMaintModeOperation () != OSIX_TRUE)
                    {
                        for (u4EntId = 0; u4EntId < RM_MAX_APPS; u4EntId++)
                        {
                            if ((u4EntId == RM_APP_ID) ||
                                (u4EntId == RM_MSR_APP_ID) ||
                                (u4EntId == RM_SNMP_APP_ID) ||
                                (u4EntId == RM_CLI_APP_ID))
                            {
                                continue;
                            }
                            RM_TRC1 (RM_NOTIF_TRC, "Event: Initiating"
                                     " hardware audit for %s module\n",
                                     gapc1AppName[u4EntId]);
                            RmUtilPostDataOrEvntToProtocol (u4EntId, (UINT1)
                                                            RM_INIT_HW_AUDIT,
                                                            NULL, 0, 0);
                        }
                    }
                }
            }

            break;

        case RM_PROTOCOL_SEND_EVENT:

            RM_TRC (RM_NOTIF_TRC, "Sending event to RM module\r\n");

            if (RM_TASK_ID == RM_INIT)
            {
                if (RM_GET_TASK_ID (RM_SELF, (const UINT1 *) RM_TASK_NAME,
                                    &RM_TASK_ID) != OSIX_SUCCESS)
                {

                    RM_TRC (RM_FAILURE_TRC, "Failed to get RM Task ID\r\n");
                    return RM_FAILURE;
                }
            }

            if (RM_SEND_EVENT (RM_TASK_ID, pEvt->u4SendEvent) != OSIX_SUCCESS)
            {
                RM_TRC (RM_FAILURE_TRC, "Failed to Send Event to RM task\r\n");
                return RM_FAILURE;
            }

            break;

        default:
            break;
    }

    if (u1IsNextHigherLayerPresent == RM_FAILURE)
    {
        /* No next protocol exists to initiate bulk update.
         * Send bulk update completion notififcation. */
        gRmInfo.u1IsBulkUpdtInProgress = RM_FALSE;
        /* All Bulk Updates are completed clear the last Bulk Sync App Id Object */

#ifdef TCP_WANTED
        if ((RM_GET_PREV_NODE_STATE () == RM_ACTIVE)
            && (RM_GET_NODE_STATE () == RM_STANDBY))
        {
            TcpStopAllTimers ();
        }
#endif
        gRmInfo.u1LastBulkSyncAppId = 0;

        /* Post the RM_DYNAMIC_BULK_UPDATE_COMPLETION event to CONFIG task */
        RmConfPostBulkUpdCompEvent (RM_DYNAMIC_BULK_UPDATE_COMPLETION);
        if (gu4TrafficToCpuAllowed == RM_FALSE)
        {
            RmTmrStopTimer (RM_CPU_TRAFFIC_CTRL_TIMER);
#ifdef NPAPI_WANTED
            RmFsNpHRSetStdyStInfo (NP_RM_HR_RESUME_COPY_TO_CPU_TRAFFIC, NULL);
#endif
            gu4TrafficToCpuAllowed = RM_TRUE;
        }

        RM_TRC (RM_SYNCUP_TRC | RM_NOTIF_TRC, "RmApiHandleProtocolEvent: "
                "Bulk update completed for all protocols.\n");

        NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
        NotifMsg.u4Status = RM_COMPLETED;
        RmTrapSendNotifications (&NotifMsg);

        /* Check for switchover completion by the protocol modules 
         * and also check u1SWOCmpltNotifFlg is not disabled */
        if (RmUtilCheckForSwCompletion (RM_APP_ID) == RM_SUCCESS)
        {
            RM_TRC (RM_NOTIF_TRC, "Send switchover completion notification \n");
            u1SendSWONotification = RM_TRUE;
        }

    }

    /* Send switchover completion notification. */
    if ((u1SendSWONotification == RM_TRUE) &&
        (gRmInfo.u1IsBulkUpdtFromSwAudit == RM_FALSE) &&
        (gRmInfo.u1IsBulkUpdtInProgress == RM_FALSE))
    {
        RM_TRC (RM_NOTIF_TRC, "Switchover completed \n");
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
        NotifMsg.u4Status = RM_COMPLETED;
        RmTrapSendNotifications (&NotifMsg);
    }

    return u1RetVal;
}

/******************************************************************************
 * Function           : RmUtilCheckAndCompleteBulkUpdt
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This API returns
 *                         RM_SUCCESS -  Bulk update Completed
 *                         RM_FAILURE -  Bulk update Not Completed
 ******************************************************************************/
VOID
RmUtilCheckAndCompleteBulkUpdt (VOID)
{
    UINT4               u4Index = 0;
    INT4                i4RetVal = RM_SUCCESS;
    for (u4Index = 0; u4Index < sizeof (tRmAppList); u4Index++)
    {
        if (gRmInfo.RmBulkUpdStsMask[u4Index] == 0)
        {
            /* No applications present in this range of index */
            continue;
        }

        if ((gRmInfo.RmBulkUpdSts[u4Index] &
             gRmInfo.RmBulkUpdStsMask[u4Index]) !=
            gRmInfo.RmBulkUpdStsMask[u4Index])
        {
            i4RetVal = RM_FAILURE;
            break;
        }
    }
    if (i4RetVal == RM_SUCCESS)
    {
        RmTmrStopTimer (RM_BULK_UPDATE_TIMER);
        if (gu4BulkUpdateInProgress == RM_TRUE)
        {
            gu4BulkUpdateInProgress = RM_FALSE;
        }
        RmTmrStopTimer (RM_CPU_TRAFFIC_CTRL_TIMER);
        if (gu4TrafficToCpuAllowed == RM_FALSE)
        {
#ifdef NPAPI_WANTED
            RmFsNpHRSetStdyStInfo (NP_RM_HR_RESUME_COPY_TO_CPU_TRAFFIC, NULL);
#endif
            gu4TrafficToCpuAllowed = RM_TRUE;
        }
    }

    return;
}

/******************************************************************************
 * Function           : RmIssuMaintenanceModeEnable
 * Description        : Clear all the pending Rm messages and set the ISSU MM 
 *                      Oper Status
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
******************************************************************************/
VOID
RmIssuMaintenanceModeEnable ()
{
    UINT1               u1SsnIndex = 0;

    if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                       (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        /* Maintenance Mode will be sent to peer once the TX buffer
         * messages are cleared */
        RM_TRC (RM_CTRL_PATH_TRC,
                "RmIssuMaintenanceModeEnable: TX LIST exists \r\n");
        return;
    }

    /* Process the pending RM queue messages */
    RmQueProcessPendingMsg ();

    if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                       (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
    {
        /* Since the Tx list is not empty, some entries are not sent
         * while processing the queue. Return without setting the 
         * ISSU maintenance mode oper status */
        return;
    }

    /* Set the ISSU MM Oper status */
    IssuSetOperMaintenanceMode (ISSU_MAINTENANCE_OPER_ENABLE);

    return;
}

INT4
RmGetFtSrvSockFd ()
{
    return (gRmInfo.i4FtSrvSockFd);
}

INT4
RmGetTcpSrvSockFd ()
{
    return (RM_TCP_SRV_SOCK_FD ());
}
