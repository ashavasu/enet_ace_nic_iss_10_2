/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmrxbuf.c,v 1.5 2010/08/09 09:51:15 prabuc Exp $
*
* Description: RM utility functions for the RX buffer handling.
***********************************************************************/
#ifndef _RM_RXBUF_C_
#define _RM_RXBUF_C_

#include "rmincs.h"

/******************************************************************************
 * Function           : RmRxBufAddPkt
 * Input(s)           : pu1Pkt - packet pointer that needs to be added in the 
 *                               RX Buffer. packet should contatin the RM 
 *                               Header. it should be a liner buffer pointer.
 *                      u4SeqNum - Sequence number of the received packet
 *                      u4PktLen - length of the entire packet.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function add the received sync-up data packet
 *                      to the RX Buffer (Hash Table).
 ******************************************************************************/
INT4
RmRxBufAddPkt (UINT1 *pu1Pkt, UINT4 u4SeqNum, UINT4 u4PktLen)
{
    tRmRxBufHashEntry  *pRxBufNode = NULL;

    /* Allocate memory for the hash node : tRmRxBufHashEntry */
    if ((pRxBufNode =
         (tRmRxBufHashEntry *) MemAllocMemBlk (gRmInfo.RmRxBufPoolId)) == NULL)
    {
        RM_TRC (RM_CRITICAL_TRC | RM_FAILURE_TRC,
                "Memory allocation failed for the RX Buff node.\n");
        RmHandleSystemRecovery (RM_RX_BUFF_NODE_ALLOC_FAILED);
        /* pu1Pkt memory should be cleared by the caller function */
        return RM_FAILURE;
    }

    /* Update the node information */
    TMO_HASH_Init_Node (&pRxBufNode->RxBufHashNode);
    pRxBufNode->u4SeqNum = u4SeqNum;
    pRxBufNode->pu1Pkt = pu1Pkt;
    pRxBufNode->u4PktLen = u4PktLen;

    /* Add the node to the Hash Table */
    TMO_HASH_Add_Node (gRmInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                       RM_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum), NULL);

    RM_RX_BUFF_NODE_COUNT ()++;

#ifdef L2RED_TEST_WANTED
    if (RM_RX_BUFF_NODE_COUNT () > RM_RX_BUFF_PEAK_COUNT ())
    {
        /* Update the max use counter of Rx buffer */
        RM_RX_BUFF_PEAK_COUNT () = RM_RX_BUFF_NODE_COUNT ();
    }
#endif

    RM_TRC2 (RM_BUFF_TRACE | RM_SYNCUP_TRC, "RmRxBufAddPkt: HASH ADD[seq# %d], "
             "(hash count = %d)\r\n", u4SeqNum, RM_RX_BUFF_NODE_COUNT ());
    return RM_SUCCESS;

}

/******************************************************************************
 * Function           : RmRxBufSearchPkt
 * Input(s)           : u4SeqNum - Sequenece number of the packet that needs
 *                                 to be searched in the RX Buffer.
 * Output(s)          : None.
 * Returns            : pointer to tRmRxBufHashEntry node if entry found
 *                      else return NULL
 * Action             : This function searchs a desired sequence numbered
 *                      packet in the RX Buffer. If entry found then returns 
 *                      the Hash Node pointer to the caller else return NULL.
 ******************************************************************************/
tRmRxBufHashEntry  *
RmRxBufSearchPkt (UINT4 u4SeqNum)
{
    tRmRxBufHashEntry  *pRxBufNode = NULL;
    tTMO_HASH_NODE     *pNode = NULL;
    UINT4               u4HashIndex = RM_RX_GET_HASH_INDEX (u4SeqNum);

    /* Get the hash index using the u4SeqNum */
    /* Scan only the required bucket. No need to scan the entire table */
    TMO_HASH_Scan_Bucket (gRmInfo.pRxBufHashTable,
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pRxBufNode = (tRmRxBufHashEntry *)
            (pNode - RM_RX_BUFF_GET_HASHNODE_OFFSET ());
        if (pRxBufNode->u4SeqNum == u4SeqNum)
        {
            /* Match found */
            return pRxBufNode;
        }
    }
    return NULL;
}

/******************************************************************************
 * Function           : RmRxBufDeleteNode
 * Input(s)           : pRxBufNode - Pointer to the HashNode that needs to be
 *                                   deleted form the Hash Table
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function deletes the specified hash node from the
 *                      hash table (RX Buffer). It also takes care of 
 *                      releasing the memory used by the node.
 ******************************************************************************/
INT4
RmRxBufDeleteNode (tRmRxBufHashEntry * pRxBufNode)
{
    UINT1               u1SsnIndex = 0;

    TMO_HASH_Delete_Node (gRmInfo.pRxBufHashTable, &pRxBufNode->RxBufHashNode,
                          RM_RX_GET_HASH_INDEX (pRxBufNode->u4SeqNum));

    /* Deleted from the Hash table. Now clean the memory for this node */
    RmRxBufFreeHashNode (&pRxBufNode->RxBufHashNode);

    RM_RX_BUFF_NODE_COUNT ()--;

    if (gRmInfo.bIsRxBuffFull == RM_TRUE)
    {
        gRmInfo.bIsRxBuffFull = RM_FALSE;
        RM_TRC (RM_CRITICAL_TRC,
                "!!! Sync message reception is resumed "
                "because Rx buffer is available !!!\n");
        /* As we support 1:1 standby, 
         * there is only one session alive */
        RmPktRcvd (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
    }
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRxBufCreate
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE
 * Action             : This function creates the RX Buffer(i.e. Hash Table)
 *                     
 ******************************************************************************/
INT4
RmRxBufCreate (VOID)
{
    /* Create the Hash Table to buffer the received sync-up messages in 
     * sequentional order */
    gRmInfo.pRxBufHashTable =
        TMO_HASH_Create_Table (RM_RXBUF_MAX_HASH_BUCKET_COUNT, NULL, FALSE);

    if (gRmInfo.pRxBufHashTable == NULL)
    {
        /* Hash table creation failed */
        RM_TRC (RM_CRITICAL_TRC, "RX Buff creation failed\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmRxBufDestroy
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function deletes the entire RX buffer (hash table)
 *                      including all the nodes. It takes care of deleting the
 *                      memory used by the hash nodes.
 ******************************************************************************/
VOID
RmRxBufDestroy (VOID)
{
    if (gRmInfo.pRxBufHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gRmInfo.pRxBufHashTable, RmRxBufFreeHashNode);
        gRmInfo.pRxBufHashTable = NULL;
    }
}

/******************************************************************************
 * Function           : RmRxBufFreeHashNode
 * Input(s)           : pNode - Hash node pointer
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases the memory used by a hash node.
 *                     
 ******************************************************************************/
VOID
RmRxBufFreeHashNode (tTMO_HASH_NODE * pNode)
{
    tRmRxBufHashEntry  *pRxBufNode = NULL;
    pRxBufNode = (tRmRxBufHashEntry *)
        (pNode - RM_RX_BUFF_GET_HASHNODE_OFFSET ());

    /* Free the packet memory liner buffer pointer */
    RM_RXBUF_PKT_MEM_FREE (pRxBufNode->pu1Pkt);

    /* Release the memblock for the hash node */
    MemReleaseMemBlock (gRmInfo.RmRxBufPoolId, (UINT1 *) pRxBufNode);
}

/******************************************************************************
 * Function           : RmRxBufClearRxBuffer
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases all the nodes added
 *                       in the RxBuffer
 ******************************************************************************/
VOID
RmRxBufClearRxBuffer (VOID)
{
    tTMO_HASH_NODE     *pNode = NULL, *pTmpNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (gRmInfo.pRxBufHashTable, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gRmInfo.pRxBufHashTable, u4HashIndex,
                                  pNode, pTmpNode, tTMO_HASH_NODE *)
        {
            TMO_HASH_Delete_Node (gRmInfo.pRxBufHashTable, pNode, u4HashIndex);
            RmRxBufFreeHashNode (pNode);
            RM_RX_BUFF_NODE_COUNT ()--;
        }
    }
    return;
}

#endif /* _RM_RXBUF_C_ */
