/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: rmnpapi.c,v 1.1 2013/03/19 12:21:57 siva Exp $
*
* Description: All  Network Processor API Function calls are done here.
***********************************************************************/
#ifndef _RMNPAPI_C_
#define _RMNPAPI_C_

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : RmRmNpUpdateNodeState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes RmNpUpdateNodeState
 *                                                                          
 *    Input(s)            : Arguments of RmNpUpdateNodeState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmRmNpUpdateNodeState (UINT1 u1Event)
{
    tFsHwNp             FsHwNp;
    tRmNpModInfo       *pRmNpModInfo = NULL;
    tRmNpWrRmNpUpdateNodeState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RM_MOD,    /* Module ID */
                         RM_NP_UPDATE_NODE_STATE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmNpModInfo = &(FsHwNp.RmNpModInfo);
    pEntry = &pRmNpModInfo->RmNpRmNpUpdateNodeState;

    pEntry->u1Event = u1Event;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RmFsNpHRSetStdyStInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpHRSetStdyStInfo
 *                                                                          
 *    Input(s)            : Arguments of FsNpHRSetStdyStInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmFsNpHRSetStdyStInfo (tRmHRAction eAction, tRmHRPktInfo * pRmHRPktInfo)
{
    tFsHwNp             FsHwNp;
    tRmNpModInfo       *pRmNpModInfo = NULL;
    tRmNpWrFsNpHRSetStdyStInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RM_MOD,    /* Module ID */
                         FS_NP_H_R_SET_STDY_ST_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmNpModInfo = &(FsHwNp.RmNpModInfo);
    pEntry = &pRmNpModInfo->RmNpFsNpHRSetStdyStInfo;

    pEntry->eAction = eAction;
    pEntry->pRmHRPktInfo = pRmHRPktInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED
#ifdef L2RED_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : RmMbsmNpProcRmNodeTransition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpProcRmNodeTransition
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpProcRmNodeTransition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmMbsmNpProcRmNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpProcRmNodeTransition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_PROC_RM_NODE_TRANSITION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpProcRmNodeTransition;

    pEntry->i4Event = i4Event;
    pEntry->u1PrevState = u1PrevState;
    pEntry->u1State = u1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* L2RED_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : RmMbsmNpClearHwTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpClearHwTbl
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpClearHwTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmMbsmNpClearHwTbl (INT4 i4SlotId)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpClearHwTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_CLEAR_HW_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpClearHwTbl;

    pEntry->i4SlotId = i4SlotId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RmMbsmNpGetStackMac                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpGetStackMac
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpGetStackMac
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmMbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpGetStackMac *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_GET_STACK_MAC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;

    pEntry->u4SlotId = u4SlotId;
    pEntry->pu1MacAddr = pu1MacAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RmMbsmNpHandleNodeTransition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpHandleNodeTransition
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpHandleNodeTransition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmMbsmNpHandleNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpHandleNodeTransition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_HANDLE_NODE_TRANSITION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpHandleNodeTransition;

    pEntry->i4Event = i4Event;
    pEntry->u1PrevState = u1PrevState;
    pEntry->u1State = u1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */

#endif /* _RMNPAPI_C_ */
