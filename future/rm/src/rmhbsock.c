/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmhbsock.c,v 1.9 2014/07/06 11:48:14 siva Exp $
*
* Description: Redundancy manager socket interface functions.
*********************************************************************/
#include "rmhbincs.h"

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>

#ifdef write
#undef write
#endif
#ifdef read
#undef read
#endif
#include <unistd.h>

/* RM  Multicast ip addr used is 224.0.0.250 */
UINT1               gRmMcastGroupAddr[4] = { 224, 0, 0, 250 };

/****************************************************************************
 * Function           : RmHbRawSockInit 
 * Input(s)           : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          : RM HB Raw socket descriptor.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create a RAW IP socket, set socket options 
 *                      for  sending and receiving RM multicast packets.
 ****************************************************************************/
INT4
RmHbRawSockInit (INT4 *pi4HbSockFd)
{
    INT4                i4SockFd = RM_INV_SOCK_FD;
    INT4                i4Flags = -1;
    UINT4               u4RetVal = RM_SUCCESS;
    struct ip_mreqn     mreqn;
    struct in_addr      SelfNodeAddr;
    UINT4               u4IfIndex;
    INT4                i4OptnVal = TRUE;
    INT4                i4MCloopVal = 0;    /*disable receiving mcast packets
                                             *sent by us*/
    INT4                i4precedence = 6;    /* precedence specified to give high
                                             * priority RM HB Packets */

    RM_HB_TRC (RM_SOCK_TRC, "RmHbRawSockInit: "
               "Raw socket creation entry...\n");
    /* Open a raw socket for heart beat messages */
    i4SockFd = socket (AF_INET, SOCK_RAW, RM_HB_PROTO);
    if (i4SockFd < 0)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmHbRawSockInit: Raw Socket "
                   "Creation Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }

    /* Get current socket flags */
    if (u4RetVal != RM_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "RmHbRawSockInit: Fcntl GET "
                       "Failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
        {
            RM_HB_TRC (RM_FAILURE_TRC, "RmHbRawSockInit: Fcntl SET "
                       "Failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        SelfNodeAddr.s_addr = OSIX_HTONL (RM_HB_GET_SELF_NODE_ID ());

        /* Set socket option to send multicast IP pkt */
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_MULTICAST_IF,
                        (char *) &SelfNodeAddr, sizeof (struct in_addr)) < 0)
        {
            perror ("RmHbRawSockInit: IP_MULTICAST_IF setsockopt failure\n");
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: setsockopt "
                       "IP_MULTICAST_IF - failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* This option allows the user to form their own IP hdr instead
         * of kernel including its IP hdr.
         * While using loopback interface lo:1 & lo:2, the ip pkt sent
         * out has incorrect src ip. Kernel fills up the src IP in
         * IP hdr as the interface's primary IP addr (which may not
         * be correct in case of an interface having multiple ip addr). */
        if ((setsockopt (i4SockFd, IPPROTO_IP, IP_HDRINCL, (&i4OptnVal),
                         sizeof (INT4))) < 0)
        {
            perror ("RmHbRawSockInit: IP_HDRINCL setsockopt failure\n");
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: setsockopt IP_HDRINCL "
                       "- failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        MEMSET (&mreqn, 0, sizeof (mreqn));
        MEMCPY ((char *) &mreqn.imr_multiaddr.s_addr,
                (char *) &gRmMcastGroupAddr, 4);

        u4IfIndex = RmGetIfIndexFromName ((UINT1 *) IssGetRmIfNameFromNvRam ());
        mreqn.imr_ifindex = u4IfIndex;

        /* Socket option to receive the multicast IP packet */
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                        (void *) &mreqn, sizeof (mreqn)) < 0)
        {
            perror ("RmHbRawSockInit: Setsockopt Failed for "
                    "IP_ADD_MEMBERSHIP. \n");
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: setsockopt IP_ADD_MEMBERSHIP "
                       "- failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    if (u4RetVal != RM_FAILURE)
    {
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_MULTICAST_LOOP,
                        &i4MCloopVal, sizeof (INT4)) < 0)
        {
            perror ("RmHbRawSockInit: Setsockopt Failed "
                    "for IP_MULTICAST_LOOP. \n");
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: setsockopt "
                       "IP_MULTICAST_LOOP - failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Priority Given to  RM-Hb messages since they are very vital
         * This is done so as to ensure that the RM-Hb Packets are not lost */
        if (setsockopt (i4SockFd, SOL_SOCKET, SO_PRIORITY,
                        (char *) &i4precedence, sizeof (i4precedence)) < 0)
        {
            perror ("RmHbRawSockInit : setsockopt  Failed  for SO_PRIORITY)");
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: setsockopt "
                       "SO_PRIORITY - failure !!!\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }

    if (u4RetVal != RM_FAILURE)
    {
        if (SelAddFd (i4SockFd, RmHbRawPktRcvd) == RM_FAILURE)
        {
            RM_HB_TRC (RM_FAILURE_TRC,
                       "RmHbRawSockInit: SelAddFd Failure "
                       "while adding RM raw socket\n");
            close (i4SockFd);
            u4RetVal = RM_FAILURE;
        }
    }
    if (u4RetVal == RM_FAILURE)
    {
        return (RM_FAILURE);
    }
    *pi4HbSockFd = i4SockFd;
    RM_HB_TRC (RM_SOCK_TRC, "RmHbRawSockInit: "
               "Raw socket created successfully - exit...\n");
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmHbRawSockSend 
 * Input(s)           : pu1Data - pointer to the data to be sent out
 *                      u2PktLen - Length of the data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the packet out thru' RAW IP socket.
 *                      This is socket is specifically used by RM module to
 *                      send the Heart Beat messages destined to the 
 *                      multicast ip address 224.0.0.250.
 ******************************************************************************/
UINT4
RmHbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    struct sockaddr_in  DestAddr;
    INT4                i4WrBytes;
    UINT4               u4RetVal = RM_SUCCESS;

    MEMSET (&DestAddr, 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestAddr.sin_port = 0;

    i4WrBytes = sendto (gRmHbInfo.i4RawSockFd, pu1Data, u2PktLen, 0,
                        (struct sockaddr *) &DestAddr,
                        sizeof (struct sockaddr_in));
    if (i4WrBytes < 0)
    {
        perror ("RmSend failure\n");
        RM_HB_TRC (RM_FAILURE_TRC, "!!! RmSendTo - failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmHbRawSockRcv 
 * Input(s)           : pu1Data - pointer to rcv the packet
 *                      u2BufSize - size of the buffer to be received.
 * Output(s)          : pu1Data - pointer to the packet rcvd. 
 *                      pu4PeerAddr - IP addr of the peer from which the 
 *                                    pkt is rcvd.   
 * Returns            : Length of the packet received.
 * Action             : Routine to receive RM protocol packets.
 ******************************************************************************/
INT4
RmHbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    INT4                i4Len;
    struct sockaddr_in  PeerAddr;
    INT4                i4AddrLen = sizeof (PeerAddr);

    i4Len = recvfrom (gRmHbInfo.i4RawSockFd, pu1Data,
                      u2BufSize, 0, (struct sockaddr *) &PeerAddr,
                      (socklen_t *) & i4AddrLen);
    *pu4PeerAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
    return (i4Len);
}

/******************************************************************************
 * Function           : RmHbCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 ******************************************************************************/
INT4
RmHbCloseSocket (INT4 i4SockFd)
{
    INT4                i4RetVal;

    i4RetVal = close (i4SockFd);
    if (i4RetVal < 0)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "!!! socket close Failure !!!\n");
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function           : RmGetIfIndexFromName
 * Input(s)           : pu1IfName - Interface name.
 * Output(s)          : None.
 * Returns            : IfIndex
 * Action             : Routine to get the Interface index of the given 
 *                      Interface name.
 ******************************************************************************/
UINT4
RmGetIfIndexFromName (UINT1 *pu1IfName)
{
    UINT4               u4IfIndex;

    u4IfIndex = if_nametoindex ((const CHR1 *) pu1IfName);
    return (u4IfIndex);
}

/******************************************************************************
 * Function           : RmHbGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : RM_SUCCESS or RM_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 ******************************************************************************/
UINT4
RmHbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo)
{
    INT4                i4SockFd = -1;
    struct ifreq        ifreq;
    struct sockaddr_in *saddr;
    UINT4               u4RetVal = RM_SUCCESS;

    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd >= 0)
    {
        STRNCPY (ifreq.ifr_name, pu1IfName, IFNAMSIZ);
        ifreq.ifr_name[IFNAMSIZ - 1] = '\0';

        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) < 0)
        {
            perror ("Error in getting RM interface\n");
            u4RetVal = RM_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            pNodeInfo->u4IpAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }

        if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) < 0)
        {
            u4RetVal = RM_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
            pNodeInfo->u4NetMask = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }

        close (i4SockFd);
    }
    else
    {
        u4RetVal = RM_FAILURE;

    }
    if (u4RetVal == RM_FAILURE)
    {
        return (RM_FAILURE);
    }

    return (RM_SUCCESS);
}
