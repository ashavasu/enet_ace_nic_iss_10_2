/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmhbapi.c,v 1.6 2013/06/19 13:25:16 siva Exp $
*
* Description: API related functions of RM Heart Beat module.
*********************************************************************/
#include "rmhbincs.h"

/******************************************************************************
 * Function           : RmHbApiSendRmEventToHb
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None 
 * Action             : Exported API to send RM core module 
 *                      actions to be done to RM HB Module.
 ******************************************************************************/
UINT4
RmHbApiSendRmEventToHb (UINT4 u4Event)
{
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbApiSendRmEventToHb: ENTRY \r\n");

    if (RM_HB_MODE == ISS_RM_HB_MODE_EXTERNAL)
    {
        RM_HB_TRC (RM_CTRL_PATH_TRC,
                   "RmHbApiSendRmEventToHb: HB Mode External \r\n");
        return RM_SUCCESS;
    }
    /* Event handled by this Heart Beat API *
     * 1. RM_HB_FSW_EVENT -> Initiate the Force-switchover
     * 2. RM_HB_SYNC_MSG_PROCESSED -> The sync messages which were 
     *                            pending are processed. 
     * 3. RM_HB_CHG_TO_TRANS_IN_PRGS -> Change the node state to 
     *                                     transition in progress as node is 
     *                                     triggered the force-switchover, during
     *                                     election is not entertained.
     * 4. RM_HB_APP_INIT_COMPLETED     -> RM enabled modules 
     *                                  shutdown and start completed.
     */
    if (RM_HB_SEND_EVENT (RM_HB_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC, "RmHbApiSendRmEventToHb: Send Event "
                   "Failed\n");
        return RM_FAILURE;
    }
    RM_HB_TRC (RM_CTRL_PATH_TRC, "RmHbApiSendRmEventToHb: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbApiSetFlagAndTxHbMsg          
 * Input(s)           : u4Flag - The flag that has to be set in the Hb messages.
 * Output(s)          : None
 * Returns            : None 
 * Action             : This API sets the given flag flag in the
 *                      HB messages sent to the peer standby node.
 ******************************************************************************/
UINT4
RmHbApiSetFlagAndTxHbMsg (UINT4 u4Flag)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_EXTERNAL)
    {
        RM_HB_TRC (RM_CTRL_PATH_TRC,
                   "RmHbApiSetFlagAndTxHbMsg: HB Mode External \r\n");
        return RM_SUCCESS;
    }

    RM_HB_LOCK ();

    RM_HB_SET_FLAGS (u4Flag);

    if (RmFormAndSendHbMsg () == RM_FAILURE)
    {
        RM_HB_TRC (RM_FAILURE_TRC,
                   "RmHbApiHandleStandbyReboot: "
                   "Sending standby reboot to peer failed !!!\n");
        RM_HB_UNLOCK ();
        return RM_FAILURE;
    }

    RM_HB_UNLOCK ();

    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHbSetKeepAliveFlag          
 * Input(s)           : u1RmKeepAliveFlag - The flag which indicates whether node 
 *                      receives rm messages from peer or not.
 * Output(s)          : None
 * Returns            : None 
 * Action             : This API sets the given flag and the flag is set whenever 
 *                      the node receives any Message from Peer.
 ******************************************************************************/
VOID
RmHbSetKeepAliveFlag (UINT1 u1RmKeepAliveFlag)
{
    gu1KeepAliveFlag = u1RmKeepAliveFlag;
    return;
}
