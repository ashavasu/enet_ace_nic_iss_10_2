/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmque.c,v 1.23 2017/01/25 13:18:28 siva Exp $
*
* Description: RM Queue processing functions.
***********************************************************************/
#include "rmincs.h"
#include "rmglob.h"

/******************************************************************************
 * Function           : RmProcessPktFromApp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to process the pkt from application.
 *                      i.e., Rm rcvs the pkt from app send it to peer.
 ******************************************************************************/
VOID
RmProcessPktFromApp (VOID)
{
    tRmPktQMsg         *pRmPktQMsg = NULL;
    UINT4               u4DestAddr = 0;
    UINT1               u1SsnIndex = 0;
    UINT1              *pu1Data = NULL;
    INT4                i4ConnFd = RM_INV_SOCK_FD;
    INT4                i4ConnState = 0;
    UINT4               u4RmPktLen = 0;
    UINT4               u4RelinquishCntr = 0;
    UINT1              *pResBuf = NULL;
    /* HITLESS RESTART */
    UINT4               u4SrcEntId = 0;
    UINT1               u1MsgType = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: ENTRY \r\n");

    /* Syncup message should not be send to the standby node if the node   
     * transition is in progress*/
    /* HITLESS RESTART */
    if ((RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE) &&
        (RM_HR_GET_STATUS () == RM_HR_STATUS_DISABLE))
    {
        RM_TRC (RM_SYNCUP_TRC, "RmProcessPktFromApp: "
                "Sending of Syncup message is blocked as Node transition is "
                "in progress \r\n");
        RmClearSyncMsgQueue ();
        return;
    }
    /* check if the peer node ip is known */
    /* If node state is in init then clear the sync messages,
     * In connection lost and restored case we will be in RM_INIT state 
     * until the shut and start operation is completed*/
    /* HITLESS RESTART */
    u4DestAddr = RM_GET_PEER_NODE_ID ();
    if (((u4DestAddr == 0) ||
         (gRmInfo.u4DynamicSyncStatus != RM_TRUE) ||
         (RM_GET_NODE_STATE () == RM_INIT)) &&
        (RM_HR_GET_STATUS () == RM_HR_STATUS_DISABLE))
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                "NO peer is learnt \r\n");
        RmClearSyncMsgQueue ();
        return;
    }

    /* HITLESS RESTART */
    if ((RmGetSsnIndexFromPeerAddr (u4DestAddr, &u1SsnIndex) == RM_FAILURE) &&
        (RM_HR_GET_STATUS () == RM_HR_STATUS_DISABLE))
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                "NO session for the peer \r\n");
        RmClearSyncMsgQueue ();
        return;
    }

    /* HITLESS RESTART */
    if (RM_HR_GET_STATUS () != RM_HR_STATUS_DISABLE)
    {
        /* to fall in the following switch statement's default conditiion */
        RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_HR_SKIP_SOCKET_CONN;
    }
    switch (RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus)
    {
        case RM_SOCK_NOT_CONNECTED:
            if (RmTcpSockConnect
                (RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr,
                 &i4ConnFd, &i4ConnState) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                        "TCP client connect failed \r\n");
                /* If connection failed, donot clear the queue, instead wait for 
                 * the peer to connect, if connection lost with peer, 
                 * HB task will intimate about the peer dead
                 * upon this event arrival clear the TX List */
                RmAddSyncMsgFromQToTxList (u1SsnIndex);
                if (gRmInfo.u1ConnRetryCnt >= RM_MAX_CONN_RETRY_CNT)
                {
                    if (RM_GET_NODE_STATE () == RM_STANDBY)
                    {
                        /* Restart the standby node when the max. connection 
                         * retry is done with the Active*/
                        RmHandleSystemRecovery (RM_MAX_CONN_RETRY_EXCEEDED);
                    }
                    else
                    {
                        /* Keep the connection retry to continue until the 
                         * standby is down or connection with the 
                         * standby is successful */
                        gRmInfo.u1ConnRetryCnt = 0;
                    }
                }
                /* Start the connection retry timer */
                RmTmrStartTimer (RM_CONN_RETRY_TIMER,
                                 RM_CONN_RETRY_TMR_INTERVAL);
                gRmInfo.u1ConnRetryCnt++;
                return;
            }
            RmTmrStopTimer (RM_CONN_RETRY_TIMER);
            gRmInfo.u1ConnRetryCnt = 0;
            if (i4ConnState == RM_SOCK_CONNECT_IN_PROGRESS)
            {
                RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
                RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                    RM_SOCK_CONNECT_IN_PROGRESS;
                RmAddSyncMsgFromQToTxList (u1SsnIndex);
                RM_TRC (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: "
                        "Connection in progress\r\n");
                KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
                return;
            }
            /* This residual memory allocation only be freed in the 
             * peer down event handle */
            if (RM_GET_SSN_INFO (u1SsnIndex).pResBuf == NULL)
            {
                if (RmMemAllocForResBuf (&pResBuf) == RM_FAILURE)
                {
                    RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                            "Residual buffer allocation failed \r\n");
                    RmCloseSocket (i4ConnFd);
                    RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus =
                        RM_SOCK_NOT_CONNECTED;
                    return;
                }
                RM_GET_SSN_INFO (u1SsnIndex).pResBuf = pResBuf;
            }
            RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = i4ConnFd;
            RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_CONNECTED;
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: "
                     "Connection is successful for the peer=%x\r\n",
                     RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr);
            break;

        case RM_SOCK_CONNECT_IN_PROGRESS:
            RM_TRC (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: "
                    "Sync packet is received when "
                    "in Connection In Progress case\r\n");
            RmAddSyncMsgFromQToTxList (u1SsnIndex);
            return;

        case RM_SOCK_CONNECTED:
            if (RM_SLL_COUNT (&RM_SSN_ENTRY_TX_LIST
                              (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
            {
                RmAddSyncMsgFromQToTxList (u1SsnIndex);
                RM_TRC (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: "
                        "Pending sync ups exist in the TX List "
                        "so, append this sync data too\r\n");
                return;
            }
            i4ConnFd = RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd;
            RM_TRC (RM_CTRL_PATH_TRC,
                    "RmProcessPktFromApp: Connection exists\r\n");
            break;
        default:
            break;
            /* Invalid state, error should be thrown */
    }

    while (RM_RECEIVE_FROM_QUEUE (RM_QUEUE_ID, (UINT1 *) &pRmPktQMsg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (RmGetLinearBufAndLenFromPktQMsg (pRmPktQMsg,
                                             &pu1Data,
                                             &u4RmPktLen) == RM_SUCCESS)
        {
			if ((pRmPktQMsg->u1MsgType == RM_SSL_GET_CERT_REQ) || 
                  (pRmPktQMsg->u1MsgType == RM_V3_GET_CRYPTSEQ_REQ))
			{
		        if (RmSendCertGenMsgtoPeer (u1SsnIndex, i4ConnFd,
                                              pu1Data,
                                              u4RmPktLen) == RM_FAILURE)
                {
                    RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                            "RmSendMsg -- Failed !!!\n");
                }
		
			}
            else
            {
#ifdef L2RED_TEST_WANTED
            if ((pRmPktQMsg->u1MsgType == RM_DYN_AUDIT_CHKSUM_REQUEST) ||
                (pRmPktQMsg->u1MsgType == RM_DYN_AUDIT_CHKSUM_RESPONSE))
            {
                if (RmSendAuditReqRespToPeer (u1SsnIndex, i4ConnFd,
                                              pu1Data,
                                              u4RmPktLen) == RM_FAILURE)
                {
                    RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                            "RmSendMsg -- Failed !!!\n");
                }
            }
            else
            {
#endif
                /* HITLESS RESTART */
                if ((RM_HR_GET_STATUS () != RM_HR_STATUS_DISABLE) &&
                    (pRmPktQMsg->u1MsgType == RM_DYNAMIC_SYNC_MSG))
                {
                    RmHRGetRmHdrInfo (pu1Data, &u1MsgType, &u4SrcEntId);

                    if (u1MsgType == RM_HR_STDY_ST_PKT_MSG)
                    {
                        RmHRProcessStdyStPktMsg (pu1Data, u4SrcEntId);
                    }
                    else if (u1MsgType == RM_HR_STDY_ST_PKT_TAIL)
                    {
                        RmHRProcessStdyStTail (pu1Data, u4SrcEntId);
                    }
                    else
                    {
                        RmHRProcessBulkMsg (pu1Data, u1MsgType, u4SrcEntId);
                    }
                    MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId,
                                        (UINT1 *) pu1Data);
                    RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
                    pRmPktQMsg = NULL;
                    KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
                    continue;
                }

                if (RM_SLL_COUNT (&RM_SSN_ENTRY_TX_LIST
                                  (RM_GET_SSN_INFO (u1SsnIndex))) == 0)
                {
                    /* Send the pkt out to peer thru' RAW socket */
                    if (RmSendMsg (u1SsnIndex, i4ConnFd,
                                   pu1Data, u4RmPktLen) == RM_FAILURE)
                    {
                        RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                                "RmSendMsg -- Failed !!!\n");
                    }
                }
                else
                {
                    RmAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4RmPktLen, 0);
                }
#ifdef L2RED_TEST_WANTED
            }
#endif
            }
        }
        RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
        pRmPktQMsg = NULL;
        /* This Relinquish point is for processing 
         * the higher priority messages */
        u4RelinquishCntr++;
        if (u4RelinquishCntr > RM_RELINQUISH_CNTR)
        {
            if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) == OSIX_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: Send Event "
                        "RM_PKT_FROM_APP Failed\n");
            }
            break;
        }
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: EXIT \r\n");
    KW_FALSEPOSITIVE_FIX (pu1Data);
    KW_FALSEPOSITIVE_FIX3 (i4ConnFd);
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmDynAuditPktFromApp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to process the pkt from application.
 *                      i.e., Rm rcvs the pkt from app send it to peer.
 ******************************************************************************/
VOID
RmDynAuditPktFromApp (VOID)
{
    tRmDynAuditChkSumMsg *pRmChkSumQMg = NULL;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                i4RetVal = 0;
    UINT2               u2BufLen = 0;
    UINT2               u2AppId = 0;
    UINT1               u1State = 0;
    INT1                i1AuditFlag = RM_TRUE;

    while (RM_RECEIVE_FROM_QUEUE (RM_APP_QUEUE_ID, (UINT1 *) &pRmChkSumQMg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        u2AppId = pRmChkSumQMg->u2AppId;
        gRmDynAuditInfo[u2AppId].i4AuditChkSumValue =
            (INT4) pRmChkSumQMg->u2ChkSumValue;

        u1State = (UINT1) RM_GET_NODE_STATE ();

        /*Check whether the checksum is received from all the APIs in Active Node */
        for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
        {
            if ((gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit == RM_TRUE) &&
                (gRmDynAuditInfo[u2AppId].i4AuditChkSumValue ==
                 RM_INVALID_CHKSUM_VALUE))
            {
                gu4DynAuditDoneForAllApp = RM_FALSE;
                i1AuditFlag = RM_FALSE;
            }
        }

        if (i1AuditFlag == RM_TRUE)
        {
            gu4DynAuditDoneForAllApp = RM_TRUE;
        }

        if ((u1State == RM_ACTIVE) && (gu4DynAuditDoneForAllApp == RM_TRUE))
        {
            if ((pMsg = RmAllocTxBuf (RM_DYN_AUDIT_CHKSUM_REQ_LEN)) == NULL)
            {
                MemReleaseMemBlock (gRmInfo.RmDynCksumPktQMsgPoolId,
                                    (UINT1 *) pRmChkSumQMg);
                RM_TRC (RM_FAILURE_TRC, "Rm alloc failed\n");
                return;
            }

            u2BufLen = RM_DYN_AUDIT_CHKSUM_REQ_LEN;
            RM_DATA_PUT_2_BYTE (pMsg, u4OffSet, u2BufLen, i4RetVal);
            if (i4RetVal == RM_FAILURE)
            {
                MemReleaseMemBlock (gRmInfo.RmDynCksumPktQMsgPoolId,
                                    (UINT1 *) pRmChkSumQMg);
                RM_FREE (pMsg);
                return;
            }

            /*Start a Timer */

            /*Send CheckSum Request to Standby */
            if (RmSendDynAuditChkSumReqToStdBy (pMsg, u2BufLen) == RM_FAILURE)
            {
                MemReleaseMemBlock (gRmInfo.RmDynCksumPktQMsgPoolId,
                                    (UINT1 *) pRmChkSumQMg);
                RM_FREE (pMsg);
                return;
            }
            if (RM_GET_NODE_STATE () == RM_ACTIVE)
            {
                RmTmrStartTimer (RM_DYN_SYNC_AUDIT_TIMER, RM_DYN_AUDIT_TMR_INT);
            }

        }
        if ((u1State == RM_STANDBY) && (gu4DynAuditDoneForAllApp == RM_TRUE) &&
            (RM_TRUE == gu1IsAuditChksumRespPend))
        {
            gu1IsAuditChksumRespPend = RM_FALSE;
            if (RmConstructChkSumResponseMsg () == RM_FAILURE)
            {
                gu4DynAuditDoneForAllApp = RM_FALSE;
                return;
            }
            for (u2AppId = 0; u2AppId < RM_MAX_APPS; u2AppId++)
            {
                gRmDynAuditInfo[u2AppId].u4AuditStatus =
                    RM_DYN_AUDIT_NOT_TRIGGERED;
                gRmDynAuditInfo[u2AppId].u4IsAppSetForAudit = RM_FALSE;
                gRmDynAuditInfo[u2AppId].i4AuditChkSumValue =
                    RM_INVALID_CHKSUM_VALUE;
            }
            gu4DynAuditDoneForAllApp = RM_FALSE;
        }

        MemReleaseMemBlock (gRmInfo.RmDynCksumPktQMsgPoolId,
                            (UINT1 *) pRmChkSumQMg);
    }
    return;
}
#endif

/******************************************************************************
 * Function           : RmGetLinearBufAndLenFromPktQMsg
 * Input(s)           : pRmPktQMsg - Packet Q message
 *                      ppu1Data - Pointer to pointer to the linear sync data
 *                      pu4RmPktLen - Pointer to the Sync packet data length
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILUIRE
 * Action             : Gets the linear buffer and length from packet Queue. 
 ******************************************************************************/
INT4
RmGetLinearBufAndLenFromPktQMsg (tRmPktQMsg * pRmPktQMsg,
                                 UINT1 **ppu1Data, UINT4 *pu4RmPktLen)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4RmPktLen = 0;
    UINT4               u4RmSeqNo = 0;
    UINT2               u2Cksum = 0;

    if (pRmPktQMsg->u1MsgType == RM_STATIC_CONF_INFO)
    {
#ifdef L2RED_TEST_WANTED
        RM_TEST_INCR_PROTO_TX_COUNT (RM_STATIC_CONF_APP_ID);
#endif
        return ((INT4) RmConfProcessConfigSyncUpInfo (&(pRmPktQMsg->ConfInfo),
                                               ppu1Data, pu4RmPktLen));
    }
    /* For RM_DYNAMIC_SYNC_MSG */
    pRmMsg = (tRmMsg *) (pRmPktQMsg->DynSyncMsg);
    /* Get the length of RM pkt to be sent */
    RM_GET_DATA_4_BYTE (pRmMsg, RM_HDR_OFF_TOT_LENGTH, u4RmPktLen);

     /*Added necessary validation to check the incoming packet size is
     greater than, then avoid the copy to the locally allocated buffer
     and return to avoid the corruption. At the same time the buffer size
     is increased from 1568 to 2000 so that it accommodates any greater size
     packets receivedAdded necessary validation to check the incoming packet
     size is greater than, then avoid the copy to the locally allocated buffer
     and return to avoid the corruption. At the same time the buffer size is
     increased from 1568 to 2000 so that it accommodates any greater size
     packets received */
     if (u4RmPktLen > RM_MAX_SYNC_PKT_LEN)
     {
         RM_TRC (RM_FAILURE_TRC, "RmGetLinearBufAndLenFromPktQMsg: "
                 "Received RM message length is greater than the allocated\n");
         return (RM_FAILURE);
     }

    /* Allocate memory for linear buffer */
    if ((pu1Data = MemAllocMemBlk (gRmInfo.RmPktMsgPoolId)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                "Rm linear buf allocation - failure !!!\n");
        return (RM_FAILURE);
    }
    RM_TRC1 (RM_CTRL_PATH_TRC, "RmProcessPktFromApp: "
             "Sync packet size to be sent with RM header=%d\r\n", u4RmPktLen);
    /* Copy the RM info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (RM_COPY_TO_LINEAR_BUF (pRmMsg, pu1Data, 0, u4RmPktLen) == CRU_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmProcessPktFromApp: "
                "Copy from CRU buf to linear buf failed\n");
        MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
        return (RM_FAILURE);
    }
   if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        if (pRmPktQMsg->u1MsgType ==  RM_DYNAMIC_SYNC_MSG)
        {
            /* Sequence No  conv of HTONL is done by PTR_ASSIGN4. The sequence
             * no is modified  for dynamic SYNC msg from Active ONLY as
             * Standby uses 0 as Sequence No.*/
            u4RmSeqNo = RM_RX_GETNEXT_VALID_SEQNUM ();
            RM_RX_INCR_LAST_SEQNUM_PROCESSED ();
            PTR_ASSIGN4 ((pu1Data + RM_HDR_OFF_SEQ_NO), u4RmSeqNo);
        }
    }

    u2Cksum = RM_LINEAR_CALC_CKSUM ((const INT1 *) pu1Data, u4RmPktLen);
    PTR_ASSIGN2 ((pu1Data + RM_HDR_OFF_CKSUM), u2Cksum);
    *ppu1Data = pu1Data;
    *pu4RmPktLen = u4RmPktLen;

    /* Print the module level trace message */
    RmTrcPrintModSyncMsgTrace (pu1Data);
    return (RM_SUCCESS);
}

/******************************************************************************
 * Function           : RmAddSyncMsgFromQToTxList
 * Input(s)           : u1SsnIndex - Session index 
 * Output(s)          : None
 * Returns            : None
 * Action             : Adds the RM sync message from Packet Q to TX List 
 ******************************************************************************/
VOID
RmAddSyncMsgFromQToTxList (UINT1 u1SsnIndex)
{
    tRmPktQMsg         *pRmPktQMsg = NULL;
    UINT4               u4RmPktLen = 0;
    UINT1              *pu1Data = NULL;

    RM_TRC (RM_CTRL_PATH_TRC, "RmAddSyncMsgFromQToTxList: ENTRY \r\n");
    /* Pending nodes are exist, 
     * so add the message to be sent in the Tx List */
    while (RM_RECEIVE_FROM_QUEUE (RM_QUEUE_ID, (UINT1 *) &pRmPktQMsg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (RmGetLinearBufAndLenFromPktQMsg (pRmPktQMsg,
                                             &pu1Data,
                                             &u4RmPktLen) == RM_SUCCESS)
        {
            RmAddSyncMsgIntoTxList (u1SsnIndex, pu1Data, u4RmPktLen, 0);
        }
        RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
        pRmPktQMsg = NULL;
    }
    KW_FALSEPOSITIVE_FIX (pu1Data);
    RM_TRC (RM_CTRL_PATH_TRC, "RmAddSyncMsgFromQToTxList: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmCtrlMsgHandler
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine processes the peer information, node state
 *                      related information and ACK from protocols.
 ******************************************************************************/
VOID
RmCtrlMsgHandler (VOID)
{
    tRmCtrlQMsg        *pCtrlRmMsg = NULL;
    UINT1               u1SsnIndex = 0;
    UINT4               u4RelinquishCnt = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmCtrlMsgHandler: ENTRY \r\n");
    while (RM_RECEIVE_FROM_QUEUE (RM_CTRL_QUEUE_ID, (UINT1 *) &pCtrlRmMsg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pCtrlRmMsg->u4MsgType)
        {
            case RM_PEER_INFO_MSG:
                /* Peer id (ip address), status (Peer up/down) are sent through
                 * peer control message*/
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmCtrlMsgHandler: RM_PEER_INFO_MSG\n");
                RmPeerInfoMsgHandle (&(pCtrlRmMsg->PeerCtrlMsg));
                break;
            case RM_WR_SOCK_MSG:
                /* Socket writable will be called when 
                 * the socket send buffer is available or 
                 * ack received from peer for socket connect 
                 * 'connection in progress' case " */
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmCtrlMsgHandler: RM_WR_SOCK_MSG\n");
                RmSockWritableHandle (pCtrlRmMsg->SktCtrlMsg.i4SockId);
                break;
            case RM_PROTO_ACK_MSG:
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmCtrlMsgHandler: RM_PROTO_ACK_MSG\n");
                /* Protocol Acknowledgement for the processed sync-up message */
                RmUtilHandleProtocolAck (pCtrlRmMsg->ProtoAckCtrlMsg);
                break;
            case RM_RD_SOCK_MSG:
                /* Rm pkt rcvd in reliable socket - 
                 * processing the msgs are handled here */
                RM_TRC (RM_EVENT_TRC | RM_CTRL_PATH_TRC,
                        "RmCtrlMsgHandler: RM_RD_SOCK_MSG\n");
                if (RmGetSsnIndexFromConnFd (pCtrlRmMsg->SktCtrlMsg.i4SockId,
                                             &u1SsnIndex) == RM_FAILURE)
                {
                    RM_TRC (RM_FAILURE_TRC,
                            "RmCtrlMsgHandler: Session does not exist "
                            "for the socket fd\n");
                    break;
                }
	        u4RelinquishCnt++;
                RmProcessRcvEvt (&(RM_GET_SSN_INFO (u1SsnIndex)));
                break;
            default:
                break;
        }
        /* Control message pool is freed here */
        KW_FALSEPOSITIVE_FIX3 (pCtrlRmMsg->SktCtrlMsg.i4SockId);
        if (MemReleaseMemBlock (gRmInfo.RmCtrlMsgPoolId, (UINT1 *) pCtrlRmMsg)
            != MEM_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC,
                    "RmCtrlMsgHandler: Mem release from ctrl msg "
                    "pool failed\n");
        }
	if (u4RelinquishCnt >= RM_RELINQUISH_CNTR)
	{
	    RmPktRcvd (RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd);
	    break;
	}
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmCtrlMsgHandler: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmClearSyncMsgQueue
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : Clears the RM sync messages from RM packet Queue
 ******************************************************************************/
VOID
RmClearSyncMsgQueue (VOID)
{
    tRmPktQMsg         *pRmPktQMsg = NULL;
    RM_TRC (RM_CTRL_PATH_TRC, "RmClearSyncMsgQueue: ENTRY \r\n");
    /* Pending nodes are exist, 
     * so add the message to be sent in the Tx List */
    while (RM_RECEIVE_FROM_QUEUE (RM_QUEUE_ID, (UINT1 *) &pRmPktQMsg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmClearSyncMsgQueue: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmQueEnqCtrlMsg
 * Input(s)           : pRmCtrlQMsg -> Control Q informations (Peer up/Peer down)
 *                                     socket writable informations etc..
 * Output(s)          : None
 * Returns            : None
 * Action             : Enqueues the control packets to RM core module task 
 ******************************************************************************/

INT4
RmQueEnqCtrlMsg (tRmCtrlQMsg * pRmCtrlQMsg)
{
    RM_TRC (RM_SOCK_TRC, "RmQueEnqCtrlMsg: ENTRY \r\n");
    if (RM_SEND_TO_QUEUE (RM_CTRL_QUEUE_ID,
                          (UINT1 *) &pRmCtrlQMsg,
                          RM_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (MemReleaseMemBlock (gRmInfo.RmCtrlMsgPoolId,
                                (UINT1 *) pRmCtrlQMsg) != MEM_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "RmQueEnqCtrlMsg : SendToQ - "
                    "Memory release failed\n");
            return RM_FAILURE;
        }
        RM_TRC (RM_FAILURE_TRC, "RmQueEnqCtrlMsg : SendToQ failed\n");
        return RM_FAILURE;
    }
    if (RM_SEND_EVENT (RM_TASK_ID, RM_CTRL_QMSG_EVENT) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmQueEnqCtrlMsg: Send Event "
                "RM_CTRL_QMSG_EVENT Failed\n");
        return RM_FAILURE;
    }
    RM_TRC (RM_SOCK_TRC, "RmQueEnqCtrlMsg: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmQueProcessPendingMsg
 * Input(s)           : This is called when force switchover message is received
 *                      All pending queue message should be processed.
 * Output(s)          : None
 * Returns            : None
 * Action             : Read all messages from queue and sent to peer.
 ******************************************************************************/

VOID
RmQueProcessPendingMsg (VOID)
{
    tRmPktQMsg         *pRmPktQMsg = NULL;
    UINT4               u4DestAddr = 0;
    UINT4               u4RmPktLen = 0;
    INT4                i4ConnFd = RM_INV_SOCK_FD;
    UINT1               u1SsnIndex = 0;
    UINT1              *pu1Data = NULL;

    RM_TRC (RM_CTRL_PATH_TRC, "RmQueProcessPendingMsg: ENTRY \r\n");

    u4DestAddr = RM_GET_PEER_NODE_ID ();

    if (RmGetSsnIndexFromPeerAddr (u4DestAddr, &u1SsnIndex) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmQueProcessPendingMsg: "
                "NO session for the peer \r\n");
        RmClearSyncMsgQueue ();
        return;
    }

    i4ConnFd = RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd;

    while (RM_RECEIVE_FROM_QUEUE (RM_QUEUE_ID, (UINT1 *) &pRmPktQMsg,
                                  RM_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (RmGetLinearBufAndLenFromPktQMsg (pRmPktQMsg,
                                             &pu1Data,
                                             &u4RmPktLen) == RM_SUCCESS)
        {
            if (RmSendMsg (u1SsnIndex, i4ConnFd,
                           pu1Data, u4RmPktLen) == RM_FAILURE)
            {
                RM_TRC (RM_FAILURE_TRC, "RmQueProcessPendingMsg: "
                        "RmSendMsg -- Failed !!!\n");
                RmClearSyncMsgQueue ();
                return;
            }

            if (TMO_SLL_Count (&RM_SSN_ENTRY_TX_LIST
                               (RM_GET_SSN_INFO (u1SsnIndex))) != 0)
            {
                /* Since the Tx list is not empty, some entries are not sent
                 * while processing the queue
                 */
                RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
                return;
            }
        }
        RmUtilReleaseMemForPktQMsg (pRmPktQMsg);
        pRmPktQMsg = NULL;
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmQueProcessPendingMsg: EXIT \r\n");
}

/******************************************************************************
 * Function           : RmSslCertGenMsgFromApp 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to process the pkt from application.
 *                      i.e., Rm rcvs the pkt from app send it to peer.
 ******************************************************************************/
VOID
RmSslCertGenMsgFromApp (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                i4RetVal = 0;
    UINT2               u2BufLen = 0;
    
	u2BufLen = RM_SSL_CERT_REQ_LEN;
	if ((pMsg = RmAllocTxBuf (RM_SSL_CERT_REQ_LEN)) == NULL)
	{
	    RM_TRC (RM_FAILURE_TRC, "Rm alloc failed\n");
		return;
	}
    RM_DATA_PUT_2_BYTE (pMsg, u4OffSet, u2BufLen, i4RetVal);
    if (i4RetVal == RM_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }

    /*Send Certificate get Request to Standby */
    if (RmSendCertGenMsgToStdBy(pMsg, u2BufLen) == RM_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }
return;
}
/******************************************************************************
 * Function           : RmV3CryptSeqMsgFromApp 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to process the pkt from application.
 *                      i.e., Rm rcvs the pkt from app send it to peer.
 ******************************************************************************/

VOID
RmV3CryptSeqMsgFromApp (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                i4RetVal = 0;
    UINT2               u2BufLen = 0;

    u2BufLen = RM_OSPF3_CERT_REQ_LEN;
    if ((pMsg = RmAllocTxBuf (RM_OSPF3_CERT_REQ_LEN)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC, "Rm alloc failed\n");
        return;
    }
    RM_DATA_PUT_2_BYTE (pMsg, u4OffSet, u2BufLen, i4RetVal);
    if (i4RetVal == RM_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }

    /*Send Certificate get Request to Standby */
    if (RmSendCryptSeqMsgToStdBy(pMsg, u2BufLen) == RM_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }
return;
}

