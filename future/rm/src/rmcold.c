/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmcold.c,v 1.19 2014/05/23 07:55:03 siva Exp $
*
* Description: RM ColdStandby support implementation.
*********************************************************************/
#ifndef _RM_COLD_C
#define _RM_COLD_C
#include "rmincs.h"
#include "fsrm.h"
#include "fsrmlw.h"
#include <unistd.h>
#define LINK_LOCAL_IP     0xa9fe0100
#define MAX_COLUMN_LENGTH 120
#ifdef SNMP_2_WANTED
static INT1         ai1TempBuffer[SNMP_MAX_OID_LENGTH];
CONST CHR1         *gau1NodeState[] =
    { "STANDBY->ACTIVE", "ACTIVE->STANDBY", "PEER_ATTACH", "PEER_DETACH" };
#endif
UINT4               gRmRestartInfo;
/******************************************************************************
 * Function           : RmGetNodePriority
 * Input(s)           : None
 * Output(s)          : configured state of switch
 * Returns            : None
 * Action             : Routine used to get the configured state of the switch
******************************************************************************/
VOID
RmGetNodePriority (UINT4 *pu4Priority)
{
    *pu4Priority = RM_CONFIGURED_STATE ();
    return;
}

/******************************************************************************
 * Function           : RmGetNodeSwitchId
 * Input(s)           : None
 * Output(s)          : Slot id of switch
 * Returns            : None
 * Action             : Routine used to get the slot id of the switch
******************************************************************************/
VOID
RmGetNodeSwitchId (UINT4 *pu4NodeId)
{
    *pu4NodeId = RM_GET_SWITCH_ID ();
    return;
}

/******************************************************************************
 * Function           : RmGetSelfNodeIp
 * Input(s)           : None
 * Output(s)          : SwitchIp
 * Returns            : None
 * Action             : Routine used to get the Ip of the switch
******************************************************************************/
VOID
RmGetSelfNodeIp (UINT4 *pu4SelfNodeIp)
{
    *pu4SelfNodeIp = RM_GET_SELF_NODE_ID ();
    return;
}

/******************************************************************************
 * Function           : RmGetSelfNodeIp
 * Input(s)           : None
 * Output(s)          : SelfNodeMask
 * Returns            : None
 * Action             : Routine used to get the Ip of the switch
******************************************************************************/
VOID
RmGetSelfNodeMask (UINT4 *pu4SelfNodeMask)
{
    *pu4SelfNodeMask = RM_GET_SELF_NODE_MASK ();
    return;
}

/******************************************************************************
 * Function           : RmProcessInfoFromMbsm
 * Input(s)           : slotid,attach/detach indication
 * Output(s)          : None.
 * Returns            : None
 * Action             : Routine used to process indication from MBSM
******************************************************************************/
VOID
RmProcessInfoFromMbsm (UINT4 u4SlotId, UINT1 u1Type)
{
    tPeerRecEntry      *pPeerEntry = NULL;
    tStackNotificationMsg NotifMsg;
    UINT4               u4SwitchId;
    UINT1               u1PrevState = 0;
    UINT1               u1State = 0;

    u4SwitchId = RM_GET_SWITCH_ID ();
    RM_LOCK ();
    switch (u1Type)
    {
        case RM_ATTACH:
            if (u4SwitchId == u4SlotId)
            {
                RmProcessSelfAttach ();
            }
            else                /*Remote Slot Attach */
            {
                /* To send Trap notification and syslog message
                 * for remote slot attach */
                gRmInfo.u4TrapSwitchId = u4SlotId;
                NotifMsg.u4NodeId = u4SlotId;
                NotifMsg.u4State = STACK_NOTIFY_PEER_ATTACH;
                UtlGetTimeStr (NotifMsg.ac1DateTime);
                StackTrapSendNotifications (&NotifMsg);

                if (RmGetPeerEntry (u4SlotId, &pPeerEntry) == RM_FALSE)
                {
                    /*Entry not present. We should add entry */
                    RmCreatePeerNodeEntry (u4SlotId, &pPeerEntry);
                    RmAddPeerToPeerTable (pPeerEntry);
                }
                RM_GET_SSN_INFO (u4SlotId).u4PeerAddr = pPeerEntry->u4StackIp;

                /* Peers will come up in 30 secs after getting remote attach indication. 
                 * 'Switchbase-Mac request' message to peers need to be send after 
                 * PeerUpTmr expired*/
#ifdef MBSM_WANTED
                gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].u4SlotId =
                    u4SlotId;
                gaStackTimer[(u4SlotId -
                              MBSM_SLOT_INDEX_START)].u1NumofRetries = 1;
                gaStackTimer[(u4SlotId -
                              MBSM_SLOT_INDEX_START)].TmrRef.u1TimerType =
                    STACK_PEER_UP_TIMER;

                if (TmrStartTimer (gStackTmrLst,
                                   &gaStackTimer[(u4SlotId -
                                                  MBSM_SLOT_INDEX_START)].
                                   TmrRef.RefTimer,
                                   (STACK_DEFAULT_PEER_UP_INTERVAL /
                                    NO_OF_MSECS_PER_UINT)) != TMR_SUCCESS)
                {
                    RM_TRC (RM_FAILURE_TRC, "!!! Start Peer Up "
                            " timer Failed !!!\n");
                }
#endif

            }

            break;
        case RM_DETACH:
            if (u4SwitchId == u4SlotId)
            {
                RmProcessSelfDetach ();
            }
            else                /* Remote Slot Detach */
            {
                /* To send Trap notification and syslog message
                 * for remote slot detach event */
                gRmInfo.u4TrapSwitchId = u4SlotId;
                NotifMsg.u4NodeId = u4SlotId;
                NotifMsg.u4State = STACK_NOTIFY_PEER_DETACH;
                UtlGetTimeStr (NotifMsg.ac1DateTime);
                StackTrapSendNotifications (&NotifMsg);

                if (RmGetPeerEntry (u4SlotId, &pPeerEntry) != RM_FALSE)
                {
                    RmDeleteFromPeerTable (u4SlotId);
                    RM_UNLOCK ();
                    return;
                }
            }
            break;
        default:
            break;
    }
    RM_UNLOCK ();
    u1PrevState = RM_GET_PREV_NODE_STATE ();
    u1State = RM_GET_NODE_STATE ();
    if (u1PrevState == RM_STANDBY && u1State == RM_ACTIVE)
    {
#ifdef MBSM_WANTED
        /* When transit from Standby to Active 
         *  bcmx_rlink should be cleared*/
        RmMbsmNpHandleNodeTransition (0, RM_STANDBY, RM_ACTIVE);
#endif
    }
    KW_FALSEPOSITIVE_FIX (pPeerEntry);
    return;
}

/******************************************************************************
 * Function           : RmProcessSelfAttach
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : None
 * Action             : Routine used to process SelfAttach in cold standby
******************************************************************************/
VOID
RmProcessSelfAttach ()
{
    UINT1               u1State;
    u1State = (UINT1) RM_GET_NODE_STATE ();
    RM_SET_ACTIVE_NODE_ID (RM_GET_SELF_NODE_ID ());
    if (u1State != RM_ACTIVE)
    {
        RmHandleNodeStateUpdate (RM_ACTIVE);
    }
    return;
}

/******************************************************************************
 * Function           : RmProcessSelfDetach
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : None
 * Action             : Routine used to process SelfDetach in cold standby
******************************************************************************/
VOID
RmProcessSelfDetach ()
{
    RmHandleNodeStateUpdate (RM_STANDBY);
    return;
}

/******************************************************************************
 * Function           : RmGetStackIpForSlot
 * Input(s)           : None
 * Output(s)          : gRmInfo.u4SelfNode
 * Returns            : None
 * Action             : Routine used to derive the Stack Ip of the switch
******************************************************************************/
UINT4
RmGetStackIpForSlot (UINT4 u4SwitchId)
{
    UINT4               u4SelfIp;
    u4SwitchId++;
    u4SelfIp = LINK_LOCAL_IP;
    u4SelfIp = u4SelfIp | u4SwitchId;
    return u4SelfIp;
}

/******************************************************************************
 * Function           : RmRestoreStaticConfig
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to apply static configurations
******************************************************************************/
VOID
RmRestoreStaticConfig (VOID)
{
    MsrRmFileReadForSelfAttach ();
    return;
}

/******************************************************************************
 * Function           : RmGetFirstPeerEntry
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to get first peer entry in peer table
******************************************************************************/
INT4
RmGetFirstPeerEntry (tPeerRecEntry ** ppPeerEntry)
{
    *ppPeerEntry = (tPeerRecEntry *) TMO_SLL_First (RM_PEER_RECORD_SLL);
    if (*ppPeerEntry != NULL)
    {
        return RM_TRUE;
    }
    return RM_FALSE;
}

/******************************************************************************
 * Function           : RmGetPeerEntry
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to get peer entry in peer table corresponding 
 *                      to slot id
******************************************************************************/
INT4
RmGetPeerEntry (UINT4 u4SlotId, tPeerRecEntry ** ppPeerEntry)
{
    tPeerRecEntry      *pPeerEntry = NULL;

    TMO_SLL_Scan (RM_PEER_RECORD_SLL, pPeerEntry, tPeerRecEntry *)
    {
        if (pPeerEntry->u4SlotIndex == u4SlotId)
        {
            *ppPeerEntry = pPeerEntry;
            return RM_TRUE;
        }
    }
    *ppPeerEntry = NULL;
    return RM_FALSE;
}

/******************************************************************************
 * Function           : RmGetNextPeerEntry
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to get next peer entry in peer table  
 *                      
******************************************************************************/
INT4
RmGetNextPeerEntry (tPeerRecEntry * pPeerEntry, tPeerRecEntry ** pNextPeerEntry)
{
    tPeerRecEntry      *pTmpPeerEntry = NULL;

    pTmpPeerEntry = (tPeerRecEntry *)
        TMO_SLL_Next (RM_PEER_RECORD_SLL, &pPeerEntry->NextNode);
    if (pTmpPeerEntry != NULL)
    {
        *pNextPeerEntry = pTmpPeerEntry;
        return RM_TRUE;
    }
    *pNextPeerEntry = NULL;
    return RM_FALSE;
}

/******************************************************************************
 * Function           : RmAddPeerToPeerTable
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to add peer entry in peer table 
 *                      
******************************************************************************/
VOID
RmAddPeerToPeerTable (tPeerRecEntry * pPeerEntry)
{
    tPeerRecEntry      *pTmpPeerEntry = NULL;
    tPeerRecEntry      *pPrevPeerEntry = NULL;

    /* Entry addition is done in assending order of Aggregator Index */
    TMO_SLL_Scan (RM_PEER_RECORD_SLL, pTmpPeerEntry, tPeerRecEntry *)
    {
        if (pTmpPeerEntry->u4SlotIndex < pPeerEntry->u4SlotIndex)
        {
            pPrevPeerEntry = pTmpPeerEntry;
            continue;
        }
        else
        {
            break;
        }
    }
    TMO_SLL_Insert (RM_PEER_RECORD_SLL, &pPrevPeerEntry->NextNode,
                    &pPeerEntry->NextNode);
    return;
}

/******************************************************************************
 * Function           : RmDeleteFromPeerTable
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to delete peer entry in peer table 
 *                      
******************************************************************************/
VOID
RmDeleteFromPeerTable (UINT4 u4SlotId)
{
    tPeerRecEntry      *pPeerEntry;

    RmGetPeerEntry (u4SlotId, &pPeerEntry);
    if (pPeerEntry != NULL)
    {
        TMO_SLL_Delete (RM_PEER_RECORD_SLL, &pPeerEntry->NextNode);
        TMO_SLL_Init_Node (&pPeerEntry->NextNode);
        MemReleaseMemBlock (gRmInfo.RmPeerTblPoolId, (UINT1 *) pPeerEntry);
    }
    return;
}

/******************************************************************************
 * Function           : RmCreatePeerNodeEntry
 * Input(s)           : None
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to create peer entry in peer table 
 *                      
******************************************************************************/
INT4
RmCreatePeerNodeEntry (UINT4 u4SlotIndex, tPeerRecEntry ** ppRetPeerEntry)
{
    tPeerRecEntry      *pPeerEntry;
    UINT1               au1StackMac[RM_MAC_ADDR_LEN];

    MEMSET (au1StackMac, 0, sizeof (au1StackMac));

    /* Allocate a block for the aggregator entry */
    if ((pPeerEntry =
         (tPeerRecEntry *) MemAllocMemBlk (gRmInfo.RmPeerTblPoolId)) == NULL)
    {
        RM_TRC (RM_FAILURE_TRC,
                "Memalloc failed for peer entry in RmCreatePeerNodeEntry\n");
        return RM_FALSE;
    }

    MEMSET (pPeerEntry, 0, sizeof (tPeerRecEntry));
    TMO_SLL_Init_Node (&pPeerEntry->NextNode);

    pPeerEntry->u4StackIp = RmGetStackIpForSlot (u4SlotIndex);
    pPeerEntry->u4SlotIndex = u4SlotIndex;
#ifdef MBSM_WANTED
    RmMbsmNpGetStackMac (u4SlotIndex, au1StackMac);
#endif
    MEMCPY (pPeerEntry->au4StackMac, au1StackMac,
            (RM_MAC_ADDR_LEN * sizeof (UINT1)));

    /* au1SwitchBaseMac will be updated after getting SwitchInfo response */
    MEMSET (pPeerEntry->au1SwitchBaseMac, 0,
            (RM_MAC_ADDR_LEN * sizeof (UINT1)));
    *ppRetPeerEntry = pPeerEntry;
    return RM_TRUE;

}

/******************************************************************************
 * Function           : StackSendSwitchInfoReqMsg
 * Input(s)           : u4SlotId - attached remote slot number
 * Output(s)          : none
 * Returns            : None
 * Action             : Routine to form and send 'SwitchInfo' request to peers
 *                      
******************************************************************************/
VOID
StackSendSwitchInfoReqMsg (UINT4 u4SlotId)
{
    tRmPeerCtrlMsg      PeerInfo;
    UINT4               u4DestIp;

    MEMSET (&PeerInfo, 0, sizeof (tRmPeerCtrlMsg));

    /* Form PeerInfo Request Messge */
    PeerInfo.u4MsgType = OSIX_HTONL (STACK_SWITCH_INFO_REQ);
    PeerInfo.u4Switchid = u4SlotId;
    /* au1SwitchBaseMac will be filled by Peer */
    MEMSET (PeerInfo.au1SwitchBaseMac, 0, CFA_ENET_ADDR_LEN);
    PeerInfo.u1PrevState = 0;
    PeerInfo.u1CurrState = 0;

    /* Find attached peer stack IP address */
    u4DestIp = RmGetStackIpForSlot (u4SlotId);
    /* Send switchInfo Request mesage to attached peer */
    if (RmTcpSendMsg (u4DestIp, &PeerInfo) != RM_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg failed!!!!\n");
    }

    else
    {
        /* SwitchInfo response message should come within STACK_DEFAULT_SWITCH_INFO_INTERVAL
         * If Master receives response pkt, it will stop SwitchInfo timer 
         * otherwise after this switchInfo timer got expired,
         * PeerUp timer will be restarted to send SwitchInfo request message
         * again. If PeerUp timer got expired in second trial also means,
         * it will not update the port mac for that particular peer slot.*/
#ifdef MBSM_WANTED

        gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].u4SlotId = u4SlotId;
        gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].TmrRef.u1TimerType =
            STACK_SWITCH_INFO_TIMER;

        if (TmrStartTimer
            (gStackTmrLst,
             &gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].TmrRef.RefTimer,
             (STACK_DEFAULT_SWITCH_INFO_INTERVAL / NO_OF_MSECS_PER_UINT)) !=
            TMR_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "!!! Start SwitchInfo Timer "
                    " Failed !!!\n");
        }
#endif
    }
    return;

}

/******************************************************************************
 * Function           : StackProcessSwitchInfoMsg
 * Input(s)           : pRmData - Data received via rm socket
 *                      u4DataLen - size of the received packet
 *                      u4PeerAddr - Address of peer who send this packet 
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to process 'SwitchInfo' request/response packet
 *                      
******************************************************************************/
UINT4
StackProcessSwitchInfoMsg (tRmPeerCtrlMsg * PeerInfo, UINT4 u4DataLen,
                           UINT4 u4PeerAddr)
{
    tPeerRecEntry      *pPeerEntry;
    tStackNotificationMsg NotifMsg;
    UINT1               au1SwitchMac[RM_MAC_ADDR_LEN];

    UNUSED_PARAM (u4DataLen);
    MEMSET (au1SwitchMac, 0, (RM_MAC_ADDR_LEN));
    if ((PeerInfo->u4MsgType == STACK_SWITCH_INFO_REQ) &&
        (PeerInfo->u4Switchid == RM_GET_SWITCH_ID ()))
    {
        /* If the received msg is switchinfo request and also it belongs to the same switch means
         * send switchbase mac to Master Ip address */
        StackSendSwitchInfoRespMsg (u4PeerAddr);
    }

    if (PeerInfo->u4MsgType == STACK_SWITCH_INFO_RESP)
    {
        /* Update peer's switch base mac in  PeerTable and update 
         * port mac address for peer's slot */
        MEMCPY (au1SwitchMac, PeerInfo->au1SwitchBaseMac,
                RM_MAC_ADDR_LEN * sizeof (UINT1));
        TMO_SLL_Scan (RM_PEER_RECORD_SLL, pPeerEntry, tPeerRecEntry *)
        {
            if ((pPeerEntry->u4SlotIndex) == PeerInfo->u4Switchid)
            {
#ifdef MBSM_WANTED

                MEMCPY (pPeerEntry->au1SwitchBaseMac, au1SwitchMac,
                        RM_MAC_ADDR_LEN * sizeof (UINT1));
                IssCustGetSlotMACAddress (pPeerEntry->u4SlotIndex,
                                          pPeerEntry->au1SwitchBaseMac);
                /*Update port mac address for peer's slot only 
                 * if it has different base mac.This is done 
                 * to have unique port mac address*/
                if ((MEMCMP
                     (pPeerEntry->au1SwitchBaseMac,
                      gIssSysGroupInfo.BaseMacAddr, RM_MAC_ADDR_LEN) != 0)
                    &&
                    (MEMCMP
                     (pPeerEntry->au1SwitchBaseMac, gNullMacAdress,
                      RM_MAC_ADDR_LEN) != 0))
                {
                    CfaUpdatePortMacAddress (pPeerEntry->u4SlotIndex,
                                             PeerInfo->au1SwitchBaseMac);
                }
#endif
                break;
            }
        }
        /* SwithInfo timer should be stopped for a particular peer, 
         * who send SwitchInfo response packet */
        StackStopSwitchInfoTimer (PeerInfo->u4Switchid);
        /* To update gRmPeerFileInfo */
        if (IssGetRestoreFlagFromNvRam () == SET_FLAG)
        {
            /* When a new slot is getting attached, if restoration
             * is alredy happened means, master needs to send indication 
             * to that new slot */

            /* After getting SwitchInfo response from peer,
             * sending static config files to that peer */
            RmSendFilesToNewPeer (PeerInfo->u4Switchid);
        }

        /* To send Trap notification and syslog message
         * when peer slot's state changed from Master to Slave */
        if ((PeerInfo->u1PrevState == RM_ACTIVE)
            && (PeerInfo->u1CurrState == RM_STANDBY))
        {
            gRmInfo.u4TrapSwitchId = PeerInfo->u4Switchid;
            NotifMsg.u4NodeId = PeerInfo->u4Switchid;
            NotifMsg.u4State = STACK_NOTIFY_ACTIVE_TO_STANDBY;
            UtlGetTimeStr (NotifMsg.ac1DateTime);
            StackTrapSendNotifications (&NotifMsg);

        }
    }
    return RM_SUCCESS;

}

/******************************************************************************
 * Function           : StackSendSwitchInfoRespMsg
 * Input(s)           : u4PeerAddr - Response to be sent to this addr 
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to send 'SwitchInfo' response packet
 *                      
 ******************************************************************************/
VOID
StackSendSwitchInfoRespMsg (UINT4 u4PeerAddr)
{
    tRmPeerCtrlMsg      PeerInfo;

    MEMSET (&PeerInfo, 0, sizeof (tRmPeerCtrlMsg));

    /* Form SwitchInfo Request Messge */

    PeerInfo.u4MsgType = OSIX_HTONL (STACK_SWITCH_INFO_RESP);    /* Response pkt */
    PeerInfo.u4Switchid = RM_GET_SWITCH_ID ();    /* Fill self slot number */
    MEMSET (PeerInfo.au1SwitchBaseMac, 0, CFA_ENET_ADDR_LEN);    /* Fill by switchbase mac */

    /* Get SwitchBaseMac address in au1HwAddr */
    MEMCPY (PeerInfo.au1SwitchBaseMac, gIssSysGroupInfo.BaseMacAddr,
            CFA_ENET_ADDR_LEN);

    /* Slave switch - current & previous state */
    PeerInfo.u1PrevState = (UINT1) RM_GET_PREV_NODE_STATE ();
    PeerInfo.u1CurrState = (UINT1) RM_GET_NODE_STATE ();

    if (RmTcpSendMsg (u4PeerAddr, &PeerInfo) != RM_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg failed!!!!\n");
    }
    return;

}

/******************************************************************************
 * Function           : StackSendSlaveStatustoMaster
 * Input(s)           : u4ActiveIp - Response to be sent to this addr 
 *                      u4MsgType  - Messgae Type         
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to send firmware upgradation status.
 *                      
******************************************************************************/
VOID
StackSendSlaveStatustoMaster (UINT4 u4ActiveIP, UINT4 u4MsgType)
{
    tRmPeerCtrlMsg      PeerInfo;

    MEMSET (&PeerInfo, 0, sizeof (tRmPeerCtrlMsg));

    PeerInfo.u4MsgType = OSIX_HTONL (u4MsgType);    /* Response pkt */
    PeerInfo.u4Switchid = RM_GET_SWITCH_ID ();    /* Fill slave  slot number */
    MEMSET (PeerInfo.au1SwitchBaseMac, 0, CFA_ENET_ADDR_LEN);    /*  switchbase mac is initialized */

    /* Slave switch - current & previous state are initialized */
    PeerInfo.u1PrevState = 0;
    PeerInfo.u1CurrState = 0;

    if (RmTcpSendMsg (u4ActiveIP, &PeerInfo))
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg failed!!!!\n");
    }
    return;

}

/******************************************************************************
 * Function           : StackCreateTimer
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to create stack timer list.   
 ******************************************************************************/
VOID
StackCreateTimer (VOID)
{
#ifdef MBSM_WANTED
    if (TmrCreateTimerList ((CONST UINT1 *) RM_TASK_NAME,
                            STACK_TMR_EXP, NULL,
                            (tTimerListId *) & gStackTmrLst) != TMR_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "!!! RM Timer List creation Failure !!!\n");
    }
#endif

}

/******************************************************************************
 * Function           : StackHandleTimerExpiry
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle stack timers expiry.   
 ******************************************************************************/
VOID
StackHandleTimerExpiry (VOID)
{
#ifdef MBSM_WANTED
    tStackTimerRef     *pStackTimer = NULL;
    tStackTimer        *pStackPeerTimer = NULL;

    while ((pStackTimer =
            (tStackTimerRef *) TmrGetNextExpiredTimer (gStackTmrLst)) != NULL)
    {
        pStackPeerTimer = (tStackTimer *) pStackTimer;
        if (pStackTimer->u1TimerType == STACK_PEER_UP_TIMER)
        {
            /* if STACK_PEER_UP_TIMER expires, SwitchInfo request
             * message should be sent to peer */
            StackSendSwitchInfoReqMsg (pStackPeerTimer->u4SlotId);
        }
        else if (pStackTimer->u1TimerType == STACK_SWITCH_INFO_TIMER)
        {
            /* if STACK_SWITCH_INFO_TIMER expires, Master 
             * will wait for PEER_UP interval to send request
             * message again */
            StackRestartPeerUpTimer (pStackPeerTimer->u4SlotId);

        }
    }
#endif
}

/******************************************************************************
 * Function           : StackRestartPeerUpTimer
 * Input(s)           : u4SlotId - slot number.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to restart peer up timer.   
 ******************************************************************************/
VOID
StackRestartPeerUpTimer (UINT4 u4SlotId)
{
#ifdef MBSM_WANTED
    gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].u4SlotId = u4SlotId;
    gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].u1NumofRetries++;
    gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].TmrRef.u1TimerType =
        STACK_PEER_UP_TIMER;

    /* Master will send SwitchInfo request second time when,
     * STACK_SWITCH_INFO_TIMER expires. It will happen for only two times */
    if (gaStackTimer[(u4SlotId - MBSM_SLOT_INDEX_START)].u1NumofRetries <=
        STACK_MAX_RETRY_COUNT)
    {
        if (TmrStartTimer (gStackTmrLst,
                           &gaStackTimer[(u4SlotId -
                                          MBSM_SLOT_INDEX_START)].TmrRef.
                           RefTimer,
                           (STACK_DEFAULT_PEER_UP_INTERVAL /
                            NO_OF_MSECS_PER_UINT)) != TMR_SUCCESS)
        {
            RM_TRC (RM_FAILURE_TRC, "!!! Peer Up start "
                    " timer Failure !!!\n");
        }
    }
    else
    {
        RM_TRC1 (RM_FAILURE_TRC,
                 "!!Unable to get SwitchBase Mac from %ld slot!!\n", u4SlotId);
    }
    return;
#else
    UNUSED_PARAM (u4SlotId);
#endif
}

/*****************************************************************************
 * Function           : StackStopSwitchInfoTimer
 * Input(s)           : u4Switchid - slot num.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to stop SwitchInfo timer.   
 ******************************************************************************/

VOID
StackStopSwitchInfoTimer (UINT4 u4Switchid)
{
#ifdef MBSM_WANTED
    /* If SwitchInfo response received within STACK_DEFAULT_SWITCH_INFO_INTERVAL
     * means, stopping the 'SwitchInfo' timer */
    if (gaStackTimer[(u4Switchid - MBSM_SLOT_INDEX_START)].TmrRef.u1TimerType !=
        STACK_INVALID_TIMER)
    {
        if (TmrStopTimer (gStackTmrLst,
                          &gaStackTimer[(u4Switchid -
                                         MBSM_SLOT_INDEX_START)].TmrRef.
                          RefTimer) != TMR_SUCCESS)
        {
            RM_TRC1 (RM_FAILURE_TRC,
                     "Unable to stop SwitchInfo Timer -%ld slot\r\n",
                     u4Switchid);
        }
        gaStackTimer[(u4Switchid - MBSM_SLOT_INDEX_START)].TmrRef.u1TimerType =
            STACK_INVALID_TIMER;
    }
#else
    UNUSED_PARAM (u4Switchid);
#endif
}

#ifdef SNMP_2_WANTED
/******************************************************************************
* Function :   StackMakeObjIdFrmDotNew
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
StackMakeObjIdFrmDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0;
             ((pi1TempPtr < pi1DotPtr)
              && (u2Index < (SNMP_MAX_OID_LENGTH - 1))); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; fs_rm_mib_oid_table[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (fs_rm_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (fs_rm_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         fs_rm_mib_oid_table[u2Index].pNumber,
                         STRLEN (fs_rm_mib_oid_table[u2Index].pNumber));
                ai1TempBuffer[STRLEN (fs_rm_mib_oid_table[u2Index].pNumber)] =
                    '\0';

                break;
            }
        }

        if (fs_rm_mib_oid_table[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         (u2Index < SNMP_MAX_OID_LENGTH) && (ai1TempBuffer[u2Index] != '\0');
         u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) StackParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   StackParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : pu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
StackParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}
#endif /* SNMP_2_WANTED */

/******************************************************************************
 * Function Name      : StackTrapSendNotifications 
 *
 * Description        : This is the function used by RM to send notifications. 
 *                      RM in the active node sends the notification 
 *                      directly to the management application through 
 *                      fault management. RM in standby node informs 
 *                      the notification to the active node through 
 *                      RM notification message. 
 *
 * Input(s)           : pNotifyInfo - Pointer to the tStackNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
StackTrapSendNotifications (tStackNotificationMsg * pNotifyInfo)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      RmPrefixTrapOid;
    tSNMP_OID_TYPE      RmSuffixTrapOid;
    tSNMP_OID_TYPE     *pRmTrapOid;
    UINT4               au4RmPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4RmSuffixTrapOid[] = { 99, 0 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[RM_MAX_OID_LEN];
    UINT1               au1LogBuf[RM_MAX_STRING];

    RmPrefixTrapOid.pu4_OidList = au4RmPrefixTrapOid;
    RmPrefixTrapOid.u4_Length = sizeof (au4RmPrefixTrapOid) / sizeof (UINT4);

    RmSuffixTrapOid.pu4_OidList = au4RmSuffixTrapOid;
    RmSuffixTrapOid.u4_Length = sizeof (au4RmSuffixTrapOid) / sizeof (UINT4);

    pRmTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pRmTrapOid == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMSET (pRmTrapOid->pu4_OidList, 0, (SNMP_MAX_OID_LENGTH * sizeof (UINT4)));
    pRmTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&RmPrefixTrapOid, &RmSuffixTrapOid,
                              pRmTrapOid) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "RmTrapSendNotifications: Adding EnterpriseOid failed \n");
        SNMP_FreeOid (pRmTrapOid);
        return;
    }

    if (pNotifyInfo == NULL)
    {
        /* Send the pending traps. */
        SNMP_FreeOid (pRmTrapOid);
        return;
    }

    MEMSET (au1LogBuf, 0, sizeof (au1LogBuf));

    RM_TRC (RM_NOTIF_TRC | RM_SNMP_TRC, "RM sending notification !!!\n");

    SNPRINTF ((CHR1 *) au1LogBuf, sizeof (au1LogBuf), "Switch Id %u : %s at %s",
              pNotifyInfo->u4NodeId,
              gau1NodeState[(pNotifyInfo->u4State) - 1],
              pNotifyInfo->ac1DateTime);
    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gRmInfo.u4SysLogId, " %s ", au1LogBuf));

    MEMSET (au1Buf, 0, sizeof (au1Buf));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Memory Allocation Failed\n");
        SNMP_FreeOid (pRmTrapOid);
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMCPY (pEnterpriseOid->pu4_OidList, pRmTrapOid->pu4_OidList,
            pRmTrapOid->u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pRmTrapOid->u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = STACK_TRAP_EVENT;
    /* Memory freeing of pRmTrapOid */
    SNMP_FreeOid (pRmTrapOid);

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    0, 0, NULL, pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Variable Binding Failed\n");
        return;
    }

    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapSwitchId");

    pOid = StackMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Not Found:1\r\n");
        return;
    }

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    pNotifyInfo->u4NodeId = OSIX_NTOHL (pNotifyInfo->u4NodeId);
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, pNotifyInfo->u4NodeId, NULL,
                                                 NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Variable Binding Failed\r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapOperation");

    pOid = StackMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Not Found:2\r\n");
        return;
    }

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    STRNCPY (au1Buf, gau1NodeState[(pNotifyInfo->u4State) - 1],
             STRLEN (gau1NodeState[(pNotifyInfo->u4State) - 1]));
    au1Buf[STRLEN (gau1NodeState[(pNotifyInfo->u4State) - 1])] = '\0';
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Form Octet string Failed\n");
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                                  0, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Variable Binding Failed\r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fsRmTrapEventTime");

    pOid = StackMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: OID Not Found:3\r\n");
        return;
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pNotifyInfo->ac1DateTime,
                                         (INT4) STACK_DATE_TIME_STR_LEN);
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Form Octet string Failed\n");
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        RM_TRC (RM_FAILURE_TRC | RM_NOTIF_TRC,
                "StackTrapSendNotifications: Variable Binding Failed\r\n");
        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    /* Sending SYSLOG message */
    /* Sending  Trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pNotifyInfo);
#endif

    return;
}

/******************************************************************************
 * Function           : StackUpdateConfigurationFile
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine used to update the configurions in iss.conf 
 *                      file if present, while changing switchid.
******************************************************************************/
VOID
StackUpdateConfigurationFile (VOID)
{
    MsrUpdateStaticConfig (RM_GET_PREV_SWITCH_ID (), RM_GET_SWITCH_ID ());
}

/******************************************************************************
 * Function           : RmGetSwitchIdFromTskId
 * Input(s)           : u4TskId
 * Output(s)          : None
 * Returns            : None
 * Action             : To Get SwitchId from TaskId
******************************************************************************/
UINT4
RmGetSwitchIdFromTskId (UINT4 u4TskId)
{
    UINT4               u4SwitchId = 0;

    u4SwitchId = u4TskId / 5;
    return (u4SwitchId);
}

/******************************************************************************
 * Function           : RmGetSwitchIdFromTskId
 * Input(s)           : u4TskId
 * Output(s)          : None
 * Returns            : None
 * Action             : To Get SwitchId from TaskId
******************************************************************************/
UINT4
RmGetTaskIdFromSwitchId (UINT4 u4SwitchId)
{
    UINT4               u4TaskId = 0;

    u4TaskId = u4SwitchId * 5;
    return (u4TaskId);
}

/******************************************************************************
 * Function           : RmGetFileIndexFromTskId
 * Input(s)           : u4TskId
 * Output(s)          : None
 * Returns            : None
 * Action             : To Get FileIndex from TaskId
******************************************************************************/
INT4
RmGetFileIndexFromTskId (UINT4 u4TskId)
{
    INT4                i4FileId = 0;

    i4FileId = u4TskId % 5;
    return (i4FileId);
}

/******************************************************************************
 * Function           : RmTcpSendMsg
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine used to send msg for getting Peer Info in
 *                      coldstandby
******************************************************************************/
UINT4
RmTcpSendMsg (UINT4 u4DestIp, tRmPeerCtrlMsg * pPeerCtrlMsg)
{

    UINT1               u1SsnIndex;
    INT4                i4ConnFd = RM_INV_SOCK_FD;
    INT4                i4ConnState = 0;
    INT4                i4WrBytes = 0;

    if (RmGetSsnIndexFromPeerAddr (u4DestIp, &u1SsnIndex) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg: "
                "NO session for the peer \r\n");
        return (RM_FAILURE);
    }

    if (RmTcpSockConnect
        (RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr,
         &i4ConnFd, &i4ConnState) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg: "
                "TCP client connect failed \r\n");
        /* If connection failed, donot clear the queue, instead wait for
         * the peer to connect, if connection lost with peer,
         * HB task will intimate abot the peer dead
         * upon this event arrival clear the TX List */
        return RM_FAILURE;
    }
    if (RmTcpSockSend
        (i4ConnFd, (UINT1 *) pPeerCtrlMsg, sizeof (tRmPeerCtrlMsg),
         &i4WrBytes) != RM_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "RmTcpSendMsg: " "TCP Sock Send failed \r\n");
        close (i4ConnFd);
        return RM_FAILURE;
    }
    close (i4ConnFd);
    RM_GET_SSN_INFO (u1SsnIndex).u4PeerAddr = 0;
    RM_GET_SSN_INFO (u1SsnIndex).i4ConnFd = RM_INV_SOCK_FD;
    RM_GET_SSN_INFO (u1SsnIndex).i1ConnStatus = RM_SOCK_NOT_CONNECTED;
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmChangeStacktoStandaloneswitch
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine used to switchover from stack to 
 *                      standalone switch.
******************************************************************************/

VOID
RmChangeStacktoStandaloneswitch (VOID)
{
    /* In Standalone switch, stack port count is zero */
    if (nmhSetFsRmStackPortCount (ISS_DEFAULT_STACK_PORT_COUNT) == SNMP_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "Unable to set stack port count \r\n");
    }
    /* In Standalone switch, stack switchid is zero */
    if (nmhSetFsRmSwitchId (0) == SNMP_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "Unable to set switchid \r\n");
    }
    /* In Standalone switch, coldstandby state is disabled */
    if (nmhSetFsRmColdStandby (RM_COLDSTANDBY_DISABLE) == SNMP_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "Unable to disable coldstdby state \r\n");
    }
    return;
}
#endif
