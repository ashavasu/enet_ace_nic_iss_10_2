/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmcli.c,v 1.39 2015/11/06 08:21:08 siva Exp $
*
* Description: Action routines of RM module specific CLI commands
*********************************************************************/
#ifndef __RMCLI_C__
#define __RMCLI_C__

#include "rmincs.h"
#include "rmcli.h"
#include "fssnmp.h"
#include "fsrmcli.h"
#include "cfacli.h"
/* included for prototype decl of nmh routines */
#include "fsrmlw.h"
#include "fsrmwr.h"

/******************************************************************************
 * Function           : CliProcessRmCmd 
 * Input(s)           : CliHandle - CLI handler 
 *                      u4Command - command identifier
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function takes variable no. of arguments and
 *                      process the cli commands for RM module. 
 ******************************************************************************/
INT4
CliProcessRmCmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *au1Args[RM_CLI_MAX_ARGS];
    INT4                i4Argno = 0;
    UINT4               u4rc = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4HbInterval;
    INT4                i4PeerDeadInt;
    INT4                i4PeerDeadIntMultiplier;
    INT4                i4Val = 0;
    INT4                i4AppId = 0;
    va_start (ap, u4Command);

    while (1)
    {
        au1Args[i4Argno++] = va_arg (ap, UINT4 *);
        if (i4Argno == RM_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case RM_CLI_SHOW_CONF:
            u4rc = RmCliShowConf (CliHandle);
            break;

        case RM_CLI_SHOW_STATS:
            u4rc = RmCliShowStats (CliHandle);
            break;

        case RM_CLI_CLEAR_STATS:
            /* Clear all Tx and Rx statistics */
            RmUtilResetStatistics ();
            u4rc = CLI_SUCCESS;
            break;

        case RM_CLI_SET_HB_INT:

            if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
            {
                if (RmGetNodeState () == RM_ACTIVE)
                {
                    i4HbInterval = *(INT4 *) (au1Args[0]);

                    u4rc = RmCliSetHbInterval (CliHandle, i4HbInterval);
                    if (u4rc == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSet Heart-Beat Interval Failed\r\n");
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nHb-Interval configuration is not allowed for the node in STANDBY state.\r\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nHeart-Beat module is not enabled\r\n");
            }
            break;

        case RM_CLI_SET_PEER_DEAD_INT:

            if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
            {
                if (RmGetNodeState () == RM_ACTIVE)
                {
                    i4PeerDeadInt = *(INT4 *) (au1Args[0]);
                    u4rc = RmCliSetPeerDeadInterval (CliHandle, i4PeerDeadInt);
                    if (u4rc == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSet Peer Dead Interval Failed \r\n");
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nPeer-Dead interval configuration is not allowed for a node in STANDBY state.\r\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nHeart-Beat module is not enabled\r\n");
            }
            break;

        case RM_CLI_SET_RM_STACKING_IP_MASK:

            u4rc =
                RmCliSetStackingInterfaceParams (CliHandle, (INT4 *) au1Args[0],
                                                 (INT4 *) au1Args[1],
                                                 (INT4 *) au1Args[2]);
            break;

        case RM_CLI_SET_PEER_DEAD_INT_MULTI:

            if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
            {
                if (RmGetNodeState () == RM_ACTIVE)
                {
                    i4PeerDeadIntMultiplier = *(INT4 *) (au1Args[0]);
                    u4rc = RmCliSetPeerDeadIntMultiplier (CliHandle,
                                                          i4PeerDeadIntMultiplier);
                    if (u4rc == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSet Peer Dead Interval Multiplier "
                                   "Failed \r\n");
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nPeer-Dead interval multiplier configuration "
                               "is not allowed for a node in STANDBY state.\r\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nHeart-Beat module is not enabled\r\n");
            }
            break;

        case RM_CLI_SET_FSW:
            u4rc = RmCliForceSwitchOver (CliHandle);
            break;

        case RM_CLI_CHANGE_CFG_MODE:
            if (CliChangePath ((CHR1 *) RM_CLI_CFG_MODE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n Change to RM config mode -- Failed\r\n");
            }
            break;

        case RM_CLI_DEBUG:
            u4rc = RmCliSetDebugTrcLevel (CliHandle,
                                          CLI_PTR_TO_U4 (au1Args[0]), RM_TRUE);
            break;

        case RM_CLI_NO_DEBUG:
            u4rc = RmCliSetDebugTrcLevel (CliHandle,
                                          CLI_PTR_TO_U4 (au1Args[0]), RM_FALSE);
            break;

        case RM_CLI_MOD_DEBUG:
            u4rc = RmCliSetDebugModTrc (CliHandle,
                                        CLI_PTR_TO_U4 (au1Args[0]), RM_TRUE);
            break;

        case RM_CLI_MOD_NO_DEBUG:
            u4rc = RmCliSetDebugModTrc (CliHandle,
                                        CLI_PTR_TO_U4 (au1Args[0]), RM_FALSE);
            break;

        case RM_CLI_STACK:
            RmCliSetColdStandby (CliHandle, RM_COLDSTANDBY_ENABLE);
            if (au1Args[0] != 0)
            {
                u4rc = RmCliSetPriority (CliHandle, CLI_PTR_TO_I4 (au1Args[0]));
            }
            if (au1Args[1] != 0)
            {
                u4rc = RmCliSetSwitchId (CliHandle, *(INT4 *) (au1Args[1]));
            }
            if (au1Args[2] != 0)
            {
                u4rc = RmCliSetNumofStackPorts (CliHandle, *(au1Args[2]));
            }
            break;
        case RM_CLI_NO_STACK:
#ifdef MBSM_WANTED
            u4rc = RmCliSetNumofStackPorts (CliHandle,
                                            ISS_DEFAULT_STACK_PORT_COUNT);
#endif
            /* In Non-Stacking case, slot number should be zero */
            RmCliSetSwitchId (CliHandle, 0);
            RmCliSetColdStandby (CliHandle, RM_COLDSTANDBY_DISABLE);
            break;
        case RM_CLI_SHOW_LINK_STATUS:
            u4rc = RmCliShowStackLink (CliHandle);
            break;

        case RM_CLI_SHOW_STACK:

            i4Val = CLI_PTR_TO_I4 (au1Args[0]);
            if ((i4Val == RM_COLDSTANDBY_STACK_BRIEF) ||
                (i4Val == RM_COLDSTANDBY_STACK_DETAILS))
            {
                u4rc = RmCliShowStackDetails (CliHandle, i4Val);
            }
            if (i4Val == RM_COLDSTANDBY_STACK_COUNTERS)
            {
                u4rc = RmCliShowStackCounters (CliHandle);
            }
            if (au1Args[1] != 0)
            {
                u4rc = RmCliShowStackPeer (CliHandle, *(INT4 *) (au1Args[1]));
            }
            break;

#ifdef L2RED_TEST_WANTED
        case RM_TRIGGER_DYNAMIC_SYNC_AUDIT:
            i4AppId = CLI_PTR_TO_I4 (au1Args[0]);
            u4rc = RmCliStartDynSyncAudit (CliHandle, i4AppId);
            if (u4rc == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\rTriggering Dynamic Synch-up audit"
                           " Failed \r\n");
            }
            break;
        case RM_SHOW_DYN_AUDIT_STATUS:
            i4AppId = CLI_PTR_TO_I4 (au1Args[0]);
            u4rc = RmCliShowDynamicAuditStatus (CliHandle, i4AppId);
            if (u4rc == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\rFailed retrieving the status of Dynamic"
                           " Synch-up audit\r\n");
            }
            break;
#endif

        case RM_CLI_SET_RESTART_ENABLE:
            u4rc = RmCliSetRestartFlag (CliHandle, RM_RESTART_ENABLE);
            break;

        case RM_CLI_SET_RESTART_DISABLE:
            u4rc = RmCliSetRestartFlag (CliHandle, RM_RESTART_DISABLE);
            break;

        case RM_CLI_SET_RESTART_COUNT:
            u4rc = RmCliSetRestartCount (CliHandle,
                                         CLI_PTR_TO_U4 (*(au1Args[0])));
            break;
/* HITLESS RESTART */
        case RM_CLI_HR_ENABLE:
            u4rc = RmCliSetHRStatus (CliHandle, RM_HR_STATUS_STORE);
            break;

        case RM_CLI_SET_INITIATE:
            u4rc = RmCliSetInitiateCopy (CliHandle, RM_COPY_STATUS_INITIATE);
            break;

        case RM_CLI_SHOW_SWOVER_TIME:

            i4AppId = CLI_PTR_TO_I4 (au1Args[0]);
            u4rc = RmCliShowSwitchOverTime (CliHandle, i4AppId);
            break;

        default:
            CliPrintf (CliHandle, "\r\nUnknown command\r\n");
            u4rc = 0;
            break;
    }

    if ((u4rc == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", RmCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (u4rc);

    return ((INT4) u4rc);
}

/******************************************************************************
 * Function           : RmShowRunningConfig
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function displays the redundancy manager
 *                       current configuration details.
 ******************************************************************************/
UINT4
RmShowRunningConfig (tCliHandle CliHandle)
{
    UINT1               u1Flag = 0;
    INT4                i4RetVal;
    UINT4               u4RetVal = 0;

    nmhGetFsRmProtocolRestartFlag (&i4RetVal);

    if (i4RetVal == RM_RESTART_ENABLE)
    {
        if (u1Flag == 0)
        {
            CliPrintf (CliHandle, "redundancy\r\n");
        }
        CliPrintf (CliHandle, "abort protocol restart enable\r\n");
        u1Flag = 1;
    }

    nmhGetFsRmProtocolRestartRetryCnt (&u4RetVal);
    if (u4RetVal != RM_DEF_RESTART_CNT)
    {
        if (u1Flag == 0)
        {
            CliPrintf (CliHandle, "redundancy\r\n");
        }
        CliPrintf (CliHandle, "abort protocol restart retry count %d \r\n",
                   u4RetVal);
        u1Flag = 1;
    }
    if (u1Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/******************************************************************************
 * Function           : RmCliShowConf 
 * Input(s)           : CliHandle - CLI handler 
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function displays the redundancy manager
 *                      configuration details.
 ******************************************************************************/
UINT4
RmCliShowConf (tCliHandle CliHandle)
{
    INT4                i4RetVal;
    INT4                i4StackPortCount = 0;
    INT4                i4SwitchId = 0;
    UINT4               u4rc = CLI_FAILURE;
    UINT1               au1Output[25];
    tSNMP_OCTET_STRING_TYPE NodeId;
    tSNMP_OCTET_STRING_TYPE SelfNodeId;
    UINT4               u4NodeId;
    tUtlInAddr          RmIpAddr;
    UINT4               u4SelfIp;
    UINT1               u1ActiveNodeId[sizeof (UINT4)];
    UINT1               au1SelfNodeId[sizeof (UINT4)];

    MEMSET (u1ActiveNodeId, 0, sizeof (u1ActiveNodeId));
    MEMSET (au1SelfNodeId, 0, sizeof (au1SelfNodeId));

    CliPrintf (CliHandle,
               "\r\n   Redundancy Manager Configuration details\r\n");
    CliPrintf (CliHandle, "   ----------------------------------------\r\n");

    NodeId.i4_Length = sizeof (UINT4);
    NodeId.pu1_OctetList = &u1ActiveNodeId[0];
    if (NodeId.pu1_OctetList == NULL)
    {
        u4rc = CLI_FAILURE;
        return (u4rc);
    }

    SelfNodeId.i4_Length = sizeof (UINT4);
    SelfNodeId.pu1_OctetList = &au1SelfNodeId[0];
    if (nmhGetFsRmSelfNodeId (&SelfNodeId) == SNMP_SUCCESS)
    {
        PTR_FETCH4 (u4SelfIp, SelfNodeId.pu1_OctetList);
        RmIpAddr.u4Addr = OSIX_NTOHL (u4SelfIp);
        CliPrintf (CliHandle, "Self NodeId: %s\r\n", INET_NTOA (RmIpAddr));
        u4rc = CLI_SUCCESS;
    }

    if (u4rc == CLI_SUCCESS)
    {
        if (nmhGetFsRmPeerNodeId (&NodeId) == SNMP_SUCCESS)
        {
            PTR_FETCH4 (u4NodeId, NodeId.pu1_OctetList);
            RmIpAddr.u4Addr = OSIX_NTOHL (u4NodeId);
            CliPrintf (CliHandle, "Peer NodeId: %s\r\n", INET_NTOA (RmIpAddr));
            u4rc = CLI_SUCCESS;
        }
    }

    if (u4rc == CLI_SUCCESS)
    {
        if (nmhGetFsRmActiveNodeId (&NodeId) == SNMP_SUCCESS)
        {
            PTR_FETCH4 (u4NodeId, NodeId.pu1_OctetList);
            RmIpAddr.u4Addr = OSIX_NTOHL (u4NodeId);
            CliPrintf (CliHandle, "Active NodeId: %s\r\n",
                       INET_NTOA (RmIpAddr));
            u4rc = CLI_SUCCESS;
        }
    }

    if (u4rc == CLI_SUCCESS)
    {
        if (nmhGetFsRmNodeState (&i4RetVal) == SNMP_SUCCESS)
        {
            u4rc = CLI_SUCCESS;
            CliPrintf (CliHandle, "Node Status: ");
            RmCliFormatNodeState (au1Output, i4RetVal);
            CliPrintf (CliHandle, "%s\r\n", au1Output);
        }
    }

    if (u4rc == CLI_SUCCESS)
    {
        if (nmhGetFsRmStackPortCount (&i4StackPortCount) == SNMP_SUCCESS)
        {
            u4rc = CLI_SUCCESS;
            CliPrintf (CliHandle, "Stack ports: ");
            CliPrintf (CliHandle, "%d\r\n", i4StackPortCount);
        }

    }

    if (u4rc == CLI_SUCCESS)
    {
        if (nmhGetFsRmSwitchId (&i4SwitchId) == SNMP_SUCCESS)
        {
            u4rc = CLI_SUCCESS;
            CliPrintf (CliHandle, "Switch Id: ");
            CliPrintf (CliHandle, "%d\r\n", i4SwitchId);
        
        }
    
    }

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        if (u4rc == CLI_SUCCESS)
        {
            if (nmhGetFsRmHbInterval (&i4RetVal) == SNMP_SUCCESS)
            {
                u4rc = CLI_SUCCESS;
                CliPrintf (CliHandle, "HeartBeat Interval: %d msecs\r\n",
                           i4RetVal);
            }
        }

        if (u4rc == CLI_SUCCESS)
        {
            if (nmhGetFsRmPeerDeadInterval (&i4RetVal) == SNMP_SUCCESS)
            {
                u4rc = CLI_SUCCESS;
                CliPrintf (CliHandle, "Peer Dead Interval : %d msecs\r\n",
                           i4RetVal);
            }
        }

        if (u4rc == CLI_SUCCESS)
        {
            if (nmhGetFsRmPeerDeadIntMultiplier (&i4RetVal) == SNMP_SUCCESS)
            {
                u4rc = CLI_SUCCESS;
                CliPrintf (CliHandle, "Peer Dead Interval Multiplier: %d\r\n",
                           i4RetVal);
            }
        }
    }

/* HITLESS RESTART */
    if (nmhGetFsRmHitlessRestartFlag (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal == RM_HR_STATUS_DISABLE)
        {
            u4rc = CLI_SUCCESS;
            CliPrintf (CliHandle, "Hitless Restart Disabled \r\n");
        }

        else if ((i4RetVal == RM_HR_STATUS_STORE) ||
                 (i4RetVal == RM_HR_STATUS_RESTORE))
        {
            u4rc = CLI_SUCCESS;
            CliPrintf (CliHandle, "Hitless Restart Enabled \r\n");
        }
    }
    return (u4rc);
}

/******************************************************************************
 * Function           : RmCliShowStats 
 * Input(s)           : CliHandle - CLI handler 
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function displays the redundancy manager
 *                      statistics.
 ******************************************************************************/
UINT4
RmCliShowStats (tCliHandle CliHandle)
{
    UINT4               u4SyncMsgTxCount = 0;
    UINT4               u4SyncMsgTxFailedCount = 0;
    UINT4               u4SyncMsgRxCount = 0;
    UINT4               u4SyncMsgProcCount = 0;
    UINT4               u4SyncMsgMissedCount = 0;
    UINT4               u4SyncMsgStaticConfFailCount = 0;

    if (nmhGetFsRmStatsSyncMsgTxCount (&u4SyncMsgTxCount) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRmStatsSyncMsgTxFailedCount (&u4SyncMsgTxFailedCount)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRmStatsSyncMsgRxCount (&u4SyncMsgRxCount) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRmStatsSyncMsgProcCount (&u4SyncMsgProcCount) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRmStatsSyncMsgMissedCount (&u4SyncMsgMissedCount)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRmStatsConfSyncMsgFailCount (&u4SyncMsgStaticConfFailCount)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nTX Statistics\r\n");
    CliPrintf (CliHandle, "-------------\r\n");

    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Sync-up message transmitted", u4SyncMsgTxCount);
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Sync-up message transmission failed", u4SyncMsgTxFailedCount);

    CliPrintf (CliHandle, "\r\nRX Statistics\r\n");
    CliPrintf (CliHandle, "-------------\r\n");

    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Sync-up message received", u4SyncMsgRxCount);
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Sync-up message processed", u4SyncMsgProcCount);
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Sync-up message missed", u4SyncMsgMissedCount);
    CliPrintf (CliHandle, "%-40s : %-5d\n",
               "Static configuration sync-up failed",
               u4SyncMsgStaticConfFailCount);
    CliPrintf (CliHandle, "\n");

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliFormatNodeState 
 * Input(s)           : pu1Output - String to be printed
 *                      i4NodeState - State of the node
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function is used to format the ouput
 *                      of the cli command "show redundancy"
 ******************************************************************************/
VOID
RmCliFormatNodeState (UINT1 *pu1Output, INT4 i4NodeState)
{
    switch (i4NodeState)
    {
        case 0:
            STRCPY (pu1Output, "Init");
            break;
        case 1:
            STRCPY (pu1Output, "Active");
            break;
        case 2:
            STRCPY (pu1Output, "Standby");
            break;
        case 3:
            STRCPY (pu1Output, "Transition_In_Progress");
            break;
        default:
            break;
    }
}

/******************************************************************************
 * Function           : RmCliSetHbInterval 
 * Input(s)           : CliHandle - CLI handle 
 *                      i4HbInt - Heart-beat interval to be set
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function sets the heart-beat interval.
 ******************************************************************************/
UINT4
RmCliSetHbInterval (tCliHandle CliHandle, INT4 i4HbInt)
{
    UINT4               u4RetVal = CLI_SUCCESS;
    UINT4               u4ErrCode;

    if (nmhTestv2FsRmHbInterval (&u4ErrCode, i4HbInt) == SNMP_SUCCESS)
    {
        if (nmhSetFsRmHbInterval (i4HbInt) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            u4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Heart-Beat interval\r\n");
        u4RetVal = CLI_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmCliSetPeerDeadInterval 
 * Input(s)           : CliHandle - CLI handle 
 *                      i4PeerDeadInt - Peer-Dead interval to be set
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function sets the peer-dead interval.
 ******************************************************************************/
UINT4
RmCliSetPeerDeadInterval (tCliHandle CliHandle, INT4 i4PeerDeadInt)
{
    UINT4               u4RetVal = CLI_SUCCESS;
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRmPeerDeadInterval (&u4ErrCode, i4PeerDeadInt)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRmPeerDeadInterval (i4PeerDeadInt) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            u4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Peer-Dead interval\r\n");
        u4RetVal = CLI_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmCliSetStackingInterfaceParams
 * Input(s)           : CliHandle - CLI handle
 *                      pi4IpAddress - RM Stacking IP Address
 *                      pi4Mask - RM Stack ip mask
 *                      pi4IntfNum   RM Stacking Interface
 * Output(s)          : None.
 * Returns            : None.
 ******************************************************************************/
UINT4
RmCliSetStackingInterfaceParams (tCliHandle CliHandle, INT4 *pi4IpAddress,
                                 INT4 *pi4IpMask, INT4 *pi4IntfNum)
{
    tSNMP_OCTET_STRING_TYPE SetValFsRmStackInterface;
    UINT1               au1StackingInterface[ISS_RM_INTERFACE_LEN];
    UINT4               u4ErrCode = 0;

    if ((pi4IpAddress != NULL)
        && (nmhTestv2FsRmIpAddress (&u4ErrCode, (UINT4) *pi4IpAddress) !=
            SNMP_SUCCESS))
    {
        CliPrintf (CliHandle, "Invalid IP Address\r\n");
        return CLI_FAILURE;
    }
    if ((pi4IpMask != NULL)
        && (nmhTestv2FsRmSubnetMask (&u4ErrCode, (UINT4) *pi4IpMask) !=
            SNMP_SUCCESS))
    {
        CliPrintf (CliHandle, "Invalid IP Mask\r\n");
        return CLI_FAILURE;
    }
    if ((pi4IntfNum != NULL) && (*pi4IntfNum != 1))
    {
        CliPrintf (CliHandle, "Only One Stacking Interface is supported\r\n");
        return CLI_FAILURE;
    }
    if (pi4IpAddress != NULL)
    {
        nmhSetFsRmIpAddress ((UINT4) *pi4IpAddress);
    }
    if (pi4IpMask != NULL)
    {
        nmhSetFsRmSubnetMask ((UINT4) *pi4IpMask);
    }
    if (pi4IntfNum != NULL)
    {
        MEMSET (&SetValFsRmStackInterface, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1StackingInterface[0], 0, ISS_RM_INTERFACE_LEN);
        SNPRINTF ((CHR1 *) au1StackingInterface, ISS_RM_INTERFACE_LEN, "%s",
                  "STK0");
        SetValFsRmStackInterface.pu1_OctetList = &au1StackingInterface[0];
        SetValFsRmStackInterface.i4_Length =
            (INT4) STRLEN (au1StackingInterface);
        nmhSetFsRmStackInterface (&SetValFsRmStackInterface);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetPeerDeadIntMultiplier
 * Input(s)           : CliHandle - CLI handle 
 *                      i4PeerDeadIntMultiplier - Peer-Dead interval 
 *                                                multiplier to be set
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function sets the peer-dead interval multiplier.
 ******************************************************************************/
UINT4
RmCliSetPeerDeadIntMultiplier (tCliHandle CliHandle,
                               INT4 i4PeerDeadIntMultiplier)
{
    UINT4               u4RetVal = CLI_SUCCESS;
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRmPeerDeadIntMultiplier (&u4ErrCode, i4PeerDeadIntMultiplier)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRmPeerDeadIntMultiplier (i4PeerDeadIntMultiplier)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            u4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Peer-Dead interval multiplier\r\n");
        u4RetVal = CLI_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmCliForceSwitchOver 
 * Input(s)           : CliHandle - CLI handle 
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function conducts a manual switch-over from
 *                      ACTIVE to STANDBY state.
 ******************************************************************************/
UINT4
RmCliForceSwitchOver (tCliHandle CliHandle)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsRmForceSwitchoverFlag (&u4ErrCode, RM_ENABLE)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmForceSwitchoverFlag (RM_ENABLE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetDebugTrcLevel
 * Input(s)           : CliHandle - CLI handle 
 *                      u4TrcLevel - Debug trace level
 *                      u1SetFlag - Indicates Set(RM_TRUE)/Reset(RM_FALSE)
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function is used to set/reset the debug trace
 *                      level.
 ******************************************************************************/
UINT4
RmCliSetDebugTrcLevel (tCliHandle CliHandle, UINT4 u4TrcLevel, UINT1 u1SetFlag)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4CurTrcLevel = 0;

    if (nmhTestv2FsRmTrcLevel (&u4ErrCode, u4TrcLevel) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsRmTrcLevel (&u4CurTrcLevel) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1SetFlag == RM_TRUE)
    {
        u4CurTrcLevel |= u4TrcLevel;
    }
    else if (u1SetFlag == RM_FALSE)
    {
        u4CurTrcLevel &= (~u4TrcLevel);
    }

    if (nmhSetFsRmTrcLevel (u4CurTrcLevel) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetDebugModTrc
 * Input(s)           : CliHandle - CLI handle 
 *                      u4ModTrc  - RM Module Debug trace
 *                      u1SetFlag - RM_TRUE -> Debug enable
 *                                  RM_FALSE -> Debug disable
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function is used to set/reset the debug trace
 *                      level.
 ******************************************************************************/
UINT4
RmCliSetDebugModTrc (tCliHandle CliHandle, UINT4 u4ModTrc, UINT1 u1SetFlag)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4CurModTrc = u4ModTrc;

    if (nmhTestv2FsRmModuleTrc (&u4ErrCode, u4ModTrc) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Debug enabling case, need to provide complete modules to be 
     * enabled instead of one by one. 
     *
     * Debug disabling case, modules can be given one by one to remove 
     * from module trace. */

    if (nmhGetFsRmModuleTrc (&u4CurModTrc) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (u1SetFlag == RM_FALSE)
    {
        u4CurModTrc &= (~u4ModTrc);
    }
    else
    {
        u4CurModTrc |= u4ModTrc;
    }

    if (nmhSetFsRmModuleTrc (u4CurModTrc) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliGetConfigPrompt 
 * Input(s)           : pi1ModeName - Cli mode name 
 *                      pi1DispStr - display string
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This function is used to get the prompt for display.
 ******************************************************************************/
INT1
RmCliGetConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        return FALSE;
    }

    u4Len = STRLEN (RM_CLI_CFG_MODE);

    if (STRNCMP (pi1ModeName, RM_CLI_CFG_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    STRCPY (pi1DispStr, RM_CLI_CFG_MODE);
    return TRUE;
}

#ifdef L2RED_TEST_WANTED
VOID
RmTestCliSetProtoAck (INT4 i4SetVal)
{
    RmTestSetProtoAck (i4SetVal);
}

VOID
RmTestCliSetSeqMiss (INT4 i4SetVal)
{
    RmTestSetSeqMiss (i4SetVal);
}

VOID
RmTestCliShowDebuggingInfo (tCliHandle CliHandle)
{
    RmTestShowDebuggingInfo (CliHandle);
}

VOID
RmTestCliSetProtoSyncBlock (UINT4 u4AppId, UINT4 u4BlockFlg)
{
    RmTestSetProtoSyncBlock (u4AppId, u4BlockFlg);
}

#endif
/******************************************************************************
 * Function           : RmCliShowStackLink
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function displays the redundancy link status.
 ******************************************************************************/
UINT4
RmCliShowStackLink (tCliHandle CliHandle)
{
#ifdef MBSM_WANTED
    UINT1               u1Port = 1;
    INT4                i4OperStat;
    INT4                i4IfIndex;
    CliPrintf (CliHandle, "\r\nPort      Status\r\n");
    CliPrintf (CliHandle, "----      ------   \r\n");

    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        CliPrintf (CliHandle, "Switch is in slave state\r\n");
        return CLI_SUCCESS;
    }

    for (i4IfIndex = CFA_STACK_PORT_START_INDEX;
         i4IfIndex <= MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS; i4IfIndex++)
    {
        nmhGetIfOperStatus (i4IfIndex, (INT4 *) &i4OperStat);
        CliPrintf (CliHandle, "%-11d", u1Port++);

        if (i4OperStat == CFA_IF_UP)
        {
            CliPrintf (CliHandle, "%-11s\n", "up");
        }
        else if (i4OperStat == CFA_IF_DOWN)
        {
            CliPrintf (CliHandle, "%-11s\n", "down");
        }
        else if (i4OperStat == CFA_IF_NP)
        {
            CliPrintf (CliHandle, "%-11s\n", "not present");
        }

    }
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetSwitchId
 * Input(s)           : CliHandle - CLI handler
                        i4SwitchId -Stack switchid 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to modify the switchid
 ******************************************************************************/
UINT4
RmCliSetSwitchId (tCliHandle CliHandle, INT4 i4SwitchId)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRmSwitchId (&u4ErrCode, i4SwitchId) == SNMP_FAILURE)
    {
#ifdef MBSM_WANTED
        CliPrintf (CliHandle, "%% Stacking is enabled only for %d slots\r\n",
                   (MBSM_MAX_SLOTS - 1));
#endif
        return CLI_FAILURE;
    }

    if (nmhSetFsRmSwitchId (i4SwitchId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetPriority    
 * Input(s)           : CliHandle - CLI handler
                        i4Prioroty - Priority value 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to modify the priority value.
 ******************************************************************************/

UINT4
RmCliSetPriority (tCliHandle CliHandle, INT4 i4Priority)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRmConfiguredState (&u4ErrCode, i4Priority) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmConfiguredState (i4Priority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliShowStackDetails    
 * Input(s)           : CliHandle - CLI handler
                        i4Val - BRIEF/DETAILS                      
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to show stack details.
 ******************************************************************************/
UINT4
RmCliShowStackDetails (tCliHandle CliHandle, INT4 i4Val)
{
#ifdef MBSM_WANTED
    tSNMP_OCTET_STRING_TYPE *pName;
    tSNMP_OCTET_STRING_TYPE SelfNodeId;
    UINT4               u4StackIp;
    INT4                i4StackPortCount;
    UINT4               u4SelfIp;
    tMacAddr            pMacAddr;
    UINT1              *pu1Temp = NULL;
    UINT1              *pu1Tmp = NULL;
    INT4                i4PrevSwitchId;
    UINT1               au1Temp[RM_MAC_ADDR_LEN];
    UINT1               au1IpId[MAX_LEN];
    UINT1               au1SelfNodeId[sizeof (UINT4)];
    INT4                i4SwitchState;
    INT4                i4SwitchId;
    INT4                i4ConfiguredState;
    INT4                i4Status;
    INT2                i2CardNameLen = 0;
    INT1                ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    CHR1               *pu1String = NULL;
    tUtlInAddr          RmIpAddr;
    /* Check if stacking is enabled */
    if (RmCliIsStackEnable (CliHandle) == CLI_FAILURE)
    {
        /* Stacking commands should not display any values when 
           stacking is disabled */
        /* return FAILURE */
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n Self Status\r\n");
    CliPrintf (CliHandle, "------------\r\n");
    MEMSET (au1SelfNodeId, 0, sizeof (au1SelfNodeId));
    SelfNodeId.i4_Length = sizeof (UINT4);
    SelfNodeId.pu1_OctetList = &au1SelfNodeId[0];

    MEMSET (pMacAddr, 0, RM_MAC_ADDR_LEN);
    MEMSET (au1Temp, 0, RM_MAC_ADDR_LEN);
    MEMSET (au1IpId, 0, MAX_LEN);
    pu1Temp = &au1Temp[0];
    pu1Tmp = &pMacAddr[0];
    pu1String = (CHR1 *) & au1IpId[0];

    if (nmhGetFsRmStackPortCount (&i4StackPortCount) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Stack ports      : %d\r\n", i4StackPortCount);
    }

    if (nmhGetFsRmSwitchId (&i4SwitchId) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Switch Id        : %d\r\n", i4SwitchId);
    }

    if (i4Val == RM_COLDSTANDBY_STACK_DETAILS)
    {
        if (nmhGetFsRmSelfNodeId (&SelfNodeId) == SNMP_SUCCESS)
        {
            PTR_FETCH4 (u4SelfIp, SelfNodeId.pu1_OctetList);
            RmIpAddr.u4Addr = OSIX_NTOHL (u4SelfIp);
            CliPrintf (CliHandle, "Stack Ip         : %s\r\n",
                       INET_NTOA (RmIpAddr));
        }

        if (nmhGetFsRmStackMacAddr (&pMacAddr) == SNMP_SUCCESS)
        {
            PrintMacAddress (pMacAddr, pu1Temp);
            CliPrintf (CliHandle, "Stack Mac        : %s\r\n", pu1Temp);
        }
    }

    if (nmhGetFsRmConfiguredState (&i4ConfiguredState) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Configured state : ");
        switch (i4ConfiguredState)
        {
            case ISS_PREFERRED_MASTER:
                CliPrintf (CliHandle, "PM\n");
                break;
            case ISS_BACKUP_MASTER:
                CliPrintf (CliHandle, "BM\n");
                break;
            case ISS_PREFERRED_SLAVE:
                CliPrintf (CliHandle, "PS\n");
                break;

        }
    }

    if (nmhGetFsRmNodeState (&i4SwitchState) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Current State    : ");
        switch (i4SwitchState)
        {
            case 0:
                CliPrintf (CliHandle, "Init\n\n");
                break;
            case 1:
                CliPrintf (CliHandle, "Master\n\n");
                break;
            case 2:
                CliPrintf (CliHandle, "slave\n\n");
                break;
        }
    }

    pName = allocmem_octetstring (MBSM_MAX_CARD_NAME_LEN + 1);
    if (pName == NULL)
    {
        return CLI_FAILURE;
    }
    pName->i4_Length = MBSM_MAX_CARD_NAME_LEN;

    CliPrintf (CliHandle, "\r\n Peer Status\r\n");
    CliPrintf (CliHandle, "------------\r\n");

    if (i4SwitchState == RM_ACTIVE)
    {
        if (i4Val == RM_COLDSTANDBY_STACK_DETAILS)
        {
            CliPrintf (CliHandle, "SwitchId    StackIP          StackMac"
                       "        SwitchState     CardName\n");

            CliPrintf (CliHandle, "--------    -------          --------"
                       "        -----------     --------\n");
        }
        else
        {
            CliPrintf (CliHandle, "SwitchId    SwitchState     CardName\n");

            CliPrintf (CliHandle, "--------    -----------     --------\n");

        }
        if (nmhGetFirstIndexFsRmPeerTable (&i4SwitchId) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        do
        {

            if (i4Val == RM_COLDSTANDBY_STACK_DETAILS)
            {
                nmhGetFsRmPeerStackIpAddr (i4SwitchId, &u4StackIp);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StackIp);
                nmhGetFsRmPeerStackMacAddr (i4SwitchId, &pMacAddr);
                PrintMacAddress (pMacAddr, pu1Temp);
                CliPrintf (CliHandle, "%d         %s      %s",
                           i4SwitchId, pu1String, pu1Temp);
            }
            else
            {
                CliPrintf (CliHandle, "%-12d ", i4SwitchId);
            }
            if (nmhGetMbsmLCConfigCardName (i4SwitchId, pName) == SNMP_FAILURE)
            {
                i2CardNameLen = 0;
                i4Status = MBSM_STATUS_NP;
            }
            else
            {
                i2CardNameLen =
                    (INT2) ((pName->i4_Length <
                             (MBSM_MAX_CARD_NAME_LEN -
                              1)) ? pName->i4_Length : (MBSM_MAX_CARD_NAME_LEN -
                                                        1));
            }

            if (i2CardNameLen > 0)
            {
                MEMCPY (ai1CardName, pName->pu1_OctetList, i2CardNameLen);
            }
            ai1CardName[i2CardNameLen] = '\0';

            if (nmhGetMbsmSlotModuleStatus (i4SwitchId, &i4Status)
                == SNMP_FAILURE)
            {
                i4Status = MBSM_STATUS_INACTIVE;
            }
            switch (i4Status)
            {
                case MBSM_STATUS_ACTIVE:
                    CliPrintf (CliHandle, "%-13s ", " UP");
                    break;

                case MBSM_STATUS_NP:
                    CliPrintf (CliHandle, "%-13s ", " NP");
                    break;

                case MBSM_STATUS_DOWN:
                    CliPrintf (CliHandle, "%-13s ", " DOWN");
                    break;

                case MBSM_STATUS_INACTIVE:
                    CliPrintf (CliHandle, "%-13s ", " INACTIVE");
                    break;

                default:
                    CliPrintf (CliHandle, "%-13s ", " UNKNOWN");
                    break;
            }
            CliPrintf (CliHandle, "%-s\r\n", ai1CardName);
            i4PrevSwitchId = i4SwitchId;
        }
        while (nmhGetNextIndexFsRmPeerTable (i4PrevSwitchId, &i4SwitchId)
               == SNMP_SUCCESS);
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Val);
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetVlanId
 * Input(s)           : CliHandle - CLI handler
                        i4StackVlanId  - Stack VlanId 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set Stack VlanId
 ******************************************************************************/
UINT4
RmCliSetVlanId (tCliHandle CliHandle, INT4 i4StackVlanId)
{
    UNUSED_PARAM (i4StackVlanId);
    CliPrintf (CliHandle,
               "%% Stack VlanID option is currently not supported\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetStackIP
 * Input(s)           : CliHandle - CLI handler
                        u4StackIpAddr  - Stack IP Address 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set stack Ip address
 ******************************************************************************/
UINT4
RmCliSetStackIP (tCliHandle CliHandle, UINT4 u4StackIpAddr)
{
    UNUSED_PARAM (u4StackIpAddr);
    CliPrintf (CliHandle,
               "%% Stack IP address option is currently not supported\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetNumofStackPorts
 * Input(s)           : CliHandle - CLI handler
                        u4NumofStackPort - Number of stack ports 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set num of stack ports
 ******************************************************************************/
UINT4
RmCliSetNumofStackPorts (tCliHandle CliHandle, UINT4 u4NumofStackPort)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRmStackPortCount (&u4ErrorCode, (INT4) u4NumofStackPort)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmStackPortCount ((INT4) u4NumofStackPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetColdStandby
 * Input(s)           : CliHandle - CLI handler
                        u4ColdStandby-enable/disable
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set stack cold stand by mode.
 ******************************************************************************/
UINT4
RmCliSetColdStandby (tCliHandle CliHandle, UINT4 u4ColdStandby)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRmColdStandby (&u4ErrorCode, (INT4) u4ColdStandby)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmColdStandby ((INT4) u4ColdStandby) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Alert: Stack parameters will be "
               "effective only after Reload\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliShowStackCounters
 * Input(s)           : CliHandle - CLI handler
                 
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : Function to display counter information of stack ports
 ******************************************************************************/
UINT4
RmCliShowStackCounters (tCliHandle CliHandle)
{
#ifdef MBSM_WANTED
    tSNMP_COUNTER64_TYPE IfHCInOctets;
    tSNMP_COUNTER64_TYPE IfHCOutOctets;
    INT1                u1Port = 1;
    INT4                i4IfIndex;
    FS_UINT8            u8IfHCInOctets;
    FS_UINT8            u8IfHCOutOctets;
    UINT4               u4InOctects;
    UINT4               u4InUcastPkts;
    UINT4               u4InDiscards;
    UINT4               u4InErrors;
    UINT4               u4OutOctects;
    UINT4               u4OutUcastPkts;
    UINT4               u4OutDiscards;
    UINT4               u4OutErrors;
    UINT1               au1HCInCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCOutCount[CFA_CLI_U8_STR_LENGTH];

    MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);

    FSAP_U8_CLR (&u8IfHCInOctets);
    FSAP_U8_CLR (&u8IfHCInOctets);
#endif
    /* Check if stacking is enabled */
    if (RmCliIsStackEnable (CliHandle) == CLI_FAILURE)
    {
        /* Stacking commands should not display any values when stacking is disabled */
        /* return FAILURE */
        return CLI_FAILURE;
    }
#ifdef MBSM_WANTED
    CliPrintf (CliHandle,
               "\r\nPort    InOctet       InUcast      InDiscard   "
               "  InErrs     InHCOctet\r\n");
    CliPrintf (CliHandle,
               "----    -------      -------       ---------   "
               "  ------     ---------\r\n");

    for (i4IfIndex = CFA_STACK_PORT_START_INDEX;
         i4IfIndex <= MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS; i4IfIndex++)
    {
        MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
        FSAP_U8_CLR (&u8IfHCInOctets);
        nmhGetIfInOctets (i4IfIndex, &u4InOctects);
        nmhGetIfInUcastPkts (i4IfIndex, &u4InUcastPkts);
        nmhGetIfInDiscards (i4IfIndex, &u4InDiscards);
        nmhGetIfInErrors (i4IfIndex, &u4InErrors);
        nmhGetIfHCInOctets (i4IfIndex, &IfHCInOctets);
        MEMCPY (&u8IfHCInOctets, &IfHCInOctets, sizeof (FS_UINT8));
        /* Converts the UINT8 value to string */
        FSAP_U8_2STR (&u8IfHCInOctets, (CHR1 *) & au1HCInCount);
        CliPrintf (CliHandle, "%-11d", u1Port++);
        CliPrintf (CliHandle, "%-13u", u4InOctects);
        CliPrintf (CliHandle, "%-13u", u4InUcastPkts);
        CliPrintf (CliHandle, "%-13u", u4InDiscards);
        CliPrintf (CliHandle, "%-13u", u4InErrors);
        CliPrintf (CliHandle, "%-15s \r\n", au1HCInCount);

    }
    CliPrintf (CliHandle,
               "Port      OutOctet     OutUcast     OutDiscard   OutErrs    "
               "OutHCOctet\r\n");
    CliPrintf (CliHandle,
               "----      --------     --------     ----------   -------    "
               "----------\r\n");
    u1Port = 1;
    for (i4IfIndex = CFA_STACK_PORT_START_INDEX; i4IfIndex <=
         MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS; i4IfIndex++)
    {
        MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);
        FSAP_U8_CLR (&u8IfHCOutOctets);
        nmhGetIfOutOctets (i4IfIndex, &u4OutOctects);
        nmhGetIfHCOutOctets (i4IfIndex, &IfHCOutOctets);
        MEMCPY (&u8IfHCOutOctets, &IfHCOutOctets, sizeof (FS_UINT8));
        nmhGetIfOutUcastPkts (i4IfIndex, &u4OutUcastPkts);
        nmhGetIfOutDiscards (i4IfIndex, &u4OutDiscards);
        nmhGetIfOutErrors (i4IfIndex, &u4OutErrors);
        /* Converts the UINT8 value to string */
        FSAP_U8_2STR (&u8IfHCOutOctets, (CHR1 *) au1HCOutCount);

        CliPrintf (CliHandle, "%-11d", u1Port++);

        CliPrintf (CliHandle, "%-15u ", u4OutOctects);
        CliPrintf (CliHandle, "%-13u ", u4OutUcastPkts);
        CliPrintf (CliHandle, "%-11u ", u4OutDiscards);
        CliPrintf (CliHandle, "%-11u ", u4OutErrors);
        CliPrintf (CliHandle, "%-20s \r\n", au1HCOutCount);
    }
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliShowStackPeer
 * Input(s)           : CliHandle - CLI handler
                        i4InSwitchId - Incoming switchid
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : Function to display Peer information during stacking
 ******************************************************************************/
UINT4
RmCliShowStackPeer (tCliHandle CliHandle, INT4 i4InSwitchId)
{
#ifdef MBSM_WANTED
    tSNMP_OCTET_STRING_TYPE *pName;
    tSNMP_OCTET_STRING_TYPE SelfNodeId;
    tPeerRecEntry      *pPeerEntry;
    tMacAddr            pMacAddr;
    UINT4               u4SelfIp;
    UINT1              *pu1Temp = NULL;
    UINT1               au1Temp[RM_MAC_ADDR_LEN];
    UINT1               au1IpId[MAX_LEN];
    UINT1              *pu1Tmp = NULL;
    UINT1               au1SelfNodeId[sizeof (UINT4)];
    INT4                i4SlotId = 0;
    INT4                i4Status = 0;
    INT2                i2CardNameLen = 0;
    INT1                ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    CHR1               *pu1String = NULL;
    tUtlInAddr          RmIpAddr;

    MEMSET (au1SelfNodeId, 0, sizeof (au1SelfNodeId));
    SelfNodeId.i4_Length = sizeof (UINT4);
    SelfNodeId.pu1_OctetList = &au1SelfNodeId[0];
    /* Check if stacking is enabled */
    if (RmCliIsStackEnable (CliHandle) == CLI_FAILURE)
    {
        /* Stacking commands should not display any values when stacking is disabled */
        /* return FAILURE */
        return CLI_FAILURE;
    }

    /* If input switchid matches with self switchid, it will display self
     * switch infomations */
    nmhGetFsRmSwitchId (&i4SlotId);
    if (i4SlotId == i4InSwitchId)
    {
        CliPrintf (CliHandle, "\r\n Self Status\r\n");
        CliPrintf (CliHandle, "------------\r\n");

        MEMSET (pMacAddr, 0, RM_MAC_ADDR_LEN);
        MEMSET (au1Temp, 0, RM_MAC_ADDR_LEN);
        MEMSET (au1IpId, 0, MAX_LEN);
        pu1Temp = &au1Temp[0];
        pu1Tmp = &pMacAddr[0];
        pu1String = (CHR1 *) & au1IpId[0];

        CliPrintf (CliHandle, "Switch Id        : %d\r\n", i4SlotId);

        if (nmhGetFsRmSelfNodeId (&SelfNodeId) == SNMP_SUCCESS)
        {
            PTR_FETCH4 (u4SelfIp, SelfNodeId.pu1_OctetList);
            RmIpAddr.u4Addr = OSIX_NTOHL (u4SelfIp);
            CliPrintf (CliHandle, "Stack Ip         : %s\r\n",
                       INET_NTOA (RmIpAddr));
        }

        if (nmhGetFsRmStackMacAddr (&pMacAddr) == SNMP_SUCCESS)
        {
            PrintMacAddress (pMacAddr, pu1Temp);
            CliPrintf (CliHandle, "Stack Mac        : %s\r\n", pu1Temp);
        }

    }
    else
    {
        CliPrintf (CliHandle, "\r\n Peer Status\r\n");
        CliPrintf (CliHandle, "------------\r\n");

        MEMSET (au1Temp, 0, RM_MAC_ADDR_LEN);
        MEMSET (au1IpId, 0, MAX_LEN);
        pu1Temp = &au1Temp[0];
        pu1String = (CHR1 *) & au1IpId[0];

        pName = allocmem_octetstring (MBSM_MAX_CARD_NAME_LEN + 1);
        if (pName == NULL)
        {
            return CLI_FAILURE;
        }
        pName->i4_Length = MBSM_MAX_CARD_NAME_LEN;

        CliPrintf (CliHandle, "SwitchId    StackMac"
                   "        SwitchState     CardName\n");

        CliPrintf (CliHandle, "--------    --------"
                   "        -----------     --------\n");

        TMO_SLL_Scan (RM_PEER_RECORD_SLL, pPeerEntry, tPeerRecEntry *)
        {
            if ((INT4) (pPeerEntry->u4SlotIndex) == i4InSwitchId)
            {
                PrintMacAddress ((UINT1 *) (&(pPeerEntry->au4StackMac[0])),
                                 pu1Temp);
                CliPrintf (CliHandle, "%d         %s", pPeerEntry->u4SlotIndex,
                           pu1Temp);

                i4SlotId = (INT4) (pPeerEntry->u4SlotIndex);
                if (nmhGetMbsmLCConfigCardName (i4SlotId, pName) ==
                    SNMP_FAILURE)
                {
                    i2CardNameLen = 0;
                    i4Status = MBSM_STATUS_NP;
                }
                else
                {
                    i2CardNameLen =
                        (INT2) ((pName->i4_Length <
                                 (MBSM_MAX_CARD_NAME_LEN -
                                  1)) ? pName->
                                i4_Length : (MBSM_MAX_CARD_NAME_LEN - 1));
                }

                if (i2CardNameLen > 0)
                {
                    MEMCPY (ai1CardName, pName->pu1_OctetList, i2CardNameLen);
                }
                ai1CardName[i2CardNameLen] = '\0';

                if (nmhGetMbsmSlotModuleStatus (i4SlotId, &i4Status)
                    == SNMP_FAILURE)
                {
                    i4Status = MBSM_STATUS_INACTIVE;
                }

                switch (i4Status)
                {
                    case MBSM_STATUS_ACTIVE:
                        CliPrintf (CliHandle, "%-13s ", " UP");
                        break;

                    case MBSM_STATUS_NP:
                        CliPrintf (CliHandle, "%-13s ", " NP");

                    case MBSM_STATUS_DOWN:
                        CliPrintf (CliHandle, "%-13s ", " DOWN");
                        break;

                    case MBSM_STATUS_INACTIVE:
                        CliPrintf (CliHandle, "%-13s ", " INACTIVE");
                        break;

                    default:
                        CliPrintf (CliHandle, "%-13s ", " UNKNOWN");
                        break;
                }

                CliPrintf (CliHandle, "%-s\r\n", ai1CardName);

            }
        }
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4InSwitchId);
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliIsStackEnable
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : Function is to check whether stacking is enabled or not.
 ******************************************************************************/
UINT4
RmCliIsStackEnable (tCliHandle CliHandle)
{
    INT4                i4StackPortCount = 0;

    if (nmhGetFsRmStackPortCount (&i4StackPortCount) == SNMP_SUCCESS)
    {
        if (i4StackPortCount == 0)
        {
            /* Stack ports are not present, they are used as normal Xe ports */
            CliPrintf (CliHandle, "stacking is disabled\n");
            return (CLI_FAILURE);
        }
    }

    return CLI_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmCliStartDynSyncAudit
 * Input(s)           : CliHandle - CLI handler
                        i4AppId - Module ID
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function triggers the dynamic audit
 ******************************************************************************/
UINT4
RmCliStartDynSyncAudit (tCliHandle CliHandle, INT4 i4AppId)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsRmDynamicSyncAuditTrigger (&u4ErrCode, (UINT4) i4AppId)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmDynamicSyncAuditTrigger ((UINT4) i4AppId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif

/******************************************************************************
 * Function           : RmCliSetRestartFlag 
 * Input(s)           : CliHandle - CLI handler
                        i4RestartState - enable/disable
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set standby reboot flag.
 ******************************************************************************/
UINT4
RmCliSetRestartFlag (tCliHandle CliHandle, INT4 i4RestartState)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRmProtocolRestartFlag (&u4ErrorCode, i4RestartState)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmProtocolRestartFlag (i4RestartState) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliSetRestartCount
 * Input(s)           : CliHandle - CLI handler
                        i4RestartCnt - Number of reboot retry count
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set standby reboot retry count.
 ******************************************************************************/
UINT4
RmCliSetRestartCount (tCliHandle CliHandle, UINT4 u4RestartCnt)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRmProtocolRestartRetryCnt (&u4ErrorCode, u4RestartCnt)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRmProtocolRestartRetryCnt (u4RestartCnt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/* HITLESS RESTART */
/******************************************************************************
 * Function           : RmCliHRSetStatus
 * Input(s)           : CliHandle - CLI handler
                        i4HRStatus - Status of hitless restart
                                     RM_HR_STATUS_STORE/RM_HR_STATUS_DISABLE
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set hitless restart status
 ******************************************************************************/
UINT4
RmCliSetHRStatus (tCliHandle CliHandle, INT4 i4HRStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRmHitlessRestartFlag (&u4ErrorCode, i4HRStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsRmHitlessRestartFlag (i4HRStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : RmCliHRSetStatus
 * Input(s)           : CliHandle - CLI handler
                        i4HRStatus - Status of hitless restart
                                     RM_HR_STATUS_STORE/RM_HR_STATUS_DISABLE
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE.
 * Action             : This function is to set hitless restart status
 ******************************************************************************/
UINT4
RmCliSetInitiateCopy (tCliHandle CliHandle, INT4 i4InitiateCopy)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRmCopyPeerSyLogFile (&u4ErrorCode, i4InitiateCopy)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Copy peer log file is in progress\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsRmCopyPeerSyLogFile (i4InitiateCopy) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmCliShowDynamicAuditStatus
 * Input(s)           : CliHandle - CLI handler
                        i4AppId - Module ID
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to show the switch over time taken for
 *                 each module.
 ******************************************************************************/
UINT4
RmCliShowDynamicAuditStatus (tCliHandle CliHandle, INT4 i4AppId)
{
    tSNMP_OCTET_STRING_TYPE AppName;
    INT4                i4ModuleId = 0;
    INT4                i4PrevModuleId = 0;
    INT4                i4DynAuditStatus = 0;
    UINT1               au1AppName[RM_MAX_FILE_NAME_LEN];

    CliPrintf (CliHandle, "\r\nAppId    AppName     DynamicAuditStatus\r\n");
    CliPrintf (CliHandle, "------  -----------  ------------------\r\n");
    if (i4AppId == RM_MAX_APPS)
    {
        if (nmhGetFirstIndexFsRmDynamicSyncAuditTable (&i4ModuleId)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        do
        {
            MEMSET (au1AppName, 0, RM_MAX_FILE_NAME_LEN);
            AppName.pu1_OctetList = au1AppName;
            nmhGetFsRmAppName (i4ModuleId, &AppName);
            nmhGetFsRmDynamicSyncAuditStatus (i4ModuleId, &i4DynAuditStatus);
            CliPrintf (CliHandle, "%5d   %-11s   ",
                       i4ModuleId, AppName.pu1_OctetList);
            if (i4DynAuditStatus == RM_DYN_AUDIT_NOT_TRIGGERED)
            {
                CliPrintf (CliHandle, "%-14s\r\n", "Not Triggered");
            }
            else if (i4DynAuditStatus == RM_DYN_AUDIT_INPROGRESS)
            {
                CliPrintf (CliHandle, "%-14s\r\n", "In Progress");
            }
            else if (i4DynAuditStatus == RM_DYN_AUDIT_ABORTED)
            {
                CliPrintf (CliHandle, "%-14s\r\n", "Aborted");
            }
            else if (i4DynAuditStatus == RM_DYN_AUDIT_SUCCESS)
            {
                CliPrintf (CliHandle, "%-14s\r\n", "Success");
            }
            else if (i4DynAuditStatus == RM_DYN_AUDIT_FAILED)
            {
                CliPrintf (CliHandle, "%-14s\r\n", "Failure");
            }
            i4PrevModuleId = i4ModuleId;
        }
        while (nmhGetNextIndexFsRmDynamicSyncAuditTable
               (i4PrevModuleId, &i4ModuleId) == SNMP_SUCCESS);
    }
    else
    {
        MEMSET (au1AppName, 0, RM_MAX_FILE_NAME_LEN);
        AppName.pu1_OctetList = au1AppName;
        nmhGetFsRmAppName (i4AppId, &AppName);
        nmhGetFsRmDynamicSyncAuditStatus (i4AppId, &i4DynAuditStatus);
        CliPrintf (CliHandle, "%5d   %-11s   ", i4AppId, AppName.pu1_OctetList);
        if (i4DynAuditStatus == RM_DYN_AUDIT_NOT_TRIGGERED)
        {
            CliPrintf (CliHandle, "%-14s\r\n", "Not Triggered");
        }
        else if (i4DynAuditStatus == RM_DYN_AUDIT_INPROGRESS)
        {
            CliPrintf (CliHandle, "%-14s\r\n", "In Progress");
        }
        else if (i4DynAuditStatus == RM_DYN_AUDIT_ABORTED)
        {
            CliPrintf (CliHandle, "%-14s\r\n", "Aborted");
        }
        else if (i4DynAuditStatus == RM_DYN_AUDIT_SUCCESS)
        {
            CliPrintf (CliHandle, "%-14s\r\n", "Success");
        }
        else if (i4DynAuditStatus == RM_DYN_AUDIT_FAILED)
        {
            CliPrintf (CliHandle, "%-14s\r\n", "Failure");
        }

    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

#endif
/******************************************************************************
 * Function           : RmCliShowSwitchOverTime    
 * Input(s)           : CliHandle - CLI handler
                        i4AppId - Module ID                      
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to show the switch over time taken for
 *                 each module.
 ******************************************************************************/
UINT4
RmCliShowSwitchOverTime (tCliHandle CliHandle, INT4 i4AppId)
{
    tSNMP_OCTET_STRING_TYPE AppName;
    INT4                i4ModuleId = 0;
    INT4                i4PrevModuleId = 0;
    UINT4               u4SwitchOverTime = 0;
    UINT1               au1AppName[RM_MAX_FILE_NAME_LEN];

    CliPrintf (CliHandle, "\r\nTotal Switch Over Time - %d msec\r\n",
               gRmTotalSwitchOverTime);
    CliPrintf (CliHandle, "\r\nAppId    AppName     SwitchOverTime\r\n");
    CliPrintf (CliHandle, "                               (msec)\r\n");
    CliPrintf (CliHandle, "------  -----------  --------------\r\n");
    if (i4AppId == RM_MAX_APPS)
    {
        if (nmhGetFirstIndexFsRmSwitchoverTimeTable (&i4ModuleId)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        do
        {
            MEMSET (au1AppName, 0, RM_MAX_FILE_NAME_LEN);
            AppName.pu1_OctetList = au1AppName;
            nmhGetFsRmAppName (i4ModuleId, &AppName);
            nmhGetFsRmSwitchoverTime (i4ModuleId, &u4SwitchOverTime);
            CliPrintf (CliHandle, "%5d   %-11s   %-14d\r\n",
                       i4ModuleId, AppName.pu1_OctetList, u4SwitchOverTime);
            i4PrevModuleId = i4ModuleId;
        }
        while (nmhGetNextIndexFsRmSwitchoverTimeTable
               (i4PrevModuleId, &i4ModuleId) == SNMP_SUCCESS);
    }
    else
    {
        MEMSET (au1AppName, 0, RM_MAX_FILE_NAME_LEN);
        AppName.pu1_OctetList = au1AppName;
        nmhGetFsRmAppName (i4AppId, &AppName);
        nmhGetFsRmSwitchoverTime (i4AppId, &u4SwitchOverTime);
        CliPrintf (CliHandle, "%5d   %-11s   %-14d\r\n",
                   i4AppId, AppName.pu1_OctetList, u4SwitchOverTime);
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssRmgrShowDebugging                                */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     RMGR module                                         */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/
VOID
IssRmgrShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4CurTrcLevel = 0;

    nmhGetFsRmTrcLevel (&u4CurTrcLevel);
    if (u4CurTrcLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rRMGR :");
    if ((u4CurTrcLevel & RM_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR critical debugging is on");
    }
    if ((u4CurTrcLevel & RM_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR failure debugging is on");
    }
    if ((u4CurTrcLevel & RM_SEM_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR state machine debugging is on");
    }
    if ((u4CurTrcLevel & RM_TMR_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR timer debugging is on");
    }
    if ((u4CurTrcLevel & RM_SOCK_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR socket debugging is on");
    }
    if ((u4CurTrcLevel & RM_FT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR file-tansfer debugging is on");
    }
    if ((u4CurTrcLevel & RM_SNMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR snmp debugging is on");
    }
    if ((u4CurTrcLevel & RM_NOTIF_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR notification debugging is on");
    }
    if ((u4CurTrcLevel & RM_SYNCUP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR syncup-trc debugging is on");
    }
    if ((u4CurTrcLevel & RM_BUFF_TRACE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR buffer debugging is on");
    }
    if ((u4CurTrcLevel & RM_EVENT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR event debugging is on");
    }
    if ((u4CurTrcLevel & RM_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR dump-trc debugging is on");
    }
    if ((u4CurTrcLevel & RM_CTRL_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR control-plane-trc debugging is on");
    }
    if ((u4CurTrcLevel & RM_SWITCHOVER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR switchover debugging is on");
    }
    if ((u4CurTrcLevel & RM_SOCK_CRIT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR socket-critical debugging is on");
    }
    if ((u4CurTrcLevel & RM_SYNC_MSG_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR sync-msg debugging is on");
    }
    if ((u4CurTrcLevel & RM_SYNC_EVENT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RMGR sync-event debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*********************************************************************************/
/*                                                                               */
/*     Function Name : IssRmgrModuleShowDebugging                                */
/*                                                                               */
/*     Description   : This function is used to display debug level for          */
/*                     RMGR-Module module                                        */
/*                                                                               */
/*     INPUT         : CliHandle                                                 */
/*                                                                               */
/*     OUTPUT        : None                                                      */
/*                                                                               */
/*     RETURNS       : None                                                      */
/*                                                                               */
/*********************************************************************************/
VOID
IssRmgrModuleShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4CurModTrace = 0;

    nmhGetFsRmModuleTrc (&u4CurModTrace);
    if (u4CurModTrace == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rRMGR-MODULE :");
    if (u4CurModTrace == RM_MOD_ALL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE all debugging is on");
        CliPrintf (CliHandle, "\r\n");
        return;
    }
    if ((u4CurModTrace & RM_MOD_RM_TRC) == RM_MOD_RM_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE rm debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_VCM_TRC) == RM_MOD_VCM_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE vcm debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_MBSM_TRC) == RM_MOD_MBSM_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE mbsm debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_MSR_TRC) == RM_MOD_MSR_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE msr debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_CFA_TRC) == RM_MOD_CFA_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE cfa debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_EOAM_TRC) == RM_MOD_EOAM_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE eoam debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_PNAC_TRC) == RM_MOD_PNAC_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE pnac debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_LA_TRC) == RM_MOD_LA_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE la debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_RSTPMSTP_TRC) == RM_MOD_RSTPMSTP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE stp debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_VLANGARP_TRC) == RM_MOD_VLANGARP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE vlan debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_MRP_TRC) == RM_MOD_MRP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE mrp debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_PBB_TRC) == RM_MOD_PBB_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE pbb debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_ECFM_TRC) == RM_MOD_ECFM_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE ecfm debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_ELPS_TRC) == RM_MOD_ELPS_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE elps debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_PBBTE_TRC) == RM_MOD_PBBTE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE pbb-te debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_ELMI_TRC) == RM_MOD_ELMI_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE elmi debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_SNOOP_TRC) == RM_MOD_SNOOP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE snoop debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_MPLS_TRC) == RM_MOD_MPLS_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE mpls debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_SNMP_TRC) == RM_MOD_SNMP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE snmp debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_LLDP_TRC) == RM_MOD_LLDP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE lldp debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_CLI_TRC) == RM_MOD_CLI_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE cli debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_NPAPI_TRC) == RM_MOD_NPAPI_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE np-app debugging is on");
    }
    if ((u4CurModTrace & RM_MOD_STATIC_CONF_TRC) == RM_MOD_STATIC_CONF_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RMGR-MODULE static-conf debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}
#endif /* __RMCLI_C__ */
