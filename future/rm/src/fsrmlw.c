/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmlw.c,v 1.44 2017/12/26 13:34:30 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fsrmlw.h"
#include  "rmincs.h"
#include  "snmccons.h"
#include  "rmcli.h"
#include  "cli.h"
#include  "iss.h"
#include "rmglob.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmSelfNodeId
 Input       :  The Indices

                The Object 
                retValFsRmSelfNodeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmSelfNodeId (tSNMP_OCTET_STRING_TYPE * pRetValFsRmSelfNodeId)
{
    pRetValFsRmSelfNodeId->i4_Length = sizeof (UINT4);
    PTR_ASSIGN4 (pRetValFsRmSelfNodeId->pu1_OctetList, RM_GET_SELF_NODE_ID ());
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmPeerNodeId
 Input       :  The Indices

                The Object 
                retValFsRmPeerNodeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerNodeId (tSNMP_OCTET_STRING_TYPE * pRetValFsRmPeerNodeId)
{
    pRetValFsRmPeerNodeId->i4_Length = sizeof (UINT4);

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        PTR_ASSIGN4 (pRetValFsRmPeerNodeId->pu1_OctetList, HbGetPeerNodeId ());
        RM_UNLOCK ();
    }
    else
    {
        RM_LOCK ();

        PTR_ASSIGN4 (pRetValFsRmPeerNodeId->pu1_OctetList,
                     RM_GET_PEER_NODE_ID ());

        RM_UNLOCK ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmActiveNodeId
 Input       :  The Indices

                The Object 
                retValFsRmActiveNodeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmActiveNodeId (tSNMP_OCTET_STRING_TYPE * pRetValFsRmActiveNodeId)
{
    pRetValFsRmActiveNodeId->i4_Length = sizeof (UINT4);

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        PTR_ASSIGN4 (pRetValFsRmActiveNodeId->pu1_OctetList,
                     HbGetActiveNodeId ());

        RM_UNLOCK ();
    }
    else
    {
        RM_LOCK ();

        PTR_ASSIGN4 (pRetValFsRmActiveNodeId->pu1_OctetList,
                     RM_GET_ACTIVE_NODE_ID ());

        RM_UNLOCK ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmNodeState
 Input       :  The Indices

                The Object 
                retValFsRmNodeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmNodeState (INT4 *pi4RetValFsRmNodeState)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        *pi4RetValFsRmNodeState = (INT4) HbGetNodeState ();

        RM_UNLOCK ();
    }
    else
    {
        RM_LOCK ();

        *pi4RetValFsRmNodeState = (INT4) RM_GET_NODE_STATE ();

        RM_UNLOCK ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmHbInterval
 Input       :  The Indices

                The Object 
                retValFsRmHbInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmHbInterval (INT4 *pi4RetValFsRmHbInterval)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        *pi4RetValFsRmHbInterval = (INT4) HbGetHbInterval ();

        RM_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (pi4RetValFsRmHbInterval);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmPeerDeadInterval
 Input       :  The Indices

                The Object 
                retValFsRmPeerDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerDeadInterval (INT4 *pi4RetValFsRmPeerDeadInterval)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        *pi4RetValFsRmPeerDeadInterval = (INT4) HbGetHbPeerDeadInterval ();

        RM_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (pi4RetValFsRmPeerDeadInterval);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmPeerDeadIntMultiplier
 Input       :  The Indices

                The Object
                retValFsRmPeerDeadIntMultiplier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerDeadIntMultiplier (INT4 *pi4RetValFsRmPeerDeadIntMultiplier)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        *pi4RetValFsRmPeerDeadIntMultiplier =
            (INT4) HbGetHbPeerDeadIntMultiplier ();

        RM_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (pi4RetValFsRmPeerDeadIntMultiplier);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmTrcLevel
 Input       :  The Indices

                The Object 
                retValFsRmTrcLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmTrcLevel (UINT4 *pu4RetValFsRmTrcLevel)
{
    RM_LOCK ();
    *pu4RetValFsRmTrcLevel = RM_GET_TRC_LEVEL ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmForceSwitchoverFlag
 Input       :  The Indices

                The Object 
                retValFsRmForceSwitchoverFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmForceSwitchoverFlag (INT4 *pi4RetValFsRmForceSwitchoverFlag)
{
    RM_LOCK ();
    if (RM_GET_FLAGS () & HB_FORCE_SWITCHOVER)
    {
        *pi4RetValFsRmForceSwitchoverFlag = RM_ENABLE;
    }
    else
    {
        *pi4RetValFsRmForceSwitchoverFlag = RM_DISABLE;
    }

    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmSwitchId
 Input       :  The Indices

                The Object
                retValFsRmSwitchId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmSwitchId (INT4 *pi4RetValFsRmSwitchId)
{
    *pi4RetValFsRmSwitchId = (UINT4) RM_GET_SWITCH_ID ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmConfiguredState
 Input       :  The Indices

                The Object
                retValFsRmConfiguredState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmConfiguredState (INT4 *pi4RetValFsRmConfiguredState)
{
    *pi4RetValFsRmConfiguredState = (UINT4) RM_CONFIGURED_STATE ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRmStackMacAddr
 Input       :  The Indices

                The Object
                retValFsRmStackMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStackMacAddr (tMacAddr * pRetValFsRmStackMacAddr)
{
    MEMCPY (pRetValFsRmStackMacAddr, gRmInfo.au1StkMacAddr, MAC_ADDR_LEN);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRmPeerSwitchBaseMacAddr
 Input       :  The Indices
                FsRmPeerSwitchId

                The Object
                retValFsRmPeerSwitchBaseMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerSwitchBaseMacAddr (INT4 i4FsRmPeerSwitchId,
                                 tMacAddr * pRetValFsRmPeerSwitchBaseMacAddr)
{

    tPeerRecEntry      *pPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        RmGetPeerEntry ((UINT4) i4FsRmPeerSwitchId, &pPeerEntry);

        if (pPeerEntry != NULL)
        {
            MEMCPY (pRetValFsRmPeerSwitchBaseMacAddr,
                    pPeerEntry->au1SwitchBaseMac, MAC_ADDR_LEN);
            return SNMP_SUCCESS;
        }

        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetFsRmStackPortCount
 Input       :  The Indices

                The Object
                retValFsRmStackPortCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStackPortCount (INT4 *pi4RetValFsRmStackPortCount)
{
#ifdef MBSM_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        *pi4RetValFsRmStackPortCount = IssGetStackPortCountFromNvRam ();
    }
    else
    {
        if (IssGetStackingModel () == ISS_CTRL_PLANE_STACKING_MODEL)
        {
            *pi4RetValFsRmStackPortCount =
                (SYS_DEF_MAX_INFRA_SYS_PORT_COUNT / 2);

        }
        else
        {

            *pi4RetValFsRmStackPortCount = ISS_DEFAULT_STACK_PORT_COUNT;
        }
    }
#else
    UNUSED_PARAM (pi4RetValFsRmStackPortCount);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmColdStandby
 Input       :  The Indices

                The Object 
                retValFsRmColdStandby
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmColdStandby (INT4 *pi4RetValFsRmColdStandby)
{
    *pi4RetValFsRmColdStandby = (INT4) RM_COLDSTANDBY_STATE ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmModuleTrc
 Input       :  The Indices

                The Object 
                retValFsRmModuleTrc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmModuleTrc (UINT4 *pu4RetValFsRmModuleTrc)
{
    RM_LOCK ();
    *pu4RetValFsRmModuleTrc = (UINT4) RM_GET_MOD_TRC ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmProtocolRestartFlag
 Input       :  The Indices

                The Object 
                retValFsRmProtocolRestartFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmProtocolRestartFlag (INT4 *pi4RetValFsRmProtocolRestartFlag)
{
    RM_LOCK ();
    *pi4RetValFsRmProtocolRestartFlag = gRmInfo.bRestartFlag;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmProtocolRestartRetryCnt
 Input       :  The Indices

                The Object 
                retValFsRmProtocolRestartRetryCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmProtocolRestartRetryCnt (UINT4 *pu4RetValFsRmProtocolRestartRetryCnt)
{
    RM_LOCK ();
    *pu4RetValFsRmProtocolRestartRetryCnt = gRmInfo.u1RestartRetryCnt;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/* HITLESS RESTART */
/****************************************************************************
 Function    :  nmhGetFsRmHitlessRestartFlag
 Input       :  The Indices

                The Object
                retValFsRmHitlessRestartFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmHitlessRestartFlag (INT4 *pi4RetValFsRmHitlessRestartFlag)
{
    RM_LOCK ();
    *pi4RetValFsRmHitlessRestartFlag = gRmInfo.u1HRFlag;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmIpAddress
 Input       :  The Indices

                The Object 
                retValFsRmIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmIpAddress (UINT4 *pu4RetValFsRmIpAddress)
{
    RM_LOCK ();
    *pu4RetValFsRmIpAddress = gRmInfo.u4SelfNode;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmSubnetMask
 Input       :  The Indices

                The Object 
                retValFsRmSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmSubnetMask (UINT4 *pu4RetValFsRmSubnetMask)
{
    RM_LOCK ();
    *pu4RetValFsRmSubnetMask = gRmInfo.u4SelfNodeMask;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStackInterface
 Input       :  The Indices

                The Object 
                retValFsRmStackInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStackInterface (tSNMP_OCTET_STRING_TYPE * pRetValFsRmStackInterface)
{
    RM_LOCK ();
    pRetValFsRmStackInterface->i4_Length = STRLEN (gRmInfo.au1RmStackInterface);
    MEMCPY (pRetValFsRmStackInterface->pu1_OctetList,
            gRmInfo.au1RmStackInterface, STRLEN (gRmInfo.au1RmStackInterface));
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/****************************************************************************
 Function    :  nmhGetFsRmDynamicSyncAuditTrigger
 Input       :  The Indices

                The Object 
                retValFsRmDynamicSyncAuditTrigger
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmDynamicSyncAuditTrigger (UINT4 *pu4RetValFsRmDynamicSyncAuditTrigger)
{
    UINT4               u4RmAppId = 0;

    *pu4RetValFsRmDynamicSyncAuditTrigger = 0;
    /*Returns the list of Modules for which the dynamic audit is in progress */
    RM_LOCK ();
    for (u4RmAppId = 0; u4RmAppId < RM_MAX_APPS; u4RmAppId++)
    {
        if ((gRmDynAuditInfo[u4RmAppId].u4IsAppSetForAudit == RM_TRUE)
            && (gRmDynAuditInfo[u4RmAppId].u4AuditStatus ==
                RM_DYN_AUDIT_INPROGRESS))
        {
            *pu4RetValFsRmDynamicSyncAuditTrigger |= u4RmAppId;
        }
    }
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}
#endif

/****************************************************************************
 Function    :  nmhGetFsRmCopyPeerSyLogFile
 Input       :  The Indices

                The Object
                retValFsRmCopyPeerSyLogFile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmCopyPeerSyLogFile (INT4 *pi4RetValFsRmCopyPeerSyLogFile)
{
    *pi4RetValFsRmCopyPeerSyLogFile = gRmInfo.u1SysLogFileStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRmHbInterval
 Input       :  The Indices

                The Object 
                setValFsRmHbInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmHbInterval (INT4 i4SetValFsRmHbInterval)
{
    if (HbSetHbInterval ((UINT4) i4SetValFsRmHbInterval) == HB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmPeerDeadInterval
 Input       :  The Indices

                The Object 
                setValFsRmPeerDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmPeerDeadInterval (INT4 i4SetValFsRmPeerDeadInterval)
{
    if (HbSetHbPeerDeadInterval ((UINT4) i4SetValFsRmPeerDeadInterval) ==
        HB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmPeerDeadIntMultiplier
 Input       :  The Indices

                The Object
                setValFsRmPeerDeadIntMultiplier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmPeerDeadIntMultiplier (INT4 i4SetValFsRmPeerDeadIntMultiplier)
{
    if (HbSetHbPeerDeadIntMultiplier ((UINT4) i4SetValFsRmPeerDeadIntMultiplier)
        == HB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmTrcLevel
 Input       :  The Indices

                The Object 
                setValFsRmTrcLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmTrcLevel (UINT4 u4SetValFsRmTrcLevel)
{
    RM_LOCK ();

    RM_SET_TRC_LEVEL (u4SetValFsRmTrcLevel);

    RM_UNLOCK ();

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        HbSetTrcLevel (u4SetValFsRmTrcLevel);

        RM_UNLOCK ();
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmForceSwitchoverFlag
 Input       :  The Indices

                The Object 
                setValFsRmForceSwitchoverFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmForceSwitchoverFlag (INT4 i4SetValFsRmForceSwitchoverFlag)
{
    UNUSED_PARAM (i4SetValFsRmForceSwitchoverFlag);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* Forcefully changing stack switch as standalone switch */
        RmChangeStacktoStandaloneswitch ();
        return SNMP_SUCCESS;
    }
    if (RM_SEND_EVENT (RM_TASK_ID, RM_TRIG_FSW_FROM_MGMT) == OSIX_FAILURE)
    {
        RM_TRC (RM_SNMP_TRC, "nmhSetFsRmForceSwitchoverFlag - Failure !!!\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRmSwitchId
 Input       :  The Indices

                The Object 
                setValFsRmSwitchId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmSwitchId (INT4 i4SetValFsRmSwitchId)
{
    if (gRmInfo.u4SwitchId == (UINT4) i4SetValFsRmSwitchId)
    {
        /* No need to update switch id, node id file and iss.conf if 
         * switch id is not changed. */
        return SNMP_SUCCESS;
    }
    gRmInfo.u4SwitchId = (UINT4) i4SetValFsRmSwitchId;
    IssSetSwitchid (gRmInfo.u4SwitchId);
    IssWriteNodeidFile ();
    /* If config file(iss.conf) is present
     * means, when standalone-switch becomes 
     * stack-switch and vice versa, 
     * port related configurations which are 
     * present in iss.conf should be updated */
    StackUpdateConfigurationFile ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmConfiguredState
 Input       :  The Indices

                The Object 
                setValFsRmConfiguredState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmConfiguredState (INT4 i4SetValFsRmConfiguredState)
{
    gRmInfo.u4ConfigState = (UINT4) i4SetValFsRmConfiguredState;
    IssSetUserPreference (gRmInfo.u4ConfigState);
    IssWriteNodeidFile ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmStackPortCount
 Input       :  The Indices

                The Object
                setValFsRmStackPortCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmStackPortCount (INT4 i4SetValFsRmStackPortCount)
{
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        gIssSysGroupInfo.u2StackPortCount = (UINT2) i4SetValFsRmStackPortCount;

        /* Set the configured stack ports to NvRam */
        IssSetStackPortCountToNvRam ((UINT2) i4SetValFsRmStackPortCount);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRmColdStandby
 Input       :  The Indices

                The Object 
                setValFsRmColdStandby
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmColdStandby (INT4 i4SetValFsRmColdStandby)
{
    gRmInfo.u4ColdStandbyState = (UINT4) i4SetValFsRmColdStandby;
    IssSetColdStandbyToNvRam (gRmInfo.u4ColdStandbyState);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmModuleTrc
 Input       :  The Indices

                The Object 
                setValFsRmModuleTrc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmModuleTrc (UINT4 u4SetValFsRmModuleTrc)
{
    RM_LOCK ();
    RM_SET_MOD_TRC (u4SetValFsRmModuleTrc);
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmProtocolRestartFlag
 Input       :  The Indices

                The Object 
                setValFsRmProtocolRestartFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmProtocolRestartFlag (INT4 i4SetValFsRmProtocolRestartFlag)
{
    RM_LOCK ();
    gRmInfo.bRestartFlag = i4SetValFsRmProtocolRestartFlag;

    /* When the Restart flag is configured, reset the restart counter */
    gRmInfo.u1RestartCntr = 0;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmProtocolRestartRetryCnt
 Input       :  The Indices

                The Object 
                setValFsRmProtocolRestartRetryCnt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmProtocolRestartRetryCnt (UINT4 u4SetValFsRmProtocolRestartRetryCnt)
{
    RM_LOCK ();
    gRmInfo.u1RestartRetryCnt = (UINT1) u4SetValFsRmProtocolRestartRetryCnt;

    /* When the Max Restart retry count is configured,
     * reset the restart counter */
    gRmInfo.u1RestartCntr = 0;
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/* HITLESS RESTART */
/****************************************************************************
 Function    :  nmhSetFsRmHitlessRestartFlag
 Input       :  The Indices

                The Object
                setValFsRmHitlessRestartFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmHitlessRestartFlag (INT4 i4SetValFsRmHitlessRestartFlag)
{
    tRmNotificationMsg  NotifMsg;
    UINT4               u4CurAppId = RM_APP_ID;
    UINT4               u4NextAppId = 0;

    RM_LOCK ();

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
    gRmInfo.u1HRFlag = (UINT1) i4SetValFsRmHitlessRestartFlag;

    if (i4SetValFsRmHitlessRestartFlag == RM_HR_STATUS_STORE)
    {
        NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
        NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_HR_START;
        NotifMsg.u4Status = RM_STARTED;
        NotifMsg.u4Error = RM_NONE;
        UtlGetTimeStr (NotifMsg.ac1DateTime);
        RmTrapSendNotifications (&NotifMsg);

        IssSzInitSizingInfoForHR ();
        RmHRDelFiles ();
        if (RmHRUtilGetNextAppId (RM_APP_ID, &u4NextAppId) == RM_SUCCESS)
        {
            RmHRStartBulkReq (u4NextAppId);
        }
    }
    else if (i4SetValFsRmHitlessRestartFlag == RM_HR_STATUS_RESTORE)
    {
        while (u4CurAppId < RM_MAX_APPS)
        {
            if (RmHRUtilGetNextAppId (u4CurAppId, &u4NextAppId) == RM_SUCCESS)
            {
                RmHRBulkMsgRestore (u4NextAppId);
                u4CurAppId = u4NextAppId;
            }
            else
            {
                /* check - this should be done after the entire restoring
                 * happened */

                gRmInfo.u1HRFlag = RM_HR_STATUS_DISABLE;
                IssSetHRFlagToNvRam (RM_HR_STATUS_DISABLE);
                NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
                NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
                NotifMsg.u4AppId = RM_APP_ID;
                NotifMsg.u4Operation = RM_HR_STOP;
                NotifMsg.u4Status = RM_COMPLETED;
                NotifMsg.u4Error = RM_NONE;
                UtlGetTimeStr (NotifMsg.ac1DateTime);
                RmTrapSendNotifications (&NotifMsg);
#ifdef NPAPI_WANTED
                RmFsNpHRSetStdyStInfo (NP_RM_HR_STDY_ST_TRIG_STOP, NULL);
#endif
                break;
            }
        }
    }
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmIpAddress
 Input       :  The Indices

                The Object 
                setValFsRmIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmIpAddress (UINT4 u4SetValFsRmIpAddress)
{
    RM_LOCK ();
    gRmInfo.u4SelfNode = u4SetValFsRmIpAddress;
    RM_UNLOCK ();
    IssSetRMIpAddress (u4SetValFsRmIpAddress);
    IssWriteNodeidFile ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmSubnetMask
 Input       :  The Indices

                The Object 
                setValFsRmSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmSubnetMask (UINT4 u4SetValFsRmSubnetMask)
{
    RM_LOCK ();
    gRmInfo.u4SelfNodeMask = u4SetValFsRmSubnetMask;
    RM_UNLOCK ();
    IssSetRMSubnetMask (u4SetValFsRmSubnetMask);
    IssWriteNodeidFile ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRmStackInterface
 Input       :  The Indices

                The Object 
                setValFsRmStackInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmStackInterface (tSNMP_OCTET_STRING_TYPE * pSetValFsRmStackInterface)
{
    RM_LOCK ();
    MEMCPY (gRmInfo.au1RmStackInterface,
            pSetValFsRmStackInterface->pu1_OctetList,
            pSetValFsRmStackInterface->i4_Length);
    gRmInfo.au1RmStackInterface[pSetValFsRmStackInterface->i4_Length] = '\0';
    RM_UNLOCK ();
    IssSetRMStackInterface (gRmInfo.au1RmStackInterface);
    IssWriteNodeidFile ();
    return SNMP_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/****************************************************************************
 Function    :  nmhSetFsRmDynamicSyncAuditTrigger
 Input       :  The Indices

                The Object
                setValFsRmDynamicSyncAuditTrigger
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmDynamicSyncAuditTrigger (UINT4 u4SetValFsRmDynamicSyncAuditTrigger)
{
    if (RmTriggerAuditEvent ((INT4) u4SetValFsRmDynamicSyncAuditTrigger) ==
        RM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
#endif

/****************************************************************************
 Function    :  nmhSetFsRmCopyPeerSyLogFile
 Input       :  The Indices

                The Object
                setValFsRmCopyPeerSyLogFile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmCopyPeerSyLogFile (INT4 i4SetValFsRmCopyPeerSyLogFile)
{
    if (i4SetValFsRmCopyPeerSyLogFile == RM_COPY_STATUS_INITIATE)
    {
        if (RM_GET_NODE_STATE () == RM_STANDBY)
        {
            RmUploadFile (FILE_SYSLOG_FILE);
            gRmInfo.u1SysLogFileStatus = RM_COPY_STATUS_INPROGRESS;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRmHbInterval
 Input       :  The Indices

                The Object 
                testValFsRmHbInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmHbInterval (UINT4 *pu4ErrorCode, INT4 i4TestValFsRmHbInterval)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        if ((i4TestValFsRmHbInterval < HB_MIN_INTERVAL) ||
            (i4TestValFsRmHbInterval > HB_MAX_INTERVAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4TestValFsRmHbInterval);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmPeerDeadInterval
 Input       :  The Indices

                The Object 
                testValFsRmPeerDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmPeerDeadInterval (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRmPeerDeadInterval)
{
    UNUSED_PARAM (*pu4ErrorCode);

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        if ((i4TestValFsRmPeerDeadInterval >= HB_MIN_PEER_DEAD_INTERVAL) &&
            (i4TestValFsRmPeerDeadInterval <= HB_MAX_PEER_DEAD_INTERVAL))
        {
            /* PeerDeadInterval should be 4 times greater than that of HbInterval */
            if ((i4TestValFsRmPeerDeadInterval / HbGetHbInterval ()) >= 4)
            {
                RM_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
        RM_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (i4TestValFsRmPeerDeadInterval);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmPeerDeadIntMultiplier
 Input       :  The Indices

                The Object
                testValFsRmPeerDeadIntMultiplier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmPeerDeadIntMultiplier (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsRmPeerDeadIntMultiplier)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        RM_LOCK ();

        /*if ((i4TestValFsRmPeerDeadIntMultiplier <
           HB_MIN_PEER_DEAD_INT_MULTIPLIER) ||
           (i4TestValFsRmPeerDeadIntMultiplier >
           HB_MAX_PEER_DEAD_INT_MULTIPLIER))
           {
           *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
           i1RetVal = SNMP_FAILURE;
           } */
        RM_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4TestValFsRmPeerDeadIntMultiplier);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmTrcLevel
 Input       :  The Indices

                The Object 
                testValFsRmTrcLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmTrcLevel (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRmTrcLevel)
{
    if ((u4TestValFsRmTrcLevel != RM_CRITICAL_TRC) &&
        (u4TestValFsRmTrcLevel != RM_FAILURE_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SEM_TRC) &&
        (u4TestValFsRmTrcLevel != RM_TMR_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SOCK_TRC) &&
        (u4TestValFsRmTrcLevel != RM_FT_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SNMP_TRC) &&
        (u4TestValFsRmTrcLevel != RM_NOTIF_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SYNCUP_TRC) &&
        (u4TestValFsRmTrcLevel != RM_BUFF_TRACE) &&
        (u4TestValFsRmTrcLevel != RM_EVENT_TRC) &&
        (u4TestValFsRmTrcLevel != RM_CTRL_PATH_TRC) &&
        (u4TestValFsRmTrcLevel != RM_DUMP_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SWITCHOVER_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SOCK_CRIT_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SYNC_EVENT_TRC) &&
        (u4TestValFsRmTrcLevel != RM_SYNC_MSG_TRC) &&
        (u4TestValFsRmTrcLevel != RM_ALL_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RM_INVALID_DEBUG_TRC_LEVEL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmForceSwitchoverFlag
 Input       :  The Indices

                The Object 
                testValFsRmForceSwitchoverFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmForceSwitchoverFlag (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsRmForceSwitchoverFlag)
{
    if (i4TestValFsRmForceSwitchoverFlag != RM_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    RM_LOCK ();
    if (RM_GET_NODE_STATE () != RM_ACTIVE)
    {
        CLI_SET_ERR (CLI_RM_FSW_ALLOWED_ONLY_IN_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RM_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef L2RED_WANTED
    if (RM_GET_PEER_NODE_STATE () != RM_PEER_UP)
    {
        CLI_SET_ERR (CLI_RM_NOT_ALLOWED_ON_STANDBY_DOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RM_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (RmCheckBulkUpdatesStatus () != RM_TRUE)
    {
        CLI_SET_ERR (CLI_RM_NOT_ALLOWED_DURING_PROTOCOL_SYNCUP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RM_UNLOCK ();
        return SNMP_FAILURE;
    }
#endif
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmSwitchId
 Input       :  The Indices

                The Object 
                testValFsRmSwitchId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmSwitchId (UINT4 *pu4ErrorCode, INT4 i4TestValFsRmSwitchId)
{
#ifdef MBSM_WANTED

    if ((i4TestValFsRmSwitchId < MBSM_SLOT_INDEX_START)
        || (i4TestValFsRmSwitchId >
            (MBSM_MAX_SLOTS - MBSM_SLOT_INDEX_START - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsRmSwitchId);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmConfiguredState
 Input       :  The Indices

                The Object 
                testValFsRmConfiguredState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmConfiguredState (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsRmConfiguredState)
{

    if ((i4TestValFsRmConfiguredState != ISS_PREFERRED_MASTER) &&
        (i4TestValFsRmConfiguredState != ISS_PREFERRED_SLAVE) &&
        (i4TestValFsRmConfiguredState != ISS_BACKUP_MASTER))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmStackPortCount
 Input       :  The Indices

                The Object
                testValFsRmStackPortCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmStackPortCount (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsRmStackPortCount)
{
#ifdef MBSM_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if ((i4TestValFsRmStackPortCount < 0) ||
            (i4TestValFsRmStackPortCount > ISS_MAX_POSSIBLE_STK_PORTS))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsRmStackPortCount);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmColdStandby
 Input       :  The Indices

                The Object 
                testValFsRmColdStandby
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmColdStandby (UINT4 *pu4ErrorCode, INT4 i4TestValFsRmColdStandby)
{
    if ((i4TestValFsRmColdStandby != RM_COLDSTANDBY_ENABLE) &&
        (i4TestValFsRmColdStandby != RM_COLDSTANDBY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmModuleTrc
 Input       :  The Indices

                The Object 
                testValFsRmModuleTrc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmModuleTrc (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRmModuleTrc)
{
    if (u4TestValFsRmModuleTrc > RM_MOD_ALL_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RM_INVALID_MODULE_DEBUG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmProtocolRestartFlag
 Input       :  The Indices

                The Object 
                testValFsRmProtocolRestartFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmProtocolRestartFlag (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsRmProtocolRestartFlag)
{
    if ((i4TestValFsRmProtocolRestartFlag != RM_RESTART_ENABLE) &&
        (i4TestValFsRmProtocolRestartFlag != RM_RESTART_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmProtocolRestartRetryCnt
 Input       :  The Indices

                The Object 
                testValFsRmProtocolRestartRetryCnt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmProtocolRestartRetryCnt (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValFsRmProtocolRestartRetryCnt)
{
    if (u4TestValFsRmProtocolRestartRetryCnt > RM_MAX_RESTART_CNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

          /* HITLESS RESTART */
/****************************************************************************
 Function    :  nmhTestv2FsRmHitlessRestartFlag
 Input       :  The Indices

                The Object
                testValFsRmHitlessRestartFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmHitlessRestartFlag (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsRmHitlessRestartFlag)
{
    if (i4TestValFsRmHitlessRestartFlag == RM_HR_STATUS_STORE)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmIpAddress
 Input       :  The Indices

                The Object 
                testValFsRmIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmIpAddress (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRmIpAddress)
{
    if (u4TestValFsRmIpAddress == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    if (!
        (((ISS_IS_ADDR_CLASS_A (u4TestValFsRmIpAddress))
          || (ISS_IS_ADDR_CLASS_B (u4TestValFsRmIpAddress))
          || (ISS_IS_ADDR_CLASS_C (u4TestValFsRmIpAddress)))
         && (ISS_IS_ADDR_VALID (u4TestValFsRmIpAddress))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsRmSubnetMask
 Input       :  The Indices

                The Object 
                testValFsRmSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmSubnetMask (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRmSubnetMask)
{

    if (IssValidateSubnetMask (u4TestValFsRmSubnetMask) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRmStackInterface
 Input       :  The Indices

                The Object 
                testValFsRmStackInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmStackInterface (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsRmStackInterface)
{
    if (pTestValFsRmStackInterface->i4_Length >= ISS_RM_INTERFACE_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (ATOI (pTestValFsRmStackInterface->pu1_OctetList + 3) >
        SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

#ifdef L2RED_TEST_WANTED
/****************************************************************************
 Function    :  nmhTestv2FsRmDynamicSyncAuditTrigger
 Input       :  The Indices

                The Object
                testValFsRmDynamicSyncAuditTrigger
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmDynamicSyncAuditTrigger (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValFsRmDynamicSyncAuditTrigger)
{
    UINT4               u4RmAppId = 0;

    if ((u4TestValFsRmDynamicSyncAuditTrigger <= 0) ||
        (u4TestValFsRmDynamicSyncAuditTrigger > 0xFFFF))
    {
        CLI_SET_ERR (CLI_RM_DYN_AUDIT_NOT_COMPLETE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsRmDynamicSyncAuditTrigger &
        ((UINT4) (~(VCM_MOD | LA_MOD | VLAN_MOD | ACL_MOD | CFA_MOD | MBSM_MOD |
                    SNTP_MOD | OSPF_MOD | RIP_MOD | BFD_MOD | ISIS_MOD | ARP_MOD
                    | RTM_MOD | RTM6_MOD | ND6_MOD | OSPF3_MOD))))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    for (u4RmAppId = 0; u4RmAppId < RM_MAX_APPS; u4RmAppId++)
    {

        if (gRmDynAuditInfo[u4RmAppId].u4AuditStatus == RM_DYN_AUDIT_INPROGRESS)
        {
            CLI_SET_ERR (CLI_RM_DYN_AUDIT_NOT_COMPLETE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
#endif

/****************************************************************************
 Function    :  nmhTestv2FsRmCopyPeerSyLogFile
 Input       :  The Indices

                The Object
                testValFsRmCopyPeerSyLogFile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmCopyPeerSyLogFile (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsRmCopyPeerSyLogFile)
{
    if (i4TestValFsRmCopyPeerSyLogFile == RM_COPY_STATUS_FAILURE ||
        i4TestValFsRmCopyPeerSyLogFile == RM_COPY_STATUS_INPROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (RM_GET_NODE_STATE () == RM_STANDBY)
    {
        if (gRmInfo.u1SysLogFileStatus == RM_COPY_STATUS_INPROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRmHbInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmHbInterval (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmPeerDeadInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmPeerDeadInterval (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmPeerDeadIntMultiplier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmPeerDeadIntMultiplier (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmTrcLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmTrcLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmForceSwitchoverFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmForceSwitchoverFlag (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmStatsSyncMsgTxCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsSyncMsgTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsSyncMsgTxCount (UINT4 *pu4RetValFsRmStatsSyncMsgTxCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsSyncMsgTxCount = RM_TX_STAT_SYNC_MSG_SENT ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStatsSyncMsgTxFailedCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsSyncMsgTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsSyncMsgTxFailedCount (UINT4
                                     *pu4RetValFsRmStatsSyncMsgTxFailedCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsSyncMsgTxFailedCount = RM_TX_STAT_SYNC_MSG_FAILED ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStatsSyncMsgRxCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsSyncMsgRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsSyncMsgRxCount (UINT4 *pu4RetValFsRmStatsSyncMsgRxCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsSyncMsgRxCount = RM_RX_STAT_SYNC_MSG_RECVD ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStatsSyncMsgProcCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsSyncMsgProcCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsSyncMsgProcCount (UINT4 *pu4RetValFsRmStatsSyncMsgProcCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsSyncMsgProcCount = RM_RX_STAT_SYNC_MSG_PROCESSED ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStatsSyncMsgMissedCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsSyncMsgMissedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsSyncMsgMissedCount (UINT4 *pu4RetValFsRmStatsSyncMsgMissedCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsSyncMsgMissedCount = RM_RX_STAT_SYNC_MSG_MISSED ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStatsConfSyncMsgFailCount
 Input       :  The Indices

                The Object 
                retValFsRmStatsConfSyncMsgFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStatsConfSyncMsgFailCount (UINT4
                                     *pu4RetValFsRmStatsConfSyncMsgFailCount)
{
    RM_LOCK ();
    *pu4RetValFsRmStatsConfSyncMsgFailCount =
        RM_RX_STAT_STATIC_CONF_SYNC_FAILED ();
    RM_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRmPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRmPeerTable
 Input       :  The Indices
                FsRmPeerSwitchId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRmPeerTable (INT4 i4FsRmPeerSwitchId)
{
#ifdef MBSM_WANTED
    tPeerRecEntry      *pPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {

        if ((i4FsRmPeerSwitchId < 0)
            || (i4FsRmPeerSwitchId > (MBSM_MAX_LC_SLOTS - 1)))
        {
            return SNMP_FAILURE;
        }
        if (RmGetPeerEntry (i4FsRmPeerSwitchId, &pPeerEntry) != RM_TRUE)
        {
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4FsRmPeerSwitchId);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRmPeerTable
 Input       :  The Indices
                FsRmPeerSwitchId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRmPeerTable (INT4 *pi4FsRmPeerSwitchId)
{
    tPeerRecEntry      *pPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (RmGetFirstPeerEntry (&pPeerEntry) == RM_TRUE)
        {
            *pi4FsRmPeerSwitchId = (INT4) pPeerEntry->u4SlotIndex;
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRmPeerTable
 Input       :  The Indices
                FsRmPeerSwitchId
                nextFsRmPeerSwitchId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRmPeerTable (INT4 i4FsRmPeerSwitchId,
                              INT4 *pi4NextFsRmPeerSwitchId)
{

    tPeerRecEntry      *pPeerEntry = NULL;
    tPeerRecEntry      *pNextPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        RmGetPeerEntry ((UINT4) i4FsRmPeerSwitchId, &pPeerEntry);
        RmGetNextPeerEntry (pPeerEntry, &pNextPeerEntry);
        if (pNextPeerEntry != NULL)
        {
            *pi4NextFsRmPeerSwitchId = (INT4) pNextPeerEntry->u4SlotIndex;
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmPeerStackIpAddr
 Input       :  The Indices
                FsRmPeerSwitchId

                The Object
                retValFsRmPeerStackIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerStackIpAddr (INT4 i4FsRmPeerSwitchId,
                           UINT4 *pu4RetValFsRmPeerStackIpAddr)
{
    tPeerRecEntry      *pPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        RmGetPeerEntry ((UINT4) i4FsRmPeerSwitchId, &pPeerEntry);

        if (pPeerEntry != NULL)
        {
            *pu4RetValFsRmPeerStackIpAddr = pPeerEntry->u4StackIp;
            return SNMP_SUCCESS;
        }

        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFsRmPeerStackMacAddr
 Input       :  The Indices
                FsRmPeerSwitchId

                The Object
                retValFsRmPeerStackMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmPeerStackMacAddr (INT4 i4FsRmPeerSwitchId,
                            tMacAddr * pRetValFsRmPeerStackMacAddr)
{
    tPeerRecEntry      *pPeerEntry = NULL;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        RmGetPeerEntry ((UINT4) i4FsRmPeerSwitchId, &pPeerEntry);

        if (pPeerEntry != NULL)
        {
            MEMCPY (pRetValFsRmPeerStackMacAddr, pPeerEntry->au4StackMac,
                    MAC_ADDR_LEN);
            return SNMP_SUCCESS;
        }

        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFsRmTrapSwitchId
 Input       :  The Indices

                The Object
                retValFsRmTrapSwitchId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmTrapSwitchId (INT4 *pi4RetValFsRmTrapSwitchId)
{
    *pi4RetValFsRmTrapSwitchId = (INT4) gRmInfo.u4TrapSwitchId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmSwitchId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmSwitchId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmConfiguredState
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmConfiguredState (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmStackPortCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmStackPortCount (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmColdStandby
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmColdStandby (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmModuleTrc
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmModuleTrc (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmProtocolRestartFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmProtocolRestartFlag (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmProtocolRestartRetryCnt
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmProtocolRestartRetryCnt (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmHitlessRestartFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmHitlessRestartFlag (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRmSwitchoverTimeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRmSwitchoverTimeTable
 Input       :  The Indices
                FsRmAppId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRmSwitchoverTimeTable (INT4 i4FsRmAppId)
{
    if ((i4FsRmAppId < RM_APP_ID) || (i4FsRmAppId >= RM_MAX_APPS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRmSwitchoverTimeTable
 Input       :  The Indices
                FsRmAppId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRmSwitchoverTimeTable (INT4 *pi4FsRmAppId)
{
    *pi4FsRmAppId = gaRmSwitchOverTime[0].u4AppId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRmSwitchoverTimeTable
 Input       :  The Indices
                FsRmAppId
                nextFsRmAppId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRmSwitchoverTimeTable (INT4 i4FsRmAppId,
                                        INT4 *pi4NextFsRmAppId)
{
    if ((i4FsRmAppId + 1) >= RM_MAX_APPS)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsRmAppId = gaRmSwitchOverTime[i4FsRmAppId + 1].u4AppId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmAppName
 Input       :  The Indices
                FsRmAppId

                The Object 
                retValFsRmAppName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmAppName (INT4 i4FsRmAppId,
                   tSNMP_OCTET_STRING_TYPE * pRetValFsRmAppName)
{
    pRetValFsRmAppName->i4_Length =
        STRLEN (gaRmSwitchOverTime[i4FsRmAppId].au1AppName);
    MEMCPY (pRetValFsRmAppName->pu1_OctetList,
            gaRmSwitchOverTime[i4FsRmAppId].au1AppName,
            pRetValFsRmAppName->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRmEntryTime
 Input       :  The Indices
                FsRmAppId

                The Object 
                retValFsRmEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmEntryTime (INT4 i4FsRmAppId, UINT4 *pu4RetValFsRmEntryTime)
{
    *pu4RetValFsRmEntryTime = gaRmSwitchOverTime[i4FsRmAppId].u4EntryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmExitTime
 Input       :  The Indices
                FsRmAppId

                The Object 
                retValFsRmExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmExitTime (INT4 i4FsRmAppId, UINT4 *pu4RetValFsRmExitTime)
{
    *pu4RetValFsRmExitTime = gaRmSwitchOverTime[i4FsRmAppId].u4ExitTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmSwitchoverTime
 Input       :  The Indices
                FsRmAppId

                The Object 
                retValFsRmSwitchoverTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmSwitchoverTime (INT4 i4FsRmAppId, UINT4 *pu4RetValFsRmSwitchoverTime)
{
    *pu4RetValFsRmSwitchoverTime =
        gaRmSwitchOverTime[i4FsRmAppId].u4SwitchOverTime;
    return SNMP_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/* LOW LEVEL Routines for Table : FsRmDynamicSyncAuditTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRmDynamicSyncAuditTable
 Input       :  The Indices
                FsRmDynAuditAppId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRmDynamicSyncAuditTable (INT4 i4FsRmDynAuditAppId)
{
    if ((i4FsRmDynAuditAppId < RM_APP_ID)
        || (i4FsRmDynAuditAppId >= RM_MAX_APPS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRmDynamicSyncAuditTable
 Input       :  The Indices
                FsRmDynAuditAppId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRmDynamicSyncAuditTable (INT4 *pi4FsRmDynAuditAppId)
{
    *pi4FsRmDynAuditAppId = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRmDynamicSyncAuditTable
 Input       :  The Indices
                FsRmDynAuditAppId
                nextFsRmDynAuditAppId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRmDynamicSyncAuditTable (INT4 i4FsRmDynAuditAppId,
                                          INT4 *pi4NextFsRmDynAuditAppId)
{
    if ((i4FsRmDynAuditAppId + 1) >= RM_MAX_APPS)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsRmDynAuditAppId = (i4FsRmDynAuditAppId + 1);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmDynamicSyncAuditStatus
 Input       :  The Indices
                FsRmDynAuditAppId

                The Object
                retValFsRmDynamicSyncAuditStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmDynamicSyncAuditStatus (INT4 i4FsRmDynAuditAppId,
                                  INT4 *pi4RetValFsRmDynamicSyncAuditStatus)
{
    *pi4RetValFsRmDynamicSyncAuditStatus =
        (INT4) gRmDynAuditInfo[i4FsRmDynAuditAppId].u4AuditStatus;
    return SNMP_SUCCESS;
}
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRmIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmIpAddress (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmSubnetMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmSubnetMask (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRmStackInterface
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmStackInterface (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef L2RED_TEST_WANTED
/****************************************************************************
 Function    :  nmhDepv2FsRmDynamicSyncAuditTrigger
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmDynamicSyncAuditTrigger (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif
/****************************************************************************
 Function    :  nmhDepv2FsRmCopyPeerSyLogFile
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmCopyPeerSyLogFile (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmStaticBulkStatus
 Input       :  None
 Output      :  Static bulk update status
 Description :  This routine will fetch the Static bulk update status
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmStaticBulkStatus (INT4 *pi4RetValFsRmStaticBulkStatus)
{
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1NextBulkAppId = 0;

    /* Check whether the node status */
    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        /* If no peer is present mark the status as not started */
        if (RM_PEER_NODE_COUNT () == 0)
        {
            *pi4RetValFsRmStaticBulkStatus = RM_NOT_STARTED;
            return SNMP_SUCCESS;
        }

        /* check whether MSR Bulk update is enabled ,if so check MSR bulk
         * update status else fetch the next APP ID status */
        for (u1NextBulkAppId = RM_MSR_APP_ID; u1NextBulkAppId < RM_MAX_APPS;
             u1NextBulkAppId++)
        {
            bResult = OSIX_FALSE;
            OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdStsMask, u1NextBulkAppId,
                                     sizeof (tRmAppList), bResult);
            if (bResult == OSIX_TRUE)
            {
                break;
            }
        }

        bResult = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdSts, u1NextBulkAppId,
                                 sizeof (tRmAppList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* Mark the status as inprogress */
            *pi4RetValFsRmStaticBulkStatus = RM_STARTED;
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmStaticBulkStatus = RM_COMPLETED;
        }
    }
    else
    {
        /* For standby node check the Bulk Update in progress variable */
        if (gRmInfo.u1IsBulkUpdtInProgress == RM_TRUE)
        {
            /* Check whether the MSR bulk updates are completed
             * and mark the static Bulk update status for standby node */
            if (gRmInfo.u1LastBulkSyncAppId > RM_MSR_APP_ID)
            {
                /* Mark the status as completed */
                *pi4RetValFsRmStaticBulkStatus = RM_COMPLETED;
            }
            else
            {
                /* Mark the status as inprogress */
                *pi4RetValFsRmStaticBulkStatus = RM_STARTED;
            }
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmStaticBulkStatus = RM_COMPLETED;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmDynamicBulkStatus
 Input       :  None
 Output      :  Dynamic bulk update status
 Description :  This routine will fetch the Dynamic bulk update status
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmDynamicBulkStatus (INT4 *pi4RetValFsRmDynamicBulkStatus)
{
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1LastBulkAppId = 0;

    /* Check whether the node status */
    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        /* If no peer is present mark the status as not started */
        if (RM_PEER_NODE_COUNT () == 0)
        {
            *pi4RetValFsRmDynamicBulkStatus = RM_NOT_STARTED;
            return SNMP_SUCCESS;
        }

        /* Find the Last App  Id which requires Bulk Update from the
         * Bulk Update Status Mask */
        for (u1LastBulkAppId = RM_MAX_APPS - 1; u1LastBulkAppId > 0;
             u1LastBulkAppId--)
        {
            OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdStsMask, u1LastBulkAppId,
                                     sizeof (tRmAppList), bResult);
            if (bResult == OSIX_TRUE)
            {
                break;
            }
        }

        bResult = OSIX_FALSE;

        /* Check whether the last app bulk update is complete */
        OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdSts, u1LastBulkAppId,
                                 sizeof (tRmAppList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* Mark the status as inprogress */
            *pi4RetValFsRmDynamicBulkStatus = RM_STARTED;
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmDynamicBulkStatus = RM_COMPLETED;
        }
    }
    else
    {
        /* For standby node check the Bulk Update in progress variable */
        if (gRmInfo.u1IsBulkUpdtInProgress == RM_TRUE)
        {
            /* Mark the status as inprogress */
            *pi4RetValFsRmDynamicBulkStatus = RM_STARTED;
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmDynamicBulkStatus = RM_COMPLETED;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRmOverallBulkStatus
 Input       :  None
 Output      :  Overall bulk update status
 Description :  This routine will fetch the Overall bulk update status
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmOverallBulkStatus (INT4 *pi4RetValFsRmOverallBulkStatus)
{
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1LastBulkAppId = 0;

    /* Check whether the node status */
    if (RM_GET_NODE_STATE () == RM_ACTIVE)
    {
        /* If no peer is present mark the status as not started */
        if (RM_PEER_NODE_COUNT () == 0)
        {
            *pi4RetValFsRmOverallBulkStatus = RM_NOT_STARTED;
            return SNMP_SUCCESS;
        }

        /* Find the Last App  Id which requires Bulk Update from the
         * Bulk Update Status Mask */
        for (u1LastBulkAppId = RM_MAX_APPS - 1; u1LastBulkAppId > 0;
             u1LastBulkAppId--)
        {
            OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdStsMask, u1LastBulkAppId,
                                     sizeof (tRmAppList), bResult);
            if (bResult == OSIX_TRUE)
            {
                break;
            }
        }

        bResult = OSIX_FALSE;

        /* Check whether the last app bulk update is complete */
        OSIX_BITLIST_IS_BIT_SET (gRmInfo.RmBulkUpdSts, u1LastBulkAppId,
                                 sizeof (tRmAppList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* Mark the status as inprogress */
            *pi4RetValFsRmOverallBulkStatus = RM_STARTED;
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmOverallBulkStatus = RM_COMPLETED;
        }
    }
    else
    {
        /* For standby node check the Bulk Update in progress variable */
        if (gRmInfo.u1IsBulkUpdtInProgress == RM_TRUE)
        {
            /* Mark the status as inprogress */
            *pi4RetValFsRmOverallBulkStatus = RM_STARTED;
        }
        else
        {
            /* Mark the status as completed */
            *pi4RetValFsRmOverallBulkStatus = RM_COMPLETED;
        }
    }

    return SNMP_SUCCESS;
}
