/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmfiletr.c,v 1.51 2016/06/24 09:45:13 siva Exp $
*
* Description: Redundancy manager File Transfer implementation.
*********************************************************************/
#include "rmincs.h"
#include <signal.h>
#include <unistd.h>
#include "fssocket.h"
/* Event rcvd by server task when a new connection req is rcvd */
#define RM_FT_NEW_CONN_REQ  1
extern UINT1        gu1FirmwareImageName[ISS_STATIC_MAX_NAME_LEN];
extern INT1        *IssGetRestoreFileNameFromNvRam (VOID);
#if defined(CLI_WANTED) || defined(WEBNM_WANTED)
extern VOID 		FpamReInit (VOID);
#endif

VOID                FileRcvTaskMain (INT4 i4Arg);

tRcvFileInfo        RcvFileInfo[MAX_NO_OF_FILES] = {
    {"iss.conf", "MFR", 0},
    {"SlotModule.conf", "MSIR", 0},
    {"issnvram.txt", "MNV", 0},
    {"privil", "MPR", 0},
    {"users", "MUR", 0},
    {"system.size", "MSS", 0},
    {"server_key_1", "MSK1", 0},
    {"server_key_2", "MSK2", 0},
    {"fsir.log", "FSLG", 0},
	{"sslservcert", "SSL", 0},
	{"ospf3at", "OS3", 0}
};

VOID                SigPipeHndler (int signum);
VOID
SigPipeHndler (int signum)
{
    UNUSED_PARAM (signum);
    printf ("Caught a SIGPIPE\n");
    signal (SIGPIPE, SigPipeHndler);
}

/******************************************************************************
 * Function           : RmFtSrvTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Rm File Transfer Task main routine responsible
 *                      for transfering files between 2 nodes.
 ******************************************************************************/
VOID
RmFtSrvTaskMain (INT1 *pi1Arg)
{
    UINT4               u4Event;
    UINT4               u4RetVal;
    UINT4               u4ConnIdx;
    INT4                i4Index;

    UNUSED_PARAM (pi1Arg);
    OsixTskIdSelf (&RM_FT_SRV_TASK_ID);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* Static configuration files will be in /mnt/ path */

        for (i4Index = 0; i4Index <= RM_MAX_NUM_FILES - 2; i4Index++)
        {
            IssAppendFilePath (RcvFileInfo[i4Index].au1FileName);
        }

#if ((!defined L2RED_WANTED) && ((defined MBSM_WANTED) && (defined RM_WANTED)))

        /* Index - 6 means, NORMAL Image 
         * Index - 7 means, FALLBACK Image
         * Firmware image to be upgraded wil be in "/tmp/" path */

        SPRINTF ((CHR1 *) RcvFileInfo[6].au1FileName, "/%s/%s", "tmp",
                 "normal");
        SPRINTF ((CHR1 *) RcvFileInfo[7].au1FileName, "/%s/%s", "tmp",
                 "fallback");
#endif
    }
    /* main loop */
    while (1)
    {
        RM_RECV_EVENT (RM_FT_SRV_TASK_ID, RM_FT_NEW_CONN_REQ,
                       OSIX_WAIT, &u4Event);

        if (u4Event & RM_FT_NEW_CONN_REQ)
        {
            RM_TRC (RM_FT_TRC,
                    "File Transfer new connection request received\n");
#ifdef L2RED_WANTED
            if (RM_GET_NODE_STATE () != RM_ACTIVE)
            {
                continue;
            }
#endif
            if ((IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE) &&
                (RM_GET_NODE_STATE () != RM_STANDBY))
            {
                continue;
            }

            /* New connection request rcvd. Get the free conn index for
             * storing the RM FT client related params */
            if ((u4ConnIdx = RmFtGetFreeConnIdx ()) == RM_MAX_SESSIONS)
            {
                RM_TRC (RM_FAILURE_TRC,
                        "RM File Transfer free client session not available\n");
                continue;
            }

            /* Increment the File Transfer client session count by 1 */
            gRmInfo.u4FtConnCnt++;

            /* Update the File Transfer Client info */
            gRmInfo.RmFtConnTbl[u4ConnIdx].i4FtCliSockFd =
                RmAcceptIncomingConn (gRmInfo.i4FtSrvSockFd);
            SelAddFd (gRmInfo.i4FtSrvSockFd, RmFtNewConnReq);
            RmGetCliTaskName (u4ConnIdx);
            gRmInfo.RmFtConnTbl[u4ConnIdx].u4Status = RM_FT_CLIENT_ACTIVE;

            u4RetVal =
                OsixTskCrt ((UINT1 *)
                            &(gRmInfo.RmFtConnTbl[u4ConnIdx].au1TskName),
                            (UINT4) RM_FT_CLIENT_TASK_PRI |
                            (UINT4) OSIX_SCHED_OTHER,
                            OSIX_DEFAULT_STACK_SIZE * 4,
                            (OsixTskEntry) RmFtCliTaskMain,
                            (INT1 *) ((FS_ULONG) u4ConnIdx),
                            &gRmInfo.RmFtConnTbl[u4ConnIdx].u4TskId);
            if (u4RetVal != OSIX_SUCCESS)
            {
                RM_TRC1 (RM_FAILURE_TRC,
                         "Ft Client Task %s creation -- Failed\n",
                         gRmInfo.RmFtConnTbl[u4ConnIdx].au1TskName);
            }
            else
            {
                RM_TRC1 (RM_FT_TRC, "Ft Client Task %s created\n",
                         gRmInfo.RmFtConnTbl[u4ConnIdx].au1TskName);
            }
        }
    }
}

/******************************************************************************
 * Function           : RmFtSrvTaskInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Rm File Transfer Task initialization routine to open
 *                      a File transfer server socket. 
 ******************************************************************************/
UINT4
RmFtSrvTaskInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    gRmInfo.i4FtSrvSockFd = RmFtServSockInit ();
    if (gRmInfo.i4FtSrvSockFd == -1)
    {
        u4RetVal = RM_FAILURE;
    }

    if (u4RetVal != RM_FAILURE)
    {
        /* Add this SockFd to SELECT library */
        SelAddFd (gRmInfo.i4FtSrvSockFd, RmFtNewConnReq);
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmFtSrvTaskDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Rm File Transfer Task De-initialization routine to close
 *                      the File transfer server socket. 
 ******************************************************************************/
VOID
RmFtSrvTaskDeInit (VOID)
{
    INT4                i4RetVal;

    if (gRmInfo.i4FtSrvSockFd != -1)
    {
        /* Remove the FD from SELECT library */
        SelRemoveFd (gRmInfo.i4FtSrvSockFd);

        /* close the socket */
        i4RetVal = RmCloseSocket (gRmInfo.i4FtSrvSockFd);
        if (i4RetVal < 0)
        {
            RM_TRC (RM_FAILURE_TRC, "!!! FT socket close Failure !!!\n");
        }
    }
    gRmInfo.i4FtSrvSockFd = -1;
}

/******************************************************************************
 * Function           : RmFtNewConnReq
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a new connection
 *                      request is rcvd from the client. 
 ******************************************************************************/
VOID
RmFtNewConnReq (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (RM_SEND_EVENT (RM_FT_SRV_TASK_ID, RM_FT_NEW_CONN_REQ) == OSIX_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "Send Event to FT SRV task - Failed\n");
    }

}

/******************************************************************************
 * Function           : RmFtCliTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Rm File Transfer Client Task main routine responsible
 *                      for transfering files between 2 nodes.
 ******************************************************************************/
VOID
RmFtCliTaskMain (INT4 i4ConnIdx)
{
    RM_TRC (RM_FT_TRC, "Inside FT client task before recving the file\n");
    signal (SIGPIPE, SigPipeHndler);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        RmGetFileFromPeer (gRmInfo.RmFtConnTbl[i4ConnIdx].i4FtCliSockFd);
        RM_TRC (RM_FT_TRC, "RmFtCliTaskMain - Getting Files\n");
    }

    else if (RmSendFile (gRmInfo.RmFtConnTbl[i4ConnIdx].i4FtCliSockFd) ==
             RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmFtCliTaskMain- SendFile Failed\n");
    }

    /* Decrement the File Transfer client session count by 1 */
    gRmInfo.u4FtConnCnt--;

    gRmInfo.RmFtConnTbl[i4ConnIdx].u4Status = RM_FT_CLIENT_INACTIVE;
    OsixTskDel (gRmInfo.RmFtConnTbl[i4ConnIdx].u4TskId);
    gRmInfo.RmFtConnTbl[i4ConnIdx].u4TskId = 0;
}

/******************************************************************************
 * Function           : RmFtGetFreeConnIdx
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Free connection index. 
 * Action             : Responsible for finding the first free connection index 
 *                      in the client connection tbl.
 ******************************************************************************/
UINT4
RmFtGetFreeConnIdx (VOID)
{
    UINT4               u4ConnIdx;
    UINT4               u4FreeConnIdx = RM_MAX_SESSIONS;
    UINT4               u4TaskId = 0;
    UINT1               au1TskName[OSIX_NAME_LEN + 4];
    VOID               *pId = NULL;

    for (u4ConnIdx = 0; u4ConnIdx < RM_MAX_SESSIONS; u4ConnIdx++)
    {
        if (gRmInfo.RmFtConnTbl[u4ConnIdx].u4Status == RM_FT_CLIENT_INACTIVE)
        {
            MEMSET (au1TskName, '\0', OSIX_NAME_LEN + 4);
            SNPRINTF ((CHR1 *) au1TskName, sizeof (au1TskName), "%s%d",
                      RM_FT_CLIENT_TASK_NAME, u4ConnIdx);
            /* This Task resource find is needed to ensure no task 
             * exist in this name.
             * If the below check is not done, there is a possibility of
             * task presence in FSAP level and this may affect next 
             * file transfer when previous file transfer is completed but 
             * FSAP level task resource release is not done. */
            pId = (VOID *) &u4TaskId;
            if (OsixRscFind (au1TskName,
                             OSIX_TSK, (VOID *) &pId) == OSIX_FAILURE)
            {
                u4FreeConnIdx = u4ConnIdx;
                break;
            }
        }
    }
    return (u4FreeConnIdx);
}

/******************************************************************************
 * Function           : RmGetCliTaskName
 * Input(s)           : ConnIndex to which the TaskName needs to be formed.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE. 
 * Action             : Forms a task name for each client session.
 ******************************************************************************/
VOID
RmGetCliTaskName (UINT4 u4ConnIdx)
{
    UINT1               au1TskName[OSIX_NAME_LEN + 4];
    UINT1              *pu1TskName = gRmInfo.RmFtConnTbl[u4ConnIdx].au1TskName;

    MEMSET (au1TskName, '\0', OSIX_NAME_LEN + 4);
    SNPRINTF ((CHR1 *) au1TskName, sizeof (au1TskName), "%s%d",
              RM_FT_CLIENT_TASK_NAME, u4ConnIdx);
    MEMCPY (pu1TskName, au1TskName, OSIX_NAME_LEN + 4);

    return;
}

/******************************************************************************
 * Function           : RmRequestFile
 * Input(s)           : u1FileId - Identifier of the file to be requested.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to request for a file. 
 ******************************************************************************/
UINT4
RmRequestFile (UINT1 u1FileId)
{
    INT4                i4TskArg = 0;
    UINT4               u4RetVal;

    /* Task to request for SlotModule.conf file from ACTIVE node */
    i4TskArg = (INT1) u1FileId;
    if (OsixTskCrt (RcvFileInfo[u1FileId].ac1TaskName,
                    RCV_FILE_TASK_PRI,
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) FileRcvTaskMain,
                    (INT1 *) ((FS_ULONG) i4TskArg),
                    &RcvFileInfo[u1FileId].u4TskId) != OSIX_SUCCESS)
    {
        RM_TRC1 (RM_FAILURE_TRC,
                 "RmRequestFile - Task creation Failed for %s file..\n",
                 RcvFileInfo[u1FileId].au1FileName);
        u4RetVal = RM_FAILURE;
    }
    else
    {
        RM_TRC (RM_FT_TRC, "FileRcvTaskMain creation successful\n");
        u4RetVal = RM_SUCCESS;
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : FileRcvTaskMain
 * Input(s)           : i4Index - Identifier of the file requested.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task to receive the requested file. 
 ******************************************************************************/
VOID
FileRcvTaskMain (INT4 i4Index)
{
    INT4                i4ImageType = 0;
    UINT4               u4ActiveIP;
    UINT4               u4RetVal = ISS_FAILURE;

    signal (SIGPIPE, SigPipeHndler);

    if (RmRcvFile ((CONST CHR1 *) RcvFileInfo[i4Index].au1FileName)
        == RM_FAILURE)
    {
        RM_TRC1 (RM_FT_TRC, "ERROR: RmRcvFile Failed for file %s\n",
                 RcvFileInfo[i4Index].au1FileName);
    }
    else
    {
        RM_TRC1 (RM_FT_TRC, "Received File %s successfully\n",
                 RcvFileInfo[i4Index].au1FileName);
    }
 
    if (STRCMP (RcvFileInfo[i4Index].au1FileName,"ospf3at") != 0)
    {
        RM_LOCK ();
        gRmInfo.u1FileRcvCount++;
        RM_TRC1 (RM_FT_TRC, "Received File Count %d\n",
                             gRmInfo.u1FileRcvCount);
        if (gRmInfo.u1FileRcvCount == (MAX_NO_OF_FILES - MANDATORY_NO_FILES))
#ifdef L2RED_WANTED
            {
                /* File count is incremented after each file transfer. If the maximum
                 * number of files is reached (excluding iss.conf)
                 */
                RM_TRC1 (RM_FT_TRC, "Max Received File Count reached %d\n",
                                        gRmInfo.u1FileRcvCount);
        RM_TRC (RM_CRITICAL_TRC,
                "### FILE TRANSFER COMPLETED ### \n");
                gRmInfo.u1FileRcvCount = 0;
                RmNotifyProtocols (RM_FILE_TRANSFER_COMPLETE);
            }
#endif 
        RM_UNLOCK ();
    }

#if defined(CLI_WANTED) || defined(WEBNM_WANTED)
	/*Inform FPAM module in Standby node to populate Users file received from Active node*/
    if (STRCMP (RcvFileInfo[i4Index].au1FileName,"users") == 0)
    {
		FpamReInit();
    }
#endif

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* Index - 6 means, NORMAL Image 
         * Index - 7 means, FALLBACK Image 
         * After received firmware image from Master,
         * passing that buffer to kernel */

        u4ActiveIP = RM_GET_ACTIVE_NODE_ID ();

        if (i4Index == 6)
        {
            i4ImageType = ISS_FLASH_NORMAL;
        }
        else if (i4Index == 7)
        {
            i4ImageType = ISS_FLASH_FALLBACK;
        }
        if ((i4ImageType == ISS_FLASH_NORMAL) ||
            (i4ImageType == ISS_FLASH_FALLBACK))
        {
            u4RetVal = (UINT4) IssCustReadFile (gu1FirmwareImageName);
            if (u4RetVal == ISS_SUCCESS)
            {
                StackSendSlaveStatustoMaster (u4ActiveIP, STACK_UPGRADE_SUCC);
                RM_TRC (RM_FT_TRC, "Firmware upgradation successful \r\n");
            }
            else if (u4RetVal == ISS_FAILURE)
            {
                StackSendSlaveStatustoMaster (u4ActiveIP, STACK_UPGRADE_FAIL);
                RM_TRC (RM_FT_TRC, " Firmware upgradation failed ! \r\n");
            }
        }
    }

    RcvFileInfo[i4Index].u4TskId = 0;
}

/******************************************************************************
 * Function           : RmSendFileToPeers
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send files
 ******************************************************************************/
VOID
RmSendFileToPeers (VOID)
{
    tPeerRecEntry      *pPeerEntry;
    INT4                i4TskArg = 0;
    UINT4               u4SwitchId = 0;
    UINT1               au1TaskName[MAX_TASK_NAME_LEN];
    /* Task to request for SlotModule.conf file from ACTIVE node */

    MEMSET (au1TaskName, 0, MAX_TASK_NAME_LEN);
    for (i4TskArg = 0; i4TskArg <= MAX_NO_OF_FILES; i4TskArg++)
    {
        u4SwitchId = RmGetSwitchIdFromTskId ((UINT4) i4TskArg);
        if (RmGetPeerEntry (u4SwitchId, &pPeerEntry) == RM_FALSE)
        {
            continue;
        }

        MEMSET (au1TaskName, 0, MAX_TASK_NAME_LEN);
        SPRINTF ((CHR1 *) au1TaskName, "%s%d", "MS", i4TskArg);
        if (OsixTskCrt (au1TaskName,
                        RCV_FILE_TASK_PRI,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) RmTaskSendFiles,
                        (INT1 *) ((FS_ULONG) i4TskArg),
                        &gau4RmFilesTrTaskId[i4TskArg]) != OSIX_SUCCESS)
        {
            RM_TRC2 (RM_FAILURE_TRC,
                     " - Task creation Failed for %ld  - %s file..\n",
                     i4TskArg, au1TaskName);
        }
        else
        {
            RM_TRC (RM_FT_TRC, "FileRcvTaskMain creation successful\n");
        }
    }
    return;
}

/******************************************************************************
 * Function           : RmTaskSendFiles
 * Input(s)           : i4TskIndex
 * Output(s)          : None.
 * Returns            : None
 * Action             : Task to Send Files 
 ******************************************************************************/
VOID
RmTaskSendFiles (INT4 i4TskIndex)
{
    if (RmSendFiletoAttachedPeer (i4TskIndex) == RM_FAILURE)
    {
        RM_TRC1 (RM_FT_TRC, "ERROR: RmTaskSendFiles Failed for task %ld\n",
                 gau4RmFilesTrTaskId[i4TskIndex]);
    }
    else
    {
        RM_TRC1 (RM_FT_TRC, "RmTaskSendFiles File %ld successfully\n",
                 gau4RmFilesTrTaskId[i4TskIndex]);
    }

    if (gau4RmFilesTrTaskId[i4TskIndex] != 0)
    {
        OsixTskDel (gau4RmFilesTrTaskId[i4TskIndex]);
        gau4RmFilesTrTaskId[i4TskIndex] = 0;
    }
    return;
}

/******************************************************************************
 * Function           : RmTaskSendImage
 * Input(s)           : i4TskIndex
 * Output(s)          : None.
 * Returns            : None
 * Action             : Task to Send Firmware Image 
 ******************************************************************************/
VOID
RmTaskSendImage (INT4 i4TskIndex)
{
    if (RmSendFirmwareImage (i4TskIndex) == RM_FAILURE)
    {
        RM_TRC1 (RM_SOCK_CRIT_TRC, "ERROR: RmRcvFile Failed for task %ld\n",
                 gau4RmFilesTrTaskId[i4TskIndex]);
    }
    else
    {
        RM_TRC1 (RM_SOCK_CRIT_TRC, "Received File %ld successfully\n",
                 gau4RmFilesTrTaskId[i4TskIndex]);
    }

    if (gau4RmFilesTrTaskId[i4TskIndex] != 0)
    {
        OsixTskDel (gau4RmFilesTrTaskId[i4TskIndex]);
        gau4RmFilesTrTaskId[i4TskIndex] = 0;
    }
    return;
}

/******************************************************************************
 * Function           : RmSendFileToNewPeer
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send files to peer attched after a
 *                      configuration save.
 ******************************************************************************/
VOID
RmSendFilesToNewPeer (UINT4 u4SwitchId)
{
    UINT4               u4TaskId = 0;
    UINT4               u4TaskArg = 0;
    UINT1               au1TaskName[MAX_TASK_NAME_LEN];

    u4TaskId = RmGetTaskIdFromSwitchId (u4SwitchId);
    for (u4TaskArg = u4TaskId; u4TaskArg <= (u4TaskId + RM_MAX_NUM_FILES - 2);
         u4TaskArg++)
    {
        MEMSET (au1TaskName, 0, MAX_TASK_NAME_LEN);
        SPRINTF ((CHR1 *) au1TaskName, "%s%u", "MS", u4TaskArg);
        if (OsixTskCrt (au1TaskName,
                        RCV_FILE_TASK_PRI,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) RmTaskSendFiles,
                        (INT1 *) ((FS_ULONG) u4TaskArg),
                        &gau4RmFilesTrTaskId[u4TaskArg]) != OSIX_SUCCESS)
        {
            RM_TRC1 (RM_FAILURE_TRC,
                     " - Task creation Failed for %ld - %s file..\n",
                     au1TaskName);
        }
        else
        {
            RM_TRC (RM_FT_TRC, "FileRcvTaskMain creation successful\n");
        }
    }
}

/******************************************************************************
 * Function           : RmSendImagetoAttachedPeers
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send files
 ******************************************************************************/
VOID
RmSendImagetoAttachedPeers (VOID)
{
    tPeerRecEntry      *pPeerEntry;
    INT4                i4TskArg = 0;
    UINT4               u4SwitchId = 0;
    UINT1               au1TaskName[MAX_TASK_NAME_LEN];
    /* Task to request for SlotModule.conf file from ACTIVE node */

    MEMSET (au1TaskName, 0, MAX_TASK_NAME_LEN);
    /* FS_ERIC PRI CHANGE - START */
    for (i4TskArg = 0; i4TskArg <= MAX_NO_OF_FILES; i4TskArg++)
    {
        u4SwitchId = RmGetSwitchIdFromTskId ((UINT4) i4TskArg);
        if (RmGetPeerEntry (u4SwitchId, &pPeerEntry) == RM_FALSE)
        {
            continue;
        }

        MEMSET (au1TaskName, 0, MAX_TASK_NAME_LEN);
        SPRINTF ((CHR1 *) au1TaskName, "%s%d", "MS", i4TskArg);
        if (OsixTskCrt (au1TaskName,
                        RCV_FILE_TASK_PRI,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) RmTaskSendImage,
                        (INT1 *) ((FS_ULONG) i4TskArg),
                        &gau4RmFilesTrTaskId[i4TskArg]) != OSIX_SUCCESS)
        {
            RM_TRC1 (RM_FAILURE_TRC,
                     " - Task creation Failed for %ld - %s file..\n",
                     au1TaskName);
        }
        else
        {
            RM_TRC (RM_FT_TRC, "FileRcvTaskMain creation successful\n");
        }
    }
    return;
}

/******************************************************************************
 * Function           : RmUploadFile
 * Input(s)           : u1FileId - Identifier of the file to be requested.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to upload a file from Standby to Active
 ******************************************************************************/
UINT4
RmUploadFile (UINT1 u1FileId)
{
    INT4                i4TskArg = 0;
    UINT4               u4RetVal = RM_FAILURE;
    if (u1FileId != FILE_SYSLOG_FILE)
    {
        /* Currently SYSLOG FILE alone is transferred from Standby to Active */
        return u4RetVal;
    }
    /* Task to request for SlotModule.conf file from ACTIVE node */
    i4TskArg = (INT1) u1FileId;
    if (OsixTskCrt (RcvFileInfo[u1FileId].ac1TaskName,
                    RCV_FILE_TASK_PRI,
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) RmFileStandbyToActiveTaskMain,
                    (INT1 *) ((FS_ULONG) i4TskArg),
                    &RcvFileInfo[u1FileId].u4TskId) != OSIX_SUCCESS)
    {
        RM_TRC1 (RM_FAILURE_TRC,
                 "RmUploadFile - Task creation Failed for %s file..\n",
                 RcvFileInfo[u1FileId].au1FileName);
        u4RetVal = RM_FAILURE;
    }
    else
    {
        RM_TRC (RM_FT_TRC, "FileStandbySendTaskMain creation successful\n");
        u4RetVal = RM_SUCCESS;
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmFileStandbyToActiveTaskMain
 * Input(s)           : u1FileId - Identifier of the file to be requested.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Task entry func to upload a file from Standby to Active
 ******************************************************************************/
VOID
RmFileStandbyToActiveTaskMain (INT4 i4Index)
{
    UINT1               au1LogFile[20];
    signal (SIGPIPE, SigPipeHndler);
#ifdef RM_WANTED
    SPRINTF ((CHR1 *) au1LogFile, "fsir.log");
#else
    SPRINTF ((CHR1 *) au1LogFile, "fsir.log.%d", getpid ());
#endif
    if (RmStandbySendFile ((CONST CHR1 *) au1LogFile) == RM_FAILURE)
    {
        RM_TRC1 (RM_FT_TRC, "ERROR: RmStandbySendFile Failed for file %s\n",
                 RcvFileInfo[i4Index].au1FileName);
    }
    else
    {
        RM_TRC1 (RM_FT_TRC, "Received File %s successfully\n",
                 RcvFileInfo[i4Index].au1FileName);
    }
    if (RcvFileInfo[i4Index].u4TskId != 0)
    {
        OsixTskDel (RcvFileInfo[i4Index].u4TskId);
        RcvFileInfo[i4Index].u4TskId = 0;
    }
}
