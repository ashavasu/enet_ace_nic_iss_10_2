/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rmtmr.c,v 1.22 2014/09/24 13:27:02 siva Exp $
*
* Description: Timer related functions of Redundancy manager module.
*********************************************************************/
#include "rmincs.h"
#include "rmglob.h"

PRIVATE VOID        RmTmrInitTmrDesc (VOID);
/******************************************************************************
 * Function           : RmTmrInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create RM timer.
 ******************************************************************************/
UINT4
RmTmrInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    RM_TRC (RM_TMR_TRC, "RmTmrInit: ENTRY\r\n");
    if (TmrCreateTimerList ((CONST UINT1 *) RM_TASK_NAME,
                            RM_TMR_EXPIRY_EVENT, NULL,
                            (tTimerListId *) & (gRmInfo.TmrListId))
        != TMR_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmTmrInit: RM core module timer list creation Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    RmTmrInitTmrDesc ();
    RM_TRC (RM_TMR_TRC, "RmTmrInit: EXIT\r\n");
    return (u4RetVal);
}

/******************************************************************************
 * Function           : RmTmrDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to delete RM timer list
 ******************************************************************************/
UINT4
RmTmrDeInit (VOID)
{
    UINT4               u4RetVal = RM_SUCCESS;

    RM_TRC (RM_TMR_TRC, "RmTmrDeInit: ENTRY\r\n");
    if (gRmInfo.TmrListId == (UINT4) 0)
    {
        u4RetVal = RM_FAILURE;
        RM_TRC (RM_TMR_TRC, "RmTmrDeInit: "
                "Timer is deleted already - EXIT\r\n");
    }
    if ((u4RetVal == RM_SUCCESS) &&
        (TmrDeleteTimerList (gRmInfo.TmrListId) != TMR_SUCCESS))
    {
        RM_TRC (RM_FAILURE_TRC,
                "RmTmrDeInit: "
                "RM core module timer list deletion Failure !!!\n");
        u4RetVal = RM_FAILURE;
    }
    gRmInfo.TmrListId = (UINT4) 0;
    RM_TRC (RM_TMR_TRC, "RmTmrDeInit: EXIT\r\n");
    return (u4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RmTmrInitTmrDesc
 *    
 *    DESCRIPTION      : This function intializes the timer desc for all
 *                       the timers in RM module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
RmTmrInitTmrDesc (VOID)
{
    RM_TRC (RM_TMR_TRC, "RmTmrInitTmrDesc: ENTRY\r\n");
    gRmInfo.aTmrDesc[RM_SEQ_RECOV_TIMER].TmrExpFn = RmHandleSeqRecovTmrExpiry;
    gRmInfo.aTmrDesc[RM_ACK_RECOV_TIMER].TmrExpFn = RmHandleAckRecovTmrExpiry;
    gRmInfo.aTmrDesc[RM_CONN_RETRY_TIMER].TmrExpFn = RmHandleConnRetryTmrExpiry;
    gRmInfo.aTmrDesc[RM_BULK_UPDATE_TIMER].TmrExpFn =
        RmHandleBulkUpdateTmrExpiry;
    gRmInfo.aTmrDesc[RM_CPU_TRAFFIC_CTRL_TIMER].TmrExpFn =
        RmHandleCpuTrafficCtrlTmrExpiry;
#ifdef L2RED_TEST_WANTED
    gRmInfo.aTmrDesc[RM_DYN_SYNC_AUDIT_TIMER].TmrExpFn =
        RmHandleDynSynchAuditTmrExpiry;
#endif
    RM_TRC (RM_TMR_TRC, "RmTmrInitTmrDesc: EXIT\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RmTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       message is received by RM core task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
RmTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;

    RM_TRC (RM_TMR_TRC, "RmTmrExpHandler: ENTRY\r\n");
    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gRmInfo.TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        RM_TRC1 (RM_TMR_TRC, "RmTmrExpHandler: "
                 "Timer Id=%d expired\r\n", u1TimerId);
        /* The timer function does not take any parameter. */
        (*(gRmInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
    }
    RM_TRC (RM_TMR_TRC, "RmTmrExpHandler: EXIT\r\n");
}

/******************************************************************************
 * Function           : RmTmrStartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to start the RM core module timers
 ******************************************************************************/
UINT4
RmTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    RM_TRC (RM_TMR_TRC, "RmTmrStartTimer: ENTRY\r\n");

    if (TmrStart (gRmInfo.TmrListId,
                  &(gRmInfo.RmTmrBlk[u1TmrId]),
                  u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        RM_TRC1 (RM_TMR_TRC, "RmTmrStartTimer: Timer Id=%d "
                 "started successfully - EXIT\r\n", u1TmrId);
        return RM_SUCCESS;
    }
    RM_TRC (RM_TMR_TRC, "RmTmrStartTimer: Timer start failed !!!\r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmTmrStopTimer
 * Input(s)           : u1TmrId - Timer id
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to stop the RM core module timers
 ******************************************************************************/
UINT4
RmTmrStopTimer (UINT1 u1TmrId)
{
    RM_TRC (RM_TMR_TRC, "RmTmrStopTimer: ENTRY\r\n");

    if (TmrStop (gRmInfo.TmrListId,
                 &(gRmInfo.RmTmrBlk[u1TmrId])) != TMR_FAILURE)
    {
        RM_TRC1 (RM_TMR_TRC, "RmTmrStopTimer: Timer Id=%d "
                 "started successfully - EXIT\r\n", u1TmrId);
        return RM_SUCCESS;
    }
    RM_TRC (RM_TMR_TRC, "RmTmrStopTimer: Timer stopping failed !!!\r\n");
    return RM_FAILURE;
}

/******************************************************************************
 * Function           : RmHandleSeqRecovTmrExpiry
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This routine handles the sequence recovery timer 
 *                      expiry.
 ******************************************************************************/
VOID
RmHandleSeqRecovTmrExpiry (VOID *pArg)
{
    UINT4               u4LastSeqNum = 0;
    tRmNotificationMsg  NotifMsg;

    UNUSED_PARAM (pArg);

    RM_TRC (RM_SYNCUP_TRC | RM_TMR_TRC,
            "Sequence Number Recovery Timer Expiry.\n");
    if (gRmInfo.bIsRxBuffFull == RM_TRUE)
    {
        RmTmrStartTimer (RM_SEQ_RECOV_TIMER, RM_SEQ_RECOV_TMR_INTERVAL);
        RM_TRC1 (RM_CRITICAL_TRC, "RmHandleSeqRecovTmrExpiry: "
                 "Sequence Number (%d) is expected from Active\n",
                 RM_RX_GETNEXT_VALID_SEQNUM ());
        return;
    }

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
    /* Proceed with the next sequence number */
    RM_RX_INCR_LAST_SEQNUM_PROCESSED ();
    u4LastSeqNum = RM_RX_LAST_SEQ_NUM_PROCESSED ();
    RM_TRC1 (RM_CRITICAL_TRC, "RmHandleSeqRecovTmrExpiry: "
             "Sequence Number (%d) is ignored\n", u4LastSeqNum);

    /* Update Statistics */
    RM_RX_STAT_SYNC_MSG_MISSED ()++;

    /* Send trap */
    NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
    NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
    NotifMsg.u4AppId = RM_APP_ID;
    NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
    NotifMsg.u4Status = RM_FAILED;
    NotifMsg.u4Error = RM_PROCESS_FAIL;
    UtlGetTimeStr (NotifMsg.ac1DateTime);
    SPRINTF (NotifMsg.ac1ErrorStr,
             "Sequence Number %u is not received by standby.", u4LastSeqNum);
    RmTrapSendNotifications (&NotifMsg);

    RM_RX_SEQ_MISSMATCH_FLG () = RM_FALSE;
    RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
    RM_TRC (RM_SYNCUP_TRC, "RmHandleSeqRecovTmrExpiry: UNLOCKING Rx Buff\r\n");

    RmRxProcessBufferedPkt ();
}

/******************************************************************************
 * Function           : RmHandleConnRetryTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This routine handles the connection retry 
 *                      timer expiry.
 ******************************************************************************/
VOID
RmHandleConnRetryTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    if (RM_SEND_EVENT (RM_TASK_ID, RM_PKT_FROM_APP) != OSIX_SUCCESS)
    {
        RM_TRC (RM_FAILURE_TRC, "SendEvent failed in "
                "RmHandleConnRetryTmrExpiry\n");
    }
}

/******************************************************************************
 * Function           : RmHandleCpuTrafficCtrlTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the connection retry
 *                      timer expiry.
 ******************************************************************************/
VOID
RmHandleCpuTrafficCtrlTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    if (gu4TrafficToCpuAllowed == RM_FALSE)
    {
#ifdef NPAPI_WANTED
        RM_UNLOCK ();
        RmFsNpHRSetStdyStInfo (NP_RM_HR_RESUME_COPY_TO_CPU_TRAFFIC, NULL);
        RM_LOCK ();
#endif
        gu4TrafficToCpuAllowed = RM_TRUE;
    }
}

/******************************************************************************
 * Function           : RmHandleAckRecovTmrExpiry
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : This routine handles the acknowledgement recovery 
 *                      timer expiry.
 ******************************************************************************/
VOID
RmHandleAckRecovTmrExpiry (VOID *pArg)
{
    UINT4               u4LastSeqNum = 0;
    tRmNotificationMsg  NotifMsg;

    UNUSED_PARAM (pArg);
    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

    u4LastSeqNum = RM_RX_LAST_SEQ_NUM_PROCESSED ();
    /* Proceed with next RX Buffered message processing */
    RM_TRC2 (RM_CRITICAL_TRC, "RmHandleAckRecovTmrExpiry: ACK not received "
             "for Seq# %d, from AppId=%s \r\n",
             u4LastSeqNum, gapc1AppName[RM_RX_LAST_APPID_PROCESSED ()]);

    NotifMsg.u4NodeId = RM_GET_SELF_NODE_ID ();
    NotifMsg.u4State = (UINT4) RM_GET_NODE_STATE ();
    NotifMsg.u4AppId = RM_APP_ID;
    NotifMsg.u4Operation = RM_NOTIF_SYNC_UP;
    NotifMsg.u4Status = RM_FAILED;
    NotifMsg.u4Error = RM_PROCESS_FAIL;
    UtlGetTimeStr (NotifMsg.ac1DateTime);
    SPRINTF (NotifMsg.ac1ErrorStr,
             "Acknowledgement not received for seq# %u from protocol=%s",
             u4LastSeqNum, gapc1AppName[RM_RX_LAST_APPID_PROCESSED ()]);
    RmTrapSendNotifications (&NotifMsg);

    if ((RM_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_IN_PROGRESS) ||
        (RM_IS_NODE_TRANSITION_IN_PROGRESS () == RM_TRUE))
    {
        /* Release Rx buff lock */
        RM_RX_LAST_ACK_RCVD_SEQ_NUM () = u4LastSeqNum;
        RM_IS_RX_BUFF_PROCESSING_ALLOWED () = RM_TRUE;
        RM_TRC (RM_SYNCUP_TRC,
                "RmHandleAckRecovTmrExpiry: UNLOCKING Rx Buff\r\n");

        /* Process next message from Rx buffer */
        RmRxProcessBufferedPkt ();
    }
}

/******************************************************************************
 * Function           : RmHandleBulkUpdateTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the connection retry
 *                      timer expiry.
 ******************************************************************************/
VOID
RmHandleBulkUpdateTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);
    gu4BulkUpdateInProgress = RM_FALSE;
}

#ifdef L2RED_TEST_WANTED
/******************************************************************************
 * Function           : RmHandleDynSynchAuditTmrExpiry
 * Input(s)           : pArg - VOID type arguments
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine handles the connection retry
 *                      timer expiry.
 ******************************************************************************/
VOID
RmHandleDynSynchAuditTmrExpiry (VOID *pArg)
{
    INT4                i4RmAppId = 0;

    UNUSED_PARAM (pArg);

    gu4DynAuditDoneForAllApp = RM_FALSE;
    for (i4RmAppId = 0; i4RmAppId < RM_MAX_APPS; i4RmAppId++)
    {
        /* Set the status to Audit aborted only for the modules 
         * that received audit trigger. 
         */
        if (gRmDynAuditInfo[i4RmAppId].u4IsAppSetForAudit == RM_TRUE)
        {
            gRmDynAuditInfo[i4RmAppId].i4AuditChkSumValue =
                RM_INVALID_CHKSUM_VALUE;
            gRmDynAuditInfo[i4RmAppId].u4IsAppSetForAudit = RM_FALSE;
            gRmDynAuditInfo[i4RmAppId].u4AuditStatus = RM_DYN_AUDIT_ABORTED;
        }
    }
}
#endif
