/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: rmhr.c,v 1.14 2014/06/03 13:02:56 siva Exp $
*
* Description: RM hitless restart functions.
***********************************************************************/
#include "rmincs.h"
#include "fsrmcli.h"

/******************************************************************************
 * Function           : RmHRRegister
 * Input(s)           : pRmHRInfo - Hitless restart info structure.
 * Output(s)          : None
 * Returns            : None
 * Action             : This is the function used by protocols/applications 
 *                      to register the store and restore functions 
 *                      for RM hitless restart functionality.
 ******************************************************************************/
VOID
RmHRRegister (tRmHRInfo * pRmHRInfo)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRRegister: ENTRY \r\n");
    gRmInfo.RmHRInfo[pRmHRInfo->u1StorageType].pFnRmHRBulkStore =
        pRmHRInfo->pFnRmHRBulkStore;
    gRmInfo.RmHRInfo[pRmHRInfo->u1StorageType].pFnRmHRBulkRestore =
        pRmHRInfo->pFnRmHRBulkRestore;
    gRmInfo.RmHRInfo[pRmHRInfo->u1StorageType].u1StorageType =
        pRmHRInfo->u1StorageType;
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRRegister: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRDeRegister
 * Input(s)           : pRmHRInfo - Hitless restart info structure.
 * Output(s)          : None
 * Returns            : None
 * Action             : This is the function used by protocols/applications 
 *                      to de register the stored and restored functions 
 *                      for RM hitless restart functionality.
 ******************************************************************************/
VOID
RmHRDeRegister (VOID)
{
    UINT1               u1Index = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRDeRegister: ENTRY \r\n");

    for (u1Index = 0; u1Index < RM_HR_MAX_STORAGE; u1Index++)
    {
        gRmInfo.RmHRInfo[u1Index].pFnRmHRBulkStore = NULL;
        gRmInfo.RmHRInfo[u1Index].pFnRmHRBulkRestore = NULL;
        gRmInfo.RmHRInfo[u1Index].u1StorageType = 0;
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRDeRegister: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRFileStore
 * Input(s)           : pu1Data     - Pkt in linear buffer
 *                      u4Length    - Length of the packet
 *                      u4ModId     - Module Identifier
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : This is the function used by RM to store the bulk 
 *                      update messages in different files based on the modules.
 ******************************************************************************/
INT4
RmHRFileStore (UINT1 *pu1Data, UINT4 u4Length, UINT4 u4ModId)
{
    INT4                i4Fd = -1;
    UINT4               u4Count = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileStore: ENTRY \r\n");

    i4Fd = FileOpen ((UINT1 *) (gapc1AppName[u4ModId]),
                     (OSIX_FILE_CR | OSIX_FILE_WO | OSIX_FILE_AP));

    if (i4Fd == -1)
    {
        RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileStore: File open failed for "
                 "protocol %d \r\n", u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileStore: EXIT \r\n");
        return RM_FAILURE;
    }

    u4Count = FileWrite (i4Fd, (CHR1 *) pu1Data, u4Length);
    if (u4Count != u4Length)
    {
        RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileStore: File write failed for "
                 "protocol %d \r\n", u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileStore: EXIT \r\n");
        FileClose (i4Fd);
        return RM_FAILURE;
    }
    /* this \n is necessary as it is used as end char for a packet 
     * while restoring */
    FileWrite (i4Fd, (CHR1 *) '\n', 1);
    FileClose (i4Fd);
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileStore: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRFileRestore
 * Input(s)           : u4ModId - Module Identifier. 
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : This function is used by RM to restore the bulk update
 *                      messages from different files based on the modules.
 ******************************************************************************/
INT4
RmHRFileRestore (UINT4 u4ModId)
{
    UINT1               au1Buffer[RM_HR_HDR_READ_COUNT];
    tRmMsg             *pPkt = NULL;
    UINT1              *pu1Data = NULL;
    INT4                i4Fd = -1;
    UINT4               u4TotLen = 0;
    UINT4               u4Count = 0;
    UINT2               u2CkSum = 0;
    UINT2               u2CalCkSum = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileRestore: ENTRY \r\n");

    i4Fd = FileOpen ((UINT1 *) gapc1AppName[u4ModId], OSIX_FILE_RO);
    if (i4Fd == -1)
    {
        RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileRestore: File open failed for "
                 "protocol %d \r\n", u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileRestore: EXIT \r\n");
        return RM_FAILURE;
    }

    MEMSET (au1Buffer, 0, sizeof (au1Buffer));
    while (FileRead (i4Fd, (CHR1 *) au1Buffer, RM_HR_HDR_READ_COUNT) ==
           RM_HR_HDR_READ_COUNT)
    {
        MEMCPY (&u2CkSum, &(au1Buffer[RM_HDR_OFF_CKSUM]), sizeof (UINT2));
        MEMCPY (&u4TotLen, &(au1Buffer[RM_HDR_OFF_TOT_LENGTH]), sizeof (UINT4));

        u2CkSum = OSIX_NTOHS (u2CkSum);
        u4TotLen = OSIX_NTOHL (u4TotLen);

        if ((pu1Data = MemAllocMemBlk (gRmInfo.RmPktMsgPoolId)) == NULL)
        {
            FileClose (i4Fd);
            RM_TRC1 (RM_FAILURE_TRC, "RmHRFileRestore: linear buffer "
                     "allocation - failure for protocol %d \r\n", u4ModId);
            return (RM_FAILURE);
        }

        if (FileSeek (i4Fd, (-RM_HR_HDR_READ_COUNT), SEEK_CUR) == -1)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileRestore: File read failed for "
                     "protocol %d \r\n", u4ModId);
            RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileRestore: EXIT \r\n");
            return (RM_FAILURE);
        }

        u4Count = FileRead (i4Fd, (CHR1 *) pu1Data, u4TotLen);

        if (u4Count != u4TotLen)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileRestore: File read failed for "
                     "protocol %d \r\n", u4ModId);
            RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileRestore: EXIT \r\n");
            return RM_FAILURE;
        }

        u2CalCkSum = RM_LINEAR_CALC_CKSUM ((const INT1 *) pu1Data, u4TotLen);

        if (u2CalCkSum != 0)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_TRC1 (RM_CRITICAL_TRC | RM_FAILURE_TRC, "RmHRFileRestore: Pkt "
                     "rcvd with invalid cksum. Discarding for protocol %d \r\n",
                     u4ModId);
            return (RM_FAILURE);
        }

        if ((pPkt = RM_ALLOC_RX_BUF (u4TotLen)) == NULL)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_TRC1 (RM_FAILURE_TRC, "RmHRFileRestore: Unable to allocate buf "
                     "for protocol %d \r\n", u4ModId);
            return RM_FAILURE;
        }

        /* copy the linear buffer to CRU buffer */
        if (RM_COPY (pPkt, pu1Data, u4TotLen) == CRU_FAILURE)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_FREE (pPkt);        /* Release the CRU Buffer */
            RM_TRC1 (RM_FAILURE_TRC, "RmHRFileRestore: Unable to copy linear "
                     "to CRU buf for protocol %d \r\n", u4ModId);
            return RM_FAILURE;
        }

        /* pPkt should not be freed here. it will be freed while deleting the
         * rx buff node */
        MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);

        /* Post the message to protocol */
        if (RmUtilPostDataOrEvntToProtocol (u4ModId, (UINT1) RM_MESSAGE,
                                            pPkt, u4TotLen, 0) == RM_FAILURE)
        {
            FileClose (i4Fd);
            MemReleaseMemBlock (gRmInfo.RmPktMsgPoolId, (UINT1 *) pu1Data);
            RM_FREE (pPkt);        /* Release the CRU Buffer */
            RM_TRC1 (RM_FAILURE_TRC, "RmHRFileRestore: Unable to post data to "
                     " to protocol %d \r\n", u4ModId);
            return RM_FAILURE;
        }

        MEMSET (au1Buffer, 0, sizeof (au1Buffer));
    }

    FileClose (i4Fd);
    /* after restoring the dynamic messages delete the file */
    if (FileDelete ((UINT1 *) gapc1AppName[u4ModId]) < 0)
    {
        RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRFileRestore: File delete failed "
                 "for protocol %d \r\n", u4ModId);
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRFileRestore: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRStartBulkReq 
 * Input(s)           : u4ModId - Module Identifier.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : This function starts up the bulk update process for the
 *                      hitless restart feature to store the bulk messages. 
 ******************************************************************************/
INT1
RmHRStartBulkReq (UINT4 u4ModId)
{
    tRmMsg             *pPkt = NULL;
    tRmNodeInfo        *pRmNodeInfo = NULL;
    UINT4               u4MsgType = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartBulkReq: ENTRY \r\n");

    /* Message Type fields for EOAM and LA modules are of 4 bytes
     * whereas for other modules, it is 1 byte only*/
    if ((u4ModId == RM_EOAM_APP_ID) || (u4ModId == RM_LA_APP_ID))
    {
        u2MsgLen = RM_HR_BULK_REQ_LEN + RM_HR_ADD_LEN_FOR_TYPE;
    }
    else
    {
        u2MsgLen = RM_HR_BULK_REQ_LEN;
    }

    if ((pPkt = RM_ALLOC_RX_BUF (RM_HDR_LENGTH + u2MsgLen)) == NULL)
    {
        RM_TRC1 (RM_FAILURE_TRC, "RmHRStartBulkReq: "
                 "Unable to allocate buffer for bulk update req msg "
                 "to protocol %d \r\n", u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartBulkReq: EXIT \r\n");
        return RM_FAILURE;
    }

    if (RM_FAILURE == RmPrependRmHdr (pPkt, u2MsgLen))
    {
        RM_FREE (pPkt);
        RM_TRC1 (RM_FAILURE_TRC, "RmHRStartBulkReq: "
                 "Unable to prepend the RM header for protocol %d \r\n",
                 u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartBulkReq: EXIT \r\n");
        return RM_FAILURE;
    }

    RM_COPY_TO_OFFSET (pPkt, &u4ModId, RM_HDR_OFF_SRC_ENTID, sizeof (UINT4));
    RM_COPY_TO_OFFSET (pPkt, &u4ModId, RM_HDR_OFF_DEST_ENTID, sizeof (UINT4));

    u1MsgType = RM_BULK_UPDT_REQ_MSG;

    /* Message Type fields for EOAM and LA modules are of 4 bytes
     * whereas for other modules, it is 1 byte only*/
    if ((u4ModId == RM_EOAM_APP_ID) || (u4ModId == RM_LA_APP_ID))
    {
        u4MsgType = u1MsgType;
        u4MsgType = OSIX_NTOHL (u4MsgType);

        RM_COPY_TO_OFFSET (pPkt, (UINT4 *) &u4MsgType, RM_HR_MSG_TYPE_OFFSET,
                           sizeof (UINT4));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        RM_COPY_TO_OFFSET (pPkt, &u2MsgLen, (RM_HR_MSG_LEN_OFFSET +
                                             RM_HR_ADD_LEN_FOR_TYPE),
                           sizeof (UINT2));
    }
    else
    {
        RM_COPY_TO_OFFSET (pPkt, &u1MsgType, RM_HR_MSG_TYPE_OFFSET,
                           sizeof (UINT1));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        RM_COPY_TO_OFFSET (pPkt, &u2MsgLen, RM_HR_MSG_LEN_OFFSET,
                           sizeof (UINT2));
    }

    if (MemAllocateMemBlock
        (gRmInfo.RmMsgPoolId, (UINT1 **) (VOID *) &pRmNodeInfo) != MEM_SUCCESS)
    {
        RM_FREE (pPkt);
        RM_TRC1 (RM_CRITICAL_TRC, "RmHRStartBulkReq:"
                 "Unable to allocate memory for "
                 "peer node state (up/down) updation to protocol %d \r\n",
                 u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartBulkReq: EXIT \r\n");
        return RM_FAILURE;
    }
    MEMSET (pRmNodeInfo, 0, sizeof (tRmNodeInfo));
    /*If the peer node count is zero,bulk update will not be initiated for that particular module
       Here,peer node couint is set to one,to indicate that an active node is present. */
    if (RM_PEER_NODE_COUNT () != RM_ONE)
    {
        RM_PEER_NODE_COUNT () = RM_ONE;
    }
    pRmNodeInfo->u1NumStandby = RM_PEER_NODE_COUNT ();
    if (RmUtilPostDataOrEvntToProtocol (u4ModId, RM_PEER_UP, (tRmMsg *) (VOID *)
                                        pRmNodeInfo, RM_NODE_COUNT_MSG_SIZE,
                                        0) == RM_FAILURE)
    {
        RM_FREE ((tRmMsg *) (VOID *) pRmNodeInfo);
    }

    if (RmUtilPostDataOrEvntToProtocol (u4ModId, RM_MESSAGE, pPkt,
                                        (RM_HDR_LENGTH + OSIX_NTOHS (u2MsgLen)),
                                        0) == RM_FAILURE)
    {
        RM_FREE (pPkt);
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartBulkReq: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRBulkMsgRestore
 * Input(s)           : u4ModId - Module Identifier. 
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is used by RM to restore the bulk upddate
 *                      messages from different files based on the modules.
 ******************************************************************************/
VOID
RmHRBulkMsgRestore (UINT4 u4ModId)
{
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRBulkMsgRestore: ENTRY \r\n");
    RmUtilPostDataOrEvntToProtocol (u4ModId, RM_STANDBY, NULL, 0, 0);

    (gRmInfo.RmHRInfo[RM_HR_FILE_STORAGE].pFnRmHRBulkRestore) (u4ModId);

    RmUtilPostDataOrEvntToProtocol (u4ModId, RM_ACTIVE, NULL, 0, 0);

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRBulkMsgRestore: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRProcessBulkMsg
 * Input(s)           : pu1Data - Packet in Linear buffer.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM hitless restart feature to process
 *                      the bulk messages from different modules and calls the
 *                      API in which the bulk messages need to be stored.
 ******************************************************************************/
INT1
RmHRProcessBulkMsg (UINT1 *pu1Data, UINT1 u1MsgType, UINT4 u4SrcEntId)
{
    UINT4               u4RmPktLen = 0;
    UINT4               u4NextAppId = 0;
    static UINT1        u1StdyStart = OSIX_FALSE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessBulkMsg: ENTRY \r\n");
    /* Get the length of RM pkt to be sent */
    RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_OFF_TOT_LENGTH, u4RmPktLen);

    if ((RmHRValidateChecksum (pu1Data) != RM_FAILURE) &&
        (u1StdyStart == OSIX_FALSE))
    {
        if ((u1MsgType != RM_BULK_UPDT_REQ_MSG) &&
            (u1MsgType != RM_BULK_UPDT_TAIL_MSG) &&
            (RM_HR_GET_STATUS () == RM_HR_STATUS_STORE))
        {
            (gRmInfo.RmHRInfo[RM_HR_FILE_STORAGE].pFnRmHRBulkStore)
                (pu1Data, u4RmPktLen, u4SrcEntId);
        }
    }
    if (u1MsgType == RM_BULK_UPDT_TAIL_MSG)
    {
        if (RmHRUtilGetNextAppId (u4SrcEntId, &u4NextAppId) == RM_SUCCESS)
        {
            RmHRStartBulkReq (u4NextAppId);
        }
        else if (u1StdyStart == OSIX_FALSE)
        {
            RM_TRC (RM_CRITICAL_TRC, "\r\nHitless Restart: Bulk storage "
                    "completed.");
            RM_TRC (RM_CRITICAL_TRC, "\r\nHitless Restart: Steady state pkt "
                    "request starts.\n");
            gRmInfo.u1HRFlag = RM_HR_STATUS_RESTORE;
            IssSetHRFlagToNvRam (RM_HR_STATUS_STORE);

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsRmHitlessRestartFlag,
                                  u4SeqNum, FALSE, RmLock, RmUnLock, 0,
                                  SNMP_SUCCESS);
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i", RM_HR_STATUS_RESTORE));

            if (RmHRUtilGetNextAppId (RM_APP_ID, &u4SrcEntId) == RM_SUCCESS)
            {
                RmHRStartStdyStReq (u4SrcEntId);
                u1StdyStart = OSIX_TRUE;
            }
        }
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessBulkMsg: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRStartStdyStReq 
 * Input(s)           : u4ModId - Module Identifier.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE.
 * Action             : This function starts up the steady state packet request
 *                      for the hitless restart feature. 
 ******************************************************************************/
INT1
RmHRStartStdyStReq (UINT4 u4ModId)
{
    tRmMsg             *pPkt = NULL;
    UINT4               u4MsgType = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartStdyStReq: ENTRY \r\n");

    /* Message Type fields for EOAM and LA modules are of 4 bytes
     * whereas for other modules, it is 1 byte only*/
    if ((u4ModId == RM_EOAM_APP_ID) || (u4ModId == RM_LA_APP_ID))
    {
        u2MsgLen = RM_HR_BULK_REQ_LEN + RM_HR_ADD_LEN_FOR_TYPE;
    }
    else
    {
        u2MsgLen = RM_HR_BULK_REQ_LEN;
    }

    if ((pPkt = RM_ALLOC_RX_BUF (RM_HDR_LENGTH + u2MsgLen)) == NULL)
    {
        RM_TRC1 (RM_FAILURE_TRC, "RmHRStartStdyStReq: "
                 "Unable to allocate buffer for steady state pkt req msg "
                 "to protocol %d \r\n", u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartStdyStReq: EXIT \r\n");
        return RM_FAILURE;
    }

    if (RM_FAILURE == RmPrependRmHdr (pPkt, u2MsgLen))
    {
        RM_FREE (pPkt);
        RM_TRC1 (RM_FAILURE_TRC, "RmHRStartStdyStReq: "
                 "Unable to prepend the RM header for protocol %d \r\n",
                 u4ModId);
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartStdyStReq: EXIT \r\n");
        return RM_FAILURE;
    }

    RM_COPY_TO_OFFSET (pPkt, &u4ModId, RM_HDR_OFF_SRC_ENTID, sizeof (UINT4));
    RM_COPY_TO_OFFSET (pPkt, &u4ModId, RM_HDR_OFF_DEST_ENTID, sizeof (UINT4));

    u1MsgType = RM_HR_STDY_ST_PKT_REQ;

    /* Message Type fields for EOAM and LA modules are of 4 bytes
     * whereas for other modules, it is 1 byte only*/
    if ((u4ModId == RM_EOAM_APP_ID) || (u4ModId == RM_LA_APP_ID))
    {
        u4MsgType = u1MsgType;
        u4MsgType = OSIX_NTOHL (u4MsgType);

        RM_COPY_TO_OFFSET (pPkt, (UINT4 *) &u4MsgType, RM_HR_MSG_TYPE_OFFSET,
                           sizeof (UINT4));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        RM_COPY_TO_OFFSET (pPkt, &u2MsgLen, (RM_HR_MSG_LEN_OFFSET +
                                             RM_HR_ADD_LEN_FOR_TYPE),
                           sizeof (UINT2));
    }
    else
    {
        RM_COPY_TO_OFFSET (pPkt, &u1MsgType, RM_HR_MSG_TYPE_OFFSET,
                           sizeof (UINT1));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        RM_COPY_TO_OFFSET (pPkt, &u2MsgLen, RM_HR_MSG_LEN_OFFSET,
                           sizeof (UINT2));
    }
    if (RmUtilPostDataOrEvntToProtocol (u4ModId, RM_MESSAGE, pPkt,
                                        (RM_HDR_LENGTH + OSIX_NTOHS (u2MsgLen)),
                                        0) == RM_FAILURE)
    {
        RM_FREE (pPkt);
    }

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRStartStdyStReq: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRProcessStdyStPktMsg
 * Input(s)           : pu1Data    - Packet in Linear buffer.
 *                      u4SrcEntId - Module identifier.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM hitless restart feature to process
 *                      the steady state packet and indicate the same to NPSIM.
 ******************************************************************************/
VOID
RmHRProcessStdyStPktMsg (UINT1 *pu1Data, UINT4 u4SrcEntId)
{
#ifdef NPAPI_WANTED
    tRmHRPktInfo        RmHRPktInfo;
#endif
    UINT1              *pu1Buf = NULL;
    UINT4               u4PktLen = 0;
    UINT4               u4TimeOut = 0;
    UINT2               u2Port = 0;
    UINT2               u2MsgLen = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessStdyStPktMsg: ENTRY \r\n");
    /* Parsing  steady state packet

       RM header is as follows
       <-- 2 bytes --><-- 2 bytes --><---- 4 bytes ----><---- 4 bytes ---->
       ___________________________________________________________________
       |             |              |                  |                   |
       |  u2Version  |  u2Checksum  |    u4TotLen      |     u4MsgType     |
       |_____________|______________|__________________|___________________|
       |                            |                  |                   |
       |       u4SrcEntId           |    u4DstEntId    |     u4SeqNo       |
       |____________________________|__________________|___________________|

       <- 24B --><-------- 1B/4B -------><-- 2B -->< 2B -><-- 4B -->
       ____________________________________________________________________
       |        |                       |         |      |         |        |
       | RM Hdr | RM_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
       |________|_______________________|_________|______|_________|________|

       NOTE: FOr LA and EOAM the message type field will be 4 bytes.
     */

    if (RmHRValidateChecksum (pu1Data) == RM_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmHRProcessStdyStPktMsg: checksum validation "
                "failed !!!\n");
        return;
    }
    RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_OFF_SRC_ENTID, u4SrcEntId);

    /* Message Type fields for EOAM and LA modules are of 4 bytes
     * whereas for other modules, it is 1 byte only*/
    if ((u4SrcEntId == RM_EOAM_APP_ID) || (u4SrcEntId == RM_LA_APP_ID))
    {
        RM_HR_GET_2BYTE_FROM_LIN_BUF (pu1Data, (RM_HR_MSG_LEN_OFFSET +
                                                RM_HR_ADD_LEN_FOR_TYPE),
                                      u2MsgLen);
        RM_HR_GET_2BYTE_FROM_LIN_BUF (pu1Data,
                                      (RM_HR_SSP_PORT_OFFSET +
                                       RM_HR_ADD_LEN_FOR_TYPE), u2Port);
        RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data,
                                      (RM_HR_SSP_TIMEOUT_OFFSET +
                                       RM_HR_ADD_LEN_FOR_TYPE), u4TimeOut);

        pu1Buf = pu1Data + RM_HR_SSP_BUFFER_OFFSET + RM_HR_ADD_LEN_FOR_TYPE;
    }
    else
    {
        RM_HR_GET_2BYTE_FROM_LIN_BUF (pu1Data, RM_HR_MSG_LEN_OFFSET, u2MsgLen);
        RM_HR_GET_2BYTE_FROM_LIN_BUF (pu1Data, RM_HR_SSP_PORT_OFFSET, u2Port);
        RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HR_SSP_TIMEOUT_OFFSET,
                                      u4TimeOut);

        pu1Buf = pu1Data + RM_HR_SSP_BUFFER_OFFSET;
    }
    u4PktLen = u2MsgLen - sizeof (UINT2) - sizeof (UINT4);

#ifdef NPAPI_WANTED
    MEMSET (&RmHRPktInfo, 0, sizeof (tRmHRPktInfo));

    MEMCPY (RmHRPktInfo.au1LinBuf, pu1Buf, u4PktLen);
    RmHRPktInfo.u4PktLen = u4PktLen;
    RmHRPktInfo.u4TimeOut = u4TimeOut;
    RmHRPktInfo.u2Port = u2Port;
    RmHRPktInfo.u1ProtoId = u4SrcEntId;

    if (RmFsNpHRSetStdyStInfo (NP_RM_HR_STORE_STDY_ST_PKT, &RmHRPktInfo)
        == FNP_FAILURE)
    {
        RM_TRC (RM_FAILURE_TRC, "RmHRProcessStdyStPktMsg: sending steady state "
                "packet to NPAPI failed !!!\n");
    }
#else
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (pu1Buf);
#endif
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessStdyStPktMsg: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRProcessStdyStTail
 * Input(s)           : pu1Data    - Packet in Linear buffer.
 *                      u4SrcEntId - module identifier.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine used by RM hitless restart feature to process  
 *                      steady state tail msg and starts next module's steady 
 *                      state request. It gives indication to NPSIM once all
 *                      the steady state pkts from all modules are ported to 
 *                      NPSIM.
 ******************************************************************************/
VOID
RmHRProcessStdyStTail (UINT1 *pu1Data, UINT4 u4SrcEntId)
{
    UINT4               u4NextAppId = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessStdyStTail: ENTRY\r\n");
    UNUSED_PARAM (pu1Data);

    if (RmHRUtilGetNextAppId (u4SrcEntId, &u4NextAppId) == RM_SUCCESS)
    {
        RmHRStartStdyStReq (u4NextAppId);
    }
    else
    {
        RM_TRC (RM_CRITICAL_TRC, "\r\n Hitless Restart: All Steady State "
                "packets are stored in NPSIM.");
        RM_TRC (RM_CRITICAL_TRC, "\r\n Do write start-up and PLEASE "
                "RESTART THE EXE\n");

        MsrInitiateConfigSave ();

#ifdef NPAPI_WANTED
        RmFsNpHRSetStdyStInfo (NP_RM_HR_STDY_ST_TRIG_START, NULL);
#endif
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRProcessStdyStTail: EXIT\r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRDelFiles
 * Input(s)           : None.
 * Output(s)          : None
 * Returns            : None
 * Action             : This is the function used by RM to delete the files.
 *                      These files were created during the previous session.
 *                      Reason behind the function is those files were not 
 *                      restored as write start up had not done. So while 
 *                      enabling hitless restart now, it is necessary to remove
 *                      those files else the dynamic contents will be appended 
 *                      in that file. 
 ******************************************************************************/
VOID
RmHRDelFiles (VOID)
{
    UINT4               u4CurAppId = RM_APP_ID;
    UINT4               u4NextAppId = 0;
    INT4                i4Fd = -1;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRDelFiles: ENTRY \r\n");

    while (u4CurAppId < RM_MAX_APPS)
    {
        i4Fd = FileOpen ((UINT1 *) gapc1AppName[u4CurAppId], OSIX_FILE_RO);
        if (i4Fd != -1)
        {
            if (FileDelete ((UINT1 *) gapc1AppName[u4CurAppId]) < 0)
            {
                RM_TRC1 (RM_CTRL_PATH_TRC, "RmHRDelFiles: File delete failed "
                         "for protocol %d \r\n", u4CurAppId);
            }
        }
        if (RmHRUtilGetNextAppId (u4CurAppId, &u4NextAppId) != RM_SUCCESS)
        {
            break;
        }
        u4CurAppId = u4NextAppId;
    }
    if (i4Fd == -1)
    {
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRDelFiles: EXIT \r\n");
        return;
    }
    FileClose (i4Fd);
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRDelFiles: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRValidateChecksum
 * Input(s)           : pu1Data - Packet in Linear buffer.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM hitless restart feature to validate 
 *                      the checksum.
 ******************************************************************************/
INT1
RmHRValidateChecksum (UINT1 *pu1Data)
{
    UINT4               u4RmPktLen = 0;
    UINT2               u2Cksum = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRValidateChecksum: ENTRY \r\n");
    /* Get the length of RM pkt to be sent */
    RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_OFF_TOT_LENGTH, u4RmPktLen);

    u2Cksum = RM_LINEAR_CALC_CKSUM ((const INT1 *) pu1Data, u4RmPktLen);

    if (u2Cksum != 0)
    {
        RM_TRC (RM_FAILURE_TRC, "RmHRValidateChecksum: "
                "Invalid checksum !!!\n");
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRValidateChecksum: EXIT \r\n");
        return RM_FAILURE;
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRValidateChecksum: EXIT \r\n");
    return RM_SUCCESS;
}

/******************************************************************************
 * Function           : RmHRGetRmHdrInfo
 * Input(s)           : pu1Data - Packet in Linear buffer.
 * Output(s)          : pu1MsgType - Message Type.
 *                      pu4SrcEntId - Module Identifier.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM hitless restart feature to get 
 *                      the message type and source module id from the packet.
 ******************************************************************************/
VOID
RmHRGetRmHdrInfo (UINT1 *pu1Data, UINT1 *pu1MsgType, UINT4 *pu4SrcEntId)
{
    UINT4               u4MsgType = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRGetRmHdrInfo: ENTRY \r\n");

    if ((pu1Data == NULL) || (pu1MsgType == NULL) || (pu4SrcEntId == NULL))
    {
        RM_TRC (RM_FAILURE_TRC, "RmHRGetRmHdrInfo: argument is NULL \r\n");
        RM_TRC (RM_CTRL_PATH_TRC, "RmHRGetRmHdrInfo: EXIT \r\n");
        return;
    }

    RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_OFF_SRC_ENTID, *pu4SrcEntId);

    if ((*pu4SrcEntId == RM_EOAM_APP_ID) || (*pu4SrcEntId == RM_LA_APP_ID))
    {
        RM_HR_GET_4BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_LENGTH, u4MsgType);
        *pu1MsgType = (UINT1) u4MsgType;
    }
    else
    {
        RM_HR_GET_1BYTE_FROM_LIN_BUF (pu1Data, RM_HDR_LENGTH, *pu1MsgType);
    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRGetRmHdrInfo: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : RmHRUtilGetNextAppId
 * Input(s)           : u4AppId - Module Id.
 *                      pu4NextId - Pointer to next valid
 *                      registered module Id.
 * Output(s)          : None.
 * Returns            : RM_SUCCESS / RM_FAILURE.
 * Action             : Routine used by RM to return next valid registered
 *                      module Id which supports for hitless restart feature.
 ******************************************************************************/
UINT1
RmHRUtilGetNextAppId (UINT4 u4AppId, UINT4 *pu4NextId)
{
    UINT1               u1RetVal = RM_FAILURE;
    UINT4               u4ModId = 0;

    RM_TRC (RM_CTRL_PATH_TRC, "RmHRUtilGetNextAppId: ENTRY \r\n");
    /* Get the next valid module ID. */
    for (u4ModId = ++u4AppId; (u4ModId < RM_MAX_APPS && u1RetVal == RM_FAILURE);
         u4ModId++)
    {
        if ((gRmInfo.RmAppInfo[u4ModId] != NULL) &&
            (gRmInfo.RmAppInfo[u4ModId]->pFnRcvPkt != NULL))

        {
            /* only the following modules are being supported for hitless restart */
            if ((u4ModId == RM_VLANGARP_APP_ID) || (u4ModId == RM_LA_APP_ID) ||
                (u4ModId == RM_EOAM_APP_ID) || (u4ModId == RM_ECFM_APP_ID) ||
                (u4ModId == RM_ERPS_APP_ID) || (u4ModId == RM_ELPS_APP_ID) ||
                (u4ModId == RM_RSTPMSTP_APP_ID) || (u4ModId == RM_LLDP_APP_ID))
            {
                /* Valid entry found */
                *pu4NextId = u4ModId;
                u1RetVal = RM_SUCCESS;
            }
        }

    }
    RM_TRC (RM_CTRL_PATH_TRC, "RmHRUtilGetNextAppId: EXIT \r\n");
    return u1RetVal;
}
