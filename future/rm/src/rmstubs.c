/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: rmstubs.c,v 1.5 2016/06/30 10:05:55 siva Exp $
 *
 * Description: This file contains all the stub functions
 *
 *******************************************************************/
#ifndef _RMSTUB_C
#define _RMSTUB_C

#include "rmincs.h"

#ifndef ISSU_WANTED

/*****************************************************************************/
/* Function Name      : IssuSetOperMaintenanceMode                           */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set Maintenance Oper status to same as    */
/*                      maintenance admin status.                            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssuSetOperMaintenanceMode (UINT1 u1OperStatus)
{
    UNUSED_PARAM (u1OperStatus); 

    return;
}

/*****************************************************************************/
/* Function Name      : IssuUpdateProcedureStatus                            */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set ISSU procedure status                 */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssuUpdateProcedureStatus (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : IssuGetIssuMode                                      */
/*                                                                           */
/* Description        : This function is called to get ISSU mode from        */
/*                      external modules.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IssuMode.                                            */
/*****************************************************************************/
UINT4
IssuGetIssuMode (VOID)
{
    return 0;
}
/*****************************************************************************/
/* Function Name      : IssuApiForceStandbyCompleted                         */
/*                                                                           */
/* Description        : This function is called from RM module to update     */
/*                      the successful completion of force standby.          */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
IssuApiForceStandbyCompleted(VOID)
{
     return;
}

/*****************************************************************************/
/* Function Name      : IssuApiRestoreCacheFile                              */
/*                                                                           */
/* Description        : This function is called from RM module to restore    */
/*                      the variables updated during the warm boot           */
/*                      procedure                                            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
IssuApiRestoreCacheFile(VOID)
{
    return ;
}

#endif
#endif
