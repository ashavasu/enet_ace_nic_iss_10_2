
/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: rmnpwr.c,v 1.1 2013/03/19 12:21:57 siva Exp $
*
* Description: This file contains the RM NPAPI related wrapper routines.
*************************************************************************/
#ifndef _RMNPWR_C_
#define _RMNPWR_C_

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : RmNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRmNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
RmNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmNpModInfo       *pRmNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRmNpModInfo = &(pFsHwNp->RmNpModInfo);

    if (NULL == pRmNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case RM_NP_UPDATE_NODE_STATE:
        {
            tRmNpWrRmNpUpdateNodeState *pEntry = NULL;
            pEntry = &pRmNpModInfo->RmNpRmNpUpdateNodeState;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = RmNpUpdateNodeState (pEntry->u1Event);
            break;
        }
        case FS_NP_H_R_SET_STDY_ST_INFO:
        {
            tRmNpWrFsNpHRSetStdyStInfo *pEntry = NULL;
            pEntry = &pRmNpModInfo->RmNpFsNpHRSetStdyStInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpHRSetStdyStInfo (pEntry->eAction, pEntry->pRmHRPktInfo);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _RMNPWR_C_ */
