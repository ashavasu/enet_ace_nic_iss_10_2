/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4port.h,v 1.52 2018/01/03 11:31:18 siva Exp $
 *
 * Description:This file contains all the definitions that had          
 *             been made in the system   
 *
 *******************************************************************/
#ifndef BGP4PORT_H
#define BGP4PORT_H

#include "bgp4dfs.h"
#include "bgp4tdfs.h"

#ifdef RRD_WANTED
#include "rtm.h"
#ifdef BGP4_IPV6_WANTED
#include "trie.h"
#include "ipv6.h"
#include "rtm6.h"
#endif
#endif

#ifdef L3VPN
#include "bgvpndfs.h"
#endif


#include "bgp4rib.h"
/*#ifdef ROUTEMAP_WANTED*/
#include "rmap.h"
/*#endif*/
#ifdef BFD_WANTED
#include "bfd.h"
#else
#ifdef MPLS_L3VPN_WANTED
#include "mplsapi.h"
#endif
#endif

UINT4 Bgp4ThrottleCalc (UINT4 *, UINT4 , UINT4, UINT4);
VOID BgpRecomputeThrottle(UINT4);
/* Definitions and Constants */
#define   BGP4_IPV4_KEY_ARR_SIZE    2
#define   BGP4_IPV4_KEY_ARR_INDX0   0
#define   BGP4_IPV4_KEY_ARR_INDX1   1

/* Defines for BGP Response Queue. */
#define BGP4_RESP_QUE             "BGRQ"
#define BGP4_RESP_Q_DEPTH          10 
#define BGP4_RESP_Q_STRING_SIZE    5 
#define BGP4_RESP_Q_MODE           OSIX_LOCAL

#define BGP4_TASK_PRIORITY         150   
#define BGP4_MAX_SYS_TIME_TICKS    0xffffffff

/* The following macros - BGP4_MAX_xxx need to be appropriately updated
 * depending on environment requirements */
#define  BGP4_MAX_MED_ENTRIES          100  /* max. no. of MED table entries */
#define  BGP4_MAX_LP_ENTRIES           100  /* max. no. of LP table entries */
#define  BGP4_MAX_AGGR_ENTRIES         100  /* max. no. of aggregation table
                                             * entries */
#define  BGP4_MAX_UPDFILTER_ENTRIES    100  /* max. no. of update-filter table 
                                             * entries*/
#define  BGP4_DEFAULT_MAX_NUM_COMM_PER_ROUTE                 10
                                            /* max. no of communities
                                             * configurable per route. */

#define  BGP4_DEFAULT_MAX_NUM_EXT_COMM_PER_ROUTE             10
                                            /* max. no of extended communities
                                             * configurable per route. */

/* maximum size of message to be received in a single TIC for each peer. */
#define  BGP4_MAX_BUF2PROCESS_MIN   (5000)
#define  BGP4_MAX_BUF2PROCESS_MAX   (25000)
extern UINT4   u4MaxBuf2Process;
#define  BGP4_MAX_BUF2PROCESS (u4MaxBuf2Process)

/* maximum size of message to be transmitted in a single TIC for each peer. */
#define  BGP4_MAX_TXBUF2PROCESS_MIN (1000)
#define  BGP4_MAX_TXBUF2PROCESS_MAX (50000)
extern UINT4   u4MaxTxBuf;
#define  BGP4_MAX_TXBUF2PROCESS (u4MaxTxBuf)

 /* max. no. of BGP packets to be processed in a single TIC */
#define  BGP4_MAX_PEER_PKTS2TX_MIN  (1000)
#define  BGP4_MAX_PEER_PKTS2TX_MAX  (6000)
extern UINT4   u4MaxPeerPkt2Tx;
#define  BGP4_MAX_PEER_PKTS2TX (u4MaxPeerPkt2Tx)


/* max no. of BGP Routes that can be processed in a single TIC by peer
 * going through init.  */
#define  BGP4_MAX_PEERINIT_RTS_MIN  (200)
#define  BGP4_MAX_PEERINIT_RTS_MAX  (4000)
extern UINT4   u4MaxPeerInitRts;
#define  BGP4_MAX_PEERINIT_RTS (u4MaxPeerInitRts)

/* max. no. of BGP packets to be processed in a single TIC */
#define  BGP4_MAX_PKTS2PROCESS_MIN  (200)
#define  BGP4_MAX_PKTS2PROCESS_MAX  (12000)
extern UINT4   u4MaxPkt2Prcs;
#define  BGP4_MAX_PKTS2PROCESS (u4MaxPkt2Prcs)

/* max. no. of soft config peer-routes to be processed in a single TIC */
#define  BGP4_MAX_SOFTCFGRTS2PROCESS_MIN (200)
#define  BGP4_MAX_SOFTCFGRTS2PROCESS_MAX (2000)
extern UINT4   u4MaxSoftCfg;
#define  BGP4_MAX_SOFTCFGRTS2PROCESS (u4MaxSoftCfg)

/* max. no. of peer-routes to be processed in a single TIC */
#define  BGP4_MAX_PEERRTS2PROCESS_MIN   (100)
#define  BGP4_MAX_PEERRTS2PROCESS_MAX   (1500)
extern UINT4   u4MaxPeerRts;
#define  BGP4_MAX_PEERRTS2PROCESS (u4MaxPeerRts)

 /* max. no. of peers to be processed for various jobs in a single TIC*/
#define  BGP4_MAX_PEERS2PROCESS_MIN     (10) 
#define  BGP4_MAX_PEERS2PROCESS_MAX     (25) 
extern UINT4   u4MaxPeer2Prcs;
#define  BGP4_MAX_PEERS2PROCESS (u4MaxPeer2Prcs)

/* max. no. of routes to be deleted from the BGP local-RIB in a single TIC */
#define  BGP4_MAX_DELRTS2PROCESS_MIN    (1000)
#define  BGP4_MAX_DELRTS2PROCESS_MAX    (6000)
extern UINT4   u4MaxDelRts;
#define  BGP4_MAX_DELRTS2PROCESS (u4MaxDelRts)

/* max. no. of routes for which synchronization needs to be checked in a
 * single TIC */
#define  BGP4_MAX_SYNC_RTS2PRCS_MIN     (100)
#define  BGP4_MAX_SYNC_RTS2PRCS_MAX     (500)
extern UINT4   u4MaxSyncRts;
#define  BGP4_MAX_SYNC_RTS2PRCS (u4MaxSyncRts)

/* max. no. of nexthops/routes for which nexthop resolution needs to be done
 * in a single TIC */
#define  BGP4_MAX_NH_MET_RTS2PRCS_MIN   (100)
#define  BGP4_MAX_NH_MET_RTS2PRCS_MAX   (1000)
extern UINT4   u4MaxNHMet;
#define  BGP4_MAX_NH_MET_RTS2PRCS (u4MaxNHMet)

/* max. no. of updates that can be handle in a single TIC by the FIB update
 * process */
#define  BGP4_MAX_FIB_UPD2PROCESS_MIN   (200)  
#define  BGP4_MAX_FIB_UPD2PROCESS_MAX   (12000)
extern UINT4   u4MaxFibUpd;
#define  BGP4_MAX_FIB_UPD2PROCESS (u4MaxFibUpd)

/* Max number of updates, the Redistribution Update process can
 * handle */
#define  BGP4_MAX_REDIS_UPD2PROCESS_MIN    (200)
#define  BGP4_MAX_REDIS_UPD2PROCESS_MAX    (1600)
extern UINT4   u4MaxRedist;
#define  BGP4_MAX_REDIS_UPD2PROCESS (u4MaxRedist)

/* Max number of updates, the External peer advertisement control
 * process can * handle */
#define  BGP4_MAX_EXT_PEER_UPD2PROCESS_MIN (200)
#define  BGP4_MAX_EXT_PEER_UPD2PROCESS_MAX (1600)
extern UINT4   u4MaxExtPeer;
#define  BGP4_MAX_EXT_PEER_UPD2PROCESS (u4MaxExtPeer)


/* Max number of updates, the Internal peer advertisement control
 * process can * handle */
#define  BGP4_MAX_INT_PEER_UPD2PROCESS_MIN (200)
#define  BGP4_MAX_INT_PEER_UPD2PROCESS_MAX (1600)
extern UINT4   u4MaxIntPeer;
#define  BGP4_MAX_INT_PEER_UPD2PROCESS (u4MaxIntPeer)

#define  BGP4_FIB_UPD_MAX_HOLD_TIME    (1)  /* max. no. of TICS after which
                                             * routes are processed for adding
                                             * to FIB */
#define  BGP4_SYNC_PRCS_INTERVAL       (60)   /* 60 seconds. */
#define  BGP4_NH_CHG_PRCS_DEF_INTERVAL     (60)   /* 60 seconds. */
#define  BGP4_NH_SCALE_FACTOR          3/4  /* This value * MAX_ROUTE decides
                                             * the size of the mempool
                                             * allocated for next hop
                                             * resolution
                                             */ 
/* This value decides the maximum number of routes that will be processing
 * while creating the aggregate entry.  */
#define  BGP4_MAX_AGGR_UP_RT2PROCESS_MIN   (200) 
#define  BGP4_MAX_AGGR_UP_RT2PROCESS_MAX   (500) 
extern UINT4   u4MaxAggrRt;
#define  BGP4_MAX_AGGR_UP_RT2PROCESS (u4MaxAggrRt)

/* This value decides the maximum number of routes that will be processing
 * while deleting the aggregate entry.  */
#define  BGP4_MAX_AGGR_DOWN_RT2PROCESS_MIN (1000) 
#define  BGP4_MAX_AGGR_DOWN_RT2PROCESS_MAX (3000) 
extern UINT4   u4MaxAggrDown;
#define  BGP4_MAX_AGGR_DOWN_RT2PROCESS (u4MaxAggrDown)

#define  BGP4_RM_PROCESS_ROUTES_THRESHOLD   20

/**************  BGP HASH TABLE MAX SIZE VALUES   *****************/

/* Definition to BGP4 Received Path Attribute Table. */
#define  BGP4_MAX_RCVD_PA_HASH_INDEX                (1024)

/* Definition to BGP4 Advetise Database. */
#define  BGP4_MAX_ADVT_PA_HASH_INDEX                (50)

#ifdef RFD_WANTED
/* Definition to RFD Route/Peer Hash Table. */
#define RFD_RTS_HASH_TABLE_SIZE                     (512)
#define RFD_PEERS_HASH_TABLE_SIZE                   (32)
#endif

/* Definition to BGP4 Hash Table MAX SIZE. */
#define BGP4_MAX_PREFIX_HASH_TBL_SIZE               (32)
#define BGP4_MAX_ROUTE_HASH_TBL_SIZE                (512)
#define COMM_MAX_HASH_BUCKET_SIZE                   (0x20)
#define EXT_COMM_MAX_HASH_BUCKET_SIZE               (0x20)

/***************  BGP HASH TABLE MAX SIZE VALUES   */
#ifdef L3VPN
#define  BGP4_MAX_PROCESS_VRF_CNT                   (100)
#define BGP4_MAX_VPNV4_RTS2PROCESS                (2000)
#define BGP4_MAX_PRCS_LSP_CHG                       (10)
#define  BGP4_MAX_PRCS_VRF_CHG                      (5)
#define  BGP4_VRF_CHG_MAX_RTS_CNT                   (100)
#endif

/* Defined to maintain with whomever other protocols BGP is registered with */
#define  BGP4_PROTO_IP_REGISTER         0x00000001
#define  BGP4_PROTO_IP_VPN_REGISTER     0x00000002
#define  BGP4_PROTO_MPLS_LBL_REGISTER   0x00000004

#define  SET_MASK                      0x80
#define  BGP_EVENT_FOR_RRD             0x00000001
#define  BGP4_ENABLE_FILTER            0x01
#define  BGP4_DISABLE_FILTER           0x02
#define  BGP4_RRD_DISABLE              0x00
#define  BGP4_RRD_ENABLE               0x01
#define  BGP4_6TO4_PREFIX              0x2002

#define  BGP4_LOOPBACK_IFACE           CFA_LOOPBACK

#ifdef L3VPN
/* This value must be in sync with LBL_ALLOC_BOTH_NUM defined in 
 * /mpls/lblmgr/inc/labelmgr.h
 */
#define BGP4_LBL_ALLOC_BOTH_NUM             1
#define BGP4_LBL_FAILURE                    0
#define BGP4_LBL_SUCCESS                    1
#endif
#define BGP4_CHECK_RCVD_EOR(Sup,Rcvd) (!(!((Sup)&&(Rcvd))&&((Sup)||(Rcvd))))

/* This macro defines all protocols for which the redistribution is supported. 
 * In future, if any additional protocol is added for redistribution, this
 * macro should be updated accordingly. */   
#define BGP4_ALL_PROTO_MASK \
            (BGP4_IMPORT_DIRECT | BGP4_IMPORT_STATIC | \
             BGP4_IMPORT_RIP | BGP4_IMPORT_OSPF | BGP4_IMPORT_ISISL1L2)
#define BGP4_ALL_MATCH_MASK \
            (BGP4_MATCH_EXT | BGP4_MATCH_INT | BGP4_MATCH_NSSA)
#define BGP4_RRD_ADMIN_STATE(u4CxtId)(gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u1BgpRRDConfiguration)
#define BGP4_RRD_PROTO_MASK(u4CxtId) (gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u4BgpRRDProtoMask)
#define BGP4_RRD_MATCH_MASK(u4CxtId) (gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u2BgpRRDMatchMask)
#define BGP4_RRD_RMAP_NAME(u4CxtId)  (gBgpCxtNode[u4CxtId]->Bgp4Redistribution.au1RMapName)

#define BGP4_RRD_METRIC_VAL(u4CxtId,u4Index)       (gBgpCxtNode[u4CxtId]->au4BGP4RRDMetricVal[u4Index - 1])
#define BGP4_RRD_METRIC_MASK(u4CxtId)              (gBgpCxtNode[u4CxtId]->u1BGP4RRDMetricMask)
#define BGP4_RRD_METRIC_MASK_SET(u4CxtId,u1Mask)   ((gBgpCxtNode[u4CxtId]->u1BGP4RRDMetricMask) |= u1Mask)
#define BGP4_RRD_METRIC_MASK_RESET(u4CxtId,u1Mask) ((gBgpCxtNode[u4CxtId]->u1BGP4RRDMetricMask) &= (~u1Mask))
                                                   

#define  BGP4_RRDIP_DISABLE_MASK_VALUE(u4CxtId) \
                    (BGP4_ALL_PROTO_MASK & \
                            (~(gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u4BgpRRDProtoMask)))

#define  BGP4_RRD_MATCH_DISABLE_MASK_VALUE(u4CxtId) \
                     (BGP4_ALL_MATCH_MASK & \
                             (~(gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u2BgpRRDMatchMask)))

#define BGP4_NON_BGP_EVENT_RCVD(u4CxtId) (gBgpCxtNode[u4CxtId]->Bgp4Redistribution.u1NonBgpEventRcvd)

#define  BGP4_TAG_AUTO_MASK       0x08000000
#define  BGP4_TAG_COMPLETE_MASK   0x40000000
#define  BGP4_TAG_AS_FLAG         0x0000ffff
#define  BGP4_TAG_FOR_DECISION    0x0f000000
#define  BGP4_TAG_1000            0x08000000
#define  BGP4_TAG_1001            0x09000000
#define  BGP4_TAG_1010            0x0a000000
#define  BGP4_TAG_1100            0x0c000000
#define  BGP4_TAG_1101            0x0d000000
#define  BGP4_TAG_1110            0x0e000000

/* For Klocwork fix code unreachable */
#define  BGP4_TAG1_1001            0x90000000
#define  BGP4_TAG1_1101            0xd0000000
#define  BGP4_TAG1_1010            0xa0000000
/* MACROS */
#define BGP4_TIMER_START(TimerListId, pTimer,u4Duration)\
    TmrStartTimer (TimerListId, pTimer,u4Duration * SYS_NUM_OF_TIME_UNITS_IN_A_SEC )

#define BGP4_TIMER_STOP(TimerId, pTimer) TmrStopTimer (TimerId, pTimer)

#define BGP4_GET_SYS_TIME(pu4CurrentTime )  \
  OsixGetSysTime ((tOsixSysTime *) (pu4CurrentTime))

#define bgp4RibUnlock() OsixGiveSem (0, (const UINT1 *)"RIB1")
#define bgp4RibLock() OsixTakeSem (0, (const UINT1 *)"RIB1", OSIX_WAIT, 0)

#define BGP4_RTREF_SOFT_PEERLIST(pMsg) (pMsg->QMsg.pSftCfgRtRefPeerList)

/* Macros for 2/4 byte get/assign between ptrs<->data */
/* ptr->data get/assign that does not require endian conversion */
#ifndef PTR_ASSIGN_2
#define PTR_ASSIGN_2(ptr, u2data)\
{\
    UINT2 u2Var = u2data;\
    MEMCPY(ptr, &u2Var, sizeof(UINT2));\
}
#endif
#ifndef PTR_FETCH_2
#define PTR_FETCH_2(u2data, ptr)\
    MEMCPY(&u2data, ptr, sizeof(UINT2))
#endif
#ifndef PTR_ASSIGN_4
#define PTR_ASSIGN_4(ptr,u4data)\
    MEMCPY(ptr, &u4data, sizeof(UINT4))
#endif
#ifndef PTR_FETCH_4
#define PTR_FETCH_4(u4data, ptr)\
    MEMCPY(&u4data, ptr, sizeof (UINT4))
#endif
#ifndef PTR_SET_2
#define PTR_SET_2(ptr, u2data)\
    MEMCPY(ptr, u2data, sizeof(UINT2))
#endif


/* ptr->data get/assign that requires endian conversion */
#ifndef PTR_ASSIGN2
#define PTR_ASSIGN2(ptr, u2data)  (*(UINT2 *)(ptr) = OSIX_NTOHS (u2data))
#endif
#ifndef PTR_FETCH2
#define PTR_FETCH2(u2data, ptr)  (u2data = OSIX_HTONS ((UINT2)* (UINT2 *)(ptr)))
#endif
#ifndef PTR_ASSIGN4
#define PTR_ASSIGN4(ptr,u4data)   (*(UINT4 *)(ptr) = OSIX_NTOHL (u4data))
#endif
#ifndef PTR_FETCH4
#define PTR_FETCH4(u4data, ptr)  (u4data = OSIX_HTONL ((UINT4)* (UINT4 *)(ptr)))
#endif

/* Structures and Typedefs */
typedef struct t_Bgp4Redistribution
{
    UINT4               u4BgpRRDProtoMask;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT2               u2BgpRRDMatchMask;
    UINT1               u1BgpRRDConfiguration;
    UINT1               u1NonBgpEventRcvd;
}tBgp4Redistribution;

#ifdef RRD_WANTED

/* QUEUE for interfacing with FutureRTM */
#define BGP4_RRD_Q_DEPTH         100 
#define BGP4_RRD_Q_STRING_SIZE   5 

typedef struct t_BgpRtmUpdate
{
    tRtmMsgHdr        RtmHdr;
    tRtmRtUpdateMsg   RtmUpdateMsg;
}tBgpRtmUpdMsg;

typedef struct t_BgpRtmRegnAck
{
    tRtmMsgHdr        RtmHdr;
    tRtmRegnAckMsg    RtmRegnMsg;
    tNetIpv4RtInfo    NetIpv4RtInfo;
}tBgpRtmRegnMsg;
#ifdef BGP4_IPV6_WANTED
typedef struct t_BgpRtm6RegnAck
{
    tRtm6MsgHdr        RtmHdr;
    tRtm6RegnAckMsg    RtmRegnMsg;
    tNetIpv6RtInfo     NetIpv6RtInfo;
}tBgpRtm6RegnMsg;
#endif
#endif

typedef struct t_BgpAggrChgMsg
{
    UINT4       u4Index;     /* Stores the index of the aggregate
                              * table. */
    INT4        i4Action;    /* Stores the new Action/policy
                              * set on the index. */
}tBgpAggrChgMsg;

typedef struct t_Bgp4PeerDftRouteMsg
{
    tAddrPrefix RemoteAddress;    /* Stores the peer address. */
    UINT1       u1DftRouteStatus; /* Default route advt status for the
                                   * peer. */
    UINT1       au1Pad[3];
}tBgp4PeerDftRouteMsg;

typedef struct t_Bgp4InterfaceMsg
{
       UINT4 u4IfIndex;
       UINT4 u4Status;
}tBgp4InterfaceMsg;


typedef struct t_Bgp4RouteInfoMsg {
    tAddrPrefix  DestAddr;
    tAddrPrefix  NextHop;
    UINT2  u2DestPrefixlen;
    UINT2  u2RtProtId;
    UINT4  u4RtIfIndex;
    INT4   i4Metric;
    UINT4 u4Action;
    UINT1  u1AlignmentByte;
    UINT1  au1Pad[3];
} tBgp4RouteInfoMsg;

typedef struct t_Bgp4TcpMd5AuthMsg
{
    tAddrPrefix RemoteAddress;
    UINT1       au1TcpMd5Passwd[BGP4_TCPMD5_PWD_SIZE];
    UINT1       u1TcpMd5PasswdLength;
    UINT1       au1Padding[3];
}tBgp4TcpMd5AuthMsg;

#ifdef L3VPN
typedef struct t_Bgp4Vpn4Msg
{
    union _Vpn4MsgType{
       tBgp4VrfName  VrfName;
       UINT4         u4LspId;
       UINT4         u4VrfId;
    }Vpn4MsgType;
    UINT4 u4Status;
    UINT1 au1RouteParam[8];
    UINT1 u1ParamType;
    UINT1 u1Action;      
    UINT1 au1Pad[2];
}tBgp4Vpn4Msg;

typedef struct t_Bgp4Vpn4RsvpTeMsg
{
    UINT4 u4WorkTnlIfIndex;
    UINT4 u4ProTnlIfIndex;
    UINT4 u4LspLabel;
    UINT1 u1LspStatus;
    UINT1 u1TnlOperStatus;
    UINT1 u1ProTnlOperStatus;
    UINT1 u1Action;
}tBgp4Vpn4RsvpTeMsg;

#endif

#ifdef EVPN_WANTED
typedef struct t_Bgp4VxlanMsg
{
    UINT4         u4VrfId;
    UINT4         u4VNId;
    UINT4         u4Status;
    UINT1         au1RouteParam[MAX_LEN_RD_VALUE];
    UINT1         au1IpAddr[BGP4_EVPN_IP6_ADDR_SIZE];
    INT4          i4IpAddrLen;
    INT4          i4SingleActiveFlag;
    UINT1         au1EthSegId[MAX_LEN_ETH_SEG_ID];
    tMacAddr      MacAddress;
    UINT1         u1Action;
    UINT1         u1ParamType;
    UINT1         u1EntryStatus;
    UINT1         au1Pad[1];
}tBgp4VxlanMsg;

#endif

#ifdef ROUTEMAP_WANTED
typedef struct t_Bgp4RMapStatusMsg
{
    UINT1   au1RMapName[RMAP_MAX_NAME_LEN];
    UINT4   u4Status;
}tBgp4RMapStatusMsg;
#endif

typedef struct t_Bgp4PathNotifyMsg
{
   tAddrPrefix  PeerAddr;
   BOOL1        b1IsOffLoaded;
   UINT1        au1Pad [3];
}tBgp4PathNotifyMsg;

typedef struct t_Bgp4QMsg
{
    UINT4                    u4MsgType;
    UINT4                    u4Flag; 
    UINT4                    u4ContextId;

    union _QMsgType{
#ifdef RRD_WANTED
        tBgpRtmUpdMsg            RtmUpdPtr;
        tBgpRtmRegnMsg           RtmRegnPtr;
        tRtmRtInfoResponseMsg    RtmInfoPtr;
#ifdef BGP4_IPV6_WANTED
        tBgpRtm6RegnMsg          Rtm6RegnPtr;
#endif        
#endif
        tAddrPrefix              RemoteAddress;
        tBgpAggrChgMsg           AggrChgMsg;
        tBgp4PeerDftRouteMsg     DftRouteMsg;
        UINT4                    u4Data; /* This variable can either hold the
                                          * actual value or the pointer to
                                          * actual value */
        tRtRefReqMsg    RtRefReqMsg;
#ifdef L3VPN
        tBgp4Vpn4Msg           Bgp4Vpn4Msg;
        tBgp4Vpn4RsvpTeMsg     Bgp4VpnRsvpTeMsg;
#endif
        tBgp4InterfaceMsg      Bgp4IfMsg;
 tBgp4RouteInfoMsg      Bgp4RtInfoMsg;
#ifdef ROUTEMAP_WANTED
        tBgp4RMapStatusMsg     Bgp4RMapStatusMsg;
#endif
        tBgp4TcpMd5AuthMsg     Bgp4TcpMd5AuthMsg;
        tBgp4VcmParams         Bgp4VcmMsg;
        tBgpPeerLocalASMSg     Bgp4PeerMsg;
        UINT1                  au1PeerGroupName[BGP_MAX_PEER_GROUP_NAME];
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
        tBgp4L2VpnEvtInfo       Bgp4L2vpnMsg;/*L2VPN message to BGP*/
#endif /*MPLS_WANTED*/
#endif /*VPLSADS_WANTED*/
        tBgp4PathNotifyMsg      Bgp4PathNotifyMsg;
#ifdef EVPN_WANTED
        tBgp4VxlanMsg           Bgp4VxlanMsg;
#endif
    }QMsg; 
}tBgp4QMsg;

/*Converts decimal string to unsigned long*/
#define BGP_DECSTRTOUL(x)              strtoul((const CHR1 *)x,(CHR1 **)NULL,10)


/* Prototype declarations */


/* BGP4MAIN_C */
INT4    Bgp4ProcessQMsg(const UINT1 *au1Qname);
INT4    Bgp4Enqueue (tBgp4QMsg *);                     
INT4    Bgp4InitPeer (UINT4);
INT4    Bgp4InitEnv (UINT4);
INT4    Bgp4CreatePools (VOID);
INT4    Bgp4InitTimer (VOID);
INT4    Bgp4IsCpuNeeded(VOID);
INT1    Bgp4GlobalAdminStatusHandler (UINT4, UINT4); 
INT4    Bgp4RtRefMemInit(UINT4);
INT4    Bgp4TcpMd5MemInit(UINT4);
INT4    Bgp4RtRefShutdown (UINT4);
INT4    Bgp4GlobalMemoryRelease(UINT4);
VOID    Bgp4InitRedistribution (UINT4);
INT1    Bgp4EnqueMsgToBgpQ (UINT4, UINT4 , UINT4 );
VOID    Bgp4InitFiltering (UINT4);

/* BGP4MEM_C */
tBgp4PeerEntry     *Bgp4AllocatePeerEntry ( UINT4, UINT4 );
INT4                Bgp4ReleasePeerEntry (tBgp4PeerEntry *);
UINT1              *Bgp4MemAllocateMsg (UINT4);
INT4                Bgp4MemReleaseMsg (UINT1 *);
tRouteProfile      *Bgp4MemAllocateRouteProfile (UINT4);
INT4                Bgp4MemReleaseRouteProfile (tRouteProfile *);
tLinkNode          *Bgp4MemAllocateLinkNode (UINT4);
tAdvRtLinkNode     * Bgp4MemAllocateAdvRtLinkNode (UINT4 );
INT4                Bgp4MemReleaseLinkNode (tLinkNode *);
INT4                Bgp4MemReleaseAdvtLinkNode (tAdvRtLinkNode * );
tBgp4Info          *Bgp4MemAllocateBgp4info (UINT4);
INT4                Bgp4MemReleaseBgp4info (tBgp4Info *);
tBgp4RcvdPathAttr  *Bgp4MemAllocatePAInfo (UINT4);
INT4                Bgp4MemReleasePAInfo (tBgp4RcvdPathAttr *);
tBgp4AdvtPathAttr  *Bgp4MemAllocateAdvtPAInfo (UINT4);
INT4                Bgp4MemReleaseAdvtPAInfo (tBgp4AdvtPathAttr *);
tAsPath            *Bgp4MemGetASNode (UINT4);
INT4                Bgp4MemReleaseASNode (tAsPath *);
tBgp4QMsg          *Bgp4MemAllocateQMsg (UINT4);
INT4                Bgp4DeallocatePeerDataList (tBgp4PeerEntry **);
INT4                Bgp4MemReleasePeerList(tTMO_SLL *);
tTcpMd5AuthPasswd * Bgp4MemAllocateTcpMd5Buf (UINT4,UINT4 );
INT4                Bgp4MemReleaseTcpMd5Buf (tTcpMd5AuthPasswd *,UINT4);
INT4                Bgp4MemReleasePeerAdvtList (tBgp4PeerEntry *);
INT4                Bgp4AllocateIgpMetricEntry (tBgp4IgpMetricEntry **);
INT4                Bgp4ReleaseIgpMetricEntry (tBgp4IgpMetricEntry *);
tTcpAoAuthMKT *     Bgp4MemAllocateTcpAoBuf (UINT4 , UINT4 );
INT4                Bgp4MemReleaseTcpAoBuf (tTcpAoAuthMKT* , UINT4 );

#ifdef L3VPN
INT4                Bgp4MemReleaseRrImportTarget (tBgp4RrImportTargetInfo *);
INT4                Bgp4MemReleaseVpnRtInstallVrfNode (tRtInstallVrfInfo *);
#endif

#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
tVplsSpecInfo  * Bgp4MemAllocateVplsSpecEntry (UINT4 u4Size);
INT4                  Bgp4MemReleaseVplsSpecEntry (tVplsSpecInfo * pVplsSpecInfo);
#endif /*MPLS_WANTED*/
INT4                  Bgp4MemReleaseRrImportTargetVpls (tBgp4VplsRrImportTargetInfo * pRrImportTarget);
tBgp4VplsRrImportTargetInfo * Bgp4MemAllocateRrImportTargetVpls (UINT4 u4Size);
#endif /*VPLSADS_WANTED*/
tBgp4IfInfo        *Bgp4MemAllocateIfEntry (UINT4);
INT4                Bgp4MemReleaseIfEntry (tBgp4IfInfo * );
tNetworkAddr       *Bgp4AllocateNetworkRtEntry(tNetworkAddr *);
INT4                Bgp4ReleaseNetworkRtEntry(tNetworkAddr *);
/* BGP4PORT_C */

INT4 BgpIpRtLookup (UINT4, tAddrPrefix *, UINT2, tAddrPrefix *,
                    INT4 *, UINT4 *,INT1 );
INT4 Bgp4GetDefaultRouteFromFDB (UINT4, UINT2);
INT4 BGP4CanRepRouteBeAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4 BGP4RTAddRouteToCommIpRtTbl (tRouteProfile * );
INT4 BGP4RTDeleteRouteInCommIpRtTbl (tRouteProfile * );
INT4 BGP4RTModifyRouteInCommIpRtTbl (tRouteProfile * );
UINT1 Bgp4IsDirectlyConnected (tAddrPrefix, UINT4 );
BOOL1 Bgp4IsOnSameSubnet (tAddrPrefix , tBgp4PeerEntry * );
UINT2 Bgp4GetNetAddrPrefixLen (tAddrPrefix);
INT4 Bgp4GetLocalAddrForPeer (tBgp4PeerEntry *, tAddrPrefix , tNetAddress *,
                              UINT1);
INT4 Bgp4DftLocalAddrForPeer (tBgp4PeerEntry *, tAddrPrefix , tNetAddress *,
                              UINT1 , UINT2);
#ifdef ROUTEMAP_WANTED
INT1
Bgp4CmpFilterRMapName (tFilteringRMap * pFilterRMap,
                       tSNMP_OCTET_STRING_TYPE * pRMapName);
#endif

INT4    Bgp4RegisterWithIP (UINT4);
INT4    Bgp4DeRegisterWithIP (UINT4);
#ifdef L3VPN
INT4 Bgp4CreateLabelSpace(VOID);
INT4 Bgp4DeleteLabelSpace (VOID);
#endif

/* General Routines */
UINT1               Bgp4DisableRRD (UINT4);

#ifdef RRD_WANTED
EXTERN INT1
nmhSetFsRrdRouterId (UINT4 );
EXTERN INT1
nmhSetFsRrdRouterASNumber (INT4 );
EXTERN INT1
nmhSetFsRrdAdminStatus (INT4 );
#endif

INT4 Bgp4TcphOpenConnection (tBgp4PeerEntry * );
INT4 Bgp4TcphCheckIncomingConn (UINT4, UINT2 );
INT4 Bgp4TcphFillAddresses (tBgp4PeerEntry * );
INT4 Bgp4TcphOpenListenPort (UINT4, UINT2);
VOID                BgpShowHoldAdvtPeerInfo (tCliHandle CliHandle);

EXTERN INT4 NetIpv4DeRegisterHigherLayerProtocolInContext(UINT4, UINT1);

extern INT4 RTMGetForwardingStateInCxt (UINT4);
extern INT4 RTM6GetForwardingStateInCxt (UINT4);
extern INT4 RtmGetIgpConvergenceStateInCxt (UINT4);
extern INT4 Rtm6GetIgpConvergenceStateInCxt (UINT4);

#ifdef BGP4_IPV6_WANTED
INT1  SendingMessageToRRDQueue6 (UINT4, UINT4 , UINT1 , UINT1*, UINT2);
INT4  Bgp4RecvMsgFromRtm6 (tRtm6RespInfo *, tRtm6MsgHdr *);
INT4  Bgp4Rtm6ProcessUpdate (tNetIpv6RtInfo *, tRtm6MsgHdr *);
INT1  Bgp4Rtm6ProcessRtChange (tNetIpv6RtInfo *, tRtm6MsgHdr *);
VOID Bgp4ProtoIdToRtm6 (INT1 *pi1ProtoId);
VOID Bgp4ProtoIdFromRtm6 (UINT2 *pu2ProtoId);
UINT1 BgpIp6FilterRouteSource (UINT4, tRouteProfile * pRtProfile);
#endif
INT1  SendingMessageToRRDQueue (UINT4, UINT4, UINT1, UINT1*,UINT2);
INT4  Bgp4RecvMsgFromRtm (tRtmRespInfo *, tRtmMsgHdr *);
INT4  Bgp4DeInitInCxt (UINT4 u4Context);


INT4 Bgp4RegisterWithBfd (UINT4, tBgp4PeerEntry *);
INT4 Bgp4DeRegisterWithBfd (UINT4, tBgp4PeerEntry *, UINT1);
#ifdef BFD_WANTED
INT1 Bgp4HandleNbrPathStatusChange (UINT4 u4ContextId,
                                    tBfdClientNbrIpPathInfo *pNbrPathInfo);
#endif

#define BGP_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#endif /* BGP4PORT_H */
