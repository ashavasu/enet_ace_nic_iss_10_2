/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgvxipf4.c,v 1.4 2014/11/18 13:38:26 siva Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGVXIPF4_C
#define BGVXIPF4_C
#include "bgp4com.h"

/*****************************************************************************/
/* Function Name :  BgpIp4RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP Fwd Table                                      */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Mask of the route entry              (u4IpMask)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pu4Rt)             */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp4RtLookup (UINT4 u4Ipaddr, UINT2 u2PrefixLen, UINT4 *pu4Rt,
                INT4 *pi4Metric, UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
    M2_IPROUTETBL       IpRouteTblEntry;
    UINT4               u4Mask, u4MaskLen;

    /* However for synchronisation lookup this needs to be used. For temporily
     * set as usused. */
    UNUSED_PARAM (u2PrefixLen);

    MEMSET (&IpRouteTblEntry, 0, sizeof (M2_IPROUTETBL));

    for (u4MaskLen = DEFAULT_MASK_LEN; u4MaskLen <= BGP4_MAX_PREFIXLEN;
         u4MaskLen += MASK_SHIFT_LEN)
    {
        u4Mask = Bgp4GetSubnetmask ((UINT1) u4MaskLen);
        IpRouteTblEntry.ipRouteDest = u4Ipaddr & u4Mask;

        if (m2IpRouteTblEntryGet (M2_EXACT_VALUE,
                                  &IpRouteTblEntry) == VXWORKS_SUCCESS)
        {
            break;
        }
    }

    if (u4MaskLen == BGP4_MAX_PREFIXLEN)
    {
        *pu4Rt = BGP4_INVALID_ROUTE;
        *pi4Metric = BGP4_INVALID_NH_METRIC;
        *pu4RtIfIndx = BGP4_INVALID_IFINDX;
        BGP4_DBG (BGP4_DBG_ALL, "\r Ip RtTbl Lookup Failed \n");
        return BGP4_FAILURE;
    }

    if (i1AppId == -1)            /* Check for all the Protocols */
    {
        *pu4Rt = IpRouteTblEntry.ipRouteNextHop;
        *pi4Metric = IpRouteTblEntry.ipRouteMetric1;
        *pu4RtIfIndx = IpRouteTblEntry.ipRouteIfIndex;
    }
    else if (i1AppId == BGP4_LOCAL_ID)    /* Check only for the direct routes */
    {
        if (IpRouteTblEntry.ipRouteProto == i1AppId)
        {
            *pu4Rt = IpRouteTblEntry.ipRouteNextHop;
            *pi4Metric = IpRouteTblEntry.ipRouteMetric1;
            *pu4RtIfIndx = IpRouteTblEntry.ipRouteIfIndex;
        }
        else
        {
            *pu4Rt = BGP4_INVALID_ROUTE;
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4RtIfIndx = BGP4_INVALID_IFINDX;
            return BGP4_FAILURE;
        }
    }
    else if (i1AppId > BGP4_LOCAL_ID && i1AppId < BGP_ID)
        /* Check for the IGP routes */
    {
        if (IpRouteTblEntry.ipRouteProto == i1AppId)
        {
            *pu4Rt = IpRouteTblEntry.ipRouteNextHop;
            *pi4Metric = IpRouteTblEntry.ipRouteMetric1;
            *pu4RtIfIndx = IpRouteTblEntry.ipRouteIfIndex;
        }
        else
        {
            *pu4Rt = BGP4_INVALID_ROUTE;
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4RtIfIndx = BGP4_INVALID_IFINDX;
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv4*/
/*                 FDB and if present then add the default route to BGP IPv4 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv4GetDefaultRouteFromFDB (VOID)
{
    M2_IPROUTETBL       IpRouteTblEntry;
    tNetAddress         NetAddr;
    tAddrPrefix         AddrPrefix;
    UINT4               u4Mask = 0;
    INT4                i4RetStatus = BGP4_SUCCESS;

    if (BGP4_RRD_PROTO_MASK == 0)
    {
        /* Redistribution is not enabled. No route will be learnt from FDB. */
        return BGP4_SUCCESS;
    }

    Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&AddrPrefix, BGP4_INET_AFI_IPV4);

    MEMSET (&IpRouteTblEntry, 0, sizeof (M2_IPROUTETBL));

    /* Try and get the Default route. */
    IpRouteTblEntry.ipRouteDest = 0;

    if (m2IpRouteTblEntryGet (M2_EXACT_VALUE,
                              &IpRouteTblEntry) == VXWORKS_FAILURE)
    {
        /* Default route is not present in the FDB. */
        BGP4_DBG (BGP4_DBG_ALL, "\rDefault Route Lookup Failed\n");
        return BGP4_FAILURE;
    }

    PTR_ASSIGN_4 (AddrPrefix.au1Address, IpRouteTblEntry.ipRouteNextHop);
    i4RetStatus = Bgp4RouteLeak (NetAddr, IpRouteTblEntry.ipRouteProto,
                                 AddrPrefix, BGP4_IMPORT_RT_ADD, 0,
                                 IpRouteTblEntry.ipRouteMetric1,
                                 IpRouteTblEntry.ipRouteIfIndex, 0);
    if (i4RetStatus == BGP4_FAILURE)
    {
        /* Adding default route to BGP fails. */
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv4RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv4 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv4RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
    /* For VxWorks currently, we are not able to replace the old route with
     * the new route. */
    UNUSED_PARAM (pNewRtProfile);
    UNUSED_PARAM (pOldRtProfile);
    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIp4RtTbl                              */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP Forwarding.                   */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4Mask;
    UINT4               u4Tos = 0;
    UINT4               u4Flags = 0;
    UINT1               u1Protocol;
    tBgp4Info          *pRouteInfo;
    UNUSED_PARAM (u4VrfId);

    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));

    u4Mask = OSIX_NTOHL (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    u1Protocol = BGP4_RT_PROTOCOL (pRtProfile);

    if ((mRouteEntryAdd (u4Dest, u4Gate, u4Mask,
                         u4Tos, u4Flags, u1Protocol)) < 0)
        /* Calling Routine to Add Route to the IP Table */
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp4RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4Mask;
    UINT4               u4Tos = 0;
    UINT4               u4Flags = 0;
    UINT1               u1Protocol;
    tBgp4Info          *pRouteInfo;
    UNUSED_PARAM (u4VrfId);
    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));

    u4Mask = OSIX_NTOHL (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    u1Protocol = BGP4_RT_PROTOCOL (pRtProfile);

    if ((mRouteEntryAdd (u4Dest, u4Gate, u4Mask,
                         u4Tos, u4Flags, u1Protocol)) < 0)
        /* Calling Routine to Add Route to the IP Table */
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIp4RtTbl                           */
/* Description   : This routine is used to delete an entry present in the    */
/*                 TRIE routing table.                                       */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile * pRtProfile)
{
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4Mask;
    UINT4               u4Protocol;
    UINT4               u4Tos = 0;
    UINT4               u4Flags = 0;
    tBgp4Info          *pRouteInfo;

    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));

    u4Mask = OSIX_NTOHL (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    u4Protocol = BGP4_RT_PROTOCOL (pRtProfile);

    if ((mRouteEntryDelete (u4Dest, u4Gate, u4Mask,
                            u4Tos, u4Flags, u4Protocol)) < 0)
        /* Calling Routine to Delete Route from IP Table */
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : BgpGetIpIfAddr                                             */
/* Description   : This routine Gets the address of the given interface       */
/* Input(s)      : Interface index                                            */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface Address or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfAddr (UINT2 u2IfIndex)
{
    M2_INTERFACETBL     IfTblEntry;

    UINT4               u4IfAddr = 0;
    INT1                i1IpAddr[INET_ADDR_LEN];

    MEMSET (&IfTblEntry, 0, sizeof (M2_INTERFACETBL));
    MEMSET (i1IpAddr, 0, INET_ADDR_LEN);

    IfTblEntry.ifIndex = u2IfIndex;
    if (m2IfTblEntryGet (M2_EXACT_VALUE, &IfTblEntry) != VXWORKS_FAILURE)
    {
        ifAddrGet (IfTblEntry.ifDescr, i1IpAddr);
        u4IfAddr = OSIX_NTOHL (inet_addr (i1IpAddr));
    }
    return u4IfAddr;
}

/******************************************************************************/
/* Function Name : BgpGetIpIfMask                                             */
/* Description   : This routine Gets the netmask of the given interface       */
/* Input(s)      : Interface index                                            */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface netmask or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfMask (UINT2 u2IfIndex)
{

    M2_INTERFACETBL     IfTblEntry;
    UINT4               u4IfMask = 0;
    INT1                i1IpMask[INET_ADDR_LEN];

    MEMSET (&IfTblEntry, 0, sizeof (M2_INTERFACETBL));
    MEMSET (i1IpMask, 0, INET_ADDR_LEN);

    IfTblEntry.ifIndex = u2IfIndex;
    if (m2IfTblEntryGet (M2_EXACT_VALUE, &IfTblEntry) != VXWORKS_FAILURE)
    {
        ifMaskGet (IfTblEntry.ifDescr, i1IpMask);
        u4IfMask = OSIX_NTOHL (inet_addr (i1IpMask));
    }
    return u4IfMask;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (u4Ipaddr1, u4Ipaddr2)                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv4IsOnSameSubnet (UINT4 u4NextHopAddr, UINT4 u4PeerAddr)
{
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    UINT2               u2Index = 0;

    /* The max interface index value as 10 is just a temp
     * assumption. This needs to be updated properly.
     */
    for (u2Index = 0; u2Index < 10; u2Index++)
    {
        u4IfAddr = BgpGetIpIfAddr (u2Index);
        u4IfMask = BgpGetIpIfMask (u2Index);
        if ((u4IfAddr == 0) || (u4IfMask == 0))
        {
            /* No Interface available to the corresponding index value */
            break;
        }

        /* Check whether input IpAddress and u4IfAddr belongs to the
         * same subnet or not */
        if (((u4NextHopAddr & u4IfMask) == (u4IfAddr & u4IfMask)) &&
            ((u4PeerAddr & u4IfMask) == (u4IfAddr & u4IfMask)))
        {
            return TRUE;
        }
    }
    return FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4GetDefaultBgpIdentifier                               */
/* Description   : FutureBgp4 uses the highest interface address as the      */
/*               : default Bgp Identifier. This function checks the          */
/*               : interface table and gets the highest interface address.   */
/*               : If no interface is configured, then "0" is returned.      */
/*               : as the Bgp Identifier                                     */
/* Input(s)      : None.                                                     */
/* Output(s)     : Default BGP Identifier (pu4BgpIdentifier)                 */
/* Return(s)     : BGP4_SUCCESS - if a valid interface address is availble   */
/*               : BGP4_FAILURE - if no interface address is available. In   */
/*                                this case the pu4BgpIdentifier value is 0  */
/*****************************************************************************/
INT1
Bgp4GetDefaultBgpIdentifier (UINT4 *pu4BgpIdentifier)
{
    UINT4               u4BgpId = 0;
    UINT4               u4TmpId = 0;
    UINT2               u2Index = 0;

    /* The max interface index value as 10 is just a temp
     * assumption. This needs to be updated properly.
     */
    for (u2Index = 0; u2Index < 10; u2Index++)
    {
        u4TmpId = BgpGetIpIfAddr (u2Index);
        u4BgpId = (u4TmpId > u4BgpId) ? u4TmpId : u4BgpId;
    }
    *pu4BgpIdentifier = u4BgpId;

    if (*pu4BgpIdentifier != 0)
        return BGP4_SUCCESS;

    return BGP4_FAILURE;

}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv4IsDirectlyConnected (UINT4 u4IpAddress)
{
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    UINT2               u2Index = 0;

    /* The max interface index value as 10 is just a temp
     * assumption. This needs to be updated properly.
     */
    for (u2Index = 0; u2Index < 10; u2Index++)
    {
        u4IfAddr = BgpGetIpIfAddr (u2Index);
        u4IfMask = BgpGetIpIfMask (u2Index);
        if ((u4IfAddr == 0) || (u4IfMask == 0))
        {
            /* No Interface available to the corresponding index value */
            break;
        }

        /* Check whether input IpAddress and u4IfAddr belongs to the
         * same subnet or not */
        if ((u4IpAddress & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            return BGP4_TRUE;
        }
    }

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetNetAddrPrefixLen                               */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address that needs to be checked (u4RemAddr)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv4GetNetAddrPrefixLen (UINT4 u4RemAddr)
{
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    UINT2               u2Index = 0;
    UINT2               u2PrefixLen = 0;

    /* The max interface index value as 10 is just a temp
     * assumption. This needs to be updated properly.
     */
    for (u2Index = 0; u2Index < 10; u2Index++)
    {
        u4IfAddr = BgpGetIpIfAddr (u2Index);
        u4IfMask = BgpGetIpIfMask (u2Index);
        if ((u4IfAddr == 0) || (u4IfMask == 0))
        {
            /* No Interface available to the corresponding index value */
            return 0;
        }

        /* Check whether input IpAddress and u4IfAddr belongs to the
         * same subnet or not */
        if ((u4IpAddress & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            break;
        }
    }

    if (u2Index >= 10)
    {
        return 0;
    }

    u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4IfMask);
    return u2PrefixLen;
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv4LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv4LocalAddrForRemoteAddr (tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
    UINT4               u4Rt = BGP4_INVALID_ROUTE;
    UINT4               u4RtIfIndx = BGP4_INVALID_IFINDX;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4RemAddr;
    UINT4               u4LocalAddr = 0;
    UINT4               u4LocalMask = 0;

    PTR_FETCH_4 (u4RemAddr, BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (RemAddr));

    BgpIp4RtLookup (u4RemAddr, BGP4_MAX_PREFIXLEN,
                    &u4Rt, &i4Metric, &u4RtIfIndx, BGP4_ALL_APPIDS);
    if ((u4Rt != BGP4_INVALID_ROUTE) && (u4RtIfIndx != BGP4_INVALID_IFINDX))
    {
        u4LocalAddr = BgpGetIpIfAddr ((UINT2) (u4RtIfIndx));
        u4LocalMask = BgpGetIpIfMask ((UINT2) (u4RtIfIndx));
    }
    if (u4LocalAddr == 0)
    {
        return BGP4_FAILURE;
    }
    PTR_ASSIGN_4 (pLocalAddr->NetAddr.au1Address, u4LocalAddr);
    pLocalAddr->u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4LocalMask);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetIfAddress                                          */
/* Description   : This function gets the address corresponding to the given */
/*               : interface id. If no interface configured then the return  */
/*               : zero.                                                     */
/* Input(s)      : Interface Id (i4IfId)                                     */
/* Output(s)     : Interface Address (pu4IfAddr)                             */
/* Return(s)     : BGP4_SUCCESS - if a valid interface address is availble   */
/*               : BGP4_FAILURE - if no interface address is available. In   */
/*                                this case the pu4IfAddr value is 0         */
/*****************************************************************************/
INT1
Bgp4GetIfAddress (INT4 i4IfId, UINT4 *pu4IfAddr)
{
    *pu4IfAddr = BgpGetIpIfAddr ((UINT2) i4IfId);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IpIsLocalAddrInCxt                                        */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
IpIsLocalAddrInCxt (UINT4 u4CxtId, UINT4 u4Addr, UINT2 *pu2Port)
{
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    UINT2               u2Index = 0;

    UNUSED_PARAM (u4CxtId);
    /* The max interface index value as 10 is just a temp
     * assumption. This needs to be updated properly.
     */
    for (u2Index = 0; u2Index < 10; u2Index++)
    {
        u4IfAddr = BgpGetIpIfAddr (u2Index);
        u4IfMask = BgpGetIpIfMask (u2Index);
        if ((u4IfAddr == 0) || (u4IfMask == 0))
        {
            /* No Interface available to the corresponding index value */
            break;
        }

        /* Check whether input IpAddress and u4IfAddr belongs to the
         * same subnet or not */
        if ((u4Addr & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            *pu2Port = u2Index;
            return IP_SUCCESS;
        }
    }
    return IP_FAILURE;
}

#endif /* BGVXIPF4_C */
