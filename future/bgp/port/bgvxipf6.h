#ifndef BGFSIPF6_H
#define BGFSIPF6_H

#include "ipv6.h"
#include "slimacs.h"
#include "bgp4com.h"

/* Prototype definitions */
INT4  BgpIp6RtLookup (tAddrPrefix *, UINT2 , tAddrPrefix *, INT4 *, UINT4 *, 
                      INT1 );
INT4  Bgp4Ipv6GetDefaultRouteFromFDB (VOID);
INT4  BGP4CanRepIpv6RtAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4  BGP4RTAddRouteToCommIp6RtTbl(tRouteProfile * );
INT4  BGP4RTDeleteRouteInCommIp6RtTbl (tRouteProfile * );
INT4  BGP4RTModifyRouteInCommIp6RtTbl (tRouteProfile * );
BOOL1 Bgp4Ipv6IsOnSameSubnet (tAddrPrefix *, tAddrPrefix *);
UINT1 Bgp4Ipv6IsDirectlyConnected (tAddrPrefix *);
UINT2 Bgp4Ipv6GetNetAddrPrefixLen (tAddrPrefix *);
INT4  Bgp4FillLinkLocalAddress(tBgp4PeerEntry *);
INT4  Ip6IsLocalAddr (UINT1 *, UINT2 *);
INT4  Ip6IsLocalSubnet (UINT1 *, UINT2 *);
INT4  BgpGetIp6IfAddr (UINT2 , UINT1 *, UINT1 *);
INT4  Bgp4GetIpv6LocalAddrForRemoteAddr( tAddrPrefix , tNetAddress *);


#define BGP4_IN6_IS_ADDR_UNSPECIFIED(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr;\
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN);\
    if (IN6_IS_ADDR_UNSPECIFIED (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}
#define BGP4_IN6_IS_ADDR_MULTICAST(pAddr, Status) \
{ \
    tIp6Addr    Ipv6Addr;\
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IN6_IS_ADDR_MUTLICAST (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_LOOPBACK(pAddr, Status) \
{ \
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IS_ADDR_LOOPBACK (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_LINKLOCAL(pAddr, Status) \
{ \
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IN6_IS_ADDR_LINKLOCAL (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define IS_ADDR_V4_COMPAT(a)   ( (a).u4_addr[3] != 0 && \
                                 (a).u4_addr[2] == 0 && \
                                 (a).u4_addr[1] == 0 && \
                                 (a).u4_addr[0] == 0 )

#define BGP4_IN6_IS_ADDR_V4COMPAT(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IN6_IS_ADDR_COMPAT (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_V4MAPPED(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IN6_IS_ADDR_V4MAPPED (Ipv6Addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_V4COMPATIBLE(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
    if (IN6_IS_ADDR_COMPAT (Ipv6Addr))  { Status = BGP4_TRUE; } \
        else if (IN6_IS_ADDR_V4MAPPED (Ipv6Addr))  { Status = BGP4_TRUE; } \
            else { Status = BGP4_FALSE; } \
}

#endif /* BGFSIPF6_H */
