/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgfsipf4.c,v 1.27 2017/09/15 06:19:51 siva Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGFSIPF4_C
#define BGFSIPF4_C
#include "bgp4com.h"

/*****************************************************************************/
/* Function Name :  BgpIp4RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP Fwd Table                                      */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Mask of the route entry              (u4IpMask)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pu4Rt)             */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp4RtLookup (UINT4 u4VrfId, UINT4 u4Ipaddr, UINT2 u2PrefixLen, UINT4 *pu4Rt,
                INT4 *pi4Metric, UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4Mask = 0;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    u4Mask = Bgp4GetSubnetmask ((UINT1) u2PrefixLen);

    RtQuery.u4DestinationIpAddress = u4Ipaddr;
    RtQuery.u4DestinationSubnetMask = u4Mask;

    if (i1AppId == BGP4_ALL_APPIDS)
    {
        RtQuery.u2AppIds = 0;
        RtQuery.u1QueryFlag = QUERY_FOR_NEXTHOP;
    }
    else if (i1AppId == BGP4_LOCAL_ID)
    {
        RtQuery.u2AppIds = RTM_DIRECT_MASK;
        RtQuery.u1QueryFlag = QUERY_FOR_NEXTHOP;
    }
    else
    {
        RtQuery.u2AppIds =
            RTM_RIP_MASK | RTM_OSPF_MASK | RTM_ISIS_MASK | RTM_STATIC_MASK;
        RtQuery.u1QueryFlag = QUERY_FOR_DESTINATION;
    }
    RtQuery.u4ContextId = u4VrfId;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        RtQuery.u4DestinationIpAddress = 0;
        RtQuery.u4DestinationSubnetMask = 0;
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
        {
            BGP4_DBG (BGP4_DBG_ALL,
                      "\tBgpIp4RtLookup() :  Ip Route Lookup FAILED !!! \n");
            *pu4Rt = BGP4_INVALID_ROUTE;
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4RtIfIndx = BGP4_INVALID_IFINDX;
            return BGP4_FAILURE;
        }
    }

    BGP4_DBG (BGP4_DBG_ALL,
              "\tBgpIp4RtLookup() :  Ip Route Lookup Succeeded !!! \n");
    *pu4Rt = NetIpRtInfo.u4NextHop;
    *pi4Metric = NetIpRtInfo.i4Metric1;
    *pu4RtIfIndx = NetIpRtInfo.u4RtIfIndx;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name :  BgpIp4GetNextRtEntry                                     */
/* Description   :  This routine is used to get the next alternate route     */
/*                  for a given destination                                  */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Mask of the route entry              (u4IpMask)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pu4Rt)             */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp4GetNextRtEntry (UINT4 u4VrfId, UINT4 u4Ipaddr,
                      UINT2 u2PrefixLen, UINT4 u4IfIndex, UINT4 *pu4Rt,
                      INT4 *pi4Metric, UINT4 *pu4RtIfIndx)
{
    INT4                i4RetVal = BGP4_FAILURE;

#ifdef RRD_WANTED
    UNUSED_PARAM (u4VrfId);
    UNUSED_PARAM (u4Ipaddr);
    UNUSED_PARAM (u2PrefixLen);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4Rt);
    UNUSED_PARAM (pi4Metric);
    UNUSED_PARAM (pu4RtIfIndx);
    return i4RetVal;
#else

    tRtInfo             InRtInfo;
    tRtInfo             OutRtInfo;

    InRtInfo.u4DestNet = u4Ipaddr;
    InRtInfo.u4DestMask = Bgp4GetSubnetmask ((UINT1) u2PrefixLen);
    InRtInfo.u4RtIfIndx = u4IfIndex;

    if (u4VrfId == BGP4_DFLT_VRFID)
    {
        i4RetVal = IpGetNextMultiPathRtEntry (InRtInfo, &OutRtInfo);
        if (i4RetVal == IP_SUCCESS)
        {
            *pu4Rt = OutRtInfo.u4NextHop;
            *pi4Metric = OutRtInfo.i4Metric1;
            *pu4RtIfIndx = OutRtInfo.u4RtIfIndx;
            return BGP4_SUCCESS;
        }
        else
        {
            return BGP4_FAILURE;
        }
    }
    return (BGP4_SUCCESS);
#endif
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv4*/
/*                 FDB and if present then add the default route to BGP IPv4 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv4GetDefaultRouteFromFDB (UINT4 u4ContextId)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetAddress         NetAddr;
    tAddrPrefix         AddrPrefix;
    INT4                i4RetStatus = BGP4_SUCCESS;
    UINT4               u4ProtoId = 0;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&AddrPrefix, BGP4_INET_AFI_IPV4);

    RtQuery.u4DestinationIpAddress = 0;
    RtQuery.u4DestinationSubnetMask = 0;

    if (BGP4_RRD_PROTO_MASK (u4ContextId) == 0)
    {
        /* Redistribution is not enabled. No route will be learnt from FDB. */
        return BGP4_FAILURE;
    }

    if ((BGP4_RRD_PROTO_MASK (u4ContextId) & BGP4_IMPORT_DIRECT) ==
        BGP4_IMPORT_DIRECT)
    {
        RtQuery.u2AppIds |= RTM_DIRECT_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (u4ContextId) & BGP4_IMPORT_STATIC) ==
        BGP4_IMPORT_STATIC)
    {
        RtQuery.u2AppIds |= RTM_STATIC_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (u4ContextId) & BGP4_IMPORT_RIP) ==
        BGP4_IMPORT_RIP)
    {
        RtQuery.u2AppIds |= RTM_RIP_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (u4ContextId) & BGP4_IMPORT_OSPF) ==
        BGP4_IMPORT_OSPF)
    {
        RtQuery.u2AppIds |= RTM_OSPF_MASK;
    }

    RtQuery.u1QueryFlag = QUERY_FOR_DESTINATION;

    RtQuery.u4ContextId = u4ContextId;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        /* Default route is not present in the FDB. */
        return BGP4_FAILURE;
    }

    if (NetIpRtInfo.u2RtProto == BGP4_OSPF_ID)
    {
        u4ProtoId = BGP4_OSPF_METRIC;
    }
    else if (NetIpRtInfo.u2RtProto == BGP4_STATIC_ID)
    {
        u4ProtoId = BGP4_STATIC_METRIC;
    }
    else if (NetIpRtInfo.u2RtProto == BGP4_LOCAL_ID)
    {
        u4ProtoId = BGP4_DIRECT_METRIC;
    }
    else if (NetIpRtInfo.u2RtProto == BGP4_RIP_ID)
    {
        u4ProtoId = BGP4_RIP_METRIC;
    }
    else if (NetIpRtInfo.u2RtProto == BGP4_ISIS_ID)
    {
        u4ProtoId = BGP4_ISIS_METRIC;
    }

    if ((u4ProtoId > 0)
        && ((BGP4_RRD_METRIC_MASK (NetIpRtInfo.u4ContextId) & u4ProtoId) ==
            u4ProtoId))
    {
        NetIpRtInfo.i4Metric1 =
            BGP4_RRD_METRIC_VAL (NetIpRtInfo.u4ContextId, u4ProtoId);
    }

    /* Default route is present in FDB. Add it to BGP. */
    PTR_ASSIGN_4 (&(AddrPrefix.au1Address), NetIpRtInfo.u4NextHop);
    i4RetStatus = Bgp4RouteLeak (u4ContextId, NetAddr,
                                 NetIpRtInfo.u2RtProto, AddrPrefix,
                                 BGP4_IMPORT_RT_ADD, 0, NetIpRtInfo.i4Metric1,
                                 NetIpRtInfo.u4RtIfIndx, 0,
                                 NetIpRtInfo.u1CommunityCnt,
                                 NetIpRtInfo.pCommunity->au4Community,
                                 NetIpRtInfo.u4SetFlag);
    if (i4RetStatus == BGP4_FAILURE)
    {
        /* Adding default route to BGP fails. */
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv4RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv4 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv4RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
#ifdef IP_WANTED                /* For FS-IPv4 Stack */
    /* In case of futureIP, a new route which differs from the old route in
     * next-hop or metric or o/p interface will be treated as an alternative
     * route. So under that case, we need to delete the route explicityly.
     */
    if (((PrefixMatch (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pNewRtProfile),
                       BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pOldRtProfile)))
         != BGP4_TRUE) ||
        (BGP4_RT_MED (pNewRtProfile) != BGP4_RT_MED (pOldRtProfile)) ||
        (BGP4_RT_GET_RT_IF_INDEX (pNewRtProfile) !=
         BGP4_RT_GET_RT_IF_INDEX (pOldRtProfile)))
    {
        return BGP4_FALSE;
    }

    return BGP4_TRUE;
#elif LINUX_IPV4_WANTED            /* For Linux-IPv4 Stack */
    /* For Linux OS currently, we are not able to replace the old route with
     * the new route because RT_MODIFY function is unable to do it. So
     * return BGP4_FALSE. If RT_MODIFY function is updated, then update
     * the route accordingly. */
    UNUSED_PARAM (pNewRtProfile);
    UNUSED_PARAM (pOldRtProfile);
    return BGP4_FALSE;
#else
    /* Port it for other OS accordingly */
    UNUSED_PARAM (pNewRtProfile);
    UNUSED_PARAM (pOldRtProfile);
    return BGP4_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIp4RtTbl                              */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP Forwarding.                   */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    UINT4               u4Addr;

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tReceived route %s for adding in IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif

#ifdef RRD_WANTED
#ifdef L3VPN
    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM
     * But If route leaking is enabled, then the redistributed connected routes of a VRF
     * are also added to RTM*/
    if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_DISABLED) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))

    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {
        /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#endif

#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be added
     * back to IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    PTR_FETCH_4 (NetIpRtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    NetIpRtInfo.u4DestMask =
        Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    PTR_FETCH_4 (NetIpRtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
#ifdef L3VPN
    /* When route Leaking is done ,then redistributed non-BGP routes 
     * needs to be programmed as learnt through BGP in the respective VRF */
    if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        NetIpRtInfo.u2RtProto = BGP_ID;
    }
    else
    {
        NetIpRtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    }
#else
    NetIpRtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
#endif
    NetIpRtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    NetIpRtInfo.i4Metric1 = BGP4_RT_MED (pRtProfile);
    NetIpRtInfo.u4Tos = 0;
    NetIpRtInfo.u4RtAge = 0;    /* Will be filled while updating to IP FWD. */
    NetIpRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetIpRtInfo.u4RouteTag = 0;
    NetIpRtInfo.u1BitMask = 0;
    NetIpRtInfo.u4RowStatus = ACTIVE;
    NetIpRtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);

    if (u4VrfId != 0)
    {
        NetIpRtInfo.u4ContextId = u4VrfId;
    }

    /* If route is learnt from a internal peer, it shud not be
     * redistributed to IGPs
     */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            if (gBgpCxtNode[BGP4_PEER_CXT_ID (pTmpPeer)]->
                u1IBGPRedistributeStatus == BGP4_IBGP_REDISTRIBUTE_DISABLE)
            {
                NetIpRtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
            }
        }

        /* Construct TAG field */
        NetIpRtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));

        PTR_FETCH_4 (u4Addr,
                     BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)));
        NetIpRtInfo.u1Preference =
            BgpIp4FilterRouteSource (BGP4_PEER_CXT_ID (pTmpPeer), pRtProfile);
    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        NetIpRtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        if (pTmpPeer != NULL)
        {
            NetIpRtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
        }
    }

    if (NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s "
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s "
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIp4RtTbl                           */
/* Description   : This routine is used to delete an entry present in the    */
/*                 TRIE routing table.                                       */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tReceived route %s for deleting from IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif
#ifdef RRD_WANTED
#ifdef L3VPN
    /* Non-BGP routes are learnt from RTM. So BGP should never send Change
     * Notification Message to remove a Non-BGP route, only if Route leaking
     * is Disabled. Else should proceed deleting the routes */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_IS_LEAK_ROUTE (pRtProfile) == BGP4_FALSE))
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {
        /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#endif

#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be added
     * back to IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    PTR_FETCH_4 (NetIpRtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    NetIpRtInfo.u4DestMask =
        Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    PTR_FETCH_4 (NetIpRtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
#ifdef L3VPN
    /* When route Leaking is done ,then redistributed non-BGP routes
     * are programmed as learnt through BGP in the respective VRF table */

    if (((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) ||
         (BGP4_RT_IS_LEAK_ROUTE (pRtProfile) == BGP4_TRUE)) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        NetIpRtInfo.u2RtProto = BGP_ID;
    }
    else
    {
        NetIpRtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    }
#else
    NetIpRtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
#endif
    NetIpRtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    NetIpRtInfo.i4Metric1 = BGP4_RT_MED (pRtProfile);
    NetIpRtInfo.u4Tos = 0;
    NetIpRtInfo.u4RtAge = 0;    /* Will be filled while updating to IP FWD. */
    NetIpRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetIpRtInfo.u4RouteTag = 0;
    NetIpRtInfo.u1BitMask = 0;
    NetIpRtInfo.u4RowStatus = IPFWD_DESTROY;
    NetIpRtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    if (u4VrfId != 0)
    {
        NetIpRtInfo.u4ContextId = u4VrfId;
    }
    /* If route is learnt from a internal peer, it shud not be
     * redistributed to IGPs
     */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            if (gBgpCxtNode[BGP4_PEER_CXT_ID (pTmpPeer)]->
                u1IBGPRedistributeStatus == BGP4_IBGP_REDISTRIBUTE_DISABLE)
            {
                NetIpRtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
            }
        }

        /* Construct TAG field */
        NetIpRtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
        NetIpRtInfo.u1Preference =
            BgpIp4FilterRouteSource (BGP4_PEER_CXT_ID (pTmpPeer), pRtProfile);

    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        NetIpRtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        NetIpRtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
    }
    bgp4RibUnlock ();
    BgpUnLock ();
    if (NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetIpRtInfo) ==
        NETIPV4_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDELETING Dest = %s Mask = %s Nexthop = %s "
                       "From IP FWD Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BgpLock ();
        bgp4RibLock ();
        return BGP4_FAILURE;
    }
    BgpLock ();
    bgp4RibLock ();
    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDELETED Dest = %s Mask = %s Nexthop = %s "
                   "From IP FWD Table Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp4RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tReceived route %s for modifying the route in IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif
#ifdef RRD_WANTED
    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be added
     * back to IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    PTR_FETCH_4 (NetIpRtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    NetIpRtInfo.u4DestMask =
        Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    PTR_FETCH_4 (NetIpRtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    NetIpRtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    NetIpRtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    NetIpRtInfo.i4Metric1 = BGP4_RT_MED (pRtProfile);
    NetIpRtInfo.u4Tos = 0;
    NetIpRtInfo.u4RtAge = 0;    /* Will be filled while updating to IP FWD. */
    NetIpRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetIpRtInfo.u4RouteTag = 0;
    NetIpRtInfo.u1BitMask = 0;
    NetIpRtInfo.u4RowStatus = ACTIVE;
    NetIpRtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);

    /* If route is learnt from a internal peer, it shud not be
     * redistributed to IGPs
     */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            NetIpRtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
        }

        /* Construct TAG field */
        NetIpRtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
        NetIpRtInfo.u1Preference =
            BgpIp4FilterRouteSource (BGP4_PEER_CXT_ID (pTmpPeer), pRtProfile);
    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        NetIpRtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        NetIpRtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
    }

    if (NetIpv4LeakRoute (NETIPV4_MODIFY_ROUTE, &NetIpRtInfo) ==
        NETIPV4_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s "
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s "
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (u4Ipaddr1, u4Ipaddr2)                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv4IsOnSameSubnet (UINT4 u4ContextId, UINT4 u4NextHopAddr,
                        UINT4 u4PeerAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT1               u1SameSubnet = FALSE;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return FALSE;
    }

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfMask = NetIpIfInfo.u4NetMask;

        /* Check whether the received address is directly
         * connected to router. */
        if ((u4NextHopAddr & u4IfMask) == (u4PeerAddr & u4IfMask))
        {
            u1SameSubnet = TRUE;
            break;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            break;
        }

    }

    return u1SameSubnet;
}

/******************************************************************************/
/* Function Name : BgpGetIpIfAddr                                             */
/* Description   : This routine Gets the local-address of given interface     */
/* Input(s)      : Interface index no.                                        */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface Address or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfAddr (UINT2 u2IfIndex)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u2IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    return (NetIpIfInfo.u4Addr);
}

/******************************************************************************/
/* Function Name : BgpGetIpIfMask                                             */
/* Description   : This routine Gets the netmask of the given interface       */
/* Input(s)      : Interface index                                            */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface netmask or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfMask (UINT2 u2IfIndex)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u2IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    return (NetIpIfInfo.u4NetMask);
}

/*****************************************************************************/
/* Function Name : Bgp4GetDefaultBgpIdentifier                               */
/* Description   : FutureBgp4 uses the highest interface address as the      */
/*               : default Bgp Identifier. This function checks the          */
/*               : interface table and gets the highest interface address.   */
/*               : If no interface is configured, then "0" is returned.      */
/*               : as the Bgp Identifier                                     */
/* Input(s)      : None.                                                     */
/* Output(s)     : Default BGP Identifier (pu4BgpIdentifier)                 */
/* Return(s)     : BGP4_SUCCESS - if a valid interface address is availble   */
/*               : BGP4_FAILURE - if no interface address is available. In   */
/*                                this case the pu4BgpIdentifier value is 0  */
/*****************************************************************************/
INT1
Bgp4GetDefaultBgpIdentifier (UINT4 u4Context, UINT4 *pu4BgpIdentifier)
{

#if (CUST_BGP_ROUTER_ID_SELECT == OSIX_TRUE)
    if (UtilSelectRouterId (u4Context, pu4BgpIdentifier) == OSIX_FAILURE)
#else
    if (NetIpv4GetHighestIpAddrInCxt (u4Context,
                                      pu4BgpIdentifier) == NETIPV4_FAILURE)
#endif
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv4IsDirectlyConnected (UINT4 u4IpAddress, UINT4 u4VrfId)
{
    UINT1               u1IsDirect = BGP4_FALSE;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfAddr = BGP4_INV_IPADDRESS;
    UINT4               u4ContextId = u4VrfId;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;

#ifndef RRD_WANTED
    UINT4               u4NextHop = 0;
    UINT4               u4RtIfIndx = 0;
    INT4                i4Metric = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    i4RetVal = BgpIp4RtLookup (u4VrfId, u4IpAddress,
                               BGP4_MAX_PREFIXLEN,
                               &u4NextHop, &i4Metric,
                               &u4RtIfIndx, BGP4_LOCAL_ID);
    if (i4RetVal == BGP4_FAILURE)
    {
        u1IsDirect = BGP4_FALSE;
    }

    if (u4NextHop == 0)
    {
        u1IsDirect = BGP4_TRUE;
    }
#else
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return BGP4_FALSE;
    }

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        if (u4ContextId == NetIpIfInfo.u4ContextId)
        {
            u4IfAddr = NetIpIfInfo.u4Addr;
            u4IfMask = NetIpIfInfo.u4NetMask;

            /* Check whether the received address is directly
             * connected to router. */
            if (u4IfAddr != 0)
            {
                if ((u4IpAddress & u4IfMask) == (u4IfAddr & u4IfMask))
                {
                    u1IsDirect = TRUE;
                    break;
                }
            }
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            break;
        }
    }
#endif
    return u1IsDirect;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetNetAddrPrefixLen                               */
/* Description   : This function get the route through which the given       */
/*               : address is reachable and return the prefix length of that */
/*               : route.                                                    */
/* Input(s)      : Ip address that needs to be checked (u4RemAddr)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv4GetNetAddrPrefixLen (UINT4 u4RemAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfAddr = BGP4_INV_IPADDRESS;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT4               u4ContextId = 0;
    UINT2               u2PrefixLen = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfAddr = NetIpIfInfo.u4Addr;
        u4IfMask = NetIpIfInfo.u4NetMask;

        /* Check whether the received address is directly
         * connected to router. */
        if ((u4RemAddr & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            break;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            /* No matching interface found. */
            return 0;
        }
    }

    u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4IfMask);
    return u2PrefixLen;
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv4LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv4LocalAddrForRemoteAddr (UINT4 u4Context, tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4Rt = BGP4_INVALID_ROUTE;
    UINT4               u4RtIfIndx = BGP4_INVALID_IFINDX;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4RemAddr;
    UINT4               u4LocalAddr = 0;
    UINT4               u4LocalMask = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    PTR_FETCH_4 (u4RemAddr, BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (RemAddr));

    BgpIp4RtLookup (u4Context, u4RemAddr, BGP4_MAX_PREFIXLEN,
                    &u4Rt, &i4Metric, &u4RtIfIndx, BGP4_ALL_APPIDS);
    if ((u4Rt != BGP4_INVALID_ROUTE) && (u4RtIfIndx != BGP4_INVALID_IFINDX))
    {
        if (NetIpv4GetIfInfo (u4RtIfIndx, &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            return BGP4_FAILURE;
        }
        u4LocalAddr = NetIpIfInfo.u4Addr;
        u4LocalMask = NetIpIfInfo.u4NetMask;
    }
    if (u4LocalAddr == 0)
    {
        return BGP4_FAILURE;
    }
    PTR_ASSIGN_4 (pLocalAddr->NetAddr.au1Address, u4LocalAddr);
    pLocalAddr->u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4LocalMask);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4CbIfChgHdlr                                           */
/* Description   : Callback function to get the information about interfaces */
/* Input(s)      : u4IfIndex - Interface Index                               */
/*                 u4BitMap -  Circuit Attribute Identifier BitMap           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
PUBLIC VOID
Bgp4CbIfChgHdlr (tNetIpv4IfInfo * pNetIpv4GetInfo, UINT4 u4BitMap)
{
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4OperStatus;

    /* OPER_STATE is defined in future/inc/ip.h
     * IFACE_DELETED is defined in future/ip/ipif/inc/ipifutls.h
     * since IFACE_DELETED is not within the scope to use, define
     * local constant which is having the same value as is defined by IP
     */
    if (u4BitMap == IP_ADDR_BIT_MASK)
    {
        if (pNetIpv4GetInfo->u4Addr == 0)
        {
            pNetIpv4GetInfo->u4Oper = BGP4_IPIF_OPER_DISABLE;
        }

        else
        {
            pNetIpv4GetInfo->u4Oper = BGP4_IPIF_OPER_ENABLE;
        }
    }

    switch (u4BitMap)
    {
        case OPER_STATE:
        case IP_ADDR_BIT_MASK:
            u4OperStatus = pNetIpv4GetInfo->u4Oper;
            break;
        case BGP4_IFACE_DELETE:
            u4OperStatus = BGP4_IFACE_DELETE;
            break;
        default:
            return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Interface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_IFACE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_IFACE_INDEX (pQMsg) = pNetIpv4GetInfo->u4IfIndex;
    BGP4_INPUTQ_IFACE_STATUS (pQMsg) = u4OperStatus;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4CbVrfChgHdlr                                          */
/* Description   : Callback function to get the VRF notifications            */
/* Input(s)      : u4VrfId   - VRF identifier                                */
/*                 u1VrfState - VRF current state (up/down/create/delete)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
PUBLIC VOID
Bgp4CbVrfChgHdlr (UINT1 *pu1VrfName, UINT1 u1VrfNameLen, UINT1 u1VrfState)
{
    tVrfNode           *pVrfNode = NULL;
    UINT1               u1MatchFound = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_VRF_STS_CHNG_LIST, pVrfNode, tVrfNode *)
    {
        if (((pVrfNode->VrfName).i4Length == u1VrfNameLen) &&
            (MEMCMP ((pVrfNode->VrfName).au1VrfName, pu1VrfName,
                     u1VrfNameLen) == 0))
        {
            u1MatchFound = BGP4_TRUE;
            break;
        }
    }
    if (u1MatchFound == BGP4_TRUE)
    {
        pVrfNode->u1Oper = BGP4_VRF_CHANGE_EVENT;
        pVrfNode->u1VrfChngSts = u1VrfState;
        return;
    }
    pVrfNode = (tVrfNode *) MemAllocMemBlk (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID);

    if (pVrfNode == NULL)
    {
        return;
    }

    MEMCPY ((pVrfNode->VrfName).au1VrfName, pu1VrfName, u1VrfNameLen);
    (pVrfNode->VrfName).i4Length = (INT4) u1VrfNameLen;
    pVrfNode->u1VrfChngSts = u1VrfState;
    pVrfNode->u1Oper = BGP4_VRF_CHANGE_EVENT;
    TMO_SLL_Add (BGP4_VRF_STS_CHNG_LIST, (tTMO_SLL_NODE *) pVrfNode);
    return;
}

#endif /* L3VPN */

/********************************************************************/
/* Function Name   : Bgp4GetIfInfoFromIp                            */
/* Description     : Gets the interface related information from IP */
/* Input(s)        : u4IfIndex - interface index                    */
/*                   pIfEntry - interface entry                     */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4GetIfInfoFromIp (UINT4 u4IfIndex, tBgp4IfInfo * pIfEntry)
{
#ifdef IP_WANTED
    tNetIpv4IfInfo      NetIpv4Info;
    INT4                i4RetVal;

    /* call the netipv4 routine to get the interface related information */
    i4RetVal = NetIpv4GetIfInfo (u4IfIndex, &NetIpv4Info);
    if (i4RetVal != NETIPV4_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    /* Fill out the address and operational status */
    BGP4_IFACE_ADDR (pIfEntry) = NetIpv4Info.u4Addr;
    BGP4_IFACE_ENTRY_INDEX (pIfEntry) = u4IfIndex;
    BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) = (UINT1) NetIpv4Info.u4Oper;
    BGP4_IFACE_ENTRY_IFTYPE (pIfEntry) = NetIpv4Info.u4IfType;
    BGP4_IFACE_ENTRY_VRFID (pIfEntry) = NetIpv4Info.u4ContextId;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIfEntry);
#endif /* IP_WANTED */
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4IsLocalAddr                                            */
/* Description   : This routine checks whether the given address belongs to   */
/*                 any of the interface address or not.                       */
/* Input(s)      : u4Addr - Address to be verified.                           */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_TRUE - if given address matches interface address     */
/*                 BGP4_FALSE - if does not match                             */
/******************************************************************************/
UINT4
Bgp4IsLocalAddr (UINT4 u4Addr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfAddr = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT4               u4ContextId = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return BGP4_FALSE;
    }

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfAddr = NetIpIfInfo.u4Addr;

        /* Check whether the received address is directly
         * connected to router. */
        if (u4IfAddr == u4Addr)
        {
            /* Given address matches the interface address. */
            return BGP4_TRUE;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId, u4IfIndex, &NetIpIfInfo) ==
            NETIPV4_FAILURE)
        {
            /* No matching interface found. */
            break;
        }
    }

    return BGP4_FALSE;
}

/******************************************************************************/
/* Function Name : Bgp4GetIfPort                                              */
/* Description   : This routine checks retrieves the interface index          */
/*                 corresponding to the ip address                            */
/*                 any of the interface address or not.                       */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
INT4
Bgp4GetIfPort (UINT4 u4IpAddr, UINT4 *pu4IfIndex)
{

    UINT4               u4ContextId = 0;
    if (NetIpv4GetIfIndexFromAddrInCxt (u4ContextId,
                                        u4IpAddr,
                                        pu4IfIndex) == NETIPV4_SUCCESS)
    {
        return BGP4_TRUE;
    }
    return BGP4_FALSE;
}

/******************************************************************************/
/* Function Name : Bgp4CbRouteChangeHandler                                   */
/* Description   : This routine gets route change notification                */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
VOID
Bgp4CbRouteChangeHandler (tNetIpv4RtInfo * pRtChg, tNetIpv4RtInfo * pRtChg1,
                          UINT1 u1BitMap)
{
    if (BGP4_GLB_RESTART_STATUS == BGP4_GR_STATUS_NONE)
    {
        if (u1BitMap != NETIPV4_MODIFY_ROUTE)
        {
            Bgp4CbRtChangeHdlr (pRtChg, u1BitMap);
            return;
        }
        Bgp4CbRtChangeHdlr (pRtChg, NETIPV4_DELETE_ROUTE);
        Bgp4CbRtChangeHdlr (pRtChg1, NETIPV4_ADD_ROUTE);
    }

}

/******************************************************************************/
/* Function Name : Bgp4CbRtChangeHdlr                                         */
/* Description   : This routine gets route change notification                */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
VOID
Bgp4CbRtChangeHdlr (tNetIpv4RtInfo * pRtChg, UINT1 u1BitMap)
{
    tNetworkAddr       *pNetworkRt = NULL;
    tNetworkAddr        NetworkAddr;
    tNetworkAddr        NetworkRoute;
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4Action;
    UINT4               u4PrefixLen;
    UINT4               u4DestAddr;
    UINT4               u4NextHopAddr;
    UINT4               u4Context = 0;

    MEMSET (&NetworkRoute, 0, sizeof (tNetworkAddr));
    MEMSET (&NetworkAddr, 0, sizeof (tNetworkAddr));

    if (pRtChg->u2RtProto == BGP_ID)
    {
        return;
    }
    switch (u1BitMap)
    {
        case NETIPV4_ADD_ROUTE:
            u4Action = BGP4_IMPORT_RT_ADD;
            break;
        case NETIPV4_DELETE_ROUTE:
            u4Action = BGP4_IMPORT_RT_DELETE;
            break;
        default:
            return;
    }

    IPV4_MASK_TO_MASKLEN (u4PrefixLen, pRtChg->u4DestMask);
    u4DestAddr = OSIX_NTOHL (pRtChg->u4DestNet);
    u4NextHopAddr = (pRtChg->u4NextHop);
    u4Context = pRtChg->u4ContextId;
    PTR_ASSIGN_4 (NetworkAddr.NetworkAddr.au1Address, u4DestAddr);
    NetworkAddr.NetworkAddr.u2AddressLen = BGP4_IPV4_PREFIX_LEN;
    NetworkAddr.NetworkAddr.u2Afi = BGP4_INET_AFI_IPV4;
    pNetworkRt =
        Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr, &NetworkRoute);

    if (pNetworkRt == NULL)
    {
        /*Network entry is not matched with the incoming route indication from RTM */
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Inteface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    Bgp4InitAddrPrefixStruct (&(BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg)),
                              BGP4_INET_AFI_IPV4);
    Bgp4InitAddrPrefixStruct (&(BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg)),
                              BGP4_INET_AFI_IPV4);

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO (pQMsg) = u4PrefixLen;
    BGP4_INPUTQ_ROUTE_ACTION (pQMsg) = u4Action;
    BGP4_INPUTQ_ROUTE_IF_INDEX (pQMsg) = pRtChg->u4RtIfIndx;
    BGP4_INPUTQ_ROUTE_METRIC (pQMsg) = pRtChg->i4Metric1;
    BGP4_INPUTQ_ROUTE_PROTO_ID (pQMsg) = pRtChg->u2RtProto;
/*    BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg), IPVX_ADDR_FMLY_IPV4,
                           (UINT1 *) &(u4DestAddr));
    IPVX_ADDR_INIT_IPV4 (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg), IPVX_ADDR_FMLY_IPV4,
                           (UINT1 *) &(u4NextHopAddr)); */
    PTR_ASSIGN_4 (pQMsg->QMsg.Bgp4RtInfoMsg.DestAddr.au1Address, u4DestAddr);
    PTR_ASSIGN_4 (pQMsg->QMsg.Bgp4RtInfoMsg.NextHop.au1Address, u4NextHopAddr);

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;

}

#endif /* BGFSIPF4_C */
