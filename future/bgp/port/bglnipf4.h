/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bglnipf4.h,v 1.17 2017/12/26 13:05:16 siva Exp $
 *
 * Description:This file includes all the BGP based header files.          
 *
 *******************************************************************/
#ifndef BGLNIPF4_H
#define BGLNIPF4_H

#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <syslog.h>

#include "bgp4dfs.h"
#include "bgp4tdfs.h"
#ifdef VPLSADS_WANTED
#include "bgp4l2vpn.h"
#endif

#ifdef EVPN_WANTED
#include "bgevpndfs.h"
#endif

#include "bgp4rib.h"
#include "bgp4port.h"


#define LINUX_IP_NETROUTE  1
#define LINUX_IP_HOSTROUTE 2

#define RTACTION_ADD   1
#define RTACTION_DEL   2
#define RTACTION_HELP  3
#define RTACTION_FLUSH 4
#define RTACTION_SHOW  5

#define E_NOTFOUND    8
#define E_SOCK        7
#define E_LOOKUP    6
#define E_VERSION    5
#define E_USAGE        4
#define E_OPTERR    3
#define E_INTERN    2
#define E_NOSUPP    1

#define FLAG_EXT      3        /* AND-Mask */
#define FLAG_NUM      4
#define FLAG_SYM      8
#define FLAG_CACHE   16
#define FLAG_FIB     32
#define FLAG_VERBOSE 64
#define BGP4_DONE     1

/* BGP-IP related macros */
#define  BGP_GET_IP_IFADDR(u2port)            IpifGetIfAddr(u2port)

/* Data Structure and definitions for quering the interface information */
#define IFI_ALIAS   1           /* ifi_addr is an alias */
#define IFI_NAME    16          /* same as IFNAMSIZ in <net/if.h> */
#define IFI_HADDR   8

struct ifi_info {
  struct sockaddr  *ifi_addr;       /* primary address */
  struct sockaddr  *ifi_netmask;    /* netmask address */
  struct sockaddr  *ifi_brdaddr;    /* broadcast address */
  struct sockaddr  *ifi_dstaddr;    /* destination address */
  struct ifi_info  *ifi_next;       /* next of these structures */
  UINT2  ifi_hlen;                  /* #bytes in hardware address: 0, 6, 8 */
  UINT2  ifi_index;                 /* interface index value */
  INT2   ifi_flags;                 /* IFF_xxx constants from <net/if.h> */
  INT2   ifi_myflags;               /* our own IFI_xxx flags */
  UINT4  u4PrefixLen;  
  UINT1  ifi_haddr[IFI_HADDR];      /* hardware address */
  INT1   ifi_name[IFI_NAME];        /* interface name, null terminated */
};

struct ifi_infmtn {
    struct ifi_info *ifi;
    struct ifi_info *head;
    struct ifi_info **tail;
};

/* Declaration for other protocols supported by BGP */
#define  BGP4_OTHERS_ID     OTHERS_ID
#define  BGP4_LOCAL_ID      CIDR_LOCAL_ID           
#define  BGP4_STATIC_ID     CIDR_STATIC_ID
#define  BGP4_RIP_ID        0x08
#define  BGP4_OSPF_ID       OSPF_ID
#define  BGP4_ISIS_ID       ISIS_ID

#define NETIPV4_ADD_ROUTE          1
#define NETIPV4_DELETE_ROUTE       2
#define NETIPV4_MODIFY_ROUTE       3

#define NETIPV4_EXACT_ROUTE        1
#define NETIPV4_BEST_ROUTE         2
#define NETIPV4_NO_ROUTE           3

#define NETIPV4_SUCCESS            0
#define NETIPV4_FAILURE           -1

#define MAX_IFNAME_SIZE            36

/* defines for compilation - END */

#define   UNUSED_VAR(x)   ((void)x)

/* PROTOTYPES */
UINT4 BgpGetIpIfAddr (UINT2 );
UINT4 BgpGetIpIfMask (UINT2);
INT4  BGP4CanRepIpv4RtAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4  BGP4RTAddRouteToCommIp4RtTbl (tRouteProfile *, UINT4 );
INT4  BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile *);
INT4  BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile *, UINT4);
INT1  Bgp4GetDefaultBgpIdentifier (UINT4, UINT4 *);
UINT1 Bgp4Ipv4IsDirectlyConnected (UINT4 , UINT4);
UINT2 Bgp4Ipv4GetNetAddrPrefixLen (UINT4);
INT4  BgpIp4RtLookup (UINT4, UINT4 , UINT2, UINT4 *, INT4 *, UINT4 *, INT1 );
INT4  Bgp4Ipv4GetDefaultRouteFromFDB (UINT4);
BOOL1 Bgp4Ipv4IsOnSameSubnet (UINT4, UINT4 , UINT4 );
INT4  Bgp4GetIpv4LocalAddrForRemoteAddr(UINT4, tAddrPrefix , tNetAddress *);
INT4  BgpIp4GetNextRtEntry (UINT4 , UINT4 ,UINT2 , UINT4 , UINT4 *,INT4 *, UINT4 *);

PUBLIC VOID Bgp4CbIfChgHdlr PROTO ((tNetIpv4IfInfo *, UINT4 ));
VOID Bgp4CbRouteChangeHandler PROTO ((tNetIpv4RtInfo *, tNetIpv4RtInfo *, UINT1 ));
VOID Bgp4CbRtChangeHdlr PROTO ((tNetIpv4RtInfo *, UINT1 ));
/* ENVIRONMENT SPECIFIC CALLS */

int    ipSetRoute(int , int , unsigned int , unsigned int , unsigned int ,
                  unsigned int );
int    ipRouteLookup(unsigned int , int *, unsigned int *, unsigned int * );
int    ipRouteLookupExact(unsigned int, unsigned int , int *, unsigned int *,
                       unsigned int *);
int    IsIp4LocalAddress(unsigned int ,unsigned short *);
char  *ProcGenFmt(const char *, int , FILE * ,...);
int    mapIfName2Index(char *);
INT4   ipMask2PrefixLen (UINT4 u4Mask);
UINT4  ipPrefixLen2Mask (UINT4 u4PrefixLen);
UINT4  Ip4MapPortToAddress(unsigned short );
UINT4  Ip4MapPortToMask (unsigned short);

#endif /* BGLNIPF4_H */
