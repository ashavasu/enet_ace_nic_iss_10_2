/** $Id: bgpfstcp.h,v 1.9 2013/09/21 11:46:57 siva Exp $ */
#ifndef BGPFSTCP_H
#define BGPFSTCP_H

#include "fssocket.h"

/* Prototype definitions */
INT1   Bgp4TcphGetSockOpt (INT4 );
INT4   Bgp4TcphSetSocketOpt (INT4, UINT4, UINT4);
INT4   Bgp4TcphMD5AuthOptSet (tBgp4PeerEntry *, UINT1 *, UINT1);
INT4   Bgp4TcphCloseConnection (tBgp4PeerEntry *);
INT4   Bgp4TcphCloseListenPort (UINT4, UINT2);
INT4   Bgp4TcphAuthOptionMktSet(tBgp4PeerEntry *, tTcpAoMktAddr *);
INT4   Bgp4TcpAoGetMktInUse(UINT4 , tAddrPrefix , INT4 * );
INT4   Bgp4TcpAoNeighCfgSet (tBgp4PeerEntry *, tTcpAoNeighCfg  *, 
                             INT4);
INT4   Bgp4TcpAoMktSet(tBgp4PeerEntry *, tTcpAoAuthMKT *);
INT4   Bgp4TcpAoIcmpCfgSet (tBgp4PeerEntry *, INT1);
INT4   Bgp4TcpAoPktDiscCfg (tBgp4PeerEntry *, INT1);
INT4   Bgp4TcpAoGetAuthStatus (tBgp4PeerEntry *, INT4 *);

#ifdef BGP_TCP6_WANTED

/* Macros for SLI_WANTED */
#define  SOCKET(a,b,c)          socket(a,b,c)
#define  CONNECT(a,b,c)         connect(a,(struct sockaddr *)b,c)
#define  BIND(a,b,c)            bind(a, (struct sockaddr *)b, c)
#define  LISTEN(a,b)            listen(a,b)
#define  CLOSE(a)               close(a)
#define  BGP4_SET_SOCK_NON_BLK(pSock,pOptVal)\
     setsockopt(pSock,SOL_SOCKET,BGP_NODELAY, pOptVal, sizeof(INT1))
#define  SEND(a,b,c,d)          send(a, (VOID *)b, c,MSG_PUSH) 
#define  RECV(a,b,c,d)          recv(a, (INT1 *)b,c,d)
#define  ACCEPT(a,b,c)          accept((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETSOCKNAME(a,b,c)     getsockname((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETPEERNAME(a,b,c)     getpeername((INT4)a,(struct sockaddr *)b, (INT4 *)c)
#define  SETSOCKOPT( a,b,c,d,e) setsockopt(a,b,c, (VOID *)d, e)
#define  GETSOCKOPT( a,b,c,d,e) getsockopt(a,b,c, (void *)d, e)
#define  BGP4_FCNTL             fcntl
#define  BGP_SOCK_SELECT        select
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  BGP_NODELAY            TCP_NODELAY 
#define  BGP_EINPROGRESS        SLI_EINPROGRESS
#define  BGP_EAGAIN             SLI_EAGAIN
#define  BGP_EISCONN            SLI_EISCONN
#define  BGP_ENETUNREACH        SLI_ENETUNREACH
#define  BGP_ECONNREFUSED       SLI_ECONNREFUSED
#define  BGP_EALREADY           SLI_EALREADY
#define  BGP_EWOULDBLOCK        SLI_EWOULDBLOCK
#define  BGP_EINTR              SLI_EINTR
#define  BGP_ENOTCONN           SLI_ENOTCONN

#ifndef RRD_WANTED
#define  MAX_ROUTING_PROTOCOLS  16    
#define  ROUTING_PROTOCOLS_MIN_INDEX 1
#endif

INT4   Bgp4Tcphv6OpenConnection (tBgp4PeerEntry *);
INT4   Bgp4Tcphv6CheckIncomingConn (UINT4);
INT4   Bgp4Tcphv6FillAddresses (tBgp4PeerEntry *);
INT4   Bgp4Tcphv6OpenListenPort (UINT4);

#endif /* BGP_TCP6_WANTED */

#ifdef BGP_TCP4_WANTED

/* Macros for SLI_WANTED */
#define  SOCKET(a,b,c)          socket(a,b,c)
#define  CONNECT(a,b,c)         connect(a,(struct sockaddr *)b,c)
#define  BIND(a,b,c)            bind(a, (struct sockaddr *)b, c)
#define  LISTEN(a,b)            listen(a,b)
#define  CLOSE(a)               close(a)
#define  BGP4_SET_SOCK_NON_BLK(pSock,pOptVal)\
     setsockopt(pSock,SOL_SOCKET,BGP_NODELAY, pOptVal, sizeof(INT1))
#define  SEND(a,b,c,d)          send(a, (VOID *)b, c,MSG_PUSH) 
#define  RECV(a,b,c,d)          recv(a, (INT1 *)b,c,d)
#define  ACCEPT(a,b,c)          accept((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETSOCKNAME(a,b,c)     getsockname((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETPEERNAME(a,b,c)     getpeername((INT4)a,(struct sockaddr *)b, (INT4 *)c)
#define  SETSOCKOPT( a,b,c,d,e) setsockopt(a,b,c, (VOID *)d, e)
#define  GETSOCKOPT( a,b,c,d,e) getsockopt(a,b,c, (void *)d, e)
#define  BGP4_FCNTL             fcntl
#define  BGP_SOCK_SELECT        select
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  BGP_NODELAY            TCP_NODELAY 
#define  BGP_EINPROGRESS        SLI_EINPROGRESS
#define  BGP_EAGAIN             SLI_EAGAIN
#define  BGP_EISCONN            SLI_EISCONN
#define  BGP_ENETUNREACH        SLI_ENETUNREACH
#define  BGP_ECONNREFUSED       SLI_ECONNREFUSED
#define  BGP_EALREADY           SLI_EALREADY
#define  BGP_EWOULDBLOCK        SLI_EWOULDBLOCK
#define  BGP_EINTR              SLI_EINTR
#define  BGP_ENOTCONN           SLI_ENOTCONN

/* Temporary - TCP MD5 Auth Option */
#define  TCP_SIGNATURE_ENABLE     1
 
#ifndef RRD_WANTED
#define  MAX_ROUTING_PROTOCOLS  16    
#define  ROUTING_PROTOCOLS_MIN_INDEX 1
#endif

#ifdef BGP_TEST_WANTED
#define BGP_MIN_TCP_PRIVATE_PORT 49152
#endif

/* Prototype definitions */
INT4  Bgp4Tcphv4OpenConnection (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4CheckIncomingConn (UINT4);
INT4  Bgp4Tcphv4FillAddresses (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4OpenListenPort (UINT4);


#endif /* BGP_TCP4_WANTED */

#endif
