/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4port.c,v 1.20 2017/09/15 06:19:51 siva Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGP4PORT_C
#define BGP4PORT_C
#include "bgp4com.h"

/*****************************************************************************/
/* Function Name :  BgpIpRtLookup                                            */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP Fwd Table                                      */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pu4Rt)             */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIpRtLookup (UINT4 u4VrfId, tAddrPrefix * pIpaddr, UINT2 u2PrefixLen,
               tAddrPrefix * pRt, INT4 *pi4Metric,
               UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
    INT4                i4RetVal = BGP4_FAILURE;
    UINT4               u4IpAddr;
    UINT4               u4Rt;
    UINT4               u4RtIfIndex;
    INT4                i4Metric;

    Bgp4InitAddrPrefixStruct (pRt, pIpaddr->u2Afi);
    switch (pIpaddr->u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
        {
            pIpaddr->u2Afi = BGP4_INET_AFI_IPV4;
            pIpaddr->u2AddressLen = BGP4_IPV4_PREFIX_LEN;
            PTR_FETCH_4 (u4IpAddr, pIpaddr->au1Address);
            i4RetVal = BgpIp4RtLookup (u4VrfId, u4IpAddr, u2PrefixLen,
                                       &u4Rt, &i4Metric, &u4RtIfIndex, i1AppId);
            if (i4RetVal == BGP4_SUCCESS)
            {
                /* Assign the searched address */
                PTR_ASSIGN_4 (pRt->au1Address, u4Rt);
                *pi4Metric = i4Metric;
                *pu4RtIfIndx = u4RtIfIndex;
                return BGP4_SUCCESS;
            }
            return BGP4_FAILURE;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            BGP4_IN6_IS_ADDR_V4COMPATIBLE (pIpaddr->au1Address, i4RetVal);
            if (i4RetVal == BGP4_TRUE)
            {
                /* IPv4-IPv6 compatible address. Validate the IPv4 prefix */
                return BGP4_FAILURE;
            }
            else
            {
                pIpaddr->u2Afi = BGP4_INET_AFI_IPV6;
                pIpaddr->u2AddressLen = BGP4_IPV6_PREFIX_LEN;
                pRt->u2Afi = BGP4_INET_AFI_IPV6;
                pRt->u2AddressLen = BGP4_IPV6_PREFIX_LEN;
                i4RetVal = BgpIp6RtLookup (u4VrfId, pIpaddr, u2PrefixLen,
                                           pRt, pi4Metric,
                                           pu4RtIfIndx, i1AppId);
            }
            return i4RetVal;
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4GetDefaultRouteFromFDB                                */
/* Description   : This routine will get the default route if present in FDB */
/*                 and if present then add the default route to BGP RIB.     */
/* Input(s)      : u2Afi - Address Family of the Default route.              */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetDefaultRouteFromFDB (UINT4 u4ContextId, UINT2 u2Afi)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
            i4RetVal = Bgp4Ipv4GetDefaultRouteFromFDB (u4ContextId);
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4RetVal = Bgp4Ipv6GetDefaultRouteFromFDB (u4ContextId);
            break;
#endif
        default:
            return BGP4_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepRouteBeAddedToFIB                               */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IP FIB, without deleting the     */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepRouteBeAddedToFIB (tRouteProfile * pNewRtProfile,
                             tRouteProfile * pOldRtProfile)
{
    INT4                i4RetVal = BGP4_FALSE;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewRtProfile),
                           BGP4_RT_SAFI_INFO (pNewRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4RetVal = BGP4CanRepIpv4RtAddedToFIB (pNewRtProfile,
                                                   pOldRtProfile);
            return i4RetVal;

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            i4RetVal = BGP4CanRepIpv6RtAddedToFIB (pNewRtProfile,
                                                   pOldRtProfile);
            return i4RetVal;
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIpRtTbl                               */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP Forwarding.                   */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIpRtTbl (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (BGP4_RT_AFI_INFO (pRtProfile))
    {
        case BGP4_INET_AFI_IPV4:
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTAddRouteToCommIp4RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTAddRouteToCommIp4RtTbl (pRtProfile, 0);
            return i4RetVal;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTAddRouteToCommIp6RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTAddRouteToCommIp6RtTbl (pRtProfile);
            return i4RetVal;
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIpRtTbl                            */
/* Description   : This routine is used to delete an entry present in the    */
/*                 TRIE routing table.                                       */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIpRtTbl (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (BGP4_RT_AFI_INFO (pRtProfile))
    {
        case BGP4_INET_AFI_IPV4:
        {
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTDeleteRouteInCommIp4RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTDeleteRouteInCommIp4RtTbl (pRtProfile, 0);
            return i4RetVal;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTDeleteRouteInCommIp6RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTDeleteRouteInCommIp6RtTbl (pRtProfile);
            return i4RetVal;
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIpRtTbl                            */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIpRtTbl (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (BGP4_RT_AFI_INFO (pRtProfile))
    {
        case BGP4_INET_AFI_IPV4:
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTModifyRouteInCommIp4RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTModifyRouteInCommIp4RtTbl (pRtProfile);
            return i4RetVal;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling BGP4RTModifyRouteInCommIp6RtTbl from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            i4RetVal = BGP4RTModifyRouteInCommIp6RtTbl (pRtProfile);
            return i4RetVal;
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4IsOnSameSubnet                                        */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (u4Ipaddr1, u4Ipaddr2)                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4IsOnSameSubnet (tAddrPrefix NexthopAddr, tBgp4PeerEntry * pPeerEntry)
{
    tNetAddress        *pLocalAddr = NULL;
    tAddrPrefix        *pPeerAddr = NULL;
    UINT2               u2Afi;
    UINT1               u1SameSubnet = FALSE;

    pPeerAddr = &(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
    pLocalAddr = &(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerEntry));
    u2Afi = pPeerAddr->u2Afi;

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NexthopAddr) != u2Afi)
    {
        pPeerAddr = &(BGP4_PEER_NETWORK_ADDR_INFO (pPeerEntry));
        pLocalAddr = &(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry));
        u2Afi = pPeerAddr->u2Afi;
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (NexthopAddr))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4PeerIpaddr = 0;
            UINT4               u4NexthopIpaddr;
#ifdef BGP4_IPV6_WANTED
            UINT4               u4PeerIpmask = 0;
            UINT2               u2Port;
            INT4                i4Ret;
            tAddrPrefix         InvPrefix;
#endif
            PTR_FETCH_4 (u4NexthopIpaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NexthopAddr));
            if (u4NexthopIpaddr == 0)
            {
                return (u1SameSubnet);
            }

            if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NexthopAddr) != u2Afi)
            {
#ifdef BGP4_IPV6_WANTED
                /* Both the NextHop AFI and peer Address AFI differs. This
                 * could occur if the peer network address is not
                 * configured. Now try and get the corresponding IPv4 
                 * interface address of the peer. */
                Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_INET_AFI_IPV4);
                if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                             BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                             (BGP4_PEER_LCLADDR_INFO (pPeerEntry)),
                             BGP4_MAX_INET_ADDRESS_LEN)) == 0)
                {
                    /* Local Interface Address not present. Update it. */
                    pPeerAddr = &(pPeerEntry->peerConfig.RemoteAddrInfo);
                    i4Ret = Ip6IsLocalSubnet (pPeerAddr->au1Address, &u2Port);
                    if (i4Ret == IP_FAILURE)
                    {
                        return FALSE;
                    }
                    u4PeerIpaddr = BgpGetIpIfAddr (u2Port);
                    u4PeerIpmask = BgpGetIpIfMask (u2Port);
                    Bgp4InitNetAddressStruct
                        (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)),
                         BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
                    PTR_ASSIGN_4 (&(BGP4_PEER_LCL_ADDR (pPeerEntry)),
                                  u4PeerIpaddr);
                    BGP4_PEER_LCL_NETADDR_PREFIXLEN (pPeerEntry) =
                        (UINT2) Bgp4GetSubnetmasklen (u4PeerIpmask);
                    pLocalAddr = &(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry));
                }
#else
                return FALSE;
#endif
            }

            if (pLocalAddr->u2PrefixLen == 0)
            {
                /* Unable to find the Local Address Mask for the Peer.
                 * Query the DB to get this information */
                PTR_FETCH_4 (u4PeerIpaddr, pPeerAddr->au1Address);
                u1SameSubnet =
                    Bgp4Ipv4IsOnSameSubnet (BGP4_PEER_CXT_ID (pPeerEntry),
                                            u4NexthopIpaddr, u4PeerIpaddr);
                return (u1SameSubnet);
            }
            else
            {
                return ((AddrMatch (NexthopAddr, pLocalAddr->NetAddr,
                                    pLocalAddr->u2PrefixLen)
                         == BGP4_TRUE) ? TRUE : FALSE);
            }
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tAddrPrefix         InvPrefix;
            tNetAddress         LocalAddr;
            UINT4               u4PeerIpaddr = 0;
            UINT4               u4NexthopIpaddr = 0;
            INT4                i4Status;

            if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NexthopAddr) != u2Afi)
            {
                /* Both the NextHop AFI and peer Address AFI differs. This
                 * could occur if the peer network address is not
                 * configured. Now try and get the corresponding IPv6 
                 * interface address of the peer. */
                BGP4_IN6_IS_ADDR_V4COMPATIBLE (NexthopAddr.au1Address,
                                               i4Status);
                if (i4Status == BGP4_TRUE)
                {
                    /* V4-Compatible Next hop. */
                    Bgp4InitNetAddressStruct (&(LocalAddr), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    /* Since the Local Address is stored in v4 and NexthopAddr
                     * is stored in V6 - Convert both to v4 Form and then
                     * check for Subnet match. */
                    PTR_FETCH4 (u4NexthopIpaddr,
                                &(NexthopAddr.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                         BGP4_IPV4_PREFIX_LEN]));
                    Bgp4InitAddrPrefixStruct (&NexthopAddr, BGP4_INET_AFI_IPV4);
                    PTR_ASSIGN_4 (NexthopAddr.au1Address, u4NexthopIpaddr);
                    pPeerAddr = &(pPeerEntry->peerConfig.RemoteAddrInfo);
                    i4Status =
                        Bgp4GetIpv4LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                           (pPeerEntry),
                                                           NexthopAddr,
                                                           &LocalAddr);
                    if (i4Status == BGP4_FAILURE)
                    {
                        return FALSE;
                    }
                    return TRUE;
                }
                else
                {
                    Bgp4InitNetAddressStruct (&(LocalAddr), BGP4_INET_AFI_IPV6,
                                              BGP4_INET_SAFI_UNICAST);
                    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_INET_AFI_IPV6);
                    if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                 (InvPrefix),
                                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                 (BGP4_PEER_LCLADDR_INFO (pPeerEntry)),
                                 BGP4_MAX_INET_ADDRESS_LEN)) == 0)
                    {
                        /* Local Interface Address not present. Update it. */
                        pPeerAddr = &(pPeerEntry->peerConfig.RemoteAddrInfo);
                        i4Status =
                            Bgp4GetIpv6LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                               (pPeerEntry),
                                                               NexthopAddr,
                                                               &LocalAddr);
                        if (i4Status == BGP4_FAILURE)
                        {
                            return FALSE;
                        }

                        Bgp4CopyNetAddressStruct
                            (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)),
                             LocalAddr);
                        return TRUE;
                    }
                    else
                    {
                        return ((AddrMatch (NexthopAddr, pLocalAddr->NetAddr,
                                            pLocalAddr->u2PrefixLen)
                                 == BGP4_TRUE) ? TRUE : FALSE);
                    }
                }
            }
            else
            {
                BGP4_IN6_IS_ADDR_V4COMPATIBLE (NexthopAddr.au1Address,
                                               i4Status);
                if (i4Status == BGP4_TRUE)
                {
                    /* NextHop Address is V4 Compatible */
                    if ((BGP4_AFI_IN_ADDR_PREFIX_INFO
                         (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))) ==
                        BGP4_INET_AFI_IPV4)
                    {
                        pLocalAddr =
                            &(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerEntry));
                    }
                    else
                    {
                        pLocalAddr = &(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry));
                    }
                    /* Since the Local Address is stored in v4 and NexthopAddr
                     * is stored in V6 - Convert both to v4 Form and then
                     * check for Subnet match. */
                    PTR_FETCH4 (u4NexthopIpaddr,
                                &(NexthopAddr.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                         BGP4_IPV4_PREFIX_LEN]));
                    Bgp4InitAddrPrefixStruct (&NexthopAddr, BGP4_INET_AFI_IPV4);
                    PTR_ASSIGN_4 (NexthopAddr.au1Address, u4NexthopIpaddr);
                }
                if (pLocalAddr->u2PrefixLen == 0)
                {
                    /* Unable to find the Local Address Mask for the Peer.
                     * Query the DB to get this information */
                    PTR_FETCH_4 (u4PeerIpaddr, pPeerAddr->au1Address);
                    u1SameSubnet =
                        Bgp4Ipv4IsOnSameSubnet (BGP4_PEER_CXT_ID (pPeerEntry),
                                                u4NexthopIpaddr, u4PeerIpaddr);
                    return (u1SameSubnet);
                }
                else
                {
                    return ((AddrMatch (NexthopAddr, pLocalAddr->NetAddr,
                                        pLocalAddr->u2PrefixLen)
                             == BGP4_TRUE) ? TRUE : FALSE);
                }
            }
        }
#endif
        default:
            break;
    }
    return u1SameSubnet;
}

/*****************************************************************************/
/* Function Name : Bgp4IsDirectlyConnected                                   */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4IsDirectlyConnected (tAddrPrefix IpAddress, UINT4 u4VrfId)
{
    UINT1               u1IsDirect = BGP4_FALSE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (IpAddress))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (IpAddress));
            u1IsDirect = Bgp4Ipv4IsDirectlyConnected (u4Ipaddr, u4VrfId);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            u1IsDirect = Bgp4Ipv6IsDirectlyConnected (&IpAddress, u4VrfId);
            break;
#endif
        default:
            break;
    }
    return u1IsDirect;
}

/*****************************************************************************/
/* Function Name : Bgp4GetNetAddrPrefixLen                                   */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address whose prefix lenght to be found (IpAddress)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4GetNetAddrPrefixLen (tAddrPrefix IpAddress)
{
    UINT2               u2PrefixLen = 0;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (IpAddress))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (IpAddress));
            u2PrefixLen = Bgp4Ipv4GetNetAddrPrefixLen (u4Ipaddr);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            u2PrefixLen = Bgp4Ipv6GetNetAddrPrefixLen (&IpAddress);
            break;
#endif
        default:
            break;
    }
    return u2PrefixLen;
}

/******************************************************************************/
/* Function Name : Bgp4GetLocalAddrForPeer                                    */
/* Description   : This function fills the local interface address            */
/*                 corresponding to peer remote address                       */
/* Input(s)      : RemAddr (Remote address of peer), peerentry information    */
/*               : u1IsSetLclAddr - flag indicating whether the LCL_NETADDR   */
/*                                  for the peer needs to be filled or not.   */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetLocalAddrForPeer (tBgp4PeerEntry * pPeerEntry,
                         tAddrPrefix RemAddr, tNetAddress * pLocalAddr,
                         UINT1 u1IsSetLclAddr)
{
    tNetAddress         NetAddr;
    INT4                i4Ret = BGP4_FAILURE;
    UINT4               u4Flag = BGP4_FALSE;
    UINT2               u2Afi = 0;

    u2Afi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

    if (u2Afi == BGP4_AFI_IN_ADDR_PREFIX_INFO (RemAddr))
    {
        u4Flag = BGP4_TRUE;
    }
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (RemAddr))
    {
        case BGP4_INET_AFI_IPV4:
            if ((u4Flag == BGP4_TRUE) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry)) ==
                 BGP4_INET_AFI_IPV4))
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LOCAL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Get peer local address corresponding to peer network
             * address */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_LCLADDR_INFO (pPeerEntry)) != 0)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LCL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Local Address information is not available for the Peer.
             * Lookup the IP Database and get the required Local Address
             * for this peer. Also update the LCL_NETADDR_INFO so that further
             * look ups can be prevented. */
            Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            Bgp4InitNetAddressStruct (pLocalAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            i4Ret =
                Bgp4GetIpv4LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                   (pPeerEntry), RemAddr,
                                                   &NetAddr);
            if (i4Ret == BGP4_SUCCESS)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr, NetAddr);
                if (u1IsSetLclAddr == BGP4_TRUE)
                {
                    Bgp4CopyNetAddressStruct
                        (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), NetAddr);
                }
            }
            else
            {
                Bgp4InitNetAddressStruct
                    (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), 0, 0);
            }
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:

            if ((u4Flag == BGP4_TRUE) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry)) ==
                 BGP4_INET_AFI_IPV6))
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LOCAL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Get peer local address corresponding to peer network
             * address */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_LCLADDR_INFO (pPeerEntry)) != 0)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LCL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Local Address information is not available for the Peer.
             * Lookup the IP Database and get the required Local Address
             * for this peer. Also update the LCL_ADDR_INFO so that further
             * look ups can be prevented. */
            Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV6,
                                      BGP4_INET_SAFI_UNICAST);
            Bgp4InitNetAddressStruct (pLocalAddr, BGP4_INET_AFI_IPV6,
                                      BGP4_INET_SAFI_UNICAST);
            i4Ret =
                Bgp4GetIpv6LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                   (pPeerEntry), RemAddr,
                                                   &NetAddr);
            if (i4Ret == BGP4_SUCCESS)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr, NetAddr);
                if (u1IsSetLclAddr == BGP4_TRUE)
                {
                    Bgp4CopyNetAddressStruct
                        (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), NetAddr);
                }
            }
            else
            {
                Bgp4InitNetAddressStruct
                    (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), 0, 0);
            }
            break;
#endif
        default:
            break;
    }
    return (i4Ret);
}

/******************************************************************************/
/* Function Name : Bgp4DftLocalAddrForPeer                                    */
/* Description   : This function fills the local interface address            */
/*                 corresponding to peer remote address                       */
/* Input(s)      : pPeerEntry - Specifies the peer to which default route is  */
/*                              to be advertised.                             */
/*               : RemAddr (Remote address of peer), peerentry information    */
/*               : u1IsSetLclAddr - flag indicating whether the LCL_NETADDR   */
/*                                  for the peer needs to be filled or not.   */
/*               : u2Afinfo - Address family of the route                     */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4DftLocalAddrForPeer (tBgp4PeerEntry * pPeerEntry,
                         tAddrPrefix RemAddr, tNetAddress * pLocalAddr,
                         UINT1 u1IsSetLclAddr, UINT2 u2Afinfo)
{
    tNetAddress         NetAddr;
    INT4                i4Ret = BGP4_FAILURE;
    UINT4               u4Flag = BGP4_FALSE;
    UINT2               u2Afi = 0;

    u2Afi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

    if (u2Afi == BGP4_AFI_IN_ADDR_PREFIX_INFO (RemAddr))
    {
        u4Flag = BGP4_TRUE;
    }

    /* The u4Flag is used to validate whether the peer address 
     * and the peer remote address are belong to same family */

    switch (u2Afinfo)
    {
        case BGP4_INET_AFI_IPV4:

            Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            Bgp4InitNetAddressStruct (pLocalAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);

            if ((u4Flag == BGP4_TRUE) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry)) ==
                 BGP4_INET_AFI_IPV4))
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LOCAL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Get peer local address corresponding to peer network
             * address */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_LCLADDR_INFO (pPeerEntry)) != 0)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LCL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Local Address information is not available for the Peer.
             * Lookup the IP Database and get the required Local Address
             * for this peer. Also update the LCL_NETADDR_INFO so that further
             * look ups can be prevented. */
            i4Ret =
                Bgp4GetIpv4LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                   (pPeerEntry), RemAddr,
                                                   &NetAddr);
            if (i4Ret == BGP4_SUCCESS)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr, NetAddr);
                if (u1IsSetLclAddr == BGP4_TRUE)
                {
                    Bgp4CopyNetAddressStruct
                        (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), NetAddr);
                }
            }
            else
            {
                Bgp4InitNetAddressStruct
                    (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), 0, 0);
            }
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:

            Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV6,
                                      BGP4_INET_SAFI_UNICAST);
            Bgp4InitNetAddressStruct (pLocalAddr, BGP4_INET_AFI_IPV6,
                                      BGP4_INET_SAFI_UNICAST);
            if ((u4Flag == BGP4_TRUE) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry)) ==
                 BGP4_INET_AFI_IPV6))
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LOCAL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Get peer local address corresponding to peer network
             * address */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_LCLADDR_INFO (pPeerEntry)) != 0)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr,
                                          BGP4_PEER_LCL_NETADDR_INFO
                                          (pPeerEntry));
                return BGP4_SUCCESS;
            }

            /* Local Address information is not available for the Peer.
             * Lookup the IP Database and get the required Local Address
             * for this peer. Also update the LCL_ADDR_INFO so that further
             * look ups can be prevented. */
            i4Ret =
                Bgp4GetIpv6LocalAddrForRemoteAddr (BGP4_PEER_CXT_ID
                                                   (pPeerEntry), RemAddr,
                                                   &NetAddr);
            if (i4Ret == BGP4_SUCCESS)
            {
                Bgp4CopyNetAddressStruct (pLocalAddr, NetAddr);
                if (u1IsSetLclAddr == BGP4_TRUE)
                {
                    Bgp4CopyNetAddressStruct
                        (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), NetAddr);
                }
            }
            else
            {
                Bgp4InitNetAddressStruct
                    (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerEntry)), 0, 0);
            }
            break;
#endif
        default:
            break;
    }
    return (i4Ret);
}

/******************************************************************************/
/* Function Name : Bgp4TcphOpenConnection                                     */
/* Description   : This function interacts with the lower layer namely SLI    */
/*                 (Socket Layer Interface) to open a TCP connection with     */
/*                 the specified peer. Depending on the result of the         */
/*                 connection proper message is enqueued in BGP task queue.   */
/* Input(s)      : Peer to which connection needs to be established           */
/*                (pPeerentry)                                                */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphOpenConnection (tBgp4PeerEntry * pPeerentry)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
            (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)))
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            i4RetVal = Bgp4Tcphv4OpenConnection (pPeerentry);
            break;
#endif /* BGP_TCP4_WANTED */
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4RetVal = Bgp4Tcphv6OpenConnection (pPeerentry);
            break;
#endif /* BGP_TCP6_WANTED */
        default:
            break;
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name : Bgp4TcphCheckIncomingConn                                  */
/* Description   : This function checks for any new incoming connection.      */
/*                 If any new connection is present then the                  */
/*                 SEM changes for that peer will be initiated.               */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/* Note          : Since all the connection are opened in the Non-Blocking    */
/*                 mode, the incoming connections need to be                  */
/*                 checked periodically. This routine is called from the      */
/*                 BGP task main loop and will get executed for every one     */
/*                 second. This may need to be changed while porting.         */
/******************************************************************************/
INT4
Bgp4TcphCheckIncomingConn (UINT4 u4Context, UINT2 u2Afi)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (u2Afi)
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            i4RetVal = Bgp4Tcphv4CheckIncomingConn (u4Context);
            break;
#endif
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4RetVal = Bgp4Tcphv6CheckIncomingConn (u4Context);
            break;
#endif
        default:
            break;
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name : Bgp4TcphFillAddresses                                      */
/* Description   : This function gets the relevant information from the       */
/*                 Connection ID of the peer and stores them in that          */
/*                 BGP Peer's information.                                    */
/* Input(s)      : BGP Peer information (pPeerentry).                         */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4TcphFillAddresses (tBgp4PeerEntry * pPeerentry)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
            (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)))
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            i4RetVal = Bgp4Tcphv4FillAddresses (pPeerentry);
#endif /* BGP_TCP4_WANTED */
            break;
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4RetVal = Bgp4Tcphv6FillAddresses (pPeerentry);
            break;
#endif /* BGP_TCP6_WANTED */
        default:
            break;
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name : Bgp4TcphOpenListenPort                                     */
/* Description   : Whenever the BGP Global Admin is made UP, this function    */
/*                 is called to open a Listen Socket. On the BGP Task main    */
/*                 loop this socket is checked for new incoming connections.  */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphOpenListenPort (UINT4 u4Context, UINT2 u2Afi)
{
    INT4                i4RetVal = BGP4_FAILURE;

    switch (u2Afi)
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            i4RetVal = Bgp4Tcphv4OpenListenPort (u4Context);
            break;
#endif
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4RetVal = Bgp4Tcphv6OpenListenPort (u4Context);
            break;
#endif
        default:
            break;
    }
    return i4RetVal;
}

UINT4               u4MaxBuf2Process =
    (BGP4_MAX_BUF2PROCESS_MIN + BGP4_MAX_BUF2PROCESS_MAX) / 2;
UINT4               u4MaxTxBuf =
    (BGP4_MAX_TXBUF2PROCESS_MIN + BGP4_MAX_TXBUF2PROCESS_MAX) / 2;
UINT4               u4MaxPeerPkt2Tx =
    (BGP4_MAX_PEER_PKTS2TX_MIN + BGP4_MAX_PEER_PKTS2TX_MAX) / 2;
UINT4               u4MaxPeerInitRts =
    (BGP4_MAX_PEERINIT_RTS_MIN + BGP4_MAX_PEERINIT_RTS_MAX) / 2;
UINT4               u4MaxPkt2Prcs =
    (BGP4_MAX_PKTS2PROCESS_MIN + BGP4_MAX_PKTS2PROCESS_MAX) / 2;
UINT4               u4MaxSoftCfg =
    (BGP4_MAX_SOFTCFGRTS2PROCESS_MIN + BGP4_MAX_SOFTCFGRTS2PROCESS_MAX) / 2;
UINT4               u4MaxPeerRts =
    (BGP4_MAX_PEERRTS2PROCESS_MIN + BGP4_MAX_PEERRTS2PROCESS_MAX) / 2;
UINT4               u4MaxPeer2Prcs =
    (BGP4_MAX_PEERS2PROCESS_MIN + BGP4_MAX_PEERS2PROCESS_MAX) / 2;
UINT4               u4MaxDelRts =
    (BGP4_MAX_DELRTS2PROCESS_MIN + BGP4_MAX_DELRTS2PROCESS_MAX) / 2;
UINT4               u4MaxSyncRts =
    (BGP4_MAX_SYNC_RTS2PRCS_MIN + BGP4_MAX_SYNC_RTS2PRCS_MAX) / 2;
UINT4               u4MaxNHMet =
    (BGP4_MAX_NH_MET_RTS2PRCS_MIN + BGP4_MAX_NH_MET_RTS2PRCS_MAX) / 2;
UINT4               u4MaxFibUpd =
    (BGP4_MAX_FIB_UPD2PROCESS_MIN + BGP4_MAX_FIB_UPD2PROCESS_MAX) / 2;
UINT4               u4MaxRedist =
    (BGP4_MAX_REDIS_UPD2PROCESS_MIN + BGP4_MAX_REDIS_UPD2PROCESS_MAX) / 2;
UINT4               u4MaxExtPeer =
    (BGP4_MAX_EXT_PEER_UPD2PROCESS_MIN + BGP4_MAX_EXT_PEER_UPD2PROCESS_MAX) / 2;
UINT4               u4MaxIntPeer =
    (BGP4_MAX_INT_PEER_UPD2PROCESS_MIN + BGP4_MAX_INT_PEER_UPD2PROCESS_MAX) / 2;
UINT4               u4MaxAggrRt =
    (BGP4_MAX_AGGR_UP_RT2PROCESS_MIN + BGP4_MAX_AGGR_UP_RT2PROCESS_MAX) / 2;
UINT4               u4MaxAggrDown =
    (BGP4_MAX_AGGR_DOWN_RT2PROCESS_MIN + BGP4_MAX_AGGR_DOWN_RT2PROCESS_MAX) / 2;
/******************************************************************************/
/* Function Name : Bgp4ThrottleCalc                                           */
/* Description   : This function calculates the max value to be used based on */
/*                 the previous execution time of Dispatcher function.        */
/* Input(s)      : MIN value of the throttling parameter (u4MinVal)           */
/*               : MAX value of the throttling parameter (u4MaxVal)           */
/*               : Time Taken by previous call to Dispatcher function.        */
/* Output(s)     : None.                                                      */
/* Return(s)     : Throttling value for the corresponding input.              */
/******************************************************************************/
UINT4
Bgp4ThrottleCalc (UINT4 *pu4LastThrotVal, UINT4 u4MinVal, UINT4 u4MaxVal,
                  UINT4 u4ProcessTime)
{
    UINT4               u4ThrotVal = *pu4LastThrotVal;
    UINT4               u4Factor = 0;

    u4Factor = (u4MaxVal - u4MinVal) * 10 / 100;
    if (u4ProcessTime < SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
    {
        u4ThrotVal = *pu4LastThrotVal + u4Factor;
    }
    else if (u4ProcessTime > SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
    {
        if (*pu4LastThrotVal > u4Factor)
        {
            u4ThrotVal = *pu4LastThrotVal - u4Factor;
        }
        else
        {
            u4ThrotVal = u4MinVal;
        }
    }

    if (u4ThrotVal < u4MinVal)
    {
        u4ThrotVal = u4MinVal;
    }
    else if (u4ThrotVal > u4MaxVal)
    {
        if (u4ProcessTime <= (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * 5 / 100))
        {
            /* Mean there is no actvity in BGP Task. */
            u4ThrotVal = u4MaxVal;
        }
    }
    BGP4_DBG1 (BGP4_DBG_ALL, "\tIP_VAL = %d\n", *pu4LastThrotVal);
    BGP4_DBG1 (BGP4_DBG_ALL, "\tPR_TIME = %d\n", u4ProcessTime);
    BGP4_DBG1 (BGP4_DBG_ALL, "\tFactor = %d\n", u4Factor);
    BGP4_DBG1 (BGP4_DBG_ALL, "\tOP_VAL = %d\n", u4ThrotVal);
    *pu4LastThrotVal = u4ThrotVal;
    return u4ThrotVal;
}

VOID
BgpRecomputeThrottle (UINT4 u4Context)
{
    UINT4               u4ProcessTime = 0;

    /* Calculate the last call process time. */
    if (BGP4_DISP_TIME (u4Context)[BGP4_PREV_EXIT_TIME_INDEX] >=
        BGP4_DISP_TIME (u4Context)[BGP4_PREV_ENTRY_TIME_INDEX])
    {
        u4ProcessTime = BGP4_DISP_TIME (u4Context)[BGP4_PREV_EXIT_TIME_INDEX] -
            BGP4_DISP_TIME (u4Context)[BGP4_PREV_ENTRY_TIME_INDEX];
    }
    else
    {
        /* This occurs whenever there is a overflow. */
        u4ProcessTime = 0xFFFFFFFF -
            BGP4_DISP_TIME (u4Context)[BGP4_PREV_ENTRY_TIME_INDEX] +
            BGP4_DISP_TIME (u4Context)[BGP4_PREV_EXIT_TIME_INDEX];
    }

    Bgp4ThrottleCalc (&u4MaxBuf2Process, BGP4_MAX_BUF2PROCESS_MIN,
                      BGP4_MAX_BUF2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxTxBuf, BGP4_MAX_TXBUF2PROCESS_MIN,
                      BGP4_MAX_TXBUF2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxPeerPkt2Tx, BGP4_MAX_PEER_PKTS2TX_MIN,
                      BGP4_MAX_PEER_PKTS2TX_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxPeerInitRts, BGP4_MAX_PEERINIT_RTS_MIN,
                      BGP4_MAX_PEERINIT_RTS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxPkt2Prcs, BGP4_MAX_PKTS2PROCESS_MIN,
                      BGP4_MAX_PKTS2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxSoftCfg, BGP4_MAX_SOFTCFGRTS2PROCESS_MIN,
                      BGP4_MAX_SOFTCFGRTS2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxPeerRts, BGP4_MAX_PEERRTS2PROCESS_MIN,
                      BGP4_MAX_PEERRTS2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxPeer2Prcs, BGP4_MAX_PEERS2PROCESS_MIN,
                      BGP4_MAX_PEERS2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxDelRts, BGP4_MAX_DELRTS2PROCESS_MIN,
                      BGP4_MAX_DELRTS2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxSyncRts, BGP4_MAX_SYNC_RTS2PRCS_MIN,
                      BGP4_MAX_SYNC_RTS2PRCS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxNHMet, BGP4_MAX_NH_MET_RTS2PRCS_MIN,
                      BGP4_MAX_NH_MET_RTS2PRCS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxFibUpd, BGP4_MAX_FIB_UPD2PROCESS_MIN,
                      BGP4_MAX_FIB_UPD2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxRedist, BGP4_MAX_REDIS_UPD2PROCESS_MIN,
                      BGP4_MAX_REDIS_UPD2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxExtPeer, BGP4_MAX_EXT_PEER_UPD2PROCESS_MIN,
                      BGP4_MAX_EXT_PEER_UPD2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxIntPeer, BGP4_MAX_INT_PEER_UPD2PROCESS_MIN,
                      BGP4_MAX_INT_PEER_UPD2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxAggrDown, BGP4_MAX_AGGR_DOWN_RT2PROCESS_MIN,
                      BGP4_MAX_AGGR_DOWN_RT2PROCESS_MAX, u4ProcessTime);
    Bgp4ThrottleCalc (&u4MaxAggrRt, BGP4_MAX_AGGR_UP_RT2PROCESS_MIN,
                      BGP4_MAX_AGGR_UP_RT2PROCESS_MAX, u4ProcessTime);
}

/**************************************************************************/
/* Function Name : BgpIp4FilterRouteSource                                */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. u4SrcAddr - source IP address                       */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
BgpIp4FilterRouteSource (UINT4 u4ContextId, tRouteProfile * pRtProfile)
{
    UINT1               u1Distance = gBgpCxtNode[u4ContextId]->u1Distance;

#ifdef ROUTEMAP_WANTED

    tRtMapInfo          MapInfo;
    tBgp4Info          *pDestBgpInfo;
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;
    tAsPath            *pAspath = NULL;
    UINT1              *pu1CommVal = NULL;
    UINT1               u1RetVal = 0;

    UINT2               u2Count = 0;
    if (gBgpCxtNode[u4ContextId]->DistanceFilterRMap.u1Status ==
        FILTERNIG_STAT_ENABLE)
    {
        MEMSET (&MapInfo, 0, sizeof (MapInfo));
        TMO_SLL_Init (&MapInfo.TSASPath);
        IPVX_ADDR_INIT_IPV4 (MapInfo.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                             pRtProfile->NetAddress.NetAddr.au1Address);

        IPVX_ADDR_INIT_IPV4 (MapInfo.NextHopXAddr, IPVX_ADDR_FMLY_IPV4,
                             pRtProfile->pRtInfo->NextHopInfo.au1Address);

        if (pRtProfile->pPEPeer != NULL)
        {
            IPVX_ADDR_INIT_IPV4 (MapInfo.SrcXAddr, IPVX_ADDR_FMLY_IPV4,
                                 pRtProfile->pPEPeer->peerConfig.RemoteAddrInfo.
                                 au1Address);
        }

        if ((pRtProfile->u1Protocol != BGP_ID) &&
            (pRtProfile->pRtInfo->TSASPath.u4_Count == 0))
        {
            MapInfo.i2RouteType = FSIP_LOCAL;
        }
        else
        {
            MapInfo.i2RouteType = REMOTE;
        }

        if (pRtProfile->pRtInfo != NULL)
        {
            pDestBgpInfo = pRtProfile->pRtInfo;

            TMO_SLL_Scan (BGP4_INFO_ASPATH (pDestBgpInfo), pSrcAspath,
                          tAsPath *)
            {
                pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pDestAspath == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                              BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                              "\tBgpIp4FilterRouteSource() : GET_ASNode failed !!! \n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                    return u1Distance;
                }
                BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
                BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
                MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
                MEMCPY (BGP4_ASPATH_NOS (pDestAspath),
                        BGP4_ASPATH_NOS (pSrcAspath),
                        (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

                TMO_SLL_Add (&(MapInfo.TSASPath), &pDestAspath->sllNode);
            }
        }

        MapInfo.u2DstPrefixLen = pRtProfile->NetAddress.u2PrefixLen;
        if (pRtProfile->pRtInfo != NULL)
        {
            MapInfo.u4LocalPref =
                BGP4_INFO_RCVD_LOCAL_PREF (pRtProfile->pRtInfo);
            MapInfo.i4Origin = pRtProfile->pRtInfo->u1Origin + 1;
            MapInfo.u2RtProto = pRtProfile->u1Protocol;

            if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
                  & BGP4_ATTR_COMM_MASK) == BGP4_ATTR_COMM_MASK)
                && (BGP4_RT_BGP_INFO (pRtProfile)->pCommunity != NULL))
            {
                pu1CommVal =
                    BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO (pRtProfile));
                u2Count = BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile));
                if (u2Count + 1 > MAX_BGP_ATTR_LEN / COMM_VALUE_LEN)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\t Exceeds Attribute Values " "FAILED!!!\n");
                    return u1Distance;
                }
                MEMCPY (&(MapInfo.au4Community),
                        pu1CommVal, (u2Count * COMM_VALUE_LEN));
                MapInfo.u1CommunityCnt = (UINT1) u2Count;
            }
            if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
                  & BGP4_ATTR_MED_MASK) == BGP4_ATTR_MED_MASK))
            {
                MEMCPY (&(MapInfo.i4Metric),
                        &BGP4_INFO_RCVD_MED (pRtProfile->pRtInfo), 4);
            }
        }

        u1RetVal = RMapApplyRule4
            (&MapInfo,
             gBgpCxtNode[u4ContextId]->DistanceFilterRMap.
             au1DistInOutFilterRMapName);

        if (u1RetVal == RMAP_ENTRY_MATCHED || u1RetVal == PERMIT_PREFIX)
        {
            u1Distance =
                gBgpCxtNode[u4ContextId]->DistanceFilterRMap.u1RMapDistance;
        }
        for (;;)
        {
            pAspath = (tAsPath *) TMO_SLL_Last (&(MapInfo.TSASPath));
            if (pAspath == NULL)
            {
                break;
            }
            TMO_SLL_Delete (&(MapInfo.TSASPath), &pAspath->sllNode);
            Bgp4MemReleaseASNode (pAspath);
        }
    }

    return u1Distance;
#else
    UNUSED_PARAM (pRtProfile);
    return u1Distance;
#endif
}

#ifdef ROUTEMAP_WANTED
/**************************************************************************/
/* Function Name : Bgp4CmpFilterRMapName                                  */
/*                                                                        */
/* Description   : Utility function to compare the route map names lexico-*/
/*                 graphically                                            */
/* Inputs        : 1. pFilterRMap - pointer to the filter map having the  */
/*                    routemap name                                       */
/*                 2. pRMapName - pointer to the 2nd route map name,      */
/*                    stored as sNMP octect string                        */
/*                                                                        */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : -1 or 0 or 1                                           */
/**************************************************************************/

INT1
Bgp4CmpFilterRMapName (tFilteringRMap * pFilterRMap,
                       tSNMP_OCTET_STRING_TYPE * pRMapName)
{
    if (pFilterRMap != NULL
        && pRMapName != NULL
        && (UINT4) pRMapName->i4_Length ==
        STRLEN (pFilterRMap->au1DistInOutFilterRMapName))
    {
        if (STRNCMP (pFilterRMap->au1DistInOutFilterRMapName,
                     pRMapName->pu1_OctetList, pRMapName->i4_Length) == 0)
        {
            return 0;
        }
        else
        {
            return (((STRNCMP (pFilterRMap->au1DistInOutFilterRMapName,
                               pRMapName->pu1_OctetList,
                               pRMapName->i4_Length)) < 0) ? (-1) : (1));
        }
    }
    return 1;
}
#endif
/**************************************************************************/
/* Function Name : Bgp4RegisterWithBfd                                    */
/*                                                                        */
/* Description   : This function is to call the BFD API to register with  */
/*                 BFD module to monitor the BGP neighbor IP path         */
/* Inputs        : u4ContextId - Context Identifier                       */
/*                 pPeerEntry  - Pointer to the BGP peer entry            */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : BGP4_SUCCESS/BGP4_FAILURE                              */
/**************************************************************************/
INT4
Bgp4RegisterWithBfd (UINT4 u4ContextId, tBgp4PeerEntry * pPeerEntry)
{
#ifdef BFD_WANTED
    static tBfdReqParams BfdInParams;
    UINT4               u4Addr = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1EventMask[1] = { 0 };

    MEMSET (&BfdInParams, 0, sizeof (tBfdReqParams));

    /* Filling BGP path info for Registering with BFD */
    BfdInParams.u4ReqType = BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING;
    BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_BGP;
    BfdInParams.u4ContextId = u4ContextId;

    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
        (UINT1) BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry);

    if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) == BGP4_INET_AFI_IPV4)
    {
        MEMCPY (&u4Addr, BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);
        u4Addr = OSIX_HTONL (u4Addr);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &u4Addr, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);

        u4Addr = 0;
        MEMCPY (&u4Addr, BGP4_PEER_LOCAL_ADDR (pPeerEntry),
                BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
        u4Addr = OSIX_HTONL (u4Addr);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &u4Addr, BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
    }
    else if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) == BGP4_INET_AFI_IPV6)
    {
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                BGP4_PEER_LOCAL_ADDR (pPeerEntry),
                BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
    }
    else
    {
        return BGP4_FAILURE;
    }

    NetIpv4GetCfaIfIndexFromPort ((UINT4) BGP4_PEER_IF_INDEX (pPeerEntry),
                                  &u4IfIndex);
    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex = u4IfIndex;

    if (Bgp4IsDirectlyConnected (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                                 u4ContextId) == BGP4_TRUE)
    {
        BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;
    }
    else
    {
        BfdInParams.BfdClntInfo.u1SessionType =
            BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS;
    }

    /* The Desired detection timer is not configurable. hence making
     * this value as default detection multipler as BFD 3Sec*/
    BfdInParams.BfdClntInfoParams.u4DesiredDetectionTime = BFD_DEFAULT_TIME_OUT;

    /* Informing status down notification to OSPF Que.
     * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
     * Hence the bit + 1 used to set/check the bit */
    OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
    MEMCPY (&(BfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

    BfdInParams.BfdClntInfoParams.pBfdNotifyPathStatusChange =
        Bgp4HandleNbrPathStatusChange;

    /* BFD API call for registering BGP Nbr path monitoring */
    if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    pPeerEntry->b1IsBfdRegistered = BGP4_TRUE;
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pPeerEntry);
    return BGP4_FAILURE;
#endif
}

/**************************************************************************/
/* Function Name : Bgp4DeRegisterWithBfd                                  */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister or  */
/*                 Disable with BFD module for the BGP neighbor IP path   */
/*                 which was already registered                           */
/* Inputs        : u4ContextId - Context Identifier                       */
/*                 pPeerEntry  - Pointer to the BGP peer entry            */
/*                 u1Flag - Flag to decide whether to de-register/disable */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : BGP4_SUCCESS/BGP4_FAILURE                              */
/**************************************************************************/
INT4
Bgp4DeRegisterWithBfd (UINT4 u4ContextId, tBgp4PeerEntry * pPeerEntry,
                       UINT1 u1Flag)
{
#ifdef BFD_WANTED
    static tBfdReqParams BfdInParams;
    UINT4               u4Addr = 0;
    UINT4               u4IfIndex = 0;

    MEMSET (&BfdInParams, 0, sizeof (tBfdReqParams));

    if (u1Flag == TRUE)
    {
        BfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;
    }
    else
    {
        BfdInParams.u4ReqType = BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING;
    }

    BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_BGP;
    BfdInParams.u4ContextId = u4ContextId;

    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
        (UINT1) BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry);

    if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) == BGP4_INET_AFI_IPV4)
    {
        MEMCPY (&u4Addr, BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);
        u4Addr = OSIX_HTONL (u4Addr);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &u4Addr, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);

        u4Addr = 0;
        MEMCPY (&u4Addr, BGP4_PEER_LOCAL_ADDR (pPeerEntry),
                BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
        u4Addr = OSIX_HTONL (u4Addr);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &u4Addr, BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
    }
    else if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) == BGP4_INET_AFI_IPV6)
    {
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry).u2AddressLen);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                BGP4_PEER_LOCAL_ADDR (pPeerEntry),
                BGP4_PEER_LOCAL_ADDR_INFO (pPeerEntry).u2AddressLen);
    }
    else
    {
        return BGP4_FAILURE;
    }

    NetIpv4GetCfaIfIndexFromPort ((UINT4) BGP4_PEER_IF_INDEX (pPeerEntry),
                                  &u4IfIndex);
    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex = u4IfIndex;

    if (Bgp4IsDirectlyConnected (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                                 u4ContextId) == BGP4_TRUE)
    {
        BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;
    }
    else
    {
        BfdInParams.BfdClntInfo.u1SessionType =
            BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS;
    }

    /* BFD API call for De-registering for the BGP Nbr path monitoring */
    if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    if (u1Flag == TRUE)
    {
        pPeerEntry->b1IsBfdRegistered = BGP4_FALSE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pPeerEntry);
    UNUSED_PARAM (u1Flag);
    return BGP4_FAILURE;
#endif
}

/**************************************************************************/
/* Function Name : Bgp4HandleNbrPathStatusChange                          */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister with*/
/*                 BFD module for the BGP neighbor IP path which was      */
/*                 already registered                                     */
/* Inputs        : u4ContextId - Context Identifier                       */
/*                 pPeerEntry  - Pointer to the BGP peer entry            */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : BGP4_SUCCESS/BGP4_FAILURE                              */
/**************************************************************************/
#ifdef BFD_WANTED
INT1
Bgp4HandleNbrPathStatusChange (UINT4 u4ContextId,
                               tBfdClientNbrIpPathInfo * pNbrPathInfo)
{
    tBgp4QMsg          *pQMsg = NULL;
    tBgpCxtNode        *pBgpCxtNode = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return OSIX_FAILURE;
    }

    pBgpCxtNode = Bgp4GetContextEntry (u4ContextId);
    if (pBgpCxtNode == NULL)
    {
        return OSIX_FAILURE;
    }

    /* BGP should process only the session DOWN notification from BFD */
    if (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_DOWN)
    {
        return OSIX_FAILURE;
    }
    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    BGP4_INPUTQ_CXT (pQMsg) = u4ContextId;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_HANDLE_SESSION_DOWN_NOTIFICATION;

    pQMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2Afi = pNbrPathInfo->u1AddrType;
    pQMsg->QMsg.Bgp4PathNotifyMsg.b1IsOffLoaded =
        pNbrPathInfo->bIsSessCtrlPlaneInDep;

    if (pNbrPathInfo->u1AddrType == BGP4_INET_AFI_IPV4)
    {
        pQMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2AddressLen =
            BGP4_IPV4_PREFIX_LEN;
    }
#ifdef BGP4_IPV6_WANTED
    else if (pNbrPathInfo->u1AddrType == BGP4_INET_AFI_IPV6)
    {
        pQMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2AddressLen =
            BGP4_IPV6_PREFIX_LEN;
    }
#endif
    else
    {
        BGP4_QMSG_FREE (pQMsg);
        return OSIX_FAILURE;
    }

    MEMCPY (&(pQMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.au1Address),
            &(pNbrPathInfo->NbrAddr),
            pQMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2AddressLen);

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif /* BFD_WANTED */

#ifdef L3VPN
#ifdef MPLS_L3VPN_WANTED
/**************************************************************************/
/* Function Name : Bgp4L3VpnRegister                                      */
/* Description   : This function is to used by MPLS to register its       */
/*                 callback utility to receive BGP default VR local As    */
/*                 update                                                 */
/* Inputs        : Bgp4L3VpnNotifyASN - Function pointer registered to BGP*/
/* Outputs       : None                                                   */
/* Return values : BGP4_SUCCESS/BGP4_FAILURE                              */
/**************************************************************************/
INT4
Bgp4L3VpnRegister (INT4 (*Bgp4L3VpnNotifyASN)
                   (UINT1 u1ASNType, UINT4 u4ASNValue))
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "ERROR - BGP Task not initialized.\n");
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    if (BGP4_NOTIFY_ASN != NULL)
    {
        return BGP4_FAILURE;
    }
    BGP4_NOTIFY_ASN = Bgp4L3VpnNotifyASN;
#else
    UNUSED_PARAM (Bgp4L3VpnNotifyASN);
#endif
    return BGP4_SUCCESS;
}
#endif
#endif

#endif /* BGP4PORT_C */
