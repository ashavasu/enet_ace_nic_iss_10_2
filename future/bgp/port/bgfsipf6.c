/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgfsipf6.c,v 1.12 2017/09/15 06:19:51 siva Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGFSIPF6_C
#define BGFSIPF6_C
#include "bgp4com.h"

#ifdef BGP4_IPV6_WANTED

/*****************************************************************************/
/* Function Name :  BgpIp6RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP6 Fwd Table                                     */
/* Input(s)      :  Route entry that has to be searched. (Ipaddr)            */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pRt)               */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp6RtLookup (UINT4 u4Context, tAddrPrefix * IpAddr, UINT2 u2PrefixLen,
                tAddrPrefix * pRt,
                INT4 *pi4Metric, UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            IpPrefix;
    tIp6If             *pIf6 = NULL;
    INT4                i4RetVal = 0;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&IpPrefix, 0, sizeof (tIp6Addr));

    MEMCPY (IpPrefix.u1_addr, IpAddr->au1Address, BGP4_IPV6_PREFIX_LEN);

    i4RetVal = NetIpv6GetFwdTableRouteEntryInCxt (u4Context, &IpPrefix,
                                                  (UINT1) u2PrefixLen,
                                                  &NetIpv6RtInfo);
    if (i4RetVal == NETIPV6_FAILURE)
    {
        MEMSET (pRt->au1Address, 0, BGP4_IPV6_PREFIX_LEN);
        *pi4Metric = BGP4_INVALID_NH_METRIC;
        *pu4RtIfIndx = BGP4_INVALID_IFINDX;
        return BGP4_FAILURE;
    }
    else
    {
        pIf6 = Ip6ifGetEntry (NetIpv6RtInfo.u4Index);
        if ((pIf6 != NULL) && (pIf6->u1OperStatus == NETIPV6_OPER_UP))
        {
            MEMCPY (pRt->au1Address, &(NetIpv6RtInfo.NextHop),
                    BGP4_IPV6_PREFIX_LEN);
            *pi4Metric = NetIpv6RtInfo.u4Metric;
            *pu4RtIfIndx = NetIpv6RtInfo.u4Index;
        }
        else
        {
            MEMSET (pRt->au1Address, 0, BGP4_IPV6_PREFIX_LEN);
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4RtIfIndx = BGP4_INVALID_IFINDX;
            return BGP4_FAILURE;
        }

        /* If i1AppId is 0 , route should be only from IGP Protocols */
        if ((i1AppId == 0)
            && ((NetIpv6RtInfo.i1Proto != RIP_ID)
                && (NetIpv6RtInfo.i1Proto != STATIC_ID)
                && (NetIpv6RtInfo.i1Proto != OSPF_ID)
                && (NetIpv6RtInfo.i1Proto != ISIS_ID)))
        {
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4RtIfIndx = BGP4_INVALID_IFINDX;
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv6*/
/*                 FDB and if present then add the default route to BGP IPv6 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv6GetDefaultRouteFromFDB (UINT4 u4CxtId)
{
    tNetAddress         NetAddr;
    tAddrPrefix         AddrPrefix;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            IpPrefix;
    INT4                i4RetVal;
    UINT4               u4ProtoId = 0;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&IpPrefix, 0, sizeof (tIp6Addr));
    Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV6,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&AddrPrefix, BGP4_INET_AFI_IPV6);

    i4RetVal = NetIpv6GetFwdTableRouteEntry (&IpPrefix, 0, &NetIpv6RtInfo);
    if (i4RetVal == NETIPV6_FAILURE)
    {
        /* Default route is not present. */
        return BGP4_FAILURE;
    }
    else
    {
        /* Default route is present. Add it to BGP */
        MEMCPY (AddrPrefix.au1Address, &(NetIpv6RtInfo.NextHop),
                BGP4_IPV6_PREFIX_LEN);

        if (NetIpv6RtInfo.i1Proto == BGP4_OSPF_ID)
        {
            u4ProtoId = BGP4_OSPF_METRIC;
        }
        else if (NetIpv6RtInfo.i1Proto == BGP4_STATIC_ID)
        {
            u4ProtoId = BGP4_STATIC_METRIC;
        }
        else if (NetIpv6RtInfo.i1Proto == BGP4_LOCAL_ID)
        {
            u4ProtoId = BGP4_DIRECT_METRIC;
        }
        else if (NetIpv6RtInfo.i1Proto == BGP4_RIP_ID)
        {
            u4ProtoId = BGP4_RIP_METRIC;
        }
        else if (NetIpv6RtInfo.i1Proto == BGP4_ISIS_ID)
        {
            u4ProtoId = BGP4_ISIS_METRIC;
        }

        if ((u4ProtoId > 0)
            && ((BGP4_RRD_METRIC_MASK (NetIpv6RtInfo.u4ContextId) & u4ProtoId)
                == u4ProtoId))
        {
            NetIpv6RtInfo.u4Metric =
                BGP4_RRD_METRIC_VAL (NetIpv6RtInfo.u4ContextId, u4ProtoId);
        }

        i4RetVal =
            Bgp4RouteLeak (u4CxtId, NetAddr, NetIpv6RtInfo.i1Proto,
                           AddrPrefix, BGP4_IMPORT_RT_ADD, 0,
                           (INT4) NetIpv6RtInfo.u4Metric, NetIpv6RtInfo.u4Index,
                           0, 0, NULL, NetIpv6RtInfo.u4SetFlag);
        if (i4RetVal == BGP4_FAILURE)
        {
            /* Adding default route to BGP fails. */
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv6RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv6 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv6RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
    /* In case of futureIP6, a new route which differs from the old route in
     * next-hop or metric or o/p interface will be treated as an alternative
     * route. So under that case, we need to delete the route explicityly.
     */
    if (((PrefixMatch (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pNewRtProfile),
                       BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pOldRtProfile)))
         != BGP4_TRUE) ||
        (BGP4_RT_MED (pNewRtProfile) != BGP4_RT_MED (pOldRtProfile)) ||
        (BGP4_RT_GET_RT_IF_INDEX (pNewRtProfile) !=
         BGP4_RT_GET_RT_IF_INDEX (pOldRtProfile)))
    {
        return BGP4_FALSE;
    }

    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIp6RtTbl                              */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP Forwarding.                   */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp6RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv6RtInfo      RtInfo;
    tAddrPrefix         DefRoute;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tIp6Addr            PeerAddr;

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\t Received route %s for adding in IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif

#ifdef RRD_WANTED
    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be added
     *      * back to IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (DefRoute.au1Address, 0, BGP4_IPV6_PREFIX_LEN);
    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);
    RtInfo.u4Index = (UINT4) BGP4_RT_GET_RT_IF_INDEX (pRtProfile);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Prefixlen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = (UINT4) BGP4_RT_MED (pRtProfile);
    RtInfo.i1Type = REMOTE;
    RtInfo.u4RouteTag = 0;        /* wud be updated based on the peer entry */
    RtInfo.u4RowStatus = ACTIVE;
    RtInfo.i1DefRtrFlag = 0;
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.i1Proto = (INT1) BGP4_RT_PROTOCOL (pRtProfile);

    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
    }

    if (MEMCMP (RtInfo.Ip6Dst.u1_addr, DefRoute.au1Address,
                BGP4_IPV6_PREFIX_LEN) == 0)
    {
        RtInfo.i1DefRtrFlag = 1;
    }

    MEMCPY (PeerAddr.u1_addr,
            BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Preference = BgpIp6FilterRouteSource (BGP4_RT_CXT_ID (pRtProfile),
                                                   pRtProfile);

    if (NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &RtInfo) == NETIPV6_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s "
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }
    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s "
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to delete an entry present in the    */
/*                 TRIE routing table.                                       */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{

    tNetIpv6RtInfo      RtInfo;
    tAddrPrefix         DefRoute;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\t Received route %s for deleting from IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif

#ifdef RRD_WANTED
    /* Non-bgp routes are learnt from RTM. Should not be deleted from RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be deleted
     *      * from IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (DefRoute.au1Address, 0, BGP4_IPV6_PREFIX_LEN);
    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);
    RtInfo.u4Index = (UINT4) BGP4_RT_GET_RT_IF_INDEX (pRtProfile);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Prefixlen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = (UINT4) BGP4_RT_MED (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.i1Type = REMOTE;
    RtInfo.u4RouteTag = 0;        /* wud be updated based on the peer entry */
    RtInfo.u4RowStatus = IPFWD_DESTROY;
    RtInfo.i1DefRtrFlag = 0;
    RtInfo.i1Proto = (INT1) BGP4_RT_PROTOCOL (pRtProfile);

    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
    }

    if (MEMCMP (RtInfo.Ip6Dst.u1_addr, DefRoute.au1Address,
                BGP4_IPV6_PREFIX_LEN) == 0)
    {
        RtInfo.i1DefRtrFlag = 1;
    }
    if (NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &RtInfo) == NETIPV6_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDELETING Dest = %s Mask = %s Nexthop = %s "
                       "From IP FWD Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDELETED Dest = %s Mask = %s Nexthop = %s "
                   "From IP FWD Table Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{

    tNetIpv6RtInfo      RtInfo;
    tAddrPrefix         DefRoute;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tIp6Addr            PeerAddr;

    MEMSET (&PeerAddr, 0, sizeof (tIp6Addr));

#ifdef L3VPN_WANTED
    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tReceived route %s for modifying the route in IP forwarding "
                           "routing table with nexthop %s immediate nexthop %s "
                           "flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
#endif

#ifdef RRD_WANTED
    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }
#else
    /* Non-bgp except STATIC routes are learnt from IP. Should not be added
     *      * back to IP */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
        (BGP4_RT_PROTOCOL (pRtProfile) != STATIC_ID))
    {                            /* Route is a non-bgp route learnt from IP */
        return (BGP4_SUCCESS);
    }
#endif

    MEMSET (&RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (DefRoute.au1Address, 0, BGP4_IPV6_PREFIX_LEN);
    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);
    RtInfo.u4Index = (UINT4) BGP4_RT_GET_RT_IF_INDEX (pRtProfile);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Prefixlen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = (UINT4) BGP4_RT_MED (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.i1Type = REMOTE;
    RtInfo.u4RouteTag = 0;        /* wud be updated based on the peer entry */
    RtInfo.u4RowStatus = ACTIVE;
    RtInfo.i1DefRtrFlag = 0;
    RtInfo.i1Proto = (INT1) BGP4_RT_PROTOCOL (pRtProfile);

    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
    }

    if (MEMCMP (RtInfo.Ip6Dst.u1_addr, DefRoute.au1Address,
                BGP4_IPV6_PREFIX_LEN) == 0)
    {
        RtInfo.i1DefRtrFlag = 1;
    }
    MEMCPY (PeerAddr.u1_addr,
            BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Preference = BgpIp6FilterRouteSource (BGP4_RT_CXT_ID (pRtProfile),
                                                   pRtProfile);

    if (NetIpv6LeakRoute (NETIPV6_MODIFY_ROUTE, &RtInfo) == NETIPV6_FAILURE)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s "
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s "
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (AddrPrefix1, AddrPrefix2)                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv6IsOnSameSubnet (tAddrPrefix * pDestAddr, tAddrPrefix * pLocalAddr)
{
    tIp6Addr            DestAddr;
    tIp6Addr            LocalAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&LocalAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, pDestAddr->au1Address, BGP4_IPV6_PREFIX_LEN);
    MEMCPY (LocalAddr.u1_addr, pLocalAddr->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (FALSE);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;
        if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
        {
            if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                == NETIPV6_SUCCESS)
            {

                for (;;)
                {
                    /* checking whether LocalAddr belongs to any of the Inerfaces */
                    if (MEMCMP (NetIpv6AddrInfo.Ip6Addr.u1_addr, LocalAddr.u1_addr, BGP4_IPV6_PREFIX_LEN) == 0)    /*chk Local or Dst */
                    {
                        /* the prefixlen of the matched If */
                        u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
                        /* Matching DestAddr and LocalAddr over the If's Prefixlen */
                        if (Ip6AddrMatch
                            (&DestAddr, &LocalAddr, (INT4) u4PrefixLen) == TRUE)
                        {
                            return (TRUE);
                        }
                        else
                        {
                            return (FALSE);
                        }
                    }
                    if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                              &NetIpv6NextAddrInfo) ==
                        NETIPV6_FAILURE)
                    {
                        break;
                    }
                    MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
        }
        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (FALSE);
        }
    }

}

/*****************************************************************************/
/* Function Name : Ip6IsLocalSubnet                                          */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalSubnet (UINT1 *pu1AddrPrefix, UINT2 *pu2Port)
{
    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, pu1AddrPrefix, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (NETIPV6_FAILURE);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;

        if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
        {
            if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                == NETIPV6_SUCCESS)
            {

                for (;;)
                {
                    /* the prefixlen of the matched If */
                    u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
                    /* checking whether LocalAddr belongs to any of the Inerfaces */
                    if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                                      (INT4) u4PrefixLen) == TRUE)
                    {
                        /*Get the Port */
                        *pu2Port = (UINT2) NetIpv6IfInfo.u4IpPort;
                        return (NETIPV6_SUCCESS);
                    }
                    if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                              &NetIpv6NextAddrInfo) ==
                        NETIPV6_FAILURE)
                    {
                        break;
                    }
                    MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
        }
        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (NETIPV6_FAILURE);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv6IsDirectlyConnected (tAddrPrefix * IpAddress, UINT4 u4Context)
{
    tIp6Addr            DestAddr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, IpAddress->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFwdTableRouteEntryInCxt (u4Context, &DestAddr,
                                           BGP4_MAX_IPV6_PREFIXLEN,
                                           &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FALSE);
    }

    /* Interface index thro which this route is learnt */
    u4Index = NetIpv6RtInfo.u4Index;

    if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
            /* Matching DestAddr and LocalAddr over the If's Prefixlen */
            if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                return (BGP4_TRUE);
            }

            if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                      &NetIpv6NextAddrInfo) == NETIPV6_FAILURE)
            {
                break;
            }
            MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                    sizeof (tNetIpv6AddrInfo));
        }
    }

    return (BGP4_FALSE);
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetNetAddrPrefixLen                               */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address that needs to be checked (IpAddress)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv6GetNetAddrPrefixLen (tAddrPrefix * IpAddress)
{
    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT2               u2PrefixLen = 0;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, IpAddress->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (u2PrefixLen);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;

        if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
        {
            if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                == NETIPV6_SUCCESS)
            {

                for (;;)
                {
                    u2PrefixLen = (UINT2) NetIpv6AddrInfo.u4PrefixLength;
                    /* checking whether LocalAddr belongs to any of the Inerfaces */
                    if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                                      (INT4) u2PrefixLen) == TRUE)
                    {
                        /* the prefixlen of the matched If */
                        return (u2PrefixLen);

                    }
                    if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                              &NetIpv6NextAddrInfo) ==
                        NETIPV6_FAILURE)
                    {
                        break;
                    }
                    MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
        }

        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (u2PrefixLen);
        }
    }

}

/*****************************************************************************/
/* Function Name : Ip6IsLocalAddr                                            */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : IPv6 address , pu2Port                                    */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalAddr (UINT4 u4Context, UINT1 *pAddr, UINT2 *pu2Port)
{

    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4Index;
    UINT4               u4CxtId;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, pAddr, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (NETIPV6_FAILURE);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;
        if ((VcmGetContextIdFromCfaIfIndex (u4Index, &u4CxtId)
             == VCM_SUCCESS) && (u4CxtId == u4Context))
        {
            if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
            {
                if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                    == NETIPV6_SUCCESS)
                {

                    for (;;)
                    {
                        /* checking whether LocalAddr belongs to any of the Interfaces */
                        if (MEMCMP
                            (NetIpv6AddrInfo.Ip6Addr.u1_addr, DestAddr.u1_addr,
                             BGP4_IPV6_PREFIX_LEN) == 0)
                        {
                            *pu2Port = (UINT2) NetIpv6IfInfo.u4IpPort;
                            return (NETIPV6_SUCCESS);

                        }
                        if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                                  &NetIpv6NextAddrInfo) ==
                            NETIPV6_FAILURE)
                        {
                            break;
                        }
                        MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
            }
        }

        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (NETIPV6_FAILURE);
        }
    }
}

/*****************************************************************************/
/* Function Name : Ip6GetAssocIfType                                         */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : IPv6 address , pu2Port                                    */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6GetAssocIfType (UINT4 u4Context, UINT1 *pAddr, UINT4 *pu4IfType)
{

    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4Index;
    UINT4               u4CxtId;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, pAddr, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (NETIPV6_FAILURE);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;
        if ((VcmGetContextIdFromCfaIfIndex (u4Index, &u4CxtId)
             == VCM_SUCCESS) && (u4CxtId == u4Context))
        {
            if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
            {
                if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                    == NETIPV6_SUCCESS)
                {

                    for (;;)
                    {
                        /* checking whether IPv6 Addr belongs to any of the Interfaces */
                        if (MEMCMP
                            (NetIpv6AddrInfo.Ip6Addr.u1_addr, DestAddr.u1_addr,
                             BGP4_IPV6_PREFIX_LEN) == 0)
                        {
                            *pu4IfType = NetIpv6IfInfo.u4InterfaceType;
                            return (NETIPV6_SUCCESS);

                        }
                        if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                                  &NetIpv6NextAddrInfo) ==
                            NETIPV6_FAILURE)
                        {
                            break;
                        }
                        MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
            }
        }

        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (NETIPV6_FAILURE);
        }
    }
    return (NETIPV6_FAILURE);
}

/*****************************************************************************/
/* Function Name : BgpGetIp6IfAddr                                           */
/* Description   : This routine Gets the local-address of given interface    */
/* Input(s)      : Interface index no.                                       */
/*                 input address                                             */
/* Output(s)     : None.                                                     */
/* Return(s)     : Interface Address or Zero                                 */
/*****************************************************************************/
INT4
BgpGetIp6IfAddr (UINT2 u2IfIndex, UINT1 *pAddr, UINT1 *pLocalAddr)
{
    tNetIpv6AddrInfo    Ipv6LocAddr;
    tNetIpv6AddrInfo    Ipv6NextLocAddr;
    UINT4               u4Index = (UINT4) u2IfIndex;
    UINT4               u4PrefixLen;

    MEMSET (&Ipv6LocAddr, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&Ipv6NextLocAddr, 0, sizeof (tNetIpv6AddrInfo));

    if (NetIpv6GetFirstIfAddr (u4Index, &Ipv6LocAddr) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = Ipv6LocAddr.u4PrefixLength;
            /* checking whether Addr belongs to any of the Local subnet */
            if (Ip6AddrMatch (&Ipv6LocAddr.Ip6Addr, (tIp6Addr *) (VOID *) pAddr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                MEMCPY (pLocalAddr, Ipv6LocAddr.Ip6Addr.u1_addr,
                        BGP4_IPV6_PREFIX_LEN);
                return (BGP4_SUCCESS);

            }
            if (NetIpv6GetNextIfAddr (u4Index, &Ipv6LocAddr,
                                      &Ipv6NextLocAddr) == NETIPV6_FAILURE)
            {
                return (BGP4_FAILURE);
            }
            MEMCPY (&Ipv6LocAddr, &Ipv6NextLocAddr, sizeof (tNetIpv6AddrInfo));
        }
    }
    return (BGP4_FAILURE);
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv6LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv6LocalAddrForRemoteAddr (UINT4 u4Context, tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
    tIp6Addr            DestAddr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, RemAddr.au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFwdTableRouteEntryInCxt (u4Context, &DestAddr,
                                           BGP4_MAX_IPV6_PREFIXLEN,
                                           &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FAILURE);
    }
    u4Index = NetIpv6RtInfo.u4Index;
    if (NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FAILURE);
    }

    u4Index = NetIpv6IfInfo.u4IfIndex;

    if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
            /* Matching DestAddr and LocalAddr over the If's Prefixlen */
            if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                MEMCPY (pLocalAddr->NetAddr.au1Address,
                        NetIpv6AddrInfo.Ip6Addr.u1_addr, BGP4_IPV6_PREFIX_LEN);
                pLocalAddr->u2PrefixLen = (UINT2) u4PrefixLen;
                return (BGP4_SUCCESS);
            }

            if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                      &NetIpv6NextAddrInfo) == NETIPV6_FAILURE)
            {
                break;
            }
            MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                    sizeof (tNetIpv6AddrInfo));
        }
    }
    return (BGP4_FAILURE);
}

INT4
Bgp4FillLinkLocalAddress (tBgp4PeerEntry * pPeerentry)
{
    tNetIpv6RtInfo      RtInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIp6Addr            DestAddr;
    tIp6Addr            InAddr;
    tIp6Addr           *pLinkLocalAddr = NULL;
    INT4                i4Ret;
    UINT2               u2Afi;
    UINT4               u4Index;

    MEMSET (&RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&InAddr, 0, sizeof (tIp6Addr));

    u2Afi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry));

    if (u2Afi == BGP4_INET_AFI_IPV6)
    {
        MEMCPY (DestAddr.u1_addr, BGP4_PEER_REMOTE_ADDR (pPeerentry),
                BGP4_IPV6_PREFIX_LEN);
    }
    else
    {
        MEMCPY (DestAddr.u1_addr, BGP4_PEER_NETWORK_ADDR (pPeerentry),
                BGP4_IPV6_PREFIX_LEN);
    }

    if (MEMCMP (&DestAddr.u1_addr, &InAddr.u1_addr, BGP4_IPV6_PREFIX_LEN) == 0)
    {
        return BGP4_FAILURE;
    }

    i4Ret = NetIpv6GetFwdTableRouteEntryInCxt (BGP4_PEER_CXT_ID (pPeerentry),
                                               &DestAddr,
                                               BGP4_MAX_IPV6_PREFIXLEN,
                                               &RtInfo);

    if (i4Ret == NETIPV6_FAILURE)
    {
        return BGP4_FAILURE;
    }

    u4Index = RtInfo.u4Index;
    if (NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FAILURE);
    }

    pLinkLocalAddr = &(NetIpv6IfInfo.Ip6Addr);

    MEMCPY (BGP4_PEER_LINK_LOCAL_ADDR (pPeerentry),
            pLinkLocalAddr->u1_addr, BGP4_IPV6_PREFIX_LEN);

    return BGP4_SUCCESS;
}

/**************************************************************************/
/* Function Name : BgpIp6FilterouteSource                                 */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pSrcAddr - source IPv6 address                      */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
BgpIp6FilterRouteSource (UINT4 u4ContextId, tRouteProfile * pRtProfile)
{
#ifdef ROUTEMAP_WANTED
    UINT1               u1Distance = gBgpCxtNode[u4ContextId]->u1Distance;
    tRtMapInfo          MapInfo;
    tBgp4Info          *pDestBgpInfo;
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;
    tAsPath            *pAspath = NULL;
    UINT1              *pu1CommVal = NULL;

    UINT2               u2Count = 0;
    UINT1               u1RetVal = 0;

    if (gBgpCxtNode[u4ContextId]->DistanceFilterRMap.u1Status ==
        FILTERNIG_STAT_ENABLE)
    {
        MEMSET (&MapInfo, 0, sizeof (MapInfo));
        TMO_SLL_Init (&MapInfo.TSASPath);
        if (pRtProfile->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
        {
            IPVX_ADDR_INIT_IPV6 (MapInfo.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->NetAddress.NetAddr.au1Address);

            IPVX_ADDR_INIT_IPV6 (MapInfo.NextHopXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->pRtInfo->NextHopInfo.au1Address);

            if (pRtProfile->pPEPeer != NULL)
            {
                IPVX_ADDR_INIT_IPV6 (MapInfo.SrcXAddr, IPVX_ADDR_FMLY_IPV6,
                                     pRtProfile->pPEPeer->peerConfig.
                                     RemoteAddrInfo.au1Address);
            }
        }

        if ((pRtProfile->u1Protocol != BGP_ID) &&
            (pRtProfile->pRtInfo->TSASPath.u4_Count == 0))
        {
            MapInfo.i2RouteType = FSIP_LOCAL;
        }
        else
        {
            MapInfo.i2RouteType = REMOTE;
        }

        if (pRtProfile->pRtInfo != NULL)
        {
            pDestBgpInfo = pRtProfile->pRtInfo;

            TMO_SLL_Scan (BGP4_INFO_ASPATH (pDestBgpInfo), pSrcAspath,
                          tAsPath *)
            {
                pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pDestAspath == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                              BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                              "\tBgpIp6FilterRouteSource() : GET_ASNode failed !!! \n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                    return IP6_PREFERENCE_BGP;
                }
                BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
                BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
                MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
                MEMCPY (BGP4_ASPATH_NOS (pDestAspath),
                        BGP4_ASPATH_NOS (pSrcAspath),
                        (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

                TMO_SLL_Add (&(MapInfo.TSASPath), &pDestAspath->sllNode);
            }
        }

        MapInfo.u2DstPrefixLen = pRtProfile->NetAddress.u2PrefixLen;
        if (pRtProfile->pRtInfo != NULL)
        {
            MapInfo.u4LocalPref =
                BGP4_INFO_RCVD_LOCAL_PREF (pRtProfile->pRtInfo);
            MapInfo.i4Origin = pRtProfile->pRtInfo->u1Origin + 1;
            MapInfo.u2RtProto = pRtProfile->u1Protocol;

            if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
                  & BGP4_ATTR_COMM_MASK) == BGP4_ATTR_COMM_MASK)
                && (BGP4_RT_BGP_INFO (pRtProfile)->pCommunity != NULL))
            {
                pu1CommVal =
                    BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO (pRtProfile));
                u2Count = BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile));
                if (u2Count + 1 > MAX_BGP_ATTR_LEN / COMM_VALUE_LEN)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\t Exceeds Attribute Values " "FAILED!!!\n");
                    return IP6_PREFERENCE_BGP;
                }
                MEMCPY (&(MapInfo.au4Community),
                        pu1CommVal, (u2Count * COMM_VALUE_LEN));
                MapInfo.u1CommunityCnt = (UINT1) u2Count;
            }
            if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
                  & BGP4_ATTR_MED_MASK) == BGP4_ATTR_MED_MASK))
            {
                MEMCPY (&(MapInfo.i4Metric),
                        &BGP4_INFO_RCVD_MED (pRtProfile->pRtInfo), 4);
            }
        }

        u1RetVal = RMapApplyRule4
            (&MapInfo,
             gBgpCxtNode[u4ContextId]->DistanceFilterRMap.
             au1DistInOutFilterRMapName);

        if (u1RetVal == RMAP_ENTRY_MATCHED || u1RetVal == PERMIT_PREFIX)
        {
            u1Distance =
                gBgpCxtNode[u4ContextId]->DistanceFilterRMap.u1RMapDistance;
        }
        for (;;)
        {
            pAspath = (tAsPath *) TMO_SLL_Last (&(MapInfo.TSASPath));
            if (pAspath == NULL)
            {
                break;
            }
            TMO_SLL_Delete (&(MapInfo.TSASPath), &pAspath->sllNode);
            Bgp4MemReleaseASNode (pAspath);
        }
    }

    return u1Distance;
#else
    UNUSED_PARAM (pRtProfile);
    UNUSED_PARAM (u4ContextId);
    return IP6_PREFERENCE_BGP;
#endif
}

/**************************************************************************/
/* Function Name : BgpIp6CbRtChgHandler                                   */
/*                                                                        */
/* Description   : This routine gets route chanhe notification from RTM6  */
/* Inputs        : none                                                   */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/

VOID
BgpIp6CbRtChgHandler (tNetIpv6HliParams * pIp6HliParams)
{
    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_ROUTE_CHANGE:
            if (pIp6HliParams->unIpv6HlCmdType.RouteChange.i1Proto !=
                IP6_BGP_PROTOID)
            {
                if (BGP4_GLB_RESTART_STATUS == BGP4_GR_STATUS_NONE)
                {
                    BgpIp6HandleRouteChgHandler (&
                                                 (pIp6HliParams->
                                                  unIpv6HlCmdType.RouteChange));
                }
            }
            break;
        default:
            break;
    }
    return;

}

/**************************************************************************/
/* Function Name : BgpIp6HandleRouteChgHandler                            */
/*                                                                        */
/* Description   : This routine gets route chanhe notification from RTM6  */
/* Inputs        : none                                                   */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/

VOID
BgpIp6HandleRouteChgHandler (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tNetworkAddr       *pNetworkRt = NULL;
    tNetworkAddr        NetworkAddr;
    tNetworkAddr        NetworkRoute;
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4Action = 0;
    UINT4               u4Context = 0;

    MEMSET (&NetworkRoute, 0, sizeof (tNetworkAddr));
    MEMSET (&NetworkAddr, 0, sizeof (tNetworkAddr));

    MEMCPY (NetworkAddr.NetworkAddr.au1Address, pNetIpv6RtInfo->Ip6Dst.u1_addr,
            BGP4_IPV6_PREFIX_LEN);
    NetworkAddr.NetworkAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
    NetworkAddr.NetworkAddr.u2Afi = BGP4_INET_AFI_IPV6;
    u4Context = pNetIpv6RtInfo->u4ContextId;

    pNetworkRt =
        Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr, &NetworkRoute);
    if (pNetworkRt == NULL)
    {
        /*Network entry is not matched with the incoming route indication from RTM */
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Inteface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }

    Bgp4InitAddrPrefixStruct (&(BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg)),
                              BGP4_INET_AFI_IPV6);
    Bgp4InitAddrPrefixStruct (&(BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg)),
                              BGP4_INET_AFI_IPV6);

    if (pNetIpv6RtInfo->u4RowStatus == RTM6_ACTIVE)
    {
        u4Action = BGP4_IMPORT_RT_ADD;
    }
    else if (pNetIpv6RtInfo->u4RowStatus == RTM6_DESTROY)
    {
        u4Action = BGP4_IMPORT_RT_DELETE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_ROUTE_ACTION (pQMsg) = u4Action;
    BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO (pQMsg) = pNetIpv6RtInfo->u1Prefixlen;
    BGP4_INPUTQ_ROUTE_PROTO_ID (pQMsg) = pNetIpv6RtInfo->i1Proto;
    BGP4_INPUTQ_ROUTE_IF_INDEX (pQMsg) = pNetIpv6RtInfo->u4Index;
    BGP4_INPUTQ_ROUTE_METRIC (pQMsg) = pNetIpv6RtInfo->u4Metric;

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
            (BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg)),
            pNetIpv6RtInfo->Ip6Dst.u1_addr, BGP4_IPV6_PREFIX_LEN);

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
            (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg)),
            pNetIpv6RtInfo->NextHop.u1_addr, BGP4_IPV6_PREFIX_LEN);

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;

}

#endif /* BGP4_IPV6_WANTED */

#endif /* BGFSIPF6_C */
