/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bglnipf6.h,v 1.10 2016/12/28 12:14:14 siva Exp $
 *
 * Description:This file includes all the BGP based header files.
 *
 *******************************************************************/
#ifndef BGLNIPF6_H
#define BGLNIPF6_H

#include "fssocket.h"
#include "ipv6.h"
#include "bglnipf4.h"


#define BGP4_IN6_IS_ADDR_UNSPECIFIED(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr;\
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN);\
    if (IN6_IS_ADDR_UNSPECIFIED (Ipv6Addr.u1_addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}
#define BGP4_IN6_IS_ADDR_MULTICAST(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr;\
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN);\
    if (IN6_IS_ADDR_MULTICAST (Ipv6Addr.u1_addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_LOOPBACK(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr;\
    MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN);\
    if (IN6_IS_ADDR_LOOPBACK (Ipv6Addr.u1_addr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}
#define BGP4_IN6_IS_ADDR_BROADCAST(pAddr, Status) \
{ \
        tIp6Addr    Ipv6Addr;\
        MEMCPY((Ipv6Addr.u1_addr), pAddr, BGP4_IPV6_PREFIX_LEN); \
        if (IS_ADDR_BROAD (Ipv6Addr))  { Status = BGP4_TRUE; } \
        else { Status = BGP4_FALSE; } \
}


#define BGP4_IN6_IS_ADDR_LINKLOCAL(pAddr, Status) \
{\
    if (IN6_IS_ADDR_LINKLOCAL (pAddr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_V4COMPAT(pAddr, Status) \
{\
    if (IN6_IS_ADDR_V4COMPAT (pAddr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_V4MAPPED(pAddr, Status) \
{\
    if (IN6_IS_ADDR_V4MAPPED(pAddr))  { Status = BGP4_TRUE; } \
    else { Status = BGP4_FALSE; } \
}

#define BGP4_IN6_IS_ADDR_V4COMPATIBLE(pAddr, Status) \
{\
    if (IN6_IS_ADDR_V4COMPAT(pAddr))  { Status = BGP4_TRUE; } \
        else if (IN6_IS_ADDR_V4MAPPED(pAddr)) { Status = BGP4_TRUE; } \
            else { Status = BGP4_FALSE; } \
}


/* Prototype definitions */
INT4  BgpIp6RtLookup (UINT4 u4Context, tAddrPrefix *, UINT2 , tAddrPrefix *, INT4 *, 
                      UINT4 *, INT1 );
INT4  Bgp4Ipv6GetDefaultRouteFromFDB (UINT4);
INT4  BGP4CanRepIpv6RtAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4  BGP4RTAddRouteToCommIp6RtTbl(tRouteProfile * );
INT4  BGP4RTDeleteRouteInCommIp6RtTbl (tRouteProfile * );
INT4  BGP4RTModifyRouteInCommIp6RtTbl (tRouteProfile * );
BOOL1 Bgp4Ipv6IsOnSameSubnet (tAddrPrefix *, tAddrPrefix *);
UINT1 Bgp4Ipv6IsDirectlyConnected (tAddrPrefix *, UINT4);
UINT2 Bgp4Ipv6GetNetAddrPrefixLen (tAddrPrefix *);
INT4  BgpGetIp6IfAddr (UINT2 , UINT1 *, UINT1 *);
INT4  BgpGetIp6IfPrefixLen (UINT2 , UINT1 *, UINT2 *);
INT4  Ip6IsLocalAddr (UINT4, UINT1 * , UINT2 *);
INT4  Ip6IsLocalSubnet (UINT1 *, UINT2 *);
INT4  Bgp4FillLinkLocalAddress(tBgp4PeerEntry *);
INT4  Bgp4GetIpv6LocalAddrForRemoteAddr (UINT4, tAddrPrefix , tNetAddress *);

INT4 Ip6MapPortToAddress (unsigned short , UINT1 *, UINT1* );
INT4 Ip6MapPortToMask (unsigned short , UINT1 *, UINT1* );
int IsIp6LocalAddress (UINT1 *, unsigned short *, UINT4);
VOID BgpIp6CbRtChgHandler(tNetIpv6HliParams * pIp6HliParams);
VOID BgpIp6HandleRouteChgHandler(tNetIpv6RtInfo * pNetIpv6RtInfo);


#endif /* BGLNIPF6_H */
