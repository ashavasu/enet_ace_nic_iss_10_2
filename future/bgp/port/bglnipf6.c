
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bglnipf6.c,v 1.14 2016/12/28 12:14:14 siva Exp $
 *
 *******************************************************************/
#ifndef BGLNIPF6_C
#define BGLNIPF6_C
#include "bgp4com.h"

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name :  BgpIp6RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP6 Fwd Table                                     */
/* Input(s)      :  Route entry that has to be searched. (Ipaddr)            */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pRt)               */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp6RtLookup (UINT4 u4Context, tAddrPrefix * IpAddr, UINT2 u2PrefixLen,
                tAddrPrefix * pRt, INT4 *pi4Metric, UINT4 *pu4RtIfIndx,
                INT1 i1AppId)
{
    tNetIpv6RtInfo      NetRtInfo;
    tIp6Addr            IpPrefix;
    INT4                i4RetVal;

    UNUSED_PARAM (i1AppId);

    MEMSET (&NetRtInfo, 0, sizeof (NetRtInfo));
    MEMSET (&IpPrefix, 0, sizeof (tIp6Addr));

    MEMCPY (IpPrefix.u1_addr, IpAddr->au1Address, BGP4_IPV6_PREFIX_LEN);

    i4RetVal = NetIpv6GetFwdTableRouteEntryInCxt (u4Context, &IpPrefix,
                                                  (UINT1) u2PrefixLen,
                                                  &NetRtInfo);
    if (i4RetVal == BGP4_FAILURE)
    {
        MEMSET (pRt->au1Address, 0, BGP4_IPV6_PREFIX_LEN);
        *pi4Metric = BGP4_INVALID_NH_METRIC;
        *pu4RtIfIndx = BGP4_INVALID_IFINDX;
        return BGP4_FAILURE;
    }
    else
    {
        MEMCPY (pRt->au1Address, &(NetRtInfo.NextHop), BGP4_IPV6_PREFIX_LEN);
        *pi4Metric = (INT4) NetRtInfo.u4Metric;
        *pu4RtIfIndx = NetRtInfo.u4Index;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv6*/
/*                 FDB and if present then add the default route to BGP IPv6 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv6GetDefaultRouteFromFDB (UINT4 u4Context)
{
    /* This routine has to modified when redistribution is implemented
     * in Linux. */
    UNUSED_PARAM (u4Context);
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv6RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv6 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv6RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
    /* For Linux OS currently, we are not able to replace the old route with
     * the new route because RT_MODIFY function is unable to do it. So 
     * return BGP4_FALSE. If RT_MODIFY function is updated, then update
     * the route accordingly. */
    UNUSED_PARAM (pNewRtProfile);
    UNUSED_PARAM (pOldRtProfile);
    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIp6RtTbl                              */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP6 Forwarding.                  */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp6RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv6RtInfo      RtInfo;
    UINT1              *pNextHop = NULL;
    UINT2               u2Ipv4Addr = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4RetVal = BGP4_FALSE;

    MEMSET (&RtInfo, 0, sizeof (RtInfo));

    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u4Index = (UINT4) BGP4_RT_GET_RT_IF_INDEX (pRtProfile);
    RtInfo.u1Prefixlen = BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = BGP4_RT_MED (pRtProfile);
    RtInfo.i1Proto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.u4RowStatus = ACTIVE;
    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    BGP4_IN6_IS_ADDR_V4COMPATIBLE (BGP4_INFO_NEXTHOP
                                   (BGP4_RT_BGP_INFO (pRtProfile)), i4RetVal);
    if (i4RetVal == BGP4_TRUE)
    {
        u2Ipv4Addr = BGP4_6TO4_PREFIX;
        pNextHop = BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile));
        PTR_FETCH_4 (u4IpAddr, &(pNextHop[TWELVE]));
        PTR_ASSIGN_2 (RtInfo.NextHop.u1_addr, u2Ipv4Addr);
        PTR_ASSIGN_4 (&(RtInfo.NextHop.u1_addr[TWO]), u4IpAddr);
    }

    i4RetVal = NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &RtInfo);
    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tADDING Dest = %s Mask = %s Nexthop = %s \n\t"
                       "TO IP FWD Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tADDING Dest = %s Mask = %s Nexthop = %s \n\t"
                   "TO IP FWD Table SUCCESSFUL\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to delete an entry present in the    */
/*                 IP6 routing table.                                        */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv6RtInfo      RtInfo;
    UINT1              *pNextHop = NULL;
    UINT2               u2Ipv4Addr = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4RetVal;

    MEMSET (&RtInfo, 0, sizeof RtInfo);

    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Prefixlen = BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = BGP4_RT_MED (pRtProfile);
    RtInfo.i1Proto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.u4RowStatus = IPFWD_DESTROY;
    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    BGP4_IN6_IS_ADDR_V4COMPATIBLE (BGP4_INFO_NEXTHOP
                                   (BGP4_RT_BGP_INFO (pRtProfile)), i4RetVal);
    if (i4RetVal == BGP4_TRUE)
    {
        u2Ipv4Addr = BGP4_6TO4_PREFIX;
        pNextHop = BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile));
        PTR_FETCH4 (u4IpAddr, &(pNextHop[TWELVE]));
        PTR_ASSIGN2 (RtInfo.NextHop.u1_addr, u2Ipv4Addr);
        PTR_ASSIGN4 (&(RtInfo.NextHop.u1_addr[TWO]), u4IpAddr);
    }

    i4RetVal = NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &RtInfo);
    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDELETING Dest = %s Mask = %s Nexthop = %s \n\t"
                       "From IP FWD Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }
    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDELETED Dest = %s Mask = %s Nexthop = %s \n\t"
                   "From IP FWD Table Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv6RtInfo      RtInfo;
    UINT1              *pNextHop = NULL;
    UINT2               u2Ipv4Addr = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4RetVal = BGP4_FALSE;

    MEMSET (&RtInfo, 0, sizeof (RtInfo));

    MEMCPY (RtInfo.NextHop.u1_addr, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    MEMCPY (RtInfo.Ip6Dst.u1_addr, BGP4_RT_IP_PREFIX (pRtProfile),
            BGP4_IPV6_PREFIX_LEN);

    RtInfo.u1Prefixlen = BGP4_RT_PREFIXLEN (pRtProfile);
    RtInfo.u4Metric = BGP4_RT_MED (pRtProfile);
    RtInfo.i1Proto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.u4RowStatus = ACTIVE;
    Bgp4ProtoIdToRtm6 (&(RtInfo.i1Proto));

    BGP4_IN6_IS_ADDR_V4COMPATIBLE (BGP4_INFO_NEXTHOP
                                   (BGP4_RT_BGP_INFO (pRtProfile)), i4RetVal);
    if (i4RetVal == BGP4_TRUE)
    {
        u2Ipv4Addr = BGP4_6TO4_PREFIX;
        pNextHop = BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile));
        PTR_FETCH4 (u4IpAddr, &(pNextHop[TWELVE]));
        PTR_ASSIGN2 (RtInfo.NextHop.u1_addr, u2Ipv4Addr);
        PTR_ASSIGN4 (&(RtInfo.NextHop.u1_addr[TWO]), u4IpAddr);
    }

    i4RetVal = NetIpv6LeakRoute (NETIPV6_MODIFY_ROUTE, &RtInfo);
    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tMODIFYING Dest = %s Mask = %s Nexthop = %s \n\t"
                       "IN IP FWD Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }
    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tMODIFYING Dest = %s Mask = %s Nexthop = %s \n\t"
                   "IN IP FWD Table SUCCESSFUL\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (AddrPrefix1, AddrPrefix2)                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv6IsOnSameSubnet (tAddrPrefix * pDestAddr, tAddrPrefix * pPeerAddr)
{
    tIp6Addr            DestAddr;
    tIp6Addr            LocalAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&LocalAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NetIpv6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));

    MEMCPY (DestAddr.u1_addr, pDestAddr->au1Address, BGP4_IPV6_PREFIX_LEN);
    MEMCPY (LocalAddr.u1_addr, pPeerAddr->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (FALSE);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;
        if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
        {
            if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                == NETIPV6_SUCCESS)
            {

                for (;;)
                {
                    /* checking whether LocalAddr belongs to any of the Inerfaces */
                    if (MEMCMP (NetIpv6AddrInfo.Ip6Addr.u1_addr,
                                LocalAddr.u1_addr, BGP4_IPV6_PREFIX_LEN) == 0)
                        /*chk Local or Dst */
                    {
                        /* the prefixlen of the matched If */
                        u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
                        /* Matching DestAddr and LocalAddr over the If's Prefixlen */
                        if (Ip6AddrMatch
                            (&DestAddr, &LocalAddr, (INT4) u4PrefixLen) == TRUE)
                        {
                            return (TRUE);
                        }
                        else
                        {
                            return (FALSE);
                        }
                    }
                    if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                              &NetIpv6NextAddrInfo) ==
                        NETIPV6_FAILURE)
                    {
                        break;
                    }
                    MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
        }
        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (FALSE);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv6IsDirectlyConnected (tAddrPrefix * pIpAddress, UINT4 u4Context)
{
    tIp6Addr            DestAddr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NetIpv6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMCPY (DestAddr.u1_addr, pIpAddress->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFwdTableRouteEntryInCxt (u4Context, &DestAddr,
                                           BGP4_MAX_IPV6_PREFIXLEN,
                                           &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FALSE);
    }

    /* Interface index thro which this route is learnt */
    u4Index = NetIpv6RtInfo.u4Index;

    if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
            /* Matching DestAddr and LocalAddr over the If's Prefixlen */
            if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                return (BGP4_TRUE);
            }

            if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                      &NetIpv6NextAddrInfo) == NETIPV6_FAILURE)
            {
                break;
            }
            MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                    sizeof (tNetIpv6AddrInfo));
        }
    }

    return (BGP4_FALSE);
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetNetAddrPrefixLen                               */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv6GetNetAddrPrefixLen (tAddrPrefix * IpAddress)
{
    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT2               u2PrefixLen = 0;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NetIpv6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMCPY (DestAddr.u1_addr, IpAddress->au1Address, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (u2PrefixLen);
    }

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;

        if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
        {
            if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
                == NETIPV6_SUCCESS)
            {

                for (;;)
                {
                    u2PrefixLen = (UINT2) NetIpv6AddrInfo.u4PrefixLength;
                    /* checking whether LocalAddr belongs to any of the Inerfaces */
                    if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                                      (INT4) u2PrefixLen) == TRUE)
                    {
                        /* the prefixlen of the matched If */
                        return (u2PrefixLen);

                    }
                    if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                              &NetIpv6NextAddrInfo) ==
                        NETIPV6_FAILURE)
                    {
                        break;
                    }
                    MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
        }

        if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return 0;
        }
    }
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv6LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv6LocalAddrForRemoteAddr (UINT4 u4Context, tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
    tAddrPrefix         AddrPrefix;
    UINT4               u4RtIfIndx = BGP4_INVALID_IFINDX;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    INT4                i4RetVal;

    Bgp4InitAddrPrefixStruct (&(AddrPrefix), BGP4_INET_AFI_IPV6);
    i4RetVal =
        BgpIp6RtLookup (u4Context, &RemAddr, BGP4_IPV6_PREFIX_LEN,
                        &AddrPrefix, &i4Metric, &u4RtIfIndx, CIDR_LOCAL_ID);
    if (i4RetVal == BGP4_SUCCESS)
    {
        BgpGetIp6IfAddr (((UINT2) u4RtIfIndx), RemAddr.au1Address,
                         pLocalAddr->NetAddr.au1Address);
        Bgp4InitAddrPrefixStruct (&(AddrPrefix), BGP4_INET_AFI_IPV6);
        BgpGetIp6IfPrefixLen (((UINT2) u4RtIfIndx), RemAddr.au1Address,
                              &pLocalAddr->u2PrefixLen);
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name : BgpGetIp6IfAddr                                            */
/* Description   : This routine Gets the local-address of given interface     */
/* Input(s)      : Interface index no., pointer to input address              */
/* Output(s)     : pointer to interface address                               */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
BgpGetIp6IfAddr (UINT2 u2IfIndex, UINT1 *pDestAddr, UINT1 *pLocalAddr)
{
    tNetIpv6AddrInfo    Ipv6LocAddr;
    tNetIpv6AddrInfo    Ipv6NextLocAddr;
    UINT4               u4Index = (UINT4) u2IfIndex;
    UINT4               u4PrefixLen;

    MEMSET (&Ipv6LocAddr, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&Ipv6NextLocAddr, 0, sizeof (tNetIpv6AddrInfo));

    if (NetIpv6GetFirstIfAddr (u4Index, &Ipv6LocAddr) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = Ipv6LocAddr.u4PrefixLength;
            /* checking whether Addr belongs to any of the Local subnet */
            if (Ip6AddrMatch (&Ipv6LocAddr.Ip6Addr, (tIp6Addr *) pDestAddr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                MEMCPY (pLocalAddr, Ipv6LocAddr.Ip6Addr.u1_addr,
                        BGP4_IPV6_PREFIX_LEN);
                return (BGP4_SUCCESS);

            }
            if (NetIpv6GetNextIfAddr (u4Index, &Ipv6LocAddr,
                                      &Ipv6NextLocAddr) == NETIPV6_FAILURE)
            {
                return (BGP4_FAILURE);
            }
            MEMCPY (&Ipv6LocAddr, &Ipv6NextLocAddr, sizeof (tNetIpv6AddrInfo));
        }
    }
    return (BGP4_FAILURE);
}

/******************************************************************************/
/* Function Name : BgpGetIp6IfPrefixLen                                       */
/* Description   : This routine Gets the prefix len of given interface        */
/* Input(s)      : Interface index no., pointer to input address              */
/* Output(s)     : pointer to interface address                               */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
BgpGetIp6IfPrefixLen (UINT2 u2IfIndex, UINT1 *pDestAddr, UINT2 *pu2PrefLen)
{
    tNetIpv6AddrInfo    Ipv6LocAddr;
    tNetIpv6AddrInfo    Ipv6NextLocAddr;
    UINT4               u4Index = (UINT4) u2IfIndex;
    UINT4               u4PrefixLen;

    MEMSET (&Ipv6LocAddr, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&Ipv6NextLocAddr, 0, sizeof (tNetIpv6AddrInfo));

    if (NetIpv6GetFirstIfAddr (u4Index, &Ipv6LocAddr) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = Ipv6LocAddr.u4PrefixLength;
            /* checking whether Addr belongs to any of the Local subnet */
            if (Ip6AddrMatch (&Ipv6LocAddr.Ip6Addr, (tIp6Addr *) pDestAddr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                *pu2PrefLen = (UINT2) u4PrefixLen;
                return (BGP4_SUCCESS);

            }
            if (NetIpv6GetNextIfAddr (u4Index, &Ipv6LocAddr,
                                      &Ipv6NextLocAddr) == NETIPV6_FAILURE)
            {
                return (BGP4_FAILURE);
            }
            MEMCPY (&Ipv6LocAddr, &Ipv6NextLocAddr, sizeof (tNetIpv6AddrInfo));
        }
    }
    return (BGP4_FAILURE);
}

/*****************************************************************************/
/* Function Name : Ip6IsLocalAddr                                             */
/* Description   : Checks if the given address is a local address             */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalAddr (UINT4 u4Context, UINT1 *pu1AddrPrefix, UINT2 *pu2Port)
{
    UNUSED_PARAM (u4Context);
    if (IsIp6LocalAddress (pu1AddrPrefix, pu2Port, BGP4_FALSE) ==
        NETIPV6_SUCCESS)
        return (IP_SUCCESS);
    return (IP_FAILURE);
}

/*****************************************************************************/
/* Function Name : Ip6IsLocalSubnet                                          */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalSubnet (UINT1 *pu1AddrPrefix, UINT2 *pu2Port)
{
    if (IsIp6LocalAddress (pu1AddrPrefix, pu2Port, BGP4_TRUE) ==
        NETIPV6_SUCCESS)
        return (IP_SUCCESS);
    return (IP_FAILURE);
}

/*****************************************************************************/
/* Function Name : IsIp6LocalAddress                                         */
/* Description   : Checks if the given address belongs to any of the         */
/*                 interface or local net.                                   */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
IsIp6LocalAddress (UINT1 *pu1IpAddr, UINT2 *pu2Port, UINT4 u4SubnetFlag)
{
    tIp6Addr            DestAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4Index;
    UINT4               u4CxtId = VCM_DEFAULT_CONTEXT;
    UINT4               u4Context = VCM_DEFAULT_CONTEXT;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NetIpv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NetIpv6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    MEMCPY (DestAddr.u1_addr, pu1IpAddr, BGP4_IPV6_PREFIX_LEN);

    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (NETIPV6_FAILURE);
    }

    u4CxtId = BgpGetContextId ();

    for (;;)
    {
        u4Index = NetIpv6IfInfo.u4IfIndex;

	if ((VcmGetContextIdFromCfaIfIndex (u4Index, &u4Context)
		    == VCM_SUCCESS) && (u4CxtId == u4Context))
	{
	    if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
	    {
		if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
			== NETIPV6_SUCCESS)
		{

		    for (;;)
		    {
			/* checking whether LocalAddr belongs to any of the 
                         * Interfaces */
			if (u4SubnetFlag == BGP4_FALSE)
			{
			    if (MEMCMP (NetIpv6AddrInfo.Ip6Addr.u1_addr,
					DestAddr.u1_addr,
					BGP4_IPV6_PREFIX_LEN) == 0)
			    {
				*pu2Port = (UINT2) NetIpv6IfInfo.u4IpPort;
				return (NETIPV6_SUCCESS);

			    }
			}
			else
			{
			    if (MEMCMP (NetIpv6AddrInfo.Ip6Addr.u1_addr,
					DestAddr.u1_addr,
					NetIpv6AddrInfo.u4PrefixLength) == 0)
			    {
				*pu2Port = (UINT2) NetIpv6IfInfo.u4IpPort;
				return (NETIPV6_SUCCESS);

			    }

			}
			if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
				    &NetIpv6NextAddrInfo) ==
				NETIPV6_FAILURE)
			{
			    break;
			}
			MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
				sizeof (tNetIpv6AddrInfo));
		    }
		}
	    }
	}

	if (NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return (NETIPV6_FAILURE);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4FillLinkLocalAddress                                  */
/* Description   : Fills the link local address of the interface through     */
/*                 which the given peer is reachable.                        */
/* Input         : pPeerentry                                                */
/* Output        : link local address of the peer                            */
/* Returns       : BGP4_SUCCESS / BGP4_FAILURE                               */
/*****************************************************************************/
INT4
Bgp4FillLinkLocalAddress (tBgp4PeerEntry * pPeerentry)
{
    tNetIpv6RtInfo      RtInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIp6Addr            DestAddr;
    tIp6Addr            InAddr;
    tIp6Addr           *pLinkLocalAddr = NULL;
    INT4                i4Ret;
    UINT2               u2Afi;
    UINT4               u4Index;

    MEMSET (&RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (&InAddr, 0, sizeof (tIp6Addr));

    u2Afi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry));

    if (u2Afi == BGP4_INET_AFI_IPV6)
    {
        MEMCPY (DestAddr.u1_addr, BGP4_PEER_REMOTE_ADDR (pPeerentry),
                BGP4_IPV6_PREFIX_LEN);
    }
    else
    {
        MEMCPY (DestAddr.u1_addr, BGP4_PEER_NETWORK_ADDR (pPeerentry),
                BGP4_IPV6_PREFIX_LEN);
    }

    if (MEMCMP (&DestAddr.u1_addr, &InAddr.u1_addr, BGP4_IPV6_PREFIX_LEN) == 0)
    {
        return BGP4_FAILURE;
    }

    i4Ret = NetIpv6GetFwdTableRouteEntryInCxt (BGP4_PEER_CXT_ID (pPeerentry),
                                               &DestAddr,
                                               BGP4_MAX_IPV6_PREFIXLEN,
                                               &RtInfo);

    if (i4Ret == NETIPV6_FAILURE)
    {
        return BGP4_FAILURE;
    }
    u4Index = RtInfo.u4Index;
    if (NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return (BGP4_FAILURE);
    }

    pLinkLocalAddr = &(NetIpv6IfInfo.Ip6Addr);

    MEMCPY (BGP4_PEER_LINK_LOCAL_ADDR (pPeerentry),
            pLinkLocalAddr->u1_addr, BGP4_IPV6_PREFIX_LEN);

    return BGP4_SUCCESS;
}
/**************************************************************************/
/* Function Name : BgpIp6CbRtChgHandler                                   */
/*                                                                        */
/* Description   : This routine gets route chanhe notification from RTM6  */
/* Inputs        : none                                                   */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/

VOID BgpIp6CbRtChgHandler (tNetIpv6HliParams * pIp6HliParams)
{
  switch (pIp6HliParams->u4Command)
   {
    case NETIPV6_ROUTE_CHANGE:
    if(pIp6HliParams->unIpv6HlCmdType.RouteChange.i1Proto != IP6_BGP_PROTOID)
    {
        if(BGP4_GLB_RESTART_STATUS == BGP4_GR_STATUS_NONE)
        {
        BgpIp6HandleRouteChgHandler (&(pIp6HliParams->unIpv6HlCmdType.
                RouteChange));
        }
    }
    break;
    default:
          break;
   }
   return;


}
/**************************************************************************/
/* Function Name : BgpIp6HandleRouteChgHandler                            */
/*                                                                        */
/* Description   : This routine gets route chanhe notification from RTM6  */
/* Inputs        : none                                                   */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/

VOID BgpIp6HandleRouteChgHandler (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
   tNetworkAddr        *pNetworkRt = NULL;
   tNetworkAddr         NetworkAddr;
   tNetworkAddr         NetworkRoute;
   tBgp4QMsg          *pQMsg = NULL;
   UINT4              u4Action = 0;
   UINT4               u4Context = 0;

    MEMSET (&NetworkRoute, 0, sizeof (tNetworkAddr));
    MEMSET (&NetworkAddr, 0, sizeof (tNetworkAddr));

    MEMCPY (NetworkAddr.NetworkAddr.au1Address,pNetIpv6RtInfo->Ip6Dst.u1_addr, BGP4_IPV6_PREFIX_LEN);
    NetworkAddr.NetworkAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
    NetworkAddr.NetworkAddr.u2Afi =  BGP4_INET_AFI_IPV6;
    u4Context = pNetIpv6RtInfo->u4ContextId;

    pNetworkRt = Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr ,&NetworkRoute);
        if (pNetworkRt == NULL)
    {
        /*Network entry is not matched with the incoming route indication from RTM*/
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Inteface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        return;
    }

   Bgp4InitAddrPrefixStruct (& (BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg)), BGP4_INET_AFI_IPV6);
   Bgp4InitAddrPrefixStruct (& (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg)), BGP4_INET_AFI_IPV6);
   if (pNetIpv6RtInfo->u4RowStatus == RTM6_ACTIVE)
   {
       u4Action = BGP4_IMPORT_RT_ADD;
   }
   else if (pNetIpv6RtInfo->u4RowStatus == RTM6_DESTROY)
   {
       u4Action = BGP4_IMPORT_RT_DELETE;
   }

   BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTE_CHG_EVENT;
   BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
   BGP4_INPUTQ_ROUTE_ACTION (pQMsg) = u4Action;
   BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO(pQMsg) = pNetIpv6RtInfo->u1Prefixlen;
   BGP4_INPUTQ_ROUTE_PROTO_ID (pQMsg) = pNetIpv6RtInfo->i1Proto;
   BGP4_INPUTQ_ROUTE_IF_INDEX (pQMsg) = pNetIpv6RtInfo->u4Index;
   BGP4_INPUTQ_ROUTE_METRIC (pQMsg) =  pNetIpv6RtInfo->u4Metric;

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (BGP4_INPUTQ_ROUTE_ADDR_INFO  (pQMsg)),
                   pNetIpv6RtInfo->Ip6Dst.u1_addr, BGP4_IPV6_PREFIX_LEN);

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO(pQMsg)),
                   pNetIpv6RtInfo->NextHop.u1_addr, BGP4_IPV6_PREFIX_LEN);

   if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

#endif
#endif
