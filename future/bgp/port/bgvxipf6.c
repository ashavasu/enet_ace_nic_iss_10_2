/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgvxipf6.c,v 1.3 2009/07/16 10:02:28 prabuc-iss Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGVXIPF6_C
#define BGVXIPF6_C
#include "bgp4com.h"

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name :  BgpIp6RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP6 Fwd Table                                     */
/* Input(s)      :  Route entry that has to be searched. (Ipaddr)            */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pRt)               */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp6RtLookup (tAddrPrefix * IpAddr, UINT2 u2PrefixLen, tAddrPrefix * pRt,
                INT4 *pi4Metric, UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv6*/
/*                 FDB and if present then add the default route to BGP IPv6 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv6GetDefaultRouteFromFDB (VOID)
{
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv6RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv6 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv6RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
}

/*****************************************************************************/
/* Function Name : BGP4RTAddRouteToCommIp6RtTbl                              */
/* Description   : This routine is used to add the route entry to the common */
/*                 routing table (TRIE) for IP Forwarding.                   */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp6RtTbl (tRouteProfile * pRtProfile)
{
}

/*****************************************************************************/
/* Function Name : BGP4RTDeleteRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to delete an entry present in the    */
/*                 TRIE routing table.                                       */
/* Input(s)      : Route entry that has to be deleted - pRtProfile           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp6RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp6RtTbl (tRouteProfile * pRtProfile)
{
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (AddrPrefix1, AddrPrefix2)                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv6IsOnSameSubnet (tAddrPrefix * pDestAddr, tAddrPrefix * pLocalAddr)
{
}

/*****************************************************************************/
/* Function Name : Ip6IsLocalSubnet                                          */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : u4Addr, pu2Port                                           */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalSubnet (UINT1 *pu1AddrPrefix, UINT2 *pu2Port)
{
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv6IsDirectlyConnected (tAddrPrefix * IpAddress)
{
    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv6GetNetAddrPrefixLen                               */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address that needs to be checked (IpAddress)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv6GetNetAddrPrefixLen (tAddrPrefix * IpAddress)
{
    return 0;
}

/*****************************************************************************/
/* Function Name : Ip6IsLocalAddr                                            */
/* Description   : Checks if the given address belongs to any of the         */
/*                 local net.                                                */
/* Input         : IPv6 address , pu2Port                                    */
/* Output        : u2Port of the Interface to which the addr belongs         */
/* Returns       : IP_SUCCESS(0) / IP_FAILURE (-1)                           */
/*****************************************************************************/
INT4
Ip6IsLocalAddr (UINT1 *pAddr, UINT2 *pu2Port)
{
}

/*****************************************************************************/
/* Function Name : BgpGetIp6IfAddr                                           */
/* Description   : This routine Gets the local-address of given interface    */
/* Input(s)      : Interface index no.                                       */
/*                 input address                                             */
/* Output(s)     : None.                                                     */
/* Return(s)     : Interface Address or Zero                                 */
/*****************************************************************************/
INT4
BgpGetIp6IfAddr (UINT2 u2IfIndex, UINT1 *pAddr, UINT1 *pLocalAddr)
{
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv6LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv6LocalAddrForRemoteAddr (tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
}

/******************************************************************************/
/* Function Name : Bgp4FillLinkLocalAddress                                   */
/* Description   : This function fills the link local address associated with */
/*                 the Peer.                                                  */
/* Input(s)      : pPeerentry (Peer entry)                                    */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4FillLinkLocalAddress (tBgp4PeerEntry * pPeerentry)
{
}
#endif /* BGP4_IPV6_WANTED */

#endif /* BGVXIPF6_C */
