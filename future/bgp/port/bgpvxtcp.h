/** $Id: bgpvxtcp.h,v 1.4 2013/06/07 13:27:26 siva Exp $ */
#ifndef BGPVXTCP_H
#define BGPVXTCP_H

#define  TCP_AO_NOMKT_MCH       23
#define  TCP_AO_ICMP_ACC        24
/*This structure tTcpAoMktAddr is dummy. Currently not used in LINUX*/
typedef struct
{
    UINT1 u1SndKeyId;                             /* Send key id in TCP-AO option */
    UINT1 u1RcvKeyId;                             /* Receive key id in TCP-AO option */
    UINT1 u1Algo;                                 /* Digest calculation algorithm HMAC-SHA-1-96(1) / AES 128(2) */
    UINT1 u1KeyLen;                               /* Master key length in bytes */
    UINT1 u1TcpOptIgn;
    UINT1 au1Padding[3];
    UINT1 au1Key[TCP_MD5SIG_MAXKEYLEN];           /* Master authentication key */
}tTcpAoMktAddr;

typedef struct
{
    UINT1 u1IcmpAccpt;
    UINT1 u1NoMktMchPckDsc;
    UINT2 u2Padding;
}tTcpAoNeighCfg;

/* Prototype definitions */
INT1   Bgp4TcphGetSockOpt (INT4 );
INT4   Bgp4TcphSetSocketOpt (INT4, UINT4, UINT4);
INT4   Bgp4TcphMD5AuthOptSet (tBgp4PeerEntry *, INT4);
INT4   Bgp4TcphCloseConnection (tBgp4PeerEntry *);
INT4   Bgp4TcphCloseListenPort (UINT2);
INT4   Bgp4TcphAuthOptionMktSet(tBgp4PeerEntry *, tTcpAoMktAddr *);
INT4   Bgp4TcpAoGetMktInUse(UINT4 , tAddrPrefix , INT4 * );
INT4   Bgp4TcpAoNeighCfgSet (tBgp4PeerEntry *, tTcpAoNeighCfg  *, 
                             INT4);
INT4   Bgp4TcpAoMktSet(tBgp4PeerEntry *, tTcpAoAuthMKT *);
INT4   Bgp4TcpAoIcmpCfgSet (tBgp4PeerEntry *, INT1);
INT4   Bgp4TcpAoPktDiscCfg (tBgp4PeerEntry *, INT1);
INT4   Bgp4TcpAoGetAuthStatus (tBgp4PeerEntry *, INT4 *);

#ifdef BGP_TCP6_WANTED

/* Macros for SLI_WANTED */
#define  SOCKET(a,b,c)          socket(a,b,c)
#define  CONNECT(a,b,c)         connect(a,(struct sockaddr *)b,c)
#define  BIND(a,b,c)            bind(a, (struct sockaddr *)b, c)
#define  LISTEN(a,b)            listen(a,b)
#define  CLOSE(a)               close(a)
#define  BGP4_SET_SOCK_NON_BLK(pSock,OptName,Arg)  ioctl(pSock,OptName,(INT4)Arg)
#define  SEND(a,b,c,d)          send(a, (VOID *)b, c,MSG_PUSH) 
#define  RECV(a,b,c,d)          recv(a, (INT1 *)b,c,d)
#define  ACCEPT(a,b,c)          accept((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETSOCKNAME(a,b,c)     getsockname((INT4)a, (struct sockaddr *)b, (INT4 *)c)
#define  GETPEERNAME(a,b,c)     getpeername((INT4)a,(struct sockaddr *)b, (INT4 *)c)
#define  SETSOCKOPT(a,b,c,d,e)  (((i4VxwTmp = setsockopt((INT4)a,(INT4)b,(INT4)c,(INT1 *)(d), \
                                (INT4)e)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  GETSOCKOPT(a,b,c,d,e)  (((i4VxwTmp = getsockopt((INT4)a,(INT4)b,(INT4)c,(INT1 *)(d), \
                                (int *)e)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  BGP4_FCNTL             fcntl
#define  BGP_SOCK_SELECT        select
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  BGP_NODELAY            TCP_NODELAY 
#define  BGP_EINPROGRESS        EINPROGRESS
#define  BGP_EAGAIN             EAGAIN
#define  BGP_EISCONN            EISCONN
#define  BGP_ENETUNREACH        ENETUNREACH
#define  BGP_ECONNREFUSED       ECONNREFUSED
#define  BGP_EALREADY           EALREADY
#define  BGP_EWOULDBLOCK        EWOULDBLOCK
#define  BGP_EINTR              EINTR

/* Temporary - TCP MD5 Auth Option */
#define  TCP_SIGNATURE_ENABLE     1
 
#ifndef RRD_WANTED
#define  MAX_ROUTING_PROTOCOLS  16    
#define  ROUTING_PROTOCOLS_MIN_INDEX 1
#endif

INT4   Bgp4Tcphv6OpenConnection (tBgp4PeerEntry *);
INT4   Bgp4Tcphv6CheckIncomingConn (VOID);
INT4   Bgp4Tcphv6FillAddresses (tBgp4PeerEntry *);
INT4   Bgp4Tcphv6OpenListenPort (VOID);

#endif /* BGP_TCP6_WANTED */

#ifdef BGP_TCP4_WANTED

/* Macros used for packing the structures*/
#define ATTR_PACK               __attribute__ ((packed))

/* Macros used for VxWorks socket calls */
#define  SOCKET(a,b,c)          (((i4VxwTmp = socket((INT4)a,(INT4)b,(INT4)c)) < 0)\
                                                    ? BGP4_FAILURE : i4VxwTmp )
#define  CONNECT(a,b,c)         (((i4VxwTmp = connect((int)a,(struct sockaddr *)b,(int)c)) == \
                                VXWORKS_FAILURE) ? BGP4_FAILURE : i4VxwTmp )
#define  BIND(a,b,c)            (((i4VxwTmp = bind((INT4)a, (struct  sockaddr *)b, (INT4)c)) \
                                < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  LISTEN( a,b)           listen((INT4)a, (INT4)b)
#define  CLOSE(a)               close(a)
#define  SEND(a,b,c,d)          (((i4VxwTmp = send((INT4)a,(UINT1 *)b,(INT4)c,(INT4)d)) \
                                < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  RECV(a,b,c,d)          recv((INT4)a, (UINT1 *)b, (INT4)c, (INT4)d)
#define  ACCEPT(a,b,c)          (((i4VxwTmp = accept((int)a, (struct sockaddr *)b, (int *)c)) \
                                < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  GETSOCKNAME(a,b,c)    (((i4VxwTmp = getsockname((INT4)a, (struct sockaddr *)b, \
                                (int *)c)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  GETPEERNAME(a,b,c)     (((i4VxwTmp = getpeername((INT4)a, (struct sockaddr *)b, \
                                (int *)c)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  GETSOCKOPT(a,b,c,d,e)  (((i4VxwTmp = getsockopt((INT4)a,(INT4)b,(INT4)c,(INT1 *)(d), \
                                (int *)e)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )
#define  SETSOCKOPT(a,b,c,d,e)  (((i4VxwTmp = setsockopt((INT4)a,(INT4)b,(INT4)c,(INT1 *)(d), \
                                (INT4)e)) < 0 ) ? BGP4_FAILURE : i4VxwTmp )

#define  BGP4_SET_SOCK_NON_BLK(pSock,OptName,Arg)  ioctl(pSock,OptName,(INT4)Arg)
#define  BGP_SOCK_SELECT        select
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  MAX_ELEMENTS_IN_FDSET  500
#define  BGP_EISCONN            EISCONN
#define  BGP_ENETUNREACH        ENETUNREACH
#define  BGP_ECONNREFUSED       ECONNREFUSED
#define  BGP_EALREADY           EALREADY
#define  BGP_EWOULDBLOCK        EWOULDBLOCK
#define  BGP_EAGAIN             EAGAIN
#define  BGP_EINTR              EINTR
#define  BGP_EINPROGRESS        EINPROGRESS

/* Temporary - TCP MD5 Auth Option */
#define  TCP_SIGNATURE_ENABLE     1
 
#ifndef RRD_WANTED
#define  MAX_ROUTING_PROTOCOLS  16    
#define  ROUTING_PROTOCOLS_MIN_INDEX 1
#endif

/* Prototype definitions */
INT4  Bgp4Tcphv4OpenConnection (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4CheckIncomingConn (VOID);
INT4  Bgp4Tcphv4FillAddresses (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4OpenListenPort (VOID);

#endif

#endif /* BGPVXTCP_H */
