/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgfsrrd4.c,v 1.28 2016/12/27 12:35:54 siva Exp $
 *
 * Description: These routines interface with the underlying SLI 
 *              layer.
 *
 *******************************************************************/

#ifndef BGFSRRD4_C
#define BGFSRRD4_C
#include "bgp4com.h"

#ifdef RRD_WANTED
UINT2               gu2RRDAsNum = 0;
#ifdef SNMP_2_WANTED
extern UINT4        FsMIBgp4Identifier[12];
#endif

/*****************************************************************************/
/* Function        : Bgp4RegAckMsg                                           */
/* Description     : This procedure processes the reg ack message from RTM   */
/*                   and updates the BGP global structure.                   */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/* Output          : None                                                    */
/* Returns         : VOID                                                    */
/*****************************************************************************/

VOID
Bgp4RegAckMsg (tBgp4QMsg * pRtmMsg)
{

#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    UINT4               u4CxtId = BGP4_INPUTQ_CXT (pRtmMsg);

    gu2RRDAsNum = pRtmMsg->QMsg.RtmRegnPtr.RtmRegnMsg.u2ASNumber;
    if (BGP4_LOCAL_ADMIN_STATUS (u4CxtId) == BGP4_ADMIN_UP)
    {
        return;
    }

    /* Store the Local Router Id, Local AS No. */
    if (BGP4_LOCAL_AS_NO (u4CxtId) == BGP4_INV_AS)
    {
        BGP4_LOCAL_AS_NO (u4CxtId) = gu2RRDAsNum;
        if (u4CxtId == BGP4_DFLT_VRFID)
        {
            BGP4_STATUS (u4CxtId) = BGP4_TRUE;
        }
        if (BGP4_GLB_LOCAL_AS_NO == BGP4_INV_AS)
        {
            /* When the global local AS is not yet configured, then taking the
             * first RTM ACK AS no as the global local AS */
            BGP4_GLB_LOCAL_AS_NO = gu2RRDAsNum;
        }
    }

#ifdef ISS_WANTED
    /*  Donot overwrite restored router-id with router-id from RTM */
    if (MSR_TRUE != MsrIsMibRestoreInProgress ())
    {
        BGP4_LOCAL_BGP_ID (u4CxtId) =
            pRtmMsg->QMsg.RtmRegnPtr.RtmRegnMsg.u4RouterId;
        BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4CxtId) = BGP4_LOCAL_BGP_ID_DYNAMIC;
    }
#else
    BGP4_LOCAL_BGP_ID (u4CxtId) =
        pRtmMsg->QMsg.RtmRegnPtr.RtmRegnMsg.u4RouterId;
    BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4CxtId) = BGP4_LOCAL_BGP_ID_DYNAMIC;
#endif /* ISS_WANTED */

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIBgp4Identifier;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIBgp4Identifier) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = BgpLock;
    SnmpNotifyInfo.pUnLockPointer = BgpUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", u4CxtId,
                      BGP4_LOCAL_BGP_ID (u4CxtId)));
#endif
    return;
}

/* ENVIRONMENT SPECIFIC CALLS */
/*****************************************************************************/
/* Function        : Bgp4RtmProcessRtChange                                  */
/* Description     : This procedure scans the Route Change Notification      */
/*                   message from RTM and sends it to the BGP task.          */
/* Input           : pRespInfo -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to the RTM message header             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/

INT1
Bgp4RtmProcessRtChange (tNetIpv4RtInfo * pNetIpRtInfo, tRtmMsgHdr * pRtmHdr)
{
    tNetAddress         ImportAddress;
    tAddrPrefix         NexthopAddress;
    UINT1               u1CommCount = 0;                
    UINT1               u1MaskLen = 0;

    if(BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return RTM_SUCCESS;
    }        

    u1MaskLen = Bgp4GetSubnetmasklen (pNetIpRtInfo->u4DestMask);
    Bgp4InitNetAddressStruct (&(ImportAddress), BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(NexthopAddress), BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (ImportAddress)),
                  pNetIpRtInfo->u4DestNet);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) = u1MaskLen;
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NexthopAddress),
                  pNetIpRtInfo->u4NextHop);

    if (BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return RTM_SUCCESS;   
    }

    if (pNetIpRtInfo->u4RowStatus == RTM_DESTROY)
    {
        /* Checking for  the 4th Bit
         * If it set then route has to be  deleted
         */
        Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                       pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                       BGP4_IMPORT_RT_DELETE, pNetIpRtInfo->u4RouteTag,
                       pNetIpRtInfo->i4Metric1, pNetIpRtInfo->u4RtIfIndx,
                       pNetIpRtInfo->u1MetricType, 0, NULL, 
                       pNetIpRtInfo->u4SetFlag);
    }
    else
    {
	    if (pNetIpRtInfo->u4RowStatus == RTM_ACTIVE)
	    {
            if (pNetIpRtInfo->pCommunity != NULL)
            {
		               for(u1CommCount = 0 ; u1CommCount < 
                              pNetIpRtInfo->u1CommunityCnt ; u1CommCount++)
		    {
                    if (pNetIpRtInfo->pCommunity->
                              au4Community[u1CommCount] == RMAP_COMM_INTERNET)
			    {
                        pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 0;
                    }
			                 pNetIpRtInfo->pCommunity->
                            au4Community[u1CommCount] =
                            OSIX_HTONL (pNetIpRtInfo->pCommunity->
                                        au4Community[u1CommCount]);
			    }
		    }
          
		    Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                           pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                           BGP4_IMPORT_RT_ADD, pNetIpRtInfo->u4RouteTag,
                           pNetIpRtInfo->i4Metric1, pNetIpRtInfo->u4RtIfIndx,
                           pNetIpRtInfo->u1MetricType, 
                      pNetIpRtInfo->u1CommunityCnt,
                      pNetIpRtInfo->pCommunity->au4Community,
                      pNetIpRtInfo->u4SetFlag);
        }
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/* Function        : Bgp4RecvMsgFromRtm                                      */
/* Description     : This is a callback function that fills the RTM message  */
/*                   required tBgp4QMsg and posts it in the BGP INPUT Q      */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
Bgp4RecvMsgFromRtm (tRtmRespInfo * pRespInfo, tRtmMsgHdr * pRtmHdr)
{
    tBgp4QMsg          *pQMsg = NULL;

    if ((pRtmHdr == NULL) || (pRespInfo == NULL))
    {
        return RTM_FAILURE;
    }

    switch (pRtmHdr->u1MessageType)
    {
        case RTM_REGISTRATION_ACK_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM_REG_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRegnAck->u4ContextId;
            MEMCPY (&(pQMsg->QMsg.RtmRegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtmMsgHdr));
            MEMCPY ((UINT1 *) &(pQMsg->QMsg.RtmRegnPtr.RtmRegnMsg),
                    pRespInfo->pRegnAck, sizeof (tRtmRegnAckMsg));
            if (Bgp4Enqueue (pQMsg) == BGP4_FAILURE)
            {
                return RTM_FAILURE;
            }
            return RTM_SUCCESS;

        case RTM_ROUTE_UPDATE_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM_ROUTE_UPDATE_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            /*BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRegnAck->u4ContextId;*/
            BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRtInfo->u4ContextId;
            MEMCPY (&(pQMsg->QMsg.RtmRegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtmMsgHdr));
            MEMCPY (&(pQMsg->QMsg.RtmRegnPtr.NetIpv4RtInfo), pRespInfo->pRtInfo, sizeof (tNetIpv4RtInfo));

            if (Bgp4Enqueue (pQMsg) == BGP4_FAILURE)
            {
                return RTM_FAILURE;
            }
            return RTM_SUCCESS;       

        case RTM_ROUTE_CHANGE_NOTIFY_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM_ROUTE_CHANGE_NOTIFY_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
	    BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRtInfo->u4ContextId;
            MEMCPY (&(pQMsg->QMsg.RtmRegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtmMsgHdr));
            MEMCPY (&(pQMsg->QMsg.RtmRegnPtr.NetIpv4RtInfo), pRespInfo->pRtInfo, sizeof (tNetIpv4RtInfo));
            if (Bgp4Enqueue (pQMsg) == BGP4_FAILURE)
            {
                return RTM_FAILURE;
            }
            return RTM_SUCCESS;
        default:
            return RTM_FAILURE;
    }

}

/*****************************************************************************/
/* Function        : Bgp4RtmProcessUpdate                                    */
/* Description     : This procedure scans the Route Update message from RTM  */
/*                   and sends the update message to BGP task                */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
Bgp4RtmProcessUpdate (tNetIpv4RtInfo * pNetIpRtInfo, tRtmMsgHdr * pRtmHdr)
{
    tNetAddress         ImportAddress;
    tAddrPrefix         NexthopAddress;
    UINT4               u4ProtoId = 0;
    UINT1               u1CommCount = 0;                
    UINT1               u1MaskLen;


    if(BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return RTM_SUCCESS;
    }        

    u1MaskLen = Bgp4GetSubnetmasklen (pNetIpRtInfo->u4DestMask);

    Bgp4InitNetAddressStruct (&(ImportAddress), BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(NexthopAddress), BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (ImportAddress)),
                  pNetIpRtInfo->u4DestNet);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) = u1MaskLen;
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NexthopAddress),
                  pNetIpRtInfo->u4NextHop);
    if (pRtmHdr->RegnId.u2ProtoId == BGP4_OSPF_ID)
    {
        u4ProtoId = BGP4_OSPF_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_STATIC_ID)
    {
        u4ProtoId = BGP4_STATIC_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_LOCAL_ID)
    {
        u4ProtoId = BGP4_DIRECT_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_RIP_ID)
    {
        u4ProtoId = BGP4_RIP_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_ISIS_ID)
    {
        u4ProtoId = BGP4_ISIS_METRIC;
    }

    if ((u4ProtoId > 0)
            && ((BGP4_RRD_METRIC_MASK (pNetIpRtInfo->u4ContextId) & u4ProtoId) == u4ProtoId)
            && (pNetIpRtInfo->u4SetFlag != METRIC_SET_RMAP ))
    {
        pNetIpRtInfo->i4Metric1 =
            BGP4_RRD_METRIC_VAL (pNetIpRtInfo->u4ContextId, u4ProtoId);
    }
    if (pNetIpRtInfo->pCommunity != NULL)
    {
    for(u1CommCount = 0 ; u1CommCount < pNetIpRtInfo->u1CommunityCnt ; u1CommCount++)
    {
            if (pNetIpRtInfo->pCommunity->au4Community[u1CommCount] == RMAP_COMM_INTERNET)
	    {
                pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 0;
            }
            pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 
                OSIX_HTONL (pNetIpRtInfo->pCommunity->au4Community[u1CommCount]);
	    }
    }
    Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                   pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                   BGP4_IMPORT_RT_ADD, pNetIpRtInfo->u4RouteTag,
                   pNetIpRtInfo->i4Metric1, pNetIpRtInfo->u4RtIfIndx,
                   pNetIpRtInfo->u1MetricType, pNetIpRtInfo->u1CommunityCnt,
                   pNetIpRtInfo->pCommunity->au4Community,pNetIpRtInfo->u4SetFlag);
    return RTM_SUCCESS;
}

/****************************************************************************
 Function    :  SendingMessageToRRDQueue 
 Description :  This function is called to send messages to the RTM input Q
 Input       :  -The bit mask correspoding to protocols with which RRD is enabled
                -The type of the message to be sent 
                -The route map name
 Output      :  none
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
SendingMessageToRRDQueue (UINT4 u4ContextId, UINT4 u4RRDSrcProtoMaskEnable,
                          UINT1 u1MessageType, UINT1 *pu1RMapName,
                          UINT2 u2MatchFlag)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tOsixMsg           *pBuf = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
    UINT2               u2Bitmask = 0;
    UINT4               u4Len = 0;

    /* prepare intermediate buf for route-map, *
     * ensure no garbage after map name        */
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN);
    if (NULL != pu1RMapName)
    {
        u4Len = (STRLEN (pu1RMapName) < sizeof (au1RMapName) ?
                 STRLEN (pu1RMapName) : sizeof (au1RMapName) - 1);
        STRNCPY (au1RMapName, pu1RMapName, u4Len);
        au1RMapName[u4Len] = '\0';
    }

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain ((sizeof (tRtmMsgHdr) +
                                        sizeof (UINT2) + RMAP_MAX_NAME_LEN),
                                       0)) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue() : Allocate Msg Buf FAILED\n");
        return SNMP_FAILURE;
    }
    pRtmMsgHdr = (tRtmMsgHdr *) CRU_BUF_Get_ModuleData (pBuf);
    pRtmMsgHdr->u1MessageType = u1MessageType;
    if (BGP4_RTM_REG_ID (u4ContextId) == 0)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    pRtmMsgHdr->RegnId.u2ProtoId = (UINT1) BGP4_RTM_REG_ID (u4ContextId);
    pRtmMsgHdr->RegnId.u4ContextId = u4ContextId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT4) + RMAP_MAX_NAME_LEN;

    /* Setting the Particular Protocol Bit and Sending the 
     * Message to the RTM queue
     */
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_STATIC) == BGP4_IMPORT_STATIC)
    {
        u2Bitmask |= RTM_STATIC_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_DIRECT) == BGP4_IMPORT_DIRECT)
    {
        u2Bitmask |= RTM_DIRECT_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_RIP) == BGP4_IMPORT_RIP)
    {
        u2Bitmask |= RTM_RIP_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_ISISL1) == BGP4_IMPORT_ISISL1)
    {
        u2Bitmask |= RTM_ISISL1_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_ISISL2) == BGP4_IMPORT_ISISL2)
    {
        u2Bitmask |= RTM_ISISL2_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_OSPF) == BGP4_IMPORT_OSPF)
    {
        u2Bitmask |= RTM_OSPF_MASK;
        u2Bitmask |= u2MatchFlag;
    }
    if ((u1MessageType == RTM_REDISTRIBUTE_DISABLE_MESSAGE) &&
        (u2MatchFlag > 0))
    {
        u2Bitmask |= u2MatchFlag;
    }
    /*put protocol-mask in packet on offset 0 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Bitmask, 0, sizeof (u2Bitmask)) == CRU_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue() : Copying Reg Msg FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /*put route-map in packet on offset 2 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, au1RMapName, sizeof (u2Bitmask),
         RMAP_MAX_NAME_LEN) == CRU_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue() : Copying Reg Msg FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pBuf) != RTM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function        : Bgp4RecvMsgFromRtm6                                      */
/* Description     : This is a callback function that fills the RTM message  */
/*                   required tBgp4QMsg and posts it in the BGP INPUT Q      */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
Bgp4RecvMsgFromRtm6 (tRtm6RespInfo * pRespInfo, tRtm6MsgHdr * pRtmHdr)
{
    tBgp4QMsg          *pQMsg = NULL;

    switch (pRtmHdr->u1MessageType)
    {
        case RTM6_REGISTRATION_ACK_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM6_REG_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRegnAck->u4ContextId;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            MEMCPY (&(pQMsg->QMsg.Rtm6RegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtm6MsgHdr));
            MEMCPY ((UINT1 *) &(pQMsg->QMsg.Rtm6RegnPtr.RtmRegnMsg),
                    pRespInfo->pRegnAck, sizeof (tRtm6RegnAckMsg));
            break;

        case RTM6_ROUTE_UPDATE_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM6_ROUTE_UPDATE_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRegnAck->u4ContextId;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;

            MEMCPY (&(pQMsg->QMsg.Rtm6RegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtm6MsgHdr));
            MEMCPY (&(pQMsg->QMsg.Rtm6RegnPtr.NetIpv6RtInfo), pRespInfo->pRtInfo, sizeof (tNetIpv6RtInfo));
            break;

        case RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE:
            /* Get buffer from mempool */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return RTM_FAILURE;
            }
            pQMsg->u4MsgType = BGP4_RTM6_ROUTE_CHANGE_NOTIFY_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = pRespInfo->pRegnAck->u4ContextId;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;

            MEMCPY (&(pQMsg->QMsg.Rtm6RegnPtr.RtmHdr), pRtmHdr,
                    sizeof (tRtm6MsgHdr));
            MEMCPY (&(pQMsg->QMsg.Rtm6RegnPtr.NetIpv6RtInfo), pRespInfo->pRtInfo, sizeof (tNetIpv6RtInfo));
            break;

        default:
            break;
    }
    /* Post the pQMsg in the BGP input Q */
    if (Bgp4Enqueue (pQMsg) == BGP4_FAILURE)
    {
        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*****************************************************************************/
/* Function        : Bgp4Rtm6ProcessUpdate                                    */
/* Description     : This procedure scans the Route Update message from RTM  */
/*                   and sends the update message to BGP task                */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
Bgp4Rtm6ProcessUpdate (tNetIpv6RtInfo     *pNetIpRtInfo, tRtm6MsgHdr * pRtmHdr)
{
    tNetAddress         ImportAddress;
    tAddrPrefix         NexthopAddress;
    tIp6Addr            IPAddr;
    tIp6Addr            IPAddr1;
    UINT1               u1CommCount = 0;                
    INT4                i4Res;
    UINT4               u4ProtoId = 0;

    if(BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return RTM_SUCCESS;
    }        

    Bgp4InitNetAddressStruct (&(ImportAddress), BGP4_INET_AFI_IPV6,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(NexthopAddress), BGP4_INET_AFI_IPV6);

    MEMCPY (&IPAddr1.u1_addr, pNetIpRtInfo->Ip6Dst.u1_addr,
            BGP4_IPV6_PREFIX_LEN);
    Ip6CopyAddrBits (&IPAddr, &IPAddr1, pNetIpRtInfo->u1Prefixlen);
    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (ImportAddress)),
            &IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) =
        pNetIpRtInfo->u1Prefixlen;

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NexthopAddress),
            pNetIpRtInfo->NextHop.u1_addr, BGP4_IPV6_PREFIX_LEN);

    if (pRtmHdr->RegnId.u2ProtoId == BGP4_OSPF_ID)
    {
        u4ProtoId = BGP4_OSPF_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_STATIC_ID)
    {
        u4ProtoId = BGP4_STATIC_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_LOCAL_ID)
    {
        u4ProtoId = BGP4_DIRECT_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_RIP_ID)
    {
        u4ProtoId = BGP4_RIP_METRIC;
    }
    else if (pRtmHdr->RegnId.u2ProtoId == BGP4_ISIS_ID)
    {
        u4ProtoId = BGP4_ISIS_METRIC;
    }

    if ((u4ProtoId > 0)
            && ((BGP4_RRD_METRIC_MASK (pNetIpRtInfo->u4ContextId) & u4ProtoId) == u4ProtoId)
            && (pNetIpRtInfo->u4SetFlag != METRIC_SET_RMAP ))
    {
        pNetIpRtInfo->u4Metric =
            BGP4_RRD_METRIC_VAL (pNetIpRtInfo->u4ContextId, u4ProtoId);
    }

    for(u1CommCount = 0 ; u1CommCount < pNetIpRtInfo->u1CommCount ; u1CommCount++)
    {
        if (pNetIpRtInfo->pCommunity->au4Community[u1CommCount] == RMAP_COMM_INTERNET)
	    {
            pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 0;
	    }
        pNetIpRtInfo->pCommunity->
            au4Community[u1CommCount] = 
             OSIX_HTONL (pNetIpRtInfo->pCommunity->au4Community[u1CommCount]);

    }
    i4Res =
        Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                       pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                       BGP4_IMPORT_RT_ADD, pNetIpRtInfo->u4RouteTag,
                       pNetIpRtInfo->u4Metric, pNetIpRtInfo->u4Index,
                       pNetIpRtInfo->u1MetricType,pNetIpRtInfo->u1CommCount,
                       pNetIpRtInfo->pCommunity->au4Community,
                       pNetIpRtInfo->u4SetFlag);

    if (i4Res != BGP4_SUCCESS)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "Bgp4Rtm6ProcessUpdate FAIL dst=%s\n",
                       Ip6PrintNtop (&(pNetIpRtInfo->Ip6Dst)));
        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*****************************************************************************/
/* Function        : Bgp4Rtm6ProcessRtChange                                  */
/* Description     : This procedure scans the Route Change Notification      */
/*                   message from RTM and sends it to the BGP task.          */
/* Input           : pRespInfo -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to the RTM message header             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/

INT1
Bgp4Rtm6ProcessRtChange (tNetIpv6RtInfo *pNetIpRtInfo, tRtm6MsgHdr * pRtmHdr)
{
    tNetAddress         ImportAddress;
    tAddrPrefix         NexthopAddress;
    tIp6Addr            IPAddr;
    tIp6Addr            IPAddr1;
    UINT1               u1CommCount = 0;                
    INT4                i4Res = 0;
    UINT4               u4ProtoId = 0;

    if(BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return RTM_SUCCESS;
    }        

    Bgp4InitNetAddressStruct (&(ImportAddress), BGP4_INET_AFI_IPV6,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(NexthopAddress), BGP4_INET_AFI_IPV6);

    MEMCPY (&IPAddr1.u1_addr, pNetIpRtInfo->Ip6Dst.u1_addr,
            BGP4_IPV6_PREFIX_LEN);
    Ip6CopyAddrBits (&IPAddr, &IPAddr1, pNetIpRtInfo->u1Prefixlen);
    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (ImportAddress)),
            &IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) =
        pNetIpRtInfo->u1Prefixlen;

    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NexthopAddress),
            pNetIpRtInfo->NextHop.u1_addr, BGP4_IPV6_PREFIX_LEN);

    if (pNetIpRtInfo->u4RowStatus == RTM6_DESTROY)
    {
        /* Checking for  the 4th Bit
         * If it set then route has to be  deleted
         */
        i4Res = Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                               pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                               BGP4_IMPORT_RT_DELETE, pNetIpRtInfo->u4RouteTag,
                               pNetIpRtInfo->u4Metric, pNetIpRtInfo->u4Index,
                               pNetIpRtInfo->u1MetricType, 0, NULL, pNetIpRtInfo->u4SetFlag);

        if (i4Res != BGP4_SUCCESS)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                           BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "Bgp4Rtm6ProcessRtChange FAIL dst=%s\n",
                           Ip6PrintNtop (&(pNetIpRtInfo->Ip6Dst)));
            return RTM_FAILURE;
        }
    }
    else
    {
        if (pNetIpRtInfo->u4RowStatus == RTM6_ACTIVE)
        {
            if (pRtmHdr->RegnId.u2ProtoId == BGP4_OSPF_ID)
            {
                u4ProtoId = BGP4_OSPF_METRIC;
            }
            else if (pRtmHdr->RegnId.u2ProtoId == BGP4_STATIC_ID)
            {
                u4ProtoId = BGP4_STATIC_METRIC;
            }
            else if (pRtmHdr->RegnId.u2ProtoId == BGP4_LOCAL_ID)
            {
                u4ProtoId = BGP4_DIRECT_METRIC;
            }
            else if (pRtmHdr->RegnId.u2ProtoId == BGP4_RIP_ID)
            {
                u4ProtoId = BGP4_RIP_METRIC;
            }
            else if (pRtmHdr->RegnId.u2ProtoId == BGP4_ISIS_ID)
            {
                u4ProtoId = BGP4_ISIS_METRIC;
            }

	           if ((u4ProtoId > 0)
		           && ((BGP4_RRD_METRIC_MASK (pNetIpRtInfo->u4ContextId) & u4ProtoId) == u4ProtoId))
            {
                pNetIpRtInfo->u4Metric =
                           BGP4_RRD_METRIC_VAL (pNetIpRtInfo->u4ContextId, u4ProtoId);
            }
		for(u1CommCount = 0 ; u1CommCount < pNetIpRtInfo->u1CommCount ; u1CommCount++)
		{
			if (pNetIpRtInfo->pCommunity->au4Community[u1CommCount] == RMAP_COMM_INTERNET)
			{
				pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 0;
			}
                pNetIpRtInfo->pCommunity->au4Community[u1CommCount] = 
                       OSIX_HTONL (pNetIpRtInfo->pCommunity->au4Community[u1CommCount]);
		}
            i4Res = Bgp4RouteLeak (pNetIpRtInfo->u4ContextId, ImportAddress,
                                   pRtmHdr->RegnId.u2ProtoId, NexthopAddress,
                                   BGP4_IMPORT_RT_ADD, pNetIpRtInfo->u4RouteTag,
                                   pNetIpRtInfo->u4Metric,
                                   pNetIpRtInfo->u4Index,
                    pNetIpRtInfo->u1MetricType, 
                    pNetIpRtInfo->u1CommCount,
                    pNetIpRtInfo->pCommunity->au4Community,
                    pNetIpRtInfo->u4SetFlag);

            if (i4Res != BGP4_SUCCESS)
            {
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                               BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "Bgp4Rtm6ProcessRtChange FAIL dst=%s\n",
                               Ip6PrintNtop (&(pNetIpRtInfo->Ip6Dst)));
                return RTM_FAILURE;
            }

        }
    }

    return RTM_SUCCESS;
}

/****************************************************************************
 Function    :  SendingMessageToRRDQueue6 
 Description :  This function is called to send messages to the RTM6 input Q
 Input       :  -The bit mask correspoding to protocols with which RRD is enabled
                -The type of the message to be sent 
                -The route map name
 Output      :  none
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
SendingMessageToRRDQueue6 (UINT4 u4ContextId, UINT4 u4RRDSrcProtoMaskEnable,
                           UINT1 u1MessageType, UINT1 *pu1RMapName,
                           UINT2 u2MatchFlag)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tOsixMsg           *pBuf = NULL;
    tRtm6MsgHdr        *pRtmMsgHdr = NULL;
    UINT2               u2Bitmask = 0;

    /* prepare intermediate buf for route-map, *
     * ensure no garbage after map name        */
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    if (NULL != pu1RMapName)
    {
        STRCPY (au1RMapName, pu1RMapName);
    }

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain ((sizeof (tRtm6MsgHdr) +
                                        sizeof (UINT2) + RMAP_MAX_NAME_LEN),
                                       0)) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue6() : Allocate Msg Buf FAILED\n");
        return SNMP_FAILURE;
    }
    pRtmMsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pBuf);
    pRtmMsgHdr->u1MessageType = u1MessageType;
    pRtmMsgHdr->RegnId.u2ProtoId = BGP6_ID;
    pRtmMsgHdr->RegnId.u4ContextId = u4ContextId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2);

    /* Setting the Particular Protocol Bit and Sending the 
     * Message to the RTM queue
     */
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_STATIC) == BGP4_IMPORT_STATIC)
    {
        u2Bitmask |= RTM6_STATIC_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_DIRECT) == BGP4_IMPORT_DIRECT)
    {
        u2Bitmask |= RTM6_DIRECT_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_RIP) == BGP4_IMPORT_RIP)
    {
        u2Bitmask |= RTM6_RIP_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_ISISL1) == BGP4_IMPORT_ISISL1)
    {
        u2Bitmask |= RTM6_ISISL1_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_ISISL2) == BGP4_IMPORT_ISISL2)
    {
        u2Bitmask |= RTM6_ISISL2_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_OSPF) == BGP4_IMPORT_OSPF)
    {
        u2Bitmask |= RTM6_OSPF_MASK;
        u2Bitmask |= u2MatchFlag;
    }
    if ((u1MessageType == RTM6_REDISTRIBUTE_DISABLE_MESSAGE) &&
        (u2MatchFlag > 0))
    {
        u2Bitmask |= u2MatchFlag;
    }

    /*put protocol-mask in packet on offset 0 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Bitmask, 0, sizeof (u2Bitmask)) == CRU_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue() : Copying Reg Msg FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /*put route-map in packet on offset 2 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, au1RMapName, sizeof (u2Bitmask),
         RMAP_MAX_NAME_LEN) == CRU_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tSendingMessageToRRDQueue() : Copying Reg Msg FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /* Send the buffer to RTM6 */
    if (RpsEnqueuePktToRtm6 (pBuf) != RTM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4ProtoIdToRtm6 
 Description :  Convert protocol-id while installing IPv6 routes 
                from BGP into RTM6
 Input       :  pi1ProtoId - protocol id enumerated as in BGP (RTM4)
 Output      :  pi1ProtoId - protocol id enumerated as in RTM6
****************************************************************************/
VOID
Bgp4ProtoIdToRtm6 (INT1 *pi1ProtoId)
{
    if (*pi1ProtoId == BGP_ID)
    {
        *pi1ProtoId = IP6_BGP_PROTOID;
    }
}

/****************************************************************************
 Function    :  Bgp4ProtoIdFromRtm6 
 Description :  Convert protocol-id while redistributing IPv6 routes 
                from RTM6 into BGP
 Input       :  pu2ProtoId - protocol id enumerated as in RTM6
 Output      :  pu2ProtoId - protocol id enumerated as in BGP (RTM4)
****************************************************************************/
VOID
Bgp4ProtoIdFromRtm6 (UINT2 *pu2ProtoId)
{
    /* inside BGP all protocols are enumerated as in RTM4 */
    switch (*pu2ProtoId)
    {
        case IP6_LOCAL_PROTOID:
            *pu2ProtoId = BGP4_LOCAL_ID;
            break;

        case IP6_NETMGMT_PROTOID:
            *pu2ProtoId = BGP4_STATIC_ID;
            break;

        case IP6_RIP_PROTOID:
            *pu2ProtoId = BGP4_RIP_ID;
            break;

        case IP6_OSPF_PROTOID:
            *pu2ProtoId = BGP4_OSPF_ID;
            break;

        case IP6_ISIS_PROTOID:
            *pu2ProtoId = BGP4_ISIS_ID;
            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "Bgp4ProtoIdFromRtm6 -- UNKNOWN PROTO ID!!!\n");
    }
}

#endif /* BGP4_IPV6_WANTED */
#endif /* RRD_WANTED */
#endif /* BGFSRRD4_C */
