/** $Id: bgplntcp.h,v 1.9 2013/07/03 12:39:59 siva Exp $ */
#include "bgp4com.h"

#include "fssocket.h"

#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <syslog.h>

#define  TCP_AO_NOMKT_MCH       23
#define  TCP_AO_ICMP_ACC        24
/*This structure tTcpAoMktAddr is dummy. Currently not used in LINUX*/
typedef struct
{
    UINT1 u1SndKeyId;                             /* Send key id in TCP-AO option */
    UINT1 u1RcvKeyId;                             /* Receive key id in TCP-AO option */
    UINT1 u1Algo;                                 /* Digest calculation algorithm HMAC-SHA-1-96(1) / AES 128(2) */
    UINT1 u1KeyLen;                               /* Master key length in bytes */
    UINT1 u1TcpOptIgn;
    UINT1 au1Padding[3];
    UINT1 au1Key[TCP_MD5SIG_MAXKEYLEN];           /* Master authentication key */
}tTcpAoMktAddr;

typedef struct
{
    UINT1 u1IcmpAccpt;
    UINT1 u1NoMktMchPckDsc;
    UINT2 u2Padding;
}tTcpAoNeighCfg;


/* Prototype definitions */
INT1  Bgp4TcphGetSockOpt (INT4 );
INT4  Bgp4TcphSetSocketOpt (INT4, UINT4 , UINT4 );
INT4  Bgp4TcphMD5AuthOptSet (tBgp4PeerEntry *, UINT1 *, UINT1);
INT4  Bgp4TcphCloseConnection (tBgp4PeerEntry *);
INT4  Bgp4TcphCloseListenPort (UINT4, UINT2);
INT4  Bgp4TcphAuthOptionMktSet(tBgp4PeerEntry *, tTcpAoMktAddr *);
INT4  Bgp4TcpAoGetMktInUse(UINT4 , tAddrPrefix , INT4 * );
INT4  Bgp4TcpAoNeighCfgSet (tBgp4PeerEntry *, tTcpAoNeighCfg  *, 
                             INT4);
INT4  Bgp4TcpAoMktSet(tBgp4PeerEntry *, tTcpAoAuthMKT *);
INT4  Bgp4TcpAoIcmpCfgSet (tBgp4PeerEntry *, INT1);
INT4  Bgp4TcpAoPktDiscCfg (tBgp4PeerEntry *, INT1);
INT4  Bgp4TcpAoGetAuthStatus (tBgp4PeerEntry *, INT4 *);

#ifdef BGP_TCP4_WANTED

/* Macros used for linux socket calls */
#define  SOCKET(a,b,c)          socket(a,b,c)
#define  CONNECT(a,b,c)         connect(a,(struct sockaddr *)b,c)
#define  BIND(a,b,c)            bind(a, (struct  sockaddr *)b, c)
#define  LISTEN( a,b)           listen(a,b)
#define  CLOSE(a)               close(a)
#define  SEND(a,b,c,d)          send(a,(INT1 *)b,c,MSG_NOSIGNAL)
#define  RECV(a,b,c,d)          recv(a,(INT1 *)b,c,d)
#define  ACCEPT(a,b,c)          accept(a, (struct sockaddr *)b, (unsigned int *)c)
#define  GETSOCKNAME( a,b,c)    getsockname(a, (struct sockaddr *)b, (unsigned int *)c)
#define  GETPEERNAME(a,b,c)     getpeername(a, (struct sockaddr *)b,(unsigned int *)c )
#define  GETSOCKOPT( a,b,c,d,e) getsockopt(a,b,c, (void *)d, (socklen_t *)e)
#define  SETSOCKOPT(a,b,c,d,e)  setsockopt((int)a,(int)b,(int)c,(void *)(d),(socklen_t)e)
#define  BGP_SOCK_SELECT        select
#define  BGP4_SET_SOCK_NON_BLK(pSock)  fcntl(pSock,F_SETFL,O_NONBLOCK)
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  MAX_ELEMENTS_IN_FDSET  1024 
#define  BGP_EINPROGRESS        EINPROGRESS
#define  BGP_EAGAIN             EAGAIN
#define  BGP_EISCONN            EISCONN
#define  BGP_ENETUNREACH        ENETUNREACH
#define  BGP_ECONNREFUSED       ECONNREFUSED
#define  BGP_EALREADY           EALREADY
#define  BGP_EWOULDBLOCK        EWOULDBLOCK
#define  BGP_EINTR              EINTR
#define  BGP_ENOTCONN           ENOTCONN

/* TCP MD5 Auth Option */
#ifndef TCP_MD5SIG
#define TCP_MD5SIG      14  /* TCP MD5 Signature (RFC2385) */
#define TCP_MD5SIG_MAXKEYLEN    80

struct tcp_md5sig {
    struct __kernel_sockaddr_storage tcpm_addr; /* address associated */
    __u16   __tcpm_pad1;                /* zero */
    __u16   tcpm_keylen;                /* key length */
    __u32   __tcpm_pad2;                /* zero */
    __u8    tcpm_key[TCP_MD5SIG_MAXKEYLEN];     /* key (binary) */
};
#endif

/* Prototype definitions */
INT4  Bgp4Tcphv4OpenConnection (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4CheckIncomingConn (UINT4);
INT4  Bgp4Tcphv4FillAddresses (tBgp4PeerEntry *);
INT4  Bgp4Tcphv4OpenListenPort (UINT4);

#endif

#ifdef BGP_TCP6_WANTED

/* Macros used for linux socket calls */
#define  SOCKET(a,b,c)          socket(a,b,c)
#define  CONNECT(a,b,c)         connect(a,(struct sockaddr *)b,c)
#define  BIND(a,b,c)            bind(a, (struct  sockaddr *)b, c)
#define  LISTEN( a,b)           listen(a,b)
#define  CLOSE(a)               close(a)
#define  SEND(a,b,c,d)          send(a,(INT1 *)b,c,MSG_NOSIGNAL)
#define  RECV(a,b,c,d)          recv(a,(INT1 *)b,c,d)
#define  ACCEPT(a,b,c)          accept(a, (struct sockaddr *)b, (unsigned int *)c)
#define  GETSOCKNAME( a,b,c)    getsockname(a, (struct sockaddr *)b, (unsigned int *)c)
#define  GETPEERNAME(a,b,c)     getpeername(a, (struct sockaddr *)b,(unsigned int *)c )
#define  GETSOCKOPT( a,b,c,d,e) getsockopt(a,b,c, (void *)d, (socklen_t *)e)
#define  SETSOCKOPT(a,b,c,d,e)  setsockopt((int)a,(int)b,(int)c,(void *)(d),(socklen_t)e)
#define  BGP_SOCK_SELECT        select
#define  BGP4_SET_SOCK_NON_BLK(pSock)  fcntl(pSock,F_SETFL,O_NONBLOCK)
#define  BGP_FD_SET_STRUCT      fd_set
#define  BGP_FD_SET             FD_SET
#define  BGP_FD_ZERO            FD_ZERO
#define  BGP_FD_ISSET           FD_ISSET
#define  BGP_FD_CLR             FD_CLR
#define  MAX_ELEMENTS_IN_FDSET  1024 
#define  BGP_EINPROGRESS        EINPROGRESS
#define  BGP_EAGAIN             EAGAIN
#define  BGP_EISCONN            EISCONN
#define  BGP_ENETUNREACH        ENETUNREACH
#define  BGP_ECONNREFUSED       ECONNREFUSED
#define  BGP_EALREADY           EALREADY
#define  BGP_EWOULDBLOCK        EWOULDBLOCK
#define  BGP_EINTR              EINTR
#define  BGP_ENOTCONN           ENOTCONN

/* Temporary - TCP MD5 Auth Option */
#define  TCP_SIGNATURE_ENABLE     1

/* Prototype definitions */
INT4  Bgp4Tcphv6OpenConnection (tBgp4PeerEntry *);
INT4  Bgp4Tcphv6CheckIncomingConn (UINT4);
INT4  Bgp4Tcphv6FillAddresses (tBgp4PeerEntry *);
INT4  Bgp4Tcphv6OpenListenPort (UINT4);


#endif
