/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4mem.c,v 1.37 2016/12/27 12:35:54 siva Exp $
 *
 * Description: This file contains the routines that allocates and 
 *              frees the space for the various data structures. 
 *
 *******************************************************************/
#ifndef BGP4MEM_C
#define BGP4MEM_C

#include "bgp4com.h"

/******************************************************************************/
/* Function Name : Bgp4AllocatePeerEntry                                      */
/* Description   : This function allocates the peer entry and initialises the */
/*                 same to the default configuration.                         */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated BGP4_PEERENTRY.                   */
/******************************************************************************/
tBgp4PeerEntry     *
Bgp4AllocatePeerEntry (UINT4 u4Context, UINT4 u4Size)
{
    tBgp4PeerEntry     *pPeerentry = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);
    pu1Block = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PeerEntryPoolId);
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer FAILED!!!\n");
        return (NULL);
    }

    pPeerentry = (tBgp4PeerEntry *) (VOID *) (pu1Block);

    Bgp4InitPeerEntry (pPeerentry);
    BGP4_PEER_LOCAL_AS (pPeerentry) = BGP4_LOCAL_AS_NO (u4Context);
    BGP4_PEER_CXT_ENTRY (pPeerentry) = Bgp4GetContextEntry (u4Context);
    Bgp4InitShadowPeerProperty (pPeerentry);

#ifdef L3VPN
    if ((BGP4_VPNV4_AFI_FLAG == TRUE) && (u4Context != BGP4_DFLT_VRFID))
    {
        BGP4_VPN4_PEER_ROLE (pPeerentry) = BGP4_VPN4_CE_PEER;
    }
#endif

    /* Default <IPv4, Unicast> address family instance will be created
     * in the peer entry.
     */
    BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry) =
        Bgp4MpeAllocateAfiSafiInstance (pPeerentry, BGP4_IPV4_UNI_INDEX);
    if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer's Route-Refresh data "
                  "FAILED!!!\n");
        Bgp4ReleasePeerEntry (pPeerentry);
        return (NULL);
    }
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerentry) = NULL;
    BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerentry) = NULL;
#endif
#ifdef BGP4_IPV6_WANTED

    BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) =
        Bgp4MpeAllocateAfiSafiInstance (pPeerentry, BGP4_IPV6_UNI_INDEX);
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer's Route-Refresh data "
                  "FAILED!!!\n");
        Bgp4ReleasePeerEntry (pPeerentry);
        return (NULL);
    }
/*    BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) = NULL;*/
#endif
#ifdef VPLSADS_WANTED

    BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry) =
        Bgp4MpeAllocateAfiSafiInstance (pPeerentry, BGP4_L2VPN_VPLS_INDEX);
    if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer's vpls afisafi insatcne data "
                  "FAILED!!!\n");
        Bgp4ReleasePeerEntry (pPeerentry);
        return (NULL);
    }
#endif
#ifdef EVPN_WANTED
    BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry) =
        Bgp4MpeAllocateAfiSafiInstance (pPeerentry, BGP4_L2VPN_EVPN_INDEX);
    if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer's Evpn afisafi insatcne data "
                  "FAILED!!!\n");
        Bgp4ReleasePeerEntry (pPeerentry);
        return (NULL);
    }
#endif
    return (pPeerentry);
}

/******************************************************************************/
/* Function Name : Bgp4ReleasePeerEntry                                       */
/* Description   : This function releases memory allocted for the peer entry  */
/* Input(s)      : Pointer to the allocated BGP4_PEERENTRY(pPeerentry).       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS - if the operation is successful              */
/*               : BGP4_FAILURE - if the operation is a failure               */
/******************************************************************************/
INT4
Bgp4ReleasePeerEntry (tBgp4PeerEntry * pPeerentry)
{
    tBufNode           *pBufNode = NULL;
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    tRouteProfile      *pRoute = NULL;
    UINT4               u4AFIndex = 0;
    INT4                i4Status;

    if (pPeerentry == NULL)
    {
        return BGP4_SUCCESS;
    }
    /* Stop all running timers */
    Bgp4TmrhStopAllTimers (pPeerentry);
    Bgp4TmrhStopTimer (BGP4_STALE_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry);

    /* Release the  ReTransmission buffer */
    if (BGP4_PEER_READVT_BUFFER (pPeerentry) != NULL)
    {
        pBufNode = (tBufNode *) (VOID *) (BGP4_PEER_READVT_BUFFER (pPeerentry));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                            BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode));
        BGP_PEER_NODE_FREE (BGP4_PEER_READVT_BUFFER (pPeerentry));
        BGP4_PEER_READVT_BUFFER (pPeerentry) = NULL;
    }

    /* When the BGP instance is moving from Standby state to Active State,
     * the peer entries in the old Standby instance, will have route synced
     * from Active linked to it. After moving to Active State, when the old 
     * peer entry is getting deleted, due to duplicate peer entry going to 
     * established state re-link the routes linked to this peer with the 
     * duplicate peer entry */
    pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
    if (pDupPeer != NULL)
    {
#if ((defined VPLSADS_WANTED) && (defined VPLS_GR_WANTED))
        for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
             u4AFIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX; u4AFIndex++)
#else
        for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
             u4AFIndex < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX -
                          BGP4_DECREMENT_BY_ONE); u4AFIndex++)
#endif
        {
            if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerentry, u4AFIndex) == NULL)
            {
                /* AFI-SAFI not negotiated for this peer. */
                continue;
            }
            if (BGP4_DLL_COUNT
                (BGP4_PEER_ROUTE_LIST (pPeerentry, u4AFIndex)) == 0)
            {
                /* The peer getting deleted has not learnt any routes.
                 * so no need to assign the route to the duplicate peer entry */
                continue;
            }
            if (BGP4_PEER_AFI_SAFI_INSTANCE (pDupPeer, u4AFIndex) != NULL)
            {
                Bgp4MpeReleaseAfiSafiInstance (pDupPeer, u4AFIndex);
            }
            /* Associate the AFI_SAFI instance to the duplicate peer */
            BGP4_PEER_AFI_SAFI_INSTANCE (pDupPeer, u4AFIndex) =
                BGP4_PEER_AFI_SAFI_INSTANCE (pPeerentry, u4AFIndex);
            BGP4_PEER_AFI_SAFI_INSTANCE (pPeerentry, u4AFIndex) = NULL;

            /* Scan the routes learnt from this peer and change the
             * back pointer of the Route to the duplicate peer entry */
            pRoute = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST
                                           (pDupPeer, u4AFIndex));
            while (pRoute != NULL)
            {
                BGP4_RT_PEER_ENTRY (pRoute) = pDupPeer;
                pRoute = BGP4_RT_PROTO_LINK_NEXT (pRoute);
            }
        }                        /* End of loop For all address families */
    }

    if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_IPV4_UNI_INDEX);
    }
    CapsDeletePeerCapsInfo (pPeerentry);
    CapsDeleteSpkrCapsInfo (pPeerentry);
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_IPV4_LBLD_INDEX);
    }
    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_VPN4_UNI_INDEX);
    }
    /* Release the site-of-origin if any */
    if (BGP4_VPN4_CE_PEER_SOO_EXT_COM (pPeerentry) != NULL)
    {
        EXT_COMM_PROFILE_FREE (BGP4_VPN4_CE_PEER_SOO_EXT_COM (pPeerentry));
    }
#endif
#ifdef BGP4_IPV6_WANTED
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_IPV6_UNI_INDEX);
    }
#endif
#ifdef VPLSADS_WANTED
    if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_L2VPN_VPLS_INDEX);
    }
#endif
#ifdef EVPN_WANTED
    if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry) != NULL)
    {
        Bgp4MpeReleaseAfiSafiInstance (pPeerentry, BGP4_L2VPN_EVPN_INDEX);
    }
#endif

    /* Remove the peer entry from the associated peer group */
    if (pPeerentry->pPeerGrp != NULL)
    {
        pPeerGroup = pPeerentry->pPeerGrp;
        pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
        TMO_DYN_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                          pTmpPeerNode, tPeerNode *)
        {
            if (pPeerNode->pPeer == pPeerentry)
            {
                /* Verify if any duplicate peer entry exists for this peer
                 * If it exists, add the duplicate entry to the peer group
                 * list. If there is no duplicate entry, remove the peer
                 * from the peer group list.
                 * when the peer entry is actually deleted, the duplicate 
                 * entry are all deleted, and then the actual entry will also
                 * be deleted. hence, it will be removed from the peer group list*/
                if (pDupPeer == NULL)
                {
                    TMO_SLL_Delete (&(pPeerGroup->PeerList),
                                    &(pPeerNode->TSNext));
                    BGP_PEER_NODE_FREE (pPeerNode);
                    pPeerentry->pPeerGrp = NULL;
                }
                else
                {
                    pPeerNode->pPeer = pDupPeer;
                    pDupPeer->pPeerGrp = pPeerGroup;
                }
                break;
            }
        }
    }

    /* Release memory for TCP MD5 data */
    if (BGP4_PEER_TCPMD5_PTR (pPeerentry) != NULL)
    {
        i4Status = MemReleaseMemBlock (BGP4_TCPMD5_MEM_POOL_ID,
                                       (UINT1 *)
                                       BGP4_PEER_TCPMD5_PTR (pPeerentry));

        if (i4Status != MEM_SUCCESS)
        {
            return BGP4_FAILURE;
        }
        BGP4_TCPMD5_ALLOC_CTR (BGP4_PEER_CXT_ID (pPeerentry))--;
    }

    /* Release the peerentry memory */
    i4Status = MemReleaseMemBlock (gBgpNode.Bgp4PeerEntryPoolId,
                                   (UINT1 *) pPeerentry);
    if (i4Status != MEM_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

#ifdef L3VPN_TEST_WANTED
/******************************************************************************/
/* Function Name : Bgp4MemAllocateMsg                                         */
/* Description   : This function allocates  buffer of given size              */
/* Input(s)      : u4Size - size of the buffer to be allocated.               */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated  buffer.                          */
/******************************************************************************/
UINT1              *
Bgp4MemAllocateMsg (UINT4 u4Size)
{
    UINT1              *pu1Msg = NULL;

    pu1Msg = MEM_CALLOC (u4Size, BGP4_ONE_BYTE, UINT1);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateMsg() : Allocate Message " "FAILED !!!\n");
        return (NULL);
    }
    BGP4_MSG_CNT++;
    return (pu1Msg);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseMsg                                          */
/* Description   : This function releases the memory alocated for the BGP     */
/*                 message buffer.                                            */
/* Input(s)      : Pointer to message be released                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseMsg (UINT1 *pu1Msg)
{
    if (pu1Msg == NULL)
    {
        return BGP4_SUCCESS;
    }
    MEM_FREE (pu1Msg);
    BGP4_MSG_CNT--;
    return BGP4_SUCCESS;
}
#endif
/******************************************************************************/
/* Function Name : Bgp4MemAllocateRouteProfile                                */
/* Description   : This function allocates  the Route Profile and initialises */
/*                 the same.                                                  */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated  tRouteProfile.                   */
/******************************************************************************/
tRouteProfile      *
Bgp4MemAllocateRouteProfile (UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;

    UNUSED_PARAM (u4Size);

    pu1Buf = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4RtProfilePoolId);
    if (pu1Buf == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateRouteProfile() : Allocate "
                  "Route Profile FAILED\n");
        return (NULL);
    }
    BGP4_RTPROFILE_CNT++;
    Bgp4InitRtProfile ((tRouteProfile *) (VOID *) pu1Buf);
    return ((tRouteProfile *) (VOID *) pu1Buf);
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateLinkNode                                    */
/* Description   : This function allocates the Link Node                      */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated  tLinkNode.                       */
/******************************************************************************/
tLinkNode          *
Bgp4MemAllocateLinkNode (UINT4 u4Size)
{
    tLinkNode          *pLinkNd = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);

    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4LinkNodePoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateLinkNode() : Allocate "
                  "Link Node FAILED\n");
        return (NULL);
    }

    pLinkNd = (tLinkNode *) (VOID *) pu1Msg;
    Bgp4InitLinkNode (pLinkNd);
    BGP4_LINKNODE_CNT++;
    return (pLinkNd);
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateAdvRtLinkNode                               */
/* Description   : This function allocates the Link Node                      */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated  tLinkNode.                       */
/******************************************************************************/
tAdvRtLinkNode     *
Bgp4MemAllocateAdvRtLinkNode (UINT4 u4Size)
{
    tAdvRtLinkNode     *pAdvLinkNd = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);

    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4AdvRtLinkNodePoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateAdvRtLinkNode () : Allocate "
                  "Link Node FAILED\n");
        return (NULL);
    }

    pAdvLinkNd = (tAdvRtLinkNode *) (VOID *) pu1Msg;

    TMO_SLL_Init_Node (&(pAdvLinkNd->TSNext));
    pAdvLinkNd->pRouteProfile = NULL;
    pAdvLinkNd->u4UpdSentTime = 0;

    BGP4_ADVT_RT_LINKNODE_CNT++;
    return (pAdvLinkNd);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseLinkNode                                 */
/* Description   : This function releases memory allocated for the link node  */
/* Input(s)      : Pointer to the allocated tLinkNode structure.              */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseLinkNode (tLinkNode * pLinknd)
{
    BGP4_LINKNODE_CNT--;
    MemReleaseMemBlock (gBgpNode.Bgp4LinkNodePoolId, (UINT1 *) pLinknd);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseAdvtLinkNode                                 */
/* Description   : To release memory allocated for the adv link node          */
/* Input(s)      : Pointer to the allocated tAdvRtLinkNode structure.         */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseAdvtLinkNode (tAdvRtLinkNode * pAdvLinknd)
{
    BGP4_ADVT_RT_LINKNODE_CNT--;
    MemReleaseMemBlock (gBgpNode.Bgp4AdvRtLinkNodePoolId, (UINT1 *) pAdvLinknd);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateBgp4info                                    */
/* Description   : This function allocates the BGP4 Information structure     */
/*                 and initialises the same.                                  */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tBgp4Info.                        */
/******************************************************************************/
tBgp4Info          *
Bgp4MemAllocateBgp4info (UINT4 u4Size)
{
    tBgp4Info          *pBGP4Info = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4BgpInfoPoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateBgp4Info() : Allocate "
                  "Bgp Info FAILED\n");
        return (NULL);
    }

    pBGP4Info = (tBgp4Info *) (VOID *) pu1Msg;
    Bgp4InitBgp4info (pBGP4Info);
    BGP4_INFO_CNT++;
    TMO_SLL_Init (BGP4_INFO_ASPATH (pBGP4Info));
    return (pBGP4Info);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseBgp4info                                     */
/* Description   : This function releases the memory allocated for the BGP    */
/*                 BGP Information.                                           */
/* Input(s)      : Pointer to the allocated tBgp4Info.                        */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseBgp4info (tBgp4Info * pBGPinfo)
{
    if (BGP4_INFO_COMM_ATTR (pBGPinfo) != NULL)
    {
        if (BGP4_INFO_COMM_ATTR_VAL (pBGPinfo) != NULL)
        {
            ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pBGPinfo));
        }
        COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pBGPinfo));
    }
    BGP4_INFO_CNT--;
    MemReleaseMemBlock (gBgpNode.Bgp4BgpInfoPoolId, (UINT1 *) pBGPinfo);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocatePAInfo                                      */
/* Description   : This function allocates the BGP4 Path Attribute node for   */
/*                 storing the identical PA and initialises the same.         */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tBgp4RcvdPathAttr                 */
/******************************************************************************/
tBgp4RcvdPathAttr  *
Bgp4MemAllocatePAInfo (UINT4 u4Size)
{
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PAPoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocatePAInfo() : Allocate "
                  "Bgp Path Attribute Info FAILED\n");
        return (NULL);
    }

    pBgp4RcvdPA = (tBgp4RcvdPathAttr *) (VOID *) pu1Msg;
    Bgp4InitBgp4PAInfo (pBgp4RcvdPA);
    BGP4_RCVD_PA_CNT++;
    return (pBgp4RcvdPA);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleasePAInfo                                       */
/* Description   : This function releases the memory allocated for the BGP    */
/*                 PA Information.                                            */
/* Input(s)      : Pointer to the allocated tBgp4RcvdPathAttr Info            */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleasePAInfo (tBgp4RcvdPathAttr * pBgp4RcvdPA)
{
    MemReleaseMemBlock (gBgpNode.Bgp4PAPoolId, (UINT1 *) pBgp4RcvdPA);
    BGP4_RCVD_PA_CNT--;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateAdvtPAInfo                                  */
/* Description   : This function allocates the BGP4 Advertise node for        */
/*                 storing the identical outbound PA and initialise the same. */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tBgp4AdvtPathAttr                 */
/******************************************************************************/
tBgp4AdvtPathAttr  *
Bgp4MemAllocateAdvtPAInfo (UINT4 u4Size)
{
    tBgp4AdvtPathAttr  *pBgp4AdvtPA = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4AdvtPAPoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocatePAInfo() : Allocate "
                  "Bgp Advertise Path Attribute Info FAILED\n");
        return (NULL);
    }

    pBgp4AdvtPA = (tBgp4AdvtPathAttr *) (VOID *) pu1Msg;
    Bgp4InitBgp4AdvtPA (pBgp4AdvtPA);
    return (pBgp4AdvtPA);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseAdvtPAInfo                                   */
/* Description   : This function releases the memory allocated for the BGP    */
/*                 Advertise PA Information.                                  */
/* Input(s)      : Pointer to the allocated tBgp4RcvdPathAttr Info            */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseAdvtPAInfo (tBgp4AdvtPathAttr * pBgp4AdvtPA)
{
    MemReleaseMemBlock (gBgpNode.Bgp4AdvtPAPoolId, (UINT1 *) pBgp4AdvtPA);
    return BGP4_SUCCESS;
}

#ifdef L3VPN
/****************************************************************************/
/* Function Name : Bgp4MemAllocateVpnRtInstallVrfNode                       */
/* Description   : This function allocates the vrf install node to put in   */
/*                 installed vrf-list of route information                  */
/* Input(s)      : size of buffer to be allocated                           */
/* Output(s)     : None.                                                    */
/* Return(s)     : Pointer to the allocated tRtInstallVrfInfo               */
/****************************************************************************/
tRtInstallVrfInfo  *
Bgp4MemAllocateVpnRtInstallVrfNode (UINT4 u4Size)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);
    pu1Block = (UINT1 *) MemAllocMemBlk (BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID);
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for VRF Installed Node FAILED !!!\n");
        return (NULL);
    }

    pRtVrfInfo = (tRtInstallVrfInfo *) (VOID *) (pu1Block);
    BGP4_VPN4_RT_INSTALL_VRF_CNT++;

    TMO_SLL_Init_Node (&(pRtVrfInfo->TsnNextVrfInfo));
    BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo) = BGP4_DFLT_VRFID;
    BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) = 0;
    (pRtVrfInfo->p_RPNext) = NULL;
    return (pRtVrfInfo);
}

/****************************************************************************/
/* Function Name : Bgp4MemReleaseVpnRtInstallVrfNode                        */
/* Description   : This function releases the vrf install node              */
/* Input(s)      : Pointer to tRtInstallVrfInfo                             */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4MemReleaseVpnRtInstallVrfNode (tRtInstallVrfInfo * pRtVrfInfo)
{
    MemReleaseMemBlock (BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID,
                        (UINT1 *) pRtVrfInfo);
    BGP4_VPN4_RT_INSTALL_VRF_CNT--;
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4MemAllocateRrImportTarget                            */
/* Description   : This function allocates RR import target information     */
/* Input(s)      : size of buffer to be allocated                           */
/* Output(s)     : None.                                                    */
/* Return(s)     : Pointer to the allocated tBgp4RrImportTargetInfo         */
/****************************************************************************/
tBgp4RrImportTargetInfo *
Bgp4MemAllocateRrImportTarget (UINT4 u4Size)
{
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);
    pu1Block = (UINT1 *) MemAllocMemBlk (BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID);
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer FAILED!!!\n");
        return (NULL);
    }

    pRrImportTarget = (tBgp4RrImportTargetInfo *) (VOID *) (pu1Block);
    TMO_SLL_Init_Node (&(pRrImportTarget->NextTarget));
    BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) = 0;
    BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget) = 0;
    BGP4_VPN4_RR_TARGETS_SET_CNT++;
    return (pRrImportTarget);
}

/****************************************************************************/
/* Function Name : Bgp4MemReleaseRrImportTarget                             */
/* Description   : This function releases the RR import target info         */
/* Input(s)      : Pointer to tBgp4RrImportTargetInfo                       */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4MemReleaseRrImportTarget (tBgp4RrImportTargetInfo * pRrImportTarget)
{
    MemReleaseMemBlock (BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID,
                        (UINT1 *) pRrImportTarget);
    BGP4_VPN4_RR_TARGETS_SET_CNT--;
    return BGP4_SUCCESS;
}
#endif

#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
/*****************************************************************************/
/* Function Name : Bgp4MemAllocateVplsSpecEntry                              */
/* Description   : This function allocates the VPLS specific instance        */
/* Input(s)      : size of buffer to be allocated                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : Pointer to the allocated tVplsSpecInfo                    */
/*****************************************************************************/
tVplsSpecInfo      *
Bgp4MemAllocateVplsSpecEntry (UINT4 u4Size)
{
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_VPLS_SPEC_CNT > MAX_BGP_VPLS_INSTANCE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateVplsSpecEntry() : Allocate "
                  "MAX_BGP_VPLS_INSTANCE already created\n");
        return (NULL);
    }

    pu1Msg = (UINT1 *) MemAllocMemBlk (BGP4_VPLS_SPEC_MEMPOOL_ID);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateVplsSpecEntry() : Allocate "
                  "VPLS Specific Info FAILED\n");
        return (NULL);
    }

    pVplsSpecInfo = (tVplsSpecInfo *) (VOID *) pu1Msg;
    TMO_SLL_Init_Node (&(pVplsSpecInfo->NextVplsNode));
    TMO_SLL_Init (&(pVplsSpecInfo->ImportTargets));
    TMO_SLL_Init (&(pVplsSpecInfo->ExportTargets));

    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = BGP4_DFLT_VPLS_INDX;

    BGP4_VPLS_SPEC_CNT++;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return (pVplsSpecInfo);
}

/*****************************************************************************/
/* Function Name : Bgp4MemReleaseVplsSpecEntry                               */
/* Description   : This function releases the memory allocated for the VPNv4 */
/*                 VRF specific instance Information.                        */
/* Input(s)      : Pointer to the released tVplsSpecInfo                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4MemReleaseVplsSpecEntry (tVplsSpecInfo * pVplsSpecInfo)
{

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /* Delete the import route targets */
    Bgp4VplsReleaseRouteTargetsFromVpls (BGP4_VPLS_SPEC_IMPORT_TARGETS
                                         (pVplsSpecInfo),
                                         BGP4_IMPORT_ROUTE_TARGET_TYPE);
    /* Delete the export route tagets */
    Bgp4VplsReleaseRouteTargetsFromVpls (BGP4_VPLS_SPEC_EXPORT_TARGETS
                                         (pVplsSpecInfo),
                                         BGP4_EXPORT_ROUTE_TARGET_TYPE);

    MemReleaseMemBlock (BGP4_VPLS_SPEC_MEMPOOL_ID, (UINT1 *) pVplsSpecInfo);
    BGP4_VPLS_SPEC_CNT--;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

#endif /*MPLS_WANTED */
/****************************************************************************/
/* Function Name : Bgp4MemAllocateRrImportTargetVpls                        */
/* Description   : This function allocates RR import target information     */
/* Input(s)      : size of buffer to be allocated                           */
/* Output(s)     : None.                                                    */
/* Return(s)     : Pointer to the allocated tBgp4RrImportTargetInfo         */
/****************************************************************************/
tBgp4VplsRrImportTargetInfo *
Bgp4MemAllocateRrImportTargetVpls (UINT4 u4Size)
{
    tBgp4VplsRrImportTargetInfo *pRrImportTarget = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);

    if (BGP4_VPLS_RR_TARGETS_SET_CNT > MAX_BGP_VPLS_RR_IMPORT_TARGETS_SET)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMAX_BGP_VPLS_RR_IMPORT_TARGETS_SET added!!!\n");
        return (NULL);
    }

    pu1Block = (UINT1 *) MemAllocMemBlk (BGP4_VPLS_RR_TARGETS_SET_MEMPOOL_ID);
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer FAILED!!!\n");
        return (NULL);
    }

    pRrImportTarget = (tBgp4VplsRrImportTargetInfo *) (VOID *) (pu1Block);
    TMO_SLL_Init_Node (&(pRrImportTarget->NextTarget));
    BGP4_VPLS_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) = 0;
    BGP4_VPLS_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget) = 0;
    BGP4_VPLS_RR_TARGETS_SET_CNT++;
    return (pRrImportTarget);
}

/****************************************************************************/
/* Function Name : Bgp4MemReleaseRrImportTargetVpls                         */
/* Description   : This function releases the RR import target info         */
/* Input(s)      : Pointer to tBgp4VplsRrImportTargetInfo                   */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4MemReleaseRrImportTargetVpls (tBgp4VplsRrImportTargetInfo * pRrImportTarget)
{
    MemReleaseMemBlock (BGP4_VPLS_RR_TARGETS_SET_MEMPOOL_ID,
                        (UINT1 *) pRrImportTarget);
    BGP4_VPLS_RR_TARGETS_SET_CNT--;
    return BGP4_SUCCESS;
}

#endif /*VPLSADS_WANTED */

/*****************************************************************************/
/* Function Name : Bgp4MemAllocateIfEntry                                    */
/* Description   : This function allocates the interface information struct  */
/* Input(s)      : size of buffer to be allocated                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : Pointer to the allocated tBgp4IfInfo                      */
/*****************************************************************************/
tBgp4IfInfo        *
Bgp4MemAllocateIfEntry (UINT4 u4Size)
{
    tBgp4IfInfo        *pBgp4IfInfo = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4IfPoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateIfEntry() : Allocate "
                  "Bgp Interface Info FAILED\n");
        return (NULL);
    }

    pBgp4IfInfo = (tBgp4IfInfo *) (VOID *) pu1Msg;
    TMO_SLL_Init_Node (&(pBgp4IfInfo->NextIfNode));
    BGP4_IFACE_ENTRY_INDEX (pBgp4IfInfo) = BGP4_INVALID_IFINDX;
    BGP4_IFACE_ENTRY_ADDR (pBgp4IfInfo) = 0;
    BGP4_IFACE_ENTRY_OPER_STATUS (pBgp4IfInfo) = 0;
    BGP4_IFACE_ENTRY_IFTYPE (pBgp4IfInfo) = 0;
#ifdef L3VPN
    BGP4_IFACE_ENTRY_VRFID (pBgp4IfInfo) = BGP4_DFLT_VRFID;
    BGP4_IFACE_ENTRY_VPN_CLASS (pBgp4IfInfo) = 0;
    BGP4_IFACE_ENTRY_LABEL (pBgp4IfInfo) = BGP4_INVALID_IGP_LABEL;
    BGP4_IFACE_LABEL_PEERS (pBgp4IfInfo) = 0;
#endif

    BGP4_IFACE_CNT++;
    return (pBgp4IfInfo);
}

/*****************************************************************************/
/* Function Name : Bgp4MemReleaseIfEntry                                     */
/* Description   : This function releases the memory allocated for the BGP   */
/*                 Interface Information.                                    */
/* Input(s)      : Pointer to the released tBgp4IfInfo                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4MemReleaseIfEntry (tBgp4IfInfo * pBgp4IfInfo)
{
    MemReleaseMemBlock (gBgpNode.Bgp4IfPoolId, (UINT1 *) pBgp4IfInfo);
    BGP4_IFACE_CNT--;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseRouteProfile                                 */
/* Description   : This function releases the memory allocated for the Route  */
/*                 Profile.                                                   */
/* Input(s)      : Pointer to the allocated tRouteProfile.                    */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseRouteProfile (tRouteProfile * pRouteProfile)
{
    Bgp4InitRtProfile (pRouteProfile);
    MemReleaseMemBlock (gBgpNode.Bgp4RtProfilePoolId, (UINT1 *) pRouteProfile);
    BGP4_RTPROFILE_CNT--;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemGetASNode                                           */
/* Description   : This function allocates the memory required for tAsPath.   */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tAsPath.                          */
/******************************************************************************/
tAsPath            *
Bgp4MemGetASNode (UINT4 u4Size)
{
    tAsPath            *pASPath = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4AsPathPoolId);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemGetASNode() : Allocate " "AS Node FAILED\n");
        return (NULL);
    }
    pASPath = (tAsPath *) (VOID *) pu1Msg;
    BGP4_ASNODE_CNT++;
    Bgp4InitASNode (pASPath);
    return (pASPath);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseASNode                                    */
/* Description   : This function releases the memory allocated for the ASPATH */
/* Input(s)      : Pointer to the allocated tAsPath.                          */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseASNode (tAsPath * pASNode)
{
    BGP4_ASNODE_CNT--;
    MemReleaseMemBlock (gBgpNode.Bgp4AsPathPoolId, (UINT1 *) pASNode);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateQMsg                                        */
/* Description   : This function allocates  pool for input q messages         */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated  input message                    */
/******************************************************************************/
tBgp4QMsg          *
Bgp4MemAllocateQMsg (UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;

    UNUSED_PARAM (u4Size);
    pu1Buf = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4InputQPoolId);
    if (pu1Buf == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateQMsg() : Allocate "
                  "Input Q message FAILED\n");
        return (NULL);
    }
    BGP4_INPUTQ_MSG_CNT++;
    MEMSET (pu1Buf, 0, sizeof (tBgp4QMsg));
    BGP4_INPUTQ_DATA (((tBgp4QMsg *) (VOID *) pu1Buf)) = 0;
    return ((tBgp4QMsg *) (VOID *) pu1Buf);
}

/******************************************************************************/
/* Function Name : Bgp4DeallocatePeerDataList                                 */
/* Description   : This function deallocates the memory allocated for the     */
/*                 the peer data list                                         */
/* Input(s)      : pointer to the memory that has to be freed                 */
/* Output(s)     : None                                                       */
/* Return(s)     : BGP4_SUCCESS                                               */
/******************************************************************************/
INT4
Bgp4DeallocatePeerDataList (tBgp4PeerEntry ** pPtr)
{
    if (pPtr == NULL)
    {
        return BGP4_SUCCESS;
    }
    MemReleaseMemBlock (gBgpNode.Bgp4PeerDataEntryPoolId, (UINT1 *) pPtr);
    BGP4_MSG_CNT--;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MemReleasePeerList                         */
/* Description     : Removes all peer-entry from the global peer    */
/*                   list                                           */
/* Input(s)        : Peer list (pList)                              */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling :                  */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS                        */
/********************************************************************/
INT4
Bgp4MemReleasePeerList (tTMO_SLL * pList)
{
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpNode = NULL;

    BGP_SLL_DYN_Scan (pList, pPeerNode, pTmpNode, tPeerNode *)
    {
        TMO_SLL_Delete (pList, &pPeerNode->TSNext);
        BGP_PEER_NODE_FREE (pPeerNode);
    }
    TMO_SLL_Init (pList);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateTcpMd5Buf                                   */
/* Description   : This function allocates memory for storing TCP-Md5 related */
/*                 information and initialises the same.                      */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tTreeNode.                        */
/******************************************************************************/
tTcpMd5AuthPasswd  *
Bgp4MemAllocateTcpMd5Buf (UINT4 u4CxtId, UINT4 u4Size)
{
    tTcpMd5AuthPasswd  *pBuf = NULL;
    UINT1              *pu1Msg = NULL;
    INT1                i1Cnt = 0;

    UNUSED_PARAM (u4Size);
    pu1Msg = (UINT1 *) MemAllocMemBlk (BGP4_TCPMD5_MEM_POOL_ID);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for TCP MD5 Buffer FAILED!!!\n");
        return (NULL);
    }

    pBuf = (tTcpMd5AuthPasswd *) pu1Msg;
    for (i1Cnt = 0; i1Cnt < BGP4_TCPMD5_PWD_SIZE; i1Cnt++)
    {
        pBuf->au1TcpMd5Passwd[i1Cnt] = 0;
    }
    pBuf->u1TcpMd5PasswdLength = 0;
    pBuf->u1PwdStatus = 0;

    BGP4_TCPMD5_ALLOC_CTR (u4CxtId)++;
    return (pBuf);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseTcpMd5Buf                                    */
/* Description   : This function releases memory allocated for storing TCP-MD5*/
/*                 related information in the peer data structure             */
/* Input(s)      : Pointer to the TCP MD5 info.                               */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4MemReleaseTcpMd5Buf (tTcpMd5AuthPasswd * pBuf, UINT4 u4CxtId)
{
    INT4                i4Status;
    i4Status =
        (INT4) (MemReleaseMemBlock (BGP4_TCPMD5_MEM_POOL_ID, (UINT1 *) pBuf));
    if (i4Status != MEM_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    else
    {
        BGP4_TCPMD5_ALLOC_CTR (u4CxtId)--;
        return BGP4_SUCCESS;
    }
}

/******************************************************************************/
/* Function Name : Bgp4MemAllocateTcpAOBuf                                    */
/* Description   : This function allocates memory for storing TCP-AO MKT      */
/*                 information and initialises the same.                      */
/* Input(s)      : size of buffer to be allocated                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : Pointer to the allocated tTreeNode.                        */
/******************************************************************************/
tTcpAoAuthMKT      *
Bgp4MemAllocateTcpAoBuf (UINT4 u4CxtId, UINT4 u4Size)
{
    tTcpAoAuthMKT      *pBuf = NULL;
/*    UINT1              *pu1Msg = NULL;*/
    INT4                i4Cnt = 0;
    UNUSED_PARAM (u4Size);
    pBuf = (tTcpAoAuthMKT *) MemAllocMemBlk (BGP4_TCPAO_MEM_POOL_ID);
    /*pu1Msg = (UINT1 *) MemAllocMemBlk (BGP4_TCPAO_MEM_POOL_ID); */

/*    if (pu1Msg == NULL)*/
    if (pBuf == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for TCP AO MKT Buffer FAILED!!!\n");
        return (NULL);
    }

/*    pBuf = (tTcpAoAuthMKT*) pu1Msg;*/
    TMO_SLL_Init_Node (&(pBuf->TSNext));
    /*Initialize the MKT config */
    for (i4Cnt = 0; i4Cnt < BGP4_TCPMD5_PWD_SIZE; i4Cnt++)
    {
        pBuf->au1TcpAOMasterKey[i4Cnt] = 0;
    }
    pBuf->u1TcpAOPasswdLength = 0;

    /*TODO set other default parameters */
    pBuf->u1TcpOptionIgnore = 1;
    pBuf->u1MktRowStatus = 0;

    BGP4_TCPAO_ALLOC_CTR (u4CxtId)++;

    return pBuf;
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseTcpAoBuf                                    */
/* Description   : This function releases memory allocated for storing TCP-MD5*/
/*                 related information in the peer data structure             */
/* Input(s)      : Pointer to the TCP MD5 info.                               */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4MemReleaseTcpAoBuf (tTcpAoAuthMKT * pBuf, UINT4 u4CxtId)
{
    INT4                i4Status;
    i4Status =
        (INT4) MemReleaseMemBlock ((UINT4) BGP4_TCPAO_MEM_POOL_ID,
                                   (UINT1 *) pBuf);
    if (i4Status != MEM_SUCCESS)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory free of TCP AO MKT Buffer FAILED!!!\n");
        return BGP4_FAILURE;
    }
    else
    {
        BGP4_TCPAO_ALLOC_CTR (u4CxtId)--;
        return BGP4_SUCCESS;
    }

}

/******************************************************************************/
/* Function Name : Bgp4MemReleasePeerAdvtList                                 */
/* Description   : This function releases memory allocated to peer advt. list */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4MemReleasePeerAdvtList (tBgp4PeerEntry * pPeerentry)
{
    /* Variable Declarations */
    tBufNode           *pBufNode = NULL;
    tBufNode           *pTmpBufNode = NULL;

    BGP_SLL_DYN_Scan (BGP4_PEER_ADVT_MSG_LIST (pPeerentry), pBufNode,
                      pTmpBufNode, tBufNode *)
    {
        TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerentry),
                        &pBufNode->TSNext);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pBufNode->pu1Msg);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId, (UINT1 *) pBufNode);
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4AllocateIgpMetricEntry                                 */
/* Description   : This function create a new IGP metric table entry.         */
/* Input(s)      : None                                                       */
/* Output(s)     : Pointer to hold the allocated entry (ppIgpMetricEntry)     */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

INT4
Bgp4AllocateIgpMetricEntry (tBgp4IgpMetricEntry ** ppIgpMetricEntry)
{
    /* Allocate the memory for a new entry from teh Igp Metric mempool. */
    *ppIgpMetricEntry =
        (tBgp4IgpMetricEntry *) MemAllocMemBlk (gBgpNode.
                                                Bgp4NextHopMetricPoolId);
    if (*ppIgpMetricEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Igp Metric Entry FAILED!!!\n");
        return (BGP4_FAILURE);
    }

    /* Initialise the new entry. */
    Bgp4InitIgpMetricEntry (*ppIgpMetricEntry);
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4ReleaseIgpMetricEntry                                  */
/* Description   : This function release the IGP metric table entry.          */
/* Input(s)      : Pointer to the entry to be released (pIgpMetricEntry)      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

INT4
Bgp4ReleaseIgpMetricEntry (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
    UINT4               u4RetVal;

    /* Release the nexthop route list */
    Bgp4DshClearLinkList (BGP4_NH_METRIC_VRFID (pIgpMetricEntry),
                          BGP4_NH_METRIC_ROUTES (pIgpMetricEntry),
                          BGP4_NEXTHOP_LIST_INDEX);

    /* Release the igp metric entry */
    u4RetVal = MemReleaseMemBlock (gBgpNode.Bgp4NextHopMetricPoolId,
                                   (UINT1 *) pIgpMetricEntry);

    if (u4RetVal == MEM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4CreateIgpMetricEntry() : CREATING an entry For"
                  "Igp Metric FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    return (BGP4_SUCCESS);
}

#ifdef RFD_WANTED
/*****************************************************************************/
/* Function Name : Bgp4MemAllocateRfdDampHist                                */
/* Description   : This function allocates the memory for RFD Damping History*/
/*                 structure.                                                */
/* Input(s)      : size of buffer to be allocated                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : Pointer to the allocated  tRtDampHist                     */
/*****************************************************************************/
tRtDampHist        *
Bgp4MemAllocateRfdDampHist (UINT4 u4Size)
{
    tRtDampHist        *pRtDampHist = NULL;
    UINT1              *pu1Msg = NULL;

    UNUSED_PARAM (u4Size);

    pu1Msg = (UINT1 *) MemAllocMemBlk (RT_DAMP_HIST_MEMPOOLID);
    if (pu1Msg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MemAllocateRfdDampHist() : Allocate "
                  "RFD Damp History FAILED\n");
        return (NULL);
    }

    pRtDampHist = (tRtDampHist *) (VOID *) pu1Msg;
    Bgp4InitRfdDampHist (pRtDampHist);
    return (pRtDampHist);
}

/******************************************************************************/
/* Function Name : Bgp4MemReleaseRfdDampHist                                  */
/* Description   : This function releases memory allocated for the Route Damp */
/*               : History.                                                   */
/* Input(s)      : Pointer to the allocated tRtDampHist structure.            */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4MemReleaseRfdDampHist (tRtDampHist * pRtDampHist)
{
    MemReleaseMemBlock (RT_DAMP_HIST_MEMPOOLID, (UINT1 *) pRtDampHist);
    return BGP4_SUCCESS;
}
#endif
/******************************************************************************/
/* Function Name : Bgp4AllocateNetworkRtEntry                                 */
/* Description   : This function create a new network route table entry.      */
/* Input(s)      : None                                                       */
/* Output(s)     : Pointer to hold the allocated entry (pNetworkRt)           */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
tNetworkAddr * 
Bgp4AllocateNetworkRtEntry (tNetworkAddr *pNetworkRt)
{
    pNetworkRt = 
          (tNetworkAddr *) MemAllocMemBlk (gBgpNode.Bgp4NetworkRtPoolId);
    if (pNetworkRt == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "\tMemory Allocation for Network route entry FAILED!!!\n");
        return (pNetworkRt);
    }
    return (pNetworkRt);
}
/******************************************************************************/
/* Function Name : Bgp4ReleaseNetworkRtEntry                                  */
/* Description   : This function release the network route table entry.       */
/* Input(s)      : Pointer to the entry to be released (pNetworkRt)           */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4ReleaseNetworkRtEntry (tNetworkAddr *pNetworkRt)
{
    UINT4               u4RetVal;      
       /* Release the network route entry */
    u4RetVal = MemReleaseMemBlock (gBgpNode.Bgp4NetworkRtPoolId,
                                   (UINT1 *)pNetworkRt );

    if (u4RetVal == MEM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tMemory free of Network route entry FAILED !!!\n");
        return (BGP4_FAILURE);
    }
    return (BGP4_SUCCESS);
}

#endif /* BGP4MEM_C */
