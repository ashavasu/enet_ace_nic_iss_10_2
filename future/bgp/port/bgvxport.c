/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgvxport.c,v 1.11 2009/07/16 10:02:28 prabuc-iss Exp $
 *
 * Description: These routines interface with the underlying VxWorks SLI 
 *              layer.
 *
 *******************************************************************/
#ifndef BGVXPORT_C
#define BGVXPORT_C
#include "bgp4com.h"

/* Structure definitions used to initialize the fsap2 library */
/* Global variables declarated below should be either here or in LR */
tSystemSize         gSystemSize;
tOsixCfg            LrOsixCfg;
tTimerCfg           LrTimerCfg;
tMemPoolCfg         LrMemPoolCfg;
tBufConfig          LrBufConfig;

#ifdef SNMP_WANTED
extern INT4 RegisterStdBGPwithFutureSNMP PROTO ((VOID));
extern INT4 RegisterFSBGPwithFutureSNMP PROTO ((VOID));
#endif

/* ENVIRONMENT SPECIFIC CALLS */
/**************************************************************************/
/* Function Name          : Bgp4SysMain            */
/* Description            : This routine Initialize the FSAP2 library.   */
/* Input (s)              : None               */
/* Output (s)             : None               */
/* Returns                : None               */
/**************************************************************************/
INT4
Bgp4SysMain (VOID)
{

/* Initialize Osix manager */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = SELF;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        PRINTF ("OsixInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize MemPool manager */
    LrMemPoolCfg.u4MaxMemPools = SYS_MAX_MEMPOOLS;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        PRINTF ("MemInitMemPool Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        PRINTF ("BufInitManager Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize the Timer Manager */
    LrTimerCfg.u4MaxTimerLists = SYS_MAX_NUM_OF_TIMER_LISTS;
    if (TmrTimerInit (&LrTimerCfg) != TMR_SUCCESS)
    {
        PRINTF ("TmrTimerInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

    if (SystemStart () == 0)
    {
        Fsap2Start (0);
    }
    else
    {
        return (1);
    }
    return (0);
}

/**************************************************************************/
/* Function Name          : SystemStart               */
/* Description            : This routine spawns the SNMP and BGP4  */
/*                          tasks and registers the standard and   */
/*                          FS BGP MIB with SNMP.                  */
/* Input (s)              : None                          */
/* Output (s)             : None                          */
/* Returns                : None                          */
/**************************************************************************/
INT4
SystemStart (VOID)
{

    /* Attaching VxIp to all Interfaces --- Shoule be removed */
    PRINTF ("\n Attaching VxIp to all the interfaces .......... !!!\n");
    attachVxIpToAllIfs ();
    ifAddrSet ("lnPci0", "10.20.20.155");
    ifAddrSet ("lnPci1", "20.20.20.155");
    ifAddrSet ("ene0", "30.20.20.155");

#ifdef SNMP_WANTED
    SNMP_TSK_Initialize_Protocol ();
    /* Registering Standard and FSBGP mib to SNMP */
    RegisterStdBGPwithFutureSNMP ();
    RegisterFSBGPwithFutureSNMP ();
#endif

    Bgp4TaskInitializeProtocol ();
    /* Initializing the M2IpLib */
    m2IpInit (0x64);
    return (0);
}
#endif /* BGVXPORT_C */
