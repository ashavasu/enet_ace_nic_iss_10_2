/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpvxtcp.c,v 1.9 2014/03/05 11:48:11 siva Exp $
 *
 * Description: These routines interface with the underlying TCP 
 *              layer.
 *
 *******************************************************************/
#ifndef BGPVXTCP_C
#define BGPVXTCP_C
#include "bgp4com.h"

/****************************************************************************/
/* Function Name : Bgp4TcphGetSockOpt                                       */
/* Description   : Whenever a new connection is opened this function is     */
/*                 called to get the socket options                         */
/* Input(s)      : i4Connid                                                 */
/* Output(s)     : None.                                                    */
/* Return(s)     : Specified OptVal for the socket                          */
/****************************************************************************/
INT1
Bgp4TcphGetSockOpt (INT4 i4Connid)
{
    INT1                i1OptVal = 0;
    INT4                i4OptLen = sizeof (INT1);

    GETSOCKOPT (i4Connid, SOL_SOCKET, SO_ERROR, &i1OptVal, &i4OptLen);
    return (i1OptVal);
}

/******************************************************************************/
/* Function Name : Bgp4TcphSetSocketOpt                                       */
/* Description   : Whenever a new connection is opened this function is       */
/*                 called to set the socket OPTION as NON-BLOCKING MODE.      */
/* Input(s)      : Socket ID (i4Socket)                                       */
/*                 Socket Option (u4SockOpt)                                  */
/*                 Option Value (u4OptVal)                                    */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphSetSocketOpt (INT4 i4Socket, UINT4 u4SockOpt, UINT4 u4Optval)
{
    INT4                i4RetVal = BGP4_FAILURE;
    INT4                i4Arg = 1;
    INT1                i1Optval;

    switch (u4SockOpt)
    {
        case SEND_BUF:
            i4RetVal = SETSOCKOPT (i4Socket, SOL_SOCKET, SO_SNDBUF,
                                   &u4Optval, sizeof (UINT4));
            break;

        case RECV_BUF:
            i4RetVal = SETSOCKOPT (i4Socket, SOL_SOCKET, SO_RCVBUF,
                                   &u4Optval, sizeof (UINT4));
            break;

        case REUSE_ADDR:
            i1Optval = BGP4_REUSEADDR_OPT_VAL;
            /* set socket as non-blocking */
            if (BGP4_SET_SOCK_NON_BLK (i4Socket, FIONBIO, &i4Arg) < 0)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                          BGP4_MOD_NAME, "tcph:Error in socket optionsset \n ");
                CLOSE (i4Socket);
                return (BGP4_FAILURE);
            }

            i4RetVal = SETSOCKOPT (i4Socket, SOL_SOCKET, SO_REUSEADDR,
                                   &i1Optval, sizeof (INT1));
            break;

        case HOPLIMIT:
        case REUSE_PORT:
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "Unsupported socket optionsset \n ");
            break;
    }

    if (i4RetVal < 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME, "tcph:Error in socket optionsset \n ");
        CLOSE (i4Socket);
        return (BGP4_FAILURE);
    }

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4TcphMD5AuthOptSet                                      */
/* Description   : Whenever a new TCP connection is opened this function is   */
/*                 called to set the TCP MD5 authentication option.           */
/* Input(s)      : Socket ID (i4Socket)                                       */
/*               : Peer Information ( pPeerInfo)                              */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphMD5AuthOptSet (tBgp4PeerEntry * pPeerInfo, INT4 i4Socket)
{
    UNUSED_PARAM (i4Socket);

    if (BGP4_PEER_TCPMD5_PTR (pPeerInfo) == NULL)
    {
        return BGP4_SUCCESS;
    }

    if (BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerInfo) == 0)
    {
        return BGP4_SUCCESS;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME, "tcph:Error in MD5 socket options\n ");
    return BGP4_FAILURE;
}

/******************************************************************************/
/* Function Name : Bgp4TcphCloseConnection                                 */
/* Description   : This function interacts with the lower layer namely SLI    */
/*                 (Socket Layer Interface) to close the already opened       */
/*                 connection and clears the corresponding peer information.  */
/* Input(s)      : Peer whose connection needs to be terminated (pPeerentry) */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphCloseConnection (tBgp4PeerEntry * pPeerentry)
{
    tBufNode           *pBufNode = NULL;
    tBufNode           *pTmpBufNode = NULL;
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpPeerNode = NULL;

    if (BGP4_PEER_CONN_ID (pPeerentry) == BGP4_INV_CONN_ID)
    {
        return (BGP4_SUCCESS);
    }
    CLOSE (BGP4_PEER_CONN_ID (pPeerentry));
    Bgp4TmrhStopAllTimers (pPeerentry);
    BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerentry),
                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
    BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerentry),
                BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID));
    BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerentry),
                BGP4_READ_SOCK_FD_BITS (BGP4_DFLT_VRFID));
    BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerentry),
                BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID));
    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_DELETE);
    BGP4_PEER_CONN_ID (pPeerentry) = BGP4_INV_CONN_ID;
    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) != BGP4_TRUE)
    {
        Bgp4InitNetAddressStruct (&(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerentry)),
                                  BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    }
    BGP4_PEER_LOCAL_PORT (pPeerentry) = BGP4_INV_PORT;
    BGP4_PEER_REMOTE_PORT (pPeerentry) = BGP4_INV_PORT;
    BGP4_PEER_BGP_ID (pPeerentry) = BGP4_INV_IPADDRESS;
    BGP4_PEER_NEG_VER (pPeerentry) = BGP4_INV_VER;
    BGP4_PEER_NEG_HOLD_INT (pPeerentry) = 0;
    BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry) = 0;
    BGP4_PEER_NEG_CAP_MASK (pPeerentry) = 0;
    BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) = 0;
    BGP4_PEER_OUT_MSGS (pPeerentry) = 0;
    BGP4_PEER_OUT_UPDATES (pPeerentry) = 0;
    BGP4_PEER_IN_UPDATES (pPeerentry) = 0;
    BGP4_PEER_IN_MSGS (pPeerentry) = 0;
    BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeerentry) = BGP4_INIT_TIME;
    BGP4_PEER_ESTAB_TIME (pPeerentry) = 0;

    if (TMO_SLL_Count (BGP4_PEER_ADVT_MSG_LIST (pPeerentry)) <=
        BGP4_MAX_BUF2PROCESS)
    {
        /* Free the peer advt message list. */
        BGP_SLL_DYN_Scan (BGP4_PEER_ADVT_MSG_LIST (pPeerentry), pBufNode,
                          pTmpBufNode, tBufNode *)
        {
            TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerentry),
                            (tTMO_SLL_NODE *) pBufNode);
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pBufNode->pu1Msg);
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                (UINT1 *) pBufNode);
        }
    }

    /* Free the peer retransmission buffer if present. */
    if (BGP4_PEER_READVT_BUFFER (pPeerentry) != NULL)
    {
        pBufNode = (tBufNode *) (BGP4_PEER_READVT_BUFFER (pPeerentry));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                            BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                            BGP4_PEER_READVT_BUFFER (pPeerentry));
        BGP4_PEER_READVT_BUFFER (pPeerentry) = NULL;
    }

    if (BGP4_PEER_IS_IN_TRANX_LIST (pPeerentry) == BGP4_TRUE)
    {
        /* Remove the Peer from the Transmission List */
        BGP_SLL_DYN_Scan (BGP4_PEER_TRANS_LIST (BGP4_DFLT_VRFID), pPeerNode,
                          pTmpPeerNode, tPeerNode *)
        {
            if (pPeerNode->pPeer == pPeerentry)
            {
                TMO_SLL_Delete (BGP4_PEER_TRANS_LIST (BGP4_DFLT_VRFID),
                                (tTMO_SLL_NODE *) pPeerNode);
                BGP_PEER_NODE_FREE (pPeerNode);
                break;
            }
        }
        BGP4_PEER_IS_IN_TRANX_LIST (pPeerentry) = BGP4_FALSE;
    }

    /* Clear the Residula Buf Data. Since this data is no more
     * useful. */
    if (BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                            BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry));
    }
    BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) = NULL;
    BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
    BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) = 0;

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4TcphCloseListenPort                                    */
/* Description   : Whenever the BGP Global Admin is made DOWN, this function  */
/*                 is called to close the listen Socket.                      */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4TcphCloseListenPort (UINT2 u2Afi)
{
    switch (u2Afi)
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            if (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID) != BGP4_INV_CONN_ID)
            {
                CLOSE (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID));
            }
            BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID) = BGP4_INV_CONN_ID;
            break;
#endif
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            if (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID) != BGP4_INV_CONN_ID)
            {
                CLOSE (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID));
            }
            BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID) = BGP4_INV_CONN_ID;
            break;
#endif
        default:
            break;
    }
    return BGP4_SUCCESS;
}

#ifdef BGP_TCP6_WANTED
/******************************************************************************/
/* Function Name : Bgp4Tcphv6OpenConnection                                   */
/* Description   : This function interacts with the lower layer namely SLI    */
/*                 (Socket Layer Interface) to open a TCP connection with     */
/*                 the specified peer. Depending on the result of the         */
/*                 connection proper message is enqueued in BGP task queue.   */
/* Input(s)      : Peer to which connection needs to be established           */
/*                (pPeerentry)                                                */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Tcphv6OpenConnection (tBgp4PeerEntry * pPeerentry)
{
    struct sockaddr_in6 Destaddr;
    struct sockaddr_in6 Srcaddr;
    INT4                i4Connid;
    INT4                i4RetVal;
    UINT1               u1PeerType = 0;
    UINT4               u4VrfId;

    /* If the peer is configured as PASSIVE, then do not initiate the
     * connection with the peer. Let the remote peer initiate the session
     */
    if (BGP4_PEER_CONN_PASSIVE (pPeerentry) == BGP4_TRUE)
    {
        return BGP4_PASSIVE_SET;
    }

    MEMSET (&Destaddr, 0, sizeof (struct sockaddr_in6));
    Destaddr.sin6_family = AF_INET6;
    Destaddr.sin6_port = OSIX_HTONS (BGP4_LOCAL_LISTEN_PORT (BGP4_DFLT_VRFID));

    MEMCPY (Destaddr.sin6_addr.s6_addr, BGP4_PEER_REMOTE_ADDR (pPeerentry),
            BGP4_IPV6_PREFIX_LEN);

    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) == BGP4_TRUE)
    {
        /* Get the local address configured for this peer */
        MEMSET (&Srcaddr, 0, sizeof (struct sockaddr_in6));
        Srcaddr.sin6_family = AF_INET6;

        MEMCPY (Srcaddr.sin6_addr.s6_addr, BGP4_PEER_LOCAL_ADDR (pPeerentry),
                BGP4_IPV6_PREFIX_LEN);
    }

    if (BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_STOP)
    {
        return BGP4_FAILURE;
    }

    if (BGP4_PEER_CONN_ID (pPeerentry) == BGP4_INV_CONN_ID)
    {
        i4Connid = SOCKET (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
        if (i4Connid == BGP4_FAILURE)
        {
            return (BGP4_FAILURE);
        }

        if (Bgp4TcphSetSocketOpt (i4Connid, REUSE_ADDR, BGP4_REUSEADDR_OPT_VAL)
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME,
                      "\tBgp4TcphOpenConnection() : Error in setting SOCKET "
                      "options.\n");
            CLOSE (i4Connid);
            return (BGP4_FAILURE);
        }

#ifdef HP_ADAPTED
        /* Set the socket option with the peers hoplimit */
        if (Bgp4TcphSetSocketOpt (i4Connid, HOPLIMIT,
                                  BGP4_PEER_HOPLIMIT (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphOpenConnection() : Error in "
                      "setting SOCKET options (HopLimit).\n");
        }
        /* Set the socket option with the peers recv-buf */
        if (Bgp4TcphSetSocketOpt (i4Connid, RECV_BUF,
                                  BGP4_PEER_RECVBUF (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphOpenConnection() : Error in "
                      "setting SOCKET options (RecvBuf).\n");
        }

        /* Set the socket option with the peers send-buf */
        if (Bgp4TcphSetSocketOpt (i4Connid, SEND_BUF,
                                  BGP4_PEER_SENDBUF (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphCheckIncomingConn() : Error in "
                      "setting SOCKET options (SendBuf).\n");
        }
#endif

        if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) == BGP4_TRUE)
        {
            /* Bind the local address configured for this peer */
            if (BIND (i4Connid, &Srcaddr, sizeof (struct sockaddr_in6)) ==
                BGP4_FAILURE)
            {
                CLOSE (i4Connid);
                return (BGP4_BIND_FAILURE);
            }
        }

        BGP4_PEER_CONN_ID (pPeerentry) = i4Connid;
        Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
        /* Set the TCP MD5 Option for the newly opened TCP connection */
        i4RetVal = Bgp4TcphMD5AuthOptSet (pPeerentry, BGP4_INV_CONN_ID);
        if (i4RetVal == BGP4_FAILURE)
        {
            CLOSE (i4Connid);
            Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_DELETE);
            BGP4_PEER_CONN_ID (pPeerentry) = BGP4_INV_CONN_ID;
            return BGP4_FAILURE;
        }
    }
    else
    {
        i4Connid = BGP4_PEER_CONN_ID (pPeerentry);
        if (BGP_FD_ISSET (i4Connid, BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID)))
        {
            return (BGP4_SUCCESS);    /* Connection already established */
        }
    }

    /* By default, EBGP peering can occur only if the
     * peers are in directly connected network. If the
     * ebgp-multihop feature is enabled for the peer,
     * then EBGP peering can even occur between peers
     * that are not in directly connected network. */
    u1PeerType = BGP4_GET_PEER_TYPE (BGP4_DFLT_VRFID, pPeerentry);
    if ((u1PeerType == BGP4_EXTERNAL_PEER) &&
        (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
    {                            /* External Peer */
        u4VrfId = BGP4_DFLT_VRFID;
#ifdef L3VPN
        if (BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER)
        {
            u4VrfId = BGP4_PEER_CXT_ID (pPeerentry);
        }
#endif
        if (Bgp4IsDirectlyConnected (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                                     u4VrfId) == BGP4_FALSE)
        {
            /* External Peer in different Subnet. Check for 
             * EBGP-multihop status for the peer. If enabled,
             * initiate the connection, else return */
            if (BGP4_PEER_EBGP_MULTIHOP (pPeerentry) ==
                BGP4_EBGP_MULTI_HOP_DISABLE)
            {
                /* EBGP Multihop disabled for the peer. */
                BGP4_SET_PEER_PEND_FLAG (pPeerentry,
                                         BGP4_PEER_MULTIHOP_PEND_START);
                return BGP4_FAILURE;
            }
        }
        else
        {
            /* Directly connected peer. */
            BGP4_RESET_PEER_PEND_FLAG (pPeerentry,
                                       BGP4_PEER_MULTIHOP_PEND_START);
        }
    }
    MEMCPY (BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID),
            BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID),
            sizeof (BGP_FD_SET_STRUCT));

    if (BGP_FD_ISSET (i4Connid, BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID)))
    {
        if (CONNECT (i4Connid, &Destaddr,
                     sizeof (struct sockaddr_in6)) == BGP4_FAILURE)
        {
            i4RetVal = Bgp4TcphCheckConnIsInProgress (i4Connid, pPeerentry);
            return (i4RetVal);
        }
        BGP_FD_CLR (i4Connid, BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID));
        BGP_FD_CLR (i4Connid, BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID));
    }
    else if (CONNECT (i4Connid, &Destaddr,
                      sizeof (struct sockaddr_in6)) == BGP4_FAILURE)
    {
        i4RetVal = Bgp4TcphCheckConnIsInProgress (i4Connid, pPeerentry);
        if (i4RetVal == BGP4_TCPH_CONN_IN_PROGRESS)
        {
            BGP_FD_SET (i4Connid, BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID));
        }
        return (i4RetVal);
    }

    /* Add to conn check list */
    BGP4_PEER_CONN_ID (pPeerentry) = i4Connid;
    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);

    /* for select */
    BGP_FD_SET (i4Connid, BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));

    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP ADDRESS 
     * IN THE PEERENTRY */
    Bgp4TcphFillAddresses (pPeerentry);

    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv6CheckIncomingConn                                */
/* Description   : This function checks for any new incoming connection.      */
/*                 If any new connection is present then the                  */
/*                 SEM changes for that peer will be initiated.               */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/* Note          : Since all the connection are opened in the Non-Blocking    */
/*                 mode, the incoming connections need to be                  */
/*                 checked periodically. This routine is called from the      */
/*                 BGP task main loop and will get executed for every one     */
/*                 second. This may need to be changed while porting.         */
/******************************************************************************/
INT4
Bgp4Tcphv6CheckIncomingConn ()
{
    tBgp4PeerEntry     *pPeerentry = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    struct sockaddr_in6 Destaddr;
    tAddrPrefix         PeerAddress;
    UINT4               u4Addrlen;
    UINT4               u4PeerIpaddr;
    INT4                i4Newconnid = BGP4_FAILURE;
    INT4                i4RetVal;
    UINT1               u1PrevState;
    UINT1               u1PeerType = 0;
    UINT4               u4VrfId;

    u4Addrlen = sizeof (struct sockaddr_in6);

    /* listen for incoming connections */
    if (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID) < 0)
    {
        return BGP4_SUCCESS;
    }

    if (BGP_FD_ISSET
        (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID),
         BGP4_READ_SOCK_FD_BITS (BGP4_DFLT_VRFID)))
    {
        BGP_FD_CLR (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID),
                    BGP4_READ_SOCK_FD_BITS (BGP4_DFLT_VRFID));
    }

    while ((i4Newconnid = ACCEPT (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID),
                                  &Destaddr, (&u4Addrlen))) != BGP4_FAILURE)
    {
        if (Bgp4TcphSetSocketOpt (i4Newconnid, REUSE_ADDR,
                                  BGP4_REUSEADDR_OPT_VAL) == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphCheckIncomingConn() : Error in"
                      "setting SOCKET options.\n");
            CLOSE (i4Newconnid);
            return BGP4_FAILURE;
        }

        Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV6);
        MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                Destaddr.sin6_addr.s6_addr, BGP4_IPV6_PREFIX_LEN);

        pPeerentry = Bgp4SnmphGetPeerEntry (PeerAddress);
        if (pPeerentry == NULL)
        {
            /* Check if Peer is IPV4 peer */
            BGP4_IN6_IS_ADDR_V4COMPATIBLE (PeerAddress.au1Address, i4RetVal);
            if (i4RetVal == BGP4_TRUE)
            {
                /* IPv4-IPv6 compatible route. Validate the IPv4 prefix */
                PTR_FETCH4 (u4PeerIpaddr,
                            &(PeerAddress.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                     BGP4_IPV4_PREFIX_LEN]));
                Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
                PTR_ASSIGN_4 (PeerAddress.au1Address, u4PeerIpaddr);
                pPeerentry = Bgp4SnmphGetPeerEntry (PeerAddress);
                if (pPeerentry == NULL)
                {
                    CLOSE (i4Newconnid);
                    return BGP4_SUCCESS;
                }
            }
            else
            {
                CLOSE (i4Newconnid);
                return BGP4_SUCCESS;
            }
        }

        /* By default, EBGP peering can occur only if the
         * peers are in directly connected network. If the
         * ebgp-multihop feature is enabled for the peer,
         * then EBGP peering can even occur between peers
         * that are not in directly connected network. */
        u1PeerType = BGP4_GET_PEER_TYPE (BGP4_DFLT_VRFID, pPeerentry);
        if ((u1PeerType == BGP4_EXTERNAL_PEER) &&
            (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
        {                        /* External Peer */
            u4VrfId = BGP4_DFLT_VRFID;
#ifdef L3VPN
            if (BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER)
            {
                u4VrfId = BGP4_PEER_CXT_ID (pPeerentry);
            }
#endif
            if (Bgp4IsDirectlyConnected
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                 u4VrfId) == BGP4_FALSE)
            {
                /* External Peer in different Subnet. Check for 
                 * EBGP-multihop status for the peer. If enabled,
                 * initiate the connection, else return */
                if (BGP4_PEER_EBGP_MULTIHOP (pPeerentry) ==
                    BGP4_EBGP_MULTI_HOP_DISABLE)
                {
                    /* EBGP Multihop disabled for the peer. */
                    BGP4_SET_PEER_PEND_FLAG (pPeerentry,
                                             BGP4_PEER_MULTIHOP_PEND_START);
                    CLOSE (i4Newconnid);
                    return BGP4_FAILURE;
                }
            }
            else
            {
                /* Directly connected peer. */
                BGP4_RESET_PEER_PEND_FLAG (pPeerentry,
                                           BGP4_PEER_MULTIHOP_PEND_START);
            }
        }

        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
             BGP4_PEER_DEINIT_INPROGRESS) ||
            ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
              BGP4_PEER_MP_CAP_CONFIG_PEND_START) ==
             BGP4_PEER_MP_CAP_CONFIG_PEND_START) ||
            ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
              BGP4_PEER_MP_CAP_RECV_PEND_START) ==
             BGP4_PEER_MP_CAP_RECV_PEND_START))
        {
            /* BGP speaker has not completely closed the previous connection.
             * So dont accept this connection. */
            CLOSE (i4Newconnid);
            return BGP4_SUCCESS;
        }
        /* Set the TCP MD5 Option for the newly opened TCP connection */
        i4RetVal = Bgp4TcphMD5AuthOptSet (pPeerentry, i4Newconnid);
        if (i4RetVal == BGP4_FAILURE)
        {
            CLOSE (i4Newconnid);
            return BGP4_FAILURE;
        }

        if (BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_START)
        {
            switch (BGP4_PEER_STATE (pPeerentry))
            {
                case BGP4_IDLE_STATE:
                    /* The peer has initiated a connection. This speaker is 
                     * administratively active but waiting for the
                     * start timer to expire. Need to accept this passive
                     * connection and initiate the peer initiation process. */
                    Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_CONNECT_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_CONNECT_STATE);
                case BGP4_CONNECT_STATE:
                case BGP4_ACTIVE_STATE:
                    /* Peer has initated a connection and this speaker is
                     * trying to establish one. Close the connection
                     * initiated by this speaker and accept the passive 
                     * connection. */
                    Bgp4SemhCloseParallelConn (pPeerentry);
                    if (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID)
                    {
                        u1PrevState = BGP4_PEER_STATE (pPeerentry);
                        Bgp4TcphCloseConnection (pPeerentry);
                        BGP4_PEER_STATE (pPeerentry) = u1PrevState;
                    }
                    BGP4_PEER_CONN_ID (pPeerentry) = i4Newconnid;
                    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
                    BGP_FD_SET (i4Newconnid,
                                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
                    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP
                       ADDRESS IN THE PEERENTRY */
                    if (Bgp4TcphFillAddresses (pPeerentry) == BGP4_FAILURE)
                    {
                        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_CLOSED);
                        return BGP4_FAILURE;
                    }
                    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);
                    break;

                case BGP4_OPENSENT_STATE:
                case BGP4_OPENCONFIRM_STATE:
                    pPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
                    if (pPeer == NULL)
                    {
                        pPeerentry = Bgp4SnmphClonePeerEntry (pPeerentry);
                        if (pPeerentry == NULL)
                        {
                            CLOSE (i4Newconnid);
                            break;
                        }
                    }
                    else
                    {
                        pPeerentry = pPeer;
                        if (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID)
                        {
                            Bgp4TcphCloseConnection (pPeerentry);
                        }
                    }
                    /* pPeerentry points to the cloned peer entry */
                    BGP4_PEER_CONN_ID (pPeerentry) = i4Newconnid;
                    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
                    BGP_FD_SET (i4Newconnid,
                                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
                    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP
                       ADDRESS IN THE PEERENTRY */
                    if (Bgp4TcphFillAddresses (pPeerentry) == BGP4_FAILURE)
                    {
                        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_CLOSED);
                        return BGP4_FAILURE;
                    }
                    Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
                    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);
                    break;

                case BGP4_ESTABLISHED_STATE:
                    CLOSE (i4Newconnid);
                    /* REJECT THE CONNECTION */
                default:
                    break;
            }
        }
        else
        {
            CLOSE (i4Newconnid);
        }
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv6FillAddresses                                    */
/* Description   : This function gets the relevant information from the       */
/*                 Connection ID of the peer and stores them in that          */
/*                 BGP Peer's information.                                    */
/* Input(s)      : BGP Peer information (pPeerentry).                         */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4Tcphv6FillAddresses (tBgp4PeerEntry * pPeerentry)
{
    struct sockaddr_in6 Sockaddr;
    tAddrPrefix         InvPrefix;
    UINT4               u4Addrlen;
    UINT4               u4PeerIpaddr;
    UINT1               au1LocalAddr[BGP4_IPV6_PREFIX_LEN] = { 0 };

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))
        == BGP4_INET_AFI_IPV4)
    {
        Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_INET_AFI_IPV6);
    }
    else
    {
        Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_INET_AFI_IPV4);
    }
    u4Addrlen = sizeof (struct sockaddr_in6);

    if (GETSOCKNAME (BGP4_PEER_CONN_ID (pPeerentry),
                     &Sockaddr, &u4Addrlen) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    BGP4_PEER_LOCAL_PORT (pPeerentry) = OSIX_NTOHS (Sockaddr.sin6_port);
    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) != BGP4_TRUE)
    {
        if (BGP4_AFI_IN_ADDR_PREFIX_INFO
            (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)) == BGP4_INET_AFI_IPV4)
        {
            Bgp4InitNetAddressStruct
                (&(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerentry)),
                 BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
            if (Sockaddr.sin6_flowinfo == 0)
            {
                MEMCPY (au1LocalAddr,
                        Sockaddr.sin6_addr.s6_addr, BGP4_IPV6_PREFIX_LEN);
                PTR_FETCH4 (u4PeerIpaddr,
                            &(au1LocalAddr[BGP4_IPV6_PREFIX_LEN -
                                           BGP4_IPV4_PREFIX_LEN]));
            }
            else
            {
                u4PeerIpaddr = OSIX_NTOHL (Sockaddr.sin6_flowinfo);
            }
            PTR_ASSIGN_4 ((BGP4_PEER_LOCAL_ADDR (pPeerentry)), u4PeerIpaddr);
        }
        else
        {
            Bgp4InitNetAddressStruct
                (&(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerentry)),
                 BGP4_INET_AFI_IPV6, BGP4_INET_SAFI_UNICAST);
            MEMCPY (BGP4_PEER_LOCAL_ADDR (pPeerentry),
                    Sockaddr.sin6_addr.s6_addr, BGP4_IPV6_PREFIX_LEN);
        }
    }

    if (GETPEERNAME (BGP4_PEER_CONN_ID (pPeerentry),
                     &Sockaddr, &u4Addrlen) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* Also Update the Peer's Local Address Prefix Length. */
    BGP4_PEER_LOCAL_NETADDR_PREFIXLEN (pPeerentry) =
        Bgp4GetNetAddrPrefixLen (BGP4_PEER_LOCAL_ADDR_INFO (pPeerentry));

    /* Also Update the Peer's LCL Address Prefix Length also if the 
     * Peer's Network Address is configured. */
    if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_NETWORK_ADDR_INFO (pPeerEntry)),
                 BGP4_MAX_INET_ADDRESS_LEN)) != 0)
    {
        /* Network Address is configured for this Peer. */
        BGP4_PEER_LCL_NETADDR_PREFIXLEN (pPeerentry) =
            Bgp4GetNetAddrPrefixLen (BGP4_PEER_LCL_ADDR_INFO (pPeerentry));
    }

    BGP4_PEER_REMOTE_PORT (pPeerentry) = OSIX_NTOHS (Sockaddr.sin6_port);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv6OpenListenPort                                   */
/* Description   : Whenever the BGP Global Admin is made UP, this function    */
/*                 is called to open a Listen Socket. On the BGP Task main    */
/*                 loop this socket is checked for new incoming connections.  */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Tcphv6OpenListenPort ()
{
    INT4                i4Connid;
    struct sockaddr_in6 Destaddr;

    i4Connid = SOCKET (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (i4Connid == BGP4_FAILURE)
    {
        return (BGP4_FAILURE);
    }

    if (Bgp4TcphSetSocketOpt (i4Connid, REUSE_ADDR, BGP4_REUSEADDR_OPT_VAL)
        == BGP4_FAILURE)
    {
        CLOSE (i4Connid);
        return (BGP4_FAILURE);
    }

    MEMSET (&Destaddr, 0, sizeof (struct sockaddr_in6));
    Destaddr.sin6_family = AF_INET6;
    Destaddr.sin6_port = OSIX_HTONS (BGP4_DEF_LSNPORT);

    if (BIND (i4Connid, &Destaddr, sizeof (struct sockaddr_in6)) ==
        BGP4_FAILURE)
    {
        CLOSE (i4Connid);
        return (BGP4_FAILURE);
    }

    BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID) = i4Connid;
    LISTEN (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID), BGP4_LISTEN_PEER_SIZE);

    BGP_FD_SET (BGP4_LOCAL_V6_LISTEN_CONN (BGP4_DFLT_VRFID),
                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
    return (BGP4_SUCCESS);
}
#endif

#ifdef BGP_TCP4_WANTED
/******************************************************************************/
/* Function Name : Bgp4Tcphv4OpenConnection                                   */
/* Description   : This function interacts with the lower layer namely SLI    */
/*                 (Socket Layer Interface) to open a TCP connection with     */
/*                 the specified peer. Depending on the result of the         */
/*                 connection proper message is enqueued in BGP task queue.   */
/* Input(s)      : Peer to which connection needs to be established           */
/*                (pPeerentry)                                               */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Tcphv4OpenConnection (tBgp4PeerEntry * pPeerentry)
{
    struct sockaddr_in  Destaddr;
    struct sockaddr_in  Srcaddr;
    UINT4               u4RemAddr;
    INT4                i4Connid;
    INT4                i4RetVal;
    UINT1               u1PeerType = 0;
    UINT4               u4VrfId;

    if (BGP4_PEER_CONN_PASSIVE (pPeerentry) == BGP4_TRUE)
    {
        return BGP4_PASSIVE_SET;
    }

    MEMSET (&Destaddr, 0, sizeof (struct sockaddr_in));
    Destaddr.sin_family = AF_INET;
    Destaddr.sin_port = OSIX_HTONS (BGP4_LOCAL_LISTEN_PORT (BGP4_DFLT_VRFID));
    PTR_FETCH4 (u4RemAddr, BGP4_PEER_REMOTE_ADDR (pPeerentry));
    Destaddr.sin_addr.s_addr = (u4RemAddr);

    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) == BGP4_TRUE)
    {
        /* Get the local address configured for this peer */
        MEMSET (&Srcaddr, 0, sizeof (struct sockaddr_in));
        Srcaddr.sin_family = AF_INET;
        PTR_FETCH4 (u4RemAddr, BGP4_PEER_LOCAL_ADDR (pPeerentry));
        Srcaddr.sin_addr.s_addr = (u4RemAddr);
    }

    if (BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_STOP)
    {
        return BGP4_FAILURE;
    }

    if (BGP4_PEER_CONN_ID (pPeerentry) == BGP4_INV_CONN_ID)
    {

        i4Connid = SOCKET (AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (i4Connid == BGP4_FAILURE)
        {
            return (BGP4_FAILURE);
        }

        if (Bgp4TcphSetSocketOpt (i4Connid, REUSE_ADDR, BGP4_REUSEADDR_OPT_VAL)
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME,
                      "\tBgp4TcphOpenConnection() : Error in setting SOCKET "
                      "options.\n");
            CLOSE (i4Connid);
            return (BGP4_FAILURE);
        }
#ifdef HP_ADAPTED
        /* Set the socket option with the peers hoplimit */
        if (Bgp4TcphSetSocketOpt (i4Connid, HOPLIMIT,
                                  BGP4_PEER_HOPLIMIT (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphOpenConnection() : Error in "
                      "setting SOCKET options (HopLimit).\n");
        }
        /* Set the socket option with the peers recv-buf */
        if (Bgp4TcphSetSocketOpt (i4Connid, RECV_BUF,
                                  BGP4_PEER_RECVBUF (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphOpenConnection() : Error in "
                      "setting SOCKET options (RecvBuf).\n");
        }

        /* Set the socket option with the peers send-buf */
        if (Bgp4TcphSetSocketOpt (i4Connid, SEND_BUF,
                                  BGP4_PEER_SENDBUF (pPeerentry))
            == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphCheckIncomingConn() : Error in "
                      "setting SOCKET options (SendBuf).\n");
        }
#endif
        if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) == BGP4_TRUE)
        {
            /* Bind the local address configured for this peer */
            if (BIND (i4Connid, &Srcaddr, sizeof (struct sockaddr_in)) ==
                BGP4_FAILURE)
            {
                CLOSE (i4Connid);
                return (BGP4_BIND_FAILURE);
            }
        }

        BGP4_PEER_CONN_ID (pPeerentry) = i4Connid;
        Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
        /* Set the TCP MD5 Option for the newly opened TCP connection */
        i4RetVal = Bgp4TcphMD5AuthOptSet (pPeerentry, BGP4_INV_CONN_ID);
        if (i4RetVal == BGP4_FAILURE)
        {
            CLOSE (i4Connid);
            Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_DELETE);
            BGP4_PEER_CONN_ID (pPeerentry) = BGP4_INV_CONN_ID;
            return BGP4_FAILURE;
        }
    }
    else
    {
        i4Connid = BGP4_PEER_CONN_ID (pPeerentry);
        if (BGP_FD_ISSET (i4Connid, BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID)))
        {
            return (BGP4_SUCCESS);    /* Connection already established */
        }
    }

    /* By default, EBGP peering can occur only if the
     * peers are in directly connected network. If the
     * ebgp-multihop feature is enabled for the peer,
     * then EBGP peering can even occur between peers
     * that are not in directly connected network. */
    u1PeerType = BGP4_GET_PEER_TYPE (BGP4_DFLT_VRFID, pPeerentry);
    if ((u1PeerType == BGP4_EXTERNAL_PEER) &&
        (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
    {                            /* External Peer */
        u4VrfId = BGP4_DFLT_VRFID;
#ifdef L3VPN
        if (BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER)
        {
            u4VrfId = BGP4_PEER_CXT_ID (pPeerentry);
        }
#endif
        if (Bgp4IsDirectlyConnected (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                                     u4VrfId) == BGP4_FALSE)
        {
            /* External Peer in different Subnet. Check for 
             * EBGP-multihop status for the peer. If enabled,
             * initiate the connection, else return */
            if (BGP4_PEER_EBGP_MULTIHOP (pPeerentry) ==
                BGP4_EBGP_MULTI_HOP_DISABLE)
            {
                /* EBGP Multihop disabled for the peer. */
                BGP4_SET_PEER_PEND_FLAG (pPeerentry,
                                         BGP4_PEER_MULTIHOP_PEND_START);
                return BGP4_FAILURE;
            }
        }
        else
        {
            /* Directly connected peer. */
            BGP4_RESET_PEER_PEND_FLAG (pPeerentry,
                                       BGP4_PEER_MULTIHOP_PEND_START);
        }
    }
    MEMCPY (BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID),
            BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID),
            sizeof (BGP_FD_SET_STRUCT));

    if (BGP_FD_ISSET (i4Connid, BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID)))
    {
        if (CONNECT (i4Connid, &Destaddr,
                     sizeof (struct sockaddr_in)) == BGP4_FAILURE)
        {
            i4RetVal = Bgp4TcphCheckConnIsInProgress (i4Connid, pPeerentry);
            return (i4RetVal);
        }
        BGP_FD_CLR (i4Connid, BGP4_WRITE_SOCK_FD_BITS (BGP4_DFLT_VRFID));
        BGP_FD_CLR (i4Connid, BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID));
    }
    else if (CONNECT (i4Connid, &Destaddr,
                      sizeof (struct sockaddr_in)) == BGP4_FAILURE)
    {
        i4RetVal = Bgp4TcphCheckConnIsInProgress (i4Connid, pPeerentry);
        if (i4RetVal == BGP4_TCPH_CONN_IN_PROGRESS)
        {
            BGP_FD_SET (i4Connid, BGP4_WRITE_SOCK_FD_SET (BGP4_DFLT_VRFID));
        }
        return (i4RetVal);
    }

    /* Add to conn check list */
    BGP4_PEER_CONN_ID (pPeerentry) = i4Connid;
    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);

    /* for select */
    BGP_FD_SET (i4Connid, BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));

    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP ADDRESS 
     * IN THE PEERENTRY */
    Bgp4TcphFillAddresses (pPeerentry);
    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv4CheckIncomingConn                                */
/* Description   : This function checks for any new incoming connection.      */
/*                 If any new connection is present then the                  */
/*                 SEM changes for that peer will be initiated.               */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/* Note          : Since all the connection are opened in the Non-Blocking    */
/*                 mode, the incoming connections need to be                  */
/*                 checked periodically. This routine is called from the      */
/*                 BGP task main loop and will get executed for every one     */
/*                 second. This may need to be changed while porting.         */
/******************************************************************************/
INT4
Bgp4Tcphv4CheckIncomingConn ()
{
    tBgp4PeerEntry     *pPeerentry = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    struct sockaddr_in  Destaddr;
    tAddrPrefix         PeerAddress;
    UINT4               u4Addrlen;
    INT4                i4Newconnid = BGP4_FAILURE;
    INT4                i4RetVal;
    UINT1               u1PrevState;
    UINT1               u1PeerType = 0;
    UINT4               u4VrfId;

    u4Addrlen = sizeof (struct sockaddr_in);

    /* listen for incoming connections */
    if (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID) < 0)
    {
        return BGP4_SUCCESS;
    }

    if (BGP_FD_ISSET
        (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID),
         BGP4_READ_SOCK_FD_BITS (BGP4_DFLT_VRFID)))
    {
        BGP_FD_CLR (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID),
                    BGP4_READ_SOCK_FD_BITS (BGP4_DFLT_VRFID));
    }

    while ((i4Newconnid = ACCEPT (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID),
                                  &Destaddr, (&u4Addrlen))) != BGP4_FAILURE)
    {
        if (Bgp4TcphSetSocketOpt (i4Newconnid, REUSE_ADDR,
                                  BGP4_REUSEADDR_OPT_VAL) == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                      BGP4_MOD_NAME, "\tBgp4TcphCheckIncomingConn() : Error in"
                      "setting SOCKET options.\n");
            CLOSE (i4Newconnid);
            return BGP4_FAILURE;
        }

        Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
        PTR_ASSIGN4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                     (Destaddr.sin_addr.s_addr));

        pPeerentry = Bgp4SnmphGetPeerEntry (PeerAddress);
        if (pPeerentry == NULL)
        {
            CLOSE (i4Newconnid);
            return BGP4_SUCCESS;
        }

        /* By default, EBGP peering can occur only if the
         * peers are in directly connected network. If the
         * ebgp-multihop feature is enabled for the peer,
         * then EBGP peering can even occur between peers
         * that are not in directly connected network. */
        u1PeerType = BGP4_GET_PEER_TYPE (BGP4_DFLT_VRFID, pPeerentry);
        if ((u1PeerType == BGP4_EXTERNAL_PEER) &&
            (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
        {                        /* External Peer */
            u4VrfId = BGP4_DFLT_VRFID;
#ifdef L3VPN
            if (BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER)
            {
                u4VrfId = BGP4_PEER_CXT_ID (pPeerentry);
            }
#endif
            if (Bgp4IsDirectlyConnected
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                 u4VrfId) == BGP4_FALSE)
            {
                /* External Peer in different Subnet. Check for 
                 * EBGP-multihop status for the peer. If enabled,
                 * initiate the connection, else return */
                if (BGP4_PEER_EBGP_MULTIHOP (pPeerentry) ==
                    BGP4_EBGP_MULTI_HOP_DISABLE)
                {
                    /* EBGP Multihop disabled for the peer. */
                    BGP4_SET_PEER_PEND_FLAG (pPeerentry,
                                             BGP4_PEER_MULTIHOP_PEND_START);
                    CLOSE (i4Newconnid);
                    return BGP4_FAILURE;
                }
            }
            else
            {
                /* Directly connected peer. */
                BGP4_RESET_PEER_PEND_FLAG (pPeerentry,
                                           BGP4_PEER_MULTIHOP_PEND_START);
            }
        }
        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
             BGP4_PEER_DEINIT_INPROGRESS) ||
            ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
              BGP4_PEER_MP_CAP_CONFIG_PEND_START) ==
             BGP4_PEER_MP_CAP_CONFIG_PEND_START) ||
            ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
              BGP4_PEER_MP_CAP_RECV_PEND_START) ==
             BGP4_PEER_MP_CAP_RECV_PEND_START))
        {
            /* BGP speaker has not completely closed the previous connection.
             * So dont accept this connection. */
            CLOSE (i4Newconnid);
            return BGP4_SUCCESS;
        }

        /* Set the TCP MD5 Option for the newly opened TCP connection */
        i4RetVal = Bgp4TcphMD5AuthOptSet (pPeerentry, i4Newconnid);
        if (i4RetVal == BGP4_FAILURE)
        {
            CLOSE (i4Newconnid);
            return BGP4_FAILURE;
        }

        if (BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_START)
        {
            switch (BGP4_PEER_STATE (pPeerentry))
            {
                case BGP4_IDLE_STATE:
                    /* The peer has initiated a connection. This speaker is 
                     * administratively active but waiting for the
                     * start timer to expire. Need to accept this passive
                     * connection and initiate the peer initiation process. */
                    Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_CONNECT_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_CONNECT_STATE);
                case BGP4_CONNECT_STATE:
                case BGP4_ACTIVE_STATE:
                    /* Peer has initated a connection and this speaker is
                     * trying to establish one. Close the connection
                     * initiated by this speaker and accept the passive 
                     * connection. */
                    Bgp4SemhCloseParallelConn (pPeerentry);
                    if (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID)
                    {
                        u1PrevState = BGP4_PEER_STATE (pPeerentry);
                        Bgp4TcphCloseConnection (pPeerentry);
                        BGP4_PEER_STATE (pPeerentry) = u1PrevState;
                    }
                    BGP4_PEER_CONN_ID (pPeerentry) = i4Newconnid;
                    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
                    BGP_FD_SET (i4Newconnid,
                                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
                    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP
                       ADDRESS IN THE PEERENTRY */
                    if (Bgp4TcphFillAddresses (pPeerentry) == BGP4_FAILURE)
                    {
                        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_CLOSED);
                        return BGP4_FAILURE;
                    }
                    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);
                    break;

                case BGP4_OPENSENT_STATE:
                case BGP4_OPENCONFIRM_STATE:
                    pPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
                    if (pPeer == NULL)
                    {
                        pPeerentry = Bgp4SnmphClonePeerEntry (pPeerentry);
                        if (pPeerentry == NULL)
                        {
                            CLOSE (i4Newconnid);
                            break;
                        }
                    }
                    else
                    {
                        pPeerentry = pPeer;
                        if (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID)
                        {
                            Bgp4TcphCloseConnection (pPeerentry);
                        }
                    }
                    /* pPeerentry points to the cloned peer entry */
                    BGP4_PEER_CONN_ID (pPeerentry) = i4Newconnid;
                    Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);
                    BGP_FD_SET (i4Newconnid,
                                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
                    /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP
                       ADDRESS IN THE PEERENTRY */
                    if (Bgp4TcphFillAddresses (pPeerentry) == BGP4_FAILURE)
                    {
                        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_CLOSED);
                        return BGP4_FAILURE;
                    }
                    Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
                    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);
                    break;

                case BGP4_ESTABLISHED_STATE:
                    CLOSE (i4Newconnid);
                    /* REJECT THE CONNECTION */
                default:
                    break;
            }
        }
        else
        {
            CLOSE (i4Newconnid);
        }
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv4FillAddresses                                    */
/* Description   : This function gets the relevant information from the       */
/*                 Connection ID of the peer and stores them in that          */
/*                 BGP Peer's information.                                    */
/* Input(s)      : BGP Peer information (pPeerentry).                         */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4Tcphv4FillAddresses (tBgp4PeerEntry * pPeerentry)
{
    struct sockaddr_in  Sockaddr;
    tAddrPrefix         InvPrefix;
    UINT4               u4Addrlen;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_INET_AFI_IPV6);
    u4Addrlen = sizeof (struct sockaddr_in);

    if (GETSOCKNAME (BGP4_PEER_CONN_ID (pPeerentry),
                     &Sockaddr, &u4Addrlen) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    BGP4_PEER_LOCAL_PORT (pPeerentry) = OSIX_NTOHS (Sockaddr.sin_port);
    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) != BGP4_TRUE)
    {
        Bgp4InitNetAddressStruct (&(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerentry)),
                                  BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
        PTR_ASSIGN4 (BGP4_PEER_LOCAL_ADDR (pPeerentry),
                     (Sockaddr.sin_addr.s_addr));
    }

    if (GETPEERNAME (BGP4_PEER_CONN_ID (pPeerentry),
                     &Sockaddr, &u4Addrlen) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* Also Update the Peer's Local Address Prefix Length. */
    BGP4_PEER_LOCAL_NETADDR_PREFIXLEN (pPeerentry) =
        Bgp4GetNetAddrPrefixLen (BGP4_PEER_LOCAL_ADDR_INFO (pPeerentry));

    /* Also Update the Peer's LCL Address Prefix Length also if the 
     * Peer's Network Address is configured. */
    if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_NETWORK_ADDR_INFO (pPeerEntry)),
                 BGP4_MAX_INET_ADDRESS_LEN)) != 0)
    {
        /* Network Address is configured for this Peer. */
        BGP4_PEER_LCL_NETADDR_PREFIXLEN (pPeerentry) =
            Bgp4GetNetAddrPrefixLen (BGP4_PEER_LCL_ADDR_INFO (pPeerentry));
    }

    BGP4_PEER_REMOTE_PORT (pPeerentry) = OSIX_NTOHS (Sockaddr.sin_port);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4Tcphv4OpenListenPort                                   */
/* Description   : Whenever the BGP Global Admin is made UP, this function    */
/*                 is called to open a Listen Socket. On the BGP Task main    */
/*                 loop this socket is checked for new incoming connections.  */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Tcphv4OpenListenPort ()
{
    INT4                i4Connid;
    struct sockaddr_in  Destaddr;

    i4Connid = SOCKET (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Connid == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME, "\tCreating Socket For BGP Server FAILS!!!");
        return (BGP4_FAILURE);
    }

    if (Bgp4TcphSetSocketOpt (i4Connid, REUSE_ADDR, BGP4_REUSEADDR_OPT_VAL)
        == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME, "\tSetting Option for BGP Server FAILS!!!");
        CLOSE (i4Connid);
        return (BGP4_FAILURE);
    }

#ifdef HP_ADAPTED
    if (Bgp4TcphSetSocketOpt (i4Connid, REUSE_PORT, BGP4_REUSEADDR_OPT_VAL)
        == BGP4_FAILURE)
    {
        CLOSE (i4Connid);
        return (BGP4_FAILURE);
    }
#endif

    MEMSET (&Destaddr, 0, sizeof (struct sockaddr_in));
    Destaddr.sin_family = AF_INET;
    Destaddr.sin_port = OSIX_HTONS (BGP4_DEF_LSNPORT);
    Destaddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (BIND (i4Connid, &Destaddr, sizeof (struct sockaddr_in)) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME, "\tBind Address For BGP Server FAILS!!!");
        CLOSE (i4Connid);
        return (BGP4_FAILURE);
    }

    BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID) = i4Connid;
    LISTEN (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID), BGP4_LISTEN_PEER_SIZE);

    BGP_FD_SET (BGP4_LOCAL_V4_LISTEN_CONN (BGP4_DFLT_VRFID),
                BGP4_READ_SOCK_FD_SET (BGP4_DFLT_VRFID));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
              "\tBGP Server OPENED Successfully!!!");
    return (BGP4_SUCCESS);
}
#endif

/******************************************************************************/
/* Function Name : Bgp4TcphAuthOptionMktSet                                      */
/* Description   : Whenever a MKT  configuration/removal is done, this  */
/*                 function is called to set TCP MD5 auth option on the listen*/
/*                 socket to add/remove password for the peer                 */
/* Input(s)      : Peer Information ( pPeerInfo )                             */
/*                 Passwors string ( pPassword )                              */
/*                 Password length ( u1Keylen )                               */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

INT4
Bgp4TcphAuthOptionMktSet (tBgp4PeerEntry * pPeerentry,
                          tTcpAoMktAddr * pTcpAoMktAdr)
{

    if (pPeerentry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME, "tcph:Error in TCP-AO socket options\n ");
    UNUSED_PARAM (pTcpAoMktAdr);
    return BGP4_FAILURE;

}

INT4
Bgp4TcpAoGetMktInUse (UINT4 u4ContextId, tAddrPrefix PeerAddress,
                      INT4 *pu4KeyIdInUse)
{

    tBgp4PeerEntry     *pPeerentry;

    pPeerentry = Bgp4SnmphGetPeerEntry (u4ContextId, PeerAddress);

    if (pPeerentry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME, "tcph:Error in TCP-AO  Get MKT In Use\n");

    UNUSED_PARAM (pu4KeyIdInUse);
    return BGP4_FAILURE;

}

INT4
Bgp4TcpAoNeighCfgSet (tBgp4PeerEntry * pPeerentry,
                      tTcpAoNeighCfg * pTcpAoNeighCfg, INT4 i4OptName)
{
    if (pPeerentry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME, "tcph:Error in TCP-AO  Neighbor Config\n");
    UNUSED_PARAM (i4OptName);
    UNUSED_PARAM (pTcpAoNeighCfg);
    return BGP4_FAILURE;
}

INT4
Bgp4TcpAoMktSet (tBgp4PeerEntry * pPeerentry, tTcpAoAuthMKT * pMkt)
{
    if (pPeerentry == NULL)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME,
              "tcph:Error in TCP-AO socket options for connected peer\n ");
    UNUSED_PARAM (pMkt);

    return BGP4_FAILURE;
}

INT4
Bgp4TcpAoIcmpCfgSet (tBgp4PeerEntry * pPeerentry, INT1 i1IcmpCfg)
{
    if (pPeerentry == NULL)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME,
              "tcph:Error in TCP-AO icmp config for connected peer\n ");
    UNUSED_PARAM (i1IcmpCfg);
    return BGP4_FAILURE;
}

INT4
Bgp4TcpAoPktDiscCfg (tBgp4PeerEntry * pPeerentry, INT1 i1PktDiscCfg)
{
    if (pPeerentry == NULL)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME,
              "tcph:Error in TCP-AO Packet Discard config for connected peer\n ");
    UNUSED_PARAM (i1PktDiscCfg);

    return BGP4_FAILURE;
}

INT4
Bgp4TcpAoGetAuthStatus (tBgp4PeerEntry * pPeerentry, INT4 *pu4TcpAoAuthSts)
{
    if (pPeerentry == NULL)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\t No matching peer exist!!!\n");

        return BGP4_FAILURE;
    }

    /* Option not supported - Return FAILURE. */
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME,
              "tcph:Error in Get TCP-AO Auth for connected peer\n ");

    UNUSED_PARAM (pu4TcpAoAuthSts);
    return BGP4_FAILURE;
}
#endif /* BGPVXTCP_C */
