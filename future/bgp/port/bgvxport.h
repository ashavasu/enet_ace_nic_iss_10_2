/********************************************************************
 * Copyright (C) Future Software,1997-98,2001
 *
 * $Id: bgvxport.h,v 1.14 2014/06/24 12:46:57 siva Exp $
 *
 * Description:This file includes all the BGP based header files.          
 *
 *******************************************************************/
#ifndef BGVXPORT_H
#define BGVXPORT_H

#include "fssocket.h"
#include <net/mbuf.h>
#include <net/socketvar.h>
#include <socket.h>
#include <types.h>
#include <sockLib.h>
#include <routeLib.h>
#include <netShow.h>
#include <inetLib.h>
#include <ioLib.h>
#include <selectLib.h>
#include <m2Lib.h>
#include <unistd.h>
#include <ifLib.h>
#include "bgp4dfs.h"
#include "bgp4tdfs.h"
#include "bgp4rib.h"
#include "bgp4port.h"

extern VOID SNMP_TSK_Initialize_Protocol (VOID);
INT4  Bgp4SysMain (VOID);
INT4  SystemStart (VOID);

/* Defined variable to store the returned
 * values of VxWorks SLI calls             */
#ifdef  BGVXSLI_C
INT4                i4VxwTmp = BGP4_FAILURE;
#else
extern INT4                i4VxwTmp;
#endif

#define  VXWORKS_FAILURE    ERROR
#define  VXWORKS_SUCCESS    OK
#define  MASK_SHIFT_LEN     8
#define  DEFAULT_MASK_LEN   1

/* Declaration for other protocols supported by BGP -
 * ??? This needs to be updated as per VxWorks 
 * Protocol Value. */
#define  BGP4_LOCAL_ID      1 
#define  BGP4_STATIC_ID     2 
#define  BGP4_RIP_ID        3 
#define  BGP4_OSPF_ID       4
#define  BGP4_ISIS_ID       5
#define  BGP4_OTHERS_ID     255

/* Macros used for packing the structures*/
#define ATTR_PACK               __attribute__ ((packed))

#endif /* BGVXPORT_H */
