 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4main.c,v 1.113 2018/01/03 11:31:18 siva Exp $
 *
 * Description: This file contains the main routine for BGP
 *
 *******************************************************************/
#ifndef BGP4MAIN_C
#define BGP4MAIN_C

#include "bgp4com.h"

#ifdef SNMP_2_WANTED
#include "stdbgpwr.h"
#include "fsbgp4wr.h"
#endif /* SNMP_2_WANTED */

#ifdef SNMP_WANTED
#include "include.h"
#include "fsbgplow.h"
#include "stdbglow.h"

INT4 RegisterStdBGPwithFutureSNMP PROTO ((void));
INT4 RegisterFSBGPwithFutureSNMP PROTO ((void));
extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));
#endif /* SNMP_WANTED */
extern INT1         gi1SyncStatus;
INT1                gi1RFDSyncStatus = 0;

#ifdef ROUTEMAP_WANTED
PRIVATE VOID        Bgp4ProcessRMapHandler (tBgp4QMsg *);
#endif /* ROUTEMAP_WANTED */

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxip.h"
#endif

/****************************************************************************
 * FUNCTION NAME : Bgp4TaskInitializeProtocol
 * DESCRIPTION   : This function is called to create the BGP task 
 * INPUTS        : None
 * OUTPUTS       : None.
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE.
 *****************************************************************************/
INT4
Bgp4TaskInitializeProtocol (void)
{
    INT4                i4RetVal;

    /* Initialize the BGP task */
    BGP4_TASK_ID = BGP4_INACTIVE;
    i4RetVal = OsixTskCrt ((UINT1 *) BGP4_TASKNAME,
                           BGP4_TASK_PRIORITY | OSIX_SCHED_RR,
                           BGP4_TASK_DEF_DEPTH,
                           (OsixTskEntry) Bgp4TaskMain, 0, &(BGP4_TASK_ID));

    if (i4RetVal == OSIX_SUCCESS)
    {
        return (BGP4_SUCCESS);
    }
    return (BGP4_FAILURE);
}

/****************************************************************************
 * FUNCTION NAME : Bgp4TaskMain
 * DESCRIPTION   : This is the main function called whenever the BGP Task gets 
 *                 created.It initializes the BGP system and allocates all 
 *                 memory needed for startup. The task waits for events in the 
 *                 main-loop in this function 
 * INPUTS        : task input params (none)   
 * OUTPUTS       : None.
 * RETURNS       : None.
 *****************************************************************************/
PUBLIC VOID
Bgp4TaskMain (INT1 *pi1TaskParam)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT4               u4events = 0;
    UINT4               u4Flags;
    INT4                i4RetVal;
    UINT4               u4Retval = 0;
    UINT1               au1Qname[BGP4_Q_NAME_SIZE] = BGP4_QUE;

    UNUSED_PARAM (pi1TaskParam);

    BgpSetContextId (BGP4_INVALID_CONTEXT_ID);

    /* Initialize the BGP task */
    i4RetVal = Bgp4Init ();
    if (i4RetVal == BGP4_SUCCESS)
    {
        BGP4_TASK_INIT_STATUS = BGP4_TRUE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_INIT_SHUT_TRC,
                  BGP4_MOD_NAME, "\tBgp4TaskMain() : BGP Init SUCCESS\n");
        BGP_INIT_COMPLETE (OSIX_SUCCESS);
        if (OsixTskIdSelf (&BGP4_TASK_ID) != OSIX_SUCCESS)
        {
            BGP_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
#ifdef SNMP_2_WANTED
        /* Register the protocol MIBs with SNMP */
        RegisterFSBGP4 ();
        RegisterSTDBGP ();
        RegisterFSMPBG ();
#endif
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tBgp4TaskMain() : BGP Init Failed.\n");
        BGP4_TASK_INIT_STATUS = BGP4_FALSE;
        Bgp4DeInit ();
        /* Indicate the status of initialization to the main routine */
        BGP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Indicate ISS module regarding module start */
    IssSetModuleSystemControl (BGP_MODULE_ID, MODULE_START);

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_BGP4, Bgp4SendRouteMapUpdateMsg);
#endif /* ROUTEMAP_WANTED */
    /* Registration with VCM */
    Bgp4RegisterWithVcm ();
#ifdef L3VPN
    Bgp4RegisterWithMpls ();
#endif
#ifdef EVPN_WANTED
    Bgp4RegisterWithVxlan ();
#endif

    BGP4_SYSLOG_ID = (UINT4) SYS_LOG_REGISTER ((UINT1 *) BGP4_TASKNAME,
                                               SYSLOG_CRITICAL_LEVEL);
    u4Flags = OSIX_WAIT | OSIX_EV_ANY;
    for (;;)
    {
        u4events = 0;

        u4Retval =
            OsixEvtRecv (BGP4_TASK_ID, BGP4_INPUT_Q_EVENT | BGP4_1S_TMR_EVENT |
                         BGP4_TMR_EVENT | BGP4_RM_EVENT, u4Flags, &u4events);

        UNUSED_PARAM (u4Retval);
        if (u4events & BGP4_TMR_EVENT)
        {
            BgpLock ();
            Bgp4TmrhProcessTimeout ();
            BgpUnLock ();
        }

        if (u4events & BGP4_INPUT_Q_EVENT)
        {
            /* BgpLock (); */
            Bgp4ProcessQMsg (au1Qname);
            /*  BgpUnLock (); */
        }
        if (u4events & BGP4_RM_EVENT)
        {
            BgpLock ();
            Bgp4RedHandleRmEvents (BGP4_TRUE);
            BgpUnLock ();
        }

        if (u4events & BGP4_1S_TMR_EVENT)
        {
            /* One second timer is expired, hence the timer node can be
             * removed from the timer list and then the timer can be
             * started.
             */
            pExpiredTimers = TmrGetNextExpiredTimer (BGP4_1S_TIMER_LISTID);
            if (NULL != pExpiredTimers)
            {
                Bgp4OneSecTimerExpiry (pExpiredTimers);
            }

            /* Bgp4Dispatcher should be called at end of the
             * one second timer expiration. This is the bgp main
             * process that schedules and executes all the BGP operations. */
            BgpLock ();
            Bgp4Dispatcher ();
            BgpUnLock ();
        }

    }                            /* end while */
}

/*************************************************************************/
/*  Function Name   : Bgp4RegisterWithVcm                                 */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Registers with VCM for handling context creation   */
/*                    and deletion                                       */
/*************************************************************************/
INT4
Bgp4RegisterWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = BgpNotifyContextChange;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = BGP_PROTOCOL_ID;
    if (VCM_SUCCESS == VcmRegisterHLProtocol (&VcmRegInfo))
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/*************************************************************************/
/*  Function Name   : Bgp4NotifyContextChange                             */
/*  Input(s)        : u4IfIndex - interface index                        */
/*                    u4ContextId - context id                           */
/*                    u1Event - Context creation/deletion event          */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Sends context creation/ deletion messages to the   */
/*                    tcp task;                                          */
/*************************************************************************/
VOID
BgpNotifyContextChange (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{

    tBgp4QMsg          *pQMsg = NULL;

    UNUSED_PARAM (u4IfIndex);
    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Peer Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_VCM_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    pQMsg->QMsg.Bgp4VcmMsg.u4ContextId = u4ContextId;
    pQMsg->QMsg.Bgp4VcmMsg.u1Event = u1Event;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "Bgp4Enqueue failed for context creation event\n");
        return;
    }

    return;
}

/*************************************************************************/
/*  Function Name   : Bgp4CreateContext                                   */
/*  Input(s)        : u4ContextId - Context Id                           */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Creates the Bgp information for the context and    */
/*                    and registers with lower layers for that context   */
/*************************************************************************/
INT4
Bgp4CreateContext (UINT4 u4CxtId)
{
    if (u4CxtId >=
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Invalid context Id %d obtained !!!\n", u4CxtId);
        return BGP4_FAILURE;
    }
    if (gBgpCxtNode[u4CxtId] != NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Context %d already exist!!!\n", u4CxtId);
        return BGP4_FAILURE;
    }
    gBgpCxtNode[u4CxtId] =
        (tBgpCxtNode *) MemAllocMemBlk (gBgpNode.BgpCxtMemPoolId);
    if (gBgpCxtNode[u4CxtId] == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Context Node allocation failed!!!\n");
        return BGP4_FAILURE;
    }
    MEMSET (gBgpCxtNode[u4CxtId], 0, sizeof (tBgpCxtNode));
    if (Bgp4InitContextGlobals (u4CxtId) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Context initialization failed!!!\n");
        MemReleaseMemBlock (gBgpNode.BgpCxtMemPoolId,
                            (UINT1 *) gBgpCxtNode[u4CxtId]);
        gBgpCxtNode[u4CxtId] = NULL;
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*************************************************************************/
/*  Function Name   : Bgp4DeleteContext                                   */
/*  Input(s)        : u4ContextId                                        */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : De-registers BGP with IP and Ipv6 and release the  */
/*                    BGP context                                        */
/*************************************************************************/
VOID
Bgp4DeleteContext (UINT4 u4CxtId)
{
    tBgpCxtNode        *pBgpCxtNode = NULL;

    pBgpCxtNode = Bgp4GetContextEntry (u4CxtId);
    if (pBgpCxtNode == NULL)
    {
        return;
    }
    BGP4_STATUS (u4CxtId) = BGP4_FALSE;
    Bgp4DeInitInCxt (u4CxtId);
    MemReleaseMemBlock (gBgpNode.BgpCxtMemPoolId,
                        (UINT1 *) gBgpCxtNode[u4CxtId]);
    gBgpCxtNode[u4CxtId] = NULL;
    return;
}

#ifdef SNMP_WANTED
/****************************************************************************
 * FUNCTION NAME : RegisterStdBGPwithFutureSNMP
 * DESCRIPTION   : This function is called to register std BGP mib with 
 *                 FutureSNMP
 * INPUTS        : None
 * OUTPUTS       : None.
 * RETURNS       : SUCCESS/FAILURE 
 *****************************************************************************/
INT4
RegisterStdBGPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdbgp4_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdbgp4_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdbgp4_FMAS_MIBObjectTable,
         stdbgp4_FMAS_Global_data, (INT4) stdbgp4_MAX_OBJECTS) == SNMP_SUCCESS)
    {
        return SUCCESS;
    }
    return FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : RegisterFSBGPwitFutureSNMP
 * DESCRIPTION   : This function is called to register FS BGP mib with 
 *                 FutureSNMP.
 * INPUTS        : None
 * OUTPUTS       : None.
 * RETURNS       : SUCCESS/FAILURE 
 *****************************************************************************/
INT4
RegisterFSBGPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & fsbgp4_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & fsbgp4_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & fsbgp4_FMAS_MIBObjectTable,
         fsbgp4_FMAS_Global_data, (INT4) fsbgp4_MAX_OBJECTS) == SNMP_SUCCESS)
    {
        return SUCCESS;
    }
    return FAILURE;
}
#endif

/********************************************************************/
/* Function Name   : Bgp4RegisterWithIP                             */
/* Description     : Registration function with IP to get interface */
/*                   related information                            */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4RegisterWithIP (UINT4 u4Context)
{
    INT4                i4NetIpRegSts;
    tNetIpRegInfo       RegInfo;

    RegInfo.u2InfoMask = 0;
    /* Registration function to get the interface related information */
    if (!(BGP4_OTHER_PROTO_REGISTER_MASK (u4Context) & BGP4_PROTO_IP_REGISTER))
    {
        RegInfo.u2InfoMask |= (NETIPV4_IFCHG_REQ | NETIPV4_ROUTECHG_REQ);
        RegInfo.pIfStChng = Bgp4CbIfChgHdlr;
        RegInfo.pRtChng = Bgp4CbRouteChangeHandler;
        RegInfo.pProtoPktRecv = NULL;
        RegInfo.u1ProtoId = BGP_ID;
        RegInfo.u4ContextId = u4Context;

        i4NetIpRegSts = NetIpv4RegisterHigherLayerProtocol (&RegInfo);
        if (i4NetIpRegSts == NETIPV4_FAILURE)
        {
            /* IP registration is failed. */
            return BGP4_FAILURE;
        }
        BGP4_OTHER_PROTO_REGISTER_MASK (u4Context) |= BGP4_PROTO_IP_REGISTER;
    }

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4DeRegisterWithIP                           */
/* Description     : DeRegistration from IP                         */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4DeRegisterWithIP (UINT4 u4Context)
{
    /* Registration function to get the interface related information */
    NetIpv4DeRegisterHigherLayerProtocolInCxt (u4Context, BGP_ID);
    BGP4_OTHER_PROTO_REGISTER_MASK (u4Context) &= ~(BGP4_PROTO_IP_REGISTER);
    return BGP4_SUCCESS;
}

#ifdef L3VPN
/********************************************************************/
/* Function Name   : Bgp4CreateLabelSpace                           */
/* Description     : Registration function with Label Manager to    */
/*                   create label space                             */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : BGP4_LBL_GROUP_ID                    */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4CreateLabelSpace (VOID)
{
#ifndef LINUX_SLI_WANTED
#ifdef L3VPN_PORT
#ifdef MPLS_WANTED
    tKeyInfoStruct      LblRangeInfo;
    UINT2               u2LblRngCount = 1;
    UINT1               u1Status = BGP4_FAILURE;

    LblRangeInfo.u4Key1Min = 0;
    LblRangeInfo.u4Key1Max = 0;
    LblRangeInfo.u4Key2Min = gSystemSize.MplsSystemSize.u4MinL3VPNLblRange;
    LblRangeInfo.u4Key2Max = gSystemSize.MplsSystemSize.u4MaxL3VPNLblRange;

    if (!
        (BGP4_OTHER_PROTO_REGISTER_MASK (BGP4_DFLT_VRFID) &
         BGP4_PROTO_MPLS_LBL_REGISTER))
    {
        u1Status = LblMgrCreateLabelSpaceGroup (BGP4_LBL_ALLOC_BOTH_NUM,
                                                &LblRangeInfo,
                                                u2LblRngCount,
                                                L3VPN_LBL_MODULE_ID,
                                                0, &(BGP4_LBL_GROUP_ID));
        if (u1Status == BGP4_LBL_FAILURE)
        {
            /* Failed to Allocate Label Space */
            return BGP4_FAILURE;
        }
        BGP4_OTHER_PROTO_REGISTER_MASK (BGP4_DFLT_VRFID) |=
            BGP4_PROTO_MPLS_LBL_REGISTER;
    }
#endif
#endif
#endif
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4DeleteLabelSpace                           */
/* Description     : Deleing Label space that has been created      */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4DeleteLabelSpace (VOID)
{
#ifndef LINUX_SLI_WANTED
#ifdef L3VPN_PORT
#ifdef MPLS_WANTED
    LblMgrDeleteLabelSpaceGroup (BGP4_LBL_GROUP_ID);
#endif
#endif
#endif
    BGP4_LBL_GROUP_ID = 0;
    BGP4_OTHER_PROTO_REGISTER_MASK (BGP4_DFLT_VRFID) &=
        ~(BGP4_PROTO_MPLS_LBL_REGISTER);
    return BGP4_SUCCESS;
}
#endif

/******************************************************************************/
/* Function Name : Bgp4DeInit                                             */
/* Description   : This function frees all globally allocated memory,         */
/*                 effectively shutting down the task                         */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4DeInit ()
{
    tBgp4QMsg          *pMsg = NULL;
    UINT4               u4Context = 0;

    if (gBgpCxtNode == NULL)
    {
        return BGP4_FAILURE;
    }

#ifdef L3VPN
    Bgp4DeleteLabelSpace ();
    Bgp4DeRegisterWithMpls ();
#endif
#ifdef EVPN_WANTED
    Bgp4DeRegisterWithVxlan ();
#endif
    do
    {
        if (NULL != gBgpCxtNode[u4Context])
        {
            Bgp4DeleteContext (u4Context);
        }
    }
    while (VcmGetNextActiveL3Context (u4Context, &u4Context) != VCM_FAILURE);

    /* free the BGP Input Q */
    if (BGP4_Q_ID != 0)
    {
        while (OsixQueRecv (BGP4_Q_ID, (UINT1 *) &pMsg,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            BGP4_QMSG_FREE (pMsg);
        }
    }

    CommShut ();
#ifdef ROUTEMAP_WANTED
    RMapDeRegister (RMAP_APP_BGP4);
#endif
    VcmDeRegisterHLProtocol (BGP_PROTOCOL_ID);
    Bgp4RedDeInitGlobalInfo ();

    BGP4_TIMER_STOP (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr);
    if (gBgpNode.pBgpRestartTmr != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                            (UINT1 *) gBgpNode.pBgpRestartTmr);
        gBgpNode.pBgpRestartTmr = NULL;
    }

    BGP4_TIMER_STOP (BGP4_TIMER_LISTID, (gBgpNode.pBgpSelectionDeferTmr));
    if (gBgpNode.pBgpSelectionDeferTmr != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                            (UINT1 *) gBgpNode.pBgpSelectionDeferTmr);
        gBgpNode.pBgpSelectionDeferTmr = NULL;
    }

    BGP4_MPE_ALLOC_SNPA_NODE_CNT = 0;

    /* Stop The one-second timer */
    BGP4_TIMER_STOP (BGP4_1S_TIMER_LISTID, gBgpNode.pBgpOneSecTmr);
    if (gBgpNode.pBgpOneSecTmr != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                            (UINT1 *) gBgpNode.pBgpOneSecTmr);
        gBgpNode.pBgpOneSecTmr = NULL;
    }

    if (BGP4_1S_TIMER_LISTID != NULL)
    {
        TmrDeleteTimerList (BGP4_1S_TIMER_LISTID);
    }

    /* Delete the timer-lists created */
    if (BGP4_TIMER_LISTID != NULL)
    {
        TmrDeleteTimerList (BGP4_TIMER_LISTID);
    }

    OsixSemDel (gBgpNode.Bgp4RedistListSemId);

    /* Delete the Input Queue */
    if (BGP4_Q_ID != 0)
    {
        OsixQueDel (BGP4_Q_ID);
    }

    /* Delete the BGP4 Response Queue */
    if (BGP4_RES_Q_ID != 0)
    {
        OsixQueDel (BGP4_RES_Q_ID);
    }

    /* Delete the sema4s created */
    OsixDeleteSem (ZERO, (const UINT1 *) "RIB1");
    OsixSemDel (gBgpNode.Bgp4ProtoSemId);
    OsixSemDel (BGP4_RIB_SEM_ID);

#ifdef L3VPN
    Bgp4Vpnv4Shutdown ();
#endif
#ifdef VPLSADS_WANTED
    Bgp4VplsShutdown ();
#endif
    ExtCommShut ();
    Bgp4MpeShutdown ();

    gBgpNode.BgpCxtMemPoolId = 0;
    gBgpNode.Bgp4PeerGroupPoolId = 0;
    gBgpNode.Bgp4PeerEntryPoolId = 0;
    gBgpNode.Bgp4RtProfilePoolId = 0;
    gBgpNode.Bgp4BgpInfoPoolId = 0;
    gBgpNode.Bgp4PAPoolId = 0;
    gBgpNode.Bgp4AdvtPAPoolId = 0;
    gBgpNode.Bgp4AsPathPoolId = 0;
    gBgpNode.Bgp4LinkNodePoolId = 0;
    gBgpNode.Bgp4AdvRtLinkNodePoolId = 0;
    gBgpNode.Bgp4NetworkRtPoolId = 0;
    gBgpNode.Bgp4InputQPoolId = 0;
    BGP4_NEXTHOP_METRIC_TBL_POOLID = 0;
    BGP4_IFACE_LIST_POOLID = 0;
    gBgpNode.Bgp4PollTmrPoolId = 0;
    gBgpNode.Bgp4PeerDataEntryPoolId = 0;
    gBgpNode.Bgp4RfdDecayPoolId = 0;
    gBgpNode.Bgp4RfdReuseIndexPoolId = 0;
    BGP4_RTREF_MEM_POOL_ID = 0;
    BGP4_ORF_MEM_POOL_ID = 0;

#ifdef EVPN_WANTED
    BGP4_EVPN_VRF_VNI_MEM_POOL_ID = 0;
    BGP4_MAC_DUP_TIMER_MEM_POOL_ID = 0;
#endif
    BGP4_TCPMD5_MEM_POOL_ID = 0;
    PEER_CAPS_MEMPOOLID = 0;
    BGP4_MPE_RT_SNPA_POOL_ID = 0;
    BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID = 0;
    INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID = 0;
    OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID = 0;
    ROUTE_CONF_EXT_COMM_POOL_ID = 0;
    EXT_COMM_PROFILE_POOL_ID = 0;
    PEER_LINK_BANDWIDTH_POOL_ID = 0;
    ROUTES_EXT_COMM_SET_STATUS_POOL_ID = 0;

    INPUT_COMM_FILTER_ENTRY_POOL_ID = 0;
    OUTPUT_COMM_FILTER_ENTRY_POOL_ID = 0;
    ROUTE_CONF_COMM_POOL_ID = 0;
    COMM_PROFILE_POOL_ID = 0;
    ROUTES_COMM_SET_STATUS_POOL_ID = 0;
    RT_DAMP_HIST_MEMPOOLID = 0;
    PEER_DAMP_HIST_MEMPOOLID = 0;
    REUSE_PEER_MEMPOOLID = 0;
    BGP_AGGR_NODE_POOL_ID = 0;
    BGP_COMMUNITY_NODE_POOL_ID = 0;
    BGP_CLUSTER_LIST_POOL_ID = 0;
    BGP_EXT_COMMUNITY_NODE_POOL_ID = 0;
    BGP_AFI_SAFI_NODE_POOL_ID = 0;
    BGP_ATTRIBUTE_LEN_POOL_ID = 0;
    BGP_PEER_NODE_POOL_ID = 0;
    BGP_CLI_MEM_BLK_BUF_POOL_ID = 0;
    gBgpCxtNode = NULL;

#ifdef CLI_WANTED
    CsrMemDeAllocation (BGP_MODULE_ID);
#endif
    BgpSizingMemDeleteMemPools ();
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4ProcessQMsg                                            */
/* Description   : This function processes the messages recvd in the BGP input*/
/* Input(s)      : Qname - The input Q from which messages need to be deQd.   */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4ProcessQMsg (const UINT1 *pu1Qname)
{
    tBgp4QMsg          *pMsg = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    UINT4               u4StoreMaskOfIPdisable;
    UINT4               u4RespMsg;
    UINT4               u4VrfId = BGP4_DFLT_VRFID;
    INT4                i4RetVal = BGP4_SUCCESS;
    INT4                i4RetVal1 = BGP4_SUCCESS;
    UINT2               u2Afi;
    UINT2               u2Safi;
    UINT1               u1ProcessStatus = BGP4_SUCCESS;
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
    UINT1               u1MsgType;
#endif
#endif

    UNUSED_PARAM (pu1Qname);
    while (OsixQueRecv (BGP4_Q_ID, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        u1ProcessStatus = BGP4_SUCCESS;
        BgpLock ();
        BgpSetContextId ((INT4) (pMsg->u4ContextId));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        tLnxVrfInfo        *pLnxVrfInfo = NULL;
        if (pMsg->u4ContextId > 0)
        {
            pLnxVrfInfo = LnxVrfInfoGet (pMsg->u4ContextId);
            if (pLnxVrfInfo != NULL)
            {
                if (LnxVrfChangeCxt
                    (LNX_VRF_NON_DEFAULT_NS,
                     (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
                {
                    u1ProcessStatus = BGP4_FAILURE;
                    i4RetVal = BGP4_FAILURE;
                }
            }
            else
            {
                u1ProcessStatus = BGP4_FAILURE;
                i4RetVal = BGP4_FAILURE;
            }
        }
#endif

        if (u1ProcessStatus == BGP4_SUCCESS)
        {
            switch (pMsg->u4MsgType)
            {
                case BGP4_SNMP_GLOBAL_ADMIN_UP_EVENT:
                    i4RetVal =
                        Bgp4GlobalAdminStatusHandler (pMsg->u4ContextId,
                                                      BGP4_ADMIN_UP);
                    break;

                case BGP4_SNMP_GLOBAL_ADMIN_DOWN_EVENT:
                    i4RetVal =
                        Bgp4GlobalAdminStatusHandler (pMsg->u4ContextId,
                                                      BGP4_ADMIN_DOWN);
                    break;

                case BGP4_SNMP_PEER_DELETE_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;

                    /* Need to Delete the peer and all the peer related 
                     * parameters & Data structures */
                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    i4RetVal = Bgp4DeletePeer (pPeerentry);
                    break;
                }

                case BGP4_SNMP_START_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    i4RetVal =
                        Bgp4PeerStartPeer (pMsg->u4ContextId, PeerRemoteAddr);
                    break;
                }

                case BGP4_SNMP_STOP_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    i4RetVal =
                        Bgp4PeerStopPeer (pMsg->u4ContextId, PeerRemoteAddr);
                    break;
                }

                case BGP4_IGP_RRD_DISABLE_EVENT:
                    u4StoreMaskOfIPdisable = BGP4_INPUTQ_DATA (pMsg);
                    u4VrfId = pMsg->u4ContextId;
                    Bgp4RedistLock ();
                    Bgp4ProcessImportList (u4StoreMaskOfIPdisable, u4VrfId);
                    Bgp4RedistUnLock ();

                    /* Update the redistribution protocol mask for the 
                     * disabled protocol */
                    BGP4_RRD_PROTO_MASK (u4VrfId) =
                        (BGP4_RRD_PROTO_MASK (u4VrfId) &
                         (~u4StoreMaskOfIPdisable));
                    break;

                case BGP4_RRD_DISABLE_EVENT:
                    u4StoreMaskOfIPdisable = BGP4_RRD_DISABLE;
                    Bgp4RedistLock ();
                    Bgp4ProcessImportList (u4StoreMaskOfIPdisable,
                                           pMsg->u4ContextId);
                    Bgp4RedistUnLock ();
                    BGP4_RRD_ADMIN_STATE (pMsg->u4ContextId) = BGP4_RRD_DISABLE;
                    break;

#ifdef RRD_WANTED
                case BGP4_RTM_RECV_INFO_EVENT:
                case BGP4_RTM_REG_EVENT:
                case BGP4_RTM6_REG_EVENT:
                    i4RetVal = Bgp4RtmMsgHandler (pMsg);
                    break;
                    break;
                case BGP4_RTM_ROUTE_UPDATE_EVENT:
                    i4RetVal =
                        Bgp4RtmProcessUpdate (&
                                              (pMsg->QMsg.RtmRegnPtr.
                                               NetIpv4RtInfo),
                                              &(pMsg->QMsg.RtmRegnPtr.RtmHdr));
                    break;
                case BGP4_RTM_ROUTE_CHANGE_NOTIFY_EVENT:
                    i4RetVal =
                        Bgp4RtmProcessRtChange (&
                                                (pMsg->QMsg.RtmRegnPtr.
                                                 NetIpv4RtInfo),
                                                &(pMsg->QMsg.RtmRegnPtr.
                                                  RtmHdr));
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_RTM6_ROUTE_UPDATE_EVENT:
                    /* inside BGP all protocols are enumerated as in RTM4 */
                    Bgp4ProtoIdFromRtm6 (&
                                         (pMsg->QMsg.Rtm6RegnPtr.RtmHdr.RegnId.
                                          u2ProtoId));

                    i4RetVal =
                        Bgp4Rtm6ProcessUpdate (&
                                               (pMsg->QMsg.Rtm6RegnPtr.
                                                NetIpv6RtInfo),
                                               &(pMsg->QMsg.Rtm6RegnPtr.
                                                 RtmHdr));
                    break;
                case BGP4_RTM6_ROUTE_CHANGE_NOTIFY_EVENT:
                    /* inside BGP all protocols are enumerated as in RTM4 */
                    Bgp4ProtoIdFromRtm6 (&
                                         (pMsg->QMsg.Rtm6RegnPtr.RtmHdr.RegnId.
                                          u2ProtoId));

                    i4RetVal =
                        Bgp4Rtm6ProcessRtChange (&
                                                 (pMsg->QMsg.Rtm6RegnPtr.
                                                  NetIpv6RtInfo),
                                                 &(pMsg->QMsg.Rtm6RegnPtr.
                                                   RtmHdr));
                    break;
#endif

#endif
                case BGP4_SNMP_LOCAL_BGP_ID_CHANGE_EVENT:
                    /* Need to STOP and RE-START the peer session between all 
                     * active peers. Get the first peer from the peer list.
                     * Dont use Scan for scanning the peer list because Disabling
                     * the peer may end up in removing the duplicate peer entry
                     * if exists and scan may be ending up in problem. */
                    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_INPUTQ_CXT (pMsg)) ==
                        BGP4_ADMIN_UP)
                    {
                        pPeerentry =
                            (tBgp4PeerEntry *)
                            TMO_SLL_First (BGP4_PEERENTRY_HEAD
                                           (BGP4_INPUTQ_CXT (pMsg)));
                        for (; pPeerentry != NULL;)
                        {
                            if ((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                 BGP4_PEER_START) ||
                                (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                 BGP4_PEER_AUTO_START))
                            {
                                i4RetVal = Bgp4DisablePeer (pPeerentry);
                                i4RetVal = Bgp4EnablePeer (pPeerentry);
                            }
                            /* Get the next peer entry. */
                            pPeerentry = (tBgp4PeerEntry *)
                                TMO_SLL_Next (BGP4_PEERENTRY_HEAD
                                              (BGP4_INPUTQ_CXT (pMsg)),
                                              &pPeerentry->TsnNextpeer);
                        }
                    }
                    break;

                case BGP4_CLI_NO_ROUTER_BGP_EVENT:
                    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_INPUTQ_CXT (pMsg)) !=
                        BGP4_ADMIN_DOWN)
                    {
                        i4RetVal =
                            Bgp4GlobalAdminStatusHandler (BGP4_INPUTQ_CXT
                                                          (pMsg),
                                                          BGP4_ADMIN_DOWN);
                        BGP4_SET_GLOBAL_FLAG (BGP4_INPUTQ_CXT (pMsg),
                                              BGP4_GLOBAL_CLEAR_PENDING);
                    }
                    else
                    {
                        /* Admin status is already down. */
                        if (BGP4_GLOBAL_STATE (BGP4_INPUTQ_CXT (pMsg)) ==
                            BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
                        {
                            BGP4_SET_GLOBAL_FLAG (BGP4_INPUTQ_CXT (pMsg),
                                                  BGP4_GLOBAL_CLEAR_PENDING);
                        }
                        else
                        {
                            i4RetVal =
                                Bgp4ClearAndReInit (BGP4_INPUTQ_CXT (pMsg));
                        }
                    }
                    if (BGP4_RRD_RMAP_NAME (BGP4_INPUTQ_CXT (pMsg)) != NULL)
                    {
                        /* Resetting the Route Map Name Associated */
                        MEMSET (BGP4_RRD_RMAP_NAME (BGP4_INPUTQ_CXT (pMsg)), 0,
                                sizeof (BGP4_RRD_RMAP_NAME
                                        (BGP4_INPUTQ_CXT (pMsg))));
                    }

                    break;

                case BGP4_SNMP_EBGP_MULTIHOP_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddress;

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddress,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddress);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    i4RetVal = Bgp4PeerEbgpMultihopHandler (pPeerentry);
                    break;
                }

                case BGP4_SNMP_RFL_CLUSTER_ID_EVENT:
                    if (RFL_CLUSTER_ID (BGP4_INPUTQ_CXT (pMsg)) ==
                        BGP4_INPUTQ_DATA (pMsg))
                    {
                        /* No Change in RFL Cluster ID value. */
                        break;
                    }
                    /* Set the Reflector Cluster Id to the given value and 
                     * Restart the Peer Session for IBGP Peers because the
                     * new cluster id should be advertised to all the 
                     * internal peers. */
                    RFL_CLUSTER_ID (BGP4_INPUTQ_CXT (pMsg)) =
                        BGP4_INPUTQ_DATA (pMsg);
                    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_INPUTQ_CXT (pMsg)) ==
                        BGP4_ADMIN_UP)
                    {
                        pPeerentry =
                            (tBgp4PeerEntry *)
                            TMO_SLL_First (BGP4_PEERENTRY_HEAD
                                           (BGP4_INPUTQ_CXT (pMsg)));
                        for (; pPeerentry != NULL;)
                        {
                            if (((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                  BGP4_PEER_START) ||
                                 (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                  BGP4_PEER_AUTO_START)) &&
                                (BGP4_GET_PEER_TYPE
                                 (BGP4_INPUTQ_CXT (pMsg),
                                  pPeerentry) == BGP4_INTERNAL_PEER))
                            {
                                i4RetVal = Bgp4DisablePeer (pPeerentry);
                                i4RetVal = Bgp4EnablePeer (pPeerentry);
                            }
                            /* Get the next peer entry. */
                            pPeerentry = (tBgp4PeerEntry *)
                                TMO_SLL_Next (BGP4_PEERENTRY_HEAD
                                              (BGP4_INPUTQ_CXT (pMsg)),
                                              &pPeerentry->TsnNextpeer);
                        }
                    }
                    break;

                case BGP4_SNMP_RFL_CLIENT_SUPP_EVENT:
                    if (RFL_CLIENT_SUPPORT (BGP4_INPUTQ_CXT (pMsg)) ==
                        (UINT1) BGP4_INPUTQ_DATA (pMsg))
                    {
                        /* No change in RFL Client Support Status */
                        break;
                    }

                    /* Set the Reflector Client to Client Reflection Support 
                     * status and Restart the Peer Session for all the Client
                     * Peers. */
                    RFL_CLIENT_SUPPORT (BGP4_INPUTQ_CXT (pMsg)) =
                        (UINT1) BGP4_INPUTQ_DATA (pMsg);
                    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_INPUTQ_CXT (pMsg)) ==
                        BGP4_ADMIN_UP)
                    {
                        pPeerentry =
                            (tBgp4PeerEntry *)
                            TMO_SLL_First (BGP4_PEERENTRY_HEAD
                                           (BGP4_INPUTQ_CXT (pMsg)));
                        for (; pPeerentry != NULL;)
                        {
                            if (((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                  BGP4_PEER_START) ||
                                 (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                  BGP4_PEER_AUTO_START)) &&
                                (BGP4_GET_PEER_TYPE
                                 (BGP4_INPUTQ_CXT (pMsg),
                                  pPeerentry) == BGP4_INTERNAL_PEER)
                                && (BGP4_PEER_RFL_CLIENT (pPeerentry) ==
                                    CLIENT))
                            {
                                i4RetVal = Bgp4DisablePeer (pPeerentry);
                                i4RetVal = Bgp4EnablePeer (pPeerentry);
                            }
                            /* Get the next peer entry. */
                            pPeerentry = (tBgp4PeerEntry *)
                                TMO_SLL_Next (BGP4_PEERENTRY_HEAD
                                              (BGP4_INPUTQ_CXT (pMsg)),
                                              &pPeerentry->TsnNextpeer);
                        }
                    }
                    break;

                case BGP4_SNMP_RFL_PEER_TYPE_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddress;

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddress,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddress);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    /* This event is generated whenever there is a change in peer
                     * client status. If this event occurs the peer session needs
                     * to be restarted. 
                     */
                    if ((BGP4_LOCAL_ADMIN_STATUS (BGP4_INPUTQ_CXT (pMsg)) ==
                         BGP4_ADMIN_DOWN)
                        || (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                            BGP4_PEER_STOP))
                    {
                        /* Update the peer rfl client status */
                        if (BGP4_PEER_RFL_CLIENT (pPeerentry) == CLIENT)
                        {
#if (defined L3VPN || defined VPLSADS_WANTED)
                            /* Decrement the client count */
                            BGP4_RR_CLIENT_CNT (BGP4_INPUTQ_CXT (pMsg))--;
#endif
                            BGP4_PEER_RFL_CLIENT (pPeerentry) = NON_CLIENT;
                        }
                        else
                        {
#if (defined L3VPN || defined VPLSADS_WANTED)
                            /* Increment the client count */
                            BGP4_RR_CLIENT_CNT (BGP4_INPUTQ_CXT (pMsg))++;
#endif
                            BGP4_PEER_RFL_CLIENT (pPeerentry) = CLIENT;
                        }
                        break;
                    }
                    if ((BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_START)
                        || (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                            BGP4_PEER_AUTO_START))
                    {
                        i4RetVal = Bgp4DisablePeer (pPeerentry);
                        if ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
                             BGP4_PEER_DEINIT_INPROGRESS) ==
                            BGP4_PEER_DEINIT_INPROGRESS)
                        {
                            /* Peer is going through deinit. Dont update the peer
                             * client status before peer session is disabled,
                             * because if the Reflector's client-client
                             * advertisment policy is disable, then resetting
                             * the peer session will cause the route from this
                             * peer to be advertised as withdrawn to other client
                             * peers even though these route are not advertised
                             * earlier as feasible.
                             */
                            BGP4_SET_PEER_PEND_FLAG (pPeerentry,
                                                     BGP4_PEER_RFL_CLIENT_PEND);
                        }
                        else
                        {
                            /* Update the peer rfl client status */
                            if (BGP4_PEER_RFL_CLIENT (pPeerentry) == CLIENT)
                            {
                                BGP4_PEER_RFL_CLIENT (pPeerentry) = NON_CLIENT;
#if (defined L3VPN || defined VPLSADS_WANTED)
                                /* Decrement the client count */
                                BGP4_RR_CLIENT_CNT (BGP4_INPUTQ_CXT (pMsg))--;
#endif
                            }
                            else
                            {
#if (defined L3VPN || defined VPLSADS_WANTED)
                                /* Increment the client count */
                                BGP4_RR_CLIENT_CNT (BGP4_INPUTQ_CXT (pMsg))++;
#endif
                                BGP4_PEER_RFL_CLIENT (pPeerentry) = CLIENT;
                            }
                        }
                        /* Restart the peer connection. */
                        i4RetVal = Bgp4EnablePeer (pPeerentry);
                    }
                    break;
                }

                case BGP4_IGP_METRIC_SET_EVENT:
                    /* Set the Default IGP Metric value to the given input value */
                    BGP4_DEFAULT_IGP_METRIC (BGP4_INPUTQ_CXT (pMsg)) =
                        BGP4_INPUTQ_DATA (pMsg);

                    /* All the routes learnt from RTM except Direct route has to be
                     * resent with this new METRIC value */
                    Bgp4ProcessImportList (BGP4_IGP_METRIC_SET_EVENT,
                                           BGP4_INPUTQ_CXT (pMsg));
                    break;

                case BGP4_SNMP_INBOUND_SOFTCONFIG_RTREF_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddress;
                    Bgp4CopyAddrPrefixStruct (&(PeerRemoteAddress),
                                              BGP4_INPUTQ_RTREF_PEER_ADDR_INFO
                                              (pMsg));
                    Bgp4GetAfiSafiFromMask (BGP4_INPUTQ_RTREF_ASAFI_MASK (pMsg),
                                            &u2Afi, &u2Safi);
                    /* Send route-refresh request/incoming soft-reconfig 
                     * to peers */
                    i4RetVal =
                        Bgp4RtRefRequestHandler (pMsg->u4ContextId,
                                                 PeerRemoteAddress, u2Afi,
                                                 u2Safi,
                                                 BGP4_INPUTQ_RTREF_OPER_TYPE
                                                 (pMsg));
                    break;
                }

                case BGP4_SNMP_OUTBOUND_SOFTCONFIG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddress;

                    Bgp4CopyAddrPrefixStruct (&(PeerRemoteAddress),
                                              BGP4_INPUTQ_RTREF_PEER_ADDR_INFO
                                              (pMsg));
                    Bgp4GetAfiSafiFromMask (BGP4_INPUTQ_RTREF_ASAFI_MASK (pMsg),
                                            &u2Afi, &u2Safi);
                    /* Apply outbound soft-reconfig for the peers */
                    i4RetVal =
                        Bgp4FiltOutBoundSoftConfigHandler (pMsg->u4ContextId,
                                                           PeerRemoteAddress,
                                                           u2Afi,
                                                           (UINT1) u2Safi);
                    break;
                }

                case BGP4_CLI_CLEAR_IP_BGP_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    i4RetVal =
                        Bgp4PeerHandleClearIpBgp (pMsg->u4ContextId,
                                                  PeerRemoteAddr);
                    break;
                }

                case BGP4_SNMP_PEER_SRCADDR_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    /* This event is generated whenever there is a change in the 
                     * Source IF configuration for the peer. If this event is 
                     * generated, then the peer session should be restarted */

                    if (((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                          BGP4_PEER_START)
                         || (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                             BGP4_PEER_AUTO_START))
                        && (BGP4_LOCAL_ADMIN_STATUS (pMsg->u4ContextId) ==
                            BGP4_ADMIN_UP))
                    {
                        i4RetVal = Bgp4DisablePeer (pPeerentry);
                        i4RetVal = Bgp4EnablePeer (pPeerentry);
                    }
                    break;
                }

                case BGP4_AGGR_ADMIN_CHG_EVENT:
                    i4RetVal = Bgp4AggrProcessAdminChange
                        (BGP4_INPUTQ_AGGR_INDEX (pMsg),
                         BGP4_INPUTQ_AGGR_ACTION (pMsg));
                    break;

                case BGP4_AGGR_POLICY_CHG_EVENT:
                    i4RetVal = Bgp4AggrProcessAdvertiseChange
                        (BGP4_INPUTQ_AGGR_INDEX (pMsg),
                         BGP4_INPUTQ_AGGR_ACTION (pMsg));
                    break;

                case BGP4_PEER_DEF_ROUTE_ORIG_CHG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_DEF_RT_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    if (BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerentry) ==
                        BGP4_INPUTQ_DEF_RT_STATUS (pMsg))
                    {
                        /* No change in policy. */
                        i4RetVal = BGP4_SUCCESS;
                        break;
                    }

                    BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerentry) =
                        BGP4_INPUTQ_DEF_RT_STATUS (pMsg);
                    i4RetVal = Bgp4PeerAdvtDftRoute (pPeerentry,
                                                     BGP4_INPUTQ_DEF_RT_STATUS
                                                     (pMsg), BGP4_FALSE);
                    break;
                }
                case BGP4_IFACE_CHG_EVENT:

                    i4RetVal =
                        Bgp4IfaceChngHandler (BGP4_INPUTQ_IFACE_INDEX (pMsg),
                                              BGP4_INPUTQ_IFACE_STATUS (pMsg));
                    break;

#ifdef L3VPN
                case BGP4_EXPORT_TARGET_CHG_EVENT:
                    i4RetVal =
                        Bgp4Vpnv4ExportTargetChgHandler (BGP4_INPUTQ_VRF_ID
                                                         (pMsg));
                    break;

                case BGP4_LSP_STATE_CHG_EVENT:
                    i4RetVal =
                        Bgp4LspChngHandler (BGP4_INPUTQ_LSP_ID (pMsg),
                                            (UINT1)
                                            BGP4_INPUTQ_LSP_STATUS (pMsg));

                    break;

                case BGP4_RSVPTE_LSP_STATE_CHG_EVENT:
                    i4RetVal =
                        Bgp4RsvpTeLspChngHandler (BGP4_INPUTQ_LSP_ID (pMsg),
                                                  (UINT1)
                                                  BGP4_INPUTQ_LSP_STATUS
                                                  (pMsg));

                    break;

                case BGP4_VRF_ADMIN_STATE_CHG_EVENT:
                    i4RetVal =
                        Bgp4VrfChangeHandler (BGP4_INPUTQ_VRF_ID (pMsg),
                                              (UINT1)
                                              BGP4_INPUTQ_VRF_STATUS (pMsg));

                    break;

                case BGP4_ROUTE_PARAMS_CHG_EVENT:
                    i4RetVal =
                        Bgp4Vpnv4RouteTargetChgHandler (BGP4_INPUTQ_VRF_ID
                                                        (pMsg),
                                                        (INT4)
                                                        BGP4_INPUTQ_PARAM_TYPE
                                                        (pMsg),
                                                        BGP4_INPUTQ_ROUTE_PARAMS
                                                        (pMsg),
                                                        (UINT1)
                                                        BGP4_INPUTQ_ACTION_TYPE
                                                        (pMsg));
                    break;
                case BGP4_VPNV4_ROUTE_TO_PEER_EVENT:
                    Bgp4Vpnv4SendVpnv4RtsToPeer (BGP4_INPUTQ_CXT (pMsg));
                    break;
#endif

#ifdef EVPN_WANTED
                case BGP4_EVPN_VRF_ADMIN_STATE_CHG_EVENT:
                    i4RetVal =
                        Bgp4EvpnVrfChangeHandler (BGP4_VXLAN_INPUTQ_VRF_ID
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_VNI_ID
                                                  (pMsg),
                                                  (UINT1)
                                                  BGP4_VXLAN_INPUTQ_VRF_STATUS
                                                  (pMsg));
                    break;
                case BGP4_EVPN_ROUTE_PARAMS_CHG_EVENT:
                    i4RetVal =
                        Bgp4EvpnRouteTargetChgHandler (BGP4_VXLAN_INPUTQ_VRF_ID
                                                       (pMsg),
                                                       BGP4_VXLAN_INPUTQ_VNI_ID
                                                       (pMsg),
                                                       (INT4)
                                                       BGP4_VXLAN_INPUTQ_PARAM_TYPE
                                                       (pMsg),
                                                       (UINT1)
                                                       BGP4_VXLAN_INPUTQ_ACTION_TYPE
                                                       (pMsg),
                                                       BGP4_VXLAN_INPUTQ_ROUTE_PARAMS
                                                       (pMsg));
                    break;

                case BGP4_EVPN_NOTIFY_MAC:
                    i4RetVal =
                        Bgp4EvpnNotifyMacHandler (BGP4_VXLAN_INPUTQ_VRF_ID
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_VNI_ID
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_MAC_ADDR
                                                  (pMsg),
                                                  (UINT1)
                                                  BGP4_VXLAN_INPUTQ_IP_ADDR_LEN
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_IP_ADDR
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_ACTION_TYPE
                                                  (pMsg),
                                                  BGP4_VXLAN_INPUTQ_ENTRY_STATUS_TYPE
                                                  (pMsg));
                    break;
                case BGP4_EVPN_NOTIFY_ESI:
                    i4RetVal =
                        Bgp4EvpnESINotification (BGP4_VXLAN_INPUTQ_VRF_ID
                                                 (pMsg),
                                                 BGP4_VXLAN_INPUTQ_VNI_ID
                                                 (pMsg),
                                                 BGP4_VXLAN_INPUTQ_ETH_SEG_ID
                                                 (pMsg),
                                                 BGP4_VXLAN_INPUTQ_IP_ADDR
                                                 (pMsg),
                                                 BGP4_VXLAN_INPUTQ_IP_ADDR_LEN
                                                 (pMsg),
                                                 BGP4_VXLAN_INPUTQ_ACTION_TYPE
                                                 (pMsg));
                    break;

                case BGP4_EVPN_NOTIFY_ETH_AD:
                    i4RetVal =
                        Bgp4EvpnEthADNotification (BGP4_VXLAN_INPUTQ_VRF_ID
                                                   (pMsg),
                                                   BGP4_VXLAN_INPUTQ_VNI_ID
                                                   (pMsg),
                                                   BGP4_VXLAN_INPUTQ_ETH_SEG_ID
                                                   (pMsg),
                                                   (UINT1)
                                                   BGP4_VXLAN_INPUTQ_SINGLE_ACTIVE_FLAG
                                                   (pMsg));
                    break;
                case BGP4_EVPN_ROUTE_TO_PEER_EVENT:
                    Bgp4EvpnIndicateRtToPeers (BGP4_INPUTQ_CXT (pMsg));
                    break;
                case BGP4_EVPN_GET_MAC_EVENT:
                    i4RetVal =
                        Bgp4EvpnGetAllRoutes (BGP4_VXLAN_INPUTQ_VRF_ID (pMsg),
                                              BGP4_VXLAN_INPUTQ_VNI_ID (pMsg));
                    break;

#endif

#ifdef ROUTEMAP_WANTED
                case BGP4_ROUTEMAP_STATUS_CHG_EVENT:
                    Bgp4ProcessRMapHandler (pMsg);
                    break;
#endif /* ROUTEMAP_WANTED */

                case BGP4_TCP_MD5_PASSWORD_CONFIG_EVENT:
                {
                    tAddrPrefix         PeerAddress;
                    struct tcp_md5sig   md5sig;
                    INT4                i4OptLen = ZERO;
                    MEMSET (&PeerAddress, ZERO, sizeof (tAddrPrefix));
                    MEMSET (&md5sig, ZERO, sizeof (struct tcp_md5sig));

                    Bgp4CopyAddrPrefixStruct
                        (&PeerAddress,
                         BGP4_INPUTQ_TCP_MD5_PEER_ADDR_INFO (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId, PeerAddress);

                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    /*Set the peer password on the listen socket */
                    i4RetVal = Bgp4TcphMD5AuthOptSet
                        (pPeerentry, BGP4_INPUTQ_TCP_MD5_PASSWORD (pMsg),
                         BGP4_INPUTQ_TCP_MD5_PASSWORD_LEN (pMsg));

                    /* If setsockopt is success, copy password to peer table */
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        MEMSET (BGP4_PEER_TCPMD5_PASSWD (pPeerentry), ZERO,
                                sizeof (BGP4_PEER_TCPMD5_PASSWD (pPeerentry)));

                        MEMCPY (BGP4_PEER_TCPMD5_PASSWD (pPeerentry),
                                BGP4_INPUTQ_TCP_MD5_PASSWORD (pMsg),
                                BGP4_INPUTQ_TCP_MD5_PASSWORD_LEN (pMsg));

                        BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerentry) =
                            BGP4_INPUTQ_TCP_MD5_PASSWORD_LEN (pMsg);
                    }

                    /* Check if a peer session is established */
                    if (((BGP4_PEER_STATE (pPeerentry) ==
                          BGP4_ESTABLISHED_STATE)
                         || (BGP4_PEER_STATE (pPeerentry) ==
                             BGP4_OPENSENT_STATE)
                         || (BGP4_PEER_STATE (pPeerentry) ==
                             BGP4_OPENCONFIRM_STATE))
                        && (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID))
                    {
                        GETSOCKOPT (BGP4_PEER_CONN_ID (pPeerentry),
                                    IPPROTO_TCP, TCP_MD5SIG, &md5sig,
                                    &i4OptLen);

                        /* Iff the established session is signed, then
                           set the new MD5 password on connected socket */
                        if (md5sig.tcpm_keylen > ZERO)
                        {
                            if ((BGP4_AFI_IN_ADDR_PREFIX_INFO
                                 (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))) ==
                                BGP4_INET_AFI_IPV4)
                            {
                                struct sockaddr_in  v4RemoteAddr;
                                MEMSET (&v4RemoteAddr, ZERO,
                                        sizeof (struct sockaddr_in));
                                v4RemoteAddr.sin_family = AF_INET;
                                PTR_FETCH4 (v4RemoteAddr.sin_addr.s_addr,
                                            BGP4_PEER_REMOTE_ADDR (pPeerentry));
                                MEMCPY (&md5sig.tcpm_addr,
                                        &v4RemoteAddr,
                                        sizeof (struct sockaddr_in));
                            }
                            else
                            {
#ifdef BGP4_IPV6_WANTED

                                struct sockaddr_in6 v6RemoteAddr;
                                MEMSET (&v6RemoteAddr, ZERO,
                                        sizeof (struct sockaddr_in6));
                                v6RemoteAddr.sin6_family = AF_INET6;
                                MEMCPY (v6RemoteAddr.sin6_addr.s6_addr,
                                        BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_IPV6_PREFIX_LEN);
                                MEMCPY (&md5sig.tcpm_addr,
                                        &v6RemoteAddr,
                                        sizeof (struct sockaddr_in6));
#endif
                            }

                            MEMCPY (&md5sig.tcpm_key,
                                    BGP4_PEER_TCPMD5_PASSWD (pPeerentry),
                                    BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerentry));
                            md5sig.tcpm_keylen =
                                BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerentry);
                            i4RetVal =
                                SETSOCKOPT (BGP4_PEER_CONN_ID (pPeerentry),
                                            IPPROTO_TCP, TCP_MD5SIG, &md5sig,
                                            sizeof (md5sig));
                            if (i4RetVal < ZERO)
                            {
                                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                          BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                          "\tconfig error - \
                                      failed to set md5 password on\
                                      connected socket\n ");
                            }
                        }
                    }
                    break;
                }

                case BGP4_TCP_MD5_PASSWORD_REMOVE_EVENT:
                {
                    tAddrPrefix         PeerAddress;
                    MEMSET (&PeerAddress, ZERO, sizeof (tAddrPrefix));

                    Bgp4CopyAddrPrefixStruct
                        (&PeerAddress,
                         BGP4_INPUTQ_TCP_MD5_PEER_ADDR_INFO (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId, PeerAddress);

                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    i4RetVal = Bgp4TcphMD5AuthOptSet (pPeerentry, NULL, ZERO);

                    /* If setsockopt is success, remove password from peer table */
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        Bgp4MemReleaseTcpMd5Buf (BGP4_PEER_TCPMD5_PTR
                                                 (pPeerentry),
                                                 BGP4_PEER_CXT_ID (pPeerentry));
                        BGP4_PEER_TCPMD5_PTR (pPeerentry) = NULL;
                    }

                    break;
                }
                case BGP4_SNMP_PEER_GROUP_DELETE_EVENT:
                {

                    /* Delete the peer group from the global list */
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup != NULL)
                    {
                        TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD
                                        (pMsg->u4ContextId),
                                        &(pPeerGroup->NextGroup));

                        TMO_DYN_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                          pTmpPeerNode, tPeerNode *)
                        {
                            pPeerentry = pPeerNode->pPeer;
                            if (BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeerentry) !=
                                BGP4_INV_AS)
                            {
                                Bgp4RestorePeerPropertyFromShadowPeer
                                    (pPeerNode->pPeer);
                            }
                            TMO_SLL_Delete (&(pPeerGroup->PeerList),
                                            &(pPeerNode->TSNext));
                            BGP_PEER_NODE_FREE (pPeerNode);
                            pPeerentry->pPeerGrp = NULL;
                            /* Delete associated peer with no remote-as */
                            if (BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeerentry) ==
                                BGP4_INV_AS)
                            {
                                Bgp4DeletePeer (pPeerentry);
                            }
                        }

                        if (MemReleaseMemBlock
                            (gBgpNode.Bgp4PeerGroupPoolId,
                             (UINT1 *) pPeerGroup) == MEM_FAILURE)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                      BGP4_ALL_FAILURE_TRC |
                                      BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                      "Unable to release the memory allocated for the peer group\n");
                            i4RetVal = BGP4_FAILURE;
                        }
                        i4RetVal = BGP4_SUCCESS;
                    }
                    else
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                                  BGP4_MOD_NAME,
                                  "Unable to obtain the peer group entry in the global list\n");
                        i4RetVal = BGP4_FAILURE;
                    }
                    break;
                }
                case BGP4_SNMP_PEER_GROUP_ENABLE_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        Bgp4PeerStartPeer (pMsg->u4ContextId,
                                           BGP4_PEER_REMOTE_ADDR_INFO
                                           (pPeerNode->pPeer));
                    }
                    pPeerGroup->u1PeerGrpStatus = ACTIVE;
                    i4RetVal = BGP4_SUCCESS;
                    break;
                }
                case BGP4_SNMP_PEER_GROUP_DISABLE_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        Bgp4PeerStopPeer (pMsg->u4ContextId,
                                          BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerNode->pPeer));
                    }
                    pPeerGroup->u1PeerGrpStatus = NOT_IN_SERVICE;
                    i4RetVal = BGP4_SUCCESS;
                    break;
                }
                case BGP4_PEER_GROUP_CLEAR_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        if ((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                             BGP4_PEER_START)
                            || (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                                BGP4_PEER_AUTO_START))
                        {
                            /* Peer is active. Reset the peer connection. */
                            i4RetVal = Bgp4DisablePeer (pPeerentry);
                            if (i4RetVal != BGP4_FAILURE)
                            {
                                i4RetVal = Bgp4EnablePeer (pPeerentry);
                            }
                        }
                    }
                    i4RetVal = BGP4_SUCCESS;
                    break;
                }
                case BGP4_PEER_GROUP_SOFT_CLEAR_IN_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        i4RetVal =
                            Bgp4RtRefRequestHandler (pMsg->u4ContextId,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry),
                                                     pPeerGroup->u2Afi,
                                                     BGP4_INET_SAFI_UNICAST, 0);
                    }
                    break;
                }
                case BGP4_PEER_GROUP_SOFT_CLEAR_OUT_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        i4RetVal =
                            Bgp4FiltOutBoundSoftConfigHandler
                            (pMsg->u4ContextId,
                             BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                             pPeerGroup->u2Afi, BGP4_INET_SAFI_UNICAST);
                    }
                    break;
                }
                case BGP4_PEER_GROUP_SOFT_CLEAR_BOTH_EVENT:
                {
                    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (pMsg->u4ContextId),
                                  pPeerGroup, tBgpPeerGroupEntry *)
                    {
                        if ((STRLEN (pPeerGroup->au1PeerGroupName)
                             == STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME (pMsg))) &&
                            (MEMCMP (pPeerGroup->au1PeerGroupName,
                                     BGP4_INPUTQ_PEER_GROUP_NAME (pMsg),
                                     STRLEN (BGP4_INPUTQ_PEER_GROUP_NAME
                                             (pMsg))) == 0))
                        {
                            break;
                        }
                    }
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        i4RetVal =
                            Bgp4RtRefRequestHandler (pMsg->u4ContextId,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry),
                                                     pPeerGroup->u2Afi,
                                                     BGP4_INET_SAFI_UNICAST, 0);
                        i4RetVal1 =
                            Bgp4FiltOutBoundSoftConfigHandler
                            (pMsg->u4ContextId,
                             BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                             pPeerGroup->u2Afi, BGP4_INET_SAFI_UNICAST);
                        if ((i4RetVal == BGP4_FAILURE)
                            || (i4RetVal1 == BGP4_FAILURE))
                        {
                            i4RetVal = BGP4_FAILURE;
                        }
                    }
                    break;
                }

#ifdef RFD_WANTED
                case BGP4_RFD_ADMIN_CHG_EVENT:
                {
                    RFD_ADMIN_STATUS (pMsg->u4ContextId) =
                        (UINT1) BGP4_INPUTQ_DATA (pMsg);
                    if (RFD_ADMIN_STATUS (pMsg->u4ContextId) == BGP4_TRUE)
                    {
                        i4RetVal = RfdEnable (pMsg->u4ContextId);
                    }
                    else
                    {
                        i4RetVal = RfdDisable (pMsg->u4ContextId);
                    }
                    break;
                }
#endif
                case BGP4_VCM_EVENT:
                {
                    if (pMsg->QMsg.Bgp4VcmMsg.u1Event == VCM_CONTEXT_CREATE)
                    {
                        Bgp4CreateContext (pMsg->QMsg.Bgp4VcmMsg.u4ContextId);
                    }
                    else if (pMsg->QMsg.Bgp4VcmMsg.u1Event ==
                             VCM_CONTEXT_DELETE)
                    {
                        if (gBgpCxtNode[pMsg->QMsg.Bgp4VcmMsg.u4ContextId] ==
                            NULL)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                      BGP4_MOD_NAME,
                                      "\tConfig Error - context not created!!!\n");
                            i4RetVal = BGP4_FAILURE;
                            break;
                        }
                        Bgp4DeleteContext (pMsg->QMsg.Bgp4VcmMsg.u4ContextId);
                    }
                    break;
                }
                case BGP4_LOCAL_AS_CFG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              pMsg->QMsg.Bgp4PeerMsg.
                                              PeerAddress);
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    BGP4_PEER_LOCAL_AS (pPeerentry) =
                        pMsg->QMsg.Bgp4PeerMsg.u4LocalAs;

                    /* When the peer session is already started,
                     * restart the peer connection */

                    if (((BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                          BGP4_PEER_START)
                         || (BGP4_PEER_ADMIN_STATUS (pPeerentry) ==
                             BGP4_PEER_AUTO_START))
                        && (BGP4_LOCAL_ADMIN_STATUS (pMsg->u4ContextId) ==
                            BGP4_ADMIN_UP))
                    {
                        i4RetVal = Bgp4DisablePeer (pPeerentry);
                        i4RetVal = Bgp4EnablePeer (pPeerentry);
                    }
                    break;
                }
                    break;
                case BGP4_TCPAO_ICMPACCEPT_CFG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    tTcpAoNeighCfg      TcpAoNeighCfg;

                    MEMSET (&PeerRemoteAddr, ZERO, sizeof (tAddrPrefix));
                    MEMSET (&TcpAoNeighCfg, ZERO, sizeof (tTcpAoNeighCfg));

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              pMsg->QMsg.Bgp4PeerMsg.
                                              PeerAddress);
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    TcpAoNeighCfg.u1IcmpAccpt =
                        (UINT1) pMsg->QMsg.Bgp4PeerMsg.u4Data;
                    /*Send msg to SLI to actually configure in TCP */

                    i4RetVal =
                        Bgp4TcpAoNeighCfgSet (pPeerentry, &TcpAoNeighCfg,
                                              (INT4) TCP_AO_ICMP_ACC);

                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        BGP4_PEER_TCPAO_ICMP_ACCEPT_STATUS (pPeerentry) =
                            (UINT1) pMsg->QMsg.Bgp4PeerMsg.u4Data;
                    }

                    /* Check if a peer session is established */
                    if ((BGP4_PEER_STATE (pPeerentry) >= BGP4_OPENSENT_STATE)
                        && (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID))
                    {

                        i4RetVal = Bgp4TcpAoIcmpCfgSet (pPeerentry,
                                                        (INT1) pMsg->QMsg.
                                                        Bgp4PeerMsg.u4Data);

                        if (i4RetVal < ZERO)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                      BGP4_MOD_NAME, "\tconfig error - \
                                      failed to set TCP-AO icmp accept config on\
                                      connected socket\n ");
                        }
                    }

                }
                    break;
                case BGP4_TCPAO_NOMKT_PKTDISC_CFG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    tTcpAoNeighCfg      TcpAoNeighCfg;

                    MEMSET (&PeerRemoteAddr, ZERO, sizeof (tAddrPrefix));
                    MEMSET (&TcpAoNeighCfg, ZERO, sizeof (tTcpAoNeighCfg));

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              pMsg->QMsg.Bgp4PeerMsg.
                                              PeerAddress);
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    TcpAoNeighCfg.u1NoMktMchPckDsc =
                        (UINT1) pMsg->QMsg.Bgp4PeerMsg.u4Data;
                    /*Send msg to SLI to actually configure in TCP */
                    i4RetVal =
                        Bgp4TcpAoNeighCfgSet (pPeerentry, &TcpAoNeighCfg,
                                              (INT4) TCP_AO_NOMKT_MCH);

                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        BGP4_PEER_TCPAO_NOMKT_PKTDISC_STATUS (pPeerentry) =
                            (UINT1) pMsg->QMsg.Bgp4PeerMsg.u4Data;
                    }

                    /* Check if a peer session is established */
                    if ((BGP4_PEER_STATE (pPeerentry) >= BGP4_OPENSENT_STATE)
                        && (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID))

                    {
                        i4RetVal = Bgp4TcpAoPktDiscCfg (pPeerentry,
                                                        (INT1) pMsg->QMsg.
                                                        Bgp4PeerMsg.u4Data);

                        if (i4RetVal < ZERO)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                      BGP4_MOD_NAME, "\tconfig error - \
                                      failed to set TCP-AO no MKT match packet \
                                      discard config on connected socket\n ");
                        }
                    }

                }
                    break;
                case BGP4_TCPAO_MKT_ASSOCIATE_CFG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;

                    tTcpAoAuthMKT      *pMkt = NULL;
                    tTcpAoMktAddr       TcpAoMktAdr;
                    INT1                i1Match = 0;

                    MEMSET (&PeerRemoteAddr, ZERO, sizeof (tAddrPrefix));
                    MEMSET (&TcpAoMktAdr, ZERO, sizeof (tTcpAoMktAddr));

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              pMsg->QMsg.Bgp4PeerMsg.
                                              PeerAddress);
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    /* Check MKt corresponfing to the keyId is configured in the MKT table */
                    TMO_SLL_Scan (&(BGP4_TCPAO_MKT_LIST (pMsg->u4ContextId)),
                                  pMkt, tTcpAoAuthMKT *)
                    {
                        if (pMsg->QMsg.Bgp4PeerMsg.u4Data ==
                            (INT4) pMkt->u1SendKeyId)
                        {
                            i1Match = TRUE;
                            break;
                        }
                    }
                    if (i1Match != TRUE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching MKT exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    TcpAoMktAdr.u1SndKeyId = pMkt->u1SendKeyId;
                    TcpAoMktAdr.u1RcvKeyId = pMkt->u1ReceiveKeyId;
                    TcpAoMktAdr.u1Algo = pMkt->u1MACAlgo;
                    TcpAoMktAdr.u1KeyLen = pMkt->u1TcpAOPasswdLength;
                    TcpAoMktAdr.u1TcpOptIgn = pMkt->u1TcpOptionIgnore;
                    MEMCPY (TcpAoMktAdr.au1Key, pMkt->au1TcpAOMasterKey,
                            TcpAoMktAdr.u1KeyLen);

                    /* Set value on listen socket */
                    i4RetVal =
                        Bgp4TcphAuthOptionMktSet (pPeerentry, &TcpAoMktAdr);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        pPeerentry->pTcpAOAuthMKT = pMkt;
                    }

                    /* Check if a peer session is established */
                    if ((BGP4_PEER_STATE (pPeerentry) >= BGP4_OPENSENT_STATE)
                        && (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID))
                    {

                        i4RetVal = Bgp4TcpAoMktSet (pPeerentry, pMkt);
                        if (i4RetVal < ZERO)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                      BGP4_MOD_NAME, "\tconfig error - \
                                      failed to set TCP-AO MKT on\
                                      connected socket\n ");
                        }
                    }
                }
                    break;

                case BGP4_TCPAO_MKT_NO_ASSOCIATE_CFG_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    tTcpAoAuthMKT      *pMkt;
                    tTcpAoMktAddr       TcpAoMktAdr;
                    INT1                i1Match = 0;

                    MEMSET (&PeerRemoteAddr, ZERO, sizeof (tAddrPrefix));
                    MEMSET (&TcpAoMktAdr, ZERO, sizeof (tTcpAoMktAddr));

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              pMsg->QMsg.Bgp4PeerMsg.
                                              PeerAddress);
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    /* Check MKt corresponfing to the keyId is configured in the MKT table */
                    TMO_SLL_Scan (&(BGP4_TCPAO_MKT_LIST (pMsg->u4ContextId)),
                                  pMkt, tTcpAoAuthMKT *)
                    {
                        if (pMsg->QMsg.Bgp4PeerMsg.u4Data ==
                            (INT4) pMkt->u1SendKeyId)
                        {
                            i1Match = TRUE;
                            break;
                        }
                    }
                    if (i1Match != TRUE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching MKT exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    else if (pMkt != pPeerentry->pTcpAOAuthMKT)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - MKT mismatch \n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    /* Set value on listen socket */
                    i4RetVal =
                        Bgp4TcphAuthOptionMktSet (pPeerentry, &TcpAoMktAdr);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        pPeerentry->pTcpAOAuthMKT = NULL;
                    }
                }
                    break;
                case BGP4_SNMP_INBOUND_ORF_EVENT:
                {
                    tAddrPrefix         PeerRemoteAddress;
                    Bgp4CopyAddrPrefixStruct (&(PeerRemoteAddress),
                                              BGP4_INPUTQ_RTREF_PEER_ADDR_INFO
                                              (pMsg));
                    Bgp4GetAfiSafiFromMask (BGP4_INPUTQ_RTREF_ASAFI_MASK (pMsg),
                                            &u2Afi, &u2Safi);
                    /* Send ORF message (route-refresh with ORF entries if configured)
                     * to peers */
                    i4RetVal =
                        Bgp4OrfRequestHandler (pMsg->u4ContextId,
                                               PeerRemoteAddress, u2Afi,
                                               u2Safi);
                    break;
                }

                case BGP4_PEER_GROUP_ORF_EVENT:
                {
                    pPeerGroup = Bgp4GetPeerGroupEntry (pMsg->u4ContextId,
                                                        BGP4_INPUTQ_PEER_GROUP_NAME
                                                        (pMsg));
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerentry = pPeerNode->pPeer;
                        i4RetVal =
                            Bgp4OrfRequestHandler (pMsg->u4ContextId,
                                                   BGP4_PEER_REMOTE_ADDR_INFO
                                                   (pPeerentry),
                                                   pPeerGroup->u2Afi,
                                                   BGP4_INET_SAFI_UNICAST);
                    }
                    pPeerGroup->u1OrfRqst = 0;
                    break;
                }

                case BGP4_PEER_SET_BFD_STATUS:
                {
                    tAddrPrefix         PeerRemoteAddr;

                    Bgp4CopyAddrPrefixStruct (&PeerRemoteAddr,
                                              BGP4_INPUTQ_PEER_ADDR_INFO
                                              (pMsg));
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    i4RetVal = Bgp4SetBfdStatus (pMsg->u4ContextId, pPeerentry);

                    break;
                }

                case BGP4_PEER_GROUP_SET_BFD_STATUS:
                {
                    pPeerGroup = Bgp4GetPeerGroupEntry (pMsg->u4ContextId,
                                                        BGP4_INPUTQ_PEER_GROUP_NAME
                                                        (pMsg));
                    if (pPeerGroup == NULL)
                    {
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }

                    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode,
                                  tPeerNode *)
                    {
                        pPeerNode->pPeer->u1BfdStatus = pPeerGroup->u1BfdStatus;
                        pDupPeer =
                            Bgp4SnmphGetDuplicatePeerEntry (pPeerNode->pPeer);

                        if (pDupPeer != NULL)
                        {
                            pDupPeer->u1BfdStatus = pPeerGroup->u1BfdStatus;
                        }

                        i4RetVal =
                            Bgp4SetBfdStatus (pMsg->u4ContextId,
                                              pPeerNode->pPeer);
                    }
                    break;
                }
                case BGP4_PEER_HANDLE_SESSION_DOWN_NOTIFICATION:
                {
                    tAddrPrefix         PeerRemoteAddr;
                    UINT4               u4Addr = 0;

                    MEMSET (&PeerRemoteAddr, 0, sizeof (tAddrPrefix));

                    /* Fill the Neighbor AddrInfo */
                    PeerRemoteAddr.u2Afi =
                        pMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2Afi;
                    PeerRemoteAddr.u2AddressLen =
                        pMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.u2AddressLen;

                    if (PeerRemoteAddr.u2Afi == BGP4_INET_AFI_IPV4)
                    {
                        MEMCPY (&u4Addr,
                                &(pMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.
                                  au1Address), BGP4_IPV4_PREFIX_LEN);
                        u4Addr = OSIX_HTONL (u4Addr);
                        MEMCPY (PeerRemoteAddr.au1Address, &u4Addr,
                                BGP4_IPV4_PREFIX_LEN);
                    }
                    else
                    {
                        MEMCPY (PeerRemoteAddr.au1Address,
                                &(pMsg->QMsg.Bgp4PathNotifyMsg.PeerAddr.
                                  au1Address), BGP4_MAX_INET_ADDRESS_LEN);
                    }

                    /* Fetch the matching BGP peer entry */
                    pPeerentry =
                        Bgp4SnmphGetPeerEntry (pMsg->u4ContextId,
                                               PeerRemoteAddr);
                    if (pPeerentry == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tConfig Error - No matching peer exist!!!\n");
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    /* Process the notification received from BFD, 
                     * only if the session state is Established*/
                    if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
                    {
                        i4RetVal = Bgp4ProcessPeerDownNotification (pPeerentry,
                                                                    pMsg->QMsg.
                                                                    Bgp4PathNotifyMsg.
                                                                    b1IsOffLoaded);
                    }

                    break;
                }
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
                    /*Interface with L2VPN with VPLS handler */
                case BGP4_L2VPN_MESSAGE:
#ifdef VPLS_GR_WANTED
                    if (BGP4_GET_NODE_STATUS != RM_ACTIVE)
                    {
                        /* If BGP Node is stand-by, No need to process the events coming from L2VPN
                         * It will update with Bulk and Dynamic Sync*/
                        break;
                    }
                    else
#endif
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                                  BGP4_MOD_NAME,
                                  "\t%s:%d In BGP4_L2VPN_MESSAGE processing\n",
                                  __func__, __LINE__);
                        u1MsgType = (UINT1) (pMsg->QMsg.Bgp4L2vpnMsg.u4MsgType);
                        i4RetVal = Bgp4VplsHandler ((UINT1 *) pMsg, u1MsgType);
                        break;
                    }
#endif /* MPLS_WANTED */
#endif /* VPLSADS_WANTED */
                case BGP4_ROUTE_CHG_EVENT:

                    Bgp4RouteChngHandler (pMsg);
                    break;

                default:
                    i4RetVal = BGP4_FAILURE;
                    break;
            }
        }
        BgpUnLock ();
        /*The Task from which the Event is posed is not waiting for the 
         * Response. Should not send Response */
        if (BGP4_INPUTQ_FLAG (pMsg) == BGP4_DONT_WAIT_FOR_RESP)
        {
            BGP4_QMSG_FREE (pMsg);
            continue;
        }
        /* After processing the QMsg, enqueue the return value to the
         * caller to indicate the status of the operation. Here the
         * Q message represents either BGP4_TRUE if configuration is
         * success or BGP4_FALSE if configuration fails.
         */
        if (i4RetVal == BGP4_SUCCESS)
        {
            u4RespMsg = BGP4_TRUE;
        }
        else
        {
            u4RespMsg = BGP4_FALSE;
        }

        /* Post the response message to the Response Queue. */
        if (OsixQueSend (BGP4_RES_Q_ID, (UINT1 *) &u4RespMsg, OSIX_DEF_MSG_LEN)
            != OSIX_SUCCESS)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tError - Posting message to Response Queue "
                      "failed!!!\n");
        }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if (pMsg->u4ContextId != VCM_DEFAULT_CONTEXT)
        {
            if (pLnxVrfInfo != NULL)
            {
                LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
            }
        }
#endif
        BGP4_QMSG_FREE (pMsg);
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name : Bgp4Enqueue                                                */
/* Description   : This function enqueues the input message in the            */
/*                 specified input-queue of the specified Task.               */
/* Input(s)      : pu1TaskName,                                               */
/*                 Queue to which the message needs to be enqueued (pu1Qname) */
/*                 Protocol message (pMsg) which needs to be posted           */
/*                 Event which needs to be posted (u4Event)                   */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Enqueue (tBgp4QMsg * pMsg)
{
    UINT4              *pu4RespMsg = 0;
    UINT4               u4Flag;
    UINT4               u4Timeout = OSIX_WAIT;

    /* Post the message first and then post the event. In few environment
     * that support Pre-emption the lower priority task will be pre-empted
     * after posting the event. So always post the message before the
     * event gets posted. */
    if (OsixQueSend (BGP4_Q_ID, (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Posting message to Queue failed!!!\n");
        BGP4_QMSG_FREE (pMsg);
        return (BGP4_FAILURE);
    }
    u4Flag = BGP4_INPUTQ_FLAG (pMsg);

    /* Send event accordingly */
    OsixEvtSend (BGP4_TASK_ID, BGP4_INPUT_Q_EVENT);

    /*If the Event id for SNMP and CLI, it should be blocked 
     * till response comes*/
    if (u4Flag == BGP4_DONT_WAIT_FOR_RESP)
    {
        return BGP4_SUCCESS;
    }
    BgpUnLock ();
    if (OsixQueRecv (BGP4_RES_Q_ID, (UINT1 *) &pu4RespMsg,
                     OSIX_DEF_MSG_LEN, (INT4) u4Timeout) == OSIX_SUCCESS)

    {
        /* The Response Q-Msg pointer just holds the status of the
         * operation performed i.e BGP4_TRUE or BGP4_FASLE.
         */
        if ((pu4RespMsg == NULL) || (pu4RespMsg == (UINT4 *) BGP4_FALSE))
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME, "\tError - Configuration FAILS!!!\n");
            return (BGP4_FAILURE);
        }
    }
    BgpLock ();
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name   : Bgp4EnqueMsgToBgpQ                                      */
/* Description     : This function enqueues the given message and date       */
/*                   to the BGP Queue.                                       */
/* Input(s)        : The EVENT to be handled by BGP Task (u4Event)           */
/*                   The Data associated with the EVENT (u4Data)             */
/* Output(s)       : None.                                                   */
/* Returns         :  SNMP_SUCCESS/SNMP_FAILURE                              */
/*****************************************************************************/
INT1
Bgp4EnqueMsgToBgpQ (UINT4 u4Context, UINT4 u4Event, UINT4 u4Data)
{
    tBgp4QMsg          *pQMsg = NULL;

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Configuration Msg to BGP "
                  "Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return SNMP_FAILURE;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = u4Event;
    BGP4_INPUTQ_DATA (pQMsg) = u4Data;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#ifdef L3VPN
/********************************************************************/
/* Function Name   : BgpMplsLspStatusChangeNotification             */
/* Description     : Call back function for MPLS LSP state NOTIFY   */
/* Input(s)        : u4LspId - LSP identifier                       */
/*                 : u4LspStatus - new status                       */
/* Output(s)       : None.                                          */
/* Returns         : None.                                          */
/********************************************************************/
VOID
BgpMplsLspStatusChangeNotification (UINT4 u4LspId, UINT1 u1LspStatus)
{
    tBgp4QMsg          *pQMsg = NULL;

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Lsp Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_LSP_STATE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_LSP_ID (pQMsg) = u4LspId;
    BGP4_INPUTQ_LSP_STATUS (pQMsg) = u1LspStatus;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/**************************************************************************/
/* Function Name   : BgpMplsVrfAdminStatusChangeNotification              */
/* Description     : Call back function for MPLS VRF admin state NOTIFY   */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
BgpMplsVrfAdminStatusChangeNotification (UINT4 u4VrfId, UINT1 u1VrfStatus)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP vrf admin Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_VRF_ADMIN_STATE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_INPUTQ_VRF_STATUS (pQMsg) = u1VrfStatus;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/**************************************************************************/
/* Function Name   : BgpMplsRouteParamsNotification                       */
/* Description     : Call back function for MPLS route params NOTIFY      */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
BgpMplsRouteParamsNotification (UINT4 u4VrfId, UINT1 u1ParamType,
                                UINT1 u1Action, UINT1 *pu1RouteParam)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP route params Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTE_PARAMS_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_VRF_ID (pQMsg) = u4VrfId;

    MEMCPY (BGP4_INPUTQ_ROUTE_PARAMS (pQMsg), pu1RouteParam,
            EXT_COMM_VALUE_LEN);
    BGP4_INPUTQ_PARAM_TYPE (pQMsg) = u1ParamType;
    BGP4_INPUTQ_ACTION_TYPE (pQMsg) = u1Action;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}
#endif

#ifdef EVPN_WANTED
/**************************************************************************/
/* Function Name   : BgpVxlanVrfAdminStatusChangeNotification             */
/* Description     : Call back function for EVPN VRF admin state NOTIFY   */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
BgpVxlanVrfAdminStatusChangeNotification (UINT4 u4VrfId, UINT4 u4VnId,
                                          UINT1 u1VrfStatus)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP vrf admin Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_VRF_ADMIN_STATE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VnId;
    BGP4_VXLAN_INPUTQ_VRF_STATUS (pQMsg) = u1VrfStatus;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/**************************************************************************/
/* Function Name   : Bgp4VxlanEvpnNotifyMac                               */
/* Description     : Call back function for EVPN Mac NOTIFY               */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
Bgp4VxlanEvpnNotifyMac (UINT4 u4VrfId, UINT4 u4VnId, tMacAddr MacAddress,
                        UINT1 u1IpAddrLen, UINT1 *pau1IpAddr, UINT1 u1Action,
                        UINT1 u1EntryStatus)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Mac Update Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_NOTIFY_MAC;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VnId;
    MEMCPY (BGP4_VXLAN_INPUTQ_MAC_ADDR (pQMsg), MacAddress, sizeof (tMacAddr));
    BGP4_VXLAN_INPUTQ_ACTION_TYPE (pQMsg) = u1Action;
    BGP4_VXLAN_INPUTQ_ENTRY_STATUS_TYPE (pQMsg) = u1EntryStatus;
    MEMCPY (BGP4_VXLAN_INPUTQ_IP_ADDR (pQMsg), pau1IpAddr, u1IpAddrLen);
    BGP4_VXLAN_INPUTQ_IP_ADDR_LEN (pQMsg) = u1IpAddrLen;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/**************************************************************************/
/* Function Name   : Bgp4VxlanEvpnESINotification                         */
/* Description     : Call back function to notify ESI for EVPN            */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                   u4VnId  - VN Id                                      */
/*                   pEthSegId - Ethernet Segment Identifier              */
/*                   u1Action - Add or Delete                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
Bgp4VxlanEvpnESINotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pEthSegId,
                              UINT1 *pu1IpAddr, INT4 i4IpAddrLen,
                              UINT1 u1Action)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Mac Update Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_NOTIFY_ESI;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VnId;
    MEMCPY (BGP4_VXLAN_INPUTQ_ETH_SEG_ID (pQMsg), pEthSegId, 10);
    MEMCPY (BGP4_VXLAN_INPUTQ_IP_ADDR (pQMsg), pu1IpAddr, i4IpAddrLen);
    BGP4_VXLAN_INPUTQ_IP_ADDR_LEN (pQMsg) = i4IpAddrLen;
    BGP4_VXLAN_INPUTQ_ACTION_TYPE (pQMsg) = u1Action;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/**************************************************************************/
/* Function Name   : Bgp4VxlanEvpnEthADNotification                       */
/* Description     : Call back function to notify Ethernet AD route       */
/*                   for EVPN                                             */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                   u4VnId  - VN Id                                      */
/*                   pEthSegId - Ethernet Segment Identifier              */
/*                   i4Flag - (0 - All Active / 1 - Single Active )       */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
Bgp4VxlanEvpnEthADNotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pEthSegId,
                                INT4 i4Flag)
{
    tBgp4QMsg          *pQMsg = NULL;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Mac Update Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_NOTIFY_ETH_AD;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VnId;
    MEMCPY (BGP4_VXLAN_INPUTQ_ETH_SEG_ID (pQMsg), pEthSegId, 10);
    BGP4_VXLAN_INPUTQ_SINGLE_ACTIVE_FLAG (pQMsg) = i4Flag;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;

}

/**************************************************************************/
/* Function Name   : BgpVxlanRouteParamsNotification                      */
/* Description     : Call back function for EVPN route params NOTIFY      */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
BgpVxlanRouteParamsNotification (UINT4 u4VrfId, UINT4 u4VNId, UINT1 u1ParamType,
                                 UINT1 u1Action, UINT1 *pu1RouteParam)
{
    tBgp4QMsg          *pQMsg = NULL;
    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP route params Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_ROUTE_PARAMS_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VNId;

    MEMCPY (BGP4_VXLAN_INPUTQ_ROUTE_PARAMS (pQMsg), pu1RouteParam,
            EXT_COMM_VALUE_LEN);
    BGP4_VXLAN_INPUTQ_PARAM_TYPE (pQMsg) = u1ParamType;
    BGP4_VXLAN_INPUTQ_ACTION_TYPE (pQMsg) = u1Action;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}
#endif

#ifdef RRD_WANTED
/*****************************************************************************/
/* Function        : bgp4RtmMsgHandler                                       */
/* Description     : This procedure dequeues the messages from RTM and       */
/*                   processes it based on the message type.                 */
/* Input           : Message received from RTM (pRtmMsg)                     */
/* Output          : None                                                    */
/* Returns         : BGP4_SUCCESS                                            */
/*****************************************************************************/
INT4
Bgp4RtmMsgHandler (tBgp4QMsg * pRtmMsg)
{
    UINT4               u4RetVal = BGP4_SUCCESS;

    switch (pRtmMsg->u4MsgType)
    {
        case BGP4_RTM_REG_EVENT:
            if (BGP4_RTM_REG_ID (BGP4_INPUTQ_CXT (pRtmMsg)) == 0)
            {
                BGP4_RTM_REG_ID (BGP4_INPUTQ_CXT (pRtmMsg)) =
                    (UINT1) pRtmMsg->QMsg.RtmRegnPtr.RtmHdr.RegnId.u2ProtoId;
            }
            Bgp4RegAckMsg (pRtmMsg);
            break;
        case BGP4_RTM_RECV_INFO_EVENT:
            if (BGP4_RTM_REG_ID (BGP4_INPUTQ_CXT (pRtmMsg)) == 0)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - RTM not registered.\n");
                u4RetVal = BGP4_FAILURE;
                break;
            }
            Bgp4EnableRRDProtoMask (BGP4_INPUTQ_CXT (pRtmMsg),
                                    BGP4_RRD_PROTO_MASK (BGP4_INPUTQ_CXT
                                                         (pRtmMsg)));
#ifdef BGP4_IPV6_WANTED
        case BGP4_RTM6_REG_EVENT:
            if (BGP4_RTM_REG_ID (BGP4_INPUTQ_CXT (pRtmMsg)) == 0)
            {
                BGP4_RTM_REG_ID (BGP4_INPUTQ_CXT (pRtmMsg)) =
                    (UINT1) pRtmMsg->QMsg.Rtm6RegnPtr.RtmHdr.RegnId.u2ProtoId;
            }
            Bgp4RegAckMsg (pRtmMsg);
            break;
#endif
        default:
            break;
    }
    if (BGP4_RRD_PROTO_MASK (BGP4_INPUTQ_CXT (pRtmMsg)) != 0)
    {
        Bgp4EnableRRDProtoMask (BGP4_INPUTQ_CXT (pRtmMsg),
                                BGP4_RRD_PROTO_MASK (BGP4_INPUTQ_CXT
                                                     (pRtmMsg)));
    }
    return u4RetVal;
}
#endif

/* ENVIRONMENT SPECIFIC INITIALIZATIONS */

/******************************************************************************/
/* Function Name : Bgp4InitEnv                                                */
/* Description   : This function uses the services provided by the environment*/
/*               : for initializing BGP at start-up.                          */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4InitEnv (UINT4 u4CxtId)
{
#ifdef RRD_WANTED
    tRtmRegnId          RegnId;
#endif
#ifdef BGP4_IPV6_WANTED
    tRtm6RegnId         rtm6RegnId;
#endif

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
#ifdef BGP4_IPV6_WANTED
    MEMSET (&rtm6RegnId, 0, sizeof (tRtm6RegnId));
#endif /* BGP4_IPV6_WANTED */

    if (Bgp4InitPeer (u4CxtId) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tBgp4InitEnv() : Initializing Peer Entries FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    if (Bgp4TrieInit (u4CxtId) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tTRIE Initialization FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    /* BGP4_GR : Do not register again with RTM for a graceful restart */
    if (BGP4_RTM_REG_ID (u4CxtId) == 0)
    {
#ifdef RRD_WANTED
        /* Register with RTM */
        RegnId.u2ProtoId = BGP_ID;
        RegnId.u4ContextId = u4CxtId;
        RtmRegister (&RegnId, SET_MASK, Bgp4RecvMsgFromRtm);
#endif

#ifdef BGP4_IPV6_WANTED
        rtm6RegnId.u2ProtoId = BGP6_ID;
        rtm6RegnId.u4ContextId = u4CxtId;
        Rtm6Register (&rtm6RegnId, RTM6_ACK_REQUIRED, Bgp4RecvMsgFromRtm6);
#endif
    }

    Bgp4InitRedistribution (u4CxtId);
    if ((IssGetCsrRestoreFlag () == BGP4_TRUE)
        && (BGP4_RTM_REG_ID (u4CxtId) == 0))
    {
        BGP4_RTM_REG_ID (u4CxtId) = BGP_ID;
    }

    Bgp4InitFiltering (u4CxtId);
    BGP4_TIMER_START (BGP4_1S_TIMER_LISTID, gBgpNode.pBgpOneSecTmr,
                      BGP4_ONE_SECOND);

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitPeer                                               */
/* Description   : This function allocates buffer pool to store information   */
/*                 about the BGP peers & also initialises the head node of    */
/*                 the BGP Peer list.                                         */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4InitPeer (UINT4 u4CxtId)
{
    if (Bgp4RtRefMemInit (u4CxtId) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitPeer: Initialization for Route "
                  "Refresh FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    if (Bgp4TcpMd5MemInit (u4CxtId) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitPeer: Peer TCP-MD5 MemInit FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    TMO_SLL_Init (BGP4_PEERENTRY_HEAD (u4CxtId));
    TMO_SLL_Init (BGP4_PEER_TRANS_LIST (u4CxtId));
    TMO_SLL_Init (BGP4_PEERGROUPENTRY_HEAD (u4CxtId));

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4CreatePools                                            */
/* Description   : This function creates mem-pools at startup, for use by BGP */
/*               : dynamically.Memory is allocated from these pools for the   */
/*               : RIB data-structures,the BGP task input Q.                  */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4CreatePools ()
{
    if (BgpSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4CreatePools() : MemPools Creation FAILED !!!\n");
        return (BGP4_FAILURE);
    }
    gBgpNode.BgpGlobalCxtMemPoolId =
        BGPMemPoolIds[MAX_BGP_GLOBAL_CXT_SIZING_ID];
    gBgpNode.BgpCxtMemPoolId = BGPMemPoolIds[MAX_BGP_CXT_NODES_SIZING_ID];
    gBgpNode.Bgp4RfdDecayPoolId =
        BGPMemPoolIds[MAX_BGP_RFD_DECAY_ARRAY_BLOCKS_SIZING_ID];
    gBgpNode.Bgp4RfdReuseIndexPoolId =
        BGPMemPoolIds[MAX_BGP_RFD_REUSE_INDEX_ARRAY_BLOCKS_SIZING_ID];
    gBgpNode.Bgp4PeerGroupPoolId = BGPMemPoolIds[MAX_BGP_PEER_GROUPS_SIZING_ID];
    gBgpNode.Bgp4PeerEntryPoolId = BGPMemPoolIds[MAX_BGP_PEERS_SIZING_ID];
    gBgpNode.Bgp4PeerDataEntryPoolId =
        BGPMemPoolIds[MAX_BGP_DATA_PEERS_SIZING_ID];
    gBgpNode.Bgp4RtProfilePoolId = BGPMemPoolIds[MAX_BGP_ROUTES_SIZING_ID];
    gBgpNode.Bgp4BufNodeMsgPoolId =
        BGPMemPoolIds[MAX_BGP_BUFNODE_MSGS_SIZING_ID];
    gBgpNode.Bgp4BufNodesPoolId = BGPMemPoolIds[MAX_BGP_BUF_NODES_SIZING_ID];
    gBgpNode.Bgp4BgpInfoPoolId =
        BGPMemPoolIds[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID];
    gBgpNode.Bgp4PAPoolId = BGPMemPoolIds[MAX_BGP_RCVD_PATH_ATTRIBS_SIZING_ID];
    gBgpNode.Bgp4AdvtPAPoolId =
        BGPMemPoolIds[MAX_BGP_ADVT_PATH_ATTRIBS_SIZING_ID];
    gBgpNode.Bgp4AsPathPoolId = BGPMemPoolIds[MAX_BGP_AS_PATHS_SIZING_ID];
    gBgpNode.Bgp4LinkNodePoolId = BGPMemPoolIds[MAX_BGP_LINK_NOCDES_SIZING_ID];
    gBgpNode.Bgp4AdvRtLinkNodePoolId =
        BGPMemPoolIds[MAX_BGP_ADVT_LINK_NODES_SIZING_ID];
    gBgpNode.Bgp4NetworkRtPoolId =
        BGPMemPoolIds[MAX_BGP_NETWORK_ROUTES_SIZING_ID];
    gBgpNode.Bgp4InputQPoolId = BGPMemPoolIds[MAX_BGP_Q_MSGS_SIZING_ID];
    BGP4_NEXTHOP_METRIC_TBL_POOLID =
        BGPMemPoolIds[MAX_BGP_IGP_METRIC_ENTRIES_SIZING_ID];
    BGP4_IFACE_LIST_POOLID = BGPMemPoolIds[MAX_BGP_IF_INFOS_SIZING_ID];
    gBgpNode.Bgp4PollTmrPoolId = BGPMemPoolIds[MAX_BGP_TIMERS_SIZING_ID];
    BGP4_ORF_MEM_POOL_ID = BGPMemPoolIds[MAX_BGP_ORF_ENTRIES_SIZING_ID];
#ifdef EVPN_WANTED
    BGP4_EVPN_VRF_VNI_MEM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_EVPN_VRF_VNI_CHG_NODES_SIZING_ID];
    BGP4_MAC_DUP_TIMER_MEM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_MAC_DUP_TIMERS_SIZING_ID];
#endif
    BGP4_RTREF_MEM_POOL_ID = BGPMemPoolIds[MAX_BGP_RT_REF_DATA_SIZING_ID];
    BGP4_TCPMD5_MEM_POOL_ID = BGPMemPoolIds[MAX_BGP_MD5_AUTH_PASSWDS_SIZING_ID];
    PEER_CAPS_MEMPOOLID = BGPMemPoolIds[MAX_BGP_SUPP_CAP_INFOS_SIZING_ID];
    BGP4_MPE_RT_SNPA_POOL_ID = BGPMemPoolIds[MAX_BGP_SNPA_INFO_NODES_SIZING_ID];
    BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID =
        BGPMemPoolIds[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID];
    INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID =
        BGPMemPoolIds[MAX_BGP_EXT_COMM_IN_FILTER_INFOS_SIZING_ID];
    OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID =
        BGPMemPoolIds[MAX_BGP_EXT_COMM_OUT_FILTER_INFOS_SIZING_ID];
    ROUTE_CONF_EXT_COMM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_RT_CONF_EXT_COMMUNITIES_SIZING_ID];
    EXT_COMM_PROFILE_POOL_ID =
        BGPMemPoolIds[MAX_BGP_EXT_COMM_PROFILES_SIZING_ID];
    PEER_LINK_BANDWIDTH_POOL_ID =
        BGPMemPoolIds[MAX_BGP_PEER_LINK_BANDWIDTHS_SIZING_ID];
    ROUTES_EXT_COMM_SET_STATUS_POOL_ID =
        BGPMemPoolIds[MAX_BGP_RT_EXT_COMM_SET_CONFIGS_SIZING_ID];

    INPUT_COMM_FILTER_ENTRY_POOL_ID =
        BGPMemPoolIds[MAX_BGP_COMM_IN_FILTER_INFOS_SIZING_ID];
    OUTPUT_COMM_FILTER_ENTRY_POOL_ID =
        BGPMemPoolIds[MAX_BGP_COMM_OUT_FILTER_INFOS_SIZING_ID];
    ROUTE_CONF_COMM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_RT_CONF_COMMUNITIES_SIZING_ID];
    COMM_PROFILE_POOL_ID = BGPMemPoolIds[MAX_BGP_COMM_PROFILES_SIZING_ID];
    ROUTES_COMM_SET_STATUS_POOL_ID =
        BGPMemPoolIds[MAX_BGP_RT_COMM_SET_CONFIGS_SIZING_ID];
    RT_DAMP_HIST_MEMPOOLID = BGPMemPoolIds[MAX_BGP_RT_DAMP_HISTORIES_SIZING_ID];
    PEER_DAMP_HIST_MEMPOOLID =
        BGPMemPoolIds[MAX_BGP_PEER_DAMP_HISTORIES_SIZING_ID];
    REUSE_PEER_MEMPOOLID = BGPMemPoolIds[MAX_BGP_REUSE_PEERS_SIZING_ID];
    BGP_AGGR_NODE_POOL_ID = BGPMemPoolIds[MAX_BGP_AGGR_NODE_SIZING_ID];
    BGP_COMMUNITY_NODE_POOL_ID =
        BGPMemPoolIds[MAX_BGP_COMMUNITY_NODE_SIZING_ID];
    BGP_CLUSTER_LIST_POOL_ID = BGPMemPoolIds[MAX_BGP_CLUSTER_LIST_SIZING_ID];
    BGP_EXT_COMMUNITY_NODE_POOL_ID =
        BGPMemPoolIds[MAX_BGP_EXT_COMM_NODE_SIZING_ID];
    BGP_AFI_SAFI_NODE_POOL_ID = BGPMemPoolIds[MAX_BGP_AFI_SAFI_NODE_SIZING_ID];
    BGP_ATTRIBUTE_LEN_POOL_ID = BGPMemPoolIds[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID];
    BGP_PEER_NODE_POOL_ID = BGPMemPoolIds[MAX_BGP_PEER_NODE_SIZING_ID];
    BGP_CLI_MEM_BLK_BUF_POOL_ID = BGPMemPoolIds[MAX_BGP_CLI_MEM_BUF_SIZING_ID];
    BGP4_RED_MSG_MEMPOOL_ID = BGPMemPoolIds[MAX_BGP_RM_MSGS_SIZING_ID];
    BGP4_TCPAO_MEM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_TCPAO_AUTH_PASSWDS_SIZING_ID];
#ifdef L3VPN
    BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_VRF_NODES_SIZING_ID];
    BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_RR_IMPORT_TARGETS_SIZING_ID];
    BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_VRF_CHG_NODES_SIZING_ID];
    BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_LSP_CHG_NODES_SIZING_ID];
#endif
#ifdef EVPN_WANTED
    BGP4_EVPN_RR_TARGETS_SET_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_EVPN_RR_IMPORT_TARGETS_SIZING_ID];
#endif
    BGP4_ADV_ROUTES_MEM_POOL_ID =
        BGPMemPoolIds[MAX_BGP_ADV_ROUTES_INFOS_SIZING_ID];
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4InitTimer                                            */
/* Description   : This function intialises the Timer List and the other      */
/*                 Timer related data structures.                             */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

INT4
Bgp4InitTimer ()
{

    /* Create Timer-Lists for One Second Timer */
    if (TmrCreateTimerList ((const UINT1 *) BGP4_TASKNAME, BGP4_1S_TMR_EVENT,
                            NULL, &(BGP4_1S_TIMER_LISTID)) != TMR_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4 One Sec Timer List Creation FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    /* Create Timer-Lists for Peers */
    if (TmrCreateTimerList ((const UINT1 *) BGP4_TASKNAME, BGP4_TMR_EVENT,
                            NULL, &(BGP4_TIMER_LISTID)) != TMR_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4 Timer List Creation FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    return (BGP4_SUCCESS);
}

/****************************************************************************
 * FUNCTION NAME : Bgp4IsCpuNeeded
 * DESCRIPTION   : This is a portable function that can be used to determine -
 *               : -if BGP needs to relinquish USE of CPU
 *               : -if any time-critical events needs to be processed
 *               :  by other tasks 
 * INPUTS        : None
 * OUTPUTS       : None
 * RETURNS       : BGP4_TRUE,if BGP events need to be processed 
 *               : BGP4_FALSE,if no processing remains 
 *****************************************************************************/
INT4
Bgp4IsCpuNeeded ()
{
    /* Block introduced to fix the klocwork warning UNREACH_RETURN; 
     * since it returns false always */
    if (BGP4_TASK_INIT_STATUS == BGP4_FALSE)
    {
        return BGP4_TRUE;
    }
    return (BGP4_FALSE);
}

/****************************************************************************
 Function    :  Bgp4GlobalAdminStatusHandler
 Description :  This function is called to process the change in global admin 
             :  status of the BGP system.
 Input       :  u4Event - Global Admin Up/Down 
 Output      :  None 
 Returns     :  BGP4_SUCCESS, if operation is a success
             :  BGP4_FAILURE, if the operation fails
****************************************************************************/
INT1
Bgp4GlobalAdminStatusHandler (UINT4 u4Context, UINT4 u4Event)
{
#ifdef RFD_WANTED
    UINT1              *pu1Block = NULL;
#endif /*RFD_WANTED */
    INT4                i4RetVal;
    UINT1               u1retValue = SNMP_FAILURE;
    tNetworkAddr       *pNetworkRt = NULL;

    switch (u4Event)
    {
        case BGP4_ADMIN_UP:
            if ((BGP4_GLOBAL_STATE (u4Context) ==
                 BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS) &&
                (BGP4_ADMIN_STATUS_FLAG (u4Context) == OSIX_FALSE))
            {
                BGP4_RESET_GLOBAL_FLAG (u4Context, BGP4_GLOBAL_CLEAR_PENDING);
                BGP4_SET_GLOBAL_FLAG (u4Context, BGP4_GLOBAL_ADMIN_UP_PENDING);
                return BGP4_SUCCESS;
            }
            if ((BGP4_GLOBAL_STATE (u4Context) != BGP4_GLOBAL_READY) &&
                (BGP4_ADMIN_STATUS_FLAG (u4Context) == OSIX_FALSE))
            {
                return BGP4_SUCCESS;
            }
            if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
            {
                return BGP4_SUCCESS;
            }
            /* Set the global Admin status. */
            BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_UP;
            BGP4_CLI_ADMIN_STATUS (u4Context) = BGP4_ADMIN_UP;
#ifdef L3VPN
            /*NOTE: Mpls Label Space should be created before registering 
             * with IP*/
            /* Register with MPLS to get information about LSPs and Labels */
            /* Create Label Space for getting labels */

            if (u4Context == BGP4_DFLT_VRFID)
            {
                i4RetVal = Bgp4CreateLabelSpace ();
                if (i4RetVal != BGP4_SUCCESS)
                {
                    BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                    return (BGP4_FAILURE);
                }

            }
#endif
            /* Register with IP to get interface related information, 
             * VRF update  Notifications */
            i4RetVal = Bgp4RegisterWithIP (u4Context);
            if (i4RetVal != BGP4_SUCCESS)
            {
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                return (BGP4_FAILURE);
            }

#ifdef BGP4_IPV6_WANTED
            i4RetVal =
                NetIpv6RegisterHigherLayerProtocolInCxt (u4Context, BGP_ID,
                                                         NETIPV6_ROUTE_CHANGE,
                                                         BgpIp6CbRtChgHandler);
            if (i4RetVal != BGP4_SUCCESS)
            {
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                return (BGP4_FAILURE);
            }
#endif

#ifdef RFD_WANTED
            /*Rfd Iniatialize to be done at Admin UP state */
            if (RfdMemInit (u4Context) != RFD_SUCCESS)
            {
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                Bgp4DeRegisterWithIP (u4Context);
                return (BGP4_FAILURE);
            }

            RfdInitialise (u4Context);

            /* Start the RFD REUSE Timer */
            pu1Block = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PollTmrPoolId);

            if (pu1Block == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tAllocate Memory from Timer MemPool FAILED !!!\n");
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                Bgp4DeRegisterWithIP (u4Context);
                RfdShutDown (u4Context);
                gu4BgpDebugCnt[MAX_BGP_TIMERS_SIZING_ID]++;
                return (BGP4_FAILURE);
            }
            RFD_REUSE_TIMER_ENTRY (u4Context) =
                (tBgp4AppTimer *) (VOID *) pu1Block;
            if (NULL != RFD_REUSE_TIMER_ENTRY (u4Context))
            {
                (RFD_REUSE_TIMER_ENTRY (u4Context))->u4Flag = BGP4_ACTIVE;
                (RFD_REUSE_TIMER_ENTRY (u4Context))->u4TimerId =
                    BGP4_RFD_REUSE_TIMER;
                (RFD_REUSE_TIMER_ENTRY (u4Context))->Tmr.u4Data =
                    (FS_ULONG) Bgp4GetContextEntry (u4Context);
            }
            BGP4_TIMER_START (BGP4_TIMER_LISTID,
                              (tTmrAppTimer *)
                              RFD_REUSE_TIMER_ENTRY (u4Context),
                              RFD_REUSE_TIMER_GRANULARITY (u4Context));
            RFD_ADMIN_STATUS (u4Context) = BGP4_TRUE;
#endif /*RFD_WANTED */
            u1retValue = Bgp4EnableRRD (u4Context);

            if ((SNMP_SUCCESS == u1retValue) &&
                (((IssGetCsrRestoreFlag () == BGP4_TRUE) &&
                  (BGP4_RTM_REG_ID (u4Context) == BGP_ZERO)) ||
                 (u4Context != BGP4_DFLT_VRFID)))
            {
                BGP4_RTM_REG_ID (u4Context) = BGP_ID;
            }
            if ((BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4Context) ==
                 BGP4_LOCAL_BGP_ID_DYNAMIC)
                && (BGP4_LOCAL_BGP_ID (u4Context) == BGP4_INV_IPADDRESS))

            {
                Bgp4GetDefaultBgpIdentifier (u4Context,
                                             &(BGP4_LOCAL_BGP_ID (u4Context)));
            }
#ifdef BGP_TCP4_WANTED
            i4RetVal = Bgp4TcphOpenListenPort (u4Context, BGP4_INET_AFI_IPV4);
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                Bgp4RRDDisableMsgToRTM (u4Context);
                BGP4_RRD_ADMIN_STATE (u4Context) = BGP4_RRD_DISABLE;
#ifdef RFD_WANTED
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                                 (tTmrAppTimer *)
                                 RFD_REUSE_TIMER_ENTRY (u4Context));
                if (RFD_REUSE_TIMER_ENTRY (u4Context) != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                                        (UINT1 *)
                                        RFD_REUSE_TIMER_ENTRY (u4Context));
                    RFD_REUSE_TIMER_ENTRY (u4Context) = NULL;
                }
                Bgp4DeRegisterWithIP (u4Context);
                RfdShutDown (u4Context);
#endif /*RFD_WANTED */
                return ((INT1) i4RetVal);
            }
#endif /* BGP_TCP4_WANTED */
#ifdef BGP_TCP6_WANTED
            i4RetVal = Bgp4TcphOpenListenPort (u4Context, BGP4_INET_AFI_IPV6);
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
                Bgp4RRDDisableMsgToRTM (u4Context);
                BGP4_RRD_ADMIN_STATE (u4Context) = BGP4_RRD_DISABLE;
#ifdef RFD_WANTED
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                                 (tTmrAppTimer *)
                                 RFD_REUSE_TIMER_ENTRY (u4Context));
                if (RFD_REUSE_TIMER_ENTRY (u4Context) != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                                        (UINT1 *)
                                        RFD_REUSE_TIMER_ENTRY (u4Context));
                    RFD_REUSE_TIMER_ENTRY (u4Context) = NULL;
                }
                RfdShutDown (u4Context);
#endif /*RFD_WANTED */
#ifdef BGP_TCP4_WANTED
                i4RetVal =
                    Bgp4TcphCloseListenPort (u4Context, BGP4_INET_AFI_IPV4);
#endif
                Bgp4DeRegisterWithIP (u4Context);

                return ((INT1) i4RetVal);
            }

#endif /*BGP_TCP6_WANTED */

            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                Bgp4PeerSetAllActivePeerAdminStatus (u4Context, BGP4_ADMIN_UP);
            }
            BGP4_GLOBAL_STATE (u4Context) = BGP4_GLOBAL_ADMIN_UP_INPROGRESS;
            Bgp4RedVerifyAndSendBulkRequest ();
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
            /* After BGP Graceful Restart, BGP will not send BGP4_ADMIN_UP event to L2VPN
             * BGP module will send L2VPN_BGP_GR_IN_PROGRESS message after GR at the time 
             * of GR_INIT*/
            if ((u4Context == BGP4_DFLT_VRFID)
#ifdef VPLS_GR_WANTED
                && (BGP4_RESTART_EXIT_REASON (u4Context) !=
                    BGP4_GR_EXIT_INPROGRESS)
#endif
                )
            {
                Bgp4AdminStatusEventToL2Vpn (BGP4_ADMIN_UP);
            }
#endif /*MPLS_WANTED */
#endif
            pNetworkRt = (tNetworkAddr *)
                RBTreeGetFirst (BGP4_NETWORK_ROUTE_DATABASE (u4Context));
            while (pNetworkRt != NULL)
            {
                BgpNetworkIpRtLookup (u4Context, &(pNetworkRt->NetworkAddr),
                                      pNetworkRt->u2PrefixLen,
                                      BGP4_NETWORK_ROUTE_ADD,
                                      BGP4_ALL_APPIDS, 0, NULL);
                pNetworkRt = (tNetworkAddr *)
                    RBTreeGetNext (BGP4_NETWORK_ROUTE_DATABASE (u4Context),
                                   (tRBElem *) pNetworkRt, NULL);
            }
#ifdef L3VPN
            if ((u4Context != BGP4_DFLT_VRFID) &&
                (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
            {
                /* If VRF route leaking is enabled Get leak routes from Peer */
                Bgp4Vpnv4GetLeakRoutesFromPeer (u4Context);
            }
#endif

            break;

        case BGP4_ADMIN_DOWN:
            BGP4_ADMIN_STATUS_FLAG (u4Context) = OSIX_FALSE;
            if ((BGP4_GET_GLOBAL_FLAG (u4Context) &
                 BGP4_GLOBAL_ADMIN_UP_PENDING) == BGP4_GLOBAL_ADMIN_UP_PENDING)
            {
                /* Admin up still pending - can be reset */
                BGP4_RESET_GLOBAL_FLAG (u4Context,
                                        BGP4_GLOBAL_ADMIN_UP_PENDING);
            }
            if (BGP4_GLOBAL_STATE (u4Context) ==
                BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
            {
                return BGP4_SUCCESS;
            }
            if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
            {
                return BGP4_SUCCESS;
            }
            /* Set the global Admin status. */
            BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
            BGP4_CLI_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;
            if (gi1RFDSyncStatus != SYNC_IN_PROGRESS)
            {
#ifdef RFD_WANTED
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                                 (tTmrAppTimer *)
                                 RFD_REUSE_TIMER_ENTRY (u4Context));
                if (RFD_REUSE_TIMER_ENTRY (u4Context) != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                                        (UINT1 *)
                                        RFD_REUSE_TIMER_ENTRY (u4Context));
                    RFD_REUSE_TIMER_ENTRY (u4Context) = NULL;
                }
                RfdShutDown (u4Context);
                RFD_ADMIN_STATUS (u4Context) = BGP4_FALSE;
                RfdUpdateDefaultConfigValues (u4Context);
#endif
            }
            Bgp4DeRegisterWithIP (u4Context);
#ifdef L3VPN
            if ((u4Context != BGP4_DFLT_VRFID) &&
                (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
            {
                /* If VRF route leaking is Enabled Delete the
                 * Leaked routes from Peer*/
                Bgp4Vpnv4DelLeakRoutesFromPeer (u4Context);
            }

            if (BGP4_VPNV4_LABEL (u4Context) != 0)
            {
                Bgp4DelBgp4Label (BGP4_VPNV4_LABEL (u4Context));
            }

            if (u4Context == BGP4_DFLT_VRFID)
            {
                Bgp4DeleteLabelSpace ();
            }
#endif
            Bgp4RRDDisableMsgToRTM (u4Context);
            BGP4_RRD_ADMIN_STATE (u4Context) = BGP4_RRD_DISABLE;
            Bgp4PeerSetAllActivePeerAdminStatus (u4Context, BGP4_ADMIN_DOWN);
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
            if (u4Context == BGP4_DFLT_VRFID)
            {
                Bgp4AdminStatusEventToL2Vpn (BGP4_ADMIN_DOWN);
            }
#endif /*MPLS_WANTED */
#endif
#ifdef BGP_TCP4_WANTED
            i4RetVal = Bgp4TcphCloseListenPort (u4Context, BGP4_INET_AFI_IPV4);
#endif
#ifdef BGP_TCP6_WANTED
            i4RetVal = Bgp4TcphCloseListenPort (u4Context, BGP4_INET_AFI_IPV6);
#endif
            BGP4_GLOBAL_STATE (u4Context) = BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS;
            /* Also set the FIB update interval to the max value so that
             * the FIB is update immediately */
            BGP4_FIB_UPD_HOLD_TIME (u4Context) = BGP4_FIB_UPD_MAX_HOLD_TIME;
            break;

        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4RtRefMemInit                                          */
/* Description   : This function creates memory-pool required by the route   */
/*                 refresh feature at startup.                               */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4RtRefMemInit (UINT4 u4CxtId)
{
    /* Initialize the mem counters */
    BGP4_RTREF_ALLOC_CTR (u4CxtId) = 0;

    TMO_SLL_Init (BGP4_RTREF_PEER_LIST (u4CxtId));

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TcpMd5MemInit                                         */
/* Description   : This function creates memory-pool required for storing    */
/*                 the peer related TCP MD5 passwords and lengths.           */
/*                                                                           */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4TcpMd5MemInit (UINT4 u4CxtId)
{
    /* Initialize the mem counters */
    BGP4_TCPMD5_ALLOC_CTR (u4CxtId) = 0;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4RtRefShutdown                              */
/* Description     : De-allocates memory for the pools used in      */
/*                   Route-Refresh feature                          */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_RTREF_GLOBAL_DATA               */
/* Global variables Modified : BGP4_RTREF_GLOBAL_DATA               */
/* Exceptions or Operating System Error Handling :                  */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4RtRefShutdown (UINT4 u4Context)
{
    /* Release memory from the route-refresh peer list */
    Bgp4MemReleasePeerList (BGP4_RTREF_PEER_LIST (u4Context));

    BGP4_RTREF_ALLOC_CTR (u4Context) = 0;

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4GlobalMemoryRelease                        */
/* Description     : De-allocates all global resources allocated    */
/*                   dynamically.                                   */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4GlobalMemoryRelease (UINT4 u4Context)
{
    /* Release the aggregated route list. */
    Bgp4AggrReleaseAggrList (u4Context);
    Bgp4InitNetAddressStruct (&(BGP4_SYNC_INIT_ROUTE_NETADDR_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    BGP4_SYNC_INTERVAL (u4Context) = 0;

    Bgp4InitNetAddressStruct (&
                              (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4);
    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4);
    BGP4_NH_CHG_INTERVAL (u4Context) = 0;

    Bgp4InitNetAddressStruct (&(BGP4_NON_BGP_INIT_ROUTE_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);

#ifdef RFD_WANTED
    BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                     (tTmrAppTimer *) RFD_REUSE_TIMER_ENTRY (u4Context));
    if (RFD_REUSE_TIMER_ENTRY (u4Context) != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4PollTmrPoolId,
                            (UINT1 *) RFD_REUSE_TIMER_ENTRY (u4Context));
        RFD_REUSE_TIMER_ENTRY (u4Context) = NULL;
    }
    RfdShutDown (u4Context);
#endif /*RFD_WANTED */
    /* Delete configured capabilities. */
    CapsDeletePeerCaps (u4Context);

    /* Resetting the global read sock-desc set */
    BGP_FD_ZERO (BGP4_READ_SOCK_FD_SET (u4Context));
    BGP_FD_ZERO (BGP4_WRITE_SOCK_FD_SET (u4Context));

    /* Clear the Redistribute Routes List. */
    BGP4_STATIC_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_STATIC_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_STATIC_IMPORT_LIST_COUNT (u4Context) = 0;
    BGP4_DIRECT_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_DIRECT_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_DIRECT_IMPORT_LIST_COUNT (u4Context) = 0;
    BGP4_RIP_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_RIP_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_RIP_IMPORT_LIST_COUNT (u4Context) = 0;
    BGP4_OSPF_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_OSPF_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_OSPF_IMPORT_LIST_COUNT (u4Context) = 0;
    BGP4_ISISL1_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_ISISL1_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_ISISL1_IMPORT_LIST_COUNT (u4Context) = 0;
    BGP4_ISISL2_IMPORT_LIST_FIRST (u4Context) = NULL;
    BGP4_ISISL2_IMPORT_LIST_LAST (u4Context) = NULL;
    BGP4_ISISL2_IMPORT_LIST_COUNT (u4Context) = 0;

    /* Release the global peer lists */
    Bgp4MemReleasePeerList (BGP4_PEER_INIT_LIST (u4Context));
    Bgp4MemReleasePeerList (BGP4_PEER_DEINIT_LIST (u4Context));
    Bgp4MemReleasePeerList (BGP4_RTREF_PEER_LIST (u4Context));
    Bgp4MemReleasePeerList (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context));

    /* Release the global routes lists */
    Bgp4DshClearLinkList (u4Context, BGP4_EXT_PEERS_ADVT_LIST (u4Context),
                          BGP4_EXT_PEER_LIST_INDEX);
    Bgp4DshClearLinkList (u4Context, BGP4_INT_PEERS_ADVT_LIST (u4Context),
                          BGP4_INT_PEER_LIST_INDEX);
    /* Clear the Change_list */
    BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = NULL;
    BGP4_CHG_LIST_LAST_ROUTE (u4Context) = NULL;
    BGP4_CHG_LIST_COUNT (u4Context) = 0;
    Bgp4DshReleaseList (BGP4_REDIST_LIST (u4Context), 0);
    Bgp4DshReleaseList (BGP4_OVERLAP_ROUTE_LIST (u4Context), 0);
    Bgp4DshReleaseList (BGP4_NONSYNC_LIST (u4Context), 0);

    /* Delete ORF entries and table */
    Bgp4OrfReleaseOrfEntries (u4Context);

    /* Clear the Igp Metric Hash Table */
    Bgp4DeleteIgpMetricTable (u4Context);

    /* Reset the rrd and  non-bgp related events */
    if (BGP4_NON_BGP_EVENT_RCVD (u4Context) != 0)
    {
        BGP4_NON_BGP_RT_EXPORT_POLICY (u4Context) =
            BGP4_NON_BGP_EVENT_RCVD (u4Context);
        BGP4_NON_BGP_EVENT_RCVD (u4Context) = 0;
    }

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitRedistribution                                     */
/* Description   : This function registers with RTM by sending the            */
/*                 Registration message and sending it to RTM module.         */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitRedistribution (UINT4 u4CxtId)
{
    /* By Default, the Route Redistribution is enabled. But by default, no 
     * protocol will be enabled. */
    BGP4_RRD_PROTO_MASK (u4CxtId) = 0;
    BGP4_RRD_ADMIN_STATE (u4CxtId) = BGP4_RRD_ENABLE;
    BGP4_NON_BGP_EVENT_RCVD (u4CxtId) = 0;

    BGP4_STATIC_IMPORT_LIST_FIRST (u4CxtId) = NULL;
    BGP4_STATIC_IMPORT_LIST_LAST (u4CxtId) = NULL;
    BGP4_STATIC_IMPORT_LIST_COUNT (u4CxtId) = 0;
    BGP4_DIRECT_IMPORT_LIST_FIRST (u4CxtId) = NULL;
    BGP4_DIRECT_IMPORT_LIST_LAST (u4CxtId) = NULL;
    BGP4_DIRECT_IMPORT_LIST_COUNT (u4CxtId) = 0;
    BGP4_RIP_IMPORT_LIST_FIRST (u4CxtId) = NULL;
    BGP4_RIP_IMPORT_LIST_LAST (u4CxtId) = NULL;
    BGP4_RIP_IMPORT_LIST_COUNT (u4CxtId) = 0;
    BGP4_OSPF_IMPORT_LIST_FIRST (u4CxtId) = NULL;
    BGP4_OSPF_IMPORT_LIST_LAST (u4CxtId) = NULL;
    BGP4_OSPF_IMPORT_LIST_COUNT (u4CxtId) = 0;

    return;
}

/****************************************************************************/
/* Function Name : Bgp4IfaceChngHandler                                     */
/* Description   : Handles the interface changes                            */
/* Input(s)      : u4IfIndex - interface index                              */
/*                 u4OperStatus - operational status                        */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4IfaceChngHandler (UINT4 u4IfIndex, UINT4 u4OperStatus)
{
    tBgp4IfInfo        *pIfEntry = NULL;
    INT4                i4RetStatus;
    UINT1               u1OldOperStatus;

    pIfEntry = Bgp4IfGetEntry (u4IfIndex);

    /* If the interface entry is not there in BGP and the status is not
     * enable, then return failure
     */
    if ((pIfEntry == NULL) && (u4OperStatus != BGP4_IPIF_OPER_ENABLE))
    {
        return BGP4_FAILURE;
    }

    /* If the interface entry is not there in BGP and the status is enable
     * then create the entry in BGP
     */
    if ((pIfEntry == NULL) && (u4OperStatus == BGP4_IPIF_OPER_ENABLE))
    {
        i4RetStatus = Bgp4IfCreate (u4IfIndex);
        if (i4RetStatus == BGP4_SUCCESS)
        {
            pIfEntry = Bgp4IfGetEntry (u4IfIndex);
            /* Handle the interface up */
            if (NULL != pIfEntry)
            {
                i4RetStatus = Bgp4ProcessIfaceUp (pIfEntry);
            }
            else
            {
                return BGP4_FAILURE;
            }
        }
        return i4RetStatus;
    }
    else if ((pIfEntry != NULL) && (u4OperStatus == BGP4_IPIF_OPER_ENABLE))
    {
        u1OldOperStatus = BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry);
        /* Get all the interface related information from IP */
        Bgp4GetIfInfoFromIp (u4IfIndex, pIfEntry);
        BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) = u1OldOperStatus;
    }
#ifdef IP_WANTED
    /* If there is no change in ther operatinal status then return */
    if (BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) == ((UINT1) u4OperStatus))
    {
        /* No change in the status */
        return BGP4_SUCCESS;
    }
#endif

    BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) = (UINT1) u4OperStatus;

    if (BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) == BGP4_IPIF_OPER_ENABLE)
    {
        /* Handle the interface up */
        i4RetStatus = Bgp4ProcessIfaceUp (pIfEntry);
    }
    else if (BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) == BGP4_IFACE_DELETE)
    {
        /* Handle the interface delete */
        i4RetStatus = Bgp4ProcessIfaceDown (pIfEntry);
        Bgp4IfDelete (pIfEntry);
    }
    else
    {
        /* Handle the interface down */
        i4RetStatus = Bgp4ProcessIfaceDown (pIfEntry);
    }
    return i4RetStatus;
}

/****************************************************************************/
/* Function Name : Bgp4ProcessIfaceUp                                       */
/* Description   : Handles the interface up                                 */
/* Input(s)      : pIfEntry - interface related information                 */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4ProcessIfaceUp (tBgp4IfInfo * pIfEntry)
{
    UINT4               u4Context = 0;
    /* Get the vrf related interface information */
    u4Context = BGP4_IFACE_ENTRY_VRFID (pIfEntry);
    if (gBgpCxtNode[u4Context] == NULL)
    {
        return BGP4_FAILURE;
    }
    if (BGP4_LOCAL_BGP_ID (u4Context) == BGP4_INV_IPADDRESS)
    {
        Bgp4GetDefaultBgpIdentifier (u4Context,
                                     &(BGP4_LOCAL_BGP_ID (u4Context)));
        if (BGP4_LOCAL_BGP_ID (u4Context) != BGP4_INV_IPADDRESS)
        {
            Bgp4IfacePeerAdminChange (BGP4_IFACE_ENTRY_INDEX (pIfEntry),
                                      BGP4_PEER_START);
        }
        else
        {
            Bgp4IfacePeerAdminChange (BGP4_IFACE_ENTRY_INDEX (pIfEntry),
                                      BGP4_PEER_STOP);
        }
    }
    else
    {
        Bgp4IfacePeerAdminChange (BGP4_IFACE_ENTRY_INDEX (pIfEntry),
                                  BGP4_PEER_START);
    }
    if (BGP4_NH_CHG_INTERVAL (u4Context) <
        BGP4_NH_CHG_PRCS_INTERVAL (u4Context))
    {
        /* Shedule the Nexthop Resolution if not. If Already shedule by
         * Dispatcher, Need not to shedule again
         */
        BGP4_NH_CHG_INTERVAL (u4Context) =
            BGP4_NH_CHG_PRCS_INTERVAL (u4Context);
        Bgp4ProcessNextHopResolution (u4Context);
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4ProcessIfaceDown                                     */
/* Description   : Handles the interface down                               */
/* Input(s)      : pIfEntry - interface related information                 */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4ProcessIfaceDown (tBgp4IfInfo * pIfEntry)
{
    UINT4               u4Context = 0;

    u4Context = BGP4_IFACE_ENTRY_VRFID (pIfEntry);
    if (gBgpCxtNode[u4Context] == NULL)
    {
        return BGP4_FAILURE;
    }
    Bgp4IfacePeerAdminChange (BGP4_IFACE_ENTRY_INDEX (pIfEntry),
                              BGP4_PEER_STOP);
    if (BGP4_NH_CHG_INTERVAL (u4Context) <
        BGP4_NH_CHG_PRCS_INTERVAL (u4Context))
    {
        /* Shedule the Nexthop Resolution if not. If Already shedule by
         * Dispatcher, Need not to shedule again*/
        BGP4_NH_CHG_INTERVAL (u4Context) =
            BGP4_NH_CHG_PRCS_INTERVAL (u4Context);
        Bgp4ProcessNextHopResolution (u4Context);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DeleteAllInterfaceInfo                               */
/* Description   : Deletes all the interfaces and releases the memory       */
/* Input(s)      : None                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4DeleteAllInterfaceInfo (UINT4 u4Context)
{
    tBgp4IfInfo        *pIfEntry = NULL;
    tBgp4IfInfo        *pTmpIfEntry = NULL;

    BGP_SLL_DYN_Scan (BGP4_IFACE_GLOBAL_LIST, pIfEntry, pTmpIfEntry,
                      tBgp4IfInfo *)
    {
        if (pIfEntry->u4VrfId != u4Context)
        {
            continue;
        }
        if (BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) == BGP4_IFACE_UP)
        {
            /* If the interface is operationally up, then made it down */
            Bgp4IfacePeerAdminChange (BGP4_IFACE_ENTRY_INDEX (pIfEntry),
                                      BGP4_PEER_STOP);
        }
        Bgp4IfDelete (pIfEntry);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4IfCreate                                             */
/* Description   : Function to add an interface entry into global list       */
/* Input(s)      : u4IfIndex    -  interface Index                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IfCreate (UINT4 u4IfIndex)
{
    tBgp4IfInfo        *pIfEntry = NULL;
    INT4                i4RetVal;

    pIfEntry = Bgp4MemAllocateIfEntry (sizeof (tBgp4IfInfo));
    if (pIfEntry == NULL)
    {
        /* There is no memory available to store the interface information. */
        return BGP4_FAILURE;
    }

    /* Get all the interface related information from IP */
    i4RetVal = Bgp4GetIfInfoFromIp (u4IfIndex, pIfEntry);
    if (i4RetVal == BGP4_FAILURE)
    {
        Bgp4MemReleaseIfEntry (pIfEntry);
        return BGP4_FAILURE;
    }
    TMO_SLL_Add (BGP4_IFACE_GLOBAL_LIST, &pIfEntry->NextIfNode);
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4IfDelete                                             */
/* Description   : Function to delete interface entry from global list      */
/* Input(s)      : pIfEntry - interface entry information                   */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4IfDelete (tBgp4IfInfo * pIfEntry)
{
    /* 
     * We should release the label also if we've got
     */
    TMO_SLL_Delete (BGP4_IFACE_GLOBAL_LIST, &pIfEntry->NextIfNode);
    Bgp4MemReleaseIfEntry (pIfEntry);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4IfGetEntry                                            */
/* Description   : Function to get an interface entry from global list       */
/* Input(s)      : u4IfIndex     - interface index                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : pIfEntry - if present, else NULL                          */
/*****************************************************************************/
tBgp4IfInfo        *
Bgp4IfGetEntry (UINT4 u4IfIndex)
{
    tBgp4IfInfo        *pIfEntry = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pIfEntry, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_ENTRY_INDEX (pIfEntry) == u4IfIndex)
        {
            /* Entry found and return */
            return pIfEntry;
        }
    }
    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4GetIfAddrFromIfIndex                       */
/* Description     : Gets the interface address for a given if index*/
/* Input(s)        : u4IfIndex - interface inded                    */
/* Output(s)       : pu4IfAddr - interface address                  */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4GetIfAddrFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4IfAddr)
{
    tBgp4IfInfo        *pIfInfo = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pIfInfo, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_INDEX (pIfInfo) == u4IfIndex)
        {
            *pu4IfAddr = BGP4_IFACE_ADDR (pIfInfo);
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4GetIfInfoFromIfIndex                       */
/* Description     : Gets the interface address for a given if index*/
/* Input(s)        : u4IfIndex - interface inded                    */
/* Output(s)       : pu4IfAddr - interface address                  */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4GetIfInfoFromIfIndex (UINT4 u4IfIndex, tBgp4IfInfo ** ppIfInfo)
{
    tBgp4IfInfo        *pTmpIfInfo = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pTmpIfInfo, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_INDEX (pTmpIfInfo) == u4IfIndex)
        {
            *ppIfInfo = pTmpIfInfo;
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4GetIfIndexFromIfAddr                       */
/* Description     : Gets the interface index for a given if address*/
/* Input(s)        : u4Addr - interface address                     */
/* Output(s)       : pu4IfIndex - interface index                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4GetIfIndexFromIfAddr (UINT4 u4Addr, UINT4 *pu4IfIndex)
{
    tBgp4IfInfo        *pIfInfo = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pIfInfo, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_ADDR (pIfInfo) == u4Addr)
        {
            *pu4IfIndex = BGP4_IFACE_INDEX (pIfInfo);
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4GetIfInfoFromIfAddr                        */
/* Description     : Gets the interface index for a given if address*/
/* Input(s)        : u4Addr - interface address                     */
/* Output(s)       : ppIfInfo   - Interface Info                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4GetIfInfoFromIfAddr (UINT4 u4Addr, tBgp4IfInfo ** ppIfInfo)
{
    tBgp4IfInfo        *pTmpIfInfo = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pTmpIfInfo, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_ADDR (pTmpIfInfo) == u4Addr)
        {
            *ppIfInfo = pTmpIfInfo;
            return BGP4_SUCCESS;
        }
    }
    *ppIfInfo = NULL;
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : BgpLock                                        */
/* Description     : Take the BGP4 protocol semaphore               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : SNMP_SUCCESS or SNMP_FAILURE                   */
/********************************************************************/
INT4
BgpLock ()
{
    if (OsixSemTake (gBgpNode.Bgp4ProtoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/********************************************************************/
/* Function Name   : BgpUnLock                                      */
/* Description     : Releases the BGP protocol semaphore            */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : SNMP_SUCCESS                                   */
/********************************************************************/
INT4
BgpUnLock ()
{
    OsixSemGive (gBgpNode.Bgp4ProtoSemId);
    return SNMP_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4RedistLock                                 */
/* Description     : Take the semaphore for redistribution list     */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4RedistLock ()
{
    if (OsixSemTake (gBgpNode.Bgp4RedistListSemId) == OSIX_FAILURE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4RedistUnLock                               */
/* Description     : Releases the semaphore for redistribution list */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Returns         : BGP4_SUCCESS                                   */
/********************************************************************/
INT4
Bgp4RedistUnLock ()
{
    OsixSemGive (gBgpNode.Bgp4RedistListSemId);
    return BGP4_SUCCESS;
}

VOID
Bgp4InitFiltering (UINT4 u4CxtId)
{
#ifdef ROUTEMAP_WANTED
    MEMSET (&(gBgpCxtNode[u4CxtId]->DistributeInFilterRMap), 0,
            sizeof (tFilteringRMap));
    MEMSET (&(gBgpCxtNode[u4CxtId]->DistributeOutFilterRMap), 0,
            sizeof (tFilteringRMap));
    MEMSET (&(gBgpCxtNode[u4CxtId]->DistanceFilterRMap), 0,
            sizeof (tFilteringRMap));
#endif /* ROUTEMAP_WANTED */
    gBgpCxtNode[u4CxtId]->u1Distance = BGP_DEFAULT_PREFERENCE;

    return;
}

#ifdef ROUTEMAP_WANTED
/*****************************************************************************/
/* Function     : Bgp4SendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This callback function is invoked from Route Map module    */
/*                if route map chamged. It posts message and event to BGP4   */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : BGP4_SUCCESS if Route Map update Msg Sent to BGP4          */
/*                successfully                                               */
/*                BGP4_FAILURE otherwise                                     */
/*****************************************************************************/
INT4
Bgp4SendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tBgp4QMsg          *pQMsg = NULL;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Len = 0;

    if (pu1RMapName == NULL)
    {
        return BGP4_FAILURE;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Peer Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return BGP4_FAILURE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTEMAP_STATUS_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    u4Len = (STRLEN (pu1RMapName) < sizeof (au1NameBuf)) ?
        STRLEN (pu1RMapName) : (sizeof (au1NameBuf) - 1);
    STRNCPY (au1NameBuf, pu1RMapName, u4Len);
    au1NameBuf[u4Len] = '\0';

    MEMCPY (pQMsg->QMsg.Bgp4RMapStatusMsg.au1RMapName,
            au1NameBuf, RMAP_MAX_NAME_LEN);

    pQMsg->QMsg.Bgp4RMapStatusMsg.u4Status = u4Status;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function     : Bgp4ProcessRMapHandler                                     */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Bgp4ProcessRMapHandler (tBgp4QMsg * pQMsg)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Context = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    if (pQMsg == NULL)
    {
        return;
    }

    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    MEMCPY (au1RMapName, pQMsg->QMsg.Bgp4RMapStatusMsg.au1RMapName,
            RMAP_MAX_NAME_LEN);

    u4Status = pQMsg->QMsg.Bgp4RMapStatusMsg.u4Status;

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }

    i4RetVal = (BgpGetFirstActiveContextID (&u4Context));
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (STRLEN
            (gBgpCxtNode[u4Context]->DistributeInFilterRMap.
             au1DistInOutFilterRMapName) != 0)
        {
            if (STRCMP
                (gBgpCxtNode[u4Context]->DistributeInFilterRMap.
                 au1DistInOutFilterRMapName, au1RMapName) == 0)
            {
                gBgpCxtNode[u4Context]->DistributeInFilterRMap.u1Status =
                    u1Status;
            }
        }

        if (STRLEN
            (gBgpCxtNode[u4Context]->DistributeOutFilterRMap.
             au1DistInOutFilterRMapName) != 0)
        {
            if (STRCMP
                (gBgpCxtNode[u4Context]->DistributeOutFilterRMap.
                 au1DistInOutFilterRMapName, au1RMapName) == 0)
            {
                gBgpCxtNode[u4Context]->DistributeOutFilterRMap.u1Status =
                    u1Status;
            }
        }

        if (STRLEN
            (gBgpCxtNode[u4Context]->DistanceFilterRMap.
             au1DistInOutFilterRMapName) != 0)
        {
            if (STRCMP
                (gBgpCxtNode[u4Context]->DistanceFilterRMap.
                 au1DistInOutFilterRMapName, au1RMapName) == 0)
            {
                gBgpCxtNode[u4Context]->DistanceFilterRMap.u1Status = u1Status;
            }
        }
        /* update the status of Filtermap in peerentries */
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if (MEMCMP
                (pPeer->FilterInRMap.au1DistInOutFilterRMapName, au1RMapName,
                 sizeof (au1RMapName)) == 0)
            {
                pPeer->FilterInRMap.u1Status = u1Status;
                pPeer->shadowPeerConfig.FilterInRMap.u1Status =
                    pPeer->FilterInRMap.u1Status;
            }
            if (MEMCMP
                (pPeer->FilterOutRMap.au1DistInOutFilterRMapName, au1RMapName,
                 sizeof (au1RMapName)) == 0)
            {
                pPeer->FilterOutRMap.u1Status = u1Status;
                pPeer->shadowPeerConfig.FilterOutRMap.u1Status =
                    pPeer->FilterOutRMap.u1Status;
            }
            if (MEMCMP
                (pPeer->IpPrefixFilterIn.au1DistInOutFilterRMapName,
                 au1RMapName, sizeof (au1RMapName)) == 0)
            {
                pPeer->IpPrefixFilterIn.u1Status = u1Status;
                pPeer->shadowPeerConfig.IpPrefixFilterIn.u1Status =
                    pPeer->IpPrefixFilterIn.u1Status;
            }
            if (MEMCMP
                (pPeer->IpPrefixFilterOut.au1DistInOutFilterRMapName,
                 au1RMapName, sizeof (au1RMapName)) == 0)
            {
                pPeer->IpPrefixFilterOut.u1Status = u1Status;
                pPeer->shadowPeerConfig.IpPrefixFilterOut.u1Status =
                    pPeer->IpPrefixFilterOut.u1Status;
            }
        }

        /* Update the routemap status of the peer group.
         * For the peers, it will be updated separately and not part of 
         * the peer group */
        TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (u4Context), pPeerGroup,
                      tBgpPeerGroupEntry *)
        {
            if (STRCMP
                (pPeerGroup->RMapIn.au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pPeerGroup->RMapIn.u1Status = u1Status;
            }
            if (STRCMP
                (pPeerGroup->RMapOut.au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pPeerGroup->RMapOut.u1Status = u1Status;
            }

            if (MEMCMP
                (pPeerGroup->IpPrefixFilterIn.au1DistInOutFilterRMapName,
                 au1RMapName, sizeof (au1RMapName)) == 0)
            {
                pPeerGroup->IpPrefixFilterIn.u1Status = u1Status;
            }
            if (MEMCMP
                (pPeerGroup->IpPrefixFilterOut.au1DistInOutFilterRMapName,
                 au1RMapName, sizeof (au1RMapName)) == 0)
            {
                pPeerGroup->IpPrefixFilterOut.u1Status = u1Status;
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return;
}
#endif /* ROUTEMAP_WANTED */

/******************************************************************************/
/* Function Name : Bgp4DeInitInCxt                                            */
/* Description   : This function frees all the allocated memory for this      */
/*                 context                                                    */
/* Input(s)      : u4Context - Context Id                                     */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4DeInitInCxt (UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmppeer = NULL;
#ifdef RRD_WANTED
    tRtmRegnId          RegnId;
#endif
#ifdef BGP4_IPV6_WANTED
    tRtm6RegnId         rtm6RegnId;
#endif
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tBgpPeerGroupEntry *pTmpPeerGrp = NULL;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
#ifdef BGP4_IPV6_WANTED
    MEMSET (&rtm6RegnId, 0, sizeof (tRtm6RegnId));
#endif /* BGP4_IPV6_WANTED */

    if (BGP4_ORF_ENTRY_TABLE (u4Context) != NULL)
    {
        RBTreeDelete (BGP4_ORF_ENTRY_TABLE (u4Context));
        BGP4_ORF_ENTRY_TABLE (u4Context) = NULL;
    }
    RflShutDown (u4Context);
    COMM_LEN_ERROR_STATS (u4Context) = 0;
    EXT_COMM_LEN_ERROR_STATS (u4Context) = 0;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    Bgp4DeleteAllInterfaceInfo (u4Context);

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
    {
        Bgp4DeRegisterWithIP (u4Context);
    }
#endif
    CapsShutDown (u4Context);

#ifdef L3VPN
    /* Delete the import and export route targets */
    Bgp4Vpnv4ReleaseRouteTargetsFromVrf
        (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4Context),
         BGP4_IMPORT_ROUTE_TARGET_TYPE);
    Bgp4Vpnv4ReleaseRouteTargetsFromVrf
        (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4Context),
         BGP4_EXPORT_ROUTE_TARGET_TYPE);
    if (BGP4_VPNV4_LABEL (u4Context) != 0)
    {
        Bgp4DelBgp4Label (BGP4_VPNV4_LABEL (u4Context));
    }
#endif

#ifdef BGP_TCP4_WANTED
    Bgp4TcphCloseListenPort (u4Context, BGP4_INET_AFI_IPV4);
#endif /* BGP_TCP4_WANTED */
#ifdef BGP_TCP6_WANTED
    Bgp4TcphCloseListenPort (u4Context, BGP4_INET_AFI_IPV6);
#endif /* BGP_TCP6_WANTED */
    Bgp4RRDDisableMsgToRTM (u4Context);
    BGP4_RRD_ADMIN_STATE (u4Context) = BGP4_RRD_DISABLE;
    Bgp4RibhClearRIB (u4Context);
/*Clear Network database*/
    BgpClearNetworkprefixDatabase (u4Context);
    Bgp4DshClearLinkList (u4Context, BGP4_FIB_UPD_LIST (u4Context),
                          BGP4_FIB_UPD_LIST_INDEX);
    Bgp4GlobalMemoryRelease (u4Context);

    /* Clear the Aggr Table if any configuration exists */
    Bgp4AggrClearAggrgateTable (u4Context);

    BGP_SLL_DYN_Scan (BGP4_PEERGROUPENTRY_HEAD (u4Context), pPeerGroup,
                      pTmpPeerGrp, tBgpPeerGroupEntry *)
    {
        /* Peers attached to the peer group will be removed from this peer group
         * in the Bgp4SnmphDeletePeerEntry itself. Hence only removing the
         * peer group node here */
        TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (u4Context),
                        &(pPeerGroup->NextGroup));
        MemReleaseMemBlock (gBgpNode.Bgp4PeerGroupPoolId, (UINT1 *) pPeerGroup);
    }

    /* Clear the Igp Metric Hash Table and delete the Next hop metric Table */
    if (BGP4_NEXTHOP_METRIC_TBL (u4Context) != NULL)
    {
        Bgp4DeleteIgpMetricTable (u4Context);
        TMO_HASH_Delete_Table (BGP4_NEXTHOP_METRIC_TBL (u4Context), NULL);
        BGP4_NEXTHOP_METRIC_TBL (u4Context) = NULL;
    }

    /* Clear the Path Attribute Hash Table and delete the
     * Path Attribute Table */
    Bgp4DeleteRcvdPADB (u4Context);

    /* Clear the Path Attribute Hash Table and delete the
     * Path Attribute Table */
    if (BGP4_ADVT_PA_DB (u4Context) != NULL)
    {
        Bgp4DeleteAdvtPADB (u4Context);
    }

    /* Deregister from RTM */
#ifdef RRD_WANTED
    if (gBgpCxtNode[u4Context]->u1RestartMode != BGP4_RESTARTING_MODE)
    {
        RegnId.u2ProtoId = BGP_ID;
        RegnId.u4ContextId = u4Context;
        RtmDeregister (&RegnId);
    }
#endif

#ifdef BGP4_IPV6_WANTED
    if (gBgpCxtNode[u4Context]->u1RestartMode != BGP4_RESTARTING_MODE)
    {
        rtm6RegnId.u2ProtoId = BGP6_ID;
        rtm6RegnId.u4ContextId = u4Context;
        Rtm6Deregister (&rtm6RegnId);
    }
#endif
    BGP4_LOCAL_ADMIN_STATUS (u4Context) = BGP4_ADMIN_DOWN;

    /* Initialize the global variables here */
    BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT (u4Context) = 0;
    Bgp4RtRefShutdown (u4Context);

    /* Clear the BGP-TRIE. */
    Bgp4TrieDelete (BGP4_ROUTE_TRIE, BGP4_IPV4_UNI_INDEX, u4Context,
                    &BGP4_IPV4_RIB_TREE (u4Context));
#ifdef BGP4_IPV6_WANTED
    Bgp4TrieDelete (BGP4_ROUTE_TRIE, BGP4_IPV6_UNI_INDEX, u4Context,
                    &(BGP4_IPV6_RIB_TREE (u4Context)));
#endif
#ifdef L3VPN
    Bgp4TrieDelete (BGP4_ROUTE_TRIE, BGP4_VPN4_UNI_INDEX, u4Context,
                    &(BGP4_VPN4_RIB_TREE (u4Context)));
#endif
#ifdef VPLSADS_WANTED
    Bgp4TrieDelete (BGP4_ROUTE_TRIE, BGP4_L2VPN_VPLS_INDEX, u4Context,
                    &(BGP4_VPLS_RIB_TREE (u4Context)));
#endif

    /* Delete all peer entries */
    pPeer = NULL;
    BGP_SLL_DYN_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, pTmppeer,
                      tBgp4PeerEntry *)
    {
        Bgp4DshReleasePeerRtLists (pPeer, BGP4_FALSE);
        Bgp4SnmphDeletePeerEntry (pPeer);
    }

    /* Deallocate the peer-data list */
    Bgp4DeallocatePeerDataList (BGP4_PEER_DATA_PEER_PTR (u4Context));
    return (BGP4_SUCCESS);
}

#ifdef EVPN_WANTED
/*******************************************************************************/
/* Function Name : Bgp4EvpnASNRegister                                         */
/* Description   : This API is exposed to BGP to register call back functuion  */
/* Input(s)      : pBgp4EvpnRegisterASNInfo contains function pointer          */
/* Output(s)     : None.                                                       */
/* Return(s)     : VOID                                                        */
/*******************************************************************************/
VOID
Bgp4EvpnASNRegister (tBgp4RegisterASNInfo * pBgp4EvpnRegisterASNInfo)
{

    BGP4_EVPN_REGISTER_ASN_INFO (BGP4_EVPN_GLOBAL_PARAMS).pBgp4NotifyASN =
        pBgp4EvpnRegisterASNInfo->pBgp4NotifyASN;

    if ((BGP4_FOUR_BYTE_ASN_SUPPORT (BGP4_DFLT_VRFID) ==
         BGP4_ENABLE_4BYTE_ASN_SUPPORT) &&
        ((gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno) <= BGP4_MAX_AS))
    {
        Bgp4IndicateEvpnASNUpdate (BGP4_ENABLE_4BYTE_ASN_SUPPORT,
                                   (gBgpCxtNode[BGP4_DFLT_VRFID]->
                                    u4BGP4LocalASno));
    }
    else if ((BGP4_FOUR_BYTE_ASN_SUPPORT (BGP4_DFLT_VRFID) ==
              BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
             ((gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno) <=
              BGP4_MAX_TWO_BYTE_AS))
    {
        Bgp4IndicateEvpnASNUpdate (BGP4_DISABLE_4BYTE_ASN_SUPPORT,
                                   (gBgpCxtNode[BGP4_DFLT_VRFID]->
                                    u4BGP4LocalASno));
    }

}

/**************************************************************************/
/* Function Name   : BgpVxlanGetMACNotification                           */
/* Description     : Call back function for EVPN route params NOTIFY      */
/* Input(s)        : u4VrfId - VRF identifier                             */
/*                 : u4VrfStatus - new status                             */
/* Output(s)       : None.                                                */
/* Returns         : None.                                                */
/**************************************************************************/
VOID
Bgp4VxlanEvpnGetMACNotification (UINT4 u4VrfId, UINT4 u4VNId)
{
    tBgp4QMsg          *pQMsg = NULL;
    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP route params Chng Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EVPN_GET_MAC_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_VXLAN_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    BGP4_VXLAN_INPUTQ_VNI_ID (pQMsg) = u4VNId;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name : Bgp4IndicateEvpnASNUpdate                                 */
/* Description   : Pointer to Call back function which is used to notify ASN */
/*                 update to L2VPN                                           */
/* Input(s)      : ASN Type and ASN Value                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
VOID
Bgp4IndicateEvpnASNUpdate (UINT1 u1ASNType, UINT4 u4ASNValue)
{
    (*(BGP4_EVPN_REGISTER_ASN_INFO (BGP4_EVPN_GLOBAL_PARAMS).pBgp4NotifyASN))
        (u1ASNType, u4ASNValue);
}
#endif /* EVPN_WANTED */

/*****************************************************************************/
/* Function Name : Bgp4RouteChngHandler                                      */
/* Description   : Pointer to Call back function which is used to notify     */
/*                 any route updatation in RTM                               */
/* Input(s)      : pMsg                                                      */
/* Output(s)     : None.                                                     */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
VOID
Bgp4RouteChngHandler (tBgp4QMsg * pMsg)
{
    UINT4               u4Context;
    UINT4               u4Protocol = 0;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tNetworkAddr       *pNetworkRt = NULL;
    tNetworkAddr        NetworkAddr;
    tNetworkAddr        NetworkRoute;
    tLinkNode          *pScanLinkNode = NULL;
    tNetAddress         IpAddr;
    UINT4               u4Ipaddr = 0;
    UINT4               u4Mask;
    UINT4               u4Addr;

    UINT4               u4Bgp4PrefixLen = 0;
    UINT2               u2PrefixLen = 0;
    UINT2               u2Afi = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4RtFound = 0;

    if (BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        return;
    }

    if (gBgpNode.u4BgpNetworkTempTmrRunning == OSIX_TRUE)
    {
        return;
    }

    MEMSET (&NetworkRoute, 0, sizeof (tNetworkAddr));
    MEMSET (&NetworkAddr, 0, sizeof (tNetworkAddr));
    MEMSET (&IpAddr, 0, sizeof (tNetAddress));

    u2Afi = BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INPUTQ_ROUTE_ADDR_INFO (pMsg));

    Bgp4CopyAddrPrefixStruct (&(NetworkAddr.NetworkAddr),
                              BGP4_INPUTQ_ROUTE_ADDR_INFO (pMsg));
    u2PrefixLen = BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO (pMsg);

    if (u2Afi == BGP4_INET_AFI_IPV4)
    {
        PTR_FETCH4 (u4Ipaddr, NetworkAddr.NetworkAddr.au1Address);
        PTR_ASSIGN_4 ((IpAddr.NetAddr.au1Address), u4Ipaddr);
    }
    else if (u2Afi == BGP4_INET_AFI_IPV6)
    {
        MEMCPY (IpAddr.NetAddr.au1Address, NetworkAddr.NetworkAddr.au1Address,
                BGP4_IPV6_PREFIX_LEN);
    }
    IpAddr.NetAddr.u2Afi = u2Afi;
    IpAddr.NetAddr.u2AddressLen = NetworkAddr.NetworkAddr.u2AddressLen;
    IpAddr.u2PrefixLen = u2PrefixLen;
    IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;

    i4RetVal = (BgpGetFirstActiveContextID (&u4Context));

    while (i4RetVal == SNMP_SUCCESS)
    {

        if (BGP4_INET_AFI_IPV4 ==
            BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INPUTQ_ROUTE_ADDR_INFO (pMsg)))
        {
            u4Bgp4PrefixLen = BGP4_IPV4_PREFIX_LEN;
        }
        else if (BGP4_INET_AFI_IPV6 ==
                 BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INPUTQ_ROUTE_ADDR_INFO
                                               (pMsg)))
        {
            u4Bgp4PrefixLen = BGP4_IPV6_PREFIX_LEN;
        }

        if (BGP4_INPUTQ_ROUTE_ACTION (pMsg) == BGP4_IMPORT_RT_ADD)
        {
            pNetworkRt =
                Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr,
                                          &NetworkRoute);
            if (pNetworkRt != NULL)
            {
                if ((pNetworkRt->u2PrefixLen == u2PrefixLen)
                    && (pNetworkRt->u4RowStatus == ACTIVE))
                {
                    pRtProfile =
                        Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
                    if (pRtProfile == NULL)
                    {
                        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC |
                                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                       BGP4_MOD_NAME,
                                       "\tMemory Allocation for Redistributed Route "
                                       "\n\t- Dest %s, Mask %s Protocol %d FAILED!!!\n",
                                       Bgp4PrintIpAddr (IpAddr.NetAddr.
                                                        au1Address,
                                                        IpAddr.NetAddr.u2Afi),
                                       Bgp4PrintIpMask ((UINT1) IpAddr.
                                                        u2PrefixLen,
                                                        IpAddr.NetAddr.u2Afi),
                                       u4Protocol);
                        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
                        return;
                    }
                    /* Assign the <AFI, SAFI> and prefix/length */
                    switch (u2Afi)
                    {
                        case BGP4_INET_AFI_IPV4:
                        {
                            u4Mask = Bgp4GetSubnetmask
                                ((UINT1)
                                 BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (IpAddr));
                            PTR_FETCH_4 (u4Addr,
                                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (IpAddr)));
                            u4Addr = u4Addr & u4Mask;

                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                                u2Afi;
                            BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                                BGP4_IPV4_PREFIX_LEN;
                            BGP4_SAFI_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)) =
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (IpAddr);
                            PTR_ASSIGN_4 (BGP4_RT_IP_PREFIX (pRtProfile),
                                          u4Addr);
                            BGP4_RT_PREFIXLEN (pRtProfile) =
                                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (IpAddr);
                            u4Protocol = BGP4_INPUTQ_ROUTE_PROTO_ID (pMsg);
                            if ((u4Protocol == BGP4_LOCAL_ID)
                                || (u4Protocol == BGP4_RIP_ID)
                                || (u4Protocol == BGP4_OSPF_ID)
                                || (u4Protocol == BGP4_ISIS_ID)
                                || (u4Protocol == BGP4_STATIC_ID))
                            {
                                Bgp4CopyAddrPrefixStruct (&
                                                          (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                                           (pRtProfile)),
                                                          BGP4_INPUTQ_ROUTE_NEXTHOP_INFO
                                                          (pMsg));
                            }
                            break;
                        }
#ifdef BGP4_IPV6_WANTED
                        case BGP4_INET_AFI_IPV6:
                        {
                            tIp6Addr            IPAddr;
                            tIp6Addr            IPAddr1;
                            MEMCPY (IPAddr1.u1_addr,
                                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                     (IpAddr)), BGP4_IPV6_PREFIX_LEN);
                            Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                             BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO
                                             (IpAddr));
                            /* Assign the <AFI, SAFI> and prefix/length */
                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                                u2Afi;
                            BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                                BGP4_IPV6_PREFIX_LEN;
                            BGP4_SAFI_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)) =
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (IpAddr);
                            MEMCPY (BGP4_RT_IP_PREFIX (pRtProfile),
                                    IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
                            BGP4_RT_PREFIXLEN (pRtProfile) =
                                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (IpAddr);
                            break;
                        }
#endif
                        default:
                        {
                            /* rightnow there is no support for any other address
                             * family, so return FAILURE
                             */
                            Bgp4DshReleaseRtInfo (pRtProfile);
                            return;
                        }
                    }

                    pRtProfile->pBgpCxtNode = Bgp4GetContextEntry (u4Context);
                    BGP4_RT_PROTOCOL (pRtProfile) =
                        (UINT1) BGP4_INPUTQ_ROUTE_PROTO_ID (pMsg);
                    BGP4_RT_METRIC_TYPE (pRtProfile) = 0;
                    BGP4_RT_GET_RT_IF_INDEX (pRtProfile) =
                        BGP4_INPUTQ_ROUTE_IF_INDEX (pMsg);
                    BGP4_RT_SET_ADV_FLAG (pRtProfile, BGP4_NETWORK_ADV_ROUTE);
                    i4RtFound =
                        Bgp4RibhLookupRtEntry (pRtProfile,
                                               BGP4_RT_CXT_ID (pRtProfile),
                                               BGP4_TREE_FIND_EXACT,
                                               &pFndRtProfileList, &pRibNode);
                    TMO_SLL_Scan (BGP4_REDIST_LIST (u4Context), pScanLinkNode,
                                  tLinkNode *)
                    {
                        pRouteProfile = pScanLinkNode->pRouteProfile;

                        if (((BGP4_RT_GET_ADV_FLAG (pRouteProfile) &
                              BGP4_NETWORK_ADV_ROUTE) == BGP4_NETWORK_ADV_ROUTE)
                            &&
                            ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                              BGP4_NETWORK_ADV_ROUTE) ==
                             BGP4_NETWORK_ADV_ROUTE))
                        {
                            if ((MEMCMP
                                 (pRouteProfile->NetAddress.NetAddr.au1Address,
                                  pRtProfile->NetAddress.NetAddr.au1Address,
                                  u4Bgp4PrefixLen) == 0)
                                && (pRouteProfile->NetAddress.u2PrefixLen ==
                                    pRtProfile->NetAddress.u2PrefixLen)
                                && (pRtProfile->NetAddress.NetAddr.u2Afi ==
                                    pRouteProfile->NetAddress.NetAddr.u2Afi))
                            {
                                if ((BGP4_RT_GET_FLAGS (pRouteProfile) &
                                     BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)
                                {
                                    BGP4_RT_RESET_FLAG (pRouteProfile,
                                                        BGP4_RT_WITHDRAWN);
                                }
                                Bgp4DshReleaseRtInfo (pRtProfile);
                                return;
                            }
                        }
                    }

                    if ((i4RtFound == BGP4_FAILURE)
                        ||
                        ((BGP4_RT_PROTOCOL (pFndRtProfileList) !=
                          BGP4_RT_PROTOCOL (pRtProfile))
                         && (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)))
                    {
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC |
                                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tAdding the route %s mask %s to RIB since network route "
                                       "matches with the route.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)));
                        Bgp4ImportRouteAdd (u4Context, IpAddr,
                                            BGP4_INPUTQ_ROUTE_PROTO_ID (pMsg),
                                            BGP4_INPUTQ_ROUTE_NEXTHOP_INFO
                                            (pMsg), BGP4_INVALID_ROUTE_TAG,
                                            BGP4_INPUTQ_ROUTE_METRIC (pMsg),
                                            BGP4_INPUTQ_ROUTE_IF_INDEX (pMsg),
                                            0, 0, NULL, BGP4_NETWORK_ADV_ROUTE,
                                            BGP4_FALSE);
                    }
                    Bgp4DshReleaseRtInfo (pRtProfile);
                }
            }
        }
        else if (BGP4_INPUTQ_ROUTE_ACTION (pMsg) == BGP4_IMPORT_RT_DELETE)
        {
            pNetworkRt =
                Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr,
                                          &NetworkRoute);
            if (pNetworkRt != NULL)
            {
                TMO_SLL_Scan (BGP4_REDIST_LIST (u4Context), pScanLinkNode,
                              tLinkNode *)
                {
                    pRouteProfile = pScanLinkNode->pRouteProfile;

                    if ((BGP4_RT_GET_ADV_FLAG (pRouteProfile) &
                         BGP4_NETWORK_ADV_ROUTE) == BGP4_NETWORK_ADV_ROUTE)
                    {
                        if ((MEMCMP
                             (pRouteProfile->NetAddress.NetAddr.au1Address,
                              pNetworkRt->NetworkAddr.au1Address,
                              u4Bgp4PrefixLen) == 0)
                            && (pRouteProfile->NetAddress.u2PrefixLen ==
                                pNetworkRt->u2PrefixLen)
                            && (pNetworkRt->NetworkAddr.u2Afi ==
                                pRouteProfile->NetAddress.NetAddr.u2Afi))
                        {
                            if ((BGP4_RT_GET_FLAGS (pRouteProfile) &
                                 BGP4_RT_WITHDRAWN) != BGP4_RT_WITHDRAWN)
                            {
                                BGP4_RT_SET_FLAG (pRouteProfile,
                                                  BGP4_RT_WITHDRAWN);
                            }
                            return;
                        }
                    }
                }

                if ((pNetworkRt->u2PrefixLen == u2PrefixLen)
                    && (pNetworkRt->u4RowStatus == ACTIVE))
                {
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tDeleting the route %s mask %s from RIB since the matching "
                                   "network route is deleted.\n",
                                   Bgp4PrintIpAddr (pNetworkRt->NetworkAddr.
                                                    au1Address,
                                                    pNetworkRt->NetworkAddr.
                                                    u2Afi),
                                   Bgp4PrintIpMask ((UINT1) pNetworkRt->
                                                    u2PrefixLen,
                                                    pNetworkRt->NetworkAddr.
                                                    u2Afi));
                    Bgp4ImportRouteDelete (u4Context, IpAddr,
                                           BGP4_INPUTQ_ROUTE_PROTO_ID (pMsg),
                                           BGP4_INPUTQ_ROUTE_NEXTHOP_INFO
                                           (pMsg),
                                           BGP4_INPUTQ_ROUTE_IF_INDEX (pMsg),
                                           BGP4_NETWORK_ADV_ROUTE);

                    /* Check for the alternate route and install in RIB if exists */
                    BgpNetworkIpRtLookup (u4Context, &(NetworkAddr.NetworkAddr),
                                          pNetworkRt->u2PrefixLen,
                                          BGP4_NETWORK_ROUTE_ADD,
                                          BGP4_ALL_APPIDS, 0, NULL);
                }
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);

    }
}

/*****************************************************************************/
/* Function Name : BgpClearNetworkprefixDatabase                             */
/* Description   : This function is used to clear the network database       */
/* Input(s)      : pMsg                                                      */
/* Output(s)     : None.                                                     */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
VOID
BgpClearNetworkprefixDatabase (UINT4 u4Context)
{
    tNetworkAddr       *pNetworkRt = NULL;

    pNetworkRt = (tNetworkAddr *)
        RBTreeGetFirst (BGP4_NETWORK_ROUTE_DATABASE (u4Context));
    while (pNetworkRt != NULL)
    {
        if (RBTreeRemove (BGP4_NETWORK_ROUTE_DATABASE (u4Context),
                          (tRBElem *) pNetworkRt) != RB_SUCCESS)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - RB Deletion Failure!!!!.\n");
        }
        MemReleaseMemBlock (gBgpNode.Bgp4NetworkRtPoolId, (UINT1 *) pNetworkRt);
        pNetworkRt = (tNetworkAddr *)
            RBTreeGetFirst (BGP4_NETWORK_ROUTE_DATABASE (u4Context));
    }
}

#endif /* BGP4MAIN_C */
