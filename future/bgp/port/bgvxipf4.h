/* $Id: bgvxipf4.h,v 1.4 2014/11/18 13:38:26 siva Exp $ */
#ifndef BGVXIPF4_H
#define BGVXIPF4_H

#include "bgp4dfs.h"
#include "bgp4tdfs.h"
#include "bgp4rib.h"
#include "bgp4port.h"


/* Prototype definitions */
INT4  BgpIp4RtLookup (UINT4 , UINT2 , UINT4 *, INT4 *, UINT4 *, INT1 );
INT4  BGP4CanRepIpv4RtAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4  BGP4RTAddRouteToCommIp4RtTbl(tRouteProfile * );
INT4  BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile * );
INT4  BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile * );
UINT4 BgpGetIpIfAddr (UINT2);
UINT4 BgpGetIpIfMask (UINT2);
INT1  Bgp4GetDefaultBgpIdentifier (UINT4 *);
UINT1 Bgp4Ipv4IsDirectlyConnected (UINT4 );
UINT2 Bgp4Ipv4GetNetAddrPrefixLen (UINT4);
BOOL1 Bgp4Ipv4IsOnSameSubnet (UINT4 , UINT4 );
INT1  Bgp4GetIfAddress (INT4, UINT4 *);
INT4  Bgp4GetIpv4LocalAddrForRemoteAddr( tAddrPrefix , tNetAddress *);
INT4  Bgp4Ipv4GetDefaultRouteFromFDB (VOID);
INT4  IpIsLocalAddrInCxt (UINT4, UINT4, UINT2 *);


#ifdef RRD_WANTED
VOID  Bgp4RegAckMsg (tBgp4QMsg *);
INT4  Bgp4RtmIp4RtLookup (UINT4 , UINT2 ,UINT4 *, INT4 *,
                          UINT4 *, INT1 );
INT4  Bgp4RtmIpv4GetDefaultRoute (VOID);
INT4  Bgp4RtmAddRouteToIp4RtTbl (tRouteProfile * );
INT4  Bgp4RtmDelRouteInIp4RtTbl (tRouteProfile * );
INT4  Bgp4RtmModifyRouteInCommIp4RtTbl (tRouteProfile * );
INT4  Bgp4ChangeNotificationMessageToRTM (tRouteProfile *);
INT1  Bgp4GetInfoFromRTM (UINT4, UINT4, UINT1, UINT2);
INT1  SendingMessageToRRDQueue (UINT4, UINT1);
INT1  Bgp4DisableFilter (VOID);
VOID  Bgp4OriginatingAS (VOID);
INT4  Bgp4RtmMsgHandler (tBgp4QMsg *);
INT4  Bgp4RecvMsgFromRtm (tOsixMsg *,tRtmMsgHdr *);
INT4  Bgp4RtmProcessUpdate (tOsixMsg *,tRtmMsgHdr * );
VOID  UtilExtractRtUpdate (tOsixMsg *, UINT2, tRtmRtUpdateMsg *);
#endif /* RRD_WANTED */


PUBLIC INT1 IpifGetFirstPortInCxt  (UINT4, UINT4, UINT2 *);
PUBLIC INT1 IpifGetNextPort (UINT2, UINT2 *);
PUBLIC INT1 IpifGetNextPortInCxt (UINT4, UINT2, UINT2 *);

#endif /* BGVXIPF4_H */
