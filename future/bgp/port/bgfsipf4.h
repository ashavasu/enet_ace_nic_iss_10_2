/* $Id: bgfsipf4.h,v 1.14 2016/12/27 12:35:54 siva Exp $ */
 

#ifndef BGFSIPF4_H
#define BGFSIPF4_H



#include "bgp4dfs.h"
#include "bgp4tdfs.h"
#ifdef VPLSADS_WANTED
#include "bgp4l2vpn.h"
#endif
#ifdef EVPN_WANTED
#include "bgevpndfs.h"
#endif
#include "bgp4rib.h"
#include "bgp4port.h"


/* Prototype definitions */
INT4  BgpIp4RtLookup (UINT4, UINT4 , UINT2 , UINT4 *, INT4 *, UINT4 *, INT1 );
INT4  BgpIp4GetNextRtEntry (UINT4 , UINT4 , UINT2 , UINT4 ,
                            UINT4 *, INT4 *, UINT4 *);
INT4  BGP4CanRepIpv4RtAddedToFIB (tRouteProfile *, tRouteProfile *);
INT4  BGP4RTAddRouteToCommIp4RtTbl(tRouteProfile *, UINT4 );
INT4  BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile *, UINT4 );
INT4  BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile * );
UINT4 BgpGetIpIfAddr (UINT2 );
UINT4 BgpGetIpIfMask (UINT2);
INT1  Bgp4GetDefaultBgpIdentifier (UINT4, UINT4 *);
UINT1 Bgp4Ipv4IsDirectlyConnected (UINT4, UINT4 );
UINT2 Bgp4Ipv4GetNetAddrPrefixLen (UINT4);
BOOL1 Bgp4Ipv4IsOnSameSubnet (UINT4, UINT4 , UINT4 );
INT4  Bgp4GetIpv4LocalAddrForRemoteAddr(UINT4, tAddrPrefix , tNetAddress *);
INT4  Bgp4Ipv4GetDefaultRouteFromFDB (UINT4);

PUBLIC VOID Bgp4CbIfChgHdlr PROTO ((tNetIpv4IfInfo *, UINT4 ));
#ifdef L3VPN
PUBLIC VOID Bgp4CbVrfChgHdlr PROTO ((UINT1 *, UINT1, UINT1 ));
#endif
VOID Bgp4CbRtChangeHdlr PROTO ((tNetIpv4RtInfo *, UINT1 ));

VOID Bgp4CbRouteChangeHandler PROTO ((tNetIpv4RtInfo *, tNetIpv4RtInfo *, UINT1 ));
extern INT1  IpifGetFirstPortInCxt (UINT4, UINT2 *);
extern INT1  IpifGetNextPortInCxt (UINT4, UINT2, UINT2 *);

/* Declaration for other protocols supported by BGP */
#define  BGP4_OTHERS_ID     OTHERS_ID
#define  BGP4_LOCAL_ID      CIDR_LOCAL_ID           
#define  BGP4_STATIC_ID     CIDR_STATIC_ID
#define  BGP4_RIP_ID        RIP_ID
#define  BGP4_OSPF_ID       OSPF_ID
#define  BGP4_ISIS_ID       ISIS_ID


#endif /* BGFSIPF4_H */
